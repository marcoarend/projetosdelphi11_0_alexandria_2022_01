object FmConfigBol: TFmConfigBol
  Left = 257
  Top = 171
  Caption = 'CFG-BOLET-001 :: Cfg. impress'#227'o de boleto'
  ClientHeight = 627
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 531
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 416
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 84
        Width = 792
        Height = 332
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = ' T'#237'tulo do balancete '
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 304
            Align = alClient
            TabOrder = 0
            object Label29: TLabel
              Left = 4
              Top = 4
              Width = 97
              Height = 13
              Caption = 'T'#237'tulo do Balancete:'
            end
            object Label30: TLabel
              Left = 300
              Top = 4
              Width = 37
              Height = 13
              Caption = 'T.fonte:'
            end
            object Label31: TLabel
              Left = 340
              Top = 4
              Width = 67
              Height = 13
              Caption = 'Margem sup.'#185':'
            end
            object Label32: TLabel
              Left = 340
              Top = 44
              Width = 58
              Height = 13
              Caption = 'Altura linha'#185':'
            end
            object EdTitBTxtTit: TdmkEdit
              Left = 4
              Top = 20
              Width = 293
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'BALANCETE DEMONSTRATIVO -'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'BALANCETE DEMONSTRATIVO -'
            end
            object EdTitBTxtSiz: TdmkEdit
              Left = 300
              Top = 20
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '4'
              ValMax = '72'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '8'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 8
            end
            object RGTitBTxtFmt: TRadioGroup
              Left = 4
              Top = 44
              Width = 333
              Height = 37
              Caption = ' Formata'#231#227'o fonte: '
              Columns = 4
              ItemIndex = 1
              Items.Strings = (
                'Nada'
                'Negrito'
                'It'#225'lico'
                'Ambos')
              TabOrder = 3
            end
            object EdTitBMrgSup: TdmkEdit
              Left = 340
              Top = 20
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdTitBLinAlt: TdmkEdit
              Left = 340
              Top = 60
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '350'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 350
            end
            object RGTitBPerFmt: TRadioGroup
              Left = 410
              Top = 5
              Width = 363
              Height = 77
              Caption = ' Formata'#231#227'o per'#237'odo: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'dd/mm/aa a dd/mm/aa'
                'dd a dd/mm/aa'
                'mmmm/aa'
                'mmm/aa'
                'mm/aa')
              TabOrder = 5
            end
            object GroupBox38: TGroupBox
              Left = 4
              Top = 84
              Width = 769
              Height = 105
              Caption = ' Logotipo: '
              TabOrder = 6
              object Label96: TLabel
                Left = 8
                Top = 16
                Width = 129
                Height = 13
                Caption = 'Caminho do meu logo [F4]: '
              end
              object Label103: TLabel
                Left = 8
                Top = 56
                Width = 62
                Height = 13
                Caption = 'Altura logo'#185'-'#178':'
              end
              object Label98: TLabel
                Left = 92
                Top = 56
                Width = 71
                Height = 13
                Caption = 'Largura logo'#185'-'#178':'
              end
              object EdMeuLogoAlt: TdmkEdit
                Left = 8
                Top = 72
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1800'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1800
              end
              object EdMeuLogoLar: TdmkEdit
                Left = 92
                Top = 72
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '4500'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 4500
              end
              object CkMeuLogoImp: TdmkCheckBox
                Left = 180
                Top = 76
                Width = 117
                Height = 17
                Caption = 'Imprime o logotipo.'
                TabOrder = 3
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object EdMeuLogoArq: TdmkEdit
                Left = 8
                Top = 32
                Width = 753
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnKeyDown = EdMeuLogoArqKeyDown
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Totais de saldos (em todo balancete) '
          ImageIndex = 1
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 784
            Height = 157
            Align = alTop
            Caption = 
              ' Totais de controle de saldos (n'#237'veis do plano de contas, cartei' +
              'ras, etc.): '
            TabOrder = 0
            object Label12: TLabel
              Left = 600
              Top = 28
              Width = 84
              Height = 13
              Caption = 'Margem superior'#185':'
            end
            object Label21: TLabel
              Left = 600
              Top = 68
              Width = 60
              Height = 13
              Caption = 'Altura t'#237'tulo'#185':'
            end
            object Label22: TLabel
              Left = 688
              Top = 68
              Width = 70
              Height = 13
              Caption = 'Altura valores'#185':'
            end
            object Label33: TLabel
              Left = 688
              Top = 28
              Width = 78
              Height = 13
              Caption = 'Margem inferior'#185':'
            end
            object GroupBox2: TGroupBox
              Left = 2
              Top = 15
              Width = 135
              Height = 138
              Caption = ' Tam. fonte: '
              TabOrder = 0
              object Label3: TLabel
                Left = 8
                Top = 20
                Width = 51
                Height = 13
                Caption = 'Descri'#231#227'o:'
              end
              object Label4: TLabel
                Left = 8
                Top = 44
                Width = 27
                Height = 13
                Caption = 'Valor:'
              end
              object EdCxaSTxtSiz: TdmkEdit
                Left = 68
                Top = 16
                Width = 33
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
              end
              object EdCxaSValSiz: TdmkEdit
                Left = 68
                Top = 40
                Width = 33
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
              end
            end
            object GroupBox3: TGroupBox
              Left = 137
              Top = 15
              Width = 304
              Height = 138
              Caption = ' Descri'#231#227'o e tamanho texto (% da linha): '
              TabOrder = 1
              object Label5: TLabel
                Left = 8
                Top = 20
                Width = 61
                Height = 13
                Caption = 'Sdo Anterior:'
              end
              object Label6: TLabel
                Left = 8
                Top = 44
                Width = 45
                Height = 13
                Caption = 'Receitas:'
              end
              object Label7: TLabel
                Left = 8
                Top = 92
                Width = 67
                Height = 13
                Caption = 'Saldo do m'#234's:'
              end
              object Label8: TLabel
                Left = 8
                Top = 68
                Width = 50
                Height = 13
                Caption = 'Despesas:'
              end
              object Label11: TLabel
                Left = 8
                Top = 116
                Width = 52
                Height = 13
                Caption = 'Saldo final:'
              end
              object Label105: TLabel
                Left = 212
                Top = 72
                Width = 68
                Height = 13
                Caption = 'Transfer'#234'ncia:'
              end
              object EdCxaSIniTam: TdmkEdit
                Left = 84
                Top = 16
                Width = 53
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '20,34'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 20.340000000000000000
              end
              object EdCxaSCreTam: TdmkEdit
                Left = 84
                Top = 40
                Width = 53
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '19,49'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 19.490000000000000000
              end
              object EdCxaSDebTam: TdmkEdit
                Left = 84
                Top = 64
                Width = 53
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '19,49'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 19.490000000000000000
              end
              object EdCxaSMovTam: TdmkEdit
                Left = 84
                Top = 88
                Width = 53
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '20,34'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 20.340000000000000000
              end
              object EdCxaSFimTam: TdmkEdit
                Left = 84
                Top = 112
                Width = 53
                Height = 21
                Alignment = taRightJustify
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '20,34'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 20.340000000000000000
              end
              object EdCxaSIniTxt: TdmkEdit
                Left = 140
                Top = 16
                Width = 70
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Sdo Anterior'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Sdo Anterior'
              end
              object EdCxaSCreTxt: TdmkEdit
                Left = 140
                Top = 40
                Width = 70
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Receitas'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Receitas'
              end
              object EdCxaSDebTxt: TdmkEdit
                Left = 140
                Top = 64
                Width = 70
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Despesas'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Despesas'
              end
              object EdCxaSMovTxt: TdmkEdit
                Left = 140
                Top = 88
                Width = 70
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Saldo do m'#234's'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Saldo do m'#234's'
              end
              object EdCxaSFimTxt: TdmkEdit
                Left = 140
                Top = 112
                Width = 70
                Height = 21
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Saldo final'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Saldo final'
              end
              object EdCxaTrnsTxt: TdmkEdit
                Left = 212
                Top = 88
                Width = 70
                Height = 21
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Transf.'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Transf.'
              end
            end
            object GroupBox4: TGroupBox
              Left = 448
              Top = 15
              Width = 147
              Height = 138
              Caption = ' Formata'#231#227'o: '
              TabOrder = 2
              object RGCxaSValFmt: TRadioGroup
                Left = 73
                Top = 15
                Width = 71
                Height = 121
                Align = alLeft
                Caption = ' Valor: '
                ItemIndex = 1
                Items.Strings = (
                  'Nada'
                  'Negrito'
                  'It'#225'lico'
                  'Ambos')
                TabOrder = 1
              end
              object RGCxaSTxtFmt: TRadioGroup
                Left = 2
                Top = 15
                Width = 71
                Height = 121
                Align = alLeft
                Caption = ' Descri'#231#227'o: '
                ItemIndex = 1
                Items.Strings = (
                  'Nada'
                  'Negrito'
                  'It'#225'lico'
                  'Ambos')
                TabOrder = 0
              end
            end
            object EdCxaSMrgSup: TdmkEdit
              Left = 600
              Top = 44
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '150'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 150
            end
            object EdCxaSTxtAlt: TdmkEdit
              Left = 600
              Top = 84
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '300'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 300
            end
            object EdCxaSValAlt: TdmkEdit
              Left = 688
              Top = 84
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '300'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 300
            end
            object EdCxaSMrgInf: TdmkEdit
              Left = 688
              Top = 44
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '100'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 100
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Plano de Contas '
          ImageIndex = 2
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 784
            Height = 304
            ActivePage = TabSheet12
            Align = alClient
            TabOrder = 0
            object TabSheet11: TTabSheet
              Caption = ' Todo plano '
              object PageControl6: TPageControl
                Left = 0
                Top = 0
                Width = 776
                Height = 276
                ActivePage = TabSheet20
                Align = alClient
                TabOrder = 0
                object TabSheet13: TTabSheet
                  Caption = ' Contas '
                  object Panel4: TPanel
                    Left = 0
                    Top = 0
                    Width = 768
                    Height = 248
                    Align = alClient
                    TabOrder = 0
                    object GroupBox6: TGroupBox
                      Left = 161
                      Top = 1
                      Width = 147
                      Height = 246
                      Align = alLeft
                      Caption = ' Formata'#231#227'o: '
                      TabOrder = 1
                      object RGNiv1ValFmt: TRadioGroup
                        Left = 73
                        Top = 15
                        Width = 71
                        Height = 229
                        Align = alLeft
                        Caption = ' Valor: '
                        ItemIndex = 0
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 1
                      end
                      object RGNiv1TxtFmt: TRadioGroup
                        Left = 2
                        Top = 15
                        Width = 71
                        Height = 229
                        Align = alLeft
                        Caption = ' Descri'#231#227'o: '
                        ItemIndex = 0
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 0
                      end
                    end
                    object Panel6: TPanel
                      Left = 1
                      Top = 1
                      Width = 160
                      Height = 246
                      Align = alLeft
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label18: TLabel
                        Left = 8
                        Top = 10
                        Width = 68
                        Height = 13
                        Caption = 'Largura valor'#185':'
                      end
                      object Label19: TLabel
                        Left = 8
                        Top = 34
                        Width = 58
                        Height = 13
                        Caption = 'Altura linha'#185':'
                      end
                      object GroupBox5: TGroupBox
                        Left = 8
                        Top = 59
                        Width = 146
                        Height = 68
                        Caption = ' Tam. fonte: '
                        TabOrder = 2
                        object Label13: TLabel
                          Left = 8
                          Top = 20
                          Width = 51
                          Height = 13
                          Caption = 'Descri'#231#227'o:'
                        end
                        object Label17: TLabel
                          Left = 8
                          Top = 44
                          Width = 27
                          Height = 13
                          Caption = 'Valor:'
                        end
                        object EdNiv1TxtSiz: TdmkEdit
                          Left = 68
                          Top = 16
                          Width = 33
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '4'
                          ValMax = '72'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '7'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 7
                        end
                        object EdNiv1ValSiz: TdmkEdit
                          Left = 68
                          Top = 40
                          Width = 33
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '4'
                          ValMax = '72'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '7'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 7
                        end
                      end
                      object EdNiv1ValLar: TdmkEdit
                        Left = 93
                        Top = 6
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1200'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1200
                      end
                      object EdNiv1LinAlt: TdmkEdit
                        Left = 93
                        Top = 30
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '270'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 270
                      end
                      object CkNiv1Grades: TCheckBox
                        Left = 8
                        Top = 132
                        Width = 97
                        Height = 17
                        Caption = 'Mostra grade.'
                        Checked = True
                        State = cbChecked
                        TabOrder = 3
                      end
                    end
                  end
                end
                object TabSheet14: TTabSheet
                  Caption = ' Sub-grupos '
                  ImageIndex = 1
                  object Panel7: TPanel
                    Left = 0
                    Top = 0
                    Width = 768
                    Height = 248
                    Align = alClient
                    TabOrder = 0
                    object GroupBox9: TGroupBox
                      Left = 1
                      Top = 1
                      Width = 140
                      Height = 246
                      Align = alLeft
                      Caption = ' T'#237'tulo: '
                      TabOrder = 0
                      object Label27: TLabel
                        Left = 8
                        Top = 22
                        Width = 58
                        Height = 13
                        Caption = 'Altura linha'#185':'
                      end
                      object Label28: TLabel
                        Left = 8
                        Top = 46
                        Width = 75
                        Height = 13
                        Caption = 'Tamanho fonte:'
                      end
                      object Label34: TLabel
                        Left = 8
                        Top = 70
                        Width = 84
                        Height = 13
                        Caption = 'Margem superior'#185':'
                      end
                      object EdTit2LinAlt: TdmkEdit
                        Left = 72
                        Top = 18
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '300'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 300
                      end
                      object RGTit2TxtFmt: TRadioGroup
                        Left = 2
                        Top = 179
                        Width = 136
                        Height = 65
                        Align = alBottom
                        Caption = ' Formata'#231#227'o: '
                        Columns = 2
                        ItemIndex = 1
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 3
                      end
                      object EdTit2TxtSiz: TdmkEdit
                        Left = 88
                        Top = 42
                        Width = 45
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '8'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 8
                      end
                      object EdTit2MrgSup: TdmkEdit
                        Left = 96
                        Top = 66
                        Width = 36
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '50'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 50
                      end
                    end
                    object GroupBox10: TGroupBox
                      Left = 141
                      Top = 1
                      Width = 308
                      Height = 246
                      Align = alLeft
                      Caption = ' Sub-total: '
                      TabOrder = 1
                      object GroupBox7: TGroupBox
                        Left = 157
                        Top = 15
                        Width = 147
                        Height = 229
                        Align = alLeft
                        Caption = ' Formata'#231#227'o: '
                        TabOrder = 1
                        object RGSom2ValFmt: TRadioGroup
                          Left = 73
                          Top = 15
                          Width = 71
                          Height = 212
                          Align = alLeft
                          Caption = ' Valor: '
                          ItemIndex = 1
                          Items.Strings = (
                            'Nada'
                            'Negrito'
                            'It'#225'lico'
                            'Ambos')
                          TabOrder = 1
                        end
                        object RGSom2TxtFmt: TRadioGroup
                          Left = 2
                          Top = 15
                          Width = 71
                          Height = 212
                          Align = alLeft
                          Caption = ' Descri'#231#227'o: '
                          ItemIndex = 2
                          Items.Strings = (
                            'Nada'
                            'Negrito'
                            'It'#225'lico'
                            'Ambos')
                          TabOrder = 0
                        end
                      end
                      object Panel8: TPanel
                        Left = 2
                        Top = 15
                        Width = 155
                        Height = 229
                        Align = alLeft
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label23: TLabel
                          Left = 8
                          Top = 6
                          Width = 68
                          Height = 13
                          Caption = 'Largura valor'#185':'
                        end
                        object Label24: TLabel
                          Left = 8
                          Top = 30
                          Width = 58
                          Height = 13
                          Caption = 'Altura linha'#185':'
                        end
                        object EdSom2LinAlt: TdmkEdit
                          Left = 93
                          Top = 26
                          Width = 60
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '300'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 300
                        end
                        object EdSom2ValLar: TdmkEdit
                          Left = 93
                          Top = 2
                          Width = 60
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '1200'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 1200
                        end
                        object GroupBox8: TGroupBox
                          Left = 0
                          Top = 48
                          Width = 154
                          Height = 66
                          Caption = ' Tam. fonte: '
                          TabOrder = 2
                          object Label25: TLabel
                            Left = 8
                            Top = 20
                            Width = 51
                            Height = 13
                            Caption = 'Descri'#231#227'o:'
                          end
                          object Label26: TLabel
                            Left = 8
                            Top = 44
                            Width = 27
                            Height = 13
                            Caption = 'Valor:'
                          end
                          object EdSom2TxtSiz: TdmkEdit
                            Left = 68
                            Top = 16
                            Width = 33
                            Height = 21
                            Alignment = taRightJustify
                            TabOrder = 0
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ValMin = '4'
                            ValMax = '72'
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '7'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 7
                          end
                          object EdSom2ValSiz: TdmkEdit
                            Left = 68
                            Top = 40
                            Width = 33
                            Height = 21
                            Alignment = taRightJustify
                            TabOrder = 1
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ValMin = '4'
                            ValMax = '72'
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '7'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 7
                          end
                        end
                      end
                    end
                  end
                end
                object TabSheet19: TTabSheet
                  Caption = ' Grupos '
                  ImageIndex = 2
                  object Panel10: TPanel
                    Left = 0
                    Top = 0
                    Width = 768
                    Height = 248
                    Align = alClient
                    TabOrder = 0
                    object GroupBox11: TGroupBox
                      Left = 1
                      Top = 1
                      Width = 140
                      Height = 246
                      Align = alLeft
                      Caption = ' T'#237'tulo: '
                      TabOrder = 0
                      object Label35: TLabel
                        Left = 8
                        Top = 22
                        Width = 58
                        Height = 13
                        Caption = 'Altura linha'#185':'
                      end
                      object Label36: TLabel
                        Left = 8
                        Top = 46
                        Width = 75
                        Height = 13
                        Caption = 'Tamanho fonte:'
                      end
                      object Label37: TLabel
                        Left = 8
                        Top = 70
                        Width = 84
                        Height = 13
                        Caption = 'Margem superior'#185':'
                      end
                      object EdTit3LinAlt: TdmkEdit
                        Left = 72
                        Top = 18
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '600'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 600
                      end
                      object RGTit3TxtFmt: TRadioGroup
                        Left = 2
                        Top = 179
                        Width = 136
                        Height = 65
                        Align = alBottom
                        Caption = ' Formata'#231#227'o: '
                        Columns = 2
                        ItemIndex = 1
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 3
                      end
                      object EdTit3TxtSiz: TdmkEdit
                        Left = 87
                        Top = 42
                        Width = 45
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '9'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 9
                      end
                      object EdTit3MrgSup: TdmkEdit
                        Left = 96
                        Top = 66
                        Width = 36
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '100'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 100
                      end
                    end
                    object GroupBox12: TGroupBox
                      Left = 141
                      Top = 1
                      Width = 308
                      Height = 246
                      Align = alLeft
                      Caption = ' Sub-total: '
                      TabOrder = 1
                      object GroupBox13: TGroupBox
                        Left = 157
                        Top = 15
                        Width = 147
                        Height = 229
                        Align = alLeft
                        Caption = ' Formata'#231#227'o: '
                        TabOrder = 1
                        object RGSom3ValFmt: TRadioGroup
                          Left = 73
                          Top = 15
                          Width = 71
                          Height = 212
                          Align = alLeft
                          Caption = ' Valor: '
                          ItemIndex = 1
                          Items.Strings = (
                            'Nada'
                            'Negrito'
                            'It'#225'lico'
                            'Ambos')
                          TabOrder = 1
                        end
                        object RGSom3TxtFmt: TRadioGroup
                          Left = 2
                          Top = 15
                          Width = 71
                          Height = 212
                          Align = alLeft
                          Caption = ' Descri'#231#227'o: '
                          ItemIndex = 1
                          Items.Strings = (
                            'Nada'
                            'Negrito'
                            'It'#225'lico'
                            'Ambos')
                          TabOrder = 0
                        end
                      end
                      object Panel11: TPanel
                        Left = 2
                        Top = 15
                        Width = 155
                        Height = 229
                        Align = alLeft
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label38: TLabel
                          Left = 8
                          Top = 6
                          Width = 68
                          Height = 13
                          Caption = 'Largura valor'#185':'
                        end
                        object Label39: TLabel
                          Left = 8
                          Top = 30
                          Width = 58
                          Height = 13
                          Caption = 'Altura linha'#185':'
                        end
                        object EdSom3LinAlt: TdmkEdit
                          Left = 93
                          Top = 26
                          Width = 60
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '450'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 450
                        end
                        object EdSom3ValLar: TdmkEdit
                          Left = 93
                          Top = 2
                          Width = 60
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '1200'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 1200
                        end
                        object GroupBox14: TGroupBox
                          Left = 0
                          Top = 48
                          Width = 154
                          Height = 66
                          Caption = ' Tam. fonte: '
                          TabOrder = 2
                          object Label40: TLabel
                            Left = 8
                            Top = 20
                            Width = 51
                            Height = 13
                            Caption = 'Descri'#231#227'o:'
                          end
                          object Label41: TLabel
                            Left = 8
                            Top = 44
                            Width = 27
                            Height = 13
                            Caption = 'Valor:'
                          end
                          object EdSom3TxtSiz: TdmkEdit
                            Left = 68
                            Top = 16
                            Width = 33
                            Height = 21
                            Alignment = taRightJustify
                            TabOrder = 0
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ValMin = '4'
                            ValMax = '72'
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '8'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 8
                          end
                          object EdSom3ValSiz: TdmkEdit
                            Left = 68
                            Top = 40
                            Width = 33
                            Height = 21
                            Alignment = taRightJustify
                            TabOrder = 1
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ValMin = '4'
                            ValMax = '72'
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '8'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 8
                          end
                        end
                      end
                    end
                  end
                end
                object TabSheet20: TTabSheet
                  Caption = ' Conjuntos '
                  ImageIndex = 3
                  object Panel12: TPanel
                    Left = 0
                    Top = 0
                    Width = 768
                    Height = 248
                    Align = alClient
                    TabOrder = 0
                    object GroupBox15: TGroupBox
                      Left = 1
                      Top = 1
                      Width = 140
                      Height = 246
                      Align = alLeft
                      Caption = ' T'#237'tulo: '
                      TabOrder = 0
                      object Label42: TLabel
                        Left = 8
                        Top = 22
                        Width = 58
                        Height = 13
                        Caption = 'Altura linha'#185':'
                      end
                      object Label43: TLabel
                        Left = 8
                        Top = 46
                        Width = 75
                        Height = 13
                        Caption = 'Tamanho fonte:'
                      end
                      object Label44: TLabel
                        Left = 8
                        Top = 70
                        Width = 84
                        Height = 13
                        Caption = 'Margem superior'#185':'
                      end
                      object EdTit4LinAlt: TdmkEdit
                        Left = 72
                        Top = 18
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '450'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 450
                      end
                      object RGTit4TxtFmt: TRadioGroup
                        Left = 2
                        Top = 179
                        Width = 136
                        Height = 65
                        Align = alBottom
                        Caption = ' Formata'#231#227'o: '
                        Columns = 2
                        ItemIndex = 1
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 3
                      end
                      object EdTit4TxtSiz: TdmkEdit
                        Left = 88
                        Top = 42
                        Width = 45
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '10'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 10
                      end
                      object EdTit4MrgSup: TdmkEdit
                        Left = 96
                        Top = 66
                        Width = 36
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                      end
                    end
                    object GroupBox16: TGroupBox
                      Left = 141
                      Top = 1
                      Width = 308
                      Height = 246
                      Align = alLeft
                      Caption = ' Sub-total: '
                      TabOrder = 1
                      object Panel13: TPanel
                        Left = 2
                        Top = 15
                        Width = 155
                        Height = 229
                        Align = alLeft
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label46: TLabel
                          Left = 8
                          Top = 30
                          Width = 58
                          Height = 13
                          Caption = 'Altura linha'#185':'
                        end
                        object EdSom4LinAlt: TdmkEdit
                          Left = 93
                          Top = 26
                          Width = 60
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '300'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 300
                        end
                      end
                    end
                  end
                end
              end
            end
            object TabSheet12: TTabSheet
              Caption = ' S'#243' conjuntos '
              ImageIndex = 1
              object Panel28: TPanel
                Left = 0
                Top = 101
                Width = 776
                Height = 175
                Align = alBottom
                TabOrder = 0
                object GroupBox39: TGroupBox
                  Left = 1
                  Top = 1
                  Width = 140
                  Height = 173
                  Align = alLeft
                  Caption = ' T'#237'tulo: '
                  TabOrder = 0
                  object Label106: TLabel
                    Left = 8
                    Top = 22
                    Width = 58
                    Height = 13
                    Caption = 'Altura linha'#185':'
                  end
                  object Label107: TLabel
                    Left = 8
                    Top = 46
                    Width = 75
                    Height = 13
                    Caption = 'Tamanho fonte:'
                  end
                  object Label108: TLabel
                    Left = 8
                    Top = 70
                    Width = 84
                    Height = 13
                    Caption = 'Margem superior'#185':'
                  end
                  object EdTit8LinAlt: TdmkEdit
                    Left = 72
                    Top = 18
                    Width = 60
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '300'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 300
                  end
                  object RGTit8TxtFmt: TRadioGroup
                    Left = 2
                    Top = 106
                    Width = 136
                    Height = 65
                    Align = alBottom
                    Caption = ' Formata'#231#227'o: '
                    Columns = 2
                    ItemIndex = 1
                    Items.Strings = (
                      'Nada'
                      'Negrito'
                      'It'#225'lico'
                      'Ambos')
                    TabOrder = 3
                  end
                  object EdTit8TxtSiz: TdmkEdit
                    Left = 87
                    Top = 42
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '4'
                    ValMax = '72'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '8'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 8
                  end
                  object EdTit8MrgSup: TdmkEdit
                    Left = 96
                    Top = 66
                    Width = 36
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '50'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 50
                  end
                end
                object GroupBox40: TGroupBox
                  Left = 453
                  Top = 1
                  Width = 308
                  Height = 173
                  Align = alLeft
                  Caption = ' Total: '
                  TabOrder = 2
                  object GroupBox41: TGroupBox
                    Left = 157
                    Top = 15
                    Width = 147
                    Height = 156
                    Align = alLeft
                    Caption = ' Formata'#231#227'o: '
                    TabOrder = 1
                    object RGSom8ValFmt: TRadioGroup
                      Left = 73
                      Top = 15
                      Width = 71
                      Height = 139
                      Align = alLeft
                      Caption = ' Valor: '
                      ItemIndex = 1
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 1
                    end
                    object RGSom8TxtFmt: TRadioGroup
                      Left = 2
                      Top = 15
                      Width = 71
                      Height = 139
                      Align = alLeft
                      Caption = ' Descri'#231#227'o: '
                      ItemIndex = 2
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 0
                    end
                  end
                  object Panel29: TPanel
                    Left = 2
                    Top = 15
                    Width = 155
                    Height = 156
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label109: TLabel
                      Left = 8
                      Top = 6
                      Width = 68
                      Height = 13
                      Caption = 'Largura valor'#185':'
                    end
                    object Label110: TLabel
                      Left = 8
                      Top = 30
                      Width = 58
                      Height = 13
                      Caption = 'Altura linha'#185':'
                    end
                    object EdSom8LinAlt: TdmkEdit
                      Left = 93
                      Top = 26
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '300'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 300
                    end
                    object EdSom8ValLar: TdmkEdit
                      Left = 93
                      Top = 2
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '1200'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 1200
                    end
                    object GroupBox42: TGroupBox
                      Left = 0
                      Top = 48
                      Width = 154
                      Height = 66
                      Caption = ' Tam. fonte: '
                      TabOrder = 2
                      object Label111: TLabel
                        Left = 8
                        Top = 20
                        Width = 51
                        Height = 13
                        Caption = 'Descri'#231#227'o:'
                      end
                      object Label112: TLabel
                        Left = 8
                        Top = 44
                        Width = 27
                        Height = 13
                        Caption = 'Valor:'
                      end
                      object EdSom8TxtSiz: TdmkEdit
                        Left = 68
                        Top = 16
                        Width = 33
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '7'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 7
                      end
                      object EdSom8ValSiz: TdmkEdit
                        Left = 68
                        Top = 40
                        Width = 33
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '7'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 7
                      end
                    end
                  end
                end
                object GroupBox43: TGroupBox
                  Left = 141
                  Top = 1
                  Width = 312
                  Height = 173
                  Align = alLeft
                  Caption = ' Dados: '
                  TabOrder = 1
                  object Panel30: TPanel
                    Left = 2
                    Top = 15
                    Width = 160
                    Height = 156
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label113: TLabel
                      Left = 8
                      Top = 10
                      Width = 68
                      Height = 13
                      Caption = 'Largura valor'#185':'
                    end
                    object Label114: TLabel
                      Left = 8
                      Top = 34
                      Width = 58
                      Height = 13
                      Caption = 'Altura linha'#185':'
                    end
                    object GroupBox44: TGroupBox
                      Left = 8
                      Top = 59
                      Width = 146
                      Height = 68
                      Caption = ' Tam. fonte: '
                      TabOrder = 2
                      object Label115: TLabel
                        Left = 8
                        Top = 20
                        Width = 51
                        Height = 13
                        Caption = 'Descri'#231#227'o:'
                      end
                      object Label116: TLabel
                        Left = 8
                        Top = 44
                        Width = 27
                        Height = 13
                        Caption = 'Valor:'
                      end
                      object EdNiv8TxtSiz: TdmkEdit
                        Left = 68
                        Top = 16
                        Width = 33
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '7'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 7
                      end
                      object EdNiv8ValSiz: TdmkEdit
                        Left = 68
                        Top = 40
                        Width = 33
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '7'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 7
                      end
                    end
                    object EdNiv8ValLar: TdmkEdit
                      Left = 93
                      Top = 6
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '1200'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 1200
                    end
                    object EdNiv8LinAlt: TdmkEdit
                      Left = 93
                      Top = 30
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '270'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 270
                    end
                    object CkNiv8Grades: TCheckBox
                      Left = 8
                      Top = 132
                      Width = 97
                      Height = 17
                      Caption = 'Mostra grade.'
                      Checked = True
                      State = cbChecked
                      TabOrder = 3
                    end
                  end
                  object GroupBox45: TGroupBox
                    Left = 162
                    Top = 15
                    Width = 147
                    Height = 156
                    Align = alLeft
                    Caption = ' Formata'#231#227'o: '
                    TabOrder = 1
                    object RGNiv8ValFmt: TRadioGroup
                      Left = 73
                      Top = 15
                      Width = 71
                      Height = 139
                      Align = alLeft
                      Caption = ' Valor: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 1
                    end
                    object RGNiv8TxtFmt: TRadioGroup
                      Left = 2
                      Top = 15
                      Width = 71
                      Height = 139
                      Align = alLeft
                      Caption = ' Descri'#231#227'o: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 0
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Carteiras'
          ImageIndex = 3
          object PageControl3: TPageControl
            Left = 0
            Top = 0
            Width = 784
            Height = 304
            ActivePage = TabSheet17
            Align = alClient
            TabOrder = 0
            object TabSheet16: TTabSheet
              Caption = ' Resumo financeiro '
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 776
                Height = 276
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label45: TLabel
                  Left = 4
                  Top = 4
                  Width = 31
                  Height = 13
                  Caption = 'T'#237'tulo:'
                end
                object Label47: TLabel
                  Left = 300
                  Top = 4
                  Width = 37
                  Height = 13
                  Caption = 'T.fonte:'
                end
                object Label48: TLabel
                  Left = 340
                  Top = 4
                  Width = 67
                  Height = 13
                  Caption = 'Margem sup.'#185':'
                end
                object Label49: TLabel
                  Left = 340
                  Top = 44
                  Width = 58
                  Height = 13
                  Caption = 'Altura linha'#185':'
                end
                object EdTitRTxtTit: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 293
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'RESUMO FINANCEIRO -'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'RESUMO FINANCEIRO -'
                end
                object EdTitRTxtSiz: TdmkEdit
                  Left = 300
                  Top = 20
                  Width = 37
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '4'
                  ValMax = '72'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '8'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 8
                end
                object RGTitRTxtFmt: TRadioGroup
                  Left = 4
                  Top = 44
                  Width = 333
                  Height = 37
                  Caption = ' Formata'#231#227'o fonte: '
                  Columns = 4
                  ItemIndex = 1
                  Items.Strings = (
                    'Nada'
                    'Negrito'
                    'It'#225'lico'
                    'Ambos')
                  TabOrder = 3
                end
                object EdTitRMrgSup: TdmkEdit
                  Left = 340
                  Top = 20
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '0'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '300'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 300
                end
                object EdTitRLinAlt: TdmkEdit
                  Left = 340
                  Top = 60
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '0'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '350'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 350
                end
                object RGTitRPerFmt: TRadioGroup
                  Left = 410
                  Top = 5
                  Width = 363
                  Height = 77
                  Caption = ' Formata'#231#227'o per'#237'odo: '
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'dd/mm/aa a dd/mm/aa'
                    'dd a dd/mm/aa'
                    'mmmm/aa'
                    'mmm/aa'
                    'mm/aa')
                  TabOrder = 5
                end
              end
            end
            object TabSheet17: TTabSheet
              Caption = ' Saldos das Carteiras'
              ImageIndex = 1
              object Panel15: TPanel
                Left = 0
                Top = 0
                Width = 776
                Height = 276
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label50: TLabel
                  Left = 4
                  Top = 4
                  Width = 31
                  Height = 13
                  Caption = 'T'#237'tulo:'
                end
                object Label51: TLabel
                  Left = 300
                  Top = 4
                  Width = 37
                  Height = 13
                  Caption = 'T.fonte:'
                end
                object Label52: TLabel
                  Left = 340
                  Top = 4
                  Width = 67
                  Height = 13
                  Caption = 'Margem sup.'#185':'
                end
                object Label53: TLabel
                  Left = 340
                  Top = 44
                  Width = 58
                  Height = 13
                  Caption = 'Altura linha'#185':'
                end
                object EdTitCTxtTit: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 293
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'CONTAS CORRENTES -'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'CONTAS CORRENTES -'
                end
                object EdTitCTxtSiz: TdmkEdit
                  Left = 300
                  Top = 20
                  Width = 37
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '4'
                  ValMax = '72'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '7'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 7
                end
                object RGTitCTxtFmt: TRadioGroup
                  Left = 4
                  Top = 44
                  Width = 333
                  Height = 37
                  Caption = ' Formata'#231#227'o fonte: '
                  Columns = 4
                  ItemIndex = 1
                  Items.Strings = (
                    'Nada'
                    'Negrito'
                    'It'#225'lico'
                    'Ambos')
                  TabOrder = 3
                end
                object EdTitCMrgSup: TdmkEdit
                  Left = 340
                  Top = 20
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '0'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '300'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 300
                end
                object EdTitCLinAlt: TdmkEdit
                  Left = 340
                  Top = 60
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '0'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '300'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 300
                end
                object RGTitCPerFmt: TRadioGroup
                  Left = 410
                  Top = 5
                  Width = 363
                  Height = 77
                  Caption = ' Formata'#231#227'o per'#237'odo: '
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'dd/mm/aa a dd/mm/aa'
                    'dd a dd/mm/aa'
                    'mmmm/aa'
                    'mmm/aa'
                    'mm/aa')
                  TabOrder = 5
                end
                object GroupBox17: TGroupBox
                  Left = 4
                  Top = 84
                  Width = 769
                  Height = 61
                  Caption = ' Descri'#231#227'o da carteira: '
                  TabOrder = 6
                  object Label54: TLabel
                    Left = 384
                    Top = 16
                    Width = 58
                    Height = 13
                    Caption = 'Altura linha'#185':'
                  end
                  object Label55: TLabel
                    Left = 344
                    Top = 16
                    Width = 37
                    Height = 13
                    Caption = 'T.fonte:'
                  end
                  object RGSdoCTxtFmt: TRadioGroup
                    Left = 8
                    Top = 17
                    Width = 333
                    Height = 37
                    Caption = ' Formata'#231#227'o fonte: '
                    Columns = 4
                    ItemIndex = 1
                    Items.Strings = (
                      'Nada'
                      'Negrito'
                      'It'#225'lico'
                      'Ambos')
                    TabOrder = 0
                  end
                  object EdSdoCLinAlt: TdmkEdit
                    Left = 384
                    Top = 32
                    Width = 65
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '0'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '300'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 300
                  end
                  object EdSdoCTxtSiz: TdmkEdit
                    Left = 344
                    Top = 32
                    Width = 37
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '4'
                    ValMax = '72'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '7'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 7
                  end
                end
              end
            end
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'Inadimpl'#234'ncia'
          ImageIndex = 8
          object Panel16: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 304
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox19: TGroupBox
              Left = 0
              Top = 0
              Width = 784
              Height = 100
              Align = alTop
              Caption = ' T'#237'tulo: '
              TabOrder = 0
              object Label60: TLabel
                Left = 8
                Top = 16
                Width = 72
                Height = 13
                Caption = 'Texto do t'#237'tulo:'
              end
              object Label61: TLabel
                Left = 304
                Top = 16
                Width = 37
                Height = 13
                Caption = 'T.fonte:'
              end
              object Label62: TLabel
                Left = 344
                Top = 16
                Width = 67
                Height = 13
                Caption = 'Margem sup.'#185':'
              end
              object Label63: TLabel
                Left = 344
                Top = 56
                Width = 58
                Height = 13
                Caption = 'Altura linha'#185':'
              end
              object EdTitITxtTit: TdmkEdit
                Left = 8
                Top = 32
                Width = 293
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'INADIMPL'#202'NCIA'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'INADIMPL'#202'NCIA'
              end
              object RGTitITxtFmt: TRadioGroup
                Left = 8
                Top = 56
                Width = 333
                Height = 37
                Caption = ' Formata'#231#227'o fonte: '
                Columns = 4
                ItemIndex = 1
                Items.Strings = (
                  'Nada'
                  'Negrito'
                  'It'#225'lico'
                  'Ambos')
                TabOrder = 3
              end
              object EdTitITxtSiz: TdmkEdit
                Left = 304
                Top = 32
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '8'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 8
              end
              object EdTitIMrgSup: TdmkEdit
                Left = 344
                Top = 32
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '300'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 300
              end
              object EdTitILinAlt: TdmkEdit
                Left = 344
                Top = 72
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '300'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 300
              end
            end
            object GroupBox20: TGroupBox
              Left = 0
              Top = 200
              Width = 784
              Height = 100
              Align = alTop
              Caption = ' Inadimpl'#234'ncia Condominio: '
              TabOrder = 2
              object Label64: TLabel
                Left = 8
                Top = 16
                Width = 30
                Height = 13
                Caption = 'Texto:'
              end
              object Label65: TLabel
                Left = 304
                Top = 16
                Width = 37
                Height = 13
                Caption = 'T.fonte:'
              end
              object Label66: TLabel
                Left = 76
                Top = 56
                Width = 68
                Height = 13
                Caption = 'Largura valor'#185':'
              end
              object Label67: TLabel
                Left = 8
                Top = 56
                Width = 58
                Height = 13
                Caption = 'Altura linha'#185':'
              end
              object Label68: TLabel
                Left = 304
                Top = 56
                Width = 37
                Height = 13
                Caption = 'V.fonte:'
              end
              object EdSdoITxTTxt: TdmkEdit
                Left = 8
                Top = 32
                Width = 293
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Inadimpl'#234'ncia todas unidades at'#233
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Inadimpl'#234'ncia todas unidades at'#233
              end
              object RGSdoITxTFmt: TRadioGroup
                Left = 348
                Top = 17
                Width = 333
                Height = 37
                Caption = ' Formata'#231#227'o (fonte) do texto: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  'Nada'
                  'Negrito'
                  'It'#225'lico'
                  'Ambos')
                TabOrder = 5
              end
              object EdSdoITxTSiz: TdmkEdit
                Left = 304
                Top = 32
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
              end
              object EdSdoILaTVal: TdmkEdit
                Left = 76
                Top = 72
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1200'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1200
              end
              object EdSdoILiTAlt: TdmkEdit
                Left = 8
                Top = 72
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '300'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 300
              end
              object EdSdoIVaTSiz: TdmkEdit
                Left = 304
                Top = 72
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
              end
              object RGSdoIVaTFmt: TRadioGroup
                Left = 348
                Top = 57
                Width = 333
                Height = 37
                Caption = ' Formata'#231#227'o (fonte) do valor: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  'Nada'
                  'Negrito'
                  'It'#225'lico'
                  'Ambos')
                TabOrder = 6
              end
            end
            object GroupBox18: TGroupBox
              Left = 0
              Top = 100
              Width = 784
              Height = 100
              Align = alTop
              Caption = ' Inadimpl'#234'ncia Cond'#244'mino: '
              TabOrder = 1
              object Label56: TLabel
                Left = 8
                Top = 16
                Width = 30
                Height = 13
                Caption = 'Texto:'
              end
              object Label57: TLabel
                Left = 304
                Top = 16
                Width = 37
                Height = 13
                Caption = 'T.fonte:'
              end
              object Label58: TLabel
                Left = 76
                Top = 56
                Width = 68
                Height = 13
                Caption = 'Largura valor'#185':'
              end
              object Label59: TLabel
                Left = 8
                Top = 56
                Width = 58
                Height = 13
                Caption = 'Altura linha'#185':'
              end
              object Label69: TLabel
                Left = 304
                Top = 56
                Width = 37
                Height = 13
                Caption = 'V.fonte:'
              end
              object EdSdoITxMTxt: TdmkEdit
                Left = 8
                Top = 32
                Width = 293
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Inadimpl'#234'ncia minha unidade at'#233
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Inadimpl'#234'ncia minha unidade at'#233
              end
              object RGSdoITxMFmt: TRadioGroup
                Left = 348
                Top = 17
                Width = 333
                Height = 37
                Caption = ' Formata'#231#227'o (fonte) do texto: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  'Nada'
                  'Negrito'
                  'It'#225'lico'
                  'Ambos')
                TabOrder = 5
              end
              object EdSdoITxMSiz: TdmkEdit
                Left = 304
                Top = 32
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
              end
              object EdSdoILaMVal: TdmkEdit
                Left = 76
                Top = 72
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1200'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1200
              end
              object EdSdoILiMAlt: TdmkEdit
                Left = 8
                Top = 72
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '300'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 300
              end
              object EdSdoIVaMSiz: TdmkEdit
                Left = 304
                Top = 72
                Width = 37
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '4'
                ValMax = '72'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
              end
              object RGSdoIVaMFmt: TRadioGroup
                Left = 348
                Top = 57
                Width = 333
                Height = 37
                Caption = ' Formata'#231#227'o (fonte) do valor: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  'Nada'
                  'Negrito'
                  'It'#225'lico'
                  'Ambos')
                TabOrder = 6
              end
            end
          end
        end
        object TabSheet10: TTabSheet
          Caption = 'Provis'#245'es'
          ImageIndex = 9
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 304
            Align = alClient
            TabOrder = 0
            object PageControl4: TPageControl
              Left = 1
              Top = 1
              Width = 782
              Height = 302
              ActivePage = TabSheet8
              Align = alClient
              TabOrder = 0
              object TabSheet6: TTabSheet
                Caption = ' Contas '
                object Panel18: TPanel
                  Left = 0
                  Top = 0
                  Width = 774
                  Height = 274
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object GroupBox22: TGroupBox
                    Left = 160
                    Top = 0
                    Width = 147
                    Height = 274
                    Align = alLeft
                    Caption = ' Formata'#231#227'o: '
                    TabOrder = 1
                    object RGCtaPValFmt: TRadioGroup
                      Left = 73
                      Top = 15
                      Width = 71
                      Height = 257
                      Align = alLeft
                      Caption = ' Valor: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 1
                    end
                    object RGCtaPTxtFmt: TRadioGroup
                      Left = 2
                      Top = 15
                      Width = 71
                      Height = 257
                      Align = alLeft
                      Caption = ' Descri'#231#227'o: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 0
                    end
                  end
                  object Panel19: TPanel
                    Left = 0
                    Top = 0
                    Width = 160
                    Height = 274
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label74: TLabel
                      Left = 8
                      Top = 10
                      Width = 68
                      Height = 13
                      Caption = 'Largura valor'#185':'
                    end
                    object Label75: TLabel
                      Left = 8
                      Top = 34
                      Width = 58
                      Height = 13
                      Caption = 'Altura linha'#185':'
                    end
                    object GroupBox23: TGroupBox
                      Left = 8
                      Top = 59
                      Width = 146
                      Height = 68
                      Caption = ' Tam. fonte: '
                      TabOrder = 2
                      object Label76: TLabel
                        Left = 8
                        Top = 20
                        Width = 51
                        Height = 13
                        Caption = 'Descri'#231#227'o:'
                      end
                      object Label77: TLabel
                        Left = 8
                        Top = 44
                        Width = 27
                        Height = 13
                        Caption = 'Valor:'
                      end
                      object EdCtaPTxtSiz: TdmkEdit
                        Left = 68
                        Top = 16
                        Width = 33
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '7'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 7
                      end
                      object EdCtaPValSiz: TdmkEdit
                        Left = 68
                        Top = 40
                        Width = 33
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '7'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 7
                      end
                    end
                    object EdCtaPLarVal: TdmkEdit
                      Left = 93
                      Top = 6
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '1200'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 1200
                    end
                    object EdCtaPLinAlt: TdmkEdit
                      Left = 93
                      Top = 30
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '270'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 270
                    end
                  end
                end
              end
              object TabSheet7: TTabSheet
                Caption = ' Sub-grupos '
                ImageIndex = 1
                object Panel20: TPanel
                  Left = 0
                  Top = 0
                  Width = 774
                  Height = 274
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object GroupBox24: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 140
                    Height = 274
                    Align = alLeft
                    Caption = ' T'#237'tulo: '
                    TabOrder = 0
                    object Label78: TLabel
                      Left = 8
                      Top = 22
                      Width = 58
                      Height = 13
                      Caption = 'Altura linha'#185':'
                    end
                    object Label79: TLabel
                      Left = 8
                      Top = 46
                      Width = 75
                      Height = 13
                      Caption = 'Tamanho fonte:'
                    end
                    object EdSgrPTitAlt: TdmkEdit
                      Left = 73
                      Top = 18
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '300'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 300
                    end
                    object RGSgrPTitFmt: TRadioGroup
                      Left = 2
                      Top = 207
                      Width = 136
                      Height = 65
                      Align = alBottom
                      Caption = ' Formata'#231#227'o: '
                      Columns = 2
                      ItemIndex = 1
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 2
                    end
                    object EdSgrPTitSiz: TdmkEdit
                      Left = 88
                      Top = 42
                      Width = 45
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '4'
                      ValMax = '72'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '7'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 7
                    end
                  end
                  object GroupBox25: TGroupBox
                    Left = 140
                    Top = 0
                    Width = 308
                    Height = 274
                    Align = alLeft
                    Caption = ' Sub-total: '
                    TabOrder = 1
                    object GroupBox26: TGroupBox
                      Left = 157
                      Top = 15
                      Width = 147
                      Height = 257
                      Align = alLeft
                      Caption = ' Formata'#231#227'o: '
                      TabOrder = 1
                      object RGSgrPSumFmt: TRadioGroup
                        Left = 2
                        Top = 15
                        Width = 71
                        Height = 240
                        Align = alLeft
                        Caption = ' Descri'#231#227'o: '
                        ItemIndex = 1
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 0
                      end
                    end
                    object Panel21: TPanel
                      Left = 2
                      Top = 15
                      Width = 155
                      Height = 257
                      Align = alLeft
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label82: TLabel
                        Left = 8
                        Top = 30
                        Width = 58
                        Height = 13
                        Caption = 'Altura linha'#185':'
                      end
                      object Label81: TLabel
                        Left = 8
                        Top = 6
                        Width = 68
                        Height = 13
                        Caption = 'Largura valor'#185':'
                      end
                      object Label91: TLabel
                        Left = 4
                        Top = 116
                        Width = 72
                        Height = 13
                        Caption = 'Texto do t'#237'tulo:'
                      end
                      object EdSgrPSumAlt: TdmkEdit
                        Left = 93
                        Top = 26
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '300'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 300
                      end
                      object GroupBox27: TGroupBox
                        Left = 0
                        Top = 48
                        Width = 154
                        Height = 66
                        Caption = ' Tam. fonte: '
                        TabOrder = 2
                        object Label83: TLabel
                          Left = 8
                          Top = 20
                          Width = 51
                          Height = 13
                          Caption = 'Descri'#231#227'o:'
                        end
                        object EdSgrPSumSiz: TdmkEdit
                          Left = 68
                          Top = 16
                          Width = 33
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '4'
                          ValMax = '72'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '7'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 7
                        end
                      end
                      object EdSgrPLarVal: TdmkEdit
                        Left = 93
                        Top = 2
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1200'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1200
                      end
                      object EdSumPTxtStr: TdmkEdit
                        Left = 4
                        Top = 132
                        Width = 145
                        Height = 21
                        TabOrder = 3
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'TOTAL PROVIS'#195'O'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'TOTAL PROVIS'#195'O'
                      end
                    end
                  end
                end
              end
              object TabSheet8: TTabSheet
                Caption = 'T'#237'tulo e total'
                ImageIndex = 2
                object Panel22: TPanel
                  Left = 0
                  Top = 0
                  Width = 774
                  Height = 274
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object GroupBox21: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 216
                    Height = 274
                    Align = alLeft
                    Caption = ' T'#237'tulo: '
                    TabOrder = 0
                    object Label70: TLabel
                      Left = 8
                      Top = 22
                      Width = 58
                      Height = 13
                      Caption = 'Altura linha'#185':'
                    end
                    object Label71: TLabel
                      Left = 8
                      Top = 46
                      Width = 75
                      Height = 13
                      Caption = 'Tamanho fonte:'
                    end
                    object Label72: TLabel
                      Left = 8
                      Top = 70
                      Width = 84
                      Height = 13
                      Caption = 'Margem superior'#185':'
                    end
                    object Label87: TLabel
                      Left = 8
                      Top = 92
                      Width = 72
                      Height = 13
                      Caption = 'Texto do t'#237'tulo:'
                    end
                    object EdPrvPTitAlt: TdmkEdit
                      Left = 72
                      Top = 18
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '350'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 350
                    end
                    object RGPrvPTitFmt: TRadioGroup
                      Left = 2
                      Top = 207
                      Width = 212
                      Height = 65
                      Align = alBottom
                      Caption = ' Formata'#231#227'o: '
                      Columns = 2
                      ItemIndex = 1
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 4
                    end
                    object EdPrvPTitSiz: TdmkEdit
                      Left = 88
                      Top = 42
                      Width = 45
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '4'
                      ValMax = '72'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '8'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 8
                    end
                    object EdPrvPMrgSup: TdmkEdit
                      Left = 96
                      Top = 66
                      Width = 36
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '300'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 300
                    end
                    object EdPrvPTitStr: TdmkEdit
                      Left = 4
                      Top = 108
                      Width = 205
                      Height = 21
                      TabOrder = 3
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PROVIS'#195'O PARA AS DESPESAS DE'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PROVIS'#195'O PARA AS DESPESAS DE'
                    end
                  end
                  object GroupBox28: TGroupBox
                    Left = 216
                    Top = 0
                    Width = 308
                    Height = 274
                    Align = alLeft
                    Caption = ' Sub-total: '
                    TabOrder = 1
                    object GroupBox29: TGroupBox
                      Left = 157
                      Top = 15
                      Width = 147
                      Height = 257
                      Align = alLeft
                      Caption = ' Formata'#231#227'o: '
                      TabOrder = 1
                      object RGSumPValFmt: TRadioGroup
                        Left = 73
                        Top = 15
                        Width = 71
                        Height = 240
                        Align = alLeft
                        Caption = ' Valor: '
                        ItemIndex = 1
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 1
                      end
                      object RGSumPTxtFmt: TRadioGroup
                        Left = 2
                        Top = 15
                        Width = 71
                        Height = 240
                        Align = alLeft
                        Caption = ' Descri'#231#227'o: '
                        ItemIndex = 1
                        Items.Strings = (
                          'Nada'
                          'Negrito'
                          'It'#225'lico'
                          'Ambos')
                        TabOrder = 0
                        ExplicitLeft = 4
                        ExplicitTop = 7
                      end
                    end
                    object Panel23: TPanel
                      Left = 2
                      Top = 15
                      Width = 155
                      Height = 257
                      Align = alLeft
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label73: TLabel
                        Left = 8
                        Top = 6
                        Width = 68
                        Height = 13
                        Caption = 'Largura valor'#185':'
                      end
                      object Label84: TLabel
                        Left = 8
                        Top = 30
                        Width = 58
                        Height = 13
                        Caption = 'Altura linha'#185':'
                      end
                      object EdSumPLinAlt: TdmkEdit
                        Left = 93
                        Top = 27
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '300'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 300
                      end
                      object EdSumPValLar: TdmkEdit
                        Left = 93
                        Top = 3
                        Width = 60
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1200'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1200
                      end
                      object GroupBox30: TGroupBox
                        Left = 0
                        Top = 48
                        Width = 154
                        Height = 66
                        Caption = ' Tam. fonte: '
                        TabOrder = 2
                        object Label85: TLabel
                          Left = 8
                          Top = 20
                          Width = 51
                          Height = 13
                          Caption = 'Descri'#231#227'o:'
                        end
                        object Label86: TLabel
                          Left = 8
                          Top = 44
                          Width = 27
                          Height = 13
                          Caption = 'Valor:'
                        end
                        object EdSumPTxtSiz: TdmkEdit
                          Left = 68
                          Top = 16
                          Width = 33
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '4'
                          ValMax = '72'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '7'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 7
                        end
                        object EdSumPValSiz: TdmkEdit
                          Left = 68
                          Top = 42
                          Width = 33
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '4'
                          ValMax = '72'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '7'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 7
                        end
                      end
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Arrecada'#231#245'es'
          ImageIndex = 6
          object PageControl5: TPageControl
            Left = 0
            Top = 0
            Width = 784
            Height = 304
            ActivePage = TabSheet18
            Align = alClient
            TabOrder = 0
            object TabSheet15: TTabSheet
              Caption = ' Conta '
              object Panel24: TPanel
                Left = 0
                Top = 0
                Width = 776
                Height = 276
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object GroupBox31: TGroupBox
                  Left = 160
                  Top = 0
                  Width = 147
                  Height = 276
                  Align = alLeft
                  Caption = ' Formata'#231#227'o: '
                  TabOrder = 1
                  object RGCtaAValFmt: TRadioGroup
                    Left = 73
                    Top = 15
                    Width = 71
                    Height = 259
                    Align = alLeft
                    Caption = ' Valor: '
                    ItemIndex = 0
                    Items.Strings = (
                      'Nada'
                      'Negrito'
                      'It'#225'lico'
                      'Ambos')
                    TabOrder = 1
                  end
                  object RGCtaATxtFmt: TRadioGroup
                    Left = 2
                    Top = 15
                    Width = 71
                    Height = 259
                    Align = alLeft
                    Caption = ' Descri'#231#227'o: '
                    ItemIndex = 0
                    Items.Strings = (
                      'Nada'
                      'Negrito'
                      'It'#225'lico'
                      'Ambos')
                    TabOrder = 0
                  end
                end
                object Panel25: TPanel
                  Left = 0
                  Top = 0
                  Width = 160
                  Height = 276
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label80: TLabel
                    Left = 8
                    Top = 10
                    Width = 68
                    Height = 13
                    Caption = 'Largura valor'#185':'
                  end
                  object Label88: TLabel
                    Left = 8
                    Top = 34
                    Width = 58
                    Height = 13
                    Caption = 'Altura linha'#185':'
                  end
                  object GroupBox32: TGroupBox
                    Left = 8
                    Top = 59
                    Width = 146
                    Height = 68
                    Caption = ' Tam. fonte: '
                    TabOrder = 2
                    object Label89: TLabel
                      Left = 8
                      Top = 20
                      Width = 51
                      Height = 13
                      Caption = 'Descri'#231#227'o:'
                    end
                    object Label90: TLabel
                      Left = 8
                      Top = 44
                      Width = 27
                      Height = 13
                      Caption = 'Valor:'
                    end
                    object EdCtaATxtSiz: TdmkEdit
                      Left = 68
                      Top = 16
                      Width = 33
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '4'
                      ValMax = '72'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '7'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 7
                    end
                    object EdCtaAValSiz: TdmkEdit
                      Left = 68
                      Top = 40
                      Width = 33
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '4'
                      ValMax = '72'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '7'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 7
                    end
                  end
                  object EdCtaALarVal: TdmkEdit
                    Left = 93
                    Top = 6
                    Width = 60
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1200'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1200
                  end
                  object EdCtaALinAlt: TdmkEdit
                    Left = 93
                    Top = 30
                    Width = 60
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '270'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 270
                  end
                end
              end
            end
            object TabSheet18: TTabSheet
              Caption = ' T'#237'tulo e total'
              ImageIndex = 1
              object Panel26: TPanel
                Left = 0
                Top = 0
                Width = 776
                Height = 276
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object GroupBox33: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 216
                  Height = 276
                  Align = alLeft
                  Caption = ' T'#237'tulo: '
                  TabOrder = 0
                  object Label92: TLabel
                    Left = 8
                    Top = 22
                    Width = 58
                    Height = 13
                    Caption = 'Altura linha'#185':'
                  end
                  object Label93: TLabel
                    Left = 8
                    Top = 46
                    Width = 75
                    Height = 13
                    Caption = 'Tamanho fonte:'
                  end
                  object Label94: TLabel
                    Left = 8
                    Top = 70
                    Width = 84
                    Height = 13
                    Caption = 'Margem superior'#185':'
                  end
                  object Label95: TLabel
                    Left = 8
                    Top = 92
                    Width = 72
                    Height = 13
                    Caption = 'Texto do t'#237'tulo:'
                  end
                  object EdTitATitAlt: TdmkEdit
                    Left = 72
                    Top = 18
                    Width = 60
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '350'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 350
                  end
                  object RGTitATitFmt: TRadioGroup
                    Left = 2
                    Top = 209
                    Width = 212
                    Height = 65
                    Align = alBottom
                    Caption = ' Formata'#231#227'o: '
                    Columns = 2
                    ItemIndex = 1
                    Items.Strings = (
                      'Nada'
                      'Negrito'
                      'It'#225'lico'
                      'Ambos')
                    TabOrder = 4
                  end
                  object EdTitATitSiz: TdmkEdit
                    Left = 88
                    Top = 42
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '4'
                    ValMax = '72'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '8'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 8
                  end
                  object EdTitAMrgSup: TdmkEdit
                    Left = 96
                    Top = 66
                    Width = 36
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '300'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 300
                  end
                  object EdTitATitStr: TdmkEdit
                    Left = 4
                    Top = 108
                    Width = 205
                    Height = 21
                    TabOrder = 3
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = 'PROVIS'#195'O PARA AS DESPESAS DE'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 'PROVIS'#195'O PARA AS DESPESAS DE'
                  end
                end
                object GroupBox34: TGroupBox
                  Left = 486
                  Top = 0
                  Width = 308
                  Height = 276
                  Align = alLeft
                  Caption = ' Sub-total: '
                  TabOrder = 2
                  object GroupBox35: TGroupBox
                    Left = 157
                    Top = 15
                    Width = 147
                    Height = 259
                    Align = alLeft
                    Caption = ' Formata'#231#227'o: '
                    TabOrder = 1
                    object RGTitASumFmt: TRadioGroup
                      Left = 2
                      Top = 15
                      Width = 71
                      Height = 242
                      Align = alLeft
                      Caption = ' Valor: '
                      ItemIndex = 1
                      Items.Strings = (
                        'Nada'
                        'Negrito'
                        'It'#225'lico'
                        'Ambos')
                      TabOrder = 0
                    end
                  end
                  object Panel27: TPanel
                    Left = 2
                    Top = 15
                    Width = 155
                    Height = 259
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label97: TLabel
                      Left = 8
                      Top = 30
                      Width = 58
                      Height = 13
                      Caption = 'Altura linha'#185':'
                    end
                    object EdTitASumAlt: TdmkEdit
                      Left = 91
                      Top = 26
                      Width = 60
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '300'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 300
                    end
                    object GroupBox36: TGroupBox
                      Left = 0
                      Top = 48
                      Width = 154
                      Height = 66
                      Caption = ' Tam. fonte: '
                      TabOrder = 1
                      object Label99: TLabel
                        Left = 8
                        Top = 44
                        Width = 27
                        Height = 13
                        Caption = 'Valor:'
                      end
                      object EdTitASumSiz: TdmkEdit
                        Left = 68
                        Top = 40
                        Width = 33
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '4'
                        ValMax = '72'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '7'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 7
                      end
                    end
                  end
                end
                object GroupBox37: TGroupBox
                  Left = 216
                  Top = 0
                  Width = 270
                  Height = 276
                  Align = alLeft
                  Caption = ' Unidade habitacional: '
                  TabOrder = 1
                  object Label100: TLabel
                    Left = 8
                    Top = 22
                    Width = 58
                    Height = 13
                    Caption = 'Altura linha'#185':'
                  end
                  object Label101: TLabel
                    Left = 8
                    Top = 46
                    Width = 75
                    Height = 13
                    Caption = 'Tamanho fonte:'
                  end
                  object Label102: TLabel
                    Left = 8
                    Top = 70
                    Width = 84
                    Height = 13
                    Caption = 'Margem superior'#185':'
                  end
                  object EdTitAAptAlt: TdmkEdit
                    Left = 72
                    Top = 18
                    Width = 60
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '350'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 350
                  end
                  object RGTitAAptFmt: TRadioGroup
                    Left = 2
                    Top = 224
                    Width = 266
                    Height = 50
                    Align = alBottom
                    Caption = ' Formata'#231#227'o: '
                    Columns = 2
                    ItemIndex = 1
                    Items.Strings = (
                      'Nada'
                      'Negrito'
                      'It'#225'lico'
                      'Ambos')
                    TabOrder = 4
                  end
                  object EdTitAAptSiz: TdmkEdit
                    Left = 88
                    Top = 42
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '4'
                    ValMax = '72'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '8'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 8
                  end
                  object EdTitAAptSup: TdmkEdit
                    Left = 96
                    Top = 66
                    Width = 36
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '300'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 300
                  end
                  object GroupBox46: TGroupBox
                    Left = 2
                    Top = 92
                    Width = 266
                    Height = 132
                    Align = alBottom
                    Caption = 'Texto do t'#237'tulo: '
                    TabOrder = 3
                    object Label14: TLabel
                      Left = 8
                      Top = 78
                      Width = 135
                      Height = 13
                      Caption = 'Vari'#225'veis para as instru'#231#245'es:'
                    end
                    object MeTitAAptTex: TdmkMemo
                      Left = 6
                      Top = 15
                      Width = 255
                      Height = 61
                      Lines.Strings = (
                        'dmkMemo1')
                      ScrollBars = ssBoth
                      TabOrder = 0
                      WantReturns = False
                      UpdType = utYes
                    end
                    object LBTitAAptTex: TListBox
                      Left = 6
                      Top = 92
                      Width = 255
                      Height = 35
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Courier New'
                      Font.Style = []
                      ItemHeight = 14
                      ParentFont = False
                      TabOrder = 1
                      OnDblClick = LBTitAAptTexDblClick
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet21: TTabSheet
          Caption = 'Web'
          ImageIndex = 7
          object Label15: TLabel
            Left = 4
            Top = 4
            Width = 31
            Height = 13
            Caption = 'T'#237'tulo:'
          end
          object EdTitWebTxt: TdmkEdit
            Left = 4
            Top = 20
            Width = 293
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'BALANCETE DEMONSTRATIVO -'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'BALANCETE DEMONSTRATIVO -'
          end
        end
      end
      object Panel31: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 84
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 8
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 60
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 20
          Width = 48
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdNome: TdmkEdit
          Left = 60
          Top = 20
          Width = 717
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object RGColunas: TRadioGroup
          Left = 10
          Top = 44
          Width = 253
          Height = 36
          Caption = ' Colunas (Balancete): '
          Columns = 7
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6')
          TabOrder = 2
        end
        object RGMesCompet: TRadioGroup
          Left = 266
          Top = 44
          Width = 511
          Height = 36
          Caption = ' M'#234's de compet'#234'ncia:  '
          Columns = 3
          ItemIndex = 2
          Items.Strings = (
            'M'#234's anterior'
            'M'#234's atual'
            'Pr'#243'ximo m'#234's')
          TabOrder = 3
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 461
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel33: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label20: TLabel
          Left = 132
          Top = 8
          Width = 234
          Height = 13
          Caption = #185': Valor em mil'#233'simos de cent'#237'metro (micrometros).'
        end
        object Label104: TLabel
          Left = 132
          Top = 24
          Width = 487
          Height = 13
          Caption = 
            #178' Um cent'#237'metro equivale a aproximadamente 26,4583162456 pontos ' +
            'de impress'#227'o  (1.000 / 37,7953). '
        end
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 531
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsConfigBol
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 68
        Top = 24
        Width = 609
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsConfigBol
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 467
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 82
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 256
        Top = 15
        Width = 534
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 401
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object BitBtn1: TBitBtn
          Tag = 10
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Padr'#245'es'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BitBtn1Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 303
        Height = 32
        Caption = 'Cfg. impress'#227'o de boleto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 303
        Height = 32
        Caption = 'Cfg. impress'#227'o de boleto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 303
        Height = 32
        Caption = 'Cfg. impress'#227'o de boleto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel32: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsConfigBol: TDataSource
    DataSet = QrConfigBol
    Left = 660
    Top = 13
  end
  object QrConfigBol: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrConfigBolBeforeOpen
    AfterOpen = QrConfigBolAfterOpen
    AfterScroll = QrConfigBolAfterScroll
    SQL.Strings = (
      'SELECT * FROM configbol'
      'WHERE Codigo > 0')
    Left = 632
    Top = 13
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'configbol.Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'configbol.Nome'
      Size = 50
    end
    object QrConfigBolTitBTxtSiz: TSmallintField
      FieldName = 'TitBTxtSiz'
      Origin = 'configbol.TitBTxtSiz'
    end
    object QrConfigBolTitBTxtFmt: TSmallintField
      FieldName = 'TitBTxtFmt'
      Origin = 'configbol.TitBTxtFmt'
    end
    object QrConfigBolTitBMrgSup: TIntegerField
      FieldName = 'TitBMrgSup'
      Origin = 'configbol.TitBMrgSup'
    end
    object QrConfigBolTitBLinAlt: TIntegerField
      FieldName = 'TitBLinAlt'
      Origin = 'configbol.TitBLinAlt'
    end
    object QrConfigBolTitBTxtTit: TWideStringField
      FieldName = 'TitBTxtTit'
      Origin = 'configbol.TitBTxtTit'
      Size = 50
    end
    object QrConfigBolTitBPerFmt: TSmallintField
      FieldName = 'TitBPerFmt'
      Origin = 'configbol.TitBPerFmt'
    end
    object QrConfigBolNiv1TxtSiz: TSmallintField
      FieldName = 'Niv1TxtSiz'
      Origin = 'configbol.Niv1TxtSiz'
    end
    object QrConfigBolNiv1ValSiz: TSmallintField
      FieldName = 'Niv1ValSiz'
      Origin = 'configbol.Niv1ValSiz'
    end
    object QrConfigBolNiv1TxtFmt: TSmallintField
      FieldName = 'Niv1TxtFmt'
      Origin = 'configbol.Niv1TxtFmt'
    end
    object QrConfigBolNiv1ValFmt: TSmallintField
      FieldName = 'Niv1ValFmt'
      Origin = 'configbol.Niv1ValFmt'
    end
    object QrConfigBolNiv1ValLar: TIntegerField
      FieldName = 'Niv1ValLar'
      Origin = 'configbol.Niv1ValLar'
    end
    object QrConfigBolNiv1LinAlt: TIntegerField
      FieldName = 'Niv1LinAlt'
      Origin = 'configbol.Niv1LinAlt'
    end
    object QrConfigBolCxaSTxtSiz: TSmallintField
      FieldName = 'CxaSTxtSiz'
      Origin = 'configbol.CxaSTxtSiz'
    end
    object QrConfigBolCxaSValSiz: TSmallintField
      FieldName = 'CxaSValSiz'
      Origin = 'configbol.CxaSValSiz'
    end
    object QrConfigBolCxaSTxtFmt: TSmallintField
      FieldName = 'CxaSTxtFmt'
      Origin = 'configbol.CxaSTxtFmt'
    end
    object QrConfigBolCxaSValFmt: TSmallintField
      FieldName = 'CxaSValFmt'
      Origin = 'configbol.CxaSValFmt'
    end
    object QrConfigBolCxaSTxtAlt: TIntegerField
      FieldName = 'CxaSTxtAlt'
      Origin = 'configbol.CxaSTxtAlt'
    end
    object QrConfigBolCxaSValAlt: TIntegerField
      FieldName = 'CxaSValAlt'
      Origin = 'configbol.CxaSValAlt'
    end
    object QrConfigBolCxaSMrgSup: TIntegerField
      FieldName = 'CxaSMrgSup'
      Origin = 'configbol.CxaSMrgSup'
    end
    object QrConfigBolCxaSIniTam: TFloatField
      FieldName = 'CxaSIniTam'
      Origin = 'configbol.CxaSIniTam'
    end
    object QrConfigBolCxaSCreTam: TFloatField
      FieldName = 'CxaSCreTam'
      Origin = 'configbol.CxaSCreTam'
    end
    object QrConfigBolCxaSDebTam: TFloatField
      FieldName = 'CxaSDebTam'
      Origin = 'configbol.CxaSDebTam'
    end
    object QrConfigBolCxaSMovTam: TFloatField
      FieldName = 'CxaSMovTam'
      Origin = 'configbol.CxaSMovTam'
    end
    object QrConfigBolCxaSFimTam: TFloatField
      FieldName = 'CxaSFimTam'
      Origin = 'configbol.CxaSFimTam'
    end
    object QrConfigBolCxaSIniTxt: TWideStringField
      FieldName = 'CxaSIniTxt'
      Origin = 'configbol.CxaSIniTxt'
      Size = 30
    end
    object QrConfigBolCxaSCreTxt: TWideStringField
      FieldName = 'CxaSCreTxt'
      Origin = 'configbol.CxaSCreTxt'
      Size = 30
    end
    object QrConfigBolCxaSDebTxt: TWideStringField
      FieldName = 'CxaSDebTxt'
      Origin = 'configbol.CxaSDebTxt'
      Size = 30
    end
    object QrConfigBolCxaSMovTxt: TWideStringField
      FieldName = 'CxaSMovTxt'
      Origin = 'configbol.CxaSMovTxt'
      Size = 30
    end
    object QrConfigBolCxaSFimTxt: TWideStringField
      FieldName = 'CxaSFimTxt'
      Origin = 'configbol.CxaSFimTxt'
      Size = 30
    end
    object QrConfigBolCxaSMrgInf: TIntegerField
      FieldName = 'CxaSMrgInf'
      Origin = 'configbol.CxaSMrgInf'
    end
    object QrConfigBolTit2TxtSiz: TSmallintField
      FieldName = 'Tit2TxtSiz'
      Origin = 'configbol.Tit2TxtSiz'
    end
    object QrConfigBolTit2TxtFmt: TSmallintField
      FieldName = 'Tit2TxtFmt'
      Origin = 'configbol.Tit2TxtFmt'
    end
    object QrConfigBolTit2MrgSup: TIntegerField
      FieldName = 'Tit2MrgSup'
      Origin = 'configbol.Tit2MrgSup'
    end
    object QrConfigBolTit2LinAlt: TIntegerField
      FieldName = 'Tit2LinAlt'
      Origin = 'configbol.Tit2LinAlt'
    end
    object QrConfigBolSom2TxtSiz: TSmallintField
      FieldName = 'Som2TxtSiz'
      Origin = 'configbol.Som2TxtSiz'
    end
    object QrConfigBolSom2ValSiz: TSmallintField
      FieldName = 'Som2ValSiz'
      Origin = 'configbol.Som2ValSiz'
    end
    object QrConfigBolSom2TxtFmt: TSmallintField
      FieldName = 'Som2TxtFmt'
      Origin = 'configbol.Som2TxtFmt'
    end
    object QrConfigBolSom2ValFmt: TSmallintField
      FieldName = 'Som2ValFmt'
      Origin = 'configbol.Som2ValFmt'
    end
    object QrConfigBolSom2ValLar: TIntegerField
      FieldName = 'Som2ValLar'
      Origin = 'configbol.Som2ValLar'
    end
    object QrConfigBolSom2LinAlt: TIntegerField
      FieldName = 'Som2LinAlt'
      Origin = 'configbol.Som2LinAlt'
    end
    object QrConfigBolTit3TxtSiz: TSmallintField
      FieldName = 'Tit3TxtSiz'
      Origin = 'configbol.Tit3TxtSiz'
    end
    object QrConfigBolTit3TxtFmt: TSmallintField
      FieldName = 'Tit3TxtFmt'
      Origin = 'configbol.Tit3TxtFmt'
    end
    object QrConfigBolTit3MrgSup: TIntegerField
      FieldName = 'Tit3MrgSup'
      Origin = 'configbol.Tit3MrgSup'
    end
    object QrConfigBolTit3LinAlt: TIntegerField
      FieldName = 'Tit3LinAlt'
      Origin = 'configbol.Tit3LinAlt'
    end
    object QrConfigBolSom3TxtSiz: TSmallintField
      FieldName = 'Som3TxtSiz'
      Origin = 'configbol.Som3TxtSiz'
    end
    object QrConfigBolSom3ValSiz: TSmallintField
      FieldName = 'Som3ValSiz'
      Origin = 'configbol.Som3ValSiz'
    end
    object QrConfigBolSom3TxtFmt: TSmallintField
      FieldName = 'Som3TxtFmt'
      Origin = 'configbol.Som3TxtFmt'
    end
    object QrConfigBolSom3ValFmt: TSmallintField
      FieldName = 'Som3ValFmt'
      Origin = 'configbol.Som3ValFmt'
    end
    object QrConfigBolSom3ValLar: TIntegerField
      FieldName = 'Som3ValLar'
      Origin = 'configbol.Som3ValLar'
    end
    object QrConfigBolSom3LinAlt: TIntegerField
      FieldName = 'Som3LinAlt'
      Origin = 'configbol.Som3LinAlt'
    end
    object QrConfigBolTit4TxtSiz: TSmallintField
      FieldName = 'Tit4TxtSiz'
      Origin = 'configbol.Tit4TxtSiz'
    end
    object QrConfigBolTit4TxtFmt: TSmallintField
      FieldName = 'Tit4TxtFmt'
      Origin = 'configbol.Tit4TxtFmt'
    end
    object QrConfigBolTit4MrgSup: TIntegerField
      FieldName = 'Tit4MrgSup'
      Origin = 'configbol.Tit4MrgSup'
    end
    object QrConfigBolTit4LinAlt: TIntegerField
      FieldName = 'Tit4LinAlt'
      Origin = 'configbol.Tit4LinAlt'
    end
    object QrConfigBolSom4LinAlt: TIntegerField
      FieldName = 'Som4LinAlt'
      Origin = 'configbol.Som4LinAlt'
    end
    object QrConfigBolTitRTxtSiz: TSmallintField
      FieldName = 'TitRTxtSiz'
      Origin = 'configbol.TitRTxtSiz'
    end
    object QrConfigBolTitRTxtFmt: TSmallintField
      FieldName = 'TitRTxtFmt'
      Origin = 'configbol.TitRTxtFmt'
    end
    object QrConfigBolTitRMrgSup: TIntegerField
      FieldName = 'TitRMrgSup'
      Origin = 'configbol.TitRMrgSup'
    end
    object QrConfigBolTitRLinAlt: TIntegerField
      FieldName = 'TitRLinAlt'
      Origin = 'configbol.TitRLinAlt'
    end
    object QrConfigBolTitRTxtTit: TWideStringField
      FieldName = 'TitRTxtTit'
      Origin = 'configbol.TitRTxtTit'
      Size = 50
    end
    object QrConfigBolTitRPerFmt: TSmallintField
      FieldName = 'TitRPerFmt'
      Origin = 'configbol.TitRPerFmt'
    end
    object QrConfigBolTitCTxtSiz: TSmallintField
      FieldName = 'TitCTxtSiz'
      Origin = 'configbol.TitCTxtSiz'
    end
    object QrConfigBolTitCTxtFmt: TSmallintField
      FieldName = 'TitCTxtFmt'
      Origin = 'configbol.TitCTxtFmt'
    end
    object QrConfigBolTitCMrgSup: TIntegerField
      FieldName = 'TitCMrgSup'
      Origin = 'configbol.TitCMrgSup'
    end
    object QrConfigBolTitCLinAlt: TIntegerField
      FieldName = 'TitCLinAlt'
      Origin = 'configbol.TitCLinAlt'
    end
    object QrConfigBolTitCTxtTit: TWideStringField
      FieldName = 'TitCTxtTit'
      Origin = 'configbol.TitCTxtTit'
      Size = 50
    end
    object QrConfigBolTitCPerFmt: TSmallintField
      FieldName = 'TitCPerFmt'
      Origin = 'configbol.TitCPerFmt'
    end
    object QrConfigBolSdoCTxtSiz: TSmallintField
      FieldName = 'SdoCTxtSiz'
      Origin = 'configbol.SdoCTxtSiz'
    end
    object QrConfigBolSdoCTxtFmt: TSmallintField
      FieldName = 'SdoCTxtFmt'
      Origin = 'configbol.SdoCTxtFmt'
    end
    object QrConfigBolSdoCLinAlt: TIntegerField
      FieldName = 'SdoCLinAlt'
      Origin = 'configbol.SdoCLinAlt'
    end
    object QrConfigBolTitITxtSiz: TSmallintField
      FieldName = 'TitITxtSiz'
      Origin = 'configbol.TitITxtSiz'
    end
    object QrConfigBolTitITxtFmt: TSmallintField
      FieldName = 'TitITxtFmt'
      Origin = 'configbol.TitITxtFmt'
    end
    object QrConfigBolTitIMrgSup: TIntegerField
      FieldName = 'TitIMrgSup'
      Origin = 'configbol.TitIMrgSup'
    end
    object QrConfigBolTitILinAlt: TIntegerField
      FieldName = 'TitILinAlt'
      Origin = 'configbol.TitILinAlt'
    end
    object QrConfigBolTitITxtTit: TWideStringField
      FieldName = 'TitITxtTit'
      Origin = 'configbol.TitITxtTit'
      Size = 50
    end
    object QrConfigBolSdoITxMSiz: TSmallintField
      FieldName = 'SdoITxMSiz'
      Origin = 'configbol.SdoITxMSiz'
    end
    object QrConfigBolSdoITxMFmt: TSmallintField
      FieldName = 'SdoITxMFmt'
      Origin = 'configbol.SdoITxMFmt'
    end
    object QrConfigBolSdoIVaMSiz: TSmallintField
      FieldName = 'SdoIVaMSiz'
      Origin = 'configbol.SdoIVaMSiz'
    end
    object QrConfigBolSdoIVaMFmt: TSmallintField
      FieldName = 'SdoIVaMFmt'
      Origin = 'configbol.SdoIVaMFmt'
    end
    object QrConfigBolSdoILiMAlt: TIntegerField
      FieldName = 'SdoILiMAlt'
      Origin = 'configbol.SdoILiMAlt'
    end
    object QrConfigBolSdoITxMTxt: TWideStringField
      FieldName = 'SdoITxMTxt'
      Origin = 'configbol.SdoITxMTxt'
      Size = 50
    end
    object QrConfigBolSdoILaMVal: TIntegerField
      FieldName = 'SdoILaMVal'
      Origin = 'configbol.SdoILaMVal'
    end
    object QrConfigBolSdoITxTSiz: TSmallintField
      FieldName = 'SdoITxTSiz'
      Origin = 'configbol.SdoITxTSiz'
    end
    object QrConfigBolSdoITxTFmt: TSmallintField
      FieldName = 'SdoITxTFmt'
      Origin = 'configbol.SdoITxTFmt'
    end
    object QrConfigBolSdoIVaTSiz: TSmallintField
      FieldName = 'SdoIVaTSiz'
      Origin = 'configbol.SdoIVaTSiz'
    end
    object QrConfigBolSdoIVaTFmt: TSmallintField
      FieldName = 'SdoIVaTFmt'
      Origin = 'configbol.SdoIVaTFmt'
    end
    object QrConfigBolSdoILiTAlt: TIntegerField
      FieldName = 'SdoILiTAlt'
      Origin = 'configbol.SdoILiTAlt'
    end
    object QrConfigBolSdoITxTTxt: TWideStringField
      FieldName = 'SdoITxTTxt'
      Origin = 'configbol.SdoITxTTxt'
      Size = 50
    end
    object QrConfigBolSdoILaTVal: TIntegerField
      FieldName = 'SdoILaTVal'
      Origin = 'configbol.SdoILaTVal'
    end
    object QrConfigBolCtaPTxtSiz: TSmallintField
      FieldName = 'CtaPTxtSiz'
      Origin = 'configbol.CtaPTxtSiz'
    end
    object QrConfigBolCtaPTxtFmt: TSmallintField
      FieldName = 'CtaPTxtFmt'
      Origin = 'configbol.CtaPTxtFmt'
    end
    object QrConfigBolCtaPValSiz: TSmallintField
      FieldName = 'CtaPValSiz'
      Origin = 'configbol.CtaPValSiz'
    end
    object QrConfigBolCtaPValFmt: TSmallintField
      FieldName = 'CtaPValFmt'
      Origin = 'configbol.CtaPValFmt'
    end
    object QrConfigBolCtaPLinAlt: TIntegerField
      FieldName = 'CtaPLinAlt'
      Origin = 'configbol.CtaPLinAlt'
    end
    object QrConfigBolCtaPLarVal: TIntegerField
      FieldName = 'CtaPLarVal'
      Origin = 'configbol.CtaPLarVal'
    end
    object QrConfigBolSgrPTitSiz: TSmallintField
      FieldName = 'SgrPTitSiz'
      Origin = 'configbol.SgrPTitSiz'
    end
    object QrConfigBolSgrPTitFmt: TSmallintField
      FieldName = 'SgrPTitFmt'
      Origin = 'configbol.SgrPTitFmt'
    end
    object QrConfigBolSgrPTitAlt: TIntegerField
      FieldName = 'SgrPTitAlt'
      Origin = 'configbol.SgrPTitAlt'
    end
    object QrConfigBolSgrPSumSiz: TSmallintField
      FieldName = 'SgrPSumSiz'
      Origin = 'configbol.SgrPSumSiz'
    end
    object QrConfigBolSgrPSumFmt: TSmallintField
      FieldName = 'SgrPSumFmt'
      Origin = 'configbol.SgrPSumFmt'
    end
    object QrConfigBolSgrPSumAlt: TIntegerField
      FieldName = 'SgrPSumAlt'
      Origin = 'configbol.SgrPSumAlt'
    end
    object QrConfigBolSgrPLarVal: TIntegerField
      FieldName = 'SgrPLarVal'
      Origin = 'configbol.SgrPLarVal'
    end
    object QrConfigBolPrvPTitSiz: TSmallintField
      FieldName = 'PrvPTitSiz'
      Origin = 'configbol.PrvPTitSiz'
    end
    object QrConfigBolPrvPTitFmt: TSmallintField
      FieldName = 'PrvPTitFmt'
      Origin = 'configbol.PrvPTitFmt'
    end
    object QrConfigBolPrvPTitAlt: TIntegerField
      FieldName = 'PrvPTitAlt'
      Origin = 'configbol.PrvPTitAlt'
    end
    object QrConfigBolPrvPTitStr: TWideStringField
      FieldName = 'PrvPTitStr'
      Origin = 'configbol.PrvPTitStr'
      Size = 50
    end
    object QrConfigBolPrvPMrgSup: TIntegerField
      FieldName = 'PrvPMrgSup'
      Origin = 'configbol.PrvPMrgSup'
    end
    object QrConfigBolSumPTxtSiz: TSmallintField
      FieldName = 'SumPTxtSiz'
      Origin = 'configbol.SumPTxtSiz'
    end
    object QrConfigBolSumPTxtFmt: TSmallintField
      FieldName = 'SumPTxtFmt'
      Origin = 'configbol.SumPTxtFmt'
    end
    object QrConfigBolSumPValSiz: TSmallintField
      FieldName = 'SumPValSiz'
      Origin = 'configbol.SumPValSiz'
    end
    object QrConfigBolSumPValFmt: TSmallintField
      FieldName = 'SumPValFmt'
      Origin = 'configbol.SumPValFmt'
    end
    object QrConfigBolSumPLinAlt: TIntegerField
      FieldName = 'SumPLinAlt'
      Origin = 'configbol.SumPLinAlt'
    end
    object QrConfigBolSumPValLar: TIntegerField
      FieldName = 'SumPValLar'
      Origin = 'configbol.SumPValLar'
    end
    object QrConfigBolSumPTxtStr: TWideStringField
      FieldName = 'SumPTxtStr'
      Origin = 'configbol.SumPTxtStr'
      Size = 50
    end
    object QrConfigBolCtaATxtSiz: TSmallintField
      FieldName = 'CtaATxtSiz'
      Origin = 'configbol.CtaATxtSiz'
    end
    object QrConfigBolCtaATxtFmt: TSmallintField
      FieldName = 'CtaATxtFmt'
      Origin = 'configbol.CtaATxtFmt'
    end
    object QrConfigBolCtaAValSiz: TSmallintField
      FieldName = 'CtaAValSiz'
      Origin = 'configbol.CtaAValSiz'
    end
    object QrConfigBolCtaAValFmt: TSmallintField
      FieldName = 'CtaAValFmt'
      Origin = 'configbol.CtaAValFmt'
    end
    object QrConfigBolCtaALinAlt: TIntegerField
      FieldName = 'CtaALinAlt'
      Origin = 'configbol.CtaALinAlt'
    end
    object QrConfigBolCtaALarVal: TIntegerField
      FieldName = 'CtaALarVal'
      Origin = 'configbol.CtaALarVal'
    end
    object QrConfigBolTitATitSiz: TSmallintField
      FieldName = 'TitATitSiz'
      Origin = 'configbol.TitATitSiz'
    end
    object QrConfigBolTitATitFmt: TSmallintField
      FieldName = 'TitATitFmt'
      Origin = 'configbol.TitATitFmt'
    end
    object QrConfigBolTitATitAlt: TIntegerField
      FieldName = 'TitATitAlt'
      Origin = 'configbol.TitATitAlt'
    end
    object QrConfigBolTitATitStr: TWideStringField
      FieldName = 'TitATitStr'
      Origin = 'configbol.TitATitStr'
      Size = 50
    end
    object QrConfigBolTitAMrgSup: TIntegerField
      FieldName = 'TitAMrgSup'
      Origin = 'configbol.TitAMrgSup'
    end
    object QrConfigBolTitAAptSiz: TSmallintField
      FieldName = 'TitAAptSiz'
      Origin = 'configbol.TitAAptSiz'
    end
    object QrConfigBolTitAAptFmt: TSmallintField
      FieldName = 'TitAAptFmt'
      Origin = 'configbol.TitAAptFmt'
    end
    object QrConfigBolTitAAptAlt: TIntegerField
      FieldName = 'TitAAptAlt'
      Origin = 'configbol.TitAAptAlt'
    end
    object QrConfigBolTitAAptSup: TIntegerField
      FieldName = 'TitAAptSup'
      Origin = 'configbol.TitAAptSup'
    end
    object QrConfigBolTitASumSiz: TSmallintField
      FieldName = 'TitASumSiz'
      Origin = 'configbol.TitASumSiz'
    end
    object QrConfigBolTitASumFmt: TSmallintField
      FieldName = 'TitASumFmt'
      Origin = 'configbol.TitASumFmt'
    end
    object QrConfigBolTitASumAlt: TIntegerField
      FieldName = 'TitASumAlt'
      Origin = 'configbol.TitASumAlt'
    end
    object QrConfigBolLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'configbol.Lk'
    end
    object QrConfigBolDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'configbol.DataCad'
    end
    object QrConfigBolDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'configbol.DataAlt'
    end
    object QrConfigBolUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'configbol.UserCad'
    end
    object QrConfigBolUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'configbol.UserAlt'
    end
    object QrConfigBolAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'configbol.AlterWeb'
    end
    object QrConfigBolAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'configbol.Ativo'
    end
    object QrConfigBolMeuLogoImp: TSmallintField
      FieldName = 'MeuLogoImp'
      Origin = 'configbol.MeuLogoImp'
    end
    object QrConfigBolMeuLogoAlt: TIntegerField
      FieldName = 'MeuLogoAlt'
      Origin = 'configbol.MeuLogoAlt'
    end
    object QrConfigBolMeuLogoLar: TIntegerField
      FieldName = 'MeuLogoLar'
      Origin = 'configbol.MeuLogoLar'
    end
    object QrConfigBolMeuLogoArq: TWideStringField
      FieldName = 'MeuLogoArq'
      Origin = 'configbol.MeuLogoArq'
      Size = 255
    end
    object QrConfigBolColunas: TSmallintField
      FieldName = 'Colunas'
      Origin = 'configbol.Colunas'
    end
    object QrConfigBolNiv1Grades: TSmallintField
      FieldName = 'Niv1Grades'
      Origin = 'configbol.Niv1Grades'
    end
    object QrConfigBolCxaTrnsTxt: TWideStringField
      FieldName = 'CxaTrnsTxt'
      Size = 30
    end
    object QrConfigBolTit8TxtSiz: TSmallintField
      FieldName = 'Tit8TxtSiz'
    end
    object QrConfigBolTit8TxtFmt: TSmallintField
      FieldName = 'Tit8TxtFmt'
    end
    object QrConfigBolTit8MrgSup: TIntegerField
      FieldName = 'Tit8MrgSup'
    end
    object QrConfigBolTit8LinAlt: TIntegerField
      FieldName = 'Tit8LinAlt'
    end
    object QrConfigBolNiv8TxtSiz: TSmallintField
      FieldName = 'Niv8TxtSiz'
    end
    object QrConfigBolNiv8ValSiz: TSmallintField
      FieldName = 'Niv8ValSiz'
    end
    object QrConfigBolNiv8Grades: TSmallintField
      FieldName = 'Niv8Grades'
    end
    object QrConfigBolNiv8TxtFmt: TSmallintField
      FieldName = 'Niv8TxtFmt'
    end
    object QrConfigBolNiv8ValFmt: TSmallintField
      FieldName = 'Niv8ValFmt'
    end
    object QrConfigBolNiv8ValLar: TIntegerField
      FieldName = 'Niv8ValLar'
    end
    object QrConfigBolNiv8LinAlt: TIntegerField
      FieldName = 'Niv8LinAlt'
    end
    object QrConfigBolSom8TxtSiz: TSmallintField
      FieldName = 'Som8TxtSiz'
    end
    object QrConfigBolSom8ValSiz: TSmallintField
      FieldName = 'Som8ValSiz'
    end
    object QrConfigBolSom8TxtFmt: TSmallintField
      FieldName = 'Som8TxtFmt'
    end
    object QrConfigBolSom8ValFmt: TSmallintField
      FieldName = 'Som8ValFmt'
    end
    object QrConfigBolSom8ValLar: TIntegerField
      FieldName = 'Som8ValLar'
    end
    object QrConfigBolSom8LinAlt: TIntegerField
      FieldName = 'Som8LinAlt'
    end
    object QrConfigBolTitAAptTex: TWideMemoField
      FieldName = 'TitAAptTex'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrConfigBolTitWebTxt: TWideStringField
      FieldName = 'TitWebTxt'
      Size = 50
    end
    object QrConfigBolMesCompet: TSmallintField
      FieldName = 'MesCompet'
    end
  end
  object PMInclui: TPopupMenu
    Left = 360
    Top = 388
    object Incluinovaconfiguraocombasenoatual1: TMenuItem
      Caption = 'Inclui nova configura'#231#227'o com base no atual'
      OnClick = Incluinovaconfiguraocombasenoatual1Click
    end
  end
  object OpenPictureDialog2: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Metafiles (*.wmf)|*.wm' +
      'f'
    Left = 241
    Top = 9
  end
end
