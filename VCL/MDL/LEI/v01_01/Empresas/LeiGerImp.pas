unit LeiGerImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  mySQLDbTables, frxClass, dmkEdit, dmkEditCB, dmkDBLookupComboBox, frxDBSet,
  frxChart, Variants;

type
  TFmLeiGerImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnSetores: TPanel;
    DBGrid2: TDBGrid;
    Panel5: TPanel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrCons: TmySQLQuery;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    QrProds: TmySQLQuery;
    DsProds: TDataSource;
    QrProdsNome: TWideStringField;
    QrProdsAtivo: TSmallintField;
    QrProdsCodigo: TIntegerField;
    frxListaConsData: TfrxReport;
    Panel6: TPanel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    QrCNS: TmySQLQuery;
    QrCNSValor: TFloatField;
    QrCNSCasas: TSmallintField;
    QrCNSUnidImp: TWideStringField;
    QrCNSUnidFat: TFloatField;
    QrCNSCONSUMO2_TXT: TWideStringField;
    QrCNSCONSUMO2: TFloatField;
    QrCNSData: TDateField;
    DsCNS: TDataSource;
    frxDsCNS: TfrxDBDataset;
    QrCNSNome: TWideStringField;
    QrCNSCons: TIntegerField;
    QrCNSMedida: TFloatField;
    QrTarifaMin: TmySQLQuery;
    QrTarifaMinValor: TFloatField;
    QrCNSTarifaMin: TFloatField;
    QrSumTarifa: TmySQLQuery;
    FloatField1: TFloatField;
    frxDsSumTarifa: TfrxDBDataset;
    CkGrafico: TCheckBox;
    PCRelatorio: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    GBPeriodo: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBAnoIni: TComboBox;
    CBMesIni: TComboBox;
    Label1: TLabel;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    Label2: TLabel;
    QrCNSPeriodo_TXT: TWideStringField;
    frxListaConsPeri: TfrxReport;
    QrCNSPeriDif: TIntegerField;
    QrCNSCons_Tipo: TIntegerField;
    QrGrafico: TmySQLQuery;
    QrGraficoValor: TFloatField;
    QrGraficoCasas: TSmallintField;
    QrGraficoUnidImp: TWideStringField;
    QrGraficoUnidFat: TFloatField;
    QrGraficoCONSUMO2_TXT: TWideStringField;
    QrGraficoCONSUMO2: TFloatField;
    QrGraficoData: TDateField;
    QrGraficoNome: TWideStringField;
    QrGraficoCons: TIntegerField;
    QrGraficoMedida: TFloatField;
    QrGraficoTarifaMin: TFloatField;
    QrGraficoPeriodo_TXT: TWideStringField;
    QrGraficoPeriDif: TIntegerField;
    QrGraficoCons_Tipo: TIntegerField;
    QrSUMCns: TmySQLQuery;
    DsSUMCns: TDataSource;
    frxDsSUMCns: TfrxDBDataset;
    QrSUMCnsCasas: TSmallintField;
    QrSUMCnsUnidImp: TWideStringField;
    QrSUMCnsUnidFat: TFloatField;
    QrSUMCnsData: TDateField;
    QrSUMCnsMedida: TFloatField;
    QrSUMCnsValor: TFloatField;
    QrSUMCnsCONSUMO2_TXT: TWideStringField;
    QrSUMCnsCONSUMO2: TFloatField;
    QrSUMCnsCons_Tipo: TIntegerField;
    QrSUMCnsCons_Tipo_Str: TWideStringField;
    frxListaConsDataN: TfrxReport;
    QrCNSMedAnt: TFloatField;
    QrCNSMedAtu: TFloatField;
    QrDatas: TMySQLQuery;
    QrDatasData: TDateField;
    QrMeses: TMySQLQuery;
    QrMesesPeriodo_TXT: TWideStringField;
    QrCNSAno: TLargeintField;
    QrCNSMes: TLargeintField;
    QrGraficoAno: TLargeintField;
    QrGraficoMes: TLargeintField;
    QrMesesAno: TLargeintField;
    QrMesesMes: TLargeintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrCNSCalcFields(DataSet: TDataSet);
    procedure frxListaConsDataGetValue(const VarName: string; var Value: Variant);
    procedure QrGraficoCalcFields(DataSet: TDataSet);
    procedure QrSUMCnsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FPerInt: Integer;
    FTmpTab, FSQL_Periodo, FProdsTxt, FProdsCod: String;
    procedure ReopenCNS(QueryCNS, QuerySUMTarifa, QuerySUMCNS: TmySQLQuery;
              TipoCns: Integer = 0);
    procedure ConfiguraCons();
    procedure ReopenProds(Nivel1: Integer);
    procedure AtivarTodos(Ativo: Integer);
    procedure ConfiguraGrafico(Relatorio: TfrxReport; NomeGrafico: String);
    function  ObtemValorTarifaMin(Empresa, Codigo: Integer): Double;
    function  ObtemProds: Boolean;
  public
    { Public declarations }
    FPeriodo: Integer;
  end;

  var
  FmLeiGerImp: TFmLeiGerImp;

implementation

uses UnMyObjects, UMySQLModule, MyVCLSkin, Module, ModuleGeral, UCreate,
  DmkDAC_PF, UnDmkProcFunc, UnConsumo;

{$R *.DFM}

const
  MaxBombas = 256;

procedure TFmLeiGerImp.AtivarTodos(Ativo: Integer);
var
  Nivel1: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Nivel1 := QrProdsCodigo.Value;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
      'UPDATE ' + FTmpTab + ' SET Ativo=' + Geral.FF0(Ativo),
      '']);
    //
    ReopenProds(Nivel1);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLeiGerImp.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmLeiGerImp.ConfiguraGrafico(Relatorio: TfrxReport; NomeGrafico: String);
const
  sProcName = 'TFmLeiGerImp.ConfiguraGrafico()';
var
  Chart1: TfrxChartView;

  procedure LimpaGrafico();
  begin
    Chart1 := Relatorio.FindObject(NomeGrafico) as TfrxChartView;
    while Chart1.SeriesData.Count > 0 do
      Chart1.SeriesData[0].Free;
    while Chart1.Chart.SeriesCount > 0 do
      Chart1.Chart.Series[0].Free;
  end;

  procedure CriaBanda(Banda: Integer; Legenda: String);
  begin
    Chart1.AddSeries(TfrxChartSeries.csLine);

    Chart1.Chart.Series[Banda].Pen.Width   := 2;
    Chart1.Chart.Series[Banda].LegendTitle := Legenda;
    Chart1.SeriesData[Banda].DataType := dtFixedData;
    Chart1.SeriesData[Banda].SortOrder := soNone;
    Chart1.SeriesData[Banda].TopN := 0;
    Chart1.SeriesData[Banda].XType := xtText;
  end;

  procedure InsereValorBanda(Banda: Integer; Valor1, Valor2: String);
  begin
    Chart1.SeriesData[Banda].XSource := Chart1.SeriesData[Banda].XSource + Valor1;
    Chart1.SeriesData[Banda].YSource := Chart1.SeriesData[Banda].YSource + Valor2;
  end;

var
  Valor1, Valor2: String;
  Cons, ConsAtu, Banda, Ini, Fim, Dias, I, J, Atu, Seq, Meses: Integer;
  ArrDatas: array of String; // TDatetime;
  ArrCons2: array of array[0..MaxBombas-1] of Double;
  //Bandas: array[0..MaxBombas-1] of String;
  DiaIni: TDateTime;
begin
  LimpaGrafico();
  //
  Banda   := 0;
  ConsAtu := 0;
  //
  if Lowercase(NomeGrafico) = 'chart1' then
  begin
(*
    C�digo de antes de 2023-07-25!!!
    =================================
    while not QrGrafico.Eof do
    begin
      Cons := QrGrafico.FieldByName('Cons').AsInteger;
      if ConsAtu <> Cons then
      begin
        CriaBanda(Banda, QrGrafico.FieldByName('Nome').AsString);
        ConsAtu := Cons;
        Banda   := Banda + 1;
      end;
      Valor1 := Geral.FDT(QrGrafico.FieldByName('Data').AsDateTime, 2) + ';';
      Valor2 := FloatToStr(QrGrafico.FieldByName('CONSUMO2').AsFloat) + ';';
      //
      InsereValorBanda(Banda - 1, Valor1, Valor2);
      //
      QrGrafico.Next;
    end;
*)
    case PCRelatorio.ActivePageIndex of
      0:// Di�rio
      begin
        QrDatas.Last;
        Fim := Trunc(QrDatasData.Value);
        QrDatas.First;
        DiaIni := QrDatasData.Value;
        Ini := Trunc(QrDatasData.Value);
        Dias := Fim - ini + 1;
        //
        SetLength(ArrDatas, Dias);
        SetLength(ArrCons2, Dias);
        for I := 0 to Dias - 1 do
        begin
          ArrDatas[I] := Geral.FDT(DiaIni + I, 2);
          for J := 0 to MaxBombas - 1 do
            ArrCons2[I, J] := 0;
        end;
        //
        while not QrGrafico.Eof do
        begin
          Cons := QrGrafico.FieldByName('Cons').AsInteger;
          if ConsAtu <> Cons then
          begin
            CriaBanda(Banda, QrGrafico.FieldByName('Nome').AsString);
            ConsAtu := Cons;
            Banda   := Banda + 1;
          end;
          Atu := Integer(Trunc(QrGrafico.FieldByName('Data').AsDateTime));
          Seq := Atu - Ini;
          ArrCons2[Seq, Banda - 1] := QrGrafico.FieldByName('CONSUMO2').AsFloat;
          //
          QrGrafico.Next;
        end;
        //
        for I := 0 to Dias - 1 do
        begin
          for J := 0 to Banda - 1 do
          begin
            Valor1 := ArrDatas[I] + ';';
            Valor2 := FloatToStr(ArrCons2[I,J]) + ';';
            InsereValorBanda(J, Valor1, Valor2);
          end;
        end;
      end;
      1: // Mensal
      begin
        QrMeses.Last;
        Fim := Geral.AnoEMesToPeriodo(QrMesesAno.Value, QrMesesMes.Value);
        QrMeses.First;
        Ini := Geral.AnoEMesToPeriodo(QrMesesAno.Value, QrMesesMes.Value);
        Meses := Fim - ini + 1;
        //
        SetLength(ArrDatas, Meses);
        SetLength(ArrCons2, Meses);
        for I := 0 to Meses - 1 do
        begin
          ArrDatas[I] := Geral.PTM(Ini + I);
          for J := 0 to MaxBombas - 1 do
            ArrCons2[I, J] := 0;
        end;
        //
        while not QrGrafico.Eof do
        begin
          Cons := QrGrafico.FieldByName('Cons').AsInteger;
          if ConsAtu <> Cons then
          begin
            CriaBanda(Banda, QrGrafico.FieldByName('Nome').AsString);
            ConsAtu := Cons;
            Banda   := Banda + 1;
          end;
          Atu := Geral.AnoEMesToPeriodo(QrGraficoAno.Value, QrGraficoMes.Value);
          Seq := Atu - Ini;
          ArrCons2[Seq, Banda - 1] := QrGrafico.FieldByName('CONSUMO2').AsFloat;
          //
          QrGrafico.Next;
        end;
        //
        for I := 0 to Meses - 1 do
        begin
          for J := 0 to Banda - 1 do
          begin
            Valor1 := ArrDatas[I] + ';';
            Valor2 := FloatToStr(ArrCons2[I,J]) + ';';
            InsereValorBanda(J, Valor1, Valor2);
          end;
        end;
      end;
      else
        Geral.MB_Erro('Per�odicidade n�o definida!' + sLineBreak + sProcName);
    end;
  end;
  //
  if Lowercase(NomeGrafico) = 'chart2' then
  begin
    CriaBanda(Banda, 'Somat�rio');
    Banda := 1;
    while not QrSumCNS.Eof do
    begin
      Valor1 := Geral.FDT(QrSumCNS.FieldByName('Data').AsDateTime, 2) + ';';
      Valor2 := FloatToStr(QrSumCNS.FieldByName('CONSUMO2').AsFloat) + ';';
      //
      InsereValorBanda(Banda - 1, Valor1, Valor2);
      //
      QrSumCNS.Next;
    end;
  end;
end;

procedure TFmLeiGerImp.BtOKClick(Sender: TObject);
var
  Relatorio: TfrxReport;
begin
  case PCRelatorio.ActivePageIndex of
    0: //Data
      Relatorio := frxListaConsDataN;
    1: //Per�odo
      Relatorio := frxListaConsPeri;
    else
    begin
      Geral.MB_Erro('Relat�rio n�o implementado!');
      Exit;
    end;
  end;
  ReopenCNS(QrCNS, QrSUMTarifa, QrSUMCNS);
  //dmkPF.LeMeuTexto(QrSUMCNS.SQL.Text);
  //
  if QrCNS.RecordCount > 0 then
  begin
    MyObjects.frxDefineDataSets(Relatorio, [
      DModG.frxDsDono,
      frxDsCNS,
      frxDsSUMCns,
      frxDsSUMTarifa
      ]);
    MyObjects.frxMostra(Relatorio, 'Lista de consumos no per�odo');
  end else
    Geral.MB_Aviso('Nenhum registro foi localizado!');
end;

procedure TFmLeiGerImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLeiGerImp.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmLeiGerImp.ConfiguraCons;
var
  Nome: String;
  Codigo: Integer;
begin
  FTmpTab := UCriar.RecriaTempTableNovo(ntrttSelCods, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCons, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM cons ',
    'ORDER BY Nome',
    '']);
  if QrCons.RecordCount > 0 then
  begin
    while not QrCons.EOF do
    begin
      Codigo := QrConsCodigo.Value;
      Nome   := QrConsNome.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FTmpTab, False,
        ['Nome', 'Ativo'], ['Nivel1'], [Nome, 1], [Codigo], False);
      //
      QrCons.Next;
    end;
    ReopenProds(0);
  end;
end;

procedure TFmLeiGerImp.DBGrid2CellClick(Column: TColumn);
var
  Nivel1, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Nivel1 := QrProdsCodigo.Value;
    //
    if QrProdsAtivo.Value = 0 then
      Ativo := 1
    else
      Ativo := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'UPDATE ' + FTmpTab,
      'SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Nivel1=' + Geral.FF0(Nivel1),
      '']);
    //
    ReopenProds(Nivel1);
  end;
end;

procedure TFmLeiGerImp.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, QrProdsAtivo.Value);
end;

procedure TFmLeiGerImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLeiGerImp.FormCreate(Sender: TObject);
var
  Chart1: TfrxChartView;
begin
  ImgTipo.SQLType := stLok;
  //
  ConfiguraCons;
end;

procedure TFmLeiGerImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLeiGerImp.FormShow(Sender: TObject);
begin
  EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
  CkGrafico.Checked      := True;
  //
  if FPeriodo <> 0 then
  begin
    TPIni.Date := dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodo);
    TPFim.Date := dmkPF.UltimoDiaDoPeriodo_Date(FPeriodo);
  end else
  begin
    TPIni.Date := Geral.PrimeiroDiaDoMes(Date);
    TPFim.Date := Geral.UltimoDiaDoMes(Date);
  end;
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni, -1);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim, 0);
  //
  PCRelatorio.ActivePageIndex := 0;
end;

procedure TFmLeiGerImp.frxListaConsDataGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VAR_PRODSTXT') = 0 then
    Value := FProdsTxt
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True, False, False, '', '')
  else if AnsiCompareText(VarName, 'VAR_MOSTRAGRAFICO') = 0 then
    Value := Geral.BoolToInt(CkGrafico.Checked)
  else if AnsiCompareText(VarName, 'VAR_GRAFICO_DATA') = 0 then
  begin
    ReopenCNS(QrGrafico, nil, nil, QrCNSCons_Tipo.Value);
    ConfiguraGrafico(frxListaConsDataN, 'Chart1');
    Value := True;
  end else
  if AnsiCompareText(VarName, 'VAR_GRAFICO_SUMD') = 0 then
  begin
    ReopenCNS(nil, nil, QrSUMCNS, QrCNSCons_Tipo.Value);
    ConfiguraGrafico(frxListaConsDataN, 'Chart2');
    Value := True;
  end else
  if AnsiCompareText(VarName, 'VAR_GRAFICO_PERI') = 0 then
  begin
    ReopenCNS(QrGrafico, nil, nil, QrCNSCons_Tipo.Value);
    ConfiguraGrafico(frxListaConsPeri, 'Chart1');
    Value := True;
  end else
  if AnsiCompareText(VarName, 'VAR_GRAFICO_SUMP') = 0 then
  begin
    ReopenCNS(nil, nil, QrSUMCNS, QrCNSCons_Tipo.Value);
    ConfiguraGrafico(frxListaConsPeri, 'Chart2');
    Value := True;
  end else
  if AnsiCompareText(VarName, 'VAR_Cons_Tipo') = 0 then
    Value := UConsumo.ObtemTipoConsumo(QrCNSCons_Tipo.Value)
  else if AnsiCompareText(VarName, 'VAR_QrSUM') = 0 then
    Value := (QrSUMCns.State <> dsInactive) and (QrSUMCns.RecordCount > 0);
end;

function TFmLeiGerImp.ObtemProds: Boolean;
var
  Reg: Integer;
  Liga1, Liga2: String;
begin
  Result := False;
  //
  if (QrProds.State <> dsInactive) and (QrProds.RecordCount > 0) then
  begin
    Liga1     := '';
    Liga2     := '';
    FProdsCod := '';
    FProdsTxt := '';
    Reg       := 0;
    //
    QrProds.First;
    //
    while not QrProds.Eof do
    begin
      if QrProdsAtivo.Value = 1 then
      begin
        Reg       := Reg + 1;
        FProdsCod := FProdsCod + Liga1 + Geral.FF0(QrProdsCodigo.Value);
        FProdsTxt := FProdsTxt + Liga2 + QrProdsNome.Value;
        Liga1 := ',';
        Liga2 := ', ';
      end;
      QrProds.Next;
    end;
    if QrProds.RecordCount = Reg then
      FProdsTxt := 'TODOS';
    //
    if Reg = 0 then
    begin
      Geral.MB_Aviso('Defina pelo menos um item!');
    end else
      Result := True;
  end;
end;

procedure TFmLeiGerImp.QrCNSCalcFields(DataSet: TDataSet);
begin
  QrCNSPeriDif.Value      := FPerInt;
  QrCNSTarifaMin.Value    := ObtemValorTarifaMin(EdEmpresa.ValueVariant, QrCNSCons.Value);
  QrCNSCONSUMO2.Value     := QrCNSMedida.Value * QrCNSUnidFat.Value;
  QrCNSConsumo2_TXT.Value := Geral.FFT(QrCNSCONSUMO2.Value,
                               QrCNSCasas.Value, siNegativo) + ' ' +
                               QrCNSUnidImp.Value;
end;

procedure TFmLeiGerImp.QrGraficoCalcFields(DataSet: TDataSet);
begin
  QrGraficoPeriDif.Value      := FPerInt;
  QrGraficoTarifaMin.Value    := ObtemValorTarifaMin(EdEmpresa.ValueVariant, QrGraficoCons.Value);
  QrGraficoCONSUMO2.Value     := QrGraficoMedida.Value * QrGraficoUnidFat.Value;
  QrGraficoConsumo2_TXT.Value := Geral.FFT(QrGraficoCONSUMO2.Value,
                               QrGraficoCasas.Value, siNegativo) + ' ' +
                               QrGraficoUnidImp.Value;
end;

procedure TFmLeiGerImp.QrSUMCnsCalcFields(DataSet: TDataSet);
begin
  QrSUMCnsCONSUMO2.Value      := QrSUMCnsMedida.Value * QrSUMCnsUnidFat.Value;
  QrSUMCnsConsumo2_TXT.Value  := Geral.FFT(QrSUMCnsCONSUMO2.Value,
                                   QrSUMCnsCasas.Value, siNegativo) + ' ' +
                                   QrSUMCnsUnidImp.Value;
  QrSUMCnsCons_Tipo_Str.Value := UConsumo.ObtemTipoConsumo(QrSUMCnsCons_Tipo.Value);
end;

procedure TFmLeiGerImp.ReopenCNS(QueryCNS, QuerySUMTarifa, QuerySUMCNS: TmySQLQuery;
  TipoCns: Integer = 0);
const
  sProcName = 'TFmLeiGerImp.ReopenCNS()';
var
  Query: TmySQLQuery;
  PeriodoIni, PeriodoFim, CliInt: Integer;
  SQLConsValores, SQLGroupBy, SQLGroupBy2, SQLTipo: String;
begin
  if QueryCNS <> nil then
    QueryCNS.Close;
  if QuerySUMTarifa <> nil then
    QuerySUMTarifa.Close;
  if QuerySUMCNS <> nil then
    QuerySUMCNS.Close;
  //
  if ObtemProds then
  begin
    CliInt := EdEmpresa.ValueVariant;
    //
    if MyObjects.FIC(CliInt = 0, EdEmpresa, 'Defina a empresa!') then Exit;
    //
    case PCRelatorio.ActivePageIndex of
      0: //Data
      begin
        FSQL_Periodo   := dmkPF.SQL_Periodo('AND cni.Data ', TPIni.Date, TPFim.Date, True, True);
        FPerInt        := 1;
        SQLConsValores := 'MedAtu - MedAnt Medida, cni.Valor ';
        SQLGroupBy     := '';
        SQLGroupBy2    := 'GROUP BY cns.Export_Tip, cni.Data';
      end;
      1: //Per�odo
      begin
        PeriodoIni     := dmkPF.PeriodoEncode(Geral.IMV(CBAnoIni.Items[CBAnoIni.ItemIndex]),
                            CBMesIni.ItemIndex + 1);
        PeriodoFim     := dmkPF.PeriodoEncode(Geral.IMV(CBAnoFim.Items[CBAnoFim.ItemIndex]),
                            CBMesFim.ItemIndex + 1);
        FSQL_Periodo   := dmkPF.SQL_Periodo('AND cni.Data ', dmkPF.PrimeiroDiaDoPeriodo_Date(PeriodoIni),
                            dmkPF.UltimoDiaDoPeriodo_Date(PeriodoFim), True, True);
        SQLConsValores := 'SUM(MedAtu - MedAnt) Medida, SUM(cni.Valor) Valor ';
        SQLGroupBy     := 'GROUP BY DATE_FORMAT(cni.Data, "%m/%Y"), cns.Codigo ';
        SQLGroupBy2    := 'GROUP BY cns.Export_Tip, DATE_FORMAT(cni.Data, "%m/%Y") ';
        FPerInt        := PeriodoFim - PeriodoIni + 1;
        //
        if FPerInt <= 0 then
          FPerInt := 1;
      end;
      else
        Exit;
    end;
    if TipoCns <> 0 then
      SQLTipo := 'AND cns.Export_Tip = ' + Geral.FF0(TipoCns)
    else
      SQLTipo := '';
    //
    if QueryCNS <> nil then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryCNS, Dmod.MyDB, [
      'SELECT cns.Export_Tip Cons_Tipo, cns.Codigo Cons, cns.Nome, cnp.Casas, cnp.UnidImp, ',
      'cnp.UnidFat, cni.Data, DATE_FORMAT(cni.Data, "%m/%Y") Periodo_TXT, ',
      'MedAnt, MedAtu, CAST(Year(cni.Data) AS UNSIGNED) Ano, CAST(MONTH(cni.Data) AS UNSIGNED) Mes, ',
      SQLConsValores,
      'FROM cons cns ',
      'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
      'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
      'WHERE cni.Empresa=' + Geral.FF0(CliInt),
      'AND cnp.Cond=' + Geral.FF0(CliInt),
      'AND cns.Codigo IN (' + FProdsCod + ')',
      FSQL_Periodo,
      'AND cni.Tipo=0 ',
      SQLTipo,
      SQLGroupBy,
      'ORDER BY Cons, cni.Data, cni.DataCad, cni.MedAtu ASC ',
      '']);
      //Geral.MB_Teste(QueryCNS.SQL.Text);
        //
      case PCRelatorio.ActivePageIndex of
        0:  // Di�rio
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrDatas, Dmod.MyDB, [
          'SELECT DISTINCT(cni.Data) Data ',
          'FROM cons cns ',
          'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
          'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
          'WHERE cni.Empresa=' + Geral.FF0(CliInt),
          'AND cnp.Cond=' + Geral.FF0(CliInt),
          'AND cns.Codigo IN (' + FProdsCod + ')',
          FSQL_Periodo,
          'AND cni.Tipo=0 ',
          SQLTipo,
          'ORDER BY Data ',
          '']);
        end;
        1:  // Mensal
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrMeses, Dmod.MyDB, [
          'SELECT DISTINCT DATE_FORMAT(cni.Data, "%m/%Y") Periodo_TXT, ',
          'CAST(Year(cni.Data) AS UNSIGNED) Ano, CAST(MONTH(cni.Data) AS UNSIGNED) Mes ',
          'FROM cons cns ',
          'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
          'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
          'WHERE cni.Empresa=' + Geral.FF0(CliInt),
          'AND cnp.Cond=' + Geral.FF0(CliInt),
          'AND cns.Codigo IN (' + FProdsCod + ')',
          FSQL_Periodo,
          'AND cni.Tipo=0 ',
          SQLTipo,
          'ORDER BY Data ',
          '']);
        end;
        else
        Geral.MB_Erro('Per�odicidade n�o definida!' + sLineBreak + sProcName);
      end;
    end;
    if QuerySUMTarifa <> nil then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QuerySUMTarifa, Dmod.MyDB, [
      'SELECT SUM(cni.Valor) Valor ',
      'FROM leigerits cni ',
      'LEFT JOIN cons cns ON cns.Codigo=cni.Codigo ',
      'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
      'WHERE cni.Empresa=' + Geral.FF0(CliInt),
      'AND cnp.Cond=' + Geral.FF0(CliInt),
      'AND cns.Codigo IN (' + FProdsCod + ')',
      SQLTipo,
      FSQL_Periodo,
      'AND Tipo=1 ',
      '']);
    end;
    if QuerySUMCNS <> nil then
    begin
      Query := TmySQLQuery.Create(Dmod.MyDB);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SELECT COUNT(DISTINCT cns.Codigo) Tipo ',
        'FROM cons cns ',
        'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
        'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
        'WHERE cni.Empresa=' + Geral.FF0(CliInt),
        'AND cnp.Cond=' + Geral.FF0(CliInt),
        'AND cns.Codigo IN (' + FProdsCod + ')',
        FSQL_Periodo,
        'AND cni.Tipo=0 ',
        SQLTipo,
        'GROUP BY cns.Export_Tip ',
        'ORDER BY Tipo DESC LIMIT 1 ',
        '']);
        if (Query.RecordCount > 0) and (Query.FieldByName('Tipo').AsInteger > 1) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QuerySUMCNS, Dmod.MyDB, [
          'SELECT cns.Export_Tip Cons_Tipo, cnp.Casas, cnp.UnidImp, ',
          'cnp.UnidFat, cni.Data, ',
          'SUM(MedAtu - MedAnt) Medida, SUM(cni.Valor) Valor ',
          'FROM cons cns ',
          'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
          'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
          'WHERE cni.Empresa=' + Geral.FF0(CliInt),
          'AND cnp.Cond=' + Geral.FF0(CliInt),
          'AND cns.Codigo IN (' + FProdsCod + ')',
          FSQL_Periodo,
          'AND cni.Tipo=0 ',
          SQLTipo,
          SQLGroupBy2,
          'ORDER BY cns.Export_Tip, cni.Data, cni.DataCad, cni.MedAtu ASC ',
          '']);
          //Geral.MB_teste(QuerySUMCNS.SQL.Text);
        end else
          QuerySUMCNS.Close;
      finally
        Query.Free;
      end;
    end;
  end;
end;

procedure TFmLeiGerImp.ReopenProds(Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProds, DModG.MyPID_DB, [
    'SELECT Nivel1 Codigo, Nome, Ativo ',
    'FROM ' + FTmpTab,
    'ORDER BY Nome',
    '']);
  if Nivel1 <> 0 then
    QrProds.Locate('Codigo', Nivel1, []);
end;

function TFmLeiGerImp.ObtemValorTarifaMin(Empresa, Codigo: Integer): Double;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTarifaMin, Dmod.MyDB, [
    'SELECT SUM(cni.Valor) Valor ',
    'FROM leigerits cni ',
    'LEFT JOIN cons cns ON cns.Codigo=cni.Codigo ',
    'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
    'WHERE cni.Empresa=' + Geral.FF0(Empresa),
    'AND cnp.Cond=' + Geral.FF0(Empresa),
    'AND cns.Codigo=' + Geral.FF0(Codigo),
    FSQL_Periodo,
    'AND Tipo=1 ',
    '']);
  Result := QrTarifaMinValor.Value;
  //
  QrTarifaMin.Close;
end;

end.
