object FmLeiGerImp: TFmLeiGerImp
  Left = 339
  Top = 185
  Caption = 'LEI-GEREN-001 :: Gerenciamento de Consumo por leitura'
  ClientHeight = 529
  ClientWidth = 729
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 729
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 670
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 611
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 559
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Consumo por leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 559
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Consumo por leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 559
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Consumo por leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 729
    Height = 330
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 729
      Height = 330
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 68
        Width = 729
        Height = 262
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object PnSetores: TPanel
          Left = 456
          Top = 18
          Width = 271
          Height = 242
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object DBGrid2: TDBGrid
            Left = 0
            Top = 60
            Width = 271
            Height = 182
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsProds
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGrid2CellClick
            OnDrawColumnCell = DBGrid2DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Title.Caption = ' X'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'd'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 129
                Visible = True
              end>
          end
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 271
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object BtTodos: TBitBtn
              Tag = 127
              Left = 6
              Top = 5
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Todos'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtTodosClick
            end
            object BtNenhum: TBitBtn
              Tag = 128
              Left = 144
              Top = 5
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Nenhum'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtNenhumClick
            end
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 729
        Height = 68
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 17
          Top = 10
          Width = 58
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Empresa:'
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 82
          Top = 30
          Width = 597
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 0
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdEmpresa: TdmkEditCB
          Left = 17
          Top = 30
          Width = 65
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 389
    Width = 729
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 725
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 443
    Width = 729
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 550
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 548
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object CkGrafico: TCheckBox
    Left = 28
    Top = 343
    Width = 120
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Mostrar gr'#225'fico'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object PCRelatorio: TPageControl
    Left = 17
    Top = 134
    Width = 357
    Height = 202
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet2
    TabHeight = 25
    TabOrder = 5
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data'
      object GroupBox2: TGroupBox
        Left = 4
        Top = 2
        Width = 307
        Height = 93
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Per'#237'odo'
        TabOrder = 0
        object Label4: TLabel
          Left = 11
          Top = 25
          Width = 69
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data inicial:'
        end
        object Label5: TLabel
          Left = 155
          Top = 25
          Width = 59
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data final:'
        end
        object TPIni: TdmkEditDateTimePicker
          Left = 11
          Top = 44
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 37670.521748657400000000
          Time = 37670.521748657400000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPFim: TdmkEditDateTimePicker
          Left = 155
          Top = 44
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 37670.521748657400000000
          Time = 37670.521748657400000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Mensal'
      ImageIndex = 1
      object GBPeriodo: TGroupBox
        Left = 12
        Top = 12
        Width = 333
        Height = 137
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Periodo'
        TabOrder = 0
        object Label32: TLabel
          Left = 10
          Top = 18
          Width = 66
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's inicial:'
        end
        object LaAnoI: TLabel
          Left = 226
          Top = 18
          Width = 64
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano inicial:'
        end
        object Label1: TLabel
          Left = 10
          Top = 78
          Width = 56
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's final:'
        end
        object Label2: TLabel
          Left = 226
          Top = 78
          Width = 54
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano final:'
        end
        object CBAnoIni: TComboBox
          Left = 225
          Top = 39
          Width = 96
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object CBMesIni: TComboBox
          Left = 11
          Top = 39
          Width = 212
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object CBMesFim: TComboBox
          Left = 11
          Top = 98
          Width = 212
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object CBAnoFim: TComboBox
          Left = 225
          Top = 98
          Width = 96
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
      end
    end
  end
  object QrCons: TmySQLQuery
    Database = Dmod.MyDB
    Left = 501
    Top = 212
    object QrConsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrProds: TmySQLQuery
    Database = Dmod.MyDB
    Left = 444
    Top = 212
    object QrProdsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrProdsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrProdsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsProds: TDataSource
    DataSet = QrProds
    Left = 473
    Top = 212
  end
  object frxListaConsData: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 42551.383289236110000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Posi: Double;                                            '
      'begin'
      '  if <VAR_MOSTRAGRAFICO> = 0 then '
      '  begin'
      '    Chart1.Visible      := False;'
      '    GroupHeader1.Height := MeProd.Height + MeDtaCol1.Height;'
      '    //'
      
        '    Posi := MeProd.Top + MeProd.Height;                         ' +
        '                                                           '
      '  end else'
      '  begin'
      '    Chart1.Visible      := True;    '
      
        '    GroupHeader1.Height := MeProd.Height + Chart1.Height + MeDta' +
        'Col1.Height;                                                    ' +
        '                            '
      '    //'
      
        '    Posi := MeProd.Top + MeProd.Height + Chart1.Height;         ' +
        '                                                                ' +
        '           '
      '  end;'
      '  //        '
      '  MeDtaCol1.Top := Posi;'
      '  MeCnsCol1.Top := Posi;'
      '  MeValCol1.Top := Posi;'
      '  //'
      '  MeDtaCol2.Top := Posi;'
      '  MeCnsCol2.Top := Posi;'
      '  MeValCol2.Top := Posi;'
      '  //'
      '  MeDtaCol3.Top := Posi;'
      '  MeCnsCol3.Top := Posi;'
      '  MeValCol3.Top := Posi;'
      '  //'
      '  MeDtaCol4.Top := Posi;'
      '  MeCnsCol4.Top := Posi;'
      '  MeValCol4.Top := Posi;'
      'end;'
      ''
      'begin  '
      'end.')
    OnGetValue = frxListaConsDataGetValue
    Left = 214
    Top = 108
    Datasets = <
      item
        DataSet = frxDsCNS
        DataSetName = 'frxDsCNS'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSumTarifa
        DataSetName = 'frxDsSumTarifa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 14.314470000000000000
        Top = 566.929499999999900000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149474490000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA CONSUMOS PER'#205'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Top = 52.913254019999990000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Produtos:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 64.251968500000000000
          Top = 52.913254019999990000
          Width = 617.543290000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAR_PRODSTXT]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 37.795300000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 64.252010000000000000
          Top = 37.795300000000010000
          Width = 616.063348500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118276220000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        Columns = 4
        ColumnWidth = 170.078740157480000000
        DataSet = frxDsCNS
        DataSetName = 'frxDsCNS'
        RowCount = 0
        object Memo4: TfrxMemoView
          Left = 52.913151500000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'CONSUMO2_TXT'
          DataSet = frxDsCNS
          DataSetName = 'frxDsCNS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCNS."CONSUMO2_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 109.606101500000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsCNS
          DataSetName = 'frxDsCNS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCNS."Valor"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 0.000165980000019772
          Width = 52.913383390000000000
          Height = 15.118110240000000000
          DataSet = frxDsCNS
          DataSetName = 'frxDsCNS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNS."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 207.874306220000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsCNS."Cons"'
        object MeProd: TfrxMemoView
          Width = 680.314960630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCNS."Nome"]')
          ParentFont = False
        end
        object MeCnsCol1: TfrxMemoView
          Left = 52.913151500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol1: TfrxMemoView
          Left = 109.606101500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol1: TfrxMemoView
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCnsCol2: TfrxMemoView
          Left = 222.992001500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol2: TfrxMemoView
          Left = 279.684951500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol2: TfrxMemoView
          Left = 170.078850000000000000
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCnsCol3: TfrxMemoView
          Left = 393.070851500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol3: TfrxMemoView
          Left = 449.763801500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol3: TfrxMemoView
          Left = 340.157700000000000000
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCnsCol4: TfrxMemoView
          Left = 563.149701500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol4: TfrxMemoView
          Left = 619.842651500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol4: TfrxMemoView
          Left = 510.236550000000000000
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Chart1: TfrxChartView
          Top = 19.000000000000000000
          Width = 680.315400000000000000
          Height = 173.858380000000000000
          HighlightColor = clBlack
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C6508104C6567656E642E416C69676E6D656E7407086C61426F7474
            6F6D104C6567656E642E526F756E6453697A65021F154C6567656E642E536861
            646F772E56697369626C6508114C6567656E642E53686170655374796C650711
            666F73526F756E6452656374616E676C650E4C6567656E642E56697369626C65
            0812537562466F6F742E466F6E742E436F6C6F720709636C44656661756C7419
            426F74746F6D417869732E4461746554696D65466F726D6174060A44442F4D4D
            2F5959595918426F74746F6D417869732E5469746C652E43617074696F6E0604
            446174610D4672616D652E56697369626C6508164C656674417869732E546974
            6C652E43617074696F6E0607436F6E73756D6F175269676874417869732E5469
            746C652E43617074696F6E0607436F6E73756D6F165669657733444F7074696F
            6E732E526F746174696F6E02000A426576656C4F75746572070662764E6F6E65
            05436F6C6F720707636C576869746511436F6C6F7250616C65747465496E6465
            78020D000B544C696E655365726965730753657269657331134D61726B732E41
            72726F772E56697369626C6509194D61726B732E43616C6C6F75742E42727573
            682E436F6C6F720707636C426C61636B1B4D61726B732E43616C6C6F75742E41
            72726F772E56697369626C65090D4D61726B732E56697369626C650816506F69
            6E7465722E496E666C6174654D617267696E73090D506F696E7465722E537479
            6C65070B707352656374616E676C650F506F696E7465722E56697369626C6508
            0C5856616C7565732E4E616D650604446174610D5856616C7565732E4F726465
            72070B6C6F417363656E64696E670C5956616C7565732E4E616D650607436F6E
            73756D6F0D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 345
          SeriesData = <
            item
              InheritedName = 'TfrxSeriesItem2'
              DataType = dtBandData
              DataBand = frxListaConsData.MasterData1
              SortOrder = soNone
              TopN = 0
              XType = xtDate
              Source1 = 'frxDsCNS."Data"'
              Source2 = 'frxDsCNS."CONSUMO2"'
              XSource = 'frxDsCNS."Data"'
              YSource = 'frxDsCNS."CONSUMO2"'
            end>
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 34.015757800000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Top = 7.559059999999988000
          Width = 166.299268740000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. total: [SUM(<frxDsCNS."CONSUMO2">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 336.378170000000000000
          Top = 7.559059999999988000
          Width = 170.078742600000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Tar. m'#237'nima $: [frxDsCNS."TarifaMin"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 166.299320000000000000
          Top = 7.559059999999988000
          Width = 170.078742600000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. total $: [SUM(<frxDsCNS."Valor">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 506.457020000000000000
          Top = 7.559059999999988000
          Width = 170.078742600000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total $: [SUM(<frxDsCNS."Valor">) + <frxDsCNS."TarifaMin">]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 487.559370000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          Left = 506.457020000000000000
          Width = 170.078742600000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total $: [SUM(<frxDsCNS."Valor">) + <frxDsSumTarifa."Valor">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 166.299320000000000000
          Width = 170.078742600000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. total $: [SUM(<frxDsCNS."Valor">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Width = 166.299268740000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. total: [SUM(<frxDsCNS."CONSUMO2">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrCNS: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNSCalcFields
    Left = 396
    Top = 260
    object QrCNSValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNSCasas: TSmallintField
      FieldName = 'Casas'
    end
    object QrCNSUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 5
    end
    object QrCNSUnidFat: TFloatField
      FieldName = 'UnidFat'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCNSCONSUMO2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2_TXT'
      Size = 30
      Calculated = True
    end
    object QrCNSCONSUMO2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2'
      Calculated = True
    end
    object QrCNSData: TDateField
      FieldName = 'Data'
    end
    object QrCNSNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCNSCons: TIntegerField
      FieldName = 'Cons'
    end
    object QrCNSMedida: TFloatField
      FieldName = 'Medida'
    end
    object QrCNSTarifaMin: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TarifaMin'
      Calculated = True
    end
    object QrCNSPeriodo_TXT: TWideStringField
      FieldName = 'Periodo_TXT'
      Size = 7
    end
    object QrCNSPeriDif: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PeriDif'
      Calculated = True
    end
  end
  object DsCNS: TDataSource
    DataSet = QrCNS
    Left = 425
    Top = 260
  end
  object frxDsCNS: TfrxDBDataset
    UserName = 'frxDsCNS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Valor=Valor'
      'Casas=Casas'
      'UnidImp=UnidImp'
      'UnidFat=UnidFat'
      'CONSUMO2_TXT=CONSUMO2_TXT'
      'CONSUMO2=CONSUMO2'
      'Data=Data'
      'Nome=Nome'
      'Cons=Cons'
      'Medida=Medida'
      'TarifaMin=TarifaMin'
      'Periodo_TXT=Periodo_TXT'
      'PeriDif=PeriDif')
    DataSet = QrCNS
    BCDToCurrency = False
    Left = 453
    Top = 260
  end
  object QrTarifaMin: TmySQLQuery
    Database = Dmod.MyDB
    Left = 482
    Top = 260
    object QrTarifaMinValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSumTarifa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 510
    Top = 260
    object FloatField1: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxDsSumTarifa: TfrxDBDataset
    UserName = 'frxDsSumTarifa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Valor=Valor')
    DataSet = QrSumTarifa
    BCDToCurrency = False
    Left = 538
    Top = 260
  end
  object frxListaConsPeri: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 42550.505627870370000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Posi: Double;                                            '
      'begin'
      '  if <VAR_MOSTRAGRAFICO> = 0 then '
      '  begin'
      '    Chart1.Visible      := False;'
      '    GroupHeader1.Height := MeProd.Height + MeDtaCol1.Height;'
      '    //'
      
        '    Posi := MeProd.Top + MeProd.Height;                         ' +
        '                                                           '
      '  end else'
      '  begin'
      '    Chart1.Visible      := True;    '
      
        '    GroupHeader1.Height := MeProd.Height + Chart1.Height + MeDta' +
        'Col1.Height;                                                    ' +
        '                            '
      '    //'
      
        '    Posi := MeProd.Top + MeProd.Height + Chart1.Height;         ' +
        '                                                                ' +
        '           '
      '  end;'
      '  //        '
      '  MeDtaCol1.Top := Posi;'
      '  MeCnsCol1.Top := Posi;'
      '  MeValCol1.Top := Posi;'
      '  //'
      '  MeDtaCol2.Top := Posi;'
      '  MeCnsCol2.Top := Posi;'
      '  MeValCol2.Top := Posi;'
      '  //'
      '  MeDtaCol3.Top := Posi;'
      '  MeCnsCol3.Top := Posi;'
      '  MeValCol3.Top := Posi;'
      '  //'
      '  MeDtaCol4.Top := Posi;'
      '  MeCnsCol4.Top := Posi;'
      '  MeValCol4.Top := Posi;'
      'end;'
      ''
      'begin  '
      'end.')
    OnGetValue = frxListaConsDataGetValue
    Left = 310
    Top = 124
    Datasets = <
      item
        DataSet = frxDsCNS
        DataSetName = 'frxDsCNS'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSumTarifa
        DataSetName = 'frxDsSumTarifa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 14.314470000000000000
        Top = 582.047620000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149474490000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA CONSUMOS PER'#205'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Top = 52.913254019999990000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Produtos:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 64.251968500000000000
          Top = 52.913254019999990000
          Width = 617.543290000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAR_PRODSTXT]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 37.795300000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 64.252010000000000000
          Top = 37.795300000000010000
          Width = 616.063348500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118276220000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        Columns = 4
        ColumnWidth = 170.078740157480000000
        DataSet = frxDsCNS
        DataSetName = 'frxDsCNS'
        RowCount = 0
        object Memo4: TfrxMemoView
          Left = 52.913151500000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'CONSUMO2_TXT'
          DataSet = frxDsCNS
          DataSetName = 'frxDsCNS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCNS."CONSUMO2_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 109.606101500000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsCNS
          DataSetName = 'frxDsCNS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCNS."Valor"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 0.000165980000019772
          Width = 52.913383390000000000
          Height = 15.118110240000000000
          DataField = 'Periodo_TXT'
          DataSet = frxDsCNS
          DataSetName = 'frxDsCNS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNS."Periodo_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 207.874306220000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsCNS."Cons"'
        object MeProd: TfrxMemoView
          Width = 680.314960630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCNS."Nome"]')
          ParentFont = False
        end
        object MeCnsCol1: TfrxMemoView
          Left = 52.913151500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol1: TfrxMemoView
          Left = 109.606101500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol1: TfrxMemoView
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Periodo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCnsCol2: TfrxMemoView
          Left = 222.992001500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol2: TfrxMemoView
          Left = 279.684951500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol2: TfrxMemoView
          Left = 170.078850000000000000
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Periodo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCnsCol3: TfrxMemoView
          Left = 393.070851500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol3: TfrxMemoView
          Left = 449.763801500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol3: TfrxMemoView
          Left = 340.157700000000000000
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Periodo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCnsCol4: TfrxMemoView
          Left = 563.149701500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeValCol4: TfrxMemoView
          Left = 619.842651500000000000
          Top = 192.756030000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeDtaCol4: TfrxMemoView
          Left = 510.236550000000000000
          Top = 192.756195980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = FmPQImp.frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Periodo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Chart1: TfrxChartView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 173.858380000000000000
          HighlightColor = clBlack
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C6508104C6567656E642E416C69676E6D656E7407086C61426F7474
            6F6D104C6567656E642E526F756E6453697A65021F154C6567656E642E536861
            646F772E56697369626C6508114C6567656E642E53686170655374796C650711
            666F73526F756E6452656374616E676C650E4C6567656E642E56697369626C65
            0819426F74746F6D417869732E4461746554696D65466F726D617406074D4D2F
            595959591A426F74746F6D417869732E4C6F6761726974686D69634261736505
            0000000000000080004018426F74746F6D417869732E5469746C652E43617074
            696F6E1408000000506572C3AD6F646F0D4672616D652E56697369626C650816
            4C656674417869732E5469746C652E43617074696F6E0607436F6E73756D6F16
            5669657733444F7074696F6E732E526F746174696F6E02000A426576656C4F75
            746572070662764E6F6E6505436F6C6F720707636C576869746511436F6C6F72
            50616C65747465496E646578020D000B544C696E655365726965730753657269
            657331134D61726B732E4172726F772E56697369626C6509194D61726B732E43
            616C6C6F75742E42727573682E436F6C6F720707636C426C61636B1B4D61726B
            732E43616C6C6F75742E4172726F772E56697369626C65090D4D61726B732E56
            697369626C650816506F696E7465722E496E666C6174654D617267696E73090D
            506F696E7465722E5374796C65070B707352656374616E676C650F506F696E74
            65722E56697369626C65080C5856616C7565732E4E616D650604446174610D58
            56616C7565732E4F72646572070B6C6F417363656E64696E670C5956616C7565
            732E4E616D650607436F6E73756D6F0D5956616C7565732E4F7264657207066C
            6F4E6F6E65000000}
          ChartElevation = 345
          SeriesData = <
            item
              InheritedName = 'TfrxSeriesItem2'
              DataType = dtBandData
              DataBand = frxListaConsPeri.MasterData1
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsCNS."Periodo_TXT"'
              Source2 = 'frxDsCNS."CONSUMO2"'
              XSource = 'frxDsCNS."Periodo_TXT"'
              YSource = 'frxDsCNS."CONSUMO2"'
            end>
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 34.015748030000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Left = 226.771800000000000000
          Top = 7.559059999999988000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. total: [SUM(<frxDsCNS."CONSUMO2">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 453.543600000000000000
          Top = 7.559059999999988000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Tar. m'#237'nima $: [frxDsCNS."TarifaMin"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 340.157700000000000000
          Top = 7.559059999999988000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total cons. $: [SUM(<frxDsCNS."Valor">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 566.929499999999900000
          Top = 7.559059999999988000
          Width = 109.606296770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total $: [SUM(<frxDsCNS."Valor">) + <frxDsCNS."TarifaMin">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Top = 7.559055119999982000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. m'#233'dio: [SUM(<frxDsCNS."CONSUMO2">) / <frxDsCNS."PeriDif">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 113.385900000000000000
          Top = 7.559059999999988000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. m'#233'dio $: [SUM(<frxDsCNS."Valor">) / <frxDsCNS."PeriDif">]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 34.015748030000000000
        Top = 487.559370000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          Left = 566.929499999999900000
          Top = 7.559055119999982000
          Width = 109.606262600000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total $: [SUM(<frxDsCNS."Valor">) + <frxDsSumTarifa."Valor">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 7.559059999999988000
          Width = 113.385792600000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total cons. $: [SUM(<frxDsCNS."Valor">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Top = 7.559059999999988000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. m'#233'dio: [SUM(<frxDsCNS."CONSUMO2">) / <frxDsCNS."PeriDif">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 113.385900000000000000
          Top = 7.559059999999988000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. m'#233'dio $: [SUM(<frxDsCNS."Valor">) / <frxDsCNS."PeriDif">]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 226.771800000000000000
          Top = 7.559059999999988000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons. total: [SUM(<frxDsCNS."CONSUMO2">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
end
