unit LeiGerImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  mySQLDbTables, frxClass, dmkEdit, dmkEditCB, dmkDBLookupComboBox, frxDBSet;

type
  TFmLeiGerImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnSetores: TPanel;
    DBGrid2: TDBGrid;
    Panel5: TPanel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrCons: TmySQLQuery;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    QrProds: TmySQLQuery;
    DsProds: TDataSource;
    QrProdsNome: TWideStringField;
    QrProdsAtivo: TSmallintField;
    QrProdsCodigo: TIntegerField;
    frxListaConsData: TfrxReport;
    Panel6: TPanel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    QrCNS: TmySQLQuery;
    QrCNSValor: TFloatField;
    QrCNSCasas: TSmallintField;
    QrCNSUnidImp: TWideStringField;
    QrCNSUnidFat: TFloatField;
    QrCNSCONSUMO2_TXT: TWideStringField;
    QrCNSCONSUMO2: TFloatField;
    QrCNSData: TDateField;
    DsCNS: TDataSource;
    frxDsCNS: TfrxDBDataset;
    QrCNSNome: TWideStringField;
    QrCNSCons: TIntegerField;
    QrCNSMedida: TFloatField;
    QrTarifaMin: TmySQLQuery;
    QrTarifaMinValor: TFloatField;
    QrCNSTarifaMin: TFloatField;
    QrSumTarifa: TmySQLQuery;
    FloatField1: TFloatField;
    frxDsSumTarifa: TfrxDBDataset;
    CkGrafico: TCheckBox;
    PCRelatorio: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    GBPeriodo: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBAnoIni: TComboBox;
    CBMesIni: TComboBox;
    Label1: TLabel;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    Label2: TLabel;
    QrCNSPeriodo_TXT: TWideStringField;
    frxListaConsPeri: TfrxReport;
    QrCNSPeriDif: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrCNSCalcFields(DataSet: TDataSet);
    procedure frxListaConsDataGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FPerInt: Integer;
    FTmpTab, FSQL_Periodo, FProdsTxt, FProdsCod: String;
    procedure ConfiguraCons();
    procedure ReopenProds(Nivel1: Integer);
    procedure AtivarTodos(Ativo: Integer);
    function  ObtemValorTarifaMin(Empresa, Codigo: Integer): Double;
    function  ObtemProds: Boolean;
  public
    { Public declarations }
    FPeriodo: Integer;
  end;

  var
  FmLeiGerImp: TFmLeiGerImp;

implementation

uses UnMyObjects, UMySQLModule, MyVCLSkin, Module, ModuleGeral, UCreate,
  DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmLeiGerImp.AtivarTodos(Ativo: Integer);
var
  Nivel1: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Nivel1 := QrProdsCodigo.Value;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
      'UPDATE ' + FTmpTab + ' SET Ativo=' + Geral.FF0(Ativo),
      '']);
    //
    ReopenProds(Nivel1);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLeiGerImp.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmLeiGerImp.BtOKClick(Sender: TObject);
var
  PeriodoIni, PeriodoFim, CliInt: Integer;
  SQLConsValores, SQLGroupBy: String;
  Relatorio: TfrxReport;
begin
  if ObtemProds then
  begin
    CliInt := EdEmpresa.ValueVariant;
    //
    if MyObjects.FIC(CliInt = 0, EdEmpresa, 'Defina a empresa!') then Exit;
    //
    case PCRelatorio.ActivePageIndex of
      0: //Data
      begin
        FSQL_Periodo   := dmkPF.SQL_Periodo('AND cni.Data ', TPIni.Date, TPFim.Date, True, True);
        FPerInt        := 1;
        SQLConsValores := 'MedAtu - MedAnt Medida, cni.Valor ';
        SQLGroupBy     := '';
        Relatorio      := frxListaConsData;
      end;
      1: //Per�odo
      begin
        PeriodoIni     := dmkPF.PeriodoEncode(Geral.IMV(CBAnoIni.Items[CBAnoIni.ItemIndex]),
                            CBMesIni.ItemIndex + 1);
        PeriodoFim     := dmkPF.PeriodoEncode(Geral.IMV(CBAnoFim.Items[CBAnoFim.ItemIndex]),
                            CBMesFim.ItemIndex + 1);
        FSQL_Periodo   := dmkPF.SQL_Periodo('AND cni.Data ', dmkPF.PrimeiroDiaDoPeriodo_Date(PeriodoIni),
                            dmkPF.UltimoDiaDoPeriodo_Date(PeriodoFim), True, True);
        SQLConsValores := 'SUM(MedAtu - MedAnt) Medida, SUM(cni.Valor) Valor ';
        SQLGroupBy     := 'GROUP BY DATE_FORMAT(cni.Data, "%m/%Y"), cns.Codigo ';
        FPerInt        := PeriodoFim - PeriodoIni + 1;
        Relatorio      := frxListaConsPeri;
        //
        if FPerInt <= 0 then
          FPerInt := 1;
      end;
      else
        Exit;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrCNS, Dmod.MyDB, [
      'SELECT cns.Codigo Cons, cns.Nome, cnp.Casas, cnp.UnidImp, ',
      'cnp.UnidFat, cni.Data, DATE_FORMAT(cni.Data, "%m/%Y") Periodo_TXT, ',
      SQLConsValores,
      'FROM cons cns ',
      'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
      'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
      'WHERE cni.Empresa=' + Geral.FF0(CliInt),
      'AND cnp.Cond=' + Geral.FF0(CliInt),
      'AND cns.Codigo IN (' + FProdsCod + ')',
      FSQL_Periodo,
      'AND cni.Tipo=0 ',
      SQLGroupBy,
      'ORDER BY Cons, cni.Data, cni.DataCad, cni.MedAtu ASC ',
      '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrSUMTarifa, Dmod.MyDB, [
      'SELECT SUM(cni.Valor) Valor ',
      'FROM leigerits cni ',
      'LEFT JOIN cons cns ON cns.Codigo=cni.Codigo ',
      'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
      'WHERE cni.Empresa=' + Geral.FF0(CliInt),
      'AND cnp.Cond=' + Geral.FF0(CliInt),
      'AND cns.Codigo IN (' + FProdsCod + ')',
      FSQL_Periodo,
      'AND Tipo=1 ',
      '']);
    if QrCNS.RecordCount > 0 then
    begin
      MyObjects.frxDefineDataSets(Relatorio, [
        DModG.frxDsDono,
        frxDsCNS,
        frxDsSUMTarifa
        ]);
      MyObjects.frxMostra(Relatorio, 'Lista de consumos no per�odo');
    end else
      Geral.MB_Aviso('Nenhum registro foi localizado!');
  end;
end;

procedure TFmLeiGerImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLeiGerImp.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmLeiGerImp.ConfiguraCons;
var
  Nome: String;
  Codigo: Integer;
begin
  FTmpTab := UCriar.RecriaTempTableNovo(ntrttSelCods, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCons, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM cons ',
    'ORDER BY Nome',
    '']);
  if QrCons.RecordCount > 0 then
  begin
    while not QrCons.EOF do
    begin
      Codigo := QrConsCodigo.Value;
      Nome   := QrConsNome.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FTmpTab, False,
        ['Nome', 'Ativo'], ['Nivel1'], [Nome, 1], [Codigo], False);
      //
      QrCons.Next;
    end;
    ReopenProds(0);
  end;
end;

procedure TFmLeiGerImp.DBGrid2CellClick(Column: TColumn);
var
  Nivel1, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Nivel1 := QrProdsCodigo.Value;
    //
    if QrProdsAtivo.Value = 0 then
      Ativo := 1
    else
      Ativo := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'UPDATE ' + FTmpTab,
      'SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Nivel1=' + Geral.FF0(Nivel1),
      '']);
    //
    ReopenProds(Nivel1);
  end;
end;

procedure TFmLeiGerImp.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, QrProdsAtivo.Value);
end;

procedure TFmLeiGerImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLeiGerImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ConfiguraCons;
end;

procedure TFmLeiGerImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLeiGerImp.FormShow(Sender: TObject);
begin
  EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
  CkGrafico.Checked      := True;
  //
  if FPeriodo <> 0 then
  begin
    TPIni.Date := dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodo);
    TPFim.Date := dmkPF.UltimoDiaDoPeriodo_Date(FPeriodo);
  end else
  begin
    TPIni.Date := Geral.PrimeiroDiaDoMes(Date);
    TPFim.Date := Geral.UltimoDiaDoMes(Date);
  end;
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni, -1);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim, 0);
  //
  PCRelatorio.ActivePageIndex := 0;
end;

procedure TFmLeiGerImp.frxListaConsDataGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VAR_PRODSTXT') = 0 then
    Value := FProdsTxt
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True, False, False, '', '')
  else if AnsiCompareText(VarName, 'VAR_MOSTRAGRAFICO') = 0 then
    Value := Geral.BoolToInt(CkGrafico.Checked);
end;

function TFmLeiGerImp.ObtemProds: Boolean;
var
  Reg: Integer;
  Liga1, Liga2: String;
begin
  Result := False;
  //
  if (QrProds.State <> dsInactive) and (QrProds.RecordCount > 0) then
  begin
    Liga1     := '';
    Liga2     := '';
    FProdsCod := '';
    FProdsTxt := '';
    Reg       := 0;
    //
    QrProds.First;
    //
    while not QrProds.Eof do
    begin
      if QrProdsAtivo.Value = 1 then
      begin
        Reg       := Reg + 1;
        FProdsCod := FProdsCod + Liga1 + Geral.FF0(QrProdsCodigo.Value);
        FProdsTxt := FProdsTxt + Liga2 + QrProdsNome.Value;
        Liga1 := ',';
        Liga2 := ', ';
      end;
      QrProds.Next;
    end;
    if QrProds.RecordCount = Reg then
      FProdsTxt := 'TODOS';
    //
    if Reg = 0 then
    begin
      Geral.MB_Aviso('Defina pelo menos um item!');
    end else
      Result := True;
  end;
end;

procedure TFmLeiGerImp.QrCNSCalcFields(DataSet: TDataSet);
begin
  QrCNSPeriDif.Value      := FPerInt;
  QrCNSTarifaMin.Value    := ObtemValorTarifaMin(EdEmpresa.ValueVariant, QrCNSCons.Value);
  QrCNSCONSUMO2.Value     := QrCNSMedida.Value * QrCNSUnidFat.Value;
  QrCNSConsumo2_TXT.Value := Geral.FFT(QrCNSCONSUMO2.Value,
                               QrCNSCasas.Value, siNegativo) + ' ' +
                               QrCNSUnidImp.Value;
end;

procedure TFmLeiGerImp.ReopenProds(Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProds, DModG.MyPID_DB, [
    'SELECT Nivel1 Codigo, Nome, Ativo ',
    'FROM ' + FTmpTab,
    'ORDER BY Nome',
    '']);
  if Nivel1 <> 0 then
    QrProds.Locate('Codigo', Nivel1, []);
end;

function TFmLeiGerImp.ObtemValorTarifaMin(Empresa, Codigo: Integer): Double;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTarifaMin, Dmod.MyDB, [
    'SELECT SUM(cni.Valor) Valor ',
    'FROM leigerits cni ',
    'LEFT JOIN cons cns ON cns.Codigo=cni.Codigo ',
    'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
    'WHERE cni.Empresa=' + Geral.FF0(Empresa),
    'AND cnp.Cond=' + Geral.FF0(Empresa),
    'AND cns.Codigo=' + Geral.FF0(Codigo),
    FSQL_Periodo,
    'AND Tipo=1 ',
    '']);
  Result := QrTarifaMinValor.Value;
  //
  QrTarifaMin.Close;
end;

end.
