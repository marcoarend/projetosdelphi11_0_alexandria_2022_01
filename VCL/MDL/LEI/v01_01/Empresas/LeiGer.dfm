object FmLeiGer: TFmLeiGer
  Left = 368
  Top = 194
  Caption = 'LEI-GEREN-001 :: Gerenciamento de Consumo por leitura'
  ClientHeight = 609
  ClientWidth = 1006
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1006
    Height = 513
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1006
      Height = 140
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 80
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 58
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object GBPeriodo: TGroupBox
        Left = 16
        Top = 59
        Width = 270
        Height = 65
        Caption = 'Periodo'
        TabOrder = 3
        object Label32: TLabel
          Left = 8
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoI: TLabel
          Left = 184
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBAno: TComboBox
          Left = 183
          Top = 32
          Width = 78
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object CBMes: TComboBox
          Left = 9
          Top = 32
          Width = 172
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 136
        Top = 32
        Width = 485
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 80
        Top = 32
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 450
      Width = 1006
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 16
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 866
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 4
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1006
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 958
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 208
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 44
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 84
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 124
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 164
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 208
      Top = 0
      Width = 750
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 476
        Height = 32
        Caption = 'Gerenciamento de Consumo por leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 476
        Height = 32
        Caption = 'Gerenciamento de Consumo por leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 476
        Height = 32
        Caption = 'Gerenciamento de Consumo por leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1006
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1002
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1006
    Height = 513
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1006
      Height = 70
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 78
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = dmkDBEdit1
      end
      object Label8: TLabel
        Left = 810
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Total:'
        FocusControl = DBEdit1
      end
      object Label10: TLabel
        Left = 905
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Pagtos.:'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsLeiGer
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 78
        Top = 32
        Width = 450
        Height = 21
        TabStop = False
        DataField = 'NOMEEMP'
        DataSource = DsLeiGer
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object GroupBox1: TGroupBox
        Left = 535
        Top = 16
        Width = 270
        Height = 45
        Caption = 'Per'#237'odo'
        TabOrder = 2
        object Label4: TLabel
          Left = 8
          Top = 19
          Width = 52
          Height = 13
          Caption = 'M'#234's / ano:'
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 65
          Top = 16
          Width = 195
          Height = 21
          TabStop = False
          DataField = 'PERIODO_TXT'
          DataSource = DsLeiGer
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object DBEdit1: TDBEdit
        Left = 810
        Top = 32
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Valor'
        DataSource = DsTotal
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 905
        Top = 32
        Width = 90
        Height = 21
        BiDiMode = bdRightToLeft
        DataField = 'Debito'
        DataSource = DsTotPagtos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 4
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 449
      Width = 1006
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 309
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 483
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtItens: TBitBtn
          Tag = 10015
          Left = 125
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Leitura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtItensClick
        end
        object BtCons: TBitBtn
          Tag = 191
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Per'#237'odo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConsClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtPagto: TBitBtn
          Tag = 187
          Left = 245
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pagto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtPagtoClick
        end
      end
    end
    object Panel15: TPanel
      Left = 0
      Top = 70
      Width = 209
      Height = 379
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Panel15'
      TabOrder = 2
      object DBGCons: TDBGrid
        Left = 0
        Top = 0
        Width = 209
        Height = 269
        Align = alClient
        DataSource = DsCons
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Produto'
            Width = 150
            Visible = True
          end>
      end
      object Panel16: TPanel
        Left = 0
        Top = 269
        Width = 209
        Height = 110
        Align = alBottom
        BevelOuter = bvLowered
        ParentBackground = False
        TabOrder = 1
        object Label21: TLabel
          Left = 4
          Top = 60
          Width = 76
          Height = 13
          Caption = 'Tarifa m'#237'nima $:'
          FocusControl = DBEdit15
        end
        object Label2: TLabel
          Left = 4
          Top = 36
          Width = 56
          Height = 13
          Caption = 'Consumo $:'
          FocusControl = DBEdit15
        end
        object Label5: TLabel
          Left = 4
          Top = 12
          Width = 47
          Height = 13
          Caption = 'Consumo:'
          FocusControl = DBEdit15
        end
        object Label6: TLabel
          Left = 4
          Top = 84
          Width = 47
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'TOTAL $:'
          ParentBiDiMode = False
        end
        object DBEdit13: TDBEdit
          Left = 92
          Top = 9
          Width = 113
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Medida'
          DataSource = DsConsTot
          ParentBiDiMode = False
          TabOrder = 0
        end
        object DBEdit14: TDBEdit
          Left = 92
          Top = 33
          Width = 113
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Valor'
          DataSource = DsConsTot
          ParentBiDiMode = False
          TabOrder = 1
        end
        object DBEdit15: TDBEdit
          Left = 92
          Top = 57
          Width = 113
          Height = 21
          BiDiMode = bdRightToLeft
          DataField = 'Valor'
          DataSource = DsTarifaMin
          ParentBiDiMode = False
          TabOrder = 2
        end
        object EdTotalCons: TdmkEdit
          Left = 92
          Top = 81
          Width = 113
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object Panel6: TPanel
      Left = 209
      Top = 70
      Width = 797
      Height = 379
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 3
      object DBGCNS: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 797
        Height = 269
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidLei'
            Title.Caption = 'Un. leit.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedAnt'
            Title.Caption = 'Medida anterior'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedAtu'
            Title.Caption = 'Medida atual'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Medida'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Carencia'
            Title.Caption = 'Car'#234'ncia'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DifCaren'
            Title.Caption = 'dc'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONSUMO1_TXT'
            Title.Caption = 'Consumo 1'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONSUMO2_TXT'
            Title.Caption = 'Consumo 1'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidFat'
            Title.Caption = 'Fator'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidImp'
            Title.Caption = 'Un. Impr.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Casas'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCNS
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FieldsCalcToOrder.Strings = (
          'CONSUMO1_TXT=Consumo'
          'CONSUMO2_TXT=Consumo')
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidLei'
            Title.Caption = 'Un. leit.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedAnt'
            Title.Caption = 'Medida anterior'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedAtu'
            Title.Caption = 'Medida atual'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Medida'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Carencia'
            Title.Caption = 'Car'#234'ncia'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DifCaren'
            Title.Caption = 'dc'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONSUMO1_TXT'
            Title.Caption = 'Consumo 1'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONSUMO2_TXT'
            Title.Caption = 'Consumo 1'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidFat'
            Title.Caption = 'Fator'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnidImp'
            Title.Caption = 'Un. Impr.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Casas'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end>
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 269
        Width = 797
        Height = 110
        Align = alBottom
        Caption = 'Pagamentos'
        TabOrder = 1
        object DBGrid4: TDBGrid
          Left = 2
          Top = 15
          Width = 793
          Height = 93
          Align = alClient
          DataSource = DsPagtos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'N'#186
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 101
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECE'
              Title.Caption = 'Fornecedor'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Visible = True
            end>
        end
      end
    end
  end
  object QrLeiGer: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrLeiGerBeforeOpen
    AfterOpen = QrLeiGerAfterOpen
    BeforeClose = QrLeiGerBeforeClose
    AfterScroll = QrLeiGerAfterScroll
    OnCalcFields = QrLeiGerCalcFields
    SortFieldNames = 'Periodo'
    SQL.Strings = (
      'SELECT *'
      'FROM leiger')
    Left = 536
    Top = 24
    object QrLeiGerCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLeiGerPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrLeiGerEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLeiGerNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrLeiGerPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 100
      Calculated = True
    end
    object QrLeiGerCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsLeiGer: TDataSource
    DataSet = QrLeiGer
    Left = 564
    Top = 24
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCons
    CanUpd01 = BtItens
    Left = 592
    Top = 24
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 736
    Top = 72
  end
  object PMCons: TPopupMenu
    OnPopup = PMConsPopup
    Left = 512
    Top = 520
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 640
    Top = 512
    object Incluileitura1: TMenuItem
      Caption = '&Inclui leitura'
      OnClick = Incluileitura1Click
    end
    object Excluileituraatual1: TMenuItem
      Caption = '&Exclui leitura atual'
      OnClick = Excluileituraatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object arifamnima1: TMenuItem
      Caption = '&Tarifa m'#237'nima'
      object Inclui2: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui2Click
      end
      object Exclui2: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui2Click
      end
    end
  end
  object QrCons: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrConsBeforeClose
    AfterScroll = QrConsAfterScroll
    Left = 340
    Top = 252
    object QrConsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrConsGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsCons: TDataSource
    DataSet = QrCons
    Left = 368
    Top = 252
  end
  object QrCNS: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNSCalcFields
    Left = 396
    Top = 252
    object QrCNSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNSControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCNSMedAnt: TFloatField
      FieldName = 'MedAnt'
    end
    object QrCNSMedAtu: TFloatField
      FieldName = 'MedAtu'
    end
    object QrCNSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNSPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrCNSConsumo: TFloatField
      FieldName = 'Consumo'
    end
    object QrCNSValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNSPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCNSCasas: TSmallintField
      FieldName = 'Casas'
    end
    object QrCNSUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrCNSUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 5
    end
    object QrCNSUnidFat: TFloatField
      FieldName = 'UnidFat'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCNSCONSUMO1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO1_TXT'
      Size = 30
      Calculated = True
    end
    object QrCNSCONSUMO2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2_TXT'
      Size = 30
      Calculated = True
    end
    object QrCNSCONSUMO2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2'
      Calculated = True
    end
    object QrCNSCasRat: TSmallintField
      FieldName = 'CasRat'
    end
    object QrCNSDifCaren: TSmallintField
      FieldName = 'DifCaren'
    end
    object QrCNSCarencia: TFloatField
      FieldName = 'Carencia'
    end
    object QrCNSData: TDateField
      FieldName = 'Data'
    end
    object QrCNSMedida: TFloatField
      FieldName = 'Medida'
    end
  end
  object DsCNS: TDataSource
    DataSet = QrCNS
    Left = 425
    Top = 252
  end
  object QrTarifaMin: TMySQLQuery
    Database = Dmod.MyDB
    Left = 454
    Top = 252
    object QrTarifaMinControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTarifaMinValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsTarifaMin: TDataSource
    DataSet = QrTarifaMin
    Left = 482
    Top = 252
  end
  object QrConsTot: TMySQLQuery
    Database = Dmod.MyDB
    Left = 340
    Top = 300
    object QrConsTotMedida: TFloatField
      FieldName = 'Medida'
    end
    object QrConsTotValor: TFloatField
      FieldName = 'Valor'
    end
    object QrConsTotCasas: TIntegerField
      FieldName = 'Casas'
    end
  end
  object DsConsTot: TDataSource
    DataSet = QrConsTot
    Left = 368
    Top = 300
  end
  object QrTotal: TMySQLQuery
    Database = Dmod.MyDB
    Left = 340
    Top = 348
    object QrTotalValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsTotal: TDataSource
    DataSet = QrTotal
    Left = 368
    Top = 348
  end
  object PMPagto: TPopupMenu
    OnPopup = PMPagtoPopup
    Left = 768
    Top = 512
    object MenuItem1: TMenuItem
      Caption = '&Inclui'
      object Total1: TMenuItem
        Caption = 'Valor &Total'
        OnClick = Total1Click
      end
      object ValordoProduto1: TMenuItem
        Caption = 'Valor do &Produto selecionado'
        OnClick = ValordoProduto1Click
      end
    end
    object MenuItem2: TMenuItem
      Caption = '&Exclui'
      OnClick = MenuItem2Click
    end
  end
  object QrPagtos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 510
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPagtosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPagtosNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPagtosBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPagtosAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPagtosConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPagtosTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrPagtosCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPagtosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPagtosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagtosNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrPagtosCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPagtosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagtosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPagtosSub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 538
    Top = 252
  end
  object QrTotPagtos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 510
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTotPagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsTotPagtos: TDataSource
    DataSet = QrTotPagtos
    Left = 538
    Top = 300
  end
end
