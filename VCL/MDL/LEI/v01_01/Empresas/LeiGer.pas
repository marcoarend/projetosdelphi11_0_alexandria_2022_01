unit LeiGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, Variants,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditCB, dmkDBLookupComboBox,
  dmkValUsu, dmkEditDateTimePicker, Vcl.Menus, dmkDBGrid, Vcl.Grids,
  Vcl.DBGrids, UnGrl_Geral;

type
  TFmLeiGer = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtItens: TBitBtn;
    BtCons: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrLeiGer: TmySQLQuery;
    QrLeiGerCodigo: TIntegerField;
    DsLeiGer: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBPeriodo: TGroupBox;
    CBAno: TComboBox;
    CBMes: TComboBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    QrLeiGerPeriodo: TIntegerField;
    QrLeiGerEmpresa: TIntegerField;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    QrLeiGerNOMEEMP: TWideStringField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    QrLeiGerPERIODO_TXT: TWideStringField;
    dmkDBEdit2: TdmkDBEdit;
    QrLoc: TmySQLQuery;
    PMCons: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMItens: TPopupMenu;
    Incluileitura1: TMenuItem;
    Excluileituraatual1: TMenuItem;
    QrCons: TmySQLQuery;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    DsCons: TDataSource;
    QrCNS: TmySQLQuery;
    QrCNSCodigo: TIntegerField;
    QrCNSControle: TIntegerField;
    QrCNSMedAnt: TFloatField;
    QrCNSMedAtu: TFloatField;
    QrCNSLk: TIntegerField;
    QrCNSDataCad: TDateField;
    QrCNSDataAlt: TDateField;
    QrCNSUserCad: TIntegerField;
    QrCNSUserAlt: TIntegerField;
    QrCNSPeriodo: TIntegerField;
    QrCNSConsumo: TFloatField;
    QrCNSValor: TFloatField;
    QrCNSPreco: TFloatField;
    QrCNSCasas: TSmallintField;
    QrCNSUnidLei: TWideStringField;
    QrCNSUnidImp: TWideStringField;
    QrCNSUnidFat: TFloatField;
    QrCNSCONSUMO1_TXT: TWideStringField;
    QrCNSCONSUMO2_TXT: TWideStringField;
    QrCNSCONSUMO2: TFloatField;
    QrCNSCasRat: TSmallintField;
    QrCNSDifCaren: TSmallintField;
    QrCNSCarencia: TFloatField;
    DsCNS: TDataSource;
    Panel15: TPanel;
    DBGCons: TDBGrid;
    Panel16: TPanel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    QrCNSData: TDateField;
    QrTarifaMin: TmySQLQuery;
    DsTarifaMin: TDataSource;
    QrTarifaMinControle: TIntegerField;
    QrTarifaMinValor: TFloatField;
    N1: TMenuItem;
    arifamnima1: TMenuItem;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdTotalCons: TdmkEdit;
    QrConsTot: TmySQLQuery;
    DsConsTot: TDataSource;
    QrConsTotMedida: TFloatField;
    QrConsTotValor: TFloatField;
    QrConsTotCasas: TIntegerField;
    QrTotal: TmySQLQuery;
    DsTotal: TDataSource;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    QrTotalValor: TFloatField;
    BtPagto: TBitBtn;
    PMPagto: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    QrLeiGerCliInt: TIntegerField;
    QrConsGenero: TIntegerField;
    Panel6: TPanel;
    DBGCNS: TdmkDBGrid;
    GroupBox2: TGroupBox;
    DBGrid4: TDBGrid;
    QrPagtos: TmySQLQuery;
    QrPagtosData: TDateField;
    QrPagtosVencimento: TDateField;
    QrPagtosBanco: TIntegerField;
    QrPagtosSEQ: TIntegerField;
    QrPagtosFatID: TIntegerField;
    QrPagtosContaCorrente: TWideStringField;
    QrPagtosDocumento: TFloatField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosFatNum: TFloatField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    QrPagtosNOMECARTEIRA2: TWideStringField;
    QrPagtosBanco1: TIntegerField;
    QrPagtosAgencia1: TIntegerField;
    QrPagtosConta1: TWideStringField;
    QrPagtosTipoDoc: TSmallintField;
    QrPagtosControle: TIntegerField;
    QrPagtosNOMEFORNECEI: TWideStringField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    QrPagtosDebito: TFloatField;
    QrPagtosAgencia: TIntegerField;
    QrPagtosFornecedor: TIntegerField;
    QrPagtosNOMEFORNECE: TWideStringField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosGenero: TIntegerField;
    DsPagtos: TDataSource;
    Total1: TMenuItem;
    ValordoProduto1: TMenuItem;
    QrTotPagtos: TmySQLQuery;
    QrTotPagtosDebito: TFloatField;
    QrPagtosTipo: TSmallintField;
    QrPagtosSub: TSmallintField;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    DsTotPagtos: TDataSource;
    QrCNSMedida: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrLeiGerAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLeiGerBeforeOpen(DataSet: TDataSet);
    procedure BtConsClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrLeiGerCalcFields(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure PMConsPopup(Sender: TObject);
    procedure Incluileitura1Click(Sender: TObject);
    procedure QrLeiGerAfterScroll(DataSet: TDataSet);
    procedure QrLeiGerBeforeClose(DataSet: TDataSet);
    procedure QrConsBeforeClose(DataSet: TDataSet);
    procedure QrConsAfterScroll(DataSet: TDataSet);
    procedure PMItensPopup(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure Excluileituraatual1Click(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure QrCNSCalcFields(DataSet: TDataSet);
    procedure MenuItem2Click(Sender: TObject);
    procedure BtPagtoClick(Sender: TObject);
    procedure PMPagtoPopup(Sender: TObject);
    procedure ValordoProduto1Click(Sender: TObject);
    procedure Total1Click(Sender: TObject);
  private
    FThisFatID: Integer;
    FTabLctA: String;
    function  PeriodoExiste(Empresa, Periodo, Codigo: Integer): Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure ReopenCons(Periodo, CliInt, Codigo: Integer);
    procedure ReopenSubTotal(Periodo, CliInt, Codigo: Integer);
    procedure ReopenCNS(CliInt, Periodo, Codigo, Controle: Integer);
    procedure ReopenTarifaMin(CliInt, Periodo, Codigo: Integer);
    procedure ReopenPagtos();
    procedure IncluiPagto(Total: Boolean);
  public
    { Public declarations }
  end;

var
  FmLeiGer: TFmLeiGer;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UnConsumoGerlJan, MyDBCheck,
{$IfNDef NO_FINANCEIRO} UnFinanceiro, ModuleFin, {$EndIf} UnPagtos,
UnInternalConsts3, UnConsumoJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmLeiGer.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmLeiGer.MenuItem2Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM leiger ',
    'WHERE Periodo>' + Geral.FF0(QrLeiGerPeriodo.Value),
    'AND Empresa=' + Geral.FF0(QrLeiGerEmpresa.Value),
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak + 'Motivo: Existe per�odo posterior!');
    Exit;
  end;
  //
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID, QrLeiGerCodigo.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  UFinanceiro.ExcluiLct_Unico(FTabLctA, Dmod.MyDB, QrPagtosData.Value,
    QrPagtosTipo.Value, QrPagtosCarteira.Value, QrPagtosControle.Value,
    QrPagtosSub.Value, dmkPF.MotivDel_ValidaCodigo(311), True, False);
  ReopenPagtos;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmLeiGer.MostraEdicao(Mostra: Integer; SQLType: TSQLType;
  Codigo: Integer);
var
  Ano, Mes: Word;
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      //
      if SQLType = stIns then
      begin
        GBPeriodo.Enabled := True;
        CBMes.Enabled     := True;
        CBAno.Enabled     := True;
        //
        EdCodigo.ValueVariant  := FormatFloat(FFormatFloat, Codigo);
        EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
        CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
        //
        MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, 0);
        //
        CBMes.SetFocus;
      end else
      begin
        GBPeriodo.Enabled := False;
        CBMes.Enabled     := False;
        CBAno.Enabled     := False;
        //
        EdCodigo.ValueVariant  := QrLeiGerCodigo.Value;
        EdEmpresa.ValueVariant := QrLeiGerEmpresa.Value;
        CBEmpresa.KeyValue     := QrLeiGerEmpresa.Value;
        //
        dmkPF.PeriodoDecode(QrLeiGerPeriodo.Value, Ano, Mes);
        //
        CBMes.ItemIndex := Mes;
        CBAno.ItemIndex := Ano;
        //
        EdEmpresa.SetFocus;
      end;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmLeiGer.Total1Click(Sender: TObject);
begin
  IncluiPagto(True);
end;

function TFmLeiGer.PeriodoExiste(Empresa, Periodo, Codigo: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM leiger ',
    'WHERE Periodo=' + Geral.FF0(Periodo),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    if Codigo <> QrLoc.FieldByName('Codigo').AsInteger then
      Result := False
    else
      Result := True;
  end else
    Result := True;
end;

procedure TFmLeiGer.PMConsPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrLeiGer.State <> dsInactive) and (QrLeiGer.RecordCount > 0);
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab;
end;

procedure TFmLeiGer.PMItensPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrLeiGer.State <> dsInactive) and (QrLeiGer.RecordCount > 0);
  Enab2 := (QrCNS.State <> dsInactive) and (QrCNS.RecordCount > 0);
  Enab3 := (QrTarifaMin.State <> dsInactive) and (QrTarifaMin.RecordCount > 0);
  //
  Inclui2.Enabled             := Enab;
  Incluileitura1.Enabled      := Enab;
  Excluileituraatual1.Enabled := Enab and Enab2;
  Exclui2.Enabled             := Enab and Enab3;
end;

procedure TFmLeiGer.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrCons.State <> dsInactive) and (QrCons.RecordCount > 0);
  Enab2 := (QrPagtos.State <> dsInactive) and (QrPagtos.RecordCount > 0);
  Enab3 := (QrTotPagtosDebito.Value < QrTotalValor.Value);
  //
  Total1.Enabled          := Enab and not Enab2;
  ValordoProduto1.Enabled := Enab and Enab3;
  MenuItem2.Enabled       := Enab and Enab2;
end;

procedure TFmLeiGer.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrLeiGerCodigo.Value, LaRegistro.Caption[2]);
end;
procedure TFmLeiGer.ValordoProduto1Click(Sender: TObject);
begin
  IncluiPagto(False);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmLeiGer.DefParams;
begin
  VAR_GOTOTABELA := 'leiger';
  VAR_GOTOMYSQLTABLE := QrLeiGer;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT cns.*, eci.CodEnti CliInt, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEEMP ');
  VAR_SQLx.Add('FROM leiger cns ');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodCliInt=cns.Empresa ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti ');
  VAR_SQLx.Add('WHERE cns.Codigo <> 0 ');
  //
  VAR_SQL1.Add('AND cns.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cns.CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmLeiGer.Exclui1Click(Sender: TObject);
var
  Periodo, CliInt, Codigo: Integer;
begin
  Periodo := QrLeiGerPeriodo.Value;
  CliInt  := QrLeiGerEmpresa.Value;
  Codigo  := QrLeiGerCodigo.Value;
  //
  ReopenCons(Periodo, CliInt, Codigo);
  ReopenTarifaMin(Periodo, CliInt, Codigo);
  //
  if (QrCons.RecordCount > 0) or (QrTarifaMin.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Voc� deve excluir todas as leituras antes de remover o per�odo!');
    Exit;
  end;
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do per�odo atual?',
    'leiger', 'Codigo', Codigo, Dmod.MyDB);
  UnDmkDAC_PF.AbreQueryApenas(QrLeiGer);
  Va(vpLast);
end;

procedure TFmLeiGer.Exclui2Click(Sender: TObject);
var
  Codigo, Controle, Periodo, Empresa: Integer;
begin
  Controle := QrTarifaMinControle.Value;
  Codigo   := QrLeiGerCodigo.Value;
  Periodo  := QrLeiGerPeriodo.Value;
  Empresa  := QrLeiGerEmpresa.Value;
  //
  //Tarifa m�nima
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM leigerits ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND Codigo=' + Geral.FF0(QrConsCodigo.Value),
    'AND MedAnt=1 ',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    if Controle <> QrLoc.FieldByName('Controle').AsInteger then
    begin
      Geral.MB_Aviso('Apenas a �ltima leitura de cada empresa pode ser exclu�da!');
      Exit;
    end;
  end;
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do registro selecionado?',
    'leigerits', 'Controle', Controle, Dmod.MyDB);
  ReopenTarifaMin(Empresa, Periodo, QrConsCodigo.Value);
end;

procedure TFmLeiGer.Excluileituraatual1Click(Sender: TObject);
var
  Codigo, Controle, Periodo, Empresa: Integer;
begin
  Controle := QrCNSControle.Value;
  Codigo   := QrLeiGerCodigo.Value;
  Periodo  := QrLeiGerPeriodo.Value;
  Empresa  := QrLeiGerEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT MAX(Controle) Controle ',
    'FROM leigerits ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND Codigo=' + Geral.FF0(QrConsCodigo.Value),
    'AND Tipo=0 ',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    if Controle <> QrLoc.FieldByName('Controle').AsInteger then
    begin
      Geral.MB_Aviso('Apenas a �ltima leitura de cada empresa pode ser exclu�da!');
      Exit;
    end;
  end;
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do registro selecionado?',
    'leigerits', 'Controle', Controle, Dmod.MyDB);
  ReopenCNS(Empresa, Periodo, QrConsCodigo.Value, 0);
  QrCNS.Last;
end;

procedure TFmLeiGer.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmLeiGer.QueryPrincipalAfterOpen;
begin
end;

procedure TFmLeiGer.ReopenCNS(CliInt, Periodo, Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNS, Dmod.MyDB, [
    'SELECT cnp.Casas, cnp.UnidLei, MedAtu - MedAnt Medida, ',
    'cnp.UnidImp, cnp.UnidFat, cnp.CasRat, cni.* ',
    'FROM cons cns ',
    'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
    'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
    'WHERE cni.Empresa=' + Geral.FF0(CliInt),
    'AND cnp.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Codigo=' + Geral.FF0(Codigo),
    'AND cni.Tipo=0 ',
    'ORDER BY cni.DataCad, cni.MedAtu ASC ',
    '']);
  if Controle <> 0 then
    QrCNS.Locate('Controle', Controle, []);
end;

procedure TFmLeiGer.ReopenSubTotal(Periodo, CliInt, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrConsTot, Dmod.MyDB, [
    'SELECT cnp.Casas, MAX(MedAtu) - MIN(MedAnt) Medida, SUM(Valor) Valor ',
    'FROM consprc cnp ',
    'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
    'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
    'WHERE cni.Empresa=' + Geral.FF0(CliInt),
    'AND cnp.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Tipo=0 ',
    'AND cni.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY cni.Codigo ',
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotal, Dmod.MyDB, [
    'SELECT SUM(Valor) Valor ',
    'FROM consprc cnp ',
    'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
    'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
    'WHERE cni.Empresa=' + Geral.FF0(CliInt),
    'AND cnp.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    '']);
end;

procedure TFmLeiGer.ReopenCons(Periodo, CliInt, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCons, Dmod.MyDB, [
    'SELECT cns.Codigo, cns.Nome, cns.Genero ',
    'FROM consprc cnp ',
    'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
    'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
    'WHERE cni.Empresa=' + Geral.FF0(CliInt),
    'AND cnp.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'GROUP BY cni.Codigo ',
    'ORDER BY cns.Nome ',
    '']);
  if Codigo <> 0 then
    QrCons.Locate('Codigo', Codigo, []);
end;

procedure TFmLeiGer.ReopenPagtos;
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
    'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
    'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, la.Tipo, la.Sub, ',
    'la.FatParcela, la.FatNum, la.Fornecedor, la.Carteira, la.Genero, ',
    'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
    'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
    'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
    'ELSE fo.Nome END NOMEFORNECEI,',
    'ca.Tipo CARTEIRATIPO, ',
    'IF(fr.Tipo=0, fr.RazaoSocial, fr.Nome) NOMEFORNECE ',
    'FROM ' + FTabLctA + ' la',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
    'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
    'LEFT JOIN entidades fr ON fr.Codigo=la.Fornecedor',
    'WHERE FatNum=' + Geral.FF0(QrLeiGerCodigo.Value),
    'AND FatID=' + Geral.FF0(FThisFatID),
    'ORDER BY la.FatParcela, la.Vencimento',
    '']);
  //Total
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotPagtos, Dmod.MyDB, [
    'SELECT SUM(la.Debito) Debito ',
    'FROM ' + FTabLctA + ' la',
    'WHERE FatNum=' + Geral.FF0(QrLeiGerCodigo.Value),
    'AND FatID=' + Geral.FF0(FThisFatID),
    '']);
{$EndIf}
end;

procedure TFmLeiGer.ReopenTarifaMin(CliInt, Periodo, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTarifaMin, Dmod.MyDB, [
    'SELECT Controle, Valor ',
    'FROM leigerits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Periodo=' + Geral.FF0(Periodo),
    'AND Empresa=' + Geral.FF0(CliInt),
    'AND Tipo=1 ',
    '']);
end;

procedure TFmLeiGer.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmLeiGer.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmLeiGer.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmLeiGer.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmLeiGer.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmLeiGer.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLeiGer.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrLeiGerCodigo.Value;
  Close;
end;

procedure TFmLeiGer.Altera1Click(Sender: TObject);
begin
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmLeiGer.BtConfirmaClick(Sender: TObject);
var
  Codigo, Empresa, Periodo: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  Periodo := dmkPF.PeriodoEncode(Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  if MyObjects.FIC(Periodo = 0, CBMes, 'Defina o per�odo!') then Exit;
  //
  Codigo  := UMyMod.BPGS1I32('leiger', 'Codigo', '', '', tsPos, ImgTipo.SQLType, QrLeiGerCodigo.Value);
  //
  if MyObjects.FIC(PeriodoExiste(Empresa, Periodo, Codigo) = False, CBMes, 'Per�odo j� cadastrado!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'leiger', False,
    ['Empresa', 'Periodo'], ['Codigo'], [Empresa, Periodo], [Codigo], True) then
  begin
    LocCod(Codigo, Codigo);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmLeiGer.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cons', 'Codigo');
    GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmLeiGer.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmLeiGer.BtPagtoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagto);
end;

procedure TFmLeiGer.BtConsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCons, BtCons);
end;

procedure TFmLeiGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  FThisFatID      := VAR_FATID_0071;
  //
  CriaOForm;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
end;

procedure TFmLeiGer.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrLeiGerCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLeiGer.SbImprimeClick(Sender: TObject);
begin
  ConsumoJan.MostraLeiGerImp(QrLeiGerPeriodo.Value);
end;

procedure TFmLeiGer.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmLeiGer.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrConsCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmLeiGer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmLeiGer.QrCNSCalcFields(DataSet: TDataSet);
begin
  QrCNSCONSUMO2.Value     := QrCNSConsumo.Value * QrCNSUnidFat.Value;
  QrCNSConsumo2_TXT.Value := Geral.FFT(QrCNSCONSUMO2.Value,
                               QrCNSCasas.Value, siNegativo) + ' ' +
                               QrCNSUnidImp.Value;
  QrCNSConsumo1_TXT.Value := Geral.FFT(QrCNSConsumo.Value,
                               QrCNSCasas.Value, siNegativo) + ' ' +
                               QrCNSUnidLei.Value;
end;

procedure TFmLeiGer.QrConsAfterScroll(DataSet: TDataSet);
var
  Casas: Integer;
  FmtTxTVal, FmtTxT: String;
begin
  ReopenCNS(QrLeiGerEmpresa.Value, QrLeiGerPeriodo.Value, QrConsCodigo.Value, 0);
  ReopenTarifaMin(QrLeiGerEmpresa.Value, QrLeiGerPeriodo.Value, QrConsCodigo.Value);
  ReopenSubTotal(QrLeiGerPeriodo.Value, QrLeiGerEmpresa.Value, QrConsCodigo.Value);
  //
  Casas     := QrConsTotCasas.Value;
  FmtTxT    := dmkPF.FormataCasas(Casas);
  FmtTxTVal := dmkPF.FormataCasas(2);
  //
  QrConsTotValor.DisplayFormat   := FmtTxTVal;
  QrConsTotMedida.DisplayFormat  := FmtTxt;
  QrTarifaMinValor.DisplayFormat := FmtTxTVal;
  QrTotalValor.DisplayFormat     := FmtTxTVal;
  //
  QrCNSMedAnt.DisplayFormat := FmtTxt;
  QrCNSMedAtu.DisplayFormat := FmtTxt;
  QrCNSMedida.DisplayFormat := FmtTxt;
  QrCNSPreco.DisplayFormat  := FmtTxTVal;
  QrCNSValor.DisplayFormat  := FmtTxTVal;
  //
  EdTotalCons.DecimalSize  := 2;
  EdTotalCons.ValueVariant := QrConsTotValor.Value + QrTarifaMinValor.Value;
end;

procedure TFmLeiGer.QrConsBeforeClose(DataSet: TDataSet);
begin
  EdTotalCons.ValueVariant := 0;
  //
  QrCNS.Close;
  QrTarifaMin.Close;
  QrConsTot.Close;
  QrTotal.Close;
end;

procedure TFmLeiGer.QrLeiGerAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmLeiGer.QrLeiGerAfterScroll(DataSet: TDataSet);
var
  FmtTxTVal: String;
begin
  if QrLeiGerCliInt.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrLeiGerEmpresa.Value)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenCons(QrLeiGerPeriodo.Value, QrLeiGerEmpresa.Value, 0);
  ReopenPagtos();
  //
  FmtTxTVal := dmkPF.FormataCasas(2);
  //
  QrTotPagtosDebito.DisplayFormat := FmtTxTVal;
end;

procedure TFmLeiGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLeiGer.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrConsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cons', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmLeiGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLeiGer.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmLeiGer.Inclui2Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de consumo';
  Prompt = 'Informe o item de consumo: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Codigo: Variant;
  Empresa, Periodo, Controle: Integer;
  Preco: Double;
  PrecoTXT, PesqSQL, Data: String;
begin
  Empresa := QrLeiGerEmpresa.Value;
  Periodo := QrLeiGerPeriodo.Value;
  //
  PesqSQL := Geral.ATS([
    'SELECT Codigo, Nome ' + Campo,
    'FROM cons ',
    'WHERE Codigo NOT IN ',
    '( ',
    'SELECT Codigo ',
    'FROM leigerits ',
    'WHERE Tipo=1 ',
    'AND Periodo=' + Geral.FF0(Periodo),
    'AND Empresa=' + Geral.FF0(Empresa),
    ') ',
    'AND Ativo=1 ',
    'ORDER BY ' + Campo,
    '']);
  //
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo,
              0, [PesqSQL], Dmod.MyDB, True);
  //
  if (Codigo <> Null) and (Codigo <> 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT cnp.* ',
      'FROM consprc cnp ',
      'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
      'WHERE cnp.Cond=' + Geral.FF0(Empresa),
      'AND cns.Codigo=' + Geral.FF0(Codigo),
      '']);
    //
    PrecoTXT := Geral.FFT(QrLoc.FieldByName('TarifaMin').AsFloat,
                  QrLoc.FieldByName('Casas').AsInteger, siPositivo);
    //
    if InputQuery('Tarifa m�nima', 'Pre�o:', PrecoTXT) then
    begin
      Preco := Geral.DMV(PrecoTXT);
      Data  := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(QrLeiGerPeriodo.Value), 01);
      //
      if Preco <> 0 then
      begin
        Controle := UMyMod.BPGS1I32('leigerits', 'Controle', '', '', tsPos, stIns, 0);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'leigerits', False,
          ['MedAnt', 'MedAtu', 'Valor', 'Empresa', 'Periodo', 'Data', 'Tipo', 'Codigo'], ['Controle'],
          [0, 0, Preco, Empresa, Periodo, Data, 1, Codigo], [Controle], True);
        //
        ReopenCons(Periodo, Empresa, Codigo);
        ReopenCNS(Empresa, Periodo, Codigo, Controle);
      end;
    end;
  end;
end;

procedure TFmLeiGer.Incluileitura1Click(Sender: TObject);
var
  Codigo, CodNovo: Integer;
begin
  Codigo  := QrConsCodigo.Value;
  CodNovo := ConsumoGerlJan.MostraCondGerLeiEdit(nil, nil, QrLeiGerEmpresa.Value,
               QrLeiGerPeriodo.Value, 0, 0, QrLeiGerPeriodo_TXT.Value, QrLeiGerNOMEEMP.Value);
  //
  if CodNovo <> 0 then
    Codigo := CodNovo;
  //
  ReopenCons(QrLeiGerPeriodo.Value, QrLeiGerEmpresa.Value, Codigo);
end;

procedure TFmLeiGer.IncluiPagto(Total: Boolean);
{$IfNDef NO_FINANCEIRO}
var
  Codigo, Empresa, Genero: Integer;
  Descricao: String;
  Valor: Double;
{$EndIf}
begin
{$IfNDef NO_FINANCEIRO}
  Codigo        := QrLeiGerCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Descricao     := 'Fatura consumo por leitura ID: ' + Geral.FF0(Codigo);
  Empresa       := QrLeiGerCliInt.Value;
  Genero        := QrConsGenero.Value;
  //
  if not Total then
    Valor := EdTotalCons.ValueVariant
  else
    Valor := QrTotalValor.Value;
  //
  UPagtos.Pagto(QrPagtos, tpDeb, Codigo, 0, FThisFatID, Genero, 0(*GenCtb*), stIns,
    'Pagto. Conta Telef�nica', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0,
    True, True, 0, 0, 0, 0, 0, FTabLctA, Descricao);
  //
  ReopenPagtos;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmLeiGer.QrLeiGerBeforeClose(DataSet: TDataSet);
begin
  QrCons.Close;
  QrPagtos.Close;
  QrTotPagtos.Close;
end;

procedure TFmLeiGer.QrLeiGerBeforeOpen(DataSet: TDataSet);
begin
  QrLeiGerCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmLeiGer.QrLeiGerCalcFields(DataSet: TDataSet);
begin
  QrLeiGerPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrLeiGerPeriodo.Value);
end;

end.

