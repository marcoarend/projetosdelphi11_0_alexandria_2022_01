unit UnConsumoJan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, DBGrids,
  UnInternalConsts2, ComCtrls, dmkEdit, dmkDBLookupComboBox, dmkGeral,
  dmkEditCB, mySQLDBTables, UnDmkEnums, DmkDAC_PF;

type
  TUnConsumoJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraLeiGer();
    procedure MostraLeiGerImp(Periodo: Integer);
  end;

var
  ConsumoJan: TUnConsumoJan;

implementation

uses MyDBCheck, LeiGer, LeiGerImp;

procedure TUnConsumoJan.MostraLeiGer();
begin
  if DBCheck.CriaFm(TFmLeiGer, FmLeiGer, afmoNegarComAviso) then
  begin
    FmLeiGer.ShowModal;
    FmLeiGer.Destroy;
  end;
end;

procedure TUnConsumoJan.MostraLeiGerImp(Periodo: Integer);
begin
  if DBCheck.CriaFm(TFmLeiGerImp, FmLeiGerImp, afmoNegarComAviso) then
  begin
    FmLeiGerImp.FPeriodo := Periodo;
    FmLeiGerImp.ShowModal;
    FmLeiGerImp.Destroy;
  end;
end;

end.
