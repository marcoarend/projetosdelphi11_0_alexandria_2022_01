object FmLeiGerEdit: TFmLeiGerEdit
  Left = 339
  Top = 185
  Caption = 'LEI-GEREN-002 :: Edi'#231#227'o de Leitura'
  ClientHeight = 374
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LaNomeCliInt: TLabel
    Left = 0
    Top = 40
    Width = 707
    Height = 16
    Align = alTop
    Alignment = taCenter
    Caption = 'LaNomeCliInt'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clInactiveCaption
    Font.Height = -14
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitTop = 47
    ExplicitWidth = 93
  end
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 707
    Height = 84
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 1
    ExplicitWidth = 867
    object LaUnidade: TLabel
      Left = 15
      Top = 2
      Width = 43
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Unidade:'
      FocusControl = DBEdUnidade
    end
    object Label2: TLabel
      Left = 73
      Top = 2
      Width = 79
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Medi'#231#227'o anterior'
      FocusControl = DBEdMedAnt
    end
    object Label3: TLabel
      Left = 169
      Top = 2
      Width = 70
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Medi'#231#227'o atual:'
      FocusControl = DBEdMedAtu
    end
    object Label4: TLabel
      Left = 265
      Top = 2
      Width = 47
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Consumo:'
      FocusControl = DBEdConsumo
    end
    object Label5: TLabel
      Left = 361
      Top = 2
      Width = 27
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valor:'
      FocusControl = DBEdValor
    end
    object Label6: TLabel
      Left = 15
      Top = 40
      Width = 41
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Per'#237'odo:'
      FocusControl = EdPeriodoTXT
    end
    object Label7: TLabel
      Left = 474
      Top = 2
      Width = 68
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pre'#231'o unit'#225'rio:'
      FocusControl = DBEdPreco
    end
    object DBEdUnidade: TDBEdit
      Left = 15
      Top = 17
      Width = 52
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Unidade'
      TabOrder = 0
    end
    object DBEdMedAnt: TDBEdit
      Left = 73
      Top = 17
      Width = 92
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'MedAnt'
      TabOrder = 1
    end
    object DBEdMedAtu: TDBEdit
      Left = 169
      Top = 17
      Width = 92
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'MedAtu'
      TabOrder = 2
    end
    object DBEdConsumo: TDBEdit
      Left = 265
      Top = 17
      Width = 92
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Consumo'
      TabOrder = 3
    end
    object DBEdValor: TDBEdit
      Left = 361
      Top = 17
      Width = 108
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Valor'
      TabOrder = 4
    end
    object EdPeriodoTXT: TEdit
      Left = 15
      Top = 55
      Width = 679
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ReadOnly = True
      TabOrder = 5
    end
    object DBEdPreco: TDBEdit
      Left = 474
      Top = 17
      Width = 108
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Preco'
      TabOrder = 6
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 189
    Width = 707
    Height = 79
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 226
    ExplicitWidth = 662
    ExplicitHeight = 82
    object Label9: TLabel
      Left = 15
      Top = 2
      Width = 73
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Leitura anterior:'
    end
    object Label10: TLabel
      Left = 100
      Top = 2
      Width = 61
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Leitura atual:'
    end
    object Label11: TLabel
      Left = 270
      Top = 2
      Width = 68
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pre'#231'o unit'#225'rio:'
    end
    object Label12: TLabel
      Left = 356
      Top = 2
      Width = 47
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Consumo:'
    end
    object Label13: TLabel
      Left = 441
      Top = 2
      Width = 47
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Consumo:'
    end
    object Label8: TLabel
      Left = 526
      Top = 2
      Width = 27
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valor:'
    end
    object Label15: TLabel
      Left = 410
      Top = 2
      Width = 6
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '2'
    end
    object Label16: TLabel
      Left = 185
      Top = 2
      Width = 45
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Car'#234'ncia:'
    end
    object Label14: TLabel
      Left = 341
      Top = 2
      Width = 6
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '1'
    end
    object EdLeiAnt: TdmkEdit
      Left = 15
      Top = 18
      Width = 82
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdLeiAntChange
    end
    object EdLeiAtu: TdmkEdit
      Left = 100
      Top = 18
      Width = 82
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdLeiAtuChange
    end
    object EdPreco: TdmkEdit
      Left = 270
      Top = 18
      Width = 82
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdPrecoChange
    end
    object EdConsumo: TdmkEdit
      Left = 356
      Top = 18
      Width = 82
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdValor: TdmkEdit
      Left = 526
      Top = 18
      Width = 92
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdConsum2: TdmkEdit
      Left = 441
      Top = 18
      Width = 82
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCarencia: TdmkEdit
      Left = 185
      Top = 18
      Width = 82
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdCarenciaChange
    end
    object CkContinuar: TCheckBox
      Left = 15
      Top = 40
      Width = 144
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 707
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 662
    object GB_R: TGroupBox
      Left = 667
      Top = 0
      Width = 40
      Height = 40
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 610
      object ImgTipo: TdmkImage
        Left = 4
        Top = 4
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 40
      Height = 40
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 40
      Top = 0
      Width = 627
      Height = 40
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 59
      ExplicitWidth = 544
      ExplicitHeight = 47
      object LaTitulo1A: TLabel
        Left = 9
        Top = 3
        Width = 212
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 6
        Width = 212
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 4
        Width = 212
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 268
    Width = 707
    Height = 36
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 703
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 658
      ExplicitHeight = 26
      object LaAviso1: TLabel
        Left = 20
        Top = -2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 19
        Top = -3
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 304
    Width = 707
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 351
    ExplicitWidth = 662
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 703
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 658
      object PnSaiDesis: TPanel
        Left = 526
        Top = 0
        Width = 177
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 481
        object BtSaida: TBitBtn
          Tag = 13
          Left = 18
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PnProduto: TPanel
    Left = 0
    Top = 56
    Width = 707
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 63
    ExplicitWidth = 662
    object LaCNAB_Cfg: TLabel
      Left = 15
      Top = 7
      Width = 40
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Produto:'
    end
    object SBCons: TSpeedButton
      Left = 445
      Top = 27
      Width = 21
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SBConsClick
    end
    object LaData: TLabel
      Left = 469
      Top = 7
      Width = 26
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data:'
    end
    object EdCons: TdmkEditCB
      Left = 15
      Top = 27
      Width = 50
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdConsChange
      OnExit = EdConsExit
      DBLookupComboBox = CBCons
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCons: TdmkDBLookupComboBox
      Left = 68
      Top = 27
      Width = 377
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCons
      TabOrder = 1
      dmkEditCB = EdCons
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object TPData: TdmkEditDateTimePicker
      Left = 469
      Top = 27
      Width = 112
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 39615.000000000000000000
      Time = 0.655720300899702100
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'Data'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
  end
  object QrCons: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cns.Codigo, cns.Nome'
      'FROM cons cns'
      'LEFT JOIN consprc prc ON prc.Codigo = cns.Codigo'
      'WHERE prc.Cond=:P0'
      'AND prc.Ativo = 1'
      'ORDER BY cns.Nome')
    Left = 576
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConsCasas: TSmallintField
      FieldName = 'Casas'
    end
    object QrConsUnidFat: TFloatField
      FieldName = 'UnidFat'
    end
    object QrConsPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrConsCarencia: TFloatField
      FieldName = 'Carencia'
    end
    object QrConsDifCaren: TSmallintField
      FieldName = 'DifCaren'
    end
    object QrConsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrConsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 10
    end
  end
  object DsCons: TDataSource
    DataSet = QrCons
    Left = 604
    Top = 72
  end
end
