unit UnConsumoGerlJan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, DBGrids,
  UnInternalConsts2, ComCtrls, dmkEdit, dmkDBLookupComboBox, dmkGeral,
  dmkEditCB, mySQLDBTables, UnDmkEnums, DmkDAC_PF;

type
  TUnConsumoGerlJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraCons(Codigo: Integer);
    function  MostraCondGerLeiEdit(QueryCNS: TmySQLQuery; DataSourceCNS: TDataSource;
              CliInt, Periodo, Lancto, Controle: Integer; PeriodoTXT, NomeCliInt: String;
              TabLctA: String = ''; TabCnsA: String = ''): Integer;
  end;

var
  ConsumoGerlJan: TUnConsumoGerlJan;

implementation

uses MyDBCheck, LeiGerEdit, Cons;

procedure TUnConsumoGerlJan.MostraCons(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCons, FmCons, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCons.LocCod(Codigo, Codigo);
    FmCons.ShowModal;
    FmCons.Destroy;
  end;
end;

function TUnConsumoGerlJan.MostraCondGerLeiEdit(QueryCNS: TmySQLQuery;
  DataSourceCNS: TDataSource; CliInt, Periodo, Lancto, Controle: Integer;
  PeriodoTXT, NomeCliInt: String; TabLctA: String = ''; TabCnsA: String = ''): Integer;
begin
  Result := 0;
  //
  if DBCheck.CriaFm(TFmLeiGerEdit, FmLeiGerEdit, afmoNegarComAviso) then
  begin
    FmLeiGerEdit.FControle      := Controle;
    FmLeiGerEdit.FLancto        := Lancto;
    FmLeiGerEdit.FCliInt        := CliInt;
    FmLeiGerEdit.FPeriodo       := Periodo;
    FmLeiGerEdit.FQueryCNS      := QueryCNS;
    FmLeiGerEdit.FDataSourceCNS := DataSourceCNS;
    FmLeiGerEdit.FPeriodoTXT    := PeriodoTXT;
    FmLeiGerEdit.FNomeCliInt    := NomeCliInt;
    FmLeiGerEdit.FTabLctA       := TabLctA;
    FmLeiGerEdit.FTabCnsA       := TabCnsA;
    FmLeiGerEdit.ShowModal;
    //
    Result := FmLeiGerEdit.FCodigo;
    //
    FmLeiGerEdit.Destroy;
  end;
end;

(*
procedure TUnConsumoGerlJan.MostraLeiGer();
begin
  if DBCheck.CriaFm(TFmLeiGer, FmLeiGer, afmoNegarComAviso) then
  begin
    FmLeiGer.ShowModal;
    FmLeiGer.Destroy;
  end;
end;
*)

end.
