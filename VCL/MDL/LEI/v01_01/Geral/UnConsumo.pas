unit UnConsumo;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, DBGrids,
  ComCtrls, dmkGeral, UnDmkEnums, DmkDAC_PF;

type
  TUnConsumo = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function ConfiguraTipoConsumo(): TStringList;
    function ObtemTipoConsumo(Tipo: Integer): String;
  end;

var
  UConsumo: TUnConsumo;
const
  CO_LEI_TIPO_MAX = 2;
  ListaLeiTipos_Str: array[0..CO_LEI_TIPO_MAX] of string =
  (
    'Outros',
    'G�s',
    '�gua'
  );


implementation

uses MyDBCheck;

{ TUnConsumo }

function TUnConsumo.ConfiguraTipoConsumo(): TStringList;
var
  Lista: TStringList;
  I: Integer;
begin
  Lista := TStringList.Create;
  try
    for I := Low(ListaLeiTipos_Str) to High(ListaLeiTipos_Str) do
    begin
      Lista.Add(ListaLeiTipos_Str[I]);
    end;
  finally
    Result := Lista;
  end;
end;

function TUnConsumo.ObtemTipoConsumo(Tipo: Integer): String;
begin
  Result := '';
  try
    Result := ListaLeiTipos_Str[Tipo];
  except
    Geral.MB_Erro('Tipo n�o implementado!');
  end;
end;

end.
