unit LeiGerEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Mask, DBCtrls, dmkEdit, dmkGeral,
  UMySQLModule, dmkImage, UnDmkEnums, UnInternalConsts, dmkDBLookupComboBox,
  dmkEditCB, Data.DB, mySQLDbTables, Variants, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmLeiGerEdit = class(TForm)
    PnDados: TPanel;
    LaUnidade: TLabel;
    DBEdUnidade: TDBEdit;
    Label2: TLabel;
    DBEdMedAnt: TDBEdit;
    Label3: TLabel;
    DBEdMedAtu: TDBEdit;
    Label4: TLabel;
    DBEdConsumo: TDBEdit;
    Label5: TLabel;
    DBEdValor: TDBEdit;
    Label6: TLabel;
    EdPeriodoTXT: TEdit;
    Label7: TLabel;
    DBEdPreco: TDBEdit;
    Panel3: TPanel;
    Label9: TLabel;
    EdLeiAnt: TdmkEdit;
    EdLeiAtu: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdPreco: TdmkEdit;
    Label12: TLabel;
    EdConsumo: TdmkEdit;
    Label13: TLabel;
    EdValor: TdmkEdit;
    Label8: TLabel;
    EdConsum2: TdmkEdit;
    Label15: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PnProduto: TPanel;
    LaCNAB_Cfg: TLabel;
    EdCons: TdmkEditCB;
    CBCons: TdmkDBLookupComboBox;
    SBCons: TSpeedButton;
    LaNomeCliInt: TLabel;
    QrCons: TmySQLQuery;
    DsCons: TDataSource;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    QrConsCasas: TSmallintField;
    QrConsUnidFat: TFloatField;
    QrConsPreco: TFloatField;
    Label16: TLabel;
    EdCarencia: TdmkEdit;
    Label14: TLabel;
    QrConsCarencia: TFloatField;
    QrConsDifCaren: TSmallintField;
    QrConsUnidLei: TWideStringField;
    QrConsUnidImp: TWideStringField;
    CkContinuar: TCheckBox;
    LaData: TLabel;
    TPData: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdLeiAntChange(Sender: TObject);
    procedure EdLeiAtuChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBConsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdConsChange(Sender: TObject);
    procedure EdCarenciaChange(Sender: TObject);
    procedure EdConsExit(Sender: TObject);
  private
    { Private declarations }
    FDifCaren: Integer;
    FUnidFat: Double;
    function  ObtemMedidaAnt(CliInt, Produto: Integer): Double;
    procedure CalculaConsumoECusto;
    procedure ReopenCons(CliInt: Integer);
    procedure MostraEdicao(SQLTipo: TSQLType; Produto: Integer = 0);
    procedure ConfiguraCasas(Casas: Integer);
    procedure ConfiguraConsumo();
  public
    { Public declarations }
    FCodigo, FControle, FLancto, FCliInt, FPeriodo: Integer;
    FQueryCNS: TmySQLQuery;
    FDataSourceCNS: TDataSource;
    FPeriodoTXT, FNomeCliInt, FTabLctA, FTabCnsA: String;
  end;

  var
    FmLeiGerEdit: TFmLeiGerEdit;

implementation

uses Module, UnMyObjects, UnConsumoGerlJan, DmkDAC_PF;

{$R *.DFM}

procedure TFmLeiGerEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLeiGerEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLeiGerEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  CkContinuar.Checked := False;
end;

procedure TFmLeiGerEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLeiGerEdit.FormShow(Sender: TObject);
var
  SQLTipo: TSQLType;
begin
  if FControle <> 0 then
    SQLTipo := stUpd
  else
    SQLTipo := stIns;
  //
  ReopenCons(FCliInt);
  MostraEdicao(SQLTipo);
end;

procedure TFmLeiGerEdit.MostraEdicao(SQLTipo: TSQLType; Produto: Integer = 0);
var
  Visi: Boolean;
  MedAnt, MedAtu: Double;
begin
  DBEdMedAnt.DataSource  := FDataSourceCNS;
  DBEdMedAnt.DataField   := 'MedAnt';
  DBEdMedAtu.DataSource  := FDataSourceCNS;
  DBEdMedAtu.DataField   := 'MedAtu';
  DBEdConsumo.DataSource := FDataSourceCNS;
  DBEdConsumo.DataField  := 'Consumo';
  DBEdValor.DataSource   := FDataSourceCNS;
  DBEdValor.DataField    := 'Valor';
  DBEdPreco.DataSource   := FDataSourceCNS;
  DBEdPreco.DataField    := 'Preco';
  //
  LaNomeCliInt.Caption  := FNomeCliInt;
  EdPeriodoTXT.Text     := FPeriodoTXT;
  EdPeriodoTXT.ReadOnly := True;
  //
  Visi := FTabCnsA <> '';
  //
  LaUnidade.Visible   := Visi;
  DBEdUnidade.Visible := Visi;
  //
  if FTabCnsA <> '' then
  begin
    DBEdUnidade.DataSource := FDataSourceCNS;
    DBEdUnidade.DataField  := 'Unidade';
    //
    LaData.Visible := False;
    TPData.Visible := False;
  end else
  begin
    LaData.Visible := True;
    TPData.Visible := True;
  end;
  //
  if SQLTipo = stIns then
  begin
    ImgTipo.SQLType    := stIns;
    EdCons.Enabled     := True;
    CBCons.Enabled     := True;
    SBCons.Enabled     := True;
    PnProduto.Enabled  := True;
    EdCarencia.Enabled := True;
    PnDados.Visible    := False;
    //
    if FTabCnsA = '' then
      TPData.Enabled := True;
    //
    if Produto = 0 then
    begin
      ConfiguraCasas(0);
      //
      FUnidFat  := 0;
      FDifCaren := 0;
      //
      TPData.Date             := Date;
      EdLeiAnt.ValueVariant   := 0;
      EdLeiAtu.ValueVariant   := 0;
      EdCons.ValueVariant     := 0;
      CBCons.KeyValue         := Null;
      EdCarencia.ValueVariant := 0;
      EdPreco.ValueVariant    := 0;
      Label14.Caption         := '';
      Label15.Caption         := '';
      //
      CkContinuar.Checked := False;
      CkContinuar.Visible := True;
      //
      EdCons.SetFocus;
    end else
    begin
      ConfiguraCasas(QrConsCasas.Value);
      //
      FUnidFat  := QrConsUnidFat.Value;
      FDifCaren := QrConsDifCaren.Value;
      MedAnt    := ObtemMedidaAnt(FCliInt, Produto);
      MedAtu    := EdLeiAtu.ValueVariant;
      //
      if MedAnt = 0 then
        EdLeiAnt.ValueVariant := Geral.FFT(EdLeiAnt.ValueVariant, QrConsCasas.Value, siPositivo)
      else
        EdLeiAnt.ValueVariant := Geral.FFT(MedAnt, QrConsCasas.Value, siPositivo);
      //
      EdLeiAtu.ValueVariant   := Geral.FFT(MedAtu, QrConsCasas.Value, siPositivo);
      EdCarencia.ValueVariant := Geral.FFT(QrConsCarencia.Value, QrConsCasas.Value, siPositivo);
      EdPreco.ValueVariant    := QrConsPreco.AsFloat;
      Label14.Caption         := QrConsUnidLei.Value;
      Label15.Caption         := QrConsUnidImp.Value;
    end;
  end else
  begin
    ImgTipo.SQLType    := stUpd;
    EdCons.Enabled     := False;
    CBCons.Enabled     := False;
    SBCons.Enabled     := False;
    TPData.Enabled     := False;
    PnProduto.Enabled  := False;
    EdCarencia.Enabled := False;
    PnDados.Visible    := True;
    //
    ConfiguraCasas(FQueryCNS.FieldByName('Casas').AsInteger);
    //
    FUnidFat  := FQueryCNS.FieldByName('UnidFat').AsInteger;
    FDifCaren := FQueryCNS.FieldByName('DifCaren').AsInteger;
    //
    if FTabCnsA = '' then
      TPData.Date := FQueryCNS.FieldByName('Data').AsDateTime;
    //
    EdCons.ValueVariant     := FQueryCNS.FieldByName('Codigo').AsInteger;
    CBCons.KeyValue         := FQueryCNS.FieldByName('Codigo').AsInteger;
    EdLeiAnt.ValueVariant   := Geral.FFT(FQueryCNS.FieldByName('MedAnt').AsFloat, FQueryCNS.FieldByName('Casas').AsInteger, siPositivo);
    EdLeiAtu.ValueVariant   := Geral.FFT(FQueryCNS.FieldByName('MedAtu').AsFloat, FQueryCNS.FieldByName('Casas').AsInteger, siPositivo);
    EdCarencia.ValueVariant := Geral.FFT(FQueryCNS.FieldByName('Carencia').AsFloat, FQueryCNS.FieldByName('Casas').AsInteger, siPositivo);
    EdPreco.ValueVariant    := FQueryCNS.FieldByName('Preco').AsFloat;
    Label14.Caption         := FQueryCNS.FieldByName('UnidLei').AsString;
    Label15.Caption         := FQueryCNS.FieldByName('UnidImp').AsString;
    //
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
    //
    EdLeiAtu.SetFocus;
  end;
end;

function TFmLeiGerEdit.ObtemMedidaAnt(CliInt, Produto: Integer): Double;
var
  Qry: TmySQLQuery;
begin
  Result        := 0;
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MAX(cni.MedAtu) MedAtu ',
      'FROM consprc cnp ',
      'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
      'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
      'WHERE cni.Empresa=' + Geral.FF0(CliInt),
      'AND cnp.Cond=' + Geral.FF0(CliInt),
      'AND cni.Codigo=' + Geral.FF0(Produto),
      'ORDER BY cni.DataCad, cni.Controle ',
      '']);
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('MedAtu').AsFloat;
  finally
    Qry.Free;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLeiGerEdit.ReopenCons(CliInt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCons, Dmod.MyDB, [
    'SELECT cns.Codigo, cns.Nome, prc.UnidFat, prc.Preco, ',
    'prc.Casas, prc.Carencia, prc.DifCaren, prc.UnidImp, prc.UnidLei ',
    'FROM cons cns ',
    'LEFT JOIN consprc prc ON prc.Codigo = cns.Codigo ',
    'WHERE prc.Cond=' + Geral.FF0(CliInt),
    'AND prc.Ativo = 1 ',
    'ORDER BY cns.Nome ',
    '']);
end;

procedure TFmLeiGerEdit.SBConsClick(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdCons.ValueVariant;
  //
  ConsumoGerlJan.MostraCons(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCons, CBCons, QrCons, VAR_CADASTRO);
    EdCons.SetFocus;
  end;
  MostraEdicao(stIns, Codigo);
end;

procedure TFmLeiGerEdit.EdCarenciaChange(Sender: TObject);
begin
  CalculaConsumoECusto;
end;

procedure TFmLeiGerEdit.EdConsChange(Sender: TObject);
begin
  ConfiguraConsumo();
end;

procedure TFmLeiGerEdit.EdConsExit(Sender: TObject);
begin
  ConfiguraConsumo();
end;

procedure TFmLeiGerEdit.ConfiguraConsumo();
var
  Cons: Integer;
begin
  Cons := EdCons.ValueVariant;
  //
  if (ImgTipo.SQLType = stIns) and (Cons <> 0) then
    MostraEdicao(stIns, Cons);
end;

procedure TFmLeiGerEdit.EdLeiAntChange(Sender: TObject);
begin
  CalculaConsumoECusto;
end;

procedure TFmLeiGerEdit.EdLeiAtuChange(Sender: TObject);
begin
  CalculaConsumoECusto;
end;

procedure TFmLeiGerEdit.EdPrecoChange(Sender: TObject);
begin
  CalculaConsumoECusto;
end;

procedure TFmLeiGerEdit.CalculaConsumoECusto;

  function ObtemConsumoAtual(): Double;
  var
    Qry: TmySQLQuery;
  begin
    Result        := 0;
    Screen.Cursor := crHourGlass;
    Qry           := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT MAX(cni.MedAtu) - MIN(cni.MedAnt) Medida ',
        'FROM consprc cnp ',
        'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
        'LEFT JOIN leigerits cni ON cni.Codigo=cns.Codigo ',
        'WHERE cni.Empresa=' + Geral.FF0(FCliInt),
        'AND cnp.Cond=' + Geral.FF0(FCliInt),
        'AND cni.Periodo=' + Geral.FF0(FPeriodo),
        'AND cns.Codigo=' + Geral.FF0(EdCons.ValueVariant),
        'AND cni.Tipo=0 ',
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('Medida').AsFloat;
    finally
      Qry.Free;
      Screen.Cursor := crDefault;
    end;
  end;

var
  Ant, Atu, Prc, Uso, Val, Us2, Caren, ConsAtu: Double;
begin
  if FTabCnsA = '' then
    ConsAtu := ObtemConsumoAtual()
  else
    ConsAtu := 0;
  //
  Ant   := Geral.DMV(EdLeiAnt.Text);
  Atu   := Geral.DMV(EdLeiAtu.Text);
  Prc   := Geral.DMV(EdPreco.Text);
  Caren := Geral.DMV(EdCarencia.Text) - ConsAtu;
  //
  if Caren < 0 then
    Caren := 0;
  //
  Uso := Atu - Ant;
  //
  if Uso <= Caren then
    Uso := 0
  else if FDifCaren = 1 then
    Uso := Uso - Caren;
  //
  Us2 := Uso * FUnidFat;
  //
  if Us2 <= 0 then
    Val := 0
  else
    Val := Trunc(((Us2 * Prc) + 0.005) * 100) / 100;
  //
  EdConsumo.ValueVariant := Uso;
  EdConsum2.ValueVariant := Us2;
  EdValor.Text           := Geral.FFT(Val, 2, siPositivo);
  BtOK.Enabled           := Us2 >= 0;
end;

procedure TFmLeiGerEdit.ConfiguraCasas(Casas: Integer);
begin
  EdLeiAnt.DecimalSize   := Casas;
  EdLeiAtu.DecimalSize   := Casas;
  EdPreco.DecimalSize    := Casas;
  EdConsumo.DecimalSize  := Casas;
  EdConsum2.DecimalSize  := Casas;
  EdCarencia.DecimalSize := Casas;
end;

procedure TFmLeiGerEdit.BtOKClick(Sender: TObject);
var
  Controle, Cons, Casas: Integer;
  MedAnt, MedAtu, Carencia, Consumo, Preco, Valor: Double;
  UnidLei, UnidImp, Data: String;
begin
  Cons     := EdCons.ValueVariant;
  MedAnt   := EdLeiAnt.ValueVariant;
  MedAtu   := EdLeiAtu.ValueVariant;
  Carencia := EdCarencia.ValueVariant;
  Consumo  := EdConsumo.ValueVariant;
  Preco    := EdPreco.ValueVariant;
  Valor    := EdValor.ValueVariant;
  //
  if MyObjects.FIC(Cons = 0, nil, 'Defina o produto!') then Exit;
  //
  if FTabCnsA <> '' then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabCnsA, False,
      ['MedAnt', 'MedAtu', 'Carencia', 'Consumo', 'Preco', 'Valor'], ['Controle'],
      [MedAnt, MedAtu, Carencia, Consumo, Preco, Valor], [FControle], True) then
    begin
      if FLancto <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabLctA, False,
          ['Credito'], ['FatID', 'Controle'], [Valor],
          [VAR_FATID_0601, FLancto], True);
      end;
      Close;
    end;
  end else
  begin
    if ImgTipo.SQLType = stIns then
    begin
      if MyObjects.FIC(FCliInt = 0, nil, 'Empresa n�o definida!') then Exit;
      if MyObjects.FIC(FPeriodo = 0, nil, 'Per�odo n�o definido!') then Exit;
      //
      Controle := UMyMod.BPGS1I32('leigerits', 'Controle', '', '', tsPos, stIns, 0);
      Casas    := QrConsCasas.Value;
      UnidLei  := QrConsUnidLei.Value;
      UnidImp  := QrConsUnidImp.Value;
      Data     := Geral.FDT(TPData.Date, 01);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'leigerits', False,
        [
        'MedAnt', 'MedAtu', 'Carencia', 'DifCaren', 'Consumo', 'Casas',
        'Preco', 'UnidLei', 'UnidImp', 'UnidFat', 'Valor', 'Empresa',
        'Periodo', 'Data', 'Tipo', 'Codigo'
        ], ['Controle'],
        [
        MedAnt, MedAtu, Carencia, FDifCaren, Consumo, Casas,
        Preco, UnidLei, UnidImp, FUnidFat, Valor, FCliInt,
        FPeriodo, Data, 0, Cons
        ], [Controle], True);
      //
      FCodigo := Cons;
      //
      if CkContinuar.Checked then
      begin
        Geral.MB_Aviso('Dados salvos com sucesso!');
        EdCons.SetFocus;
      end else
        Close;
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'leigerits', False,
        ['MedAnt', 'MedAtu', 'Carencia', 'Consumo', 'Preco', 'Valor'], ['Controle'],
        [MedAnt, MedAtu, Carencia, Consumo, Preco, Valor], [FControle], True);
      //
      FCodigo := Cons;
      Close;
    end;
  end;
end;

end.
