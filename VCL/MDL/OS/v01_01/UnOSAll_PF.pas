unit UnOSAll_PF;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, Forms, Menus,
  DmkGeral, DmkDAC_PF, UnInternalConsts, UnDmkProcFunc, dmkEdit,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TUnOSAll_PF = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure CopiaCliente(Key: Word; EdCliente, Ed: TdmkEdit; CB: TdmkDBLookupComboBox);
    procedure AtualizaCadastroCunsIts(CunsGru, CunsSub: Integer);
    function  AchouNovosClientes(var MaiorCodigo: Integer): Boolean;
    function  ObtemListaDePergunta(Equipamento: Integer): Integer;
    function  ObtemPeriodoDiasContrato(Contrato: Integer): Integer;
  end;

var
  OSAll_PF: TUnOSAll_PF;


implementation

uses Module, UMySQLModule;

{ TUnOSAll_PF }

procedure TUnOSAll_PF.CopiaCliente(Key: Word; EdCliente, Ed: TdmkEdit; CB: TdmkDBLookupComboBox);
begin
  if Key = VK_F4 then
  begin
    if Ed <> nil then
      Ed.ValueVariant := EdCliente.ValueVariant;
    if CB <> nil then
      CB.KeyValue := EdCliente.ValueVariant;
  end;
end;

function TUnOSAll_PF.ObtemListaDePergunta(Equipamento: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT PrgLstCab ',
      'FROM grag1eqmo ',
      'WHERE Nivel1=' + Geral.FF0(Equipamento),
      '']);
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('PrgLstCab').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnOSAll_PF.ObtemPeriodoDiasContrato(Contrato: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  //
  if Contrato = 0 then
    Exit;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM contratos ',
      'WHERE DInstal > "1900-01-01" ',
      'AND DtaPrxRenw > "1900-01-01" ',
      'AND Codigo=' + Geral.FF0(Contrato),
      '']);
    if Qry.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DATEDIFF(DtaPrxRenw, DInstal) Dif ',
        'FROM contratos ',
        'WHERE Codigo=' + Geral.FF0(Contrato),
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('Dif').AsInteger;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnOSAll_PF.AchouNovosClientes(var MaiorCodigo: Integer): Boolean;
var
  Codigo: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
{$IfNDef NAO_CUNS}
  MaiorCodigo := 0;
  Codigo := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE Cliente1="V" ',
    'AND NOT (Codigo IN ( ',
    '     SELECT Codigo FROM cunscad ',
    '     ) ',
    ') ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Qry.First;
      while not Qry.Eof do
      begin
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cunscad', False, [
        (*'AtivPrinc', 'HowFind', 'Account',
        'DataCon'*)], [
        'Codigo'], [
        (*AtivPrinc, HowFind, Account,
        DataCon*)], [
        Codigo], True);
        //
        Qry.Next;
      end;
      MaiorCodigo := Codigo;
      Result := True;
    end;
  except
    Qry.Free;
  end;
{$Else}
  //dmkPF.InfoSemModulo(mdlappCuns);
{$EndIf}
end;

procedure TUnOSAll_PF.AtualizaCadastroCunsIts(CunsGru, CunsSub: Integer);
var
  Controle: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    if (CunsGru <> 0) and (CunsSub <> 0) and (CunsGru <> CunsSub ) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM cunsits ',
      'WHERE CunsGru=' + Geral.FF0(CunsGru),
      'AND CunsSub=' + Geral.FF0(CunsSub),
      '']);
      //
      if Qry.RecordCount = 0 then
      begin
        Controle := UMyMod.BPGS1I32('cunsits', 'Controle', '', '', tsPos, stIns, 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cunsits', False, [
        'CunsGru', 'CunsSub'], [
        'Controle'], [
        CunsGru, CunsSub], [
        Controle], True);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

end.
