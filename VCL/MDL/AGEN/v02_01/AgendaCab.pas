unit AgendaCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkEdit,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, mySQLDbTables,
  dmkImage, DmkDAC_PF, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, UnitAgendaAux;

type
  TFmAgendaCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label12: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    MeDescri: TMemo;
    GridPanel1: TGridPanel;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    Shape6: TShape;
    Shape7: TShape;
    Shape8: TShape;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Label1: TLabel;
    Shape1: TShape;
    Label3: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VUEmpresa: TdmkValUsu;
    CkDiaTodo: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Shape1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FCodigo: Integer;
    FCor: TColor;
    procedure ConfiguraGradeCores(CorAtal: TColor);
  public
    { Public declarations }
    procedure MostraEdicao(SQLType: TSQLType; Codigo: Integer);
  end;

  var
  FmAgendaCab: TFmAgendaCab;

implementation

uses UnMyObjects, Module, UnDmkImg, UMySQLModule, ModuleGeral, UnitAgenda;

{$R *.DFM}

procedure TFmAgendaCab.BtOKClick(Sender: TObject);
var
  SQLTipo: TSQLType;
  Empresa: Integer;
  Nome, Descri, Cor: String;
begin
  Nome   := EdNome.ValueVariant;
  Descri := MeDescri.Text;
  Empresa := EdEmpresa.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina o nome!') then Exit;
  if MyObjects.FIC(FCor = clNone, nil, 'Defina a cor!') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  //
  Cor := DmkImg.TColorToHex(FCor);
  //
  if FCodigo = 0 then
  begin
    SQLTipo := stIns;
    FCodigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'agencab', 'Codigo', [],
                 [], stIns, 0, siPositivo, nil);
  end else
  begin
    SQLTipo := stUpd;
  end;

  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'agencab', False,
    ['Nome', 'Descri', 'Empresa', 'Cor', 'Mostra'], ['Codigo'],
    [Nome, Descri, VUEmpresa.ValueVariant, Cor, 1], [FCodigo], True) then
  begin
    VAR_CADASTRO := FCodigo;
    Close;
  end;
end;

procedure TFmAgendaCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Close;
end;

procedure TFmAgendaCab.ConfiguraGradeCores(CorAtal: TColor);

  procedure ConfiguraShape(Shape: TShape; Cor: TColor; Seleciona: Boolean);
  begin
    Shape.Brush.Color := Cor;
    Shape.Brush.Style := bsSolid;
    Shape.Pen.Color   := clWhite;
    Shape.Pen.Style   := psSolid;
    //
    if Seleciona = True then
      Shape.Pen.Width := 5
    else
      Shape.Pen.Width := 0;
  end;
var
  Cor1, Cor2, Cor3, Cor4, Cor5, Cor6, Cor7, Cor8, Cor9, Cor10, Cor11, Cor12: TColor;
  CorAtalTxt: String;
begin
  Cor1  := UnAgendaAux.ObtemCorAgenda(0);
  Cor2  := UnAgendaAux.ObtemCorAgenda(1);
  Cor3  := UnAgendaAux.ObtemCorAgenda(2);
  Cor4  := UnAgendaAux.ObtemCorAgenda(3);
  Cor5  := UnAgendaAux.ObtemCorAgenda(4);
  Cor6  := UnAgendaAux.ObtemCorAgenda(5);
  Cor7  := UnAgendaAux.ObtemCorAgenda(6);
  Cor8  := UnAgendaAux.ObtemCorAgenda(7);
  Cor9  := UnAgendaAux.ObtemCorAgenda(8);
  Cor10 := UnAgendaAux.ObtemCorAgenda(9);
  Cor11 := UnAgendaAux.ObtemCorAgenda(10);
  Cor12 := UnAgendaAux.ObtemCorAgenda(11);
  //
  ConfiguraShape(Shape1,  Cor1,  Cor1  = CorAtal);
  ConfiguraShape(Shape2,  Cor2,  Cor2  = CorAtal);
  ConfiguraShape(Shape3,  Cor3,  Cor3  = CorAtal);
  ConfiguraShape(Shape4,  Cor4,  Cor4  = CorAtal);
  ConfiguraShape(Shape5,  Cor5,  Cor5  = CorAtal);
  ConfiguraShape(Shape6,  Cor6,  Cor6  = CorAtal);
  ConfiguraShape(Shape7,  Cor7,  Cor7  = CorAtal);
  ConfiguraShape(Shape8,  Cor8,  Cor8  = CorAtal);
  ConfiguraShape(Shape9,  Cor9,  Cor9  = CorAtal);
  ConfiguraShape(Shape10, Cor10, Cor10 = CorAtal);
  ConfiguraShape(Shape11, Cor11, Cor11 = CorAtal);
  ConfiguraShape(Shape12, Cor12, Cor12 = CorAtal);
end;

procedure TFmAgendaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAgendaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCor    := clNone;
  FCodigo := 0;
end;

procedure TFmAgendaCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgendaCab.MostraEdicao(SQLType: TSQLType; Codigo: Integer);
var
  Qry: TMySQLQuery;
begin
  ImgTipo.SQLType := SQLType;
  //
  if SQLType = stIns then
  begin
    FCodigo                := 0;
    FCor                   := clNone;
    EdNome.ValueVariant    := '';
    EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    MeDescri.Text          := '';
  end else
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM agencab ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      if Qry.RecordCount > 0 then
      begin
        FCodigo                := Qry.FieldByName('Codigo').AsInteger;
        FCor                   := DmkImg.HexToTColor(Qry.FieldByName('Cor').AsString);
        EdNome.ValueVariant    := Qry.FieldByName('Nome').AsString;
        VUEmpresa.ValueVariant := Qry.FieldByName('Empresa').AsInteger;
        MeDescri.Text          := Qry.FieldByName('Descri').AsString;
      end else
      begin
        Geral.MB_Erro('Falha ao obter dados!');
        Close;
      end;
    finally
      Qry.Free;
    end;
  end;
  ConfiguraGradeCores(FCor);
end;

procedure TFmAgendaCab.Shape1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FCor := TShape(Sender).Brush.Color;
  //
  ConfiguraGradeCores(FCor);
end;

end.
