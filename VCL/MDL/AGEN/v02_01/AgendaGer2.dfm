object FmAgendaGer2: TFmAgendaGer2
  Left = 339
  Top = 185
  Caption = 'AGE-GEREN-001 :: Gerenciamento de Agendas'
  ClientHeight = 774
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtOpcoes: TBitBtn
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 396
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 396
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 396
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 629
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 629
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 629
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnAgendas: TPanel
          Left = 2
          Top = 18
          Width = 329
          Height = 609
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 272
            Width = 329
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 0
            object BtAgeExclui: TBitBtn
              Tag = 12
              Left = 103
              Top = 5
              Width = 50
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtAgeExcluiClick
            end
            object BtAgeAltera: TBitBtn
              Tag = 11
              Left = 54
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtAgeAlteraClick
            end
            object BtAgeInclui: TBitBtn
              Tag = 10
              Left = 5
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtAgeIncluiClick
            end
            object BtReabre: TBitBtn
              Tag = 18
              Left = 153
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BtReabreClick
            end
          end
          object MCCalendario: TMonthCalendar
            Left = 0
            Top = 20
            Width = 329
            Height = 252
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Date = 42272.978767534730000000
            TabOrder = 1
            OnClick = MCCalendarioClick
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 0
            Width = 66
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Agendas'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
          object LVAgendas: TListView
            Left = 0
            Top = 331
            Width = 329
            Height = 278
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Checkboxes = True
            Columns = <
              item
                Caption = 'Descri'#231#227'o'
                Width = 308
              end>
            ColumnClick = False
            Groups = <
              item
                Header = 'Agendas do sistema'
                GroupID = 1
                State = [lgsNormal]
                HeaderAlign = taLeftJustify
                FooterAlign = taLeftJustify
                TitleImage = -1
              end
              item
                Header = 'Agendas'
                GroupID = 0
                State = [lgsNormal]
                HeaderAlign = taLeftJustify
                FooterAlign = taLeftJustify
                TitleImage = -1
              end>
            GroupView = True
            ReadOnly = True
            ShowColumnHeaders = False
            SmallImages = ImageList1
            TabOrder = 3
            ViewStyle = vsReport
            OnItemChecked = LVAgendasItemChecked
          end
        end
        object PnItens: TPanel
          Left = 331
          Top = 18
          Width = 908
          Height = 609
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          DoubleBuffered = False
          ParentDoubleBuffered = False
          TabOrder = 1
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 908
            Height = 609
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 0
            object TabSheet3: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Novo'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Splitter2: TSplitter
                Left = 322
                Top = 79
                Width = 12
                Height = 499
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                ExplicitLeft = 319
                ExplicitTop = 80
                ExplicitHeight = 490
              end
              object PnCompromisso: TPanel
                Left = 334
                Top = 79
                Width = 566
                Height = 499
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 0
                object PCCompromisso: TPageControl
                  Left = 0
                  Top = 0
                  Width = 566
                  Height = 499
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ActivePage = TSCompromissos
                  Align = alClient
                  TabOrder = 0
                  object TSCompromissos: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Compromisso'
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object SGCompromissos: TStringGrid
                      Left = 0
                      Top = 59
                      Width = 558
                      Height = 409
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      FixedCols = 0
                      RowCount = 2
                      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
                      ScrollBars = ssNone
                      TabOrder = 0
                      OnDrawCell = SGCompromissosDrawCell
                      ColWidths = (
                        64
                        64
                        64
                        64
                        64)
                    end
                    object Panel7: TPanel
                      Left = 0
                      Top = 0
                      Width = 558
                      Height = 59
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      ParentBackground = False
                      TabOrder = 1
                      object BtAlteraAge: TBitBtn
                        Tag = 11
                        Left = 5
                        Top = 5
                        Width = 49
                        Height = 49
                        Cursor = crHandPoint
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtAlteraAgeClick
                      end
                      object BtExcluiAge: TBitBtn
                        Tag = 12
                        Left = 54
                        Top = 5
                        Width = 49
                        Height = 49
                        Cursor = crHandPoint
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 1
                        OnClick = BtExcluiAgeClick
                      end
                    end
                  end
                end
              end
              object PCCalendario: TPageControl
                Left = 0
                Top = 79
                Width = 322
                Height = 499
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TSTarefas
                Align = alClient
                TabOrder = 1
                object TSMes: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Calend'#225'rio'
                  ImageIndex = 1
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                end
                object TSAgenda: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Calend'#225'rio'
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                end
                object TSTarefas: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Tarefas'
                  ImageIndex = 2
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                end
              end
              object Panel6: TPanel
                Left = 0
                Top = 20
                Width = 900
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                ParentBackground = False
                TabOrder = 2
                object RGExibir: TRadioGroup
                  Left = 591
                  Top = 1
                  Width = 308
                  Height = 57
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alRight
                  Caption = ' Exibir '
                  TabOrder = 0
                end
                object BitBtn1: TBitBtn
                  Tag = 10
                  Left = 5
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtIncluiItemClick
                end
              end
              object StaticText2: TStaticText
                Left = 0
                Top = 0
                Width = 900
                Height = 20
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 'Compromissos'
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentColor = False
                ParentFont = False
                TabOrder = 3
                ExplicitWidth = 107
              end
            end
            object TabSheet4: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Antigo'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGAgendas: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 238
                Height = 578
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Mostra'
                    Width = 25
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 130
                    Visible = True
                  end>
                DataSource = DsAgendas
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                ParentColor = True
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnCellClick = DBGAgendasCellClick
                OnDrawColumnCell = DBGAgendasDrawColumnCell
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Mostra'
                    Width = 25
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 130
                    Visible = True
                  end>
              end
              object Panel8: TPanel
                Left = 238
                Top = 0
                Width = 662
                Height = 578
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 1
                object Panel9: TPanel
                  Left = 1
                  Top = 1
                  Width = 660
                  Height = 59
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  TabOrder = 0
                  object Label32: TLabel
                    Left = 12
                    Top = 5
                    Width = 36
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Exibir:'
                  end
                  object BtHoje: TBitBtn
                    Tag = 107
                    Left = 215
                    Top = 5
                    Width = 50
                    Height = 49
                    Cursor = crHandPoint
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtHojeClick
                  end
                  object BtIncluiItem: TBitBtn
                    Tag = 10
                    Left = 265
                    Top = 5
                    Width = 49
                    Height = 49
                    Cursor = crHandPoint
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtIncluiItemClick
                  end
                  object BtAlteraTar: TBitBtn
                    Tag = 11
                    Left = 314
                    Top = 5
                    Width = 49
                    Height = 49
                    Cursor = crHandPoint
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtAlteraTarClick
                  end
                  object BtExcluiTar: TBitBtn
                    Tag = 12
                    Left = 363
                    Top = 5
                    Width = 49
                    Height = 49
                    Cursor = crHandPoint
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 3
                    OnClick = BtExcluiTarClick
                  end
                  object CBExibir: TComboBox
                    Left = 12
                    Top = 25
                    Width = 197
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    AutoDropDown = True
                    Style = csDropDownList
                    Color = clWhite
                    DropDownCount = 12
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = 7622183
                    Font.Height = -15
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 4
                    OnChange = CBExibirChange
                  end
                end
              end
            end
            object TabSheet1: TTabSheet
              Caption = 'TabSheet1'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ListView1: TListView
                Left = 0
                Top = 0
                Width = 900
                Height = 578
                Align = alClient
                Columns = <
                  item
                    Width = 250
                  end
                  item
                    Width = 100
                  end>
                Items.ItemData = {
                  05E20000000200000001000000FFFFFFFFFFFFFFFF01000000FFFFFFFF000000
                  001549006D0070006C0065006D0065006E0074006100E700E3006F0020006400
                  6F0020005300500045004400023100300028A0A42901000000FFFFFFFFFFFFFF
                  FF01000000FFFFFFFF000000003749006D0070006C0065006D0065006E007400
                  6100E700E3006F00200064006100200070006C0061007400610066006F007200
                  6D006100200064006500200064006500730065006E0076006F006C0076006900
                  6D0065006E0074006F0020006D0075006C007400690020004F00530002310035
                  00C0B9A429FFFFFFFF}
                ShowColumnHeaders = False
                SmallImages = FmMyGlyfs.Lista_32X32_Textos
                TabOrder = 0
                ViewStyle = vsReport
              end
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 12
        Width = 1060
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 18
          Width = 1056
          Height = 34
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 16
            Top = 2
            Width = 15
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 15
            Top = 1
            Width = 15
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 546
    Top = 11
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 520
    Top = 11
  end
  object QrAgendas: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrAgendasBeforeClose
    Left = 64
    Top = 227
    object QrAgendasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgendasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrAgendasMostra: TSmallintField
      FieldName = 'Mostra'
      MaxValue = 1
    end
    object QrAgendasCor: TWideStringField
      FieldName = 'Cor'
      Size = 10
    end
    object QrAgendasTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
  end
  object DsAgendas: TDataSource
    DataSet = QrAgendas
    Left = 93
    Top = 227
  end
  object PMItens: TPopupMenu
    Left = 496
    Top = 320
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object ImageList1: TImageList
    Left = 768
    Top = 288
    Bitmap = {
      494C01010C003000640010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328FFFFF328F
      FFFF328FFFFF328FFFFF328FFFFF328FFFFF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB83FF00BB
      83FF00BB83FF00BB83FF00BB83FF00BB83FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B403FFB4B4
      03FFB4B403FFB4B403FFB4B403FFB4B403FFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFFFFB3
      5EFFFFB35EFFFFB35EFFFFB35EFFFFB35EFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648D2FF2648
      D2FF2648D2FF2648D2FF2648D2FF2648D2FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF998200FF998200FF998200FF9982
      00FF998200FF998200FF998200FF998200FF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFF3F1A
      ACFF3F1AACFF3F1AACFF3F1AACFF3F1AACFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFAE50
      DCFFAE50DCFFAE50DCFFAE50DCFFAE50DCFFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FFC6720000C67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC67200FFC672
      00FFC67200FFC67200FFC67200FFC67200FFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFFB56A
      5FFFB56A5FFFB56A5FFFB56A5FFFB56A5FFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF95008DFF9500
      8DFF95008DFF95008DFF95008DFF95008DFF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A00FF188A
      00FF188A00FF188A00FF188A00FF188A00FF424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
end
