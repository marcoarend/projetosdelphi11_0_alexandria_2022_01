unit UnitAgendaAux;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls(*, PlanRecurrEdit, Planner*);

type
  TUnitAgendaAux = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //Configura componentes
    //procedure ConfiguraPlanner(PLAgenda: TPlanner);
    function  ConfiguraListaExibicao(): TStringList;
    function  ConfiguraListaPrioridades(): TStringList;
    //procedure ConfiguraRecorrencia(RERecorrencia: TPlannerRecurrencyEditor);
    //Cores Agenda
    function  ObtemIndexCorAgenda(Cor: TColor): Integer;
    function  ObtemCorAgenda(Index: Integer): TColor;
  end;

var
  UnAgendaAux: TUnitAgendaAux;

implementation

uses DmkDAC_PF;

{ TUnitAgendaAux }

(*
procedure TUnitAgendaAux.ConfiguraRecorrencia(RERecorrencia: TPlannerRecurrencyEditor);
begin
  with RERecorrencia.LanguageSettings do
  begin
    ButtonAdd         := 'Confirma';
    ButtonCancel      := 'Desiste';
    ButtonClear       := 'Limpar';
    ButtonOK          := 'Ok';
    ButtonRemove      := 'Exclui';
    Caption           := 'Repeti��es';
    DayFriday         := 'Sexta-feira';
    DayMonday         := 'Segunda-feira';
    DaySaturday       := 'S�bado';
    DaySunday         := 'Domingo';
    DayThursday       := 'Quinta-feira';
    DayTuesday        := 'Ter�a-feira';
    DayWednesday      := 'Quarta-feira';
    DayWeekday        := 'Dia da semana';
    DayWeekend        := 'Fim de semana';
    Every             := 'Cada';
    EveryDay          := 'Todos os dias';
    EveryFirst        := 'Cada primeiro';
    EveryFourth       := 'Cada quarta';
    EveryMonthDay     := 'Todo dia do m�s';
    EverySecond       := 'Cada segundo';
    EveryThird        := 'Cada terceiro';
    EveryWeekDay      := 'Semana toda';
    EveryYearDay      := 'Ano todo';
    Exceptions        := 'Exce��es';
    FreqDaily         := 'Di�rio';
    FreqHourly        := 'A cada hora';
    FreqMonthly       := 'Mensal';
    FreqNone          := 'Nenhum';
    FreqWeekly        := 'Semanal';
    FreqYearly        := 'Anual';
    Interval          := 'Intervalo';
    MonthApril        := 'Abr';
    MonthAugust       := 'Ago';
    MonthDecember     := 'Dez';
    MonthFebruary     := 'Fev';
    MonthJanuary      := 'Jan';
    MonthJuly         := 'Jul';
    MonthJune         := 'Jun';
    MonthMarch        := 'Mar';
    MonthMay          := 'Maio';
    MonthNovember     := 'Nov';
    MonthOctober      := 'Out';
    MonthSeptember    := 'Set';
    PatternDetails    := 'Detalhes padr�o';
    Range             := 'Alcance';
    RangeFor          := 'Para';
    RangeInfinite     := 'Infinito';
    RangeOccurences   := 'Ocorr�ncias';
    RangeUntil        := 'At� � data';
    RecurrencyPattern := 'Padr�o de repeti��es';
    Settings          := 'Configura��es';
  end;
end;
*)

function TUnitAgendaAux.ConfiguraListaExibicao: TStringList;
var
  Itens: TStringList;
begin
  Itens := TStringList.Create;
  try
    Itens.Add('M�s');
    Itens.Add('Semana');
    Itens.Add('Dia');
    Itens.Add('Tarefas');
  finally
    Result := Itens;
  end;
end;

function TUnitAgendaAux.ConfiguraListaPrioridades: TStringList;
var
  Itens: TStringList;
begin
  Itens := TStringList.Create;
  try
    Itens.Add('Muito baixa');
    Itens.Add('Baixa');
    Itens.Add('Normal');
    Itens.Add('Muito alta');
    Itens.Add('Alta');
  finally
    Result := Itens;
  end;
end;

(*
procedure TUnitAgendaAux.ConfiguraPlanner(PLAgenda: TPlanner);
begin
  PLAgenda.DayNames.Clear;
  PLAgenda.DayNames.Add('Dom');
  PLAgenda.DayNames.Add('Seg');
  PLAgenda.DayNames.Add('Ter');
  PLAgenda.DayNames.Add('Qua');
  PLAgenda.DayNames.Add('Qui');
  PLAgenda.DayNames.Add('Sex');
  PLAgenda.DayNames.Add('Sab');
  //
  PLAgenda.Sidebar.TimeIndicator      := True;
  PLAgenda.Sidebar.TimeIndicatorColor := $00FA27B0;
  PLAgenda.Display.ColorCurrent       := $00FCEBDC;
  PLAgenda.Display.ShowCurrent        := True;
end;
*)

function TUnitAgendaAux.ObtemCorAgenda(Index: Integer): TColor;
begin
  case Index of
      0: Result := $00C67200;
      1: Result := $00B56A5F;
      2: Result := $0095008D;
      3: Result := $00188A00;
      4: Result := $002648D2;
      5: Result := $00998200;
      6: Result := $003F1AAC;
      7: Result := $00AE50DC;
      8: Result := $00328FFF;
      9: Result := $0000BB83;
     10: Result := $00B4B403;
     11: Result := $00FFB35E;
    else Result := clNone;
  end;
end;

function TUnitAgendaAux.ObtemIndexCorAgenda(Cor: TColor): Integer;
begin
  case Cor of
    $00C67200: Result := 0;
    $00B56A5F: Result := 1;
    $0095008D: Result := 2;
    $00188A00: Result := 3;
    $002648D2: Result := 4;
    $00998200: Result := 5;
    $003F1AAC: Result := 6;
    $00AE50DC: Result := 7;
    $00328FFF: Result := 8;
    $0000BB83: Result := 9;
    $00B4B403: Result := 10;
    $00FFB35E: Result := 11;
    else Result := -1;
  end;
end;

end.
