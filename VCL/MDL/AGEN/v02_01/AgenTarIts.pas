unit AgenTarIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkEdit,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, mySQLDbTables,
  dmkImage, DmkDAC_PF, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, UnitAgendaAux,
  dmkEditDateTimePicker(*, PlanRecurrEdit*);

type
  TFmAgenTarIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label12: TLabel;
    EdNome: TdmkEdit;
    MeDescri: TMemo;
    Label4: TLabel;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    BtRepeticoes: TBitBtn;
    LaCred: TLabel;
    EdDuracao: TdmkEdit;
    CBPrioridade: TComboBox;
    Label9: TLabel;
    Label11: TLabel;
    EdRespons: TdmkEditCB;
    CBRespons: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdAgenTarCab: TdmkEditCB;
    CBAgenTarCab: TdmkDBLookupComboBox;
    QrAgenTarCab: TmySQLQuery;
    DsAgenTarCab: TDataSource;
    QrUsuarios: TmySQLQuery;
    DsUsuarios: TDataSource;
    QrAgenTarCabCodigo: TIntegerField;
    QrAgenTarCabNome: TWideStringField;
    QrUsuariosCodigo: TIntegerField;
    QrUsuariosNome: TWideStringField;
    SpeedButton1: TSpeedButton;
    Label3: TLabel;
    TPDataCad: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtRepeticoesClick(Sender: TObject);
    procedure EdAgenTarCabChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    //procedure MostraRecorrencia();
    procedure MostraEdicao(SQLType: TSQLType; Codigo, Controle: Integer);
    procedure ReopenUsuarios();
  public
    { Public declarations }
    FDescri, FRecorrencia: String;
    FCodigo, FControle, FOriID, FPrioridade: Integer;
    FOriNum: Double;
    FDataCad: TDate;
  end;

  var
  FmAgenTarIts: TFmAgenTarIts;

implementation

uses UnMyObjects, Module, UnDmkImg, UMySQLModule, ModuleGeral, UnitAgenda,
  UnGrlAgenda, UnGrl_Vars, UnitAgendaJan;

{$R *.DFM}

procedure TFmAgenTarIts.BtOKClick(Sender: TObject);
var
  Codigo, Prioridade, Respons: Integer;
  Duracao: Double;
  Nome, Descri, Recorrencia: String;
  DataCad, DataExe: TDate;
begin
  Codigo      := EdAgenTarCab.ValueVariant;
  Nome        := EdNome.ValueVariant;
  Descri      := MeDescri.Text;
  DataCad     := TPDataCad.Date;
  DataExe     := TPData.Date;
  Prioridade  := CBPrioridade.ItemIndex;
  Duracao     := EdDuracao.ValueVariant / 60;
  Respons     := EdRespons.ValueVariant;
  Recorrencia := FRecorrencia;
  //
  if GrlAgenda.InsUpdTarefasItem(Dmod.MyDB, Dmod.QrUpd, stDesktop, Nome, Descri,
    Recorrencia, Duracao, FOriNum, DataCad, DataExe, Prioridade, Respons, FOriID,
    Codigo, FControle, EdAgenTarCab, EdNome, TPData, CBPrioridade, EdRespons) then
  begin
    VAR_CADASTRO := FControle;
    Close;
  end;
end;

procedure TFmAgenTarIts.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Close;
end;

procedure TFmAgenTarIts.EdAgenTarCabChange(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := EdAgenTarCab.ValueVariant;
  //
  BtRepeticoes.Visible := (Codigo = -1);
end;

procedure TFmAgenTarIts.BtRepeticoesClick(Sender: TObject);
begin
  //MostraRecorrencia;
end;

(*
procedure TFmAgenTarIts.MostraRecorrencia;
begin
  RERecorrencia.Recurrency := FRecorrencia;
  //
  if RERecorrencia.Execute then
    FRecorrencia := RERecorrencia.Recurrency;
end;
*)

procedure TFmAgenTarIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAgenTarIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCodigo   := 0;
  FControle := 0;
  //
  CBAgenTarCab.ListSource := DsAgenTarCab;
  CBRespons.ListSource    := DsUsuarios;
  //
  //UnAgendaAux.ConfiguraRecorrencia(RERecorrencia);
  GrlAgenda.ReopenAgenTarGer(Dmod.MyDB, QrAgenTarCab, 1, lanpTodosSZero);
  ReopenUsuarios;
end;

procedure TFmAgenTarIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgenTarIts.FormShow(Sender: TObject);
begin
  if FControle = 0 then
    MostraEdicao(stIns, FCodigo, 0)
  else
    MostraEdicao(stUpd, FCodigo, FControle);
end;

procedure TFmAgenTarIts.MostraEdicao(SQLType: TSQLType; Codigo, Controle: Integer);
var
  Cod: Integer;
  Qry: TMySQLQuery;
begin
  ImgTipo.SQLType := SQLType;
  //
  CBPrioridade.Items.Clear;
  CBPrioridade.Items.AddStrings(GrlAgenda.ObtemListaPrioridades);
  //
  if SQLType = stIns then
  begin
    Cod := Codigo;
    //
    if Cod = 0 then
      Cod := 1;
    //
    FControle                 := 0;
    EdAgenTarCab.ValueVariant := Cod;
    CBAgenTarCab.KeyValue     := Cod;
    EdNome.ValueVariant       := '';
    MeDescri.Text             := FDescri;
    TPDataCad.Date            := FDataCad;
    TPData.Date               := Date;
    CBPrioridade.ItemIndex    := FPrioridade;
    EdDuracao.ValueVariant    := 0;
    EdRespons.ValueVariant    := VAR_WEB_USR_ID;
    CBRespons.KeyValue        := VAR_WEB_USR_ID;
    FRecorrencia              := '';
    BtRepeticoes.Visible      := Codigo = -1;
  end else
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM agentarits ',
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
      if Qry.RecordCount > 0 then
      begin
        FControle                 := Qry.FieldByName('Controle').AsInteger;
        EdAgenTarCab.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        CBAgenTarCab.KeyValue     := Qry.FieldByName('Codigo').AsInteger;
        EdNome.ValueVariant       := Qry.FieldByName('Nome').AsString;
        MeDescri.Text             := Qry.FieldByName('Descri').AsString;
        TPData.Date               := Qry.FieldByName('DataExe').AsDateTime;
        CBPrioridade.ItemIndex    := Qry.FieldByName('Prioridade').AsInteger;
        EdDuracao.ValueVariant    := Qry.FieldByName('Duracao').AsFloat * 60;
        EdRespons.ValueVariant    := Qry.FieldByName('Respons').AsInteger;
        CBRespons.KeyValue        := Qry.FieldByName('Respons').AsInteger;
        FRecorrencia              := Qry.FieldByName('Recorrencia').AsString;
        FOriID                    := Qry.FieldByName('OriID').AsInteger;
        FOriNum                   := Qry.FieldByName('OriNum').AsFloat;
        FDataCad                  := Qry.FieldByName('DataCad').AsDateTime;
        BtRepeticoes.Visible      := Qry.FieldByName('Codigo').AsInteger = -1;
      end else
      begin
        Geral.MB_Erro('Falha ao obter dados!');
        Close;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmAgenTarIts.ReopenUsuarios();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsuarios, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM usuarios ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmAgenTarIts.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnAgendaJan.MostraFormAgenTarCab(0);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdAgenTarCab, CBAgenTarCab, QrAgenTarCab, VAR_CADASTRO);
    //
    EdAgenTarCab.SetFocus;
  end;
end;

end.
