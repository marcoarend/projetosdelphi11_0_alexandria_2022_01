unit AgendaIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkValUsu, dmkCompoStore,
  mySQLDbTables, (*PlanRecurrEdit, *)UnDmkProcFunc, (*Planner, *)
  UnitAgenda, UnitAgendaAux;

type
  TFmAgendaIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrAgendas: TmySQLQuery;
    DsAgendas: TDataSource;
    QrLoc: TmySQLQuery;
    QrAgendasCodigo: TIntegerField;
    QrAgendasEmpresa: TIntegerField;
    QrAgendasNome: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    QrAgendasCor: TWideStringField;
    PageControl1: TPageControl;
    TSCompromissos: TTabSheet;
    TSTarefas: TTabSheet;
    Label3: TLabel;
    EdAgenda: TdmkEditCB;
    CBAgenda: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdNome: TdmkEdit;
    CkDiaTodo: TCheckBox;
    GBDataIni: TGroupBox;
    Label18: TLabel;
    LaHoraIni: TLabel;
    TPDataIni: TDateTimePicker;
    EdHoraIni: TdmkEdit;
    GBDataFim: TGroupBox;
    Label1: TLabel;
    LaHoraFim: TLabel;
    TPDataFim: TDateTimePicker;
    EdHoraFim: TdmkEdit;
    BtRepeticoes: TBitBtn;
    Label4: TLabel;
    MeDescri: TMemo;
    Label2: TLabel;
    EdEndereco: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Label9: TLabel;
    CBTarPrioridade: TComboBox;
    LaCred: TLabel;
    EdTarDuracao: TdmkEdit;
    Label11: TLabel;
    EdTarRespons: TdmkEditCB;
    CBTarRespons: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTarEmpresa: TdmkEditCB;
    CBTarEmpresa: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdTarNome: TdmkEdit;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    TPTarDataIni: TDateTimePicker;
    GroupBox3: TGroupBox;
    Label13: TLabel;
    TPTarDataFim: TDateTimePicker;
    BtTarRepeticoes: TBitBtn;
    Label15: TLabel;
    MeTarDescri: TMemo;
    VUTarEmpresa: TdmkValUsu;
    QrUsuarios: TmySQLQuery;
    DsUsuarios: TDataSource;
    QrUsuariosNumero: TIntegerField;
    QrUsuariosNome: TWideStringField;
    Label7: TLabel;
    CBTarStatus: TComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRepeticoesClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdAgendaChange(Sender: TObject);
    procedure CkDiaTodoClick(Sender: TObject);
    procedure TPDataIniExit(Sender: TObject);
    procedure EdHoraIniExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtTarRepeticoesClick(Sender: TObject);
  private
    { Private declarations }
    FTodos: Boolean;
    FControle: Integer;
    FDataIniDB: TDateTime;
    FRecorrencia, FRecorrenciaDB, FTimeZoneDifUTC: String;
    procedure ReopenAgendas();
    procedure ConfiguraTimeZone(Empresa: Integer);
    procedure MostraRecorrencia();
  public
    { Public declarations }
    procedure MostraEdicao(SQLType: TSQLType; AgendaTipo: TAgendaTipo; Codigo,
                Controle: Integer; DataPadrao: TDateTime; Todos: Boolean);
  end;

  var
  FmAgendaIts: TFmAgendaIts;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UnDmkWeb,
  UMySQLModule, MyGlyfs, Principal, UnGOTOy, UnGrlAgenda;

{$R *.DFM}

procedure TFmAgendaIts.BtOKClick(Sender: TObject);
var
  SQLTipo, SQLTipo2: TSQLType;
  Agenda, DiaTodo, Empresa, Respons, Prioridade, Status, ControleDB: Integer;
  DataIni, DataFim, DataHoraIniUTC, DataHoraFimUTC, DataHoraIniTZ,
  DataHoraFimTZ: TDateTime;
  DataHoraIniUTC_Txt, DataHoraFimUTC_Txt, DataHoraIniTZ_Txt,
  DataHoraFimTZ_Txt, Nome, Descri, Endereco, HoraIni, HoraFim, Cor: String;
  Duracao: Double;
begin
  if TSTarefas.TabVisible = False then //Compromissos
  begin
    if MyObjects.FIC(TPDataIni.Date = 0, TPDataIni, 'Defina a data de in�cio!') then Exit;
    if MyObjects.FIC(TPDataFim.Date = 0, TPDataFim, 'Defina a data de t�rmino!') then Exit;
    //
    if MyObjects.FIC(TPDataFim.Date < TPDataIni.Date, TPDataFim,
      'A data de t�rmino deve ser igual ou superior a data de in�cio!') then Exit;
    //
    Agenda     := EdAgenda.ValueVariant;
    DataIni    := TPDataIni.Date;
    DataFim    := TPDataFim.Date;
    DiaTodo    := Geral.BoolToInt(CkDiaTodo.Checked);
    Nome       := EdNome.ValueVariant;
    Descri     := MeDescri.Text;
    Endereco   := EdEndereco.ValueVariant;
    Empresa    := 0;
    Duracao    := 0;
    Respons    := 0;
    Status     := 0;
    Prioridade := 0;
    //
    if DiaTodo = 1 then
    begin
      HoraIni := '00:00';
      HoraFim := '00:00';
    end else
    begin
      HoraIni := EdHoraIni.Text;
      HoraFim := EdHoraFim.Text;
      //
      if MyObjects.FIC(StrToTime(HoraFim + ':00') <= StrToTime(HoraIni + ':00'),
        EdHoraFim, 'A hora de t�rmino deve ser superior a hora de in�cio!') then Exit;
    end;
    //
    if MyObjects.FIC(Agenda = 0, EdAgenda, 'Defina a agenda!') then Exit;
    if MyObjects.FIC(Nome = '', EdNome, 'Defina o nome!') then Exit;
  end else
  begin //Tarefas
    if MyObjects.FIC(TPTarDataIni.Date = 0, TPTarDataIni, 'Defina a data de in�cio!') then Exit;
    if MyObjects.FIC(TPTarDataFim.Date = 0, TPTarDataFim, 'Defina a data de t�rmino!') then Exit;
    //
    if MyObjects.FIC(TPTarDataFim.Date < TPTarDataIni.Date, TPTarDataFim,
      'A data de t�rmino deve ser igual ou superior a data de in�cio!') then Exit;
    //
    Agenda     := Integer(istATTarefas);
    DataIni    := TPTarDataIni.Date;
    DataFim    := TPTarDataFim.Date;
    HoraIni    := '00:00';
    HoraFim    := '00:00';
    DiaTodo    := 0;
    Nome       := EdTarNome.ValueVariant;
    Descri     := MeTarDescri.Text;
    Endereco   := '';
    Empresa    := VUTarEmpresa.ValueVariant;
    Prioridade := CBTarPrioridade.ItemIndex;
    Status     := CBTarStatus.ItemIndex;
    Duracao    := EdTarDuracao.ValueVariant;
    Respons    := EdTarRespons.ValueVariant;
    //
    if MyObjects.FIC(EdTarEmpresa.ValueVariant = 0, EdTarEmpresa, 'Empresa n�o definida!') then Exit;
    if MyObjects.FIC(Nome = '', EdTarNome, 'Defina o nome!') then Exit;
    if MyObjects.FIC(Prioridade < 0, CBTarPrioridade, 'Defina a prioridade!') then Exit;
    if MyObjects.FIC(Status < 0, CBTarStatus, 'Defina a prioridade!') then Exit;
    if MyObjects.FIC(Respons = 0, EdTarRespons, 'Defina o respons�vel!') then Exit;
  end;
  //
  Cor := QrAgendasCor.Value;
  //
  DataHoraIniTZ_Txt  := Geral.FDT(DataIni, 1) + ' ' + HoraIni + ':00';
  DataHoraIniUTC     := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataHoraIniTZ_Txt, FTimeZoneDifUTC, '+00:00');
  DataHoraIniTZ      := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataHoraIniTZ_Txt, FTimeZoneDifUTC, FTimeZoneDifUTC);
  DataHoraIniUTC_Txt := Geral.FDT(DataHoraIniUTC, 9);
  DataHoraFimTZ_Txt  := Geral.FDT(DataFim, 1) + ' ' + HoraFim + ':00';
  DataHoraFimUTC     := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataHoraFimTZ_Txt, FTimeZoneDifUTC, '+00:00');
  DataHoraFimTZ      := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataHoraIniTZ_Txt, FTimeZoneDifUTC, FTimeZoneDifUTC);
  DataHoraFimUTC_Txt := Geral.FDT(DataHoraFimUTC, 9);
  //
  if FControle = 0 then
  begin
    SQLTipo   := stIns;
    SQLTipo2  := stIns;
    FControle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'agendasits', 'Controle',
                   [], [], stIns, 0, siPositivo, nil);
  end else
  begin
    if FTodos = True then
    begin
      SQLTipo   := stUpd;
      SQLTipo2  := stUpd;
    end else
    begin
      SQLTipo    := stIns;
      SQLTipo2   := stUpd;
      ControleDB := FControle;
      FControle  := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'agendasits', 'Controle',
                     [], [], stIns, 0, siPositivo, nil);
    end;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'agendasits', False,
    ['Nome', 'Descri', 'Endereco', 'DataIni', 'DataFim',
    'DiaTodo', 'Recorrencia', 'Empresa', 'Prioridade', 'Duracao',
    'Respons', 'Status', 'Codigo'], ['Controle'],
    [Nome, Descri, Endereco, DataHoraIniUTC_Txt, DataHoraFimUTC_Txt,
    DiaTodo, FRecorrencia, Empresa, Prioridade, Duracao,
    Respons, Status, Agenda], [FControle], True) then
  begin
    if (SQLTipo2 = stUpd) and (FTodos = False) then
    begin
      UnAgenda.CriaExcecao(ControleDB, FRecorrenciaDB, FDataIniDB, Dmod.MyDB);
    end;
    //
    VAR_CADASTRO := FControle;
    //
    if TFmAgendaIts(Self).Owner is TApplication then
      Close
    else
      MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
  end;
end;

procedure TFmAgendaIts.BtRepeticoesClick(Sender: TObject);
begin
  MostraRecorrencia();
end;

procedure TFmAgendaIts.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if TFmAgendaIts(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmAgendaIts.BtTarRepeticoesClick(Sender: TObject);
begin
  MostraRecorrencia();
end;

procedure TFmAgendaIts.CkDiaTodoClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := CkDiaTodo.Checked = False;
  //
  LaHoraIni.Visible := Visi;
  EdHoraIni.Visible := Visi;
  LaHoraFim.Visible := Visi;
  EdHoraFim.Visible := Visi;
end;

procedure TFmAgendaIts.ConfiguraTimeZone(Empresa: Integer);
begin
  DmodG.ReopenParamsEmp(Empresa);
  //
  FTimeZoneDifUTC := dmkPF.TZD_UTC_FloatToSignedStr(DmodG.QrParamsEmpTZD_UTC.Value);
end;

procedure TFmAgendaIts.EdAgendaChange(Sender: TObject);
begin
  ConfiguraTimeZone(QrAgendasEmpresa.Value);
end;

procedure TFmAgendaIts.EdHoraIniExit(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    EdHoraFim.ValueVariant := Geral.STT(EdHoraIni.Text, '00:30:00', False);
end;

procedure TFmAgendaIts.FormActivate(Sender: TObject);
begin
  if TFmAgendaIts(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmAgendaIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FRecorrencia    := '';
  FTimeZoneDifUTC := '+00:00';
  //
  ReopenAgendas();
  UMyMod.AbreQuery(QrUsuarios, Dmod.MyDB);
  //
  //UnAgendaAux.ConfiguraRecorrencia(RERecorrencia);
end;

procedure TFmAgendaIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgendaIts.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmAgendaIts.MostraEdicao(SQLType: TSQLType; AgendaTipo: TAgendaTipo;
  Codigo, Controle: Integer; DataPadrao: TDateTime; Todos: Boolean);
var
  Qry: TMySQLQuery;
  AgoraTZ, DataIniTZ, DataFimTZ: TDateTime;
  AgoraTZ_Txt, DataIniTZ_Txt, DataFimTZ_Txt: String;
  DiaTodo: Boolean;
  Usuario: Integer;
begin
  ImgTipo.SQLType := SQLType;
  FTodos          := Todos;
  //
  if AgendaTipo = istATTarefas then
  begin
    TSCompromissos.TabVisible := False;
    TSTarefas.TabVisible      := True;
    //
    CBTarPrioridade.Items.Clear;
    CBTarPrioridade.Items.AddStrings(GrlAgenda.ObtemListaPrioridades);
    //
    CBTarStatus.Items.Clear;
    CBTarStatus.Items.AddStrings(GrlAgenda.ObtemListaStatus);
    //
    if SQLType = stIns then
    begin
      if VAR_USUARIO > 0 then
        Usuario := VAR_USUARIO
      else
        Usuario := 0;
      //
      FControle                 := 0;
      EdTarEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
      CBTarEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
      EdTarNome.ValueVariant    := '';
      TPTarDataIni.Date         := DmodG.ObtemAgora();
      TPTarDataFim.Date         := DmodG.ObtemAgora();
      MeTarDescri.Text          := '';
      CBTarPrioridade.ItemIndex := 0;
      EdTarDuracao.ValueVariant := 0;
      CBTarStatus.ItemIndex     := 0;
      EdTarRespons.ValueVariant := Usuario;
      CBTarRespons.KeyValue     := Usuario;
      FRecorrencia              := '';
    end else
    begin
      Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT its.*, age.Empresa ',
          'FROM agendasits its ',
          'LEFT JOIN agencab age ON age.Codigo = its.Codigo ',
          'WHERE its.Controle=' + Geral.FF0(Controle),
          'AND age.Codigo=' + Geral.FF0(Codigo),
          '']);
        if Qry.RecordCount > 0 then
        begin
          ConfiguraTimeZone(Qry.FieldByName('Empresa').AsInteger);
          //
          DataIniTZ_Txt := Geral.FDT(Qry.FieldByName('DataIni').AsDateTime, 9);
          DataIniTZ     := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataIniTZ_Txt, '+00:00', FTimeZoneDifUTC);
          DataFimTZ_Txt := Geral.FDT(Qry.FieldByName('DataFim').AsDateTime, 9);
          DataFimTZ     := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataFimTZ_Txt, '+00:00', FTimeZoneDifUTC);
          //
          FControle                 := Qry.FieldByName('Controle').AsInteger;
          VUTarEmpresa.ValueVariant := Qry.FieldByName('Empresa').AsInteger;
          EdTarNome.ValueVariant    := Qry.FieldByName('Nome').AsString;
          TPTarDataIni.Date         := DataIniTZ;
          TPTarDataFim.Date         := DataFimTZ;
          MeTarDescri.Text          := Qry.FieldByName('Descri').AsString;
          CBTarPrioridade.ItemIndex := Qry.FieldByName('Prioridade').AsInteger;
          EdTarDuracao.ValueVariant := Qry.FieldByName('Duracao').AsFloat;
          CBTarStatus.ItemIndex     := Qry.FieldByName('Status').AsInteger;
          EdTarRespons.ValueVariant := Qry.FieldByName('Respons').AsInteger;
          CBTarRespons.KeyValue     := Qry.FieldByName('Respons').AsInteger;
          FRecorrenciaDB            := Qry.FieldByName('Recorrencia').AsString;
          FDataIniDB                := Qry.FieldByName('DataIni').AsDateTime;
          //
          if FTodos = False then
            FRecorrencia := ''
          else
            FRecorrencia := Qry.FieldByName('Recorrencia').AsString;
        end;
      finally
        Qry.Free;
      end;
    end;
  end else
  begin
    TSCompromissos.TabVisible := True;
    TSTarefas.TabVisible      := False;
    //
    if SQLType = stIns then
    begin
      FControle := 0;
      //
      if Codigo > 0 then
      begin
        EdAgenda.ValueVariant := Codigo;
        CBAgenda.KeyValue     := Codigo;
        //
        ConfiguraTimeZone(QrAgendasEmpresa.Value);
        //
        AgoraTZ_Txt := Geral.FDT(DmodG.ObtemAgora(True), 9);
        AgoraTZ     := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, AgoraTZ_Txt, '+00:00', FTimeZoneDifUTC);
        //
        if DataPadrao <> 0 then
        begin
          TPDataIni.Date := DataPadrao;
          TPDataFim.Date := DataPadrao;
        end else
        begin
          TPDataIni.Date := AgoraTZ;
          TPDataFim.Date := AgoraTZ;
        end;
        EdHoraIni.Text         := Geral.FDT(AgoraTZ, 102);
        EdHoraFim.ValueVariant := Geral.STT(EdHoraIni.Text, '00:30:00', False);
      end else
      begin
        EdAgenda.ValueVariant := 0;
        CBAgenda.KeyValue     := Null;
        EdHoraIni.Text        := '00:00';
        EdHoraFim.Text        := '00:00';
        //
        if DataPadrao <> 0 then
        begin
          TPDataIni.Date := DataPadrao;
          TPDataFim.Date := DataPadrao;
        end else
        begin
          TPDataIni.Date := Date;
          TPDataFim.Date := Date;
        end;
      end;
      CkDiaTodo.Checked       := False;
      EdNome.ValueVariant     := '';
      MeDescri.Text           := '';
      EdEndereco.ValueVariant := '';
      FRecorrencia            := '';
    end else
    begin
      Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT its.*, age.Empresa ',
          'FROM agendasits its ',
          'LEFT JOIN agencab age ON age.Codigo = its.Codigo ',
          'WHERE its.Controle=' + Geral.FF0(Controle),
          'AND age.Codigo > 0 ',
          '']);
        if Qry.RecordCount > 0 then
        begin
          ConfiguraTimeZone(Qry.FieldByName('Empresa').AsInteger);
          //
          DataIniTZ_Txt := Geral.FDT(Qry.FieldByName('DataIni').AsDateTime, 9);
          DataIniTZ     := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataIniTZ_Txt, '+00:00', FTimeZoneDifUTC);
          DataFimTZ_Txt := Geral.FDT(Qry.FieldByName('DataFim').AsDateTime, 9);
          DataFimTZ     := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDB, QrLoc, DataFimTZ_Txt, '+00:00', FTimeZoneDifUTC);
          //
          FControle               := Qry.FieldByName('Controle').AsInteger;
          EdAgenda.ValueVariant   := Qry.FieldByName('Codigo').AsInteger;
          CBAgenda.KeyValue       := Qry.FieldByName('Codigo').AsInteger;
          CkDiaTodo.Checked       := Geral.IntToBool(Qry.FieldByName('DiaTodo').AsInteger);
          TPDataIni.Date          := DataIniTZ;
          EdHoraIni.Text          := Geral.FDT(DataIniTZ, 102);
          TPDataFim.Date          := DataFimTZ;
          EdHoraFim.Text          := Geral.FDT(DataFimTZ, 102);
          EdNome.ValueVariant     := Qry.FieldByName('Nome').AsString;
          MeDescri.Text           := Qry.FieldByName('Descri').AsString;
          EdEndereco.ValueVariant := Qry.FieldByName('Endereco').AsString;
          FRecorrenciaDB          := Qry.FieldByName('Recorrencia').AsString;
          FDataIniDB              := Qry.FieldByName('DataIni').AsDateTime;
          //
          if FTodos = False then
            FRecorrencia := ''
          else
            FRecorrencia := Qry.FieldByName('Recorrencia').AsString;
        end else
        begin
          Geral.MB_Erro('Falha ao obter dados!');
          Close;
        end;
      finally
        Qry.Free;
      end;
    end;
    DiaTodo := CkDiaTodo.Checked = False;
    //
    LaHoraIni.Visible := DiaTodo;
    EdHoraIni.Visible := DiaTodo;
    LaHoraFim.Visible := DiaTodo;
    EdHoraFim.Visible := DiaTodo;
  end;
end;

procedure TFmAgendaIts.MostraRecorrencia;
begin
  (*
  RERecorrencia.Recurrency := FRecorrencia;
  //
  if RERecorrencia.Execute then
    FRecorrencia := RERecorrencia.Recurrency;
  *)
end;

procedure TFmAgendaIts.ReopenAgendas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgendas, Dmod.MyDB, [
    'SELECT Codigo, Nome, Empresa, Cor ',
    'FROM agencab ',
    'WHERE Codigo > 0 ',
    'ORDER BY Codigo ',
    '']);
end;

procedure TFmAgendaIts.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2, True);
  //
  if VAR_CADASTRO <> 0 then
  begin
    EdEndereco.ValueVariant := GOTOy.EnderecoDeEntidade2(VAR_CADASTRO, 0);
    EdEndereco.SetFocus;
  end;
end;

procedure TFmAgendaIts.TPDataIniExit(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    TPDataFim.Date := TPDataIni.Date;
end;

end.
