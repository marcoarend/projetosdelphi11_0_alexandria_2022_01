unit UnitAgendaJan;

interface

uses
  System.DateUtils, StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt,
  UnInternalConsts, Db, DbCtrls, Variants, mySQLDbTables, UnDmkEnums,
  UnDmkProcFunc;

type
  TUnitAgendaJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormAgenTarGer(Form: TForm; AbrirEmAba: Boolean;
              InOwner: TWincontrol; Pager: TWinControl; Controle: Integer);
    function  MostraFormAgenTarIts(Codigo, Controle: Integer; OriID: Integer = 0;
              OriNum: Double = 0; Descri: String = ''; Prioridade: Integer = -1;
              DataCad: TDate = 0): Integer;
    procedure MostraFormAgenTarCab(Codigo: Integer);
  end;

var
  UnAgendaJan: TUnitAgendaJan;

implementation

uses Module, UnMyObjects, DmkDAC_PF, UnDmkImg, MyDBCheck, AgenTarGer,
  AgenTarCab, AgenTarIts, UnGrlUsuarios;

{ TUnitAgendaJan }

procedure TUnitAgendaJan.MostraFormAgenTarCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgenTarCab, FmAgenTarCab, afmoNegarComAviso) then
  begin
    FmAgenTarCab.FCodigo := Codigo;
    FmAgenTarCab.ShowModal;
    FmAgenTarCab.Destroy;
  end;
end;

function TUnitAgendaJan.MostraFormAgenTarIts(Codigo, Controle: Integer;
  OriID: Integer = 0; OriNum: Double = 0; Descri: String = '';
  Prioridade: Integer = -1; DataCad: TDate = 0): Integer;
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmAgenTarIts, FmAgenTarIts, afmoNegarComAviso) then
  begin
    FmAgenTarIts.FCodigo     := Codigo;
    FmAgenTarIts.FControle   := Controle;
    FmAgenTarIts.FOriID      := OriID;
    FmAgenTarIts.FOriNum     := OriNum;
    FmAgenTarIts.FDescri     := Descri;
    FmAgenTarIts.FPrioridade := Prioridade;
    FmAgenTarIts.FDataCad    := DataCad;
    FmAgenTarIts.ShowModal;
    FmAgenTarIts.Destroy;
  end;
  Result := VAR_CADASTRO;
end;

procedure TUnitAgendaJan.MostraFormAgenTarGer(Form: TForm; AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; Controle: Integer);
var
  Frm: TForm;
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if AbrirEmAba then
    begin
      Frm := MyObjects.FormTDICria(TFmAgenTarGer, InOwner, Pager, True, True);
      //
      (*
      if Controle <> 0 then
        TFmAgenTarGer(Frm).LocalizaControle(Controle);
      *)
    end else
    begin
      if DBCheck.CriaFm(TFmAgenTarGer, FmAgenTarGer, afmoNegarComAviso) then
      begin
        (*
        if Controle <> 0 then
          FmAgenTarGer.LocalizaControle(Controle);
        *)
        FmAgenTarGer.ShowModal;
        FmAgenTarGer.Destroy;
      end;
    end;
  end;
end;

end.
