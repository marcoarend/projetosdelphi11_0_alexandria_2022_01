object FmAgenTarGer: TFmAgenTarGer
  Left = 339
  Top = 185
  Caption = 'AGE-TAREF-001 :: Gerenciamento de Tarefas'
  ClientHeight = 774
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 881
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 376
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Tarefas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 376
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Tarefas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 376
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Tarefas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 999
    Height = 629
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 999
      Height = 629
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 999
        Height = 629
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnAgendas: TPanel
          Left = 2
          Top = 18
          Width = 329
          Height = 609
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 137
            Width = 329
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 0
            object BtLstExclui: TBitBtn
              Tag = 12
              Left = 107
              Top = 5
              Width = 50
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtLstExcluiClick
            end
            object BtLstAltera: TBitBtn
              Tag = 11
              Left = 56
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtLstAlteraClick
            end
            object BtLstInclui: TBitBtn
              Tag = 10
              Left = 5
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtLstIncluiClick
            end
            object BtLstReabre: TBitBtn
              Tag = 18
              Left = 209
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BtLstReabreClick
            end
            object BtLstReordena: TBitBtn
              Tag = 73
              Left = 158
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 4
              OnClick = BtLstReordenaClick
            end
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 117
            Width = 329
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Listas'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            ExplicitWidth = 45
          end
          object LVListas: TListView
            Left = 0
            Top = 196
            Width = 329
            Height = 413
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <>
            Items.ItemData = {
              054C0000000200000000000000FFFFFFFFFFFFFFFF00000000FFFFFFFF000000
              000654006500730074006500310000000000FFFFFFFFFFFFFFFF00000000FFFF
              FFFF0000000006540065007300740065003200}
            RowSelect = True
            TabOrder = 2
            TabStop = False
            ViewStyle = vsReport
            OnSelectItem = LVListasSelectItem
            ExplicitLeft = -7
            ExplicitTop = 199
          end
          object PnPesquisa: TPanel
            Left = 0
            Top = 0
            Width = 329
            Height = 117
            Align = alTop
            TabOrder = 3
            object Label4: TLabel
              Left = 5
              Top = 5
              Width = 57
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Entidade:'
            end
            object RGAtivo: TRadioGroup
              Left = 1
              Top = 57
              Width = 327
              Height = 59
              Align = alBottom
              Caption = 'Status'
              Columns = 3
              ItemIndex = 1
              Items.Strings = (
                'Inativos'
                'Ativos'
                'Ambos')
              TabOrder = 0
              OnClick = RGAtivoClick
            end
            object EdEmpresa: TdmkEditCB
              Left = 5
              Top = 25
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmpresaChange
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 75
              Top = 25
              Width = 240
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DsEmpresas
              TabOrder = 1
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
        object TBTarefas: TTabControl
          Left = 331
          Top = 18
          Width = 666
          Height = 609
          Align = alClient
          MultiLine = True
          TabHeight = 25
          TabOrder = 1
          TabPosition = tpLeft
          Tabs.Strings = (
            'A fazer'
            'Completadas'
            'Todas')
          TabIndex = 0
          OnChange = TBTarefasChange
          object PnLista: TPanel
            Left = 29
            Top = 4
            Width = 633
            Height = 59
            Align = alTop
            TabOrder = 0
            object BtInclui: TBitBtn
              Tag = 10
              Left = 6
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtIncluiClick
            end
            object BtAltera: TBitBtn
              Tag = 11
              Left = 56
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtAlteraClick
            end
            object BtExclui: TBitBtn
              Tag = 12
              Left = 105
              Top = 5
              Width = 50
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtExcluiClick
            end
            object PnData: TPanel
              Left = 438
              Top = 1
              Width = 194
              Height = 57
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 3
              object Label1: TLabel
                Left = 5
                Top = 5
                Width = 32
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Data:'
              end
              object TPData: TdmkEditDateTimePicker
                Left = 5
                Top = 25
                Width = 130
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 39615.655720300900000000
                Time = 39615.655720300900000000
                TabOrder = 0
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'Data'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object BtReabre: TBitBtn
                Tag = 18
                Left = 140
                Top = 5
                Width = 49
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtReabreClick
              end
            end
          end
          object STTarefas: TStaticText
            Left = 29
            Top = 63
            Width = 633
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Tarefas'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            ExplicitWidth = 58
          end
          object STTarefasTot: TStaticText
            Left = 29
            Top = 485
            Width = 633
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Total de tens:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            ExplicitWidth = 99
          end
          object PnDescri: TPanel
            Left = 29
            Top = 505
            Width = 633
            Height = 100
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 2
            object MeDescri: TMemo
              Left = 0
              Top = 0
              Width = 633
              Height = 100
              Align = alClient
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 999
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 820
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 818
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 12
        Width = 818
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 18
          Width = 814
          Height = 34
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 16
            Top = 2
            Width = 150
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 15
            Top = 1
            Width = 150
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 460
    Top = 443
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 488
    Top = 443
  end
  object QrAgenTarCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 128
    Top = 371
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    Left = 55
    Top = 306
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresasFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresasNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 84
    Top = 306
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 128
    Top = 499
  end
  object QrAgenTarIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 220
    Top = 371
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 516
    Top = 443
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 520
    Top = 272
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Localizarorigem1: TMenuItem
      Caption = '&Localizar origem'
      OnClick = Localizarorigem1Click
    end
  end
end
