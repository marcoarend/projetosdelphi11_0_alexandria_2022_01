unit AgenTarCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkEdit,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, mySQLDbTables,
  dmkImage, DmkDAC_PF, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, UnitAgendaAux;

type
  TFmAgenTarCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label12: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VUEmpresa: TdmkValUsu;
    CkSincro: TCheckBox;
    MeDescri: TMemo;
    Label4: TLabel;
    CkAtivo: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType; Codigo: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmAgenTarCab: TFmAgenTarCab;

implementation

uses UnMyObjects, Module, UnDmkImg, UMySQLModule, ModuleGeral, UnitAgenda,
  UnGrlAgenda;

{$R *.DFM}

procedure TFmAgenTarCab.BtOKClick(Sender: TObject);
var
  Empresa, Sincro, Ativo: Integer;
  Nome, Descri: String;
begin
  Nome    := EdNome.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  Descri  := MeDescri.Text;
  Sincro  := Geral.BoolToInt(CkSincro.Checked);
  Ativo   := Geral.BoolToInt(CkAtivo.Checked);
  //
  if Empresa <> 0 then
    Empresa := VUEmpresa.ValueVariant;
  //
  if GrlAgenda.InsUpdTarefasLista(Dmod.MyDB, Dmod.QrUpd, stDesktop, Nome,
    Descri, Empresa, Sincro, Ativo, FCodigo, EdNome, EdEmpresa) then
  begin
    VAR_CADASTRO := FCodigo;
    Close;
  end;
end;

procedure TFmAgenTarCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Close;
end;

procedure TFmAgenTarCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAgenTarCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCodigo := 0;
end;

procedure TFmAgenTarCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgenTarCab.FormShow(Sender: TObject);
begin
  if FCodigo = 0 then
    MostraEdicao(stIns, 0)
  else
    MostraEdicao(stUpd, FCodigo);
end;

procedure TFmAgenTarCab.MostraEdicao(SQLType: TSQLType; Codigo: Integer);
var
  Qry: TMySQLQuery;
begin
  ImgTipo.SQLType := SQLType;
  //
  if SQLType = stIns then
  begin
    FCodigo                := 0;
    EdNome.ValueVariant    := '';
    EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    MeDescri.Text          := '';
    CkSincro.Checked       := True;
    CkAtivo.Checked        := True;
  end else
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM agentarcab ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      if Qry.RecordCount > 0 then
      begin
        FCodigo                := Qry.FieldByName('Codigo').AsInteger;
        EdNome.ValueVariant    := Qry.FieldByName('Nome').AsString;
        VUEmpresa.ValueVariant := Qry.FieldByName('Empresa').AsInteger;
        MeDescri.Text          := Qry.FieldByName('Descri').AsString;
        CkSincro.Checked       := Geral.IntToBool(Qry.FieldByName('Sincro').Value);
        CkAtivo.Checked        := Geral.IntToBool(Qry.FieldByName('Ativo').Value);
      end else
      begin
        Geral.MB_Erro('Falha ao obter dados!');
        Close;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

end.
