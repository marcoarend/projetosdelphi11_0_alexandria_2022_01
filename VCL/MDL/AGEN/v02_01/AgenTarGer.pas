unit AgenTarGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkCompoStore, dmkPermissoes,
  mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkValUsu,
  dmkEditDateTimePicker, UnDmkProcFunc, Vcl.Menus, frxDock;

type
  TFmAgenTarGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnAgendas: TPanel;
    Panel5: TPanel;
    BtLstExclui: TBitBtn;
    BtLstAltera: TBitBtn;
    BtLstInclui: TBitBtn;
    BtLstReabre: TBitBtn;
    StaticText1: TStaticText;
    BtLstReordena: TBitBtn;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    LVListas: TListView;
    QrAgenTarCab: TmySQLQuery;
    PnPesquisa: TPanel;
    RGAtivo: TRadioGroup;
    QrEmpresas: TmySQLQuery;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasFilial: TIntegerField;
    QrEmpresasNOMEFILIAL: TWideStringField;
    DsEmpresas: TDataSource;
    QrLoc: TmySQLQuery;
    TBTarefas: TTabControl;
    PnLista: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    STTarefas: TStaticText;
    PnData: TPanel;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    QrAgenTarIts: TmySQLQuery;
    STTarefasTot: TStaticText;
    BtReabre: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    PMMenu: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    N1: TMenuItem;
    Localizarorigem1: TMenuItem;
    PnDescri: TPanel;
    MeDescri: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLstIncluiClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtLstAlteraClick(Sender: TObject);
    procedure BtLstReabreClick(Sender: TObject);
    procedure BtLstReordenaClick(Sender: TObject);
    procedure RGAtivoClick(Sender: TObject);
    procedure BtLstExcluiClick(Sender: TObject);
    procedure LVListasSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure TBTarefasChange(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure TDLTarefasEditDone(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure TDLTarefasCompleteClick(Sender: TObject; ItemIndex: Integer);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    //procedure TDLTarefasItemRightClick(Sender: TObject; ATodoItem: TTodoItem);
    procedure Localizarorigem1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    //procedure TDLTarefasItemSelect(Sender: TObject; ATodoItem: TTodoItem);
  private
    { Private declarations }
    FReabre: Boolean;
    procedure MostraEdicao(Acao: Integer; ListaCod: Integer; ListaTxt: String);
    procedure ConfiguraEmpresa();
    procedure ReordenaLista;
    procedure ReopenAgenTarGer(AgenTarCab: Integer = 0);
    //procedure ReopenAgenTarIts(Codigo: Integer);
    //procedure MostraFormAgenTarIts(SQLTipo: TSQLType);
    procedure MostraFormAgenTarCab(SQLTipo: TSQLType);
    procedure ExcluiLista();
    //procedure ExcluiItem(Codigo: Integer);
    //procedure AtualizaItem();
    //procedure LocalizaOrigem();
    //procedure ConfiguraDescricao(Item: TTodoItem);
  public
    { Public declarations }
    //procedure LocalizaControle(Controle: Integer);
  end;

  var
  FmAgenTarGer: TFmAgenTarGer;

implementation

uses MyGlyfs, Principal, UnMyObjects, Module, ModuleGeral, UnitAgendaJan,
  UnitAgenda, UnGrlAgenda, DmkDAC_PF, UnReordena, UnGrl_Vars, UnAppPF;

{$R *.DFM}

procedure TFmAgenTarGer.BtLstReordenaClick(Sender: TObject);
begin
  ReordenaLista;
end;

procedure TFmAgenTarGer.BtReabreClick(Sender: TObject);
begin
  //ReopenAgenTarIts(STTarefas.Tag);
end;

procedure TFmAgenTarGer.Altera1Click(Sender: TObject);
begin
  //MostraFormAgenTarIts(stUpd);
end;

(*
procedure TFmAgenTarGer.AtualizaItem;
var
  Descri: String;
  Realizado, Finalizado, Status, Controle: Integer;
  DataFim: TDate;
  Sel: TTodoItem;
begin
  Sel := TDLTarefas.Selected;
  //
  if Sel <> nil then
  begin
    Descri     := Sel.Notes.Text;
    Realizado  := Sel.Completion;
    Finalizado := Geral.BoolToInt(Sel.Complete);
    Controle   := Sel.Tag;
    //
    if Finalizado = 1 then
    begin
      DataFim := Date;
      Status  := UnAgenda.StatusToInt(tsDeferred);
    end else
    begin
      DataFim := 0;
      Status  := UnAgenda.StatusToInt(Sel.Status);
    end;
    //
    if GrlAgenda.InsUpdTarefasItemParcial(Dmod.MyDB, Dmod.QrUpd, stDesktop,
      Descri, DataFim, Realizado, Finalizado, Status, Controle) then
    begin
      Sel.Status         := UnAgenda.IntToStatus(Status);
      Sel.CompletionDate := DataFim;
    end;
    if (FReabre) and (STTarefas.Tag <> 0) then
      ReopenAgenTarIts(STTarefas.Tag);
    FReabre := False;
  end;
end;
*)

procedure TFmAgenTarGer.BtAlteraClick(Sender: TObject);
begin
  //MostraFormAgenTarIts(stUpd);
end;

procedure TFmAgenTarGer.BtExcluiClick(Sender: TObject);
begin
  //ExcluiItem(STTarefas.Tag);
end;

procedure TFmAgenTarGer.BtIncluiClick(Sender: TObject);
begin
  //MostraFormAgenTarIts(stIns);
end;

procedure TFmAgenTarGer.BtLstAlteraClick(Sender: TObject);
begin
  MostraFormAgenTarCab(stUpd);
end;

procedure TFmAgenTarGer.BtLstExcluiClick(Sender: TObject);
begin
  ExcluiLista;
end;

procedure TFmAgenTarGer.BtLstIncluiClick(Sender: TObject);
begin
  MostraFormAgenTarCab(stIns);
end;

procedure TFmAgenTarGer.BtLstReabreClick(Sender: TObject);
begin
  ReopenAgenTarGer;
end;

procedure TFmAgenTarGer.BtSaidaClick(Sender: TObject);
begin
  if TFmAgenTarGer(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmAgenTarGer.ConfiguraEmpresa;
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  if Empresa <> 0 then
    Empresa := VUEmpresa.ValueVariant;
  //
  VAR_WEB_USR_ENT := Empresa;
end;

procedure TFmAgenTarGer.EdEmpresaChange(Sender: TObject);
begin
  ConfiguraEmpresa;
  ReopenAgenTarGer;
end;

procedure TFmAgenTarGer.Exclui1Click(Sender: TObject);
begin
  //ExcluiItem(STTarefas.Tag);
end;

(*
procedure TFmAgenTarGer.ExcluiItem(Codigo: Integer);
var
  Msg: String;
  Controle: Integer;
  Item: TTodoItem;
begin
  Item := TDLTarefas.Selected;
  //
  if Item <> nil then
  begin
    //Codigo   := STTarefasTot.Tag;
    Controle := Item.Tag;
    Msg      := 'Confirma a exclus�o do item selecionado?';
    //
    if Controle <> 0 then
    begin
      if GrlAgenda.ExcluiTarefasItem(Dmod.MyDB, QrLoc, Controle, Msg) then
        ReopenAgenTarIts(Codigo)
      else
        Geral.MB_Erro('Falha ao excluir item!');
    end else
      Geral.MB_Aviso('Este item n�o pode ser exclu�do!');
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;
*)

procedure TFmAgenTarGer.ExcluiLista;
var
  Nome, Msg: String;
  Codigo: Integer;
  Item: TListItem;
begin
  Codigo := STTarefas.Tag;
  Nome   := STTarefas.Caption;
  Msg    := 'Confirma a exclus�o do item: "' + Nome + '"?';
  //
  if Codigo > 1 then
  begin
    if GrlAgenda.ExcluiTarefasLista(Dmod.MyDB, QrLoc, Codigo, Msg) then
      ReopenAgenTarGer
    else
      Geral.MB_Erro('Falha ao excluir lista!');
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado ou esta lista n�o pode ser exclu�da!');
end;

procedure TFmAgenTarGer.FormActivate(Sender: TObject);
begin
  if TFmAgenTarGer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmAgenTarGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType      := stLok;
  FReabre              := False;
  //TDLTarefas.PopupMenu := PMMenu;
  //
  MostraEdicao(0, 0, '');
  MostraEdicao(1, 0, '');
end;

procedure TFmAgenTarGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgenTarGer.FormShow(Sender: TObject);
begin
  {$IfNDef cSkinRank} //Berlin
  {$IfNDef cAlphaSkin} //Berlin
    FmMyGlyfs.DefineGlyfs(TForm(Sender));
  {$EndIf}
  {$EndIf}
  {$IfDef cSkinRank} //Berlin
    if FmPrincipal.Sd1.Active then
      FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  {$EndIf}
  {$IfDef cAlphaSkin} //Berlin
    if FmPrincipal.sSkinManager1.Active then
      FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
  {$EndIf}
  //
  LVListas.Groups.BeginUpdate;
  try
    UnAgenda.InsereGrupos(LVListas);
  finally
    LVListas.Groups.EndUpdate;
  end;
end;

procedure TFmAgenTarGer.Inclui1Click(Sender: TObject);
begin
  //MostraFormAgenTarIts(stIns);
end;

(*
procedure TFmAgenTarGer.LocalizaControle(Controle: Integer);
var
  TDItem: TTodoItem;
  Item: TListItem;
  Codigo, Idx: Integer;
begin
  Codigo := UnAgenda.ObtemCodigoGrupo(Dmod.MyDB, Controle);
  Idx    := UnAgenda.LocalizaGrupo(LVListas, Codigo);
  //
  if Idx <> -1 then
  begin
    Item := LVListas.Items[Idx];
    //
    if Item <> nil then
    begin
      Item.Selected := True;
      MostraEdicao(1, Item.StateIndex, Item.Caption);
      ReopenAgenTarIts(Item.StateIndex);
      //
      Idx := UnAgenda.LocalizaItem(TDLTarefas, Controle);
      //
      if Idx <> -1 then
      begin
        TDItem := TDLTarefas.Items[Idx];
        //
        if TDItem <> nil then
          TDItem.Select;
      end;
    end;
  end;
end;
*)

procedure TFmAgenTarGer.Localizarorigem1Click(Sender: TObject);
begin
  //LocalizaOrigem();
end;

(*
procedure TFmAgenTarGer.LocalizaOrigem();
var
  Sel: TTodoItem;
  Controle, OriID: Integer;
  OriNum: Double;
begin
  Sel := TDLTarefas.Selected;
  //
  if Sel <> nil then
  begin
    Controle := Sel.Tag;
    //
    if Controle <> 0 then
    begin
      if QrAgenTarIts.Locate('Controle', Controle, []) then
      begin
        OriID  := QrAgenTarIts.FieldByName('OriID').AsInteger;
        OriNum := QrAgenTarIts.FieldByName('OriNum').AsFloat;
        //
        AppPF.AGEN_MostraFormOrigem(OriID, OriNum);
      end;
    end;
  end;
end;
*)

procedure TFmAgenTarGer.LVListasSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  if (Item <> nil) and (Selected = True) then
  begin
    MostraEdicao(1, Item.StateIndex, Item.Caption);
    //ReopenAgenTarIts(Item.StateIndex);
  end;
end;

procedure TFmAgenTarGer.MostraEdicao(Acao: Integer; ListaCod: Integer;
  ListaTxt: String);
begin
  if Acao = 0 then //Listas
  begin
    UnAgenda.ConfiguraListView(LVListas);
    //UnAgenda.ConfiguraToDoList(TDLTarefas);
    //
    DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa, '', False, QrEmpresas);
    //
    CBEmpresa.ListSource := DsEmpresas;
    LVListas.SmallImages := FmMyGlyfs.Lista_32X32_Textos;
    RGAtivo.ItemIndex    := 1;
    //
    EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    //
    STTarefas.Caption := 'Listas';
    STTarefas.Tag     := 0;
  end else
  begin //Tarefas
    //ConfiguraDescricao(nil);
    //
    if ListaTxt = '' then
    begin
      TBTarefas.Visible := False;
    end else
    begin
      if ListaCod = 0 then
      begin
        PnData.Visible := True;
        TpData.Date    := Date;
      end else
        PnData.Visible := False;
      //
      TBTarefas.Visible  := True;
      TBTarefas.TabIndex := 0;
      STTarefas.Caption  := ListaTxt;
      STTarefas.Tag      := ListaCod;
    end;
  end;
end;

procedure TFmAgenTarGer.MostraFormAgenTarCab(SQLTipo: TSQLType);
var
  Codigo: Integer;
  Sel: TListItem;
begin
  if SQLTipo = stIns then
  begin
    UnAgendaJan.MostraFormAgenTarCab(0);
    ReopenAgenTarGer;
  end else
  begin
    Codigo := STTarefas.Tag;
    //
    if Codigo > 1 then
    begin
      UnAgendaJan.MostraFormAgenTarCab(Codigo);
      ReopenAgenTarGer;
    end else
      Geral.MB_Aviso('Nenhum item foi selecionado ou esta lista n�o pode ser editada!');
  end;
end;

(*
procedure TFmAgenTarGer.MostraFormAgenTarIts(SQLTipo: TSQLType);
var
  Codigo, Controle: Integer;
  Sel: TTodoItem;
begin
  Codigo := STTarefas.Tag;
  //
  if SQLTipo = stIns then
  begin
    UnAgendaJan.MostraFormAgenTarIts(Codigo, 0, CO_Agend_Categorias_Cod[0], 0);
    ReopenAgenTarIts(Codigo);
    ReopenAgenTarGer(Codigo);
  end else
  begin
    Sel := TDLTarefas.Selected;
    //
    if Sel <> nil then
    begin
      Controle := Sel.Tag;
      //
      if Controle <> 0 then
      begin
        UnAgendaJan.MostraFormAgenTarIts(Codigo, Controle);
        ReopenAgenTarIts(Codigo);
      end else
        Geral.MB_Aviso('Este item n�o pode ser editado!');
    end else
      Geral.MB_Aviso('Nenhum item foi selecionado!');
  end;
end;
*)

procedure TFmAgenTarGer.PMMenuPopup(Sender: TObject);
(*
var
  Visi: Boolean;
  Sel: TTodoItem;
  Controle, OriID: Integer;
*)
begin
(*
  Visi := False;
  Sel  := TDLTarefas.Selected;
  //
  if Sel <> nil then
  begin
    Controle := Sel.Tag;
    //
    if Controle <> 0 then
    begin
      if QrAgenTarIts.Locate('Controle', Controle, []) then
      begin
        OriID := QrAgenTarIts.FieldByName('OriID').AsInteger;
        //
        if OriID <> 0 then
          Visi := True;
      end;
    end;
  end;
  N1.Visible               := Visi;
  Localizarorigem1.Visible := Visi;
*)
end;

procedure TFmAgenTarGer.ReopenAgenTarGer(AgenTarCab: Integer = 0);

  procedure LocalizaAgenTarCab();
  var
    I: Integer;
  begin
    if AgenTarCab <> 0 then
    begin
      for I := 0 to LVListas.Items.Count - 1 do
      begin
        if LVListas.Items[I].StateIndex = AgenTarCab then
        begin
          LVListas.Items[I].Selected := True;
          Break;
        end;
      end;
    end;
  end;

var
  Codigo, TotalIts, Grupo, Ativo: Integer;
  Texto: String;
begin
  LVListas.Items.BeginUpdate;
  try
    LVListas.ClearSelection;
    LVListas.Items.Clear;
    //
    Ativo := RGAtivo.ItemIndex;
    //
    GrlAgenda.ReopenAgenTarGer(Dmod.MyDB, QrAgenTarCab, Ativo, lanpTodos);
    //
    if QrAgenTarCab.RecordCount > 0 then
    begin
      QrAgenTarCab.First;
      //
      while not QrAgenTarCab.EOF do
      begin
        Codigo   := QrAgenTarCab.FieldByName('Codigo').AsInteger;
        Texto    := QrAgenTarCab.FieldByName('Nome').AsString;
        TotalIts := QrAgenTarCab.FieldByName('TotalIts').AsInteger;
        //
        case Codigo of
          -1:
            Grupo := 2;
          0:
            Grupo := 0;
          else
            Grupo := 1;
        end;
        UnAgenda.InsereTarefaLista(LVListas, TGrupoAgenTar(Grupo), Codigo,
          Texto, TotalIts);
        //
        QrAgenTarCab.Next;
      end;
    end;
  finally
    LVListas.Items.EndUpdate;
    //
    LocalizaAgenTarCab();
  end;
end;

(*
procedure TFmAgenTarGer.ReopenAgenTarIts(Codigo: Integer);
var
  Data, DataExe, DataFim: TDate;
  Controle, Finalizado, Realizado, Status, OriID, Prioridade: Integer;
  Duracao: Double;
  Finaliz: Boolean;
  Nome, ResponsTxt: String;
begin
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando lista de tarefas!');
    //
    TDLTarefas.Items.Clear;
    //
    Finalizado := TBTarefas.TabIndex;
    //
    if PnData.Visible then
    begin
      Data := TPData.Date;
      //
      UnAgenda.CriaItemTarefaRecorrente(Dmod.MyDB, Data);
    end else
      Data := 0;
    //
    GrlAgenda.ReopenAgenTarIts(Dmod.MyDB, QrAgenTarIts, Codigo, Finalizado, Data);
    //
    if QrAgenTarIts.RecordCount > 0 then
    begin
      QrAgenTarIts.First;
      //
      while not QrAgenTarIts.EOF do
      begin
        Controle   := QrAgenTarIts.FieldByName('Controle').AsInteger;
        Nome       := QrAgenTarIts.FieldByName('Nome').AsString;
        ResponsTxt := QrAgenTarIts.FieldByName('ResponsTxt').AsString;
        Realizado  := QrAgenTarIts.FieldByName('Realizado').AsInteger;
        Status     := QrAgenTarIts.FieldByName('Status').AsInteger;
        OriID      := QrAgenTarIts.FieldByName('OriID').AsInteger;
        Prioridade := QrAgenTarIts.FieldByName('Prioridade').AsInteger;
        DataExe    := QrAgenTarIts.FieldByName('DataExe').AsDateTime;
        DataFim    := QrAgenTarIts.FieldByName('DataFim').AsDateTime;
        Duracao    := QrAgenTarIts.FieldByName('Duracao').AsFloat;
        Finaliz    := Geral.IntToBool(QrAgenTarIts.FieldByName('Finalizado').AsInteger);
        //
        UnAgenda.InsereTarefaItem(TDLTarefas, Controle, Realizado, Status, OriID,
          Prioridade, Nome, ResponsTxt, DataExe, DataFim, Duracao, Finaliz);
        //
        QrAgenTarIts.Next;
      end;
    end;
  finally
    TDLTarefas.Items.EndUpdate;
    //
    STTarefasTot.Caption := 'Total de itens: ' + Geral.FF0(QrAgenTarIts.RecordCount);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '..............................');
  end;
end;
*)

procedure TFmAgenTarGer.ReordenaLista;
var
  Query: TmySQLQuery;
  Ativo: Integer;
begin
  Query := TmySQLQuery.Create(Dmod);
  try
    Ativo := RGAtivo.ItemIndex;
    //
    GrlAgenda.ReopenAgenTarGer(Dmod.MyDB, Query, Ativo, lanpPositivo);
    //
    if Query.RecordCount > 0 then
    begin
      UReordena.ReordenaItens(Query, Dmod.QrUpd, 'agentarcab',
        'Ordem', 'Codigo', 'Nome', '', '', '', nil);
      //
      ReopenAgenTarGer;
    end;
  finally
    Query.Free;
  end;
end;

procedure TFmAgenTarGer.RGAtivoClick(Sender: TObject);
begin
  ReopenAgenTarGer;
end;

procedure TFmAgenTarGer.TBTarefasChange(Sender: TObject);
begin
  //ReopenAgenTarIts(STTarefas.Tag);
end;

procedure TFmAgenTarGer.TDLTarefasCompleteClick(Sender: TObject;
  ItemIndex: Integer);
begin
  FReabre := True;
end;

procedure TFmAgenTarGer.TDLTarefasEditDone(Sender: TObject);
begin
  //AtualizaItem;
end;

(*
procedure TFmAgenTarGer.TDLTarefasItemRightClick(Sender: TObject;
  ATodoItem: TTodoItem);
begin
  ATodoItem.Select;
end;
*)

(*
procedure TFmAgenTarGer.TDLTarefasItemSelect(Sender: TObject;
  ATodoItem: TTodoItem);
begin
  ConfiguraDescricao(ATodoItem);
end;
*)

(*
procedure TFmAgenTarGer.ConfiguraDescricao(Item: TTodoItem);

  procedure ConfiguraDescri(Mostra: Boolean);
  begin
    if (Mostra) and (QrAgenTarIts.FieldByName('Descri').AsString <> '') then
    begin
      PnDescri.Visible  := True;
      MeDescri.Text     := QrAgenTarIts.FieldByName('Descri').AsString;
    end else
    begin
      PnDescri.Visible  := False;
      MeDescri.Text     := '';
    end;
  end;

var
  Controle: Integer;
begin
  if Item <> nil then
  begin
    Controle := Item.Tag;
    //
    if QrAgenTarIts.Locate('Controle', Controle, []) then
      ConfiguraDescri(True)
    else
      ConfiguraDescri(False);
  end else
    ConfiguraDescri(False);
end;
*)

end.
