unit AgendaGer2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, dmkCompoStore,
  Vcl.CheckLst, (*AsgCombo,*) System.ImageList, Vcl.ImgList, Vcl.Menus,
  mySQLDbTables, dmkDBGrid, (*ColorCombo, Planner, PlannerMonthView, TodoList,
  PlanRecurrEdit, *)UnitAgenda, (*PlanPeriodEdit,
  PlanItemEdit, PlanSimpleEdit, *)System.DateUtils, UnDmkProcFunc,
  (*DBPlannerMonthView, *)Vcl.TabNotBk, UnitAgendaAux;

type
  TFmAgendaGer2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    PnAgendas: TPanel;
    PnItens: TPanel;
    QrAgendas: TmySQLQuery;
    QrAgendasCodigo: TIntegerField;
    QrAgendasNome: TWideStringField;
    QrAgendasMostra: TSmallintField;
    QrAgendasCor: TWideStringField;
    DsAgendas: TDataSource;
    BtOpcoes: TBitBtn;
    QrAgendasTZD_UTC: TFloatField;
    PMItens: TPopupMenu;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    Panel5: TPanel;
    BtAgeExclui: TBitBtn;
    BtAgeAltera: TBitBtn;
    BtAgeInclui: TBitBtn;
    BtReabre: TBitBtn;
    MCCalendario: TMonthCalendar;
    StaticText1: TStaticText;
    ImageList1: TImageList;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    PnCompromisso: TPanel;
    PCCompromisso: TPageControl;
    TSCompromissos: TTabSheet;
    SGCompromissos: TStringGrid;
    Panel7: TPanel;
    BtAlteraAge: TBitBtn;
    BtExcluiAge: TBitBtn;
    Splitter2: TSplitter;
    PCCalendario: TPageControl;
    TSMes: TTabSheet;
    TSAgenda: TTabSheet;
    TSTarefas: TTabSheet;
    Panel6: TPanel;
    StaticText2: TStaticText;
    DBGAgendas: TdmkDBGrid;
    Panel8: TPanel;
    Panel9: TPanel;
    BtHoje: TBitBtn;
    BtIncluiItem: TBitBtn;
    BtAlteraTar: TBitBtn;
    BtExcluiTar: TBitBtn;
    CBExibir: TComboBox;
    Label32: TLabel;
    RGExibir: TRadioGroup;
    LVAgendas: TListView;
    BitBtn1: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    TabSheet1: TTabSheet;
    ListView1: TListView;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBExibirChange(Sender: TObject);
    procedure BtIncluiItemClick(Sender: TObject);
    procedure PMVMesDaySelect(Sender: TObject; SelDate: TDateTime);
    procedure MCCalendarioClick(Sender: TObject);
    //procedure PLAgendaItemSelect(Sender: TObject; Item: TPlannerItem);
    procedure DBGAgendasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGAgendasCellClick(Column: TColumn);
    procedure QrAgendasBeforeClose(DataSet: TDataSet);
    procedure PMVMesYearChange(Sender: TObject; origDate, newDate: TDateTime);
    procedure BtAgeIncluiClick(Sender: TObject);
    procedure BtAgeAlteraClick(Sender: TObject);
    procedure BtAgeExcluiClick(Sender: TObject);
    //procedure PMVMesItemSelect(Sender: TObject; Item: TPlannerItem);
    procedure BtExcluiAgeClick(Sender: TObject);
    procedure SGCompromissosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtAlteraAgeClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure PLAgendaPlannerSelectCell(Sender: TObject; Index, Pos: Integer;
      var CanSelect: Boolean);
    procedure BtHojeClick(Sender: TObject);
    procedure BtAlteraTarClick(Sender: TObject);
    (*
    procedure TDLTarefasItemInsert(Sender: TObject; ATodoItem: TTodoItem;
      var Allow: Boolean);
    *)
    (*
    procedure TDLTarefasItemDelete(Sender: TObject; ATodoItem: TTodoItem;
      var Allow: Boolean);
    *)
    procedure TDLTarefasEditDone(Sender: TObject);
    procedure BtExcluiTarClick(Sender: TObject);
    //procedure PLAgendaItemAlarm(Sender: TObject; Item: TPlannerItem);
    procedure LVAgendasItemChecked(Sender: TObject; Item: TListItem);
  private
    { Private declarations }
    procedure ConfiguraJanela();



    //procedure ConfiguraCalendario(Tipo: TCalTipo; Data: TDateTime);
    //procedure ConfiguraGradeDoDia(Tipo: TCalTipo; Data: TDate);
    procedure ConfiguraStringGridCompromissos(Grid: TStringGrid);
    procedure ExcluiItem(Controle: Integer; Data: TDateTime);
    //procedure ReconfiguraCalendario(Data: TDateTime);
  public
    { Public declarations }
  end;

  var
  FmAgendaGer2: TFmAgendaGer2;

implementation

uses UnMyObjects, Module, MyGlyfs, Principal, UnDmkImg, UMySQLModule, Feriados,
  MyDBCheck, ModuleGeral;

{$R *.DFM}

procedure TFmAgendaGer2.Altera1Click(Sender: TObject);
(*
var
  Item: TPlannerItem;
  Codigo, Controle: Integer;
*)
begin
(*
  if TCalTipo(CBExibir.ItemIndex) = istCTMes then
    Item := PMVMes.Items.Selected
  else
    Item := PLAgenda.Items.Selected;
  //
  if Item <> nil then
  begin
    Controle := Item.ID;
    Codigo   := Item.Tag;
    //
    UnAgenda.MostraAgendaIts(stUpd, Codigo, Controle, MCCalendario.Date, Dmod.MyDB);
    UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario, PMVMes,
      PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
*)
end;

procedure TFmAgendaGer2.BtAgeAlteraClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrAgendas.State <> dsInactive) and (QrAgendas.RecordCount > 0) then
  begin
    Codigo := QrAgendasCodigo.Value;
    //
    if Codigo > 0 then
    begin
      VAR_CADASTRO := 0;
      //
      UnAgenda.MostraAgendaCab(stUpd, Codigo);
      //
      if VAR_CADASTRO <> 0 then
      begin
        (*
        UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario,
          PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
        *)
      end;
    end else
      Geral.MB_Aviso('Esta agenda n�o pode ser editada!');
  end;
end;

procedure TFmAgendaGer2.BtAgeExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrAgendas.State <> dsInactive) and (QrAgendas.RecordCount > 0) then
  begin
    Codigo := QrAgendasCodigo.Value;
    //
    if UnAgenda.ExcluiAgenda(Codigo, Dmod.MyDB) then
    begin
      (*
      UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario,
        PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
      *)
    end;
  end;
end;

procedure TFmAgendaGer2.BtAgeIncluiClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnAgenda.MostraAgendaCab(stIns, 0);
  //
  if VAR_CADASTRO <> 0 then
  begin
    (*
    UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario, PMVMes,
      PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
    *)
  end;
end;

procedure TFmAgendaGer2.BtAlteraAgeClick(Sender: TObject);
var
  Linha, Codigo, Controle: Integer;
begin
  if TSCompromissos.TabVisible = True then
  begin
    Linha := SGCompromissos.Row;
    //
    if Linha > 0 then
    begin
      Controle := Geral.IMV(SGCompromissos.Cells[3, Linha]);
      Codigo   := Geral.IMV(SGCompromissos.Cells[4, Linha]);
      //
      UnAgenda.MostraAgendaIts(stUpd, Codigo, Controle, MCCalendario.Date, Dmod.MyDB);
      (*
      UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario,
        PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
      *)
    end else
      Geral.MB_Aviso('Selecione na lista abaixo o item que voc� deseja excluir!');
  end;
end;

procedure TFmAgendaGer2.BtAlteraTarClick(Sender: TObject);
(*
var
  Item: TTodoItem;
  Controle: Integer;
*)
begin
(*
  Item := TDLTarefas.Selected;
  //
  if Item <> nil then
  begin
    Controle := Item.Tag;
    //
    UnAgenda.MostraAgendaIts(stUpd, Integer(istATTarefas), Controle, MCCalendario.Date, Dmod.MyDB);
    UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario,
      PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
*)
end;

procedure TFmAgendaGer2.ExcluiItem(Controle: Integer; Data: TDateTime);
begin
  if UnAgenda.ExcluiItemAgenda(Controle, Data, Dmod.MyDB) then
  begin
    (*
    UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario,
      PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
    *)
    //
    if PnCompromisso.Visible then
      MyObjects.ExcluiLinhaStringGrid(SGCompromissos, False);
  end;
end;

procedure TFmAgendaGer2.BtExcluiAgeClick(Sender: TObject);
var
  Data: TDateTime;
  Linha, Controle: Integer;
begin
  if TSCompromissos.TabVisible = True then
  begin
    Linha := SGCompromissos.Row;
    //
    if Linha > 0 then
    begin
      Controle := Geral.IMV(SGCompromissos.Cells[3, Linha]);
      Data     := StrToDateTime(SGCompromissos.Cells[5, Linha]);
      //
      ExcluiItem(Controle, Data);
    end else
      Geral.MB_Aviso('Selecione na lista abaixo o item que voc� deseja excluir!');
  end;
end;

procedure TFmAgendaGer2.BtExcluiTarClick(Sender: TObject);
(*
var
  Item: TTodoItem;
  Data: TDateTime;
  Linha, Controle: Integer;
*)
begin
(*
  Item := TDLTarefas.Selected;
  //
  if Item <> nil then
  begin
    Controle := Item.Tag;
    Data     := Item.CreationDate;
    //
    ExcluiItem(Controle, Data);
  end else
    Geral.MB_Aviso('Voc� deve selecionar algum item!');
*)
end;

procedure TFmAgendaGer2.BtHojeClick(Sender: TObject);
begin
  //ReconfiguraCalendario(DmodG.ObtemAgora());
end;

procedure TFmAgendaGer2.BtIncluiItemClick(Sender: TObject);
(*
var
  Item: TPlannerItem;
*)
begin
(*
  if (QrAgendas.State <> dsInactive) and (QrAgendas.RecordCount > 0) then
  begin
    if TCalTipo(CBExibir.ItemIndex) = istCTMes then
      Item := PMVMes.Items.Add
    else
      Item := PLAgenda.Items.Add;
    //
    UnAgenda.MostraAgendaIts(stIns, QrAgendasCodigo.Value, 0, MCCalendario.Date, Dmod.MyDB);
    UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario,
      PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
  end else
    Geral.MB_Aviso('Antes de criar um evento voc� deve cadastrar uma agenda!');
*)
end;

procedure TFmAgendaGer2.BtReabreClick(Sender: TObject);
begin
(*
  UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario, PMVMes,
    PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
*)
end;

procedure TFmAgendaGer2.BtSaidaClick(Sender: TObject);
begin
  if TFmAgendaGer2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmAgendaGer2.CBExibirChange(Sender: TObject);
(*
var
  Tipo: Integer;
  Data: TDate;
*)
begin
  (*
  Tipo := CBExibir.ItemIndex;
  Data := MCCalendario.Date;
  //
  case Tipo of
    0: //M�s
      ConfiguraCalendario(istCTMes, Data);
    1: //Semana
      ConfiguraCalendario(istCTSemana, Data);
    2: //Dia
      ConfiguraCalendario(istCTDia, Data);
    3: //Tarefas
      ConfiguraCalendario(istCTTarefas, Data);
  end;
  UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario, PMVMes,
    PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
  *)
end;

(*
procedure TFmAgendaGer2.ConfiguraCalendario(Tipo: TCalTipo; Data: TDateTime);
var
  Dta: TDateTime;
  i, N: Integer;
  X: String;
begin
  PMVMes.Items.Clear;
  PLAgenda.Items.Clear;
  TDLTarefas.Items.Clear;
  //
  if Data = 0 then
    Dta := Date
  else
    Dta := Data;
  //
  MCCalendario.Date     := Dta;
  PnCompromisso.Visible := False;
  Splitter2.Visible     := False;
  //
  case tipo of
    istCTMes:
    begin
      PCCalendario.ActivePageIndex := 0;
      TSAgenda.TabVisible          := False;
      TSMes.TabVisible             := True;
      TSTarefas.TabVisible         := False;
      PMVMes.Date                  := Dta;
      //
      BtAlteraTar.Visible := False;
      BtExcluiTar.Visible := False;
    end;
    istCTSemana:
    begin
      PCCalendario.ActivePageIndex := 1;
      TSAgenda.TabVisible          := True;
      TSMes.TabVisible             := False;
      TSTarefas.TabVisible         := False;
      PLAgenda.Mode.PlannerType    := plDay;
      PLAgenda.Caption.Title       := Geral.FDT(Dta, 5);
      PLAgenda.Mode.TimeLineStart  := Dta;
      PLAgenda.Positions           := 7;
      //
      PLAgenda.Header.Captions.Clear;
      PLAgenda.Header.Captions.Add('');
      for i := 1 to PLAgenda.Positions do
      begin
        N := Trunc(Dta + i - 1);
        X := Copy(dmkPF.NomeDiaDaSemana(DayOfWeek(N)), 1, 3);
        PLAgenda.Header.Captions.Add(X + ', ' + Geral.FDT(N, 2));
      end;
      PLAgenda.Header.Captions.Add('');
      //
      BtAlteraTar.Visible := False;
      BtExcluiTar.Visible := False;
    end;
    istCTDia:
    begin
      PCCalendario.ActivePageIndex  := 1;
      TSAgenda.TabVisible           := True;
      TSMes.TabVisible              := False;
      TSTarefas.TabVisible          := False;
      PLAgenda.Mode.PlannerType     := plDay;
      PLAgenda.Caption.Title        := Geral.FDT(Dta, 2);
      PLAgenda.Mode.TimeLineStart   := Dta;
      PLAgenda.Positions            := 1;
      //
      PLAgenda.Header.Captions.Clear;
      //
      BtAlteraTar.Visible := False;
      BtExcluiTar.Visible := False;
    end;
    istCTTarefas:
    begin
      PCCalendario.ActivePageIndex := 2;
      TSAgenda.TabVisible          := False;
      TSMes.TabVisible             := False;
      TSTarefas.TabVisible         := True;
      //
      BtAlteraTar.Visible := True;
      BtExcluiTar.Visible := True;
    end;
  end;
end;
*)

(*
procedure TFmAgendaGer2.ConfiguraGradeDoDia(Tipo: TCalTipo; Data: TDate);
var
  Total, Linhas, I: Integer;
  Item: TPlannerItem;
  Visi: Boolean;
begin
  if Tipo = istCTMes then
    Visi := PMVMes.FindFirstItemAtDate(Data) <> nil
  else
    Visi := PLAgenda.Items.Count > 0;
  //
  PnCompromisso.Visible := Visi;
  Splitter2.Visible     := Visi;
  //
  if PnCompromisso.Visible = True then
  begin
    ConfiguraStringGridCompromissos(SGCompromissos);
    //
    if Tipo = istCTMes then
    begin
      Total := PMVMes.Items.NumItemsAtDate(Data);
      //
      if Total > 0 then
      begin
        for I := 1 to Total do
        begin
          if I = 1 then
          begin
            Item := PMVMes.FindFirstItemAtDate(Data);

            if Item <> nil then
            begin
              SGCompromissos.Cells[0, 1] := '';
              SGCompromissos.Cells[1, 1] := Item.Text.Text;
              SGCompromissos.Cells[2, 1] := ColorToString(Item.Color);
              SGCompromissos.Cells[3, 1] := Geral.FF0(Item.ID);
              SGCompromissos.Cells[4, 1] := Geral.FF0(Item.Tag);
              SGCompromissos.Cells[5, 1] := Geral.FDT(Item.ItemStartTime, 0);
            end;
          end else
          begin
            Item := PMVMes.FindNextItemAtDate(Data);
            //
            if Item <> nil then
            begin
              Linhas := SGCompromissos.RowCount + 1;
              //
              SGCompromissos.RowCount := Linhas;
              //
              SGCompromissos.Cells[0, Linhas - 1] := '';
              SGCompromissos.Cells[1, Linhas - 1] := Item.Text.Text;
              SGCompromissos.Cells[2, Linhas - 1] := ColorToString(Item.Color);
              SGCompromissos.Cells[3, Linhas - 1] := Geral.FF0(Item.ID);
              SGCompromissos.Cells[4, Linhas - 1] := Geral.FF0(Item.Tag);
              SGCompromissos.Cells[5, Linhas - 1] := Geral.FDT(Item.ItemStartTime, 0);
            end;
          end;
        end;
      end;
    end else
    begin
      Total := PLAgenda.Items.Count;
      //
      if Total > 0 then
      begin
        for I := 1 to Total do
        begin
          Item := PLAgenda.Items[I - 1];

          if Item <> nil then
          begin
            if I = 1 then
              Linhas := 2
            else
              Linhas := SGCompromissos.RowCount + 1;
            //
            SGCompromissos.RowCount := Linhas;
            //
            SGCompromissos.Cells[0, Linhas - 1] := '';
            SGCompromissos.Cells[1, Linhas - 1] := Item.Text.Text;
            SGCompromissos.Cells[2, Linhas - 1] := ColorToString(Item.Color);
            SGCompromissos.Cells[3, Linhas - 1] := Geral.FF0(Item.ID);
            SGCompromissos.Cells[4, Linhas - 1] := Geral.FF0(Item.Tag);
            SGCompromissos.Cells[5, Linhas - 1] := Geral.FDT(Item.ItemStartTime, 0);
          end;
        end;
      end;
    end;
  end;
end;
*)

procedure TFmAgendaGer2.ConfiguraJanela;

  procedure CriaGrupo(Id: Integer; Texto: String);
  var
    Item: TListGroup;
  begin
    Item := LVAgendas.Groups.Add;
    Item.GroupID := Id;
    Item.Header  := Texto;
    Item.State   := [lgsNormal, lgsSelected, lgsCollapsible];
  end;

var
  Coluna: TListColumn;
begin
  //Exemplos interessantes
  //13, 27 recorrencia
  //17 som
  //Display.ActiveStar Hor�rio �til configurar e colocar nas op��es
  //
  RGExibir.Items.Clear;
  RGExibir.Items.AddStrings(UnAgendaAux.ConfiguraListaExibicao);
  RGExibir.ItemIndex := 0;
  //
  RGExibir.Columns := RGExibir.Items.Count;
  RGExibir.Width   := RGExibir.Items.Count * 70;
  //
  LVAgendas.GroupView         := True;
  LVAgendas.ShowColumnHeaders := False;
  LVAgendas.SmallImages       := ImageList1;
  LVAgendas.ViewStyle         := vsReport;
  LVAgendas.Checkboxes        := True;
  //
  LVAgendas.Columns.Clear;
  LVAgendas.Columns.BeginUpdate;
  try
    Coluna := LVAgendas.Columns.Add;
    Coluna.Caption := 'Descri��o';
    Coluna.Width   := 250;
  finally
    LVAgendas.Columns.EndUpdate;
  end;
  //
  LVAgendas.Groups.Clear;
  LVAgendas.Groups.BeginUpdate;
  try
    CriaGrupo(0, 'Agendas do aplicativo');
    CriaGrupo(1, 'Agendas');
  finally
    LVAgendas.Groups.EndUpdate;
  end;
end;

procedure TFmAgendaGer2.ConfiguraStringGridCompromissos(Grid: TStringGrid);
begin
  MyObjects.LimpaGrade(Grid, 0, 0, True);
  //
  Grid.ColCount  := 6;
  Grid.FixedCols := 0;
  Grid.FixedRows := 1;
  //
  Grid.ColWidths[0] := 10;
  Grid.ColWidths[1] := 420;
  Grid.ColWidths[2] := 0;
  Grid.ColWidths[3] := 0;
  Grid.ColWidths[4] := 0;
  Grid.ColWidths[5] := 0;
  //
  Grid.Cells[0, 0] := '';
  Grid.Cells[1, 0] := 'Descri��o';
  Grid.Cells[2, 0] := 'Agenda Cor';
  Grid.Cells[3, 0] := 'ID';
  Grid.Cells[4, 0] := 'Agenda ID';
  Grid.Cells[5, 0] := 'Data Ini';
end;

procedure TFmAgendaGer2.DBGAgendasCellClick(Column: TColumn);
var
  Codigo, Mostra: Integer;
begin
  if (QrAgendas.State <> dsInactive) and (QrAgendas.RecordCount > 0) then
  begin
    if Lowercase(Column.FieldName) = Lowercase('Mostra') then
    begin
      Screen.Cursor := crHourGlass;
      try
        Codigo := QrAgendasCodigo.Value;
        //
        if QrAgendasMostra.Value = 1 then
          Mostra := 0
        else
          Mostra := 1;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agencab', False,
          ['Mostra'], ['Codigo'], [Mostra], [Codigo], True) then
        begin
          (*
          UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario,
            PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
          *)
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmAgendaGer2.DBGAgendasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'Nome')then
  begin
    Cor := DmkImg.HexToTColor(QrAgendasCor.Value);
    //
    with DBGAgendas.Canvas do
    begin
      Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmAgendaGer2.Exclui1Click(Sender: TObject);
(*
var
  Item: TPlannerItem;
  Controle: Integer;
  Data: TDateTime;
*)
begin
(*
  if TCalTipo(CBExibir.ItemIndex) = istCTMes then
    Item := PMVMes.Items.Selected
  else
    Item := PLAgenda.Items.Selected;
  //
  if Item <> nil then
  begin
    Controle := Item.ID;
    Data     := Item.ItemStartTime;
    //
    ExcluiItem(Controle, Data);
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
*)
end;

procedure TFmAgendaGer2.FormActivate(Sender: TObject);
begin
  if TFmAgendaGer2(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmAgendaGer2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ConfiguraJanela();
  //
  ListView1.SmallImages := FmMyGlyfs.Lista_32X32_Textos;
  //
  (*
  ConfiguraCalendario(istCTMes, Date);
  UnAgendaAux.ConfiguraPlanner(PLAgenda);
  UnAgenda.ConfiguraToDoList(TDLTarefas);
  *)
  //
  CBExibir.Items.Clear;
  CBExibir.Items.AddStrings(UnAgendaAux.ConfiguraListaExibicao);
  //
  CBExibir.ItemIndex := 0;
  VAR_CADASTRO       := 0;
  //
  ConfiguraStringGridCompromissos(SGCompromissos);
  //
  (*
  UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario, PMVMes,
    PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
  *)
end;

procedure TFmAgendaGer2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgendaGer2.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmAgendaGer2.LVAgendasItemChecked(Sender: TObject; Item: TListItem);
var
  Codigo, Mostra: Integer;
begin
  if LVAgendas.Focused then
  begin
    Screen.Cursor := crHourGlass;
    try
      Codigo := Item.StateIndex;
      Mostra := Geral.BoolToInt(Item.Checked);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agencab', False,
        ['Mostra'], ['Codigo'], [Mostra], [Codigo], True) then
      begin
        (*
        UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, nil, MCCalendario,
          PMVMes, PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
        *)
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmAgendaGer2.MCCalendarioClick(Sender: TObject);
begin
  //ReconfiguraCalendario(MCCalendario.Date);
end;

(*
procedure TFmAgendaGer2.PLAgendaItemAlarm(Sender: TObject; Item: TPlannerItem);
begin
  //
end;
*)

(*
procedure TFmAgendaGer2.PLAgendaItemSelect(Sender: TObject; Item: TPlannerItem);
begin
  MCCalendario.Date := PLAgenda.Mode.TimeLineStart;
  //
  Item.PopupMenu := PMItens;
  ConfiguraGradeDoDia(TCalTipo(CBExibir.ItemIndex), PLAgenda.Mode.TimeLineStart);
end;
*)

procedure TFmAgendaGer2.PLAgendaPlannerSelectCell(Sender: TObject; Index,
  Pos: Integer; var CanSelect: Boolean);
begin
  (*
  MCCalendario.Date := PLAgenda.Mode.TimeLineStart;
  //
  ConfiguraGradeDoDia(TCalTipo(CBExibir.ItemIndex), PLAgenda.Mode.TimeLineStart);
  *)
end;

procedure TFmAgendaGer2.PMVMesDaySelect(Sender: TObject; SelDate: TDateTime);
begin
  (*
  MCCalendario.Date := PMVMes.Date;
  //
  ConfiguraGradeDoDia(TCalTipo(CBExibir.ItemIndex), PMVMes.Date);
  *)
end;

(*
procedure TFmAgendaGer2.PMVMesItemSelect(Sender: TObject; Item: TPlannerItem);
begin
  PMVMes.Date := Item.ItemStartTime;
  //
  ConfiguraGradeDoDia(TCalTipo(CBExibir.ItemIndex), PMVMes.Date);
  //
  Item.PopupMenu := PMItens;
end;
*)

procedure TFmAgendaGer2.PMVMesYearChange(Sender: TObject; origDate,
  newDate: TDateTime);
begin
  (*
  UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario, PMVMes,
    PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
  *)
end;

procedure TFmAgendaGer2.QrAgendasBeforeClose(DataSet: TDataSet);
begin
  (*
  PMVMes.Items.Clear;
  PLAgenda.Items.Clear;
  *)
end;

(*
procedure TFmAgendaGer2.ReconfiguraCalendario(Data: TDateTime);
begin
  MCCalendario.Date           := Data;
  PMVMes.Date                 := Data;
  PLAgenda.Mode.TimeLineStart := Data;
  //
  ConfiguraCalendario(TCalTipo(CBExibir.ItemIndex), Data);
  UnAgenda.ReopenAgendas(QrAgendas, Dmod.MyDB, LVAgendas, MCCalendario, PMVMes,
    PLAgenda, TDLTarefas, TCalTipo(CBExibir.ItemIndex));
end;
*)

procedure TFmAgendaGer2.SGCompromissosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Atualiza: Boolean;
begin
  Atualiza := SGCompromissos.Cells[2, ARow] <> '';
  //
  with SGCompromissos do
  begin
    if (ARow > 0) and (ACol = 0) and (Atualiza = True) then
    begin
      Canvas.Font.Color  := clWindow;
      Canvas.Font.Style  := [fsBold];
      Canvas.Brush.Color := StringToColor(SGCompromissos.Cells[2, ARow]);
    end else
    begin
      Canvas.Font.Color  := clBlack;
      Canvas.Font.Style  := [];
      Canvas.Brush.Color := clWindow;
    end;
    Canvas.FillRect(Rect);
    Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[ACol, ARow]);
  end;
end;

procedure TFmAgendaGer2.TDLTarefasEditDone(Sender: TObject);
(*
var
  Item: TTodoItem;
  StatusStr: String;
*)
begin
(*
  Item := TDLTarefas.Selected;
  //
    case Item.Status of
      tsNotStarted:
        StatusStr := 'tsNotStarted';
      tsCompleted:
        StatusStr := 'tsCompleted';
      tsInProgress:
        StatusStr := 'tsInProgress';
      tsDeferred:
        StatusStr := 'tsInProgress';
    end;
  //
  if Item <> nil then
  begin
    Geral.MB_Aviso(
      'Realizado = ' + Geral.FF0(Item.Completion) + sLineBreak +
      'Finalizado = ' + Geral.FF0(Geral.BoolToInt(Item.Complete)) + sLineBreak +
      'Status = ' + StatusStr
      );
  end;
*)
end;

(*
procedure TFmAgendaGer2.TDLTarefasItemDelete(Sender: TObject;
  ATodoItem: TTodoItem; var Allow: Boolean);
begin
  Abort;
end;
*)

(*
procedure TFmAgendaGer2.TDLTarefasItemInsert(Sender: TObject;
  ATodoItem: TTodoItem; var Allow: Boolean);
begin
  Abort;
end;
*)

end.
