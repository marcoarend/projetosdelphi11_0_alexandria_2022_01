unit UnitAgenda;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, UnInternalConsts2, Variants, mySQLDbTables, (*Planner, PlannerMonthView,
  PlanRecurrEdit, PlanRecurr, *)System.DateUtils, (*TodoList, *)UnDmkEnums,
  (*AdvToolBar, *)UnDmkProcFunc, UnDmkWeb, UnGrlAgenda, UnitAgendaAux, UnGrl_Vars;

type
  TCalTipo = (istCTMes=0, istCTSemana=1, istCTDia=2, istCTTarefas=3);
  TAgendaTipo = (istATFeriados=-1, istATAniversarios=-2, istATTarefas=-3);
  TUnitAgenda = class(TObject)
  private
    { Private declarations }
    //Cria item agenda - In�cio
    (*procedure CriaItem(Item: TPlannerItem; PeriodoIni, DataIni, DataFim: TDateTime;
                Descri, Cor: String; AgendaID, ItemID: Integer; DiaTodo: Boolean);*)
    (*procedure CriaItemTarefa(Item: TTodoItem; Duracao: Double; DataIni, DataFim,
                DataFinaliz: TDateTime; Nome, Descri: String; Prioridade,
                Finalizado, Status, FatID, ItemID: Integer);*)
    //Cria item agenda - Fim
    function  QuaisItens(Msg: String): TSelType;
    procedure DataToDiaTodo(var DataIni, DataFim: TDateTime);
    (*procedure InsereAgendaFeriados(DB: TMySQLdatabase; PMVMes: TPlannerMonthView;
                PLAgenda: TPlanner; Cor: String; AgendaID: Integer;
                TZD_UTC: Double; Exibir: TCalTipo);*)
    (*procedure InsereAgendaAniversarios(DB: TMySQLdatabase;
                PMVMes: TPlannerMonthView; PLAgenda: TPlanner; Cor: String;
                AgendaID: Integer; TZD_UTC: Double; Exibir: TCalTipo);*)
    (*procedure InsereAgendaAvulsa(DB: TMySQLdatabase; PMVMes: TPlannerMonthView;
                PLAgenda: TPlanner; Cor: String; AgendaID: Integer; TZD_UTC:
                Double; Exibir: TCalTipo);*)
    (*procedure InsereAgendaTarefas(DB: TMySQLdatabase; MCCalendario: TMonthCalendar;
                TDLTarefas: TTodoList; Cor: String; AgendaID: Integer;
                TZD_UTC: Double; Exibir: TCalTipo);*)
    (*procedure InsereItem(PMVMes: TPlannerMonthView; PLAgenda: TPlanner;
                DataIni, DataFim: TDateTime; Cor, Descricao, Repeticoes: String;
                AgendaID, ItemID: Integer; DiaTodo: Boolean; Exibir: TCalTipo);*)
    (*procedure InsereItemTarefa(TDLTarefas: TTodoList; MCCalendario: TMonthCalendar;
                Duracao: Double; DataIni, DataFim, DataFinaliz: TDateTime; Cor,
                Nome, Descricao, Repeticoes: String; Prioridade, Finalizado, Status,
                FatID, AgendaID, ItemID: Integer; Exibir: TCalTipo);*)
    (*procedure ConfiguraPeriodoIniFim(const PMVMes: TPlannerMonthView;
                const PLAgenda: TPlanner; const MCCalendario: TMonthCalendar;
                const Exibir: TCalTipo; var DtaIni, DtaFim: TDateTime);*)
    //Categorias - In�cio
    function ObtemItemDeListaCategorias(FatID: Integer): Integer;
    function ObtemFatIDDeListaCategorias(ItemLista: Integer): Integer;
    function ObtemDescriItemDeListaCategorias(FatID: Integer): String;
    //Categorias - Fim
  public
    { Public declarations }
    //Cores Agenda
    (*
    Movido para o UnitAgendaAux
    function ObtemCorAgenda(Index: Integer): TColor;
    function ObtemIndexCorAgenda(Cor: TColor): Integer;
    *)
    //Configura componentes
    (*
    Movido para o UnitAgendaAux
    procedure ConfiguraRecorrencia(RERecorrencia: TPlannerRecurrencyEditor);
    procedure ConfiguraPlanner(PLAgenda: TPlanner);
    function  ConfiguraListaExibicao(): TStringList;
    function  ConfiguraListaPrioridades(): TStringList;
    *)
    //Reabertura de querys
    (*procedure ReopenAgendas(Query: TMySQLquery; DB: TMySQLdatabase;
              LVAgendas: TListView; MCCalendario: TMonthCalendar;
              PMVMes: TPlannerMonthView; PLAgenda: TPlanner;
              TDLTarefas: TTodoList; Exibir: TCalTipo);*)
    //Exclus�es
    function  ExcluiAgenda(Codigo: Integer; DB: TMySQLdatabase): Boolean;
    function  ExcluiItemAgenda(Controle: Integer; Data: TDateTime;
                DB: TMySQLdatabase): Boolean;
    //Janelas
    procedure MostraAgendaCab(SQLType: TSQLType; Codigo: Integer);
    procedure MostraAgendaIts(SQLType: TSQLType; Codigo, Controle: Integer;
                DataPadrao: TDate; DB: TMySQLdatabase);
    //Outros
    procedure CriaExcecao(Controle: Integer; Recorrencia: String;
                Data: TDateTime; DB: TMySQLdatabase);
    function  ValidaItemAgenda(const Controle: Integer; const DB: TMySQLdatabase;
                var Recorrencia: String): Boolean;

    // C O N F I  G U R A � � E S
    procedure ConfiguraListView(ListView: TListView);
    //procedure ConfiguraToDoList(TodoList: TTodoList);
    // I T E N S
    function  ObtemCodigoGrupo(DB: TMySQLdatabase; Controle: Integer): Integer;
    function  LocalizaGrupo(Lista: TListView; Codigo: Integer): Integer;
    //function  LocalizaItem(Lista: TTodoList; Controle: Integer): Integer;
    procedure InsereGrupos(ListView: TListView);
    procedure InsereTarefaLista(ListView: TListView; Grupo: TGrupoAgenTar; Id: Integer;
              Texto: String; TotalIts: Integer);
    (*procedure InsereTarefaItem(Lista: TTodoList; Controle, Realizado, Status,
              OriID, Prioridade: Integer; Nome, ResponsTxt: String; DataExe,
              DataFim: TDate; Duracao: Double; Finalizado: Boolean);*)
    //procedure CriaItemTarefaRecorrente(DataBase: TMySQLdatabase; Data: TDate);
    // C O N V E R S � E S
    //function  PriorityToInt(Priority: TTodoPriority): Integer;
    //function  IntToPriority(Priority: Integer): TTodoPriority;
    //function  StatusToInt(Status: TTodoStatus): Integer;
    //function  IntToStatus(Status: Integer): TTodoStatus;
  end;

var
  UnAgenda: TUnitAgenda;
const
  CO_Agend_Tarefas = -3;
  CO_Agend_Anual   = 'RRULE:FREQ=YEARLY';

implementation

uses DmkDAC_PF, UnDmkImg, MyDBCheck, AgendaCab, UMySQLModule, AgendaIts,
  UnMyObjects, ModuleGeral, Feriados, QuaisItens, UnGrl_DmkDB;

{ TUnitAgenda }

procedure TUnitAgenda.ConfiguraListView(ListView: TListView);
var
  Item: TListColumn;
begin
  ListView.Columns.Clear;
  ListView.Groups.Clear;
  ListView.Items.Clear;
  //
  Item := ListView.Columns.Add;
  Item.Caption  := 'Descri��o';
  Item.Width    := 220;
  //
  Item := ListView.Columns.Add;
  Item.Caption  := 'Itens';
  Item.Width    := 50;
  //
  ListView.HideSelection     := True;
  ListView.ViewStyle         := vsReport;
  ListView.ShowColumnHeaders := False;
  ListView.GroupView         := True;
  ListView.RowSelect         := True;
  ListView.ReadOnly          := True;
  ListView.Font.Size         := 10;
  ListView.SortType          := stData;
end;

procedure TUnitAgenda.InsereGrupos(ListView: TListView);

  procedure CriaGrupo(Grupo: TGrupoAgenTar);
  var
    Item: TListGroup;
    Id: Integer;
    Texto: String;
  begin
    Id    := GrlAgenda.ObtemGrupoID(Grupo);
    Texto := GrlAgenda.ObtemGrupoNome(Grupo);
    //
    Item := ListView.Groups.Add;
    Item.GroupID := Id;
    Item.Header  := Texto;
    Item.State   := [lgsNormal, lgsSelected, lgsCollapsible];
  end;

begin
  CriaGrupo(gatTarefas);
  CriaGrupo(gatListas);
  CriaGrupo(gatRecorrentes);
end;

(*
procedure TUnitAgenda.ConfiguraPeriodoIniFim(const PMVMes: TPlannerMonthView;
  const PLAgenda: TPlanner; const MCCalendario: TMonthCalendar;
  const Exibir: TCalTipo; var DtaIni, DtaFim: TDateTime);
var
  Ano, Mes, Dia: Word;
begin
  case Exibir of
    istCTMes:
    begin
      DtaIni := Geral.PrimeiroDiaDoMes(PMVMes.Date);
      DtaFim := Geral.UltimoDiaDoMes(PMVMes.Date);
    end;
    istCTSemana:
    begin
      DtaIni := PLAgenda.Mode.TimeLineStart;
      DtaFim := PLAgenda.Mode.TimeLineStart + 7;
    end;
    istCTDia:
    begin
      DtaIni := PLAgenda.Mode.TimeLineStart;
      DtaFim := PLAgenda.Mode.TimeLineStart;
    end;
    istCTTarefas:
    begin
      DtaIni := MCCalendario.Date;
      //
      DecodeDate(DtaIni, Ano, Mes, Dia);
      //
      DtaIni := EncodeDate(Ano, Mes, Dia);
      DtaFim := EncodeDate(Ano, Mes, Dia);
    end;
  end;
end;
*)

(*
procedure TUnitAgenda.ConfiguraToDoList(TodoList: TTodoList);

  procedure ConfiguraItem(Item: TTodoColumnItem; Descri: String; Editavel: Boolean;
    ToDoData: TTodoData; Largura: Integer);
  var
    Prefix: String;
  begin
    if Editavel = True then
      Prefix := '* '
    else
      Prefix := '';
    //
    Item.Caption  := Prefix + Descri;
    Item.Editable := Editavel;
    Item.TodoData := ToDoData;
    //
    if Largura <> 0 then
      Item.Width := Largura;
  end;

var
  Col0, Col1, Col2, Col3, Col4, Col5, Col6, Col7, Col8, Col9, Col10: TTodoColumnItem;
  ListaFatID: TStringList;
begin
  TodoList.Columns.Clear;
  //
  Col0  := TodoList.Columns.Add;
  Col1  := TodoList.Columns.Add;
  Col2  := TodoList.Columns.Add;
  Col3  := TodoList.Columns.Add;
  Col4  := TodoList.Columns.Add;
  Col5  := TodoList.Columns.Add;
  Col6  := TodoList.Columns.Add;
  Col7  := TodoList.Columns.Add;
  Col8  := TodoList.Columns.Add;
  Col9  := TodoList.Columns.Add;
  Col10 := TodoList.Columns.Add;
  //
  ConfiguraItem(Col0, '', False, tdHandle, 32);
  ConfiguraItem(Col1, '% Realizado', True, tdCompletion, 75);
  ConfiguraItem(Col2, 'Finalizado?', True, tdComplete, 75);
  ConfiguraItem(Col3, 'Descri��o', False, tdSubject, 530);
  ConfiguraItem(Col4, 'Status', True, tdStatus, 150);
  ConfiguraItem(Col5, 'Prioridade', False, tdPriority, 100);
  ConfiguraItem(Col6, 'Tempo aprox. (h)', False, tdTotalTime, 100);
  ConfiguraItem(Col7, 'Data', False, tdDueDate, 100);
  ConfiguraItem(Col8, 'Categoria', False, tdCategory, 100);
  ConfiguraItem(Col9, 'Finalizado em', False, tdCompletionDate, 100);
  ConfiguraItem(Col10, 'Respons�vel', False, tdResource, 0);
  //
  TodoList.PriorityStrings.Lowest  := CO_Agend_Prioridades[0];
  TodoList.PriorityStrings.Low     := CO_Agend_Prioridades[1];
  TodoList.PriorityStrings.Normal  := CO_Agend_Prioridades[2];
  TodoList.PriorityStrings.High    := CO_Agend_Prioridades[3];
  TodoList.PriorityStrings.Highest := CO_Agend_Prioridades[4];
  //
  TodoList.StatusStrings.NotStarted := CO_Agend_Status[0];
  TodoList.StatusStrings.Inprogress := CO_Agend_Status[1];
  TodoList.StatusStrings.Completed  := CO_Agend_Status[2];
  TodoList.StatusStrings.Deferred   := CO_Agend_Status[3];
  //
  TodoList.NullDate := 'N�o informado';
  //
  TodoList.ShowSelection           := True;
  TodoList.CompletionGraphic       := True;
  TodoList.CompleteCheck.CheckType := ctCheckMark;
  TodoList.MultiSelect             := False;
  TodoList.ShowPriorityText        := True;
  TodoList.TotalTimeSuffix         := 'h';
  TodoList.StretchLastColumn       := True;
  TodoList.ScrollHorizontal        := True;
  //
  TodoList.Category.Clear;
  TodoList.Category.AddStrings(GrlAgenda.ObtemListaCategorias);
end;
*)

(*
procedure TUnitAgenda.InsereAgendaAniversarios(DB: TMySQLdatabase;
  PMVMes: TPlannerMonthView; PLAgenda: TPlanner; Cor: String; AgendaID: Integer;
  TZD_UTC: Double; Exibir: TCalTipo);
var
  Qry: TmySQLQuery;
  Codigo: Integer;
  Nome, TimeZoneDifUTC, DtaIniPesq_Txt, DtaFimPesq_Txt: String;
  DataIni, DataFim: TDateTime;
begin
  Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    ConfiguraPeriodoIniFim(PMVMes, PLAgenda, nil, Exibir, DataIni, DataFim);
    //
    TimeZoneDifUTC := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
    DtaIniPesq_Txt := FormatDateTime('mm-dd', DataIni);
    DtaFimPesq_Txt := FormatDateTime('mm-dd', DataFim);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT Codigo, Tipo, ',
      'CONVERT_TZ(ENatal, "+00:00", "' + TimeZoneDifUTC + '") ENatal_TZ, ',
      'CONVERT_TZ(PNatal, "+00:00", "' + TimeZoneDifUTC + '") PNatal_TZ, ',
      'IF(Tipo=0, RazaoSocial, Nome) Nome ',
      'FROM entidades ',
      'WHERE IF(Tipo=0, ENatal, PNatal) > "1900-01-01" ',
      'AND Ativo = 1 ',
      'AND DATE_FORMAT(CONVERT_TZ(IF(Tipo=0, ENatal, PNatal), "+00:00", "' + TimeZoneDifUTC + '"), "%m-%d") BETWEEN "' + DtaIniPesq_Txt + '" AND "' + DtaFimPesq_Txt + '" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.EOF do
      begin
        if Qry.FieldByName('Tipo').AsInteger = 0 then
          DataIni := Qry.FieldByName('ENatal_TZ').AsDateTime
        else
          DataIni := Qry.FieldByName('PNatal_TZ').AsDateTime;

        Codigo := Qry.FieldByName('Codigo').AsInteger;
        Nome   := Qry.FieldByName('Nome').AsString;
        //
        InsereItem(PMVMes, PLAgenda, DataIni, DataIni, Cor, Nome, CO_Agend_Anual, AgendaID, Codigo, True, Exibir);
        //
        Qry.Next;
      end;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT con.Codigo, con.Nome,  ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NomeEnt, ',
      'CONVERT_TZ(con.DtaNatal, "+00:00", "' + TimeZoneDifUTC + '") DtaNatal_TZ ',
      'FROM enticontat con ',
      'LEFT JOIN entidades ent ON ent.Codigo = con.Codigo ',
      'WHERE con.DtaNatal > "1900-01-01" ',
      'AND ent.Ativo = 1 ',
      'AND DATE_FORMAT(CONVERT_TZ(con.DtaNatal, "+00:00", "' + TimeZoneDifUTC + '"), "%m-%d") BETWEEN "' + DtaIniPesq_Txt + '" AND "' + DtaFimPesq_Txt + '" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.EOF do
      begin
        DataIni := Qry.FieldByName('DtaNatal_TZ').AsDateTime;
        Codigo  := Qry.FieldByName('Codigo').AsInteger;
        Nome    := Qry.FieldByName('Nome').AsString + ' (' + Qry.FieldByName('NomeEnt').AsString + ')';
        //
        InsereItem(PMVMes, PLAgenda, DataIni, DataIni, Cor, Nome, CO_Agend_Anual, AgendaID, Codigo, True, Exibir);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;
*)

(*
procedure TUnitAgenda.InsereAgendaAvulsa(DB: TMySQLdatabase;
  PMVMes: TPlannerMonthView; PLAgenda: TPlanner; Cor: String; AgendaID: Integer;
  TZD_UTC: Double; Exibir: TCalTipo);
var
  Qry: TmySQLQuery;
  DataIni, DataFim: TDateTime;
  Nome, Repete, TimeZoneDifUTC: String;
  Controle: Integer;
  DiaTodo: Boolean;
begin
  Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    ConfiguraPeriodoIniFim(PMVMes, PLAgenda, nil, Exibir, DataIni, DataFim);
    //
    TimeZoneDifUTC := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT its.*, ',
      'CONVERT_TZ(its.DataIni, "+00:00", "' + TimeZoneDifUTC + '") DataIni_TZ, ',
      'CONVERT_TZ(its.DataFim, "+00:00", "' + TimeZoneDifUTC + '") DataFim_TZ ',
      'FROM agendasits its ',
      'LEFT JOIN agencab age ON age.Codigo = its.Codigo ',
      'WHERE its.Codigo=' + Geral.FF0(AgendaID),
      'ORDER BY its.DataIni ASC, its.DiaTodo DESC ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.EOF do
      begin
        Controle := Qry.FieldByName('Controle').AsInteger;
        Repete   := Qry.FieldByName('Recorrencia').AsString;
        DataIni  := Qry.FieldByName('DataIni_TZ').AsDateTime;
        DataFim  := Qry.FieldByName('DataFim_TZ').AsDateTime;
        Nome     := Qry.FieldByName('Nome').AsString;
        //
        if TAgendaTipo(AgendaID) = istATTarefas then
          DiaTodo := True
        else
          DiaTodo := Geral.IntToBool(Qry.FieldByName('DiaTodo').AsInteger);
        //
        InsereItem(PMVMes, PLAgenda, DataIni, DataFim, Cor, Nome, Repete, AgendaID, Controle, DiaTodo, Exibir);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;
*)

(*
procedure TUnitAgenda.InsereAgendaFeriados(DB: TMySQLdatabase;
  PMVMes: TPlannerMonthView; PLAgenda: TPlanner; Cor: String; AgendaID: Integer;
  TZD_UTC: Double; Exibir: TCalTipo);
var
  Qry: TmySQLQuery;
  DataIni, DataFim: TDateTime;
  Nome, Repete, TimeZoneDifUTC: String;
begin
  Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    ConfiguraPeriodoIniFim(PMVMes, PLAgenda, nil, Exibir, DataIni, DataFim);
    TimeZoneDifUTC := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT Motivo, Anual, ',
      'CONVERT_TZ(Data, "+00:00", "' + TimeZoneDifUTC + '") Data_TZ ',
      'FROM feriados ',
      'WHERE Ativo = 1 ',
      'AND ( ',
      'Anual = 1 ',
      'OR ',
      'CONVERT_TZ(Data, "+00:00", "' + TimeZoneDifUTC + '") BETWEEN "' + Geral.FDT(DataIni, 1) + '" AND "' + Geral.FDT(DataFim, 1) + '" ',
      ') ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.EOF do
      begin
        DataIni := Qry.FieldByName('Data_TZ').AsDateTime;
        DataFim := Qry.FieldByName('Data_TZ').AsDateTime;
        Nome    := Qry.FieldByName('Motivo').AsString;
        //
        if (Qry.FieldByName('Anual').AsInteger = 1) then
          Repete := CO_Agend_Anual
        else
          Repete := '';
        //
        InsereItem(PMVMes, PLAgenda, DataIni, DataFim, Cor, Nome, Repete, AgendaID, 0, True, Exibir);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;
*)

(*
procedure TUnitAgenda.InsereAgendaTarefas(DB: TMySQLdatabase;
  MCCalendario: TMonthCalendar; TDLTarefas: TTodoList; Cor: String;
  AgendaID: Integer; TZD_UTC: Double; Exibir: TCalTipo);
var
  Qry: TmySQLQuery;
  Controle, Finalizado, Status, FatID, Prioridade, Duracao: Integer;
  Nome, Descri, Repete, TimeZoneDifUTC: String;
  DataIni, DataFim, DataFinaliz: TDateTime;
begin
  Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    ConfiguraPeriodoIniFim(nil, nil, MCCalendario, Exibir, DataIni, DataFim);
    //
    TimeZoneDifUTC := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT its.*, ',
      'CONVERT_TZ(its.DataIni, "+00:00", "' + TimeZoneDifUTC + '") DataIni_TZ, ',
      'CONVERT_TZ(its.DataFim, "+00:00", "' + TimeZoneDifUTC + '") DataFim_TZ, ',
      'CONVERT_TZ(its.DataFinaliz, "+00:00", "' + TimeZoneDifUTC + '") DataFinaliz_TZ ',
      'FROM agendasits its ',
      'LEFT JOIN agencab age ON age.Codigo = its.Codigo ',
      'WHERE its.Codigo=' + Geral.FF0(AgendaID),
      'AND  its.Empresa=' + Geral.FF0(DmodG.QrFiliLogCodigo.Value),
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.EOF do
      begin
        Controle    := Qry.FieldByName('Controle').AsInteger;
        Finalizado  := Qry.FieldByName('Finalizado').AsInteger;
        Status      := Qry.FieldByName('Status').AsInteger;
        Nome        := Qry.FieldByName('Nome').AsString;
        Descri      := Qry.FieldByName('Descri').AsString;
        FatID       := Qry.FieldByName('FatID').AsInteger;
        DataIni     := Qry.FieldByName('DataIni_TZ').AsDateTime;
        DataFim     := Qry.FieldByName('DataFim_TZ').AsDateTime;
        Prioridade  := Qry.FieldByName('Prioridade').AsInteger;
        Duracao     := Qry.FieldByName('Duracao').AsInteger;
        DataFinaliz := Qry.FieldByName('DataFinaliz').AsDateTime;
        Repete      := Qry.FieldByName('Recorrencia').AsString;
        //
        InsereItemTarefa(TDLTarefas, MCCalendario, Duracao, DataIni, DataFim,
          DataFinaliz, Cor, Nome, Descri, Repete, Prioridade, Finalizado,
          Status, FatID, AgendaID, Controle, Exibir);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;
*)

(*
procedure TUnitAgenda.CriaItem(Item: TPlannerItem; PeriodoIni, DataIni,
  DataFim: TDateTime; Descri, Cor: String; AgendaID, ItemID: Integer; DiaTodo: Boolean);
var
  Posi: Integer;
  DtaIni, DtaFim: TDateTime;
  Titulo, Nome, Ini, Fim: String;
begin
  if Item <> nil then
  begin
    DtaIni := DataIni;
    DtaFim := DataFim;
    Ini    := Geral.FDT(DtaIni, 3);
    Fim    := Geral.FDT(DtaFim, 3);
    //
    if DiaTodo = True then
    begin
      if Ini <> Fim then
        Titulo := Ini + ' - Dia todo ' + Fim + ' - Dia todo. '
      else
        Titulo := Ini + ' - Dia todo. ';
    end else
    begin
      if Ini <> Fim then
        Titulo := Geral.FDT(DtaIni, 106) + ' at� ' + Geral.FDT(DtaFim, 106) + '. '
      else
        Titulo := Ini + ' - ' + Geral.FDT(DtaIni, 102) + ' at� ' + Geral.FDT(DtaFim, 102) + '. ';
    end;

    case TAgendaTipo(AgendaID) of
      istATFeriados:
        Nome := 'Feriado - ' + Descri;
      istATAniversarios:
        Nome := 'Anivers�rio - ' + Descri;
      else
        Nome := Descri;
    end;

    Posi := Trunc(DataIni - PeriodoIni);

    if (Posi < 0) or (Posi > 7) then
      Posi := 0;

    Item.ReadOnly      := True;
    Item.FixedPos      := True;
    Item.FixedPosition := True;
    Item.FixedSize     := True;
    Item.FixedTime     := True;
    //
    Item.Color            := DmkImg.HexToTColor(Cor);
    Item.ColorTo          := DmkImg.HexToTColor(Cor);
    Item.Font.Color       := clWhite;
    Item.SelectColor      := DmkImg.HexToTColor(Cor);
    Item.SelectColorTo    := DmkImg.HexToTColor(Cor);
    Item.SelectFontColor  := clWhite;
    Item.TrackColor       := clWhite;
    Item.TrackSelectColor := clWhite;
    Item.Shadow           := False;
    //
    Item.ItemStartTime := DataIni;
    Item.ItemEndTime   := DataFim;
    Item.ItemPos       := Posi;
    Item.ID            := ItemID;
    Item.Tag           := AgendaID;
    Item.Text.Add(Titulo);
    Item.Text.Add(Nome);
  end;
end;
*)

(*
procedure TUnitAgenda.CriaItemTarefa(Item: TTodoItem; Duracao: Double; DataIni,
  DataFim, DataFinaliz: TDateTime; Nome, Descri: String; Prioridade,
  Finalizado, Status, FatID, ItemID: Integer);
begin
  Item.Category       := ObtemDescriItemDeListaCategorias(FatID);
  Item.Complete       := Finalizado = 100;
  Item.Completion     := Finalizado;
  Item.CompletionDate := DataFinaliz;
  Item.CreationDate   := DataIni;
  Item.DueDate        := DataFim;
  Item.Notes.Text     := Descri;
  Item.Subject        := Nome;
  Item.TotalTime      := Duracao;
  Item.Tag            := ItemID;
  Item.Priority       := IntToPriority(Prioridade);
  Item.Status         := IntToStatus(Status);
end;
*)

(*
procedure TUnitAgenda.CriaItemTarefaRecorrente(DataBase: TMySQLdatabase; Data: TDate);

  function VerificaSeCriaItem(DtaIni, DtaFim: TDate; Repeticoes: String): Boolean;
  var
    I, Tot: Integer;
    RecIni, Dta: TDate;
    Recor: TRecurrencyHandler;
  begin
    Result := False;
    Recor  := TRecurrencyHandler.Create;
    try
      with Recor do
      begin
        Recurrency := Repeticoes;
        StartTime  := DtaIni;
        EndTime    := DtaFim;
        TimeSpan   := Data;
        Parse;
        Generate;
        //
        Tot := Dates.Count;
        //
        for I := 0 to Tot - 1 do
        begin
          Dta := Dates[I].StartDate;
          //
          if Geral.FDT(Data, 1) = Geral.FDT(Dta, 1) then
          begin
            Result := True;
            Exit;
          end;
        end;
      end;
    finally
      Recor.Free;
    end;
  end;

var
  ControleDB, Controle: Integer;
  DataExe: TDate;
  Recorrencia: String;
  Qry, QryUpd: TMySQLQuery;
begin
  Qry    := TmySQLQuery.Create(DataBase);
  QryUpd := TmySQLQuery.Create(DataBase);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT its.* ',
      'FROM agentarits its ',
      'WHERE its.Codigo = -1 ',
      'AND its.Finalizado = 0 ',
      'AND its.RecID = 0 ',
      'AND its.CliIntSync IN(0, ' + Geral.FF0(VAR_WEB_USR_ENT) + ')',
      'AND its.LastAcao<>' + Geral.FF0(Integer(laDel)),
      'AND its.Controle NOT IN ',
      '( ',
      'SELECT RecID ',
      'FROM agentarits ',
      'WHERE Codigo = -1 ',
      'AND RecID <> 0 ',
      'AND CliIntSync IN(0, ' + Geral.FF0(VAR_WEB_USR_ENT) + ')',
      'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
      'AND DataExe = "' + Geral.FDT(Data, 1) + '" ',
      ') ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.Eof do
      begin
        DataExe     := Qry.FieldByName('DataExe').AsDateTime;
        Recorrencia := Qry.FieldByName('Recorrencia').AsString;
        ControleDB  := Qry.FieldByName('Controle').AsInteger;
        //
        if VerificaSeCriaItem(DataExe, DataExe, Recorrencia) then
        begin
          Controle := Grl_DmkDB.ObtemCodigoInt_Sinc('agentarits', 'Controle', QryUpd,
                      DataBase, stIns, stDesktop);
          if Controle <> 0 then
          begin
            if not UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1,
              'agentarits', TMeuDB, ['Controle'], [ControleDB],
              ['Controle', 'Recorrencia', 'RecID'], [Controle, '', ControleDB],
              '', True, nil, nil) then
            begin
              Geral.MB_Erro('Falha ao atualizar itens recorrentes!');
              Exit;
            end;
          end;
        end;
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
    QryUpd.Free;
  end;
end;
*)

procedure TUnitAgenda.DataToDiaTodo(var DataIni, DataFim: TDateTime);
var
  Dia, Mes, Ano: Word;
begin
  DecodeDate(DataIni, Ano, Mes, Dia);
  //
  DataIni := EncodeDateTime(Ano, Mes, Dia, 0, 0, 1, 0);
  //
  DecodeDate(DataFim, Ano, Mes, Dia);
  //
  DataFim := EncodeDateTime(Ano, Mes, Dia, 23, 59, 59, 0);
end;

function TUnitAgenda.ExcluiAgenda(Codigo: Integer; DB: TMySQLdatabase): Boolean;
var
  Qry: TMySQLQuery;
begin
  Result := False;
  //
  if Codigo > 0 then
  begin
    Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
        'SELECT * ',
        'FROM agendasits ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      if Qry.RecordCount = 0 then
      begin
        if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o desta agenda?',
          'agendas', 'Codigo', Codigo, DB) = ID_YES
        then
          Result := True;
      end else
        Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak + 'Motivo: Esta agenda possui �tens!');
    finally
      Qry.Free;
    end;
  end else
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak + 'Motivo: Esta agenda n�o pode ser exclu�da!');
end;

function TUnitAgenda.QuaisItens(Msg: String): TSelType;
begin
  Result := istNenhum;
  //
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      Caption := 'XXX-XXXXX-999 :: ' + Msg;
      Width   := 500;
      //
      ShowModal;
      if FSelecionou then
        Result := FEscolha;
      Destroy;
    end;
  end;
end;

procedure TUnitAgenda.CriaExcecao(Controle: Integer; Recorrencia: String;
  Data: TDateTime; DB: TMySQLdatabase);
var
  Qry, QryUpd: TmySQLQuery;
  Txt, DtaExce, TimeZoneDifUTC: String;
  DataIni: TDateTime;
  Dia, Mes, Ano, Hor, Min, Seg, MSeg: Word;
begin
  Qry    := TmySQLQuery.Create(TDataModule(DB.Owner));
  QryUpd := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
        'SELECT emp.TZD_UTC ',
        'FROM agendasits its ',
        'LEFT JOIN agencab age ON age.Codigo = its.Codigo ',
        'LEFT JOIN paramsemp emp ON emp.Codigo = IF(age.Codigo = -3, its.Empresa, age.Empresa) ',
        'WHERE its.Controle=' + Geral.FF0(Controle),
        '']);
    if Qry.RecordCount > 0 then
    begin
      QryUpd.DataBase := DB;
      //
      TimeZoneDifUTC := dmkPF.TZD_UTC_FloatToSignedStr(Qry.FieldByName('TZD_UTC').AsFloat);
      DataIni        := DmkWeb.DataTimeZoneToNewTimeZone(DB, Qry, Geral.FDT(Data, 9), TimeZoneDifUTC, '+00:00');
      //
      DecodeDateTime(DataIni, Ano, Mes, Dia, Hor, Min, Seg, MSeg);
      //
      DtaExce := FormatFloat('0000', Ano) + FormatFloat('00', Mes) +
                 FormatFloat('00', Dia) + 'T000000';
      //
      if Pos('EXDATES:', Recorrencia) > 0 then
      begin
        if Pos(DtaExce, Recorrencia) > 0 then
          Exit;
        Txt := Recorrencia + ',';
      end else
        Txt := Recorrencia + '#EXDATES:';
      //
      Txt := Txt + DtaExce + '/' + DtaExce;
      //
      UMyMod.SQLInsUpd(QryUpd, stUpd, 'agendasits', False,
        ['Recorrencia'], ['Controle'],
        [Txt], [Controle], True);
    end;
  finally
    Qry.Free;
    QryUpd.Free;
  end;
end;


function TUnitAgenda.ValidaItemAgenda(const Controle: Integer;
  const DB: TMySQLdatabase; var Recorrencia: String): Boolean;
var
  Qry: TMySQLQuery;
begin
  Result      := False;
  Recorrencia := '';
  Qry         := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT * ',
      'FROM agendasits ',
      'WHERE Controle=' + Geral.FF0(Controle),
      'AND (Codigo > 0 OR Codigo = -3)',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Recorrencia := Qry.FieldByName('Recorrencia').AsString;
      Result      := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnitAgenda.ExcluiItemAgenda(Controle: Integer; Data: TDateTime;
  DB: TMySQLdatabase): Boolean;

  procedure ExcluiRegistro();
  begin
    UMyMod.ExcluiRegistroInt1('', 'agendasits', 'Controle', Controle, DB);
  end;

var
  Todos: Boolean;
  Selecionou: TSelType;
  Msg, Recorrencia: String;
begin
  Result := False;
  //
  if Controle > 0 then
  begin
    if ValidaItemAgenda(Controle, DB, Recorrencia) then
    begin
      if (Recorrencia <> '') then
      begin
        Selecionou := QuaisItens('Quais itens deseja excluir?');
        //
        if Selecionou = istNenhum then Exit;
        //
        if Selecionou = istTodos then
          Todos := True
        else
          Todos := False;
      end else
      begin
        if Geral.MB_Pergunta('Confirma a exclus�o do item atual?') = ID_YES then
          Todos := True
        else
          Exit;
      end;
      if (Todos = True) then
        ExcluiRegistro
      else
        CriaExcecao(Controle, Recorrencia, Data, DB);
      Result := True;
    end else
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Motivo: Este item n�o foi localizado ou n�o pode ser exclu�do neste local!');
  end else
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak + 'Motivo: Este item n�o pode ser exclu�do!');
end;

(*
procedure TUnitAgenda.InsereItem(PMVMes: TPlannerMonthView; PLAgenda: TPlanner;
  DataIni, DataFim: TDateTime; Cor, Descricao, Repeticoes: String; AgendaID,
  ItemID: Integer; DiaTodo: Boolean; Exibir: TCalTipo);

  procedure InsereItemRepeticoes();
  var
    pr: trecurrencyhandler;
    Item: TPlannerItem;
    i: Integer;
    PerIni, PerFim: TDateTime;
  begin
    ConfiguraPeriodoIniFim(PMVMes, PLAgenda, nil, Exibir, PerIni, PerFim);
    DataToDiaTodo(PerIni, PerFim);
    //
    if DiaTodo then
      DataToDiaTodo(DataIni, DataFim);
    //
    pr            := TRecurrencyHandler.Create;
    pr.Recurrency := Repeticoes;
    pr.StartTime  := DataIni;
    pr.EndTime    := DataFim;
    pr.TimeSpan   := PerFim;
    pr.Parse;
    pr.Generate;

    for i := 1 to pr.Dates.Count do
    begin
      DataIni := pr.Dates[i - 1].StartDate;
      DataFim := pr.Dates[i - 1].EndDate;
      //
      if (DataIni >= PerIni) and (PerFim >= DataFim) then
      begin
        if Exibir = istCTMes then
        begin
          Item := PMVMes.Items.Add;
          CriaItem(Item, PerIni, DataIni, DataFim, Descricao, Cor, AgendaID, ItemID, DiaTodo);
        end else
        if Exibir = istCTTarefas then
        begin
          //N�o implementado!
          Exit;
        end else
        begin
          Item := PLAgenda.Items.Add;
          CriaItem(Item, PerIni, DataIni, DataFim, Descricao, Cor, AgendaID, ItemID, DiaTodo);
        end;
      end;
    end;
    pr.Free;
  end;

  procedure InsereItemAtual();
  var
    Item: TPlannerItem;
    PerIni, PerFim: TDateTime;
  begin
    ConfiguraPeriodoIniFim(PMVMes, PLAgenda, nil, Exibir, PerIni, PerFim);
    DataToDiaTodo(PerIni, PerFim);
    //
    if DiaTodo then
      DataToDiaTodo(DataIni, DataFim);
    //
    if (DataIni >= PerIni) and (PerFim >= DataFim) then
    begin
      if Exibir = istCTMes then
      begin
        Item := PMVMes.Items.Add;
        CriaItem(Item, PerIni, DataIni, DataFim, Descricao, Cor, AgendaID, ItemID, DiaTodo);
      end else
      if Exibir = istCTTarefas then
      begin
        //N�o implementado!
        Exit;
      end else
      begin
        Item := PLAgenda.Items.Add;
        CriaItem(Item, PerIni, DataIni, DataFim, Descricao, Cor, AgendaID, ItemID, DiaTodo);
      end;
    end;
  end;

begin
  if Repeticoes <> '' then
    InsereItemRepeticoes
  else
    InsereItemAtual;
end;
*)

(*
procedure TUnitAgenda.InsereItemTarefa(TDLTarefas: TTodoList;
  MCCalendario: TMonthCalendar; Duracao: Double; DataIni, DataFim,
  DataFinaliz: TDateTime; Cor, Nome, Descricao, Repeticoes: String; Prioridade,
  Finalizado, Status, FatID, AgendaID, ItemID: Integer; Exibir: TCalTipo);

  procedure InsereItemRepeticoes();
  var
    pr: trecurrencyhandler;
    Item: TTodoItem;
    i: Integer;
    PerIni, PerFim: TDateTime;
  begin
    ConfiguraPeriodoIniFim(nil, nil, MCCalendario, Exibir, PerIni, PerFim);
    //
    pr            := TRecurrencyHandler.Create;
    pr.Recurrency := Repeticoes;
    pr.StartTime  := DataIni;
    pr.EndTime    := DataFim;
    pr.TimeSpan   := PerFim;
    pr.Parse;
    pr.Generate;

    for i := 1 to pr.Dates.Count do
    begin
      DataIni := pr.Dates[i - 1].StartDate;
      DataFim := pr.Dates[i - 1].EndDate;
      //
      if (DataIni >= PerIni) and (PerFim >= DataFim) then
      begin
        if Exibir = istCTTarefas then
        begin
          Item := TDLTarefas.Items.Add;
          //
          CriaItemTarefa(Item, Duracao, DataIni, DataFim, DataFinaliz, Nome, Descricao,
            Prioridade, Finalizado, Status, FatID, ItemID);
          Exit;
        end else
        begin
          //N�o implementado!
        end;
      end;
    end;
    pr.Free;
  end;

  procedure InsereItemAtual();
  var
    Item: TTodoItem;
    PerIni, PerFim: TDateTime;
  begin
    ConfiguraPeriodoIniFim(nil, nil, MCCalendario, Exibir, PerIni, PerFim);
    //
    if (DataIni >= PerIni) and (PerFim >= DataFim) then
    begin
      if Exibir = istCTTarefas then
      begin
        Item := TDLTarefas.Items.Add;
        //
        CriaItemTarefa(Item, Duracao, DataIni, DataFim, DataFinaliz, Nome, Descricao,
          Prioridade, Finalizado, Status, FatID, ItemID);
        Exit;
      end else
      begin
        //N�o implementado!
      end;
    end;
  end;

begin
  if Repeticoes <> '' then
    InsereItemRepeticoes
  else
    InsereItemAtual;
end;
*)

(*
procedure TUnitAgenda.InsereTarefaItem(Lista: TTodoList; Controle, Realizado,
  Status, OriID, Prioridade: Integer; Nome, ResponsTxt: String; DataExe,
  DataFim: TDate; Duracao: Double; Finalizado: Boolean);
var
  Item: TTodoItem;
begin
  Item := Lista.Items.Add;
  Item.Tag            := Controle;
  Item.Completion     := Realizado;
  Item.Complete       := Finalizado;
  Item.Status         := IntToStatus(Status);
  Item.Subject        := Nome;
  Item.Category       := CO_Agend_Categorias_Txt[OriID];
  Item.DueDate        := DataExe;
  Item.Priority       := IntToPriority(Prioridade);
  Item.TotalTime      := Duracao;
  Item.CompletionDate := DataFim;
  Item.Resource       := ResponsTxt;
end;
*)

procedure TUnitAgenda.InsereTarefaLista(ListView: TListView; Grupo: TGrupoAgenTar;
  Id: Integer; Texto: String; TotalIts: Integer);
var
  Item: TListItem;
  I, GrupoId: Integer;
begin
  GrupoId := GrlAgenda.ObtemGrupoID(Grupo);
  Item    := ListView.Items.Add;
  Item.GroupID    := GrupoId;
  Item.Caption    := Texto;
  Item.StateIndex := Id;
  Item.ImageIndex := 1;
  //
  if Id <> 0 then
    Item.SubItems.Add(Geral.FF0(TotalIts))
  else
    Item.SubItems.Add('');
end;

(*
function TUnitAgenda.IntToPriority(Priority: Integer): TTodoPriority;
begin
  case Priority of
    0:
      Result := tpLowest;
    1:
      Result := tpLow;
    2:
      Result := tpNormal;
    3:
      Result := tpHigh;
    else
      Result := tpHighest;
  end;

end;
*)

(*
function TUnitAgenda.IntToStatus(Status: Integer): TTodoStatus;
begin
  case Status of
    0:
      Result := tsNotStarted;
    1:
      Result := tsInProgress;
    2:
      Result := tsCompleted;
    else
      Result := tsDeferred;
  end;
end;
*)

function TUnitAgenda.LocalizaGrupo(Lista: TListView; Codigo: Integer): Integer;
var
  I: Integer;
  Item: TListItem;
begin
  Result := -1;
  //
  if (Lista.Items.Count > 0) and (Codigo <> 0) then
  begin
    for I := 0 to Lista.Items.Count - 1 do
    begin
      Item := Lista.Items[I];
      //
      if Item.StateIndex = Codigo then
      begin
        Result := I;
      end;
    end;
  end;
end;

(*
function TUnitAgenda.LocalizaItem(Lista: TTodoList; Controle: Integer): Integer;
var
  I: Integer;
  Item: TTodoItem;
begin
  Result := -1;
  //
  if (Lista.Items.Count > 0) and (Controle <> 0) then
  begin
    for I := 0 to Lista.Items.Count - 1 do
    begin
      Item := Lista.Items[I];
      //
      if Item.Tag = Controle then
      begin
        Result := I;
      end;
    end;
  end;
end;
*)

(*
procedure TUnitAgenda.ReopenAgendas(Query: TMySQLquery; DB: TMySQLdatabase;
  LVAgendas: TListView; MCCalendario: TMonthCalendar; PMVMes: TPlannerMonthView;
  PLAgenda: TPlanner; TDLTarefas: TTodoList; Exibir: TCalTipo);
var
  Codigo, Mostra, GrupoId: Integer;
  AgendaNome, SQLCompl, Cor: String;
  TZD_UTC: Double;
  Item: TListItem;
begin
  if TCalTipo(Exibir) = istCTTarefas then
    SQLCompl := 'AND age.Codigo = ' + Geral.FF0(CO_Agend_Tarefas)
  else
    SQLCompl := '';
  //
  PMVMes.Items.Clear;
  PLAgenda.Items.Clear;
  TDLTarefas.Items.Clear;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DB, [
    'SELECT age.*, emp.TZD_UTC ',
    'FROM agencab age ',
    'LEFT JOIN paramsemp emp ON emp.Codigo = age.Empresa',
    'WHERE age.Ativo = 1 ',
    SQLCompl,
    '']);
  if Query.RecordCount > 0 then
  begin
    if LVAgendas <> nil then
    begin
      LVAgendas.Clear;
      LVAgendas.Items.BeginUpdate;
    end;
    try
      while not Query.EOF do
      begin
        Codigo     := Query.FieldByName('Codigo').AsInteger;
        AgendaNome := Query.FieldByName('Nome').AsString;
        Cor        := Query.FieldByName('Cor').AsString;
        Mostra     := Query.FieldByName('Mostra').AsInteger;
        TZD_UTC    := Query.FieldByName('TZD_UTC').AsFloat;
        //
        if LVAgendas <> nil then
        begin
          if Codigo < 0 then
            GrupoId := 0
          else
            GrupoId := 1;
          //
          Item := LVAgendas.Items.Add;
          Item.GroupID    := GrupoId;
          Item.Caption    := AgendaNome;
          Item.StateIndex := Codigo;
          Item.ImageIndex := UnAgendaAux.ObtemIndexCorAgenda(DmkImg.HexToTColor(Cor));
          Item.Checked    := Geral.IntToBool(Mostra);
        end;
        //
        if Mostra = 1 then
        begin
          if TCalTipo(Exibir) <> istCTTarefas then
          begin
            if TAgendaTipo(Codigo) = istATFeriados then
              InsereAgendaFeriados(DB, PMVMes, PLAgenda, Cor, Codigo, TZD_UTC, Exibir)
            else
            if TAgendaTipo(Codigo) = istATAniversarios then
              InsereAgendaAniversarios(DB, PMVMes, PLAgenda, Cor, Codigo, TZD_UTC, Exibir)
            else
              InsereAgendaAvulsa(DB, PMVMes, PLAgenda, Cor, Codigo, TZD_UTC, Exibir);
          end else
            InsereAgendaTarefas(DB, MCCalendario, TDLTarefas, Cor, Codigo, TZD_UTC, Exibir);
        end;
        Query.Next;
      end;
    finally
      if LVAgendas <> nil then
      begin
        LVAgendas.Items.EndUpdate;
      end;
    end;
  end;
end;
*)

(*
function TUnitAgenda.StatusToInt(Status: TTodoStatus): Integer;
begin
  case Status of
    tsNotStarted:
      Result := 0;
    tsInProgress:
      Result := 1;
    tsCompleted:
      Result := 2;
    tsDeferred:
      Result := 3;
  end;
end;
*)

//Janelas
procedure TUnitAgenda.MostraAgendaCab(SQLType: TSQLType; Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgendaCab, FmAgendaCab, afmoNegarComAviso) then
  begin
    FmAgendaCab.MostraEdicao(SQLType, Codigo);
    FmAgendaCab.ShowModal;
    FmAgendaCab.Destroy;
  end;
end;

procedure TUnitAgenda.MostraAgendaIts(SQLType: TSQLType; Codigo,
  Controle: Integer; DataPadrao: TDate; DB: TMySQLdatabase);

  procedure MostraAgndaIts();
  var
    Selecionou: TSelType;
    Recorrencia: String;
    Todos: Boolean;
  begin
    if SQLType = stUpd then
    begin
      if ValidaItemAgenda(Controle, DB, Recorrencia) then
      begin
        if (Recorrencia <> '') then
        begin
          Selecionou := QuaisItens('Quais itens deseja alterar?');
          //
          if Selecionou = istNenhum then Exit;
          //
          if Selecionou = istTodos then
            Todos := True
          else
            Todos := False;
        end else
          Todos := True;
      end else
      begin
        Geral.MB_Aviso('Altera��o abortada!' + sLineBreak +
          'Motivo: Este item n�o foi localizado ou n�o pode ser alterado neste local!');
        Exit;
      end;
    end;
   if DBCheck.CriaFm(TFmAgendaIts, FmAgendaIts, afmoNegarComAviso) then
    begin
      FmAgendaIts.MostraEdicao(SQLType, TAgendaTipo(Codigo), Codigo, Controle, DataPadrao, Todos);
      FmAgendaIts.ShowModal;
      FmAgendaIts.Destroy;
    end;
  end;

var
  Form: TForm;
begin
  if Codigo < 0 then
  begin
    if TAgendaTipo(Codigo) = istATFeriados then
    begin
      if DBCheck.CriaFm(TFmFeriados, FmFeriados, afmoNegarComAviso) then
      begin
        FmFeriados.ShowModal;
        FmFeriados.Destroy;
      end;
    end else
    if TAgendaTipo(Codigo) = istATAniversarios then
    begin
      DModG.CadastroDeEntidade(Controle, fmcadEntidade2, fmcadEntidade2, True);
    end else
    if TAgendaTipo(istATTarefas) = istATTarefas then
    begin
      MostraAgndaIts;
    end;
  end else
  begin
    MostraAgndaIts;
  end;
end;

function TUnitAgenda.ObtemCodigoGrupo(DB: TMySQLdatabase; Controle: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT Codigo ',
      'FROM agentarits ',
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnitAgenda.ObtemDescriItemDeListaCategorias(FatID: Integer): String;
var
  Item: Integer;
begin
  Item := ObtemItemDeListaCategorias(FatID) + 1;
  //
  Result := CO_Agend_Categorias_Txt[Item];
end;

function TUnitAgenda.ObtemFatIDDeListaCategorias(ItemLista: Integer): Integer;
var
  Item: Integer;
begin
  Item := ItemLista + 1;
  //
  case Item of
      0: Result := 0;
      1: Result := VAR_FATID_8001;
    else Result := -1;
  end;
end;

function TUnitAgenda.ObtemItemDeListaCategorias(FatID: Integer): Integer;
begin
  case FatID of
    0:
      Result := 0;
    VAR_FATID_8001:
      Result := 1;
    else
      Result := -1;
  end;
end;

(*
function TUnitAgenda.PriorityToInt(Priority: TTodoPriority): Integer;
begin
  case Priority of
    tpLowest:
      Result := 0;
    tpLow:
      Result := 1;
    tpNormal:
      Result := 2;
    tpHigh:
      Result := 3;
    tpHighest:
      Result := 4;
  end;
end;
*)

end.
