object FmAgendaCab: TFmAgendaCab
  Left = 339
  Top = 185
  Caption = 'AGE-GEREN-002 :: Cadastro de Agendas'
  ClientHeight = 617
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 633
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 574
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 515
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 308
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 308
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 308
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 633
    Height = 417
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 633
      Height = 417
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 633
        Height = 417
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Label12: TLabel
          Left = 17
          Top = 16
          Width = 40
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Nome:'
        end
        object Label4: TLabel
          Left = 17
          Top = 119
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object Label1: TLabel
          Left = 17
          Top = 289
          Width = 24
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cor:'
        end
        object Label3: TLabel
          Left = 17
          Top = 68
          Width = 58
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Empresa:'
        end
        object EdNome: TdmkEdit
          Left = 17
          Top = 37
          Width = 598
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 100
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object MeDescri: TMemo
          Left = 17
          Top = 140
          Width = 598
          Height = 143
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          ScrollBars = ssVertical
          TabOrder = 3
        end
        object EdEmpresa: TdmkEditCB
          Left = 17
          Top = 89
          Width = 54
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Filial'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 74
          Top = 89
          Width = 541
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 2
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CkDiaTodo: TCheckBox
          Left = 283
          Top = 308
          Width = 222
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Sincronizar com dispositivos'
          TabOrder = 4
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 476
    Width = 633
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 629
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 530
    Width = 633
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 454
      Top = 18
      Width = 177
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 452
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GridPanel1: TGridPanel
    Left = 17
    Top = 367
    Width = 259
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    BevelOuter = bvNone
    ColumnCollection = <
      item
        Value = 16.666666666666670000
      end
      item
        Value = 16.666666666666670000
      end
      item
        Value = 16.666666666666670000
      end
      item
        Value = 16.666666666666670000
      end
      item
        Value = 16.666666666666670000
      end
      item
        Value = 16.666666666666670000
      end>
    ControlCollection = <
      item
        Column = 1
        Control = Shape2
        Row = 0
      end
      item
        Column = 2
        Control = Shape3
        Row = 0
      end
      item
        Column = 3
        Control = Shape4
        Row = 0
      end
      item
        Column = 4
        Control = Shape5
        Row = 0
      end
      item
        Column = 5
        Control = Shape6
        Row = 0
      end
      item
        Column = 0
        Control = Shape7
        Row = 1
      end
      item
        Column = 1
        Control = Shape8
        Row = 1
      end
      item
        Column = 2
        Control = Shape9
        Row = 1
      end
      item
        Column = 3
        Control = Shape10
        Row = 1
      end
      item
        Column = 4
        Control = Shape11
        Row = 1
      end
      item
        Column = 5
        Control = Shape12
        Row = 1
      end
      item
        Column = 0
        Control = Shape1
        Row = 0
      end>
    RowCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end>
    TabOrder = 4
    object Shape2: TShape
      Left = 43
      Top = 0
      Width = 43
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 11867974
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 42
      ExplicitWidth = 42
    end
    object Shape3: TShape
      Left = 86
      Top = 0
      Width = 43
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 9765005
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 84
      ExplicitWidth = 42
    end
    object Shape4: TShape
      Left = 129
      Top = 0
      Width = 43
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 1608192
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 126
      ExplicitWidth = 41
    end
    object Shape5: TShape
      Left = 172
      Top = 0
      Width = 43
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 2509010
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 167
    end
    object Shape6: TShape
      Left = 215
      Top = 0
      Width = 44
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 10060288
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 210
      ExplicitWidth = 48
    end
    object Shape7: TShape
      Left = 0
      Top = 43
      Width = 43
      Height = 44
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 4135596
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitWidth = 42
    end
    object Shape8: TShape
      Left = 43
      Top = 43
      Width = 43
      Height = 44
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 11423964
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 42
      ExplicitWidth = 42
    end
    object Shape9: TShape
      Left = 86
      Top = 43
      Width = 43
      Height = 44
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 3378943
      Pen.Color = clWhite
      Pen.Width = 5
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 84
      ExplicitWidth = 42
    end
    object Shape10: TShape
      Left = 129
      Top = 43
      Width = 43
      Height = 44
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 48003
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 126
      ExplicitWidth = 41
    end
    object Shape11: TShape
      Left = 172
      Top = 43
      Width = 43
      Height = 44
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 11842563
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 167
    end
    object Shape12: TShape
      Left = 215
      Top = 43
      Width = 44
      Height = 44
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 16757598
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      OnMouseUp = Shape1MouseUp
      ExplicitLeft = 210
      ExplicitWidth = 48
    end
    object Shape1: TShape
      Left = 0
      Top = 0
      Width = 43
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Brush.Color = 13005312
      ParentShowHint = False
      Pen.Color = clWhite
      Pen.Width = 0
      Shape = stSquare
      ShowHint = False
      OnMouseUp = Shape1MouseUp
      ExplicitWidth = 42
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 368
    Top = 11
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 272
    Top = 192
  end
end
