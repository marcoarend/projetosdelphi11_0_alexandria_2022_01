object DmModAgenda: TDmModAgenda
  OldCreateOrder = False
  Height = 381
  Width = 850
  object QrLocEve: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, QuestaoTyp, QuestaoCod, QuestaoExe'
      'FROM agendaeve'
      'WHERE QuestaoTyp=?'
      'AND QuestaoCod=?'
      'AND QuestaoExe=?')
    Left = 24
    Top = 16
    object QrLocEveCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocEveQuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
    end
    object QrLocEveQuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
    end
    object QrLocEveQuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
    end
  end
  object QrEOSs: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      
        'SELECT Codigo, AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCo' +
        'rHin'
      'FROM estatusoss'
      'WHERE Codigo=2002')
    Left = 24
    Top = 64
    object QrEOSsAgeCorIni: TIntegerField
      FieldName = 'AgeCorIni'
    end
    object QrEOSsAgeCorFim: TIntegerField
      FieldName = 'AgeCorFim'
    end
    object QrEOSsAgeCorDir: TSmallintField
      FieldName = 'AgeCorDir'
    end
    object QrEOSsAgeCorFon: TIntegerField
      FieldName = 'AgeCorFon'
    end
    object QrEOSsAgeCorHin: TIntegerField
      FieldName = 'AgeCorHin'
    end
    object QrEOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
