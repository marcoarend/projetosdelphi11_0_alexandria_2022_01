unit AgenGrCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmAgenGrCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrAgenGrCab: TmySQLQuery;
    DsAgenGrCab: TDataSource;
    QrAgenGrEnt: TmySQLQuery;
    DsAgenGrEnt: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrAgenGrCabCodigo: TIntegerField;
    QrAgenGrCabNome: TWideStringField;
    QrAgenGrCabColLarg: TIntegerField;
    QrAgenGrCabInterMinut: TIntegerField;
    QrAgenGrEntCodigo: TIntegerField;
    QrAgenGrEntControle: TIntegerField;
    QrAgenGrEntEntidade: TIntegerField;
    QrAgenGrEntCorEnt: TIntegerField;
    QrAgenGrEntNO_ENT: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdColLarg: TdmkEdit;
    EdInterMinut: TdmkEdit;
    Label8: TLabel;
    CBCorMul: TColorBox;
    DBCorMul: TColorBox;
    Label10: TLabel;
    QrAgenGrCabCorMul: TIntegerField;
    SbCor: TSpeedButton;
    QrAgenGrEntSigla: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAgenGrCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAgenGrCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrAgenGrCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrAgenGrCabBeforeClose(DataSet: TDataSet);
    procedure SbCorClick(Sender: TObject);
    procedure DGDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraAgenGrEnt(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenAgenGrEnt(Controle: Integer);
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmAgenGrCab: TFmAgenGrCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, AgenGrEnt;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAgenGrCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAgenGrCab.MostraAgenGrEnt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmAgenGrEnt, FmAgenGrEnt, afmoNegarComAviso) then
  begin
    FmAgenGrEnt.ImgTipo.SQLType := SQLType;
    FmAgenGrEnt.FQrCab := QrAgenGrCab;
    FmAgenGrEnt.FDsCab := DsAgenGrCab;
    FmAgenGrEnt.FQrIts := QrAgenGrEnt;
    if SQLType = stIns then
      //
    else
    begin
      FmAgenGrEnt.EdControle.ValueVariant := QrAgenGrEntControle.Value;
      FmAgenGrEnt.EdEntidade.ValueVariant := QrAgenGrEntEntidade.Value;
      FmAgenGrEnt.CBEntidade.KeyValue     := QrAgenGrEntEntidade.Value;
      FmAgenGrEnt.CBCorEnt.Selected       := QrAgenGrEntCorEnt.Value;
      FmAgenGrEnt.EdSigla.Text            := QrAgenGrEntSigla.Value;
    end;
    FmAgenGrEnt.ShowModal;
    FmAgenGrEnt.Destroy;
  end;
end;

procedure TFmAgenGrCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrAgenGrCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrAgenGrCab, QrAgenGrEnt);
end;

procedure TFmAgenGrCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrAgenGrCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrAgenGrEnt);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrAgenGrEnt);
end;

procedure TFmAgenGrCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAgenGrCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAgenGrCab.DefParams;
begin
  VAR_GOTOTABELA := 'agengrcab';
  VAR_GOTOMYSQLTABLE := QrAgenGrCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM agengrcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmAgenGrCab.DGDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorUnica: Integer;
begin
  if Column.FieldName = 'CorEnt' then
  begin
    CorUnica := QrAgenGrEntCorEnt.Value;
    MyObjects.DesenhaTextoEmDBGrid(DGDados, Rect, CorUnica, CorUnica,
      taLeftJustify, Geral.FF0(QrAgenGrEntCorEnt.Value));
  end;
end;

procedure TFmAgenGrCab.ItsAltera1Click(Sender: TObject);
begin
  MostraAgenGrEnt(stUpd);
end;

procedure TFmAgenGrCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmAgenGrCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAgenGrCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAgenGrCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'AgenGrEnt', 'Controle', QrAgenGrEntControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrAgenGrEnt,
      QrAgenGrEntControle, QrAgenGrEntControle.Value);
    ReopenAgenGrEnt(Controle);
  end;
end;

procedure TFmAgenGrCab.ReopenAgenGrEnt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgenGrEnt, Dmod.MyDB, [
  'SELECT age.*, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM agengrent age ',
  'LEFT JOIN entidades ent ON ent.Codigo=age.Entidade ',
  'WHERE age.Codigo=' + Geral.FF0(QrAgenGrCabCodigo.Value),
  'ORDER BY NO_ENT ',
  '']);
  //
  QrAgenGrEnt.Locate('Controle', Controle, []);
end;


procedure TFmAgenGrCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAgenGrCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAgenGrCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAgenGrCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAgenGrCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAgenGrCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAgenGrCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAgenGrCabCodigo.Value;
  Close;
end;

procedure TFmAgenGrCab.ItsInclui1Click(Sender: TObject);
begin
  MostraAgenGrEnt(stIns);
end;

procedure TFmAgenGrCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrAgenGrCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'agengrcab');
end;

procedure TFmAgenGrCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, ColLarg, InterMinut, CorMul: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ColLarg        := EdColLarg.ValueVariant;
  InterMinut     := EdInterMinut.ValueVariant;
  CorMul         := CBCorMul.Selected;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32(
    'agengrcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'agengrcab', False, [
  'Nome', 'ColLarg', 'InterMinut',
  'CorMul'], [
  'Codigo'], [
  Nome, ColLarg, InterMinut,
  CorMul], [
  Codigo], True) then
  begin
    LocCod(Codigo, Codigo);
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //
    if FSeq = 1 then Close;
  end;
end;

procedure TFmAgenGrCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'agengrcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'agengrcab', 'Codigo');
end;

procedure TFmAgenGrCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmAgenGrCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmAgenGrCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmAgenGrCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAgenGrCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgenGrCab.SbCorClick(Sender: TObject);
begin
  CBCorMul.Selected := MyObjects.SelecionaCor(Self, CBCorMul.Selected);
end;

procedure TFmAgenGrCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAgenGrCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrAgenGrCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAgenGrCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAgenGrCab.QrAgenGrCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAgenGrCab.QrAgenGrCabAfterScroll(DataSet: TDataSet);
begin
  DBCorMul.Selected := QrAgenGrCabCorMul.Value;
  ReopenAgenGrEnt(0);
end;

procedure TFmAgenGrCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrAgenGrCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmAgenGrCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAgenGrCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'agengrcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAgenGrCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgenGrCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAgenGrCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'agengrcab');
end;

procedure TFmAgenGrCab.QrAgenGrCabBeforeClose(DataSet: TDataSet);
begin
  DBCorMul.Selected := clBlack;
end;

procedure TFmAgenGrCab.QrAgenGrCabBeforeOpen(DataSet: TDataSet);
begin
  QrAgenGrCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

