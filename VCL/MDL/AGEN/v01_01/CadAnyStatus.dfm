object FmCadAnyStatus: TFmCadAnyStatus
  Left = 339
  Top = 185
  Caption = '!!!-!!!!!-### :: Status de OSs'
  ClientHeight = 636
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 309
    Width = 1008
    Height = 327
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 496
        Top = 16
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 417
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox14: TGroupBox
        Left = 12
        Top = 56
        Width = 565
        Height = 121
        Caption = ' Cor de visualiza'#231#227'o de compromisso na agenda: '
        TabOrder = 2
        object Label22: TLabel
          Left = 12
          Top = 24
          Width = 135
          Height = 13
          Caption = 'Cor degrad'#234' inicial de fundo:'
        end
        object Label23: TLabel
          Left = 12
          Top = 48
          Width = 128
          Height = 13
          Caption = 'Cor degrad'#234' final de fundo:'
        end
        object Label24: TLabel
          Left = 12
          Top = 72
          Width = 61
          Height = 13
          Caption = 'Cor da fonte:'
        end
        object Label25: TLabel
          Left = 12
          Top = 96
          Width = 195
          Height = 13
          Caption = 'Cor do indicador de texto suspenso (hint):'
        end
        object CBAgeCorIni: TColorBox
          Left = 244
          Top = 20
          Width = 145
          Height = 22
          Selected = clWhite
          Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
          TabOrder = 0
        end
        object CBAgeCorFim: TColorBox
          Left = 244
          Top = 44
          Width = 145
          Height = 22
          DefaultColorColor = clWhite
          Selected = clWhite
          Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
          TabOrder = 1
        end
        object CBAgeCorFon: TColorBox
          Left = 244
          Top = 68
          Width = 145
          Height = 22
          DefaultColorColor = clWhite
          Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
          TabOrder = 2
        end
        object CBAgeCorHin: TColorBox
          Left = 244
          Top = 92
          Width = 145
          Height = 22
          DefaultColorColor = clWhite
          Selected = clRed
          Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
          TabOrder = 3
        end
        object RGAgeCorDir: TdmkRadioGroup
          Left = 404
          Top = 24
          Width = 145
          Height = 85
          Caption = ' Dire'#231#227'o do degrad'#234':'
          ItemIndex = 0
          Items.Strings = (
            'Horizontal'
            'Vertical')
          TabOrder = 4
          QryCampo = 'AgeCorDir'
          UpdCampo = 'AgeCorDir'
          UpdType = utYes
          OldValor = 0
        end
      end
      object EdOrdem: TdmkEdit
        Left = 496
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object RGExecucao: TdmkRadioGroup
        Left = 12
        Top = 180
        Width = 565
        Height = 57
        Caption = ' Execu'#231#227'o da OS:'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'UnProjGroup_Consts.sListaStatusOSExec')
        TabOrder = 4
        QryCampo = 'Execucao'
        UpdCampo = 'Execucao'
        UpdType = utYes
        OldValor = 0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 264
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel8: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 177
        Height = 32
        Caption = 'Status de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 177
        Height = 32
        Caption = 'Status de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 177
        Height = 32
        Caption = 'Status de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 92
    Width = 1008
    Height = 217
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 147
      Width = 1008
      Height = 70
      Align = alBottom
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 862
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 860
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtInclui: TBitBtn
          Tag = 10
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 136
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 260
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 147
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 147
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBGrid1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 792
          Height = 147
          Align = alClient
          DataSource = DsCad
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDrawColumnCell = DBGrid1DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              ReadOnly = True
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 282
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AgeCorIni'
              Title.Caption = 'Cor ini.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AgeCorFim'
              Title.Caption = 'Cor fin.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXT_AgeCorDir'
              Title.Caption = 'Dire'#231#227'o'
              Width = 91
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AgeCorFon'
              Title.Caption = 'Cor fonte'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AgeCorHin'
              Title.Caption = 'Cor hint'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EXECUCAO'
              Title.Caption = 'Execu'#231#227'o da OS'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 808
          Top = 0
          Width = 200
          Height = 147
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 200
            Height = 85
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 46
              Height = 13
              Caption = 'Pesquisa:'
            end
            object EdPesq: TEdit
              Left = 8
              Top = 20
              Width = 185
              Height = 21
              TabOrder = 0
              OnChange = EdPesqChange
            end
            object TBTam: TTrackBar
              Left = 4
              Top = 40
              Width = 193
              Height = 41
              Min = 1
              ParentShowHint = False
              Position = 4
              SelStart = 3
              ShowHint = True
              TabOrder = 1
              OnChange = TBTamChange
            end
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 85
            Width = 200
            Height = 62
            Align = alClient
            DataSource = DsPesq
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid2DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'd.'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 131
                Visible = True
              end>
          end
        end
        object LbItensMD: TListBox
          Left = 792
          Top = 0
          Width = 16
          Height = 147
          Align = alRight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ItemHeight = 14
          ParentFont = False
          TabOrder = 2
          Visible = False
          OnDblClick = LbItensMDDblClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object TbCad: TMySQLTable
    Database = Dmod.MyDB
    AfterInsert = TbCadAfterInsert
    BeforePost = TbCadBeforePost
    AfterPost = TbCadAfterPost
    OnCalcFields = TbCadCalcFields
    OnNewRecord = TbCadNewRecord
    OnDeleting = TbCadDeleting
    TableName = 'estatusoss'
    Left = 216
    Top = 144
    object TbCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbCadNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object TbCadOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object TbCadAgeCorIni: TIntegerField
      FieldName = 'AgeCorIni'
      Required = True
    end
    object TbCadAgeCorFim: TIntegerField
      FieldName = 'AgeCorFim'
      Required = True
    end
    object TbCadAgeCorDir: TSmallintField
      FieldName = 'AgeCorDir'
      Required = True
    end
    object TbCadAgeCorFon: TIntegerField
      FieldName = 'AgeCorFon'
      Required = True
    end
    object TbCadAgeCorHin: TIntegerField
      FieldName = 'AgeCorHin'
      Required = True
    end
    object TbCadTXT_AgeCorDir: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_AgeCorDir'
      Calculated = True
    end
    object TbCadExecucao: TSmallintField
      FieldName = 'Execucao'
    end
    object TbCadNO_EXECUCAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EXECUCAO'
      Size = 30
      Calculated = True
    end
  end
  object DsCad: TDataSource
    DataSet = TbCad
    Left = 244
    Top = 144
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 216
    Top = 172
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 244
    Top = 172
  end
end
