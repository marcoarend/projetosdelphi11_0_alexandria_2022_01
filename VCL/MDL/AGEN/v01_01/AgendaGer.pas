unit AgendaGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, MyListas,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, PlannerMonthView, DBPlannerMonthView, Planner, DBPlanner,
  dmkCompoStore, Menus, PlannerWaitList, System.DateUtils, UnDmkEnums;

type
  TFmAgendaGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PlaDia: TPlanner;
    TimerIndicator: TTimer;
    Panel5: TPanel;
    Label1: TLabel;
    EdAgenGrCabDia: TdmkEditCB;
    CBAgenGrCabDia: TdmkDBLookupComboBox;
    QrAgenGrCabDia: TmySQLQuery;
    DsAgenGrCabDia: TDataSource;
    QrAgenGrCabDiaCodigo: TIntegerField;
    QrAgenGrCabDiaNome: TWideStringField;
    SBAgenGrCabDia: TSpeedButton;
    SBMostraAgendaDia: TSpeedButton;
    TPDataDia: TdmkEditDateTimePicker;
    Label2: TLabel;
    QrEventos2: TmySQLQuery;
    QrEventos2Codigo: TIntegerField;
    QrEventos2QuestaoTyp: TIntegerField;
    QrEventos2QuestaoCod: TIntegerField;
    QrEventos2QuestaoExe: TIntegerField;
    QrEventos2Inicio: TDateTimeField;
    QrEventos2Termino: TDateTimeField;
    QrEventos2EmprEnti: TIntegerField;
    QrEventos2Terceiro: TIntegerField;
    QrEventos2Local: TIntegerField;
    QrEventos2Cor: TIntegerField;
    QrEventos2Cption: TSmallintField;
    QrEventos2Nome: TWideStringField;
    QrEventos2Notas: TWideStringField;
    QrAgentes: TmySQLQuery;
    QrAgentesEntidade: TIntegerField;
    QrAgenGrEnt: TmySQLQuery;
    QrAgenGrEntCodigo: TIntegerField;
    QrAgenGrEntControle: TIntegerField;
    QrAgenGrEntEntidade: TIntegerField;
    QrAgenGrEntCorEnt: TIntegerField;
    QrAgenGrEntNO_ENT: TWideStringField;
    QrAgenGrCabDiaColLarg: TIntegerField;
    QrAgenGrCabDiaInterMinut: TIntegerField;
    QrAgenGrCabDiaCorMul: TIntegerField;
    TabSheet3: TTabSheet;
    DsAgenGrEnt: TDataSource;
    DsEventos2: TDataSource;
    DsAgentes: TDataSource;
    QrAgenGrEntSigla: TWideStringField;
    QrAgendaEve: TmySQLQuery;
    QrAgendaEveCodigo: TIntegerField;
    QrAgendaEveQuestaoTyp: TIntegerField;
    QrAgendaEveQuestaoCod: TIntegerField;
    QrAgendaEveQuestaoExe: TIntegerField;
    QrAgendaEveInicio: TDateTimeField;
    QrAgendaEveTermino: TDateTimeField;
    QrAgendaEveEmprEnti: TIntegerField;
    QrAgendaEveTerceiro: TIntegerField;
    QrAgendaEveLocal: TIntegerField;
    QrAgendaEveCor: TIntegerField;
    QrAgendaEveCption: TSmallintField;
    QrAgendaEveNome: TWideStringField;
    QrAgendaEveNotas: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    QrEventos2NO_ENT: TWideStringField;
    QrEventos2FatoGeradr: TIntegerField;
    QrEventos2NO_FATOGERADR: TWideStringField;
    PMAgendaDiaItem_2: TPopupMenu;
    Excluirentidadedoenvento1: TMenuItem;
    PlaSem: TPlanner;
    Panel6: TPanel;
    Label4: TLabel;
    TPDataSem: TdmkEditDateTimePicker;
    DsAgenGrCabSem: TDataSource;
    QrAgenGrCabSem: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    DsAgenGrCabMes: TDataSource;
    QrAgenGrCabMes: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField2: TWideStringField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    PlaMes: TPlannerMonthView;
    PMAgendaSemItem_2: TPopupMenu;
    N1: TMenuItem;
    PMAgendaMesItem_2: TPopupMenu;
    N2: TMenuItem;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    SbSair: TBitBtn;
    BtRefresh: TBitBtn;
    QrAgentesControle: TIntegerField;
    QrEventos1: TmySQLQuery;
    QrEventos1NO_FATOGERADR: TWideStringField;
    QrEventos1NO_QUESTAOEXE: TWideStringField;
    QrEventos1NO_EMPRESA: TWideStringField;
    QrEventos1NO_TERCEIRO: TWideStringField;
    QrEventos1NO_LUGAR: TWideStringField;
    QrEventos1Codigo: TIntegerField;
    QrEventos1QuestaoTyp: TIntegerField;
    QrEventos1QuestaoCod: TIntegerField;
    QrEventos1QuestaoExe: TIntegerField;
    QrEventos1Inicio: TDateTimeField;
    QrEventos1Termino: TDateTimeField;
    QrEventos1EmprEnti: TIntegerField;
    QrEventos1Terceiro: TIntegerField;
    QrEventos1Local: TIntegerField;
    QrEventos1Cor: TIntegerField;
    QrEventos1Cption: TSmallintField;
    QrEventos1Nome: TWideStringField;
    QrEventos1Notas: TWideStringField;
    QrEventos1FatoGeradr: TIntegerField;
    QrEnti1: TmySQLQuery;
    QrEnti1Entidade: TIntegerField;
    QrEnti1Controle: TIntegerField;
    PMAgendaDiaItem_1: TPopupMenu;
    PMAgendaSemItem_1: TPopupMenu;
    PMAgendaMesItem_1: TPopupMenu;
    Alterarevento1: TMenuItem;
    Removerentidadedoevento1: TMenuItem;
    Alterarevento2: TMenuItem;
    Alterarevento3: TMenuItem;
    BtLimpa: TBitBtn;
    SbImprime: TBitBtn;
    QrEventos1Status: TIntegerField;
    QrEventos2Status: TIntegerField;
    QrAgendaLup: TmySQLQuery;
    QrAgendaLupCodigo: TIntegerField;
    QrAgendaLupDtHrIni: TDateTimeField;
    QrAgendaLupDtHrFim: TDateTimeField;
    QrEventos1LupPeri: TSmallintField;
    QrEventos1LupModo: TSmallintField;
    QrEventos1LupDFim: TDateTimeField;
    QrEventos1LupQtde: TIntegerField;
    QrAgendaLupLupPeri: TIntegerField;
    TMCarrega: TTimer;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerIndicatorTimer(Sender: TObject);
    procedure EdAgenGrCabDiaChange(Sender: TObject);
    procedure SBAgenGrCabDiaClick(Sender: TObject);
    procedure SBMostraAgendaDiaClick(Sender: TObject);
    procedure TPDataDiaChange(Sender: TObject);
    procedure TPDataDiaClick(Sender: TObject);
    procedure PlaDiaItemMove(Sender: TObject; Item: TPlannerItem; FromBegin,
      FromEnd, FromPos, ToBegin, ToEnd, ToPos: Integer);
    procedure QrEventos2AfterScroll(DataSet: TDataSet);
    procedure QrEventos2BeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure PlaDiaItemDblClick(Sender: TObject; Item: TPlannerItem);
    procedure FormShow(Sender: TObject);
    procedure PlaDiaPlannerNext(Sender: TObject);
    procedure PlaDiaPlannerPrev(Sender: TObject);
    procedure PlaDiaItemMoving(Sender: TObject; Item: TPlannerItem;
      DeltaBegin, DeltaPos: Integer; var Allow: Boolean);
    procedure Excluirentidadedoenvento1Click(Sender: TObject);
    procedure SBAgenGrCabSemClick(Sender: TObject);
    procedure SBMostraAgendaSemClick(Sender: TObject);
    procedure TPDataSemChange(Sender: TObject);
    procedure TPDataSemClick(Sender: TObject);
    procedure EdAgenGrCabSemChange(Sender: TObject);
    procedure TPDataMesChange(Sender: TObject);
    procedure TPDataMesClick(Sender: TObject);
    procedure EdAgenGrCabMesChange(Sender: TObject);
    procedure SBAgenGrCabMesClick(Sender: TObject);
    procedure PlaDiaItemSize(Sender: TObject; Item: TPlannerItem; Position,
      FromBegin, FromEnd, ToBegin, ToEnd: Integer);
    procedure PlaSemItemDblClick(Sender: TObject; Item: TPlannerItem);
    procedure PlaMesItemDblClick(Sender: TObject; Item: TPlannerItem);
    procedure PlaSemItemSize(Sender: TObject; Item: TPlannerItem; Position,
      FromBegin, FromEnd, ToBegin, ToEnd: Integer);
    procedure SBMostraAgendaMesClick(Sender: TObject);
    procedure PlaMesYearChange(Sender: TObject; origDate, newDate: TDateTime);
    procedure PlaMesYearChanged(Sender: TObject; origDate, newDate: TDateTime);
    procedure PlaMesMonthChange(Sender: TObject; origDate, newDate: TDateTime);
    procedure PlaMesMonthChanged(Sender: TObject; origDate, newDate: TDateTime);
    procedure PlaSemItemMove(Sender: TObject; Item: TPlannerItem; FromBegin,
      FromEnd, FromPos, ToBegin, ToEnd, ToPos: Integer);
    procedure PlaMesItemMove(Sender: TObject; APlannerItem: TPlannerItem;
      FromStartDate, FromEndDate, ToStartDate, ToEndDate: TDateTime);
    procedure SbSairClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure PlaDiaDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure PlaDiaDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure PlaSemPlannerNext(Sender: TObject);
    procedure PlaSemPlannerPrev(Sender: TObject);
    procedure QrEventos1AfterScroll(DataSet: TDataSet);
    procedure Alterarevento1Click(Sender: TObject);
    procedure PlaDiaItemRightClick(Sender: TObject; Item: TPlannerItem);
    procedure Removerentidadedoevento1Click(Sender: TObject);
    procedure Alterarevento2Click(Sender: TObject);
    procedure PlaSemItemRightClick(Sender: TObject; Item: TPlannerItem);
    procedure PlaMesItemRightClick(Sender: TObject; Item: TPlannerItem);
    procedure Alterarevento3Click(Sender: TObject);
    procedure BtLimpaClick(Sender: TObject);
    procedure PlaDiaPlannerDblClick(Sender: TObject; Position, FromSel,
      FromSelPrecise, ToSel, ToSelPrecise: Integer);
    procedure PlaSemPlannerDblClick(Sender: TObject; Position, FromSel,
      FromSelPrecise, ToSel, ToSelPrecise: Integer);
    procedure PlaMesDblClick(Sender: TObject; SelDate: TDateTime);
    procedure SbImprimeClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TMCarregaTimer(Sender: TObject);
  private
    { Private declarations }
    // Agenda do dia
    FDataDia, FDataSem: Integer;
    FCols1_Cod: array of Integer;
    FCols1_Nom: array of String;
    FIniPosDay(*, FOnAfterUserMoved*): Boolean;
    //FEntidade: Integer;
    FItem_Dia, FItem_Sem, FItem_Mes: TPlannerItem;
    (*FEntiMoving: Integer;*)
    //
    // Agenda da Semana
    //FDiaInicial: TDateTime;
    //
    // Agenda do m�s
    FAtualizaMes: Boolean;
    //
    procedure AtualizaTudo();
    function  ColunaDeEntidade(Codigo: Integer; Lista: array of Integer):
              Integer;
    procedure ConfiguraPlannerMode(Planner: TCustomControl; Data: TDateTime;
              AddDd: Integer);
    function  EntidadeDeColuna(Coluna: Integer; Lista: array of Integer):
              Integer;
    function  EventoLupEstaNoDia(Data: TDateTime): Boolean;
    function  NO_EntiDeColuna(Coluna: Integer; Lista: array of String): String;
    procedure HabilitaReShowDia();
    procedure HabilitaReShowSem();
    procedure HabilitaReShowMes();
    procedure LimpaLoopsDeAgenda();
    procedure LimpaPlanner(Planner: TCustomControl);
    procedure ReopenAgenGrEnt();
    procedure ReopenAgendaEve(ItemDBTag: Integer);
    procedure MostraCompromissosDoDia();
    procedure MostraCompromissosDaSem(Force: Boolean);
    procedure MostraCompromissosDoMes();
    procedure UpdateHeadersSem();
    procedure MostraFormAgendaImp();
    procedure ColoreItem(Item: TPlannerItem; Questao: TAgendaQuestao;
              Status: Integer);

  public
    { Public declarations }
  end;

  var
  FmAgendaGer: TFmAgendaGer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, Principal, UMySQLModule, UnDmkProcFunc,
  ModAgenda, Descanso, UnAgendaGerApp, MyGlyfs, ModuleGeral, AgendaImp, MyDBCheck,
  UnAgenda_Tabs, UnAgendaGerAll;

{$R *.DFM}

const
  FMultinstancia = True;

procedure TFmAgendaGer.Alterarevento1Click(Sender: TObject);
var
  Coluna, Entidade, QuestaoCod: Integer;
begin
  Coluna := TPlannerItem(FItem_Dia).ItemPos + 1;
  Entidade := EntidadeDeColuna(Coluna, FCols1_Cod);
  QuestaoCod := DmModAgenda.ObtemDeAgendaEve_QuestaoCodDeCodigo(
    TPlannerItem(FItem_Dia).DBTag);
  AgendaGerAll.MostraFormAgendaEve(QuestaoCod, Entidade, False, 0, 0, 0, 0, []);
  //
  MostraCompromissosDoDia();
end;

procedure TFmAgendaGer.Alterarevento2Click(Sender: TObject);
var
  QuestaoCod: Integer;
begin
  QuestaoCod := DmModAgenda.ObtemDeAgendaEve_QuestaoCodDeCodigo(
    TPlannerItem(FItem_Sem).DBTag);
  AgendaGerAll.MostraFormAgendaEve(QuestaoCod, 0, False, 0, 0, 0, 0, []);
  //
  MostraCompromissosDaSem(True);
end;

procedure TFmAgendaGer.Alterarevento3Click(Sender: TObject);
var
  QuestaoCod: Integer;
begin
  QuestaoCod := DmModAgenda.ObtemDeAgendaEve_QuestaoCodDeCodigo(
    TPlannerItem(FItem_Mes).DBTag);
  AgendaGerAll.MostraFormAgendaEve(QuestaoCod, 0, False, 0, 0, 0, 0, []);
  //
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.AtualizaTudo();
begin
  MostraCompromissosDoDia();
  MostraCompromissosDaSem(True);
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.BtLimpaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    // B U G S T R O L
    if CO_DMKID_APP = 24 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE agendaeve AS eve ',
      'LEFT JOIN oscab as cab ON cab.Codigo=eve.QuestaoCod ',
      //'SET eve.Termino=eve.Inicio ',
      'SET eve.Inicio="0000-00-00 00:00:00", ',
      'eve.Termino="0000-00-00 00:00:00"',
      //'WHERE eve.QuestaoTyp=2 ',
      'WHERE eve.QuestaoTyp=' + Geral.FF0(Integer(qagOSBgstrl)),
      'AND cab.Codigo IS NULL ',
      '']);
      {
      SELECT eve.*
      FROM agendaeve eve
      LEFT JOIN oscab cab ON cab.Codigo=eve.QuestaoCod
      WHERE eve.QuestaoTyp=' + Geral.FF0(Integer(qagOSBgstrl)),
      AND cab.Codigo IS NULL
      }
    end;

    //

    MostraCompromissosDoDia();
    MostraCompromissosDaSem(True);
    MostraCompromissosDoMes();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAgendaGer.BtOKClick(Sender: TObject);
{
var
  I: Integer;
}
begin
{
  QrAgenGrEnt.First;
  while not QrAgenGrEnt.Eof do
  begin
    FCols1_Cod[QrAgenGrEnt.RecNo] := QrAgenGrEntEntidade.Value;
    PlaDia.Header.Captions.Add(QrAgenGrEntSigla.Value);
    //
    QrAgenGrEnt.Next;
  end;
}
end;

procedure TFmAgendaGer.BtRefreshClick(Sender: TObject);
begin
  AtualizaTudo();
end;

procedure TFmAgendaGer.BtSaidaClick(Sender: TObject);
begin
  Close;
  //
  MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmAgendaGer.ColoreItem(Item: TPlannerItem; Questao: TAgendaQuestao;
Status: Integer);
var
  AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer;
begin
  case Questao of
    qagAvulso: AgendaGerAll.MQLDeMemoryCoresStatusAvul(QrEventos2Status.Value,
               AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
    qagOSBgstrl: AgendaGerAll.MQLDeMemoryCoresStatusOS(QrEventos2Status.Value,
                 AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
    else
    begin
      Geral.MB_Erro('Tipo de item de agenda n�o definido!' + sLineBreak +
      'TFmAgendaGer.ColoreItem()');
      //
      AgeCorIni := clLime;
      AgeCorFim := clYellow;
      AgeCorDir := Integer(gdVertical);
      AgeCorFon := clNavy;
      AgeCorHin := clWhite;
    end;
  end;
  //
  Item.ColorDirection := TPlannerGradientDirection(AgeCorDir);
  Item.Color := AgeCorIni;
  Item.ColorTo := AgeCorFim;
  Item.Font.Color := AgeCorFon;
  Item.HintIndicatorColor := AgeCorHin;
  //
  Item.BorderColor := VAR_AgeCorBgst;
  Item.TrackColor  := VAR_AgeCorBgst;
  Item.Shadow := False;
end;

function TFmAgendaGer.ColunaDeEntidade(Codigo: Integer;
  Lista: array of Integer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to Length(Lista) -1 do
  begin
    if Lista[I] = Codigo then
    begin
      Result := I - 1;
      Exit;
    end;
  end;
end;

procedure TFmAgendaGer.ConfiguraPlannerMode(Planner: TCustomControl;
  Data: TDateTime; AddDd: Integer);
var
  MyPlan: TPlanner;
  Dia, Mes, Ano: Word;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  if Planner is TPlanner then
  begin
    MyPlan := TPlanner(Planner);
    //MyPlan.Mode.TimeLineStart     := Geral.FDT(Data, 2);

    MyPlan.Mode.DateTimeFormat := 'dd/mm/yyyy';

    MyPlan.Mode.Day               := Dia;
    MyPlan.Mode.Month             := Mes;
    MyPlan.Mode.Year              := Ano;

    MyPlan.Mode.PeriodStartDay    := Dia;
    MyPlan.Mode.PeriodStartMonth  := Mes;
    MyPlan.Mode.PeriodStartYear   := Ano;

    MyPlan.Mode.PeriodEndDay      := Dia + AddDd;
    MyPlan.Mode.PeriodEndMonth    := Mes + AddDd;
    MyPlan.Mode.PeriodEndYear     := Ano + AddDd;

    //MyPlan.Mode.WeekStart         := Geral.NumeroSemanaNoAno(Data);

  end
  else
    Geral.MB_Erro(TCustomControl(Planner).Name +
    ' n�o � do tipo "Planner"!');
end;

procedure TFmAgendaGer.EdAgenGrCabDiaChange(Sender: TObject);
begin
  HabilitaReShowDia();
end;

procedure TFmAgendaGer.EdAgenGrCabMesChange(Sender: TObject);
begin
  HabilitaReShowMes();
end;

procedure TFmAgendaGer.EdAgenGrCabSemChange(Sender: TObject);
begin
  HabilitaReShowSem();
end;

function TFmAgendaGer.EntidadeDeColuna(Coluna: Integer;
  Lista: array of Integer): Integer;
begin
  Result := Lista[Coluna];
end;

function TFmAgendaGer.EventoLupEstaNoDia(Data: TDateTime): Boolean;
var
  DiaSemIni, DiaSemAtu: Integer;
  DiaPriIni, DiaPriAtu: TDateTime;
  SemMesIni, SemMesAtu: Integer;
  QtdSemIni, QtdSemAtu: Integer;
  Ano, Mes, Dia: Word;
begin
  Result := False;
  if (Data < Trunc(QrAgendaLupDtHrIni.Value))
  or (Data > QrAgendaLupDtHrFim.Value) then
    Exit;
  case QrAgendaLupLupPeri.Value of
    CO_AGENDA_LOOP_PERI_Nenhuma          : Result := False;
    CO_AGENDA_LOOP_PERI_Diaria           : Result := True;
    CO_AGENDA_LOOP_PERI_Semanal          :
    begin
     Result := DayOfWeek(Data) = DayOfWeek(QrAgendaLupDtHrIni.Value);
    end;
    //CO_AGENDA_LOOP_PERI_Decendial        : Result := Inicio + (LupQtde * 10);
    CO_AGENDA_LOOP_PERI_ACada2Semanas    :
    begin
      Result := (Trunc(Data) - Trunc(QrAgendaLupDtHrIni.Value)) mod 14 = 0;
    end;
    CO_AGENDA_LOOP_PERI_MensalDiaSemana  :
    begin
      DiaSemIni := DayOfWeek(QrAgendaLupDtHrIni.Value);
      DiaSemAtu := DayOfWeek(Data);
      if DiaSemIni = DiaSemAtu then
      begin
        DecodeDate(QrAgendaLupDtHrIni.Value, Ano, Mes, Dia);
        DiaPriIni := EncodeDate(Ano, Mes, 1);
        DiaPriIni := DiaPriIni - DayOfWeek(DiaPriIni) + 1;
        QtdSemIni := (Trunc(QrAgendaLupDtHrIni.Value - DiaPriIni) div 7) + 1;
        //
        DecodeDate(Data, Ano, Mes, Dia);
        DiaPriAtu := EncodeDate(Ano, Mes, 1);
        DiaPriAtu := DiaPriAtu - DayOfWeek(DiaPriAtu) + 1;
        QtdSemAtu := (Trunc(Data - DiaPriAtu) div 7) + 1;
        //
        Result := QtdSemIni = QtdSemAtu;
      end;
    end;
    CO_AGENDA_LOOP_PERI_MensalDiaDoMes   :
    begin
      Result := DayOfTheMonth(Data) = DayOfTheMonth(QrAgendaLupDtHrIni.Value);
    end;
    CO_AGENDA_LOOP_PERI_Anual            :
    begin
      Result := Geral.FDT(Data, 12) = Geral.FDT(QrAgendaLupDtHrIni.Value, 12);
    end
    else Geral.MB_Erro(
      '"Periodicidade" n�o definida na function "EventoLupEstaNoDia"');
  end;
end;

procedure TFmAgendaGer.Excluirentidadedoenvento1Click(Sender: TObject);
begin
  ShowMessage('Falta implementar');
end;

procedure TFmAgendaGer.FormCreate(Sender: TObject);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  FAtualizaMes := False;
  FDataDia := 0;
  FDataSem := 0;
  Data := DModG.ObtemAgora();
  DecodeDate(Data, Ano, Mes, Dia);
  TPDataDia.Date := Data;
  TPDataSem.Date := Data;
  (*�TPDataMes.Date := Data*);
  //
  PageControl1.ActivePageIndex := 0;
  PageControl1.ActivePageIndex := 1;
  //
{
  TabSheet5.Visible := False;
  TabSheet6.Visible := False;
}
  FIniPosDay := False;
  //
  PlaMes.Year  := Ano;
  PlaMes.Month := Mes;
  PlaMes.Day   := Dia;
  //
  PlaMes.MinDate.Use := True;
  PlaMes.MinDate.Date := Data;
(*
  ShowMessage(IntToStr(PlaMes.MinDate.Year));
  ShowMessage(IntToStr(PlaMes.MinDate.Month));
  ShowMessage(IntToStr(PlaMes.MinDate.Day));
*)
  TMCarrega.Enabled := True;
end;

procedure TFmAgendaGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgendaGer.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmAgendaGer.HabilitaReShowDia();
begin
  LimpaPlanner(PlaDia);
  PlaDia.Visible := False;
  SbMostraAgendaDia.Enabled :=
    (TPDataDia.Date > 1) and (EdAgenGrCabDia.ValueVariant <> 0);
end;

procedure TFmAgendaGer.HabilitaReShowMes();
begin
  LimpaPlanner(PlaMes);
  (*�PlaMes.Visible := False;
  SbMostraAgendaMes.Enabled :=
    (TPDataMes.Date > 1) and (EdAgenGrCabMes.ValueVariant <> 0);*)
end;

procedure TFmAgendaGer.HabilitaReShowSem();
begin
  MostraCompromissosDaSem(False);
{�
  LimpaPlanner(PlaSem);
  PlaSem.Visible := False;
  SbMostraAgendaSem.Enabled :=
    (TPDataSem.Date > 1) and (EdAgenGrCabSem.ValueVariant <> 0);
}
end;

procedure TFmAgendaGer.LimpaLoopsDeAgenda();
var
  Data: TDateTime;
begin
// N�o posso excluir pois o cliente pode olhar a agenda passada!
end;

procedure TFmAgendaGer.LimpaPlanner(Planner: TCustomControl);
begin
  if Planner is TPlanner then
  begin
    TPlanner(Planner).Header.Captions.Clear;
    TPlanner(Planner).Items.Clear;
  end else
  if Planner is TPlannerMonthView then
  begin
    //TPlannerMonthView(Planner).Header.Captions.Clear;
    TPlannerMonthView(Planner).Items.Clear;
    TPlannerMonthView(Planner).Color := clWhite;
  end else
    Geral.MB_Erro(TCustomControl(Planner).Name +
    ' n�o � do tipo "Planner"!');
end;

procedure TFmAgendaGer.MostraCompromissosDaSem(Force: Boolean);
var
  Inicio, Termino, SQLData, TxtTmp, TxtOS, TxtCL: String;
  DtIni, DtFim: TDateTime;
  //Indice(*, Sincro*): Integer;
  //
  I, J, K, Data(*, MinNow*): Integer;
  AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer;
  //
  LupIni, LupFim: String;
  DtHr: TDateTime;
begin
  Data := Trunc(TPDataSem.Date);
  if (FDataSem <> Data) or Force then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preenchendo agenda da semana!');
    FDataSem := Data;
    //
    UpdateHeadersSem();
    //
    DtIni    := Data;
    DtFim    := DtIni + 7 - (1/86400);
    Inicio  := dmkPF.SQL_Periodo('Inicio' , DtIni, DtFim, True, True);
    Termino := dmkPF.SQL_Periodo('Termino', DtIni, DtFim, True, True);
    SQLData := Geral.ATS([
      'AND ( ',
      '  (' + Inicio + ') ',
      '  OR ',
      '  (' + Termino + ') ',
      ') ']);
    //
  /////////////// G E N � R I C O S  ///////////////////////////////////////////
    UnDmkDAC_PF.AbreMySQLQuery0(QrEventos1, Dmod.MyDB, [
    'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO, ',
    'stc.Nome NO_LUGAR, eve.* ',
    'FROM agendaeve eve ',
    'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr ',
    'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe ',
    'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti ',
    'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti ',
    'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local ',
    'WHERE eve.LupPeri=0 ', // Loop ser� feito abaixo
    'AND eve.QuestaoTyp=' + Geral.FF0(Integer(qagAvulso)),
    SQLData,
    'AND (eve.EmprEnti IN (' + VAR_LIB_FILIAIS + ') OR eve.EmprEnti = 0)',
    '']);

    I := 0;
    QrEventos1.First;
    while not QrEventos1.Eof do
    begin
      I := I + 1;
      //Indice := PlaSem.Items.Count;
      //
      with PlaSem.CreateItem do
      begin
        ItemStartTime := QrEventos1Inicio.Value;
        ItemEndTime   := QrEventos1Termino.Value;
        Text.Text     := QrEventos1Nome.Value + ' - ' +
                         ' TERC: ' + QrEventos1NO_TERCEIRO.Value +
                         ' FATO: ' + QrEventos1NO_FATOGERADR.Value +
                         ' ACAO: ' + QrEventos1NO_QUESTAOEXE.Value +
                         ' LUGAR: ' + QrEventos1NO_LUGAR.Value;
        Hint          := Text.Text;
        HintIndicator := True;
        ItemPos := Trunc(QrEventos1Inicio.Value - DtIni);
        DBTag := QrEventos1Codigo.Value;
        PopupMenu := PMAgendaSemItem_1;
        //
(*
        Dmod.MQLDeMemoryCoresStatusAvul(QrEventos1Status.Value,
          AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
        //CaptionType := ctTime;
        ColorDirection := TPlannerGradientDirection(AgeCorDir); //gdVertical;
        Color := AgeCorIni; //clLime;
        ColorTo := AgeCorFim; //clYellow;
        Font.Color := AgeCorFon; //clNavy;
        HintIndicatorColor := AgeCorHin; //clWhite;
        //
        BorderColor := VAR_AgeCorAvul;
        TrackColor  := VAR_AgeCorAvul;
        Shadow := False;
*)
          ColoreItem(PlaSem.Items[PlaSem.Items.Count - 1], qagAvulso,
          QrEventos1Status.Value);
      end;
      QrEventos1.Next;
    end;
    // A T I V I D A D E S   E M   L O O P
    LupIni := Geral.FDT(DtIni, 1);
    LupFim := Geral.FDT(DtFim, 109);
    UnDmkDAC_PF.AbreMySQLQuery0(QrAgendaLup, Dmod.MyDB, [
    'SELECT * ',
    'FROM agendalup ',
    'WHERE "' + LupIni + '" BETWEEN DtHrIni AND DtHrFIm ',
    'OR "' + LupFim + '" BETWEEN DtHrIni AND DtHrFIm ',
    'OR ( ',
    '     "' + LupIni + '" < DtHrIni ',
    '     AND ',
    '     "' + LupFim + '" > DtHrFIm ',
    ') ',
    '']);
    QrAgendaLup.First;
    while not QrAgendaLup.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEventos1, Dmod.MyDB, [
      'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE, ',
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO, ',
      'stc.Nome NO_LUGAR, eve.* ',
      'FROM agendaeve eve ',
      'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr ',
      'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe ',
      'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti ',
      'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti ',
      'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro ',
      'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local ',
      'WHERE eve.Codigo=' + Geral.FF0(QrAgendaLupCodigo.Value),
      'AND (eve.EmprEnti IN (' + VAR_LIB_FILIAIS + ') OR eve.EmprEnti = 0)',
      '']);
      //
      K := 0;
      for J := Trunc(DtIni) to Trunc(DtIni) + 7 do
      begin
        K := K + 1;
        if EventoLupEstaNoDia(J) then
        begin
          with PlaSem.CreateItem do
          begin
            DtHr := J + (QrEventos1Inicio.Value - Trunc(QrEventos1Inicio.Value));
            ItemStartTime := DtHr;
            DtHr := J + (QrEventos1Termino.Value - Trunc(QrEventos1Termino.Value));
            ItemEndTime   := DtHr;
            Text.Text     := QrEventos1Nome.Value + ' - ' +
                             ' TERC: ' + QrEventos1NO_TERCEIRO.Value +
                             ' FATO: ' + QrEventos1NO_FATOGERADR.Value +
                             ' ACAO: ' + QrEventos1NO_QUESTAOEXE.Value +
                             ' LUGAR: ' + QrEventos1NO_LUGAR.Value;
            Hint          := Text.Text;
            HintIndicator := True;
            //ItemPos := Trunc(QrEventos1Inicio.Value - DtIni);
            ItemPos := K -1;
            DBTag := QrEventos1Codigo.Value;
            PopupMenu := PMAgendaSemItem_1;
            //
(*
            Dmod.MQLDeMemoryCoresStatusAvul(QrEventos1Status.Value,
              AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
            //CaptionType := ctTime;
            ColorDirection := TPlannerGradientDirection(AgeCorDir); //gdVertical;
            Color := AgeCorIni; //clLime;
            ColorTo := AgeCorFim; //clYellow;
            Font.Color := AgeCorFon; //clNavy;
            HintIndicatorColor := AgeCorHin; //clWhite;
            //
            BorderColor := VAR_AgeCorAvul;
            TrackColor  := VAR_AgeCorAvul;
            Shadow := False;
*)
            ColoreItem(PlaSem.Items[PlaSem.Items.Count - 1], qagAvulso,
            QrEventos1Status.Value);
          end;
        end;
      end;
      QrAgendaLup.Next;
    end;
    //////////////////////////////////////////////////////////////////////////////
    /////////////// E S P E C I F I C O S ////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //
    //////////////////////////////////////////////////////////////////////////////
    // B U G S T R O L
    if CO_DMKID_APP = 24 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEventos2, Dmod.MyDB, [
      'SELECT fge.Nome NO_FATOGERADR, eve.*,',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
      'FROM agendaeve eve',
      'LEFT JOIN entidades ent ON ent.Codigo=eve.Terceiro',
      'LEFT JOIN fatogeradr fge ON fge.Codigo=eve.FatoGeradr',
      //'WHERE eve.QuestaoTyp=2 ',
      'WHERE eve.QuestaoTyp=' + Geral.FF0(Integer(qagOSBgstrl)),
      SQLData,
      'AND (eve.EmprEnti IN (' + VAR_LIB_FILIAIS + ') OR eve.EmprEnti = 0)',
      '']);
      QrEventos2.First;
      while not QrEventos2.Eof do
      begin
        //Indice := PlaSem.Items.Count;
        //
        with PlaSem.CreateItem do
        begin
{
          TxtOS := 'OS ' + Geral.FF0(QrEventos2QuestaoCod.Value) + ' '
                           + AgendaGerApp.AgendaEveQuestaoExe_ValueToText(
                           QrEventos2QuestaoExe.Value);
          TxtCL := 'CL' + Geral.FF0(
                           QrEventos2Terceiro.Value) + '-' +
                           QrEventos2NO_ENT.Value;
          if VAR_ORDEM_TXT_POPUP_AGENDA = 0 then
            TxtTmp := TxtOS + ' ' + TxtCL
          else
            TxtTmp := TxtCL + ' ' + TxtOS;
          //
          Text.Text     := TxtTmp + ' FG' + Geral.FF0(
                           QrEventos2FatoGeradr.Value) + '-' +
                           QrEventos2NO_FATOGERADR.Value;
}
          Text.Text := AgendaGerAll.MontagemTextoCompromissoApp(
            QrEventos2QuestaoCod.Value, QrEventos2QuestaoExe.Value,
            QrEventos2Terceiro.Value, QrEventos2NO_ENT.Value,
            QrEventos2FatoGeradr.Value, QrEventos2NO_FATOGERADR.Value);
          //
          ItemStartTime := QrEventos2Inicio.Value;
          ItemEndTime   := QrEventos2Termino.Value;
          Hint          := Text.Text;
          HintIndicator := True;
          ItemPos := Trunc(QrEventos2Inicio.Value - DtIni);
          DBTag := QrEventos2Codigo.Value;
          PopupMenu := PMAgendaSemItem_2;
          //
(*
          Dmod.MQLDeMemoryCoresStatusOS(QrEventos2Status.Value,
            AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
          //CaptionType := ctTime;
          ColorDirection := TPlannerGradientDirection(AgeCorDir); //gdVertical;
          Color := AgeCorIni; //clLime;
          ColorTo := AgeCorFim; //clYellow;
          Font.Color := AgeCorFon; //clNavy;
          HintIndicatorColor := AgeCorHin; //clWhite;
          //
          BorderColor := VAR_AgeCorBgst;
          TrackColor  := VAR_AgeCorBgst;
          Shadow := False;
          //
*)
          ColoreItem(PlaSem.Items[PlaSem.Items.Count - 1], qagOSBgstrl,
          QrEventos2Status.Value);
        end;
        QrEventos2.Next;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////////
    //
    //////////////////////////////////////////////////////////////////////////////
    /////////////// F I M    E S P E C I F I C O S ///////////////////////////////
    //////////////////////////////////////////////////////////////////////////////

    //
    //
  {
    BtOKClick(Self);
    if FIniPosDay = False then
    begin
      FIniPosDay := True;
      MinNow := Trunc(Time() * 24 * 60);
      MinNow := MinNow div PlaSem.Display.DisplayUnit;
      if MinNow > 1 then
        MinNow := MinNow - 1;
      PlaSem.GridTopRow := MinNow;
    end;
    //
  }
    //////////////////////////////////////////////////////////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmAgendaGer.MostraCompromissosDoDia();
var
 Inicio, Termino, SQLData, TxtTmp, TxtOS, TxtCL, LupIni, LupFim: String;
 Data: TDateTime;
 //Indice(*, Sincro*): Integer;
 Dia, Mes, Ano: Word;
 //
 MinNow: Integer;
begin
  if EdAgenGrCabDia.ValueVariant = 0 then
  begin
    PlaDia.Visible := False;
    Exit;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preenchendo agenda do dia!');

  //
  ReopenAgenGrEnt();
  //
  Data    := Int(TPDataDia.Date);
  DecodeDate(Data, Ano, Mes, Dia);
  PlaDia.Mode.Year  := Ano;
  PlaDia.Mode.Month := Mes;
  PlaDia.Mode.Day   := Dia;
  Inicio  := dmkPF.SQL_Periodo('Inicio' , Data, Data, True, True);
  Termino := dmkPF.SQL_Periodo('Termino', Data, Data, True, True);
  SQLData := Geral.ATS([
    'AND ( ',
    '  (' + Inicio + ') ',
    '  OR ',
    '  (' + Termino + ') ',
    ') ']);

  //
  /////////////// G E N � R I C O S  ///////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrEventos1, Dmod.MyDB, [
  'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO, ',
  'stc.Nome NO_LUGAR, eve.* ',
  'FROM agendaeve eve ',
  'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr ',
  'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe ',
  'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti ',
  'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti ',
  'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local ',
  'WHERE eve.LupPeri=0 ', // Loop ser� feito abaixo
  'AND eve.QuestaoTyp=' + Geral.FF0(Integer(qagAvulso)),
  SQLData,
  '']);
  QrEventos1.First;
  while not QrEventos1.Eof do
  begin
    QrEnti1.First;
    while not QrEnti1.Eof do
    begin
      //Indice := PlaDia.Items.Count;
      //
      with PlaDia.CreateItem do
      begin
        //
        ItemStartTime := QrEventos1Inicio.Value;
        ItemEndTime   := QrEventos1Termino.Value;
        Text.Text     := QrEventos1Nome.Value + ' - ' +
                         ' TERC: ' + QrEventos1NO_TERCEIRO.Value +
                         ' FATO: ' + QrEventos1NO_FATOGERADR.Value +
                         ' ACAO: ' + QrEventos1NO_QUESTAOEXE.Value +
                         ' LUGAR: ' + QrEventos1NO_LUGAR.Value;
        Hint          := Text.Text;
        HintIndicator := True;
        ItemPos       := ColunaDeEntidade(QrEnti1Entidade.Value, FCols1_Cod);
        DBTag         := QrEventos1Codigo.Value;
        PopupMenu     := PMAgendaDiaItem_1;
        //
        ColoreItem(PlaDia.Items[PlaDia.Items.Count - 1], qagAvulso,
          QrEventos1Status.Value);
      end;
      //
      QrEnti1.Next;
    end;
    QrEventos1.Next;
  end;
  // A T I V I D A D E S   E M   L O O P
  LupIni := Geral.FDT(Data, 1);
  LupFim := LupIni + ' 23:59:59';
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgendaLup, Dmod.MyDB, [
  'SELECT * ',
  'FROM agendalup ',
  'WHERE "' + LupIni + '" BETWEEN DtHrIni AND DtHrFIm ',
  'OR "' + LupFim + '" BETWEEN DtHrIni AND DtHrFIm ',
  '']);
  QrAgendaLup.First;
  while not QrAgendaLup.Eof do
  begin
    if EventoLupEstaNoDia(Data) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEventos1, Dmod.MyDB, [
      'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE, ',
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO, ',
      'stc.Nome NO_LUGAR, eve.* ',
      'FROM agendaeve eve ',
      'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr ',
      'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe ',
      'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti ',
      'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti ',
      'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro ',
      'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local ',
      'WHERE eve.Codigo=' + Geral.FF0(QrAgendaLupCodigo.Value),
      '']);
      //
      QrEnti1.First;
      while not QrEnti1.Eof do
      begin
        with PlaDia.CreateItem do
        begin
          //
          ItemStartTime := QrEventos1Inicio.Value;
          ItemEndTime   := QrEventos1Termino.Value;
          Text.Text     := QrEventos1Nome.Value + ' - ' +
                           ' TERC: ' + QrEventos1NO_TERCEIRO.Value +
                           ' FATO: ' + QrEventos1NO_FATOGERADR.Value +
                           ' ACAO: ' + QrEventos1NO_QUESTAOEXE.Value +
                           ' LUGAR: ' + QrEventos1NO_LUGAR.Value;
          Hint          := Text.Text;
          HintIndicator := True;
          ItemPos       := ColunaDeEntidade(QrEnti1Entidade.Value, FCols1_Cod);
          DBTag         := QrEventos1Codigo.Value;
          PopupMenu     := PMAgendaDiaItem_1;
          //
          ColoreItem(PlaDia.Items[PlaDia.Items.Count - 1], qagAvulso,
            QrEventos1Status.Value);
        end;
        QrEnti1.Next;
      end;
    end;
    QrAgendaLup.Next;
  end;
  //////////////////////////////////////////////////////////////////////////////
  /////////////// E S P E C I F I C O S ////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //
  //////////////////////////////////////////////////////////////////////////////
  // B U G S T R O L
  if CO_DMKID_APP = 24 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEventos2, Dmod.MyDB, [
    'SELECT fge.Nome NO_FATOGERADR, eve.*,',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
    'FROM agendaeve eve',
    'LEFT JOIN entidades ent ON ent.Codigo=eve.Terceiro',
    'LEFT JOIN fatogeradr fge ON fge.Codigo=eve.FatoGeradr',
    //'WHERE eve.QuestaoTyp=2 ',
    'WHERE eve.QuestaoTyp=' + Geral.FF0(Integer(qagOSBgstrl)),
    SQLData,
    '']);
    QrEventos2.First;
    while not QrEventos2.Eof do
    begin
      QrAgentes.First;
      //SetLength(par, QrAgentes.RecordCount);
      while not QrAgentes.Eof do
      begin
        //Sincro := QrAgentes.RecordCount -1;
        //Indice := PlaDia.Items.Count;
        //
        with PlaDia.CreateItem do
        begin
{
          TxtOS := 'OS ' + Geral.FF0(QrEventos2QuestaoCod.Value) + ' '
                           + AgendaGerApp.AgendaEveQuestaoExe_ValueToText(
                           QrEventos2QuestaoExe.Value);
          TxtCL := 'CL' + Geral.FF0(
                           QrEventos2Terceiro.Value) + '-' +
                           QrEventos2NO_ENT.Value;
          if VAR_ORDEM_TXT_POPUP_AGENDA = 0 then
            TxtTmp := TxtOS + ' ' + TxtCL
          else
            TxtTmp := TxtCL + ' ' + TxtOS;
          //

          //
          Text.Text     := TxtTmp  + ' FG' + Geral.FF0(
                           QrEventos2FatoGeradr.Value) + '-' +
                           QrEventos2NO_FATOGERADR.Value;
}
          Text.Text := AgendaGerAll.MontagemTextoCompromissoApp(
            QrEventos2QuestaoCod.Value, QrEventos2QuestaoExe.Value,
            QrEventos2Terceiro.Value, QrEventos2NO_ENT.Value,
            QrEventos2FatoGeradr.Value, QrEventos2NO_FATOGERADR.Value);
          //
          ItemStartTime := QrEventos2Inicio.Value;
          ItemEndTime   := QrEventos2Termino.Value;
          Hint          := Text.Text;
          HintIndicator := True;
          ItemPos       := ColunaDeEntidade(QrAgentesEntidade.Value, FCols1_Cod);
          DBTag         := QrEventos2Codigo.Value;
          PopupMenu     := PMAgendaDiaItem_2;
          // dmkPlannerItem
          {
          Evento.Codigo            := QrEventos2Codigo.Value;
          Evento.FatoGerouCodi     := QrEventos2FatoGeradr.Value;
          Evento.FatoGerouNome     := QrEventos2NO_FATOGERADR.Value;
          Evento.OldItemPos        := ItemPos;
          Evento.QuestaoCod        := QrEventos2QuestaoCod.Value;
          Evento.QuestaoExe        := QrEventos2QuestaoExe.Value;
          Evento.QuestaoTyp        := TAgendaQuestao(QrEventos2QuestaoTyp.Value);
          Evento.ParticipanteCodi  := QrEventos2terceiro.Value;
          Evento.ParticipanteNome  := QrEventos2NO_ENT.Value;
          Evento.ParticipanteTbID  := QrAgentesControle.Value;
          }
          ColoreItem(PlaDia.Items[PlaDia.Items.Count - 1], qagOSBgstrl,
          QrEventos2Status.Value);
        end;
        //par[QrAgentes.RecNo] := PlaDia.Items[PlaDia.Items.Count-1];

  {
    // create link from item 1 to item 2
    PlaDia.Items[0].LinkedItem :=  PlaDia.Items[1];
    PlaDia.Items[0].LinkType := ltLinkFull;   // begin & end time of items will simultanously change

    // create link from item 2 to item 1
    PlaDia.Items[1].LinkedItem :=  PlaDia.Items[0];
    PlaDia.Items[1].LinkType := ltLinkFull;
  }

        //
        QrAgentes.Next;
      end;
      {
      PlaDia.MultiSelect := True;
      PlaDia.LinkItems(par);
      PlaDia.AutoSelectLinkedItems := True;
      PlaDia.SelectLinkedItems(PlaDia.Items[0]);
      }
      QrEventos2.Next;
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  //
  //////////////////////////////////////////////////////////////////////////////
  /////////////// F I M    E S P E C I F I C O S ///////////////////////////////
  //////////////////////////////////////////////////////////////////////////////

  //
  //
  BtOKClick(Self);
  if FIniPosDay = False then
  begin
    FIniPosDay := True;
    MinNow := Trunc(Time() * 24 * 60);
    MinNow := MinNow div PlaDia.Display.DisplayUnit;
    if MinNow > 1 then
      MinNow := MinNow - 1;
    PlaDia.GridTopRow := MinNow;
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmAgendaGer.MostraCompromissosDoMes();
var
  Inicio, Termino, SQLData, TxtTmp, TxtOS, TxtCL: String;
  DtIni, DtFim: TDateTime;
  //Indice(*, Sincro*): Integer;
  //
(*
  MinNow: Integer;
  Dia, Mes, Ano: Word;*)
  LupIni, LupFim: String;
begin
  if FAtualizaMes then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preenchendo agenda do m�s!');
    //
    LimpaPlanner(PlaMes);
    PlaMes.Visible := True;
    //
    (*�DecodeDate(TPDataMes.Date, Ano, Mes, Dia);
    PlaMes.Year  := Ano;
    PlaMes.Month := Mes;
    PlaMes.Day   := Dia;
    *)
    DtIni    := EncodeDate(PlaMes.Year, PlaMes.Month, 1);
    DtFim    := 0;//IncMonth(DtIni) - 1;
    Inicio  := dmkPF.SQL_Periodo('Inicio' , DtIni, DtFim, True, False);
    Termino := dmkPF.SQL_Periodo('Termino', DtIni, DtFim, True, False);
    SQLData := Geral.ATS([
      'AND ( ',
      '  (' + Inicio + ') ',
      '  OR ',
      '  (' + Termino + ') ',
      ') ']);
    //

  //
  /////////////// G E N � R I C O S  ///////////////////////////////////////////
    UnDmkDAC_PF.AbreMySQLQuery0(QrEventos1, Dmod.MyDB, [
    'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO, ',
    'stc.Nome NO_LUGAR, eve.* ',
    'FROM agendaeve eve ',
    'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr ',
    'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe ',
    'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti ',
    'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti ',
    'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local ',
    'WHERE eve.QuestaoTyp=' + Geral.FF0(Integer(qagAvulso)),
    SQLData,
    '']);
    QrEventos1.First;
    while not QrEventos1.Eof do
    begin
      //Indice := PlaMes.Items.Count;
      //
      with PlaMes.CreateItem do
      begin
        ItemStartTime := QrEventos1Inicio.Value;
        ItemEndTime   := QrEventos1Termino.Value;
        Text.Text     := QrEventos1Nome.Value + ' - ' +
                         ' TERC: ' + QrEventos1NO_TERCEIRO.Value +
                         ' FATO: ' + QrEventos1NO_FATOGERADR.Value +
                         ' ACAO: ' + QrEventos1NO_QUESTAOEXE.Value +
                         ' LUGAR: ' + QrEventos1NO_LUGAR.Value;
        Hint          := Text.Text;
        HintIndicator := True;
        ItemPos := Trunc(QrEventos1Inicio.Value - DtIni);
        DBTag := QrEventos1Codigo.Value;
        PopupMenu := PMAgendaMesItem_1;
      end;
      QrEventos1.Next;
    end;
  // A T I V I D A D E S   E M   L O O P
  {
  LupIni := Geral.FDT(DtIni, 1);
  LupFim := Geral.FDT(IncMonth(DtIni, 1)-1) + ' 23:59:59';
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgendaLup, Dmod.MyDB, [
  'SELECT * ',
  'FROM agendalup ',
  'WHERE "' + LupIni + '" BETWEEN DtHrIni AND DtHrFIm ',
  'OR "' + LupFim + '" BETWEEN DtHrIni AND DtHrFIm ',
  '']);
  QrAgendaLup.First;
  while not QrAgendaLup.Eof do
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEventos1, Dmod.MyDB, [
    'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO, ',
    'stc.Nome NO_LUGAR, eve.* ',
    'FROM agendaeve eve ',
    'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr ',
    'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe ',
    'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti ',
    'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti ',
    'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local ',
    'WHERE eve.Codigo=' + Geral.FF0(QrAgendaLupCodigo.Value),
    '']);
    //
    with PlaDia.CreateItem do
    begin
      //
      ItemStartTime := QrEventos1Inicio.Value;
      ItemEndTime   := QrEventos1Termino.Value;
      Text.Text     := QrEventos1Nome.Value + ' - ' +
                       ' TERC: ' + QrEventos1NO_TERCEIRO.Value +
                       ' FATO: ' + QrEventos1NO_FATOGERADR.Value +
                       ' ACAO: ' + QrEventos1NO_QUESTAOEXE.Value +
                       ' LUGAR: ' + QrEventos1NO_LUGAR.Value;
      Hint          := Text.Text;
      HintIndicator := True;
      ItemPos       := ColunaDeEntidade(QrEnti1Entidade.Value, FCols1_Cod);
      DBTag         := QrEventos1Codigo.Value;
      PopupMenu     := PMAgendaDiaItem_1;
    end;
    //
    QrAgendaLup.Next;
  end;
  }

    //////////////////////////////////////////////////////////////////////////////
    /////////////// E S P E C I F I C O S ////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //
    //////////////////////////////////////////////////////////////////////////////
    // B U G S T R O L
    if CO_DMKID_APP = 24 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEventos2, Dmod.MyDB, [
      'SELECT fge.Nome NO_FATOGERADR, eve.*,',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
      'FROM agendaeve eve',
      'LEFT JOIN entidades ent ON ent.Codigo=eve.Terceiro',
      'LEFT JOIN fatogeradr fge ON fge.Codigo=eve.FatoGeradr',
      //'WHERE eve.QuestaoTyp=2 ',
      'WHERE eve.QuestaoTyp=' + Geral.FF0(Integer(qagOSBgstrl)),
      SQLData,
      '']);
      QrEventos2.First;
      while not QrEventos2.Eof do
      begin
        //Indice := PlaMes.Items.Count;
        //
        with PlaMes.CreateItem do
        begin
{
            TxtOS := 'OS ' + Geral.FF0(QrEventos2QuestaoCod.Value) + ' '
                           + AgendaGerApp.AgendaEveQuestaoExe_ValueToText(
                           QrEventos2QuestaoExe.Value);
            TxtCL := 'CL' + Geral.FF0(
                           QrEventos2Terceiro.Value) + '-' +
                           QrEventos2NO_ENT.Value;
            if VAR_ORDEM_TXT_POPUP_AGENDA = 0 then
              TxtTmp := TxtOS + ' ' + TxtCL
            else
              TxtTmp := TxtCL + ' ' + TxtOS;
            //

            //
          Text.Text     := TxtTmp + ' FG' + Geral.FF0(
                           QrEventos2FatoGeradr.Value) + '-' +
                           QrEventos2NO_FATOGERADR.Value;
}
          Text.Text := AgendaGerAll.MontagemTextoCompromissoApp(
            QrEventos2QuestaoCod.Value, QrEventos2QuestaoExe.Value,
            QrEventos2Terceiro.Value, QrEventos2NO_ENT.Value,
            QrEventos2FatoGeradr.Value, QrEventos2NO_FATOGERADR.Value);
          //
          ItemStartTime := QrEventos2Inicio.Value;
          ItemEndTime   := QrEventos2Termino.Value;
          Hint          := Text.Text;
          HintIndicator := True;
          ItemPos := Trunc(QrEventos2Inicio.Value - DtIni);
          DBTag := QrEventos2Codigo.Value;
          PopupMenu := PMAgendaMesItem_2;
        end;
        QrEventos2.Next;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////////
    //
    //////////////////////////////////////////////////////////////////////////////
    /////////////// F I M    E S P E C I F I C O S ///////////////////////////////
    //////////////////////////////////////////////////////////////////////////////

    //
    //
  {
    BtOKClick(Self);
    if FIniPosDay = False then
    begin
      FIniPosDay := True;
      MinNow := Trunc(Time() * 24 * 60);
      MinNow := MinNow div PlaMes.Display.DisplayUnit;
      if MinNow > 1 then
        MinNow := MinNow - 1;
      PlaMes.GridTopRow := MinNow;
    end;
    //
  }
    //////////////////////////////////////////////////////////////////////////////
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmAgendaGer.MostraFormAgendaImp();
begin
  if DBCheck.CriaFm(TFmAgendaImp, FmAgendaImp, afmoNegarComAviso) then
  begin
    FmAgendaImp.ShowModal;
    FmAgendaImp.Destroy;
  end;
end;

function TFmAgendaGer.NO_EntiDeColuna(Coluna: Integer;
  Lista: array of String): String;
begin
  Result := Lista[Coluna];
end;

procedure TFmAgendaGer.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 2 then // m�s
  begin
    if FAtualizaMes = False then
    begin
      FAtualizaMes := True;
      //
      MostraCompromissosDoMes();
    end;
  end;
end;

procedure TFmAgendaGer.PlaDiaDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  WL: TPlannerWaitList;
begin
  if (Source is TPlannerWaitList) then
  begin
    WL := TPlannerWaitList(Source);
    WL.MoveToPlanner(PlaDia, WL.ItemIndex, X, Y);
  end;
end;

procedure TFmAgendaGer.PlaDiaDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept := (Source is TPlannerWaitList);
end;

procedure TFmAgendaGer.PlaDiaItemDblClick(Sender: TObject;
  Item: TPlannerItem);
const
  Data = 0;
  Hora = 0;
  Terceiro = 0;
var
  Coluna, Entidade, QuestaoCod: Integer;
begin
  ReopenAgendaEve(Item.DBTag);
  //
  case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
    (*0*)qagIndefinido : ; // nada
    (*1*)qagAvulso     :
    begin
      QuestaoCod := QrAgendaEveQuestaoCod.Value;
      Coluna := Item.ItemPos + 1;
      Entidade := EntidadeDeColuna(Coluna, FCols1_Cod);
      //
      AgendaGerAll.MostraFormAgendaEve(QuestaoCod, Terceiro, False, Data, Hora, Data, Hora, [Entidade]);
    end;
    else
    // Espec�ficos do app
    (*2*)(*qagOSBgstrl*)
    AgendaGerAll.MostraFormItemAgenda(Self,
                         (*PlaDia,*) Item,
                         QrAgendaEveQuestaoTyp.Value,
                         QrAgendaEveQuestaoCod.Value,
                         QrAgendaEveQuestaoExe.Value);
  end;
end;

procedure TFmAgendaGer.PlaDiaItemMove(Sender: TObject; Item: TPlannerItem;
  FromBegin, FromEnd, FromPos, ToBegin, ToEnd, ToPos: Integer);
  function RecompoeData(DH: TDateTime): TDateTime;
  var
    Dia: Integer;
  begin
    Dia := Trunc(TPDataDia.Date);
    Result := Dia + (DH - Trunc(DH));
  end;
var
  Evento, I, P, Terceiro, Controle, EntiFrom, Entidade: Integer;
  DHIni, DHFim: TDateTime;
  Inicio, Termino, NO_Terceiro: String;
  MudouTerceiro: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    Evento  := Item.DBTag;
    DHIni   := RecompoeData(Item.ItemStartTime);
    DHFim   := RecompoeData(Item.ItemEndTime);
    Inicio  := Geral.FDT(DHIni, 109);
    Termino := Geral.FDT(DHFim, 109);
    for I := 0 to PlaDia.Items.Count - 1 do
    begin
      if PlaDia.Items[I].DBTag = Evento then
      begin
        if (PlaDia.Items[I].ItemStartTime <> DHIni) or
        (PlaDia.Items[I].ItemEndTime <> DHFim) then
        begin

          P := PlaDia.Items[I].ItemPos;
          //
          PlaDia.Items[I].ItemStartTime := DHIni;
          PlaDia.Items[I].ItemEndTime := DHFim;
          // ??? porque estraga????
          PlaDia.Items[I].ItemPos := P;
        end;
      end;
    end;
    // Disponibilizar dados b�sicos do evento
    ReopenAgendaEve(Item.DBTag);
    Terceiro := EntidadeDeColuna(ToPos + 1, FCols1_Cod);
    NO_Terceiro := NO_EntiDeColuna(ToPos + 1, FCols1_Nom);
    MudouTerceiro := FromPos <> ToPos;
    //
    Controle := 0;
    // Caso mudou a entidade do Item...
    if MudouTerceiro then
    begin
      // ... verificar se a entidade j� est� no compromisso!
      //
      Controle := AgendaGerAll.EntidadeEstaNoEvento(Terceiro,
        Item.DBTag,
        QrAgendaEveQuestaoTyp.Value,
        QrAgendaEveQuestaoCod.Value,
        QrAgendaEveQuestaoExe.Value);
      if Controle <> 0 then
      begin
        Geral.MB_Aviso('Altera��o cancelada! ' + sLineBreak +
        'A entidade receptora j� est� no evento e seu registro n�o pode ser duplicado!');
        //
        // Voltar tudo...
        MostraCompromissosDoDia();
        // ... e desistir de mudar.
        Exit;
      end else
      begin
        EntiFrom := EntidadeDeColuna(FromPos + 1, FCols1_Cod);
        Controle := AgendaGerAll.EntidadeEstaNoEvento(EntiFrom,
          Item.DBTag,
          QrAgendaEveQuestaoTyp.Value,
          QrAgendaEveQuestaoCod.Value,
          QrAgendaEveQuestaoExe.Value);
      end;
    end;
    //fazer atualiza��o de hor�rio
    // 2013-06-12
    // ERRO!!! Colocando Agente no lugar do cliente (campo "Terceiro")!
    // Desabilitado alteracao equivocada do terceiro!
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaeve', False, [
    'Inicio', 'Termino'(*, 'Terceiro'*)], [
    'Codigo'], [
    Inicio, Termino(*, Terceiro*)], [
    Item.DBTag], True);
    // Fim 2013-06-12

    case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
      (*0*)qagIndefinido : ; // nada
      (*1*)qagAvulso     :
      begin
        if Terceiro <> EntiFrom then
        begin
          Entidade := Terceiro;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaent', False, [
          'Entidade'], ['Controle'], [
          Entidade], [Controle], True);
        end;
      end;
      else
      (*2*)(*qagOSBgstrl*)
        if AgendaGerAll.AtualizaHorarioCompromisso(Inicio, Termino,
        QrAgendaEveQuestaoTyp.Value, QrAgendaEveQuestaoCod.Value,
        QrAgendaEveQuestaoExe.Value, Terceiro, Controle, MudouTerceiro) then
        begin
          {
          MyItem.Evento.ParticipanteCodi := Terceiro;
          MyItem.Evento.ParticipanteNome := NO_Terceiro;
          }
        end;
        ;
    end;
  finally
    //FOnAfterUserMoved := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAgendaGer.PlaDiaItemMoving(Sender: TObject; Item: TPlannerItem;
  DeltaBegin, DeltaPos: Integer; var Allow: Boolean);
begin
{
  if FOnAfterUserMoved = False then
    FEntiMoving := EntidadeDeColuna(Item.ItemPos, FCols1_Cod);
}
end;

procedure TFmAgendaGer.PlaDiaItemRightClick(Sender: TObject;
  Item: TPlannerItem);
begin
  FItem_Dia := Item;
end;

procedure TFmAgendaGer.PlaDiaItemSize(Sender: TObject; Item: TPlannerItem;
  Position, FromBegin, FromEnd, ToBegin, ToEnd: Integer);
const
  Terceiro = 0;
  Controle = 0;
  MudouTerceiro = False;
var
  DIni, DFim, Dta: TDateTime;
  Inicio, Termino: String;
  PDU, PoI: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    ReopenAgendaEve(Item.DBTag);
    //
    PDU := PlaSem.Display.DisplayUnit;
    Dta := TPDataDia.Date;
    PoI := 0; // Position;
    DIni := AgendaGerAll.ItemBandToItemTime(ToBegin, PDU, Dta, PoI);
    DFim := AgendaGerAll.ItemBandToItemTime(ToEnd  , PDU, Dta, PoI);
    Inicio  := Geral.FDT(DIni, 109);
    Termino := Geral.FDT(DFim, 109);
    //fazer atualiza��o de hor�rio
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaeve', False, [
    'Inicio', 'Termino'], [
    'Codigo'], [
    Inicio, Termino], [
    Item.DBTag], True);
    case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
      (*0*)qagIndefinido : ; // nada
      (*1*)qagAvulso     : ;  //nada?;
      else
      (*2*)(*qagOSBgstrl*)
      if AgendaGerAll.AtualizaHorarioCompromisso(Inicio, Termino,
      QrAgendaEveQuestaoTyp.Value, QrAgendaEveQuestaoCod.Value,
      QrAgendaEveQuestaoExe.Value, Terceiro, Controle, MudouTerceiro) then
        MostraCompromissosDoDia();
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAgendaGer.PlaDiaPlannerDblClick(Sender: TObject; Position, FromSel,
  FromSelPrecise, ToSel, ToSelPrecise: Integer);
const
  Terceiro = 0;
var
  Coluna, Entidade, QuestaoCod: Integer;
  DataInicio, DataTermino: TDateTime;
  HoraInicio, HoraTermino: TTime;
begin
  Coluna      := Position + 1;
  Entidade    := EntidadeDeColuna(Coluna, FCols1_Cod);
  QuestaoCod  := 0;
  DataInicio  := EncodeDate(PlaDia.Mode.Year, PlaDia.Mode.Month, PlaDia.Mode.Day);
  DataTermino := DataInicio;
  //
  HoraInicio  := (FromSelPrecise / 60 / 24);
  HoraTermino := (ToSelPrecise / 60 / 24);
  //
  AgendaGerAll.MostraFormAgendaEve(QuestaoCod, Terceiro, True,
    DataInicio, HoraInicio, DataTermino, HoraTermino, [Entidade]);
  //
  MostraCompromissosDoDia();
end;

procedure TFmAgendaGer.PlaDiaPlannerNext(Sender: TObject);
begin
  TPDataDia.Date := TPDataDia.Date + 1;
  MostraCompromissosDoDia();
end;

procedure TFmAgendaGer.PlaDiaPlannerPrev(Sender: TObject);
begin
  TPDataDia.Date := TPDataDia.Date - 1;
  MostraCompromissosDoDia();
end;

procedure TFmAgendaGer.PlaMesDblClick(Sender: TObject; SelDate: TDateTime);
const
  Entidade = 0;
  QuestaoCod = 0;
  HoraInicio = 0;
  HoraTermino = 0;
var
  DataInicio, DataTermino: TDateTime;
begin
  DataInicio  := SelDate;
  DataTermino := DataInicio;
  //
  //
  AgendaGerAll.MostraFormAgendaEve(QuestaoCod, Entidade, True,
    DataInicio, HoraInicio, DataTermino, HoraTermino, []);
  //
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.PlaMesItemDblClick(Sender: TObject; Item: TPlannerItem);
var
  QuestaoCod, Coluna, Entidade: Integer;
begin
  ReopenAgendaEve(Item.DBTag);
  //
  case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
    (*0*)qagIndefinido : ; // nada
    (*1*)qagAvulso     :
    begin
      QuestaoCod := QrAgendaEveQuestaoCod.Value;
      Coluna     := Item.ItemPos + 1;
      Entidade   := EntidadeDeColuna(Coluna, FCols1_Cod);
      //
      AgendaGerAll.MostraFormAgendaEve(QuestaoCod, Entidade, False, 0, 0, 0, 0, []);
    end;
    else
    (*2*)(*qagOSBgstrl*)
    AgendaGerAll.MostraFormItemAgenda(Self,
                         (*PlaMes,*) Item,
                         QrAgendaEveQuestaoTyp.Value,
                         QrAgendaEveQuestaoCod.Value,
                         QrAgendaEveQuestaoExe.Value);
  end;
end;

procedure TFmAgendaGer.PlaMesItemMove(Sender: TObject;
  APlannerItem: TPlannerItem; FromStartDate, FromEndDate, ToStartDate,
  ToEndDate: TDateTime);
const
  Terceiro = 0;
  Controle = 0;
  MudouTerceiro = False;
var
  Inicio, Termino: String;
  Dias: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    // Disponibilizar dados b�sicos do evento
    ReopenAgendaEve(APlannerItem.DBTag);
    Dias := Trunc(ToStartDate) - Trunc(FromStartDate);
    Inicio  := Geral.FDT(QrAgendaEveInicio.Value  + Dias, 109);
    Termino := Geral.FDT(QrAgendaEveTermino.Value + Dias, 109);
    //
    //fazer atualiza��o de hor�rio
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaeve', False, [
    'Inicio', 'Termino'], ['Codigo'], [
    Inicio, Termino], [APlannerItem.DBTag], True);
    case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
      (*0*)qagIndefinido : ; // nada
      (*1*)qagAvulso     : ;  //nada?;
      else
      (*2*)(*qagOSBgstrl*)
        if AgendaGerAll.AtualizaHorarioCompromisso(Inicio, Termino,
        QrAgendaEveQuestaoTyp.Value, QrAgendaEveQuestaoCod.Value,
        QrAgendaEveQuestaoExe.Value, Terceiro, Controle, MudouTerceiro) then
        ;
    end;
  finally
    //FOnAfterUserMoved := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAgendaGer.PlaMesItemRightClick(Sender: TObject;
  Item: TPlannerItem);
begin
  FItem_Mes := Item;
end;

procedure TFmAgendaGer.PlaMesMonthChange(Sender: TObject; origDate,
  newDate: TDateTime);
begin
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.PlaMesMonthChanged(Sender: TObject; origDate,
  newDate: TDateTime);
begin
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.PlaMesYearChange(Sender: TObject; origDate,
  newDate: TDateTime);
begin
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.PlaMesYearChanged(Sender: TObject; origDate,
  newDate: TDateTime);
begin
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.PlaSemItemDblClick(Sender: TObject; Item: TPlannerItem);
var
  QuestaoCod, Coluna, Entidade: Integer;
begin
  ReopenAgendaEve(Item.DBTag);
  //
  case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
    (*0*)qagIndefinido : ; // nada
    (*1*)qagAvulso     :
    begin
      QuestaoCod := QrAgendaEveQuestaoCod.Value;
      Coluna     := Item.ItemPos + 1;
      Entidade   := EntidadeDeColuna(Coluna, FCols1_Cod);
      //
      AgendaGerAll.MostraFormAgendaEve(QuestaoCod, Entidade, False, 0, 0, 0, 0, []);
    end;
    else
    (*2*)(*qagOSBgstrl*)
    AgendaGerAll.MostraFormItemAgenda(Self,
                         (*PlaSem,*) Item,
                         QrAgendaEveQuestaoTyp.Value,
                         QrAgendaEveQuestaoCod.Value,
                         QrAgendaEveQuestaoExe.Value);
  end;
end;

procedure TFmAgendaGer.PlaSemItemMove(Sender: TObject; Item: TPlannerItem;
  FromBegin, FromEnd, FromPos, ToBegin, ToEnd, ToPos: Integer);
const
  Terceiro = 0;
  Controle = 0;
  MudouTerceiro = False;
var
  DHIni, DHFim: TDateTime;
  Inicio, Termino: String;
begin
  Screen.Cursor := crHourGlass;
  try
    DHIni   := Item.ItemStartTime;
    DHFim   := Item.ItemEndTime;
    Inicio  := Geral.FDT(DHIni, 109);
    Termino := Geral.FDT(DHFim, 109);
    // Disponibilizar dados b�sicos do evento
    ReopenAgendaEve(Item.DBTag);
    //
    //fazer atualiza��o de hor�rio
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaeve', False, [
    'Inicio', 'Termino'], ['Codigo'], [
    Inicio, Termino], [Item.DBTag], True);
    case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
      (*0*)qagIndefinido : ; // nada
      (*1*)qagAvulso     : ;  //nada?;
      else
      (*2*)(*qagOSBgstrl*)
        if AgendaGerAll.AtualizaHorarioCompromisso(Inicio, Termino,
        QrAgendaEveQuestaoTyp.Value, QrAgendaEveQuestaoCod.Value,
        QrAgendaEveQuestaoExe.Value, Terceiro, Controle, MudouTerceiro) then
        ;
    end;
  finally
    //FOnAfterUserMoved := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAgendaGer.PlaSemItemRightClick(Sender: TObject;
  Item: TPlannerItem);
begin
  FItem_Sem := Item;
end;

procedure TFmAgendaGer.PlaSemItemSize(Sender: TObject; Item: TPlannerItem;
  Position, FromBegin, FromEnd, ToBegin, ToEnd: Integer);
const
  Terceiro = 0;
  Controle = 0;
  MudouTerceiro = False;
var
  DIni, DFim, Dta: TDateTime;
  Inicio, Termino: String;
  PDU, PoI: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    ReopenAgendaEve(Item.DBTag);
    //
    PDU := PlaSem.Display.DisplayUnit;
    Dta := TPDataSem.Date;
    PoI := Position;
    DIni := AgendaGerAll.ItemBandToItemTime(ToBegin, PDU, Dta, PoI);
    DFim := AgendaGerAll.ItemBandToItemTime(ToEnd  , PDU, Dta, PoI);
    Inicio  := Geral.FDT(DIni, 109);
    Termino := Geral.FDT(DFim, 109);
    //fazer atualiza��o de hor�rio
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaeve', False, [
    'Inicio', 'Termino'], [
    'Codigo'], [
    Inicio, Termino], [
    Item.DBTag], True);
    case TAgendaQuestao(QrAgendaEveQuestaoTyp.Value) of
      (*0*)qagIndefinido : ; // nada
      (*1*)qagAvulso     : ;  //nada?;
      else
      (*2*)(*qagOSBgstrl*)
      if AgendaGerAll.AtualizaHorarioCompromisso(Inicio, Termino,
      QrAgendaEveQuestaoTyp.Value, QrAgendaEveQuestaoCod.Value,
      QrAgendaEveQuestaoExe.Value, Terceiro, Controle, MudouTerceiro) then
        MostraCompromissosDaSem(True);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAgendaGer.PlaSemPlannerDblClick(Sender: TObject; Position, FromSel,
  FromSelPrecise, ToSel, ToSelPrecise: Integer);
const
  Entidade   = 0;
  QuestaoCod = 0;
var
  DataInicio, DataTermino: TDateTime;
  HoraInicio, HoraTermino: TTime;
begin
   DataInicio  :=
    EncodeDate(PlaSem.Mode.Year, PlaSem.Mode.Month, PlaSem.Mode.Day) + Position;
  DataTermino := DataInicio;
  //
  HoraInicio  := (FromSelPrecise / 60 / 24);
  HoraTermino := (ToSelPrecise / 60 / 24);
  //
  AgendaGerAll.MostraFormAgendaEve(QuestaoCod, Entidade, True,
    DataInicio, HoraInicio, DataTermino, HoraTermino, []);
  //
  MostraCompromissosDaSem(True);
end;

procedure TFmAgendaGer.PlaSemPlannerNext(Sender: TObject);
begin
  TPDataSem.Date := TPDataSem.Date + 7;
  MostraCompromissosDaSem(False);
end;

procedure TFmAgendaGer.PlaSemPlannerPrev(Sender: TObject);
begin
  TPDataSem.Date := TPDataSem.Date - 7;
  MostraCompromissosDaSem(False);
end;

procedure TFmAgendaGer.QrEventos1AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEnti1, Dmod.MyDB, [
  'SELECT ent.Entidade, aee.Controle  ',
  'FROM agendaent aee  ',
  'INNER JOIN agengrent ent ON ent.Entidade=aee.Entidade ',
  'WHERE aee.Codigo=' + Geral.FF0(QrEventos1Codigo.Value),
  '']);
end;

procedure TFmAgendaGer.QrEventos2AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgentes, Dmod.MyDB, [
  'SELECT ent.Entidade, osa.Controle ',
  'FROM osage osa ',
  'INNER JOIN agengrent ent ON ent.Entidade=osa.Agente ',
  'WHERE osa.Codigo=' + Geral.FF0(QrEventos2QuestaoCod.Value),
  'AND ent.Codigo IS NOT NULL',
  '']);
end;

procedure TFmAgendaGer.QrEventos2BeforeClose(DataSet: TDataSet);
begin
  QrAgentes.Close;
end;

procedure TFmAgendaGer.Removerentidadedoevento1Click(Sender: TObject);
const
  Pergunta = 'Confirma a remo��o da entidade do evento?';
  Tabela = 'agendaent';
var
  Coluna, Entidade, Codigo: Integer;
begin
  Coluna   := TPlannerItem(FItem_Dia).ItemPos + 1;
  Entidade := EntidadeDeColuna(Coluna, FCols1_Cod);
  Codigo   := TPlannerItem(FItem_Dia).DBTag;
  //
  if UMyMod.ExcluiRegistroIntArr(Pergunta, Tabela, [
  'Codigo', 'Entidade'], [Codigo, Entidade]) = ID_YES then
    MostraCompromissosDoDia();
end;

procedure TFmAgendaGer.ReopenAgendaEve(ItemDBTag: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgendaEve, DmoD.MyDB, [
  'SELECT * ',
  'FROM agendaeve ',
  'WHERE Codigo=' + Geral.FF0(ItemDBTag),
  '']);
end;

procedure TFmAgendaGer.ReopenAgenGrEnt();
var
  Data: TDatetime;
begin
  LimpaPlanner(PlaDia);
  Data := Int(TPDataDia.Date);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgenGrEnt, Dmod.MyDB, [
  'SELECT age.*, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM agengrent age ',
  'LEFT JOIN entidades ent ON ent.Codigo=age.Entidade ',
  'WHERE age.Codigo=' + Geral.FF0(QrAgenGrCabDiaCodigo.Value),
  'ORDER BY Sigla ',
  '']);
  SetLength(FCols1_Cod, QrAgenGrEnt.RecordCount + 1);
  SetLength(FCols1_Nom, QrAgenGrEnt.RecordCount + 1);
  //
  PlaDia.Positions := QrAgenGrEnt.RecordCount;
  PlaDia.PositionWidth := QrAgenGrCabDiaColLarg.Value;
  PlaDia.Visible := True;
  //PlaDia.Header.Captions.Count := QrAgenGrEnt.RecordCount + 1;
  ConfiguraPlannerMode(PlaDia, Data, 0);

  PlaDia.Header.Captions.Clear;
  PlaDia.Header.Captions.Add('#');
  QrAgenGrEnt.First;
  while not QrAgenGrEnt.Eof do
  begin
    FCols1_Cod[QrAgenGrEnt.RecNo] := QrAgenGrEntEntidade.Value;
    FCols1_Nom[QrAgenGrEnt.RecNo] := QrAgenGrEntNO_ENT.Value;
    //
    PlaDia.Header.Captions.Add(QrAgenGrEntSigla.Value);
    //PlaDia.Header.Captions[QrAgenGrEnt.RecNo] := QrAgenGrEntSigla.Value;
    //PlaDia.PositionWidths[QrAgenGrEnt.RecNo - 1] := QrAgenGrCabDiaColLarg.Value;
    //
    QrAgenGrEnt.Next;
  end;
  PlaDia.Header.Captions.Add('#2');
  PlaDia.Header.Captions.Add('#3');
  PlaDia.Header.Captions.Add('#4');
  PlaDia.Display.DisplayUnit := QrAgenGrCabDiaInterMinut.Value;
  PlaDia.Refresh;
  Application.ProcessMessages;
end;

procedure TFmAgendaGer.SBAgenGrCabDiaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  AgendaGerAll.MostraFormAgenGrCab(EdAgenGrCabDia.ValueVariant);
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(
      EdAgenGrCabDia, CBAgenGrCabDia, QrAgenGrCabDia, VAR_CADASTRO);
end;

procedure TFmAgendaGer.SBAgenGrCabMesClick(Sender: TObject);
begin
(*�  VAR_CADASTRO := 0;
  FmPrincipal.MostraAgenGrCab(EdAgenGrCabMes.ValueVariant);
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(
      EdAgenGrCabMes, CBAgenGrCabMes, QrAgenGrCabMes, VAR_CADASTRO);
*)
end;

procedure TFmAgendaGer.SBAgenGrCabSemClick(Sender: TObject);
begin
{�
  VAR_CADASTRO := 0;
  FmPrincipal.MostraAgenGrCab(EdAgenGrCabSem.ValueVariant);
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(
      EdAgenGrCabSem, CBAgenGrCabSem, QrAgenGrCabSem, VAR_CADASTRO);
}
end;

procedure TFmAgendaGer.SbImprimeClick(Sender: TObject);
begin
  MostraFormAgendaImp();
end;

procedure TFmAgendaGer.SBMostraAgendaDiaClick(Sender: TObject);
begin
  MostraCompromissosDoDia();
end;

procedure TFmAgendaGer.SBMostraAgendaMesClick(Sender: TObject);
begin
  MostraCompromissosDoMes();
end;

procedure TFmAgendaGer.SBMostraAgendaSemClick(Sender: TObject);
begin
  MostraCompromissosDaSem(True);
end;

procedure TFmAgendaGer.SbSairClick(Sender: TObject);
begin
  Close;
  //
  MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmAgendaGer.TimerIndicatorTimer(Sender: TObject);
begin
  PlaDia.SideBar.TimeIndicator := not PlaDia.SideBar.TimeIndicator;
  PlaSem.SideBar.TimeIndicator := not PlaSem.SideBar.TimeIndicator;
end;

procedure TFmAgendaGer.TMCarregaTimer(Sender: TObject);

  procedure ConfiguraComponentes(Habilitado: Boolean);
  begin
    GB_L.Enabled         := Habilitado;
    PageControl1.Enabled := Habilitado;
    BtOK.Enabled         := Habilitado;
    BtSaida.Enabled      := Habilitado;
  end;

begin
  TMCarrega.Enabled := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados da agenda!');
  try
    ConfiguraComponentes(False);
    //
    UnDmkDAC_PF.AbreQuery(QrAgenGrCabDia, Dmod.MyDB);
    UnDmkDAC_PF.AbreQuery(QrAgenGrCabSem, Dmod.MyDB);
    UnDmkDAC_PF.AbreQuery(QrAgenGrCabMes, Dmod.MyDB);
    LimpaPlanner(PlaDia);
    //
    AgendaGerApp.ConfiguraCompsAgenGrCabDia(EdAgenGrCabDia, CBAgenGrCabDia,
      QrAgenGrCabDia);
    LimpaLoopsDeAgenda();
    AtualizaTudo();
  finally
    ConfiguraComponentes(True);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmAgendaGer.TPDataDiaChange(Sender: TObject);
begin
  HabilitaReShowDia();
end;

procedure TFmAgendaGer.TPDataDiaClick(Sender: TObject);
begin
  HabilitaReShowDia();
end;

procedure TFmAgendaGer.TPDataMesChange(Sender: TObject);
begin
  HabilitaReShowMes();
end;

procedure TFmAgendaGer.TPDataMesClick(Sender: TObject);
begin
  HabilitaReShowMes();
end;

procedure TFmAgendaGer.TPDataSemChange(Sender: TObject);
begin
  HabilitaReShowSem();
end;

procedure TFmAgendaGer.TPDataSemClick(Sender: TObject);
begin
  HabilitaReShowSem();
end;

procedure TFmAgendaGer.UpdateHeadersSem();
var
  I, N: Integer;
  x: String;
  DataIni: TDateTime;
begin
  PlaSem.Visible := True;
  LimpaPlanner(PlaSem);
  //
  DataIni := Int(TPDataSem.Date);
  ConfiguraPlannerMode(PlaSem, DataIni, 7);
  PlaSem.Header.Captions.Clear;
  PlaSem.Header.Captions.Add('');
  for i := 1 to PlaSem.Positions do
  begin
    N := Trunc(DataIni + i - 1);
    X := Copy(dmkPF.NomeDiaDaSemana(DayOfWeek(N)), 1, 3);
    PlaSem.Header.Captions.Add(X + ', ' + Geral.FDT(N, 2));
  end;
  PlaSem.Header.Captions.Add('');
end;

{
object Panel8: TPanel
  Left = 0
  Top = 0
  Width = 996
  Height = 45
  Align = alTop
  BevelOuter = bvNone
  ParentBackground = False
  TabOrder = 0
  object Label7: TLabel
    Left = 120
    Top = 4
    Width = 108
    Height = 13
    Caption = 'Grupo de visualiza'#231#227'o:'
  end
  object SBAgenGrCabMes: TSpeedButton
    Left = 508
    Top = 20
    Width = 21
    Height = 21
    Caption = '...'
    OnClick = SBAgenGrCabMesClick
  end
  object SBMostraAgendaMes: TSpeedButton
    Left = 528
    Top = 20
    Width = 21
    Height = 21
    Caption = '>'
    Enabled = False
    OnClick = SBMostraAgendaMesClick
  end
  object Label8: TLabel
    Left = 8
    Top = 4
    Width = 55
    Height = 13
    Caption = 'Data inicial:'
  end
  object EdAgenGrCabMes: TdmkEditCB
    Left = 120
    Top = 20
    Width = 56
    Height = 21
    Alignment = taRightJustify
    TabOrder = 1
    FormatType = dmktfInteger
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ValMin = '-2147483647'
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    Texto = '0'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = 0
    OnChange = EdAgenGrCabMesChange
    DBLookupComboBox = CBAgenGrCabMes
    IgnoraDBLookupComboBox = False
  end
  object CBAgenGrCabMes: TdmkDBLookupComboBox
    Left = 176
    Top = 20
    Width = 333
    Height = 21
    KeyField = 'Codigo'
    ListField = 'Nome'
    ListSource = DsAgenGrCabMes
    TabOrder = 2
    dmkEditCB = EdAgenGrCabMes
    UpdType = utYes
  end
  object TPDataMes: TdmkEditDateTimePicker
    Left = 8
    Top = 20
    Width = 108
    Height = 21
    Date = 41322.621779293980000000
    Time = 41322.621779293980000000
    TabOrder = 0
    OnClick = TPDataMesClick
    OnChange = TPDataMesChange
    ReadOnly = False
    DefaultEditMask = '!99/99/99;1;_'
    AutoApplyEditMask = True
    UpdType = utYes
  end
end
}

////////////////////////////////////////////////////////////////////////////////

{
object Label3: TLabel
  Left = 120
  Top = 4
  Width = 108
  Height = 13
  Caption = 'Grupo de visualiza'#231#227'o:'
end
object EdAgenGrCabSem: TdmkEditCB
  Left = 120
  Top = 20
  Width = 56
  Height = 21
  Alignment = taRightJustify
  TabOrder = 1
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '-2147483647'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  OnChange = EdAgenGrCabSemChange
  DBLookupComboBox = CBAgenGrCabSem
  IgnoraDBLookupComboBox = False
end
object CBAgenGrCabSem: TdmkDBLookupComboBox
  Left = 176
  Top = 20
  Width = 333
  Height = 21
  KeyField = 'Codigo'
  ListField = 'Nome'
  ListSource = DsAgenGrCabSem
  TabOrder = 2
  dmkEditCB = EdAgenGrCabSem
  UpdType = utYes
end
object SBAgenGrCabSem: TSpeedButton
  Left = 508
  Top = 20
  Width = 21
  Height = 21
  Caption = '...'
  OnClick = SBAgenGrCabSemClick
end
object SBMostraAgendaSem: TSpeedButton
  Left = 528
  Top = 20
  Width = 21
  Height = 21
  Caption = '>'
  Enabled = False
  OnClick = SBMostraAgendaSemClick
end
}

end.
