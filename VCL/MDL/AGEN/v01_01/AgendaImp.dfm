object FmAgendaImp: TFmAgendaImp
  Left = 339
  Top = 185
  Caption = 'AGE-GEREN-004 :: Impress'#227'o de Compromissos Agendados'
  ClientHeight = 621
  ClientWidth = 654
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 654
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 606
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 558
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 498
        Height = 32
        Caption = 'Impress'#227'o de Compromissos Agendados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 498
        Height = 32
        Caption = 'Impress'#227'o de Compromissos Agendados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 498
        Height = 32
        Caption = 'Impress'#227'o de Compromissos Agendados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 654
    Height = 459
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 654
      Height = 459
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 654
        Height = 459
        Align = alClient
        TabOrder = 0
        object GroupBox5: TGroupBox
          Left = 2
          Top = 15
          Width = 650
          Height = 294
          Align = alTop
          Caption = ' Relacionamentos nos agendamento: '
          TabOrder = 0
          object Panel12: TPanel
            Left = 2
            Top = 15
            Width = 646
            Height = 277
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaCliInt: TLabel
              Left = 4
              Top = 8
              Width = 70
              Height = 13
              Caption = 'Cliente interno:'
            end
            object LaEntidade1: TLabel
              Left = 4
              Top = 32
              Width = 68
              Height = 14
              Caption = 'Participante:'
              Font.Charset = ANSI_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaEntidade2: TLabel
              Left = 4
              Top = 56
              Width = 49
              Height = 14
              Caption = 'Terceiro:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaContratante: TLabel
              Left = 4
              Top = 80
              Width = 68
              Height = 14
              Caption = 'Contratante:'
              Font.Charset = ANSI_CHARSET
              Font.Color = clGreen
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaPagante: TLabel
              Left = 4
              Top = 104
              Width = 47
              Height = 14
              Caption = 'Pagante:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clPurple
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label3: TLabel
              Left = 4
              Top = 236
              Width = 86
              Height = 13
              Caption = 'T'#237'tulo do relat'#243'rio:'
            end
            object EdEmpresa: TdmkEditCB
              Left = 76
              Top = 4
              Width = 48
              Height = 22
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmpresaChange
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 124
              Top = 4
              Width = 508
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DModG.DsEmpresas
              TabOrder = 1
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdEntidade: TdmkEditCB
              Left = 76
              Top = 28
              Width = 48
              Height = 22
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEntidadeChange
              DBLookupComboBox = CBEntidade
              IgnoraDBLookupComboBox = False
            end
            object CBEntidade: TdmkDBLookupComboBox
              Left = 124
              Top = 28
              Width = 508
              Height = 22
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsClientes
              ParentFont = False
              TabOrder = 3
              dmkEditCB = EdEntidade
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdTerceiro: TdmkEditCB
              Left = 76
              Top = 52
              Width = 48
              Height = 22
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTerceiroChange
              DBLookupComboBox = CBTerceiro
              IgnoraDBLookupComboBox = False
            end
            object CBTerceiro: TdmkDBLookupComboBox
              Left = 124
              Top = 52
              Width = 508
              Height = 22
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsTerceiro
              ParentFont = False
              TabOrder = 5
              dmkEditCB = EdTerceiro
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object GroupBox2: TGroupBox
              Left = 364
              Top = 127
              Width = 269
              Height = 62
              Caption = ' Per'#237'odo do contato: '
              TabOrder = 7
              object Label1: TLabel
                Left = 8
                Top = 20
                Width = 58
                Height = 13
                Caption = 'Data inicial: '
              end
              object Label2: TLabel
                Left = 124
                Top = 20
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPInicioIni: TdmkEditDateTimePicker
                Left = 8
                Top = 36
                Width = 112
                Height = 21
                Date = 0.730390057869954000
                Time = 0.730390057869954000
                TabOrder = 0
                OnClick = TPInicioIniClick
                OnChange = TPInicioIniChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
              object TPInicioFim: TdmkEditDateTimePicker
                Left = 124
                Top = 36
                Width = 112
                Height = 21
                Date = 41108.730390057870000000
                Time = 41108.730390057870000000
                TabOrder = 1
                OnClick = TPInicioFimClick
                OnChange = TPInicioFimChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
            end
            object CGOperacao: TdmkCheckGroup
              Left = 4
              Top = 128
              Width = 357
              Height = 61
              Caption = ' Origens dos agendamentos (tipos de compromissos): '
              Columns = 2
              Items.Strings = (
                '???Avulso???'
                '????OS??????')
              TabOrder = 6
              OnClick = CGOperacaoClick
              QryCampo = 'Operacao'
              UpdCampo = 'Operacao'
              UpdType = utYes
              Value = 0
              OldValor = 0
            end
            object EdContratante: TdmkEditCB
              Left = 76
              Top = 76
              Width = 48
              Height = 22
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdContratanteChange
              DBLookupComboBox = CBContratante
              IgnoraDBLookupComboBox = False
            end
            object CBContratante: TdmkDBLookupComboBox
              Left = 124
              Top = 76
              Width = 508
              Height = 22
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsContratante
              ParentFont = False
              TabOrder = 9
              dmkEditCB = EdContratante
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPagante: TdmkEditCB
              Left = 76
              Top = 100
              Width = 48
              Height = 22
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clPurple
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 10
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdPaganteChange
              DBLookupComboBox = CBPagante
              IgnoraDBLookupComboBox = False
            end
            object CBPagante: TdmkDBLookupComboBox
              Left = 124
              Top = 100
              Width = 508
              Height = 22
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clPurple
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsPagante
              ParentFont = False
              TabOrder = 11
              dmkEditCB = EdPagante
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdVarf_Titulo: TEdit
              Left = 4
              Top = 252
              Width = 629
              Height = 21
              TabOrder = 12
              Text = 'AGENDA DE COMPROMISSOS'
            end
            object CGQuestaoExe_01: TdmkCheckGroup
              Left = 4
              Top = 192
              Width = 629
              Height = 41
              Caption = '  Espec'#237'fico para OSs: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'Vistoria'
                'Execu'#231#227'o'
                'Escalonamento')
              TabOrder = 13
              OnClick = CGQuestaoExe_01Click
              UpdType = utYes
              Value = 1
              OldValor = 0
            end
          end
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 309
          Width = 650
          Height = 148
          Align = alClient
          DataSource = DsPesq
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 507
    Width = 654
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 650
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 551
    Width = 654
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 508
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 506
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 156
    Top = 65524
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrClientesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 184
    Top = 65524
  end
  object QrTerceiro: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 212
    Top = 65524
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrTerceiroCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrTerceiroCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTerceiroNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTerceiro: TDataSource
    DataSet = QrTerceiro
    Left = 240
    Top = 65524
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDBn
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM _agendaimp_'
      'ORDER BY Inicio')
    Left = 528
    Top = 252
    object QrPesqCtrlParti: TIntegerField
      FieldName = 'CtrlParti'
      Required = True
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPesqQuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
      Required = True
    end
    object QrPesqQuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
      Required = True
    end
    object QrPesqQuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
      Required = True
    end
    object QrPesqInicio: TDateTimeField
      FieldName = 'Inicio'
      Required = True
    end
    object QrPesqTermino: TDateTimeField
      FieldName = 'Termino'
      Required = True
    end
    object QrPesqEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
      Required = True
    end
    object QrPesqTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrPesqLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrPesqCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrPesqCption: TSmallintField
      FieldName = 'Cption'
      Required = True
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrPesqNotas: TWideStringField
      FieldName = 'Notas'
      Required = True
      Size = 255
    end
    object QrPesqFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
      Required = True
    end
    object QrPesqNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
      Required = True
      Size = 60
    end
    object QrPesqNO_QuestaoExe: TWideStringField
      FieldName = 'NO_QuestaoExe'
      Required = True
      Size = 60
    end
    object QrPesqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesqNO_Lugar: TWideStringField
      FieldName = 'NO_Lugar'
      Size = 100
    end
    object QrPesqNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 556
    Top = 252
  end
  object frxAGE_GEREN_004_01: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 41410.866535844910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxAGE_GEREN_004_01GetValue
    Left = 612
    Top = 252
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        Stretched = True
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 71.811026060000000000
          Height = 20.787401570000000000
          DataField = 'Inicio'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."Inicio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 71.811021180000000000
          Width = 71.811026060000000000
          Height = 20.787401570000000000
          DataField = 'Termino'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."Termino"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 143.622042360000000000
          Width = 268.346454250000000000
          Height = 20.787401570000000000
          DataField = 'NO_Lugar'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_Lugar"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 0.000048820000000000
          Top = 20.787404020000000000
          Width = 226.771738980000000000
          Height = 17.007874020000000000
          DataField = 'NO_QuestaoExe'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_QuestaoExe"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 226.881928820000000000
          Top = 20.787404020000000000
          Width = 453.543538980000000000
          Height = 17.007874020000000000
          DataField = 'NO_FatoGeradr'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_FatoGeradr"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 3.779578820000000000
          Top = 37.795278029999990000
          Width = 672.756278980000000000
          Height = 37.795278030000000000
          StretchMode = smMaxHeight
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_DESCRI_ATIVI]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 411.968818820000000000
          Width = 268.346454250000000000
          Height = 20.787401570000000000
          DataField = 'NO_ENT'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NO_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 120.944884330000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PARTICIPANTE]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 415.748300000000000000
          Top = 64.252010000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Top = 64.252010000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa: [VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 83.149660000000000000
          Width = 71.811026060000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 71.811021180000000000
          Top = 83.149660000000000000
          Width = 71.811026060000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T'#233'rmino')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 143.622042360000000000
          Top = 83.149660000000000000
          Width = 268.346454250000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Top = 100.157480310000000000
          Width = 226.771738980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Atividade')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 226.881928820000000000
          Top = 100.157480310000000000
          Width = 453.543538980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pauta')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 411.968770000000000000
          Top = 83.149660000000000000
          Width = 268.346454250000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        RowCount = 1
      end
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CtrlParti=CtrlParti'
      'Codigo=Codigo'
      'QuestaoTyp=QuestaoTyp'
      'QuestaoCod=QuestaoCod'
      'QuestaoExe=QuestaoExe'
      'Inicio=Inicio'
      'Termino=Termino'
      'EmprEnti=EmprEnti'
      'Terceiro=Terceiro'
      'Local=Local'
      'Cor=Cor'
      'Cption=Cption'
      'Nome=Nome'
      'Notas=Notas'
      'FatoGeradr=FatoGeradr'
      'NO_FatoGeradr=NO_FatoGeradr'
      'NO_QuestaoExe=NO_QuestaoExe'
      'Ativo=Ativo'
      'DESCRI_ATIVI=DESCRI_ATIVI'
      'NO_Lugar=NO_Lugar'
      'NO_ENT=NO_ENT')
    DataSet = QrPesq
    BCDToCurrency = False
    Left = 584
    Top = 252
  end
  object QrContratante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 276
    Top = 65524
    object QrContratanteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrContratanteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsContratante: TDataSource
    DataSet = QrContratante
    Left = 304
    Top = 65524
  end
  object QrPagante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 340
    Top = 65524
    object QrPaganteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrPaganteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPagante: TDataSource
    DataSet = QrPagante
    Left = 368
    Top = 65524
  end
end
