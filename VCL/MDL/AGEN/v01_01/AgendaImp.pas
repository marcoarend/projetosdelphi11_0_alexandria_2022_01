unit AgendaImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkListas, Menus, frxClass, frxDBSet, UnDmkEnums;

type
  TFmAgendaImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesCodUsu: TIntegerField;
    QrClientesCliInt: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    QrTerceiro: TmySQLQuery;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroCliInt: TIntegerField;
    QrTerceiroCodUsu: TIntegerField;
    QrTerceiroNOMEENTIDADE: TWideStringField;
    DsTerceiro: TDataSource;
    GroupBox5: TGroupBox;
    Panel12: TPanel;
    LaCliInt: TLabel;
    LaEntidade1: TLabel;
    LaEntidade2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    TPInicioIni: TdmkEditDateTimePicker;
    Label1: TLabel;
    TPInicioFim: TdmkEditDateTimePicker;
    Label2: TLabel;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    DBGrid1: TDBGrid;
    CGOperacao: TdmkCheckGroup;
    QrPesqCtrlParti: TIntegerField;
    QrPesqCodigo: TIntegerField;
    QrPesqQuestaoTyp: TIntegerField;
    QrPesqQuestaoCod: TIntegerField;
    QrPesqQuestaoExe: TIntegerField;
    QrPesqInicio: TDateTimeField;
    QrPesqTermino: TDateTimeField;
    QrPesqEmprEnti: TIntegerField;
    QrPesqTerceiro: TIntegerField;
    QrPesqLocal: TIntegerField;
    QrPesqCor: TIntegerField;
    QrPesqCption: TSmallintField;
    QrPesqNome: TWideStringField;
    QrPesqNotas: TWideStringField;
    QrPesqFatoGeradr: TIntegerField;
    QrPesqNO_FatoGeradr: TWideStringField;
    QrPesqNO_QuestaoExe: TWideStringField;
    QrPesqAtivo: TSmallintField;
    frxAGE_GEREN_004_01: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    QrPesqNO_Lugar: TWideStringField;
    QrPesqNO_ENT: TWideStringField;
    LaContratante: TLabel;
    EdContratante: TdmkEditCB;
    CBContratante: TdmkDBLookupComboBox;
    LaPagante: TLabel;
    EdPagante: TdmkEditCB;
    CBPagante: TdmkDBLookupComboBox;
    QrContratante: TmySQLQuery;
    DsContratante: TDataSource;
    QrPagante: TmySQLQuery;
    DsPagante: TDataSource;
    QrContratanteCodigo: TIntegerField;
    QrContratanteNOMEENTIDADE: TWideStringField;
    QrPaganteCodigo: TIntegerField;
    QrPaganteNOMEENTIDADE: TWideStringField;
    Label3: TLabel;
    EdVarf_Titulo: TEdit;
    CGQuestaoExe_01: TdmkCheckGroup;
    BtImprime: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure frxAGE_GEREN_004_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdContratanteChange(Sender: TObject);
    procedure EdPaganteChange(Sender: TObject);
    procedure CGOperacaoClick(Sender: TObject);
    procedure TPInicioIniClick(Sender: TObject);
    procedure TPInicioIniChange(Sender: TObject);
    procedure TPInicioFimChange(Sender: TObject);
    procedure TPInicioFimClick(Sender: TObject);
    procedure CGQuestaoExe_01Click(Sender: TObject);
  private
    { Private declarations }
    FAgendaEmp, FAgendaImp: String;
    F_Psq_OSAlv_, F_Psq_OSAge_, F_Psq_OSCabAlv_: String;
    procedure FechaPesquisa();
    procedure Pesquisa();
    procedure Imprime();
  public
    { Public declarations }
  end;

  var
  FmAgendaImp: TFmAgendaImp;

implementation

uses UnMyObjects, Module, dmkDAC_PF, UnDmkProcFunc, ModuleGeral, CreateAgenda,
MyDBCheck, UMySQLModule, UnAgendaGerApp, UnAgendaGerAll, MyListas;

{$R *.DFM}

procedure TFmAgendaImp.BtImprimeClick(Sender: TObject);
begin
  Imprime();
end;

procedure TFmAgendaImp.BtPesquisaClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmAgendaImp.Pesquisa();
var
  Empresa, Entidade, EntiContat, Status, Servico, FatoGeradr, Item, Terceiro,
  EntContrat, EntPagante: Integer;
  bSC, bCO, bIN: Boolean;
  sSC, sCo, sIN: String;
  OrcamTotal_Max, OrcamTotal_Min, PragasCabAlv_A, PragasCabAlv_Z, PragasAlv,
  SQLPragas, SQLCabAlv, SQLAlv, SQLAge, Agentes, DataExecucao, ExeIni, ExeFim: String;
  //
  SQL_Periodo, SQL_QuestaoExe, TodaSQL, SQL_Agente: String;
begin
  Empresa := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  Entidade := EdEntidade.ValueVariant;
  Terceiro := EdTerceiro.ValueVariant;
  EntContrat := EdContratante.ValueVariant;
  EntPagante := EdPagante.ValueVariant;
(*
  if MyObjects.FIC((Entidade = 0) and (Terceiro = 0), nil,
    'Informe o participante ou o terceiro ou os dois!') then
    Exit;
*)
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Recriando tabelas tempor�rias');
(*
  FAgendaEmp :=
    UnCreateGeral.RecriaTempTableNovo(ntrtt_AgendaEnt, DModG.MyPID_DB, False);
*)
  FAgendaImp :=
    CriarAgenda.RecriaTempTableNovo(ntrtt_AgendaImp, DModG.QrUpdPID1, False);

  SQL_Periodo := dmkPF.SQL_Periodo(
  'AND eve.Inicio ', TPInicioIni.Date, TPInicioFim.Date, True, True);



  // Agenda Avulsa
  Item := 0;
  if CGOperacao.Checked[Item] then
  begin
    if MyObjects.FIC((EntContrat <> 0) or (EntPagante <> 0), nil,
    '"' + CGOperacao.Items[0] + '" n�o possui "' + LaContratante.Caption +
    '" ou "' + LaPagante.Caption + '"!') then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando ' +
        CGOperacao.Items[Item]);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO _agendaimp_ ',
    'SELECT ent.Controle CtrlParti, eve.Codigo, ',
    'eve.QuestaoTyp, eve.QuestaoCod, eve.QuestaoExe, ',
    'eve.Inicio, eve.Termino, eve.EmprEnti, ',
    'eve.Terceiro, eve.Local, eve.Cor, ',
    'eve.Cption, eve.Nome, eve.Notas, ',
    'eve.FatoGeradr, fge.Nome NO_FatoGeradr, ',
    'qex.Nome NO_QuestaoExe, ',
    'stc.Nome NO_Lugar, ',
    '1 Ativo ',
    'FROM ' + TMeuDB + '.agendaeve eve ',
    'LEFT JOIN ' + TMeuDB + '.agendaent ent ON ent.Codigo=eve.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.agenfgcad fge ON fge.Codigo=eve.FatoGeradr',
    'LEFT JOIN ' + TMeuDB + '.agenexcad qex ON qex.Codigo=eve.QuestaoExe',
    'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=eve.Local',
    'WHERE eve.QuestaoTyp=1 ',
    // 2013-07-28 - ERRO!!
    //'AND ent.Entidade=' + Geral.FF0(Entidade),
    Geral.ATS_if(Entidade <> 0, ['AND ent.Entidade=' + Geral.FF0(Entidade)]),
    Geral.ATS_if(Terceiro <> 0, ['AND eve.Terceiro=' + Geral.FF0(Terceiro)]),
    // FIM 2013-07-28
    SQL_Periodo,
    '']);

  end;
  if CO_DMKID_APP = 24 then
  begin
    SQL_Agente := Geral.ATS([
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON Cab.Codigo=eve.QuestaoCod',
      Geral.ATS_if(Entidade <> 0, [
      'LEFT JOIN ' + TMeuDB + '.osage age ON ',
      '  age.Codigo=cab.Codigo AND ',
      '  age.Agente=' + Geral.FF0(Entidade)])]);
  end else
  begin
    SQL_Agente := '';
    Geral.MB_Erro('Aplicativo sem Link de Agente <> OS definido!');
  end;
  // Agenda O.S.
  Item := 1;
  if CGOperacao.Checked[Item] then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando ' +
        CGOperacao.Items[Item]);
    case CGQuestaoExe_01.Value of
      1: SQL_QuestaoExe := 'AND eve.QuestaoExe=1';
      2: SQL_QuestaoExe := 'AND eve.QuestaoExe=2';
      3: SQL_QuestaoExe := 'AND eve.QuestaoExe IN (1,2)';
      4: SQL_QuestaoExe := 'AND eve.QuestaoExe=3';
      5: SQL_QuestaoExe := 'AND eve.QuestaoExe IN (1,3)';
      6: SQL_QuestaoExe := 'AND eve.QuestaoExe IN (2,3)';
      //0,7: SQL_QuestaoExe := 'AND eve.QuestaoExe IN (1,2,3)';
      else SQL_QuestaoExe := '';
    end;
    TodaSQL := Geral.ATS([
    'INSERT INTO _agendaimp_ ',
    'SELECT eve.Codigo, ',
    Geral.ATS_if(Entidade <> 0, ['age.Controle CtrlParti, ']),
    Geral.ATS_if(Entidade =  0, ['0 CtrlParti, ']),
    'eve.QuestaoTyp, eve.QuestaoCod, eve.QuestaoExe, ',
    'eve.Inicio, eve.Termino, eve.EmprEnti, ',
    'eve.Terceiro, eve.Local, eve.Cor, ',
    'eve.Cption, eve.Nome, eve.Notas, ',
    'eve.FatoGeradr, fge.Nome NO_FatoGeradr, ',
    'ELT(eve.QuestaoExe + 1, ' +
    AgendaGerAll.AgendaEveQuestaoExe_ArrayDeAspasDuplas() + ') NO_QuestaoExe, ',
    'stc.Nome NO_Lugar, ',
    '1 Ativo ',
    'FROM ' + TMeuDB + '.agendaeve eve ',
    //
    SQL_Agente,
    //
    'LEFT JOIN ' + TMeuDB + '.fatogeradr fge ON fge.Codigo=eve.FatoGeradr',
    'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Codigo=eve.Local',
    'WHERE eve.QuestaoTyp=2',
    SQL_QuestaoExe,
    SQL_Periodo,
    Geral.ATS_if(Entidade <> 0, ['AND age.Agente=' + Geral.FF0(Entidade)]),
    Geral.ATS_if(Terceiro <> 0, ['AND eve.Terceiro=' + Geral.FF0(Terceiro)]),
    Geral.ATS_if(EntContrat <> 0, ['AND EntContrat=' + Geral.FF0(EntContrat)]),
    Geral.ATS_if(EntPagante <> 0, ['AND EntPagante=' + Geral.FF0(EntPagante)]),
    '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [TodaSQL, '']);
  end;
  //
  // ...
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, DModG.MyPID_DB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'agi.* ',
  'FROM ' + FAgendaImp + ' agi ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=agi.Terceiro ',
  // N�o usa, e est� multiplicando registros!
  //'LEFT JOIN ' + TMeuDB + '.siaptercad stc ON stc.Cliente=agi.Terceiro ',
  'ORDER BY agi.Inicio ',
  '']);
  //
  //MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmAgendaImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAgendaImp.CGOperacaoClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.EdContratanteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.EdEntidadeChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.EdPaganteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.FechaPesquisa();
begin
  QrPesq.Close;
end;

procedure TFmAgendaImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAgendaImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  TPInicioIni.Date := Date;
  TPInicioFim.Date := Date + 7;
  //
  CGQuestaoExe_01.SetMaxValue();
  MyObjects.ConfiguraCheckGroup(CGOperacao, sListaAgendaQuestao, 3, High(Integer), False);
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTerceiro, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContratante, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPagante, Dmod.MyDB);
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmAgendaImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgendaImp.frxAGE_GEREN_004_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NO_EMPRESA' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VARF_PARTICIPANTE' then
    Value := CBEntidade.Text
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp(TPInicioIni.Date,
    TPInicioFim.Date, 0, 0, True, True, False, False, '', '')
  else
  if VarName = 'VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_TITULO' then
    Value := EdVarf_Titulo.Text
  else
  if VarName = 'VARF_DESCRI_ATIVI' then
  begin
    case TAgendaQuestao(QrPesqQuestaoTyp.Value) of
      qagIndefinido : Value := '';
      qagAvulso     : Value := QrPesqNome.Value;
      qagOSBgstrl   : Value := AgendaGerApp.DadosAtividadeApp(QrPesqQuestaoCod.Value);
      else            Value := '';
    end;
  end
  else
end;

procedure TFmAgendaImp.Imprime();
begin
  MyObjects.frxDefineDataSets(frxAGE_GEREN_004_01, [
  DModG.frxDsDono,
  frxDsPesq
  ]);
  //
  MyObjects.frxMostra(frxAGE_GEREN_004_01, 'Compromissos Agendados');
  //
end;

procedure TFmAgendaImp.QrPesqAfterOpen(DataSet: TDataSet);
const
  Txt = 'Registros localizados na pesquisa: ';
begin
  BtImprime.Enabled := QrPesq.RecordCount > 0;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt + IntToStr(QrPesq.RecordCount));
end;

procedure TFmAgendaImp.QrPesqBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmAgendaImp.CGQuestaoExe_01Click(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.TPInicioFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.TPInicioFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.TPInicioIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmAgendaImp.TPInicioIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
