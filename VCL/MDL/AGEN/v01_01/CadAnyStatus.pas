unit CadAnyStatus;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, dmkRadioGroup,
  UnDmkListas, UnDmkEnums, UnProjGroup_Consts;

type
  TFmCadAnyStatus = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    TbCad: TmySQLTable;
    DsCad: TDataSource;
    TbCadCodigo: TIntegerField;
    TbCadNome: TWideStringField;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGridZTO;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    LbItensMD: TListBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    TbCadOrdem: TIntegerField;
    TbCadAgeCorIni: TIntegerField;
    TbCadAgeCorFim: TIntegerField;
    TbCadAgeCorDir: TSmallintField;
    TbCadAgeCorFon: TIntegerField;
    TbCadAgeCorHin: TIntegerField;
    GroupBox14: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CBAgeCorIni: TColorBox;
    CBAgeCorFim: TColorBox;
    CBAgeCorFon: TColorBox;
    CBAgeCorHin: TColorBox;
    RGAgeCorDir: TdmkRadioGroup;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    TbCadTXT_AgeCorDir: TWideStringField;
    RGExecucao: TdmkRadioGroup;
    TbCadExecucao: TSmallintField;
    TbCadNO_EXECUCAO: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbCadBeforePost(DataSet: TDataSet);
    procedure TbCadDeleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbCadAfterPost(DataSet: TDataSet);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbCadAfterInsert(DataSet: TDataSet);
    procedure TbCadNewRecord(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure TbCadCalcFields(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    FPermiteExcluir: Boolean;
    FNovoCodigo: TNovoCodigo;
    FReservados: Variant;
    FSoReserva: Boolean;
    FFldID: String;
    FFldNome: String;
    FDBGInsert: Boolean;
    FModoInclusao: TdmkModoInclusao;
    //
    procedure ReabrePesquisa();
  end;

  var
  FmCadAnyStatus: TFmCadAnyStatus;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista(*, UnAgendaGerAll*);

{$R *.DFM}

procedure TFmCadAnyStatus.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, TmySQLQuery(TbCad), [PnDados],
  [PnEdita], EdNome, ImgTipo, TbCad.TableName);
  //
  CBAgeCorIni.Selected := TbCadAgeCorIni.Value;
  CBAgeCorFim.Selected := TbCadAgeCorFim.Value;
  CBAgeCorFon.Selected := TbCadAgeCorFon.Value;
  CBAgeCorHin.Selected := TbCadAgeCorHin.Value;
end;

procedure TFmCadAnyStatus.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if UnCfgCadLista.PoeTbEmEdicao(TbCad, ImgTipo.SQLType) then
  begin
    TbCadNome.Value := EdNome.Text;
    TbCadOrdem.Value := EdOrdem.ValueVariant;
    //
    TbCadAgeCorIni.Value := CBAgeCorIni.Selected;
    TbCadAgeCorFim.Value := CBAgeCorFim.Selected;
    TbCadAgeCorFon.Value := CBAgeCorFon.Selected;
    TbCadAgeCorHin.Value := CBAgeCorHin.Selected;
    //
    TbCadAgeCorDir.Value := RGAgeCorDir.ItemIndex;
    //
    TbCadExecucao.Value := RGExecucao.ItemIndex;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    TbCad.Refresh;
  end;
end;

procedure TFmCadAnyStatus.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmCadAnyStatus.BtIncluiClick(Sender: TObject);
begin
  UnCfgCadLista.IncluiNovoRegistro(FModoInclusao, Self, PnEdita, [PnDados],
  [PnEdita], EdNome, ImgTipo, FReservados, TbCad, FFldID);
  EdOrdem.ValueVariant := 999999999;
end;

procedure TFmCadAnyStatus.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCadAnyStatus.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt: String;
  Cor, CorBG: TColor;
begin
  if Column.FieldName = 'AgeCorIni' then
  begin
    Txt   := '';
    Cor   := TbCadAgeCorIni.Value;
    CorBG := TbCadAgeCorIni.Value;
  end else
  if Column.FieldName = 'AgeCorFim' then
  begin
    Txt   := '';
    Cor   := TbCadAgeCorFim.Value;
    CorBG := TbCadAgeCorFim.Value;
  end else
  if Column.FieldName = 'AgeCorFon' then
  begin
    Txt   := '';
    Cor   := TbCadAgeCorFon.Value;
    CorBG := TbCadAgeCorFon.Value;
  end else
  if Column.FieldName = 'AgeCorHin' then
  begin
    Txt   := '';
    Cor   := TbCadAgeCorHin.Value;
    CorBG := TbCadAgeCorHin.Value;
  end else
  if Column.FieldName = 'Nome' then
  begin
    Txt   := Column.Field.DisplayText;
    Cor   := TbCadAgeCorFon.Value;
    CorBG := TbCadAgeCorIni.Value;
  end else
    Exit;
  //
  MyObjects.DesenhaTextoEmDBGrid(
    TDbGrid(DBGrid1), Rect, Cor, CorBG, Column.Alignment, Txt);
end;

procedure TFmCadAnyStatus.DBGrid2DblClick(Sender: TObject);
begin
  UnCfgCadLista.LocalizaPesquisadoInt1(TbCad, QrPesq, FFldID, True);
end;

procedure TFmCadAnyStatus.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCadAnyStatus.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
  BtInclui.Visible := FReservados <> Null;
end;

procedure TFmCadAnyStatus.FormClose(Sender: TObject; var Action: TCloseAction);
begin
{20200920
  AgendaGerAll.PoeEmMemoryCoresStatus();
}
end;

procedure TFmCadAnyStatus.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGExecucao, sListaStatusOSExec, 4, 0);
  PnDados.Align := alClient;
  FNovoCodigo := ncGerlSeq1;
  FReservados := 0;
  FSoReserva := False;
  FPermiteExcluir := False;
  FModoInclusao := dmkmiUserDefine;
  FFldID := CO_CODIGO;
  FFldNome := CO_NOME;
  //
end;

procedure TFmCadAnyStatus.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadAnyStatus.FormShow(Sender: TObject);
begin
  TbCad. Open;
end;

procedure TFmCadAnyStatus.LbItensMDDblClick(Sender: TObject);
begin
  UnCfgCadLista.AddVariavelEmTexto(LbItensMD, TbCad, FFldNome, DBGrid1);
end;

procedure TFmCadAnyStatus.ReabrePesquisa();
begin
  UnCfgCadLista.ReabrePesquisa(
    QrPesq, TbCad, EdPesq, TBTam, FFldID, FFldNome);
end;

procedure TFmCadAnyStatus.TbCadAfterInsert(DataSet: TDataSet);
begin
 if FDBGInsert = false then
   TbCad.Cancel;
end;

procedure TFmCadAnyStatus.TbCadAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TbCadCodigo.Value;
end;

procedure TFmCadAnyStatus.TbCadBeforePost(DataSet: TDataSet);
begin
  UnCfgCadLista.TbBeforePost(TbCad, FNovoCodigo, FFldID, FReservados);
end;

procedure TFmCadAnyStatus.TbCadCalcFields(DataSet: TDataSet);
begin
  TbCadTXT_AgeCorDir.Value :=
    sPlannerGradientDirection[TbCadAgeCorDir.Value];
  TbCadNO_EXECUCAO.Value :=
    sListaStatusOSExec[TbCadExecucao.Value];
end;

procedure TFmCadAnyStatus.TbCadDeleting(Sender: TObject; var Allow: Boolean);
begin
  if FPermiteExcluir then
  begin
   Allow := Geral.MB_Pergunta(
   'Confirma a exclus�o do registro selecionado?') = ID_YES;
  end
  else
    Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmCadAnyStatus.TbCadNewRecord(DataSet: TDataSet);
begin
  if FDBGInsert = False then
    TbCad.Cancel;
end;

procedure TFmCadAnyStatus.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
