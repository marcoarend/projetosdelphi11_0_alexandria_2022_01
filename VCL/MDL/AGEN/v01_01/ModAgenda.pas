unit ModAgenda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Db, mySQLDbTables, comctrls, frxClass, frxDBSet, stdctrls, dmkGeral,
  dmkLabel, dmkEdit, Variants, dmkEditCB, dmkDBLookupComboBox, Menus, UnMsgInt,
  UnInternalConsts, UnInternalConsts2, UnDmkProcFunc, dmkDBGrid,
  dmkImage, UnDmkEnums;

type
  TDmModAgenda = class(TDataModule)
    QrLocEve: TmySQLQuery;
    QrLocEveCodigo: TIntegerField;
    QrLocEveQuestaoTyp: TIntegerField;
    QrLocEveQuestaoCod: TIntegerField;
    QrLocEveQuestaoExe: TIntegerField;
    QrEOSs: TmySQLQuery;
    QrEOSsAgeCorIni: TIntegerField;
    QrEOSsAgeCorFim: TIntegerField;
    QrEOSsAgeCorDir: TSmallintField;
    QrEOSsAgeCorFon: TIntegerField;
    QrEOSsAgeCorHin: TIntegerField;
    QrEOSsCodigo: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
    FListaEstatusOSs, FListaEstatusAvul: TArrayListaEstatus;
    //
    function  NaoVaiAgendar(Inicio, Termino: TDateTime): Boolean;
    function  DesistePorqueNaoVaiAgendar(Inicio, Termino: TDateTime;
              Txt: String): Boolean;
    procedure AtualizaCompromissoNaAgenda(QuestaoTyp: TAgendaQuestao;
              QuestaoExe: TAgendaAfazer; QuestaoCod: Integer; Inicio, Termino:
              TDateTime; EmprEnti, Terceiro, Local, FatoGeradr: Integer;Nome,
              Notas: String; Cor: Integer; Cption: Byte; Status: Integer);
    function  ObtemDeAgendaEve_QuestaoCodDeCodigo(Codigo: Integer): Integer;
    function  ObtemDeAgendaEve_Codigo(QuestaoTyp: TAgendaQuestao;
              QuestaoExe: TAgendaAfazer; QuestaoCod: Integer): Integer;
    procedure AtualizaStatusTodosEventosMesmoTipoCodigo(Status: Integer;
              QuestaoTyp: TAgendaQuestao; QuestaoCod: Integer);
  end;

var
  DmModAgenda: TDmModAgenda;

implementation

uses Module, DmkDAC_PF, UMySQLModule;

{$R *.dfm}

{ TDmModAgenda }

procedure TDmModAgenda.AtualizaCompromissoNaAgenda(QuestaoTyp: TAgendaQuestao;
  QuestaoExe: TAgendaAfazer; QuestaoCod: Integer; Inicio, Termino: TDateTime;
  EmprEnti, Terceiro, Local, FatoGeradr: Integer; Nome, Notas: String;
  Cor: Integer; Cption: Byte; Status: Integer);
var
  SQLType: TSQLType;
  Codigo: Integer;
begin
  Codigo := ObtemDeAgendaEve_Codigo(QuestaoTyp, QuestaoExe, QuestaoCod);
  if (NaoVaiAgendar(Inicio, Termino) = False) or (Codigo <> 0) then
  begin
    {
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocEve, Dmod.MyDB, [
    'SELECT Codigo, QuestaoTyp, QuestaoCod, QuestaoExe ',
    'FROM agendaeve ',
    'WHERE QuestaoTyp=' + Geral.FF0(Integer(QuestaoTyp)),
    'AND QuestaoCod=' + Geral.FF0(QuestaoCod),
    'AND QuestaoExe=' + Geral.FF0(Integer(QuestaoExe)),
    '']);
    if QrLocEveCodigo.Value <> 0 then
    }
    if Codigo <> 0 then
      SQLType := stUpd
    else
    begin
      SQLType := stIns;
      Codigo := UMyMod.BPGS1I32('agendaeve', 'Codigo', '', '', tsPos, SQLType, 0);
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'agendaeve', False, [
    'QuestaoTyp', 'QuestaoCod', 'QuestaoExe',
    'Inicio', 'Termino', 'EmprEnti',
    'Terceiro', 'Local', 'Cor',
    'Cption', 'Nome', 'Notas',
    'FatoGeradr', 'Status'], [
    'Codigo'], [
    QuestaoTyp, QuestaoCod, QuestaoExe,
    Geral.FDT(Inicio, 109), Geral.FDT(Termino, 109), EmprEnti,
    Terceiro, Local, Cor,
    Cption, Nome, Notas,
    FatoGeradr, Status], [
    Codigo], True) then
    begin
      AtualizaStatusTodosEventosMesmoTipoCodigo(Status, QuestaoTyp, QuestaoCod);
      {
      //////////////////////////////////////////////////////////////////////////
      // Igualizar o Status de todos eventos do mesmo tipo e c�digo
      // Obrigatorio para OS no Bgstrl >  qagOSBgstrl
      //////////////////////////////////////////////////////////////////////////
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaeve', False, [
      'Status'], ['QuestaoTyp', 'QuestaoCod'], [
      Status], [QuestaoTyp, QuestaoCod], True);
      }
    end;
  end;
end;

procedure TDmModAgenda.AtualizaStatusTodosEventosMesmoTipoCodigo(Status: Integer;
  QuestaoTyp: TAgendaQuestao; QuestaoCod: Integer);
begin
  //////////////////////////////////////////////////////////////////////////
  // Igualizar o Status de todos eventos do mesmo tipo e c�digo
  // Obrigatorio para OS no Bgstrl >  qagOSBgstrl
  //////////////////////////////////////////////////////////////////////////
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'agendaeve', False, [
  'Status'], ['QuestaoTyp', 'QuestaoCod'], [
  Status], [QuestaoTyp, QuestaoCod], True);
end;

function TDmModAgenda.DesistePorqueNaoVaiAgendar(Inicio, Termino: TDateTime;
Txt: String): Boolean;
begin
  if NaoVaiAgendar(Inicio, Termino) then
  begin
    Result := Geral.MB_Pergunta('O item "' + Txt +
    '" n�o ser� agendado por falta ou equ�voco de informa��es!' + sLineBreak +
    'Deseja continuar assim mesmo?') <> ID_YES;
  end else
    Result := False;
end;

function TDmModAgenda.NaoVaiAgendar(Inicio, Termino: TDateTime): Boolean;
begin
  Result := not((Inicio > 2) and (Termino > Inicio));
end;

function TDmModAgenda.ObtemDeAgendaEve_QuestaoCodDeCodigo(
  Codigo: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocEve, Dmod.MyDB, [
  'SELECT Codigo, QuestaoTyp, QuestaoCod, QuestaoExe ',
  'FROM agendaeve ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  Result := QrLocEveQuestaoCod.Value;
end;

function TDmModAgenda.ObtemDeAgendaEve_Codigo(QuestaoTyp: TAgendaQuestao;
  QuestaoExe: TAgendaAfazer; QuestaoCod: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocEve, Dmod.MyDB, [
  'SELECT Codigo, QuestaoTyp, QuestaoCod, QuestaoExe ',
  'FROM agendaeve ',
  'WHERE QuestaoTyp=' + Geral.FF0(Integer(QuestaoTyp)),
  'AND QuestaoCod=' + Geral.FF0(QuestaoCod),
  'AND QuestaoExe=' + Geral.FF0(Integer(QuestaoExe)),
  '']);
  //
  Result := QrLocEveCodigo.Value;
end;

end.
