unit UnAgendaGerAll;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  ComCtrls, (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral,
  Variants,
  //
  //AdvToolBar,
  //
  mySQLDbTables,
  //Planner,
  UnDmkEnums;

type
  TUnAgendaGerAll = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AgendaEveQuestaoExe_ValueToText(ID, QuestaoExe: Integer): String;
    function  AgendaEveQuestaoExe_ArrayDeAspasDuplas(): String;
    function  AgendaEveQuestaoExe_ValueToField(const QuestaoExe: Integer;
              var CampoIni, CampoFim: String): String;
    procedure MostraFormItemAgenda(FormOwner: TForm; (*Planner: TPlanner;*)
              (*Item: TPlannerItem;*) QuestaoTyp, QuestaoCod, QuestaoExe: Integer);
    function  AtualizaHorarioCompromisso(Inicio, Termino: String; QuestaoTyp,
              QuestaoCod, QuestaoExe, Agente, ID_OSAge: Integer; MudouAgente:
              Boolean): Boolean;
    function  EntidadeNoAvulso(Entidade, QuestaoCod: Integer): Integer;
    function  EntidadeEstaNoEvento(Entidade, AgendaEve, QuestaoTyp,
              QuestaoCod, QuestaoExe: Integer): Integer;
    function  ItemBandToItemTime(Banda, Unidades: Integer; Data: TDateTime;
              Position: Integer): TDateTime;
    function  MontagemTextoCompromissoApp(QuestaoCod, QuestaoExe,
              Terceiro: Integer; NO_ENT: String; FatoGeradr: Integer;
              NO_FATOGERADR: String): String;
    procedure MostraFormAgenGrCab(Codigo: Integer);
    procedure MostraFormAgendaEve(QuestaoCod, Entidade: Integer; AgendaNovo:
              Boolean; DataInicio: TDateTime; HoraInicio: TTime;
              DataTermino: TDateTime; HoraTermino: TTime;
              Participantes: array of Integer);
    procedure MostraFormAgendaGer(PageControl1: TPageControl
              (*; AdvToolBarPager1: TAdvToolBarPager*));
    procedure MostraFormAgenExCad();
    procedure MostraFormAgenFgCad();
    procedure MostraFormAgendaStatus();
    function  MQLDeMemoryCoresStatusOS(const Codigo: Integer; var AgeCorIni,
              AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
    function  MQLDeMemoryCoresStatusAvul(const Codigo: Integer; var AgeCorIni,
              AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
    function  MQLDeMemoryCoresStatusXXX(const Lista: TArrayListaEstatus;
              const Codigo: Integer; var AgeCorIni, AgeCorFim, AgeCorDir,
              AgeCorFon, AgeCorHin: Integer): Boolean;
    procedure PoeEmMemoryCoresStatus();
    procedure PoeEmMemoryCoresStatusOS();
    procedure PoeEmMemoryCoresStatusAvul();
    procedure PoeEmMemoryCoresStatusXXX(var Lista: TArrayListaEstatus; Tabela: String);
    //
  end;

//const

var
  AgendaGerAll: TUnAgendaGerAll;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnAgendaGerApp, ModAgenda,
  MyDBCheck, AgenGrCab, AgendaEve, AgendaGer, CfgCadLista, CadAnyStatus;

{ TUnAgendaGerAll }

const
  FMultinstancia = True;

function TUnAgendaGerAll.AgendaEveQuestaoExe_ArrayDeAspasDuplas(): String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to  Integer(aagDesconhecido) do
    Result := Result + ', "' + AgendaEveQuestaoExe_ValueToText(0, I) + '"';
  Result := Copy(Result, 3);
end;

function TUnAgendaGerAll.AgendaEveQuestaoExe_ValueToField(const QuestaoExe:
Integer; var CampoIni, CampoFim: String): String;
var
 AgendaAfazer: TAgendaAfazer;
 // = (aagIndefinido, aagVistoria, aagExecucao);
begin
  CampoIni := '???';
  CampoFim := '???';
  //
  AgendaAFAzer := TAgendaAfazer(QuestaoExe);
  case AgendaAFazer of
    //(*0*)aagIndefinido : Result := 'Indefinido';
    (*1*)aagVistoria   : begin CampoIni := 'DtaVisPrv'; CampoFim := 'FimVisPrv'; end;
    (*2*)aagExecucao   : begin CampoIni := 'DtaExePrv'; CampoFim := 'FimExePrv'; end;
    //else                 Result := 'Desconhecido';
  end;
end;

function TUnAgendaGerAll.AgendaEveQuestaoExe_ValueToText(
  ID, QuestaoExe: Integer): String;
var
 AgendaAfazer: TAgendaAfazer;
 // = (aagIndefinido, aagVistoria, aagExecucao);
begin
  AgendaAFAzer := TAgendaAfazer(QuestaoExe);
  case AgendaAFazer of
    (*0*)aagIndefinido    : Result := 'Indefinido';
    (*1*)aagVistoria      : Result := 'Vistoria';
    (*2*)aagExecucao      : Result := 'Execu��o';
    (*3*)aagEscalonamento : Result := 'Escalonamento';
    else                    Result := 'Desconhecido';
  end;
  if ID <> 0 then
  begin
    case AgendaAFazer of
      (*0*)aagIndefinido    : ; //Result := 'Indefinido';
      (*1*)aagVistoria      : Result := 'OS ' + Geral.FF0(ID) + ' ' + Result;
      (*2*)aagExecucao      : Result := 'OS ' + Geral.FF0(ID) + ' ' + Result;
      (*3*)aagEscalonamento : Result := 'Ctrl ' + Geral.FF0(ID) + ' ' + Result;
      else                    Result := '?? ' + Geral.FF0(ID) + ' ' + Result;
    end;
  end;
end;

function  TUnAgendaGerAll.AtualizaHorarioCompromisso(Inicio, Termino: String;
  QuestaoTyp, QuestaoCod, QuestaoExe, Agente, ID_OSAge: Integer; MudouAgente:
  Boolean): Boolean;
var
  CampoIni, CampoFim: String;
  Codigo, Controle: Integer;
begin
  Result := False;
  case TAgendaQuestao(QuestaoTyp) of
    //(*0*)qagIndefinido : ; // nada
    //(*1*)qagAvulso     : ;  //nada?;
    (*2*)qagOSBgstrl   :
    begin
      Codigo  := QuestaoCod;
      AgendaEveQuestaoExe_ValueToField(QuestaoExe, CampoIni, CampoFim);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
      CampoIni, CampoFim], ['Codigo'], [
      Inicio, Termino], [Codigo], True);
      //
      if MudouAgente then
      begin
        if ID_OSAGe <> 0 then
        begin
          Controle := ID_OSAGe;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osage', False, [
          'Agente'], ['Controle'], [
          Agente], [Controle], True);
        end else Geral.MB_ERRO('ID do Agente n�o definido em "AtualizaHorarioCompromisso()"');
      end;
      //
      Result := True;
    end;
    else Geral.MB_Erro(
    '"QuestaoTyp" n�o definido na procedure "AtualizaHorarioCompromisso()"');
  end;
end;

function TUnAgendaGerAll.EntidadeEstaNoEvento(Entidade, AgendaEve, QuestaoTyp, QuestaoCod,
  QuestaoExe: Integer): Integer;
begin
  Result := 0;
  case TAgendaQuestao(QuestaoTyp) of
    (*1*)qagAvulso: Result := EntidadeNoAvulso(Entidade, AgendaEve);
    (*2*)qagOSBgstrl: Result := AgendaGerApp.IDEntidadePorApp(Entidade, QuestaoCod);
    else Geral.MB_Erro(
      '"QuestaoTyp" n�o implementado no procedimento "EntidadeEstaNoEvento()"');
  end;
end;

function TUnAgendaGerAll.EntidadeNoAvulso(Entidade,
  QuestaoCod: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM agendaent ',
  'WHERE Codigo=' + Geral.FF0(QuestaoCod),
  'AND Entidade=' + Geral.FF0(Entidade),
  '']);
  //
  Result := Qry.FieldByName('Controle').AsInteger;
end;

function TUnAgendaGerAll.ItemBandToItemTime(Banda, Unidades: Integer;
  Data: TDateTime; Position: Integer): TDateTime;
begin
  Result := Int(Data) + Position + (Banda * Unidades / (24 * 60));
end;

function TUnAgendaGerAll.MontagemTextoCompromissoApp(QuestaoCod, QuestaoExe,
Terceiro: Integer; NO_ENT: String; FatoGeradr: Integer; NO_FATOGERADR:
String): String;
var
  TxtOS, TxtCL: String;
begin
  if QuestaoExe <> 0 then
    //TxtOS := 'OS ' + Geral.FF0(QuestaoCod) + ' ' +
    TxtOS := AgendaEveQuestaoExe_ValueToText(QuestaoCod, QuestaoExe)
  else
    TxtOS := '';
  //
  if Terceiro <> 0 then
    TxtCL := 'CL' + Geral.FF0(Terceiro) + '-' + NO_ENT
  else
    TxtCL := '';
  //
  if VAR_ORDEM_TXT_POPUP_AGENDA = 0 then
    Result := Trim(TxtOS + ' ' + TxtCL)
  else
    Result := Trim(TxtCL + ' ' + TxtOS);
  //
  if (FatoGeradr <> 0) and (NO_FATOGERADR <> '') then
    Result := Result + ' FG' + Geral.FF0(FatoGeradr) + '-' + NO_FATOGERADR;
end;

procedure TUnAgendaGerAll.MostraFormAgendaEve(QuestaoCod, Entidade: Integer;
  AgendaNovo: Boolean; DataInicio: TDateTime; HoraInicio: TTime;
  DataTermino: TDateTime; HoraTermino: TTime; Participantes: array of Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmAgendaEve, FmAgendaEve, afmoNegarComAviso) then
  begin
    if QuestaoCod <> 0 then
    begin
      FmAgendaEve.LocCod(QuestaoCod, QuestaoCod);
      FmAgendaEve.QrAgendaEnt.Locate('Entidade', Entidade, []);
    end;
    if AgendaNovo then
    begin
      FmAgendaEve.FTerceiro   := Entidade;
      FmAgendaEve.FDataInicio := DataInicio;
      FmAgendaEve.FHoraInicio := HoraInicio;
      FmAgendaEve.FDataTermino := DataTermino;
      FmAgendaEve.FHoraTermino := HoraTermino;
      //
      SetLength(FmAgendaEve.FParticipantes, Length(Participantes));
      for I := 0 to Length(Participantes) - 1 do
        FmAgendaEve.FParticipantes[I] := Participantes[I];
      FmAgendaEve.AgendaCompromissoDeOutroForm();
      //
    end;
    FmAgendaEve.ShowModal;
    FmAgendaEve.Destroy;
  end;
end;

procedure TUnAgendaGerAll.MostraFormAgendaGer(PageControl1: TPageControl;
  AdvToolBarPager1: TAdvToolBarPager);
begin
  Application.MainForm.WindowState := wsMaximized;
  MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1, False, True);
end;

procedure TUnAgendaGerAll.MostraFormAgendaStatus;
begin
  if DBCheck.CriaFm(TFmCadAnyStatus, FmCadAnyStatus, afmoNegarComAviso,
  'AGE-STATU-001 :: Status de Compromissos') then
  begin
    FmCadAnyStatus.TbCad.TableName := 'agenstats';
    FmCadAnyStatus.ShowModal;
    FmCadAnyStatus.Destroy;
  end;
end;

procedure TUnAgendaGerAll.MostraFormAgenExCad;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'AgenExCad', 60, ncGerlSeq1,
  'Atividades a serem executadas',
  [], False, Null, [], [], False);
end;

procedure TUnAgendaGerAll.MostraFormAgenFgCad;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'AgenFgCad', 60, ncGerlSeq1,
  'Fatos geradores dos compromissos',
  [], False, Null, [], [], False);
end;

procedure TUnAgendaGerAll.MostraFormAgenGrCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgenGrCab, FmAgenGrCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmAgenGrCab.LocCod(Codigo, Codigo);
    FmAgenGrCab.ShowModal;
    FmAgenGrCab.Destroy;
  end;
end;

procedure TUnAgendaGerAll.MostraFormItemAgenda(FormOwner: TForm;(* Planner: TPlanner;*)
  (*Item: TPlannerItem;*) QuestaoTyp, QuestaoCod, QuestaoExe: Integer);
var
  Form: TForm;
  TabSheet: TTabSheet;
  PageControl: TPageControl;
  I(*, P*): Integer;
  Achou: Boolean;
  OSCab: Integer;
  //
  Qry: TmySQLQuery;
begin
  AgendaGerApp.MostraFormItemAgendaApp(
    FormOwner, Item, QuestaoTyp, QuestaoCod, QuestaoExe);
{
  OSCab := 0;
  case TAgendaQuestao(QuestaoTyp) of
    (*2*)qagOSBgstrl:
    begin
      Achou := False;
      case TAgendaAfazer(QuestaoExe) of
        //(*0*)aagIndefinido    : OSCab := 0;
        (*1*)aagVistoria      : OSCab := QuestaoCod;
        (*2*)aagExecucao      : OSCab := QuestaoCod;
        (*3*)aagEscalonamento :
        begin
          Qry := TmySQLQuery.Create(Dmod);
          try
            UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT Codigo ',
            'FROM oscabxtr ',
            'WHERE Controle=' + Geral.FF0(QuestaoCod),
            '']);
            //
            OSCab := Qry.FieldByName('Codigo').AsInteger;
          finally
            Qry.Free;
          end;
        end;
        else OSCab := 0;
      end;
      //
      PageControl := TPageControl(TTabSheet(TForm(FormOwner).Owner).Owner);
      for I := 0 to PageControl.PageCount - 1 do
      begin
        Form := TForm(PageControl.Pages[I].Components[0]);
        //Geral.MB_Info(Form.Name);
        if Form is TFmOSCab2 then
        begin
          if TFmOSCab2(Form).QrOSCabCodigo.Value = OSCab then
          begin
            PageControl.ActivePage := PageControl.Pages[I];
            PageControl.ActivePageIndex := PageControl.Pages[I].PageIndex;
            Achou := PageControl.ActivePageIndex = I;
            Break;
          end;
        end;
      end;
      if not Achou then
      begin
        TabSheet := TTabSheet(FormOwner.Owner);
        Form := MyObjects.FormTDICria(TFmOSCab2, FormOwner, nil);
        OSApp_PF.ReopenLocOS_Idx(DmModOS.QrLocOS, OSCab);
        (*

        TFmOSCab2(Form).LocCod(OSCab, OSCab, dispIndefinido,
          DmModOS.QrLocOSSiapTerCad.Value, DmModOS.QrLocOSOpcao.Value, True,
          DmModOS.QrLocOSCodigo.Value);
        *)
        TFmOSCab2(Form).LocCod(
          DmModOS.QrLocOSGrupo.Value,
          DmModOS.QrLocOSGrupo.Value,
          dispIndefinido,
          DmModOS.QrLocOSSiapTerCad.Value,
          DmModOS.QrLocOSOpcao.Value,
          True,
          DmModOS.QrLocOSCodigo.Value);
        TFmOSCab2(Form).CSTabSheetChamou.Component := TabSheet;
      end;
    end;
    else Geral.MB_Erro(
      '"QuestaoTyp" n�o implementado no procedimento "MostraFormItemAgenda()"');
  end;
}
end;

function TUnAgendaGerAll.MQLDeMemoryCoresStatusAvul(const Codigo: Integer;
  var AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
begin
  MQLDeMemoryCoresStatusXXX(DmModAgenda.FListaEstatusAvul, Codigo,
    AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
end;

function TUnAgendaGerAll.MQLDeMemoryCoresStatusOS(const Codigo: Integer;
  var AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
begin
  MQLDeMemoryCoresStatusXXX(DmModAgenda.FListaEstatusOSs, Codigo,
    AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin);
end;

function TUnAgendaGerAll.MQLDeMemoryCoresStatusXXX(
  const Lista: TArrayListaEstatus; const Codigo: Integer; var AgeCorIni,
  AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin: Integer): Boolean;
var
  I: Integer;
begin
  AgeCorIni := clWhite;
  AgeCorFim := clWhite;
  AgeCorDir := 1;
  AgeCorFon := clBlack;
  AgeCorHin := clRed;
  //
  for I := 0 to High(Lista) do
  begin
    if Lista[I][0] = Codigo then
    begin
      AgeCorIni := Lista[I][1];
      AgeCorFim := Lista[I][2];
      AgeCorDir := Lista[I][3];
      AgeCorFon := Lista[I][4];
      AgeCorHin := Lista[I][5];
      //
      Result := True;
      Exit;
    end;
  end;
end;

procedure TUnAgendaGerAll.PoeEmMemoryCoresStatus;
begin
  PoeEmMemoryCoresStatusAvul();
  PoeEmMemoryCoresStatusOS();
end;

procedure TUnAgendaGerAll.PoeEmMemoryCoresStatusAvul;
begin
  PoeEmMemoryCoresStatusXXX(DmModAgenda.FListaEstatusAvul, 'agenstats');
end;

procedure TUnAgendaGerAll.PoeEmMemoryCoresStatusOS;
begin
  PoeEmMemoryCoresStatusXXX(DmModAgenda.FListaEstatusOSs, 'estatusoss');
end;

procedure TUnAgendaGerAll.PoeEmMemoryCoresStatusXXX(
  var Lista: TArrayListaEstatus; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmModAgenda.QrEOSs, Dmod.MyDB, [
  'SELECT Codigo, AgeCorIni, AgeCorFim, AgeCorDir, AgeCorFon, AgeCorHin ',
  'FROM ' + Tabela,
  '']);
  //
  SetLength(Lista, DmModAgenda.QrEOSs.RecordCount);
  DmModAgenda.QrEOSs.First;
  while not DmModAgenda.QrEOSs.Eof do
  begin
    Lista[DmModAgenda.QrEOSs.RecNo - 1][0] := DmModAgenda.QrEOSsCodigo.Value;
    Lista[DmModAgenda.QrEOSs.RecNo - 1][1] := DmModAgenda.QrEOSsAgeCorIni.Value;
    Lista[DmModAgenda.QrEOSs.RecNo - 1][2] := DmModAgenda.QrEOSsAgeCorFim.Value;
    Lista[DmModAgenda.QrEOSs.RecNo - 1][3] := DmModAgenda.QrEOSsAgeCorDir.Value;
    Lista[DmModAgenda.QrEOSs.RecNo - 1][4] := DmModAgenda.QrEOSsAgeCorFon.Value;
    Lista[DmModAgenda.QrEOSs.RecNo - 1][5] := DmModAgenda.QrEOSsAgeCorHin.Value;
    //
    DmModAgenda.QrEOSs.Next;
  end;
end;

end.
