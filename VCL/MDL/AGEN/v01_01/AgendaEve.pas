unit AgendaEve;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, Variants,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, dmkEditCB,
  dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker, Menus, Grids, DBGrids,
  DateUtils, UnDmkEnums;

type
  TFmAgendaEve = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtIts: TBitBtn;
    BtCab: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrAgendaEve: TmySQLQuery;
    DsAgendaEve: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdQuestaoCod: TdmkEdit;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrAgendaEveNO_FATOGERADR: TWideStringField;
    QrAgendaEveNO_QUESTAOEXE: TWideStringField;
    QrAgendaEveNO_EMPRESA: TWideStringField;
    QrAgendaEveNO_TERCEIRO: TWideStringField;
    QrAgendaEveCodigo: TIntegerField;
    QrAgendaEveQuestaoTyp: TIntegerField;
    QrAgendaEveQuestaoCod: TIntegerField;
    QrAgendaEveQuestaoExe: TIntegerField;
    QrAgendaEveInicio: TDateTimeField;
    QrAgendaEveTermino: TDateTimeField;
    QrAgendaEveEmprEnti: TIntegerField;
    QrAgendaEveTerceiro: TIntegerField;
    QrAgendaEveLocal: TIntegerField;
    QrAgendaEveCor: TIntegerField;
    QrAgendaEveCption: TSmallintField;
    QrAgendaEveNome: TWideStringField;
    QrAgendaEveNotas: TWideStringField;
    QrAgendaEveFatoGeradr: TIntegerField;
    QrAgenExCad: TmySQLQuery;
    DsAgenExCad: TDataSource;
    QrAgenFgCad: TmySQLQuery;
    DsAgenFgCad: TDataSource;
    DsSiapTerCad: TDataSource;
    CBEmprEnti: TdmkDBLookupComboBox;
    EdEmprEnti: TdmkEditCB;
    Label3: TLabel;
    CBTerceiro: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdTerceiro: TdmkEditCB;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNO_Enti: TWideStringField;
    Label5: TLabel;
    EdQuestaoExe: TdmkEditCB;
    CBQuestaoExe: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdFatoGeradr: TdmkEditCB;
    CBFatoGeradr: TdmkDBLookupComboBox;
    SbAgenExCad: TSpeedButton;
    SbAgenFgCad: TSpeedButton;
    QrAgenFgCadCodigo: TIntegerField;
    QrAgenFgCadNome: TWideStringField;
    QrAgenExCadCodigo: TIntegerField;
    QrAgenExCadNome: TWideStringField;
    Label52: TLabel;
    TPInicio: TdmkEditDateTimePicker;
    EdInicio: TdmkEdit;
    Label53: TLabel;
    TPTermino: TdmkEditDateTimePicker;
    EdTermino: TdmkEdit;
    Label8: TLabel;
    EdLocal: TdmkEditCB;
    CBLocal: TdmkDBLookupComboBox;
    SbLocal: TSpeedButton;
    PMLocais: TPopupMenu;
    MeTerCadEnder_Edit: TMemo;
    QrSiapTerCad: TmySQLQuery;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    QrSiapTerCadCliente: TIntegerField;
    QrSiapTerCadSLograd: TSmallintField;
    QrSiapTerCadSRua: TWideStringField;
    QrSiapTerCadSNumero: TIntegerField;
    QrSiapTerCadSCompl: TWideStringField;
    QrSiapTerCadSBairro: TWideStringField;
    QrSiapTerCadSCidade: TWideStringField;
    QrSiapTerCadSUF: TWideStringField;
    QrSiapTerCadSCEP: TIntegerField;
    QrSiapTerCadSPais: TWideStringField;
    QrSiapTerCadSEndeRef: TWideStringField;
    QrSiapTerCadSCodMunici: TIntegerField;
    QrSiapTerCadSCodiPais: TIntegerField;
    QrSiapTerCadM2Constru: TFloatField;
    QrSiapTerCadM2NaoBuild: TFloatField;
    QrSiapTerCadM2Terreno: TFloatField;
    QrSiapTerCadM2Total: TFloatField;
    QrSiapTerCadNOMELOGRAD: TWideStringField;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdQuestaoCod: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    QrAgendaEveNO_LUGAR: TWideStringField;
    MeTerCadEnder_Dado: TMemo;
    QrSTC: TmySQLQuery;
    QrSTCNOMELOGRAD: TWideStringField;
    QrSTCCodigo: TIntegerField;
    QrSTCNome: TWideStringField;
    QrSTCCliente: TIntegerField;
    QrSTCSLograd: TSmallintField;
    QrSTCSRua: TWideStringField;
    QrSTCSNumero: TIntegerField;
    QrSTCSCompl: TWideStringField;
    QrSTCSBairro: TWideStringField;
    QrSTCSCidade: TWideStringField;
    QrSTCSUF: TWideStringField;
    QrSTCSCEP: TIntegerField;
    QrSTCSPais: TWideStringField;
    QrSTCSEndeRef: TWideStringField;
    QrSTCSCodMunici: TIntegerField;
    QrSTCSCodiPais: TIntegerField;
    QrSTCSTe1: TWideStringField;
    QrSTCM2Constru: TFloatField;
    QrSTCM2NaoBuild: TFloatField;
    QrSTCM2Terreno: TFloatField;
    QrSTCM2Total: TFloatField;
    QrAgendaEnt: TmySQLQuery;
    DsAgendaEnt: TDataSource;
    DBGAgendaEnt: TDBGrid;
    PMCab: TPopupMenu;
    PMIts: TPopupMenu;
    Agendanovocompromisso1: TMenuItem;
    Alteracompromissoatual1: TMenuItem;
    Adicionaentidadeaoevento1: TMenuItem;
    Retiraentidadesdoevento1: TMenuItem;
    QrAgendaEntCodigo: TIntegerField;
    QrAgendaEntControle: TIntegerField;
    QrAgendaEntEntidade: TIntegerField;
    QrAgendaEntNO_ENT: TWideStringField;
    Timer1: TTimer;
    Excluicompromissoatual1: TMenuItem;
    Label2: TLabel;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    QrAgenStats: TmySQLQuery;
    QrAgenStatsCodigo: TIntegerField;
    QrAgenStatsNome: TWideStringField;
    DsAgenStats: TDataSource;
    QrAgendaEveStatus: TIntegerField;
    Label18: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    QrAgendaEveNO_STATUS: TWideStringField;
    RGLupPeri: TdmkRadioGroup;
    GroupBox1: TGroupBox;
    RGLupModo: TdmkRadioGroup;
    TPLupDFim: TdmkEditDateTimePicker;
    EdLupQtde: TdmkEdit;
    QrAgendaEveLupPeri: TSmallintField;
    QrAgendaEveLupModo: TSmallintField;
    QrAgendaEveLupDFim: TDateField;
    QrAgendaEveLupQtde: TIntegerField;
    DBRGLupPeri: TDBRadioGroup;
    GroupBox2: TGroupBox;
    dmkRadioGroup2: TDBRadioGroup;
    Label9: TLabel;
    Panel6: TPanel;
    DBEdit15: TDBEdit;
    Panel7: TPanel;
    DBEdit16: TDBEdit;
    Label19: TLabel;
    EdInfoPrivat: TdmkEdit;
    QrAgendaEveInfoPrivat: TWideStringField;
    DBEdInfoPrivat: TdmkDBEdit;
    Label15: TLabel;
    Label20: TLabel;
    QrAgendaEveUserCad: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAgendaEveAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAgendaEveBeforeOpen(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbAgenFgCadClick(Sender: TObject);
    procedure SbAgenExCadClick(Sender: TObject);
    procedure SbLocalClick(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdLocalChange(Sender: TObject);
    procedure QrAgendaEveBeforeClose(DataSet: TDataSet);
    procedure QrAgendaEveAfterScroll(DataSet: TDataSet);
    procedure Agendanovocompromisso1Click(Sender: TObject);
    procedure Alteracompromissoatual1Click(Sender: TObject);
    procedure Retiraentidadesdoevento1Click(Sender: TObject);
    procedure Adicionaentidadeaoevento1Click(Sender: TObject);
    procedure Excluicompromissoatual1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure TPInicioChange(Sender: TObject);
    procedure TPInicioClick(Sender: TObject);
    procedure QrAgendaEveBeforeScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenSiapTerCad(Todos: Boolean);
    procedure ReopenAgendaEnt(Controle: Integer);
    procedure IncluiEntidadeNoEvento();
    procedure IncluiParticipantesNoEvento(Codigo: Integer);
    function  DefineDataHoraEveLup_Fim(Inicio: TDateTime; LupPeri, LupModo:
              Integer; LupDFim: TDateTime; LupQtde: Integer): TDateTime;
    procedure MudaTextoPeriodicidade(RGPeri: TCustomRadioGroup; DtIni:
              TDateTime);
    procedure ConfiguraTextoPrivativo();
  public
    { Public declarations }
    FTerceiro: Integer;
    FDataInicio: TDateTime;
    FHoraInicio: TTime;
    FDataTermino: TDateTime;
    FHoraTermino: TTime;
    FParticipantes: array of Integer;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure AgendaCompromissoDeOutroForm();
  end;

var
  FmAgendaEve: TFmAgendaEve;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, Principal, (*ModOS,*) MyDBCheck, ModuleGeral,
  UnAgenda_Tabs, UnEntities, UnAgendaGerAll, UnAgendaGerApp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAgendaEve.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAgendaEve.MudaTextoPeriodicidade(RGPeri: TCustomRadioGroup; DtIni:
TDateTime);
var
  TxtDiaAno, TxtDiaSem, ArtDiaSem, TxtSemMes, TxtDiaMes: String;
  IntDiaEra, IntDiaMes, IntDiaSem, IntSemMes: Integer;
begin
  if DtIni < 2 then
  begin
    TRadioGroup(RGPeri).Items.Text := Geral.ATS([
    'Nenhuma',
    'Di�ria',
    'Semanal',
    'A cada 2 semanas',
    'Mensal dia / semana',
    'Mensal dia do m�s',
    'Anual']);
  end else
  begin
    IntDiaEra := Trunc(DtIni);
    TxtDiaAno := Geral.FDT(IntDiaEra, 12);
    IntDiaMes := DayOfTheMonth(DtIni);
    IntDiaSem := DayOfTheWeek(IntDiaEra);
    IntSemMes := ((IntDiaMes - IntDiaSem) div 7) + 1;
    if IntSemMes > 4 then
      IntSemMes := 4;
    TxtDiaSem := dmkPF.NomeDiaDaSemana(IntDiaSem, False);
    ArtDiaSem := dmkPF.NomeDiaDaSemana_Artigo(DayOfTheWeek(IntDiaEra), False);
    TxtSemMes := Geral.FF0(IntSemMes) + '� ';
    TxtDiaMes := Geral.FF0(IntDiaMes);
    //
    TRadioGroup(RGPeri).Items[CO_AGENDA_LOOP_PERI_Semanal] :=
      'Semanal, n' + ArtDiaSem + ' ' + TxtDiaSem;
    TRadioGroup(RGPeri).Items[CO_AGENDA_LOOP_PERI_ACada2Semanas] :=
      'A cada duas semanas n' + ArtDiaSem + ' ' + TxtDiaSem;
    TRadioGroup(RGPeri).Items[CO_AGENDA_LOOP_PERI_MensalDiaSemana] :=
      'Mensal, na ' + TxtSemMes + TxtDiaSem;
    TRadioGroup(RGPeri).Items[CO_AGENDA_LOOP_PERI_MensalDiaDoMes] :=
      'Mensal, no dia ' + TxtDiaMes;
    TRadioGroup(RGPeri).Items[CO_AGENDA_LOOP_PERI_Anual] :=
      'Anual, no dia ' + TxtDiaAno;
  end;
end;

procedure TFmAgendaEve.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(Alteracompromissoatual1, QrAgendaEve);
  MyObjects.HabilitaMenuItemCabDel(Excluicompromissoatual1, QrAgendaEve, QrAgendaEnt);
end;

procedure TFmAgendaEve.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAgendaEveQuestaoCod.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAgendaEve.DefParams;
begin
  VAR_GOTOTABELA := 'agendaeve';
  VAR_GOTOMYSQLTABLE := QrAgendaEve;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'QuestaoCod';
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'QuestaoTyp=' + Geral.FF0(Integer(qagAvulso));

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO,');
  VAR_SQLx.Add('stc.Nome NO_LUGAR, sta.Nome NO_STATUS, eve.*');
  VAR_SQLx.Add('FROM agendaeve eve');
  VAR_SQLx.Add('LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr');
  VAR_SQLx.Add('LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti');
  VAR_SQLx.Add('LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro');
  VAR_SQLx.Add('LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local');
  VAR_SQLx.Add('LEFT JOIN agenstats sta ON sta.Codigo=eve.Status');
  VAR_SQLx.Add('WHERE eve.QuestaoCod>0');
  VAR_SQLx.Add('AND eve.QuestaoTyp=' + Geral.FF0(Integer(qagAvulso)));
  VAR_SQLx.Add('');


  //
  VAR_SQL1.Add('AND eve.QuestaoCod=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND eve.Nome LIKE :P0');
  //
end;

procedure TFmAgendaEve.EdLocalChange(Sender: TObject);
begin
  MeTerCadEnder_Edit.Text :=
    Entities.MontaEndereco(QrSiapTerCadNOMELOGRAD.Value,
    QrSiapTerCadSRua.Value, QrSiapTerCadSNumero.Value, QrSiapTerCadSCompl.Value,
    QrSiapTerCadSBairro.Value, QrSiapTerCadSCidade.Value, QrSiapTerCadSUF.Value,
    QrSiapTerCadSPais.Value, QrSiapTerCadSCEP.Value);
end;

procedure TFmAgendaEve.EdTerceiroChange(Sender: TObject);
begin
  ReopenSiapTerCad(False);
end;

procedure TFmAgendaEve.Excluicompromissoatual1Click(Sender: TObject);
{
var
  ID_Item: Integer;
}
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do compromisso atual?',
  'agendaeve', 'Codigo', QrAgendaEveCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    LocCod(QrAgendaEveCodigo.Value, QrAgendaEveCodigo.Value);
  end;
end;

procedure TFmAgendaEve.ConfiguraTextoPrivativo();
begin
  if (QrAgendaEveUserCad.Value = VAR_USUARIO)
  or ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then
  begin
    DBEdInfoPrivat.Visible := True;
  end else
  begin
    DBEdInfoPrivat.Visible := False;
  end;
end;

procedure TFmAgendaEve.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAgendaEve.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAgendaEve.ReopenAgendaEnt(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgendaEnt, Dmod.MyDB, [
  'SELECT age.*, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM agendaent age ',
  'LEFT JOIN entidades ent ON ent.Codigo=age.Entidade ',
  'WHERE age.Codigo=' + Geral.FF0(QrAgendaEveCodigo.Value),
  '']);
  //
  QrAgendaEnt.Locate('Controle', Controle, []);
end;

procedure TFmAgendaEve.ReopenSiapTerCad(Todos: Boolean);
var
  SQL: String;
begin
  if Todos then
    SQL := 'WHERE stc.Cliente=' + Geral.FF0(EdTerceiro.ValueVariant)
  else SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapTerCad, Dmod.MyDB, [
  'SELECT llo.Nome NOMELOGRAD, stc.* ',
  'FROM siaptercad stc ',
  'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd ',
  SQL,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmAgendaEve.Retiraentidadesdoevento1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrAgendaEnt, TDBGrid(DBGAgendaEnt),
    'agendaent', ['Controle'], ['Controle'], istPergunta, '');
end;

function TFmAgendaEve.DefineDataHoraEveLup_Fim(Inicio: TDateTime; LupPeri, LupModo: Integer;
  LupDFim: TDateTime; LupQtde: Integer): TDateTime;
var
  Qtde: Integer;
begin
  Result := 0;
  case LupModo of
    CO_AGENDA_MODO_LOOP_NENHUMA  : Result := 0;
    CO_AGENDA_MODO_LOOP_ATEADATA : Result := LupDFim;
    CO_AGENDA_MODO_LOOP_X_VEZES  :
    begin
      Qtde := LupQtde - 1;
      //
      case LupPeri of
        CO_AGENDA_LOOP_PERI_Nenhuma          : Result := 0;
        CO_AGENDA_LOOP_PERI_Diaria           : Result := Inicio + LupQtde;
        CO_AGENDA_LOOP_PERI_Semanal          : Result := Inicio + (LupQtde * 7);
        //CO_AGENDA_LOOP_PERI_Decendial        : Result := Inicio + (LupQtde * 10);
        CO_AGENDA_LOOP_PERI_ACada2Semanas    : Result := Inicio + (LupQtde * 14);
        CO_AGENDA_LOOP_PERI_MensalDiaSemana  : Result := IncMonth(Inicio, LupQtde);
        CO_AGENDA_LOOP_PERI_MensalDiaDoMes   : Result := IncMonth(Inicio, LupQtde);
        CO_AGENDA_LOOP_PERI_Anual            : Result := IncMonth(Inicio, LupQtde * 12);
        else Geral.MB_Erro(
          '"Quantidade" n�o definida na function "DefineDataHoraEveLup_Fim"');
      end;
    end;
    else Geral.MB_Erro(
      '"Periodicidade" n�o definida na function "DefineDataHoraEveLup_Fim"');
  end;
end;

procedure TFmAgendaEve.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAgendaEve.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAgendaEve.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAgendaEve.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAgendaEve.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAgendaEve.TPInicioChange(Sender: TObject);
begin
  MudaTextoPeriodicidade(RGLupPeri, TPInicio.Date);
end;

procedure TFmAgendaEve.TPInicioClick(Sender: TObject);
begin
  MudaTextoPeriodicidade(RGLupPeri, TPInicio.Date);
end;

procedure TFmAgendaEve.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAgendaEve.Adicionaentidadeaoevento1Click(Sender: TObject);
begin
  IncluiEntidadeNoEvento();
end;

procedure TFmAgendaEve.AgendaCompromissoDeOutroForm();
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAgendaEve, [PnDados],
  [PnEdita], EdEmprEnti, ImgTipo, 'agendaeve');
  EdTerceiro.ValueVariant := FTerceiro;
  CBTerceiro.KeyValue     := FTerceiro;
  TPInicio.Date           := FDataInicio;
  EdInicio.ValueVariant   := FHoraInicio;
  TPTermino.Date          := FDataTermino;
  EdTermino.ValueVariant  := FHoraTermino;
  FDataInicio             := 0;
  FHoraInicio             := 0;
  FDataTermino            := 0;
  FHoraTermino            := 0;
end;

procedure TFmAgendaEve.Agendanovocompromisso1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrAgendaEve, [PnDados],
  [PnEdita], EdEmprEnti, ImgTipo, 'agendaeve');
end;

procedure TFmAgendaEve.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmAgendaEve.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAgendaEveCodigo.Value;
  VAR_CADASTRO2 := QrAgendaEveQuestaoCod.Value;
  Close;
end;

procedure TFmAgendaEve.BtConfirmaClick(Sender: TObject);
var
  Inicio, Termino, Nome, Notas, SQLQuestaoCod, LupDFim, DtHrIni, DtHrFim,
  InfoPrivat: String;
  Codigo, QuestaoTyp, QuestaoCod, QuestaoExe, EmprEnti, Terceiro, Local, Cor,
  Cption, FatoGeradr, Status, LupPeri, LupModo, LupQtde: Integer;
  Ini, Fim, LupF, LupDFim_Date: TDateTime;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Nome := EdNome.ValueVariant;
  LupDFim_Date := TPLupDFim.Date;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('agendaeve', 'Codigo', '', '',
    tsPos, SQLType, QrAgendaEveCodigo.Value);
  QuestaoTyp     := Integer(qagAvulso);
  SQLQuestaoCod  := 'QuestaoTyp=' + Geral.FF0(QuestaoTyp);
  QuestaoCod := UMyMod.BPGS1I32('agendaeve', 'QuestaoCod', SQLQuestaoCod, '',
    tsPos, SQLType, QrAgendaEveQuestaoCod.Value);
  QuestaoExe     := EdQuestaoExe.ValueVariant;
  Inicio         := Geral.FDT(TPInicio.Date, 1) + ' ' + EdInicio.Text + ':00';
  Termino        := Geral.FDT(TPTermino.Date, 1) + ' ' + EdTermino.Text + ':00';
  EmprEnti       := EdEmprEnti.ValueVariant;
  Terceiro       := EdTerceiro.ValueVariant;
  Local          := EdLocal.ValueVariant;
  Cor            := 0; // N�o usa
  Cption         := 0; // N�o usa
  Notas          := ''; // N�o usa
  FatoGeradr     := EdFatoGeradr.ValueVariant;
  Status         := EdStatus.ValueVariant;
  //
  LupPeri        := RGLupPeri.ItemIndex;
  LupModo        := RGLupModo.ItemIndex;
  LupQtde        := EdLupQtde.ValueVariant;
  LupDFim        := Geral.FDT(LupDFim_Date, 1);
  //
  Fim := Int(TPTermino.Date) + EdTermino.ValueVariant;
  Ini := Int(TPInicio.Date) + EdInicio.ValueVariant;
  if MyObjects.FIC(Ini >= Fim, nil, 'T�rmino deve ser ap�s inicio!') then
    Exit;
  if LupPeri > 0 then
  begin
    LupF := DefineDataHoraEveLup_Fim(Ini, LupPeri, LupModo, LupDFim_Date, LupQtde);
    //
    if MyObjects.FIC(LupF + 1 < Ini, nil, 'Data final de loop inv�lida!') then
      Exit;
  end;
  InfoPrivat := EdInfoPrivat.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'agendaeve', False, [
  'QuestaoTyp', 'QuestaoCod', 'QuestaoExe',
  'Inicio', 'Termino', 'EmprEnti',
  'Terceiro', 'Local', 'Cor',
  'Cption', 'Nome', 'Notas',
  'FatoGeradr', 'Status', 'LupPeri',
  'LupModo', 'LupQtde', 'LupDFim',
  'InfoPrivat'
  ], [
  'Codigo'], [
  QuestaoTyp, QuestaoCod, QuestaoExe,
  Inicio, Termino, EmprEnti,
  Terceiro, Local, Cor,
  Cption, Nome, Notas,
  FatoGeradr, Status, LupPeri,
  LupModo, LupQtde, LupDFim,
  InfoPrivat], [
  Codigo], True) then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM agendalup ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    if LupPeri > 0 then
    begin
      LupF := DefineDataHoraEveLup_Fim(Ini, LupPeri, LupModo, LupDFim_Date, LupQtde);
      //
      DtHrIni := Inicio;
      DtHrFim := Geral.FDT(LupF, 109);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'agendalup', False, [
      'DtHrIni', 'DtHrFim', 'LupPeri'], [
      'Codigo'], [
      DtHrIni, DtHrFim, LupPeri], [
      Codigo], True);
    end;
    LocCod(QuestaoCod, QuestaoCod);
    PnDados.Visible := True;
    PnEdita.Visible := False;
    ImgTipo.SQLType := stlok;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    if SQLType = stIns then
    begin
      if Length(FParticipantes) > 0 then
        IncluiParticipantesNoEvento(Codigo)
      else
        IncluiEntidadeNoEvento();
    end;
  end;
end;

procedure TFmAgendaEve.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmAgendaEve.Alteracompromissoatual1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaUsuario(QrAgendaEveUserCad.Value) then
    Exit;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrAgendaEve, [PnDados],
  [PnEdita], EdEmprEnti, ImgTipo, 'agendaeve');
end;

procedure TFmAgendaEve.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmAgendaEve.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CBEmprEnti.ListSource := DModG.DsEmpresas;
  DBGAgendaEnt.Align := alClient;
  MeTerCadEnder_Edit.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrTerceiros, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAgenExCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAgenFgCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAgenStats, Dmod.MyDB);
  //ReopenSiapTerCad(False);
end;

procedure TFmAgendaEve.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAgendaEveQuestaoCod.Value, LaRegistro.Caption);
end;

procedure TFmAgendaEve.SbAgenExCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  AgendaGerAll.MostraFormAgenExCad();
  UMyMod.SetaCodigoPesquisado(EdQuestaoExe, CBQuestaoExe, QrAgenExCad, VAR_CADASTRO);
end;

procedure TFmAgendaEve.SbAgenFgCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  AgendaGerAll.MostraFormAgenFgCad();
  UMyMod.SetaCodigoPesquisado(EdFatoGeradr, CBFatoGeradr, QrAgenFgCad, VAR_CADASTRO);
end;

procedure TFmAgendaEve.SbLocalClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  AgendaGerApp.MostraFormCunsCad(False, EdTerceiro.ValueVariant, 0);
  ReopenSiapTerCad(True);
  UMyMod.SetaCodigoPesquisado(EdLocal, CBLocal, QrSiapTerCad, VAR_CADASTRO2);
end;

procedure TFmAgendaEve.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAgendaEve.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrAgendaEveQuestaoCod.Value, LaRegistro.Caption);
end;

procedure TFmAgendaEve.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAgendaEve.QrAgendaEveAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAgendaEve.QrAgendaEveAfterScroll(DataSet: TDataSet);
begin
  ConfiguraTextoPrivativo();
  MudaTextoPeriodicidade(DBRGLupPeri, QrAgendaEveInicio.Value);
  ReopenAgendaEnt(0);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSTC, Dmod.MyDB, [
  'SELECT llo.Nome NOMELOGRAD, stc.* ',
  'FROM siaptercad stc ',
  'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd ',
  'WHERE stc.Codigo=' + Geral.FF0(QrAgendaEveLocal.Value),
  '']);
  //
  MeTerCadEnder_Dado.Text :=
    Entities.MontaEndereco(QrSTCNOMELOGRAD.Value,
    QrSTCSRua.Value, QrSTCSNumero.Value, QrSTCSCompl.Value,
    QrSTCSBairro.Value, QrSTCSCidade.Value, QrSTCSUF.Value,
    QrSTCSPais.Value, QrSTCSCEP.Value);

end;

procedure TFmAgendaEve.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAgendaEve.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAgendaEveQuestaoCod.Value,
  CuringaLoc.CriaForm('QuestaoCod', CO_NOME, 'agendaeve', Dmod.MyDB, 'AND ' + VAR_GOTOVAR1));
end;

procedure TFmAgendaEve.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgendaEve.IncluiEntidadeNoEvento();
const
  Aviso  = '...';
  Titulo = 'Sele��o de Participantes do Evento';
  Prompt = 'Informe a entidade:';
  Campo  = 'Descricao';
var
  Codigo, Controle: Integer;
  Entidade: Variant;
begin
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) ' + Campo,
  'FROM entidades ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, True);
  if Entidade <> Null then
  begin
    try
      Codigo         := QrAgendaEveCodigo.Value;
      Controle       := UMyMod.BPGS1I32('agendaent', 'Controle', '', '', tsPos, stIns, 0);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'agendaent', False, [
      'Codigo', 'Entidade'], [
      'Controle'], [
      Codigo, Entidade], [
      Controle], True) then
      begin
        ReopenAgendaEnt(Controle);
        IncluiEntidadeNoEvento();
      end;
    except;
      Geral.MB_Aviso('Poss�vel erro de duplica��o de participante!');
      raise;
    end;
  end;
end;

procedure TFmAgendaEve.IncluiParticipantesNoEvento(Codigo: Integer);
var
  I, Controle, Entidade: Integer;
begin
  try
    for I := 0 to Length(FParticipantes) - 1 do
    begin
      Entidade := FParticipantes[I];
      //
      Controle := UMyMod.BPGS1I32('agendaent', 'Controle', '', '', tsPos, stIns, 0);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'agendaent', False, [
      'Codigo', 'Entidade'], [
      'Controle'], [
      Codigo, Entidade], [
      Controle], True) then
        ReopenAgendaEnt(Controle);
    end;
  finally
    SetLength(FParticipantes, 0);
  end;
end;

procedure TFmAgendaEve.QrAgendaEveBeforeClose(DataSet: TDataSet);
begin
  QrAgendaEnt.Close;
  MeTerCadEnder_Dado.Text := '';
  MudaTextoPeriodicidade(DBRGLupPeri, 0);
end;

procedure TFmAgendaEve.QrAgendaEveBeforeOpen(DataSet: TDataSet);
begin
  QrAgendaEveCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmAgendaEve.QrAgendaEveBeforeScroll(DataSet: TDataSet);
begin
  DBEdInfoPrivat.Visible := False;
end;

end.

