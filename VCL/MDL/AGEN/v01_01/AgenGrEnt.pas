unit AgenGrEnt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmAgenGrEnt = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label1: TLabel;
    SbEntidade: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    VU_Sel_: TdmkValUsu;
    Label2: TLabel;
    SbCor: TSpeedButton;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    CBCorEnt: TColorBox;
    ColorDialog1: TColorDialog;
    EdSigla: TdmkEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure SbCorClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAgenGrEnt(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmAgenGrEnt: TFmAgenGrEnt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, UnEntities;

{$R *.DFM}

procedure TFmAgenGrEnt.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Entidade, CorEnt: Integer;
  Sigla: String;
begin
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Entidade := EdEntidade.ValueVariant;
  CorEnt   := CBCorEnt.Selected;
  Sigla    := EdSigla.Text;
  //
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Informe a entidade!') then
    Exit;
  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BPGS1I32(
    'agengrent', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'agengrent', False, [
  'Codigo', 'Entidade', 'CorEnt', 'Sigla'], ['Controle'], [
  Codigo, Entidade, CorEnt, Sigla], [Controle], True) then
  begin
    ReopenAgenGrEnt(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdEntidade.ValueVariant  := 0;
      CBEntidade.KeyValue      := Null;
      CBCorEnt.Selected        := clWhite;
      //
      EdEntidade.SetFocus;
    end else Close;
  end;
end;

procedure TFmAgenGrEnt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAgenGrEnt.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmAgenGrEnt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmAgenGrEnt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAgenGrEnt.ReopenAgenGrEnt(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    UnDMkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmAgenGrEnt.SbCorClick(Sender: TObject);
begin
  CBCorEnt.Selected := MyObjects.SelecionaCor(Self, CBCorEnt.Selected);
end;

procedure TFmAgenGrEnt.SbEntidadeClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Entities.CadastroDeEntidade(
    EdEntidade.ValueVariant, fmcadEntidade2, FmcadEntidade2);
  UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO);
end;

end.
