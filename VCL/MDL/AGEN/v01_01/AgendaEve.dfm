object FmAgendaEve: TFmAgendaEve
  Left = 368
  Top = 194
  Caption = 'AGE-GEREN-002 :: Agendamento de Compromisso'
  ClientHeight = 691
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 365
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 408
        Top = 16
        Width = 164
        Height = 13
        Caption = 'Terceiro (Cliente, fornecedor, etc.):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 126
        Height = 13
        Caption = 'Atividade a ser executada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 372
        Top = 56
        Width = 124
        Height = 13
        Caption = 'Fato gerador da atividade:'
        Color = clBtnFace
        ParentColor = False
      end
      object SbAgenExCad: TSpeedButton
        Left = 344
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbAgenExCadClick
      end
      object SbAgenFgCad: TSpeedButton
        Left = 740
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbAgenFgCadClick
      end
      object Label52: TLabel
        Left = 16
        Top = 176
        Width = 30
        Height = 13
        Caption = 'In'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 168
        Top = 176
        Width = 41
        Height = 13
        Caption = 'T'#233'rmino:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 320
        Top = 176
        Width = 163
        Height = 13
        Caption = 'Lugar do compromisso / atividade:'
        Color = clBtnFace
        ParentColor = False
      end
      object SbLocal: TSpeedButton
        Left = 752
        Top = 192
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbLocalClick
      end
      object Label2: TLabel
        Left = 344
        Top = 220
        Width = 112
        Height = 13
        Caption = 'Status do compromisso:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 96
        Width = 149
        Height = 13
        Caption = 'Descri'#231#227'o (informa'#231#227'o p'#250'blica):'
      end
      object Label19: TLabel
        Left = 16
        Top = 136
        Width = 166
        Height = 13
        Caption = 'Complemento (informa'#231#227'o privada):'
      end
      object EdQuestaoCod: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'QuestaoCod'
        UpdCampo = 'QuestaoCod'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 112
        Width = 757
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBEmprEnti: TdmkDBLookupComboBox
        Left = 128
        Top = 32
        Width = 277
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmprEnti
        QryCampo = 'EmprEnti'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmprEnti: TdmkEditCB
        Left = 76
        Top = 32
        Width = 48
        Height = 22
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EmprEnti'
        UpdCampo = 'EmprEnti'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmprEnti
        IgnoraDBLookupComboBox = False
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 456
        Top = 32
        Width = 317
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_Enti'
        ListSource = DsTerceiros
        TabOrder = 4
        dmkEditCB = EdTerceiro
        QryCampo = 'Terceiro'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTerceiro: TdmkEditCB
        Left = 408
        Top = 32
        Width = 48
        Height = 22
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Terceiro'
        UpdCampo = 'Terceiro'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
      end
      object EdQuestaoExe: TdmkEditCB
        Left = 16
        Top = 72
        Width = 48
        Height = 22
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'QuestaoExe'
        UpdCampo = 'QuestaoExe'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBQuestaoExe
        IgnoraDBLookupComboBox = False
      end
      object CBQuestaoExe: TdmkDBLookupComboBox
        Left = 64
        Top = 72
        Width = 277
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsAgenExCad
        TabOrder = 6
        dmkEditCB = EdQuestaoExe
        QryCampo = 'QuestaoExe'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFatoGeradr: TdmkEditCB
        Left = 372
        Top = 72
        Width = 48
        Height = 22
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FatoGeradr'
        UpdCampo = 'FatoGeradr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFatoGeradr
        IgnoraDBLookupComboBox = False
      end
      object CBFatoGeradr: TdmkDBLookupComboBox
        Left = 420
        Top = 72
        Width = 317
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsAgenFgCad
        TabOrder = 8
        dmkEditCB = EdFatoGeradr
        QryCampo = 'FatoGeradr'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPInicio: TdmkEditDateTimePicker
        Left = 16
        Top = 192
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 11
        OnClick = TPInicioClick
        OnChange = TPInicioChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'Inicio'
        UpdCampo = 'Inicio'
        UpdType = utYes
      end
      object EdInicio: TdmkEdit
        Left = 124
        Top = 192
        Width = 40
        Height = 21
        TabOrder = 12
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'Inicio'
        UpdCampo = 'Inicio'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPTermino: TdmkEditDateTimePicker
        Left = 168
        Top = 192
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 13
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'Termino'
        UpdCampo = 'Termino'
        UpdType = utYes
      end
      object EdTermino: TdmkEdit
        Left = 276
        Top = 192
        Width = 40
        Height = 21
        TabOrder = 14
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'Termino'
        UpdCampo = 'Termino'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdLocal: TdmkEditCB
        Left = 320
        Top = 192
        Width = 48
        Height = 22
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Local'
        UpdCampo = 'Local'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdLocalChange
        DBLookupComboBox = CBLocal
        IgnoraDBLookupComboBox = False
      end
      object CBLocal: TdmkDBLookupComboBox
        Left = 368
        Top = 192
        Width = 381
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSiapTerCad
        TabOrder = 16
        dmkEditCB = EdLocal
        QryCampo = 'Local'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStatus: TdmkEditCB
        Left = 344
        Top = 236
        Width = 48
        Height = 22
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Status'
        UpdCampo = 'Status'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdLocalChange
        DBLookupComboBox = CBStatus
        IgnoraDBLookupComboBox = False
      end
      object CBStatus: TdmkDBLookupComboBox
        Left = 392
        Top = 236
        Width = 381
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsAgenStats
        TabOrder = 18
        dmkEditCB = EdStatus
        QryCampo = 'Status'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGLupPeri: TdmkRadioGroup
        Left = 16
        Top = 220
        Width = 325
        Height = 137
        Caption = ' Periodicidade: '
        ItemIndex = 0
        Items.Strings = (
          'Nenhuma'
          'Di'#225'ria'
          'Semanal'
          'A cada 2 semanas'
          'Mensal dia / semana'
          'Mensal dia do m'#234's'
          'Anual')
        TabOrder = 19
        QryCampo = 'LupPeri'
        UpdCampo = 'LupPeri'
        UpdType = utYes
        OldValor = 0
      end
      object GroupBox1: TGroupBox
        Left = 344
        Top = 260
        Width = 429
        Height = 97
        Caption = ' Quantidade: '
        TabOrder = 20
        object RGLupModo: TdmkRadioGroup
          Left = 2
          Top = 15
          Width = 411
          Height = 80
          Align = alLeft
          Caption = ' Forma: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Nenhuma'
            'At'#233' a data:'
            'Repeti'#231#245'es:')
          TabOrder = 0
          QryCampo = 'LupModo'
          UpdCampo = 'LupModo'
          UpdType = utYes
          OldValor = 0
        end
        object TPLupDFim: TdmkEditDateTimePicker
          Left = 88
          Top = 66
          Width = 112
          Height = 21
          Date = 41563.429133738430000000
          Time = 41563.429133738430000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'LupDFim'
          UpdCampo = 'LupDFim'
          UpdType = utYes
        end
        object EdLupQtde: TdmkEdit
          Left = 292
          Top = 36
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'LupQtde'
          UpdCampo = 'LupQtde'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object EdInfoPrivat: TdmkEdit
        Left = 16
        Top = 152
        Width = 757
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'InfoPrivat'
        UpdCampo = 'InfoPrivat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 532
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object MeTerCadEnder_Edit: TMemo
      Left = 0
      Top = 365
      Width = 784
      Height = 28
      TabStop = False
      Align = alTop
      Alignment = taCenter
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 2
      ExplicitTop = 325
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 361
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdQuestaoCod
      end
      object Label10: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 408
        Top = 16
        Width = 164
        Height = 13
        Caption = 'Terceiro (Cliente, fornecedor, etc.):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 16
        Top = 56
        Width = 126
        Height = 13
        Caption = 'Atividade a ser executada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 372
        Top = 56
        Width = 124
        Height = 13
        Caption = 'Fato gerador da atividade:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 16
        Top = 176
        Width = 30
        Height = 13
        Caption = 'In'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 168
        Top = 176
        Width = 41
        Height = 13
        Caption = 'T'#233'rmino:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 320
        Top = 176
        Width = 163
        Height = 13
        Caption = 'Lugar do compromisso / atividade:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label18: TLabel
        Left = 348
        Top = 218
        Width = 112
        Height = 13
        Caption = 'Status do compromisso:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 16
        Top = 96
        Width = 149
        Height = 13
        Caption = 'Descri'#231#227'o (informa'#231#227'o p'#250'blica):'
      end
      object Label20: TLabel
        Left = 16
        Top = 136
        Width = 166
        Height = 13
        Caption = 'Complemento (informa'#231#227'o privada):'
      end
      object DBEdQuestaoCod: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'QuestaoCod'
        DataSource = DsAgendaEve
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 16
        Top = 112
        Width = 750
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsAgendaEve
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 48
        Height = 21
        DataField = 'EmprEnti'
        DataSource = DsAgendaEve
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 124
        Top = 32
        Width = 281
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsAgendaEve
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 408
        Top = 32
        Width = 48
        Height = 21
        DataField = 'Terceiro'
        DataSource = DsAgendaEve
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 456
        Top = 32
        Width = 312
        Height = 21
        DataField = 'NO_TERCEIRO'
        DataSource = DsAgendaEve
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 72
        Width = 48
        Height = 21
        DataField = 'QuestaoExe'
        DataSource = DsAgendaEve
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 64
        Top = 72
        Width = 304
        Height = 21
        DataField = 'NO_QUESTAOEXE'
        DataSource = DsAgendaEve
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 420
        Top = 72
        Width = 345
        Height = 21
        DataField = 'NO_FATOGERADR'
        DataSource = DsAgendaEve
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 372
        Top = 72
        Width = 48
        Height = 21
        DataField = 'FatoGeradr'
        DataSource = DsAgendaEve
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 16
        Top = 192
        Width = 148
        Height = 21
        DataField = 'Inicio'
        DataSource = DsAgendaEve
        TabOrder = 10
      end
      object DBEdit10: TDBEdit
        Left = 168
        Top = 192
        Width = 148
        Height = 21
        DataField = 'Termino'
        DataSource = DsAgendaEve
        TabOrder = 11
      end
      object DBEdit11: TDBEdit
        Left = 320
        Top = 192
        Width = 48
        Height = 21
        DataField = 'Local'
        DataSource = DsAgendaEve
        TabOrder = 12
      end
      object DBEdit12: TDBEdit
        Left = 368
        Top = 192
        Width = 396
        Height = 21
        DataField = 'NO_LUGAR'
        DataSource = DsAgendaEve
        TabOrder = 13
      end
      object DBEdit13: TDBEdit
        Left = 348
        Top = 234
        Width = 48
        Height = 21
        DataField = 'Status'
        DataSource = DsAgendaEve
        TabOrder = 14
      end
      object DBEdit14: TDBEdit
        Left = 396
        Top = 234
        Width = 369
        Height = 21
        DataField = 'NO_STATUS'
        DataSource = DsAgendaEve
        TabOrder = 15
      end
      object DBRGLupPeri: TDBRadioGroup
        Left = 16
        Top = 216
        Width = 325
        Height = 137
        Caption = ' Periodicidade: '
        DataField = 'LupPeri'
        DataSource = DsAgendaEve
        Items.Strings = (
          'Nenhuma'
          'Di'#225'ria'
          'Semanal'
          'A cada 2 semanas'
          'Mensal dia / semana'
          'Mensal dia do m'#234's'
          'Anual')
        ParentBackground = True
        TabOrder = 16
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object GroupBox2: TGroupBox
        Left = 344
        Top = 256
        Width = 421
        Height = 97
        Caption = ' Quantidade: '
        TabOrder = 17
        object dmkRadioGroup2: TDBRadioGroup
          Left = 2
          Top = 15
          Width = 411
          Height = 80
          Align = alLeft
          Caption = ' Forma: '
          Columns = 2
          DataField = 'LupModo'
          DataSource = DsAgendaEve
          Items.Strings = (
            'Nenhuma'
            'At'#233' a data:'
            'Repeti'#231#245'es:')
          ParentBackground = True
          TabOrder = 0
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object Panel6: TPanel
          Left = 84
          Top = 66
          Width = 121
          Height = 25
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object DBEdit15: TDBEdit
            Left = 0
            Top = 0
            Width = 112
            Height = 21
            DataField = 'LupDFim'
            DataSource = DsAgendaEve
            TabOrder = 0
          end
        end
        object Panel7: TPanel
          Left = 320
          Top = 34
          Width = 40
          Height = 25
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object DBEdit16: TDBEdit
            Left = 0
            Top = 0
            Width = 32
            Height = 21
            DataField = 'LupQtde'
            DataSource = DsAgendaEve
            TabOrder = 0
          end
        end
      end
      object DBEdInfoPrivat: TdmkDBEdit
        Left = 16
        Top = 152
        Width = 750
        Height = 21
        Color = clWhite
        DataField = 'InfoPrivat'
        DataSource = DsAgendaEve
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 18
        Visible = False
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 531
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtIts: TBitBtn
          Tag = 100
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Participante'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtItsClick
        end
        object BtCab: TBitBtn
          Tag = 191
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Evento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object MeTerCadEnder_Dado: TMemo
      Left = 0
      Top = 361
      Width = 784
      Height = 79
      TabStop = False
      Align = alTop
      Alignment = taCenter
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 2
      ExplicitTop = 325
    end
    object DBGAgendaEnt: TDBGrid
      Left = 0
      Top = 440
      Width = 784
      Height = 45
      Align = alTop
      DataSource = DsAgendaEnt
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Entidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ENT'
          Title.Caption = 'Nome da entidade'
          Width = 616
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 381
        Height = 32
        Caption = 'Agendamento de Compromisso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 381
        Height = 32
        Caption = 'Agendamento de Compromisso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 381
        Height = 32
        Caption = 'Agendamento de Compromisso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrAgendaEve: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrAgendaEveBeforeOpen
    AfterOpen = QrAgendaEveAfterOpen
    BeforeClose = QrAgendaEveBeforeClose
    BeforeScroll = QrAgendaEveBeforeScroll
    AfterScroll = QrAgendaEveAfterScroll
    SQL.Strings = (
      'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO,'
      'stc.Nome NO_LUGAR, eve.*'
      'FROM agendaeve eve'
      'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr'
      'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe'
      'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti'
      'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti'
      'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro'
      'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local'
      'WHERE eve.QuestaoCod>0'
      'AND eve.QuestaoTyp=1')
    Left = 64
    Top = 64
    object QrAgendaEveNO_FATOGERADR: TWideStringField
      FieldName = 'NO_FATOGERADR'
      Size = 60
    end
    object QrAgendaEveNO_QUESTAOEXE: TWideStringField
      FieldName = 'NO_QUESTAOEXE'
      Size = 60
    end
    object QrAgendaEveNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrAgendaEveNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrAgendaEveCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgendaEveQuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
    end
    object QrAgendaEveQuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
    end
    object QrAgendaEveQuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
    end
    object QrAgendaEveInicio: TDateTimeField
      FieldName = 'Inicio'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrAgendaEveTermino: TDateTimeField
      FieldName = 'Termino'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrAgendaEveEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrAgendaEveTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrAgendaEveLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrAgendaEveCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrAgendaEveCption: TSmallintField
      FieldName = 'Cption'
    end
    object QrAgendaEveNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrAgendaEveNotas: TWideStringField
      FieldName = 'Notas'
      Size = 255
    end
    object QrAgendaEveFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrAgendaEveNO_LUGAR: TWideStringField
      FieldName = 'NO_LUGAR'
      Size = 100
    end
    object QrAgendaEveStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAgendaEveNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrAgendaEveLupPeri: TSmallintField
      FieldName = 'LupPeri'
    end
    object QrAgendaEveLupModo: TSmallintField
      FieldName = 'LupModo'
    end
    object QrAgendaEveLupDFim: TDateField
      FieldName = 'LupDFim'
    end
    object QrAgendaEveLupQtde: TIntegerField
      FieldName = 'LupQtde'
    end
    object QrAgendaEveInfoPrivat: TWideStringField
      FieldName = 'InfoPrivat'
      Size = 255
    end
    object QrAgendaEveUserCad: TIntegerField
      FieldName = 'UserCad'
    end
  end
  object DsAgendaEve: TDataSource
    DataSet = QrAgendaEve
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    CanUpd01 = BtIts
    Left = 120
    Top = 64
  end
  object QrAgenExCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM agenexcad'
      'ORDER BY Nome')
    Left = 224
    Top = 60
    object QrAgenExCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgenExCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAgenExCad: TDataSource
    DataSet = QrAgenExCad
    Left = 248
    Top = 60
  end
  object QrAgenFgCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM agenfgcad'
      'ORDER BY Nome')
    Left = 276
    Top = 60
    object QrAgenFgCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgenFgCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAgenFgCad: TDataSource
    DataSet = QrAgenFgCad
    Left = 304
    Top = 60
  end
  object DsSiapTerCad: TDataSource
    DataSet = QrSiapTerCad
    Left = 248
    Top = 88
  end
  object QrTerceiros: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_Enti'
      'FROM entidades'
      'ORDER BY NO_Enti')
    Left = 276
    Top = 88
    object QrTerceirosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTerceirosNO_Enti: TWideStringField
      FieldName = 'NO_Enti'
      Size = 100
    end
  end
  object DsTerceiros: TDataSource
    DataSet = QrTerceiros
    Left = 304
    Top = 88
  end
  object PMLocais: TPopupMenu
    Left = 648
    Top = 48
  end
  object QrSiapTerCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT llo.Nome NOMELOGRAD, stc.* '
      'FROM siaptercad stc'
      'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd'
      'WHERE stc.Cliente=:P0')
    Left = 220
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapTerCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapTerCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSiapTerCadCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSiapTerCadSLograd: TSmallintField
      FieldName = 'SLograd'
    end
    object QrSiapTerCadSRua: TWideStringField
      FieldName = 'SRua'
      Size = 60
    end
    object QrSiapTerCadSNumero: TIntegerField
      FieldName = 'SNumero'
    end
    object QrSiapTerCadSCompl: TWideStringField
      FieldName = 'SCompl'
      Size = 60
    end
    object QrSiapTerCadSBairro: TWideStringField
      FieldName = 'SBairro'
      Size = 60
    end
    object QrSiapTerCadSCidade: TWideStringField
      FieldName = 'SCidade'
      Size = 60
    end
    object QrSiapTerCadSUF: TWideStringField
      FieldName = 'SUF'
      Size = 3
    end
    object QrSiapTerCadSCEP: TIntegerField
      FieldName = 'SCEP'
    end
    object QrSiapTerCadSPais: TWideStringField
      FieldName = 'SPais'
      Size = 60
    end
    object QrSiapTerCadSEndeRef: TWideStringField
      FieldName = 'SEndeRef'
      Size = 100
    end
    object QrSiapTerCadSCodMunici: TIntegerField
      FieldName = 'SCodMunici'
    end
    object QrSiapTerCadSCodiPais: TIntegerField
      FieldName = 'SCodiPais'
    end
    object QrSiapTerCadM2Constru: TFloatField
      FieldName = 'M2Constru'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadM2NaoBuild: TFloatField
      FieldName = 'M2NaoBuild'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadM2Terreno: TFloatField
      FieldName = 'M2Terreno'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadM2Total: TFloatField
      FieldName = 'M2Total'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
  end
  object QrSTC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT llo.Nome NOMELOGRAD, stc.* '
      'FROM siaptercad stc'
      'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd'
      'WHERE stc.Cliente=:P0')
    Left = 148
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSTCNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrSTCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSTCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSTCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSTCSLograd: TSmallintField
      FieldName = 'SLograd'
    end
    object QrSTCSRua: TWideStringField
      FieldName = 'SRua'
      Size = 60
    end
    object QrSTCSNumero: TIntegerField
      FieldName = 'SNumero'
    end
    object QrSTCSCompl: TWideStringField
      FieldName = 'SCompl'
      Size = 60
    end
    object QrSTCSBairro: TWideStringField
      FieldName = 'SBairro'
      Size = 60
    end
    object QrSTCSCidade: TWideStringField
      FieldName = 'SCidade'
      Size = 60
    end
    object QrSTCSUF: TWideStringField
      FieldName = 'SUF'
      Size = 3
    end
    object QrSTCSCEP: TIntegerField
      FieldName = 'SCEP'
    end
    object QrSTCSPais: TWideStringField
      FieldName = 'SPais'
      Size = 60
    end
    object QrSTCSEndeRef: TWideStringField
      FieldName = 'SEndeRef'
      Size = 100
    end
    object QrSTCSCodMunici: TIntegerField
      FieldName = 'SCodMunici'
    end
    object QrSTCSCodiPais: TIntegerField
      FieldName = 'SCodiPais'
    end
    object QrSTCSTe1: TWideStringField
      FieldName = 'STe1'
    end
    object QrSTCM2Constru: TFloatField
      FieldName = 'M2Constru'
    end
    object QrSTCM2NaoBuild: TFloatField
      FieldName = 'M2NaoBuild'
    end
    object QrSTCM2Terreno: TFloatField
      FieldName = 'M2Terreno'
    end
    object QrSTCM2Total: TFloatField
      FieldName = 'M2Total'
    end
  end
  object QrAgendaEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT age.*, '
      'IF(ent.Tipo, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM agendaent age '
      'LEFT JOIN entidades ent ON ent.Codigo=age.Entidade ')
    Left = 120
    Top = 92
    object QrAgendaEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgendaEntControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAgendaEntEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrAgendaEntNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsAgendaEnt: TDataSource
    DataSet = QrAgendaEnt
    Left = 148
    Top = 92
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 276
    Top = 480
    object Agendanovocompromisso1: TMenuItem
      Caption = 'Agenda novo compromisso'
      OnClick = Agendanovocompromisso1Click
    end
    object Alteracompromissoatual1: TMenuItem
      Caption = 'Altera compromisso atual'
      OnClick = Alteracompromissoatual1Click
    end
    object Excluicompromissoatual1: TMenuItem
      Caption = '&Exclui compromisso atual'
      OnClick = Excluicompromissoatual1Click
    end
  end
  object PMIts: TPopupMenu
    Left = 396
    Top = 480
    object Adicionaentidadeaoevento1: TMenuItem
      Caption = 'Adiciona entidade ao evento'
      OnClick = Adicionaentidadeaoevento1Click
    end
    object Retiraentidadesdoevento1: TMenuItem
      Caption = 'Retira entidade(s) do evento'
      OnClick = Retiraentidadesdoevento1Click
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    Left = 244
    Top = 284
  end
  object QrAgenStats: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM agenstats'
      'ORDER BY Codigo')
    Left = 84
    Top = 360
    object QrAgenStatsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgenStatsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAgenStats: TDataSource
    DataSet = QrAgenStats
    Left = 84
    Top = 408
  end
end
