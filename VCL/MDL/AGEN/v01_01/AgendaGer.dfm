object FmAgendaGer: TFmAgendaGer
  Left = 339
  Top = 185
  Caption = 'AGE-GEREN-001 :: Gerenciamento de Agendas'
  ClientHeight = 712
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 169
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbSair: TBitBtn
        Tag = 13
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbSairClick
      end
      object BtRefresh: TBitBtn
        Tag = 18
        Left = 44
        Top = 8
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtRefreshClick
      end
      object BtLimpa: TBitBtn
        Tag = 68
        Left = 84
        Top = 8
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtLimpaClick
      end
      object SbImprime: TBitBtn
        Tag = 5
        Left = 124
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 169
      Top = 0
      Width = 357
      Height = 52
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 337
        Height = 32
        Caption = 'Gerenciamento de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 337
        Height = 32
        Caption = 'Gerenciamento de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 337
        Height = 32
        Caption = 'Gerenciamento de Agendas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 526
      Top = 0
      Width = 434
      Height = 52
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 430
        Height = 35
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 590
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 590
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 590
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 1004
          Height = 573
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 0
          OnChange = PageControl1Change
          object TabSheet1: TTabSheet
            Caption = 'Agenda do dia'
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 45
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label1: TLabel
                Left = 240
                Top = 4
                Width = 108
                Height = 13
                Caption = 'Grupo de visualiza'#231#227'o:'
              end
              object SBAgenGrCabDia: TSpeedButton
                Left = 628
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SBAgenGrCabDiaClick
              end
              object SBMostraAgendaDia: TSpeedButton
                Left = 648
                Top = 20
                Width = 21
                Height = 21
                Caption = '>'
                Enabled = False
                OnClick = SBMostraAgendaDiaClick
              end
              object Label2: TLabel
                Left = 128
                Top = 4
                Width = 26
                Height = 13
                Caption = 'Data:'
              end
              object EdAgenGrCabDia: TdmkEditCB
                Left = 240
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdAgenGrCabDiaChange
                DBLookupComboBox = CBAgenGrCabDia
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBAgenGrCabDia: TdmkDBLookupComboBox
                Left = 296
                Top = 20
                Width = 333
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsAgenGrCabDia
                TabOrder = 2
                dmkEditCB = EdAgenGrCabDia
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object TPDataDia: TdmkEditDateTimePicker
                Left = 128
                Top = 20
                Width = 108
                Height = 21
                Date = 41322.621779293980000000
                Time = 41322.621779293980000000
                TabOrder = 0
                OnClick = TPDataDiaClick
                OnChange = TPDataDiaChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Agenda da semana'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 45
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label4: TLabel
                Left = 128
                Top = 4
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object TPDataSem: TdmkEditDateTimePicker
                Left = 128
                Top = 20
                Width = 108
                Height = 21
                Date = 41322.621779293980000000
                Time = 41322.621779293980000000
                TabOrder = 0
                OnClick = TPDataSemClick
                OnChange = TPDataSemChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Agenda do m'#234's'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 642
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    Visible = False
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object TimerIndicator: TTimer
    OnTimer = TimerIndicatorTimer
    Left = 400
    Top = 4
  end
  object QrAgenGrCabDia: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM agengrcab'
      'ORDER BY Nome')
    Left = 236
    Top = 276
    object QrAgenGrCabDiaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgenGrCabDiaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrAgenGrCabDiaColLarg: TIntegerField
      FieldName = 'ColLarg'
    end
    object QrAgenGrCabDiaInterMinut: TIntegerField
      FieldName = 'InterMinut'
    end
    object QrAgenGrCabDiaCorMul: TIntegerField
      FieldName = 'CorMul'
    end
  end
  object DsAgenGrCabDia: TDataSource
    DataSet = QrAgenGrCabDia
    Left = 264
    Top = 276
  end
  object QrEventos2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEventos2BeforeClose
    AfterScroll = QrEventos2AfterScroll
    SQL.Strings = (
      'SELECT fge.Nome NO_FATOGERADR, eve.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM agendaeve eve'
      'LEFT JOIN entidades ent ON ent.Codigo=eve.Terceiro'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=eve.FatoGeradr'
      'WHERE eve.QuestaoTyp=2 '
      ''
      'AND ( '
      '  (Inicio BETWEEN "2013-02-12" AND  "2013-02-16 23:59:59")'
      '  OR'
      '  (Termino BETWEEN "2013-02-12" AND  "2013-02-16 23:59:59")'
      ')')
    Left = 260
    Top = 152
    object QrEventos2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventos2QuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
    end
    object QrEventos2QuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
    end
    object QrEventos2QuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
    end
    object QrEventos2Inicio: TDateTimeField
      FieldName = 'Inicio'
    end
    object QrEventos2Termino: TDateTimeField
      FieldName = 'Termino'
    end
    object QrEventos2EmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrEventos2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEventos2Local: TIntegerField
      FieldName = 'Local'
    end
    object QrEventos2Cor: TIntegerField
      FieldName = 'Cor'
    end
    object QrEventos2Cption: TSmallintField
      FieldName = 'Cption'
    end
    object QrEventos2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEventos2Notas: TWideStringField
      FieldName = 'Notas'
      Size = 255
    end
    object QrEventos2NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEventos2FatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrEventos2NO_FATOGERADR: TWideStringField
      FieldName = 'NO_FATOGERADR'
    end
    object QrEventos2Status: TIntegerField
      FieldName = 'Status'
    end
  end
  object QrAgentes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Entidade, osa.Controle'
      'FROM osage osa'
      'LEFT JOIN agengrent ent ON ent.Entidade=osa.Agente'
      'WHERE osa.Codigo=71'
      'AND ent.Codigo=1')
    Left = 260
    Top = 180
    object QrAgentesEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrAgentesControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrAgenGrEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT age.*, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM agengrent age'
      'LEFT JOIN entidades ent ON ent.Codigo=age.Entidade'
      'WHERE age.Codigo=:P0'
      'ORDER BY NO_ENT')
    Left = 260
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAgenGrEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgenGrEntControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAgenGrEntEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrAgenGrEntCorEnt: TIntegerField
      FieldName = 'CorEnt'
    end
    object QrAgenGrEntNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrAgenGrEntSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 30
    end
  end
  object DsAgenGrEnt: TDataSource
    DataSet = QrAgenGrEnt
    Left = 288
    Top = 124
  end
  object DsEventos2: TDataSource
    DataSet = QrEventos2
    Left = 288
    Top = 152
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 288
    Top = 180
  end
  object QrAgendaEve: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEventos2BeforeClose
    AfterScroll = QrEventos2AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM agendaeve'
      'WHERE Codigo=:P0')
    Left = 316
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAgendaEveCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgendaEveQuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
    end
    object QrAgendaEveQuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
    end
    object QrAgendaEveQuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
    end
    object QrAgendaEveInicio: TDateTimeField
      FieldName = 'Inicio'
    end
    object QrAgendaEveTermino: TDateTimeField
      FieldName = 'Termino'
    end
    object QrAgendaEveEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrAgendaEveTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrAgendaEveLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrAgendaEveCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrAgendaEveCption: TSmallintField
      FieldName = 'Cption'
    end
    object QrAgendaEveNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrAgendaEveNotas: TWideStringField
      FieldName = 'Notas'
      Size = 255
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 920
    Top = 12
  end
  object PMAgendaDiaItem_2: TPopupMenu
    Left = 376
    Top = 196
    object Excluirentidadedoenvento1: TMenuItem
      Caption = '&Excluir entidade do envento'
      OnClick = Excluirentidadedoenvento1Click
    end
  end
  object DsAgenGrCabSem: TDataSource
    DataSet = QrAgenGrCabSem
    Left = 264
    Top = 304
  end
  object QrAgenGrCabSem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM agengrcab'
      'ORDER BY Nome')
    Left = 236
    Top = 304
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'ColLarg'
    end
    object IntegerField3: TIntegerField
      FieldName = 'InterMinut'
    end
    object IntegerField4: TIntegerField
      FieldName = 'CorMul'
    end
  end
  object DsAgenGrCabMes: TDataSource
    DataSet = QrAgenGrCabMes
    Left = 264
    Top = 332
  end
  object QrAgenGrCabMes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM agengrcab'
      'ORDER BY Nome')
    Left = 236
    Top = 332
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField6: TIntegerField
      FieldName = 'ColLarg'
    end
    object IntegerField7: TIntegerField
      FieldName = 'InterMinut'
    end
    object IntegerField8: TIntegerField
      FieldName = 'CorMul'
    end
  end
  object PMAgendaSemItem_2: TPopupMenu
    Left = 404
    Top = 196
    object N1: TMenuItem
      Caption = '???'
    end
  end
  object PMAgendaMesItem_2: TPopupMenu
    Left = 432
    Top = 196
    object N2: TMenuItem
      Caption = '? ? ? /'
    end
  end
  object QrEventos1: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEventos1AfterScroll
    SQL.Strings = (
      'SELECT fgc.Nome NO_FATOGERADR, exc.Nome NO_QUESTAOEXE,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TERCEIRO,'
      'stc.Nome NO_LUGAR, eve.*'
      'FROM agendaeve eve'
      'LEFT JOIN agenfgcad fgc ON fgc.Codigo=eve.FatoGeradr'
      'LEFT JOIN agenexcad exc ON exc.Codigo=eve.QuestaoExe'
      'LEFT JOIN enticliint eci ON eci.CodCliInt=eve.EmprEnti'
      'LEFT JOIN entidades emp ON emp.Codigo=eci.CodEnti'
      'LEFT JOIN entidades ter ON ter.Codigo=eve.Terceiro'
      'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local'
      'WHERE eve.QuestaoCod>0'
      'AND eve.QuestaoTyp=1')
    Left = 196
    Top = 124
    object QrEventos1NO_FATOGERADR: TWideStringField
      FieldName = 'NO_FATOGERADR'
      Size = 60
    end
    object QrEventos1NO_QUESTAOEXE: TWideStringField
      FieldName = 'NO_QUESTAOEXE'
      Size = 60
    end
    object QrEventos1NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEventos1NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrEventos1NO_LUGAR: TWideStringField
      FieldName = 'NO_LUGAR'
      Size = 100
    end
    object QrEventos1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventos1QuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
    end
    object QrEventos1QuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
    end
    object QrEventos1QuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
    end
    object QrEventos1Inicio: TDateTimeField
      FieldName = 'Inicio'
    end
    object QrEventos1Termino: TDateTimeField
      FieldName = 'Termino'
    end
    object QrEventos1EmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrEventos1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEventos1Local: TIntegerField
      FieldName = 'Local'
    end
    object QrEventos1Cor: TIntegerField
      FieldName = 'Cor'
    end
    object QrEventos1Cption: TSmallintField
      FieldName = 'Cption'
    end
    object QrEventos1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEventos1Notas: TWideStringField
      FieldName = 'Notas'
      Size = 255
    end
    object QrEventos1FatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrEventos1Status: TIntegerField
      FieldName = 'Status'
    end
    object QrEventos1LupPeri: TSmallintField
      FieldName = 'LupPeri'
    end
    object QrEventos1LupModo: TSmallintField
      FieldName = 'LupModo'
    end
    object QrEventos1LupDFim: TDateTimeField
      FieldName = 'LupDFim'
    end
    object QrEventos1LupQtde: TIntegerField
      FieldName = 'LupQtde'
    end
  end
  object QrEnti1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Entidade, aee.Controle  '
      'FROM agendaent aee  '
      'INNER JOIN agengrent ent ON ent.Entidade=aee.Entidade '
      'WHERE aee.Codigo>0')
    Left = 196
    Top = 152
    object QrEnti1Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEnti1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object PMAgendaDiaItem_1: TPopupMenu
    OwnerDraw = True
    Left = 376
    Top = 224
    object Alterarevento1: TMenuItem
      Caption = 'Alterar evento'
      OnClick = Alterarevento1Click
    end
    object Removerentidadedoevento1: TMenuItem
      Caption = '&Remover entidade do evento'
      OnClick = Removerentidadedoevento1Click
    end
  end
  object PMAgendaSemItem_1: TPopupMenu
    Left = 404
    Top = 224
    object Alterarevento2: TMenuItem
      Caption = '&Alterar evento'
      OnClick = Alterarevento2Click
    end
  end
  object PMAgendaMesItem_1: TPopupMenu
    Left = 432
    Top = 224
    object Alterarevento3: TMenuItem
      Caption = '&Alterar evento'
      OnClick = Alterarevento3Click
    end
  end
  object QrAgendaLup: TMySQLQuery
    Database = Dmod.MyDB
    Left = 236
    Top = 404
    object QrAgendaLupCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgendaLupDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrAgendaLupDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrAgendaLupLupPeri: TIntegerField
      FieldName = 'LupPeri'
    end
  end
  object TMCarrega: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TMCarregaTimer
    Left = 584
    Top = 240
  end
end
