unit GraTecCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkImage, UnDmkEnums;

type
  TFmGraTecCad = class(TForm)
    PainelDados: TPanel;
    DsGraTecCad: TDataSource;
    QrGraTecCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrGraTecCadCodigo: TIntegerField;
    QrGraTecCadCodUsu: TIntegerField;
    QrGraTecCadNome: TWideStringField;
    DBGItens: TdmkDBGrid;
    PMAtributos: TPopupMenu;
    Incluinovoatributo1: TMenuItem;
    Alteraatributoatual1: TMenuItem;
    ExcluiAtributoatual1: TMenuItem;
    QrGraTecIts: TmySQLQuery;
    DsGraTecIts: TDataSource;
    QrGraTecItsCodigo: TIntegerField;
    QrGraTecItsControle: TIntegerField;
    QrGraTecItsPercent: TFloatField;
    QrGraTecItsCODUSUFIB: TIntegerField;
    QrGraTecItsNOMEFIB: TWideStringField;
    QrGraTecItsSigla: TWideStringField;
    QrGraTecCadPercent: TFloatField;
    Label4: TLabel;
    DBEdPercent: TDBEdit;
    Panel4: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtAtributos: TBitBtn;
    BtItens: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraTecCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraTecCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure Incluinovoatributo1Click(Sender: TObject);
    procedure Alteraatributoatual1Click(Sender: TObject);
    procedure BtAtributosClick(Sender: TObject);
    procedure PMAtributosPopup(Sender: TObject);
    procedure QrGraTecCadBeforeClose(DataSet: TDataSet);
    procedure QrGraTecCadAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure DBEdPercentChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenGraAtrIts(Controle: Integer);
  end;

var
  FmGraTecCad: TFmGraTecCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, GraAtrIts, MyDBCheck, GraTecIts, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraTecCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraTecCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraTecCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraTecCad.DefParams;
begin
  VAR_GOTOTABELA := 'GraTecCad';
  VAR_GOTOMYSQLTABLE := QrGraTecCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Percent');
  VAR_SQLx.Add('FROM grateccad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraTecCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraTecCad', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmGraTecCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraTecCad.PMAtributosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrGraTecCad.State <> dsInactive) and (QrGraTecCad.RecordCount > 0);
  //
  Alteraatributoatual1.Enabled := Enab;
end;

procedure TFmGraTecCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmGraTecCad.AlteraRegistro;
var
  GraTecCad : Integer;
begin
  GraTecCad := QrGraTecCadCodigo.Value;
  if QrGraTecCadCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(GraTecCad, Dmod.MyDB, 'GraTecCad', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GraTecCad, Dmod.MyDB, 'GraTecCad', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGraTecCad.IncluiRegistro;
var
  Cursor : TCursor;
  GraTecCad : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    GraTecCad := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GraTecCad', 'GraTecCad', 'Codigo');
    if Length(FormatFloat(FFormatFloat, GraTecCad))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, GraTecCad);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmGraTecCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraTecCad.DBEdPercentChange(Sender: TObject);
begin
  if FmGraTecCad.QrGraTecCadPercent.Value = 100 then
    DBEdPercent.Font.Color := clBlue else
    DBEdPercent.Font.Color := clRed;
end;

procedure TFmGraTecCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraTecCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraTecCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraTecCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraTecCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraTecCad.Alteraatributoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraTecCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'grateccad');
end;

procedure TFmGraTecCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraTecCadCodigo.Value;
  Close;
end;

procedure TFmGraTecCad.BtAtributosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtributos, BtAtributos);
end;

procedure TFmGraTecCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
(*
  Codigo := UMyMod.BuscaEmLivreY_Def('GraTecCad', 'Codigo', ImgTipo.SQLType,
    QrGraTecCadCodigo.Value);
*)
  //
  Codigo := EdCodigo.ValueVariant;
  Codigo := UMyMod.BPGS1I32('grateccad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraTecCad, PainelEdit,
    'GraTecCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, true) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraTecCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraTecCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraTecCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraTecCad', 'Codigo');
end;

procedure TFmGraTecCad.BtItensClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraTecIts, FmGraTecIts, afmoNegarComAviso) then
  begin
    FmGraTecIts.ShowModal;
    FmGraTecIts.Destroy;
  end;
end;

procedure TFmGraTecCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  DBGItens.Align    := alClient;
  CriaOForm;
end;

procedure TFmGraTecCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraTecCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraTecCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmGraTecCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraTecCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraTecCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraTecCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraTecCad.QrGraTecCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraTecCad.QrGraTecCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraAtrIts(0);
end;

procedure TFmGraTecCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraTecCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraTecCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GraTecCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraTecCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraTecCad.Incluinovoatributo1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrGraTecCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'grateccad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraTecCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
end;

procedure TFmGraTecCad.QrGraTecCadBeforeClose(DataSet: TDataSet);
begin
  QrGraTecIts.Close;
end;

procedure TFmGraTecCad.QrGraTecCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraTecCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGraTecCad.ReopenGraAtrIts(Controle: Integer);
begin
  QrGraTecIts.Close;
  QrGraTecIts.Params[0].AsInteger :=   QrGraTecCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraTecIts, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrGraTecIts.Locate('Controle', Controle, []);
end;

end.

