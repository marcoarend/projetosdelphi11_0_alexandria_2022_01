unit NCMsMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, dmkValUsu, dmkDBGridZTO;

type
  TFmNCMsMul = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label6: TLabel;
    VUUnidMed: TdmkValUsu;
    DBGErrGrandz: TdmkDBGridZTO;
    QrErrNCMs: TmySQLQuery;
    DsErrNCMs: TDataSource;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrErrNCMsNivel1: TIntegerField;
    QrErrNCMsNome: TWideStringField;
    QrErrNCMsNCM: TWideStringField;
    EdNCM: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure EdNCMRedefinido(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ReopenErrNCMs(SQL: String; DB: TmySQLDataBase);
  end;

  var
  FmNCMsMul: TFmNCMsMul;

implementation

uses UnMyObjects, UnUMedi_PF, DmkDAC_PF, Module, UMySQLModule;

{$R *.DFM}

procedure TFmNCMsMul.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGErrGrandz), False);
end;

procedure TFmNCMsMul.BtOKClick(Sender: TObject);
var
  NCM: String;
  Nivel1, N: Integer;
begin
  NCM := Trim(EdNCM.Text);
  if MyObjects.FIC(NCM = '', EdNCM, 'Informe o NCM!') then
    Exit;
  if MyObjects.FIC(DBGErrGrandz.SelectedRows.Count = 0, nil,
  'Nenhum item foi selecionado!') then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    with DBGErrGrandz.DataSource.DataSet do
    for N := 0 to DBGErrGrandz.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGErrGrandz.SelectedRows.Items[N]));
      GotoBookmark(DBGErrGrandz.SelectedRows.Items[N]);
      //
      Nivel1 := QrErrNCMsNivel1.Value;
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
      'NCM'], ['Nivel1'], [NCM], [Nivel1], True);
    end;
    //
    MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGErrGrandz), False);
    ReopenErrNCMs(QrErrNCMs.SQL.Text, QrErrNCMs.Database);
  finally
    Screen.Cursor := crdefault;
  end;
end;

procedure TFmNCMsMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNCMsMul.BtTodosClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGErrGrandz), True);
end;

procedure TFmNCMsMul.EdNCMRedefinido(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmNCMsMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNCMsMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmNCMsMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNCMsMul.ReopenErrNCMs(SQL: String; DB:
  TmySQLDataBase);
begin
  UnDmkDac_PF.AbreMySQLQUery0(QrErrNCMs, DB, [SQL]);
end;

end.
