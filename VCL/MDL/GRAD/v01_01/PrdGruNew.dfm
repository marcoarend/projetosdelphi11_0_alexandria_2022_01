object FmPrdGruNew: TFmPrdGruNew
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-003 :: Seleciona Tipo de Grupo'
  ClientHeight = 629
  ClientWidth = 650
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnTipo: TPanel
    Left = 0
    Top = 90
    Width = 650
    Height = 5
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
  end
  object Panel3: TPanel
    Left = 0
    Top = 95
    Width = 650
    Height = 38
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label2: TLabel
      Left = 12
      Top = 0
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Left = 96
      Top = 0
      Width = 49
      Height = 13
      Caption = 'Produ'#231#227'o:'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Left = 376
      Top = 0
      Width = 56
      Height = 13
      Caption = 'Fracionado:'
      FocusControl = DBEdit3
    end
    object Label5: TLabel
      Left = 220
      Top = 0
      Width = 68
      Height = 13
      Caption = 'Aplicabilidade:'
      FocusControl = DBEdit4
    end
    object Label6: TLabel
      Left = 440
      Top = 0
      Width = 29
      Height = 13
      Caption = 'N'#237'vel:'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 476
      Top = 0
      Width = 99
      Height = 13
      Caption = 'Faixa de numera'#231#227'o:'
      FocusControl = DBEdit6
    end
    object DBEdit1: TDBEdit
      Left = 12
      Top = 16
      Width = 80
      Height = 21
      DataField = 'Codigo'
      DataSource = DsPrdGrupTip
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 96
      Top = 16
      Width = 121
      Height = 21
      DataField = 'NOME_MADEBY'
      DataSource = DsPrdGrupTip
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 376
      Top = 16
      Width = 61
      Height = 21
      DataField = 'NOME_FRACIO'
      DataSource = DsPrdGrupTip
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 220
      Top = 16
      Width = 153
      Height = 21
      DataField = 'NOME_TIPPRD'
      DataSource = DsPrdGrupTip
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 440
      Top = 16
      Width = 33
      Height = 21
      DataField = 'NivCad'
      DataSource = DsPrdGrupTip
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Left = 476
      Top = 16
      Width = 80
      Height = 21
      DataField = 'FaixaIni'
      DataSource = DsPrdGrupTip
      TabOrder = 5
    end
    object DBEdit7: TDBEdit
      Left = 556
      Top = 16
      Width = 80
      Height = 21
      DataField = 'FaixaFim'
      DataSource = DsPrdGrupTip
      TabOrder = 6
    end
  end
  object PnNiveis: TPanel
    Left = 0
    Top = 177
    Width = 650
    Height = 344
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnNivel3: TPanel
      Left = 0
      Top = 120
      Width = 650
      Height = 60
      Align = alTop
      Enabled = False
      TabOrder = 2
      Visible = False
      object Label8: TLabel
        Left = 12
        Top = 20
        Width = 75
        Height = 13
        Caption = 'C'#243'digo [F3][F4]:'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 96
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label14: TLabel
        Left = 556
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdNivel3
      end
      object EdNome3: TdmkEdit
        Left = 96
        Top = 36
        Width = 458
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdNome3KeyDown
      end
      object StaticText3: TStaticText
        Left = 1
        Top = 1
        Width = 648
        Height = 17
        Align = alTop
        Alignment = taCenter
        BevelKind = bkSoft
        Caption = 'N'#205'VEL 3 (DOIS N'#205'VEIS ACIMA DO GRUPO DE PRODUTOS)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdNivel3: TDBEdit
        Left = 556
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Nivel3'
        DataSource = DsGraGru3
        Enabled = False
        TabOrder = 3
      end
      object EdCodUsu3: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodUsu3Change
        OnEnter = EdCodUsu3Enter
        OnExit = EdCodUsu3Exit
        OnKeyDown = EdCodUsu3KeyDown
      end
    end
    object PnNivel2: TPanel
      Left = 0
      Top = 180
      Width = 650
      Height = 60
      Align = alTop
      Enabled = False
      TabOrder = 3
      Visible = False
      object Label11: TLabel
        Left = 96
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label10: TLabel
        Left = 12
        Top = 20
        Width = 75
        Height = 13
        Caption = 'C'#243'digo [F3][F4]:'
        FocusControl = DBEdit1
      end
      object Label15: TLabel
        Left = 556
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdNivel2
      end
      object EdCodUsu2: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodUsu2Change
        OnEnter = EdCodUsu2Enter
        OnExit = EdCodUsu2Exit
        OnKeyDown = EdCodUsu2KeyDown
      end
      object EdNome2: TdmkEdit
        Left = 96
        Top = 36
        Width = 458
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdNome2KeyDown
      end
      object StaticText2: TStaticText
        Left = 1
        Top = 1
        Width = 648
        Height = 17
        Align = alTop
        Alignment = taCenter
        BevelKind = bkSoft
        Caption = 'N'#205'VEL 2 (UM N'#205'VEL ACIMA DO GRUPO DE PRODUTOS)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdNivel2: TDBEdit
        Left = 556
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Nivel2'
        DataSource = DsGraGru2
        Enabled = False
        TabOrder = 3
      end
    end
    object PnNivel1: TPanel
      Left = 0
      Top = 240
      Width = 650
      Height = 104
      Align = alClient
      Enabled = False
      TabOrder = 4
      Visible = False
      object Label13: TLabel
        Left = 96
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label12: TLabel
        Left = 12
        Top = 20
        Width = 75
        Height = 13
        Caption = 'C'#243'digo [F3][F4]:'
        FocusControl = DBEdit1
      end
      object Label16: TLabel
        Left = 556
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdNivel1
      end
      object Label26: TLabel
        Left = 116
        Top = 60
        Width = 117
        Height = 13
        Caption = 'Unidade de Medida [F3]:'
      end
      object SBUnidMed: TSpeedButton
        Left = 568
        Top = 75
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SBUnidMedClick
      end
      object Label47: TLabel
        Left = 12
        Top = 60
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object SbNOME_NOVO_GG1: TSpeedButton
        Left = 516
        Top = 36
        Width = 39
        Height = 22
        Caption = 'ctrl+v'
        OnClick = SbNOME_NOVO_GG1Click
      end
      object EdCodUsu1: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodUsu1Change
        OnEnter = EdCodUsu1Enter
        OnExit = EdCodUsu1Exit
        OnKeyDown = EdCodUsu1KeyDown
      end
      object EdNome1: TdmkEdit
        Left = 96
        Top = 36
        Width = 417
        Height = 21
        MaxLength = 50
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdNome1KeyDown
      end
      object StaticText1: TStaticText
        Left = 1
        Top = 1
        Width = 648
        Height = 17
        Align = alTop
        Alignment = taCenter
        BevelKind = bkSoft
        Caption = 'N'#205'VEL 1 (GRUPO DE PRODUTOS)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdNivel1: TDBEdit
        Left = 556
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Nivel1'
        DataSource = DsGraGru1
        Enabled = False
        TabOrder = 3
      end
      object EdUnidMed: TdmkEditCB
        Left = 116
        Top = 76
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdUnidMedChange
        OnKeyDown = EdUnidMedKeyDown
        DBLookupComboBox = CBUnidMed
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdSigla: TdmkEdit
        Left = 172
        Top = 76
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdSiglaChange
        OnExit = EdSiglaExit
        OnKeyDown = EdSiglaKeyDown
      end
      object CBUnidMed: TdmkDBLookupComboBox
        Left = 212
        Top = 76
        Width = 353
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsUnidMed
        TabOrder = 7
        OnKeyDown = CBUnidMedKeyDown
        dmkEditCB = EdUnidMed
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNCM: TdmkEdit
        Left = 12
        Top = 76
        Width = 100
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NCM'
        UpdCampo = 'NCM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNCMRedefinido
      end
    end
    object PnNivel4: TPanel
      Left = 0
      Top = 60
      Width = 650
      Height = 60
      Align = alTop
      Enabled = False
      TabOrder = 1
      Visible = False
      object Label19: TLabel
        Left = 12
        Top = 20
        Width = 75
        Height = 13
        Caption = 'C'#243'digo [F3][F4]:'
        FocusControl = DBEdit1
      end
      object Label20: TLabel
        Left = 96
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label21: TLabel
        Left = 556
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdNivel4
      end
      object EdNome4: TdmkEdit
        Left = 96
        Top = 36
        Width = 458
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdNome4KeyDown
      end
      object StaticText4: TStaticText
        Left = 1
        Top = 1
        Width = 648
        Height = 17
        Align = alTop
        Alignment = taCenter
        BevelKind = bkSoft
        Caption = 'N'#205'VEL 4 (DOIS N'#205'VEIS ACIMA DO GRUPO DE PRODUTOS)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdNivel4: TDBEdit
        Left = 556
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Nivel4'
        DataSource = DsGraGru4
        Enabled = False
        TabOrder = 3
      end
      object EdCodUsu4: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodUsu4Change
        OnEnter = EdCodUsu4Enter
        OnExit = EdCodUsu4Exit
        OnKeyDown = EdCodUsu4KeyDown
      end
    end
    object PnNivel5: TPanel
      Left = 0
      Top = 0
      Width = 650
      Height = 60
      Align = alTop
      Enabled = False
      TabOrder = 0
      Visible = False
      object Label22: TLabel
        Left = 12
        Top = 20
        Width = 75
        Height = 13
        Caption = 'C'#243'digo [F3][F4]:'
        FocusControl = DBEdit1
      end
      object Label23: TLabel
        Left = 96
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label24: TLabel
        Left = 556
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdNivel5
      end
      object EdNome5: TdmkEdit
        Left = 96
        Top = 36
        Width = 458
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdNome5KeyDown
      end
      object StaticText5: TStaticText
        Left = 1
        Top = 1
        Width = 648
        Height = 17
        Align = alTop
        Alignment = taCenter
        BevelKind = bkSoft
        Caption = 'N'#205'VEL 5 (DOIS N'#205'VEIS ACIMA DO GRUPO DE PRODUTOS)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdNivel5: TDBEdit
        Left = 556
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Nivel5'
        DataSource = DsGraGru5
        Enabled = False
        TabOrder = 3
      end
      object EdCodUsu5: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodUsu5Change
        OnEnter = EdCodUsu5Enter
        OnExit = EdCodUsu5Exit
        OnKeyDown = EdCodUsu5KeyDown
      end
    end
  end
  object PnMudaTipo: TPanel
    Left = 0
    Top = 48
    Width = 650
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 7
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 108
      Height = 13
      Caption = 'Tipo de grupo produto:'
    end
    object EdPrdGruTip: TdmkEditCB
      Left = 12
      Top = 20
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'CodUsu'
      UpdCampo = 'CodUsu'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdPrdGruTipRedefinido
      DBLookupComboBox = CBPrdGruTip
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPrdGruTip: TdmkDBLookupComboBox
      Left = 96
      Top = 20
      Width = 540
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsPrdGrupTip
      TabOrder = 1
      dmkEditCB = EdPrdGruTip
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 650
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 602
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 1
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 2
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 554
      Height = 48
      Align = alClient
      TabOrder = 0
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 299
        Height = 32
        Caption = 'Seleciona Tipo de Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 32
        Caption = 'Seleciona Tipo de Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 299
        Height = 32
        Caption = 'Seleciona Tipo de Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 521
    Width = 650
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 646
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 565
    Width = 650
    Height = 64
    Align = alBottom
    TabOrder = 1
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 646
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label17: TLabel
        Left = 264
        Top = 5
        Width = 200
        Height = 18
        Caption = '[F3] Pesquisa item existente'
        FocusControl = DBEdit1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 264
        Top = 23
        Width = 231
        Height = 18
        Caption = '[F4] Gera c'#243'digo para novo item.'
        FocusControl = DBEdit1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PnSaiDesis: TPanel
        Left = 520
        Top = 0
        Width = 126
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 136
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtAlteraClick
      end
    end
  end
  object PnGraTabApp: TPanel
    Left = 0
    Top = 133
    Width = 650
    Height = 44
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Label25: TLabel
      Left = 12
      Top = 4
      Width = 172
      Height = 13
      Caption = 'Sele'#231#227'o de Agrega'#231#227'o de Produtos:'
    end
    object EdGraTabApp: TdmkEdit
      Left = 12
      Top = 20
      Width = 80
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdGraTabApp_TXT: TdmkEdit
      Left = 96
      Top = 20
      Width = 540
      Height = 21
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPrdGrupTipAfterScroll
    SQL.Strings = (
      'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, '
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'pgt.TitNiv1, pgt.TitNiv2, pgt.TitNiv3, pgt.TitNiv4, pgt.TitNiv5,'
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      
        'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO, pgt.Gradeado, pgt.U' +
        'saCores,'
      
        'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') NOM' +
        'E_TIPPRD'
      'FROM prdgruptip pgt'
      'ORDER BY pgt.Nome')
    Left = 8
    Top = 8
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipMadeBy: TSmallintField
      FieldName = 'MadeBy'
      Required = True
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
      Required = True
    end
    object QrPrdGrupTipTipPrd: TSmallintField
      FieldName = 'TipPrd'
      Required = True
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
      Required = True
    end
    object QrPrdGrupTipNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrPrdGrupTipNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrPrdGrupTipNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
    object QrPrdGrupTipFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
      Required = True
    end
    object QrPrdGrupTipFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
      Required = True
    end
    object QrPrdGrupTipGradeado: TSmallintField
      FieldName = 'Gradeado'
      Required = True
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrPrdGrupTipTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipUsaCores: TSmallintField
      FieldName = 'UsaCores'
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 36
    Top = 8
  end
  object QrGraGru5: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, gg5.CodUsu, gg5.Nivel5, gg5.Nome, '
      'IF(pgt.Nome <> "", pgt.Nome, "???") NOMEPGT'
      'FROM gragru5 gg5'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel5=gg5.Nivel5'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      ''
      ''
      'WHERE gg5.CodUsu=:P0')
    Left = 584
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru5PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru5.PrdGrupTip'
    end
    object QrGraGru5CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru5.CodUsu'
    end
    object QrGraGru5Nivel5: TIntegerField
      FieldName = 'Nivel5'
      Origin = 'gragru5.Nivel5'
    end
    object QrGraGru5Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru5.Nome'
      Size = 30
    end
    object QrGraGru5NOMEPGT: TWideStringField
      FieldName = 'NOMEPGT'
      Size = 30
    end
  end
  object DsGraGru5: TDataSource
    DataSet = QrGraGru5
    Left = 612
    Top = 172
  end
  object QrGraGru2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, gg2.CodUsu, gg2.Nivel2, '
      'gg2.Nome, gg2.Nivel3, gg3.Nome NOMENIVEL3'
      'FROM gragru2 gg2'
      'LEFT JOIN gragru3 gg3 ON gg2.Nivel3=gg3.Nivel3'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel2=gg2.Nivel2'
      'WHERE gg2.CodUsu=:P0')
    Left = 588
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru2CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru2.CodUsu'
    end
    object QrGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Origin = 'gragru2.Nivel2'
    end
    object QrGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru2.Nome'
      Size = 30
    end
    object QrGraGru2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrGraGru2Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Origin = 'gragru2.Nivel3'
      Required = True
    end
    object QrGraGru2NOMENIVEL3: TWideStringField
      FieldName = 'NOMENIVEL3'
      Origin = 'gragru3.Nome'
      Size = 30
    end
  end
  object DsGraGru2: TDataSource
    DataSet = QrGraGru2
    Left = 616
    Top = 360
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, gg1.CodUsu, gg1.Nivel1, '
      'gg1.Nome, gg1.Nivel2, gg2.Nome NOMENIVEL2'
      'FROM gragru1 gg1'
      'LEFT JOIN gragru2 gg2 ON gg1.Nivel2=gg2.Nivel2'
      'WHERE gg1.CodUsu=:P0')
    Left = 592
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
      Required = True
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru1.CodUsu'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Origin = 'gragru1.Nivel2'
      Required = True
    end
    object QrGraGru1NOMENIVEL2: TWideStringField
      FieldName = 'NOMENIVEL2'
      Origin = 'gragru2.Nome'
      Size = 30
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 620
    Top = 428
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 232
    Top = 360
    object AlteraCdigodoNivel5: TMenuItem
      Caption = 'Altera C'#243'digo do Nivel 5 '
      OnClick = AlteraCdigodoNivel5Click
    end
    object AlteraCdigodoNivel4: TMenuItem
      Caption = 'Altera C'#243'digo do Nivel 4'
      OnClick = AlteraCdigodoNivel4Click
    end
    object AlteraCdigodoNivel3: TMenuItem
      Caption = 'Altera C'#243'digo do Nivel 3 '
      OnClick = AlteraCdigodoNivel3Click
    end
    object AlteraCdigodoNivel2: TMenuItem
      Caption = 'Altera C'#243'digo do Nivel 2'
      OnClick = AlteraCdigodoNivel2Click
    end
    object AlteraCdigodoNivel1: TMenuItem
      Caption = 'Altera C'#243'digo do Nivel 1'
      OnClick = AlteraCdigodoNivel1Click
    end
  end
  object QrGraGru4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, gg4.CodUsu, gg4.Nivel4, '
      'gg4.Nome, gg4.Nivel5, gg5.Nome NOMENIVEL5'
      'FROM gragru4 gg4'
      'LEFT JOIN gragru5 gg5 ON gg4.Nivel5=gg5.Nivel5'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel4=gg4.Nivel4'
      'WHERE gg4.CodUsu=:P0'
      '')
    Left = 584
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru4PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrGraGru4CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru4.CodUsu'
    end
    object QrGraGru4Nivel4: TIntegerField
      FieldName = 'Nivel4'
      Origin = 'gragru4.Nivel4'
    end
    object QrGraGru4Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru4.Nome'
      Size = 30
    end
    object QrGraGru4Nivel5: TIntegerField
      FieldName = 'Nivel5'
      Origin = 'gragru4.Nivel5'
    end
    object QrGraGru4NOMENIVEL5: TWideStringField
      FieldName = 'NOMENIVEL5'
      Origin = 'gragru5.Nome'
      Size = 30
    end
  end
  object DsGraGru4: TDataSource
    DataSet = QrGraGru4
    Left = 612
    Top = 236
  end
  object QrGraGru3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, gg3.CodUsu, gg3.Nivel3, '
      'gg3.Nome, gg3.Nivel4, gg4.Nome NOMENIVEL4'
      'FROM gragru3 gg3'
      'LEFT JOIN gragru4 gg4 ON gg3.Nivel4=gg4.Nivel4'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel3=gg3.Nivel3'
      'WHERE gg3.CodUsu=:P0'
      '')
    Left = 580
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru3PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrGraGru3CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru3.CodUsu'
    end
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Origin = 'gragru3.Nivel3'
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru3.Nome'
      Size = 30
    end
    object QrGraGru3Nivel4: TIntegerField
      FieldName = 'Nivel4'
      Origin = 'gragru3.Nivel4'
    end
    object QrGraGru3NOMENIVEL4: TWideStringField
      FieldName = 'NOMENIVEL4'
      Origin = 'gragru4.Nome'
      Size = 30
    end
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 608
    Top = 304
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 388
    Top = 16
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 388
    Top = 64
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 452
    Top = 12
  end
end
