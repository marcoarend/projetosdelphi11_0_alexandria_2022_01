object FmGraGru1CorTam: TFmGraGru1CorTam
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-020 :: Cadastro de Produto'
  ClientHeight = 620
  ClientWidth = 1036
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1036
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 793
    object GB_R: TGroupBox
      Left = 988
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 745
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 940
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 697
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 251
        Height = 32
        Caption = 'Cadastro de Produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 251
        Height = 32
        Caption = 'Cadastro de Produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 251
        Height = 32
        Caption = 'Cadastro de Produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 481
    Height = 458
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 453
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 481
      Height = 65
      Align = alTop
      Caption = ' Produto em edi'#231#227'o: '
      TabOrder = 0
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 477
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 7
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label6: TLabel
          Left = 128
          Top = 7
          Width = 31
          Height = 13
          Caption = 'Nome:'
        end
        object Label7: TLabel
          Left = 68
          Top = 7
          Width = 49
          Height = 13
          Caption = 'ID N'#237'vel1:'
        end
        object EdCodUsu_Ant: TdmkEdit
          Left = 8
          Top = 23
          Width = 56
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome_Ant: TdmkEdit
          Left = 128
          Top = 23
          Width = 341
          Height = 21
          TabStop = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNivel1: TdmkEdit
          Left = 68
          Top = 23
          Width = 56
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 65
      Width = 481
      Height = 393
      Align = alClient
      TabOrder = 1
      ExplicitHeight = 388
      object Panel3: TPanel
        Left = 2
        Top = 61
        Width = 477
        Height = 336
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LaPrdGrupTip: TLabel
          Left = 12
          Top = 4
          Width = 131
          Height = 13
          Caption = 'Tipo de Grupo de Produtos:'
        end
        object LaNivel3: TLabel
          Left = 12
          Top = 124
          Width = 36
          Height = 13
          Caption = 'Nivel 3:'
          Enabled = False
        end
        object LaNivel2: TLabel
          Left = 12
          Top = 164
          Width = 36
          Height = 13
          Caption = 'Nivel 2:'
          Enabled = False
        end
        object LaNivel1: TLabel
          Left = 12
          Top = 204
          Width = 54
          Height = 13
          Caption = 'C'#243'digo:[F4]'
        end
        object LaNivel5: TLabel
          Left = 12
          Top = 44
          Width = 36
          Height = 13
          Caption = 'Nivel 5:'
          Enabled = False
        end
        object LaNivel4: TLabel
          Left = 12
          Top = 84
          Width = 36
          Height = 13
          Caption = 'Nivel 4:'
          Enabled = False
        end
        object SbNivel5: TSpeedButton
          Left = 448
          Top = 60
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel5Click
        end
        object SbNivel4: TSpeedButton
          Left = 448
          Top = 100
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel4Click
        end
        object SbNivel3: TSpeedButton
          Left = 448
          Top = 140
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel3Click
        end
        object SbNivel2: TSpeedButton
          Left = 448
          Top = 180
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel2Click
        end
        object LaGrade: TLabel
          Left = 12
          Top = 244
          Width = 96
          Height = 13
          Caption = 'Grade de tamanhos:'
          Enabled = False
        end
        object SBGrade: TSpeedButton
          Left = 448
          Top = 260
          Width = 23
          Height = 22
          Caption = '...'
          Enabled = False
          OnClick = SBGradeClick
        end
        object SbPrdgrupTip: TSpeedButton
          Left = 448
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbPrdgrupTipClick
        end
        object Label46: TLabel
          Left = 12
          Top = 283
          Width = 117
          Height = 13
          Caption = 'Unidade de Medida [F3]:'
        end
        object SBUnidMed: TSpeedButton
          Left = 448
          Top = 298
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SBUnidMedClick
        end
        object EdPrdGrupTip: TdmkEditCB
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPrdGrupTipChange
          DBLookupComboBox = CBPrdGrupTip
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPrdGrupTip: TdmkDBLookupComboBox
          Left = 68
          Top = 20
          Width = 373
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsPrdGrupTip
          TabOrder = 1
          OnClick = CBPrdGrupTipClick
          dmkEditCB = EdPrdGrupTip
          QryCampo = 'PrdGrupTip'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNivel3: TdmkEditCB
          Left = 12
          Top = 140
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel3Change
          DBLookupComboBox = CBNivel3
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel3: TdmkDBLookupComboBox
          Left = 68
          Top = 140
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru3
          TabOrder = 7
          dmkEditCB = EdNivel3
          QryCampo = 'Nivel3'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNivel2: TdmkEditCB
          Left = 12
          Top = 180
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel2Change
          DBLookupComboBox = CBNivel2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel2: TdmkDBLookupComboBox
          Left = 68
          Top = 180
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru2
          TabOrder = 9
          dmkEditCB = EdNivel2
          QryCampo = 'Nivel2'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCodUsu_New: TdmkEdit
          Left = 12
          Top = 220
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdCodUsu_NewKeyDown
        end
        object EdNome_New: TdmkEdit
          Left = 68
          Top = 220
          Width = 401
          Height = 21
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNivel5: TdmkEditCB
          Left = 12
          Top = 60
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel5Change
          DBLookupComboBox = CBNivel5
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel5: TdmkDBLookupComboBox
          Left = 66
          Top = 60
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru5
          TabOrder = 3
          dmkEditCB = EdNivel5
          QryCampo = 'Nivel5'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNivel4: TdmkEditCB
          Left = 12
          Top = 100
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel4Change
          DBLookupComboBox = CBNivel4
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel4: TdmkDBLookupComboBox
          Left = 66
          Top = 100
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru4
          TabOrder = 5
          dmkEditCB = EdNivel4
          QryCampo = 'Nivel4'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGrade: TdmkEditCB
          Left = 12
          Top = 260
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGrade
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGrade: TdmkDBLookupComboBox
          Left = 68
          Top = 260
          Width = 377
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraTamCad
          TabOrder = 13
          dmkEditCB = EdGrade
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdUnidMed: TdmkEditCB
          Left = 12
          Top = 299
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedChange
          OnKeyDown = EdUnidMedKeyDown
          DBLookupComboBox = CBUnidMed
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdSigla: TdmkEdit
          Left = 68
          Top = 299
          Width = 40
          Height = 21
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaChange
          OnExit = EdSiglaExit
          OnKeyDown = EdSiglaKeyDown
        end
        object CBUnidMed: TdmkDBLookupComboBox
          Left = 108
          Top = 299
          Width = 338
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMed
          TabOrder = 16
          dmkEditCB = EdUnidMed
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 477
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 4
          Width = 98
          Height = 13
          Caption = 'Tabela customizada:'
        end
        object EdGraTabApp: TdmkEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdCodUsu_NewKeyDown
        end
        object EdTXT_GraTabApp: TdmkEdit
          Left = 64
          Top = 20
          Width = 401
          Height = 21
          TabStop = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 550
    Width = 1036
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 545
    ExplicitWidth = 793
    object PnSaiDesis: TPanel
      Left = 890
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 647
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 888
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 645
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkRespeitarPai: TCheckBox
        Left = 152
        Top = 4
        Width = 161
        Height = 17
        Caption = 'Respeitar filia'#231#227'o dos n'#237'veis.'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CkRespeitarPaiClick
      end
      object DBCkGradeado: TDBCheckBox
        Left = 152
        Top = 27
        Width = 179
        Height = 17
        Caption = 'N'#227'o criar reduzido em inclus'#227'o.'
        DataField = 'Gradeado'
        DataSource = DsPrdGrupTip
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object BitBtn1: TBitBtn
        Tag = 58
        Left = 336
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Grade'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
      object BtCor: TBitBtn
        Tag = 29
        Left = 460
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Cor'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtCorClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 506
    Width = 1036
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 501
    ExplicitWidth = 793
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1032
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 789
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBCores: TGroupBox
    Left = 481
    Top = 48
    Width = 555
    Height = 458
    Align = alClient
    Caption = ' Cores: '
    Enabled = False
    TabOrder = 4
    ExplicitWidth = 312
    ExplicitHeight = 453
    object Panel7: TPanel
      Left = 305
      Top = 15
      Width = 248
      Height = 441
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 5
      ExplicitHeight = 436
      object Label1: TLabel
        Left = 0
        Top = 13
        Width = 248
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Duplo clique elimina item da lista!'
        ExplicitWidth = 155
      end
      object Label4: TLabel
        Left = 0
        Top = 0
        Width = 248
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Duplo clique elimina item da lista!'
        ExplicitWidth = 155
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 26
        Width = 248
        Height = 415
        Align = alClient
        DataSource = DsSelGCC
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 149
            Visible = True
          end>
      end
    end
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 303
      Height = 441
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitHeight = 436
      object DBGCores: TdmkDBGridZTO
        Left = 0
        Top = 41
        Width = 303
        Height = 400
        Align = alClient
        DataSource = DsGraCorCad
        Enabled = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnDblClick = DBGCoresDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end>
      end
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 303
        Height = 41
        Align = alTop
        TabOrder = 1
        object Label3: TLabel
          Left = 1
          Top = 27
          Width = 301
          Height = 13
          Align = alBottom
          Alignment = taCenter
          Caption = 'Duplo clique adiciona cor '#224' lista ao lado  --->>'
          ExplicitTop = 43
          ExplicitWidth = 215
        end
        object CkFiltrar: TCheckBox
          Left = 8
          Top = 4
          Width = 53
          Height = 17
          Caption = 'Filtrar:'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = CkFiltrarClick
        end
        object EdFiltro: TEdit
          Left = 60
          Top = 4
          Width = 221
          Height = 21
          TabOrder = 1
          Text = '%%'
          OnChange = EdFiltroChange
        end
      end
    end
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, '
      'Nome, NivCad, Gradeado,'
      'TitNiv1, TitNiv2, TitNiv3,'
      'TitNiv4, TitNiv5'
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 516
    Top = 108
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
    end
    object QrPrdGrupTipGradeado: TSmallintField
      FieldName = 'Gradeado'
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrPrdGrupTipTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Size = 15
    end
    object QrPrdGrupTipTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Size = 15
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 516
    Top = 156
  end
  object QrGraGru3: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru3BeforeClose
    SQL.Strings = (
      'SELECT Nivel3, CodUsu, Nome'
      'FROM gragru3'
      'WHERE Nivel4=0'
      'OR Nivel4=:P0'
      'ORDER BY Nome')
    Left = 516
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGru3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 516
    Top = 444
  end
  object QrGraGru2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru2BeforeClose
    SQL.Strings = (
      'SELECT Nivel2, CodUsu, Nome'
      'FROM gragru2'
      'WHERE Nivel3=0'
      'OR Nivel3=:P0'
      'ORDER BY Nome')
    Left = 600
    Top = 108
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGru2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru2: TDataSource
    DataSet = QrGraGru2
    Left = 600
    Top = 156
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome'
      'FROM GraGru1'
      'WHERE Nivel2=:P0'
      'ORDER BY Nome')
    Left = 600
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 600
    Top = 252
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu '
      'FROM gragru1'
      'WHERE CodUsu=:P0')
    Left = 600
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrGraGru5: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru3BeforeClose
    SQL.Strings = (
      'SELECT Nivel5, CodUsu, Nome'
      'FROM gragru5'
      'ORDER BY Nome')
    Left = 516
    Top = 204
    object QrGraGru5Nivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGru5CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru5: TDataSource
    DataSet = QrGraGru5
    Left = 516
    Top = 252
  end
  object QrGraGru4: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru2BeforeClose
    SQL.Strings = (
      'SELECT Nivel4, CodUsu, Nome'
      'FROM gragru4'
      'WHERE Nivel5=0'
      'OR Nivel5=:P0'
      'ORDER BY Nome')
    Left = 516
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru4Nivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGru4CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru4: TDataSource
    DataSet = QrGraGru4
    Left = 516
    Top = 348
  end
  object PMInsAlt: TPopupMenu
    Left = 388
    Top = 336
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = Altera1Click
    end
  end
  object QrGraTamCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gratamcad '
      'ORDER BY Nome')
    Left = 684
    Top = 200
    object QrGraTamCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraTamCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraTamCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraTamCad: TDataSource
    DataSet = QrGraTamCad
    Left = 684
    Top = 244
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdGrade
    QryCampo = 'GraTamCad'
    UpdCampo = 'GraTamCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM gracorcad'
      'ORDER BY Nome'
      '')
    Left = 684
    Top = 108
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 685
    Top = 156
  end
  object QrGraTamIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 684
    Top = 292
    object QrGraTamItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrGG1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 616
    Top = 388
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 688
    Top = 364
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 688
    Top = 412
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 432
    Top = 24
  end
  object QrSelGCC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM gracorcad'
      'ORDER BY Nome'
      '')
    Left = 872
    Top = 116
    object QrSelGCCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSelGCCNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsSelGCC: TDataSource
    DataSet = QrSelGCC
    Left = 873
    Top = 164
  end
end
