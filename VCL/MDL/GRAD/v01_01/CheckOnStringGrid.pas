unit CheckOnStringGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, dmkImage, UnDmkEnums;

type
  TFmCheckOnStringGrid = class(TForm)
    Panel1: TPanel;
    Grade: TStringGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeDblClick(Sender: TObject);
    procedure GradeClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FColSel: Integer;
    FResult: String;
  end;

  var
  FmCheckOnStringGrid: TFmCheckOnStringGrid;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmCheckOnStringGrid.BtOKClick(Sender: TObject);
begin
  if FResult <> '' then
    Close
  else
    Application.MessageBox('Nenhum item foi selecionado!', 'Avido',
    MB_OK+MB_ICONWARNING);  
end;

procedure TFmCheckOnStringGrid.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCheckOnStringGrid.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCheckOnStringGrid.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Grade.ColWidths[1] := 18;
  Grade.Cells[01,00] := 'Sel';
  FResult := '';
end;

procedure TFmCheckOnStringGrid.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCheckOnStringGrid.GradeClick(Sender: TObject);
begin
  if (Grade.Col <> 0) and (Grade.Row <> 0) then
    FResult := Grade.Cells[FColSel, Grade.Row];
end;

procedure TFmCheckOnStringGrid.GradeDblClick(Sender: TObject);
begin
  if (Grade.Col <> 0) and (Grade.Row <> 0) then
    Close;
end;

end.
