object FmGraGruXEdit: TFmGraGruXEdit
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-039 :: Cadastro de Reduzido'
  ClientHeight = 701
  ClientWidth = 485
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 485
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 437
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 389
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 271
        Height = 32
        Caption = 'Cadastro de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 271
        Height = 32
        Caption = 'Cadastro de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 271
        Height = 32
        Caption = 'Cadastro de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 485
    Height = 539
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 485
      Height = 109
      Align = alTop
      Caption = ' Produto em edi'#231#227'o: '
      TabOrder = 0
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 481
        Height = 92
        Align = alClient
        BevelOuter = bvNone
        FullRepaint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 7
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label6: TLabel
          Left = 128
          Top = 7
          Width = 31
          Height = 13
          Caption = 'Nome:'
        end
        object Label7: TLabel
          Left = 68
          Top = 7
          Width = 49
          Height = 13
          Caption = 'ID N'#237'vel1:'
        end
        object LaNivel1: TLabel
          Left = 8
          Top = 48
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 24
          Width = 56
          Height = 21
          DataField = 'GraGru1'
          DataSource = DsGraGruX
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 68
          Top = 24
          Width = 56
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 128
          Top = 24
          Width = 340
          Height = 21
          DataField = 'Nome'
          DataSource = DsGraGruX
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 64
          Width = 56
          Height = 21
          DataField = 'Controle'
          DataSource = DsGraGruX
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 64
          Top = 64
          Width = 404
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsGraGruX
          TabOrder = 4
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 109
      Width = 485
      Height = 430
      Align = alClient
      TabOrder = 1
      object Label3: TLabel
        Left = 16
        Top = 344
        Width = 129
        Height = 13
        Caption = 'C'#243'digo do item SPED EFD:'
      end
      object Label4: TLabel
        Left = 16
        Top = 384
        Width = 373
        Height = 13
        Caption = 
          'C'#243'digo de Barras espec'#237'fico deste reduzido, pr'#243'prio ou de tercei' +
          'ros (NFe I03a):'
      end
      object Panel3: TPanel
        Left = 2
        Top = 61
        Width = 481
        Height = 286
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LaPrdGrupTip: TLabel
          Left = 12
          Top = 4
          Width = 131
          Height = 13
          Caption = 'Tipo de Grupo de Produtos:'
        end
        object LaNivel3: TLabel
          Left = 12
          Top = 124
          Width = 36
          Height = 13
          Caption = 'Nivel 3:'
          Enabled = False
        end
        object LaNivel2: TLabel
          Left = 12
          Top = 164
          Width = 36
          Height = 13
          Caption = 'Nivel 2:'
          Enabled = False
        end
        object LaNivel5: TLabel
          Left = 12
          Top = 44
          Width = 36
          Height = 13
          Caption = 'Nivel 5:'
          Enabled = False
        end
        object LaNivel4: TLabel
          Left = 12
          Top = 84
          Width = 36
          Height = 13
          Caption = 'Nivel 4:'
          Enabled = False
        end
        object SbNivel5: TSpeedButton
          Left = 448
          Top = 60
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel5Click
        end
        object SbNivel4: TSpeedButton
          Left = 448
          Top = 100
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel4Click
        end
        object SbNivel3: TSpeedButton
          Left = 448
          Top = 140
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel3Click
        end
        object SbNivel2: TSpeedButton
          Left = 448
          Top = 180
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
          OnClick = SbNivel2Click
        end
        object LaGrade: TLabel
          Left = 12
          Top = 204
          Width = 84
          Height = 13
          Caption = 'Grade / tamanho:'
        end
        object SBGrade: TSpeedButton
          Left = 448
          Top = 220
          Width = 21
          Height = 21
          Caption = '...'
          Visible = False
        end
        object Label1: TLabel
          Left = 12
          Top = 244
          Width = 19
          Height = 13
          Caption = 'Cor:'
        end
        object SpeedButton1: TSpeedButton
          Left = 448
          Top = 260
          Width = 21
          Height = 21
          Caption = '...'
          Visible = False
        end
        object EdPrdGrupTip: TdmkEditCB
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPrdGrupTipChange
          DBLookupComboBox = CBPrdGrupTip
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPrdGrupTip: TdmkDBLookupComboBox
          Left = 68
          Top = 20
          Width = 400
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsPrdGrupTip
          TabOrder = 1
          OnClick = CBPrdGrupTipClick
          dmkEditCB = EdPrdGrupTip
          QryCampo = 'PrdGrupTip'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNivel3: TdmkEditCB
          Left = 12
          Top = 140
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel3Change
          DBLookupComboBox = CBNivel3
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel3: TdmkDBLookupComboBox
          Left = 68
          Top = 140
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru3
          TabOrder = 7
          dmkEditCB = EdNivel3
          QryCampo = 'Nivel3'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNivel2: TdmkEditCB
          Left = 12
          Top = 180
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel2Change
          DBLookupComboBox = CBNivel2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel2: TdmkDBLookupComboBox
          Left = 68
          Top = 180
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru2
          TabOrder = 9
          dmkEditCB = EdNivel2
          QryCampo = 'Nivel2'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNivel5: TdmkEditCB
          Left = 12
          Top = 60
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel5Change
          DBLookupComboBox = CBNivel5
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel5: TdmkDBLookupComboBox
          Left = 66
          Top = 60
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru5
          TabOrder = 3
          dmkEditCB = EdNivel5
          QryCampo = 'Nivel5'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNivel4: TdmkEditCB
          Left = 12
          Top = 100
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNivel4Change
          DBLookupComboBox = CBNivel4
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel4: TdmkDBLookupComboBox
          Left = 66
          Top = 100
          Width = 376
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru4
          TabOrder = 5
          dmkEditCB = EdNivel4
          QryCampo = 'Nivel4'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraTamIts: TdmkEditCB
          Left = 12
          Top = 220
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGraTamIts
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraTamIts: TdmkDBLookupComboBox
          Left = 68
          Top = 220
          Width = 376
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_GRADE_ITEM'
          ListSource = DsTamanho
          TabOrder = 11
          dmkEditCB = EdGraTamIts
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraCorCad: TdmkEditCB
          Left = 12
          Top = 260
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGraCorCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraCorCad: TdmkDBLookupComboBox
          Left = 68
          Top = 260
          Width = 376
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsGraCorCad
          TabOrder = 13
          dmkEditCB = EdGraCorCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 481
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 4
          Width = 98
          Height = 13
          Caption = 'Tabela customizada:'
        end
        object EdGraGruY: TdmkEditCB
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPrdGrupTipChange
          DBLookupComboBox = CBGraGruY
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGruY: TdmkDBLookupComboBox
          Left = 68
          Top = 20
          Width = 400
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsGraGruY
          TabOrder = 1
          OnClick = CBPrdGrupTipClick
          dmkEditCB = EdGraGruY
          QryCampo = 'PrdGrupTip'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object EdCOD_ITEM: TdmkEdit
        Left = 16
        Top = 360
        Width = 453
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'COD_ITEM'
        UpdCampo = 'COD_ITEM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edprod_cBarra: TdmkEdit
        Left = 16
        Top = 400
        Width = 453
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'prod_cBarra'
        UpdCampo = 'prod_cBarra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 631
    Width = 485
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 339
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 337
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkRespeitarPai: TCheckBox
        Left = 152
        Top = 4
        Width = 161
        Height = 17
        Caption = 'Respeitar filia'#231#227'o dos n'#237'veis.'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CkRespeitarPaiClick
      end
      object DBCkGradeado: TDBCheckBox
        Left = 152
        Top = 27
        Width = 165
        Height = 17
        Caption = 'N'#227'o criar reduzido em inclus'#227'o.'
        DataField = 'Gradeado'
        DataSource = DsPrdGrupTip
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 587
    Width = 485
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 481
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, '
      'Nome, NivCad, Gradeado,'
      'TitNiv1, TitNiv2, TitNiv3,'
      'TitNiv4, TitNiv5'
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 136
    Top = 88
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
    end
    object QrPrdGrupTipGradeado: TSmallintField
      FieldName = 'Gradeado'
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrPrdGrupTipTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Size = 15
    end
    object QrPrdGrupTipTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Size = 15
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 136
    Top = 136
  end
  object QrGraGru3: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru3BeforeClose
    SQL.Strings = (
      'SELECT Nivel3, CodUsu, Nome'
      'FROM gragru3'
      'WHERE Nivel4=0'
      'OR Nivel4=:P0'
      'ORDER BY Nome')
    Left = 204
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGru3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 204
    Top = 308
  end
  object QrGraGru2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru2BeforeClose
    SQL.Strings = (
      'SELECT Nivel2, CodUsu, Nome'
      'FROM gragru2'
      'WHERE Nivel3=0'
      'OR Nivel3=:P0'
      'ORDER BY Nome')
    Left = 200
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGru2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru2: TDataSource
    DataSet = QrGraGru2
    Left = 200
    Top = 396
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome'
      'FROM GraGru1'
      'WHERE Nivel2=:P0'
      'ORDER BY Nome')
    Left = 200
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 196
    Top = 484
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu '
      'FROM gragru1'
      'WHERE CodUsu=:P0')
    Left = 448
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrGraGru5: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru3BeforeClose
    SQL.Strings = (
      'SELECT Nivel5, CodUsu, Nome'
      'FROM gragru5'
      'ORDER BY Nome')
    Left = 204
    Top = 88
    object QrGraGru5Nivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGru5CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru5: TDataSource
    DataSet = QrGraGru5
    Left = 204
    Top = 132
  end
  object QrGraGru4: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru2BeforeClose
    SQL.Strings = (
      'SELECT Nivel4, CodUsu, Nome'
      'FROM gragru4'
      'WHERE Nivel5=0'
      'OR Nivel5=:P0'
      'ORDER BY Nome')
    Left = 200
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru4Nivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGru4CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru4: TDataSource
    DataSet = QrGraGru4
    Left = 200
    Top = 220
  end
  object PMInsAlt: TPopupMenu
    Left = 388
    Top = 336
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = Altera1Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, '
      'gg1.Nivel5, gg1.Nivel4, gg1.Nivel3, gg1.Nivel2,'
      'gg1.Nivel1, gg1.Nome, gg1.CodUsu, ggx.* '
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ')
    Left = 276
    Top = 88
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGruXNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGruXNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGruXNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruXNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGraGruXCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraGruXGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrGraGruXGraTamIts: TIntegerField
      FieldName = 'GraTamIts'
    end
    object QrGraGruXCOD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrGraGruXprod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 276
    Top = 136
  end
  object QrGraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggy.* '
      'FROM gragruy ggy'
      'WHERE ggy.Codigo > 0'
      '')
    Left = 276
    Top = 181
    object QrGraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object QrGraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrGraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrGraGruYLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruYDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruYDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruYUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruYUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruYAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruYAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsGraGruY: TDataSource
    DataSet = QrGraGruY
    Left = 276
    Top = 225
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM gracorcad'
      'ORDER BY Nome'
      '')
    Left = 348
    Top = 88
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 349
    Top = 136
  end
  object QrTamanho: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gtc.Codigo, gti.Controle,  '
      'CONCAT(gtc.Nome, " ",  gti.Nome) NO_GRADE_ITEM '
      'FROM gratamits gti  '
      'LEFT JOIN gratamcad gtc ON gti.Codigo=gtc.Codigo '
      'ORDER BY NO_GRADE_ITEM ')
    Left = 348
    Top = 180
    object QrTamanhoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTamanhoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTamanhoNO_GRADE_ITEM: TWideStringField
      FieldName = 'NO_GRADE_ITEM'
      Size = 56
    end
  end
  object DsTamanho: TDataSource
    DataSet = QrTamanho
    Left = 348
    Top = 224
  end
end
