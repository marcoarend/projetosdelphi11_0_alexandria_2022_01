unit GraImpLista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, DB, mySQLDbTables,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, Variants, Menus, DmkDAC_PF,
  UnDmkProcFunc, dmkImage, UnDmkEnums, dmkDBGridZTO, dmkLabel, dmkValUsu,
  UnProjGroup_Consts, UMySQLDB;

type
  TFmGraImpLista = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    QrStqMovIts: TmySQLQuery;
    DsStqMovIts: TDataSource;
    QrPrdMov: TmySQLQuery;
    DsPrdMov: TDataSource;
    QrPrdMovNivel1: TIntegerField;
    QrPrdMovNO_PRD: TWideStringField;
    QrPrdMovPrdGrupTip: TIntegerField;
    QrPrdMovUnidMed: TIntegerField;
    QrPrdMovNO_PGT: TWideStringField;
    QrPrdMovSIGLA: TWideStringField;
    QrPrdMovNO_TAM: TWideStringField;
    QrPrdMovNO_COR: TWideStringField;
    QrPrdMovGraCorCad: TIntegerField;
    QrPrdMovGraGruC: TIntegerField;
    QrPrdMovGraGru1: TIntegerField;
    QrPrdMovGraTamI: TIntegerField;
    QrPrdMovDataHora: TDateTimeField;
    QrPrdMovIDCtrl: TIntegerField;
    QrPrdMovTipo: TSmallintField;
    QrPrdMovOriCodi: TIntegerField;
    QrPrdMovOriCtrl: TIntegerField;
    QrPrdMovOriCnta: TIntegerField;
    QrPrdMovOriPart: TIntegerField;
    QrPrdMovEmpresa: TIntegerField;
    QrPrdMovStqCenCad: TIntegerField;
    QrPrdMovGraGruX: TIntegerField;
    QrPrdMovQtde: TFloatField;
    QrPrdMovPecas: TFloatField;
    QrPrdMovPeso: TFloatField;
    QrPrdMovAtivo: TSmallintField;
    QrPrdMovAreaM2: TFloatField;
    QrPrdMovAreaP2: TFloatField;
    QrPrdMovFatorClas: TFloatField;
    QrPrdMovSdoPecas: TFloatField;
    QrPrdMovSdoPeso: TFloatField;
    QrPrdMovSdoAreaM2: TFloatField;
    QrPrdMovSdoAreaP2: TFloatField;
    QrPrdMovSdoQtde: TFloatField;
    frxPRD_PRINT_001_01: TfrxReport;
    frxDsPrdMov: TfrxDBDataset;
    QrPrdMovNO_TIPO: TWideStringField;
    Panel4: TPanel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    PageControl1: TPageControl;
    TabSheet12: TTabSheet;
    TabSheet20: TTabSheet;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPDataIni1: TdmkEditDateTimePicker;
    TPDataFim1: TdmkEditDateTimePicker;
    Panel6: TPanel;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    DBGPrdMov: TdmkDBGrid;
    PainelConfirma: TPanel;
    LaAviso3A: TLabel;
    BtPesquisa1: TBitBtn;
    Panel2: TPanel;
    BtSaida1: TBitBtn;
    PB1: TProgressBar;
    BtImprime1: TBitBtn;
    PnDados0: TPanel;
    QrPrdEstq: TmySQLQuery;
    DsPrdEstq: TDataSource;
    DBGPrdEstq: TdmkDBGridZTO;
    QrPrdEstqNivel1: TIntegerField;
    QrPrdEstqNO_PRD: TWideStringField;
    QrPrdEstqPrdGrupTip: TIntegerField;
    QrPrdEstqUnidMed: TIntegerField;
    QrPrdEstqNO_PGT: TWideStringField;
    QrPrdEstqSIGLA: TWideStringField;
    QrPrdEstqNO_TAM: TWideStringField;
    QrPrdEstqNO_COR: TWideStringField;
    QrPrdEstqGraCorCad: TIntegerField;
    QrPrdEstqGraGruC: TIntegerField;
    QrPrdEstqGraGru1: TIntegerField;
    QrPrdEstqGraTamI: TIntegerField;
    QrPrdEstqQTDE: TFloatField;
    QrPrdEstqPECAS: TFloatField;
    QrPrdEstqPESO: TFloatField;
    QrPrdEstqAREAM2: TFloatField;
    QrPrdEstqAREAP2: TFloatField;
    QrPrdEstqGraGruX: TIntegerField;
    frxPRD_PRINT_001_00E_1: TfrxReport;
    frxDsPrdEstq: TfrxDBDataset;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    QrPrdEstqNCM: TWideStringField;
    Panel8: TPanel;
    Panel10: TPanel;
    QrGGX2: TmySQLQuery;
    QrSCC2: TmySQLQuery;
    QrSMIA2: TmySQLQuery;
    QrGGX2Controle: TIntegerField;
    QrSCC2Codigo: TIntegerField;
    QrSMIA2Ini: TDateTimeField;
    QrSMIA2Fim: TDateTimeField;
    QrSMIA2Itens: TLargeintField;
    QrSumA2: TmySQLQuery;
    QrSumA2Nivel1: TIntegerField;
    QrSumA2NO_PRD: TWideStringField;
    QrSumA2PrdGrupTip: TIntegerField;
    QrSumA2UnidMed: TIntegerField;
    QrSumA2NO_PGT: TWideStringField;
    QrSumA2SIGLA: TWideStringField;
    QrSumA2NO_TAM: TWideStringField;
    QrSumA2NO_COR: TWideStringField;
    QrSumA2GraCorCad: TIntegerField;
    QrSumA2GraGruC: TIntegerField;
    QrSumA2GraGru1: TIntegerField;
    QrSumA2GraTamI: TIntegerField;
    QrSumA2QTDE: TFloatField;
    QrSumA2PECAS: TFloatField;
    QrSumA2PESO: TFloatField;
    QrSumA2AREAM2: TFloatField;
    QrSumA2AREAP2: TFloatField;
    QrSumA2GraGruX: TIntegerField;
    QrSumA2NCM: TWideStringField;
    QrGSS: TmySQLQuery;
    DsGSS: TDataSource;
    frxDsGSS: TfrxDBDataset;
    QrGSSNivel1: TIntegerField;
    QrGSSNO_PRD: TWideStringField;
    QrGSSPrdGrupTip: TIntegerField;
    QrGSSUnidMed: TIntegerField;
    QrGSSNO_PGT: TWideStringField;
    QrGSSSIGLA: TWideStringField;
    QrGSSNO_TAM: TWideStringField;
    QrGSSNO_COR: TWideStringField;
    QrGSSGraCorCad: TIntegerField;
    QrGSSGraGruC: TIntegerField;
    QrGSSGraGru1: TIntegerField;
    QrGSSGraTamI: TIntegerField;
    QrGSSQTDE: TFloatField;
    QrGSSPECAS: TFloatField;
    QrGSSPESO: TFloatField;
    QrGSSAREAM2: TFloatField;
    QrGSSAREAP2: TFloatField;
    QrGSSGraGruX: TIntegerField;
    QrGSSNCM: TWideStringField;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    Panel13: TPanel;
    Panel11: TPanel;
    LaAviso2A: TLabel;
    BtPesq2: TBitBtn;
    Panel12: TPanel;
    BitBtn2: TBitBtn;
    PB2: TProgressBar;
    BtImprime2: TBitBtn;
    BtExporta2: TBitBtn;
    Panel14: TPanel;
    Panel15: TPanel;
    QrSMIA3: TmySQLQuery;
    Panel16: TPanel;
    Label8: TLabel;
    BtPesquisa3: TBitBtn;
    Panel17: TPanel;
    BitBtn4: TBitBtn;
    ProgressBar1: TProgressBar;
    BtImprime3: TBitBtn;
    dmkDBGrid2: TdmkDBGrid;
    DsSMIA3: TDataSource;
    frxPRD_PRINT_001_03: TfrxReport;
    frxDsSMIA3: TfrxDBDataset;
    QrSMIA3Nivel1: TIntegerField;
    QrSMIA3NO_PRD: TWideStringField;
    QrSMIA3PrdGrupTip: TIntegerField;
    QrSMIA3UnidMed: TIntegerField;
    QrSMIA3NO_PGT: TWideStringField;
    QrSMIA3SIGLA: TWideStringField;
    QrSMIA3NO_TAM: TWideStringField;
    QrSMIA3NO_COR: TWideStringField;
    QrSMIA3GraCorCad: TIntegerField;
    QrSMIA3GraGruC: TIntegerField;
    QrSMIA3GraGru1: TIntegerField;
    QrSMIA3GraTamI: TIntegerField;
    QrSMIA3IDCtrl: TIntegerField;
    QrSMIA3Tipo: TIntegerField;
    QrSMIA3OriCodi: TIntegerField;
    QrSMIA3OriCtrl: TIntegerField;
    QrSMIA3OriCnta: TIntegerField;
    QrSMIA3OriPart: TIntegerField;
    QrSMIA3Empresa: TIntegerField;
    QrSMIA3StqCenCad: TIntegerField;
    QrSMIA3GraGruX: TIntegerField;
    QrSMIA3Pecas: TFloatField;
    QrSMIA3Peso: TFloatField;
    QrSMIA3AreaM2: TFloatField;
    QrSMIA3AreaP2: TFloatField;
    QrSMIA3Qtde: TFloatField;
    QrSMIA3FatorClas: TFloatField;
    QrSMIA3Ativo: TSmallintField;
    QrSMIA3DataHora: TDateTimeField;
    RGSMI: TRadioGroup;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    TPDataIni3: TdmkEditDateTimePicker;
    TPDataFim3: TdmkEditDateTimePicker;
    BitBtn3: TBitBtn;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Panel19: TPanel;
    QrCPR: TmySQLQuery;
    DsCPR: TDataSource;
    QrCPRNivel1: TIntegerField;
    QrCPRNO_PRD: TWideStringField;
    QrCPRPrdGrupTip: TIntegerField;
    QrCPRUnidMed: TIntegerField;
    QrCPRNO_PGT: TWideStringField;
    QrCPRSIGLA: TWideStringField;
    QrCPRNO_TAM: TWideStringField;
    QrCPRNO_COR: TWideStringField;
    QrCPRGraCorCad: TIntegerField;
    QrCPRGraGruC: TIntegerField;
    QrCPRGraGru1: TIntegerField;
    QrCPRGraTamI: TIntegerField;
    QrCPRQTDE: TFloatField;
    QrCPRPECAS: TFloatField;
    QrCPRPESO: TFloatField;
    QrCPRAREAM2: TFloatField;
    QrCPRAREAP2: TFloatField;
    QrCPRGraGruX: TIntegerField;
    QrCPRNCM: TWideStringField;
    QrGG2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField2: TWideStringField;
    StringField3: TWideStringField;
    StringField4: TWideStringField;
    StringField5: TWideStringField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    IntegerField8: TIntegerField;
    StringField6: TWideStringField;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadEntiSitio: TIntegerField;
    QrStqCenCadSitProd: TSmallintField;
    QrStqCenCadCNPJ: TWideStringField;
    QrStqCenCadIE: TWideStringField;
    QrGSSStqCenCad: TIntegerField;
    QrStqCenCadNO_UF: TWideStringField;
    QrGraCusPrc: TmySQLQuery;
    DsGraCusPrc: TDataSource;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    QrGraCusPrcCusPrc: TWideStringField;
    QrGraCusPrcNomeTC: TWideStringField;
    QrGraCusPrcNOMEMOEDA: TWideStringField;
    QrGraCusPrcSIGLAMOEDA: TWideStringField;
    QrPrdEstqPrcCusUni: TFloatField;
    QrPrdEstqValorTot: TFloatField;
    QrPGT_SCC: TmySQLQuery;
    QrPGT_SCCPrdGrupTip: TIntegerField;
    QrPGT_SCCStqCenCad: TIntegerField;
    QrAbertura2a: TmySQLQuery;
    QrAbertura2aAbertura: TDateTimeField;
    QrAbertura2aGrupoBal: TIntegerField;
    QrSMIC2: TmySQLQuery;
    CGAgrup2: TdmkCheckGroup;
    QrSMIC2Nivel1: TIntegerField;
    QrSMIC2NO_PRD: TWideStringField;
    QrSMIC2PrdGrupTip: TIntegerField;
    QrSMIC2UnidMed: TIntegerField;
    QrSMIC2NCM: TWideStringField;
    QrSMIC2NO_PGT: TWideStringField;
    QrSMIC2SIGLA: TWideStringField;
    QrSMIC2NO_TAM: TWideStringField;
    QrSMIC2NO_COR: TWideStringField;
    QrSMIC2GraCorCad: TIntegerField;
    QrSMIC2GraGruC: TIntegerField;
    QrSMIC2GraGru1: TIntegerField;
    QrSMIC2GraTamI: TIntegerField;
    QrSMIC2Empresa: TIntegerField;
    QrSMIC2StqCenCad: TIntegerField;
    QrSMIC2Qtde: TFloatField;
    QrSMIC2SitProd: TSmallintField;
    QrSMIC2ENS_CNPJ: TWideStringField;
    QrSMIC2ENS_IE: TWideStringField;
    QrSMIC2ENS_NO_UF: TWideStringField;
    QrSMIC2EMP_CNPJ: TWideStringField;
    QrSMIC2EMP_IE: TWideStringField;
    QrSMIC2EMP_NO_UF: TWideStringField;
    DsSMIC2: TDataSource;
    PnGraCusPrc: TPanel;
    Label7: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    QrSMIC2PECAS: TFloatField;
    QrSMIC2PESO: TFloatField;
    QrSMIC2AREAM2: TFloatField;
    QrSMIC2AREAP2: TFloatField;
    QrSMIC2PrcCusUni: TFloatField;
    QrSMIC2GraGruX: TIntegerField;
    QrSMIC2ValorTot: TFloatField;
    QrPGT_SCCLstPrcFisc: TSmallintField;
    QrPGT_SCCEmpresa: TLargeintField;
    QrPGT_SCCEntiSitio: TLargeintField;
    QrAbertura2b: TmySQLQuery;
    QrAbertura2bAbertura: TDateTimeField;
    DsPGT_SCC: TDataSource;
    DBGrid1: TDBGrid;
    frxPRD_PRINT_001_02_A: TfrxReport;
    frxDs: TfrxDBDataset;
    PageControl3: TPageControl;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    MeExp: TMemo;
    dmkDBGrid1: TdmkDBGrid;
    TabSheet11: TTabSheet;
    QrExport2: TmySQLQuery;
    DsExport2: TDataSource;
    dmkDBGrid4: TdmkDBGrid;
    QrListErr1: TmySQLQuery;
    QrListErr1My_Idx: TIntegerField;
    QrListErr1Codigo: TWideStringField;
    QrListErr1Texto1: TWideStringField;
    DsListErr1: TDataSource;
    DBGrid2: TDBGrid;
    QrSMIC2My_Idx: TIntegerField;
    QrSMIC2PrintTam: TSmallintField;
    QrSMIC2PrintCor: TSmallintField;
    QrSMIC2EntiSitio: TIntegerField;
    QrSMIC2Exportado: TLargeintField;
    QrExport2My_Idx: TIntegerField;
    QrExport2Nivel1: TIntegerField;
    QrExport2NO_PRD: TWideStringField;
    QrExport2PrdGrupTip: TIntegerField;
    QrExport2UnidMed: TIntegerField;
    QrExport2NCM: TWideStringField;
    QrExport2NO_PGT: TWideStringField;
    QrExport2SIGLA: TWideStringField;
    QrExport2PrintTam: TSmallintField;
    QrExport2NO_TAM: TWideStringField;
    QrExport2PrintCor: TSmallintField;
    QrExport2NO_COR: TWideStringField;
    QrExport2GraCorCad: TIntegerField;
    QrExport2GraGruC: TIntegerField;
    QrExport2GraGru1: TIntegerField;
    QrExport2GraTamI: TIntegerField;
    QrExport2SitProd: TSmallintField;
    QrExport2EntiSitio: TIntegerField;
    QrExport2GraGruX: TIntegerField;
    QrExport2Empresa: TIntegerField;
    QrExport2StqCenCad: TIntegerField;
    QrExport2QTDE: TFloatField;
    QrExport2PECAS: TFloatField;
    QrExport2PESO: TFloatField;
    QrExport2AREAM2: TFloatField;
    QrExport2AREAP2: TFloatField;
    QrExport2PrcCusUni: TFloatField;
    QrExport2ValorTot: TFloatField;
    QrExport2Exportado: TLargeintField;
    Panel20: TPanel;
    RGTipoPesq2: TRadioGroup;
    CkPositivo2: TCheckBox;
    TabSheet13: TTabSheet;
    QrLayout001: TmySQLQuery;
    DsLayout001: TDataSource;
    QrLayout001DataInvent: TWideStringField;
    QrLayout001MesAnoInic: TWideStringField;
    QrLayout001MesAnoFina: TWideStringField;
    QrLayout001CodigoProd: TWideStringField;
    QrLayout001SituacProd: TWideStringField;
    QrLayout001CNPJTercei: TWideStringField;
    QrLayout001IETerceiro: TWideStringField;
    QrLayout001UFTerceiro: TWideStringField;
    QrLayout001Quantidade: TWideStringField;
    QrLayout001ValorUnita: TWideStringField;
    QrLayout001ValorTotal: TWideStringField;
    QrLayout001ICMSRecupe: TWideStringField;
    QrLayout001Observacao: TWideStringField;
    QrLayout001DescriProd: TWideStringField;
    QrLayout001GrupoProdu: TWideStringField;
    QrLayout001ClassifNCM: TWideStringField;
    QrLayout001RESERVADOS: TWideStringField;
    QrLayout001UnidMedida: TWideStringField;
    QrLayout001DescrGrupo: TWideStringField;
    DBGrid3: TDBGrid;
    frxPRD_PRINT_001_04: TfrxReport;
    frxDsExporta2: TfrxDBDataset;
    frxDsListErr1: TfrxDBDataset;
    QrSMIA3Baixa: TSmallintField;
    QrSMIA3BAIXA_TXT: TWideStringField;
    Panel22: TPanel;
    BitBtn1: TBitBtn;
    QrSemCusUni: TmySQLQuery;
    QrSemCusUniMy_Idx: TIntegerField;
    QrSemCusUniNivel1: TIntegerField;
    QrSemCusUniNO_PRD: TWideStringField;
    QrSemCusUniPrdGrupTip: TIntegerField;
    QrSemCusUniUnidMed: TIntegerField;
    QrSemCusUniNCM: TWideStringField;
    QrSemCusUniNO_PGT: TWideStringField;
    QrSemCusUniSIGLA: TWideStringField;
    QrSemCusUniPrintTam: TSmallintField;
    QrSemCusUniNO_TAM: TWideStringField;
    QrSemCusUniPrintCor: TSmallintField;
    QrSemCusUniNO_COR: TWideStringField;
    QrSemCusUniGraCorCad: TIntegerField;
    QrSemCusUniGraGruC: TIntegerField;
    QrSemCusUniGraGru1: TIntegerField;
    QrSemCusUniGraTamI: TIntegerField;
    QrSemCusUniSitProd: TSmallintField;
    QrSemCusUniEntiSitio: TIntegerField;
    QrSemCusUniGraGruX: TIntegerField;
    QrSemCusUniEmpresa: TIntegerField;
    QrSemCusUniStqCenCad: TIntegerField;
    QrSemCusUniQTDE: TFloatField;
    QrSemCusUniPECAS: TFloatField;
    QrSemCusUniPESO: TFloatField;
    QrSemCusUniAREAM2: TFloatField;
    QrSemCusUniAREAP2: TFloatField;
    QrSemCusUniPrcCusUni: TFloatField;
    QrSemCusUniValorTot: TFloatField;
    QrSemCusUniExportado: TLargeintField;
    QrEntrada: TmySQLQuery;
    QrEntradaQtde: TFloatField;
    QrEntradaCustoAll: TFloatField;
    CkInfoMP_e_Classif: TCheckBox;
    QrLocLote: TmySQLQuery;
    QrLocLoteLote: TWideStringField;
    QrPrdMovLoteMP: TWideStringField;
    TabSheet14: TTabSheet;
    Panel23: TPanel;
    TabSheet15: TTabSheet;
    Panel24: TPanel;
    Panel25: TPanel;
    LaAviso5A: TLabel;
    BtPesquisa5: TBitBtn;
    Panel26: TPanel;
    BtSaida5: TBitBtn;
    PB5: TProgressBar;
    BtImprime5: TBitBtn;
    DBG5Entrada: TdmkDBGrid;
    QrSMPI: TmySQLQuery;
    QrSumBx: TmySQLQuery;
    QrSumBxQtde: TFloatField;
    QrSumBxPecas: TFloatField;
    QrSumBxPeso: TFloatField;
    QrSumEr: TmySQLQuery;
    QrSumErQtde: TFloatField;
    QrSumErPecas: TFloatField;
    QrSumErPeso: TFloatField;
    DsSmiaMPIn: TDataSource;
    QrSMPIDataHora: TDateTimeField;
    QrSMPIIDCtrl: TIntegerField;
    QrSMPITipo: TIntegerField;
    QrSMPIOriCodi: TIntegerField;
    QrSMPIOriCtrl: TIntegerField;
    QrSMPIGraGruX: TIntegerField;
    QrSMPIMP_Qtde: TFloatField;
    QrSMPIMP_Pecas: TFloatField;
    QrSMPIMP_Peso: TFloatField;
    QrSMPIBx_Qtde: TFloatField;
    QrSMPIBx_Pecas: TFloatField;
    QrSMPIBx_Peso: TFloatField;
    QrSMPIEr_Qtde: TFloatField;
    QrSMPIEr_Pecas: TFloatField;
    QrSMPIEr_Peso: TFloatField;
    QrSMPIParCodi: TIntegerField;
    QrSmiaMPIn: TmySQLQuery;
    QrSmiaMPInNO_PRD: TWideStringField;
    QrSmiaMPInSIGLA: TWideStringField;
    QrSmiaMPInGraGru1: TIntegerField;
    QrSmiaMPInDataHora: TDateTimeField;
    QrSmiaMPInIDCtrl: TIntegerField;
    QrSmiaMPInTipo: TIntegerField;
    QrSmiaMPInOriCodi: TIntegerField;
    QrSmiaMPInOriCtrl: TIntegerField;
    QrSmiaMPInGraGruX: TIntegerField;
    QrSmiaMPInMP_Qtde: TFloatField;
    QrSmiaMPInMP_Pecas: TFloatField;
    QrSmiaMPInMP_Peso: TFloatField;
    QrSmiaMPInBx_Qtde: TFloatField;
    QrSmiaMPInBx_Pecas: TFloatField;
    QrSmiaMPInBx_Peso: TFloatField;
    QrSmiaMPInEr_Qtde: TFloatField;
    QrSmiaMPInEr_Pecas: TFloatField;
    QrSmiaMPInEr_Peso: TFloatField;
    QrSmiaMPInParCodi: TIntegerField;
    Panel27: TPanel;
    QrSMPIBxa: TmySQLQuery;
    DsSMPIBxa: TDataSource;
    QrSMPIBxaNO_PRD: TWideStringField;
    QrSMPIBxaSIGLA: TWideStringField;
    QrSMPIBxaDataHora: TDateTimeField;
    QrSMPIBxaIDCtrl: TIntegerField;
    QrSMPIBxaGraGruX: TIntegerField;
    QrSMPIBxaQtde: TFloatField;
    QrSMPIBxaPecas: TFloatField;
    QrSMPIBxaPeso: TFloatField;
    QrSMPIGer: TmySQLQuery;
    DsSMPIGer: TDataSource;
    QrSMPIGerNO_PRD: TWideStringField;
    QrSMPIGerSIGLA: TWideStringField;
    QrSMPIGerDataHora: TDateTimeField;
    QrSMPIGerIDCtrl: TIntegerField;
    QrSMPIGerGraGruX: TIntegerField;
    QrSMPIGerQtde: TFloatField;
    QrSMPIGerPecas: TFloatField;
    QrSMPIGerPeso: TFloatField;
    Panel28: TPanel;
    Label11: TLabel;
    DBG5Gerados: TDBGrid;
    Panel29: TPanel;
    Label12: TLabel;
    DBG5Baixas: TDBGrid;
    Splitter1: TSplitter;
    GroupBox3: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    TPDataIni5: TdmkEditDateTimePicker;
    TPDataFim5: TdmkEditDateTimePicker;
    CBFiltro5: TdmkCheckGroup;
    BtCorrige5: TBitBtn;
    Label15: TLabel;
    QrSmiaMPInMP_MEDIA: TFloatField;
    PMCorrige5: TPopupMenu;
    CorrigeReduzidodoitensbaixados1: TMenuItem;
    Alteraitembaixadoatual1: TMenuItem;
    AlteraitemClassificadoougeradaselelcionado1: TMenuItem;
    QrSMPIBxaTipo: TIntegerField;
    QrSMPIBxaAreaM2: TFloatField;
    QrSMPIBxaAreaP2: TFloatField;
    QrSMPIGerTipo: TIntegerField;
    QrSMPIGerAreaM2: TFloatField;
    QrSMPIGerAreaP2: TFloatField;
    QrSMPIBxaStqCenCad: TIntegerField;
    QrSMPIGerStqCenCad: TIntegerField;
    frxPRD_PRINT_001_05: TfrxReport;
    frxDsSmiaMPIn: TfrxDBDataset;
    frxDsSMPIBxa: TfrxDBDataset;
    frxDsSMPIGer: TfrxDBDataset;
    Panel30: TPanel;
    CkExclEstqMP: TCheckBox;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpEstq0UsoCons: TSmallintField;
    EdGraGru2: TdmkEditCB;
    Label16: TLabel;
    CBGraGru2: TdmkDBLookupComboBox;
    Label17: TLabel;
    EdGraGru3: TdmkEditCB;
    CBGraGru3: TdmkDBLookupComboBox;
    QrGraGru3: TmySQLQuery;
    QrGraGru3Codusu: TIntegerField;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru3PrdGrupTip: TIntegerField;
    DsGraGru3: TDataSource;
    DsGraGru2: TDataSource;
    QrGraGru2: TmySQLQuery;
    QrGraGru2Codusu: TIntegerField;
    QrGraGru2Nivel3: TIntegerField;
    QrGraGru2Nivel2: TIntegerField;
    QrGraGru2Nome: TWideStringField;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1IPI_CST: TSmallintField;
    QrGraGru1IPI_Alq: TFloatField;
    QrGraGru1IPI_cEnq: TWideStringField;
    QrGraGru1TipDimens: TSmallintField;
    QrGraGru1PerCuztMin: TFloatField;
    QrGraGru1PerCuztMax: TFloatField;
    QrGraGru1MedOrdem: TIntegerField;
    QrGraGru1PartePrinc: TIntegerField;
    QrGraGru1NO_MedOrdem: TWideStringField;
    QrGraGru1Medida1: TWideStringField;
    QrGraGru1Medida2: TWideStringField;
    QrGraGru1Medida3: TWideStringField;
    QrGraGru1Medida4: TWideStringField;
    QrGraGru1NO_PartePrinc: TWideStringField;
    QrGraGru1InfAdProd: TWideStringField;
    QrGraGru1SiglaCustm: TWideStringField;
    QrGraGru1HowBxaEstq: TSmallintField;
    QrGraGru1GerBxaEstq: TSmallintField;
    QrGraGru1PIS_CST: TSmallintField;
    QrGraGru1PIS_AlqP: TFloatField;
    QrGraGru1PIS_AlqV: TFloatField;
    QrGraGru1PISST_AlqP: TFloatField;
    QrGraGru1PISST_AlqV: TFloatField;
    QrGraGru1COFINS_CST: TSmallintField;
    QrGraGru1COFINS_AlqP: TFloatField;
    QrGraGru1COFINS_AlqV: TFloatField;
    QrGraGru1COFINSST_AlqP: TFloatField;
    QrGraGru1COFINSST_AlqV: TFloatField;
    QrGraGru1ICMS_modBC: TSmallintField;
    QrGraGru1ICMS_modBCST: TSmallintField;
    QrGraGru1ICMS_pRedBC: TFloatField;
    QrGraGru1ICMS_pRedBCST: TFloatField;
    QrGraGru1ICMS_pMVAST: TFloatField;
    QrGraGru1ICMS_pICMSST: TFloatField;
    QrGraGru1IPI_TpTrib: TSmallintField;
    QrGraGru1IPI_vUnid: TFloatField;
    QrGraGru1ICMS_Pauta: TFloatField;
    QrGraGru1ICMS_MaxTab: TFloatField;
    QrGraGru1IPI_pIPI: TFloatField;
    QrGraGru1cGTIN_EAN: TWideStringField;
    QrGraGru1EX_TIPI: TWideStringField;
    QrGraGru1PIS_pRedBC: TFloatField;
    QrGraGru1PISST_pRedBCST: TFloatField;
    QrGraGru1COFINS_pRedBC: TFloatField;
    QrGraGru1COFINSST_pRedBCST: TFloatField;
    QrGraGru1ICMSRec_pRedBC: TFloatField;
    QrGraGru1IPIRec_pRedBC: TFloatField;
    QrGraGru1PISRec_pRedBC: TFloatField;
    QrGraGru1COFINSRec_pRedBC: TFloatField;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1IPIRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    QrGraGru1ICMSRec_tCalc: TSmallintField;
    QrGraGru1IPIRec_tCalc: TSmallintField;
    QrGraGru1PISRec_tCalc: TSmallintField;
    QrGraGru1COFINSRec_tCalc: TSmallintField;
    QrGraGru1ICMSAliqSINTEGRA: TFloatField;
    QrGraGru1ICMSST_BaseSINTEGRA: TFloatField;
    QrGraGru1FatorClas: TIntegerField;
    QrGraGru1PerComissF: TFloatField;
    QrGraGru1PerComissR: TFloatField;
    QrGraGru1PerComissZ: TSmallintField;
    DsGraGru1: TDataSource;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrPrdGrupTipMadeBy: TSmallintField;
    QrPrdGrupTipFracio: TSmallintField;
    QrPrdGrupTipTipPrd: TSmallintField;
    QrPrdGrupTipNivCad: TSmallintField;
    QrPrdGrupTipFaixaIni: TIntegerField;
    QrPrdGrupTipFaixaFim: TIntegerField;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    QrPrdGrupTipNOME_MADEBY: TWideStringField;
    QrPrdGrupTipNOME_FRACIO: TWideStringField;
    QrPrdGrupTipNOME_TIPPRD: TWideStringField;
    QrPrdGrupTipImpedeCad: TSmallintField;
    QrPrdGrupTipPerComissF: TFloatField;
    QrPrdGrupTipPerComissR: TFloatField;
    Panel31: TPanel;
    Panel32: TPanel;
    BitBtn5: TBitBtn;
    DBGrid4: TDBGrid;
    QrPesqESel_Sel: TmySQLQuery;
    DsPesqESel_Sel: TDataSource;
    QrPesqESel_SelCodigo: TIntegerField;
    QrPesqESel_SelNome: TWideStringField;
    QrPesqESel_SelAtivo: TSmallintField;
    Panel33: TPanel;
    CGAgrup0: TdmkCheckGroup;
    CGSitEstq: TdmkCheckGroup;
    CkSelecao0: TCheckBox;
    BtLocaliza1: TBitBtn;
    Panel34: TPanel;
    EdTabePrcCab2: TdmkEditCB;
    CBTabePrcCab2: TdmkDBLookupComboBox;
    CkTabePrcCab2: TCheckBox;
    QrTabePrcCab: TmySQLQuery;
    DsTabePrcCab: TDataSource;
    Label18: TLabel;
    QrTabePrcCabCodigo: TIntegerField;
    QrTabePrcCabCodUsu: TIntegerField;
    QrTabePrcCabNome: TWideStringField;
    QrSMIC2TPC_GCP_TAB: TIntegerField;
    QrSMIC2TPC_GCP_ID: TFloatField;
    QrExport2TPC_GCP_ID: TFloatField;
    QrExport2TPC_GCP_TAB: TIntegerField;
    QrSemCusUniTPC_GCP_ID: TFloatField;
    QrSemCusUniTPC_GCP_TAB: TIntegerField;
    TabSheet16: TTabSheet;
    Panel18: TPanel;
    dmkDBGrid3: TdmkDBGrid;
    TabSheet17: TTabSheet;
    Panel35: TPanel;
    Panel36: TPanel;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    TPDataFim2: TdmkEditDateTimePicker;
    TPDataIni2: TdmkEditDateTimePicker;
    CkDataIni2: TCheckBox;
    QrSMIC1: TmySQLQuery;
    DsSMIC1: TDataSource;
    QrSMIC1Nivel1: TIntegerField;
    QrSMIC1NO_PRD: TWideStringField;
    QrSMIC1PrdGrupTip: TIntegerField;
    QrSMIC1UnidMed: TIntegerField;
    QrSMIC1NCM: TWideStringField;
    QrSMIC1NO_PGT: TWideStringField;
    QrSMIC1SIGLA: TWideStringField;
    QrSMIC1NO_TAM: TWideStringField;
    QrSMIC1NO_COR: TWideStringField;
    QrSMIC1GraCorCad: TIntegerField;
    QrSMIC1GraGruC: TIntegerField;
    QrSMIC1GraGru1: TIntegerField;
    QrSMIC1GraTamI: TIntegerField;
    QrSMIC1Empresa: TIntegerField;
    QrSMIC1StqCenCad: TIntegerField;
    QrSMIC1Qtde: TFloatField;
    QrSMIC1SitProd: TSmallintField;
    QrSMIC1ENS_CNPJ: TWideStringField;
    QrSMIC1ENS_IE: TWideStringField;
    QrSMIC1ENS_NO_UF: TWideStringField;
    QrSMIC1EMP_CNPJ: TWideStringField;
    QrSMIC1EMP_IE: TWideStringField;
    QrSMIC1EMP_NO_UF: TWideStringField;
    QrSMIC1PECAS: TFloatField;
    QrSMIC1PESO: TFloatField;
    QrSMIC1AREAM2: TFloatField;
    QrSMIC1AREAP2: TFloatField;
    QrSMIC1PrcCusUni: TFloatField;
    QrSMIC1GraGruX: TIntegerField;
    QrSMIC1ValorTot: TFloatField;
    QrSMIC1My_Idx: TIntegerField;
    QrSMIC1PrintTam: TSmallintField;
    QrSMIC1PrintCor: TSmallintField;
    QrSMIC1EntiSitio: TIntegerField;
    QrSMIC1Exportado: TLargeintField;
    QrSMIC1TPC_GCP_ID: TFloatField;
    QrSMIC1TPC_GCP_TAB: TIntegerField;
    QrGGXs2: TmySQLQuery;
    QrGGXs2GraGruX: TIntegerField;
    QrSum1: TmySQLQuery;
    QrSum2: TmySQLQuery;
    QrSum2QTDE: TFloatField;
    QrSum1QTDE: TFloatField;
    QrSumM: TmySQLQuery;
    QrSumMQtdSum: TFloatField;
    QrSumMQtdPos: TFloatField;
    QrSumMQtdNeg: TFloatField;
    QrSum2ValorTot: TFloatField;
    QrSum1ValorTot: TFloatField;
    QrSumA: TmySQLQuery;
    QrSumAQtdSum: TFloatField;
    QrSumAQtdPos: TFloatField;
    QrSumAQtdNeg: TFloatField;
    QrReduzido: TmySQLQuery;
    QrDiverge2: TmySQLQuery;
    QrReduzidoNivel1: TIntegerField;
    QrReduzidoNO_PRD: TWideStringField;
    QrReduzidoNivel2: TIntegerField;
    QrReduzidoNO_Ni2: TWideStringField;
    QrReduzidoNivel3: TIntegerField;
    QrReduzidoNO_Ni3: TWideStringField;
    QrReduzidoPrdGrupTip: TIntegerField;
    QrReduzidoNO_PGT: TWideStringField;
    frxPRD_PRINT_001_02_B: TfrxReport;
    QrGIL_Mov: TmySQLQuery;
    QrGIL_MovEmpresa: TIntegerField;
    QrGIL_MovGraGruX: TIntegerField;
    QrGIL_MovPrcCusUni: TFloatField;
    QrGIL_MovIniQtd: TFloatField;
    QrGIL_MovIniVal: TFloatField;
    QrGIL_MovInnQtd: TFloatField;
    QrGIL_MovInnVal: TFloatField;
    QrGIL_MovOutQtd: TFloatField;
    QrGIL_MovOutVal: TFloatField;
    QrGIL_MovBalQtd: TFloatField;
    QrGIL_MovBalVal: TFloatField;
    QrGIL_MovFimQtd: TFloatField;
    QrGIL_MovFimVal: TFloatField;
    QrGIL_MovPrdGrupTip: TIntegerField;
    QrGIL_MovNO_PGT: TWideStringField;
    QrGIL_MovNivel1: TIntegerField;
    QrGIL_MovNO_PRD: TWideStringField;
    QrGIL_MovNivel2: TIntegerField;
    QrGIL_MovNO_Ni2: TWideStringField;
    QrGIL_MovNivel3: TIntegerField;
    QrGIL_MovNO_Ni3: TWideStringField;
    frxDsGIL_Mov: TfrxDBDataset;
    frxPRD_PRINT_001_00L: TfrxReport;
    CkListaReduzidos: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel37: TPanel;
    PnSaiDesis: TPanel;
    BtSaida0: TBitBtn;
    frxPRD_PRINT_001_00E_2: TfrxReport;
    CkStqCenCad: TCheckBox;
    QrPrdEstqNO_SCC: TWideStringField;
    QrPrdEstqCodUsu: TIntegerField;
    CkPrdGrupo: TCheckBox;
    frxPRD_PRINT_001_00E_3: TfrxReport;
    LaStqTotal: TLabel;
    Panel9: TPanel;
    BtImprime0: TBitBtn;
    BtPesquisa0: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel21: TPanel;
    LaAviso1B: TLabel;
    LaAviso1A: TLabel;
    PB0: TProgressBar;
    dmkLabel3: TdmkLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    LaInventarioFisco: TLabel;
    QrStqCenCadPsq: TmySQLQuery;
    DsStqCenCadPsq: TDataSource;
    QrStqCenCadPsqCodigo: TIntegerField;
    QrStqCenCadPsqCodUsu: TIntegerField;
    QrStqCenCadPsqNome: TWideStringField;
    BtExcluiIncond: TBitBtn;
    TabSheet18: TTabSheet;
    TabSheet19: TTabSheet;
    GroupBox6: TGroupBox;
    Label6: TLabel;
    Label19: TLabel;
    TPDataIni7: TdmkEditDateTimePicker;
    TPDataFim7: TdmkEditDateTimePicker;
    Panel38: TPanel;
    Panel39: TPanel;
    LaAviso07: TLabel;
    BtPesquisa7: TBitBtn;
    Panel40: TPanel;
    BitBtn7: TBitBtn;
    ProgressBar2: TProgressBar;
    BtImprime7: TBitBtn;
    QrReposicao7: TMySQLQuery;
    QrReposicao7Nivel1: TIntegerField;
    QrReposicao7NO_PRD: TWideStringField;
    QrReposicao7PrdGrupTip: TIntegerField;
    QrReposicao7UnidMed: TIntegerField;
    QrReposicao7NO_PGT: TWideStringField;
    QrReposicao7SIGLA: TWideStringField;
    QrReposicao7NO_TAM: TWideStringField;
    QrReposicao7NO_COR: TWideStringField;
    QrReposicao7GraCorCad: TIntegerField;
    QrReposicao7GraGruC: TIntegerField;
    QrReposicao7GraGru1: TIntegerField;
    QrReposicao7GraTamI: TIntegerField;
    QrReposicao7Qtde: TFloatField;
    QrEstoque7: TMySQLQuery;
    QrEstoque7Qtde: TFloatField;
    QrEstoque7GraGruX: TIntegerField;
    QrReposicao7GraGruX: TIntegerField;
    QrReposicao7Estoque: TFloatField;
    DBGReposicao7: TdmkDBGridZTO;
    DsReposicao7: TDataSource;
    frxPRD_PRINT_001_07_A: TfrxReport;
    frxDsReposicao7: TfrxDBDataset;
    QrReposicao7Referencia: TWideStringField;
    frxPRD_PRINT_001_07_B: TfrxReport;
    QrReposicao7NO_MARCA: TWideStringField;
    QrReposicao7_Marca_: TIntegerField;
    QrSMIA3NO_VarFatId: TWideStringField;
    QrPrdMovNO_LOC_CEN: TWideStringField;
    QrPrdMovStqCenLoc: TIntegerField;
    QrPrdEstqNO_PRD_TAM_COR: TWideStringField;
    procedure BtSaida1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisa1Click(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure TPDataIni1Change(Sender: TObject);
    procedure TPDataFim1Change(Sender: TObject);
    procedure QrPrdMovAfterOpen(DataSet: TDataSet);
    procedure QrPrdMovBeforeClose(DataSet: TDataSet);
    procedure BtImprime1Click(Sender: TObject);
    procedure QrPrdMovCalcFields(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure BtPesquisa0Click(Sender: TObject);
    procedure QrPrdEstqAfterOpen(DataSet: TDataSet);
    procedure QrPrdEstqBeforeClose(DataSet: TDataSet);
    procedure BtExporta2Click(Sender: TObject);
    procedure BtPesq2Click(Sender: TObject);
    procedure BtImprime0Click(Sender: TObject);
    procedure BtImprime2Click(Sender: TObject);
    procedure BtPesquisa3Click(Sender: TObject);
    procedure QrSMIA3AfterOpen(DataSet: TDataSet);
    procedure QrSMIA3BeforeClose(DataSet: TDataSet);
    procedure BtImprime3Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrPrdEstqCalcFields(DataSet: TDataSet);
    procedure RGTipoPesq2Click(Sender: TObject);
    procedure TPDataFim2Change(Sender: TObject);
    procedure QrSMIC2BeforeClose(DataSet: TDataSet);
    procedure QrSMIC2AfterOpen(DataSet: TDataSet);
    procedure frxPRD_PRINT_001_00E_1GetValue(const VarName: string;
      var Value: Variant);
    procedure QrExport2BeforeClose(DataSet: TDataSet);
    procedure QrExport2AfterScroll(DataSet: TDataSet);
    procedure CkPositivo2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtPesquisa5Click(Sender: TObject);
    procedure QrSmiaMPInBeforeClose(DataSet: TDataSet);
    procedure QrSmiaMPInAfterScroll(DataSet: TDataSet);
    procedure Panel24Resize(Sender: TObject);
    procedure CBFiltro5Click(Sender: TObject);
    procedure BtCorrige5Click(Sender: TObject);
    procedure QrSmiaMPInAfterOpen(DataSet: TDataSet);
    procedure CorrigeReduzidodoitensbaixados1Click(Sender: TObject);
    procedure Alteraitembaixadoatual1Click(Sender: TObject);
    procedure AlteraitemClassificadoougeradaselelcionado1Click(Sender: TObject);
    procedure PMCorrige5Popup(Sender: TObject);
    procedure BtImprime5Click(Sender: TObject);
    procedure QrGraGru2AfterScroll(DataSet: TDataSet);
    procedure QrGraGru2AfterClose(DataSet: TDataSet);
    procedure QrGraGru3AfterClose(DataSet: TDataSet);
    procedure QrGraGru3AfterScroll(DataSet: TDataSet);
    procedure QrPrdGrupTipAfterScroll(DataSet: TDataSet);
    procedure QrGraGru1AfterClose(DataSet: TDataSet);
    procedure BitBtn5Click(Sender: TObject);
    procedure QrPesqESel_SelBeforeClose(DataSet: TDataSet);
    procedure QrPesqESel_SelAfterOpen(DataSet: TDataSet);
    procedure BtLocaliza1Click(Sender: TObject);
    procedure CkTabePrcCab2Click(Sender: TObject);
    procedure TPDataFim2Click(Sender: TObject);
    procedure CkStqCenCadClick(Sender: TObject);
    procedure CkPrdGrupoClick(Sender: TObject);
    procedure QrPrdEstqAfterScroll(DataSet: TDataSet);
    procedure EdGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtExcluiIncondClick(Sender: TObject);
    procedure BtPesquisa7Click(Sender: TObject);
    procedure BtImprime7Click(Sender: TObject);
    procedure QrReposicao7BeforeClose(DataSet: TDataSet);
    procedure QrReposicao7AfterOpen(DataSet: TDataSet);
    procedure LaAviso07Click(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
  private
    { Private declarations }
    FSelecionouGG1: Boolean;
    FExportarItem: Boolean;
    FSmiaMPIn, FGGX_SCC_Stq, FGGX_SCC_CPR, FPrdMov: String;
    FAgora: TDateTime;
    //procedure ReopenGraGru1(PrdGrupTip: Integer);
    procedure FechaPesquisa();
    //procedure GeraEstoqueEm(Empresa_Txt: String; DataFim: TDateTime);
    function VeriStr(Tam, Item: Integer; Texto: String): String;
    function PertenceAoGrupoPGT(): Boolean;
    function PertenceAoGrupoPRD(): Boolean;
    function PertenceAoGrupoNI2(): Boolean;
    procedure ReopenSmiaMPIn(IDCtrl: Integer);
    procedure ReopenStqMovIts();
    procedure CorrigeBaixa();
    procedure NeutralizaUsoConsumo_4();
    procedure ReopenGraGru1(Nivel1: Integer);
    procedure ReopenGraGru2(Nivel2: Integer);
    procedure ReopenGraGru3(Nivel3: Integer);
    procedure ReopenTabePrcCab();
    procedure ConfiguraGradeEstoque(PrdGrupo: Boolean);
  public
    { Public declarations }
  end;

  var
  FmGraImpLista: TFmGraImpLista;

implementation

uses
  {$IFNDef semNFe_v0000}ModuleNFe_0000, EntradaCab, {$EndIF}
  UnMyObjects, ModuleGeral, Module, dmkGeral, UnInternalConsts, UCreate,
  UMySQLModule, ModProd, CfgExpFile, MyDBCheck, UnGrade_Tabs, UnGrade_PF,
  {$IfNDef NAO_GPED}UnGFat_Jan, {$EndIF}
  Principal, MyListas, UnGrade_Create, MeuDBUses, UnGrade_Jan;

{$R *.DFM}

const
  FSMIC1 = '_smic1_';
  FSMIC2 = '_smic2_';

procedure TFmGraImpLista.BtPesq2Click(Sender: TObject);
var
  NO_PRD, TxtSQL_Part1, TxtSQL_Part2, NilA, NilB, NO_Ni2, NO_Ni3, NO_PGT,
  Empresa_Txt: String;
  Empresa, PGT, GG1, GG2, GG3, StqCenCad, GraGruX, PrdGrupTip, Nivel1, Nivel2,
  Nivel3: Integer;
  SumVal, SumQtd, PrcCusUni, IniQtd, IniVal, InnQtd, InnVal, OutQtd, OutVal,
  BalQtd, BalVal, FimQtd, FimVal: Double;
begin
  PageControl1.ActivePageIndex := 2;
  PageControl2.ActivePageIndex := 2;
  PageControl3.ActivePageIndex := 0;
  NeutralizaUsoConsumo_4();
(* Desabilitado em 2015-02-07 Ainda precisa?
  if CkExclEstqMP.Checked then
    Dmod.ExcluiItensEstqNFsCouroImportadas();
*)
  CorrigeBaixa();
  if (Int(TPDataFim2.Date) <> Geral.UltimoDiaDoMes(Int(TPDataFim2.Date)))
  and (RGTipoPesq2.ItemIndex = 2)  then
  begin
    if Geral.MB_Pergunta('O bot�o de ' +
    'exporta��o n�o ser� habilitado pois a data escolhida n�o � final de mes!' +
    sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  end;
  //
  if EdEmpresa.ValueVariant <> 0 then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    Empresa_Txt := FormatFloat('0', Empresa);
  end else begin
    Geral.MB_Aviso('Informe a empresa!');
    Exit;
  end;


////////////////////////////////////////////////////////////////////////////////
    PGT       := EdPrdGrupTip.ValueVariant;
    GG1       := EdGraGru1.ValueVariant;
    GG2       := EdGraGru2.ValueVariant;
    GG3       := EdGraGru3.ValueVariant;
    StqCenCad := EdStqCenCad.ValueVariant;
    //
    if StqCenCad <> 0 then
      StqCenCad := QrStqCenCadPsqCodigo.Value;
////////////////////////////////////////////////////////////////////////////////

  Grade_PF.GeraEstoqueEm_2(
  Empresa_Txt, TPDataIni2.Date, TPDataFim2.Date, PGT, GG1, GG2, GG3, StqCenCad,
  RGTipoPesq2.ItemIndex, CBGraCusPrc.KeyValue, CBTabePrcCab2.KeyValue,
  QrGraCusPrcCodigo.Value, QrTabePrcCabCodigo.Value, QrPrdGrupTipCodigo.Value,
  QrGraGru1Nivel1.Value, QrGraGru2Nivel2.Value, QrGraGru3Nivel3.Value,
  QrPGT_SCC, QrAbertura2a, QrAbertura2b,
  CkTabePrcCab2.Checked, CkPositivo2.Checked, LaAviso2A,
  FSMIC2, QrSMIC2, TxtSQL_Part1, TxtSQL_Part2);
  //
  if CkDataIni2.Checked then
  begin
    Grade_PF.GeraEstoqueEm_2(
    Empresa_Txt, 0, TPDataIni2.Date - 1, PGT, GG1, GG2, GG3, StqCenCad,
    RGTipoPesq2.ItemIndex, CBGraCusPrc.KeyValue, CBTabePrcCab2.KeyValue,
    QrGraCusPrcCodigo.Value, QrTabePrcCabCodigo.Value, QrPrdGrupTipCodigo.Value,
    QrGraGru1Nivel1.Value, QrGraGru2Nivel2.Value, QrGraGru3Nivel3.Value,
    QrPGT_SCC, QrAbertura2a, QrAbertura2b,
    CkTabePrcCab2.Checked, CkPositivo2.Checked, LaAviso2A,
    FSMIC1, QrSMIC1, NilA, NilB);
    //
    GradeCriar.RecriaTempTableNovo(ntrttGIL_Mov, DModG.QrUpdPID1, False);
    //
    QrGGXs2.Close;
    QrGGXs2.Database := DmodG.MyPID_DB;
    QrGGXs2.SQL.Clear;
    QrGGXs2.SQL.Add('SELECT GraGruX');
    QrGGXs2.SQL.Add('FROM _smic1_');
    QrGGXs2.SQL.Add('');
    QrGGXs2.SQL.Add('UNION');
    QrGGXs2.SQL.Add('');
    QrGGXs2.SQL.Add('SELECT GraGruX');
    QrGGXs2.SQL.Add('FROM _smic2_');
    QrGGXs2.SQL.Add('');
(*  Busca Negativos?
    QrGGXs2.SQL.Add('UNION');
    QrGGXs2.SQL.Add('');
    QrGGXs2.SQL.Add('SELECT DISTINCT GraGruX');
    QrGGXs2.SQL.Add('FROM ' + TMeuDB + '.stqmovitsa');
    QrGGXs2.SQL.Add(dmkPF.SQL_Periodo('WHERE DataHora ', TPDataIni2.Date,
                    TPDataFim2.Date, True, True));
    QrGGXs2.SQL.Add('');
*)
    QrGGXs2.SQL.Add('ORDER BY GraGruX');
    QrGGXs2.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrGGXs2, DModG.MyPID_DB);
    QrGGXs2.First;
    PB2.Position := 0;
    PB2.Max := QrGGXs2.RecordCount;

    //  Movimento no Per�odo
    QrSumM.Close;
    QrSumM.Database := DmodG.MyPID_DB;
    QrSumM.SQL.Clear;
    QrSumM.SQL.Add(TxtSQL_Part1);
    QrSumM.SQL.Add('WHERE smic.Tipo > 0');
    QrSumM.SQL.Add(TxtSQL_Part2);

    //  Ajustes no Per�odo
    QrSumA.Close;
    QrSumA.Database := DmodG.MyPID_DB;
    QrSumA.SQL.Clear;
    QrSumA.SQL.Add(TxtSQL_Part1);
    QrSumA.SQL.Add('WHERE smic.Tipo < 1');
    QrSumA.SQL.Add(TxtSQL_Part2);
    //
    while not QrGGXs2.Eof do
    begin
      PB2.Position := PB2.Position + 1;
      MyObjects.Informa(LaAviso2A, True, 'Calculando reduzido n�mero ' +
      FormatFloat('000000000', QrGGXs2GraGruX.Value));
      //
      GraGruX := QrGGXs2GraGruX.Value;
      //
      QrSum1.Close;
      QrSum1.Params[0].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrSum1, DModG.MyPID_DB);
      //
      QrSum2.Close;
      QrSum2.Params[0].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrSum2, DModG.MyPID_DB);
      //
      QrSumM.Close;
      QrSumM.Params[0].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrSumM, DModG.MyPID_DB);
      //
      QrSumA.Close;
      QrSumA.Params[0].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrSumA, DModG.MyPID_DB);
      //
      QrReduzido.Close;
      QrReduzido.Params[0].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrReduzido, Dmod.MyDB);
      //
      SumQtd := QrSum1QTDE.Value + QrSum2QTDE.Value;
      SumVal := QrSum1ValorTot.Value + QrSum2ValorTot.Value;
      if SumQtd = 0 then
        PrcCusUni := 0
      else
        PrcCusUni := SumVal / SumQtd;
      if PrcCusUni < 0 then
        PrcCusUni := PrcCusUni * -1;
      //
      IniQtd := QrSum1QTDE.Value;
      IniVal := QrSum1ValorTot.Value;
      InnQtd := QrSumMQtdPos.Value;
      InnVal := QrSumMQtdPos.Value * PrcCusUni;
      OutQtd := QrSumMQtdNeg.Value;
      OutVal := QrSumMQtdNeg.Value * PrcCusUni;
      BalQtd := QrSumAQtdSum.Value;
      BalVal := QrSumMQtdSum.Value * PrcCusUni;
      FimQtd := QrSum2QTDE.Value;
      FimVal := QrSum2ValorTot.Value;
      NO_PRD := QrReduzidoNO_PRD.Value;
      PrdGrupTip := QrReduzidoPrdGrupTip.Value;
      NO_PGT := QrReduzidoNO_PGT.Value;
      Nivel1 := QrReduzidoNivel1.Value;
      Nivel2 := QrReduzidoNivel2.Value;
      NO_Ni2 := QrReduzidoNO_Ni2.Value;
      Nivel3 := QrReduzidoNivel3.Value;
      NO_Ni3 := QrReduzidoNO_Ni3.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_gil_mov_', False, [
      'Empresa', 'PrcCusUni', 'IniQtd',
      'IniVal', 'InnQtd', 'InnVal',
      'OutQtd', 'OutVal', 'BalQtd',
      'BalVal', 'FimQtd', 'FimVal',
      'PrdGrupTip', 'NO_PGT', 'Nivel1',
      'NO_PRD', 'Nivel2', 'NO_Ni2',
      'Nivel3', 'NO_Ni3'], ['GraGruX'], [
      Empresa, PrcCusUni, IniQtd,
      IniVal, InnQtd, InnVal,
      OutQtd, OutVal, BalQtd,
      BalVal, FimQtd, FimVal,
      PrdGrupTip, NO_PGT, Nivel1,
      NO_PRD, Nivel2, NO_Ni2,
      Nivel3, NO_Ni3], [GraGruX], False);
      //
      QrGGXs2.Next;
    end;
    QrDiverge2.Close;
    UnDmkDAC_PF.AbreQuery(QrDiverge2, DModG.MyPID_DB);
    if QrDiverge2.RecordCount > 0 then
      Geral.MB_Aviso('H� diverg�ncias na pesquisa de movimento!' + sLineBreak +
      'Avise a DERMATEK!');
    //
    PB2.Position := 0;
    MyObjects.Informa(LaAviso2A, False, '...');
  end;
end;

procedure TFmGraImpLista.ConfiguraGradeEstoque(PrdGrupo: Boolean);
begin
  DBGPrdEstq.Columns[9].Visible  := not PrdGrupo;
  DBGPrdEstq.Columns[10].Visible := not PrdGrupo;
end;

procedure TFmGraImpLista.BtImprime2Click(Sender: TObject);
var
  Ordem: String;
begin
  if (PageControl1.ActivePageIndex = 2) and (PageControl2.ActivePageIndex = 2)
    and (PageControl3.ActivePageIndex = 2) then
    MyObjects.frxMostra(frxPRD_PRINT_001_04, 'Falhas na Exporta��o')
  else
  if CkDataIni2.Checked then
  begin
    Ordem := UMyMod.MontaOrdemSQL([
      Geral.IntInConjunto(1, CGAgrup2.Value),
      Geral.IntInConjunto(2, CGAgrup2.Value),
      Geral.IntInConjunto(4, CGAgrup2.Value)], ['NO_PGT', 'NO_Ni2', 'NO_PRD']);
    QrGIL_Mov.Close;
    QrGIL_Mov.Database := DmodG.MyPID_DB;
    QrGIL_Mov.SQL.Clear;
    QrGIL_Mov.SQL.Add('SELECT * FROM _gil_mov_');
    QrGIL_Mov.SQL.Add('');
    QrGIL_Mov.SQL.Add(Ordem);
    QrGIL_Mov.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrGIL_Mov, DModG.MyPID_DB);
    //
    MyObjects.frxMostra(frxPRD_PRINT_001_02_B, 'Movimento do Estoque');
  end else
    MyObjects.frxMostra(frxPRD_PRINT_001_02_A, 'Estoque final em ...');
end;

procedure TFmGraImpLista.BtImprime3Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxPRD_PRINT_001_03, 'Lan�amentos no estoque');
end;

procedure TFmGraImpLista.BtImprime5Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxPRD_PRINT_001_05, 'Panorama da mat�ria-prima');
end;

procedure TFmGraImpLista.BtImprime7Click(Sender: TObject);
begin
  DBGReposicao7.DataSource := nil;
  try
    MyObjects.frxDefineDataSets(frxPRD_PRINT_001_07_B, [
     frxDsReposicao7
    ]);
    //
    MyObjects.frxMostra(frxPRD_PRINT_001_07_B, 'Reposi��o');
  finally
    DBGReposicao7.DataSource := DsReposicao7;
  end;
end;

procedure TFmGraImpLista.BtLocaliza1Click(Sender: TObject);
var
  ShowMensagem: Boolean;
  NomeFatID_NFe: String;
  {$IfNDef SemNFe_0000}
  IDCtrl, CodUsu: Integer;
  {$EndIf}
begin
  ShowMensagem := False;
  case QrPrdMovTipo.Value of
    VAR_FATID_0051, VAR_FATID_0151:
    begin
      {$IfNDef SemNFe_0000}
      if Grade_Jan.CriaFormEntradaCab(tnfUsoEConsumo, False, 0) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
          'SELECT IDCtrl',
          'FROM nfecaba',
          'WHERE FatID=' + Geral.FF0(QrPrdMovTipo.Value),
          'AND FatNum=' + Geral.FF0(QrPrdMovOriCodi.Value),
          '']);
        IDCtrl := Dmod.QrAux.FieldByName('IDCtrl').AsInteger;
        FmEntradaCab.LocCod(0, IDCtrl);
        //
        if (FmEntradaCab.QrNFeCabAIDCtrl.Value = IDCtrl) and (IDCtrl <> 0) then
          FmEntradaCab.ShowModal
        else
          ShowMensagem := True;
        FmEntradaCab.Destroy;
      end;
      {$EndIf}
    end;
    VAR_FATID_0001,
    VAR_FATID_0002:
    begin
      {$IfNDef NAO_GPED}
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'SELECT ped.CodUsu ',
        'FROM fatpedcab cab ',
        'LEFT JOIN pedivda ped ON ped.Codigo = cab.Pedido ',
        'WHERE cab.Pedido <> 0 ',
        'AND cab.Codigo=' + Geral.FF0(QrPrdMovOriCodi.Value),
        '']);
      CodUsu := Dmod.QrAux.FieldByName('CodUsu').Value;
      //
      if CodUsu > 0 then
        GFat_Jan.MostraFormFatPedCab(QrPrdMovOriCodi.Value)
      else
        GFat_Jan.MostraFormFatDivGer(QrPrdMovOriCodi.Value);
      {$EndIf}
    end
    else begin
      NomeFatID_NFe := '???';
(* ini 2023-08-16
{$IFNDef semNFe_v0000}
      NomeFatID_NFe := DmNFe_0000.NomeFatID_NFe(FatID);
{$EndIf}
  *)
      NomeFatID_NFe := DmkEnums.NomeFatID_All(QrPrdMovTipo.Value);
// fim 2023-08-16
      Geral.MB_Info('Tipo de lan�amento sem localiza��o implementada:' +
      sLineBreak + FormatFloat('0', QrPrdMovTipo.Value) + ': ' +
      NomeFatID_NFe + sLineBreak +
      'AVISE A DERMATEK!');
    end;
  end;
  if ShowMensagem then
    Geral.MB_Erro('N�o foi poss�vel localizar o lan�amento!' +
    sLineBreak + 'AVISE A DERMATEK!');
end;

procedure TFmGraImpLista.Alteraitembaixadoatual1Click(Sender: TObject);
begin
  Grade_PF.AlteraSMIA(QrSMPIBxa);
  ReopenStqMovIts();
end;

procedure TFmGraImpLista.AlteraitemClassificadoougeradaselelcionado1Click(
  Sender: TObject);
begin
  Grade_PF.AlteraSMIA(QrSMPIGer);
  ReopenStqMovIts();
end;

procedure TFmGraImpLista.BitBtn1Click(Sender: TObject);
var
  Data: String;
  S, N: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Data := Geral.FDT(Int(TPDataFim2.Date) + 1, 1);
    S := 0;
    N := 0;
    //
    QrSemCusUni.Close;
    UnDmkDAC_PF.AbreQuery(QrSemCusUni, DModG.MyPID_DB);
    PB2.Position := 0;
    PB2.Max := QrSemCusUni.RecordCount;
    QrSemCusUni.First;
    while not QrSemCusUni.Eof do
    begin
      PB2.Position := PB2.Position + 1;
      //
      QrEntrada.Close;
      QrEntrada.Params[00].AsString  := Data;
      QrEntrada.Params[01].AsInteger := QrSemCusUniGraGruX.Value;
      UnDmkDAC_PF.AbreQuery(QrEntrada, Dmod.MyDB);
      //
      if QrEntrada.RecordCount > 0 then
        S := S + 1
      else
        N := N + 1;
      //
      MyObjects.Informa(LaAviso2A, True, 'N�o = ' + IntToStr(N) +
                                   '  #  Sim = ' + IntToStr(S));
      //
      QrSemCusUni.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGraImpLista.BitBtn3Click(Sender: TObject);
  //BtPesquisa0Click(Self);
var
  Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip: Integer;
  //Qtde, Pecas, Peso, AreaM2, AreaP2: Double;
begin
  if QrGSS.State = dsInactive then
    BtPesq2Click(Self);
  //
  (*FGGX_SCC_CPR := UCriar.RecriaTempTable('GGX_SCC_Stq', DmodG.QrUpdPID1, False, 0, 'GGX_SCC_CPR');
  'GGX_SCC_Stq', DmodG.QrUpdPID1, False, 0, 'GGX_SCC_CPR');*)
  FGGX_SCC_CPR := GradeCriar.RecriaTempTableNovo(ntrttGGX_SCC_Stq, DModG.QrUpdPID1, False, 0, 'GGX_SCC_CPR');
  //
  PB1.Position := 0;
  //
  Empresa := EdEmpresa.ValueVariant;
  GraGru1 := EdGraGru1.ValueVariant;
  GraGru2 := EdGraGru2.ValueVariant;
  GraGru3 := EdGraGru3.ValueVariant;
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  if Empresa = 0 then
  begin
    Geral.MB_Aviso('Defina a empresa!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1B, LaAviso1B, True, 'Pesquisando');
    //
    //FGGX_SCC_Stq := UCriar.RecriaTempTable('GGX_SCC_Stq', DmodG.QrUpdPID1, False);
    FGGX_SCC_Stq := GradeCriar.RecriaTempTableNovo(ntrttGGX_SCC_Stq, DModG.QrUpdPID1, False);
    QrSumA2.Close;
    QrSumA2.Database := Dmod.MyDB;
    QrSumA2.SQL.Clear;
    QrSumA2.SQL.Add('INSERT INTO ' + DmodG.MyPID_DB.DataBaseName + '.' + FGGX_SCC_Stq);
    QrSumA2.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
    QrSumA2.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
    QrSumA2.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
    QrSumA2.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,');
    //
    QrSumA2.SQL.Add('-SUM(smia.Qtde * smia.Baixa) QTDE, -SUM(smia.Pecas) PECAS,');
    QrSumA2.SQL.Add('-SUM(smia.Peso) PESO, -SUM(smia.AreaM2) AREAM2,');
    QrSumA2.SQL.Add('-SUM(smia.AreaP2) AREAP2, 0 PrcCusUni, ');
    {
    QrSumA2.SQL.Add('SUM(smia.Qtde) A_QTDE, SUM(smia.Pecas) A_PECAS,');
    QrSumA2.SQL.Add('SUM(smia.Peso) A_PESO, SUM(smia.AreaM2) A_AREAM2,');
    QrSumA2.SQL.Add('SUM(smia.AreaP2) A_AREAP2, ');
    QrSumA2.SQL.Add('0 A_QTDE, 0 A_PECAS, 0 A_PESO, 0 A_AREAM2, 0 A_AREAP2, ');
    }
    QrSumA2.SQL.Add('smia.GraGruX, smia.StqCenCad, ');
    if CkStqCenCad.Checked then
      QrSumA2.SQL.Add('scc.Nome, ')
    else
      QrSumA2.SQL.Add('"TODOS", ');
    QrSumA2.SQL.Add('gg1.NCM');
    QrSumA2.SQL.Add('FROM stqmovitsa smia');
    QrSumA2.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrSumA2.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
    QrSumA2.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
    QrSumA2.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
    QrSumA2.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrSumA2.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrSumA2.SQL.Add('LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed');
    QrSumA2.SQL.Add('LEFT JOIN stqcencad  scc ON scc.Codigo=smia.StqCenCad');
    QrSumA2.SQL.Add('WHERE smia.Ativo=1');
    QrSumA2.SQL.Add('AND smia.ValiStq <> 101');  // 2020-11-22
    //
    if Empresa <> 0 then
      QrSumA2.SQL.Add('AND smia.Empresa=' + FormatFloat('0', DModG.QrEmpresasCodigo.Value));
    //
    if GraGru1 <> 0 then
      QrSumA2.SQL.Add('AND ggx.GraGru1=' + FormatFloat('0', QrGraGru1Nivel1.Value))
    else
    if GraGru2 <> 0 then
      QrSumA2.SQL.Add('AND ggx.GraGru2=' + FormatFloat('0', QrGraGru2Nivel2.Value))
    else
    if GraGru3 <> 0 then
      QrSumA2.SQL.Add('AND ggx.GraGru3=' + FormatFloat('0', QrGraGru3Nivel3.Value))
    else
    if PrdGrupTip <> 0 then
      QrSumA2.SQL.Add('AND gg1.PrdGrupTip=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
    QrSumA2.SQL.Add('');
    //
    if CkStqCenCad.Checked then
      QrSumA2.SQL.Add('GROUP BY smia.GraGruX, smia.StqCenCad')
    else
      QrSumA2.SQL.Add('GROUP BY smia.GraGruX');
    QrSumA2.ExecSQL;
    //
    MyObjects.Informa2(LaAviso1B, LaAviso1B, False, '...');
    PB1.Position := 0;

    //

    QrGG2.ExecSQL;
    QrCPR.Close;
    UnDmkDAC_PF.AbreQuery(QrCPR, Dmod.MyDB);
    PageControl1.ActivePageIndex := 6;
    PageControl2.ActivePageIndex := 6;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGraImpLista.BitBtn5Click(Sender: TObject);
begin
  if not FSelecionouGG1 then
  begin
    UCriar.RecriaTempTableNovo(ntrttPesqESel, DmodG.QrUpdPID1, False, 1, 'pesqesel_psq');
    UCriar.RecriaTempTableNovo(ntrttPesqESel, DmodG.QrUpdPID1, False, 1, 'pesqesel_sel');
    FSelecionouGG1 := True;
  end;
  UMyMod.PesquisaESeleciona('gragru1', 'Nome', 'Nivel1');
  QrPesqESel_Sel.Close;
  UnDmkDAC_PF.AbreQuery(QrPesqESel_Sel, DModG.MyPID_DB);
  if QrPesqESel_Sel.RecordCount > 0 then
    CkSelecao0.Checked := True;
end;

procedure TFmGraImpLista.BtCorrige5Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCorrige5, BtCorrige5);
end;

procedure TFmGraImpLista.BtExcluiIncondClick(Sender: TObject);
var
  sIDCtrl: String;
begin
  if (QrSMIA3.State <> dsInactive) and (QrSMIA3.RecordCount > 0)  then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
    //
    if Geral.MB_Pergunta(
    'Deseja realmente excluir incondicionalmente o registro selecionado?') =
    ID_YES then
    begin
      sIDCtrl := Geral.FF0(QrSMIA3IDCtrl.Value);
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsa WHERE IDCtrl=' + sIDCtrl);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovvala WHERE IDCtrl=' + sIDCtrl);
      //
      BtPesquisa3Click(Self);
    end;
  end else
    Geral.MB_Aviso('N�o h� itens pesquisados!');
end;

procedure TFmGraImpLista.BtExporta2Click(Sender: TObject);
  procedure ItemNaoExportado(CodMot, TxtMot: String);
  const
    Exportado = 0;
  var
    //EntiSitio, SitProd, GraGruX, Empresa, StqCenCad,
    My_Idx: Integer;
  begin
    FExportarItem := False;
    {
    EntiSitio := QrSMIC2EntiSitio.Value;
    SitProd   := QrSMIC2SitProd  .Value;
    GraGruX   := QrSMIC2GraGruX  .Value;
    Empresa   := QrSMIC2Empresa  .Value;
    StqCenCad := QrSMIC2StqCenCad.Value;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, '_smic2_', False, [
      'Exportado', 'MotivoNE'
    ], [
      'EntiSitio', 'SitProd', 'GraGruX', 'Empresa', 'StqCenCad'
    ], [
      Exportado, MotivoNE
    ], [
      EntiSitio, SitProd, GraGruX, Empresa, StqCenCad
    ], False);
    }
    My_Idx := QrSMIC2My_Idx.Value;
    UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, '_smic2_', False, [
      'Exportado'], ['My_Idx'], [Exportado], [My_Idx], False);
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stIns, 'listerr1', False, [
    'Codigo', 'Texto1'], ['My_Idx'], [
    CodMot, TxtMot], [My_Idx], False);
    //
  end;
const
  ICMS_A_Recuperar = 0;
var
Fld001_008,
Fld009_004,
Fld013_004,
Fld017_020,
Fld037_001,
Fld038_014,
Fld052_020,
Fld072_002,
//
Fld074_021,
Fld095_017,
Fld112_017,
Fld129_017,
Fld146_060,
Fld206_080,
Fld286_004,
Fld290_010,
Fld300_030,
Fld330_003,
Fld333_030,
  Linha,
  CNPJ, IE, UF, Terceiro, NomeReduzido, Observacao, NCM, Txt: String;
  //
  //, MotivoNE, EntiSitio, GraGruX, Empresa, StqCenCad
  PT, SitProd, Exportados: Integer;
begin
  MeExp.Text := '';
  PageControl3.ActivePageIndex := 0;
  Exportados := 0;
  Observacao := '';
  DmProd.FListErr1 := UCriar.RecriaTempTable('ListErr1', DmodG.QrUpdPID1, False);
  DmProd.FLayout001 := UCriar.RecriaTempTable('Layout001', DmodG.QrUpdPID1, False);
  //
  QrSMIC2.Close;
  UnDmkDAC_PF.AbreQuery(QrSMIC2, DModG.MyPID_DB);
  QrSMIC2.First;
  while not QrSMIC2.Eof do
  begin
    FExportarItem := True;
    // Pr�prio X Terceiro:
    PT := 0;
    if QrSMIC2Empresa.Value = DModG.QrEmpresasCodigo.Value then PT := 10;
    if QrSMIC2EntiSitio.Value = DModG.QrEmpresasCodigo.Value then PT := PT + 1;
    //
    case PT of
      // de terceiros com terceiros (inv�lido)
      0: SitProd := 0;
      // de terceiros com a empresa (3)
      1: SitProd := 3;
      // da empresa com terceiros (2)
      10,11:
      begin
        // case (4) em transito, ou (5) inaproveit�vel
        case QrSMIC2SitProd.Value of
          1,2,3: if PT = 10 then SitProd := 2 else SitProd := 1;
          4,5: SitProd := QrSMIC2SitProd.Value;
          else SitProd := -1; // desconhecido
        end;
      end;
      else SitProd := -1; // desconhecido!
    end;
    case SitProd of
      -1: ItemNaoExportado('037.001.-01', 'Produto de terceiro em poder de terceiro');
       0: ItemNaoExportado('037.001.000', 'Situa��o do produto n�o definida (se � pr�prio e com quem est�)');
       1..5: (* Nada *);
       else ItemNaoExportado('037.001.' + FormatFloat('000', Sitprod), 'Situa��o do produto desconhecida');
     end;
    //
    if SitProd in ([2,3]) then //  n�o tem terceiro
    begin
      CNPJ := '';
      IE   := '';
      UF   := '';
    end else begin
      if QrSMIC2Empresa.Value <> DModG.QrEmpresasCodigo.Value then
      begin
        CNPJ := QrSMIC2EMP_CNPJ.Value;
        IE   := Geral.SoNumero_TT(QrSMIC2EMP_IE.Value);
        UF   := QrSMIC2EMP_NO_UF.Value;
        Terceiro := IntToStr(QrSMIC2Empresa.Value);
      end else begin
        CNPJ := QrSMIC2ENS_CNPJ.Value;
        IE   := Geral.SoNumero_TT(QrSMIC2ENS_IE.Value);
        UF   := QrSMIC2ENS_NO_UF.Value;
        Terceiro := IntToStr(QrSMIC2EntiSitio.Value);
      end;
      if Length(CNPJ) <> 14 then ItemNaoExportado('038.014.000', 'CNPJ de terceiro (c�digo = ' + Terceiro + ') n�o definido');
      if Length(IE) = 0     then ItemNaoExportado('052.020.000', 'I.E. de terceiro (c�digo = ' + Terceiro + ') n�o definido');
      if Length(UF) <> 2    then ItemNaoExportado('072.002.000', 'UF de terceiro (c�digo = ' + Terceiro + ') n�o definido');
    end;
    //  Parei aqui Ver se faz pelo GraGru1 ou pelo graGruX!
    if QrSMIC2GraGruX.Value = 0 then
      ItemNaoExportado('017.020.000', 'Produto sem c�digo de reduzido');
    if QrSMIC2QTDE.Value <= 0 then
      ItemNaoExportado('074.021.000', 'Quantidade do item zerado ou negativo');
    if QrSMIC2PrcCusUni.Value <= 0 then
    begin
      if (*(QrSMIC2TPC_GCP_ID.Value = 0) and*) (QrSMIC2TPC_GCP_TAB.Value = 0) then
      begin
        if QrSMIC2.FieldByName('TPC_GCP_TAB').AsString = '' then // C�digo nulo
          ItemNaoExportado('095.017.001', 'Pre�o n�o definido na lista')
        else
          ItemNaoExportado('095.017.001', 'Lista de pre�os n�o definida');
      end else
        ItemNaoExportado('095.017.000', 'Valor unit�rio do item zerado ou negativo');
    end;
    //
    NomeReduzido := DmProd.FormaNomeProduto(QrSMIC2NO_PRD.Value, QrSMIC2NO_TAM.Value,
      QrSMIC2NO_COR.Value, QrSMIC2PrintTam.Value, QrSMIC2PrintCor.Value);
    //
    if Trim(NomeReduzido) = '' then
      ItemNaoExportado('206.080.000', 'Nome do item n�o definido');
    //
    NCM := Geral.SoNumero_TT(QrSMIC2NCM.Value);
    if Length(NCM) < 7 then
      ItemNaoExportado('290.010.000', 'NCM inv�lido: ' + QrSMIC2NCM.Value);
    //
    if Trim(QrSMIC2SIGLA.Value) = '' then
      ItemNaoExportado('330.003.000', 'Unidade de medida n�o definida');
    //
    Fld001_008 := VeriStr(008, 01, FormatDateTime('ddmmyyyy', TPDataFim2.Date));
    Fld009_004 := VeriStr(004, 02, FormatDateTime('mmyy', TPDataFim2.Date));
    Fld013_004 := VeriStr(004, 03, FormatDateTime('mmyy', TPDataFim2.Date));
    Fld017_020 := VeriStr(020, 04, Geral.CompletaString2(IntToStr(QrSMIC2GraGruX.Value), ' ', 20, taLeftJustify, False, ''));
    Fld037_001 := VeriStr(001, 05, FormatFloat('0', SitProd(*2011-03-18 QrSMIC2SitProd.Value*)));
    Fld038_014 := VeriStr(014, 06, Geral.CompletaString2(CNPJ, '0', 14, taRightJustify, False, ''));
    Fld052_020 := VeriStr(020, 07, Geral.CompletaString2(IE, ' ', 20, taLeftJustify, False, ''));
    Fld072_002 := VeriStr(002, 08, Geral.CompletaString2(UF, ' ', 2, taLeftJustify, False, ''));
    //            VeriStr(
    Fld074_021 := VeriStr(021, 09, Geral.FTX(QrSMIC2QTDE.Value, 15, 6, siPositivo));
    Fld095_017 := VeriStr(017, 10, Geral.FTX(QrSMIC2PrcCusUni.Value, 13, 4, siPositivo));
    Fld112_017 := VeriStr(017, 11, Geral.FTX(QrSMIC2ValorTot.Value * QrSMIC2QTDE.Value, 15, 2, siPositivo));
    Fld129_017 := VeriStr(017, 12, Geral.FTX(ICMS_A_Recuperar, 15, 2, siPositivo));
    Fld146_060 := VeriStr(060, 13, Geral.CompletaString2(Observacao, ' ', 60, taLeftJustify, False, ''));
    Fld206_080 := VeriStr(080, 14, Geral.CompletaString2(NomeReduzido, ' ', 80, taLeftJustify, False, ''));
    Fld286_004 := VeriStr(004, 15, Geral.FTX(QrSMIC2PrdGrupTip.Value, 4, 0, siNegativo));  // por causa dos negativos!
    Fld290_010 := VeriStr(010, 16, Geral.CompletaString2(NCM, '0', 10, taLeftJustify, False, ''));
    Fld300_030 := VeriStr(030, 17, Geral.CompletaString2('', ' ', 30, taLeftJustify, False, ''));
    Fld330_003 := VeriStr(003, 18, Geral.CompletaString2(QrSMIC2SIGLA.Value, ' ', 3, taLeftJustify, True, ''));
    Fld333_030 := VeriStr(030, 19, Geral.CompletaString2(QrSMIC2NO_PGT.Value, ' ', 30, taLeftJustify, True, ''));
    //
    if FExportarItem then
    begin
      Linha :=
      Fld001_008 +
      Fld009_004 +
      Fld013_004 +
      Fld017_020 +
      Fld037_001 +
      Fld038_014 +
      Fld052_020 +
      Fld072_002 +
      //         +
      Fld074_021 +
      Fld095_017 +
      Fld112_017 +
      Fld129_017 +
      Fld146_060 +
      Fld206_080 +
      Fld286_004 +
      Fld290_010 +
      Fld300_030 +
      Fld330_003 +
      Fld333_030;
      if Length(Linha) <> 362 then
        Geral.MB_Aviso('A linha ' + IntToStr(MeExp.Lines.Count + 1) +
        ' n�o tem 362 caracteres!' + sLineBreak + 'Ela tem ' + IntToStr(Length(Linha)) +
        ' caracteres.');
      MeExp.Lines.Add(Linha);
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stIns, 'layout001', False, [
      'DataInvent', 'MesAnoInic', 'MesAnoFina',
      'CodigoProd', 'SituacProd', 'CNPJTercei',
      'IETerceiro', 'UFTerceiro', 'Quantidade',
      'ValorUnita', 'ValorTotal', 'ICMSRecupe',
      'Observacao', 'DescriProd', 'GrupoProdu',
      'ClassifNCM', 'RESERVADOS', 'UnidMedida',
      'DescrGrupo'], [
      ], [
      {
      DataInvent, MesAnoInic, MesAnoFina,
      CodigoProd, SituacProd, CNPJTercei,
      IETerceiro, UFTerceiro, Quantidade,
      ValorUnita, ValorTotal, ICMSRecupe,
      Observacao, DescriProd, GrupoProdu,
      ClassifNCM, RESERVADOS, UnidMedida,
      DescrGrupo
      }
      Fld001_008, Fld009_004, Fld013_004,
      Fld017_020, Fld037_001, Fld038_014,
      Fld052_020, Fld072_002, Fld074_021,
      Fld095_017, Fld112_017, Fld129_017,
      Fld146_060, Fld206_080, Fld286_004,
      Fld290_010, Fld300_030, Fld330_003,
      Fld333_030
      ], [
      ], False);
      Exportados := Exportados + 1;      
    end;
    //
    QrSMIC2.Next;
  end;
  PageControl3.ActivePageIndex := 1;
  Update;
  Application.ProcessMessages;
  QrExport2.Close;
  UnDmkDAC_PF.AbreQuery(QrExport2, DModG.MyPID_DB);
  if QrExport2.RecordCount > 0 then
  begin
    PageControl3.ActivePageIndex := 2;
    if Exportados = 0 then
      Txt := 'Nenhum item pesquisado foi exportado'
    else begin
      if Exportados = 1 then
        Txt := 'Um item dos ' + IntToStr(QrSMIC2.RecordCount) +
        ' pesquisados foi exportado!' + sLineBreak +
        'Os demais itens n�o foram exportados'
      else
        Txt := IntToStr(Exportados) + ' itens dos ' +
        IntToStr(QrSMIC2.RecordCount) + ' pesquisados foram exportados!' +
        sLineBreak + 'Os demais itens n�o foram exportados';
    end;
    //
    Txt := Txt + ' por falhas que devem ser corrigidas!';
    Geral.MB_Aviso(Txt);
  end;
  //
  QrLayout001.Close;
  UnDmkDAC_PF.AbreQuery(QrLayout001, DModG.MyPID_DB);
  //
  if DBCheck.CriaFm(TFmCfgExpFile, FmCfgExpFile, afmoNegarComAviso) then
  begin
    FmCfgExpFile.Memo.WantReturns := MeExp.WantReturns;
    FmCfgExpFile.Memo.WantTabs    := MeExp.WantTabs;
    FmCfgExpFile.Memo.WordWrap    := MeExp.WordWrap;
    FmCfgExpFile.Memo.Text        := MeExp.Text;
    //
    FmCfgExpFile.EdDir.Text       := 'C:\Dermatek\Inventario\' +
                                     FormatDateTime('YYYY', TPDataFim2.Date);
    FmCfgExpFile.EdArq.Text       := Geral.SoNumero_TT(
                                     DModG.QrEmpresasCNPJ_CPF.Value) + '_' +
                                     FormatDateTime('YYYYMM', TPDataFim2.Date);
    FmCfgExpFile.EdExt.Text       := 'txt';
    //
    FmCfgExpFile.ShowModal;
    FmCfgExpFile.Destroy;
  end;

  //
{
  QrStqCenCad.Close;
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  QrGSS.First;
  while not QrGSS.Eof do
  begin
    if QrStqCenCadCodigo.Value <> QrGSSStqCenCad.Value then
    begin
      if not QrStqCenCad.Locate('Codigo', QrGSSStqCenCad.Value, []) then
      begin
        Geral.MB_('O centro de estoque de ID = ' + FormatFloat('0',
        QrGSSStqCenCad.Value) + ' n�o foi localizado na base de dados!' +
        sLineBreak + 'Exporta��o abortada!', 'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    if QrStqCenCadSitProd.Value < 1 then
    begin
      Geral.MB_('Situa��o de produto inv�lida  para o centro de estoque '
      + FormatFloat('0', QrGSSStqCenCad.Value) + '!' +
      sLineBreak + 'Exporta��o abortada!', 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
    if QrStqCenCadEntiSitio.Value = EdEmpresa.ValueVariant then
    begin
      CNPJ := '';
      IE   := '';
      UF   := '';
    end else begin
      CNPJ := QrStqCenCadCNPJ.Value;
      IE   := Geral.SoNumero_TT(QrStqCenCadCNPJ.Value);
      UF   := QrStqCenCadNO_UF.Value;
    end;
    Fld001_008 := VeriStr(008, 01, FormatDateTime('ddmmyyyy', TPDataFim2.Date));
    Fld009_004 := VeriStr(004, 02, FormatDateTime('mmyy', TPDataFim2.Date));
    Fld013_004 := VeriStr(004, 03, FormatDateTime('mmyy', TPDataFim2.Date));
    Fld017_020 := VeriStr(020, 04, Geral.FTX(QrGSSGraGru1.Value, 20, 0, False)); // GraGruX ou GraGru1 ???
    Fld037_001 := VeriStr(001, 05, FormatFloat('0', QrStqCenCadSitProd.Value));
    Fld038_014 := VeriStr(014, 06, Geral.CompletaString2(CNPJ, '0', 14, taRightJustify, False, ''));
    Fld052_020 := VeriStr(020, 07, Geral.CompletaString2(IE, ' ', 20, taLeftJustify, False, ''));
    Fld072_002 := VeriStr(002, 08, Geral.CompletaString2(UF, ' ', 2, taLeftJustify, False, ''));
    //            VeriStr(
    Fld074_021 := VeriStr(021, 09, Geral.FTX(QrGSSQTDE.Value, 15, 6, False));
    Fld097_017 := VeriStr(017, 10, Geral.FTX(11.11, 13, 4, False));
    Fld112_017 := VeriStr(017, 11, Geral.FTX(11.11 * QrGSSQTDE.Value, 15, 2, False));
    Fld129_017 := VeriStr(017, 12, Geral.FTX(0, 15, 2, False));
    Fld146_060 := VeriStr(060, 13, Geral.CompletaString2('', ' ', 60, taLeftJustify, False, ''));
    Fld206_080 := VeriStr(080, 14, Geral.CompletaString2(QrGSSNO_PRD.Value, ' ', 80, taLeftJustify, False, ''));
    Fld286_004 := VeriStr(004, 15, Geral.FTX(1, 4, 0, False));
    Fld290_010 := VeriStr(010, 16, Geral.CompletaString2(Geral.SoNumero_TT(QrGSSNCM.Value), '0', 10, taLeftJustify, False, ''));
    Fld300_030 := VeriStr(030, 17, Geral.CompletaString2('', ' ', 30, taLeftJustify, False, ''));
    Fld330_003 := VeriStr(003, 18, Geral.CompletaString2(QrGSSSIGLA.Value, ' ', 3, taLeftJustify, True, ''));
    Fld333_030 := VeriStr(030, 19, Geral.CompletaString2(QrGSSNO_PGT.Value, ' ', 30, taLeftJustify, True, ''));
    //
    Linha :=
    Fld001_008 +
    Fld009_004 +
    Fld013_004 +
    Fld017_020 +
    Fld037_001 +
    Fld038_014 +
    Fld052_020 +
    Fld072_002 +
    //         +
    Fld074_021 +
    Fld097_017 +
    Fld112_017 +
    Fld129_017 +
    Fld146_060 +
    Fld206_080 +
    Fld286_004 +
    Fld290_010 +
    Fld300_030 +
    Fld330_003 +
    Fld333_030;
    if Length(Linha) <> 362 then
      Geral.MB_('A linha ' + IntToStr(MeExp.Lines.Count + 1) +
      ' n�o tem 362 caracteres!' + sLineBreak + 'Ela tem ' + IntToStr(Length(Linha)) +
      ' caracteres.', 'Aviso', MB_OK+MB_ICONWARNING);
    MeExp.Lines.Add(Linha);
    QrGSS.Next;
  end;
}
end;

procedure TFmGraImpLista.BtImprime0Click(Sender: TObject);
begin
  DBGPrdEstq.DataSource := nil;
  QrPrdEstq.DisableControls;
  try
    if CkPrdGrupo.Checked then
    begin
        MyObjects.frxMostra(frxPRD_PRINT_001_00E_3, 'Estoque de Produto');
    end else
    begin
      if CkListaReduzidos.Checked then
        MyObjects.frxMostra(frxPRD_PRINT_001_00L, 'Lista de Reduzidos')
      else
        MyObjects.frxMostra(frxPRD_PRINT_001_00E_2, 'Estoque de Produto');
    end;
  finally
    QrPrdEstq.EnableControls;
    DBGPrdEstq.DataSource := DsPrdEstq;
  end;
end;

procedure TFmGraImpLista.BtImprime1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxPRD_PRINT_001_01, 'Hist�rico de Produto');
end;

{
procedure TFmGraImpLista.BtPesquisa0Click(Sender: TObject);
var
  Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip, GraCusPrc, StqCenCad: Integer;
begin
  CorrigeBaixa();
  //
  if CGSitEstq.Value = 0 then
  begin
    Geral.MB_Aviso('Informe pelo menos uma "Situa��o do estoque"!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  PB1.Position := 0;
  DBGPrdEstq.Visible := False;
  QrSumA2.DisableControls;
  //
  Empresa    := EdEmpresa.ValueVariant;
  GraGru1    := EdGraGru1.ValueVariant;
  GraGru2    := EdGraGru2.ValueVariant;
  GraGru3    := EdGraGru3.ValueVariant;
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  //
  if Empresa = 0 then
  begin
    Geral.MB_Aviso('Defina a empresa!');
    Exit;
  end;
  if CBGraCusPrc.KeyValue <> NULL then
    GraCusPrc := QrGraCusPrcCodigo.Value
  else
    GraCusPrc := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1B, LaAviso1B, True, 'Pesquisando');
    //
    //FGGX_SCC_Stq := UCriar.RecriaTempTable('GGX_SCC_Stq', DmodG.QrUpdPID1, False);
    FGGX_SCC_Stq := GradeCriar.RecriaTempTableNovo(ntrttGGX_SCC_Stq, DModG.QrUpdPID1, False);
    QrSumA2.Close;
    QrSumA2.Database := Dmod.MyDB;
    QrSumA2.SQL.Clear;
(*
    QrSumA2.SQL.Add('INSERT INTO ' + DmodG.MyPID_DB.DataBaseName + '.' + FGGX_SCC_Stq);
    QrSumA2.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
    QrSumA2.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
    QrSumA2.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
    QrSumA2.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,');
    QrSumA2.SQL.Add('SUM(smia.Qtde) QTDE, SUM(smia.Pecas) PECAS, ');
    QrSumA2.SQL.Add('SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2, ');
    QrSumA2.SQL.Add('SUM(smia.AreaP2) AREAP2, smia.GraGruX, gg1.NCM');
    QrSumA2.SQL.Add('FROM stqmovitsa smia');
    QrSumA2.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrSumA2.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
    QrSumA2.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
    QrSumA2.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
    QrSumA2.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrSumA2.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrSumA2.SQL.Add('LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed');
*)
    QrSumA2.SQL.Add('INSERT INTO ' + DmodG.MyPID_DB.DataBaseName + '.' + FGGX_SCC_Stq);
    QrSumA2.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
    QrSumA2.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
    QrSumA2.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
    QrSumA2.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,');
    (*
    Alterado em 22/04/2014 foi corrigido o campo baixa
    QrSumA2.SQL.Add('SUM(smia.Qtde) QTDE, SUM(smia.Pecas) PECAS,');
    *)
    QrSumA2.SQL.Add('SUM(smia.Qtde * smia.Baixa) QTDE, SUM(smia.Pecas) PECAS,'); //N�o estava funcionando agora est�
    QrSumA2.SQL.Add('SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2,');
    QrSumA2.SQL.Add('SUM(smia.AreaP2) AREAP2, ggv.CustoPreco PrcCusUni, ');
    (*
    QrSumA2.SQL.Add('SUM(smia.Qtde) A_QTDE, SUM(smia.Pecas) A_PECAS,');
    QrSumA2.SQL.Add('SUM(smia.Peso) A_PESO, SUM(smia.AreaM2) A_AREAM2,');
    QrSumA2.SQL.Add('SUM(smia.AreaP2) A_AREAP2, ');
    QrSumA2.SQL.Add('0 B_QTDE, 0 B_PECAS, 0 B_PESO, 0 B_AREAM2, 0 B_AREAP2, ');
    *)
    QrSumA2.SQL.Add('smia.GraGruX, smia.StqCenCad, ');
    //
    if (CkStqCenCad.Checked) or (StqCenCad <> 0) then
      QrSumA2.SQL.Add('scc.Nome, ')
    else
      QrSumA2.SQL.Add('"TODOS", ');
    //
    QrSumA2.SQL.Add('gg1.NCM');
    QrSumA2.SQL.Add('FROM stqmovitsa smia');
    QrSumA2.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrSumA2.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
    QrSumA2.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
    QrSumA2.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
    QrSumA2.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrSumA2.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrSumA2.SQL.Add('LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed');
    QrSumA2.SQL.Add('LEFT JOIN stqcencad  scc ON scc.Codigo=smia.StqCenCad');
    //
    //
    QrSumA2.SQL.Add('LEFT JOIN gragruval  ggv ON ggv.GraGruX=smia.GraGruX');
    // Evitar n�o encontrar registros sem pre�o!
    QrSumA2.SQL.Add('          AND ggv.GraCusPrc=' + FormatFloat('0', GraCusPrc));
    QrSumA2.SQL.Add('          AND ggv.Entidade=' + FormatFloat('0', DModG.QrEmpresasCodigo.Value));
    //
    //
    //QrSumA2.SQL.Add('WHERE smia.Ativo=1');
    //QrSumA2.SQL.Add('AND smia.Baixa=1'); ver acima baixa * Qtde!!!
    (*
    Alterado em 22/04/2014 foi corrigido o campo baixa
    QrSumA2.SQL.Add('WHERE smia.Baixa=1');
    *)
    QrSumA2.SQL.Add('WHERE smia.Ativo=1');
    QrSumA2.SQL.Add('AND smia.ValiStq <> 101');  // 2020-11-22

    //QrSumA2.SQL.Add('AND ggv.GraCusPrc=' + FormatFloat('0', GraCusPrc));
    //
    if Empresa <> 0 then
      QrSumA2.SQL.Add('AND smia.Empresa=' + FormatFloat('0', DModG.QrEmpresasCodigo.Value));
    //
    if CkSelecao0.Enabled and CkSelecao0.Checked then
    begin
      QrSumA2.SQL.Add('AND ggx.GraGru1 IN (');
      QrSumA2.SQL.Add('  SELECT Codigo ');
      QrSumA2.SQL.Add('  FROM ' + VAR_MyPID_DB_NOME + '.pesqesel_sel');
      QrSumA2.SQL.Add(')');
    end else
    begin
      if GraGru1 <> 0 then
        QrSumA2.SQL.Add('AND ggx.GraGru1=' + FormatFloat('0', QrGraGru1Nivel1.Value));
      if GraGru2 <> 0 then
        QrSumA2.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', QrGraGru2Nivel2.Value));
      if GraGru3 <> 0 then
        QrSumA2.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', QrGraGru3Nivel3.Value));
      if PrdGrupTip <> 0 then
        QrSumA2.SQL.Add('AND gg1.PrdGrupTip=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
      if StqCenCad <> 0 then
        QrSumA2.SQL.Add('AND smia.StqCenCad=' + FormatFloat('0', QrStqCenCadPsqCodigo.Value));
    end;
    if CkPrdGrupo.Checked = True then
    begin
      if CkStqCenCad.Checked then
        QrSumA2.SQL.Add('GROUP BY gg1.Nivel1, smia.StqCenCad')
      else
        QrSumA2.SQL.Add('GROUP BY gg1.Nivel1');
    end else
    begin
      if CkStqCenCad.Checked then
        QrSumA2.SQL.Add('GROUP BY smia.GraGruX, smia.StqCenCad')
      else
        QrSumA2.SQL.Add('GROUP BY smia.GraGruX');
    end;
    QrSumA2.SQL.Add('ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX');
    QrSumA2.SQL.Add('');
    FAgora := Now();
    QrSumA2.ExecSQL;
    //Geral.MB_Teste(QrSumA2.SQl.Text);

    MyObjects.Informa2(LaAviso1B, LaAviso1B, True, 'Abrindo tabela');
    //
    QrPrdEstq.Close;
    QrPrdEstq.Database := DmodG.MyPID_DB;
    QrPrdEstq.SQL.Clear;
    QrPrdEstq.SQL.Add('SELECT stq.*, gg1.CodUsu ');
    QrPrdEstq.SQL.Add('FROM ' + FGGX_SCC_Stq + ' stq');
    QrPrdEstq.SQL.Add('LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1 = stq.Nivel1 ');
    case CGSitEstq.Value of
      1: QrPrdEstq.SQL.Add('WHERE QTDE < 0');
      2: QrPrdEstq.SQL.Add('WHERE QTDE = 0');
      3: QrPrdEstq.SQL.Add('WHERE QTDE <= 0');
      4: QrPrdEstq.SQL.Add('WHERE QTDE > 0');
      5: QrPrdEstq.SQL.Add('WHERE QTDE <> 0');
      6: QrPrdEstq.SQL.Add('WHERE QTDE >= 0');
      7: QrPrdEstq.SQL.Add(''); // Tudo
    end;
    UnDmkDAC_PF.AbreQuery(QrPrdEstq, DModG.MyPID_DB);
    //
    MyObjects.Informa2(LaAviso1B, LaAviso1B, False, '...');
    PB1.Position := 0;
    QrSumA2.EnableControls;
    DBGPrdEstq.Visible := True;
  finally
    if CGSitEstq.Value = 0 then
    begin
      QrPrdEstq.EnableControls;
      DBGPrdEstq.DataSource := DsPrdEstq;
    end;
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmGraImpLista.BtPesquisa0Click(Sender: TObject);
const
  InatIncTipo = 0;
  InatIncOriCodi = 0;
var
  Filial, Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip, GraCusPrc, StqCenCad: Integer;
  SQL_Sum, SQL_Its: String;
begin
  CorrigeBaixa();
  //
  if CGSitEstq.Value = 0 then
  begin
    Geral.MB_Aviso('Informe pelo menos uma "Situa��o do estoque"!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  PB1.Position := 0;
  DBGPrdEstq.Visible := False;
  QrSumA2.DisableControls;
  //
  Filial     := EdEmpresa.ValueVariant;
  GraGru1    := EdGraGru1.ValueVariant;
  GraGru2    := EdGraGru2.ValueVariant;
  GraGru3    := EdGraGru3.ValueVariant;
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  GraCusPrc  := EdGraCusPrc.ValueVariant;
  //
  if MyObjects.FIC(Filial = 0, EdEmpresa,'Defina a empresa!') then Exit;
  Empresa := DmodG.QrEmpresasCodigo.Value;
  //
  MyObjects.Informa2(LaAviso1B, LaAviso1B, True, 'Pesquisando');
  //
  if Grade_PF.SQL_Estoque(Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip,
  StqCenCad, GraCusPrc, CGSitEstq.Value, CkStqCenCad.Checked,
  CkSelecao0.Enabled, CkSelecao0.Checked, CkPrdGrupo.Checked,
  InatIncTipo, InatIncOriCodi, LaAviso1A, LaAviso1B, SQL_Sum, SQL_Its) then
  begin
    FAgora := Now();
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB, SQL_Sum);
    //Geral.MB_Teste(QrSumA2.SQl.Text);

    MyObjects.Informa2(LaAviso1B, LaAviso1B, True, 'Abrindo tabela');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrdEstq, DModG.MyPID_DB, [SQL_Its]);
    //
    MyObjects.Informa2(LaAviso1B, LaAviso1B, False, '...');
    QrSumA2.EnableControls;
    DBGPrdEstq.Visible := True;
    QrPrdEstq.EnableControls;
    DBGPrdEstq.DataSource := DsPrdEstq;
  end;
end;

procedure TFmGraImpLista.BtPesquisa1Click(Sender: TObject);
var
  Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip, GraGruX, IDCtrl, StqCenCad: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2: Double;
  LoteMP: String;
begin
  CorrigeBaixa();
  PB1.Position := 0;
  DBGPrdMov.Visible := False;
  QrPrdMov.DisableControls;
  //
  Empresa    := EdEmpresa.ValueVariant;
  GraGru1    := EdGraGru1.ValueVariant;
  GraGru2    := EdGraGru2.ValueVariant;
  GraGru3    := EdGraGru3.ValueVariant;
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  //
  if Empresa = 0 then
  begin
    Geral.MB_Aviso('Defina a empresa!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa(LaAviso3A, True, 'Pesquisando');
    //
    FPrdMov := UCriar.RecriaTempTable('PrdMov', DmodG.QrUpdPID1, False);
    //
    QrStqMovIts.Close;
    QrStqMovIts.Database := DmodG.MyPID_DB;
    QrStqMovIts.SQL.Clear;
    QrStqMovIts.SQL.Add('INSERT INTO ' + FPrdMov);
    QrStqMovIts.SQL.Add('SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
    QrStqMovIts.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
    QrStqMovIts.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
    QrStqMovIts.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.DataHora, ');
    QrStqMovIts.SQL.Add('smia.IDCtrl, smia.Tipo, smia.OriCodi,');
    QrStqMovIts.SQL.Add('smia.OriCtrl, smia.OriCnta, smia.OriPart,');
    QrStqMovIts.SQL.Add('smia.Empresa, smia.StqCenCad, ');
    // ini 2023-08-16
    QrStqMovIts.SQL.Add('smia.StqCenLoc, ');
    QrStqMovIts.SQL.Add('CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ');
    // fim 2023-08-16
    QrStqMovIts.SQL.Add('smia.GraGruX, ');
    QrStqMovIts.SQL.Add('smia.Pecas, smia.Peso, smia.AreaM2,');
    QrStqMovIts.SQL.Add('smia.AreaP2, (smia.Qtde * smia.Baixa) Qtde, smia.FatorClas,');
    QrStqMovIts.SQL.Add('0 SdoPecas, 0 SdoPeso, 0 SdoAreaM2, 0 SdoAreaP2, ');
    QrStqMovIts.SQL.Add('0 SdoQtde, "" LoteMP, smia.Ativo');
    QrStqMovIts.SQL.Add('');
    QrStqMovIts.SQL.Add('FROM '+TMeuDB+'.stqmovitsa smia');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux    ggx ON ggx.Controle=smia.GraGruX');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruc    ggc ON ggc.Controle=ggx.GraGruC');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.gratamits  gti ON gti.Controle=ggx.GraTamI');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.unidmed    med ON med.Codigo=gg1.UnidMed');
    // ini 2023-08-16
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smia.StqCenCad');
    QrStqMovIts.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcenloc scl ON scl.Codigo=smia.StqCenLoc');
    // fim 2023-08-16
    // ini 2020-12-11
    //QrStqMovIts.SQL.Add(dmkPF.SQL_Periodo('WHERE smia.DataHora ',
      //TPDataIni1.Date, TPDataFim1.Date + 1, True, True)); // +1 porque � dd/mm/yyyy hh:nn:ss
    QrStqMovIts.SQL.Add('WHERE smia.Ativo=1');
    QrStqMovIts.SQL.Add('AND smia.ValiStq <> 101');
    QrStqMovIts.SQL.Add(dmkPF.SQL_Periodo('AND smia.DataHora ',
      TPDataIni1.Date, TPDataFim1.Date + 1, True, True)); // +1 porque � dd/mm/yyyy hh:nn:ss
    // fim 2020-12-11

    if Empresa <> 0 then
      QrStqMovIts.SQL.Add('AND smia.Empresa=' + FormatFloat('0', DModG.QrEmpresasCodigo.Value));
    //
    if GraGru1 <> 0 then
      QrStqMovIts.SQL.Add('AND ggx.GraGru1=' + FormatFloat('0', QrGraGru1Nivel1.Value));
    if GraGru2 <> 0 then
      QrStqMovIts.SQL.Add('AND ggx.GraGru2=' + FormatFloat('0', QrGraGru2Nivel2.Value));
    if GraGru3 <> 0 then
      QrStqMovIts.SQL.Add('AND ggx.GraGru3=' + FormatFloat('0', QrGraGru3Nivel3.Value));
    if PrdGrupTip <> 0 then
      QrStqMovIts.SQL.Add('AND gg1.PrdGrupTip=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
    if StqCenCad <> 0 then
      QrStqMovIts.SQL.Add('AND smia.StqCenCad=' + FormatFloat('0', QrStqCenCadPsqCodigo.Value));
    QrStqMovIts.SQL.Add('');
    //QrStqMovIts.ExecSQL;
    UnDmkDAC_PF.ExecutaQuery(QrStqMovIts, DModG.MyPID_DB);
    //
    QrPrdMov.Close;
    UnDmkDAC_PF.AbreQuery(QrPrdMov, DModG.MyPID_DB);
    //
    MyObjects.Informa(LaAviso3A, True, 'Gerando saldos');
    PB1.Max := QrPrdMov.RecordCount;
    QrPrdMov.First;
    Qtde    := 0;
    Pecas   := 0;
    Peso    := 0;
    AreaM2  := 0;
    AreaP2  := 0;
    GraGruX := 0;
    while not QrPrdMov.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      //
      if QrPrdMovGraGruX.Value <> GraGruX then
      begin
        Qtde    := 0;
        Pecas   := 0;
        Peso    := 0;
        AreaM2  := 0;
        AreaP2  := 0;
        GraGruX := QrPrdMovGraGruX.Value;
      end;
      Qtde    := Qtde   + QrPrdMovQtde  .Value;
      Pecas   := Pecas  + QrPrdMovPecas .Value;
      Peso    := Peso   + QrPrdMovPeso  .Value;
      AreaM2  := AreaM2 + QrPrdMovAreaM2.Value;
      AreaP2  := AreaP2 + QrPrdMovAreaP2.Value;
      //
      IDCtrl  := QrPrdMovIDCtrl.Value;
      //
      LoteMP := '';
      case QrPrdMovTipo.Value of
        VAR_FATID_0101, VAR_FATID_0104:
        begin
          if CkInfoMP_e_Classif.Checked then
          begin
            QrLocLote.Close;
            QrLocLote.Params[0].AsInteger := QrPrdMovOriCodi.Value;
            UnDmkDAC_PF.AbreQuery(QrLocLote, Dmod.MyDB);
            //
            LoteMP := QrLocLoteLote.Value;
          end;
        end;
      end;
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'prdmov', False, [
        'SdoPecas', 'SdoPeso', 'SdoAreaM2', 'SdoAreaP2', 'SdoQtde', 'LoteMP'], [
        'IDCtrl'], [Pecas, Peso, AreaM2, AreaP2, Qtde, LoteMP], [IDCtrl], False);
      //
      QrPrdMov.Next;
    end;
    //
    QrPrdMov.Close;
    UnDmkDAC_PF.AbreQuery(QrPrdMov, DModG.MyPID_DB);
    MyObjects.Informa(LaAviso3A, False, '...');
    PB1.Position := 0;
    QrPrdMov.EnableControls;
    DBGPrdMov.Visible := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGraImpLista.BtPesquisa3Click(Sender: TObject);
var
  Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip, StqCenCad: Integer;
begin
  PB1.Position := 0;
  DBGPrdEstq.Visible := False;
  QrPrdEstq.DisableControls;
  //
  Empresa    := EdEmpresa.ValueVariant;
  GraGru1    := EdGraGru1.ValueVariant;
  GraGru2    := EdGraGru2.ValueVariant;
  GraGru3    := EdGraGru3.ValueVariant;
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  //
  if Empresa = 0 then
  begin
    Geral.MB_Aviso('Defina a empresa!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  QrSMIA3.Close;
  QrSMIA3.Database := Dmod.MyDB;
  QrSMIA3.SQL.Clear;
  QrSMIA3.SQL.Add('SELECT vfi.Nome NO_VarFatId, gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,');
  QrSMIA3.SQL.Add('gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,');
  QrSMIA3.SQL.Add('gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,');
  QrSMIA3.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,');
  QrSMIA3.SQL.Add('smia.IDCtrl, smia.Tipo, smia.OriCodi,');
  QrSMIA3.SQL.Add('smia.OriCtrl, smia.OriCnta, smia.OriPart,');
  QrSMIA3.SQL.Add('smia.Empresa, smia.StqCenCad, smia.GraGruX,');
  QrSMIA3.SQL.Add('smia.Pecas, smia.Peso, smia.AreaM2,');
  QrSMIA3.SQL.Add('smia.AreaP2, (smia.Qtde * smia.Baixa) Qtde, smia.Baixa, smia.FatorClas,');
  QrSMIA3.SQL.Add('smia.Ativo, smia.DataHora, ');
  QrSMIA3.SQL.Add('ELT(smia.Baixa+2,"Baixado","Nulo","Adicionado") BAIXA_TXT ');
  QrSMIA3.SQL.Add('FROM stqmovits' + Lowercase(RGSMI.Items[RGSMI.ItemIndex][1]) + ' smia');
  QrSMIA3.SQL.Add('LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX');
  QrSMIA3.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  QrSMIA3.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  QrSMIA3.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  QrSMIA3.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrSMIA3.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
  QrSMIA3.SQL.Add('LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed');
  QrSMIA3.SQL.Add('LEFT JOIN varfatid   vfi ON vfi.Codigo=smia.Tipo');
  QrSMIA3.SQL.Add('WHERE smia.Empresa=' + FormatFloat('0', DModG.QrEmpresasCodigo.Value));
  QrSMIA3.SQL.Add(dmkPF.SQL_Periodo('AND smia.DataHora ', TPDataIni3.Date,
    TPDataFim3.Date, True, True));
  if GraGru1 <> 0 then
    QrSMIA3.SQL.Add('AND ggx.GraGru1=' + FormatFloat('0', QrGraGru1Nivel1.Value));
  if GraGru2 <> 0 then
    QrSMIA3.SQL.Add('AND ggx.GraGru2=' + FormatFloat('0', QrGraGru2Nivel2.Value));
  if GraGru3 <> 0 then
    QrSMIA3.SQL.Add('AND ggx.GraGru3=' + FormatFloat('0', QrGraGru3Nivel3.Value));
  if PrdGrupTip <> 0 then
    QrSMIA3.SQL.Add('AND gg1.PrdGrupTip=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
  if StqCenCad <> 0 then
    QrSMIA3.SQL.Add('AND smia.StqCenCad=' + FormatFloat('0', QrStqCenCadPsqCodigo.Value));
  QrSMIA3.SQL.Add('');
  //
  QrSMIA3.SQL.Add('ORDER BY DataHora DESC');
  UnDmkDAC_PF.AbreQuery(QrSMIA3, Dmod.MyDB);
  //Geral.MB_Teste(QrSMIA3.SQL.Text);
  Screen.Cursor := crDefault;
end;

procedure TFmGraImpLista.BtPesquisa5Click(Sender: TObject);
var
  Bx_Qtde, Bx_Pecas, Bx_Peso, Er_Qtde, Er_Pecas, Er_Peso: Double;
  IDCtrl: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    FSmiaMPIn := UCriar.RecriaTempTableNovo(ntrttSmiaMPIn, DmodG.QrUpdPID1, False);
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSmiaMPIn);
    DmodG.QrUpdPID1.SQL.Add('SELECT smia.DataHora, smia.IDCtrl, smia.Tipo, ');
    DmodG.QrUpdPID1.SQL.Add('smia.OriCodi, smia.OriCtrl,smia.GraGruX, ');
    DmodG.QrUpdPID1.SQL.Add('(smia.Qtde * smia.Baixa) Qtde, smia.Pecas, smia.Peso, ');
    DmodG.QrUpdPID1.SQL.Add('0, 0, 0, 0, 0, 0, ');//0, 0, 0, 0, 0, 0, 0, 0, 0,');
    DmodG.QrUpdPID1.SQL.Add('smia.ParCodi');
    DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.stqmovitsa smia');
    DmodG.QrUpdPID1.SQL.Add('WHERE smia.ParTipo=' + FormatFloat('0', GRADE_TABS_PARTIPO_0002));
    DmodG.QrUpdPID1.SQL.Add('AND smia.ParCodi <> 0');
    DmodG.QrUpdPID1.SQL.Add(dmkPF.SQL_Periodo('AND smia.DataHora ',
      Int(TPDataIni5.Date), Int(TPDataFim5.Date) + 0.999999, True, True)); // +1 porque � dd/mm/yyyy hh:nn:ss
    if EdGraGru1.ValueVariant <> 0 then // GraGruX e GraGru1 s�o iguais para MP
      DmodG.QrUpdPID1.SQL.Add('AND GraGrux=' + FormatFloat('0', EdGraGru1.ValueVariant));
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrSMPI.Close;
    UnDmkDAC_PF.AbreQuery(QrSMPI, DModG.MyPID_DB);
    PB5.Position := 0;
    PB5.Max := QrSMPI.RecordCount;
    while not QrSMPI.Eof do
    begin
      PB5.Position := PB5.Position + 1;
      MyObjects.Informa(LaAviso5A, True, 'Verificando entrada ID = ' +
        FormatFloat('0', QrSMPIParCodi.Value));
      //
      QrSumBx.Close;
      QrSumBx.Params[00].AsInteger := QrSMPIParCodi.Value;
      QrSumBx.Params[01].AsInteger := QrSMPIGraGruX.Value;
      UnDmkDAC_PF.AbreQuery(QrSumBx, Dmod.MyDB);
      //
      QrSumEr.Close;
      QrSumEr.Params[00].AsInteger := QrSMPIParCodi.Value;
      QrSumEr.Params[01].AsInteger := QrSMPIGraGruX.Value;
      UnDmkDAC_PF.AbreQuery(QrSumEr, Dmod.MyDB);
      //
      Bx_Qtde  := QrSumBxQtde.Value;
      Bx_Pecas := QrSumBxPecas.Value;
      Bx_Peso  := QrSumBxPeso.Value;
      Er_Qtde  := QrSumErQtde.Value;
      Er_Pecas := QrSumErPecas.Value;
      Er_Peso  := QrSumErPeso.Value;
      IDCtrl   := QrSMPIIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FSmiaMPIn, False, [
      'Bx_Qtde', 'Bx_Pecas', 'Bx_Peso',
      'Er_Qtde', 'Er_Pecas', 'Er_Peso'], [
      'IDCtrl'], [
      Bx_Qtde, Bx_Pecas, Bx_Peso,
      Er_Qtde, Er_Pecas, Er_Peso], [
      IDCtrl], False);
      //
      QrSMPI.Next;
    end;
    ReopenSmiaMPIn(0);
    //
    PB5.Position := 0;
    MyObjects.Informa(LaAviso5A, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGraImpLista.BtPesquisa7Click(Sender: TObject);
var
 sEmpresa: String;

begin
  if not USQLDB.TabelasExistem(
  ['gragxvend', 'gragxpatr', 'gragxoutr', 'gragxserv'], DMod.MyDB) then
  begin
     Geral.MB_Aviso('Este relat�rio n�o est� habilitado para este aplicativo!');
     Exit;
  end;
  //
  sEmpresa := Geral.FF0(DModG.QrEmpresasCodigo.Value);
  // Atualizar gragrux._Marca_
  MyObjects.Informa(LaAviso07, True, 'Atualizando marcas de reduzidos');
  Dmod.MyDB.Execute(Geral.ATS([
  'UPDATE gragrux  ',
  'INNER JOIN gragxvend ',
  'ON gragrux.Controle = gragxvend.GraGruX ',
  'SET gragrux._Marca_= gragxvend.Marca ',
  ' ',
  '; ',
  ' ',
  'UPDATE gragrux  ',
  'INNER JOIN gragxpatr ',
  'ON gragrux.Controle = gragxpatr.GraGruX ',
  'SET gragrux._Marca_= gragxpatr.Marca ',
  ' ',
  '; ',
  ' ',
  '/* ',
  'UPDATE gragrux  ',
  'INNER JOIN gragxoutr ',
  'ON gragrux.Controle = gragxoutr.GraGruX ',
  'SET gragrux._Marca_= gragxoutr.Marca ',
  ' ',
  '; ',
  'UPDATE gragrux  ',
  'INNER JOIN gragxserv ',
  'ON gragrux.Controle = gragxserv.GraGruX ',
  'SET gragrux._Marca_= gragxserv.Marca ',
  ' ',
  '; ',
  '*/ ',
  ' ',
  '']));

  // Deve ser antes
  MyObjects.Informa(LaAviso07, True, 'Buscando dados');
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque7, Dmod.MyDB, [
  'SELECT GraGruX, SUM(Qtde * Baixa) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Ativo=1 ',
  'AND ValiStq <> 101',
  'AND Empresa=' + sEmpresa,
  //'AND StqCenCad=' + Geral.FF0(StqCenCad),
  'GROUP BY GraGruX',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrReposicao7, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, ',
  'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, ggx._Marca_, ',
  'gfm.Nome NO_MARCA, ',
  'smia.GraGruX, ',
  '-SUM((smia.Qtde * smia.Baixa)) Qtde  ',
  '/* ',
  'smia.IDCtrl, smia.Tipo, smia.OriCodi, ',
  'smia.OriCtrl, smia.OriCnta, smia.OriPart, ',
  'smia.Empresa, smia.StqCenCad, smia.GraGruX, ',
  'smia.Pecas, smia.Peso, smia.AreaM2, ',
  'smia.AreaP2,  ',
  'SUM((smia.Qtde * smia.Baixa)) Qtde,  ',
  'smia.Baixa, smia.FatorClas, ',
  'smia.Ativo, smia.DataHora,  ',
  'ELT(smia.Baixa+1,"Nulo","Adiciona","Subtrai") BAIXA_TXT  ',
  '*/ ',
  ' ',
  'FROM stqmovitsa smia ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN grafabmar  gfm ON ggx._Marca_=gfm.Controle  ',
  'WHERE smia.Empresa=' + sEmpresa,
  dmkPF.SQL_Periodo('AND smia.DataHora ',
      TPDataIni7.Date, TPDataFim7.Date + 1, True, True), // +1 porque � dd/mm/yyyy hh:nn:ss
  //'AND smia.DataHora  BETWEEN "2021-01-23" AND "2021-04-30 23:59:59" ',
  'AND smia.Ativo=1 ',
  'AND smia.ValiStq <> 101',
  'AND smia.Baixa=-1 ',
  'GROUP BY smia.GraGruX ',
  'ORDER BY NO_MARCA, NO_PRD ',
  '']);
  //Geral.MB_Teste()
  MyObjects.Informa(LaAviso07, False, '...');
end;

procedure TFmGraImpLista.BtSaida1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmGraImpLista.CBFiltro5Click(Sender: TObject);
begin
  ReopenSmiaMPIn(0);
end;

procedure TFmGraImpLista.CkPositivo2Click(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraImpLista.CkPrdGrupoClick(Sender: TObject);
begin
  ConfiguraGradeEstoque(CkPrdGrupo.Checked);
  FechaPesquisa();
end;

procedure TFmGraImpLista.CkStqCenCadClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraImpLista.CkTabePrcCab2Click(Sender: TObject);
begin
  ReopenTabePrcCab();
  PageControl2Change(Self);
  PageControl1Change(Self);
  FechaPesquisa();
end;

procedure TFmGraImpLista.CorrigeBaixa();
begin
  EXIT;
  //
(*  N'ao USAR !!!!!! Gera Erro!!!!!!!!!!!!
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa');
  Dmod.QrUpd.SQL.Add('SET Baixa=1');
  Dmod.QrUpd.SQL.Add('WHERE Baixa=-1');
  Dmod.QrUpd.ExecSQL;
*)
end;

procedure TFmGraImpLista.CorrigeReduzidodoitensbaixados1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE stqmovitsa SET GraGruX=:P0 WHERE IDCtrl=:P1');
    QrSmiaMPIn.First;
    while not QrSmiaMPIn.Eof do
    begin
      QrSMPIBxa.First;
      while not QrSMPIBxa.Eof do
      begin
        if QrSMPIBxaGraGruX.Value <> QrSmiaMPInGraGruX.Value then
        begin
          DMod.QrUpd.Params[00].AsInteger := QrSmiaMPInGraGruX.Value;
          DMod.QrUpd.Params[01].AsInteger := QrSMPIBxaIDCtrl.Value;
          DMod.QrUpd.ExecSQL;
          //
        end;
        //
        QrSMPIBxa.Next;
      end;
      //
      QrSmiaMPIn.Next;
    end;
    QrSmiaMPIn.Close;
  finally
    Screen.Cursor := crDefault;
  end;
  BtPesquisa5Click(Self);
end;

procedure TFmGraImpLista.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraImpLista.EdGraGru1Change(Sender: TObject);
begin
 FechaPesquisa();
end;

procedure TFmGraImpLista.EdGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
  begin
    EdPrdGrupTip.ValueVariant := 0;
    CBPrdGrupTip.KeyValue     := 0;
    //
    EdGraGru3.ValueVariant := 0;
    CBGraGru3.KeyValue     := 0;
    //
    EdGraGru2.ValueVariant := 0;
    CBGraGru2.KeyValue     := 0;
    //
    ReopenGraGru1(0);
  end;
end;

procedure TFmGraImpLista.EdPrdGrupTipChange(Sender: TObject);
{
var
  PrdGrupTip: Integer;
}
begin
  //PrdGrupTip := EdPrdGrupTip.ValueVariant;
  //ReopenGraGru1(PrdGrupTip);
  QrGraGru3.Close;
  QrGraGru2.Close;
  QrGraGru1.Close;
  //
  case QrPrdgrupTipNivCad.Value of
    3: ReopenGraGru3(0);
    2: ReopenGraGru2(0);
    1: ReopenGraGru1(0);
  end;
  FechaPesquisa();
end;

procedure TFmGraImpLista.EdStqCenCadChange(Sender: TObject);
begin
 FechaPesquisa();
end;

procedure TFmGraImpLista.FechaPesquisa;
begin
  QrPrdMov.Close;
  QrPrdEstq.Close;
  QrGSS.Close;
  QrSMIA3.Close;
  QrCPR.Close;
  // Estoque em...
  QrPGT_SCC.Close;
  QrSMIC2.Close;
  QrExport2.Close;
  QrLayout001.Close;
  // Panorama MP
  QrSmiaMPIn.Close;
end;

procedure TFmGraImpLista.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraImpLista.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CkInfoMP_e_Classif.Visible := CO_DMKID_APP = 2; //Bluederm
  CkExclEstqMP.Visible       := CO_DMKID_APP = 2; //Bluederm
  //
  FSelecionouGG1 := False;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  //
  CGAgrup0.Value := 4;
  //
  QrGSS.Database := DModG.MyPID_DB;
  QrStqMovIts.DataBase := DModG.MyPID_DB;
  QrPrdMov.DataBase := DModG.MyPID_DB;
  QrGG2.DataBase := DModG.MyPID_DB;
  QrPrdEstq.DataBase := DModG.MyPID_DB;
  QrPGT_SCC.DataBase := DModG.MyPID_DB;
  QrSMIC2.DataBase := DModG.MyPID_DB;
  QrExport2.DataBase := DModG.MyPID_DB;
  QrListErr1.DataBase := DModG.MyPID_DB;
  QrLayout001.DataBase := DModG.MyPID_DB;
  QrSemCusUni.DataBase := DModG.MyPID_DB;
  QrSMPI.DataBase := DModG.MyPID_DB;
  QrPesqESel_Sel.DataBase := DModG.MyPID_DB;
  QrGGXs2.DataBase := DModG.MyPID_DB;
  QrSum1.DataBase := DModG.MyPID_DB;
  QrSum2.DataBase := DModG.MyPID_DB;
  QrSumM.DataBase := DModG.MyPID_DB;
  QrSumA.DataBase := DModG.MyPID_DB;
  QrDiverge2.DataBase := DModG.MyPID_DB;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  //
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCadPsq, Dmod.MyDB);
  //
  TPDataIni1.Date := Geral.PrimeiroDiaDoMes(IncMonth(Date, -1));
  TPDataFim1.Date := Date;// M L A G e r a l .PrimeiroDiaDoMes(Date) -1;
  //
  TPDataIni2.Date := Geral.PrimeiroDiaDoMes(IncMonth(Date, -1));
  TPDataFim2.Date := Geral.UltimoDiaDoMes(IncMonth(Date, -1));
  //
  TPDataIni3.Date := 0;
  TPDataFim3.Date := Date;
  //
  TPDataIni5.Date := 0;
  TPDataFim5.Date := Date + 365;
  //
  TPDataFim7.Date := Date - 30;
  TPDataFim7.Date := Date;
  //
  // Evitar erros de Centros de estoques
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET StqCenCad=1 WHERE StqCenCad=0');
  Dmod.QrUpd.ExecSQL;
  // Evitar erros de Centros de estoques
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsb SET StqCenCad=1 WHERE StqCenCad=0');
  Dmod.QrUpd.ExecSQL;
  //
  DmProd.ReopenGraCusPrcU(QrGraCusPrc, 0);
  CBFiltro5.SetMaxValue;
  // ini 2023-09-05
  //CGSitEstq.SetMaxValue;
  CGSitEstq.Value := 5; // Estoque<>  0
  // fim 2023-09-05
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PnGraCusPrc.Visible := (PageControl1.ActivePageIndex = 0) or
  ((PageControl1.ActivePageIndex = 2) and (RGTipoPesq2.ItemIndex <> 2));
  //
  CkPrdGrupo.Checked := False;
  //
  ConfiguraGradeEstoque(False);
end;

procedure TFmGraImpLista.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1A, LaAviso1B], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraImpLista.frxPRD_PRINT_001_00E_1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VFR_EMPRESA' then Value := dmkPF.CampoReportTxt(CBEmpresa.Text, 'TODAS')
  else if VarName = 'VFR_NO_PGT' then Value := dmkPF.CampoReportTxt(CBPrdGrupTip.Text, 'TODOS')
  else if VarName = 'VFR_NO_PRD' then Value := dmkPF.CampoReportTxt(CBGraGru1.Text, 'TODOS')
  else if VarName = 'VFR_NO_NI2' then Value := dmkPF.CampoReportTxt(CBGraGru2.Text, 'TODOS')
  else if VarName = 'VFR_NO_CEN' then Value := dmkPF.CampoReportTxt(CBStqCenCad.Text, 'TODOS')
  else if VarName = 'DATAHORA_TXT' then Value := Geral.FDT(FAgora, 8)
  else if VarName = 'DATA_EM_TXT' then
    Value := Geral.FDT(TPDataFim2.Date, 2)
  else if VarName = 'PERIODO_TXT' then Value := dmkPF.PeriodoImp(
    TPDataIni1.Date, TPDataFim1.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VARF_PGT_VISIBLE' then Value := PertenceAoGrupoPGT()
  else if VarName = 'VARF_PRD_VISIBLE' then Value := PertenceAoGrupoPrd()
  else if VarName = 'VARF_NI2_VISIBLE' then Value := PertenceAoGrupoNi2()
  else if VarName = 'VFR_LISTA_PRECO' then Value := CBGraCusPrc.Text
  //
  else if VarName = 'PERIODO_TXT_2' then Value := dmkPF.PeriodoImp(
    TPDataIni2.Date, TPDataFim2.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'PERIODO_TXT_5' then Value := dmkPF.PeriodoImp(
    TPDataIni5.Date, TPDataFim5.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'PERIODO_TXT_7' then Value := dmkPF.PeriodoImp(
    TPDataIni7.Date, TPDataFim7.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VFR_FILTRO_5' then
  begin
   Value := 'TUDO';
    case CBFiltro5.Value of
      //0: CBFiltro5.SetMaxValue; // Tudo
      1: Value := 'Baixa correta';
      2: Value := 'Sem baixa';
      3: Value := 'Baixa correta ou sem baixa';
      4: Value := 'Erro baixa';
      5: Value := 'Baixa correta ou incorreta';
      6: Value := 'Baixa incorreta ou sem baixa';
      7: ; //Tudo
    end;
  end;
end;

procedure TFmGraImpLista.LaAviso07Click(Sender: TObject);
begin

end;

{ Desabilitado para n�o confundir, mas funciona!
procedure TFmGraImpLista.GeraEstoqueEm(Empresa_Txt: String; DataFim: TDateTime);
var
  PGT, GG1, GG2, GG3: Integer;
  PrdGrupTip_Txt, StqCenCad_Txt, DiaSeguinte, GrupoBal_Txt,
  GraCusPrc_Txt, TabePrcCab_Txt, TextoA, TextoB, TextoC, CliInt_Txt,
  EntiSitio_Txt: String;
begin
  Screen.Cursor := crHourGlass;
  try
    PGT := EdPrdGrupTip.ValueVariant;
    GG1 := EdGraGru1.ValueVariant;
    GG2 := EdGraGru2.ValueVariant;
    GG3 := EdGraGru3.ValueVariant;
    //
    //DiaSeguinte := FormatDateTime('yyyy-mm-dd', TPDataFim2.Date + 1);
    DiaSeguinte := FormatDateTime('yyyy-mm-dd', DataFim + 1);
    if RGTipoPesq2.ItemIndex = 1 then
    begin
      if CBGraCusPrc.KeyValue <> NULL then
        GraCusPrc_Txt := FormatFloat('0', QrGraCusPrcCodigo.Value)
      else // N�o pode ser nulo!
        GraCusPrc_Txt := '0';
      //
    end;
    if CBTabePrcCab2.KeyValue <> NULL then
      TabePrcCab_Txt := FormatFloat('0', QrTabePrcCabCodigo.Value)
    else // N�o pode ser nulo?
      TabePrcCab_Txt := '0';
    //
    MyObjects.Informa(LaAviso2A, True, 'Tabela temporaria - Excluindo caso exista');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS stqmovitsc;');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso2A, True, 'Tabela temporaria - Adicionando dados do arquivo morto');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('CREATE TABLE stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT smi.*');
    DmodG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.stqmovitsb smi');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smi.GraGruX');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    DmodG.QrUpdPID1.SQL.Add('WHERE '(*NOT *)+'gg1.PrdGrupTip IN');
    DmodG.QrUpdPID1.SQL.Add('(');
    DmodG.QrUpdPID1.SQL.Add('  SELECT DISTINCT prdgruptip');
    DmodG.QrUpdPID1.SQL.Add('  FROM '+TMeuDB+'.stqbalcad');
    DmodG.QrUpdPID1.SQL.Add('  WHERE Abertura >= "' + DiaSeguinte + '"');
    DmodG.QrUpdPID1.SQL.Add(')');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso2A, True, 'Tabela temporaria - Adicionando dados do arquivo ativo');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT * FROM '+TMeuDB+'.stqmovitsa');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso2A, True, 'Tipos de Grupos de Produtos - abrindo tabela');
    QrPGT_SCC.Close;
    QrPGT_SCC.SQL.Clear;
    case RGTipoPesq2.ItemIndex of
      1:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad, ');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, ' + Empresa_Txt + ' Empresa, ' + '0' + ' EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('WHERE GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        QrPGT_SCC.SQL.Add('AND smic.Empresa=' + Empresa_Txt);
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', QrGraGru1Nivel1.Value))
        else
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', QrGraGru2Nivel2.Value))
        else
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', QrGraGru3Nivel3.Value))
        else
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');//, smic.GraGruX');
      end;
      2:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad,');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, FLOOR(smic.Empresa + 0.1) Empresa, FLOOR(scc.EntiSitio + 0.1) EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
        QrPGT_SCC.SQL.Add('WHERE (smic.Empresa=' + Empresa_Txt + ' OR scc.EntiSitio=' + Empresa_Txt + ')');
        QrPGT_SCC.SQL.Add('AND GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', QrGraGru1Nivel1.Value))
        else
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', QrGraGru2Nivel2.Value))
        else
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', QrGraGru3Nivel3.Value))
        else
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrPGT_SCC, DModG.MyPID_DB);
    //
    TextoA     := '';
    TextoB     := '';
    TextoC     := '';
    // Caso n�o ache nada!
    if QrPGT_SCC.RecordCount = 0 then
    begin
      QrSMIC2.Close;
      QrSMIC2.SQL.Clear;
      QrSMIC2.SQL.Add('DELETE FROM ' + FSMIC2 + ';');
      QrSMIC2.ExecSQL;
    end;
    //
    while not QrPGT_SCC.Eof do
    begin
      CliInt_Txt    := FormatFloat('0', QrPGT_SCCEmpresa.Value);
      EntiSitio_Txt := FormatFloat('0', QrPGT_SCCEntiSitio.Value);
      if RGTipoPesq2.ItemIndex = 2 then
      begin
        GraCusPrc_Txt := FormatFloat('0', QrPGT_SCCLstPrcFisc.Value)
      end;
      PrdGrupTip_Txt := FormatFloat('0', QrPGT_SCCPrdGrupTip.Value);
      StqCenCad_Txt  := FormatFloat('0', QrPGT_SCCStqCenCad.Value);
      TextoA := 'Tipo Prod.: ' + PrdGrupTip_Txt + '  -  ' +
                'Centro estq: ' + StqCenCad_Txt + '  -  ' +
                'Sitio: ' + EntiSitio_Txt + '  -  ' +
                'Empresa: ' + CliInt_Txt;
      TextoB := '';
      TextoC := '';
      MyObjects.Informa(LaAviso2A, False, TextoA + TextoB + TextoC);
      //
      QrAbertura2b.Close;
      QrAbertura2b.SQL.Clear;
      QrAbertura2b.SQL.Add('SELECT Abertura');
      QrAbertura2b.SQL.Add('FROM stqbalcad');
      QrAbertura2b.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
      QrAbertura2b.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
      QrAbertura2b.SQL.Add('AND Abertura >= "' + DiaSeguinte + '"');
      QrAbertura2b.SQL.Add('AND Empresa=' + Empresa_Txt);
      QrAbertura2b.SQL.Add('ORDER BY Abertura DESC');
      QrAbertura2b.SQL.Add('LIMIT 1');
      UnDmkDAC_PF.AbreQuery(QrAbertura2b, Dmod.MyDB);
      if QrAbertura2b.RecordCount > 0 then
      begin
        QrAbertura2a.Close;
        QrAbertura2a.SQL.Clear;
        QrAbertura2a.SQL.Add('SELECT Abertura, GrupoBal');
        QrAbertura2a.SQL.Add('FROM stqbalcad');
        QrAbertura2a.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
        QrAbertura2a.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
        QrAbertura2a.SQL.Add('AND Abertura < "' + DiaSeguinte + '"');
        QrAbertura2a.SQL.Add('AND Empresa=' + Empresa_Txt);
        QrAbertura2a.SQL.Add('ORDER BY Abertura DESC');
        QrAbertura2a.SQL.Add('LIMIT 1');
        UnDmkDAC_PF.AbreQuery(QrAbertura2a, Dmod.MyDB);
        //
        //if QrAbertura2aAbertura.Value > Int(TPDataFim2.Date) + 1 then
        if QrAbertura2aAbertura.Value > Int(DataFim) + 1 then
          GrupoBal_Txt := FormatFloat('0', QrAbertura2aGrupoBal.Value)
        else
          GrupoBal_Txt := '0';
      end else GrupoBal_Txt := '0';
      //
      QrSMIC2.Close;
      QrSMIC2.SQL.Clear;
      if QrPGT_SCC.RecNo = 1 then
      begin
        QrSMIC2.SQL.Add('DROP TABLE IF EXISTS ' + FSMIC2 + ';');
        QrSMIC2.SQL.Add('CREATE TABLE ' + FSMIC2);
      end else begin
        QrSMIC2.SQL.Add('INSERT INTO ' + FSMIC2);
      end;
      QrSMIC2.SQL.Add('SELECT smic.IDCtrl My_Idx, gg1.Nivel1, ');
      QrSMIC2.SQL.Add('gg1.Nome NO_PRD, gg1.PrdGrupTip, gg1.UnidMed, ');
      QrSMIC2.SQL.Add('gg1.NCM, pgt.Nome NO_PGT, ');
      QrSMIC2.SQL.Add('med.Sigla SIGLA, gti.PrintTam, gti.Nome NO_TAM, ');
      QrSMIC2.SQL.Add('gcc.PrintCor, gcc.Nome NO_COR, ggc.GraCorCad, ');
      QrSMIC2.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, ');
      QrSMIC2.SQL.Add('scc.SitProd, scc.EntiSitio,');
      QrSMIC2.SQL.Add('smic.GraGruX, smic.Empresa, smic.StqCenCad,');
      QrSMIC2.SQL.Add('SUM(smic.Qtde * smic.Baixa) QTDE, SUM(smic.Pecas) PECAS,');
      QrSMIC2.SQL.Add('SUM(smic.Peso) PESO, SUM(smic.AreaM2) AREAM2,');
      QrSMIC2.SQL.Add('SUM(smic.AreaP2) AREAP2,');
      if CkTabePrcCab2.Checked then
      begin
        // Por tabela de pre�o de GraGru1 (TPC_GCP_ID = 2) ou de GraGruX (TPC_GCP_ID = 3)
        QrSMIC2.SQL.Add('TRUNCATE(IF(tpi.Preco IS NULL, 2, 3), 0) TPC_GCP_ID,');
        QrSMIC2.SQL.Add('IF(tpi.Preco IS NULL, tpc.Codigo, tpi.TabePrcCab) TPC_GCP_TAB,');
        QrSMIC2.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(tpc.Preco, 6), TRUNCATE(tpi.Preco, 6)) PrcCusUni,');
        QrSMIC2.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpc.Preco, 6), 2),');
        QrSMIC2.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpi.Preco, 6), 2)) ValorTot,');
        QrSMIC2.SQL.Add('');
      end else
      begin
        // Por lista de Pre�o (TPC_GCP_ID = 1)
        QrSMIC2.SQL.Add('TRUNCATE(1,0) TPC_GCP_ID, ggv.GraCusPrc TPC_GCP_TAB, ');
        QrSMIC2.SQL.Add('TRUNCATE(ggv.CustoPreco, 6) PrcCusUni, ');
        QrSMIC2.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * ');
        QrSMIC2.SQL.Add('TRUNCATE(ggv.CustoPreco, 6), 2) ValorTot, ');
      end;
      QrSMIC2.SQL.Add('1 Exportado ');
      //QrSMIC2.SQL.Add('1 Exportado, 0 Mot_SitPrd, 0 Mot_Sitio, 0 MotQtdVal, 0 MotProduto');
      QrSMIC2.SQL.Add('FROM stqmovitsc smic');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux    ggx ON ggx.Controle=smic.GraGruX');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruc    ggc ON ggc.Controle=ggx.GraGruC');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gratamits  gti ON gti.Controle=ggx.GraTamI');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.unidmed    med ON med.Codigo=gg1.UnidMed');
      if CkTabePrcCab2.Checked then
      begin
        QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgrg  tpc ON tpc.Nivel1=ggx.GraGru1');
        QrSMIC2.SQL.Add('          AND tpc.Codigo=' + TabePrcCab_Txt);
        QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgri  tpi ON tpi.GraGruX=smic.GraGruX');
        QrSMIC2.SQL.Add('          AND tpi.TabePrcCab=' + TabePrcCab_Txt);
      end else begin
        QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruval  ggv ON ggv.GraGruX=smic.GraGruX');
        // Evitar n�o encontrar registros sem pre�o!
        QrSMIC2.SQL.Add('          AND ggv.GraCusPrc=' + GraCusPrc_Txt);
      end;
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
      //
      QrSMIC2.SQL.Add('WHERE smic.Baixa> 0');
      //QrSMIC2.SQL.Add('AND smic.GrupoBal=' + GrupoBal_Txt);
      QrSMIC2.SQL.Add('AND smic.DataHora < "' + DiaSeguinte  + '"');
      QrSMIC2.SQL.Add('AND smic.StqCenCad=' + StqCenCad_Txt);
      QrSMIC2.SQL.Add('AND gg1.PrdGrupTip=' + PrdGrupTip_Txt);
      //
      QrSMIC2.SQL.Add('AND smic.EMPRESA=' + CliInt_Txt);
      if RGTipoPesq2.ItemIndex = 2 then
        QrSMIC2.SQL.Add('AND scc.EntiSitio=' + EntiSitio_Txt);
      //
      //  Filtros de pesquisa
      if GG1 <> 0 then
        QrSMIC2.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', QrGraGru1Nivel1.Value))
      else
      if GG2 <> 0 then
        QrSMIC2.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', QrGraGru2Nivel2.Value))
      else
      if GG3 <> 0 then
        QrSMIC2.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', QrGraGru3Nivel3.Value))
      else
      if PGT <> 0 then
        QrSMIC2.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
      //
      //
      QrSMIC2.SQL.Add('GROUP BY smic.GraGruX');
      QrSMIC2.ExecSQL;
      //
      QrPGT_SCC.Next;
    end;
    //
    MyObjects.Informa(LaAviso2A, False, 'Abrindo dados gerados');
    QrSMIC2.Close;
    QrSMIC2.SQL.Clear;
    QrSMIC2.SQL.Add('SELECT sm2.*, scc.SitProd,');
    QrSMIC2.SQL.Add('ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,');
    QrSMIC2.SQL.Add('emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF');
    QrSMIC2.SQL.Add('FROM ' + FSMIC2 + ' sm2');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=sm2.StqCenCad');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades ens ON ens.Codigo=scc.EntiSitio');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades emp ON emp.Codigo=sm2.Empresa');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf1 ON uf1.Codigo=ens.EUF');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf2 ON uf2.Codigo=emp.EUF');
    if CkPositivo2.Checked then
      QrSMIC2.SQL.Add('WHERE sm2.QTDE >= 0.001');
    QrSMIC2.SQL.Add('ORDER BY NO_PGT, NO_PRD');
    UnDmkDAC_PF.AbreQuery(QrSMIC2, Dmod.MyDB);
    //
    MyObjects.Informa(LaAviso2A, False, '...');
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmGraImpLista.NeutralizaUsoConsumo_4();
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    QrParamsEmp.Close;
    QrParamsEmp.Params[0].AsInteger := DmodG.QrEmpresasCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
    //
    if QrParamsEmpEstq0UsoCons.Value = 1 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa smia, gragrux ggx, gragru1 gg1');
      Dmod.QrUpd.SQL.Add('SET smia.Baixa=0');
      Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
      Dmod.QrUpd.SQL.Add('AND gg1.Nivel1=ggx.GraGru1');
      Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
      Dmod.QrUpd.SQL.Add('AND gg1.Nivel2<>-2');
      Dmod.QrUpd.SQL.Add('AND smia.Baixa <> 0');
      Dmod.QrUpd.SQL.Add('AND smia.Empresa=:P0');
      Dmod.QrUpd.Params[0].AsInteger := DmodG.QrEmpresasCodigo.Value;
      Dmod.QrUpd.ExecSQL;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmGraImpLista.PageControl1Change(Sender: TObject);
begin
  if PageControl2.ActivePageIndex <> PageControl1.ActivePageIndex then
    PageControl2.ActivePageIndex := PageControl1.ActivePageIndex;
  PnGraCusPrc.Visible := (PageControl1.ActivePageIndex = 0) or
  ((PageControl1.ActivePageIndex = 2) and (RGTipoPesq2.ItemIndex <> 2));
  LaInventarioFisco.Visible :=
    (PageControl1.ActivePageIndex = 2) and (RGTipoPesq2.ItemIndex = 2)
    and (CkTabePrcCab2.Checked = False);
  //
  if PageControl1.ActivePageIndex in [0, 4, 6] then
    GBRodaPe.Visible := True
  else
    GBRodaPe.Visible := False;
end;

procedure TFmGraImpLista.PageControl2Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex <> PageControl2.ActivePageIndex then
    PageControl1.ActivePageIndex := PageControl2.ActivePageIndex;
  PnGraCusPrc.Visible := (PageControl1.ActivePageIndex = 0) or
  ((PageControl1.ActivePageIndex = 2) and (RGTipoPesq2.ItemIndex <> 2));
  LaInventarioFisco.Visible :=
    (PageControl1.ActivePageIndex = 2) and (RGTipoPesq2.ItemIndex = 2);
  //
  if PageControl2.ActivePageIndex in [0, 4, 6] then
    GBRodaPe.Visible := True
  else
    GBRodaPe.Visible := False;
end;

procedure TFmGraImpLista.Panel24Resize(Sender: TObject);
begin
  //Panel27.Height := Panel24.Height div 2;
end;

function TFmGraImpLista.PertenceAoGrupoNI2: Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(2, CGAgrup0.Value);
    2: Result := Geral.IntInConjunto(2, CGAgrup2.Value);
    else Result := False;
  end;
end;

function TFmGraImpLista.PertenceAoGrupoPGT(): Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(1, CGAgrup0.Value);
    2: Result := Geral.IntInConjunto(1, CGAgrup2.Value);
    else Result := False;
  end;
end;

function TFmGraImpLista.PertenceAoGrupoPRD(): Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(4, CGAgrup0.Value);
    2: Result := Geral.IntInConjunto(4, CGAgrup2.Value);
    else Result := False;
  end;
end;

procedure TFmGraImpLista.PMCorrige5Popup(Sender: TObject);
begin
  Alteraitembaixadoatual1.Enabled :=
    (QrSMPIBxa.State <> dsInactive) and
    (QrSMPIBxa.RecordCount > 0);
  AlteraitemClassificadoougeradaselelcionado1.Enabled :=
    (QrSMPIGer.State <> dsInactive) and
    (QrSMPIGer.RecordCount > 0);
end;

procedure TFmGraImpLista.QrExport2AfterScroll(DataSet: TDataSet);
begin                
  //BtSalva2.Enabled := (QrExport2.RecordCount = 0) and (MeExp.Text <> '');
  QrListErr1.Close;
  QrListErr1.Params[0].AsInteger := QrExport2My_Idx.Value;
  UnDmkDAC_PF.AbreQuery(QrListErr1, DModG.MyPID_DB);
end;

procedure TFmGraImpLista.QrExport2BeforeClose(DataSet: TDataSet);
begin
  QrListErr1.Close;
  //BtSalva2.Enabled := False;
end;

procedure TFmGraImpLista.QrGraGru1AfterClose(DataSet: TDataSet);
begin
  EdGraGru1.ValueVariant := 0;
  CBGraGru1.KeyValue := 0;
end;

procedure TFmGraImpLista.QrGraGru2AfterClose(DataSet: TDataSet);
begin
  ReopenGraGru1(0);
  EdGraGru2.ValueVariant := 0;
  CBGraGru2.KeyValue := 0;
end;

procedure TFmGraImpLista.QrGraGru2AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru1(0);
end;

procedure TFmGraImpLista.QrGraGru3AfterClose(DataSet: TDataSet);
begin
  ReopenGraGru2(0);
  EdGraGru3.ValueVariant := 0;
  CBGraGru3.KeyValue := 0;
end;

procedure TFmGraImpLista.QrGraGru3AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru2(0);
end;

procedure TFmGraImpLista.QrPesqESel_SelAfterOpen(DataSet: TDataSet);
begin
  CkSelecao0.Checked := QrPesqESel_Sel.RecordCount > 0;
end;

procedure TFmGraImpLista.QrPesqESel_SelBeforeClose(DataSet: TDataSet);
begin
  CkSelecao0.Checked := False;
end;

procedure TFmGraImpLista.QrPrdEstqAfterOpen(DataSet: TDataSet);
begin
  BtImprime0.Enabled := QrPrdEstq.RecordCount > 0;
end;

procedure TFmGraImpLista.QrPrdEstqAfterScroll(DataSet: TDataSet);
begin
  LaStqTotal.Caption := 'Total de itens: ' + Geral.FF0(QrPrdEstq.RecordCount)
end;

procedure TFmGraImpLista.QrPrdEstqBeforeClose(DataSet: TDataSet);
begin
  LaStqTotal.Caption := '';
  //
  BtImprime0.Enabled := False;
end;

procedure TFmGraImpLista.QrPrdEstqCalcFields(DataSet: TDataSet);
begin
  QrPrdEstqValorTot.Value := QrPrdEstqQTDE.Value * QrPrdEstqPrcCusUni.Value; 
end;

procedure TFmGraImpLista.QrPrdGrupTipAfterScroll(DataSet: TDataSet);
begin
  case QrPrdGrupTipNivCad.Value of
    3: ReopenGraGru3(0);
    2: ReopenGraGru2(0);
    1: ReopenGraGru1(0);
  end;
end;

procedure TFmGraImpLista.QrPrdMovAfterOpen(DataSet: TDataSet);
begin
  BtImprime1.Enabled  := QrPrdMov.RecordCount > 0;
  BtLocaliza1.Enabled := QrPrdMov.RecordCount > 0;
end;

procedure TFmGraImpLista.QrPrdMovBeforeClose(DataSet: TDataSet);
begin
  BtImprime1.Enabled := False;
  BtLocaliza1.Enabled := False;
end;

procedure TFmGraImpLista.QrPrdMovCalcFields(DataSet: TDataSet);
begin
(* ini 2023-08-16
{$IFNDef semNFe_v0000}
   QrPrdMovNO_TIPO.Value := DmNFe_0000.NomeFatID_NFe(QrPrdMovTipo.Value);
{$EndIf}
  *)
// fim 2023-08-16
  QrPrdMovNO_TIPO.Value := DmkEnums.NomeFatID_All(QrPrdMovTipo.Value);
end;

procedure TFmGraImpLista.QrReposicao7AfterOpen(DataSet: TDataSet);
begin
  BtImprime7.Enabled := QrReposicao7.RecordCount > 0;
end;

procedure TFmGraImpLista.QrReposicao7BeforeClose(DataSet: TDataSet);
begin
  BtImprime7.Enabled := False;
end;

procedure TFmGraImpLista.QrSMIA3AfterOpen(DataSet: TDataSet);
begin
  BtImprime3.Enabled := QrSMIA3.RecordCount > 0;
end;

procedure TFmGraImpLista.QrSMIA3BeforeClose(DataSet: TDataSet);
begin
  BtImprime3.Enabled := False;
end;

procedure TFmGraImpLista.QrSmiaMPInAfterOpen(DataSet: TDataSet);
begin
  BtCorrige5.Enabled := QrSmiaMPIn.RecordCount > 0;
  BtImprime5.Enabled := QrSmiaMPIn.RecordCount > 0;
end;

procedure TFmGraImpLista.QrSmiaMPInAfterScroll(DataSet: TDataSet);
begin
  ReopenStqMovIts();
end;

procedure TFmGraImpLista.QrSmiaMPInBeforeClose(DataSet: TDataSet);
begin
  QrSMPIBxa.Close;
  QrSMPIGer.Close;
  BtCorrige5.Enabled := False;
  BtImprime5.Enabled := False;
end;

procedure TFmGraImpLista.QrSMIC2AfterOpen(DataSet: TDataSet);
begin
  BtImprime2.Enabled := QrSMIC2.RecordCount > 0;
  BtExporta2.Enabled :=
    (QrSMIC2.RecordCount > 0) and (RGTipoPesq2.Itemindex = 2) and
    (Int(TPDataFim2.Date) = Geral.UltimoDiaDoMes(Int(TPDataFim2.Date)));
end;

procedure TFmGraImpLista.QrSMIC2BeforeClose(DataSet: TDataSet);
begin
  BtExporta2.Enabled := False;
  BtImprime2.Enabled := False;
end;

procedure TFmGraImpLista.RGTipoPesq2Click(Sender: TObject);
begin
  BtPesq2.Enabled := RGTipoPesq2.ItemIndex > 0;
  PageControl2Change(Self);
  PageControl1Change(Self);
  FechaPesquisa();
{
  if RGTipoPesq2.ItemIndex = 2 then
    MyObjects.Informa(LaAviso2A, False, '')
  else
    MyObjects.Informa(LaAviso2A, True, '');
}
end;

{
procedure TFmGraImpLista.ReopenGraGru1(PrdGrupTip: Integer);
begin
  QrGraGru1.Close;
  QrGraGru1.SQL.Clear;
  QrGraGru1.SQL.Add('SELECT Nivel1, CodUsu, Nome');
  QrGraGru1.SQL.Add('FROM gragru1');
  if PrdGrupTip <> 0 then
    QrGraGru1.SQL.Add('WHERE PrdGrupTip=' + FormatFloat('0', PrdGrupTip));
  QrGraGru1.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
end;
}

procedure TFmGraImpLista.ReopenGraGru1(Nivel1: Integer);
{
var
  Nivel2: Integer;
begin
  QrGraGru1.Close;
  if QrPrdGrupTipNivCad.Value >= 1 then
  begin
    if QrGraGru2.State = dsInactive then
      Nivel2 := 0
    else
      Nivel2 := QrGraGru2Nivel2.Value;
    QrGraGru1.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
    QrGraGru1.Params[1].AsInteger := Nivel2;
    //QrGraGru1.Params[2].AsString  := '%' + EdFiltroNiv1.Text + '%';
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    //
    if Nivel1 <> 0 then
      QrGraGru1.Locate('Nivel1', Nivel1, []);
  end;
}
var
  COD_PGT, COD_GG2, COD_GG3: Integer;
  SQL_PGT, SQL_GG2, SQL_GG3: String;
begin
  SQL_PGT := '';
  SQL_GG2 := '';
  SQL_GG3 := '';
  //
  COD_PGT := EdPrdGrupTip.ValueVariant;
  COD_GG2 := EdGraGru2.ValueVariant;
  COD_GG3 := EdGraGru2.ValueVariant;
  //
  if COD_PGT <> 0 then
  begin
    SQL_PGT := 'WHERE gg1.PrdGrupTip=' + Geral.FF0(COD_PGT);
    //
    if COD_GG2 <> 0 then
      SQL_GG2 := 'AND gg1.Nivel2=' + Geral.FF0(COD_GG2);
    //
    if COD_GG3 <> 0 then
      SQL_GG3 := 'AND gg1.Nivel3=' + Geral.FF0(COD_GG3);
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
  'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, ',
  'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, ',
  'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD, ',
  'gg1.CST_A,gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso, ',
  'gg1.IPI_Alq, gg1.IPI_CST, gg1.IPI_cEnq, gg1.TipDimens, ',
  'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, ',
  'unm.Nome NOMEUNIDMED, gg1.PartePrinc,  ',
  'gg1.PerCuztMin, gg1.PerCuztMax, gg1.MedOrdem, gg1.FatorClas, ',
  'mor.Nome NO_MedOrdem, mor.Medida1, mor.Medida2, ',
  'mor.Medida3, mor.Medida4, mpc.Nome NO_PartePrinc, ',
  'InfAdProd, SiglaCustm, HowBxaEstq, GerBxaEstq, ',
  'PIS_CST, PIS_AlqP, PIS_AlqV, PISST_AlqP, PISST_AlqV, ',
  'COFINS_CST, COFINS_AlqP, COFINS_AlqV,COFINSST_AlqP,  ',
  'COFINSST_AlqV, ICMS_modBC, ICMS_modBCST, ICMS_pRedBC, ',
  'ICMS_pRedBCST, ICMS_pMVAST, ICMS_pICMSST, ',
  'IPI_pIPI, IPI_TpTrib, IPI_vUnid, EX_TIPI, ',
  'ICMS_Pauta, ICMS_MaxTab, cGTIN_EAN, ',
  'PIS_pRedBC, PISST_pRedBCST, ',
  'COFINS_pRedBC, COFINSST_pRedBCST, ',
  ' ',
  'ICMSRec_pRedBC, IPIRec_pRedBC, ',
  'PISRec_pRedBC, COFINSRec_pRedBC, ',
  'ICMSRec_pAliq, IPIRec_pAliq, ',
  'PISRec_pAliq, COFINSRec_pAliq, ',
  'ICMSRec_tCalc, IPIRec_tCalc, ',
  'PISRec_tCalc, COFINSRec_tCalc, ',
  'ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA, ',
  'gg1.PerComissF, gg1.PerComissR, gg1.PerComissZ ',
  ' ',
  'FROM gragru1 gg1 ',
  'LEFT JOIN gragru2   gg2 ON gg1.Nivel2=gg2.Nivel2 ',
  'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN medordem mor ON mor.Codigo= gg1.MedOrdem ',
  'LEFT JOIN matpartcad mpc ON mpc.Codigo=gg1.PartePrinc ',
  //'WHERE gg1.PrdGrupTip=:P0 ',
  SQL_PGT,
  //'AND gg1.Nivel2=:P1 ',
  SQL_GG2,
  SQL_GG3,
  'ORDER BY gg1.Nome ',
  '']);
end;

procedure TFmGraImpLista.ReopenGraGru2(Nivel2: Integer);
var
  Nivel3: Integer;
begin
  QrGraGru2.Close;
  if QrPrdGrupTipNivCad.Value >= 2 then
  begin
    if QrGraGru3.State = dsInactive then
      Nivel3 := 0
    else
      Nivel3 := QrGraGru3Nivel3.Value;
    QrGraGru2.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
    QrGraGru2.Params[1].AsInteger := Nivel3;
    //QrGraGru2.Params[2].AsString  := '%' + EdFiltroNiv2.Text + '%';
    UnDmkDAC_PF.AbreQuery(QrGraGru2, Dmod.MyDB);
    //
    if Nivel2 <> 0 then
      QrGraGru2.Locate('Nivel2', Nivel2, []);
  end;
end;

procedure TFmGraImpLista.ReopenGraGru3(Nivel3: Integer);
begin
  QrGraGru3.Close;
  if QrPrdGrupTipNivCad.Value >= 3 then
  begin
    QrGraGru3.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
    //QrGraGru3.Params[1].AsString  := '%' + EdFiltroNiv3.Text + '%';
    UnDmkDAC_PF.AbreQuery(QrGraGru3, Dmod.MyDB);
    //
    if Nivel3 <> 0 then
      QrGraGru3.Locate('Nivel3', Nivel3, []);
  end;
end;

procedure TFmGraImpLista.ReopenSmiaMPIn(IDCtrl: Integer);
var
  Filtro: String;
begin
  if FSmiaMPIn <> '' then
  begin
    Filtro := '';
    case CBFiltro5.Value of
      0: CBFiltro5.SetMaxValue; // Tudo
      1: Filtro := 'WHERE MP_Qtde + Bx_Qtde = 0';
      2: Filtro := 'WHERE (Bx_Qtde=0) AND (Er_Qtde = 0)';
      3: Filtro := 'WHERE (MP_Qtde = Bx_Qtde) OR (Bx_Qtde=0)';
      4: Filtro := 'WHERE Er_Qtde <> 0';
      5: Filtro := 'WHERE (MP_Qtde = Bx_Qtde) OR (Er_Qtde <> 0)';
      6: Filtro := 'WHERE MP_Qtde <> Bx_Qtde';
      7: ; //Tudo
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrSmiaMPIn, DModG.MyPID_DB, [
      'SELECT gg1.Nome NO_PRD, med.Sigla SIGLA, ggx.GraGru1, ',
      'smia.MP_Qtde/smia.MP_Pecas MP_MEDIA, smia.*',
      'FROM ' + FSmiaMPIn + ' smia',
      'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=smia.GraGruX',
      'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed',
      Filtro,
      'ORDER BY DataHora, NO_PRD, smia.GraGruX',
      '']);
  end;
end;

procedure TFmGraImpLista.ReopenStqMovIts();
begin
  QrSMPIBxa.Close;
  QrSMPIBxa.Params[0].AsInteger := QrSmiaMPInParCodi.Value;
  UnDmkDAC_PF.AbreQuery(QrSMPIBxa, Dmod.MyDB);
  //
  QrSMPIGer.Close;
  QrSMPIGer.Params[0].AsInteger := QrSmiaMPInParCodi.Value;
  UnDmkDAC_PF.AbreQuery(QrSMPIGer, Dmod.MyDB);
  //
end;

procedure TFmGraImpLista.ReopenTabePrcCab();
var
  Data: String;
begin
  QrTabePrcCab.Close;
  if CkTabePrcCab2.Checked then
  begin
    Data := Geral.FDT(TPDataFim2.Date, 1);
    QrTabePrcCab.SQL.Clear;
    QrTabePrcCab.SQL.Add('SELECT Codigo, CodUsu, Nome');
    QrTabePrcCab.SQL.Add('FROM tabeprccab');
    QrTabePrcCab.SQL.Add('WHERE Aplicacao & 8 <> 0');
    QrTabePrcCab.SQL.Add('AND "' + Data + '" BETWEEN DataI AND DataF');
    QrTabePrcCab.SQL.Add('ORDER BY Nome');
    UnDmkDAC_PF.AbreQuery(QrTabePrcCab, Dmod.MyDB);
  end;
end;

procedure TFmGraImpLista.TPDataFim1Change(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraImpLista.TPDataFim2Change(Sender: TObject);
begin
  FechaPesquisa();
  ReopenTabePrcCab();
end;

procedure TFmGraImpLista.TPDataFim2Click(Sender: TObject);
begin
  ReopenTabePrcCab();
end;

procedure TFmGraImpLista.TPDataIni1Change(Sender: TObject);
begin
  FechaPesquisa();
end;

function TFmGraImpLista.VeriStr(Tam, Item: Integer; Texto: String): String;
begin
  Result := Texto;
  if Length(Result) <> Tam then
  Geral.MB_Aviso('O item com tamanho inv�lido: ' + sLineBreak +
  'Item n�mero: ' + FormatFloat('0', Item) + sLineBreak +
  'Tamanho esperado: ' + FormatFloat('0', Tam) + sLineBreak +
  'Tamanho do texto informado: ' + FormatFloat('0', Length(Result)) + sLineBreak +
  'Texto informado: "' + Texto + '"');
end;

{

  Parei Aqui!
Estoque em:
Arrunmar a grade
Buscar pre�os
Incluir Pe�as, Areas e pesos na pesquisa!
Data ??/??/????
}

{
SELECT *
FROM mpin
WHERE Controle In (2182,2183,2184,2185,2186,2187,2188, 2189, 2190, 2191)





SELECT wi.Codigo, wi.Controle, wi.Data, wi.ClienteI,
wi.Procedencia, wi.Transportadora, wi.Marca, wi.Lote,
wi.Pecas, wi.PLE,
wi.PecasOut, wi.PecasSal, wi.Estq_Pecas, wi.Estq_M2,
wi.Estq_P2, wi.Estq_Peso, wi.StqMovIts,
0 Debctrl, 0 DebStqCenCad, 0 DebGraGruX, 1 GerBxaEstq,
0 Clas_Qtde, 0 Clas_Pecas, 0 Clas_M2, 0 Clas_P2, 0 Clas_Peso,
0 Bxa_Qtde, 0 Bxa_Pecas, 0 Bxa_M2, 0 Bxa_P2, 0 Bxa_Peso,
0 DataHora
FROM mpin wi
LEFT JOIN entidades ci ON ci.Codigo=wi.ClienteI
LEFT JOIN entidades fo ON fo.Codigo=wi.Procedencia
LEFT JOIN entidades ve ON ve.Codigo=wi.Corretor
LEFT JOIN entidades qa ON qa.Codigo=wi.QuemAssina
LEFT JOIN entimp    em ON em.Codigo=fo.Codigo
LEFT JOIN ufs ufE ON ufE.Codigo=fo.EUF
LEFT JOIN ufs ufP ON ufP.Codigo=fo.PUF
WHERE  wi.Lote LIKE "%cv 013%"
AND wi.Encerrado=0

ORDER BY Data DESC, Ficha DESC, Controle DESC
}



{
DROP TABLE IF EXISTS _SMIA_MPIN_;
CREATE TABLE _SMIA_MPIN_
SELECT DataHora, IDCtrl, Tipo, OriCodi, OriCtrl,
GraGruX, Qtde, Pecas, Peso, ParCodi
FROM stqmovitsa
WHERE ParTipo=2 AND ParCodi <> 0;
SELECT smp.*, smia.Qtde
FROM _smia_mpin_ smp
LEFT JOIN stqmovitsa smia ON smia.OriCodi=smp.ParCodi AND smia.Tipo=104
}


end.
