unit StqInnPack;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, Vcl.Mask;

type
  TFmStqInnPack = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrGGXFrac: TMySQLQuery;
    QrGGXFracNO_GG1: TWideStringField;
    QrGGXFracNO_COR: TWideStringField;
    QrGGXFracNO_TAM: TWideStringField;
    QrGGXFracGraGruX: TIntegerField;
    QrGGXFracHowBxaEstq: TSmallintField;
    QrGGXFracGerBxaEstq: TSmallintField;
    QrGGXFracNo_SIGLA: TWideStringField;
    QrGGXFracSigla: TWideStringField;
    QrGGXFracGrandeza: TIntegerField;
    QrGGXFracFracio: TIntegerField;
    DsGGXFrac: TDataSource;
    QrGGXPack: TMySQLQuery;
    QrGGXPackNO_GG1: TWideStringField;
    QrGGXPackNO_COR: TWideStringField;
    QrGGXPackNO_TAM: TWideStringField;
    QrGGXPackGraGruX: TIntegerField;
    QrGGXPackHowBxaEstq: TSmallintField;
    QrGGXPackGerBxaEstq: TSmallintField;
    QrGGXPackNo_SIGLA: TWideStringField;
    QrGGXPackSigla: TWideStringField;
    QrGGXPackGrandeza: TIntegerField;
    QrGGXPackFracio: TIntegerField;
    DsGGXPack: TDataSource;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Panel6: TPanel;
    Label11: TLabel;
    SpeedButton1: TSpeedButton;
    EdGraGruX: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    PainelGGXFrac: TPanel;
    PainelGGXPack: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Panel8: TPanel;
    Label8: TLabel;
    SpeedButton2: TSpeedButton;
    EdPackGGX: TdmkEdit;
    EdQtde: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdPackQtde: TdmkEdit;
    QrGGXFracUnidMed: TIntegerField;
    QrGGXPackUnidMed: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPackGGXChange(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPackGGXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    procedure AbrePesquisaFrac(GraGruX: Integer);
    procedure AbrePesquisaPack(GraGruX: Integer);
    procedure MostraGraGruPesqFrac1(Nivel1: Integer);
    procedure MostraGraGruPesqPack1(Nivel1: Integer);
    procedure ReopenGGX(Qr: TmySQLQuery);
    procedure VerificaPainelGGXFrac();
    procedure VerificaPainelGGXPack();
  public
    { Public declarations }
    FIDCtrl: Integer;
  end;

  var
  FmStqInnPack: TFmStqInnPack;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, ModuleGeral, StqInnCad,
  ModProd, MyDBCheck, GraGruPesq1;

{$R *.DFM}

procedure TFmStqInnPack.AbrePesquisaFrac(GraGruX: Integer);
var
  Nivel1: Integer;
begin
  if GraGruX <> 0 then
    Nivel1 := DmProd.ObtemGraGru1deGraGruX(GraGruX)
  else
    Nivel1 := 0;
  //
  MostraGraGruPesqFrac1(Nivel1);
  ReopenGGX(QrGGXFrac);
  VerificaPainelGGXFrac();
end;

procedure TFmStqInnPack.AbrePesquisaPack(GraGruX: Integer);
var
  Nivel1: Integer;
begin
  if GraGruX <> 0 then
    Nivel1 := DmProd.ObtemGraGru1deGraGruX(GraGruX)
  else
    Nivel1 := 0;
  //
  MostraGraGruPesqPack1(Nivel1);
  ReopenGGX(QrGGXPack);
  VerificaPainelGGXPack();
end;

procedure TFmStqInnPack.BtOKClick(Sender: TObject);
var
  GraGruX, PackGGX, PackUnMed: Integer;
  Qtde, PackQtde: Double;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  //
  GraGruX    := EdGraGruX.ValueVariant;
  Qtde       := EdQtde.ValueVariant;
  PackGGX    := EdPackGGX.ValueVariant;
  PackUnMed  := QrGGXPackUnidMed.Value; //EdPackUnMed.ValueVariant;
  PackQtde   := EdPackQtde.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
  'GraGruX', 'Qtde',
  'PackGGX', 'PackUnMed', 'PackQtde'], [
  'IDCtrl'], [
  GraGruX, Qtde,
  PackGGX, PackUnMed, PackQtde], [
  FIDCtrl], False) then
  begin
    DmodG.AtualizaPrecosGraGruVal2(GraGruX, FmStqInnCad.QrStqInnCadEmpresa.Value);
    FmStqInnCad.LocCod(
      FmStqInnCad.QrStqInnCadCodigo.Value, FmStqInnCad.QrStqInnCadCodigo.Value);
    Close;
  end;
end;

procedure TFmStqInnPack.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqInnPack.EdGraGruXChange(Sender: TObject);
begin
  VerificaPainelGGXFrac();
end;

procedure TFmStqInnPack.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    AbrePesquisaFrac(EdGraGruX.ValueVariant);
end;

procedure TFmStqInnPack.EdPackGGXChange(Sender: TObject);
begin
  VerificaPainelGGXPack();
end;

procedure TFmStqInnPack.EdPackGGXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    AbrePesquisaPack(EdPackGGX.ValueVariant);
end;

procedure TFmStqInnPack.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqInnPack.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenGGX(QrGGXFrac);
  ReopenGGX(QrGGXPack);
end;

procedure TFmStqInnPack.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqInnPack.MostraGraGruPesqFrac1(Nivel1: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FmGraGruPesq1.QrGraGru1, Dmod.MyDB, [
      'SELECT gg1.CodUsu, gg1.Nivel1, gg1.UsaSubsTrib, ',
      'gg1.GraTamCad, gg1.Nome, gg1.PrdGrupTip, pgt.Fracio ',
      'FROM gragru1 gg1',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
      'ORDER BY gg1.Nome',
      '']);
    //
    FmGraGruPesq1.VUGraGru1.ValueVariant := Nivel1;
    //
    FmGraGruPesq1.ShowModal;
    //
    EdGraGruX.Text := FmGraGruPesq1.FGraGruX;
    //FFracio
    //
    FmGraGruPesq1.Destroy;
  end;
end;

procedure TFmStqInnPack.MostraGraGruPesqPack1(Nivel1: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FmGraGruPesq1.QrGraGru1, Dmod.MyDB, [
      'SELECT gg1.CodUsu, gg1.Nivel1, gg1.UsaSubsTrib, ',
      'gg1.GraTamCad, gg1.Nome, gg1.PrdGrupTip, pgt.Fracio ',
      'FROM gragru1 gg1',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
      'ORDER BY gg1.Nome',
      '']);
    //
    FmGraGruPesq1.VUGraGru1.ValueVariant := Nivel1;
    //
    FmGraGruPesq1.ShowModal;
    //
    EdPackGGX.Text := FmGraGruPesq1.FGraGruX;
    //FPackio
    //
    FmGraGruPesq1.Destroy;
  end;
end;

procedure TFmStqInnPack.ReopenGGX(Qr: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr, Dmod.MyDB, [
    'SELECT ggx.Controle GraGruX, gg1.UnidMed, ',
    'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  ',
    'gcc.Nome NO_COR,  gti.Nome NO_TAM, pgt.Fracio, ',
    'med.Nome No_SIGLA, med.Sigla, med.Grandeza ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    '']);
end;

procedure TFmStqInnPack.SpeedButton1Click(Sender: TObject);
begin
  AbrePesquisaFrac(EdGraGruX.ValueVariant);
end;

procedure TFmStqInnPack.SpeedButton2Click(Sender: TObject);
begin
  AbrePesquisaPack(EdPackGGX.ValueVariant);
end;

procedure TFmStqInnPack.VerificaPainelGGXFrac();
var
  GraGruX: Integer;
begin
  GraGruX                  := EdGraGruX.ValueVariant;
  PainelGGXFrac.Visible    := QrGGXFrac.Locate('GraGruX', GraGruX, []);
(*
  GBOutrasUnidades.Visible := QrGraGruXHowBxaEstq.Value > 0;
  //
  if GraGruX <> 0 then
    ConfiguraFracioProd(QrGraGruXFracio.Value);
*)
end;

procedure TFmStqInnPack.VerificaPainelGGXPack();
var
  PackGGX: Integer;
begin
  PackGGX                  := EdPackGGX.ValueVariant;
  PainelGGXPack.Visible    := QrGGXPack.Locate('GraGruX', PackGGX, []);
(*
  GBOutrasUnidades.Visible := QrGraGruXHowBxaEstq.Value > 0;
  //
  if GraGruX <> 0 then
    ConfiguraFracioProd(QrGraGruXFracio.Value);
*)
end;

end.
