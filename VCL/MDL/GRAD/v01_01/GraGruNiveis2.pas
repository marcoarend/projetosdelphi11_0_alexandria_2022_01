unit GraGruNiveis2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums;

type
  TFmGraGruNiveis2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    TbGraGru2: TmySQLTable;
    DsGraGru2: TDataSource;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGridZTO;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    LbItensMD: TListBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    TbGraGru2Nivel5: TIntegerField;
    TbGraGru2Nivel4: TIntegerField;
    TbGraGru2Nivel3: TIntegerField;
    TbGraGru2Nivel2: TIntegerField;
    TbGraGru2CodUsu: TIntegerField;
    TbGraGru2Nome: TWideStringField;
    TbGraGru2ContaCTB: TIntegerField;
    TbGraGru2PrdGrupTip: TIntegerField;
    TbGraGru2Lk: TIntegerField;
    TbGraGru2DataCad: TDateField;
    TbGraGru2DataAlt: TDateField;
    TbGraGru2UserCad: TIntegerField;
    TbGraGru2UserAlt: TIntegerField;
    TbGraGru2AlterWeb: TSmallintField;
    TbGraGru2Ativo: TSmallintField;
    TbGraGru2Tipo_ITEM: TSmallintField;
    TbGraGru2NO_Tipo_Item: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbGraGru2BeforePost(DataSet: TDataSet);
    procedure TbGraGru2Deleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbGraGru2AfterPost(DataSet: TDataSet);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbGraGru2AfterInsert(DataSet: TDataSet);
    procedure TbGraGru2NewRecord(DataSet: TDataSet);
    procedure TbGraGru2BeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure TbGraGru2CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FPermiteExcluir: Boolean;
    FNovoCodigo: TNovoCodigo;
    FReservados: Variant;
    FSoReserva: Boolean;
    FFldID: String;
    FFldNome: String;
    FDBGInsert: Boolean;
    FModoInclusao: TdmkModoInclusao;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    FFldIndices: array of String;
    FValIndices: array of Variant;
*)
    //
    procedure ReabrePesquisa();
  end;

  var
  FmGraGruNiveis2: TFmGraGruNiveis2;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista,
{$IfNDef NO_FINANCEIRO}UnFinanceiro, {$EndIf}
UnGrade_Jan;

{$R *.DFM}

procedure TFmGraGruNiveis2.BtAlteraClick(Sender: TObject);
begin
(*  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, TmySQLQuery(TBGraGru2), [PnDados],
  [PnEdita], EdNome, ImgTipo, TBGraGru2.TableName);
*)
  Grade_Jan.MostraFormEditGraGru2(TbGraGru2Nivel2.Value, TbGraGru2Nome.Value,
  TbGraGru2Tipo_Item.Value);
  //ReopenGraGru2(QrGraGru2Nivel2.Value);
  TbGraGru2.Refresh;
end;

procedure TFmGraGruNiveis2.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32(TBGraGru2.TableName, FFldID, '', '',
    tsPos, ImgTipo.SQLType, TBGraGru2Codigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    TBGraGru2.TableName, Codigo, Dmod.QrUpd, [PnEdita], [PnDados],
    ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
*)
    TBGraGru2.Refresh;
    TBGraGru2.Locate(FFldID, Codigo, []);
  end;
}
end;

procedure TFmGraGruNiveis2.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmGraGruNiveis2.BtIncluiClick(Sender: TObject);
begin
(*
  UnCfgCadLista.IncluiNovoRegistro(FModoInclusao, Self, PnEdita, [PnDados],
  [PnEdita], EdNome, ImgTipo, FReservados, TBGraGru2, FFldID);
*)
end;

procedure TFmGraGruNiveis2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruNiveis2.DBGrid2DblClick(Sender: TObject);
begin
  UnCfgCadLista.LocalizaPesquisadoInt1(TBGraGru2, QrPesq, FFldID, True);
end;

procedure TFmGraGruNiveis2.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmGraGruNiveis2.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
  BtInclui.Visible := FReservados <> Null;
end;

procedure TFmGraGruNiveis2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align := alClient;
  FNovoCodigo := ncGerlSeq1;
  FReservados := Null;
  FSoReserva := True;
  FPermiteExcluir := False;
  FModoInclusao := dmkmiAutomatico;
  FFldID := 'Nivel2';
  FFldNome := CO_NOME;
  //
  TbGraGru2.TableName := 'gragru2';
end;

procedure TFmGraGruNiveis2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruNiveis2.FormShow(Sender: TObject);
begin
  UnDmkDAC_PF.AbreTable(TBGraGru2, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
end;

procedure TFmGraGruNiveis2.LbItensMDDblClick(Sender: TObject);
begin
  UnCfgCadLista.AddVariavelEmTexto(LbItensMD, TBGraGru2, FFldNome, DBGrid1);
end;

procedure TFmGraGruNiveis2.ReabrePesquisa();
begin
  UnCfgCadLista.ReabrePesquisa(
    QrPesq, TBGraGru2, EdPesq, TBTam, FFldID, FFldNome);
end;

procedure TFmGraGruNiveis2.TbGraGru2AfterInsert(DataSet: TDataSet);
begin
 if FDBGInsert = false then
   TBGraGru2.Cancel;
end;

procedure TFmGraGruNiveis2.TbGraGru2AfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TBGraGru2Nivel2.Value;
end;

procedure TFmGraGruNiveis2.TbGraGru2BeforeOpen(DataSet: TDataSet);
(*
var
  I: Integer;
  Txt: String;
*)
begin
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
  I := High(FFldIndices);
  TBGraGru2.Filter := '';
  TBGraGru2.Close;  // Ter certeza ??
  TBGraGru2.Filtered := False;
  for I := Low(FFldIndices) to High(FFldIndices) do
  begin
    Txt := 'and ' + FFldIndices[I] + Geral.VariavelToString(FFldIndices[I]);
  end;
  if Txt <> '' then
  begin
    Txt := Copy(Txt, 4);
    TBGraGru2.Filter := Txt;
    TBGraGru2.Filtered := True;
  end;
*)
end;

procedure TFmGraGruNiveis2.TbGraGru2BeforePost(DataSet: TDataSet);
begin
  UnCfgCadLista.TbBeforePost(TBGraGru2, FNovoCodigo, FFldID, FReservados);
  //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    for I := Low(FFldIndices) to High(FFldIndices) do
      TBGraGru2.FieldByName(FFldIndices[I]).AsVariant := FFldIndices[I];
*)
end;

procedure TFmGraGruNiveis2.TbGraGru2CalcFields(DataSet: TDataSet);
begin
{$IfNDef NO_FINANCEIRO}
  TbGraGru2NO_Tipo_Item.Value := UFinanceiro.SPED_Tipo_Item_Get(TbGraGru2Tipo_Item.Value);
{$EndIf}
end;

procedure TFmGraGruNiveis2.TbGraGru2Deleting(Sender: TObject; var Allow: Boolean);
begin
  if FPermiteExcluir then
  begin
   Allow := Geral.MB_Pergunta(
   'Confirma a exclus�o do registro selecionado?') = ID_YES;
  end
  else
    Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmGraGruNiveis2.TbGraGru2NewRecord(DataSet: TDataSet);
begin
  if FDBGInsert = False then
    TBGraGru2.Cancel;
end;

procedure TFmGraGruNiveis2.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
