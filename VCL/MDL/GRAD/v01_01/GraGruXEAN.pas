unit GraGruXEAN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, DmkDAC_PF, mySQLDbTables;

type
  TFmGraGruXEAN = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GradeC: TStringGrid;
    GradeEAN: TStringGrid;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    QrLoc: TmySQLQuery;
    PB1: TProgressBar;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GradeEANKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeEANDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    function LocalizaEANnoGraGruX(EAN: String): Integer;
  public
    { Public declarations }
    FGrade, FNivel1: Integer;
  end;

  var
  FmGraGruXEAN: TFmGraGruXEAN;

implementation

uses UnMyObjects, Module, ModProd, UMySQLModule, MyDBCheck;

{$R *.DFM}

function TFmGraGruXEAN.LocalizaEANnoGraGruX(EAN: String): Integer;
begin
  if EAN <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      'WHERE EAN13 = ' + EAN,
      '']);
    if QrLoc.RecordCount > 0 then
      Result := QrLoc.FieldByName('Controle').AsInteger
    else
      Result := 0;
  end else
    Result := 0;
end;

procedure TFmGraGruXEAN.BitBtn1Click(Sender: TObject);
var
  GraGruX: Integer;
  cProd: String;
begin
(* Ver o que fazer!  Mudou para GraGruECad e GraGruEIts!
  if DBCheck.LiberaPelaSenhaAdmin then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT cProd, GraGruX ',
      'FROM gragrue ',
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        GraGruX := QrLoc.FieldByName('GraGruX').AsInteger;
        cProd   := QrLoc.FieldByName('cProd').AsString;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE gragrux SET EAN13="' + cProd + '"',
          'WHERE Controle=' + Geral.FF0(GraGruX),
          '']);
        //
        QrLoc.Next;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM gragrue ',
        '']);
    end;
    Geral.MB_Aviso('Atualiza��o finalizada!');
  end;
*)
end;

procedure TFmGraGruXEAN.BtOKClick(Sender: TObject);
var
  I, J, GraGruX, GraGruXOld, Len: Integer;
  EAN: String;
  Janela, Descricao: array of string;
begin
  try
    Screen.Cursor := crHourGlass;
    PB1.Visible   := True;
    PB1.Max       := GradeEAN.ColCount;
    PB1.Position  := 0;
    //
    for I := 1 to GradeEAN.ColCount - 1 do
    begin
      for J := 1 to GradeEAN.RowCount - 1 do
      begin
        GraGruX := Geral.IMV(GradeC.Cells[I, J]);
        EAN     := GradeEAN.Cells[I, J];
        EAN     := StringReplace(EAN, #$D#$A, '', [rfReplaceAll]);
        //
        if GraGruX <> 0 then
        begin
          GraGruXOld := LocalizaEANnoGraGruX(EAN);
          //
          if GraGruXOld = 0 then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, ['EAN13'],
              ['Controle'], [EAN], [GraGruX], False);
            //
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end else
          begin
            if GraGruXOld <> GraGruX then
            begin
              Len := Length(Janela) + 1;
              //
              SetLength(Janela, Len);
              SetLength(Descricao, Len);
              //
              Janela[Len - 1]   := '';
              Descricao[Len -1] := 'O c�digo de barras ' + EAN + ' j� foi adicionado para a reduzido n�mero ' + Geral.FF0(GraGruXOld);
            end;
          end;
        end;
      end;
    end;
    PB1.Visible := False;
  finally
    if Len > 0 then
      DBCheck.MostraInfoSeq(Janela, Descricao);
    //
    Screen.Cursor := crDefault;
    Close;
  end;
end;

procedure TFmGraGruXEAN.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruXEAN.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruXEAN.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmGraGruXEAN.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruXEAN.FormShow(Sender: TObject);
begin
  if DmProd.PreparaGrades(FGrade, FNivel1, [GradeC, GradeEAN]) then
  begin
    DmProd.AtualizaGradesAtivos1(FNivel1, GradeA, GradeC);
    DmProd.AtualizaGradesEANGeral(FNivel1, GradeEAN);
  end;
end;

procedure TFmGraGruXEAN.GradeEANDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeEAN, GradeA, nil, ACol, ARow, Rect, State,
    '', 0, 0, False);
end;

procedure TFmGraGruXEAN.GradeEANKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeEAN, GradeC, Key, Shift);
end;

end.
