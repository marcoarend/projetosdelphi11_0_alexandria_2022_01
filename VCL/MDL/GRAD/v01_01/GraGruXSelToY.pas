unit GraGruXSelToY;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmGraGruXSelToY = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    DsGraGruX: TDataSource;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGraGruX, FGraGruY: Integer;
  end;

  var
  FmGraGruXSelToY: TFmGraGruXSelToY;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmGraGruXSelToY.BtOKClick(Sender: TObject);
var
  Controle, GraGruY: Integer;
begin
  Controle := EdGraGruX.ValueVariant;
  //
  if MyObjects.FIC(Controle = 0, EdGraGruX, 'Selecione o reduzido!') then
    Exit;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
  'GraGruY'], ['Controle'], [FGraGruY], [Controle], True) then
  begin
    FGraGruX := Controle;
    Close;
  end;
end;

procedure TFmGraGruXSelToY.BtSaidaClick(Sender: TObject);
begin
  FGraGruX := 0;
  Close;
end;

procedure TFmGraGruXSelToY.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruXSelToY.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FGraGruX := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE ggx.GraGruY=0 ',
  'AND ggx.Controle > 0 ',
  ' ']);
  //
end;

procedure TFmGraGruXSelToY.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
