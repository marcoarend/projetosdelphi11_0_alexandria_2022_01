object FmGraGruAtr: TFmGraGruAtr
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-007 :: Atributo de Grupo de Produtos'
  ClientHeight = 307
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 560
    Height = 151
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 52
      Width = 60
      Height = 13
      Caption = 'Atributo [F3]:'
    end
    object Label2: TLabel
      Left = 20
      Top = 96
      Width = 97
      Height = 13
      Caption = 'Item do atributo [F3]:'
    end
    object Label3: TLabel
      Left = 20
      Top = 12
      Width = 46
      Height = 13
      Caption = 'ID Grupo:'
      FocusControl = DBEdit1
    end
    object Label4: TLabel
      Left = 104
      Top = 12
      Width = 74
      Height = 13
      Caption = 'ID item atributo:'
      FocusControl = DBEdit1
    end
    object SpeedButton1: TSpeedButton
      Left = 501
      Top = 68
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 501
      Top = 112
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object EdGraAtrCad: TdmkEditCB
      Left = 20
      Top = 68
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraAtrCadChange
      OnKeyDown = EdGraAtrCadKeyDown
      DBLookupComboBox = CBGraAtrCad
      IgnoraDBLookupComboBox = False
    end
    object CBGraAtrCad: TdmkDBLookupComboBox
      Left = 76
      Top = 68
      Width = 425
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsGraAtrCad
      TabOrder = 1
      OnKeyDown = CBGraAtrCadKeyDown
      dmkEditCB = EdGraAtrCad
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CBGraAtrIts: TdmkDBLookupComboBox
      Left = 76
      Top = 112
      Width = 425
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsGraAtrIts
      TabOrder = 3
      OnKeyDown = CBGraAtrItsKeyDown
      dmkEditCB = EdGraAtrIts
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGraAtrIts: TdmkEditCB
      Left = 20
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdGraAtrItsKeyDown
      DBLookupComboBox = CBGraAtrIts
      IgnoraDBLookupComboBox = False
    end
    object DBEdit1: TdmkDBEdit
      Left = 20
      Top = 28
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Nivel1'
      DataSource = FmGraGruN.DsGraGru1
      Enabled = False
      TabOrder = 4
      UpdCampo = 'Nivel1'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object EdControle: TdmkEdit
      Left = 104
      Top = 28
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'CTRL_ATR'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 560
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 512
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 464
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 369
        Height = 32
        Caption = 'Atributo de Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 369
        Height = 32
        Caption = 'Atributo de Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 369
        Height = 32
        Caption = 'Atributo de Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 199
    Width = 560
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 556
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 243
    Width = 560
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 556
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 412
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraAtrCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM graatrcad'
      'ORDER BY Nome'
      ' ')
    Left = 184
    Top = 116
    object QrGraAtrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraAtrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraAtrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraAtrCad: TDataSource
    DataSet = QrGraAtrCad
    Left = 212
    Top = 116
  end
  object QrGraAtrIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome '
      'FROM graatrits'
      'WHERE Codigo=:P0'
      'ORDER BY Nome'
      ' ')
    Left = 184
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraAtrItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraAtrItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraAtrItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraAtrIts: TDataSource
    DataSet = QrGraAtrIts
    Left = 212
    Top = 156
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdGraAtrCad
    QryCampo = 'GraAtrCad'
    UpdCampo = 'GraAtrCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 8
    Top = 12
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdGraAtrIts
    QryCampo = 'GraAtrIts'
    UpdCampo = 'GraAtrIts'
    RefCampo = 'Controle'
    UpdType = utYes
    Left = 36
    Top = 12
  end
end
