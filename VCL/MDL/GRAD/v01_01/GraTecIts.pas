unit GraTecIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, MyDBCheck, Mask, DBCtrls, Grids, DBGrids,
  dmkDBGrid, DB, mySQLDbTables, dmkLabel, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkGeral, dmkValUsu, dmkDBEdit, dmkImage, UnDmkEnums;

type
  TFmGraTecIts = class(TForm)
    PnEdita: TPanel;
    PnCabeca: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    QrGraFibCad: TmySQLQuery;
    QrGraFibCadCodigo: TIntegerField;
    QrGraFibCadCodUsu: TIntegerField;
    QrGraFibCadNome: TWideStringField;
    QrGraFibCadSigla: TWideStringField;
    DsGraFibCad: TDataSource;
    EdGraFibCad: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBGraFibCad: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrPesq1: TmySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Sigla: TWideStringField;
    EdPorcentagem: TdmkEdit;
    Label4: TLabel;
    dmkValUsu1: TdmkValUsu;
    Label5: TLabel;
    EdControle: TdmkEdit;
    Label6: TLabel;
    DBGItens: TdmkDBGrid;
    dmkDBEdit1: TdmkDBEdit;
    QrSum1: TmySQLQuery;
    QrSum1Percent: TFloatField;
    Label7: TLabel;
    DBEdPercent: TDBEdit;
    Label8: TLabel;
    Panel3: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBControla: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    Panel7: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdGraFibCadChange(Sender: TObject);
    procedure EdGraFibCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraFibCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBEdPercentChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtAlteraClick(Sender: TObject);
  private
    { Private declarations }
    procedure PesquisaPorSigla(Limpa: Boolean);
    procedure PesquisaPorCodigo();
    procedure AtualizaPercent(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmGraTecIts: TFmGraTecIts;

implementation

uses UnMyObjects, GraTecCad, UnMySQLCuringa, Module, GraFibCad,
  UnInternalConsts, UMySQLModule, UnGOTOy, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraTecIts.AtualizaPercent(Controle: Integer);
var
  Codigo: Integer;
begin
  Codigo := FmGraTecCad.QrGraTecCadCodigo.Value;
  QrSum1.Close;
  QrSum1.Params[0].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(QrSum1, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE grateccad SET Percent=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[00].AsFloat   := QrSum1Percent.Value;
  Dmod.QrUpd.Params[01].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  FmGraTecCad.LocCod(Codigo, Codigo);
  FmGraTecCad.QrGraTecIts.Locate('Controle', Controle, []);
end;

procedure TFmGraTecIts.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, FmGraTecIts, PnEdita, FmGraTecCad.QrGraTecIts, [GBControla],
    [PnEdita], EdGraFibCad, ImgTipo, 'gratecits');
end;

procedure TFmGraTecIts.BtConfirmaClick(Sender: TObject);
var
  Controle: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
(*
  Controle := UMyMod.BuscaEmLivreY_Def('gratecits', 'Controle', ImgTipo.SQLType,
    Geral.IMV(EdControle.Text));
*)
  //
  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BPGS1I32('gratecits', 'Controle', '', '', tsPos, SQLType, Controle);
  EdControle.ValueVariant := Controle;
  UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraTecIts, PnEdita, 'GraTecIts',
    Controle, Dmod.QrUpd, [PnEdita], [GBControla], ImgTipo, True);
  AtualizaPercent(Controle);
end;

procedure TFmGraTecIts.BtDesisteClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := EdControle.ValueVariant;
  //
  //if ImgTipo.SQLType = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'gratecits', Controle);
  //
  UMyMod.UpdUnlockY(Controle, Dmod.MyDB, 'gratecits', 'Controle');
  //
  GBControla.Visible := True;
  PnEdita.Visible    := False;
  ImgTipo.SQLType    := stLok;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraTecIts.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, FmGraTecIts, PnEdita, FmGraTecCad.QrGraTecIts,
    [GBControla], [PnEdita], EdGraFibCad, ImgTipo, 'gratecits');
end;

procedure TFmGraTecIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraTecIts.CBGraFibCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraFibCad', Dmod.MyDB,
    ''(*Extra*), EdGraFibCad, CBGraFibCad, dmktfInteger);
end;

procedure TFmGraTecIts.DBEdPercentChange(Sender: TObject);
begin
  if FmGraTecCad.QrGraTecCadPercent.Value = 100 then
    DBEdPercent.Font.Color := clBlue else
    DBEdPercent.Font.Color := clRed;
end;

procedure TFmGraTecIts.EdGraFibCadChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    PesquisaPorCodigo();
end;

procedure TFmGraTecIts.EdGraFibCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraFibCad', Dmod.MyDB,
    ''(*Extra*), EdGraFibCad, CBGraFibCad, dmktfInteger);
end;

procedure TFmGraTecIts.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    PesquisaPorSigla(False);
end;

procedure TFmGraTecIts.EdSiglaExit(Sender: TObject);
begin
  PesquisaPorSigla(True);
end;

procedure TFmGraTecIts.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraFibCad', Dmod.MyDB,
    ''(*Extra*), EdGraFibCad, CBGraFibCad, dmktfInteger);
end;

procedure TFmGraTecIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraTecIts.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (FmGraTecCad.QrGraTecCadPercent.Value <> 100) and
     (FmGraTecCad.QrGraTecCadPercent.Value <>   0) then
  begin
    Application.MessageBox('Antes de fechar a janela, o percentual total de ' +
    'fibras deve ser 100%', 'Aviso', MB_OK+MB_ICONWARNING);
    CanClose := False;
  end;
end;

procedure TFmGraTecIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrGraFibCad, Dmod.MyDB);
end;

procedure TFmGraTecIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraTecIts.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraFibCad, FmGraFibCad, afmoNegarComAviso) then
  begin
    FmGraFibCad.ShowModal;
    FmGraFibCad.Destroy;
    QrGraFibCad.Close;
    UnDmkDAC_PF.AbreQuery(QrGraFibCad, Dmod.MyDB);
    if VAR_CADASTRO <> 0 then
    begin
      EdGraFibCad.ValueVariant := VAR_CADASTRO;
      CBGraFibCad.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmGraTecIts.PesquisaPorSigla(Limpa: Boolean);
begin
  QrPesq1.Close;
  QrPesq1.Params[0].AsString := EdSigla.Text;
  UnDmkDAC_PF.AbreQuery(QrPesq1, Dmod.MyDB);
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraFibCad.ValueVariant <> QrPesq1CodUsu.Value then
      EdGraFibCad.ValueVariant := QrPesq1CodUsu.Value;
    if CBGraFibCad.KeyValue     <> QrPesq1CodUsu.Value then
      CBGraFibCad.KeyValue     := QrPesq1CodUsu.Value;
  end else if Limpa then
    EdSigla.ValueVariant := '';
end;

procedure TFmGraTecIts.PesquisaPorCodigo();
begin
  QrPesq2.Close;
  QrPesq2.Params[0].AsString := EdGraFibCad.Text;
  UnDmkDAC_PF.AbreQuery(QrPesq2, Dmod.MyDB);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdSigla.ValueVariant <> QrPesq2Sigla.Value then
      EdSigla.ValueVariant := QrPesq2Sigla.Value;
  end else EdSigla.ValueVariant := '';
end;

end.
