unit GraGruX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmGraGruX = class(TForm)
    Panel1: TPanel;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    DBText1: TDBText;
    Panel3: TPanel;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraCorCad: TmySQLQuery;
    DsGraCorCad: TDataSource;
    QrGraTamCad: TmySQLQuery;
    DsGraTamCad: TDataSource;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    Label2: TLabel;
    EdGraTamCad: TdmkEditCB;
    CBGraTamCad: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdGraCorCad: TdmkEditCB;
    CBGraCorCad: TdmkDBLookupComboBox;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadCodUsu: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    QrGraTamCadCodigo: TIntegerField;
    QrGraTamCadCodUsu: TIntegerField;
    QrGraTamCadNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGraGruX: Integer;
  end;

  var
  FmGraGruX: TFmGraGruX;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruX.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruX.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
end;

procedure TFmGraGruX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
