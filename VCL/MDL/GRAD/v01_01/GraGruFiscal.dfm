object FmGraGruFiscal: TFmGraGruFiscal
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-014 :: Configura'#231#245'es Fiscais do Grupo de Produtos'
  ClientHeight = 687
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 531
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 12
        Top = 8
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label4: TLabel
        Left = 80
        Top = 8
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object Label30: TLabel
        Left = 204
        Top = 8
        Width = 115
        Height = 13
        Caption = 'GTIN(EAN, DUN, UPC):'
      end
      object SbNCM: TSpeedButton
        Left = 182
        Top = 24
        Width = 20
        Height = 21
        Caption = '?'
        OnClick = SbNCMClick
      end
      object Label52: TLabel
        Left = 328
        Top = 8
        Width = 219
        Height = 13
        Caption = 'C'#243'd. Barras pr'#243'prio ou de terceiros (NFe I03a):'
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 12
        Top = 24
        Width = 65
        Height = 21
        TabStop = False
        DataField = 'Nivel1'
        DataSource = FmGraGruN.DsGraGru1
        TabOrder = 0
        UpdCampo = 'Nivel1'
        UpdType = utInc
        Alignment = taLeftJustify
      end
      object EdNCM: TdmkEdit
        Left = 80
        Top = 24
        Width = 100
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NCM'
        UpdCampo = 'NCM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNCMRedefinido
      end
      object EdcGTIN_EAN: TdmkEdit
        Left = 204
        Top = 24
        Width = 121
        Height = 21
        MaxLength = 14
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'cGTIN_EAN'
        UpdCampo = 'cGTIN_EAN'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGDadosFisc: TdmkRadioGroup
        Left = 552
        Top = 0
        Width = 225
        Height = 48
        Caption = ' Obten'#231#227'o das informa'#231#245'es Fiscais na NF-e: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Da Regra Fiscal'
          'Daqui do Cadastro')
        TabOrder = 4
        Visible = False
        WordWrap = True
        QryCampo = 'DadosFisc'
        UpdCampo = 'DadosFisc'
        UpdType = utYes
        OldValor = 0
      end
      object Edprod_cBarra: TdmkEdit
        Left = 328
        Top = 24
        Width = 217
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'prod_cBarra'
        UpdCampo = 'prod_cBarra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 48
      Width = 784
      Height = 483
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 483
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 784
          Height = 444
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 57
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label15: TLabel
              Left = 12
              Top = 0
              Width = 197
              Height = 13
              Caption = 'Informa'#231#245'es adicionais do produto (NF-e):'
            end
            object Label16: TLabel
              Left = 532
              Top = 0
              Width = 92
              Height = 13
              Caption = 'Sigla custom. NF-e:'
            end
            object EdInfAdProd: TdmkEdit
              Left = 12
              Top = 16
              Width = 517
              Height = 21
              MaxLength = 255
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'InfAdProd'
              UpdCampo = 'InfAdProd'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object Ckprod_indEscala: TdmkCheckBox
              Left = 12
              Top = 38
              Width = 169
              Height = 17
              Caption = 'Indicador de Escala Relevante'
              TabOrder = 2
              QryCampo = 'prod_indEscala'
              UpdCampo = 'prod_indEscala'
              UpdType = utYes
              ValCheck = 'S'
              ValUncheck = 'N'
              OldValor = #0
            end
            object EdSiglaCustm: TdmkEdit
              Left = 532
              Top = 16
              Width = 96
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SiglaCustm'
              UpdCampo = 'SiglaCustm'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object RGUsaSubsTrib: TdmkRadioGroup
              Left = 632
              Top = 1
              Width = 145
              Height = 48
              Caption = 'Substituto tribut'#225'rio:'
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'N'#227'o'
                'Sim'
                'N/D')
              TabOrder = 3
              QryCampo = 'UsaSubsTrib'
              UpdCampo = 'UsaSubsTrib'
              UpdType = utYes
              OldValor = 0
            end
          end
          object PageControl1: TPageControl
            Left = 0
            Top = 57
            Width = 784
            Height = 387
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 1
            object TabSheet1: TTabSheet
              Caption = ' ICMS '
              object PageControl2: TPageControl
                Left = 0
                Top = 0
                Width = 776
                Height = 359
                ActivePage = TabSheet6
                Align = alClient
                TabOrder = 0
                object TabSheet6: TTabSheet
                  Caption = ' ICMS 1 '
                  object GroupBox1: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 768
                    Height = 331
                    Align = alClient
                    Caption = 
                      ' CST - ICMS (Imposto sobre Circula'#231#227'o de Mercadorias e Servi'#231'os)' +
                      ': '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 0
                    object GroupBox3: TGroupBox
                      Left = 2
                      Top = 93
                      Width = 764
                      Height = 124
                      Align = alTop
                      Caption = ' ICMS Normal: '
                      Color = clBtnFace
                      ParentBackground = False
                      ParentColor = False
                      TabOrder = 1
                      object Panel17: TPanel
                        Left = 2
                        Top = 15
                        Width = 760
                        Height = 78
                        Align = alTop
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object Label8: TLabel
                          Left = 444
                          Top = 0
                          Width = 83
                          Height = 13
                          Caption = 'N14: % Red. BC'#178':'
                          Enabled = False
                        end
                        object Label47: TLabel
                          Left = 444
                          Top = 40
                          Width = 112
                          Height = 13
                          Caption = 'N16b: % do diferimento:'
                        end
                        object Label28: TLabel
                          Left = 532
                          Top = 0
                          Width = 76
                          Height = 13
                          Caption = 'Pre'#231'o da pauta:'
                        end
                        object Label29: TLabel
                          Left = 620
                          Top = 0
                          Width = 81
                          Height = 13
                          Caption = 'Pre'#231'o m'#225'x. Tab.:'
                        end
                        object Label48: TLabel
                          Left = 576
                          Top = 40
                          Width = 123
                          Height = 13
                          Caption = 'N27a: % da desonera'#231#227'o:'
                        end
                        object RGICMS_modBC: TdmkRadioGroup
                          Left = 0
                          Top = 0
                          Width = 435
                          Height = 78
                          Align = alLeft
                          Caption = ' N13 - Modalidade de determina'#231#227'o da BC do ICMS: '
                          Columns = 2
                          Items.Strings = (
                            'Margem valor agregado (%)'
                            'Pauta (valor)'
                            'Pre'#231'o tabelado m'#225'ximo (valor)'
                            'Valor da opera'#231#227'o')
                          TabOrder = 0
                          QryCampo = 'ICMS_modBC'
                          UpdCampo = 'ICMS_modBC'
                          UpdType = utYes
                          OldValor = 0
                        end
                        object EdICMS_pRedBC: TdmkEdit
                          Left = 444
                          Top = 16
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          Enabled = False
                          TabOrder = 1
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMS_pRedBC'
                          UpdCampo = 'ICMS_pRedBC'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pDif: TdmkEdit
                          Left = 444
                          Top = 56
                          Width = 128
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 4
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 4
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,0000'
                          QryCampo = 'ICMS_pDif'
                          UpdCampo = 'ICMS_pDif'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_Pauta: TdmkEdit
                          Left = 532
                          Top = 16
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 2
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMS_Pauta'
                          UpdCampo = 'ICMS_Pauta'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_MaxTab: TdmkEdit
                          Left = 620
                          Top = 16
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 3
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMS_MaxTab'
                          UpdCampo = 'ICMS_MaxTab'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdICMS_pICMSDeson: TdmkEdit
                          Left = 576
                          Top = 56
                          Width = 128
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 5
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 4
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,0000'
                          QryCampo = 'ICMS_pICMSDeson'
                          UpdCampo = 'ICMS_pICMSDeson'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                      end
                      object Panel16: TPanel
                        Left = 2
                        Top = 93
                        Width = 760
                        Height = 29
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        object Label49: TLabel
                          Left = 4
                          Top = 8
                          Width = 159
                          Height = 13
                          Caption = 'Motivo da desonera'#231#227'o do ICMS:'
                        end
                        object EdICMS_motDesICMS: TdmkEdit
                          Left = 168
                          Top = 4
                          Width = 25
                          Height = 21
                          Alignment = taRightJustify
                          CharCase = ecUpperCase
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          QryCampo = 'ICMS_motDesICMS'
                          UpdCampo = 'ICMS_motDesICMS'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          OnChange = EdICMS_motDesICMSChange
                        end
                        object EdICMS_motDesICMS_TXT: TEdit
                          Left = 196
                          Top = 4
                          Width = 564
                          Height = 21
                          ReadOnly = True
                          TabOrder = 1
                        end
                      end
                    end
                    object Panel6: TPanel
                      Left = 2
                      Top = 15
                      Width = 764
                      Height = 78
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label1: TLabel
                        Left = 8
                        Top = 8
                        Width = 156
                        Height = 13
                        Caption = 'N11 - Origem da mercadoria: [F3]'
                      end
                      object Label11: TLabel
                        Left = 8
                        Top = 32
                        Width = 202
                        Height = 13
                        Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                      end
                      object Label51: TLabel
                        Left = 8
                        Top = 55
                        Width = 356
                        Height = 13
                        Caption = 
                          'N12a - C'#243'digo de Situa'#231#227'o da Opera'#231#227'o - Simples Nacional (CSOSN)' +
                          ': [F3] '
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                      end
                      object EdCST_A: TdmkEdit
                        Left = 220
                        Top = 4
                        Width = 45
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0'
                        QryCampo = 'CST_A'
                        UpdCampo = 'CST_A'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        OnChange = EdCST_AChange
                        OnKeyDown = EdCST_AKeyDown
                      end
                      object EdTextoA: TdmkEdit
                        Left = 268
                        Top = 4
                        Width = 492
                        Height = 21
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                        OnKeyDown = EdCST_AKeyDown
                      end
                      object EdCST_B: TdmkEdit
                        Left = 220
                        Top = 28
                        Width = 45
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 2
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '00'
                        QryCampo = 'CST_B'
                        UpdCampo = 'CST_B'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        OnChange = EdCST_BChange
                        OnKeyDown = EdCST_BKeyDown
                      end
                      object EdTextoB: TdmkEdit
                        Left = 268
                        Top = 28
                        Width = 492
                        Height = 21
                        TabOrder = 3
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdCSOSN: TdmkEdit
                        Left = 368
                        Top = 52
                        Width = 35
                        Height = 21
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 4
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 2
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryCampo = 'CSOSN'
                        UpdCampo = 'CSOSN'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                        OnChange = EdCSOSNChange
                        OnKeyDown = EdCSOSNKeyDown
                      end
                      object EdTextoC: TdmkEdit
                        Left = 404
                        Top = 52
                        Width = 356
                        Height = 21
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 5
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                  end
                end
                object TabSheet7: TTabSheet
                  Caption = ' ICMS 2 '
                  ImageIndex = 1
                  object GroupBox15: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 768
                    Height = 331
                    Align = alClient
                    TabOrder = 0
                    ExplicitLeft = 128
                    ExplicitTop = 96
                    ExplicitWidth = 185
                    ExplicitHeight = 105
                    object GroupBox9: TGroupBox
                      Left = 2
                      Top = 15
                      Width = 764
                      Height = 92
                      Align = alTop
                      Caption = ' ICMS ST ( Substitui'#231#227'o Tribut'#225'ria): '
                      Color = clBtnFace
                      ParentBackground = False
                      ParentColor = False
                      TabOrder = 0
                      ExplicitLeft = 4
                      ExplicitTop = 19
                      object Panel2: TPanel
                        Left = 2
                        Top = 15
                        Width = 760
                        Height = 75
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object Label36: TLabel
                          Left = 440
                          Top = 0
                          Width = 86
                          Height = 13
                          Caption = 'N19: % MVA. BC'#178':'
                        end
                        object Label37: TLabel
                          Left = 528
                          Top = 0
                          Width = 83
                          Height = 13
                          Caption = 'N20: % Red. BC'#178':'
                        end
                        object Label38: TLabel
                          Left = 616
                          Top = 0
                          Width = 77
                          Height = 13
                          Caption = 'N22: % Aliq. ST:'
                        end
                        object Label50: TLabel
                          Left = 440
                          Top = 44
                          Width = 178
                          Height = 13
                          Caption = 'I05w: CEST - C'#243'd. Ident. Subst. Trib.:'
                        end
                        object dmkRadioGroup1: TdmkRadioGroup
                          Left = 0
                          Top = 0
                          Width = 435
                          Height = 75
                          Align = alLeft
                          Caption = ' N18 - Modalidade de determina'#231#227'o da BC do ICMS: '
                          Columns = 2
                          Items.Strings = (
                            '0 - Pre'#231'o tabelado ou m'#225'ximo sugerido'
                            '1 - Lista Negativa (valor)'
                            '2 - Lista Positiva (valor)'
                            '3 - Lista Neutra (valor)'
                            '4 - Margem valor agregado (%)'
                            '5 - Pauta (valor)')
                          TabOrder = 0
                          QryCampo = 'ICMS_modBCST'
                          UpdCampo = 'ICMS_modBCST'
                          UpdType = utYes
                          OldValor = 0
                        end
                        object dmkEdit1: TdmkEdit
                          Left = 440
                          Top = 16
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMS_pMVAST'
                          UpdCampo = 'ICMS_pMVAST'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object dmkEdit2: TdmkEdit
                          Left = 528
                          Top = 16
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 2
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMS_pRedBCST'
                          UpdCampo = 'ICMS_pRedBCST'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object dmkEdit3: TdmkEdit
                          Left = 616
                          Top = 16
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 3
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMS_pICMSST'
                          UpdCampo = 'ICMS_pICMSST'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object Edprod_CEST: TdmkEdit
                          Left = 632
                          Top = 40
                          Width = 68
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 4
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 7
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0000000'
                          QryCampo = 'prod_CEST'
                          UpdCampo = 'prod_CEST'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                        end
                      end
                    end
                    object Panel13: TPanel
                      Left = 2
                      Top = 107
                      Width = 764
                      Height = 84
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 1
                      ExplicitTop = 139
                      ExplicitWidth = 277
                      object GroupBox4: TGroupBox
                        Left = 0
                        Top = 0
                        Width = 366
                        Height = 84
                        Align = alLeft
                        Caption = ' Recupera'#231#227'o de ICMS de Notas Fiscais de entrada: '
                        Color = clBtnFace
                        ParentBackground = False
                        ParentColor = False
                        TabOrder = 0
                        object Label12: TLabel
                          Left = 12
                          Top = 20
                          Width = 57
                          Height = 13
                          Caption = '% Red. BC'#178':'
                        end
                        object Label40: TLabel
                          Left = 100
                          Top = 20
                          Width = 54
                          Height = 13
                          Caption = '% Al'#237'quota:'
                        end
                        object EdICMSRec_pRedBC: TdmkEdit
                          Left = 12
                          Top = 36
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '100,00'
                          QryCampo = 'ICMSRec_pRedBC'
                          UpdCampo = 'ICMSRec_pRedBC'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 100.000000000000000000
                          ValWarn = False
                        end
                        object EdICMSRec_pAliq: TdmkEdit
                          Left = 100
                          Top = 36
                          Width = 57
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMSRec_pAliq'
                          UpdCampo = 'ICMSRec_pAliq'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object RGICMSRec_tCalc: TdmkRadioGroup
                          Left = 164
                          Top = 12
                          Width = 197
                          Height = 47
                          Caption = ' C'#225'lculo: '
                          Columns = 2
                          ItemIndex = 1
                          Items.Strings = (
                            'Pela aliquota'
                            'Pela NFe')
                          TabOrder = 2
                          QryCampo = 'ICMSRec_tCalc'
                          UpdCampo = 'ICMSRec_tCalc'
                          UpdType = utYes
                          OldValor = 0
                        end
                      end
                      object GroupBox13: TGroupBox
                        Left = 366
                        Top = 0
                        Width = 191
                        Height = 84
                        Align = alLeft
                        Caption = ' SINTEGRA: '
                        TabOrder = 1
                        object Label44: TLabel
                          Left = 12
                          Top = 20
                          Width = 75
                          Height = 13
                          Caption = '% ICMS interno:'
                        end
                        object Label45: TLabel
                          Left = 100
                          Top = 20
                          Width = 73
                          Height = 13
                          Caption = 'Base ICMS ST:'
                        end
                        object EdICMSAliqSINTEGRA: TdmkEdit
                          Left = 12
                          Top = 36
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '17,00'
                          QryCampo = 'ICMSAliqSINTEGRA'
                          UpdCampo = 'ICMSAliqSINTEGRA'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 17.000000000000000000
                          ValWarn = False
                        end
                        object EdICMSST_BaseSINTEGRA: TdmkEdit
                          Left = 100
                          Top = 36
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 1
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'ICMSST_BaseSINTEGRA'
                          UpdCampo = 'ICMSST_BaseSINTEGRA'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                      end
                      object GroupBox14: TGroupBox
                        Left = 557
                        Top = 0
                        Width = 207
                        Height = 84
                        Align = alClient
                        Caption = 'SPED EFD: '
                        TabOrder = 2
                        object Label46: TLabel
                          Left = 12
                          Top = 20
                          Width = 75
                          Height = 13
                          Caption = '% ICMS interno:'
                        end
                        object EdSPEDEFD_ALIQ_ICMS: TdmkEdit
                          Left = 12
                          Top = 36
                          Width = 84
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '0'
                          ValMax = '100'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          QryCampo = 'SPEDEFD_ALIQ_ICMS'
                          UpdCampo = 'SPEDEFD_ALIQ_ICMS'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                      end
                    end
                  end
                end
              end
            end
            object TabSheet2: TTabSheet
              Caption = ' IPI '
              ImageIndex = 1
              object GroupBox2: TGroupBox
                Left = 0
                Top = 0
                Width = 776
                Height = 61
                Align = alTop
                Caption = ' CST - IPI (Imposto sobre produtos industrializados): '
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 0
                object Label10: TLabel
                  Left = 8
                  Top = 16
                  Width = 199
                  Height = 13
                  Caption = 'O09: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                end
                object EdIPI_CST: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 45
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '00'
                  QryCampo = 'IPI_CST'
                  UpdCampo = 'IPI_CST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdIPI_CSTChange
                  OnKeyDown = EdIPI_CSTKeyDown
                end
                object EdTextoIPI_CST: TdmkEdit
                  Left = 56
                  Top = 32
                  Width = 709
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object Panel12: TPanel
                Left = 0
                Top = 61
                Width = 776
                Height = 298
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label27: TLabel
                  Left = 112
                  Top = 16
                  Width = 158
                  Height = 13
                  Caption = 'O12: Valor por unidade tribut'#225'vel:'
                end
                object Label17: TLabel
                  Left = 112
                  Top = 40
                  Width = 109
                  Height = 13
                  Caption = 'O13: % Aliquota de IPI:'
                end
                object Label31: TLabel
                  Left = 112
                  Top = 88
                  Width = 112
                  Height = 13
                  Caption = 'I06: C'#243'digo EX da TIPI:'
                end
                object Label9: TLabel
                  Left = 112
                  Top = 64
                  Width = 140
                  Height = 13
                  Caption = 'O06: C'#243'd. enq. legal IPI'#185': [F4]'
                end
                object RGIPI_TpTrib: TdmkRadioGroup
                  Left = 8
                  Top = 4
                  Width = 97
                  Height = 101
                  Caption = ' Tipo tributa'#231#227'o: '
                  ItemIndex = 0
                  Items.Strings = (
                    'Nenhuma'
                    'Unidade'
                    'Al'#237'quota')
                  TabOrder = 0
                  QryCampo = 'IPI_TpTrib'
                  UpdCampo = 'IPI_TpTrib'
                  UpdType = utYes
                  OldValor = 0
                end
                object EdIPI_vUnid: TdmkEdit
                  Left = 272
                  Top = 12
                  Width = 121
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryCampo = 'IPI_vUnid'
                  UpdCampo = 'IPI_vUnid'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdIPI_pIPI: TdmkEdit
                  Left = 272
                  Top = 36
                  Width = 121
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'IPI_pIPI'
                  UpdCampo = 'IPI_pIPI'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdIPI_cEnq: TdmkEdit
                  Left = 272
                  Top = 60
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 3
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '999'
                  QryCampo = 'IPI_cEnq'
                  UpdCampo = 'IPI_cEnq'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 999
                  ValWarn = False
                  OnChange = EdIPI_cEnqChange
                  OnKeyDown = EdIPI_cEnqKeyDown
                end
                object EdEX_TIPI: TdmkEdit
                  Left = 272
                  Top = 88
                  Width = 121
                  Height = 21
                  Alignment = taRightJustify
                  MaxLength = 3
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 3
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '000'
                  QryCampo = 'EX_TIPI'
                  UpdCampo = 'EX_TIPI'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object GroupBox10: TGroupBox
                  Left = 0
                  Top = 234
                  Width = 776
                  Height = 64
                  Align = alBottom
                  Caption = ' Recupera'#231#227'o de IPI: '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 5
                  object Label6: TLabel
                    Left = 12
                    Top = 20
                    Width = 57
                    Height = 13
                    Caption = '% Red. BC'#178':'
                  end
                  object Label41: TLabel
                    Left = 100
                    Top = 20
                    Width = 54
                    Height = 13
                    Caption = '% Al'#237'quota:'
                  end
                  object EdIPIRec_pRedBC: TdmkEdit
                    Left = 12
                    Top = 36
                    Width = 84
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '100'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '100,00'
                    QryCampo = 'IPIRec_pRedBC'
                    UpdCampo = 'IPIRec_pRedBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 100.000000000000000000
                    ValWarn = False
                  end
                  object EdIPIRec_pAliq: TdmkEdit
                    Left = 100
                    Top = 36
                    Width = 57
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '100'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'IPIRec_pAliq'
                    UpdCampo = 'IPIRec_pAliq'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object RgIPIRec_tCalc: TdmkRadioGroup
                    Left = 164
                    Top = 12
                    Width = 197
                    Height = 47
                    Caption = ' C'#225'lculo: '
                    Columns = 2
                    ItemIndex = 1
                    Items.Strings = (
                      'Pela aliquota'
                      'Pela NFe')
                    TabOrder = 2
                    QryCampo = 'IPIRec_tCalc'
                    UpdCampo = 'IPIRec_tCalc'
                    UpdType = utYes
                    OldValor = 0
                  end
                end
                object EdIPI_cEnqGrupo: TdmkEdit
                  Left = 312
                  Top = 60
                  Width = 81
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'Outros'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'Outros'
                  ValWarn = False
                end
                object MeIPI_cEnqTXT: TMemo
                  Left = 396
                  Top = 12
                  Width = 369
                  Height = 97
                  Lines.Strings = (
                    'Tributa'#231#227'o normal IPI; Outros;')
                  TabOrder = 7
                end
              end
            end
            object TabSheet3: TTabSheet
              Caption = ' PIS '
              ImageIndex = 2
              object GroupBox5: TGroupBox
                Left = 0
                Top = 0
                Width = 776
                Height = 61
                Align = alTop
                Caption = ' CST - PIS (Programa de Integra'#231#227'o Social): '
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 0
                object Label7: TLabel
                  Left = 8
                  Top = 16
                  Width = 199
                  Height = 13
                  Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                end
                object EdPIS_CST: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 45
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '1'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '01'
                  QryCampo = 'PIS_CST'
                  UpdCampo = 'PIS_CST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                  OnChange = EdPIS_CSTChange
                  OnKeyDown = EdPIS_CSTKeyDown
                end
                object EdTextoPIS_CST: TdmkEdit
                  Left = 56
                  Top = 32
                  Width = 709
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox6: TGroupBox
                Left = 0
                Top = 109
                Width = 776
                Height = 250
                Align = alClient
                Caption = ' PIS ST: '
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 2
                object Label20: TLabel
                  Left = 8
                  Top = 16
                  Width = 86
                  Height = 13
                  Caption = 'R03: Aliq. PIS (%):'
                end
                object Label21: TLabel
                  Left = 144
                  Top = 16
                  Width = 84
                  Height = 13
                  Caption = 'R05: Aliq. PIS ($):'
                end
                object Label33: TLabel
                  Left = 280
                  Top = 16
                  Width = 57
                  Height = 13
                  Caption = '% Red. BC'#178':'
                end
                object EdPISST_AlqP: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'PISST_AlqP'
                  UpdCampo = 'PISST_AlqP'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPISST_AlqV: TdmkEdit
                  Left = 144
                  Top = 32
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'PISST_AlqV'
                  UpdCampo = 'PISST_AlqV'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPISST_pRedBCST: TdmkEdit
                  Left = 280
                  Top = 32
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'PISST_pRedBCST'
                  UpdCampo = 'PISST_pRedBCST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object GroupBox11: TGroupBox
                  Left = 2
                  Top = 184
                  Width = 772
                  Height = 64
                  Align = alBottom
                  Caption = ' Recupera'#231#227'o de PIS: '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 3
                  object Label14: TLabel
                    Left = 12
                    Top = 20
                    Width = 57
                    Height = 13
                    Caption = '% Red. BC'#178':'
                  end
                  object Label42: TLabel
                    Left = 100
                    Top = 20
                    Width = 54
                    Height = 13
                    Caption = '% Al'#237'quota:'
                  end
                  object EdPISRec_pRedBC: TdmkEdit
                    Left = 12
                    Top = 36
                    Width = 84
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '100'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '100,00'
                    QryCampo = 'PISRec_pRedBC'
                    UpdCampo = 'PISRec_pRedBC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 100.000000000000000000
                    ValWarn = False
                  end
                  object EdPISRec_pAliq: TdmkEdit
                    Left = 100
                    Top = 36
                    Width = 57
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ValMax = '100'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'PISRec_pAliq'
                    UpdCampo = 'PISRec_pAliq'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object RgPISRec_tCalc: TdmkRadioGroup
                    Left = 164
                    Top = 12
                    Width = 197
                    Height = 47
                    Caption = ' C'#225'lculo: '
                    Columns = 2
                    ItemIndex = 1
                    Items.Strings = (
                      'Pela aliquota'
                      'Pela NFe')
                    TabOrder = 2
                    QryCampo = 'PISRec_tCalc'
                    UpdCampo = 'PISRec_tCalc'
                    UpdType = utYes
                    OldValor = 0
                  end
                end
              end
              object Panel10: TPanel
                Left = 0
                Top = 61
                Width = 776
                Height = 48
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label19: TLabel
                  Left = 144
                  Top = 4
                  Width = 84
                  Height = 13
                  Caption = 'Q11: Aliq. PIS ($):'
                end
                object Label18: TLabel
                  Left = 8
                  Top = 4
                  Width = 86
                  Height = 13
                  Caption = 'Q08: Aliq. PIS (%):'
                end
                object Label32: TLabel
                  Left = 280
                  Top = 4
                  Width = 57
                  Height = 13
                  Caption = '% Red. BC'#178':'
                end
                object EdPIS_AlqV: TdmkEdit
                  Left = 144
                  Top = 20
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'PIS_AlqV'
                  UpdCampo = 'PIS_AlqV'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPIS_AlqP: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'PIS_AlqP'
                  UpdCampo = 'PIS_AlqP'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPIS_pRedBC: TdmkEdit
                  Left = 280
                  Top = 20
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'PIS_pRedBC'
                  UpdCampo = 'PIS_pRedBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object TabSheet4: TTabSheet
              Caption = ' COFINS '
              ImageIndex = 3
              object GroupBox7: TGroupBox
                Left = 0
                Top = 0
                Width = 776
                Height = 61
                Align = alTop
                Caption = 
                  ' CST - COFINS (Contribui'#231#227'o para o Financiamento da Seguridade S' +
                  'ocial): '
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 0
                object Label22: TLabel
                  Left = 8
                  Top = 16
                  Width = 198
                  Height = 13
                  Caption = 'S06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                end
                object EdCOFINS_CST: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 45
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '1'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '01'
                  QryCampo = 'COFINS_CST'
                  UpdCampo = 'COFINS_CST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                  OnChange = EdCOFINS_CSTChange
                  OnKeyDown = EdCOFINS_CSTKeyDown
                end
                object EdTextoCOFINS_CST: TdmkEdit
                  Left = 56
                  Top = 32
                  Width = 709
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object Panel11: TPanel
                Left = 0
                Top = 61
                Width = 776
                Height = 48
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label23: TLabel
                  Left = 144
                  Top = 4
                  Width = 105
                  Height = 13
                  Caption = 'S10: Aliq. COFINS ($):'
                end
                object Label24: TLabel
                  Left = 8
                  Top = 4
                  Width = 107
                  Height = 13
                  Caption = 'S08: Aliq. COFINS (%):'
                end
                object Label34: TLabel
                  Left = 280
                  Top = 4
                  Width = 57
                  Height = 13
                  Caption = '% Red. BC'#178':'
                end
                object EdCOFINS_AlqV: TdmkEdit
                  Left = 144
                  Top = 20
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'COFINS_AlqV'
                  UpdCampo = 'COFINS_AlqV'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCOFINS_AlqP: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'COFINS_AlqP'
                  UpdCampo = 'COFINS_AlqP'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCOFINS_pRedBC: TdmkEdit
                  Left = 280
                  Top = 20
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'COFINS_pRedBC'
                  UpdCampo = 'COFINS_pRedBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox8: TGroupBox
                Left = 0
                Top = 109
                Width = 776
                Height = 185
                Align = alClient
                Caption = ' PIS ST: '
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 2
                ExplicitHeight = 192
                object Label25: TLabel
                  Left = 8
                  Top = 16
                  Width = 107
                  Height = 13
                  Caption = 'T03: Aliq. COFINS (%):'
                end
                object Label26: TLabel
                  Left = 144
                  Top = 16
                  Width = 105
                  Height = 13
                  Caption = 'T05: Aliq. COFINS ($):'
                end
                object Label35: TLabel
                  Left = 280
                  Top = 16
                  Width = 57
                  Height = 13
                  Caption = '% Red. BC'#178':'
                end
                object EdCOFINSST_AlqP: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'COFINSST_AlqP'
                  UpdCampo = 'COFINSST_AlqP'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCOFINSST_AlqV: TdmkEdit
                  Left = 144
                  Top = 32
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'COFINSST_AlqV'
                  UpdCampo = 'COFINSST_AlqV'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCOFINSST_pRedBCST: TdmkEdit
                  Left = 280
                  Top = 32
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'COFINSST_pRedBCST'
                  UpdCampo = 'COFINSST_pRedBCST'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox12: TGroupBox
                Left = 0
                Top = 294
                Width = 776
                Height = 65
                Align = alBottom
                Caption = ' Recupera'#231#227'o de COFINS: '
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 3
                ExplicitTop = 301
                object Label39: TLabel
                  Left = 12
                  Top = 20
                  Width = 57
                  Height = 13
                  Caption = '% Red. BC'#178':'
                end
                object Label43: TLabel
                  Left = 100
                  Top = 20
                  Width = 54
                  Height = 13
                  Caption = '% Al'#237'quota:'
                end
                object EdCOFINSRec_pRedBC: TdmkEdit
                  Left = 12
                  Top = 36
                  Width = 84
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '100,00'
                  QryCampo = 'COFINSRec_pRedBC'
                  UpdCampo = 'COFINSRec_pRedBC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 100.000000000000000000
                  ValWarn = False
                end
                object EdCOFINSRec_pAliq: TdmkEdit
                  Left = 100
                  Top = 36
                  Width = 57
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'COFINSRec_pAliq'
                  UpdCampo = 'COFINSRec_pAliq'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object RgCOFINSRec_tCalc: TdmkRadioGroup
                  Left = 164
                  Top = 12
                  Width = 197
                  Height = 47
                  Caption = ' C'#225'lculo: '
                  Columns = 2
                  ItemIndex = 1
                  Items.Strings = (
                    'Pela aliquota'
                    'Pela NFe')
                  TabOrder = 2
                  QryCampo = 'COFINSRec_tCalc'
                  UpdCampo = 'COFINSRec_tCalc'
                  UpdType = utYes
                  OldValor = 0
                end
              end
            end
            object TabSheet5: TTabSheet
              Caption = 'SPED'
              ImageIndex = 4
            end
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 444
          Width = 784
          Height = 39
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object Label2: TLabel
            Left = 12
            Top = 4
            Width = 612
            Height = 13
            Caption = 
              #185': C'#243'digo de enquadramento legal do IPI. Tabela a ser criada pel' +
              'a Receita Federal, informar 999 enquanto a tabela n'#227'o for criada' +
              '.'
          end
          object Label5: TLabel
            Left = 12
            Top = 20
            Width = 236
            Height = 13
            Caption = #178': Percentual de redu'#231#227'o da BC (base de c'#225'lculo).'
          end
          object Label13: TLabel
            Left = 340
            Top = 20
            Width = 284
            Height = 13
            Caption = #179': Margem do valor agregado adicionado ao ICMS ST (N19).'
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 544
        Height = 32
        Caption = 'Configura'#231#245'es Fiscais do Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 544
        Height = 32
        Caption = 'Configura'#231#245'es Fiscais do Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 544
        Height = 32
        Caption = 'Configura'#231#245'es Fiscais do Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 579
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel14: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 707
        Height = 17
        Caption = 
          'O valor aproximado dos produtos calculado pelo IBPTax leva em co' +
          'nsidera'#231#227'o o NCM e o EX da TIPI (IPI) !!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 707
        Height = 17
        Caption = 
          'O valor aproximado dos produtos calculado pelo IBPTax leva em co' +
          'nsidera'#231#227'o o NCM e o EX da TIPI (IPI) !!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 623
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel15: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkValUsu1: TdmkValUsu
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 44
    Top = 8
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 72
    Top = 8
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM unidmed'
      'WHERE Sigla = :P0')
    Left = 100
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla'
      'FROM unidmed'
      'WHERE CodUsu = :P0')
    Left = 128
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object QrMedOrdem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM medordem'
      'ORDER BY Nome')
    Left = 696
    Top = 532
    object QrMedOrdemCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMedOrdemCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMedOrdemNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMedOrdem: TDataSource
    DataSet = QrMedOrdem
    Left = 720
    Top = 532
  end
  object QrMatPartCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM matpartcad'
      'WHERE Parte=2'
      'ORDER BY Nome')
    Left = 692
    Top = 560
    object QrMatPartCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'matpartcad.Codigo'
    end
    object QrMatPartCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'matpartcad.CodUsu'
    end
    object QrMatPartCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'matpartcad.Nome'
      Size = 30
    end
  end
  object DsMatPartCad: TDataSource
    DataSet = QrMatPartCad
    Left = 720
    Top = 560
  end
  object PMNCM: TPopupMenu
    Left = 228
    Top = 80
    object CadastrodeNCM1: TMenuItem
      Caption = 'Cadastro de NCM'
      OnClick = CadastrodeNCM1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object PesquisaemNFes1: TMenuItem
      Caption = 'Pesquisa em NFes'
      OnClick = PesquisaemNFes1Click
    end
  end
end
