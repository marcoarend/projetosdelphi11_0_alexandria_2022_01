unit GraTamCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBEdit, DBCGrids,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmGraTamCad = class(TForm)
    PainelDados: TPanel;
    DsGraTamCad: TDataSource;
    QrGraTamCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrGraTamCadCodigo: TIntegerField;
    QrGraTamCadCodUsu: TIntegerField;
    QrGraTamCadNome: TWideStringField;
    PainelItens: TPanel;
    DBCGGraTamIts: TDBCtrlGrid;
    TbGraTamIts: TmySQLTable;
    DsGraTamIts: TDataSource;
    TbGraTamItsCodigo: TIntegerField;
    TbGraTamItsControle: TIntegerField;
    TbGraTamItsNome: TWideStringField;
    TbGraTamItsLk: TIntegerField;
    TbGraTamItsDataCad: TDateField;
    TbGraTamItsDataAlt: TDateField;
    TbGraTamItsUserCad: TIntegerField;
    TbGraTamItsUserAlt: TIntegerField;
    TbGraTamItsAlterWeb: TSmallintField;
    TbGraTamItsAtivo: TSmallintField;
    DBEdItem: TdmkDBEdit;
    RGPrintTam: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrGraTamCadPrintTam: TSmallintField;
    TbGraTamItsPrintTam: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraTamCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraTamCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrGraTamCadBeforeClose(DataSet: TDataSet);
    procedure QrGraTamCadAfterScroll(DataSet: TDataSet);
    procedure TbGraTamItsBeforePost(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenGraTamIts(Controle: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmGraTamCad: TFmGraTamCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UCreate, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraTamCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraTamCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraTamCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraTamCad.DefParams;
begin
  VAR_GOTOTABELA := 'GraTamCad';
  VAR_GOTOMYSQLTABLE := QrGraTamCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, PrintTam');
  VAR_SQLx.Add('FROM gratamcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraTamCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraTamCad', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmGraTamCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraTamCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraTamCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraTamCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraTamCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraTamCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraTamCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraTamCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraTamCad.TbGraTamItsBeforePost(DataSet: TDataSet);
var
  Controle: Integer;
begin
  if TbGraTamIts.State = dsInsert then
  begin
    TbGraTamItsCodigo.Value := QrGraTamCadCodigo.Value;
    //DModG.BuscaProximoCodigoInt_Novo('gratamits', 'Controle', '', 0, '', Controle);
    Controle := DModG.BuscaProximoInteiro('gratamits', 'Controle', '', 0);
    //
    TbGraTamItsControle.Value := Controle;
    TbGraTamItsPrintTam.Value := QrGraTamCadPrintTam.Value;
  end;
  TbGraTamItsNome.Value := Geral.SemAcento(TbGraTamItsNome.Value);
end;

procedure TFmGraTamCad.BtAlteraClick(Sender: TObject);
begin
  if (QrGraTamCad.State = dsInactive) or (QrGraTamCad.RecordCount = 0) then Exit;  
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraTamCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'gratamcad');
end;

procedure TFmGraTamCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraTamCadCodigo.Value;
  VAR_CAD_ITEM := TBGraTamItsControle.Value;
  Close;
end;

procedure TFmGraTamCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('GraTamCad', 'Codigo', ImgTipo.SQLType,
    QrGraTamCadCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraTamCad, PainelEdit,
    'GraTamCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE gratamits ',
    'SET PrintTam=' + Geral.FF0(RGPrintTam.ItemIndex),
    'WHERE Codigo=' + Geral.FF0(QrGraTamCadCodigo.Value),
    '']);
    //
    LocCod(Codigo, Codigo);
    DBEdItem.SetFocus;
  end;
end;

procedure TFmGraTamCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraTamCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraTamCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraTamCad', 'Codigo');
end;

procedure TFmGraTamCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrGraTamCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'gratamcad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraTamCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
end;

procedure TFmGraTamCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align   := alClient;
  PainelItens.Align  := alClient;
  CriaOForm;
end;

procedure TFmGraTamCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraTamCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraTamCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmGraTamCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraTamCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraTamCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraTamCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraTamCad.QrGraTamCadAfterOpen(DataSet: TDataSet);
begin
  TbGraTamIts.Filter := 'Codigo=' + FormatFloat('0', QrGraTamCadCodigo.Value);
  TbGraTamIts.Filtered := True;
  QueryPrincipalAfterOpen;
end;

procedure TFmGraTamCad.QrGraTamCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraTamIts(0);
end;

procedure TFmGraTamCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraTamCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraTamCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GraTamCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraTamCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraTamCad.QrGraTamCadBeforeClose(DataSet: TDataSet);
begin
  //QrGraTamIts.Close;
  DBCGGraTamIts.AllowInsert := False;
end;

procedure TFmGraTamCad.QrGraTamCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraTamCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGraTamCad.ReopenGraTamIts(Controle: Integer);
begin
  {
  QrGraTamIts.Close;
  QrGraTamIts.Params[0].AsInteger := QrGraTamCadCodigo.Value;
  QrGraTamIts . O p e n ;
  //
  if Controle <> 0 then
    QrGraTamIts.Locate('Controle', Controle, []);
  }
  TbGraTamIts.Filter := 'Codigo=' + FormatFloat('0', QrGraTamCadCodigo.Value);
  DBCGGraTamIts.AllowInsert := QrGraTamCad.RecordCount > 0;
  if (QrGraTamCad.RecordCount > 0) and (TbGraTamIts.State = dsInactive) then
    UnDmkDAC_PF.AbreTable(TbGraTamIts, Dmod.MyDB);
end;

end.

