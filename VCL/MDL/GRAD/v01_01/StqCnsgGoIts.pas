unit StqCnsgGoIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DB,
  mySQLDbTables, dmkRadioGroup, Mask, DBCtrls, dmkEdit, dmkCheckGroup,
  dmkGeral, dmkEditCalc, dmkImage, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox,
  dmkValUsu, UnProjGroup_Consts, UnStqPF;

type
  TFmStqCnsgGoIts = class(TForm)
    Panel1: TPanel;
    PnData: TPanel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    DsGraGruX: TDataSource;
    QrGraGruXHowBxaEstq: TSmallintField;
    QrGraGruXGerBxaEstq: TSmallintField;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrGraGruXNo_SIGLA: TWideStringField;
    QrGraGruXSigla: TWideStringField;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    QrStqCenLocOri: TMySQLQuery;
    DsStqCenLocOri: TDataSource;
    QrStqCenLocOriControle: TIntegerField;
    QrStqCenLocOriNome: TWideStringField;
    QrGraGruXGrandeza: TIntegerField;
    QrStqCenCadOri: TMySQLQuery;
    QrStqCenCadOriCodigo: TIntegerField;
    QrStqCenCadOriCodUsu: TIntegerField;
    QrStqCenCadOriNome: TWideStringField;
    DsStqCenCadOri: TDataSource;
    QrGraGruXFracio: TIntegerField;
    EdCustoBuy: TdmkEdit;
    Label8: TLabel;
    EdCustoFrt: TdmkEdit;
    Label9: TLabel;
    EdValorAll: TdmkEdit;
    Label20: TLabel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    dmkLabel3: TdmkLabel;
    Label7: TLabel;
    EdStqCenCadOri: TdmkEditCB;
    CBStqCenCadOri: TdmkDBLookupComboBox;
    EdStqCenLocOri: TdmkEditCB;
    CBStqCenLocOri: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    dmkLabel1: TdmkLabel;
    Label2: TLabel;
    EdStqCenCadDst: TdmkEditCB;
    CBStqCenCadDst: TdmkDBLookupComboBox;
    EdStqCenLocDst: TdmkEditCB;
    CBStqCenLocDst: TdmkDBLookupComboBox;
    QrStqCenLocDst: TMySQLQuery;
    QrStqCenLocDstControle: TIntegerField;
    QrStqCenLocDstNome: TWideStringField;
    DsStqCenLocDst: TDataSource;
    DsStqCenCadDst: TDataSource;
    QrStqCenCadDst: TMySQLQuery;
    QrStqCenCadDstCodigo: TIntegerField;
    QrStqCenCadDstCodUsu: TIntegerField;
    QrStqCenCadDstNome: TWideStringField;
    QrEstoque: TMySQLQuery;
    QrEstoqueQtde: TFloatField;
    DsEstoque: TDataSource;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    DBEdit6: TDBEdit;
    Label4: TLabel;
    DBEdit7: TDBEdit;
    QrEstoqueCustoAll: TFloatField;
    SbPreco: TSpeedButton;
    Panel5: TPanel;
    PnGGX: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    Panel2: TPanel;
    Label11: TLabel;
    SpeedButton1: TSpeedButton;
    EdGraGruX: TdmkEdit;
    QrPreco: TMySQLQuery;
    QrPrecoPreco: TFloatField;
    Label5: TLabel;
    DBEdit8: TDBEdit;
    DsPreco: TDataSource;
    QrEstoqueCusMed: TFloatField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdStqCenCadOriChange(Sender: TObject);
    procedure EdCustoBuyRedefinido(Sender: TObject);
    procedure EdCustoFrtRedefinido(Sender: TObject);
    procedure EdStqCenCadDstChange(Sender: TObject);
    procedure EdStqCenLocOriRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure SbPrecoClick(Sender: TObject);
    procedure EdGraGruXExit(Sender: TObject);
    procedure DBEdit5Change(Sender: TObject);
  private
    { Private declarations }
    //procedure AbrePesquisa(GraGruX: Integer);
    //procedure VerificaPainelGGX();
    //procedure MostraGraGruPesq1(Nivel1: Integer);
    procedure MostraEdicao(Tipo: TSQLType);
    //procedure CalculaValorAll();
    //procedure ReopenEstoque();
    //procedure DefinePreco();
    //procedure DefineCustoRem();


  public
    { Public declarations }
    FTabePrcCab: Integer;
  end;

  var
  FmStqCnsgGoIts: TFmStqCnsgGoIts;

implementation

uses UnMyObjects, StqCnsgGoCab, UMySQLModule, Module, GraGruPesq1, MyDBCheck,
  ModuleGeral, DmkDAC_PF, ModProd, MyListas, GetValor;

{$R *.DFM}

procedure TFmStqCnsgGoIts.BtOKClick(Sender: TObject);
const
  OriCnta = 0;
  FatorClas = 1;
  OriPart = 0;
var
  DataHora, Texto: String;
  GerBxaEstq,
  t, OriCodi, Empresa, TwnCtrl, GraGruX: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoBuy, CustoFrt, CustoAll, ValorAll,
  CusUni: Double;
  //
  function InsereItem(Tipo, OriCtrl, TwnBrth, Baixa, StqCenCad, StqCenLoc:
  Integer): Boolean;
  begin
    Result := False;
    //
    //OBS.: Ativo 0 para atualizar o estoque somente no encerramento
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'Empresa',
      'StqCenCad', 'StqCenLoc',
      'GraGruX', 'Qtde',
      'OriPart', 'Pecas', 'Peso',
      'AreaM2', 'AreaP2', 'FatorClas',
      'CustoBuy', 'CustoFrt',
      'CustoAll', 'ValorAll', 'Ativo',
      'TwnCtrl', 'TwnBrth', 'Baixa'
      {, 'QuemUsou', 'Retorno', 'ParTipo',
      'ParCodi', 'DebCtrl', 'SMIMultIns'}], [
      'IDCtrl'], [
      DataHora, Tipo, OriCodi,
      OriCtrl, OriCnta, Empresa,
      StqCenCad, StqCenLoc,
      GraGruX, Qtde,
      OriPart, Pecas, Peso,
      AreaM2, AreaP2, FatorClas,
      CustoBuy, CustoFrt,
      CustoAll, ValorAll, 0,
      TwnCtrl, TwnBrth, Baixa
      {, QuemUsou, Retorno, ParTipo,
      ParCodi, DebCtrl, SMIMultIns}], [
      OriCtrl], False) then
    begin
      Result := True;
  { N�o precisa?
  ? := UMyMod.BuscaEmLivreY_Def('stqmovvala', ''ID', ImgTipo.SQLType, CodAtual);
  if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovvala', auto_increment?[
  'IDCtrl', 'Tipo', 'OriCodi',
  'OriCtrl', 'OriCnta', 'SeqInReduz',
  'Empresa', 'StqCenCad', 'GraGruX',
  'Qtde', 'Preco', 'Total',
  'CFOP', 'InfAdCuztm', 'PercCustom',
  'MedidaC', 'MedidaL', 'MedidaA',
  'MedidaE', 'MedOrdem', 'TipoNF',
  'refNFe', 'modNF', 'Serie',
  'nNF', 'SitDevolu', 'Servico',
  'RefProd', 'CFOP_Contrib', 'CFOP_MesmaUF',
  'CFOP_Proprio'], [
  'ID'], [
  IDCtrl, Tipo, OriCodi,
  OriCtrl, OriCnta, SeqInReduz,
  Empresa, StqCenCad, GraGruX,
  Qtde, Preco, Total,
  CFOP, InfAdCuztm, PercCustom,
  MedidaC, MedidaL, MedidaA,
  MedidaE, MedOrdem, TipoNF,
  refNFe, modNF, Serie,
  nNF, SitDevolu, Servico,
  RefProd, CFOP_Contrib, CFOP_MesmaUF,
  CFOP_Proprio], [
  ID], UserDataAlterweb?, IGNORE?
  }
    //
    end;
  end;
var
  IDCtrl1, IDCtrl2, StqCenCadOri, StqCenLocOri, StqCenCadDst, StqCenLocDst: Integer;
begin
  //
  DataHora  := DModG.ObtemAgoraTxt();
  OriCodi   := FmStqCnsgGoCab.QrStqCnsgGoCabCodigo.Value;
  Empresa   := FmStqCnsgGoCab.QrStqCnsgGoCabEmpresa.Value;
  StqCenCadOri := EdStqCenCadOri.ValueVariant;
  StqCenLocOri := EdStqCenLocOri.ValueVariant;
  StqCenCadDst := EdStqCenCadDst.ValueVariant;
  StqCenLocDst := EdStqCenLocDst.ValueVariant;
  //
  if MyObjects.FIC(StqCenCadOri = 0, EdStqCenCadOri, 'Defina o centro de estoque de origem!') then Exit;
  if MyObjects.FIC(StqCenCadDst = 0, EdStqCenCadDst, 'Defina o centro de estoque de destino!') then Exit;
  //
  if MyObjects.FIC(StqCenLocOri = 0, EdStqCenLocOri, 'Defina o local do centro de estoque de origem!') then Exit;
  if MyObjects.FIC(StqCenLocDst = 0, EdStqCenLocDst, 'Defina o local do centro de estoque de destino!') then Exit;
  //
  GraGruX   := EdGraGruX.ValueVariant;
  Pecas     := 0;// EdPecas.ValueVariant;
  Peso      := 0;// EdPLE.ValueVariant;
  AreaM2    := 0;// EdAreaM2.ValueVariant;
  AreaP2    := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  Qtde := EdQtde.ValueVariant;

  if QrEstoqueQtde.Value > 0 then
    CusUni := QrEstoqueCustoAll.Value / QrEstoqueQtde.Value
  else
    CusUni := 0.00;
  CustoAll := CusUni * Qtde;
  CustoBuy  := 0.00;
  CustoFrt  := 0.00;
  ValorAll  := 0.00;
  //
  if MyObjects.FIC(Qtde <= 0, nil, 'Quantidade n�o definida!') then Exit;
  if MyObjects.FIC(Qtde > QrEstoqueQtde.Value, nil, 'Estoque insuficiente!') then Exit;
  if MyObjects.FIC(CustoAll <= 0, nil, 'Custo total n�o definido!') then Exit;
  //
  Texto := '';
  t := QrGraGruXHowBxaEstq.Value;
  if GraGruX = 0 then
    Texto := Texto + sLineBreak + '-> Falta informar o reduzido.';
  if (Geral.IntInConjunto(1, t) and (Pecas = 0)) then
    Texto := Texto + sLineBreak + '-> Falta informar a quantidade de pe�as.';
  if (Geral.IntInConjunto(2, t) and (AreaM2 = 0)) then
    Texto := Texto + sLineBreak + '-> Falta informar a �rea.';
  if (Geral.IntInConjunto(4, t) and (Peso = 0)) then
    Texto := Texto + sLineBreak + '-> Falta informar o peso.';
  //
  if Texto <> '' then
  begin
    Texto := 'Neste item falta:' + Texto;
    Geral.MensagemBox(Texto, 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  IDCtrl1 :=  UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  IDCtrl2 :=  UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  TwnCtrl :=  UMyMod.BPGS1I32('stqmovitsa', 'TwnCtrl', '', '', tsPos, ImgTipo.SQLType, 0);
  if InsereItem(FmStqCnsgGoCab.FThisFatID_Rem_Bxa, IDCtrl1, IDCtrl2, (*Baixa*) -1, StqCenCadOri, StqCenLocOri) then
  begin
    CustoBuy  := EdCustoBuy.ValueVariant;
    CustoFrt  := EdCustoFrt.ValueVariant;
    ValorAll  := EdValorAll.ValueVariant;
    if InsereItem(FmStqCnsgGoCab.FThisFatID_Rem_Inn, IDCtrl2, IDCtrl1, (*Baixa*) 1, StqCenCadDst, StqCenLocDst) then
    begin
      if not CkContinuar.Checked then
      begin
        FmStqCnsgGoCab.LocCod(
          FmStqCnsgGoCab.QrStqCnsgGoCabCodigo.Value, FmStqCnsgGoCab.QrStqCnsgGoCabCodigo.Value);
        FmStqCnsgGoCab.ReopenStqMovIts(IDCtrl2);
        Close;
      end else
      begin
        FmStqCnsgGoCab.ReopenStqMovIts(IDCtrl2);
        MostraEdicao(stIns);
      end;
    end;
  end;
end;

procedure TFmStqCnsgGoIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

(*
procedure TFmStqCnsgGoIts.CalculaValorAll();
var
  ValorAll, CustoBuy, CustoFrt: Double;
begin
  CustoBuy := EdCustoBuy.ValueVariant;
  CustoFrt := EdCustoFrt.ValueVariant;
  //
  ValorAll := CustoBuy + CustoFrt;
  EdValorAll.ValueVariant := ValorAll;
end;
*)

procedure TFmStqCnsgGoIts.DBEdit5Change(Sender: TObject);
begin
  LaQtde.Caption := DBEdit5.Text + ':';
end;

(*
procedure TFmStqCnsgGoIts.DefineCustoRem();
var
  Preco, Qtde, CustoBuy: Double;
begin
  if QrPreco.State <> dsInactive then
    Preco := QrPrecoPreco.Value
  else
    Preco := 0.00;
  Qtde := EdQtde.ValueVariant;
  CustoBuy := Qtde * Preco;
  EdCustoBuy.ValueVariant := CustoBuy;
end;
*)

(*
procedure TFmStqCnsgGoIts.DefinePreco();
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPreco, Dmod.MyDB, [
    //'SELECT ggx.Controle, gg1.Nivel1, tpc.Preco TPC_Preco, tpi.Preco TPI_Preco ',
    'SELECT IF(tpi.Preco > 0, tpi.Preco, tpc.Preco) Preco ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN tabeprcgrg  tpc ON tpc.Nivel1=ggx.GraGru1 ',
    '          AND tpc.Codigo=' + Geral.FF0(FTabePrcCab),
    'LEFT JOIN tabeprcgri  tpi ON tpi.GraGruX=ggx.Controle ',
    '          AND tpi.TabePrcCab=' + Geral.FF0(FTabePrcCab),
    'WHERE ggx.Controle=' + Geral.FF0(GragruX),
    '']);
  end else
    QrPreco.Close;
end;
*)

procedure TFmStqCnsgGoIts.EdCustoBuyRedefinido(Sender: TObject);
begin
  StqPF.CalculaValorAll(EdCustoBuy, EdCustoFrt, EdValorAll);
end;

procedure TFmStqCnsgGoIts.EdCustoFrtRedefinido(Sender: TObject);
begin
  StqPF.CalculaValorAll(EdCustoBuy, EdCustoFrt, EdValorAll);
end;

procedure TFmStqCnsgGoIts.EdGraGruXChange(Sender: TObject);
begin
  StqPF.VerificaPainelGGX(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;

procedure TFmStqCnsgGoIts.EdGraGruXExit(Sender: TObject);
begin
  if PnData.Visible then
    EdStqCenCadOri.SetFocus
end;

procedure TFmStqCnsgGoIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    StqPF.AbrePesquisa(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;

procedure TFmStqCnsgGoIts.EdGraGruXRedefinido(Sender: TObject);
begin
  StqPF.ReopenEstoque(QrEstoque, EdGraGruX, EdStqCenLocOri);
  StqPF.DefinePreco(QrPreco, EdGraGruX.ValueVariant, FTabePrcCab);
end;

procedure TFmStqCnsgGoIts.EdQtdeRedefinido(Sender: TObject);
begin
  StqPF.DefineCustoRem(QrPreco, EdQtde, EdCustoBuy);
end;

procedure TFmStqCnsgGoIts.SbPrecoClick(Sender: TObject);
  function ObtemPrecoManual(var Preco: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Preco        := 0;
    Result        := False;
    CasasDecimais := 2;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'Pre�o', 'Informe o pre�o: ',
    0, ResVar) then
    begin
      Preco := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  Preco, Qtde, CustoBuy: Double;
begin
  if QrPreco.State <> dsInactive then
    Preco := QrPrecoPreco.Value
  else
    Preco := 0.00;
  if ObtemPrecoManual(Preco) then
  begin
    Qtde := EdQtde.ValueVariant;
    CustoBuy := Qtde * Preco;
    EdCustoBuy.ValueVariant := CustoBuy;
  end;
end;

procedure TFmStqCnsgGoIts.SpeedButton1Click(Sender: TObject);
begin
  StqPF.AbrePesquisa(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;

(*
procedure TFmStqCnsgGoIts.AbrePesquisa(GraGruX: Integer);
var
  Nivel1: Integer;
begin
  if GraGruX <> 0 then
    Nivel1 := DmProd.ObtemGraGru1deGraGruX(GraGruX)
  else
    Nivel1 := 0;
  //
  StqPF.MostraGraGruPesq1(Nivel1, EdGraGruX);
  //StqPF.ReopenGraGruX(QrGraGruX);
  StqPF.VerificaPainelGGX(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;
*)

procedure TFmStqCnsgGoIts.EdStqCenCadDstChange(Sender: TObject);
var
  StqCenCad: Integer;
begin
  StqCenCad := EdStqCenCadDst.ValueVariant;
  //
  if StqCenCad <> 0 then
    StqPF.ReopenStqCenLoc(StqCenCad, QrStqCenLocDst, EdStqCenLocDst, CBStqCenLocDst);
end;

procedure TFmStqCnsgGoIts.EdStqCenCadOriChange(Sender: TObject);
var
  StqCenCad: Integer;
begin
  StqCenCad := EdStqCenCadOri.ValueVariant;
  //
  if StqCenCad <> 0 then
    StqPF.ReopenStqCenLoc(StqCenCad, QrStqCenLocOri, EdStqCenLocOri, CBStqCenLocOri);
end;

procedure TFmStqCnsgGoIts.EdStqCenLocOriRedefinido(Sender: TObject);
begin
  StqPF.ReopenEstoque(QrEstoque, EdGraGruX, EdStqCenLocOri);
end;

procedure TFmStqCnsgGoIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqCnsgGoIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  StqPF.ReopenStqCenCad(QrStqCenCadOri,
    FmStqCnsgGoCab.QrStqCnsgGoCabEmpresa.Value, EdStqCenCadOri, CBStqCenCadOri, cmprIgual);
  StqPF.ReopenStqCenCad(QrStqCenCadDst,
    FmStqCnsgGoCab.QrStqCnsgGoCabFornece.Value, EdStqCenCadDst, CBStqCenCadDst, cmprIgual);
  //
  StqPF.ReopenGraGruX(QrGraGruX);
  //
  CkContinuar.Checked := ImgTipo.SQLType = stIns;
end;

procedure TFmStqCnsgGoIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCnsgGoIts.MostraEdicao(Tipo: TSQLType);
begin
  case Tipo of
    stIns:
    begin
      EdGraGruX.ValueVariant  := '';
      EdValorAll.ValueVariant := 0;
      //
      EdQtde.ValueVariant     := 0;
      EdCustoBuy.ValueVariant := 0;
      EdCustoFrt.ValueVariant := 0;
      //
      StqPF.VerificaPainelGGX(QrGraGruX, EdGraGruX, PnGGX, PnData);
    end;
  end;
  EdGraGruX.SetFocus;
end;

(*
procedure TFmStqCnsgGoIts.MostraGraGruPesq1(Nivel1: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FmGraGruPesq1.QrGraGru1, Dmod.MyDB, [
      'SELECT gg1.CodUsu, gg1.Nivel1, gg1.UsaSubsTrib, ',
      'gg1.GraTamCad, gg1.Nome, gg1.PrdGrupTip, pgt.Fracio ',
      'FROM gragru1 gg1',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
      'ORDER BY gg1.Nome',
      '']);
    //
    FmGraGruPesq1.VUGraGru1.ValueVariant := Nivel1;
    //
    FmGraGruPesq1.ShowModal;
    //
    EdGraGruX.Text := FmGraGruPesq1.FGraGruX;
    //FFracio
    //
    FmGraGruPesq1.Destroy;
  end;
end;
*)

(*
procedure TFmStqCnsgGoIts.ReopenEstoque();
var
  GraGruX, StqCenLoc: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  StqCenLoc := EdStqCenLocOri.ValueVariant;
  //
  StqPF.ReopenEstoque_GraGruX_StqCenLoc(QrEstoque, GraGruX, StqCenLoc);
end;
*)

(*
procedure TFmStqCnsgGoIts.ReopenStqCenCadDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqCenCadDst, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome',
  'FROM stqcencad',
  'WHERE EntiSitio=' + Geral.FF0(FmStqCnsgGoCab.QrStqCnsgGoCabFornece.Value),
  'ORDER BY Nome',
  '']);
  //
  if QrStqCenCadDst.RecordCount = 1 then
  begin
    EdStqCenCadDst.ValueVariant := QrStqCenCadDstCodigo.Value;
    CBStqCenCadDst.KeyValue     := QrStqCenCadDstCodigo.Value;
  end;
end;

procedure TFmStqCnsgGoIts.ReopenStqCenCadOri();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqCenCadOri, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome',
  'FROM stqcencad',
  'WHERE EntiSitio=' + Geral.FF0(FmStqCnsgGoCab.QrStqCnsgGoCabEmpresa.Value),
  'ORDER BY Nome',
  '']);
  //
  if QrStqCenCadOri.RecordCount = 1 then
  begin
    EdStqCenCadOri.ValueVariant := QrStqCenCadOriCodigo.Value;
    CBStqCenCadOri.KeyValue     := QrStqCenCadOriCodigo.Value;
  end;
end;
*)

(*
procedure TFmStqCnsgGoIts.ReopenStqCenLoc(StqCenCad: Integer; Qry: TmySQLQuery;
EdStqCenLoc: TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox);
begin
  UnDmkDac_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM stqcenloc ',
  'WHERE Codigo=' + Geral.FF0(StqCenCad),
  'ORDER BY Nome ',
  '']);
  if Qry.RecordCount = 1 then
  begin
    EdStqCenLoc.ValueVariant := Qry.FieldByName('Controle').Value;
    CBStqCenLoc.KeyValue     := Qry.FieldByName('Controle').Value;
  end else
  begin
    EdStqCenLoc.ValueVariant := 0;
    CBStqCenLoc.KeyValue     := 0;
  end;
end;
*)

(*
procedure TFmStqCnsgGoIts.VerificaPainelGGX();
var
  GraGruX: Integer;
begin
  if QrGraGruX.State <> dsInactive then
  begin
    GraGruX       := Geral.IMV(EdGraGruX.Text);
    PnGGX.Visible := QrGraGruX.Locate('GraGruX', GraGruX, []);
    PnData.Visible := PnGGX.Visible;
  end;
end;
*)

end.
