unit GraGruValDta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkEdit, dmkImage, UnDmkEnums,
  Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmGraGruValDta = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdCustoPreco: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    TPDataIni: TdmkEditDateTimePicker;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FControle, FGraGruX, FGraGru1, FGraCusPrc, FEntidade: Integer;
  end;

  var
  FmGraGruValDta: TFmGraGruValDta;

implementation

uses UnMyObjects, UMySQLModule, Module, GraCusPrc;

{$R *.DFM}

procedure TFmGraGruValDta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruValDta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruValDta.BtOKClick(Sender: TObject);
var
  DataIni: String;
  Controle, GraGruX, GraGru1, GraCusPrc, Entidade: Integer;
  CustoPreco: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Controle       := FControle;
  GraGruX        := FGraGruX;
  GraGru1        := FGraGru1;
  GraCusPrc      := FGraCusPrc;
  CustoPreco     := EdCustoPreco.ValueVariant;
  Entidade       := FEntidade;
  DataIni        := Geral.FDT(TPDataIni.Date, 1);
  //
  Controle  := UMyMod.BuscaEmLivreY_Def('GraGruVal', 'Controle', SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
  'GraGruX', 'GraGru1', 'GraCusPrc',
  'CustoPreco', 'Entidade', 'DataIni'], [
  'Controle'], [
  GraGruX, GraGru1, GraCusPrc,
  CustoPreco, Entidade, DataIni], [
  Controle], True) then
  begin
    FmGraCusPrc.ReopenGraGruVal(Controle);
    Close;
  end;
end;

procedure TFmGraGruValDta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPDataIni.Date := Date;
  //
end;

procedure TFmGraGruValDta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
