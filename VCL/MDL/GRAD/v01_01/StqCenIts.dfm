object FmStqCenIts: TFmStqCenIts
  Left = 339
  Top = 185
  Caption = 'STQ-MANUA-005 :: Item de Movimenta'#231#227'o Entre Centros'
  ClientHeight = 437
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 292
    Width = 617
    Height = 31
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 12
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 617
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 533
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 617
    Height = 180
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label4: TLabel
      Left = 96
      Top = 16
      Width = 89
      Height = 13
      Caption = 'Produto (reduzido):'
    end
    object Label1: TLabel
      Left = 96
      Top = 56
      Width = 83
      Height = 13
      Caption = 'Centro de origem:'
    end
    object Label2: TLabel
      Left = 96
      Top = 96
      Width = 86
      Height = 13
      Caption = 'Centro de destino:'
    end
    object Label7: TLabel
      Left = 292
      Top = 136
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label8: TLabel
      Left = 12
      Top = 56
      Width = 14
      Height = 13
      Caption = 'ID:'
      Enabled = False
    end
    object Label9: TLabel
      Left = 12
      Top = 96
      Width = 14
      Height = 13
      Caption = 'ID:'
      Enabled = False
    end
    object Label6: TLabel
      Left = 396
      Top = 136
      Width = 31
      Height = 13
      Caption = 'Pre'#231'o:'
    end
    object Label10: TLabel
      Left = 504
      Top = 136
      Width = 30
      Height = 13
      Caption = 'Custo:'
    end
    object Label11: TLabel
      Left = 12
      Top = 136
      Width = 143
      Height = 13
      Caption = 'Lista de custos da regra fiscal:'
      Enabled = False
      FocusControl = DBEdit1
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 453
      Height = 21
      KeyField = 'GraGruX'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGraGruX
      TabOrder = 1
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7CodiFldName = 'Nivel1'
      LocF7NameFldName = 'Nome'
      LocF7TableName = 'gragru1'
      LocF7SQLMasc = '$#'
    end
    object EdGraGruX: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'Embalagem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      OnEnter = EdGraGruXEnter
      OnExit = EdGraGruXExit
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdCencadSorc: TdmkEditCB
      Left = 96
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'Embalagem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdCencadSorcChange
      OnEnter = EdCencadSorcEnter
      OnExit = EdCencadSorcExit
      DBLookupComboBox = CBCencadSorc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCencadSorc: TdmkDBLookupComboBox
      Left = 152
      Top = 72
      Width = 453
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsStqCenCadSorc
      TabOrder = 3
      dmkEditCB = EdCencadSorc
      UpdType = utYes
      LocF7CodiFldName = 'Nivel1'
      LocF7NameFldName = 'Nome'
      LocF7TableName = 'gragru1'
      LocF7SQLMasc = '$#'
    end
    object EdCencadDest: TdmkEditCB
      Left = 96
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'Embalagem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCencadDest
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCencadDest: TdmkDBLookupComboBox
      Left = 152
      Top = 112
      Width = 453
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsStqCenCadDest
      TabOrder = 5
      dmkEditCB = EdCencadDest
      UpdType = utYes
      LocF7CodiFldName = 'Nivel1'
      LocF7NameFldName = 'Nome'
      LocF7TableName = 'gragru1'
      LocF7SQLMasc = '$#'
    end
    object EdQtde: TdmkEdit
      Left = 292
      Top = 152
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdQtdeChange
    end
    object EdIDCtrlSorc: TdmkEdit
      Left = 12
      Top = 72
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdIDCtrlDest: TdmkEdit
      Left = 12
      Top = 112
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdPreco: TdmkEdit
      Left = 396
      Top = 152
      Width = 105
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdPrecoChange
    end
    object EdCustoAll: TdmkEdit
      Left = 504
      Top = 152
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdCustoAllChange
    end
    object DBEdit1: TDBEdit
      Left = 12
      Top = 152
      Width = 40
      Height = 21
      DataField = 'GraCusPrc'
      DataSource = DsFRC
      Enabled = False
      TabOrder = 11
    end
    object DBEdit2: TDBEdit
      Left = 56
      Top = 152
      Width = 230
      Height = 21
      DataField = 'NO_GCP'
      DataSource = DsFRC
      Enabled = False
      TabOrder = 12
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 452
        Height = 32
        Caption = 'Item de Movimenta'#231#227'o Entre Centros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 452
        Height = 32
        Caption = 'Item de Movimenta'#231#227'o Entre Centros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 452
        Height = 32
        Caption = 'Item de Movimenta'#231#227'o Entre Centros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 323
    Width = 617
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 513
        Height = 16
        Caption = 
          'O pre'#231'o de custo '#233' oriundo da tabela conforme definido na movime' +
          'nta'#231#227'o da regra fiscal.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 513
        Height = 16
        Caption = 
          'O pre'#231'o de custo '#233' oriundo da tabela conforme definido na movime' +
          'nta'#231#227'o da regra fiscal.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 367
    Width = 617
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 471
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 469
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle GraGruX, ggv.Controle CtrlGGV, '
      'ggv.CustoPreco, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, pgt.Fracio '
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON gg1.PrdGrupTip=pgt.Codigo'
      'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle'
      '  AND ggv.GraCusPrc=4'
      '  AND ggv.Entidade=-11'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 196
    Top = 36
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXCtrlGGV: TIntegerField
      FieldName = 'CtrlGGV'
    end
    object QrGraGruXCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 196
    Top = 84
  end
  object QrStqCenCadSorc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 280
    Top = 36
    object QrStqCenCadSorcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadSorcNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadSorc: TDataSource
    DataSet = QrStqCenCadSorc
    Left = 280
    Top = 84
  end
  object QrStqCenCadDest: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 380
    Top = 36
    object QrStqCenCadDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadDestNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadDest: TDataSource
    DataSet = QrStqCenCadDest
    Left = 380
    Top = 84
  end
  object QrFRC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.GraCusPrc'
      'FROM fisregmvt frm'
      'LEFT JOIN fisregcad frc ON frc.Codigo=frm.Codigo'
      'WHERE frc.Codigo=3')
    Left = 404
    Top = 140
    object QrFRCGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrFRCNO_GCP: TWideStringField
      FieldName = 'NO_GCP'
      Size = 30
    end
  end
  object DsFRC: TDataSource
    DataSet = QrFRC
    Left = 404
    Top = 188
  end
end
