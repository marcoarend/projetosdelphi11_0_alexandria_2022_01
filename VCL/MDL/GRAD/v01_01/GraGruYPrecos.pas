unit GraGruYPrecos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBLookupComboBox, dmkEditCB;

type
  TFmGraGruYPrecos = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrGraGruY: TMySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYNome: TWideStringField;
    DsGraGruY: TDataSource;
    Panel3: TPanel;
    EdGraGruY: TdmkEditCB;
    CBGraGruY: TdmkDBLookupComboBox;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    TbGraGruXCou: TMySQLTable;
    TbGraGruXCouBaseValVenda: TFloatField;
    TbGraGruXCouBaseCliente: TWideStringField;
    TbGraGruXCouBaseImpostos: TFloatField;
    TbGraGruXCouBasePerComis: TFloatField;
    TbGraGruXCouBasFrteVendM2: TFloatField;
    TbGraGruXCouBaseValLiq: TFloatField;
    TbGraGruXCouGraGruX: TIntegerField;
    TbGraGruXCouNO_PRD_TAM_COR: TWideStringField;
    TbGraGruXCouGraGruY: TIntegerField;
    QrGraGruXGraGruY: TIntegerField;
    DsGraGruXCou: TDataSource;
    QrGGXs: TMySQLQuery;
    QrGGXsControle: TIntegerField;
    TbGraGruXCouBaseValCusto: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGruYRedefinido(Sender: TObject);
    procedure TbGraGruXCouBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmGraGruYPrecos: TFmGraGruYPrecos;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnVS_PF;

{$R *.DFM}

procedure TFmGraGruYPrecos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruYPrecos.EdGraGruYRedefinido(Sender: TObject);
  procedure MostraTudo();
  begin
    TbGraGruXCou.Filter := '';
    TbGraGruXCou.Filtered := False;
  end;
var
  GraGruY: Integer;
  Corda: String;
begin
  TbGraGruXCou.Active := False;
  GraGruY := EdGraGruY.ValueVariant;
  if GraGruY <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGGXs, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragrux ',
    'WHERE GraGruY=' + Geral.FF0(GraGruY),
    '']);
    Corda := MyObjects.CordaDeQuery(QrGGXs, 'Controle', '-999999999');
    try
      TbGraGruXCou.Filter := 'GraGruX IN (' + Corda + ')';
      TbGraGruXCou.Filtered := True;
    except
      MostraTudo();
    end;
  end else
    MostraTudo();
  TbGraGruXCou.Active := True;
end;

procedure TFmGraGruYPrecos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruYPrecos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
end;

procedure TFmGraGruYPrecos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruYPrecos.FormShow(Sender: TObject);
begin
  TbGraGruXCou.Active := True;
end;

procedure TFmGraGruYPrecos.TbGraGruXCouBeforePost(DataSet: TDataSet);
var
  FatrClase, BaseValVenda, BaseImpostos, BasePerComis, BasFrteVendM2,
  BaseValLiq: Double;
begin
  BaseValVenda   := TbGraGruXCouBaseValVenda.Value;
  BaseImpostos   := TbGraGruXCouBaseImpostos.Value;
  BasePerComis   := TbGraGruXCouBasePerComis.Value;
  BasFrteVendM2  := TbGraGruXCouBasFrteVendM2.Value;
  //
  BaseValLiq := VS_PF.CalculaBaseValLiq(BaseValVenda, BaseImpostos, BasePerComis, BasFrteVendM2);
  //
  TbGraGruXCouBaseValLiq.Value := BaseValLiq;
end;

end.
