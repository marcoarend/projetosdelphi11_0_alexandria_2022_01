unit MatFichCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables, dmkDBGrid,
  ComCtrls, dmkEdit, Variants, Menus, dmkImage, UnDmkEnums;

type
  TFmMatFichCab = class(TForm)
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    DsGraGru1: TDataSource;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrGraGru1GraTamCad: TIntegerField;
    Panel5: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    Panel6: TPanel;
    LaNivel1: TLabel;
    EdFiltroNiv1: TEdit;
    dmkDBGrid1: TdmkDBGrid;
    QrGraMatIts: TmySQLQuery;
    Panel7: TPanel;
    DsGraMatIts: TDataSource;
    GroupBox5: TGroupBox;
    EdSelecao: TdmkEdit;
    EdSelCodi: TdmkEdit;
    EdSelNome: TdmkEdit;
    GradeItens: TdmkDBGrid;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GradeC: TStringGrid;
    TabSheet2: TTabSheet;
    GradeA: TStringGrid;
    TabSheet3: TTabSheet;
    GradeX: TStringGrid;
    PMAltera: TPopupMenu;
    Alteraparte1: TMenuItem;
    AlteraAbrangncia1: TMenuItem;
    QrGraMatItsNO_Parte: TWideStringField;
    QrGraMatItsNO_MatPartCad: TWideStringField;
    QrGraMatItsAbrange_TXT: TWideStringField;
    QrGraMatItsCU1_PrdGrupTip: TIntegerField;
    QrGraMatItsNO1_PrdGrupTip: TWideStringField;
    QrGraMatItsCU1_GraGru1: TIntegerField;
    QrGraMatItsNO1_GraGru1: TWideStringField;
    QrGraMatItsGraCorCad: TIntegerField;
    QrGraMatItsCU1_COR: TIntegerField;
    QrGraMatItsNO1_COR: TWideStringField;
    QrGraMatItsNO1_TAM: TWideStringField;
    QrGraMatItsCU2_PrdGrupTip: TIntegerField;
    QrGraMatItsNO2_PrdGrupTip: TWideStringField;
    QrGraMatItsCU2_GraGru1: TIntegerField;
    QrGraMatItsNO2_GraGru1: TWideStringField;
    QrGraMatItsGraCorCad_1: TIntegerField;
    QrGraMatItsCU2_COR: TIntegerField;
    QrGraMatItsNO2_COR: TWideStringField;
    QrGraMatItsNO2_TAM: TWideStringField;
    QrGraMatItsControle: TIntegerField;
    QrGraMatItsNivAbrange: TSmallintField;
    QrGraMatItsAbrangePgt: TIntegerField;
    QrGraMatItsAbrangeGg1: TIntegerField;
    QrGraMatItsAbrangeGgc: TIntegerField;
    QrGraMatItsAbrangeGti: TIntegerField;
    QrGraMatItsAbrangeGgx: TIntegerField;
    QrGraMatItsNivPermite: TSmallintField;
    QrGraMatItsPermitePgt: TIntegerField;
    QrGraMatItsPermiteGg1: TIntegerField;
    QrGraMatItsPermiteGgc: TIntegerField;
    QrGraMatItsPermiteGti: TIntegerField;
    QrGraMatItsPermiteGgx: TIntegerField;
    QrGraMatItsMatPartCad: TIntegerField;
    QrGraMatItsABRANGENCIA: TWideStringField;
    QrGraMatItsPERMISSOES: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdFiltroNiv1Exit(Sender: TObject);
    procedure QrPrdGrupTipAfterScroll(DataSet: TDataSet);
    procedure QrPrdGrupTipBeforeClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QrGraGru1AfterScroll(DataSet: TDataSet);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGraGru1BeforeClose(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure GradeCClick(Sender: TObject);
    procedure CkSoAbrangenciaClick(Sender: TObject);
    procedure QrGraGru1BeforeScroll(DataSet: TDataSet);
    procedure QrGraMatItsCalcFields(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrGraMatItsBeforeClose(DataSet: TDataSet);
    procedure QrGraMatItsAfterOpen(DataSet: TDataSet);
    procedure Alteraparte1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure dmkDBGrid1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenGraGru1(Nivel1: Integer);
    procedure LimpaPesquisa();
    procedure DefineAbrange(NivAbrange: Integer);
  public
    { Public declarations }
    FNivAbrange,
    FAbrangePgt,
    FAbrangeGg1,
    FAbrangeGgc,
    FAbrangeGti,
    FAbrangeGgx: Integer;
    procedure ReopenGraMatIts(Controle: Integer);

  end;

  var
  FmMatFichCab: TFmMatFichCab;

implementation

uses UnMyObjects, Module, ModProd, MatFichIts, MyDBCheck, SelCod, UnInternalConsts,
  GraGruC, UMYSQLModule, dmkGeral;

{$R *.DFM}

procedure TFmMatFichCab.Alteraparte1Click(Sender: TObject);
  procedure ExecutaSQL(Parte: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE gramatits SET MatPartCad=:P0 ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[00].AsInteger := Parte;
    Dmod.QrUpd.Params[01].AsInteger := QrGraMatItsControle.Value;
    Dmod.QrUpd.ExecSQL;
  end;
const
  Aviso  = '...';
  Titulo = 'Ficha de Materiais';
  Prompt = 'Informe o Material:';
  Campo  = 'Descricao';
var
  Codigo, Controle, N: Integer;
begin
  Controle := QrGraMatItsControle.Value;
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM matpartcad ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if Codigo <> 0 then
  begin
    Screen.Cursor := crHourGlass;
    with GradeItens.DataSource.DataSet do
    for n := 0 to GradeItens.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(GradeItens.SelectedRows.Items[n]));
      ExecutaSQL(Codigo);
    end;
    Screen.Cursor := crDefault;
    ReopenGraMatIts(Controle);
  end;
end;

procedure TFmMatFichCab.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmMatFichCab.BtExcluiClick(Sender: TObject);
{
  procedure ExcluiItemAtual();
  begin
    // parei aqui

  end;
}
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrGraMatIts,  TDBGrid(GradeItens),
   'gramatits', ['Controle'], ['Controle'], istPergunta, '');
{
  if Application.MessageBox('Confirma a retirada do iten(s) selecionado(s)?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if dmkDBGrid2.SelectedRows.Count < 2 then
      ExcluiItemAtual()
    else
    begin
      with dmkDBGrid2.DataSource.DataSet do
      for n := 0 to dmkDBGrid2.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(dmkDBGrid2.SelectedRows.Items[n]));
        ExcluiItemAtual()
      end;
    end;
  end;
}
end;

procedure TFmMatFichCab.BtIncluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatFichIts, FmMatFichIts, afmoNegarComAviso) then
  begin
    FmMatFichIts.EdAbrange.Text := EdSelecao.Text;
    FmMatFichIts.EdAbrangeCodi.Text := EdSelCodi.Text;
    FmMatFichIts.EdAbrangeNome.Text := EdSelNome.Text;
    FmMatFichIts.ShowModal;
    FmMatFichIts.Destroy;
  end;
end;

procedure TFmMatFichCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatFichCab.CkSoAbrangenciaClick(Sender: TObject);
begin
  ReopenGraMatIts(0);
end;

procedure TFmMatFichCab.DBGrid1CellClick(Column: TColumn);
begin
  DefineAbrange(5);
end;

procedure TFmMatFichCab.DefineAbrange(NivAbrange: Integer);
var
  Codigo: Integer;
begin
  LimpaPesquisa();
  //
  if NivAbrange = 5 then
  begin
    FNivAbrange := 5;
    FAbrangePgt := QrPrdGrupTipCodigo.Value;
    FAbrangeGg1 := QrGraGru1Nivel1.Value;
    FAbrangeGgc := 0;
    FAbrangeGti := 0;
    FAbrangeGgx := 0;
    if QrPrdGrupTip.RecordCount > 0 then
    begin
      EdSelecao.ValueVariant := 'Todo grupos do Tipo ';
      EdSelCodi.ValueVariant := QrPrdGrupTipCodUsu.Value;
      EdSelNome.ValueVariant := QrPrdGrupTipNome.Value;
    end;
  end else
  if (NivAbrange = 4) or ((GradeC.Col = 0) and (GradeC.Row = 0)) then
  begin
    FNivAbrange := 4;
    FAbrangePgt := 0;
    FAbrangeGg1 := QrGraGru1Nivel1.Value;
    FAbrangeGgc := 0;
    FAbrangeGti := 0;
    FAbrangeGgx := 0;
    if QrGraGru1.RecordCount > 0 then
    begin
      EdSelecao.ValueVariant := 'Todo grupo ';
      EdSelCodi.ValueVariant := QrGraGru1CodUsu.Value;
      EdSelNome.ValueVariant := QrGraGru1Nome.Value;
    end;
  end else if (GradeC.Col = 0) then
  begin
    Codigo := Geral.IMV(GradeX.Cells[GradeC.Col,GradeC.Row]);
    FNivAbrange := 3;
    FAbrangePgt := 0;
    FAbrangeGg1 := QrGraGru1Nivel1.Value;
    FAbrangeGgc := Codigo;
    FAbrangeGti := 0;
    FAbrangeGgx := 0;
    if Codigo <> 0 then
    begin
      EdSelecao.ValueVariant := 'Todos tamanhos ativos da cor ';
      EdSelCodi.ValueVariant := Codigo;
      EdSelNome.ValueVariant := GradeC.Cells[GradeC.Col,GradeC.Row];
    end;
  end else if (GradeC.Row = 0) then
  begin
    Codigo := Geral.IMV(GradeX.Cells[GradeC.Col,GradeC.Row]);
    FNivAbrange := 2;
    FAbrangePgt := 0;
    FAbrangeGg1 := QrGraGru1Nivel1.Value;
    FAbrangeGgc := 0;
    FAbrangeGti := Codigo;
    FAbrangeGgx := 0;
    if Codigo <> 0 then
    begin
      EdSelecao.ValueVariant := 'Todas cores ativas do tamanho ';
      EdSelCodi.ValueVariant := Codigo;
      EdSelNome.ValueVariant := GradeC.Cells[GradeC.Col,GradeC.Row];
    end;
  end else begin
    Codigo := Geral.IMV(GradeC.Cells[GradeC.Col,GradeC.Row]);
    FNivAbrange := 1;
    FAbrangePgt := 0;
    FAbrangeGg1 := QrGraGru1Nivel1.Value;
    FAbrangeGgc := 0;
    FAbrangeGti := 0;
    FAbrangeGgx := Codigo;
    if Codigo <> 0 then
    begin
      EdSelecao.ValueVariant := 'Reduzido ';
      EdSelCodi.ValueVariant := Codigo;
      EdSelNome.ValueVariant := DmProd.NomeReduzido(QrGraGru1Nome.Value,
        GradeC, GradeC.Col, GradeC.Row);
    end;
  end;
  BtInclui.Enabled := EdSelecao.ValueVariant <> '';
  //
  ReopenGraMatIts(0);
end;

procedure TFmMatFichCab.dmkDBGrid1CellClick(Column: TColumn);
begin
  DefineAbrange(4);
end;

procedure TFmMatFichCab.EdFiltroNiv1Exit(Sender: TObject);
begin
  ReopenGraGru1(QrGraGru1Nivel1.Value);
end;

procedure TFmMatFichCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DefineAbrange(5);
end;

procedure TFmMatFichCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  FNivAbrange := 0;
  FAbrangePgt := 0;
  FAbrangeGg1 := 0;
  FAbrangeGgc := 0;
  FAbrangeGti := 0;
  FAbrangeGgx := 0;
  QrPrdGrupTip.Open;
  //
  GradeC.ColWidths[0] := 128;
  GradeA.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
end;

procedure TFmMatFichCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMatFichCab.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmMatFichCab.GradeCClick(Sender: TObject);
begin
  DefineAbrange(0);
end;

procedure TFmMatFichCab.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmMatFichCab.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmMatFichCab.LimpaPesquisa();
begin
  FNivAbrange := 0;
  FAbrangePgt := 0;
  FAbrangeGg1 := 0;
  FAbrangeGgc := 0;
  FAbrangeGti := 0;
  FAbrangeGgx := 0;
  //
  EdSelecao.ValueVariant := '';
  EdSelCodi.ValueVariant := 0;
  EdSelNome.ValueVariant := '';
  //
  QrGraMatIts.Close;
  BtInclui.Enabled := False;
end;

procedure TFmMatFichCab.PMAlteraPopup(Sender: TObject);
begin
  AlteraAbrangncia1.Enabled := EdSelCodi.ValueVariant <> '';
end;

procedure TFmMatFichCab.QrGraGru1AfterScroll(DataSet: TDataSet);
begin
  DmProd.ConfigGrades0(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC);
end;

procedure TFmMatFichCab.QrGraGru1BeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);
  LimpaPesquisa();
end;

procedure TFmMatFichCab.QrGraGru1BeforeScroll(DataSet: TDataSet);
begin
  LimpaPesquisa();
end;

procedure TFmMatFichCab.QrGraMatItsAfterOpen(DataSet: TDataSet);
begin
  BtExclui.Enabled := QrGraMatIts.RecordCount > 0;
  BtAltera.Enabled := QrGraMatIts.RecordCount > 0;
end;

procedure TFmMatFichCab.QrGraMatItsBeforeClose(DataSet: TDataSet);
begin
  BtExclui.Enabled := False;
  BtAltera.Enabled := False;
end;

procedure TFmMatFichCab.QrGraMatItsCalcFields(DataSet: TDataSet);
var
  Txt: AnsiString;
begin
  case QrGraMatItsNivPermite.Value of
    1: Txt := 'Reduzido: ' +
              QrGraMatItsNO2_GraGru1.Value + ' ' +
              QrGraMatItsNO2_COR.Value + ' ' +
              QrGraMatItsNO2_TAM.Value;
    2: Txt := 'Todas cores de : ' +
              QrGraMatItsNO2_GraGru1.Value + ' ' +
              QrGraMatItsNO2_TAM.Value;
    3: Txt := 'Todos tamanhos de: ' +
              QrGraMatItsNO2_GraGru1.Value + ' ' +
              QrGraMatItsNO2_COR.Value;
    4: Txt := 'Todos tamanhos e cores do grupo: ' + QrGraMatItsNO2_GraGru1.Value;
    5: Txt := 'Todos grupos do tipo: ' + QrGraMatItsNO2_PrdGrupTip.Value;
    else Txt := '********* ERRO **********';
  end;
  QrGraMatItsPERMISSOES.Value := Txt;

  //

  case QrGraMatItsNivAbrange.Value of
    1: Txt := 'Reduzido: ' +
              QrGraMatItsNO1_GraGru1.Value + ' ' +
              QrGraMatItsNO1_COR.Value + ' ' +
              QrGraMatItsNO1_TAM.Value;
    2: Txt := 'Todas cores de : ' +
              QrGraMatItsNO1_GraGru1.Value + ' ' +
              QrGraMatItsNO1_TAM.Value;
    3: Txt := 'Todos tamanhos de: ' +
              QrGraMatItsNO1_GraGru1.Value + ' ' +
              QrGraMatItsNO1_COR.Value;
    4: Txt := 'Todos tamanhos e cores do grupo: ' + QrGraMatItsNO1_GraGru1.Value;
    5: Txt := 'Todos grupos do tipo: ' + QrGraMatItsNO1_PrdGrupTip.Value;
    else Txt := '********* ERRO **********';
  end;
  QrGraMatItsABRANGENCIA.Value := Txt;

end;

procedure TFmMatFichCab.QrPrdGrupTipAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru1(0);
end;

procedure TFmMatFichCab.QrPrdGrupTipBeforeClose(DataSet: TDataSet);
begin
  QrGraGru1.Close
end;

procedure TFmMatFichCab.ReopenGraGru1(Nivel1: Integer);
begin
  QrGraGru1.Close;
  QrGraGru1.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
  QrGraGru1.Params[1].AsString  := '%' + EdFiltroNiv1.Text + '%';
  QrGraGru1.Open;
  if Nivel1 <> 0 then
    QrGraGru1.Locate('Nivel1', Nivel1, []);
end;

procedure TFmMatFichCab.ReopenGraMatIts(Controle: Integer);
var
  AbrangePgt,
  AbrangeGg1: Integer;
begin
  AbrangePgt := QrPrdGrupTipCodigo.Value;
  AbrangeGg1 := QrGraGru1Nivel1.Value;
  {
  AbrangeGgc :=
  AbrangeGti :=
  AbrangeGgx :=
  }
  QrGraMatIts.Close;
  QrGraMatIts.SQL.Clear;
QrGraMatIts.SQL.Add('SELECT ELT(mpc.Parte, "Secundária", "Principal",');
QrGraMatIts.SQL.Add('"? ? ?" ) NO_Parte, mpc.Nome NO_MatPartCad,');
QrGraMatIts.SQL.Add('ELT(NivAbrange, "Reduzido","Tamanho", "Cor", "Grupo", "Tipo", "?") Abrange_TXT,');
QrGraMatIts.SQL.Add('');
QrGraMatIts.SQL.Add('pgt1.CodUsu CU1_PrdGrupTip, pgt1.Nome NO1_PrdGrupTip,');
QrGraMatIts.SQL.Add('gg11.CodUsu CU1_GraGru1, gg11.Nome NO1_GraGru1, ggc1.GraCorCad, ');
QrGraMatIts.SQL.Add('gcc1.CodUsu CU1_COR, gcc1.Nome NO1_COR, gti1.Nome NO1_TAM, ');
QrGraMatIts.SQL.Add('');
QrGraMatIts.SQL.Add('pgt2.CodUsu CU2_PrdGrupTip, pgt2.Nome NO2_PrdGrupTip,');
QrGraMatIts.SQL.Add('gg12.CodUsu CU2_GraGru1, gg12.Nome NO2_GraGru1, ggc2.GraCorCad, ');
QrGraMatIts.SQL.Add('gcc2.CodUsu CU2_COR, gcc2.Nome NO2_COR, gti2.Nome NO2_TAM, ');
QrGraMatIts.SQL.Add('gmi.*');
QrGraMatIts.SQL.Add('FROM gramatits gmi');
QrGraMatIts.SQL.Add('LEFT JOIN matpartcad mpc ON mpc.Codigo=gmi.MatPartCad');
QrGraMatIts.SQL.Add('');
QrGraMatIts.SQL.Add('LEFT JOIN gragrux   ggx1 ON ggx1.Controle=gmi.AbrangeGgx');
QrGraMatIts.SQL.Add('LEFT JOIN gragru1   gg11 ON gg11.Nivel1=gmi.AbrangeGg1');
QrGraMatIts.SQL.Add('LEFT JOIN gragruc   ggc1 ON ggc1.Controle=gmi.AbrangeGgc');
QrGraMatIts.SQL.Add('LEFT JOIN gracorcad gcc1 ON gcc1.Codigo=ggc1.GraCorCad');
QrGraMatIts.SQL.Add('LEFT JOIN gratamits gti1 ON gti1.Controle=gmi.AbrangeGti');
QrGraMatIts.SQL.Add('LEFT JOIN prdgruptip pgt1 ON pgt1.Codigo=gmi.AbrangePgt');
QrGraMatIts.SQL.Add('');
QrGraMatIts.SQL.Add('LEFT JOIN gragrux ggx2 ON ggx2.Controle=gmi.PermiteGgx');
QrGraMatIts.SQL.Add('LEFT JOIN gragru1   gg12 ON gg12.Nivel1=gmi.PermiteGg1');
QrGraMatIts.SQL.Add('LEFT JOIN gragruc   ggc2 ON ggc2.Controle=gmi.PermiteGgc');
QrGraMatIts.SQL.Add('LEFT JOIN gracorcad gcc2 ON gcc2.Codigo=ggc2.GraCorCad');
QrGraMatIts.SQL.Add('LEFT JOIN gratamits gti2 ON gti2.Controle=gmi.PermiteGti');
QrGraMatIts.SQL.Add('LEFT JOIN prdgruptip pgt2 ON pgt2.Codigo=gmi.PermitePgt');
QrGraMatIts.SQL.Add('');
QrGraMatIts.SQL.Add('WHERE gmi.Controle > 0 ');
QrGraMatIts.SQL.Add('AND(');
QrGraMatIts.SQL.Add('   (gmi.NivAbrange=5 AND gmi.AbrangePgt=' + FormatFloat('0', AbrangePgt) + ')');
QrGraMatIts.SQL.Add('OR (gmi.NivAbrange=4 AND gmi.AbrangeGg1=' + FormatFloat('0', AbrangeGg1) + ')');
{
QrGraMatIts.SQL.Add('OR (gmi.NivAbrange=3 AND gmi.AbrangeGgc=' + FormatFloat('0', AbrangeGgc) + ')');
QrGraMatIts.SQL.Add('OR (gmi.NivAbrange=2 AND gmi.AbrangeGti=' + FormatFloat('0', AbrangeGti) + ')');
QrGraMatIts.SQL.Add('OR (gmi.NivAbrange=1 AND gmi.AbrangeGgx=' + FormatFloat('0', AbrangeGgx) + ')');
}
QrGraMatIts.SQL.Add(')');
  QrGraMatIts.Open;
  if Controle > 0 then
    QrGraMatIts.Locate('Controle', Controle, []);
end;

end.

