unit GraGruXEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  UnInternalConsts, mySQLDbTables, Menus, UnDmkProcFunc, dmkImage, UnDmkEnums,
  Vcl.Mask;

type
  TFmGraGruXEdit = class(TForm)
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    QrGraGru3: TmySQLQuery;
    DsGraGru3: TDataSource;
    QrGraGru2: TmySQLQuery;
    DsGraGru2: TDataSource;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrLoc: TmySQLQuery;
    QrLocCodUsu: TIntegerField;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrGraGru5: TmySQLQuery;
    DsGraGru5: TDataSource;
    QrGraGru4: TmySQLQuery;
    DsGraGru4: TDataSource;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGru3CodUsu: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru2Nivel2: TIntegerField;
    QrGraGru2CodUsu: TIntegerField;
    QrGraGru2Nome: TWideStringField;
    QrGraGru4Nivel4: TIntegerField;
    QrGraGru4CodUsu: TIntegerField;
    QrGraGru4Nome: TWideStringField;
    QrGraGru5Nivel5: TIntegerField;
    QrGraGru5CodUsu: TIntegerField;
    QrGraGru5Nome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    LaPrdGrupTip: TLabel;
    LaNivel3: TLabel;
    LaNivel2: TLabel;
    LaNivel5: TLabel;
    LaNivel4: TLabel;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    EdNivel3: TdmkEditCB;
    CBNivel3: TdmkDBLookupComboBox;
    EdNivel2: TdmkEditCB;
    CBNivel2: TdmkDBLookupComboBox;
    EdNivel5: TdmkEditCB;
    CBNivel5: TdmkDBLookupComboBox;
    EdNivel4: TdmkEditCB;
    CBNivel4: TdmkDBLookupComboBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CkRespeitarPai: TCheckBox;
    QrPrdGrupTipNivCad: TSmallintField;
    SbNivel5: TSpeedButton;
    SbNivel4: TSpeedButton;
    SbNivel3: TSpeedButton;
    SbNivel2: TSpeedButton;
    QrPrdGrupTipGradeado: TSmallintField;
    DBCkGradeado: TDBCheckBox;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    QrPrdGrupTipTitNiv4: TWideStringField;
    QrPrdGrupTipTitNiv5: TWideStringField;
    PMInsAlt: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Panel6: TPanel;
    Label2: TLabel;
    LaNivel1: TLabel;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXNivel5: TIntegerField;
    QrGraGruXNivel4: TIntegerField;
    QrGraGruXNivel3: TIntegerField;
    QrGraGruXNivel2: TIntegerField;
    QrGraGruXNivel1: TIntegerField;
    QrGraGruXNome: TWideStringField;
    QrGraGruXCodUsu: TIntegerField;
    QrGraGruXGraGruC: TIntegerField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraTamI: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXEAN13: TWideStringField;
    QrGraGruXGraGruY: TIntegerField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    QrGraGruY: TmySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    QrGraGruYLk: TIntegerField;
    QrGraGruYDataCad: TDateField;
    QrGraGruYDataAlt: TDateField;
    QrGraGruYUserCad: TIntegerField;
    QrGraGruYUserAlt: TIntegerField;
    QrGraGruYAlterWeb: TSmallintField;
    QrGraGruYAtivo: TSmallintField;
    DsGraGruY: TDataSource;
    EdGraGruY: TdmkEditCB;
    CBGraGruY: TdmkDBLookupComboBox;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraCorCad: TmySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    QrTamanho: TmySQLQuery;
    DsTamanho: TDataSource;
    LaGrade: TLabel;
    EdGraTamIts: TdmkEditCB;
    CBGraTamIts: TdmkDBLookupComboBox;
    SBGrade: TSpeedButton;
    Label1: TLabel;
    EdGraCorCad: TdmkEditCB;
    CBGraCorCad: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrGraGruXGraCorCad: TIntegerField;
    QrGraGruXGraTamIts: TIntegerField;
    QrTamanhoCodigo: TIntegerField;
    QrTamanhoControle: TIntegerField;
    QrTamanhoNO_GRADE_ITEM: TWideStringField;
    EdCOD_ITEM: TdmkEdit;
    Label3: TLabel;
    QrGraGruXCOD_ITEM: TWideStringField;
    Label4: TLabel;
    Edprod_cBarra: TdmkEdit;
    QrGraGruXprod_cBarra: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
    procedure QrGraGru3BeforeClose(DataSet: TDataSet);
    procedure EdNivel3Change(Sender: TObject);
    procedure QrGraGru2BeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure EdNivel5Change(Sender: TObject);
    procedure CkRespeitarPaiClick(Sender: TObject);
    procedure SbNivel5Click(Sender: TObject);
    procedure SbNivel4Click(Sender: TObject);
    procedure SbNivel3Click(Sender: TObject);
    procedure SbNivel2Click(Sender: TObject);
    procedure CBPrdGrupTipClick(Sender: TObject);
    procedure EdNivel4Change(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure EdNivel2Change(Sender: TObject);
  private
    { Private declarations }
    FNivCad, FNivClk: Integer;
    FEmReabertura: Boolean;
    //
    function  FiltroNivel(Nivel, Codigo: Integer): String;
    procedure AlteraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
              EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
    procedure CadastraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
              EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
    //
    procedure ReopenNiveisAPartirDe(Nivel: Integer);
    procedure ReopenNivelX(Nivel: Integer; Qry: TmySQLQuery;
              EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
  public
    { Public declarations }
    FQrGraGru1, FQrGraGruX: TmySQLQuery;
    procedure HabilitaComponentes();
    procedure ReopenGraGruX(GragruX: Integer);
  end;

  var
  FmGraGruXEdit: TFmGraGruXEdit;

implementation

uses UnMyObjects, Module, UMySQLModule, GraGruN, DmkDAC_PF, ModProd, UnAppPF,
  UnGrade_Jan, UnGrade_PF;

{$R *.DFM}

procedure TFmGraGruXEdit.Altera1Click(Sender: TObject);
begin
  case FNivClk of
    2: AlteraRegistroNivel(2, QrGraGru2, EdNivel2, CBNivel2);
    3: AlteraRegistroNivel(3, QrGraGru3, EdNivel3, CBNivel3);
    4: AlteraRegistroNivel(4, QrGraGru4, EdNivel4, CBNivel4);
    5: AlteraRegistroNivel(5, QrGraGru5, EdNivel5, CBNivel5);
  end;
end;

procedure TFmGraGruXEdit.AlteraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
  EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);

var
  X, Nome: String;
  NivelX, CodUsu: Integer;
begin
  X := Geral.FF0(Nivel);
  Nome := CBNivel.Text;
  if InputQuery('Altera��o de Item no N�vel ' + X, 'Informe a descri��o:', Nome) then
  begin
    UMyMod.ObtemCodigoDeCodUsu(TdmkEditCB(EdNivel), NivelX, '', 'Nivel' + X);
    CodUsu := EdNivel.ValueVariant;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru' + X, False, [
    'Nome'], ['Nivel' + X], [Nome], [
    NivelX], True) then
    begin
      ReopenNiveisAPartirDe(Nivel);
      EdNivel.ValueVariant := CodUsu;
      CBNivel.KeyValue := CodUsu;
    end;
  end;
end;

procedure TFmGraGruXEdit.BtOKClick(Sender: TObject);
var
  Nivel1, GraCorCad, GraGruC: Integer;
  //
  GraGru1, GraTamI, Controle, GraGruY: Integer;
  SQLType: TSQLType;
  COD_ITEM, prod_cBarra: String;
var
  Nivel5, Nivel4, Nivel3, Nivel2, PrdGrupTip, GraTamCad: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  GraTamCad      := 0;
  //
  Nivel5         := EdNivel5.ValueVariant;
  Nivel4         := EdNivel4.ValueVariant;
  Nivel3         := EdNivel3.ValueVariant;
  Nivel2         := EdNivel2.ValueVariant;
  Nivel1         := QrGraGruXNivel1.Value;
  PrdGrupTip     := EdPrdGrupTip.ValueVariant;
  GraTamI        := EdGraTamIts.ValueVariant;
  COD_ITEM       := EdCOD_ITEM.Text;
  prod_cBarra    := Edprod_cBarra.Text;
  if GraTamI <> 0 then
    GraTamCad    := QrTamanhoCodigo.Value;
  //
  if Grade_PF.RegistrarACor(Nivel1, GraCorCad, GraGruC) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru1', False, [
    'Nivel5', 'Nivel4', 'Nivel3',
    'Nivel2', 'PrdGrupTip', 'GraTamCad'], [
    'Nivel1'], [
    Nivel5, Nivel4, Nivel3,
    Nivel2, PrdGrupTip, GraTamCad], [
    Nivel1], True) then
    begin
      GraGru1        := Nivel1;
      Controle       := QrGraGruXControle.Value;
      GraGruY        := EdGraGruY.ValueVariant;
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrux', False, [
      'GraGruC', 'GraGru1', 'GraTamI',
      'GraGruY', 'COD_ITEM', 'prod_cBarra'], [
      'Controle'], [
      GraGruC, GraGru1, GraTamI,
      GraGruY, COD_ITEM, prod_cBarra], [
      Controle], False);
      Close;
    end;
  end;
end;

procedure TFmGraGruXEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruXEdit.CadastraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
  EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
var
  SQL: String;
  //
  procedure SQLNiv(Nivel: Integer; EdNivel: TdmkEditCB);
  var
    Codigo: Integer;
    Fld: String;
  begin
    Fld := 'Nivel' + IntToStr(Nivel);
    UMyMod.ObtemCodigoDeCodUsu(EdNivel, Codigo, '', Fld);

    if EdNivel.ValueVariant = 0 then
      SQL := SQL + Fld + '=0,'
    else
      SQL := SQL + Fld + '=' + Geral.FF0(Codigo) + ',';
  end;
var
  X, Nome: String;
  NivelX, CodUsu, PrdGrupTip: Integer;
begin
  X := Geral.FF0(Nivel);
  Nome := '';
  if InputQuery('Cadastro de Item no N�vel ' + X, 'Informe a descri��o:', Nome) then
  begin
    SQL := '';
    if Nivel = 5 then SQL := 'Nivel6=' + Geral.FF0(QrPrdGrupTipCodigo.Value);
    //
    if Nivel < 5 then SQLNiv(5, EdNivel5);
    if Nivel < 4 then SQLNiv(4, EdNivel4);
    if Nivel < 3 then SQLNiv(3, EdNivel3);
    //
    if Length(SQL) > 1 then
      if SQL[Length(SQL)] = ',' then
        SQL := Copy(SQL, 1, Length(SQL) - 1);
    NivelX := UMyMod.BPGS1I32(
    'GraGru' + X, 'Nivel' + X, '', '', tsPos, stIns, 0);
    CodUsu := UMyMod.BPGS1I32(
    'GraGru' + X, 'CodUsu', '', '', tsPos, stIns, 0);
    //
    PrdGrupTip := EdPrdGrupTip.ValueVariant;
    //
    if PrdGrupTip <> 0 then
      PrdGrupTip := QrPrdGrupTipCodigo.value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru' + X, False, [
    CO_JOKE_SQL, 'CodUsu', 'Nome', 'PrdGrupTip'], [
    'Nivel' + X], [
    SQL, CodUsu, Nome, PrdGrupTip], [
    NivelX], True) then
    begin
      ReopenNiveisAPartirDe(Nivel);
      EdNivel.ValueVariant := CodUsu;
      CBNivel.KeyValue := CodUsu;
    end;
  end;
end;

procedure TFmGraGruXEdit.CBPrdGrupTipClick(Sender: TObject);
begin
  HabilitaComponentes();
end;

procedure TFmGraGruXEdit.CkRespeitarPaiClick(Sender: TObject);
begin
  HabilitaComponentes();
end;

procedure TFmGraGruXEdit.EdNivel2Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(1);
end;

procedure TFmGraGruXEdit.EdNivel3Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(2);
end;

procedure TFmGraGruXEdit.EdNivel4Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(3);
end;

procedure TFmGraGruXEdit.EdNivel5Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(4);
end;

procedure TFmGraGruXEdit.EdPrdGrupTipChange(Sender: TObject);
begin
  HabilitaComponentes();
end;

function TFmGraGruXEdit.FiltroNivel(Nivel, Codigo: Integer): String;
begin
  if CkRespeitarPai.Checked then
    Result := 'OR Nivel' + Geral.FF0(Nivel) + '=' + Geral.FF0(Codigo)
  else
    Result := 'OR Nivel' + Geral.FF0(Nivel) + '<>0';
end;

procedure TFmGraGruXEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruXEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  VAR_CADASTRO := 0;
  FQrGraGru1 := nil;
  FQrGraGruX := nil;
  //
  FEmReabertura := False;
  //
  FNivCad := 0;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTamanho, Dmod.MyDB);
end;

procedure TFmGraGruXEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruXEdit.HabilitaComponentes();
var
  Habil: Boolean;
begin
  FNivCad := QrPrdGrupTipNivCad.Value;
  //
  Habil := FNivCad >= 5;
  LaNivel5.Enabled := Habil;
  EdNivel5.Enabled := Habil;
  CBNivel5.Enabled := Habil;
  SbNivel5.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel5.Caption := QrPrdGrupTipTitNiv5.Value;
  //
  Habil := FNivCad >= 4;
  LaNivel4.Enabled := Habil;
  EdNivel4.Enabled := Habil;
  CBNivel4.Enabled := Habil;
  SbNivel4.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel4.Caption := QrPrdGrupTipTitNiv4.Value;
  //
  Habil := FNivCad >= 3;
  LaNivel3.Enabled := Habil;
  EdNivel3.Enabled := Habil;
  CBNivel3.Enabled := Habil;
  SbNivel3.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel3.Caption := QrPrdGrupTipTitNiv3.Value;
  //
  Habil := FNivCad >= 2;
  LaNivel2.Enabled := Habil;
  EdNivel2.Enabled := Habil;
  CBNivel2.Enabled := Habil;
  SbNivel2.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel2.Caption := QrPrdGrupTipTitNiv2.Value;
  //
  LaNivel1.Caption := QrPrdGrupTipTitNiv1.Value;
  //
  ReopenNiveisAPartirDe(5);
end;

procedure TFmGraGruXEdit.Inclui1Click(Sender: TObject);
begin
  case FNivClk of
    2: CadastraRegistroNivel(2, QrGraGru2, EdNivel2, CBNivel2);
    3: CadastraRegistroNivel(3, QrGraGru3, EdNivel3, CBNivel3);
    4: CadastraRegistroNivel(4, QrGraGru4, EdNivel4, CBNivel4);
    5: CadastraRegistroNivel(5, QrGraGru5, EdNivel5, CBNivel5);
  end;
end;

procedure TFmGraGruXEdit.QrGraGru2BeforeClose(DataSet: TDataSet);
begin
//  QrGraGru1.Close;
end;

procedure TFmGraGruXEdit.QrGraGru3BeforeClose(DataSet: TDataSet);
begin
  //QrGraGru2.Close;
end;

procedure TFmGraGruXEdit.ReopenNivelX(Nivel: Integer; Qry: TmySQLQuery;
EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
  function NivAcima(Nivel: Integer; EdNivel: TdmkEdit): Integer;
  begin
    if EdNivel.ValueVariant = 0 then
      Result := 0
    else
      Result := Nivel;
  end;
var
  Acima,
  Este: Integer;
  Compl, X, Y: String;
  //
  sNivel2, sNivel3, sNivel4, sNivel5, sPrdGrupTip: String;
begin
  Acima := 0;
  X     := Geral.FF0(Nivel);
  Y     := Geral.FF0(Nivel + 1);
  //
  case Nivel of
    5: Acima := NivAcima(QrPrdGrupTipCodigo.Value, EdPrdGrupTip);
    4: Acima := NivAcima(QrGraGru5Nivel5.Value, EdNivel5);
    3: Acima := NivAcima(QrGraGru4Nivel4.Value, EdNivel4);
    2: Acima := NivAcima(QrGraGru3Nivel3.Value, EdNivel3);
    1: Acima := NivAcima(QrGraGru2Nivel2.Value, EdNivel2);
  end;
  if Qry.State <> dsInactive then
  begin
    //Acima := Qry.FieldByName('Nivel' + X).AsInteger;
    Compl := FiltroNivel(Nivel + 1, Acima);
  end else
  begin
    //Acima := 0;
    Compl := '';
  end;
  //  ini 2021-07-16
  sPrdGrupTip := Geral.FF0(EdPrdGrupTip.ValueVariant);
  sNivel5 := Geral.FF0(EdNivel5.ValueVariant);
  sNivel4 := Geral.FF0(EdNivel4.ValueVariant);
  sNivel3 := Geral.FF0(EdNivel3.ValueVariant);
  sNivel2 := Geral.FF0(EdNivel2.ValueVariant);
  case Nivel of
    2:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Nivel2, CodUsu, Nome ',
      'FROM gragru2 ',
      'WHERE PrdGrupTip=' + sPrdGrupTip,
      'AND Nivel5=' + sNivel5,
      'AND Nivel4=' + sNivel4,
      'AND Nivel3=' + sNivel3,
      'ORDER BY Nome ',
      '']);
    end;
    3:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Nivel3, CodUsu, Nome ',
      'FROM gragru3 ',
      'WHERE PrdGrupTip=' + sPrdGrupTip,
      'AND Nivel5=' + sNivel5,
      'AND Nivel4=' + sNivel4,
      'ORDER BY Nome ',
      '']);
    end;
    4:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Nivel4, CodUsu, Nome ',
      'FROM gragru4 ',
      'WHERE PrdGrupTip=' + sPrdGrupTip,
      'AND Nivel5=' + sNivel5,
      'ORDER BY Nome ',
      '']);
    end;
    5:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Nivel5, CodUsu, Nome ',
      'FROM gragru5 ',
      'WHERE PrdGrupTip=' + sPrdGrupTip,
      'AND Nivel5=' + sNivel5,
      'ORDER BY Nome ',
      '']);
    end;
    else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Nivel' + X + ', CodUsu, Nome ',
      'FROM gragru' + X,
      'WHERE Nivel' + Y + '=0 ',
      Compl,
      'ORDER BY Nome ',
      '']);
    end;
  end;
  //Geral.MB_Teste(Qry.SQL.Text);
  // fim 2021-07-06
  Este := EdNivel.ValueVariant;
  if (FNivCad < Nivel) or (not QrGraGru5.Locate('CodUsu', Este, [])) then
  begin
    EdNivel.ValueVariant := 0;
    CBNivel.KeyValue := 0;
  end;
end;

procedure TFmGraGruXEdit.SbNivel2Click(Sender: TObject);
begin
  FNivClk := 2;
  Altera1.Enabled := EdNivel2.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel2);
end;

procedure TFmGraGruXEdit.SbNivel3Click(Sender: TObject);
begin
  FNivClk := 3;
  Altera1.Enabled := EdNivel3.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel3);
end;

procedure TFmGraGruXEdit.SbNivel4Click(Sender: TObject);
begin
  FNivClk := 4;
  Altera1.Enabled := EdNivel4.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel4);
end;

procedure TFmGraGruXEdit.SbNivel5Click(Sender: TObject);
begin
  FNivClk := 5;
  Altera1.Enabled := EdNivel5.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel5);
end;

procedure TFmGraGruXEdit.ReopenGraGruX(GragruX: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggc.GraCorCad, gti.Controle GraTamIts, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
  'gg1.Nivel5, gg1.Nivel4, gg1.Nivel3, gg1.Nivel2,',
  'gg1.Nivel1, gg1.Nome, gg1.CodUsu, gg1.PrdGrupTip, ',
  'ggx.* ',
  'FROM gragrux ggx',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
  '']);
  EdGraGruY.ValueVariant := QrGraGruXGraGruY.Value;
  CBGraGruY.KeyValue     := QrGraGruXGraGruY.Value;
  //
  EdPrdGrupTip.ValueVariant := QrGraGruXPrdGrupTip.Value;
  CBPrdGrupTip.KeyValue     := QrGraGruXPrdGrupTip.Value;
  //
  EdNivel5.ValueVariant := QrGraGruXNivel5.Value;
  CBNivel5.KeyValue     := QrGraGruXNivel5.Value;
  //
  EdNivel4.ValueVariant := QrGraGruXNivel4.Value;
  CBNivel4.KeyValue     := QrGraGruXNivel4.Value;
  //
  EdNivel3.ValueVariant := QrGraGruXNivel3.Value;
  CBNivel3.KeyValue     := QrGraGruXNivel3.Value;
  //
  EdNivel2.ValueVariant := QrGraGruXNivel2.Value;
  CBNivel2.KeyValue     := QrGraGruXNivel2.Value;
  //
  EdGraTamIts.ValueVariant := QrGraGruXGraTamIts.Value;
  CBGraTamIts.KeyValue     := QrGraGruXGraTamIts.Value;
  //
  EdGraCorCad.ValueVariant := QrGraGruXGraCorCad.Value;
  CBGraCorCad.KeyValue     := QrGraGruXGraCorCad.Value;
  //
  Edprod_cBarra.ValueVariant := QrGraGruXprod_cBarra.Value;
  //
(*
  EdNivel1.ValueVariant := QrGraGruXNivel1.Value;
  CBNivel1.KeyValue     := QrGraGruXNivel1.Value;
  //
*)
  EdCOD_ITEM.ValueVariant := QrGraGruXCOD_ITEM.Value;
end;

procedure TFmGraGruXEdit.ReopenNiveisAPartirDe(Nivel: Integer);
begin
  if FEmReabertura then
    Exit;
  FEmReabertura := True;
  try
    if Nivel >= 5 then ReopenNivelX(5, QrGraGru5, EdNivel5, CBNivel5);
    if Nivel >= 4 then ReopenNivelX(4, QrGraGru4, EdNivel4, CBNivel4);
    if Nivel >= 3 then ReopenNivelX(3, QrGraGru3, EdNivel3, CBNivel3);
    if Nivel >= 2 then ReopenNivelX(2, QrGraGru2, EdNivel2, CBNivel2);
  finally
    FEmReabertura := False;
  end;
end;

//colocar tambem grade e cor!

end.
