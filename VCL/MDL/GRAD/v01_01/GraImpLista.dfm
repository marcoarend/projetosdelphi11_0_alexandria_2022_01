object FmGraImpLista: TFmGraImpLista
  Left = 339
  Top = 185
  Caption = 'PRD-PRINT-001 :: Impress'#227'o de Produtos por Lista'
  ClientHeight = 732
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 620
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 310
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 468
        Height = 310
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label3: TLabel
          Left = 8
          Top = 44
          Width = 79
          Height = 13
          Caption = 'Tipo de Produto:'
        end
        object Label5: TLabel
          Left = 8
          Top = 164
          Width = 90
          Height = 13
          Caption = 'Produto [F3 todos]:'
        end
        object Label16: TLabel
          Left = 8
          Top = 124
          Width = 36
          Height = 13
          Caption = 'Nivel 2:'
        end
        object Label17: TLabel
          Left = 8
          Top = 84
          Width = 36
          Height = 13
          Caption = 'Nivel 3:'
        end
        object dmkLabel3: TdmkLabel
          Left = 8
          Top = 204
          Width = 90
          Height = 13
          Caption = 'Centro de estoque:'
          UpdType = utYes
          SQLType = stNil
        end
        object LaInventarioFisco: TLabel
          Left = 8
          Top = 245
          Width = 421
          Height = 13
          Caption = 
            'A tabela da pre'#231'os a ser usada '#233' definida no cadastro de tipo de' +
            ' produto!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object PnGraCusPrc: TPanel
          Left = 0
          Top = 262
          Width = 468
          Height = 48
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 12
          Visible = False
          object Label7: TLabel
            Left = 8
            Top = 3
            Width = 76
            Height = 13
            Caption = 'Lista de Pre'#231'os:'
          end
          object EdGraCusPrc: TdmkEditCB
            Left = 8
            Top = 19
            Width = 53
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGraGru1Change
            DBLookupComboBox = CBGraCusPrc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraCusPrc: TdmkDBLookupComboBox
            Left = 64
            Top = 19
            Width = 400
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsGraCusPrc
            TabOrder = 1
            dmkEditCB = EdGraCusPrc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 20
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 400
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPrdGrupTip: TdmkEditCB
          Left = 8
          Top = 60
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPrdGrupTipChange
          DBLookupComboBox = CBPrdGrupTip
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPrdGrupTip: TdmkDBLookupComboBox
          Left = 64
          Top = 60
          Width = 400
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsPrdGrupTip
          TabOrder = 3
          dmkEditCB = EdPrdGrupTip
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraGru1: TdmkEditCB
          Left = 8
          Top = 180
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGru1Change
          OnKeyDown = EdGraGru1KeyDown
          DBLookupComboBox = CBGraGru1
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGru1: TdmkDBLookupComboBox
          Left = 64
          Top = 180
          Width = 400
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru1
          TabOrder = 9
          dmkEditCB = EdGraGru1
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraGru2: TdmkEditCB
          Left = 8
          Top = 140
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGru1Change
          DBLookupComboBox = CBGraGru2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGru2: TdmkDBLookupComboBox
          Left = 64
          Top = 140
          Width = 400
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru2
          TabOrder = 7
          dmkEditCB = EdGraGru2
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraGru3: TdmkEditCB
          Left = 8
          Top = 100
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGru1Change
          DBLookupComboBox = CBGraGru3
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGru3: TdmkDBLookupComboBox
          Left = 64
          Top = 100
          Width = 400
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru3
          TabOrder = 5
          dmkEditCB = EdGraGru3
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdStqCenCad: TdmkEditCB
          Left = 8
          Top = 220
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdStqCenCadChange
          DBLookupComboBox = CBStqCenCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenCad: TdmkDBLookupComboBox
          Left = 64
          Top = 220
          Width = 400
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsStqCenCadPsq
          TabOrder = 11
          dmkEditCB = EdStqCenCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object PageControl1: TPageControl
        Left = 468
        Top = 0
        Width = 540
        Height = 310
        ActivePage = TabSheet5
        Align = alClient
        TabOrder = 1
        OnChange = PageControl1Change
        object TabSheet20: TTabSheet
          Caption = ' Estoque '
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 282
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel31: TPanel
              Left = 180
              Top = 0
              Width = 352
              Height = 282
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel32: TPanel
                Left = 0
                Top = 0
                Width = 352
                Height = 38
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object BitBtn5: TBitBtn
                  Left = 4
                  Top = 4
                  Width = 157
                  Height = 25
                  Caption = 'Seleciona v'#225'rios produtos'
                  TabOrder = 0
                  OnClick = BitBtn5Click
                end
                object CkSelecao0: TCheckBox
                  Left = 176
                  Top = 2
                  Width = 213
                  Height = 17
                  Caption = 'Pesquisar pela sele'#231#227'o.'
                  TabOrder = 1
                end
                object CkListaReduzidos: TCheckBox
                  Left = 176
                  Top = 19
                  Width = 221
                  Height = 17
                  Caption = 'Imprime lista de reduzidos.'
                  TabOrder = 2
                end
              end
              object DBGrid4: TDBGrid
                Left = 0
                Top = 38
                Width = 352
                Height = 244
                Align = alClient
                DataSource = DsPesqESel_Sel
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'C'#243'digo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Visible = True
                  end>
              end
            end
            object Panel33: TPanel
              Left = 0
              Top = 0
              Width = 180
              Height = 282
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object CGAgrup0: TdmkCheckGroup
                Left = 0
                Top = 0
                Width = 180
                Height = 89
                Align = alTop
                Caption = ' Agrupamentos: '
                Items.Strings = (
                  'Tipos de produtos'
                  'Nivel 2'
                  'Produtos')
                TabOrder = 0
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
              object CGSitEstq: TdmkCheckGroup
                Left = 0
                Top = 89
                Width = 180
                Height = 88
                Align = alTop
                Caption = ' Situa'#231#227'o do estoque: '
                Items.Strings = (
                  'Negativos'
                  'Zerados'
                  'Positivos')
                TabOrder = 1
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
              object CkStqCenCad: TCheckBox
                Left = 4
                Top = 184
                Width = 175
                Height = 17
                Caption = 'Por centro de estoque.'
                TabOrder = 2
                OnClick = CkStqCenCadClick
              end
              object CkPrdGrupo: TCheckBox
                Left = 4
                Top = 202
                Width = 175
                Height = 17
                Caption = 'Sintetizar por grupo de produto.'
                TabOrder = 3
                OnClick = CkPrdGrupoClick
              end
            end
          end
        end
        object TabSheet12: TTabSheet
          Caption = ' Hist'#243'rico '
          ImageIndex = 1
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 282
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 133
              Height = 282
              Align = alLeft
              Caption = ' Per'#237'odo: '
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 16
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label2: TLabel
                Left = 8
                Top = 56
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPDataIni1: TdmkEditDateTimePicker
                Left = 8
                Top = 36
                Width = 113
                Height = 21
                Date = 40165.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 0
                OnChange = TPDataIni1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPDataFim1: TdmkEditDateTimePicker
                Left = 8
                Top = 72
                Width = 113
                Height = 21
                Date = 40165.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 1
                OnChange = TPDataFim1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
            object CkInfoMP_e_Classif: TCheckBox
              Left = 140
              Top = 8
              Width = 353
              Height = 17
              Caption = 
                'Informar o lote de origem da mat'#233'ria-prima e dos couros classifi' +
                'cados.'
              TabOrder = 1
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Estoque final em... '
          ImageIndex = 2
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 232
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitHeight = 178
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 128
              Height = 232
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitHeight = 178
              object CGAgrup2: TdmkCheckGroup
                Left = 0
                Top = 84
                Width = 128
                Height = 148
                Align = alClient
                Caption = ' Agrupamentos: '
                ItemIndex = 2
                Items.Strings = (
                  'Tipos de produtos'
                  'Nivel 2'
                  'Produtos')
                TabOrder = 0
                UpdType = utYes
                Value = 4
                OldValor = 0
                ExplicitHeight = 94
              end
              object GroupBox4: TGroupBox
                Left = 0
                Top = 40
                Width = 128
                Height = 44
                Align = alTop
                Caption = '                          '
                TabOrder = 1
                object TPDataIni2: TdmkEditDateTimePicker
                  Left = 8
                  Top = 20
                  Width = 112
                  Height = 21
                  Date = 40269.000000000000000000
                  Time = 0.408699699073622500
                  TabOrder = 0
                  OnClick = TPDataFim2Click
                  OnChange = TPDataFim2Change
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkDataIni2: TCheckBox
                  Left = 12
                  Top = 0
                  Width = 97
                  Height = 17
                  Caption = 'Data inicial:'
                  TabOrder = 1
                end
              end
              object GroupBox5: TGroupBox
                Left = 0
                Top = 0
                Width = 128
                Height = 40
                Align = alTop
                Caption = ' Data Final: '
                TabOrder = 2
                object TPDataFim2: TdmkEditDateTimePicker
                  Left = 8
                  Top = 16
                  Width = 112
                  Height = 21
                  Date = 40269.000000000000000000
                  Time = 0.408699699073622500
                  TabOrder = 0
                  OnClick = TPDataFim2Click
                  OnChange = TPDataFim2Change
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
              end
            end
            object DBGrid1: TDBGrid
              Left = 301
              Top = 0
              Width = 231
              Height = 232
              Align = alRight
              DataSource = DsPGT_SCC
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PrdGrupTip'
                  Title.Caption = 'Grupo'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'StqCenCad'
                  Title.Caption = 'Centro'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Empresa'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EntiSitio'
                  Title.Caption = 'S'#237'tio'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LstPrcFisc'
                  Title.Caption = 'Lista'
                  Width = 38
                  Visible = True
                end>
            end
            object Panel20: TPanel
              Left = 128
              Top = 0
              Width = 169
              Height = 232
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              ExplicitHeight = 178
              object RGTipoPesq2: TRadioGroup
                Left = 0
                Top = 0
                Width = 169
                Height = 85
                Align = alTop
                Caption = ' Tipo de consulta: '
                ItemIndex = 0
                Items.Strings = (
                  'Nenhum'
                  'Relat'#243'rio do estoque pr'#243'prio'
                  'Relat'#243'rio de invent'#225'rio (Fisco)')
                TabOrder = 0
                OnClick = RGTipoPesq2Click
              end
              object CkPositivo2: TCheckBox
                Left = 8
                Top = 92
                Width = 109
                Height = 17
                Caption = 'Somente positivos'
                Checked = True
                State = cbChecked
                TabOrder = 1
                OnClick = CkPositivo2Click
              end
              object CkExclEstqMP: TCheckBox
                Left = 8
                Top = 108
                Width = 265
                Height = 33
                Caption = 'Excluir lan'#231'tos de couro '#13#10'importados do SINTEGRA.'
                TabOrder = 2
                WordWrap = True
              end
            end
          end
          object Panel34: TPanel
            Left = 0
            Top = 232
            Width = 532
            Height = 50
            Align = alBottom
            ParentBackground = False
            TabOrder = 1
            ExplicitTop = 178
            object Label18: TLabel
              Left = 164
              Top = 6
              Width = 201
              Height = 13
              Caption = 'Selecione a data antes da tabela!!!'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object EdTabePrcCab2: TdmkEditCB
              Left = 8
              Top = 23
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdGraGru1Change
              DBLookupComboBox = CBTabePrcCab2
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBTabePrcCab2: TdmkDBLookupComboBox
              Left = 64
              Top = 23
              Width = 453
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsTabePrcCab
              TabOrder = 1
              dmkEditCB = EdTabePrcCab2
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CkTabePrcCab2: TCheckBox
              Left = 8
              Top = 4
              Width = 153
              Height = 17
              Caption = 'Usar esta tabela de pre'#231'os:'
              TabOrder = 2
              OnClick = CkTabePrcCab2Click
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = ' Lan'#231'amentos '
          ImageIndex = 3
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 282
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object RGSMI: TRadioGroup
              Left = 0
              Top = 0
              Width = 77
              Height = 282
              Align = alLeft
              Caption = ' Estoque: '
              ItemIndex = 0
              Items.Strings = (
                'A - Ativo'
                'B - Morto')
              TabOrder = 0
            end
            object GroupBox2: TGroupBox
              Left = 77
              Top = 0
              Width = 133
              Height = 282
              Align = alLeft
              Caption = ' Per'#237'odo: '
              TabOrder = 1
              object Label9: TLabel
                Left = 8
                Top = 16
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label10: TLabel
                Left = 8
                Top = 56
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPDataIni3: TdmkEditDateTimePicker
                Left = 8
                Top = 36
                Width = 113
                Height = 21
                Date = 45150.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 0
                OnChange = TPDataIni1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPDataFim3: TdmkEditDateTimePicker
                Left = 8
                Top = 72
                Width = 113
                Height = 21
                Date = 40165.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 1
                OnChange = TPDataFim1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
            object BtExcluiIncond: TBitBtn
              Tag = 12
              Left = 220
              Top = 20
              Width = 177
              Height = 40
              Caption = 'Exclus'#227'o Incondicional'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtExcluiIncondClick
            end
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'Movimento'
          ImageIndex = 4
          object Panel19: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 282
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
          end
        end
        object TabSheet14: TTabSheet
          Caption = ' Panorama MP'
          ImageIndex = 5
          object Panel23: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 282
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 133
              Height = 282
              Align = alLeft
              Caption = ' Per'#237'odo da pesquisa: '
              TabOrder = 0
              object Label13: TLabel
                Left = 8
                Top = 16
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label14: TLabel
                Left = 8
                Top = 56
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPDataIni5: TdmkEditDateTimePicker
                Left = 8
                Top = 36
                Width = 113
                Height = 21
                Date = 45150.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 0
                OnChange = TPDataIni1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPDataFim5: TdmkEditDateTimePicker
                Left = 8
                Top = 72
                Width = 113
                Height = 21
                Date = 40165.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 1
                OnChange = TPDataFim1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
            object CBFiltro5: TdmkCheckGroup
              Left = 133
              Top = 0
              Width = 103
              Height = 282
              Align = alLeft
              Caption = ' Filtro Baixa: '
              Items.Strings = (
                'Baixa correta'
                'Sem Baixa'
                'Erro Baixa')
              TabOrder = 1
              OnClick = CBFiltro5Click
              UpdType = utYes
              Value = 0
              OldValor = 0
            end
            object Panel30: TPanel
              Left = 236
              Top = 0
              Width = 296
              Height = 282
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 2
            end
          end
        end
        object TabSheet17: TTabSheet
          Caption = ' Compara "Estoque" e "Estoque em..." '
          ImageIndex = 6
          object Panel35: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 282
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
          end
        end
        object TabSheet18: TTabSheet
          Caption = ' Reposi'#231#227'o '
          ImageIndex = 7
          object Panel38: TPanel
            Left = 0
            Top = 0
            Width = 532
            Height = 282
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object GroupBox6: TGroupBox
              Left = 1
              Top = 1
              Width = 133
              Height = 280
              Align = alLeft
              Caption = ' Per'#237'odo: '
              TabOrder = 0
              object Label6: TLabel
                Left = 8
                Top = 16
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label19: TLabel
                Left = 8
                Top = 56
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPDataIni7: TdmkEditDateTimePicker
                Left = 8
                Top = 32
                Width = 113
                Height = 21
                Date = 45150.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 0
                OnChange = TPDataIni1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPDataFim7: TdmkEditDateTimePicker
                Left = 8
                Top = 72
                Width = 113
                Height = 21
                Date = 40165.000000000000000000
                Time = 0.499141331019927700
                TabOrder = 1
                OnChange = TPDataFim1Change
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 310
      Width = 1008
      Height = 310
      ActivePage = TabSheet6
      Align = alClient
      TabOrder = 1
      OnChange = PageControl2Change
      object TabSheet1: TTabSheet
        Caption = ' Estoque '
        object PnDados0: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 282
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object LaStqTotal: TLabel
            Left = 1
            Top = 268
            Width = 998
            Height = 13
            Align = alBottom
            ExplicitWidth = 3
          end
          object DBGPrdEstq: TdmkDBGridZTO
            Left = 1
            Top = 1
            Width = 998
            Height = 267
            Align = alClient
            DataSource = DsPrdEstq
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PGT'
                Title.Caption = 'Tipo de Produto'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTDE'
                Title.Caption = 'Quantidade'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrcCusUni'
                Title.Caption = '$ Unit'#225'rio'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorTot'
                Title.Caption = '$ Estoque'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nivel1'
                Title.Caption = 'ID'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Sigla'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Produto'
                Width = 223
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tam.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SCC'
                Title.Caption = 'Centro de estoque'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PECAS'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AREAM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AREAP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PESO'
                Title.Caption = 'Peso kg'
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Hist'#243'rico'
        ImageIndex = 1
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 234
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object DBGPrdMov: TdmkDBGrid
            Left = 1
            Top = 1
            Width = 998
            Height = 232
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Width = 109
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TIPO'
                Title.Caption = 'A'#231#227'o'
                Width = 203
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_LOC_CEN'
                Title.Caption = 'Local (Centro)'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PGT'
                Title.Caption = 'Tipo de Produto'
                Width = 83
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoQtde'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Sigla'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Produto'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tam.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCodi'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCtrl'
                Title.Caption = 'Controle'
                Width = 56
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPrdMov
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Width = 109
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TIPO'
                Title.Caption = 'A'#231#227'o'
                Width = 203
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_LOC_CEN'
                Title.Caption = 'Local (Centro)'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PGT'
                Title.Caption = 'Tipo de Produto'
                Width = 83
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoQtde'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Sigla'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Produto'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tam.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCodi'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCtrl'
                Title.Caption = 'Controle'
                Width = 56
                Visible = True
              end>
          end
        end
        object PainelConfirma: TPanel
          Left = 0
          Top = 234
          Width = 1000
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object LaAviso3A: TLabel
            Left = 468
            Top = 8
            Width = 13
            Height = 13
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BtPesquisa1: TBitBtn
            Tag = 18
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Pequisa'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtPesquisa1Click
          end
          object Panel2: TPanel
            Left = 889
            Top = 0
            Width = 111
            Height = 48
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida1: TBitBtn
              Tag = 13
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaida1Click
            end
          end
          object PB1: TProgressBar
            Left = 468
            Top = 24
            Width = 413
            Height = 17
            TabOrder = 2
          end
          object BtImprime1: TBitBtn
            Tag = 5
            Left = 112
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Imprime'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtImprime1Click
          end
          object BtLocaliza1: TBitBtn
            Tag = 22
            Left = 204
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Localiza'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtLocaliza1Click
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Estoque final em... '
        ImageIndex = 2
        object Panel11: TPanel
          Left = 0
          Top = 234
          Width = 1000
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object LaAviso2A: TLabel
            Left = 300
            Top = 8
            Width = 13
            Height = 13
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BtPesq2: TBitBtn
            Tag = 18
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Pequisa'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtPesq2Click
          end
          object Panel12: TPanel
            Left = 889
            Top = 0
            Width = 111
            Height = 48
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn2: TBitBtn
              Tag = 13
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaida1Click
            end
          end
          object PB2: TProgressBar
            Left = 300
            Top = 24
            Width = 485
            Height = 17
            TabOrder = 2
          end
          object BtImprime2: TBitBtn
            Tag = 5
            Left = 112
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Imprime'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtImprime2Click
          end
          object BtExporta2: TBitBtn
            Tag = 5
            Left = 204
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Exporta'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtExporta2Click
          end
          object BitBtn3: TBitBtn
            Left = 790
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'Compara'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = BitBtn3Click
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 234
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object PageControl3: TPageControl
            Left = 0
            Top = 0
            Width = 1000
            Height = 234
            ActivePage = TabSheet9
            Align = alClient
            TabOrder = 0
            object TabSheet9: TTabSheet
              Caption = ' Resultado da pesquisa '
              object dmkDBGrid1: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 206
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsSMIC2
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
              end
            end
            object TabSheet10: TTabSheet
              Caption = ' Texto de Exporta'#231#227'o '
              ImageIndex = 1
              object MeExp: TMemo
                Left = 0
                Top = 0
                Width = 992
                Height = 206
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                WordWrap = False
                ExplicitHeight = 260
              end
            end
            object TabSheet11: TTabSheet
              Caption = ' Falhas na exporta'#231#227'o '
              ImageIndex = 2
              object dmkDBGrid4: TdmkDBGrid
                Left = 0
                Top = 49
                Width = 578
                Height = 157
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QTDE'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsExport2
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QTDE'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
              end
              object DBGrid2: TDBGrid
                Left = 578
                Top = 49
                Width = 414
                Height = 157
                Align = alRight
                DataSource = DsListErr1
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Texto1'
                    Width = 300
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Width = 76
                    Visible = True
                  end>
              end
              object Panel22: TPanel
                Left = 0
                Top = 0
                Width = 992
                Height = 49
                Align = alTop
                ParentBackground = False
                TabOrder = 2
                object BitBtn1: TBitBtn
                  Left = 6
                  Top = 4
                  Width = 131
                  Height = 40
                  Cursor = crHandPoint
                  Caption = 'Verifica pre'#231'o m'#233'dio'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BitBtn1Click
                end
              end
            end
            object TabSheet13: TTabSheet
              Caption = ' Campos exporta'#231#227'o '
              ImageIndex = 3
              object DBGrid3: TDBGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 206
                Align = alClient
                DataSource = DsLayout001
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DataInvent'
                    Title.Caption = 'Dta.Invent.'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MesAnoInic'
                    Title.Caption = 'Mes I.'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MesAnoInic'
                    Title.Caption = 'Mes F.'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CodigoProd'
                    Title.Caption = 'Codigo do Produto'
                    Width = 128
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SituacProd'
                    Title.Caption = 'S'
                    Width = 14
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CNPJTercei'
                    Title.Caption = 'CNPJ Terceiro'
                    Width = 92
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'IETerceiro'
                    Title.Caption = 'I.E. Terceiro'
                    Width = 128
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'UFTerceiro'
                    Title.Caption = 'UF Terceiro'
                    Width = 20
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Quantidade'
                    Width = 134
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorUnita'
                    Title.Caption = 'Valor unit'#225'rio'
                    Width = 110
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTotal'
                    Title.Caption = 'Valor Total'
                    Width = 110
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMSRecupe'
                    Title.Caption = 'ICMS a recuperar'
                    Width = 110
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observacao'
                    Title.Caption = 'Observa'#231#227'o'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DescriProd'
                    Title.Caption = 'Descri'#231#227'o do produto'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GrupoProdu'
                    Title.Caption = 'Grupo'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ClassifNCM'
                    Title.Caption = 'Classif. NCM'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'RESERVADOS'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'UnidMedida'
                    Title.Caption = 'Unidade'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DescrGrupo'
                    Title.Caption = 'Descri'#231#227'o do Grupo'
                    Width = 248
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' Lan'#231'amentos '
        ImageIndex = 3
        object Panel15: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 282
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel16: TPanel
            Left = 0
            Top = 234
            Width = 1000
            Height = 48
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label8: TLabel
              Left = 208
              Top = 8
              Width = 13
              Height = 13
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object BtPesquisa3: TBitBtn
              Tag = 18
              Left = 16
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Pequisa'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtPesquisa3Click
            end
            object Panel17: TPanel
              Left = 889
              Top = 0
              Width = 111
              Height = 48
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BitBtn4: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaida1Click
              end
            end
            object ProgressBar1: TProgressBar
              Left = 208
              Top = 24
              Width = 672
              Height = 17
              TabOrder = 2
            end
            object BtImprime3: TBitBtn
              Tag = 5
              Left = 112
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Imprime'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 3
              OnClick = BtImprime3Click
            end
          end
          object dmkDBGrid2: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 234
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_VarFatId'
                Title.Caption = 'Forma'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data/Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PGT'
                Title.Caption = 'Tipo de Produto'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BAIXA_TXT'
                Title.Caption = 'Baixa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Sigla'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Produto'
                Width = 223
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tam.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDCtrl'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCnta'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCodi'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCtrl'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriPart'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Title.Caption = 'Forma'
                Width = 40
                Visible = True
              end>
            Color = clWindow
            DataSource = DsSMIA3
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_VarFatId'
                Title.Caption = 'Forma'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data/Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PGT'
                Title.Caption = 'Tipo de Produto'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BAIXA_TXT'
                Title.Caption = 'Baixa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Sigla'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Produto'
                Width = 223
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tam.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Peso'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDCtrl'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCnta'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCodi'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriCtrl'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OriPart'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Title.Caption = 'Forma'
                Width = 40
                Visible = True
              end>
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Movimento'
        ImageIndex = 4
        object Panel36: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 282
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
        end
      end
      object TabSheet15: TTabSheet
        Caption = ' Panorama MP '
        ImageIndex = 5
        object Panel24: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 282
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          OnResize = Panel24Resize
          ExplicitHeight = 336
          object Splitter1: TSplitter
            Left = 0
            Top = 125
            Width = 1000
            Height = 5
            Cursor = crVSplit
            Align = alBottom
            ExplicitTop = 177
            ExplicitWidth = 998
          end
          object Label15: TLabel
            Left = 0
            Top = 0
            Width = 77
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Itens Entrada'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Panel25: TPanel
            Left = 0
            Top = 234
            Width = 1000
            Height = 48
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitTop = 288
            object LaAviso5A: TLabel
              Left = 364
              Top = 4
              Width = 13
              Height = 13
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object BtPesquisa5: TBitBtn
              Tag = 18
              Left = 20
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Pequisa'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtPesquisa5Click
            end
            object Panel26: TPanel
              Left = 889
              Top = 0
              Width = 111
              Height = 48
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtSaida5: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaida1Click
              end
            end
            object PB5: TProgressBar
              Left = 364
              Top = 20
              Width = 461
              Height = 17
              TabOrder = 2
            end
            object BtImprime5: TBitBtn
              Tag = 5
              Left = 112
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Imprime'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 3
              OnClick = BtImprime5Click
            end
            object BtCorrige5: TBitBtn
              Left = 204
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Corrige'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 4
              OnClick = BtCorrige5Click
            end
          end
          object DBG5Entrada: TdmkDBGrid
            Left = 0
            Top = 13
            Width = 1000
            Height = 112
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'OriCtrl'
                Title.Caption = 'OS'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDCtrl'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Nome'
                Width = 144
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_Qtde'
                Title.Caption = 'Qtd entrou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_Pecas'
                Title.Caption = 'P'#199' entrou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_Peso'
                Title.Caption = 'kg entrou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_MEDIA'
                Title.Caption = 'M. Qtde/P'#199
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bx_Qtde'
                Title.Caption = 'Qtd Baixa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bx_Pecas'
                Title.Caption = 'P'#199' baixa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bx_Peso'
                Title.Caption = 'kg Baixa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Er_Qtde'
                Title.Caption = 'Qtd erro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Er_Pecas'
                Title.Caption = 'P'#199' erro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Er_Peso'
                Title.Caption = 'kg erro'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsSmiaMPIn
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'OriCtrl'
                Title.Caption = 'OS'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDCtrl'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Nome'
                Width = 144
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_Qtde'
                Title.Caption = 'Qtd entrou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_Pecas'
                Title.Caption = 'P'#199' entrou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_Peso'
                Title.Caption = 'kg entrou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MP_MEDIA'
                Title.Caption = 'M. Qtde/P'#199
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bx_Qtde'
                Title.Caption = 'Qtd Baixa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bx_Pecas'
                Title.Caption = 'P'#199' baixa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bx_Peso'
                Title.Caption = 'kg Baixa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Er_Qtde'
                Title.Caption = 'Qtd erro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Er_Pecas'
                Title.Caption = 'P'#199' erro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Er_Peso'
                Title.Caption = 'kg erro'
                Visible = True
              end>
          end
          object Panel27: TPanel
            Left = 0
            Top = 130
            Width = 1000
            Height = 104
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 2
            ExplicitTop = 184
            object Panel28: TPanel
              Left = 652
              Top = 0
              Width = 652
              Height = 104
              Align = alLeft
              BevelOuter = bvNone
              Caption = 'Panel28'
              TabOrder = 0
              object Label11: TLabel
                Left = 0
                Top = 0
                Width = 80
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Itens Gerados'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBG5Gerados: TDBGrid
                Left = 0
                Top = 13
                Width = 652
                Height = 91
                Align = alClient
                DataSource = DsSMPIGer
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'IDCtrl'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / Hora'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 50
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Nome'
                    Width = 144
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Peso'
                    Visible = True
                  end>
              end
            end
            object Panel29: TPanel
              Left = 0
              Top = 0
              Width = 652
              Height = 104
              Align = alLeft
              BevelOuter = bvNone
              Caption = 'Panel28'
              TabOrder = 1
              object Label12: TLabel
                Left = 0
                Top = 0
                Width = 84
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Itens Baixados'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBG5Baixas: TDBGrid
                Left = 0
                Top = 13
                Width = 652
                Height = 91
                Align = alClient
                DataSource = DsSMPIBxa
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'IDCtrl'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / Hora'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 50
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Nome'
                    Width = 144
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Peso'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object TabSheet16: TTabSheet
        Caption = ' Compara'#231#227'o de "Estoque" e "Estoque em..." '
        ImageIndex = 6
        object Panel18: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 282
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object dmkDBGrid3: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 282
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PGT'
                Title.Caption = 'Tipo de Produto'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTDE'
                Title.Caption = 'Quantidade'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Sigla'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Produto'
                Width = 223
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tam.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PECAS'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AREAM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AREAP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PESO'
                Title.Caption = 'Peso kg'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsCPR
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PGT'
                Title.Caption = 'Tipo de Produto'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTDE'
                Title.Caption = 'Quantidade'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA'
                Title.Caption = 'Sigla'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD'
                Title.Caption = 'Produto'
                Width = 223
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tam.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PECAS'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AREAM2'
                Title.Caption = #193'rea m'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AREAP2'
                Title.Caption = #193'rea ft'#178
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PESO'
                Title.Caption = 'Peso kg'
                Visible = True
              end>
          end
        end
      end
      object TabSheet19: TTabSheet
        Caption = ' Reposi'#231#227'o '
        ImageIndex = 7
        object Panel39: TPanel
          Left = 0
          Top = 234
          Width = 1000
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object LaAviso07: TLabel
            Left = 208
            Top = 8
            Width = 13
            Height = 13
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = LaAviso07Click
          end
          object BtPesquisa7: TBitBtn
            Tag = 18
            Left = 16
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Pequisa'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtPesquisa7Click
          end
          object Panel40: TPanel
            Left = 889
            Top = 0
            Width = 111
            Height = 48
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn7: TBitBtn
              Tag = 13
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaida1Click
            end
          end
          object ProgressBar2: TProgressBar
            Left = 208
            Top = 24
            Width = 672
            Height = 17
            TabOrder = 2
          end
          object BtImprime7: TBitBtn
            Tag = 5
            Left = 112
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Imprime'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtImprime7Click
          end
        end
        object DBGReposicao7: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1000
          Height = 234
          Align = alClient
          DataSource = DsReposicao7
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 54
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLA'
              Title.Caption = 'Unidade'
              Width = 54
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Estoque'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 73
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD'
              Title.Caption = 'Produto'
              Width = 223
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PGT'
              Title.Caption = 'Grupo'
              Width = 100
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 394
        Height = 32
        Caption = 'Impress'#227'o de Produtos por Lista'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 394
        Height = 32
        Caption = 'Impress'#227'o de Produtos por Lista'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 394
        Height = 32
        Caption = 'Impress'#227'o de Produtos por Lista'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 668
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel37: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 859
        Top = 0
        Width = 145
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida0: TBitBtn
          Tag = 13
          Left = 10
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaida1Click
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 270
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object BtImprime0: TBitBtn
          Tag = 5
          Left = 144
          Top = 3
          Width = 120
          Height = 40
          Caption = '&Imprime'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtImprime0Click
        end
        object BtPesquisa0: TBitBtn
          Tag = 18
          Left = 20
          Top = 3
          Width = 120
          Height = 40
          Caption = '&Pequisa'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtPesquisa0Click
        end
      end
      object GBAvisos1: TGroupBox
        Left = 270
        Top = 0
        Width = 589
        Height = 47
        Align = alClient
        Caption = ' Avisos: '
        TabOrder = 2
        object Panel21: TPanel
          Left = 2
          Top = 15
          Width = 585
          Height = 30
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1B: TLabel
            Left = 13
            Top = 2
            Width = 120
            Height = 16
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso1A: TLabel
            Left = 12
            Top = 1
            Width = 120
            Height = 16
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object PB0: TProgressBar
            Left = 0
            Top = 13
            Width = 585
            Height = 17
            Align = alBottom
            TabOrder = 0
          end
        end
      end
    end
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPrdGrupTipAfterScroll
    SQL.Strings = (
      'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, '
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'pgt.TitNiv1, pgt.TitNiv2, pgt.TitNiv3,'
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO,'
      'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') '
      'NOME_TIPPRD, pgt.ImpedeCad, pgt.PerComissF, pgt.PerComissR'
      'FROM prdgruptip pgt'
      'ORDER BY pgt.Nome'
      '')
    Left = 140
    Top = 132
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrPrdGrupTipTipPrd: TSmallintField
      FieldName = 'TipPrd'
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
    end
    object QrPrdGrupTipFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
    end
    object QrPrdGrupTipFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrPrdGrupTipNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrPrdGrupTipNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrPrdGrupTipNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
    object QrPrdGrupTipImpedeCad: TSmallintField
      FieldName = 'ImpedeCad'
    end
    object QrPrdGrupTipPerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrPrdGrupTipPerComissR: TFloatField
      FieldName = 'PerComissR'
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 168
    Top = 132
  end
  object QrStqMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,'
      'smia.IDCtrl, smia.Tipo, smia.OriCodi,'
      'smia.OriCtrl, smia.OriCnta, smia.OriPart,'
      'smia.Empresa, smia.StqCenCad, smia.GraGruX,'
      'smia.Pecas, smia.Peso, smia.AreaM2,'
      'smia.AreaP2, smia.Qtde, smia.FatorClas,'
      'smia.Ativo'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora'
      '')
    Left = 140
    Top = 244
  end
  object DsStqMovIts: TDataSource
    DataSet = QrStqMovIts
    Left = 168
    Top = 244
  end
  object QrPrdMov: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPrdMovAfterOpen
    BeforeClose = QrPrdMovBeforeClose
    OnCalcFields = QrPrdMovCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM PrdMov'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, GraGruX, DataHora')
    Left = 140
    Top = 272
    object QrPrdMovNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
    end
    object QrPrdMovNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = 'gragru1.Nome'
      Size = 511
    end
    object QrPrdMovPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrPrdMovUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
    end
    object QrPrdMovNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = 'prdgruptip.Nome'
      Size = 30
    end
    object QrPrdMovSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrPrdMovNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrPrdMovNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrPrdMovGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrPrdMovGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = 'gragrux.GraGruC'
    end
    object QrPrdMovGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
    end
    object QrPrdMovGraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = 'gragrux.GraTamI'
    end
    object QrPrdMovDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'stqmovitsa.DataHora'
    end
    object QrPrdMovIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovitsa.IDCtrl'
    end
    object QrPrdMovTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqmovitsa.Tipo'
    end
    object QrPrdMovOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovitsa.OriCodi'
    end
    object QrPrdMovOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'stqmovitsa.OriCtrl'
    end
    object QrPrdMovOriCnta: TIntegerField
      FieldName = 'OriCnta'
      Origin = 'stqmovitsa.OriCnta'
    end
    object QrPrdMovOriPart: TIntegerField
      FieldName = 'OriPart'
      Origin = 'stqmovitsa.OriPart'
    end
    object QrPrdMovEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovitsa.Empresa'
    end
    object QrPrdMovStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'stqmovitsa.StqCenCad'
    end
    object QrPrdMovGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'stqmovitsa.GraGruX'
    end
    object QrPrdMovQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'stqmovitsa.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPrdMovPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'stqmovitsa.Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPrdMovPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'stqmovitsa.Peso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPrdMovAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovitsa.Ativo'
    end
    object QrPrdMovAreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = 'stqmovitsa.AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrdMovAreaP2: TFloatField
      FieldName = 'AreaP2'
      Origin = 'stqmovitsa.AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrdMovFatorClas: TFloatField
      FieldName = 'FatorClas'
      Origin = 'stqmovitsa.FatorClas'
    end
    object QrPrdMovSdoPecas: TFloatField
      FieldName = 'SdoPecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrPrdMovSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPrdMovSdoAreaM2: TFloatField
      FieldName = 'SdoAreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrdMovSdoAreaP2: TFloatField
      FieldName = 'SdoAreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrdMovSdoQtde: TFloatField
      FieldName = 'SdoQtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrPrdMovNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 50
      Calculated = True
    end
    object QrPrdMovLoteMP: TWideStringField
      FieldName = 'LoteMP'
    end
    object QrPrdMovNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrPrdMovStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
  object DsPrdMov: TDataSource
    DataSet = QrPrdMov
    Left = 168
    Top = 272
  end
  object frxPRD_PRINT_001_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //  '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 36
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPrdMov
        DataSetName = 'frxDsPrdMov'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 120.944952680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 370.393940000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'HIST'#211'RICO DE ESTOQUE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 665.196938270000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PERIODO: [PERIODO_TXT]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 219.212598430000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 60.472480000000000000
          Width = 219.212598425196900000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Top = 109.606370000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 109.606370000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 109.606370000000000000
          Width = 22.677165350000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 109.606370000000000000
          Width = 139.842512360000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Opera'#231#227'o')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 109.606370000000000000
          Width = 49.133870470000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 109.606370000000000000
          Width = 83.149628270000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DataHora')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 109.606370000000000000
          Width = 52.913400470000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lan'#231'amento')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 109.606370000000000000
          Width = 151.181102360000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local (Centro)')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 60.472480000000000000
          Width = 219.212598430000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Centro: [VFR_NO_CEN]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.566936460000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 11.338590000000000000
          Width = 313.700990000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPrdMov
        DataSetName = 'frxDsPrdMov'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataField = 'NO_TAM'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdMov."NO_TAM"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'NO_TAM'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdMov."NO_TAM"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Width = 22.677165350000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdMov."SIGLA"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataField = 'Qtde'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdMov."Qtde"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataField = 'SdoQtde'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdMov."SdoQtde"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Width = 143.622140000000000000
          Height = 11.338582680000000000
          DataField = 'NO_TIPO'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdMov."NO_TIPO"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 49.133870470000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdMov."GraGruX"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 83.149628270000000000
          Height = 11.338582680000000000
          DataField = 'DataHora'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdMov."DataHora"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913400470000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdMov."IDCtrl"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 151.181200000000000000
          Height = 11.338582680000000000
          DataField = 'NO_LOC_CEN'
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdMov."NO_LOC_CEN"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdMov."PrdGrupTip"'
        object frxDsPrdMovPrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdMov."PrdGrupTip">)]')
          ParentFont = False
        end
        object frxDsPrdMovNO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 801.260360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdMov."NO_PGT"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de produto: ')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338580240000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdMov."GraGru1"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdMov."GraGru1">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 805.039890000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdMov."NO_PRD"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdMov
          DataSetName = 'frxDsPrdMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448816460000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsPrdMov: TfrxDBDataset
    UserName = 'frxDsPrdMov'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'DataHora=DataHora'
      'IDCtrl=IDCtrl'
      'Tipo=Tipo'
      'OriCodi=OriCodi'
      'OriCtrl=OriCtrl'
      'OriCnta=OriCnta'
      'OriPart=OriPart'
      'Empresa=Empresa'
      'StqCenCad=StqCenCad'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'Pecas=Pecas'
      'Peso=Peso'
      'Ativo=Ativo'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'FatorClas=FatorClas'
      'SdoPecas=SdoPecas'
      'SdoPeso=SdoPeso'
      'SdoAreaM2=SdoAreaM2'
      'SdoAreaP2=SdoAreaP2'
      'SdoQtde=SdoQtde'
      'NO_TIPO=NO_TIPO'
      'LoteMP=LoteMP'
      'NO_LOC_CEN=NO_LOC_CEN'
      'StqCenLoc=StqCenLoc')
    DataSet = QrPrdMov
    BCDToCurrency = False
    DataSetOptions = []
    Left = 196
    Top = 272
  end
  object QrPrdEstq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPrdEstqAfterOpen
    BeforeClose = QrPrdEstqBeforeClose
    AfterScroll = QrPrdEstqAfterScroll
    OnCalcFields = QrPrdEstqCalcFields
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,'
      'SUM(smia.Qtde) QTDE, SUM(smia.Pecas) PECAS, '
      'SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2, '
      'SUM(smia.AreaP2) AREAP2, smia.GraGruX, gg1.NCM'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Ativo=1'
      'AND smia.Empresa=-11'
      'AND gg1.PrdGrupTip=2'
      'GROUP BY smia.GraGruX'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX'
      '')
    Left = 140
    Top = 300
    object QrPrdEstqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 511
    end
    object QrPrdEstqNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPrdEstqNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrPrdEstqPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrPrdEstqUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrPrdEstqNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrPrdEstqSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrPrdEstqNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrPrdEstqNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrPrdEstqGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrPrdEstqGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrPrdEstqGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrPrdEstqGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrPrdEstqQTDE: TFloatField
      FieldName = 'QTDE'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPrdEstqPECAS: TFloatField
      FieldName = 'PECAS'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrPrdEstqPESO: TFloatField
      FieldName = 'PESO'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPrdEstqAREAM2: TFloatField
      FieldName = 'AREAM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPrdEstqAREAP2: TFloatField
      FieldName = 'AREAP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPrdEstqGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPrdEstqNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrPrdEstqPrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPrdEstqValorTot: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ValorTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
    object QrPrdEstqNO_SCC: TWideStringField
      FieldName = 'NO_SCC'
      Size = 30
    end
    object QrPrdEstqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsPrdEstq: TDataSource
    DataSet = QrPrdEstq
    Left = 168
    Top = 300
  end
  object frxPRD_PRINT_001_00E_1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 288
    Top = 100
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 136.063072680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DO MOMENTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[DATAHORA_TXT]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Top = 124.724490000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 124.724490000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Top = 124.724490000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 124.724490000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 124.724490000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 124.724490000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 124.724490000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 124.724490000000000000
          Width = 154.960637240000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Pre'#231'os: [VFR_LISTA_PRECO]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 124.724490000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 124.724490000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 124.724490000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 487.559370000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582680000000000
        ParentFont = False
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataField = 'NO_TAM'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataField = 'NO_COR'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."QTDE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'Pecas'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'AreaM2'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'Peso'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 154.960637240000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'PrcCusUni'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."PrcCusUni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'ValorTot'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."ValorTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdEstq."PrdGrupTip"'
        object frxDsPrdEstqPrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637795275590000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdEstq."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsPrdEstqNO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 18.897637795275590000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdEstq."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637795275590000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118112680000000000
        ParentFont = False
        Top = 332.598640000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 294.803149606299200000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPrdEstq."QTDE">)=0,0,SUM(<frxDsPrdEstq."ValorTot"' +
              '>) / SUM(<frxDsPrdEstq."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdEstq."Nivel1"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdEstq."Nivel1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118112680000000000
        ParentFont = False
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 294.803149606299200000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsPrdEstq."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPrdEstq."QTDE">)=0,0,SUM(<frxDsPrdEstq."ValorTot"' +
              '>) / SUM(<frxDsPrdEstq."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897642680000000000
        Top = 445.984540000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 7.559059999999988000
          Width = 64.251968500000000000
          Height = 11.338582677165350000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 7.559059999999988000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 7.559059999999988000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 7.559059999999988000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559059999999988000
          Width = 294.803149610000000000
          Height = 11.338582677165350000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL PESQUISA')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 7.559059999999988000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 7.559059999999988000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPrdEstq."QTDE">)=0,0,SUM(<frxDsPrdEstq."ValorTot"' +
              '>) / SUM(<frxDsPrdEstq."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPrdEstq: TfrxDBDataset
    UserName = 'frxDsPrdEstq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'QTDE=QTDE'
      'PECAS=PECAS'
      'PESO=PESO'
      'AREAM2=AREAM2'
      'AREAP2=AREAP2'
      'GraGruX=GraGruX'
      'NCM=NCM'
      'PrcCusUni=PrcCusUni'
      'ValorTot=ValorTot'
      'NO_SCC=NO_SCC'
      'CodUsu=CodUsu')
    DataSet = QrPrdEstq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 196
    Top = 300
  end
  object QrGGX2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Ativo = 1'
      'AND pgt.Codigo=:P0')
    Left = 140
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGX2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSCC2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM stqcencad')
    Left = 168
    Top = 328
    object QrSCC2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSMIA2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(DataHora) Ini,'
      'MAX(DataHora) Fim,'
      'COUNT(IDCtrl) Itens '
      'FROM stqmovitsa'
      'WHERE Empresa=:P0'
      'AND GraGruX=:P1'
      'AND StqCenCad=:P2'
      'AND DataHora < :P3')
    Left = 196
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSMIA2Ini: TDateTimeField
      FieldName = 'Ini'
    end
    object QrSMIA2Fim: TDateTimeField
      FieldName = 'Fim'
    end
    object QrSMIA2Itens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrSumA2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO GGX_SCC_Stq'
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,'
      'SUM(smia.Qtde) QTDE, SUM(smia.Pecas) PECAS, '
      'SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2, '
      'SUM(smia.AreaP2) AREAP2, smia.GraGruX, smia.StqCenCad, gg1.NCM'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Empresa=:P0'
      'AND smia.GraGruX=:P1'
      'AND smia.StqCenCad=:P2'
      'AND smia.DataHora < :P3'
      'GROUP BY smia.GraGruX')
    Left = 112
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSumA2Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrSumA2NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrSumA2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrSumA2UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSumA2NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrSumA2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSumA2NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSumA2NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSumA2GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSumA2GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSumA2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSumA2GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSumA2QTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrSumA2PECAS: TFloatField
      FieldName = 'PECAS'
    end
    object QrSumA2PESO: TFloatField
      FieldName = 'PESO'
    end
    object QrSumA2AREAM2: TFloatField
      FieldName = 'AREAM2'
    end
    object QrSumA2AREAP2: TFloatField
      FieldName = 'AREAP2'
    end
    object QrSumA2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSumA2NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object QrGSS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ggx_scc_stq')
    Left = 140
    Top = 400
    object QrGSSNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGSSNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrGSSPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGSSUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGSSNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrGSSSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrGSSNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGSSNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGSSGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrGSSGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGSSGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGSSGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGSSQTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrGSSPECAS: TFloatField
      FieldName = 'PECAS'
    end
    object QrGSSPESO: TFloatField
      FieldName = 'PESO'
    end
    object QrGSSAREAM2: TFloatField
      FieldName = 'AREAM2'
    end
    object QrGSSAREAP2: TFloatField
      FieldName = 'AREAP2'
    end
    object QrGSSGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGSSStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrGSSNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object DsGSS: TDataSource
    DataSet = QrGSS
    Left = 168
    Top = 400
  end
  object frxDsGSS: TfrxDBDataset
    UserName = 'frxDsGSS'
    CloseDataSource = False
    DataSet = QrGSS
    BCDToCurrency = False
    DataSetOptions = []
    Left = 196
    Top = 400
  end
  object QrSMIA3: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSMIA3AfterOpen
    BeforeClose = QrSMIA3BeforeClose
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,'
      'smia.IDCtrl, smia.Tipo, smia.OriCodi,'
      'smia.OriCtrl, smia.OriCnta, smia.OriPart,'
      'smia.Empresa, smia.StqCenCad, smia.GraGruX,'
      'smia.Pecas, smia.Peso, smia.AreaM2,'
      'smia.AreaP2, smia.Qtde, smia.Baixa, smia.FatorClas,'
      'smia.Ativo, smia.DataHora, '
      'ELT(smia.Baixa+1,"Nulo","Adiciona","Subtrai") BAIXA_TXT'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'ORDER BY DataHora DESC')
    Left = 84
    Top = 452
    object QrSMIA3Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrSMIA3NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrSMIA3PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrSMIA3UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSMIA3NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrSMIA3SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSMIA3NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSMIA3NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSMIA3GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSMIA3GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSMIA3GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSMIA3GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSMIA3IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMIA3Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSMIA3OriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrSMIA3OriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrSMIA3OriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrSMIA3OriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrSMIA3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSMIA3StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrSMIA3GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMIA3Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSMIA3Peso: TFloatField
      FieldName = 'Peso'
    end
    object QrSMIA3AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSMIA3AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSMIA3Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSMIA3FatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrSMIA3Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSMIA3DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSMIA3Baixa: TSmallintField
      FieldName = 'Baixa'
    end
    object QrSMIA3BAIXA_TXT: TWideStringField
      DisplayWidth = 20
      FieldName = 'BAIXA_TXT'
    end
    object QrSMIA3NO_VarFatId: TWideStringField
      FieldName = 'NO_VarFatId'
      Size = 255
    end
  end
  object DsSMIA3: TDataSource
    DataSet = QrSMIA3
    Left = 112
    Top = 452
  end
  object frxPRD_PRINT_001_03: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 92
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSMIA3
        DataSetName = 'frxDsSMIA3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 120.189047170000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LAN'#199'AMENTOS NO ESTOQUE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[DATAHORA_TXT]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 109.606370000000000000
          Width = 52.913385830000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 109.606370000000000000
          Width = 98.267716540000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 109.606370000000000000
          Width = 37.795275590000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Top = 109.606370000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 109.606370000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 109.606370000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 109.606370000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 109.606370000000000000
          Width = 177.637890470000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 109.606370000000000000
          Width = 71.811023622047240000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data/hora')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 10.582677170000000000
        ParentFont = False
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSMIA3
        DataSetName = 'frxDsSMIA3'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 52.913385830000000000
          Height = 10.582677170000000000
          DataField = 'NO_TAM'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIA3."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Width = 98.267716540000000000
          Height = 10.582677170000000000
          DataField = 'NO_COR'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIA3."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 37.795275590000000000
          Height = 10.582677170000000000
          DataField = 'SIGLA'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIA3."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'QTDE'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIA3."QTDE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'Pecas'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIA3."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'AreaM2'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIA3."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'Peso'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIA3."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Width = 177.637870940000000000
          Height = 10.582677170000000000
          DataField = 'NO_PRD'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIA3."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 71.811023622047240000
          Height = 10.582677170000000000
          DataField = 'DataHora'
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIA3."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSMIA3."PrdGrupTip"'
        object frxDsSMIA3PrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637795275590000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsSMIA3."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsSMIA3NO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 18.897637795275590000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsSMIA3."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637795275590000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897640240000000000
        ParentFont = False
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952936540000000000
          Height = 15.118110236220470000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsSMIA3."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSMIA3."Nivel1"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsSMIA3."Nivel1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 17.007874020000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsSMIA3."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897640240000000000
        ParentFont = False
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 377.952936540000000000
          Height = 15.118110236220470000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsSMIA3."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677170240000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 7.559060000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIA3."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 377.952936540000000000
          Height = 15.118110236220470000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL PESQUISA')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsSMIA3: TfrxDBDataset
    UserName = 'frxDsSMIA3'
    CloseDataSource = False
    DataSet = QrSMIA3
    BCDToCurrency = False
    DataSetOptions = []
    Left = 140
    Top = 452
  end
  object QrCPR: TMySQLQuery
    Database = Dmod.MyDB
    Filtered = True
    SQL.Strings = (
      'SELECT * '
      'FROM ggx_scc_cpr'
      'WHERE QTDE <> 0')
    Left = 196
    Top = 452
    object QrCPRNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrCPRNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrCPRPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrCPRUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrCPRNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrCPRSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrCPRNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrCPRNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrCPRGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrCPRGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrCPRGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrCPRGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrCPRQTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrCPRPECAS: TFloatField
      FieldName = 'PECAS'
    end
    object QrCPRPESO: TFloatField
      FieldName = 'PESO'
    end
    object QrCPRAREAM2: TFloatField
      FieldName = 'AREAM2'
    end
    object QrCPRAREAP2: TFloatField
      FieldName = 'AREAP2'
    end
    object QrCPRGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCPRNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object DsCPR: TDataSource
    DataSet = QrCPR
    Left = 224
    Top = 452
  end
  object QrGG2: TMySQLQuery
    Database = Dmod.MyDB
    Filtered = True
    SQL.Strings = (
      'DELETE FROM ggx_scc_cpr;'
      'INSERT INTO ggx_scc_cpr'
      'SELECT Nivel1, NO_PRD, PrdGrupTip, UnidMed, NO_PGT, SIGLA,'
      'NO_TAM, NO_COR, GraCorCad,  GraGruC, GraGru1, GraTamI, '
      'SUM(QTDE) QTDE, SUM(PECAS) PECAS,  SUM(PESO) PESO,'
      'SUM(AREAM2) AREAM2,  SUM(AREAP2) AREAP2,'
      'GraGruX, StqCenCad, NCM '
      'FROM ggx_scc_stq'
      'GROUP BY GraGruX')
    Left = 168
    Top = 452
    object IntegerField1: TIntegerField
      FieldName = 'Nivel1'
    end
    object StringField1: TWideStringField
      FieldName = 'NO_PRD'
      Size = 30
    end
    object IntegerField2: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object IntegerField3: TIntegerField
      FieldName = 'UnidMed'
    end
    object StringField2: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object StringField3: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object StringField4: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object StringField5: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object IntegerField4: TIntegerField
      FieldName = 'GraCorCad'
    end
    object IntegerField5: TIntegerField
      FieldName = 'GraGruC'
    end
    object IntegerField6: TIntegerField
      FieldName = 'GraGru1'
    end
    object IntegerField7: TIntegerField
      FieldName = 'GraTamI'
    end
    object FloatField1: TFloatField
      FieldName = 'QTDE'
    end
    object FloatField2: TFloatField
      FieldName = 'PECAS'
    end
    object FloatField3: TFloatField
      FieldName = 'PESO'
    end
    object FloatField4: TFloatField
      FieldName = 'AREAM2'
    end
    object FloatField5: TFloatField
      FieldName = 'AREAP2'
    end
    object IntegerField8: TIntegerField
      FieldName = 'GraGruX'
    end
    object StringField6: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT scc.Codigo, scc.EntiSitio, scc.SitProd,'
      'ent.CNPJ, ent.IE, ufs.Nome NO_UF'
      'FROM stqcencad scc'
      'LEFT JOIN entidades ent ON ent.Codigo=scc.EntiSitio'
      'LEFT JOIN ufs ufs ON ufs.Codigo=ent.EUF'
      'WHERE scc.Codigo > 0')
    Left = 224
    Top = 400
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrStqCenCadSitProd: TSmallintField
      FieldName = 'SitProd'
    end
    object QrStqCenCadCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrStqCenCadIE: TWideStringField
      FieldName = 'IE'
    end
    object QrStqCenCadNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT gcp.Codigo, gcp.CodUsu, gcp.Nome,'
      'ELT(CustoPreco+1, "Custo", "Pre'#231'o") CusPrc,'
      
        'ELT(TipoCalc+1, "Manual", "Pre'#231'o m'#233'dio", "'#218'ltima compra") NomeTC' +
        ','
      'mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA'
      'FROM gracusprc gcp '
      'LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda')
    Left = 712
    Top = 8
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraCusPrcCusPrc: TWideStringField
      FieldName = 'CusPrc'
      Size = 5
    end
    object QrGraCusPrcNomeTC: TWideStringField
      FieldName = 'NomeTC'
      Size = 13
    end
    object QrGraCusPrcNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrGraCusPrcSIGLAMOEDA: TWideStringField
      FieldName = 'SIGLAMOEDA'
      Size = 5
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 740
    Top = 8
  end
  object QrPGT_SCC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad, pgt.LstPrcFisc, '
      
        'FLOOR(smic.Empresa+0.1) Empresa, FLOOR(scc.EntiSitio+0.1) EntiSi' +
        'tio'
      'FROM stqmovitsc smic'
      'LEFT JOIN BlueDerm.gragrux ggx ON ggx.Controle=smic.GraGruX'
      'LEFT JOIN BlueDerm.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN BlueDerm.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN BlueDerm.stqcencad scc ON scc.Codigo=smic.StqCenCad'
      'WHERE (smic.Empresa=-11 OR scc.EntiSitio=-11)'
      'AND GraGruX <> 0'
      'ORDER BY gg1.PrdGrupTip, smic.StqCenCad')
    Left = 800
    Top = 4
    object QrPGT_SCCPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrPGT_SCCStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrPGT_SCCLstPrcFisc: TSmallintField
      FieldName = 'LstPrcFisc'
    end
    object QrPGT_SCCEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPGT_SCCEntiSitio: TLargeintField
      FieldName = 'EntiSitio'
    end
  end
  object QrAbertura2a: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Abertura, GrupoBal'
      'FROM stqbalcad'
      'WHERE Empresa=-11'
      'AND PrdGrupTip=-2'
      'AND StqCenCad=1'
      'AND Abertura < "2009-08-01"'
      'ORDER BY Abertura DESC'
      'LIMIT 1')
    Left = 856
    Top = 4
    object QrAbertura2aAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrAbertura2aGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
  end
  object QrSMIC2: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSMIC2AfterOpen
    BeforeClose = QrSMIC2BeforeClose
    SQL.Strings = (
      'SELECT sm2.*, '
      'ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,'
      'emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF'
      'FROM _smic2_ sm2'
      'LEFT JOIN bluederm.entidades ens ON ens.Codigo=sm2.EntiSitio'
      'LEFT JOIN bluederm.entidades emp ON emp.Codigo=sm2.Empresa'
      'LEFT JOIN bluederm.ufs uf1 ON uf1.Codigo=ens.EUF'
      'LEFT JOIN bluederm.ufs uf2 ON uf2.Codigo=emp.EUF'
      'ORDER BY NO_PGT, NO_PRD')
    Left = 912
    Top = 4
    object QrSMIC2Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = '_smic2_.Nivel1'
    end
    object QrSMIC2NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = '_smic2_.NO_PRD'
      Size = 511
    end
    object QrSMIC2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = '_smic2_.PrdGrupTip'
    end
    object QrSMIC2UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = '_smic2_.UnidMed'
    end
    object QrSMIC2NCM: TWideStringField
      FieldName = 'NCM'
      Origin = '_smic2_.NCM'
      Size = 10
    end
    object QrSMIC2NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = '_smic2_.NO_PGT'
      Size = 30
    end
    object QrSMIC2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = '_smic2_.SIGLA'
      Size = 6
    end
    object QrSMIC2NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = '_smic2_.NO_TAM'
      Size = 5
    end
    object QrSMIC2NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = '_smic2_.NO_COR'
      Size = 30
    end
    object QrSMIC2GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = '_smic2_.GraCorCad'
    end
    object QrSMIC2GraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = '_smic2_.GraGruC'
    end
    object QrSMIC2GraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = '_smic2_.GraGru1'
    end
    object QrSMIC2GraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = '_smic2_.GraTamI'
    end
    object QrSMIC2Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_smic2_.Empresa'
    end
    object QrSMIC2StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = '_smic2_.StqCenCad'
    end
    object QrSMIC2Qtde: TFloatField
      FieldName = 'Qtde'
      Origin = '_smic2_.QTDE'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrSMIC2SitProd: TSmallintField
      FieldName = 'SitProd'
      Origin = '_smic2_.SitProd'
    end
    object QrSMIC2ENS_CNPJ: TWideStringField
      FieldName = 'ENS_CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrSMIC2ENS_IE: TWideStringField
      FieldName = 'ENS_IE'
      Origin = 'entidades.IE'
    end
    object QrSMIC2ENS_NO_UF: TWideStringField
      FieldName = 'ENS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrSMIC2EMP_CNPJ: TWideStringField
      FieldName = 'EMP_CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrSMIC2EMP_IE: TWideStringField
      FieldName = 'EMP_IE'
      Origin = 'entidades.IE'
    end
    object QrSMIC2EMP_NO_UF: TWideStringField
      FieldName = 'EMP_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrSMIC2PECAS: TFloatField
      FieldName = 'PECAS'
      Origin = '_smic2_.PECAS'
      DisplayFormat = '#,###,###,##0.0'
    end
    object QrSMIC2PESO: TFloatField
      FieldName = 'PESO'
      Origin = '_smic2_.PESO'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrSMIC2AREAM2: TFloatField
      FieldName = 'AREAM2'
      Origin = '_smic2_.AREAM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC2AREAP2: TFloatField
      FieldName = 'AREAP2'
      Origin = '_smic2_.AREAP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC2PrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
      Origin = '_smic2_.PrcCusUni'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSMIC2GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = '_smic2_.GraGruX'
    end
    object QrSMIC2ValorTot: TFloatField
      FieldName = 'ValorTot'
      Origin = '_smic2_.ValorTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC2My_Idx: TIntegerField
      FieldName = 'My_Idx'
      Origin = '_smic2_.My_Idx'
    end
    object QrSMIC2PrintTam: TSmallintField
      FieldName = 'PrintTam'
      Origin = '_smic2_.PrintTam'
    end
    object QrSMIC2PrintCor: TSmallintField
      FieldName = 'PrintCor'
      Origin = '_smic2_.PrintCor'
    end
    object QrSMIC2EntiSitio: TIntegerField
      FieldName = 'EntiSitio'
      Origin = '_smic2_.EntiSitio'
    end
    object QrSMIC2Exportado: TLargeintField
      FieldName = 'Exportado'
      Origin = '_smic2_.Exportado'
    end
    object QrSMIC2TPC_GCP_ID: TFloatField
      FieldName = 'TPC_GCP_ID'
      Origin = '_smic2_.TPC_GCP_ID'
    end
    object QrSMIC2TPC_GCP_TAB: TIntegerField
      FieldName = 'TPC_GCP_TAB'
      Origin = '_smic2_.TPC_GCP_TAB'
    end
  end
  object DsSMIC2: TDataSource
    DataSet = QrSMIC2
    Left = 940
    Top = 4
  end
  object QrAbertura2b: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Abertura'
      'FROM stqbalcad'
      'WHERE Empresa=-11'
      'AND PrdGrupTip=-2'
      'AND StqCenCad=1'
      'AND Abertura >= "2009-08-01"'
      'ORDER BY Abertura '
      'LIMIT 1')
    Left = 884
    Top = 4
    object QrAbertura2bAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
  end
  object DsPGT_SCC: TDataSource
    DataSet = QrPGT_SCC
    Left = 828
    Top = 4
  end
  object frxPRD_PRINT_001_02_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 64
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDs
        DataSetName = 'frxDsSMIC2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 117.165422680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE FINAL DO DIA [DATA_EM_TXT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 105.826840000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 105.826840000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Top = 105.826840000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 105.826840000000000000
          Width = 124.724397240000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 105.826840000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 105.826840000000000000
          Width = 37.795207240000000000
          Height = 11.338582680000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582680000000000
        ParentFont = False
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        DataSet = frxDs
        DataSetName = 'frxDsSMIC2'
        RowCount = 0
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'NO_TAM'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIC2."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          DataField = 'NO_COR'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIC2."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIC2."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'Qtde'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIC2."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'PECAS'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIC2."PECAS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'AREAM2'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIC2."AREAM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'PESO'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIC2."PESO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 124.724397240000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMIC2."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'PrcCusUni'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIC2."PrcCusUni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataField = 'ValorTot'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIC2."ValorTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795207240000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMIC2."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 196.535560000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSMIC2."PrdGrupTip"'
        object frxDsSMIC2PrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsSMIC2."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsSMIC2NO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsSMIC2."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 294.803149606299000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsSMIC2."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSMIC2."QTDE">)=0,0,SUM(<frxDsSMIC2."ValorTot">) /' +
              ' SUM(<frxDsSMIC2."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSMIC2."Nivel1"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsSMIC2."Nivel1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsSMIC2."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 351.496290000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 294.803149606299000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsSMIC2."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSMIC2."QTDE">)=0,0,SUM(<frxDsSMIC2."ValorTot">) /' +
              ' SUM(<frxDsSMIC2."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897642680000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 7.559060000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 7.559060000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."Pecas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 7.559060000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."AreaM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 7.559060000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 294.803149606299000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL PESQUISA')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 7.559060000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSMIC2."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 7.559060000000000000
          Width = 64.251968500000000000
          Height = 11.338582680000000000
          DataSet = frxDs
          DataSetName = 'frxDsSMIC2'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSMIC2."QTDE">)=0,0,SUM(<frxDsSMIC2."ValorTot">) /' +
              ' SUM(<frxDsSMIC2."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDs: TfrxDBDataset
    UserName = 'frxDsSMIC2'
    CloseDataSource = False
    DataSet = QrSMIC2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 968
    Top = 4
  end
  object QrExport2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrExport2BeforeClose
    AfterScroll = QrExport2AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM _smic2_'
      'WHERE Exportado=0'
      'ORDER BY NO_PGT, NO_PRD')
    Left = 336
    Top = 384
    object QrExport2My_Idx: TIntegerField
      FieldName = 'My_Idx'
      Origin = '_smic2_.My_Idx'
    end
    object QrExport2Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = '_smic2_.Nivel1'
    end
    object QrExport2NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = '_smic2_.NO_PRD'
      Size = 511
    end
    object QrExport2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = '_smic2_.PrdGrupTip'
    end
    object QrExport2UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = '_smic2_.UnidMed'
    end
    object QrExport2NCM: TWideStringField
      FieldName = 'NCM'
      Origin = '_smic2_.NCM'
      Size = 10
    end
    object QrExport2NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = '_smic2_.NO_PGT'
      Size = 30
    end
    object QrExport2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = '_smic2_.SIGLA'
      Size = 6
    end
    object QrExport2PrintTam: TSmallintField
      FieldName = 'PrintTam'
      Origin = '_smic2_.PrintTam'
    end
    object QrExport2NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = '_smic2_.NO_TAM'
      Size = 5
    end
    object QrExport2PrintCor: TSmallintField
      FieldName = 'PrintCor'
      Origin = '_smic2_.PrintCor'
    end
    object QrExport2NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = '_smic2_.NO_COR'
      Size = 30
    end
    object QrExport2GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = '_smic2_.GraCorCad'
    end
    object QrExport2GraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = '_smic2_.GraGruC'
    end
    object QrExport2GraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = '_smic2_.GraGru1'
    end
    object QrExport2GraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = '_smic2_.GraTamI'
    end
    object QrExport2SitProd: TSmallintField
      FieldName = 'SitProd'
      Origin = '_smic2_.SitProd'
    end
    object QrExport2EntiSitio: TIntegerField
      FieldName = 'EntiSitio'
      Origin = '_smic2_.EntiSitio'
    end
    object QrExport2GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = '_smic2_.GraGruX'
    end
    object QrExport2Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_smic2_.Empresa'
    end
    object QrExport2StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = '_smic2_.StqCenCad'
    end
    object QrExport2QTDE: TFloatField
      FieldName = 'QTDE'
      Origin = '_smic2_.QTDE'
    end
    object QrExport2PECAS: TFloatField
      FieldName = 'PECAS'
      Origin = '_smic2_.PECAS'
    end
    object QrExport2PESO: TFloatField
      FieldName = 'PESO'
      Origin = '_smic2_.PESO'
    end
    object QrExport2AREAM2: TFloatField
      FieldName = 'AREAM2'
      Origin = '_smic2_.AREAM2'
    end
    object QrExport2AREAP2: TFloatField
      FieldName = 'AREAP2'
      Origin = '_smic2_.AREAP2'
    end
    object QrExport2PrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
      Origin = '_smic2_.PrcCusUni'
    end
    object QrExport2ValorTot: TFloatField
      FieldName = 'ValorTot'
      Origin = '_smic2_.ValorTot'
    end
    object QrExport2Exportado: TLargeintField
      FieldName = 'Exportado'
      Origin = '_smic2_.Exportado'
    end
    object QrExport2TPC_GCP_ID: TFloatField
      FieldName = 'TPC_GCP_ID'
      Origin = '_smic2_.TPC_GCP_ID'
    end
    object QrExport2TPC_GCP_TAB: TIntegerField
      FieldName = 'TPC_GCP_TAB'
      Origin = '_smic2_.TPC_GCP_TAB'
    end
  end
  object DsExport2: TDataSource
    DataSet = QrExport2
    Left = 364
    Top = 384
  end
  object QrListErr1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM listerr1'
      'WHERE My_Idx=:P0')
    Left = 420
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrListErr1My_Idx: TIntegerField
      FieldName = 'My_Idx'
    end
    object QrListErr1Codigo: TWideStringField
      FieldName = 'Codigo'
    end
    object QrListErr1Texto1: TWideStringField
      FieldName = 'Texto1'
      Size = 255
    end
  end
  object DsListErr1: TDataSource
    DataSet = QrListErr1
    Left = 448
    Top = 384
  end
  object QrLayout001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM Layout001')
    Left = 504
    Top = 384
    object QrLayout001DataInvent: TWideStringField
      FieldName = 'DataInvent'
      Size = 8
    end
    object QrLayout001MesAnoInic: TWideStringField
      FieldName = 'MesAnoInic'
      Size = 4
    end
    object QrLayout001MesAnoFina: TWideStringField
      FieldName = 'MesAnoFina'
      Size = 4
    end
    object QrLayout001CodigoProd: TWideStringField
      FieldName = 'CodigoProd'
    end
    object QrLayout001SituacProd: TWideStringField
      FieldName = 'SituacProd'
      Size = 1
    end
    object QrLayout001CNPJTercei: TWideStringField
      FieldName = 'CNPJTercei'
      Size = 14
    end
    object QrLayout001IETerceiro: TWideStringField
      FieldName = 'IETerceiro'
    end
    object QrLayout001UFTerceiro: TWideStringField
      FieldName = 'UFTerceiro'
      Size = 2
    end
    object QrLayout001Quantidade: TWideStringField
      FieldName = 'Quantidade'
      Size = 21
    end
    object QrLayout001ValorUnita: TWideStringField
      FieldName = 'ValorUnita'
      Size = 17
    end
    object QrLayout001ValorTotal: TWideStringField
      FieldName = 'ValorTotal'
      Size = 17
    end
    object QrLayout001ICMSRecupe: TWideStringField
      FieldName = 'ICMSRecupe'
      Size = 17
    end
    object QrLayout001Observacao: TWideStringField
      FieldName = 'Observacao'
      Size = 60
    end
    object QrLayout001DescriProd: TWideStringField
      FieldName = 'DescriProd'
      Size = 80
    end
    object QrLayout001GrupoProdu: TWideStringField
      FieldName = 'GrupoProdu'
      Size = 4
    end
    object QrLayout001ClassifNCM: TWideStringField
      FieldName = 'ClassifNCM'
      Size = 10
    end
    object QrLayout001RESERVADOS: TWideStringField
      FieldName = 'RESERVADOS'
      Size = 30
    end
    object QrLayout001UnidMedida: TWideStringField
      FieldName = 'UnidMedida'
      Size = 3
    end
    object QrLayout001DescrGrupo: TWideStringField
      FieldName = 'DescrGrupo'
      Size = 30
    end
  end
  object DsLayout001: TDataSource
    DataSet = QrLayout001
    Left = 532
    Top = 384
  end
  object frxPRD_PRINT_001_04: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 92
    Top = 36
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExporta2
        DataSetName = 'frxDsExporta2'
      end
      item
        DataSet = frxDsListErr1
        DataSetName = 'frxDsListErr1'
      end
      item
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 96.377952760000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'FALHAS NA EXPORTA'#199#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 85.039370078740160000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 85.039370078740160000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Top = 85.039370078740160000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 85.039370078740160000
          Width = 162.519697240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 85.039370078740160000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 85.039370078740160000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 85.039370078740160000
          Width = 272.126123390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Texto')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 434.645950000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582680000000000
        ParentFont = False
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        DataSet = frxDsExporta2
        DataSetName = 'frxDsExporta2'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'NO_TAM'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          DataField = 'NO_COR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Width = 162.519697240000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 272.126142910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExporta2."PrdGrupTip"'
        object frxDsPrdEstqPrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsExporta2."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsPrdEstqNO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsExporta2."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 328.819110000000000000
        Width = 680.315400000000000000
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExporta2."Nivel1"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsExporta2."Nivel1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsExporta2."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 351.496290000000000000
        Width = 680.315400000000000000
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 411.968770000000000000
        Width = 680.315400000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = frxDsListErr1
        DataSetName = 'frxDsListErr1'
        RowCount = 0
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 272.126142910000000000
          Height = 11.338582680000000000
          DataField = 'Texto1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListErr1."Texto1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          DataField = 'Codigo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListErr1."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 162.519790000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 268.346630000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Width = 162.519697240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsExporta2: TfrxDBDataset
    UserName = 'frxDsExporta2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'My_Idx=My_Idx'
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NCM=NCM'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'PrintTam=PrintTam'
      'NO_TAM=NO_TAM'
      'PrintCor=PrintCor'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'SitProd=SitProd'
      'EntiSitio=EntiSitio'
      'GraGruX=GraGruX'
      'Empresa=Empresa'
      'StqCenCad=StqCenCad'
      'QTDE=QTDE'
      'PECAS=PECAS'
      'PESO=PESO'
      'AREAM2=AREAM2'
      'AREAP2=AREAP2'
      'GraCusPrc=GraCusPrc'
      'PrcCusUni=PrcCusUni'
      'ValorTot=ValorTot'
      'Exportado=Exportado')
    DataSet = QrExport2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 392
    Top = 384
  end
  object frxDsListErr1: TfrxDBDataset
    UserName = 'frxDsListErr1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'My_Idx=My_Idx'
      'Codigo=Codigo'
      'Texto1=Texto1')
    DataSet = QrListErr1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 476
    Top = 384
  end
  object QrSemCusUni: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _smic2_'
      'WHERE Exportado=0'
      'AND (PrcCusUni IS NULL OR PrcCusUni = 0)'
      'ORDER BY NO_PGT, NO_PRD')
    Left = 336
    Top = 412
    object QrSemCusUniMy_Idx: TIntegerField
      FieldName = 'My_Idx'
    end
    object QrSemCusUniNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSemCusUniNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrSemCusUniPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrSemCusUniUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSemCusUniNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrSemCusUniNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrSemCusUniSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSemCusUniPrintTam: TSmallintField
      FieldName = 'PrintTam'
    end
    object QrSemCusUniNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSemCusUniPrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
    object QrSemCusUniNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSemCusUniGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSemCusUniGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSemCusUniGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSemCusUniGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSemCusUniSitProd: TSmallintField
      FieldName = 'SitProd'
    end
    object QrSemCusUniEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrSemCusUniGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSemCusUniEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSemCusUniStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrSemCusUniQTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrSemCusUniPECAS: TFloatField
      FieldName = 'PECAS'
    end
    object QrSemCusUniPESO: TFloatField
      FieldName = 'PESO'
    end
    object QrSemCusUniAREAM2: TFloatField
      FieldName = 'AREAM2'
    end
    object QrSemCusUniAREAP2: TFloatField
      FieldName = 'AREAP2'
    end
    object QrSemCusUniPrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
    end
    object QrSemCusUniValorTot: TFloatField
      FieldName = 'ValorTot'
    end
    object QrSemCusUniExportado: TLargeintField
      FieldName = 'Exportado'
    end
    object QrSemCusUniTPC_GCP_ID: TFloatField
      FieldName = 'TPC_GCP_ID'
    end
    object QrSemCusUniTPC_GCP_TAB: TIntegerField
      FieldName = 'TPC_GCP_TAB'
    end
  end
  object QrEntrada: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Qtde, CustoAll'
      'FROM stqmovitsa'
      'WHERE GraGruX =:P0'
      'AND DataHora <:P1'
      'AND Qtde > 0'
      'AND CustoAll > 0'
      'ORDER BY DataHora DESC')
    Left = 336
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntradaQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEntradaCustoAll: TFloatField
      FieldName = 'CustoAll'
    end
  end
  object QrLocLote: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mpi.Lote'
      'FROM stqmovitsa smia'
      'LEFT JOIN mpin mpi ON mpi.Controle=smia.OriCodi'
      'WHERE smia.OriCodi=:P0')
    Left = 116
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocLoteLote: TWideStringField
      FieldName = 'Lote'
      Size = 11
    end
  end
  object QrSMPI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM smiampin')
    Left = 620
    Top = 432
    object QrSMPIDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'smiampin.DataHora'
    end
    object QrSMPIIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'smiampin.IDCtrl'
    end
    object QrSMPITipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'smiampin.Tipo'
    end
    object QrSMPIOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'smiampin.OriCodi'
    end
    object QrSMPIOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'smiampin.OriCtrl'
    end
    object QrSMPIGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'smiampin.GraGruX'
    end
    object QrSMPIMP_Qtde: TFloatField
      FieldName = 'MP_Qtde'
      Origin = 'smiampin.MP_Qtde'
    end
    object QrSMPIMP_Pecas: TFloatField
      FieldName = 'MP_Pecas'
      Origin = 'smiampin.MP_Pecas'
    end
    object QrSMPIMP_Peso: TFloatField
      FieldName = 'MP_Peso'
      Origin = 'smiampin.MP_Peso'
    end
    object QrSMPIBx_Qtde: TFloatField
      FieldName = 'Bx_Qtde'
      Origin = 'smiampin.Bx_Qtde'
    end
    object QrSMPIBx_Pecas: TFloatField
      FieldName = 'Bx_Pecas'
      Origin = 'smiampin.Bx_Pecas'
    end
    object QrSMPIBx_Peso: TFloatField
      FieldName = 'Bx_Peso'
      Origin = 'smiampin.Bx_Peso'
    end
    object QrSMPIEr_Qtde: TFloatField
      FieldName = 'Er_Qtde'
      Origin = 'smiampin.Er_Qtde'
    end
    object QrSMPIEr_Pecas: TFloatField
      FieldName = 'Er_Pecas'
      Origin = 'smiampin.Er_Pecas'
    end
    object QrSMPIEr_Peso: TFloatField
      FieldName = 'Er_Peso'
      Origin = 'smiampin.Er_Peso'
    end
    object QrSMPIParCodi: TIntegerField
      FieldName = 'ParCodi'
      Origin = 'smiampin.ParCodi'
    end
  end
  object QrSumBx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde) Qtde, '
      'SUM(smia.Pecas) Pecas, SUM(smia.Peso) Peso'
      'FROM stqmovitsa smia'
      'WHERE smia.Tipo=104'
      'AND smia.OriCodi=:P0'
      'AND smia.GraGruX=:P1'
      '')
    Left = 648
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumBxQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumBxPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumBxPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object QrSumEr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde) Qtde, '
      'SUM(smia.Pecas) Pecas, SUM(smia.Peso) Peso'
      'FROM stqmovitsa smia'
      'WHERE smia.Tipo=104'
      'AND smia.OriCodi=:P0'
      'AND smia.GraGruX<>:P1'
      '')
    Left = 676
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumErQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumErPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumErPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object DsSmiaMPIn: TDataSource
    DataSet = QrSmiaMPIn
    Left = 648
    Top = 460
  end
  object QrSmiaMPIn: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSmiaMPInAfterOpen
    BeforeClose = QrSmiaMPInBeforeClose
    AfterScroll = QrSmiaMPInAfterScroll
    SQL.Strings = (
      'SELECT gg1.Nome NO_PRD, med.Sigla SIGLA, ggx.GraGru1, '
      'smia.MP_Qtde/smia.MP_Pecas MP_MEDIA, smia.* '
      'FROM locbderm__1.smiampin smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'ORDER BY DataHora, NO_PRD, smia.GraGruX ')
    Left = 620
    Top = 460
    object QrSmiaMPInNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = 'gragru1.Nome'
      Size = 511
    end
    object QrSmiaMPInSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrSmiaMPInGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
    end
    object QrSmiaMPInDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'smiampin.DataHora'
      Required = True
    end
    object QrSmiaMPInIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'smiampin.IDCtrl'
      Required = True
    end
    object QrSmiaMPInTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'smiampin.Tipo'
      Required = True
    end
    object QrSmiaMPInOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'smiampin.OriCodi'
      Required = True
    end
    object QrSmiaMPInOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'smiampin.OriCtrl'
      Required = True
    end
    object QrSmiaMPInGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'smiampin.GraGruX'
      Required = True
    end
    object QrSmiaMPInMP_Qtde: TFloatField
      FieldName = 'MP_Qtde'
      Origin = 'smiampin.MP_Qtde'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInMP_Pecas: TFloatField
      FieldName = 'MP_Pecas'
      Origin = 'smiampin.MP_Pecas'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInMP_Peso: TFloatField
      FieldName = 'MP_Peso'
      Origin = 'smiampin.MP_Peso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInBx_Qtde: TFloatField
      FieldName = 'Bx_Qtde'
      Origin = 'smiampin.Bx_Qtde'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInBx_Pecas: TFloatField
      FieldName = 'Bx_Pecas'
      Origin = 'smiampin.Bx_Pecas'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInBx_Peso: TFloatField
      FieldName = 'Bx_Peso'
      Origin = 'smiampin.Bx_Peso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInEr_Qtde: TFloatField
      FieldName = 'Er_Qtde'
      Origin = 'smiampin.Er_Qtde'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInEr_Pecas: TFloatField
      FieldName = 'Er_Pecas'
      Origin = 'smiampin.Er_Pecas'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInEr_Peso: TFloatField
      FieldName = 'Er_Peso'
      Origin = 'smiampin.Er_Peso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSmiaMPInParCodi: TIntegerField
      FieldName = 'ParCodi'
      Origin = 'smiampin.ParCodi'
      Required = True
    end
    object QrSmiaMPInMP_MEDIA: TFloatField
      FieldName = 'MP_MEDIA'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object QrSMPIBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_PRD, med.Sigla SIGLA,'
      'smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Tipo=104'
      'AND smia.OriCodi=:P0')
    Left = 620
    Top = 488
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSMPIBxaNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrSMPIBxaSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSMPIBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSMPIBxaIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMPIBxaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMPIBxaQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIBxaPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIBxaTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSMPIBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSMPIBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSMPIBxaStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
  end
  object DsSMPIBxa: TDataSource
    DataSet = QrSMPIBxa
    Left = 648
    Top = 488
  end
  object QrSMPIGer: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_PRD, med.Sigla SIGLA,'
      'smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Tipo=101'
      'AND smia.OriCodi=:P0')
    Left = 620
    Top = 516
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSMPIGerNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrSMPIGerSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSMPIGerDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSMPIGerIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMPIGerGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMPIGerQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIGerPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIGerPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMPIGerTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSMPIGerAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSMPIGerAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSMPIGerStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
  end
  object DsSMPIGer: TDataSource
    DataSet = QrSMPIGer
    Left = 648
    Top = 516
  end
  object PMCorrige5: TPopupMenu
    OnPopup = PMCorrige5Popup
    Left = 240
    Top = 496
    object CorrigeReduzidodoitensbaixados1: TMenuItem
      Caption = 
        'Corrige &Reduzido dos itens baixados de TODAS entradas listadas ' +
        'na pesquisa'
      OnClick = CorrigeReduzidodoitensbaixados1Click
    end
    object Alteraitembaixadoatual1: TMenuItem
      Caption = 'Altera item &Baixado selecionado'
      OnClick = Alteraitembaixadoatual1Click
    end
    object AlteraitemClassificadoougeradaselelcionado1: TMenuItem
      Caption = 'Altera item &Gerado selelcionado'
      OnClick = AlteraitemClassificadoougeradaselelcionado1Click
    end
  end
  object frxPRD_PRINT_001_05: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 148
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSmiaMPIn
        DataSetName = 'frxDsSmiaMPIn'
      end
      item
        DataSet = frxDsSMPIBxa
        DataSetName = 'frxDsSMPIBxa'
      end
      item
        DataSet = frxDsSMPIGer
        DataSetName = 'frxDsSMPIGer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 94.488242680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PANORAMA DA MAT'#201'RIA-PRIMA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo: [PERIODO_TXT_5]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Filtro: [VFR_FILTRO_5]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'OS')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236166770000000000
          Top = 83.149660000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IDCtrl')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031442360000000000
          Top = 83.149660000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724355750000000000
          Top = 83.149660000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960571340000000000
          Top = 83.149660000000000000
          Width = 75.590551180000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551102990000000000
          Top = 83.149660000000000000
          Width = 22.677162910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SIGLA')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 253.228265910000000000
          Top = 83.149660000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd entrou')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362131500000000000
          Top = 83.149660000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#199' entrou')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157407090000000000
          Top = 83.149660000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg entrou')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291272680000000000
          Top = 83.149660000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M. Qtde/P'#199)
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086548270000000000
          Top = 83.149660000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd Baixa')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220413860000000000
          Top = 83.149660000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#199' baixa')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 514.062770000000000000
          Top = 83.149660000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg Baixa')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149555040000000000
          Top = 83.149660000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd erro')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 600.944830630000000000
          Top = 83.149660000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#199' erro')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740106220000000000
          Top = 83.149660000000000000
          Width = 41.574803150000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg erro')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118112680000000000
        ParentFont = False
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSmiaMPIn
        DataSetName = 'frxDsSmiaMPIn'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataField = 'OriCtrl'
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSmiaMPIn."OriCtrl"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 30.236166770000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'IDCtrl'
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSmiaMPIn."IDCtrl"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031442360000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataField = 'DataHora'
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsSmiaMPIn."DataHora"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724355750000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSmiaMPIn."GraGruX"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960571340000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSmiaMPIn."NO_PRD"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551102990000000000
          Top = 3.779530000000000000
          Width = 22.677162910000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSmiaMPIn."SIGLA"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 253.228265910000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSmi' +
              'aMPIn."MP_Qtde">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362131500000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.0;-#,###,###,##0.0; '#39',<frxDsSmiaMPI' +
              'n."MP_Pecas">)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157407090000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSmi' +
              'aMPIn."MP_Peso">)]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291272680000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsSmiaM' +
              'PIn."MP_MEDIA">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086548270000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSmi' +
              'aMPIn."Bx_Qtde">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220413860000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.0;-#,###,###,##0.0; '#39',<frxDsSmiaMPI' +
              'n."Bx_Pecas">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 514.062770000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSmi' +
              'aMPIn."Bx_Peso">)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149555040000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSmi' +
              'aMPIn."Er_Qtde">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 600.944830630000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.0;-#,###,###,##0.0; '#39',<frxDsSmiaMPI' +
              'n."Er_Pecas">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740106220000000000
          Top = 3.779530000000000000
          Width = 41.574803150000000000
          Height = 11.338582680000000000
          DataSet = frxDsSmiaMPIn
          DataSetName = 'frxDsSmiaMPIn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSmi' +
              'aMPIn."Er_Peso">)]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSMPIBxa
        DataSetName = 'frxDsSMPIBxa'
        RowCount = 0
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataField = 'IDCtrl'
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMPIBxa."IDCtrl"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141732280000000000
          Width = 60.472440940000000000
          Height = 11.338582680000000000
          DataField = 'DataHora'
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMPIBxa."DataHora"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614239130000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMPIBxa."GraGruX"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409465910000000000
          Width = 113.385826770000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMPIBxa."NO_PRD"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795287800000000000
          Width = 22.677165350000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMPIBxa."SIGLA"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472472680000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSMP' +
              'IBxa."Qtde">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606323620000000000
          Width = 41.574803150000000000
          Height = 11.338582680000000000
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSMP' +
              'IBxa."Pecas">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181119450000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSMP' +
              'IBxa."Peso">)]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSMPIBxa."Tipo"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IDCtrl')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141732280000000000
          Width = 60.472440940000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614239130000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409465910000000000
          Width = 113.385826770000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795287800000000000
          Width = 22.677165350000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SIGLA')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472472680000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606323620000000000
          Width = 41.574803150000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pecas')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181119450000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 101.795300000000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BAIXAS')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 279.685220000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSMPIBxa."Tipo"'
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IDCtrl')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141732280000000000
          Width = 60.472440940000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614239130000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409465910000000000
          Width = 113.385826770000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795287800000000000
          Width = 22.677165350000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SIGLA')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472472680000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606323620000000000
          Width = 41.574803150000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pecas')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181119450000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 101.795300000000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'CLASSIFICADOS E GERADOS')
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 336.378170000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSMPIGer
        DataSetName = 'frxDsSMPIGer'
        RowCount = 0
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataField = 'IDCtrl'
          DataSet = frxDsSMPIGer
          DataSetName = 'frxDsSMPIGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMPIGer."IDCtrl"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141732280000000000
          Width = 60.472440940000000000
          Height = 11.338582680000000000
          DataField = 'DataHora'
          DataSet = frxDsSMPIGer
          DataSetName = 'frxDsSMPIGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMPIGer."DataHora"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614239130000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsSMPIGer
          DataSetName = 'frxDsSMPIGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSMPIGer."GraGruX"]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409465910000000000
          Width = 113.385826770000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          DataSet = frxDsSMPIGer
          DataSetName = 'frxDsSMPIGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMPIGer."NO_PRD"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795287800000000000
          Width = 22.677165350000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDsSMPIGer
          DataSetName = 'frxDsSMPIGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSMPIGer."SIGLA"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472472680000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSMPIGer
          DataSetName = 'frxDsSMPIGer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSMP' +
              'IGer."Qtde">)]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606323620000000000
          Width = 41.574803150000000000
          Height = 11.338582680000000000
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSMP' +
              'IGer."Pecas">)]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181119450000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = frxDsSMPIBxa
          DataSetName = 'frxDsSMPIBxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              ' [FormatFloat('#39'#,###,###,##0.000;-#,###,###,##0.000; '#39',<frxDsSMP' +
              'IGer."Peso">)]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 370.393940000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsSmiaMPIn: TfrxDBDataset
    UserName = 'frxDsSmiaMPIn'
    CloseDataSource = False
    DataSet = QrSmiaMPIn
    BCDToCurrency = False
    DataSetOptions = []
    Left = 652
    Top = 540
  end
  object frxDsSMPIBxa: TfrxDBDataset
    UserName = 'frxDsSMPIBxa'
    CloseDataSource = False
    DataSet = QrSMPIBxa
    BCDToCurrency = False
    DataSetOptions = []
    Left = 676
    Top = 488
  end
  object frxDsSMPIGer: TfrxDBDataset
    UserName = 'frxDsSMPIGer'
    CloseDataSource = False
    DataSet = QrSMPIGer
    BCDToCurrency = False
    DataSetOptions = []
    Left = 676
    Top = 516
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Estq0UsoCons '
      'FROM paramsemp emp'
      'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo'
      'WHERE emp.Codigo=:P0'
      '')
    Left = 244
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpEstq0UsoCons: TSmallintField
      FieldName = 'Estq0UsoCons'
    end
  end
  object QrGraGru3: TMySQLQuery
    Database = Dmod.MyDB
    AfterClose = QrGraGru3AfterClose
    AfterScroll = QrGraGru3AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg1.PrdGrupTip, gg3.Codusu, gg3.Nivel3, '
      'gg3.Nome'
      'FROM gragru1 gg1'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3'
      'WHERE gg1.PrdGrupTip=:P0'
      '/*AND gg3.Nome LIKE :P1*/'
      'ORDER BY gg3.Nome')
    Left = 140
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru3Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru3PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 168
    Top = 160
  end
  object DsGraGru2: TDataSource
    DataSet = QrGraGru2
    Left = 168
    Top = 188
  end
  object QrGraGru2: TMySQLQuery
    Database = Dmod.MyDB
    AfterClose = QrGraGru2AfterClose
    AfterScroll = QrGraGru2AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg2.Codusu, gg2.Nivel3, gg2.Nivel2, '
      'gg2.Nome'
      'FROM gragru2 gg2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg2.Nivel3'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel2=gg2.Nivel2'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg2.Nivel3=:P1'
      '/*AND gg2.Nome LIKE :P2*/'
      'ORDER BY gg2.Nome')
    Left = 140
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGru2Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru2Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    AfterClose = QrGraGru1AfterClose
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,'
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,'
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,'
      'gg1.CST_A,gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,'
      'gg1.IPI_Alq, gg1.IPI_CST, gg1.IPI_cEnq, gg1.TipDimens,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED,'
      'unm.Nome NOMEUNIDMED, gg1.PartePrinc, '
      'gg1.PerCuztMin, gg1.PerCuztMax, gg1.MedOrdem, gg1.FatorClas,'
      'mor.Nome NO_MedOrdem, mor.Medida1, mor.Medida2,'
      'mor.Medida3, mor.Medida4, mpc.Nome NO_PartePrinc,'
      'InfAdProd, SiglaCustm, HowBxaEstq, GerBxaEstq,'
      'PIS_CST, PIS_AlqP, PIS_AlqV, PISST_AlqP, PISST_AlqV,'
      'COFINS_CST, COFINS_AlqP, COFINS_AlqV,COFINSST_AlqP, '
      'COFINSST_AlqV, ICMS_modBC, ICMS_modBCST, ICMS_pRedBC,'
      'ICMS_pRedBCST, ICMS_pMVAST, ICMS_pICMSST,'
      'IPI_pIPI, IPI_TpTrib, IPI_vUnid, EX_TIPI,'
      'ICMS_Pauta, ICMS_MaxTab, cGTIN_EAN,'
      'PIS_pRedBC, PISST_pRedBCST,'
      'COFINS_pRedBC, COFINSST_pRedBCST,'
      ''
      'ICMSRec_pRedBC, IPIRec_pRedBC,'
      'PISRec_pRedBC, COFINSRec_pRedBC,'
      'ICMSRec_pAliq, IPIRec_pAliq,'
      'PISRec_pAliq, COFINSRec_pAliq,'
      'ICMSRec_tCalc, IPIRec_tCalc,'
      'PISRec_tCalc, COFINSRec_tCalc,'
      'ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,'
      'gg1.PerComissF, gg1.PerComissR, gg1.PerComissZ'
      ''
      'FROM gragru1 gg1'
      'LEFT JOIN gragru2   gg2 ON gg1.Nivel2=gg2.Nivel2'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN medordem mor ON mor.Codigo= gg1.MedOrdem'
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=gg1.PartePrinc'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg1.Nivel2=:P1'
      '/*AND gg1.Nome LIKE :P2*/'
      'ORDER BY gg1.Nome')
    Left = 140
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Origin = 'gragru1.CodUsu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Origin = 'gragru1.Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Origin = 'gragru1.Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Origin = 'gragru1.GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Origin = 'gratamcad.Nome'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Origin = 'gratamcad.CodUsu'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Origin = 'gragru1.CST_A'
      Required = True
      DisplayFormat = '0'
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Origin = 'gragru1.CST_B'
      Required = True
      DisplayFormat = '00'
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
      Required = True
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Origin = 'gragru1.Peso'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Origin = 'gragru1.NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Origin = 'unidmed.Sigla'
      Size = 3
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Origin = 'unidmed.Nome'
      Size = 30
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Origin = 'unidmed.CodUsu'
      Required = True
    end
    object QrGraGru1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrGraGru1IPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
      Origin = 'gragru1.IPI_Alq'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Origin = 'gragru1.IPI_cEnq'
      Size = 3
    end
    object QrGraGru1TipDimens: TSmallintField
      FieldName = 'TipDimens'
      Origin = 'gragru1.TipDimens'
    end
    object QrGraGru1PerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
      Origin = 'gragru1.PerCuztMin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGraGru1PerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
      Origin = 'gragru1.PerCuztMax'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGraGru1MedOrdem: TIntegerField
      FieldName = 'MedOrdem'
      Origin = 'gragru1.MedOrdem'
    end
    object QrGraGru1PartePrinc: TIntegerField
      FieldName = 'PartePrinc'
      Origin = 'gragru1.PartePrinc'
    end
    object QrGraGru1NO_MedOrdem: TWideStringField
      FieldName = 'NO_MedOrdem'
      Origin = 'medordem.Nome'
      Size = 50
    end
    object QrGraGru1Medida1: TWideStringField
      FieldName = 'Medida1'
      Origin = 'medordem.Medida1'
    end
    object QrGraGru1Medida2: TWideStringField
      FieldName = 'Medida2'
      Origin = 'medordem.Medida2'
    end
    object QrGraGru1Medida3: TWideStringField
      FieldName = 'Medida3'
      Origin = 'medordem.Medida3'
    end
    object QrGraGru1Medida4: TWideStringField
      FieldName = 'Medida4'
      Origin = 'medordem.Medida4'
    end
    object QrGraGru1NO_PartePrinc: TWideStringField
      FieldName = 'NO_PartePrinc'
      Origin = 'matpartcad.Nome'
      Size = 50
    end
    object QrGraGru1InfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Origin = 'gragru1.InfAdProd'
      Size = 255
    end
    object QrGraGru1SiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Origin = 'gragru1.SiglaCustm'
      Size = 15
    end
    object QrGraGru1HowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
      Origin = 'gragru1.HowBxaEstq'
    end
    object QrGraGru1GerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
      Origin = 'gragru1.GerBxaEstq'
    end
    object QrGraGru1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrGraGru1PIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
      Origin = 'gragru1.PIS_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
      Origin = 'gragru1.PIS_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
      Origin = 'gragru1.PISST_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
      Origin = 'gragru1.PISST_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrGraGru1COFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
      Origin = 'gragru1.COFINS_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
      Origin = 'gragru1.COFINS_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
      Origin = 'gragru1.COFINSST_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
      Origin = 'gragru1.COFINSST_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
      Origin = 'gragru1.ICMS_modBC'
    end
    object QrGraGru1ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
      Origin = 'gragru1.ICMS_modBCST'
    end
    object QrGraGru1ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      Origin = 'gragru1.ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      Origin = 'gragru1.ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      Origin = 'gragru1.ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      Origin = 'gragru1.ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object QrGraGru1IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrGraGru1ICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrGraGru1EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrGraGru1PIS_pRedBC: TFloatField
      FieldName = 'PIS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_pRedBCST: TFloatField
      FieldName = 'PISST_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_pRedBC: TFloatField
      FieldName = 'COFINS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_pRedBCST: TFloatField
      FieldName = 'COFINSST_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_tCalc: TSmallintField
      FieldName = 'ICMSRec_tCalc'
    end
    object QrGraGru1IPIRec_tCalc: TSmallintField
      FieldName = 'IPIRec_tCalc'
    end
    object QrGraGru1PISRec_tCalc: TSmallintField
      FieldName = 'PISRec_tCalc'
    end
    object QrGraGru1COFINSRec_tCalc: TSmallintField
      FieldName = 'COFINSRec_tCalc'
    end
    object QrGraGru1ICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrGraGru1ICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object QrGraGru1FatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGru1PerComissF: TFloatField
      FieldName = 'PerComissF'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraGru1PerComissR: TFloatField
      FieldName = 'PerComissR'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraGru1PerComissZ: TSmallintField
      FieldName = 'PerComissZ'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 168
    Top = 216
  end
  object QrPesqESel_Sel: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqESel_SelAfterOpen
    BeforeClose = QrPesqESel_SelBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM pesqesel_sel')
    Left = 800
    Top = 192
    object QrPesqESel_SelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqESel_SelNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPesqESel_SelAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsPesqESel_Sel: TDataSource
    DataSet = QrPesqESel_Sel
    Left = 828
    Top = 192
  end
  object QrTabePrcCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM tabeprccab'
      'WHERE :P0 BETWEEN DataI AND DataF'
      'AND Aplicacao & 8 <> 0'
      'ORDER BY Nome')
    Left = 884
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTabePrcCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTabePrcCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTabePrcCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsTabePrcCab: TDataSource
    DataSet = QrTabePrcCab
    Left = 912
    Top = 264
  end
  object QrSMIC1: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSMIC2AfterOpen
    BeforeClose = QrSMIC2BeforeClose
    SQL.Strings = (
      'SELECT sm2.*, '
      'ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,'
      'emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF'
      'FROM _smic2_ sm2'
      'LEFT JOIN bluederm.entidades ens ON ens.Codigo=sm2.EntiSitio'
      'LEFT JOIN bluederm.entidades emp ON emp.Codigo=sm2.Empresa'
      'LEFT JOIN bluederm.ufs uf1 ON uf1.Codigo=ens.EUF'
      'LEFT JOIN bluederm.ufs uf2 ON uf2.Codigo=emp.EUF'
      'ORDER BY NO_PGT, NO_PRD')
    Left = 912
    Top = 32
    object QrSMIC1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = '_smic2_.Nivel1'
    end
    object QrSMIC1NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = '_smic2_.NO_PRD'
      Size = 511
    end
    object QrSMIC1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = '_smic2_.PrdGrupTip'
    end
    object QrSMIC1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = '_smic2_.UnidMed'
    end
    object QrSMIC1NCM: TWideStringField
      FieldName = 'NCM'
      Origin = '_smic2_.NCM'
      Size = 10
    end
    object QrSMIC1NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = '_smic2_.NO_PGT'
      Size = 30
    end
    object QrSMIC1SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = '_smic2_.SIGLA'
      Size = 6
    end
    object QrSMIC1NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = '_smic2_.NO_TAM'
      Size = 5
    end
    object QrSMIC1NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = '_smic2_.NO_COR'
      Size = 30
    end
    object QrSMIC1GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = '_smic2_.GraCorCad'
    end
    object QrSMIC1GraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = '_smic2_.GraGruC'
    end
    object QrSMIC1GraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = '_smic2_.GraGru1'
    end
    object QrSMIC1GraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = '_smic2_.GraTamI'
    end
    object QrSMIC1Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_smic2_.Empresa'
    end
    object QrSMIC1StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = '_smic2_.StqCenCad'
    end
    object QrSMIC1Qtde: TFloatField
      FieldName = 'Qtde'
      Origin = '_smic2_.QTDE'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrSMIC1SitProd: TSmallintField
      FieldName = 'SitProd'
      Origin = '_smic2_.SitProd'
    end
    object QrSMIC1ENS_CNPJ: TWideStringField
      FieldName = 'ENS_CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrSMIC1ENS_IE: TWideStringField
      FieldName = 'ENS_IE'
      Origin = 'entidades.IE'
    end
    object QrSMIC1ENS_NO_UF: TWideStringField
      FieldName = 'ENS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrSMIC1EMP_CNPJ: TWideStringField
      FieldName = 'EMP_CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrSMIC1EMP_IE: TWideStringField
      FieldName = 'EMP_IE'
      Origin = 'entidades.IE'
    end
    object QrSMIC1EMP_NO_UF: TWideStringField
      FieldName = 'EMP_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrSMIC1PECAS: TFloatField
      FieldName = 'PECAS'
      Origin = '_smic2_.PECAS'
      DisplayFormat = '#,###,###,##0.0'
    end
    object QrSMIC1PESO: TFloatField
      FieldName = 'PESO'
      Origin = '_smic2_.PESO'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrSMIC1AREAM2: TFloatField
      FieldName = 'AREAM2'
      Origin = '_smic2_.AREAM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC1AREAP2: TFloatField
      FieldName = 'AREAP2'
      Origin = '_smic2_.AREAP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC1PrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
      Origin = '_smic2_.PrcCusUni'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSMIC1GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = '_smic2_.GraGruX'
    end
    object QrSMIC1ValorTot: TFloatField
      FieldName = 'ValorTot'
      Origin = '_smic2_.ValorTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC1My_Idx: TIntegerField
      FieldName = 'My_Idx'
      Origin = '_smic2_.My_Idx'
    end
    object QrSMIC1PrintTam: TSmallintField
      FieldName = 'PrintTam'
      Origin = '_smic2_.PrintTam'
    end
    object QrSMIC1PrintCor: TSmallintField
      FieldName = 'PrintCor'
      Origin = '_smic2_.PrintCor'
    end
    object QrSMIC1EntiSitio: TIntegerField
      FieldName = 'EntiSitio'
      Origin = '_smic2_.EntiSitio'
    end
    object QrSMIC1Exportado: TLargeintField
      FieldName = 'Exportado'
      Origin = '_smic2_.Exportado'
    end
    object QrSMIC1TPC_GCP_ID: TFloatField
      FieldName = 'TPC_GCP_ID'
      Origin = '_smic2_.TPC_GCP_ID'
    end
    object QrSMIC1TPC_GCP_TAB: TIntegerField
      FieldName = 'TPC_GCP_TAB'
      Origin = '_smic2_.TPC_GCP_TAB'
    end
  end
  object DsSMIC1: TDataSource
    DataSet = QrSMIC1
    Left = 940
    Top = 32
  end
  object QrGGXs2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 888
    Top = 164
    object QrGGXs2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrSum1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(QTDE) Qtde, SUM(ValorTot) ValorTot'
      'from _smic1_'
      'WHERE GraGruX=:P0'
      '')
    Left = 836
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum1QTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrSum1ValorTot: TFloatField
      FieldName = 'ValorTot'
    end
  end
  object QrSum2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(QTDE) Qtde, SUM(ValorTot) ValorTot'
      'from _smic2_'
      'WHERE GraGruX=:P0'
      '')
    Left = 864
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum2QTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrSum2ValorTot: TFloatField
      FieldName = 'ValorTot'
    end
  end
  object QrSumM: TMySQLQuery
    Database = Dmod.MyDB
    Left = 892
    Top = 404
    object QrSumMQtdSum: TFloatField
      FieldName = 'QtdSum'
    end
    object QrSumMQtdPos: TFloatField
      FieldName = 'QtdPos'
    end
    object QrSumMQtdNeg: TFloatField
      FieldName = 'QtdNeg'
    end
  end
  object QrSumA: TMySQLQuery
    Database = Dmod.MyDB
    Left = 920
    Top = 404
    object QrSumAQtdSum: TFloatField
      FieldName = 'QtdSum'
    end
    object QrSumAQtdPos: TFloatField
      FieldName = 'QtdPos'
    end
    object QrSumAQtdNeg: TFloatField
      FieldName = 'QtdNeg'
    end
  end
  object QrReduzido: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg2.Nivel2, '
      'gg2.Nome NO_Ni2, gg3.Nivel3, gg3.Nome NO_Ni3,'
      'gg1.PrdGrupTip, pgt.Nome NO_PGT'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN gragru3    gg3 ON gg3.Nivel3=gg2.Nivel3'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 948
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrReduzidoNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrReduzidoNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrReduzidoNivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrReduzidoNO_Ni2: TWideStringField
      FieldName = 'NO_Ni2'
      Size = 30
    end
    object QrReduzidoNivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrReduzidoNO_Ni3: TWideStringField
      FieldName = 'NO_Ni3'
      Size = 30
    end
    object QrReduzidoPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrReduzidoNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
  end
  object QrDiverge2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT Truncate((IniQtd + InnQtd + OutQtd + BalQtd) * 1000, 0) X' +
        ', Truncate(FimQtd * 1000, 0) y,'
      
        'Truncate(FimQtd*1000, 0) - Truncate((IniQtd + InnQtd + OutQtd + ' +
        'BalQtd)*1000, 0) Difer'
      'FROM _gil_mov_ gil'
      
        'WHERE Truncate(IniQtd + InnQtd + OutQtd + BalQtd, 1) <> Truncate' +
        '(FimQtd , 1)')
    Left = 836
    Top = 432
  end
  object frxPRD_PRINT_001_02_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39716.814898726800000000
    ReportOptions.LastChange = 39716.814898726800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_NI2.Visible := <VARF_NI2_VISIBLE>;                         ' +
        '                            '
      '  GF_NI2.Visible := <VARF_NI2_VISIBLE>;'
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 64
    Top = 36
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGIL_Mov
        DataSetName = 'frxDsGIL_Mov'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 332.598640000000000000
        Width = 971.339210000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsGIL_Mov
        DataSetName = 'frxDsGIL_Mov'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866131970000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'InnVal'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."InnVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 196.535462360000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGIL_Mov."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275580790000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'InnQtd'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."InnQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236188740000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'OutQtd'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."OutQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826739920000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'OutVal'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."OutVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905641180000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."IniVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."IniQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'FimQtd'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."FimQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748561180000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'FimVal'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."FimVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'BalQtd'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."BalQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787831180000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'BalVal'
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGIL_Mov."BalVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.110080000000000000
        Top = 207.874150000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsGIL_Mov."PrdGrupTip"'
        object frxDsSMIC2PrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsGIL_Mov."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsSMIC2NO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 778.583180000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsGIL_Mov."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH_NI2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.196660000000000000
        Top = 249.448980000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsGIL_Mov."Nivel2"'
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsGIL_Mov."Nivel2">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 778.583180000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsGIL_Mov."NO_Ni2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Nivel 2: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779527560000000000
        Top = 461.102660000000000000
        Width = 971.339210000000000000
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527559055118000
          Width = 22.677165350000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165350000000000
          Top = 3.779527559055118000
          Width = 173.858257950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsGIL_Mov."NO_Ni2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866131970000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275580790000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236188740000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826739920000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905641180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748561180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787831180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_NI2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779530000000000000
        Top = 415.748300000000000000
        Width = 971.339210000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527559055118000
          Width = 22.677165350000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165350000000000
          Top = 3.779527559055118000
          Width = 173.858257950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsGIL_Mov."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866131970000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275580790000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236188740000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826739920000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905641180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748561180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787831180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 25.559057560000000000
        Top = 544.252320000000000000
        Width = 971.339210000000000000
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527560000000000
          Width = 94.488152360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL  ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866131970000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275580790000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236188740000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826739920000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905641180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748561180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787831180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 126.614244020000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 967.559680000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 702.992580000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE INSUMOS QU'#205'MICOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 971.339210000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PERIODO: [PERIODO_TXT_2]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 60.472480000000000000
          Width = 476.220780000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866131970000000000
          Top = 109.606321180000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 109.606321180000000000
          Width = 196.535418430000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275580790000000000
          Top = 109.606321180000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236188740000000000
          Top = 109.606321180000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826739920000000000
          Top = 109.606321180000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905641180000000000
          Top = 109.606321180000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Anterior R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 109.606321180000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Anterior kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 109.606370000000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748561180000000000
          Top = 109.606370000000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 109.606370000000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ajuste kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787831180000000000
          Top = 109.606370000000000000
          Width = 75.590551180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ajuste R$')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.566936460000000000
        Top = 593.386210000000000000
        Width = 971.339210000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 653.858690000000000000
          Top = 11.338590000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 291.023810000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsGIL_Mov."Nivel1"'
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsGIL_Mov."Nivel1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 778.583180000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsGIL_Mov."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.779530000000000000
        Top = 370.393940000000000000
        Width = 971.339210000000000000
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527559055118000
          Width = 22.677165354330710000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677165354330710000
          Top = 3.779527559055118000
          Width = 173.858257950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsGIL_Mov."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866131970000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275580790000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."InnQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236188740000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 585.826739920000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."OutVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905641180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."IniQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748561180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."FimVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalQtd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787831180000000000
          Top = 3.779527559055118000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsGIL_Mov
          DataSetName = 'frxDsGIL_Mov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGIL_Mov."BalVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrGIL_Mov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _gil_mov_')
    Left = 864
    Top = 432
    object QrGIL_MovEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrGIL_MovGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGIL_MovPrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
    end
    object QrGIL_MovIniQtd: TFloatField
      FieldName = 'IniQtd'
    end
    object QrGIL_MovIniVal: TFloatField
      FieldName = 'IniVal'
    end
    object QrGIL_MovInnQtd: TFloatField
      FieldName = 'InnQtd'
    end
    object QrGIL_MovInnVal: TFloatField
      FieldName = 'InnVal'
    end
    object QrGIL_MovOutQtd: TFloatField
      FieldName = 'OutQtd'
    end
    object QrGIL_MovOutVal: TFloatField
      FieldName = 'OutVal'
    end
    object QrGIL_MovBalQtd: TFloatField
      FieldName = 'BalQtd'
    end
    object QrGIL_MovBalVal: TFloatField
      FieldName = 'BalVal'
    end
    object QrGIL_MovFimQtd: TFloatField
      FieldName = 'FimQtd'
    end
    object QrGIL_MovFimVal: TFloatField
      FieldName = 'FimVal'
    end
    object QrGIL_MovPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGIL_MovNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrGIL_MovNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGIL_MovNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrGIL_MovNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGIL_MovNO_Ni2: TWideStringField
      FieldName = 'NO_Ni2'
      Size = 30
    end
    object QrGIL_MovNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGIL_MovNO_Ni3: TWideStringField
      FieldName = 'NO_Ni3'
      Size = 30
    end
  end
  object frxDsGIL_Mov: TfrxDBDataset
    UserName = 'frxDsGIL_Mov'
    CloseDataSource = False
    DataSet = QrGIL_Mov
    BCDToCurrency = False
    DataSetOptions = []
    Left = 892
    Top = 432
  end
  object frxPRD_PRINT_001_00L: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 196
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 120.944947800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE REDUZIDOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 102.047310000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 102.047310000000000000
          Width = 151.181151180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 102.047310000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Nivel 2: [VFR_NO_NI2]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 302.362204720000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 464.882190000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'NO_TAM'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_TAM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 151.181151180000000000
          Height = 18.897637800000000000
          DataField = 'NO_COR'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_COR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'SIGLA'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."SIGLA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'GraGruX'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."GraGruX"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 302.362204724409400000
          Height = 18.897637800000000000
          DataField = 'NO_PRD'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdEstq."PrdGrupTip"'
        object frxDsPrdEstqPrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdEstq."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsPrdEstqNO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 18.897637795275600000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdEstq."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637795275600000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 7.559052680000000000
        ParentFont = False
        Top = 325.039580000000000000
        Width = 680.315400000000000000
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdEstq."Nivel1"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdEstq."CodUsu">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 7.559052680000000000
        ParentFont = False
        Top = 355.275820000000000000
        Width = 680.315400000000000000
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897642680000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxPRD_PRINT_001_00E_2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 288
    Top = 148
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 137.952836460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DO MOMENTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[DATAHORA_TXT]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 124.724490000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 124.724490000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 124.724490000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 124.724490000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 124.724490000000000000
          Width = 188.976377952755900000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Pre'#231'os: [VFR_LISTA_PRECO]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000100000
          Top = 124.724490000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 124.724490000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 124.724490000000000000
          Width = 45.354330708661410000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 124.724490000000000000
          Width = 98.267728740000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Centro de estoque')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 498.897960000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 13.228346460000000000
        ParentFont = False
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'NO_TAM'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataField = 'NO_COR'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'SIGLA'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."QTDE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 188.976377952755900000
          Height = 13.228346460000000000
          DataField = 'NO_PRD'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000100000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataField = 'PrcCusUni'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."PrcCusUni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataField = 'ValorTot'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."ValorTot"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330708661410000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 98.267728740000000000
          Height = 13.228346460000000000
          DataField = 'NO_SCC'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_SCC"]')
          ParentFont = False
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdEstq."PrdGrupTip"'
        object frxDsPrdEstqPrdGrupTip: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637795275590000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdEstq."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsPrdEstqNO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016028740000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdEstq."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 476.220589610000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000100000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPrdEstq."QTDE">)=0,0,SUM(<frxDsPrdEstq."ValorTot"' +
              '>) / SUM(<frxDsPrdEstq."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrdEstq."Nivel1"'
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsPrdEstq."CodUsu">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Width = 514.016028740000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            ' - [frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118112680000000000
        ParentFont = False
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Width = 476.220589610000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsPrdEstq."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000100000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPrdEstq."QTDE">)=0,0,SUM(<frxDsPrdEstq."ValorTot"' +
              '>) / SUM(<frxDsPrdEstq."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677170240000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559059999999988000
          Width = 476.220589610000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL PESQUISA')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."ValorTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000100000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPrdEstq."QTDE">)=0,0,SUM(<frxDsPrdEstq."ValorTot"' +
              '>) / SUM(<frxDsPrdEstq."QTDE">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxPRD_PRINT_001_00E_3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 288
    Top = 204
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 137.952836460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DO MOMENTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 98.267780000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[DATAHORA_TXT]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Top = 124.724490000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 124.724490000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 124.724490000000000000
          Width = 385.511937950000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Pre'#231'os: [VFR_LISTA_PRECO]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 124.724490000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 124.724490000000000000
          Width = 98.267728740000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Centro de estoque')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 124.724490000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 13.228346460000000000
        ParentFont = False
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPrdEstq
        DataSetName = 'frxDsPrdEstq'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 476.220780000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'SIGLA'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."QTDE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Width = 385.511937950000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'CodUsu'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."CodUsu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Width = 98.267728740000000000
          Height = 13.228346460000000000
          DataField = 'NO_SCC'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrdEstq."NO_SCC"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'Nivel1'
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrdEstq."Nivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677170240000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559059999999988000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrdEstq."QTDE">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559059999999988000
          Width = 612.283669610000000000
          Height = 15.118110240000000000
          DataSet = frxDsPrdEstq
          DataSetName = 'frxDsPrdEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL PESQUISA')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrStqCenCadPsq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 512
    Top = 560
    object QrStqCenCadPsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadPsqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadPsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadPsq: TDataSource
    DataSet = QrStqCenCadPsq
    Left = 540
    Top = 560
  end
  object QrReposicao7: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrReposicao7AfterOpen
    BeforeClose = QrReposicao7BeforeClose
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, '
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, '
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, '
      'SUM((smia.Qtde * smia.Baixa)) Qtde  '
      '/* '
      'smia.IDCtrl, smia.Tipo, smia.OriCodi, '
      'smia.OriCtrl, smia.OriCnta, smia.OriPart, '
      'smia.Empresa, smia.StqCenCad, smia.GraGruX, '
      'smia.Pecas, smia.Peso, smia.AreaM2, '
      'smia.AreaP2,  '
      'SUM((smia.Qtde * smia.Baixa)) Qtde,  '
      'smia.Baixa, smia.FatorClas, '
      'smia.Ativo, smia.DataHora,  '
      'ELT(smia.Baixa+1,"Nulo","Adiciona","Subtrai") BAIXA_TXT  '
      '*/ '
      ' '
      'FROM stqmovitsa smia '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed '
      'WHERE smia.Empresa=-11 '
      
        'AND smia.DataHora  BETWEEN "2021-01-23" AND "2021-04-30 23:59:59' +
        '" '
      'AND Baixa=-1 '
      'GROUP BY smia.GraGruX '
      'ORDER BY NO_PRD ')
    Left = 688
    Top = 168
    object QrReposicao7Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrReposicao7NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrReposicao7PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrReposicao7UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrReposicao7NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 60
    end
    object QrReposicao7SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrReposicao7NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrReposicao7NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrReposicao7GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrReposicao7GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrReposicao7GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrReposicao7GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrReposicao7Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrReposicao7GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrReposicao7Estoque: TFloatField
      FieldKind = fkLookup
      FieldName = 'Estoque'
      LookupDataSet = QrEstoque7
      LookupKeyFields = 'GraGruX'
      LookupResultField = 'Qtde'
      KeyFields = 'GraGruX'
      Lookup = True
    end
    object QrReposicao7Referencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrReposicao7NO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Size = 60
    end
    object QrReposicao7_Marca_: TIntegerField
      FieldName = '_Marca_'
    end
  end
  object QrEstoque7: TMySQLQuery
    Database = Dmod.MyDB
    Left = 688
    Top = 268
    object QrEstoque7Qtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEstoque7GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsReposicao7: TDataSource
    DataSet = QrReposicao7
    Left = 688
    Top = 216
  end
  object frxPRD_PRINT_001_07_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 752
    Top = 268
    Datasets = <
      item
        DataSet = frxDsReposicao7
        DataSetName = 'frxDsReposicao7'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 74.834687170000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'REPOSI'#199#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[PERIODO_TXT_7]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde vendida')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estoque')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 94.488210940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de Produto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 427.086870470000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 10.582677170000000000
        ParentFont = False
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        DataSet = frxDsReposicao7
        DataSetName = 'frxDsReposicao7'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 37.795275590000000000
          Height = 10.582677170000000000
          DataField = 'SIGLA'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'Qtde'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReposicao7."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'Estoque'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReposicao7."Estoque"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 94.488210940000000000
          Height = 10.582677170000000000
          DataField = 'NO_PGT'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 291.023770940000000000
          Height = 10.582677170000000000
          DataField = 'NO_PRD'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574790940000000000
          Height = 10.582677170000000000
          DataField = 'GraGruX'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReposicao7."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 94.488210940000000000
          Height = 10.582677170000000000
          DataField = 'Referencia'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."Referencia"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsReposicao7: TfrxDBDataset
    UserName = 'frxDsReposicao7'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'Qtde=Qtde'
      'GraGruX=GraGruX'
      'Estoque=Estoque'
      'Referencia=Referencia'
      'NO_MARCA=NO_MARCA'
      '_Marca_=_Marca_')
    DataSet = QrReposicao7
    BCDToCurrency = False
    DataSetOptions = []
    Left = 688
    Top = 320
  end
  object frxPRD_PRINT_001_07_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPRD_PRINT_001_00E_1GetValue
    Left = 756
    Top = 316
    Datasets = <
      item
        DataSet = frxDsReposicao7
        DataSetName = 'frxDsReposicao7'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 74.834687170000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'REPOSI'#199#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[PERIODO_TXT_7]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde vendida')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Estoque')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 94.488210940000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de Produto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 427.086870470000000000
          Height = 10.582677170000000000
          DataSet = frxDsSMIA3
          DataSetName = 'frxDsSMIA3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 10.582677170000000000
        ParentFont = False
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        DataSet = frxDsReposicao7
        DataSetName = 'frxDsReposicao7'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 37.795275590000000000
          Height = 10.582677170000000000
          DataField = 'SIGLA'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'Qtde'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReposicao7."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 10.582677170000000000
          DataField = 'Estoque'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReposicao7."Estoque"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 94.488210940000000000
          Height = 10.582677170000000000
          DataField = 'NO_PGT'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 291.023770940000000000
          Height = 10.582677170000000000
          DataField = 'NO_PRD'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 41.574790940000000000
          Height = 10.582677170000000000
          DataField = 'GraGruX'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReposicao7."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 94.488210940000000000
          Height = 10.582677170000000000
          DataField = 'Referencia'
          DataSet = frxDsReposicao7
          DataSetName = 'frxDsReposicao7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReposicao7."Referencia"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 15.118120000000000000
        ParentFont = False
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReposicao7."NO_MARCA"'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReposicao7."_Marca_"] - [frxDsReposicao7."NO_MARCA"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 226.771800000000000000
        Width = 680.315400000000000000
      end
    end
  end
end
