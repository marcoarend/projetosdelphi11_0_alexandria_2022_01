object FmGraGruN: TFmGraGruN
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-001 :: Cadastro de Grupos de Produtos'
  ClientHeight = 849
  ClientWidth = 1148
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 69
    Width = 1148
    Height = 716
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PainelDados: TPanel
      Left = 0
      Top = 0
      Width = 1148
      Height = 716
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      object SplitterMeio: TSplitter
        Left = 0
        Top = 249
        Width = 1148
        Height = 10
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 1008
      end
      object Panel9: TPanel
        Left = 0
        Top = 259
        Width = 1148
        Height = 457
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PCGeral: TPageControl
          Left = 309
          Top = 0
          Width = 839
          Height = 457
          ActivePage = TabSheet8
          Align = alClient
          TabOrder = 0
          OnChange = PCGeralChange
          OnChanging = PCGeralChanging
          object TabSheet1: TTabSheet
            Caption = 'Ativos'
            object GradeA: TStringGrid
              Left = 0
              Top = 17
              Width = 831
              Height = 412
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnClick = GradeAClick
              OnDrawCell = GradeADrawCell
              ColWidths = (
                65
                65)
              RowHeights = (
                18
                18)
            end
            object StaticText2: TStaticText
              Left = 0
              Top = 0
              Width = 475
              Height = 17
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
                'esativar o produto.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Pre'#231'os'
            ImageIndex = 1
            object GradeP: TStringGrid
              Left = 0
              Top = 0
              Width = 531
              Height = 429
              Align = alClient
              ColCount = 2
              DefaultColWidth = 100
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnClick = GradePClick
              OnDrawCell = GradePDrawCell
              OnMouseUp = GradePMouseUp
              ColWidths = (
                100
                100)
              RowHeights = (
                18
                18)
            end
            object PnEntidade: TPanel
              Left = 531
              Top = 0
              Width = 300
              Height = 429
              Align = alRight
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object dmkDBGrid1: TdmkDBGridZTO
                Left = 0
                Top = 90
                Width = 300
                Height = 291
                Align = alClient
                DataSource = DsGraCusPrcU
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CodUsu'
                    Title.Caption = 'C'#243'digo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 192
                    Visible = True
                  end>
              end
              object Panel32: TPanel
                Left = 0
                Top = 0
                Width = 300
                Height = 90
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object Label53: TLabel
                  Left = 4
                  Top = 4
                  Width = 121
                  Height = 13
                  Caption = 'Entidade (Cliente interno):'
                end
                object EdEntidade: TdmkEditCB
                  Left = 4
                  Top = 20
                  Width = 53
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdEntidadeChange
                  DBLookupComboBox = CBEntidade
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBEntidade: TdmkDBLookupComboBox
                  Left = 58
                  Top = 20
                  Width = 235
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'NO_ENTI'
                  ListSource = DsClientes
                  TabOrder = 1
                  dmkEditCB = EdEntidade
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object RgTipoListaPreco: TRadioGroup
                  Left = 0
                  Top = 45
                  Width = 300
                  Height = 45
                  Align = alBottom
                  Caption = 'Tipo de tabela de pre'#231'os'
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'Custo'
                    'Pre'#231'o')
                  TabOrder = 2
                  OnClick = RgTipoListaPrecoClick
                end
              end
              object PnEntiMenu: TPanel
                Left = 0
                Top = 381
                Width = 300
                Height = 48
                Align = alBottom
                TabOrder = 2
                object BtValores: TBitBtn
                  Tag = 10107
                  Left = 4
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Valores'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtValoresClick
                end
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'C'#243'digos'
            ImageIndex = 2
            object Splitter6: TSplitter
              Left = 252
              Top = 0
              Width = 10
              Height = 429
              ExplicitHeight = 431
            end
            object Panel36: TPanel
              Left = 262
              Top = 0
              Width = 569
              Height = 429
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object SplitterQuarto: TSplitter
                Left = 0
                Top = 277
                Width = 569
                Height = 5
                Cursor = crVSplit
                Align = alBottom
                ExplicitTop = 279
                ExplicitWidth = 131
              end
              object GradeC: TStringGrid
                Left = 0
                Top = 17
                Width = 569
                Height = 260
                Align = alClient
                BevelOuter = bvRaised
                ColCount = 2
                DefaultColWidth = 65
                DefaultRowHeight = 18
                RowCount = 2
                TabOrder = 0
                OnDblClick = GradeCDblClick
                OnDrawCell = GradeCDrawCell
                OnSelectCell = GradeCSelectCell
                ColWidths = (
                  65
                  65)
                RowHeights = (
                  18
                  19)
              end
              object StaticText1: TStaticText
                Left = 0
                Top = 0
                Width = 569
                Height = 17
                Align = alTop
                BorderStyle = sbsSunken
                Caption = 
                  'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
                  'dente na guia "Ativos".'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                ExplicitWidth = 502
              end
              object Panel33: TPanel
                Left = 0
                Top = 282
                Width = 569
                Height = 147
                Align = alBottom
                ParentBackground = False
                TabOrder = 2
                object SBReduzido: TStatusBar
                  Left = 1
                  Top = 1
                  Width = 567
                  Height = 19
                  Align = alTop
                  Panels = <
                    item
                      Text = 'Reduzido selecionado:'
                      Width = 128
                    end
                    item
                      Width = 76
                    end
                    item
                      Width = 50
                    end>
                end
                object PageControl4: TPageControl
                  Left = 1
                  Top = 20
                  Width = 567
                  Height = 126
                  ActivePage = TabSheet18
                  Align = alClient
                  TabOrder = 1
                  object TabSheet18: TTabSheet
                    Caption = 'Codifica'#231#227'o de fornecedores'
                    object GradeGraGruEX: TdmkDBGridZTO
                      Left = 0
                      Top = 0
                      Width = 559
                      Height = 98
                      Align = alClient
                      DataSource = DsGraGruEX
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'cProd'
                          Title.Caption = 'C'#243'digo Fornecedor'
                          Width = 191
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_FORNECE'
                          Title.Caption = 'Fornecedor'
                          Width = 194
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_EMBALAG'
                          Title.Caption = 'Embalagem'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Observacao'
                          Title.Caption = 'Observa'#231#227'o'
                          Visible = True
                        end>
                    end
                  end
                  object TabSheet19: TTabSheet
                    Caption = 'Nomes vinculados'
                    ImageIndex = 1
                    object GradeGraGruVinc: TDBGrid
                      Left = 0
                      Top = 0
                      Width = 24
                      Height = 105
                      Align = alClient
                      DataSource = DsGraGruVinc
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Nome'
                          Width = 484
                          Visible = True
                        end>
                    end
                    object Panel34: TPanel
                      Left = 24
                      Top = 0
                      Width = 99
                      Height = 105
                      Align = alRight
                      ParentBackground = False
                      TabOrder = 1
                      object BtExclVinc: TBitBtn
                        Left = 3
                        Top = 4
                        Width = 90
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Excluir'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtExclVincClick
                      end
                    end
                  end
                end
              end
            end
            object Panel37: TPanel
              Left = 0
              Top = 0
              Width = 252
              Height = 429
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object DBGrid2: TDBGrid
                Left = 0
                Top = 65
                Width = 252
                Height = 364
                Align = alClient
                DataSource = DsGraGruX
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'TAM'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'COR'
                    Width = 75
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruC'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraTamI'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraCorCad'
                    Visible = True
                  end>
              end
              object Panel38: TPanel
                Left = 0
                Top = 0
                Width = 252
                Height = 65
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object StaticText6: TStaticText
                  Left = 0
                  Top = 48
                  Width = 63
                  Height = 17
                  Align = alBottom
                  BorderStyle = sbsSunken
                  Caption = 'Reduzidos'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 2
                end
                object BtReduzidoL: TBitBtn
                  Tag = 14
                  Left = 128
                  Top = 4
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Reduzido'
                  Enabled = False
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtReduzidoLClick
                end
                object BtCodificacao: TBitBtn
                  Tag = 41
                  Left = 4
                  Top = 4
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = 'Codifica'#231#227'o'
                  Enabled = False
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtCodificacaoClick
                end
              end
            end
          end
          object TabSheet8: TTabSheet
            Caption = 'Dados Gerais'
            ImageIndex = 3
            object PageControl2: TPageControl
              Left = 0
              Top = 0
              Width = 831
              Height = 429
              ActivePage = TabSheet28
              Align = alClient
              TabOrder = 0
              object TabSheet5: TTabSheet
                Caption = ' Grandezas e medidas '
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 823
                  Height = 401
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Panel18: TPanel
                    Left = 0
                    Top = 41
                    Width = 823
                    Height = 360
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label55: TLabel
                      Left = 4
                      Top = 48
                      Width = 85
                      Height = 13
                      Caption = '% comiss'#227'o fatur.:'
                    end
                    object Label56: TLabel
                      Left = 100
                      Top = 48
                      Width = 91
                      Height = 13
                      Caption = '% comiss'#227'o receb.:'
                    end
                    object Label57: TLabel
                      Left = 196
                      Top = 48
                      Width = 94
                      Height = 13
                      Caption = 'C'#243'digo do servi'#231'o '#179':'
                    end
                    object Label54: TLabel
                      Left = 4
                      Top = 92
                      Width = 410
                      Height = 13
                      Caption = 
                        #179': C'#243'digo do servi'#231'o conforme lista do Anexo I da Lei Complement' +
                        'ar Federal n'#186' 116/03.'
                    end
                    object dmkDBCheckGroup1: TdmkDBCheckGroup
                      Left = 4
                      Top = 4
                      Width = 325
                      Height = 38
                      Caption = ' Grandezas obrigat'#243'rias em classifica'#231#227'o (gera'#231#227'o de produtos): '
                      Columns = 3
                      DataField = 'HowBxaEstq'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        'Pe'#231'as'
                        #193'rea (m'#178', ft'#178')'
                        'Peso (kg, ton)')
                      ParentBackground = False
                      TabOrder = 0
                    end
                    object DBRadioGroup1: TDBRadioGroup
                      Left = 332
                      Top = 4
                      Width = 345
                      Height = 38
                      Caption = ' Unidade controladora do estoque (gera'#231#227'o de produtos): '
                      Columns = 6
                      DataField = 'GerBxaEstq'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        '? ? ?'
                        'Pe'#231'a'
                        'm'#178
                        'kg'
                        't (ton)'
                        'ft'#178)
                      TabOrder = 1
                      Values.Strings = (
                        '0'
                        '1'
                        '2'
                        '3'
                        '4'
                        '5'
                        '6'
                        '7'
                        '8'
                        '9'
                        '10')
                    end
                    object DBEdit51: TDBEdit
                      Left = 4
                      Top = 64
                      Width = 92
                      Height = 21
                      DataField = 'PerComissF'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                    object DBEdit52: TDBEdit
                      Left = 100
                      Top = 64
                      Width = 92
                      Height = 21
                      DataField = 'PerComissR'
                      DataSource = DsGraGru1
                      TabOrder = 3
                    end
                    object DBEdit53: TDBEdit
                      Left = 196
                      Top = 64
                      Width = 97
                      Height = 21
                      DataField = 'COD_LST'
                      DataSource = DsGraGru1
                      TabOrder = 4
                    end
                  end
                  object Panel16: TPanel
                    Left = 0
                    Top = 0
                    Width = 823
                    Height = 41
                    Align = alTop
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object Label3: TLabel
                      Left = 4
                      Top = 4
                      Width = 95
                      Height = 13
                      Caption = 'Unidade de medida:'
                    end
                    object Label58: TLabel
                      Left = 608
                      Top = 4
                      Width = 55
                      Height = 13
                      Caption = 'Refer'#234'ncia:'
                      FocusControl = DBEdit54
                    end
                    object DBEdit9: TDBEdit
                      Left = 4
                      Top = 20
                      Width = 56
                      Height = 21
                      DataField = 'CODUSUUNIDMED'
                      DataSource = DsGraGru1
                      TabOrder = 0
                    end
                    object DBEdit10: TDBEdit
                      Left = 60
                      Top = 20
                      Width = 43
                      Height = 21
                      DataField = 'SIGLAUNIDMED'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit6: TDBEdit
                      Left = 104
                      Top = 20
                      Width = 501
                      Height = 21
                      DataField = 'NOMEUNIDMED'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                    object DBEdit54: TDBEdit
                      Left = 608
                      Top = 20
                      Width = 69
                      Height = 21
                      DataField = 'Referencia'
                      DataSource = DsGraGru1
                      TabOrder = 3
                    end
                  end
                end
              end
              object TabSheet9: TTabSheet
                Caption = ' Customiza'#231#227'o '
                ImageIndex = 1
                object Panel20: TPanel
                  Left = 0
                  Top = 0
                  Width = 823
                  Height = 401
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Panel21: TPanel
                    Left = 0
                    Top = 0
                    Width = 823
                    Height = 61
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object RGTipDimens: TDBRadioGroup
                      Left = 4
                      Top = 5
                      Width = 621
                      Height = 52
                      Caption = ' Mensuramento: '
                      Columns = 2
                      DataField = 'TipDimens'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        'Unimensur'#225'vel  -  pe'#231'a, kg, metros, litros, etc.'
                        'Bimensur'#225'vell  -  '#193'rea (m'#178')'
                        'Trimensur'#225'vel - Volume (m'#179' > Compr. x largura x altura)'
                        'Quadrimensur'#225'vel - Volume x peso espec'#237'fico')
                      TabOrder = 0
                      Values.Strings = (
                        '0'
                        '1'
                        '2'
                        '3')
                    end
                  end
                  object Panel19: TPanel
                    Left = 0
                    Top = 61
                    Width = 823
                    Height = 128
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 1
                    object Label13: TLabel
                      Left = 12
                      Top = 4
                      Width = 186
                      Height = 13
                      Caption = 'Ordem das medidas no mensuramento: '
                    end
                    object Label6: TLabel
                      Left = 12
                      Top = 44
                      Width = 206
                      Height = 13
                      Caption = 'Parte principal em produtos personaliz'#225'veis:'
                    end
                    object Label7: TLabel
                      Left = 12
                      Top = 84
                      Width = 47
                      Height = 13
                      Caption = 'Medida 1:'
                      FocusControl = DBEdit17
                    end
                    object Label8: TLabel
                      Left = 164
                      Top = 84
                      Width = 47
                      Height = 13
                      Caption = 'Medida 2:'
                      FocusControl = DBEdit18
                    end
                    object Label9: TLabel
                      Left = 316
                      Top = 84
                      Width = 47
                      Height = 13
                      Caption = 'Medida 3:'
                      FocusControl = DBEdit19
                    end
                    object Label10: TLabel
                      Left = 468
                      Top = 84
                      Width = 47
                      Height = 13
                      Caption = 'Medida 4:'
                      FocusControl = DBEdit20
                    end
                    object DBEdit8: TDBEdit
                      Left = 12
                      Top = 20
                      Width = 56
                      Height = 21
                      DataField = 'MedOrdem'
                      DataSource = DsGraGru1
                      TabOrder = 0
                    end
                    object DBEdit12: TDBEdit
                      Left = 12
                      Top = 60
                      Width = 56
                      Height = 21
                      DataField = 'PartePrinc'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit15: TDBEdit
                      Left = 72
                      Top = 20
                      Width = 545
                      Height = 21
                      DataField = 'NO_MedOrdem'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                    object DBEdit16: TDBEdit
                      Left = 72
                      Top = 60
                      Width = 545
                      Height = 21
                      DataField = 'NO_PartePrinc'
                      DataSource = DsGraGru1
                      TabOrder = 3
                    end
                    object DBEdit17: TDBEdit
                      Left = 12
                      Top = 100
                      Width = 148
                      Height = 21
                      DataField = 'Medida1'
                      DataSource = DsGraGru1
                      TabOrder = 4
                    end
                    object DBEdit18: TDBEdit
                      Left = 164
                      Top = 100
                      Width = 148
                      Height = 21
                      DataField = 'Medida2'
                      DataSource = DsGraGru1
                      TabOrder = 5
                    end
                    object DBEdit19: TDBEdit
                      Left = 316
                      Top = 100
                      Width = 148
                      Height = 21
                      DataField = 'Medida3'
                      DataSource = DsGraGru1
                      TabOrder = 6
                    end
                    object DBEdit20: TDBEdit
                      Left = 468
                      Top = 100
                      Width = 148
                      Height = 21
                      DataField = 'Medida4'
                      DataSource = DsGraGru1
                      TabOrder = 7
                    end
                  end
                  object Panel22: TPanel
                    Left = 0
                    Top = 189
                    Width = 823
                    Height = 212
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 2
                    object Label15: TLabel
                      Left = 12
                      Top = 4
                      Width = 83
                      Height = 13
                      Caption = 'Peso espec'#237'fico'#178':'
                    end
                    object Label12: TLabel
                      Left = 116
                      Top = 4
                      Width = 178
                      Height = 13
                      Caption = '% m'#237'nimo e m'#225'ximo de customiza'#231#227'o:'
                    end
                    object DBEdit11: TDBEdit
                      Left = 12
                      Top = 21
                      Width = 100
                      Height = 21
                      DataField = 'Peso'
                      DataSource = DsGraGru1
                      TabOrder = 0
                    end
                    object DBEdit7: TDBEdit
                      Left = 116
                      Top = 21
                      Width = 98
                      Height = 21
                      DataField = 'PerCuztMin'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit4: TDBEdit
                      Left = 220
                      Top = 21
                      Width = 98
                      Height = 21
                      DataField = 'PerCuztMax'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                  end
                end
              end
              object TabSheet28: TTabSheet
                Caption = ' Cont'#225'bil '
                ImageIndex = 2
                object Panel7: TPanel
                  Left = 0
                  Top = 0
                  Width = 823
                  Height = 401
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label74: TLabel
                    Left = 8
                    Top = 8
                    Width = 72
                    Height = 13
                    Caption = 'Grupo cont'#225'bil:'
                  end
                  object Label75: TLabel
                    Left = 8
                    Top = 48
                    Width = 125
                    Height = 13
                    Caption = 'Conta do plano de contas:'
                  end
                  object DBEdit59: TDBEdit
                    Left = 8
                    Top = 24
                    Width = 56
                    Height = 21
                    DataField = 'CtbCadGru'
                    DataSource = DsGraGru1
                    TabOrder = 0
                  end
                  object DBEdit60: TDBEdit
                    Left = 64
                    Top = 24
                    Width = 701
                    Height = 21
                    DataField = 'NO_CtbCadGru'
                    DataSource = DsGraGru1
                    TabOrder = 1
                  end
                  object DBEdit61: TDBEdit
                    Left = 8
                    Top = 64
                    Width = 56
                    Height = 21
                    DataField = 'CtbPlaCta'
                    DataSource = DsGraGru1
                    TabOrder = 2
                  end
                  object DBEdit62: TDBEdit
                    Left = 67
                    Top = 64
                    Width = 701
                    Height = 21
                    DataField = 'NO_CtbPlaCta'
                    DataSource = DsGraGru1
                    TabOrder = 3
                  end
                end
              end
            end
          end
          object TabSheet10: TTabSheet
            Caption = 'Fiscal'
            ImageIndex = 4
            object PageControl3: TPageControl
              Left = 0
              Top = 0
              Width = 831
              Height = 429
              ActivePage = TabSheet17
              Align = alClient
              TabOrder = 0
              object TabSheet12: TTabSheet
                Caption = ' Geral '
                ImageIndex = 1
                object Panel24: TPanel
                  Left = 0
                  Top = 0
                  Width = 823
                  Height = 401
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label14: TLabel
                    Left = 108
                    Top = 8
                    Width = 27
                    Height = 13
                    Caption = 'NCM:'
                  end
                  object Label22: TLabel
                    Left = 8
                    Top = 7
                    Width = 92
                    Height = 13
                    Caption = 'Sigla custom. NF-e:'
                  end
                  object Label18: TLabel
                    Left = 8
                    Top = 49
                    Width = 197
                    Height = 13
                    Caption = 'Informa'#231#245'es adicionais do produto (NF-e):'
                  end
                  object Label70: TLabel
                    Left = 12
                    Top = 116
                    Width = 62
                    Height = 13
                    Caption = 'GTIN / EAN:'
                    FocusControl = DBEdit57
                  end
                  object Label73: TLabel
                    Left = 204
                    Top = 116
                    Width = 191
                    Height = 13
                    Caption = 'C'#243'digo de barras pr'#243'prio ou de terceiros:'
                    FocusControl = DBEdit58
                  end
                  object DBEdit5: TDBEdit
                    Left = 108
                    Top = 25
                    Width = 100
                    Height = 21
                    DataField = 'NCM'
                    DataSource = DsGraGru1
                    TabOrder = 0
                  end
                  object DBEdit22: TDBEdit
                    Left = 8
                    Top = 24
                    Width = 97
                    Height = 21
                    DataField = 'SiglaCustm'
                    DataSource = DsGraGru1
                    TabOrder = 1
                  end
                  object DBEdit21: TDBEdit
                    Left = 8
                    Top = 66
                    Width = 661
                    Height = 21
                    DataField = 'InfAdProd'
                    DataSource = DsGraGru1
                    TabOrder = 2
                  end
                  object DBRadioGroup9: TDBRadioGroup
                    Left = 212
                    Top = 0
                    Width = 341
                    Height = 48
                    Caption = ' Obten'#231#227'o das informa'#231#245'es Fiscais na NF-e: '
                    Columns = 2
                    DataField = 'DadosFisc'
                    DataSource = DsGraGru1
                    Items.Strings = (
                      'Da Regra Fiscal'
                      'Daqui do Cadastro')
                    TabOrder = 3
                    Visible = False
                  end
                  object DBCheckBox1: TDBCheckBox
                    Left = 8
                    Top = 92
                    Width = 189
                    Height = 17
                    Caption = 'Indicador de Escala Relevante'
                    DataField = 'prod_indEscala'
                    DataSource = DsGraGru1
                    TabOrder = 4
                    ValueChecked = '1'
                    ValueUnchecked = '0'
                  end
                  object DBCheckBox2: TDBCheckBox
                    Left = 216
                    Top = 92
                    Width = 189
                    Height = 17
                    Caption = 'Indicador de Escala Relevante'
                    DataField = 'UsaSubsTrib'
                    DataSource = DsGraGru1
                    TabOrder = 5
                    ValueChecked = '1'
                    ValueUnchecked = '0'
                  end
                  object DBEdit57: TDBEdit
                    Left = 12
                    Top = 132
                    Width = 186
                    Height = 21
                    DataField = 'cGTIN_EAN'
                    DataSource = DsGraGru1
                    TabOrder = 6
                  end
                  object DBEdit58: TDBEdit
                    Left = 204
                    Top = 132
                    Width = 394
                    Height = 21
                    DataField = 'prod_cBarra'
                    DataSource = DsGraGru1
                    TabOrder = 7
                  end
                end
              end
              object TabSheet11: TTabSheet
                Caption = ' ICMS '
                object GroupBox1: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 823
                  Height = 401
                  Align = alClient
                  Caption = 
                    ' CST - ICMS (Imposto sobre Circula'#231#227'o de Mercadorias e Servi'#231'os)' +
                    ': '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 0
                  object GroupBox3: TGroupBox
                    Left = 2
                    Top = 141
                    Width = 819
                    Height = 101
                    Align = alTop
                    Caption = ' ICMS Normal: '
                    TabOrder = 0
                    object Panel35: TPanel
                      Left = 2
                      Top = 15
                      Width = 815
                      Height = 54
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label11: TLabel
                        Left = 372
                        Top = 8
                        Width = 83
                        Height = 13
                        Caption = 'N14: % Red. BC'#178':'
                      end
                      object Label65: TLabel
                        Left = 468
                        Top = 8
                        Width = 83
                        Height = 13
                        Caption = 'N16b: % do diferi:'
                      end
                      object Label66: TLabel
                        Left = 572
                        Top = 8
                        Width = 93
                        Height = 13
                        Caption = 'N27a: % da deson.:'
                      end
                      object DBRadioGroup2: TDBRadioGroup
                        Left = 0
                        Top = 0
                        Width = 359
                        Height = 54
                        Align = alLeft
                        Caption = ' N13 - Modalidade de determina'#231#227'o da BC do ICMS: '
                        Columns = 2
                        DataField = 'ICMS_modBC'
                        DataSource = DsGraGru1
                        Items.Strings = (
                          'Margem valor agregado (%)'
                          'Pauta (valor)'
                          'Pre'#231'o tabelado m'#225'ximo (valor)'
                          'Valor da opera'#231#227'o')
                        TabOrder = 0
                        Values.Strings = (
                          '0'
                          '1'
                          '2'
                          '3')
                      end
                      object DBEdit23: TDBEdit
                        Left = 372
                        Top = 24
                        Width = 84
                        Height = 21
                        DataField = 'ICMS_pRedBC'
                        DataSource = DsGraGru1
                        TabOrder = 1
                      end
                      object DBEdit55: TDBEdit
                        Left = 468
                        Top = 24
                        Width = 100
                        Height = 21
                        DataField = 'ICMS_pDif'
                        DataSource = DsGraGru1
                        TabOrder = 2
                      end
                      object DBEdit56: TDBEdit
                        Left = 572
                        Top = 24
                        Width = 100
                        Height = 21
                        DataField = 'ICMS_pICMSDeson'
                        DataSource = DsGraGru1
                        TabOrder = 3
                      end
                    end
                    object TPanel
                      Left = 2
                      Top = 69
                      Width = 815
                      Height = 30
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 1
                      object Label44: TLabel
                        Left = 4
                        Top = 8
                        Width = 159
                        Height = 13
                        Caption = 'Motivo da desonera'#231#227'o do ICMS:'
                      end
                      object EdDBICMS_motDesICMS_TXT: TEdit
                        Left = 198
                        Top = 4
                        Width = 475
                        Height = 21
                        ReadOnly = True
                        TabOrder = 0
                      end
                      object EdDBICMS_motDesICMS: TDBEdit
                        Left = 172
                        Top = 4
                        Width = 25
                        Height = 21
                        DataField = 'ICMS_motDesICMS'
                        DataSource = DsGraGru1
                        TabOrder = 1
                        OnChange = EdDBICMS_motDesICMSChange
                      end
                    end
                  end
                  object Panel25: TPanel
                    Left = 2
                    Top = 15
                    Width = 819
                    Height = 126
                    Align = alTop
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object Label23: TLabel
                      Left = 8
                      Top = 4
                      Width = 156
                      Height = 13
                      Caption = 'N11 - Origem da mercadoria: [F3]'
                    end
                    object Label24: TLabel
                      Left = 8
                      Top = 44
                      Width = 202
                      Height = 13
                      Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                    end
                    object Label69: TLabel
                      Left = 8
                      Top = 84
                      Width = 356
                      Height = 13
                      Caption = 
                        'N12a - C'#243'digo de Situa'#231#227'o da Opera'#231#227'o - Simples Nacional (CSOSN)' +
                        ': [F3] '
                    end
                    object EdCST_A: TDBEdit
                      Left = 8
                      Top = 20
                      Width = 45
                      Height = 21
                      DataField = 'CST_A'
                      DataSource = DsGraGru1
                      TabOrder = 0
                      OnChange = EdCST_AChange
                    end
                    object EdTextoA: TdmkEdit
                      Left = 56
                      Top = 20
                      Width = 613
                      Height = 21
                      ReadOnly = True
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCST_B: TDBEdit
                      Left = 8
                      Top = 61
                      Width = 45
                      Height = 21
                      DataField = 'CST_B'
                      DataSource = DsGraGru1
                      TabOrder = 2
                      OnChange = EdCST_BChange
                    end
                    object EdTextoB: TdmkEdit
                      Left = 56
                      Top = 61
                      Width = 613
                      Height = 21
                      ReadOnly = True
                      TabOrder = 3
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object DBEdCSOSN: TDBEdit
                      Left = 8
                      Top = 101
                      Width = 45
                      Height = 21
                      DataField = 'CSOSN'
                      DataSource = DsGraGru1
                      TabOrder = 4
                      OnChange = DBEdCSOSNChange
                    end
                    object EdTextoC: TdmkEdit
                      Left = 56
                      Top = 101
                      Width = 613
                      Height = 21
                      ReadOnly = True
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                  object GroupBox4: TGroupBox
                    Left = 2
                    Top = 242
                    Width = 819
                    Height = 88
                    Align = alTop
                    Caption = ' ICMS ST ( Substitui'#231#227'o Tribut'#225'ria): '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 2
                    object Label25: TLabel
                      Left = 412
                      Top = 20
                      Width = 86
                      Height = 13
                      Caption = 'N19: % MVA. BC'#178':'
                    end
                    object Label26: TLabel
                      Left = 500
                      Top = 20
                      Width = 83
                      Height = 13
                      Caption = 'N20: % Red. BC'#178':'
                    end
                    object Label27: TLabel
                      Left = 588
                      Top = 20
                      Width = 77
                      Height = 13
                      Caption = 'N22: % Aliq. ST:'
                      Enabled = False
                    end
                    object DBRadioGroup3: TDBRadioGroup
                      Left = 2
                      Top = 15
                      Width = 407
                      Height = 71
                      Align = alLeft
                      Caption = ' N18 - Modalidade de determina'#231#227'o da BC do ICMS: '
                      Columns = 2
                      DataField = 'ICMS_modBCST'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        '0 - Pre'#231'o tabelado ou m'#225'ximo sugerido'
                        '1 - Lista Negativa (valor)'
                        '2 - Lista Positiva (valor)'
                        '3 - Lista Neutra (valor)'
                        '4 - Margem valor agregado (%)'
                        '5 - Pauta (valor)')
                      TabOrder = 0
                      Values.Strings = (
                        '0'
                        '1'
                        '2'
                        '3')
                    end
                    object DBEdit24: TDBEdit
                      Left = 412
                      Top = 36
                      Width = 84
                      Height = 21
                      DataField = 'ICMS_pRedBCST'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit25: TDBEdit
                      Left = 500
                      Top = 36
                      Width = 84
                      Height = 21
                      DataField = 'ICMS_pMVAST'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                    object DBEdit26: TDBEdit
                      Left = 588
                      Top = 36
                      Width = 84
                      Height = 21
                      DataField = 'ICMS_pICMSST'
                      DataSource = DsGraGru1
                      TabOrder = 3
                    end
                  end
                  object GroupBox9: TGroupBox
                    Left = 2
                    Top = 335
                    Width = 819
                    Height = 64
                    Align = alBottom
                    Caption = ' Recupera'#231#227'o de ICMS de Notas Fiscais de entrada: '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 3
                    object Label45: TLabel
                      Left = 12
                      Top = 20
                      Width = 57
                      Height = 13
                      Caption = '% Red. BC'#178':'
                    end
                    object Label46: TLabel
                      Left = 100
                      Top = 20
                      Width = 54
                      Height = 13
                      Caption = '% Al'#237'quota:'
                    end
                    object DBRadioGroup5: TDBRadioGroup
                      Left = 164
                      Top = 12
                      Width = 197
                      Height = 47
                      Caption = ' C'#225'lculo: '
                      Columns = 2
                      DataField = 'ICMSRec_tCalc'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        'Pela aliquota'
                        'Pela NFe')
                      TabOrder = 0
                      Values.Strings = (
                        '0'
                        '1')
                    end
                    object DBEdit43: TDBEdit
                      Left = 12
                      Top = 36
                      Width = 85
                      Height = 21
                      DataField = 'ICMSRec_pRedBC'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit44: TDBEdit
                      Left = 100
                      Top = 36
                      Width = 57
                      Height = 21
                      DataField = 'ICMSRec_pAliq'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                  end
                end
              end
              object TabSheet13: TTabSheet
                Caption = ' IPI '
                ImageIndex = 2
                object Panel23: TPanel
                  Left = 0
                  Top = 0
                  Width = 386
                  Height = 406
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label36: TLabel
                    Left = 104
                    Top = 76
                    Width = 158
                    Height = 13
                    Caption = 'O12: Valor por unidade tribut'#225'vel:'
                  end
                  object Label37: TLabel
                    Left = 104
                    Top = 100
                    Width = 109
                    Height = 13
                    Caption = 'O13: % Aliquota de IPI:'
                  end
                  object Label38: TLabel
                    Left = 104
                    Top = 124
                    Width = 119
                    Height = 13
                    Caption = 'O06: C'#243'd. enq. legal IPI'#185':'
                  end
                  object Label43: TLabel
                    Left = 408
                    Top = 81
                    Width = 122
                    Height = 13
                    Caption = '% Red. BC Recupera'#231#227'o:'
                    FocusControl = DBEdit42
                  end
                  object DBEdit13: TDBEdit
                    Left = 272
                    Top = 97
                    Width = 121
                    Height = 24
                    DataField = 'IPI_Alq'
                    DataSource = DsGraGru1
                    TabOrder = 0
                  end
                  object DBEdit14: TDBEdit
                    Left = 272
                    Top = 121
                    Width = 121
                    Height = 24
                    DataField = 'IPI_cEnq'
                    DataSource = DsGraGru1
                    TabOrder = 1
                  end
                  object GroupBox2: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 386
                    Height = 61
                    Align = alTop
                    Caption = ' CST - IPI (Imposto sobre produtos industrializados): '
                    TabOrder = 2
                    object Label28: TLabel
                      Left = 8
                      Top = 16
                      Width = 199
                      Height = 13
                      Caption = 'O09: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                    end
                    object EdIPI_CST: TDBEdit
                      Left = 8
                      Top = 32
                      Width = 45
                      Height = 24
                      DataField = 'IPI_CST'
                      DataSource = DsGraGru1
                      TabOrder = 0
                      OnChange = EdIPI_CSTChange
                    end
                    object EdTextoIPI_CST: TdmkEdit
                      Left = 56
                      Top = 32
                      Width = 613
                      Height = 21
                      ReadOnly = True
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                  object DBEdit27: TDBEdit
                    Left = 272
                    Top = 73
                    Width = 121
                    Height = 24
                    DataField = 'IPI_vUnid'
                    DataSource = DsGraGru1
                    TabOrder = 3
                  end
                  object DBRadioGroup4: TDBRadioGroup
                    Left = 1
                    Top = 68
                    Width = 97
                    Height = 77
                    Caption = ' Tipo tributa'#231#227'o: '
                    DataField = 'IPI_TpTrib'
                    DataSource = DsGraGru1
                    Items.Strings = (
                      'Nenhuma'
                      'Unidade'
                      'Al'#237'quota')
                    TabOrder = 4
                    Values.Strings = (
                      '0'
                      '1'
                      '2')
                  end
                  object DBEdit42: TDBEdit
                    Left = 408
                    Top = 97
                    Width = 134
                    Height = 24
                    DataField = 'IPIRec_pRedBC'
                    DataSource = DsGraGru1
                    TabOrder = 5
                  end
                  object GroupBox10: TGroupBox
                    Left = 0
                    Top = 343
                    Width = 386
                    Height = 63
                    Align = alBottom
                    Caption = ' Recupera'#231#227'o de IPI de Notas Fiscais de entrada: '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 6
                    object Label47: TLabel
                      Left = 12
                      Top = 20
                      Width = 57
                      Height = 13
                      Caption = '% Red. BC'#178':'
                    end
                    object Label48: TLabel
                      Left = 100
                      Top = 20
                      Width = 54
                      Height = 13
                      Caption = '% Al'#237'quota:'
                    end
                    object DBRadioGroup6: TDBRadioGroup
                      Left = 164
                      Top = 12
                      Width = 197
                      Height = 47
                      Caption = ' C'#225'lculo: '
                      Columns = 2
                      DataField = 'IPIRec_tCalc'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        'Pela aliquota'
                        'Pela NFe')
                      TabOrder = 0
                      Values.Strings = (
                        '0'
                        '1')
                    end
                    object DBEdit45: TDBEdit
                      Left = 12
                      Top = 36
                      Width = 85
                      Height = 24
                      DataField = 'IPIRec_pRedBC'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit46: TDBEdit
                      Left = 100
                      Top = 36
                      Width = 57
                      Height = 24
                      DataField = 'IPIRec_pAliq'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                  end
                end
              end
              object TabSheet14: TTabSheet
                Caption = ' PIS '
                ImageIndex = 3
                object GroupBox5: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 386
                  Height = 61
                  Align = alTop
                  Caption = ' CST - PIS (Programa de Integra'#231#227'o Social): '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 0
                  object Label16: TLabel
                    Left = 8
                    Top = 16
                    Width = 199
                    Height = 13
                    Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                  end
                  object EdTextoPIS_CST: TdmkEdit
                    Left = 56
                    Top = 32
                    Width = 613
                    Height = 21
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdPIS_CST: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 45
                    Height = 24
                    DataField = 'PIS_CST'
                    DataSource = DsGraGru1
                    TabOrder = 1
                    OnChange = EdPIS_CSTChange
                  end
                end
                object Panel26: TPanel
                  Left = 0
                  Top = 61
                  Width = 386
                  Height = 48
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object Label17: TLabel
                    Left = 144
                    Top = 4
                    Width = 84
                    Height = 13
                    Caption = 'Q11: Aliq. PIS ($):'
                  end
                  object Label20: TLabel
                    Left = 8
                    Top = 4
                    Width = 86
                    Height = 13
                    Caption = 'Q08: Aliq. PIS (%):'
                  end
                  object Label19: TLabel
                    Left = 280
                    Top = 4
                    Width = 57
                    Height = 13
                    Caption = '% Red. BC'#178':'
                  end
                  object Label42: TLabel
                    Left = 420
                    Top = 4
                    Width = 122
                    Height = 13
                    Caption = '% Red. BC Recupera'#231#227'o:'
                    FocusControl = DBEdit41
                  end
                  object DBEdit28: TDBEdit
                    Left = 8
                    Top = 20
                    Width = 132
                    Height = 24
                    DataField = 'PIS_AlqP'
                    DataSource = DsGraGru1
                    TabOrder = 0
                  end
                  object DBEdit29: TDBEdit
                    Left = 144
                    Top = 20
                    Width = 132
                    Height = 24
                    DataField = 'PIS_AlqV'
                    DataSource = DsGraGru1
                    TabOrder = 1
                  end
                  object DBEdit32: TDBEdit
                    Left = 280
                    Top = 20
                    Width = 134
                    Height = 24
                    DataField = 'PIS_pRedBC'
                    DataSource = DsGraGru1
                    TabOrder = 2
                  end
                  object DBEdit41: TDBEdit
                    Left = 420
                    Top = 20
                    Width = 134
                    Height = 24
                    DataField = 'PISRec_pRedBC'
                    DataSource = DsGraGru1
                    TabOrder = 3
                  end
                end
                object GroupBox6: TGroupBox
                  Left = 0
                  Top = 109
                  Width = 386
                  Height = 297
                  Align = alClient
                  Caption = ' PIS ST: '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 2
                  object Label29: TLabel
                    Left = 8
                    Top = 16
                    Width = 86
                    Height = 13
                    Caption = 'R03: Aliq. PIS (%):'
                  end
                  object Label30: TLabel
                    Left = 144
                    Top = 16
                    Width = 84
                    Height = 13
                    Caption = 'R05: Aliq. PIS ($):'
                  end
                  object Label21: TLabel
                    Left = 280
                    Top = 16
                    Width = 57
                    Height = 13
                    Caption = '% Red. BC'#178':'
                  end
                  object DBEdit30: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 132
                    Height = 24
                    DataField = 'PISST_AlqP'
                    DataSource = DsGraGru1
                    TabOrder = 0
                  end
                  object DBEdit31: TDBEdit
                    Left = 144
                    Top = 32
                    Width = 132
                    Height = 24
                    DataField = 'PISST_AlqV'
                    DataSource = DsGraGru1
                    TabOrder = 1
                  end
                  object DBEdit37: TDBEdit
                    Left = 280
                    Top = 32
                    Width = 134
                    Height = 24
                    DataField = 'PISST_pRedBCST'
                    DataSource = DsGraGru1
                    TabOrder = 2
                  end
                  object GroupBox11: TGroupBox
                    Left = 2
                    Top = 232
                    Width = 382
                    Height = 64
                    Align = alBottom
                    Caption = ' Recupera'#231#227'o de PIS de Notas Fiscais de entrada: '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 3
                    object Label49: TLabel
                      Left = 12
                      Top = 20
                      Width = 57
                      Height = 13
                      Caption = '% Red. BC'#178':'
                    end
                    object Label50: TLabel
                      Left = 100
                      Top = 20
                      Width = 54
                      Height = 13
                      Caption = '% Al'#237'quota:'
                    end
                    object DBRadioGroup7: TDBRadioGroup
                      Left = 164
                      Top = 12
                      Width = 197
                      Height = 47
                      Caption = ' C'#225'lculo: '
                      Columns = 2
                      DataField = 'PISRec_tCalc'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        'Pela aliquota'
                        'Pela NFe')
                      TabOrder = 0
                      Values.Strings = (
                        '0'
                        '1')
                    end
                    object DBEdit47: TDBEdit
                      Left = 12
                      Top = 36
                      Width = 85
                      Height = 24
                      DataField = 'PISRec_pRedBC'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit48: TDBEdit
                      Left = 100
                      Top = 36
                      Width = 57
                      Height = 24
                      DataField = 'PISRec_pAliq'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                  end
                end
              end
              object TabSheet15: TTabSheet
                Caption = ' COFINS '
                ImageIndex = 4
                object GroupBox8: TGroupBox
                  Left = 0
                  Top = 109
                  Width = 386
                  Height = 297
                  Align = alClient
                  Caption = ' PIS ST: '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 0
                  object Label31: TLabel
                    Left = 8
                    Top = 16
                    Width = 107
                    Height = 13
                    Caption = 'T03: Aliq. COFINS (%):'
                  end
                  object Label32: TLabel
                    Left = 144
                    Top = 16
                    Width = 106
                    Height = 13
                    Caption = 'R05: Aliq. COFINS ($):'
                  end
                  object Label40: TLabel
                    Left = 280
                    Top = 16
                    Width = 57
                    Height = 13
                    Caption = '% Red. BC'#178':'
                  end
                  object DBEdit35: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 132
                    Height = 24
                    DataField = 'COFINSST_AlqP'
                    DataSource = DsGraGru1
                    TabOrder = 0
                  end
                  object DBEdit36: TDBEdit
                    Left = 144
                    Top = 32
                    Width = 132
                    Height = 24
                    DataField = 'COFINSST_AlqV'
                    DataSource = DsGraGru1
                    TabOrder = 1
                  end
                  object DBEdit39: TDBEdit
                    Left = 280
                    Top = 32
                    Width = 134
                    Height = 24
                    DataField = 'COFINSST_pRedBCST'
                    DataSource = DsGraGru1
                    TabOrder = 2
                  end
                  object GroupBox12: TGroupBox
                    Left = 2
                    Top = 232
                    Width = 382
                    Height = 64
                    Align = alBottom
                    Caption = ' Recupera'#231#227'o de COFINS de Notas Fiscais de entrada: '
                    Color = clBtnFace
                    ParentBackground = False
                    ParentColor = False
                    TabOrder = 3
                    object Label51: TLabel
                      Left = 12
                      Top = 20
                      Width = 57
                      Height = 13
                      Caption = '% Red. BC'#178':'
                    end
                    object Label52: TLabel
                      Left = 100
                      Top = 20
                      Width = 54
                      Height = 13
                      Caption = '% Al'#237'quota:'
                    end
                    object DBRadioGroup8: TDBRadioGroup
                      Left = 164
                      Top = 12
                      Width = 197
                      Height = 47
                      Caption = ' C'#225'lculo: '
                      Columns = 2
                      DataField = 'COFINSRec_tCalc'
                      DataSource = DsGraGru1
                      Items.Strings = (
                        'Pela aliquota'
                        'Pela NFe')
                      TabOrder = 0
                      Values.Strings = (
                        '0'
                        '1')
                    end
                    object DBEdit49: TDBEdit
                      Left = 12
                      Top = 36
                      Width = 85
                      Height = 24
                      DataField = 'COFINSRec_pRedBC'
                      DataSource = DsGraGru1
                      TabOrder = 1
                    end
                    object DBEdit50: TDBEdit
                      Left = 100
                      Top = 36
                      Width = 57
                      Height = 24
                      DataField = 'COFINSRec_pAliq'
                      DataSource = DsGraGru1
                      TabOrder = 2
                    end
                  end
                end
                object Panel27: TPanel
                  Left = 0
                  Top = 61
                  Width = 386
                  Height = 48
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object Label33: TLabel
                    Left = 144
                    Top = 4
                    Width = 105
                    Height = 13
                    Caption = 'S10: Aliq. COFINS ($):'
                  end
                  object Label34: TLabel
                    Left = 8
                    Top = 4
                    Width = 107
                    Height = 13
                    Caption = 'S08: Aliq. COFINS (%):'
                  end
                  object Label39: TLabel
                    Left = 280
                    Top = 4
                    Width = 57
                    Height = 13
                    Caption = '% Red. BC'#178':'
                  end
                  object Label41: TLabel
                    Left = 420
                    Top = 4
                    Width = 122
                    Height = 13
                    Caption = '% Red. BC Recupera'#231#227'o:'
                    FocusControl = DBEdit40
                  end
                  object DBEdit33: TDBEdit
                    Left = 8
                    Top = 20
                    Width = 132
                    Height = 24
                    DataField = 'COFINS_AlqP'
                    DataSource = DsGraGru1
                    TabOrder = 0
                  end
                  object DBEdit34: TDBEdit
                    Left = 144
                    Top = 20
                    Width = 132
                    Height = 24
                    DataField = 'COFINS_AlqV'
                    DataSource = DsGraGru1
                    TabOrder = 1
                  end
                  object DBEdit38: TDBEdit
                    Left = 280
                    Top = 20
                    Width = 134
                    Height = 24
                    DataField = 'COFINS_pRedBC'
                    DataSource = DsGraGru1
                    TabOrder = 2
                  end
                  object DBEdit40: TDBEdit
                    Left = 420
                    Top = 20
                    Width = 134
                    Height = 24
                    DataField = 'COFINSRec_pRedBC'
                    DataSource = DsGraGru1
                    TabOrder = 3
                  end
                end
                object GroupBox7: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 386
                  Height = 61
                  Align = alTop
                  Caption = 
                    ' CST - COFINS (Contribui'#231#227'o para o Financiamento da Seguridade S' +
                    'ocial): '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 2
                  object Label35: TLabel
                    Left = 8
                    Top = 16
                    Width = 198
                    Height = 13
                    Caption = 'S06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
                  end
                  object EdTextoCOFINS_CST: TdmkEdit
                    Left = 56
                    Top = 32
                    Width = 709
                    Height = 21
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdCOFINS_CST: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 45
                    Height = 24
                    DataField = 'COFINS_CST'
                    DataSource = DsGraGru1
                    TabOrder = 1
                    OnChange = EdCOFINS_CSTChange
                  end
                end
              end
              object TabSheet16: TTabSheet
                Caption = 'SPED'
                ImageIndex = 5
                object GroupBox15: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 823
                  Height = 249
                  Align = alTop
                  Caption = 'Registro SPED EFD 0210: Consumo espec'#237'fico padronizado: '
                  Color = clBtnFace
                  ParentBackground = False
                  ParentColor = False
                  TabOrder = 0
                  object DBGGraGru1Cons: TdmkDBGridZTO
                    Left = 2
                    Top = 15
                    Width = 819
                    Height = 232
                    Align = alClient
                    DataSource = DsGraGru1Cons
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'COD_ITEM_COMP'
                        Title.Caption = 'Nivel1'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_COD_ITEM_COMP'
                        Title.Caption = 'Nome do produto'
                        Width = 132
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Qtd_Comp'
                        Title.Caption = 'Qtd consumo'
                        Width = 72
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Perda'
                        Title.Caption = '% Perda'
                        Width = 72
                        Visible = True
                      end>
                  end
                end
              end
              object TabSheet17: TTabSheet
                Caption = ' Situa'#231#245'es especiais '
                ImageIndex = 6
                object Panel29: TPanel
                  Left = 0
                  Top = 0
                  Width = 823
                  Height = 401
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object DBGrid1: TDBGrid
                    Left = 0
                    Top = 48
                    Width = 823
                    Height = 353
                    Align = alClient
                    DataSource = DsGraGruCST
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Alignment = taCenter
                        Expanded = False
                        FieldName = 'UF_Orig'
                        Title.Caption = 'Origem'
                        Width = 40
                        Visible = True
                      end
                      item
                        Alignment = taCenter
                        Expanded = False
                        FieldName = 'UF_Dest'
                        Title.Caption = 'Destino'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_B'
                        Title.Caption = 'CST ICMS'
                        Width = 60
                        Visible = True
                      end>
                  end
                  object Panel43: TPanel
                    Left = 0
                    Top = 0
                    Width = 823
                    Height = 48
                    Align = alTop
                    TabOrder = 1
                    object BtEspecial: TBitBtn
                      Tag = 502
                      Left = 4
                      Top = 4
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Especial'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtEspecialClick
                    end
                  end
                end
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Atributos'
            ImageIndex = 5
            object GradeAtrIts: TdmkDBGridZTO
              Left = 0
              Top = 36
              Width = 831
              Height = 393
              Align = alClient
              DataSource = DsGraAtrI
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CODUSU_CAD'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CAD'
                  Title.Caption = 'Atributo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CODUSU_ITS'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_ITS'
                  Title.Caption = 'Descri'#231#227'o do item do atributo'
                  Width = 236
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CTRL_ATR'
                  Title.Caption = 'ID'
                  Visible = True
                end>
            end
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 831
              Height = 36
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 262
                Height = 36
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object Label4: TLabel
                  Left = 0
                  Top = 0
                  Width = 36
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Atributo'
                end
                object EdFiltroAtribCad: TEdit
                  Left = 0
                  Top = 15
                  Width = 262
                  Height = 21
                  Align = alBottom
                  TabOrder = 0
                  OnExit = EdFiltroAtribCadExit
                end
              end
              object Panel15: TPanel
                Left = 262
                Top = 0
                Width = 569
                Height = 36
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Label5: TLabel
                  Left = 0
                  Top = 0
                  Width = 73
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Item de atributo'
                end
                object EdFiltroAtribIts: TEdit
                  Left = 0
                  Top = 15
                  Width = 569
                  Height = 21
                  Align = alBottom
                  TabOrder = 0
                  OnExit = EdFiltroAtribCadExit
                end
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Estoque'
            ImageIndex = 6
            object GradeE: TStringGrid
              Left = 0
              Top = 17
              Width = 831
              Height = 412
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              TabOrder = 0
              OnDrawCell = GradeEDrawCell
              ColWidths = (
                65
                65)
              RowHeights = (
                18
                18)
            end
            object StaticText3: TStaticText
              Left = 0
              Top = 0
              Width = 16
              Height = 17
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = '   '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object TabSheet21: TTabSheet
            Caption = 'Ficha de Consumo'
            ImageIndex = 7
            object Panel28: TPanel
              Left = 0
              Top = 0
              Width = 831
              Height = 429
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Splitter7: TSplitter
                Left = 0
                Top = 157
                Width = 831
                Height = 3
                Cursor = crVSplit
                Align = alTop
                ExplicitWidth = 83
              end
              object Panel31: TPanel
                Left = 0
                Top = 0
                Width = 831
                Height = 157
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label67: TLabel
                  Left = 0
                  Top = 0
                  Width = 62
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Itens b'#225'sicos'
                end
                object DBGFiConsBas: TdmkDBGridZTO
                  Left = 0
                  Top = 13
                  Width = 831
                  Height = 144
                  Align = alClient
                  DataSource = DsFiConsBas
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdUso'
                      Title.Caption = 'Qtd.Uso'
                      Width = 84
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GGXSorc'
                      Title.Caption = 'Red.Pref.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GGXSorc_NO_PRD_TAM_COR'
                      Title.Caption = 'Prefer'#234'ncial'
                      Width = 300
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GGXSubs'
                      Title.Caption = 'Red.Subs.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GGXSubs_NO_PRD_TAM_COR'
                      Title.Caption = 'Substituto'
                      Width = 300
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Parte'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Parte'
                      Title.Caption = 'Parte'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Obrigatorio'
                      Title.Caption = 'Ob?'
                      Width = 36
                      Visible = True
                    end>
                end
              end
              object Panel44: TPanel
                Left = 0
                Top = 160
                Width = 831
                Height = 269
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Label68: TLabel
                  Left = 0
                  Top = 0
                  Width = 831
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Itens n'#227'o b'#225'sicos'
                  ExplicitWidth = 83
                end
                object DBGFiConsOri: TdmkDBGridZTO
                  Left = 0
                  Top = 13
                  Width = 645
                  Height = 256
                  Align = alLeft
                  DataSource = DsFiConsOri
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'GGXSorc_NO_PRD_TAM_COR'
                      Title.Caption = 'Reduzido de origem'
                      Width = 200
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Sorc_SIGLAUNIDMED'
                      Title.Caption = 'Un.med.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Obrigatorio'
                      Title.Caption = 'Ob?'
                      Width = 30
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Parte'
                      Title.Caption = 'Parte'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GGXSubs_NO_PRD_TAM_COR'
                      Title.Caption = 'Reduzido substituto'
                      Width = 179
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Subs_SIGLAUNIDMED'
                      Title.Caption = 'Un.med.'
                      Visible = True
                    end>
                end
                object PnDest: TPanel
                  Left = 645
                  Top = 13
                  Width = 186
                  Height = 256
                  Align = alClient
                  TabOrder = 1
                  object PCGrades: TPageControl
                    Left = 1
                    Top = 1
                    Width = 184
                    Height = 254
                    ActivePage = TabSheet22
                    Align = alClient
                    TabOrder = 0
                    object TabSheet22: TTabSheet
                      Caption = ' Quantidades '
                      object GradeK7: TStringGrid
                        Left = 0
                        Top = 0
                        Width = 176
                        Height = 226
                        Align = alClient
                        ColCount = 2
                        DefaultColWidth = 65
                        DefaultRowHeight = 18
                        FixedCols = 0
                        RowCount = 2
                        FixedRows = 0
                        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goFixedColClick]
                        TabOrder = 0
                        OnDrawCell = GradeK7DrawCell
                        RowHeights = (
                          18
                          18)
                      end
                    end
                    object TabSheet23: TTabSheet
                      Caption = ' Ativos '
                      ImageIndex = 1
                      object GradeA7: TStringGrid
                        Left = 0
                        Top = 0
                        Width = 176
                        Height = 226
                        Align = alClient
                        ColCount = 2
                        DefaultColWidth = 65
                        DefaultRowHeight = 18
                        FixedCols = 0
                        RowCount = 2
                        FixedRows = 0
                        TabOrder = 0
                        ExplicitWidth = 821
                        RowHeights = (
                          18
                          18)
                      end
                    end
                    object TabSheet24: TTabSheet
                      Caption = ' Reduzidos '
                      ImageIndex = 2
                      object GradeC7: TStringGrid
                        Left = 0
                        Top = 0
                        Width = 176
                        Height = 226
                        Align = alClient
                        ColCount = 2
                        DefaultColWidth = 65
                        DefaultRowHeight = 18
                        FixedCols = 0
                        RowCount = 2
                        FixedRows = 0
                        TabOrder = 0
                        RowHeights = (
                          18
                          18)
                      end
                    end
                    object TabSheet25: TTabSheet
                      Caption = ' X '
                      ImageIndex = 3
                      object GradeX7: TStringGrid
                        Left = 0
                        Top = 0
                        Width = 176
                        Height = 226
                        Align = alClient
                        ColCount = 2
                        DefaultColWidth = 65
                        DefaultRowHeight = 18
                        FixedCols = 0
                        RowCount = 2
                        FixedRows = 0
                        TabOrder = 0
                        RowHeights = (
                          18
                          18)
                      end
                    end
                  end
                end
              end
            end
          end
          object TabSheet26: TTabSheet
            Caption = ' Composi'#231#227'o '
            ImageIndex = 8
            object PnDados: TPanel
              Left = 0
              Top = 0
              Width = 831
              Height = 429
              Align = alClient
              BevelOuter = bvNone
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentBackground = False
              ParentFont = False
              TabOrder = 0
              ExplicitTop = 96
              ExplicitWidth = 1008
              ExplicitHeight = 467
              object Splitter9: TSplitter
                Left = 472
                Top = 29
                Height = 400
                Align = alRight
                ExplicitLeft = 608
                ExplicitTop = 136
                ExplicitHeight = 100
              end
              object GBDados: TGroupBox
                Left = 0
                Top = 0
                Width = 831
                Height = 29
                Align = alTop
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 0
                object DBEdCodigo: TdmkDBEdit
                  Left = 4
                  Top = 2
                  Width = 56
                  Height = 21
                  TabStop = False
                  DataField = 'Codigo'
                  DataSource = DsGraCmpCad
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ReadOnly = True
                  ShowHint = True
                  TabOrder = 0
                  UpdType = utYes
                  Alignment = taRightJustify
                end
                object DBEdNome: TdmkDBEdit
                  Left = 64
                  Top = 2
                  Width = 689
                  Height = 21
                  Color = clWhite
                  DataField = 'Nome'
                  DataSource = DsGraCmpCad
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  UpdType = utYes
                  Alignment = taLeftJustify
                end
              end
              object PnPar: TPanel
                Left = 0
                Top = 29
                Width = 472
                Height = 400
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                ExplicitTop = 73
                ExplicitWidth = 513
                ExplicitHeight = 216
                object Label71: TLabel
                  Left = 0
                  Top = 0
                  Width = 87
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Partes do material:'
                end
                object DBGPar: TDBGrid
                  Left = 0
                  Top = 13
                  Width = 472
                  Height = 387
                  Align = alClient
                  DataSource = DsGraCmpPar
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = DBGParDrawColumnCell
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'ID'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Parte'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Parte'
                      Title.Caption = 'Descri'#231#227'o da Parte'
                      Width = 200
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdeImp'
                      Title.Caption = 'Qtd Imp.'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdeTot'
                      Title.Caption = 'Qtd Tot.'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PercImp'
                      Title.Caption = '% Imp'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PercTot'
                      Title.Caption = '% Tot.'
                      Width = 48
                      Visible = True
                    end>
                end
              end
              object PnFib: TPanel
                Left = 475
                Top = 29
                Width = 356
                Height = 400
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 2
                ExplicitTop = 61
                ExplicitHeight = 304
                object Label72: TLabel
                  Left = 0
                  Top = 0
                  Width = 194
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Fibras componentes da parte selecinada:'
                end
                object DBGFib: TDBGrid
                  Left = 0
                  Top = 13
                  Width = 356
                  Height = 387
                  Align = alClient
                  DataSource = DsGraCmpFib
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Conta'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Fibra'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Fibra'
                      Title.Caption = 'Nome da Fibra'
                      Width = 140
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PercComp'
                      Title.Caption = '% Comp.'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Imprime'
                      Title.Caption = 'Imp?'
                      Width = 28
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet27: TTabSheet
            Caption = 'Pack'
            ImageIndex = 9
            object Splitter8: TSplitter
              Left = 0
              Top = 120
              Width = 831
              Height = 3
              Cursor = crVSplit
              Align = alTop
              ExplicitWidth = 309
            end
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 831
              Height = 120
              Align = alTop
              DataSource = DsGraPckCad
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Visible = True
                end>
            end
            object GradeK9: TStringGrid
              Left = 0
              Top = 123
              Width = 831
              Height = 306
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 1
              OnDrawCell = GradeK9DrawCell
              RowHeights = (
                18
                18)
            end
          end
          object TabSheet20: TTabSheet
            Caption = 'Lotes e laudos'
            ImageIndex = 10
            object DBGrid3: TDBGrid
              Left = 0
              Top = 21
              Width = 345
              Height = 408
              Align = alLeft
              DataSource = DsGraGru1Lot
              PopupMenu = PMGraGru1Lot
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Lote'
                  Width = 304
                  Visible = True
                end>
            end
            object Panel41: TPanel
              Left = 0
              Top = 0
              Width = 831
              Height = 21
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Exclua os lotes e laudos que n'#227'o existam mais no estoque!'
              TabOrder = 1
            end
            object DBGrid4: TDBGrid
              Left = 345
              Top = 21
              Width = 486
              Height = 408
              Align = alClient
              DataSource = DsGraGru1Lau
              PopupMenu = PMGraGru1Lau
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Laudo'
                  Width = 304
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'X'
            ImageIndex = 11
            object GradeX: TStringGrid
              Left = 0
              Top = 0
              Width = 831
              Height = 429
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              ColWidths = (
                65
                65)
              RowHeights = (
                18
                18)
            end
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 309
          Height = 457
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel11'
          TabOrder = 1
          object StaticText5: TStaticText
            Left = 0
            Top = 0
            Width = 9
            Height = 457
            Align = alLeft
            AutoSize = False
            Caption = 'C'#13#10'O'#13#10'R'#13#10'E'#13#10'S'
            TabOrder = 0
          end
          object DBGrid6: TdmkDBGridZTO
            Left = 9
            Top = 0
            Width = 300
            Height = 457
            Align = alClient
            DataSource = DsGraGruC
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsuGTC'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NomeGTC'
                Title.Caption = 'Descri'#231#227'o'
                Width = 192
                Visible = True
              end>
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1148
        Height = 249
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object PnGrades: TPanel
          Left = 0
          Top = 177
          Width = 1148
          Height = 72
          Align = alBottom
          TabOrder = 0
          object Panel5: TPanel
            Left = 1
            Top = 1
            Width = 1146
            Height = 70
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 798
              Height = 70
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label2: TLabel
                Left = 4
                Top = 4
                Width = 96
                Height = 13
                Caption = 'Grade de tamanhos:'
              end
              object DBCtrlGrid1: TDBCtrlGrid
                Left = 1
                Top = 21
                Width = 780
                Height = 51
                AllowDelete = False
                ColCount = 15
                DataSource = DsGraTamI
                Orientation = goHorizontal
                PanelBorder = gbNone
                PanelHeight = 34
                PanelWidth = 52
                TabOrder = 0
                RowCount = 1
                object DBEdit1: TDBEdit
                  Left = 4
                  Top = 4
                  Width = 45
                  Height = 21
                  DataField = 'Nome'
                  DataSource = DsGraTamI
                  TabOrder = 0
                end
              end
              object DBEdit2: TDBEdit
                Left = 104
                Top = 0
                Width = 134
                Height = 21
                DataField = 'CODUSUGRATAMCAD'
                DataSource = DsGraGru1
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 240
                Top = 0
                Width = 556
                Height = 21
                DataField = 'NOMEGRATAMCAD'
                DataSource = DsGraGru1
                TabOrder = 2
              end
            end
            object GBAvisos1: TGroupBox
              Left = 798
              Top = 0
              Width = 348
              Height = 70
              Align = alClient
              Caption = ' Avisos: '
              TabOrder = 1
              object Panel39: TPanel
                Left = 2
                Top = 15
                Width = 344
                Height = 53
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object LaAviso1: TLabel
                  Left = 13
                  Top = 2
                  Width = 588
                  Height = 17
                  Caption = 
                    'Clique com o bot'#227'o direito do mouse no n'#237'vel correspondente para' +
                    ' abrir o menu de op'#231#245'es'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clSilver
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object LaAviso2: TLabel
                  Left = 12
                  Top = 1
                  Width = 588
                  Height = 17
                  Caption = 
                    'Clique com o bot'#227'o direito do mouse no n'#237'vel correspondente para' +
                    ' abrir o menu de op'#231#245'es'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
              end
            end
          end
        end
        object PnPaineis: TPanel
          Left = 0
          Top = 0
          Width = 1148
          Height = 177
          Align = alClient
          BevelOuter = bvNone
          Caption = ' Painel das grrades de grupos de produtos.'
          TabOrder = 1
          object Splitter3: TSplitter
            Left = 550
            Top = 0
            Width = 5
            Height = 177
          end
          object Splitter2: TSplitter
            Left = 735
            Top = 0
            Width = 5
            Height = 177
          end
          object Splitter1: TSplitter
            Left = 920
            Top = 0
            Width = 5
            Height = 177
          end
          object Splitter4: TSplitter
            Left = 365
            Top = 0
            Width = 5
            Height = 177
          end
          object Splitter5: TSplitter
            Left = 180
            Top = 0
            Width = 5
            Height = 177
          end
          object PainelN: TPanel
            Left = 0
            Top = 0
            Width = 180
            Height = 177
            Align = alLeft
            BevelOuter = bvNone
            Constraints.MinWidth = 180
            TabOrder = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 180
              Height = 47
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 0
                Top = 0
                Width = 180
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Tipo de produto'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 91
              end
              object Label60: TLabel
                Left = 0
                Top = 13
                Width = 180
                Height = 13
                Align = alTop
                Caption = 'Pesquisa:'
                ExplicitWidth = 46
              end
              object EdFiltroNiv0: TEdit
                Left = 0
                Top = 26
                Width = 180
                Height = 21
                Align = alBottom
                Enabled = False
                TabOrder = 0
              end
            end
            object DBGPrdgrupTip: TdmkDBGridZTO
              Left = 0
              Top = 47
              Width = 180
              Height = 130
              Align = alClient
              Constraints.MinWidth = 180
              DataSource = DsGraGruN
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object Painel1: TPanel
            Left = 925
            Top = 0
            Width = 223
            Height = 177
            Align = alClient
            BevelOuter = bvNone
            Constraints.MinWidth = 180
            TabOrder = 3
            OnResize = Painel1Resize
            object DBGNivel1: TdmkDBGridZTO
              Left = 0
              Top = 47
              Width = 223
              Height = 130
              Align = alClient
              Constraints.MinWidth = 180
              DataSource = DsGraGru1
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              PopupMenu = PMGraGru1
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnDblClick = DBGNivel1DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codusu'
                  Title.Caption = 'C'#243'digo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nivel1'
                  Title.Caption = 'ID Nivel'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NCM'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 180
                  Visible = True
                end>
            end
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 223
              Height = 47
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object LaNivel1: TLabel
                Left = 0
                Top = 0
                Width = 223
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Grupo de Produtos'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 107
              end
              object Label64: TLabel
                Left = 0
                Top = 13
                Width = 223
                Height = 13
                Align = alTop
                Caption = 'Pesquisa:'
                ExplicitWidth = 46
              end
              object EdFiltroNiv1: TEdit
                Left = 0
                Top = 26
                Width = 223
                Height = 21
                Align = alBottom
                TabOrder = 0
                OnExit = EdFiltroNiv1Exit
              end
            end
          end
          object Painel2: TPanel
            Left = 740
            Top = 0
            Width = 180
            Height = 177
            Align = alLeft
            BevelOuter = bvNone
            Constraints.MinWidth = 180
            TabOrder = 2
            object DBGNivel2: TdmkDBGridZTO
              Left = 0
              Top = 47
              Width = 180
              Height = 130
              Align = alClient
              Constraints.MinWidth = 180
              DataSource = DsGraGru2
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              PopupMenu = PMGraGru2
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codusu'
                  Title.Caption = 'C'#243'digo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 100
                  Visible = True
                end>
            end
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 180
              Height = 47
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object LaNivel2: TLabel
                Left = 0
                Top = 0
                Width = 180
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Nivel 2'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 41
              end
              object Label63: TLabel
                Left = 0
                Top = 13
                Width = 180
                Height = 13
                Align = alTop
                Caption = 'Pesquisa:'
                ExplicitWidth = 46
              end
              object EdFiltroNiv2: TEdit
                Left = 0
                Top = 26
                Width = 180
                Height = 21
                Align = alBottom
                TabOrder = 0
                OnExit = EdFiltroNiv2Exit
              end
            end
          end
          object Painel3: TPanel
            Left = 555
            Top = 0
            Width = 180
            Height = 177
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object DBGNivel3: TdmkDBGridZTO
              Left = 0
              Top = 47
              Width = 180
              Height = 130
              Align = alClient
              Constraints.MinWidth = 180
              DataSource = DsGraGru3
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              PopupMenu = PMGraGru3
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codusu'
                  Title.Caption = 'C'#243'digo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 100
                  Visible = True
                end>
            end
            object Panel30: TPanel
              Left = 0
              Top = 0
              Width = 180
              Height = 47
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object LaNivel3: TLabel
                Left = 0
                Top = 0
                Width = 180
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Nivel 3'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 41
              end
              object Label62: TLabel
                Left = 0
                Top = 13
                Width = 180
                Height = 13
                Align = alTop
                Caption = 'Pesquisa:'
                ExplicitWidth = 46
              end
              object EdFiltroNiv3: TEdit
                Left = 0
                Top = 26
                Width = 180
                Height = 21
                Align = alBottom
                TabOrder = 0
                OnExit = EdFiltroNiv3Exit
              end
            end
          end
          object Painel4: TPanel
            Left = 370
            Top = 0
            Width = 180
            Height = 177
            Align = alLeft
            BevelOuter = bvNone
            Constraints.MinWidth = 180
            TabOrder = 4
            object DBGNivel4: TdmkDBGridZTO
              Left = 0
              Top = 47
              Width = 180
              Height = 130
              Align = alClient
              Constraints.MinWidth = 180
              DataSource = DsGraGru4
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              PopupMenu = PMGraGru4
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codusu'
                  Title.Caption = 'C'#243'digo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 100
                  Visible = True
                end>
            end
            object Panel40: TPanel
              Left = 0
              Top = 0
              Width = 180
              Height = 47
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object LaNivel4: TLabel
                Left = 0
                Top = 0
                Width = 180
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'N'#237'vel 4'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 43
              end
              object Label59: TLabel
                Left = 0
                Top = 13
                Width = 180
                Height = 13
                Align = alTop
                Caption = 'Pesquisa:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ExplicitWidth = 46
              end
              object EdFiltroNiv4: TEdit
                Left = 0
                Top = 26
                Width = 180
                Height = 21
                Align = alBottom
                TabOrder = 0
                OnExit = EdFiltroNiv4Exit
              end
            end
          end
          object Painel5: TPanel
            Left = 185
            Top = 0
            Width = 180
            Height = 177
            Align = alLeft
            BevelOuter = bvNone
            Constraints.MinWidth = 180
            TabOrder = 5
            object DBGNivel5: TdmkDBGridZTO
              Left = 0
              Top = 47
              Width = 180
              Height = 130
              Align = alClient
              Constraints.MinWidth = 180
              DataSource = DsGraGru5
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              PopupMenu = PMGraGru5
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codusu'
                  Title.Caption = 'C'#243'digo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 100
                  Visible = True
                end>
            end
            object Panel42: TPanel
              Left = 0
              Top = 0
              Width = 180
              Height = 47
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object LaNivel5: TLabel
                Left = 0
                Top = 0
                Width = 180
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'N'#237'vel 5'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 43
              end
              object Label61: TLabel
                Left = 0
                Top = 13
                Width = 180
                Height = 13
                Align = alTop
                Caption = 'Pesquisa:'
                ExplicitWidth = 46
              end
              object EdFiltroNiv5: TEdit
                Left = 0
                Top = 26
                Width = 180
                Height = 21
                Align = alBottom
                TabOrder = 0
                OnExit = EdFiltroNiv5Exit
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1148
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1100
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 300
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtOpcoesGrad: TBitBtn
        Tag = 348
        Left = 214
        Top = 8
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtOpcoesGradClick
      end
      object BitBtn1: TBitBtn
        Tag = 435
        Left = 256
        Top = 8
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = BitBtn1Click
      end
    end
    object GB_M: TGroupBox
      Left = 300
      Top = 0
      Width = 800
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 400
        Height = 32
        Caption = 'Cadastro de Grupos de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 400
        Height = 32
        Caption = 'Cadastro de Grupos de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 400
        Height = 32
        Caption = 'Cadastro de Grupos de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 785
    Width = 1148
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 13
      Width = 1144
      Height = 49
      Align = alBottom
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object BtCor: TBitBtn
        Tag = 29
        Left = 176
        Top = 4
        Width = 78
        Height = 40
        Cursor = crHandPoint
        Caption = '&Cor'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtCorClick
      end
      object BtGrade: TBitBtn
        Tag = 10054
        Left = 96
        Top = 4
        Width = 78
        Height = 40
        Cursor = crHandPoint
        Caption = '&Grade'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtGradeClick
      end
      object BtInclui: TBitBtn
        Tag = 503
        Left = 5
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = 'Gr&upo'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object Panel2: TPanel
        Left = 1035
        Top = 0
        Width = 109
        Height = 49
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 6
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtDados: TBitBtn
        Tag = 504
        Left = 256
        Top = 4
        Width = 78
        Height = 40
        Cursor = crHandPoint
        Caption = '&Dados'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtDadosClick
      end
      object BtAtributos: TBitBtn
        Tag = 10099
        Left = 416
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Atributos'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtAtributosClick
      end
      object BtFiscal: TBitBtn
        Tag = 502
        Left = 336
        Top = 4
        Width = 78
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fiscal'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtFiscalClick
      end
      object BtGraGru1Cons: TBitBtn
        Left = 508
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = 'Consumo &EP'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = BtGraGru1ConsClick
      end
      object BtNiveis2: TBitBtn
        Tag = 323
        Left = 599
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = 'N'#237'veis 2'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = BtNiveis2Click
      end
      object BtFichaDeConsumo: TBitBtn
        Left = 690
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = 'Ficha de'#13#10'Consumo'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnClick = BtFichaDeConsumoClick
      end
      object BtComposicao: TBitBtn
        Left = 782
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = 'Composi'#231#227'o '#13#10'(por Cor)'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnClick = BtComposicaoClick
      end
      object BtPack: TBitBtn
        Left = 874
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = 'Pack'#13#10'Frequ'#234'ncia'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        OnClick = BtPackClick
      end
    end
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 52
    Width = 1148
    Height = 17
    Align = alTop
    TabOrder = 3
    Visible = False
  end
  object QrGraGru3: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru3BeforeClose
    AfterClose = QrGraGru3AfterClose
    AfterScroll = QrGraGru3AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg3.Codusu, gg3.Nivel4, gg3.Nivel3, '
      'gg3.Nome'
      'FROM gragru3 gg3'
      'LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg3.Nivel4'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel3=gg3.Nivel3'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg3.Nivel4=:P1'
      'AND gg3.Nome LIKE :P2')
    Left = 60
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGru3Codusu: TIntegerField
      FieldName = 'Codusu'
    end
    object QrGraGru3Nivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 88
    Top = 320
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtGrade
    CanDel01 = BtCor
    Left = 68
    Top = 12
  end
  object QrGraGruN: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGruNBeforeClose
    AfterClose = QrGraGruNAfterClose
    AfterScroll = QrGraGruNAfterScroll
    SQL.Strings = (
      
        'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, pgt.Gradead' +
        'o,'
      'pgt.UsaCores,'
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'pgt.TitNiv1, pgt.TitNiv2, pgt.TitNiv3, pgt.TitNiv4, pgt.TitNiv5,'
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO,'
      'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') '
      'NOME_TIPPRD, pgt.ImpedeCad, pgt.PerComissF, pgt.PerComissR,'
      'pgt.GraTabApp, app.Nome GraTabApp_TXT'
      'FROM prdgruptip pgt'
      'LEFT JOIN gratabapp app ON app.Codigo = pgt.GraTabApp'
      'WHERE pgt.Codigo<>0'
      'AND pgt.Ativo = 1'
      'ORDER BY pgt.Nome')
    Left = 100
    Top = 12
    object QrGraGruNCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraGruNCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraGruNNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGruNMadeBy: TSmallintField
      FieldName = 'MadeBy'
      Required = True
    end
    object QrGraGruNFracio: TSmallintField
      FieldName = 'Fracio'
      Required = True
    end
    object QrGraGruNTipPrd: TSmallintField
      FieldName = 'TipPrd'
      Required = True
    end
    object QrGraGruNNivCad: TSmallintField
      FieldName = 'NivCad'
      Required = True
    end
    object QrGraGruNFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
      Required = True
    end
    object QrGraGruNFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
      Required = True
    end
    object QrGraGruNNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrGraGruNNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrGraGruNNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
    object QrGraGruNTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Required = True
      Size = 15
    end
    object QrGraGruNTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrGraGruNTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrGraGruNTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Required = True
      Size = 15
    end
    object QrGraGruNTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Required = True
      Size = 15
    end
    object QrGraGruNImpedeCad: TSmallintField
      FieldName = 'ImpedeCad'
    end
    object QrGraGruNPerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrGraGruNPerComissR: TFloatField
      FieldName = 'PerComissR'
    end
    object QrGraGruNGradeado: TIntegerField
      FieldName = 'Gradeado'
    end
    object QrGraGruNGraTabApp: TIntegerField
      FieldName = 'GraTabApp'
    end
    object QrGraGruNGraTabApp_TXT: TWideStringField
      FieldName = 'GraTabApp_TXT'
      Size = 100
    end
    object QrGraGruNUsaCores: TSmallintField
      FieldName = 'UsaCores'
    end
  end
  object DsGraGruN: TDataSource
    DataSet = QrGraGruN
    Left = 128
    Top = 12
  end
  object QrGraGru2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru2BeforeClose
    AfterClose = QrGraGru2AfterClose
    AfterScroll = QrGraGru2AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg2.Codusu, gg2.Nivel3, gg2.Nivel2, '
      'gg2.Nome'
      'FROM gragru2 gg2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg2.Nivel3'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel2=gg2.Nivel2'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg2.Nivel3=:P1'
      'AND gg2.Nome LIKE :P2'
      '')
    Left = 60
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGru2Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru2Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru2Tipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
  end
  object DsGraGru2: TDataSource
    DataSet = QrGraGru2
    Left = 88
    Top = 348
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru1BeforeClose
    AfterClose = QrGraGru1AfterClose
    AfterScroll = QrGraGru1AfterScroll
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel5, gg1.Nivel4,  '
      'gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD, '
      'gg1.CST_A,gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso, '
      'gg1.IPI_Alq, gg1.IPI_CST, gg1.IPI_cEnq, gg1.TipDimens, '
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED, gg1.PartePrinc, '
      'gg1.PerCuztMin, gg1.PerCuztMax, gg1.MedOrdem, gg1.FatorClas, '
      'mor.Nome NO_MedOrdem, mor.Medida1, mor.Medida2, '
      'mor.Medida3, mor.Medida4, mpc.Nome NO_PartePrinc, '
      'InfAdProd, SiglaCustm, HowBxaEstq, GerBxaEstq, '
      'PIS_CST, PIS_AlqP, PIS_AlqV, PISST_AlqP, PISST_AlqV, '
      'COFINS_CST, COFINS_AlqP, COFINS_AlqV,COFINSST_AlqP, '
      'COFINSST_AlqV, ICMS_modBC, ICMS_modBCST, ICMS_pRedBC, '
      'ICMS_pRedBCST, ICMS_pMVAST, ICMS_pICMSST, '
      'IPI_pIPI, IPI_TpTrib, IPI_vUnid, EX_TIPI, '
      'ICMS_Pauta, ICMS_MaxTab, cGTIN_EAN, '
      'PIS_pRedBC, PISST_pRedBCST, '
      'COFINS_pRedBC, COFINSST_pRedBCST, '
      ' '
      'gg1.ICMS_pDif, gg1.ICMS_pICMSDeson, gg1.ICMS_motDesICMS, '
      ' '
      'ICMSRec_pRedBC, IPIRec_pRedBC, '
      'PISRec_pRedBC, COFINSRec_pRedBC, '
      'ICMSRec_pAliq, IPIRec_pAliq, '
      'PISRec_pAliq, COFINSRec_pAliq, '
      'ICMSRec_tCalc, IPIRec_tCalc, '
      'PISRec_tCalc, COFINSRec_tCalc, '
      'ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA, '
      'gg1.PerComissF, gg1.PerComissR, gg1.PerComissZ, '
      'gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS, '
      'gg1.Referencia, gg1.DadosFisc, gg1.GraTabApp,'
      ''
      'gta.Nome NO_GraTabApp '
      ' '
      'FROM gragru1 gg1 '
      'LEFT JOIN gragru2   gg2 ON gg1.Nivel2=gg2.Nivel2 '
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed '
      'LEFT JOIN medordem mor ON mor.Codigo= gg1.MedOrdem '
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=gg1.PartePrinc '
      'LEFT JOIN gratabapp gta ON gta.Codigo=gg1.GraTabApp'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg1.Nivel2=:P1'
      'AND gg1.Nome LIKE :P2')
    Left = 60
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Origin = 'gragru1.CodUsu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Origin = 'gragru1.Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Origin = 'gragru1.Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Required = True
      Size = 120
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Origin = 'gragru1.GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Origin = 'gratamcad.Nome'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Origin = 'gratamcad.CodUsu'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Origin = 'gragru1.CST_A'
      Required = True
      DisplayFormat = '0'
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Origin = 'gragru1.CST_B'
      Required = True
      DisplayFormat = '00'
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
      Required = True
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Origin = 'gragru1.Peso'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Origin = 'gragru1.NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Origin = 'unidmed.Sigla'
      Size = 3
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Origin = 'unidmed.Nome'
      Size = 30
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Origin = 'unidmed.CodUsu'
      Required = True
    end
    object QrGraGru1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
      Origin = 'gragru1.IPI_CST'
    end
    object QrGraGru1IPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
      Origin = 'gragru1.IPI_Alq'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Origin = 'gragru1.IPI_cEnq'
      Size = 3
    end
    object QrGraGru1TipDimens: TSmallintField
      FieldName = 'TipDimens'
      Origin = 'gragru1.TipDimens'
    end
    object QrGraGru1PerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
      Origin = 'gragru1.PerCuztMin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGraGru1PerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
      Origin = 'gragru1.PerCuztMax'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGraGru1MedOrdem: TIntegerField
      FieldName = 'MedOrdem'
      Origin = 'gragru1.MedOrdem'
    end
    object QrGraGru1PartePrinc: TIntegerField
      FieldName = 'PartePrinc'
      Origin = 'gragru1.PartePrinc'
    end
    object QrGraGru1NO_MedOrdem: TWideStringField
      FieldName = 'NO_MedOrdem'
      Origin = 'medordem.Nome'
      Size = 50
    end
    object QrGraGru1Medida1: TWideStringField
      FieldName = 'Medida1'
      Origin = 'medordem.Medida1'
    end
    object QrGraGru1Medida2: TWideStringField
      FieldName = 'Medida2'
      Origin = 'medordem.Medida2'
    end
    object QrGraGru1Medida3: TWideStringField
      FieldName = 'Medida3'
      Origin = 'medordem.Medida3'
    end
    object QrGraGru1Medida4: TWideStringField
      FieldName = 'Medida4'
      Origin = 'medordem.Medida4'
    end
    object QrGraGru1NO_PartePrinc: TWideStringField
      FieldName = 'NO_PartePrinc'
      Origin = 'matpartcad.Nome'
      Size = 50
    end
    object QrGraGru1InfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Origin = 'gragru1.InfAdProd'
      Size = 255
    end
    object QrGraGru1SiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Origin = 'gragru1.SiglaCustm'
      Size = 15
    end
    object QrGraGru1HowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
      Origin = 'gragru1.HowBxaEstq'
    end
    object QrGraGru1GerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
      Origin = 'gragru1.GerBxaEstq'
    end
    object QrGraGru1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      Origin = 'gragru1.PIS_CST'
    end
    object QrGraGru1PIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
      Origin = 'gragru1.PIS_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
      Origin = 'gragru1.PIS_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
      Origin = 'gragru1.PISST_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
      Origin = 'gragru1.PISST_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      Origin = 'gragru1.COFINS_CST'
    end
    object QrGraGru1COFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
      Origin = 'gragru1.COFINS_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
      Origin = 'gragru1.COFINS_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
      Origin = 'gragru1.COFINSST_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
      Origin = 'gragru1.COFINSST_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
      Origin = 'gragru1.ICMS_modBC'
    end
    object QrGraGru1ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
      Origin = 'gragru1.ICMS_modBCST'
    end
    object QrGraGru1ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      Origin = 'gragru1.ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      Origin = 'gragru1.ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      Origin = 'gragru1.ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      Origin = 'gragru1.ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
      Origin = 'gragru1.IPI_TpTrib'
    end
    object QrGraGru1IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      Origin = 'gragru1.IPI_vUnid'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrGraGru1ICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
      Origin = 'gragru1.ICMS_Pauta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
      Origin = 'gragru1.ICMS_MaxTab'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      Origin = 'gragru1.IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Origin = 'gragru1.cGTIN_EAN'
      Size = 14
    end
    object QrGraGru1EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Origin = 'gragru1.EX_TIPI'
      Size = 3
    end
    object QrGraGru1PIS_pRedBC: TFloatField
      FieldName = 'PIS_pRedBC'
      Origin = 'gragru1.PIS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_pRedBCST: TFloatField
      FieldName = 'PISST_pRedBCST'
      Origin = 'gragru1.PISST_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_pRedBC: TFloatField
      FieldName = 'COFINS_pRedBC'
      Origin = 'gragru1.COFINS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_pRedBCST: TFloatField
      FieldName = 'COFINSST_pRedBCST'
      Origin = 'gragru1.COFINSST_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Origin = 'gragru1.ICMSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Origin = 'gragru1.IPIRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Origin = 'gragru1.PISRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Origin = 'gragru1.COFINSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Origin = 'gragru1.ICMSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Origin = 'gragru1.IPIRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Origin = 'gragru1.PISRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Origin = 'gragru1.COFINSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_tCalc: TSmallintField
      FieldName = 'ICMSRec_tCalc'
      Origin = 'gragru1.ICMSRec_tCalc'
    end
    object QrGraGru1IPIRec_tCalc: TSmallintField
      FieldName = 'IPIRec_tCalc'
      Origin = 'gragru1.IPIRec_tCalc'
    end
    object QrGraGru1PISRec_tCalc: TSmallintField
      FieldName = 'PISRec_tCalc'
      Origin = 'gragru1.PISRec_tCalc'
    end
    object QrGraGru1COFINSRec_tCalc: TSmallintField
      FieldName = 'COFINSRec_tCalc'
      Origin = 'gragru1.COFINSRec_tCalc'
    end
    object QrGraGru1ICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
      Origin = 'gragru1.ICMSAliqSINTEGRA'
    end
    object QrGraGru1ICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
      Origin = 'gragru1.ICMSST_BaseSINTEGRA'
    end
    object QrGraGru1FatorClas: TIntegerField
      FieldName = 'FatorClas'
      Origin = 'gragru1.FatorClas'
    end
    object QrGraGru1PerComissF: TFloatField
      FieldName = 'PerComissF'
      Origin = 'gragru1.PerComissF'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraGru1PerComissR: TFloatField
      FieldName = 'PerComissR'
      Origin = 'gragru1.PerComissR'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraGru1PerComissZ: TSmallintField
      FieldName = 'PerComissZ'
      Origin = 'gragru1.PerComissZ'
    end
    object QrGraGru1COD_LST: TWideStringField
      FieldName = 'COD_LST'
      Origin = 'gragru1.COD_LST'
      Size = 4
    end
    object QrGraGru1SPEDEFD_ALIQ_ICMS: TFloatField
      FieldName = 'SPEDEFD_ALIQ_ICMS'
    end
    object QrGraGru1Referencia: TWideStringField
      DisplayWidth = 25
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGru1DadosFisc: TSmallintField
      FieldName = 'DadosFisc'
    end
    object QrGraGru1Nivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGru1Nivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGru1ICMS_pDif: TFloatField
      FieldName = 'ICMS_pDif'
      DisplayFormat = '0.0000'
    end
    object QrGraGru1ICMS_pICMSDeson: TFloatField
      FieldName = 'ICMS_pICMSDeson'
      DisplayFormat = '0.0000'
    end
    object QrGraGru1ICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrGraGru1GraTabApp: TIntegerField
      FieldName = 'GraTabApp'
    end
    object QrGraGru1NO_GraTabApp: TWideStringField
      FieldName = 'NO_GraTabApp'
      Size = 100
    end
    object QrGraGru1prod_CEST: TIntegerField
      FieldName = 'prod_CEST'
    end
    object QrGraGru1prod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrGraGru1UsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
    end
    object QrGraGru1CSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrGraGru1prod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrGraGru1NO_CtbPlaCta: TWideStringField
      FieldName = 'NO_CtbPlaCta'
      Size = 60
    end
    object QrGraGru1NO_CtbCadGru: TWideStringField
      FieldName = 'NO_CtbCadGru'
      Size = 60
    end
    object QrGraGru1CtbCadGru: TIntegerField
      FieldName = 'CtbCadGru'
    end
    object QrGraGru1CtbPlaCta: TIntegerField
      FieldName = 'CtbPlaCta'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 88
    Top = 172
  end
  object QrGraTamI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gratamits'
      'WHERE Codigo=:P0')
    Left = 376
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraTamIControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrGraTamINome: TWideStringField
      Alignment = taCenter
      FieldName = 'Nome'
      Size = 5
    end
  end
  object DsGraTamI: TDataSource
    DataSet = QrGraTamI
    Left = 404
    Top = 428
  end
  object PMCor: TPopupMenu
    OnPopup = PMCorPopup
    Left = 220
    Top = 640
    object Incluinovacor1: TMenuItem
      Caption = '&Inclui nova cor (uma a uma)'
      OnClick = Incluinovacor1Click
    end
    object Incluinovascoresvriasdeumavez1: TMenuItem
      Caption = 'Inclui novas cores (v'#225'rias de uma vez)'
      OnClick = Incluinovascoresvriasdeumavez1Click
    end
    object Alteracoratual1: TMenuItem
      Caption = '&Altera cor atual'
      OnClick = Alteracoratual1Click
    end
    object Excluicoratual1: TMenuItem
      Caption = '&Exclui cor atual'
      OnClick = Excluicoratual1Click
    end
  end
  object QrGraGruC: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGruCBeforeClose
    AfterScroll = QrGraGruCAfterScroll
    SQL.Strings = (
      'SELECT ggc.Nivel1, ggc.Controle, ggc.GraCorCad,'
      'gtc.CodUsu CodUsuGTC, gtc.Nome NomeGTC, '
      'ggc.GraCmpCad'
      'FROM gragruc ggc'
      'LEFT JOIN gracorcad gtc ON gtc.Codigo=ggc.GraCorCad'
      'WHERE ggc.Nivel1=:P0')
    Left = 48
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruCNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraGruCGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
    end
    object QrGraGruCCodUsuGTC: TIntegerField
      FieldName = 'CodUsuGTC'
      Required = True
    end
    object QrGraGruCNomeGTC: TWideStringField
      FieldName = 'NomeGTC'
      Size = 50
    end
    object QrGraGruCGraCmpCad: TIntegerField
      FieldName = 'GraCmpCad'
      Required = True
    end
  end
  object DsGraGruC: TDataSource
    DataSet = QrGraGruC
    Left = 80
    Top = 408
  end
  object PMAtributos: TPopupMenu
    Left = 448
    Top = 588
    object Incluinovoitemdeatributo1: TMenuItem
      Caption = '&Inclui novo item de atributo'
      OnClick = Incluinovoitemdeatributo1Click
    end
    object Incluidiversosnovositensdeatributo1: TMenuItem
      Caption = 'Inclui diversos novos itens de atributo'
      OnClick = Incluidiversosnovositensdeatributo1Click
    end
    object Alteraitemdeatributoselecionado1: TMenuItem
      Caption = '&Altera item de atributo selecionado'
      OnClick = Alteraitemdeatributoselecionado1Click
    end
    object Excluiitemdeatributoselecionado1: TMenuItem
      Caption = '&Exclui item de atributo selecionado'
      OnClick = Excluiitemdeatributoselecionado1Click
    end
  end
  object DsGraAtrI: TDataSource
    DataSet = QrGraAtrI
    Left = 132
    Top = 628
  end
  object QrGraAtrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gga.GraAtrCad, gga.GraAtrIts,'
      'gga.Controle CTRL_ATR, gai.Controle CTRL_ITS,'
      'gai.CodUsu CODUSU_ITS, gai.Nome NOME_ITS,'
      'gac.CodUsu CODUSU_CAD, gac.Nome NOME_CAD'
      'FROM gragruatr gga'
      'LEFT JOIN graatrits gai ON gai.Controle=gga.GraAtrIts'
      'LEFT JOIN graatrcad gac ON gac.Codigo=gai.Codigo'
      'WHERE Nivel1=:P0'
      'AND gac.Nome LIKE :P1'
      'AND gai.Nome LIKE :P2')
    Left = 104
    Top = 628
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraAtrIGraAtrCad: TIntegerField
      FieldName = 'GraAtrCad'
      Required = True
    end
    object QrGraAtrIGraAtrIts: TIntegerField
      FieldName = 'GraAtrIts'
      Required = True
    end
    object QrGraAtrICTRL_ATR: TIntegerField
      FieldName = 'CTRL_ATR'
      Required = True
    end
    object QrGraAtrICTRL_ITS: TIntegerField
      FieldName = 'CTRL_ITS'
      Required = True
    end
    object QrGraAtrICODUSU_ITS: TIntegerField
      FieldName = 'CODUSU_ITS'
    end
    object QrGraAtrINOME_ITS: TWideStringField
      FieldName = 'NOME_ITS'
      Size = 50
    end
    object QrGraAtrICODUSU_CAD: TIntegerField
      FieldName = 'CODUSU_CAD'
      Required = True
    end
    object QrGraAtrINOME_CAD: TWideStringField
      FieldName = 'NOME_CAD'
      Size = 30
    end
  end
  object QrGraCusPrcU: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrGraCusPrcUAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT cpu.Codigo, gcp.CodUsu, gcp.Nome,'
      'ELT(CustoPreco+1, '#39'Custo'#39', '#39'Pre'#231'o'#39') CusPrc,'
      
        'ELT(TipoCalc+1, '#39'Manual'#39', '#39'Pre'#231'o m'#233'dio'#39', '#39#218'ltima compra'#39') NomeTC' +
        ','
      'mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA'
      'FROM gracusprcu cpu'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=cpu.Codigo'
      'LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda'
      'WHERE Usuario=:P0')
    Left = 104
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCusPrcUCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCusPrcUCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraCusPrcUNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraCusPrcUCusPrc: TWideStringField
      FieldName = 'CusPrc'
      Size = 5
    end
    object QrGraCusPrcUNomeTC: TWideStringField
      FieldName = 'NomeTC'
      Size = 13
    end
    object QrGraCusPrcUNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrGraCusPrcUSIGLAMOEDA: TWideStringField
      FieldName = 'SIGLAMOEDA'
      Size = 5
    end
  end
  object DsGraCusPrcU: TDataSource
    DataSet = QrGraCusPrcU
    Left = 132
    Top = 660
  end
  object PMNumero: TPopupMenu
    Left = 152
    Top = 60
    object Reduzido1: TMenuItem
      Caption = '&Reduzido'
      OnClick = Reduzido1Click
    end
    object IDNivel11: TMenuItem
      Caption = 'ID Nivel 1'
      OnClick = IDNivel11Click
    end
    object CdigoNivel11: TMenuItem
      Caption = 'C'#243'digo Nivel 1'
      OnClick = CdigoNivel11Click
    end
  end
  object PMListaPrecos: TPopupMenu
    Left = 208
    Top = 84
    object Atualizapreomdioeltimacompra1: TMenuItem
      Caption = '&Atualiza pre'#231'o (m'#233'dio e '#250'ltima compra)'
      OnClick = Atualizapreomdioeltimacompra1Click
    end
  end
  object QrGraGruCST: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragrucst'
      'WHERE Nivel1=:P0'
      'ORDER BY UF_Orig, UF_Dest')
    Left = 60
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruCSTNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruCSTUF_Orig: TWideStringField
      FieldName = 'UF_Orig'
      Size = 2
    end
    object QrGraGruCSTUF_Dest: TWideStringField
      FieldName = 'UF_Dest'
      Size = 2
    end
    object QrGraGruCSTCST_B: TSmallintField
      FieldName = 'CST_B'
    end
  end
  object DsGraGruCST: TDataSource
    DataSet = QrGraGruCST
    Left = 88
    Top = 200
  end
  object PMEspecial: TPopupMenu
    Left = 48
    Top = 476
    object Incluinovoitem1: TMenuItem
      Caption = '&Inclui novo item'
      OnClick = Incluinovoitem1Click
    end
    object Alteraitem1: TMenuItem
      Caption = '&Altera item selecionado'
    end
    object Excluiitemselecionado1: TMenuItem
      Caption = '&Exclui item selecionado'
    end
  end
  object QrLocod: TMySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 60
    object QrLocodPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrLocodNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrLocodNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrLocodNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrLocodNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrLocodNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
  end
  object PMQuery: TPopupMenu
    Left = 188
    Top = 28
    object Nivel12: TMenuItem
      Caption = 'Nivel &1'
      OnClick = Nivel12Click
    end
    object Referencia1: TMenuItem
      Caption = '&Refer'#234'ncia'
      OnClick = Referencia1Click
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENTI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NO_ENTI')
    Left = 192
    Top = 172
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENTI: TWideStringField
      FieldName = 'NO_ENTI'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 220
    Top = 172
  end
  object QrGraGruEX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emb.Nome NO_EMBALAG, gge.*,'
      'IF(frn.Tipo=0,frn.RazaoSocial,frn.Nome) NO_FORNECE '
      'FROM gragrueits gge'
      'LEFT JOIN entidades frn ON frn.Codigo=gge.Fornece'
      'LEFT JOIN embalagens emb ON emb.Codigo=gge.Embalagem'
      'WHERE gge.GraGruX=:P0')
    Left = 360
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruEXNO_EMBALAG: TWideStringField
      FieldName = 'NO_EMBALAG'
    end
    object QrGraGruEXcProd: TWideStringField
      FieldName = 'cProd'
      Size = 60
    end
    object QrGraGruEXNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruEXFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrGraGruEXEmbalagem: TIntegerField
      FieldName = 'Embalagem'
    end
    object QrGraGruEXObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrGraGruEXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruEXNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrGraGruEXxProd: TWideStringField
      FieldName = 'xProd'
      Size = 120
    end
    object QrGraGruEXNCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 8
    end
    object QrGraGruEXCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrGraGruEXuCom: TWideStringField
      FieldName = 'uCom'
      Required = True
      Size = 6
    end
    object QrGraGruEXCFOP_Inn: TIntegerField
      FieldName = 'CFOP_Inn'
    end
  end
  object DsGraGruEX: TDataSource
    DataSet = QrGraGruEX
    Left = 356
    Top = 552
  end
  object QrGraGruVinc: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraGruVincAfterOpen
    BeforeClose = QrGraGruVincBeforeClose
    SQL.Strings = (
      'SELECT *'
      'FROM gragruvinc'
      'WHERE GraGruX=:P0')
    Left = 116
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruVincNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGraGruVincGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsGraGruVinc: TDataSource
    DataSet = QrGraGruVinc
    Left = 144
    Top = 228
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraGruXAfterOpen
    BeforeClose = QrGraGruXBeforeClose
    AfterScroll = QrGraGruXAfterScroll
    SQL.Strings = (
      
        'SELECT ggx.Controle, gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCo' +
        'rCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI , gti.Codigo GraTamCad'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'WHERE ggx.GraGru1=:P0'
      'ORDER BY ggx.Controle')
    Left = 60
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrGraGruXGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraGruXGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 88
    Top = 228
  end
  object PMReduzidoL: TPopupMenu
    Left = 528
    Top = 382
    object CadastrodoReduzido1: TMenuItem
      Caption = '&Cadastro do Reduzido'
      OnClick = CadastrodoReduzido1Click
    end
    object Gerenciamentodoreduzido1: TMenuItem
      Caption = '&Gerenciamento do reduzido'
      OnClick = Gerenciamentodoreduzido1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluireduzido1: TMenuItem
      Caption = '&Exclui reduzido'
      OnClick = Excluireduzido1Click
    end
  end
  object QrGraGru5: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru5BeforeClose
    AfterClose = QrGraGru5AfterClose
    AfterScroll = QrGraGru5AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg1.PrdGrupTip, gg5.Codusu, gg5.Nivel5, '
      'gg5.Nome'
      'FROM gragru1 gg1'
      'LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg5.Nome LIKE :P1'
      '')
    Left = 60
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGru5Codusu: TIntegerField
      FieldName = 'Codusu'
    end
    object QrGraGru5Nivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGru5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGru5PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrGraGru4: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru4BeforeClose
    AfterClose = QrGraGru4AfterClose
    AfterScroll = QrGraGru4AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg4.Codusu, gg4.Nivel5, gg4.Nivel4, '
      'gg4.Nome'
      'FROM gragru4 gg4'
      'LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg4.Nivel5'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel4=gg4.Nivel4'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg4.Nivel5=:P1'
      'AND gg4.Nome LIKE :P2'
      '')
    Left = 60
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGru4Codusu: TIntegerField
      FieldName = 'Codusu'
    end
    object QrGraGru4Nivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGru4Nivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGru4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru5: TDataSource
    DataSet = QrGraGru5
    Left = 88
    Top = 264
  end
  object DsGraGru4: TDataSource
    DataSet = QrGraGru4
    Left = 88
    Top = 292
  end
  object QrGraGru1Lau: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM gragru1lau')
    Left = 44
    Top = 528
    object QrGraGru1LauNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1LauControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGru1LauNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrGraGru1LauLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGru1LauDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGru1LauDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGru1LauUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGru1LauUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGru1LauAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGru1LauAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsGraGru1Lau: TDataSource
    DataSet = QrGraGru1Lau
    Left = 44
    Top = 576
  end
  object QrGraGru1Lot: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM gragru1lau')
    Left = 120
    Top = 528
    object QrGraGru1LotNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1LotControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGru1LotNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrGraGru1LotLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGru1LotDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGru1LotDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGru1LotUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGru1LotUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGru1LotAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGru1LotAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsGraGru1Lot: TDataSource
    DataSet = QrGraGru1Lot
    Left = 120
    Top = 576
  end
  object PMGraGru1Lau: TPopupMenu
    Left = 40
    Top = 624
    object Incluinovolaudo1: TMenuItem
      Caption = '&Inclui novo laudo'
      OnClick = Incluinovolaudo1Click
    end
    object Excluilaudoatual1: TMenuItem
      Caption = '&Exclui laudo atual'
      OnClick = Excluilaudoatual1Click
    end
  end
  object PMGraGru1Lot: TPopupMenu
    Left = 40
    Top = 672
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      OnClick = Excluiloteatual1Click
    end
  end
  object PMGraGru5: TPopupMenu
    OnPopup = PMGraGru5Popup
    Left = 440
    Top = 160
    object IncluiGru5: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiGru5Click
    end
    object AlteraGru5: TMenuItem
      Caption = '&Altera'
      OnClick = AlteraGru5Click
    end
    object ExcluiGru5: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiGru5Click
    end
  end
  object PMGraGru4: TPopupMenu
    OnPopup = PMGraGru4Popup
    Left = 468
    Top = 160
    object IncluiGru4: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiGru4Click
    end
    object AlteraGru4: TMenuItem
      Caption = '&Altera'
      OnClick = AlteraGru4Click
    end
    object ExcluiGru4: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiGru4Click
    end
  end
  object PMGraGru3: TPopupMenu
    OnPopup = PMGraGru3Popup
    Left = 496
    Top = 160
    object IncluiGru3: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiGru3Click
    end
    object AlteraGru3: TMenuItem
      Caption = '&Altera'
      OnClick = AlteraGru3Click
    end
    object ExcluiGru3: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiGru3Click
    end
  end
  object PMGraGru2: TPopupMenu
    OnPopup = PMGraGru2Popup
    Left = 524
    Top = 160
    object IncluiGru2: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiGru2Click
    end
    object AlteraGru2: TMenuItem
      Caption = 'Altera descri'#231#227'o'
      OnClick = AlteraGru2Click
    end
    object Alteradados1: TMenuItem
      Caption = '&Altera dados'
      OnClick = Alteradados1Click
    end
    object ExcluiGru2: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiGru2Click
    end
  end
  object PMGraGru1: TPopupMenu
    OnPopup = PMGraGru1Popup
    Left = 552
    Top = 160
    object IncluiGru1: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiGru1Click
    end
    object AlteraGru1: TMenuItem
      Caption = '&Altera dados'
      OnClick = AlteraGru1Click
    end
    object AlteraGradedeTamanhos1: TMenuItem
      Caption = 'Altera &grade de tamanhos'
      OnClick = AlteraGradedeTamanhos1Click
    end
    object ExcluiGru1: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiGru1Click
    end
  end
  object PMCodificacao: TPopupMenu
    OnPopup = PMCodificacaoPopup
    Left = 408
    Top = 292
    object GerenciarcodificaodeFornecedor1: TMenuItem
      Caption = 'Gerenciar codifica'#231#227'o de &Fornecedor'
      OnClick = GerenciarcodificaodeFornecedor1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Gerenciarcodificaoprpria1: TMenuItem
      Caption = '&Gerenciar codifica'#231#227'o pr'#243'pria'
      OnClick = Gerenciarcodificaoprpria1Click
    end
  end
  object PMGrupo: TPopupMenu
    OnPopup = PMGrupoPopup
    Left = 132
    Top = 728
    object IncluiProduto1: TMenuItem
      Caption = '&Inclui Produto'
      OnClick = IncluiProduto1Click
    end
    object AlteraProduto1: TMenuItem
      Caption = '&AlteraProduto'
      OnClick = AlteraProduto1Click
    end
  end
  object QrTabePrCCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM tabeprccab'#11
      'WHERE Ativo = 1'
      'AND Codigo <> 0')
    Left = 524
    Top = 512
    object QrTabePrCCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTabePrCCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTabePrCCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsTabePrCCab: TDataSource
    DataSet = QrTabePrCCab
    Left = 552
    Top = 512
  end
  object PMGraGru1Cons: TPopupMenu
    OnPopup = PMGraGru1ConsPopup
    Left = 540
    Top = 868
    object IncluiConsumoEP1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiConsumoEP1Click
    end
    object AlteraConsumoEP1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraConsumoEP1Click
    end
    object ExcluiConsumoEP1: TMenuItem
      Caption = '&Exclui item'
      OnClick = ExcluiConsumoEP1Click
    end
  end
  object QrGraGru1Cons: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_COD_ITEM_COMP, g1c.* '
      'FROM gragru1cons g1c'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=g1c.COD_ITEM_COMP'
      'WHERE g1c.Nivel1=1'
      'ORDER BY NO_COD_ITEM_COMP')
    Left = 208
    Top = 528
    object QrGraGru1ConsNO_Niv1_COMP: TWideStringField
      FieldName = 'NO_Niv1_COMP'
      Size = 120
    end
    object QrGraGru1ConsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1ConsNiv1_COMP: TIntegerField
      FieldName = 'Niv1_COMP'
    end
    object QrGraGru1ConsQtd_Comp: TFloatField
      FieldName = 'Qtd_Comp'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrGraGru1ConsPerda: TFloatField
      FieldName = 'Perda'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrGraGru1ConsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGru1ConsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGru1ConsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGru1ConsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGru1ConsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGru1ConsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGru1ConsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGru1ConsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsGraGru1Cons: TDataSource
    DataSet = QrGraGru1Cons
    Left = 208
    Top = 576
  end
  object QrGraGruYPGT: TMySQLQuery
    Database = Dmod.MyDB
    Left = 708
    Top = 440
  end
  object DsGraGruYPGT: TDataSource
    DataSet = QrGraGruYPGT
    Left = 708
    Top = 488
  end
  object PMFichaDeConsumo: TPopupMenu
    OnPopup = PMFichaDeConsumoPopup
    Left = 713
    Top = 752
    object Inclui1: TMenuItem
      Caption = 'Item &B'#225'sico'
      object BasicoInclui1: TMenuItem
        Caption = '&Inclui'
        OnClick = BasicoInclui1Click
      end
      object BasicoAltera1: TMenuItem
        Caption = '&Altera'
        OnClick = BasicoAltera1Click
      end
      object BasicoExclui1: TMenuItem
        Caption = '&Exclui'
        OnClick = BasicoExclui1Click
      end
    end
    object ItemNobsico1: TMenuItem
      Caption = 'Item &N'#227'o b'#225'sico'
      OnClick = ItemNobsico1Click
    end
  end
  object QrFiConsBas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fcb.*,'
      ''
      'CONCAT(gg1a.Nome, '
      
        'IF(gtia.PrintTam=0 OR gtia.Codigo IS NULL, "", CONCAT(" ", gtia.' +
        'Nome)), '
      
        'IF(gcca.PrintCor=0 OR gcca.Codigo IS NULL, "", CONCAT(" ", gcca.' +
        'Nome))) '
      'GGXSorc_NO_PRD_TAM_COR,'
      ''
      'CONCAT(gg1b.Nome, '
      
        'IF(gtib.PrintTam=0 OR gtib.Codigo IS NULL, "", CONCAT(" ", gtib.' +
        'Nome)), '
      
        'IF(gccb.PrintCor=0 OR gccb.Codigo IS NULL, "", CONCAT(" ", gccb.' +
        'Nome))) '
      'GGXSubs_NO_PRD_TAM_COR,'
      ''
      
        'gpc.Nome NO_Parte, IF(fcb.Obrigatorio=0, "N'#227'o", "Sim") NO_Obriga' +
        'torio'
      ''
      'FROM ficonsbas fcb'
      ''
      'LEFT JOIN gragrux    ggxa ON ggxa.Controle=fcb.GGXSorc'
      'LEFT JOIN gragruc    ggca ON ggca.Controle=ggxa.GraGruC '
      'LEFT JOIN gracorcad  gcca ON gcca.Codigo=ggca.GraCorCad '
      'LEFT JOIN gratamits  gtia ON gtia.Controle=ggxa.GraTamI '
      'LEFT JOIN gragru1    gg1a ON gg1a.Nivel1=ggxa.GraGru1'
      ''
      'LEFT JOIN gragrux    ggxb ON ggxb.Controle=fcb.GGXSubs'
      'LEFT JOIN gragruc    ggcb ON ggcb.Controle=ggxb.GraGruC '
      'LEFT JOIN gracorcad  gccb ON gccb.Codigo=ggcb.GraCorCad '
      'LEFT JOIN gratamits  gtib ON gtib.Controle=ggxb.GraTamI '
      'LEFT JOIN gragru1    gg1b ON gg1b.Nivel1=ggxb.GraGru1'
      ''
      'LEFT JOIN graparcad  gpc  ON gpc.Codigo=fcb.Parte '
      'WHERE fcb.GG1Dest>0')
    Left = 333
    Top = 636
    object QrFiConsBasGG1Dest: TIntegerField
      FieldName = 'GG1Dest'
      Required = True
    end
    object QrFiConsBasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFiConsBasGGXSorc: TIntegerField
      FieldName = 'GGXSorc'
      Required = True
    end
    object QrFiConsBasGGXSubs: TIntegerField
      FieldName = 'GGXSubs'
      Required = True
    end
    object QrFiConsBasParte: TIntegerField
      FieldName = 'Parte'
      Required = True
    end
    object QrFiConsBasQtdUso: TFloatField
      FieldName = 'QtdUso'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrFiConsBasObrigatorio: TSmallintField
      FieldName = 'Obrigatorio'
      Required = True
    end
    object QrFiConsBasLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrFiConsBasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFiConsBasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFiConsBasUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrFiConsBasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrFiConsBasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFiConsBasAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFiConsBasAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFiConsBasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFiConsBasGGXSorc_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'GGXSorc_NO_PRD_TAM_COR'
      Size = 157
    end
    object QrFiConsBasGGXSubs_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'GGXSubs_NO_PRD_TAM_COR'
      Size = 157
    end
    object QrFiConsBasNO_Parte: TWideStringField
      FieldName = 'NO_Parte'
      Size = 60
    end
    object QrFiConsBasNO_Obrigatorio: TWideStringField
      FieldName = 'NO_Obrigatorio'
      Required = True
      Size = 3
    end
  end
  object DsFiConsBas: TDataSource
    DataSet = QrFiConsBas
    Left = 333
    Top = 684
  end
  object QrFiConsOri: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFiConsOriAfterScroll
    SQL.Strings = (
      'SELECT fcb.*, '
      ' '
      'CONCAT(gg1a.Nome,  '
      
        'IF(gtia.PrintTam=0 OR gtia.Codigo IS NULL, "", CONCAT(" ", gtia.' +
        'Nome)),  '
      
        'IF(gcca.PrintCor=0 OR gcca.Codigo IS NULL, "", CONCAT(" ", gcca.' +
        'Nome)))  '
      'GGXSorc_NO_PRD_TAM_COR, unma.Sigla Sorc_SIGLAUNIDMED, '
      'unma.CodUsu Sorc_CODUSUUNIDMED, unma.Nome Sorc_NOMEUNIDMED,'
      'ggca.GraCorCad Sorc_GraCorCad, ggxa.GraTamI Sorc_GraTamI,'
      'ggxa.GraGru1 Sorc_GraGru1,'
      ' '
      'CONCAT(gg1b.Nome,  '
      
        'IF(gtib.PrintTam=0 OR gtib.Codigo IS NULL, "", CONCAT(" ", gtib.' +
        'Nome)),  '
      
        'IF(gccb.PrintCor=0 OR gccb.Codigo IS NULL, "", CONCAT(" ", gccb.' +
        'Nome)))  '
      'GGXSubs_NO_PRD_TAM_COR, unmb.Sigla Subs_SIGLAUNIDMED, '
      'unmb.CodUsu Subs_CODUSUUNIDMED, unmb.Nome Subs_NOMEUNIDMED,'
      ' '
      
        'gpc.Nome NO_Parte, IF(fcb.Obrigatorio=0, "N'#227'o", "Sim") NO_Obriga' +
        'torio '
      ' '
      'FROM ficonsori fcb '
      ' '
      'LEFT JOIN gragrux    ggxa ON ggxa.Controle=fcb.GGXSorc '
      'LEFT JOIN gragruc    ggca ON ggca.Controle=ggxa.GraGruC  '
      'LEFT JOIN gracorcad  gcca ON gcca.Codigo=ggca.GraCorCad  '
      'LEFT JOIN gratamits  gtia ON gtia.Controle=ggxa.GraTamI  '
      'LEFT JOIN gragru1    gg1a ON gg1a.Nivel1=ggxa.GraGru1 '
      'LEFT JOIN unidmed    unma ON unma.Codigo=gg1a.UnidMed '
      ''
      'LEFT JOIN gragrux    ggxb ON ggxb.Controle=fcb.GGXSubs '
      'LEFT JOIN gragruc    ggcb ON ggcb.Controle=ggxb.GraGruC  '
      'LEFT JOIN gracorcad  gccb ON gccb.Codigo=ggcb.GraCorCad  '
      'LEFT JOIN gratamits  gtib ON gtib.Controle=ggxb.GraTamI  '
      'LEFT JOIN gragru1    gg1b ON gg1b.Nivel1=ggxb.GraGru1 '
      'LEFT JOIN unidmed    unmb ON unmb.Codigo=gg1b.UnidMed '
      ' '
      'LEFT JOIN graparcad  gpc  ON gpc.Codigo=fcb.Parte  '
      'WHERE fcb.GG1Dest>0')
    Left = 401
    Top = 636
    object QrFiConsOriGG1Dest: TIntegerField
      FieldName = 'GG1Dest'
      Required = True
    end
    object QrFiConsOriControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFiConsOriGGXSorc: TIntegerField
      FieldName = 'GGXSorc'
      Required = True
    end
    object QrFiConsOriGGXSubs: TIntegerField
      FieldName = 'GGXSubs'
      Required = True
    end
    object QrFiConsOriParte: TIntegerField
      FieldName = 'Parte'
      Required = True
    end
    object QrFiConsOriObrigatorio: TSmallintField
      FieldName = 'Obrigatorio'
      Required = True
    end
    object QrFiConsOriGGXSorc_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'GGXSorc_NO_PRD_TAM_COR'
      Size = 157
    end
    object QrFiConsOriSorc_SIGLAUNIDMED: TWideStringField
      FieldName = 'Sorc_SIGLAUNIDMED'
      Size = 6
    end
    object QrFiConsOriSorc_CODUSUUNIDMED: TIntegerField
      FieldName = 'Sorc_CODUSUUNIDMED'
    end
    object QrFiConsOriSorc_NOMEUNIDMED: TWideStringField
      FieldName = 'Sorc_NOMEUNIDMED'
      Size = 30
    end
    object QrFiConsOriGGXSubs_NO_PRD_TAM_COR: TWideStringField
      FieldName = 'GGXSubs_NO_PRD_TAM_COR'
      Size = 157
    end
    object QrFiConsOriSubs_SIGLAUNIDMED: TWideStringField
      FieldName = 'Subs_SIGLAUNIDMED'
      Size = 6
    end
    object QrFiConsOriSubs_CODUSUUNIDMED: TIntegerField
      FieldName = 'Subs_CODUSUUNIDMED'
    end
    object QrFiConsOriSubs_NOMEUNIDMED: TWideStringField
      FieldName = 'Subs_NOMEUNIDMED'
      Size = 30
    end
    object QrFiConsOriNO_Parte: TWideStringField
      FieldName = 'NO_Parte'
      Size = 60
    end
    object QrFiConsOriNO_Obrigatorio: TWideStringField
      FieldName = 'NO_Obrigatorio'
      Required = True
      Size = 3
    end
    object QrFiConsOriSorc_GraCorCad: TIntegerField
      FieldName = 'Sorc_GraCorCad'
    end
    object QrFiConsOriSorc_GraTamI: TIntegerField
      FieldName = 'Sorc_GraTamI'
    end
    object QrFiConsOriSorc_GraGru1: TIntegerField
      FieldName = 'Sorc_GraGru1'
    end
  end
  object DsFiConsOri: TDataSource
    DataSet = QrFiConsOri
    Left = 401
    Top = 684
  end
  object QrGraPckCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraPckCadBeforeClose
    AfterScroll = QrGraPckCadAfterScroll
    SQL.Strings = (
      'SELECT gpk.*, gg1.Nome NO_GraGru1'
      'FROM grapckcad gpk'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=gpk.GraGru1')
    Left = 472
    Top = 637
    object QrGraPckCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraPckCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraPckCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGraPckCadGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
  end
  object DsGraPckCad: TDataSource
    DataSet = QrGraPckCad
    Left = 472
    Top = 685
  end
  object QrGraCmpCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraCmpCadBeforeClose
    AfterScroll = QrGraCmpCadAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM gracmpcad'
      'WHERE Codigo > 0')
    Left = 548
    Top = 637
    object QrGraCmpCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCmpCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraCmpCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGraCmpCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraCmpCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraCmpCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraCmpCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraCmpCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraCmpCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraCmpCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraCmpCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraCmpCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsGraCmpCad: TDataSource
    DataSet = QrGraCmpCad
    Left = 548
    Top = 681
  end
  object QrGraCmpPar: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraCmpParBeforeClose
    AfterScroll = QrGraCmpParAfterScroll
    SQL.Strings = (
      'SELECT gpc.Nome NO_Parte, gcp.*'
      'FROM gracmppar gcp'
      'LEFT JOIN GraParCad gpc ON gpc.Codigo=gcp.Parte'
      'WHERE gcp.Codigo=:P0'
      'ORDER BY gcp.Controle')
    Left = 624
    Top = 637
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCmpParNO_Parte: TWideStringField
      FieldName = 'NO_Parte'
      Size = 60
    end
    object QrGraCmpParCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCmpParControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraCmpParParte: TIntegerField
      FieldName = 'Parte'
      Required = True
    end
    object QrGraCmpParLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraCmpParDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraCmpParDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraCmpParUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraCmpParUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraCmpParAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraCmpParAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraCmpParAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraCmpParAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrGraCmpParPercTot: TFloatField
      FieldName = 'PercTot'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpParPercImp: TFloatField
      FieldName = 'PercImp'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpParPercNoI: TFloatField
      FieldName = 'PercNoI'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpParQtdeTot: TIntegerField
      FieldName = 'QtdeTot'
      DisplayFormat = '0;-0; '
    end
    object QrGraCmpParQtdeImp: TIntegerField
      FieldName = 'QtdeImp'
      DisplayFormat = '0;-0; '
    end
    object QrGraCmpParQtdeNoI: TIntegerField
      FieldName = 'QtdeNoI'
      DisplayFormat = '0;-0; '
    end
  end
  object DsGraCmpPar: TDataSource
    DataSet = QrGraCmpPar
    Left = 624
    Top = 681
  end
  object QrGraCmpFib: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gfc.Nome NO_Fibra, gcf.*'
      'FROM gracmpfib gcf'
      'LEFT JOIN GraFIbCad gfc ON gfc.Codigo=gcf.Fibra'
      'WHERE gcf.Codigo=:P0'
      'ORDER BY gcf.Controle')
    Left = 700
    Top = 637
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCmpFibNO_Fibra: TWideStringField
      FieldName = 'NO_Fibra'
      Size = 30
    end
    object QrGraCmpFibCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCmpFibControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraCmpFibConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrGraCmpFibFibra: TIntegerField
      FieldName = 'Fibra'
      Required = True
    end
    object QrGraCmpFibPercComp: TFloatField
      FieldName = 'PercComp'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpFibImprime: TSmallintField
      FieldName = 'Imprime'
      Required = True
    end
    object QrGraCmpFibLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraCmpFibDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraCmpFibDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraCmpFibUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraCmpFibUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraCmpFibAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraCmpFibAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraCmpFibAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraCmpFibAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrGraCmpFibNO_Imprime: TWideStringField
      FieldName = 'NO_Imprime'
      Size = 3
    end
  end
  object DsGraCmpFib: TDataSource
    DataSet = QrGraCmpFib
    Left = 700
    Top = 681
  end
  object PmComposicao: TPopupMenu
    OnPopup = PmComposicaoPopup
    Left = 809
    Top = 785
    object Janeladecriaoalterao1: TMenuItem
      Caption = '&Janela de cria'#231#227'o / altera'#231#227'o'
      OnClick = Janeladecriaoalterao1Click
    end
    object Atrelaexistentecorselecionada1: TMenuItem
      Caption = 'Atrela existente '#224' cor selecionada'
      OnClick = Atrelaexistentecorselecionada1Click
    end
  end
end
