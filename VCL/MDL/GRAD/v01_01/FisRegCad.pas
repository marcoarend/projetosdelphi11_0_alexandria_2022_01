unit FisRegCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,  
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids,
  dmkDBGrid, Menus, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox,
  dmkMemo, dmkValUsu, ComCtrls, DmkDAC_PF, dmkImage, UnDmkEnums, dmkDBGridZTO,
  SPED_Listas;

type
  THackCustomGrid = class(TCustomGrid);
  TFmFisRegCad = class(TForm)
    PainelDados: TPanel;
    DsFisRegCad: TDataSource;
    QrFisRegCad: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PMRegra: TPopupMenu;
    DsFisRegMvt: TDataSource;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadTipoMov: TSmallintField;
    QrFisRegCadICMS_Usa: TSmallintField;
    QrFisRegCadICMS_Frm: TWideMemoField;
    QrFisRegCadIPI_Usa: TSmallintField;
    QrFisRegCadIPI_Frm: TWideMemoField;
    QrFisRegCadPIS_Usa: TSmallintField;
    QrFisRegCadPIS_Frm: TWideMemoField;
    QrFisRegCadCOFINS_Usa: TSmallintField;
    QrFisRegCadCOFINS_Frm: TWideMemoField;
    QrFisRegCadIR_Usa: TSmallintField;
    QrFisRegCadIR_Alq: TFloatField;
    QrFisRegCadIR_Frm: TWideMemoField;
    QrFisRegCadCS_Usa: TSmallintField;
    QrFisRegCadCS_Alq: TFloatField;
    QrFisRegCadCS_Frm: TWideMemoField;
    QrFisRegCadISS_Usa: TSmallintField;
    QrFisRegCadISS_Alq: TFloatField;
    QrFisRegCadISS_Frm: TWideMemoField;
    QrFisRegCadAplicacao: TSmallintField;
    QrFisRegCadFinanceiro: TSmallintField;
    QrFisRegCadTipoEmiss: TSmallintField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadAgrupador: TIntegerField;
    QrFisRegCadObserva: TWideStringField;
    QrFisRegMvt: TmySQLQuery;
    QrFisRegMvtCodigo: TIntegerField;
    QrFisRegMvtControle: TIntegerField;
    QrFisRegMvtEmpresa: TIntegerField;
    QrFisRegMvtStqCenCad: TIntegerField;
    QrFisRegMvtTipoMov: TSmallintField;
    QrFisRegCadNOMETIPOMOV: TWideStringField;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdTipoMov: TdmkEditCB;
    CBTipoMov: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    QrImprime: TmySQLQuery;
    DsImprime: TDataSource;
    QrImprimeCodigo: TIntegerField;
    QrImprimeNome: TWideStringField;
    QrFisAgrCad: TmySQLQuery;
    QrFisAgrCadCodigo: TIntegerField;
    QrFisAgrCadCodUsu: TIntegerField;
    QrFisAgrCadNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    DsFisAgrCad: TDataSource;
    QrFisRegCadNOMEMODELONF: TWideStringField;
    QrFisRegCadNOMEAGRUPADOR: TWideStringField;
    QrFisRegCadCODUSU_AGRUPADOR: TIntegerField;
    QrFisRegMvtTipoCalc: TSmallintField;
    QrFisRegMvtNOME_EMP: TWideStringField;
    QrFisRegMvtNOME_StqCenCad: TWideStringField;
    QrFisRegMvtNOME_TIPOCALC: TWideStringField;
    QrFisRegMvtNOME_TIPOMOV: TWideStringField;
    QrFisRegMvtSEQ: TIntegerField;
    QrFisRegMvtFILIAL: TIntegerField;
    QrFisRegMvtCODUSU_STQCENCAD: TIntegerField;
    PMMovimentacao: TPopupMenu;
    Incluiitemdemovimentao1: TMenuItem;
    Alteraitemdemovimentaoatual1: TMenuItem;
    Excluiitemnsdemovimentao1: TMenuItem;
    QrFisRegCFOP: TmySQLQuery;
    DsFisRegCFOP: TDataSource;
    QrFisRegCFOPCodigo: TIntegerField;
    QrFisRegCFOPInterno: TSmallintField;
    QrFisRegCFOPContribui: TSmallintField;
    QrFisRegCFOPProprio: TSmallintField;
    QrFisRegCFOPCFOP: TWideStringField;
    QrFisRegCFOPNO_CFOP: TWideStringField;
    QrFisRegCFOPOrdCFOPGer: TIntegerField;
    QrFisRegMvtCU_GraCusPrc: TIntegerField;
    QrFisRegMvtNO_GraCusPrc: TWideStringField;
    QrFisRegMvtGraCusApl: TSmallintField;
    QrFisRegMvtGraCusPrc: TIntegerField;
    QrFisRegMvtNO_GraCusApl: TWideStringField;
    PMCFOP: TPopupMenu;
    IncluinovoCFOP1: TMenuItem;
    AlteraCFOPatual1: TMenuItem;
    RetiraCFOPAtual1: TMenuItem;
    QrFisRegCadInfAdFisco: TWideStringField;
    QrFisRegUFs: TmySQLQuery;
    QrFisRegUFsCodigo: TIntegerField;
    QrFisRegUFsInterno: TSmallintField;
    QrFisRegUFsContribui: TSmallintField;
    QrFisRegUFsProprio: TSmallintField;
    QrFisRegUFsUFEmit: TWideStringField;
    QrFisRegUFsUFDest: TWideStringField;
    QrFisRegUFsICMSAliq: TFloatField;
    DsFisRegUFs: TDataSource;
    PMEmitDest: TPopupMenu;
    Incluiitemdealquotas1: TMenuItem;
    Excluiumseltodos1: TMenuItem;
    Label17: TLabel;
    DBEdit4: TDBEdit;
    QrFisRegCadICMS_Alq: TFloatField;
    QrFisRegCadIPI_Alq: TFloatField;
    QrFisRegCadPIS_Alq: TFloatField;
    QrFisRegCadCOFINS_Alq: TFloatField;
    QrFisRegCadide_natOp: TWideStringField;
    Edide_natOp: TdmkEdit;
    Label18: TLabel;
    QrFisRegCFOPServico: TSmallintField;
    SpeedButton12: TSpeedButton;
    Incluinovaregra1: TMenuItem;
    Alteraregraatual1: TMenuItem;
    Excluiregraatual1: TMenuItem;
    QrFisRegUFsCST_B: TWideStringField;
    QrFisRegUFspRedBC: TFloatField;
    QrFisRegUFsmodBC: TSmallintField;
    QrFisRegUFsModBC_TXT: TWideStringField;
    QrFisRegUFx: TmySQLQuery;
    DsFisRegUFx: TDataSource;
    QrFisRegUFxCodigo: TIntegerField;
    QrFisRegUFxInterno: TSmallintField;
    QrFisRegUFxContribui: TSmallintField;
    QrFisRegUFxProprio: TSmallintField;
    QrFisRegUFxUFEmit: TWideStringField;
    QrFisRegUFxUFDest: TWideStringField;
    QrFisRegUFxNivel: TSmallintField;
    QrFisRegUFxCodNiv: TIntegerField;
    QrFisRegUFxICMSAliq: TFloatField;
    QrFisRegUFxCST_B: TWideStringField;
    QrFisRegUFxpRedBC: TFloatField;
    QrFisRegUFxmodBC: TSmallintField;
    PMExcecao: TPopupMenu;
    Incluinovaexceo1: TMenuItem;
    Alteraexceoatual1: TMenuItem;
    Excluiexceoatual1: TMenuItem;
    QrFisRegUFxModBC_TXT: TWideStringField;
    QrFisRegCadII_Usa: TSmallintField;
    QrFisRegCadII_Alq: TFloatField;
    QrFisRegCadII_Frm: TWideMemoField;
    PMNatOper: TPopupMenu;
    Descrio1: TMenuItem;
    Cdigo1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel13: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtRegra: TBitBtn;
    BtMovimentacao: TBitBtn;
    BtCFOP: TBitBtn;
    BtEmitDest: TBitBtn;
    BtExcecao: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel15: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrFisRegCadTpCalcTrib: TSmallintField;
    QrFisRegUFspBCUFDest: TFloatField;
    QrFisRegUFspFCPUFDest: TFloatField;
    QrFisRegUFspICMSUFDest: TFloatField;
    QrFisRegUFspICMSInter: TFloatField;
    QrFisRegUFspICMSInterPart: TFloatField;
    Splitter2: TSplitter;
    PageControl3: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet5: TTabSheet;
    PnTributacao: TPanel;
    GroupBox8: TGroupBox;
    dmkCheckBox1: TDBCheckBox;
    dmkCheckBox2: TDBCheckBox;
    dmkCheckBox3: TDBCheckBox;
    dmkCheckBox4: TDBCheckBox;
    dmkCheckBox5: TDBCheckBox;
    dmkCheckBox6: TDBCheckBox;
    dmkCheckBox7: TDBCheckBox;
    DBCheckBox1: TDBCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Splitter1: TSplitter;
    Panel9: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    DBGFisRegCFOP2: TdmkDBGridZTO;
    Panel1: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    dmkDBGrid1: TdmkDBGridZTO;
    DBGFisRegUFs: TdmkDBGridZTO;
    TabSheet2: TTabSheet;
    DBMemo_01: TDBMemo;
    DBMemo_02: TDBMemo;
    DBMemo_03: TDBMemo;
    DBMemo_04: TDBMemo;
    DBMemo_05: TDBMemo;
    DBMemo_06: TDBMemo;
    DBMemo_07: TDBMemo;
    DBMemo3: TDBMemo;
    GroupBox9: TGroupBox;
    DBEdit_01: TDBEdit;
    DBEdit_02: TDBEdit;
    DBEdit_03: TDBEdit;
    DBEdit_04: TDBEdit;
    DBEdit_05: TDBEdit;
    DBEdit_06: TDBEdit;
    DBEdit_07: TDBEdit;
    DBEdit5: TDBEdit;
    Panel16: TPanel;
    DBRadioGroup3: TDBRadioGroup;
    DBRadioGroup4: TDBRadioGroup;
    DBGTitle1: TDBGrid;
    DBGFisRegMvt: TDBGrid;
    Panel4: TPanel;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    Panel14: TPanel;
    GroupBox6: TGroupBox;
    DBMemo1: TDBMemo;
    GroupBox10: TGroupBox;
    DBMemo2: TDBMemo;
    Panel8: TPanel;
    Label3: TLabel;
    Label16: TLabel;
    DBEdit100: TDBEdit;
    DBEdit101: TDBEdit;
    DBEdit102: TDBEdit;
    DBEdit103: TDBEdit;
    Splitter3: TSplitter;
    QrFisRegUFsUsaInterPartLei: TSmallintField;
    Alteraitemdealiquotas1: TMenuItem;
    DBGFisRegCFOP: TdmkDBGrid;
    PMQuery: TPopupMenu;
    Descrio2: TMenuItem;
    CFOP1: TMenuItem;
    QrFisRegUFxcBenef: TWideStringField;
    QrFisRegUFscBenef: TWideStringField;
    QrFisRegUFxpDif: TFloatField;
    QrFisRegUFspDif: TFloatField;
    TabSheet6: TTabSheet;
    BtEstricao: TBitBtn;
    PMEstricao: TPopupMenu;
    Incluientidade1: TMenuItem;
    Excluientidade1: TMenuItem;
    DBGFisRegEnt: TDBGrid;
    QrFisRegEnt: TMySQLQuery;
    DsFisRegEnt: TDataSource;
    QrFisRegEntNO_ENT: TWideStringField;
    QrFisRegEntCodigo: TIntegerField;
    QrFisRegEntEntidade: TIntegerField;
    QrFisRegEntLk: TIntegerField;
    QrFisRegEntDataCad: TDateField;
    QrFisRegEntDataAlt: TDateField;
    QrFisRegEntUserCad: TIntegerField;
    QrFisRegEntUserAlt: TIntegerField;
    QrFisRegEntAlterWeb: TSmallintField;
    QrFisRegEntAWServerID: TIntegerField;
    QrFisRegEntAWStatSinc: TSmallintField;
    QrFisRegEntAtivo: TSmallintField;
    QrFisRegEntObservacao: TWideStringField;
    QrFisRegCFOPSubsTrib: TSmallintField;
    QrFisRegUFsSubsTrib: TSmallintField;
    QrFisRegUFxSubsTrib: TSmallintField;
    QrFisRegUFsCSOSN: TWideStringField;
    QrFisRegUFxCSOSN: TWideStringField;
    QrCtbCadMoF: TMySQLQuery;
    QrCtbCadMoFCodigo: TIntegerField;
    QrCtbCadMoFNome: TWideStringField;
    DsCtbCadMoF: TDataSource;
    QrFisRegCadCtbCadMoF: TIntegerField;
    QrFisRegCadNO_CtbCadMoF: TWideStringField;
    QrFisRegCadCtbPlaCta: TIntegerField;
    QrFisRegCadNO_CtbPlaCta: TWideStringField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet7: TTabSheet;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    CkICMS_Usa: TdmkCheckBox;
    CkIPI_Usa: TdmkCheckBox;
    CkPIS_Usa: TdmkCheckBox;
    CkCOFINS_Usa: TdmkCheckBox;
    CkIR_Usa: TdmkCheckBox;
    CkCS_Usa: TdmkCheckBox;
    CkISS_Usa: TdmkCheckBox;
    CkII_Usa: TdmkCheckBox;
    GroupBox5: TGroupBox;
    EdICMS_Alq: TdmkEdit;
    EdIPI_Alq: TdmkEdit;
    EdPIS_Alq: TdmkEdit;
    EdCOFINS_Alq: TdmkEdit;
    EdIR_Alq: TdmkEdit;
    EdCS_Alq: TdmkEdit;
    EdISS_Alq: TdmkEdit;
    EdII_Alq: TdmkEdit;
    PageControl4: TPageControl;
    TabSheet8: TTabSheet;
    Panel10: TPanel;
    SpeedButton11: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton13: TSpeedButton;
    EdICMS_Frm: TdmkEdit;
    EdIPI_Frm: TdmkEdit;
    EdPIS_Frm: TdmkEdit;
    EdCOFINS_Frm: TdmkEdit;
    EdIR_Frm: TdmkEdit;
    EdCS_Frm: TdmkEdit;
    EdISS_Frm: TdmkEdit;
    EdII_Frm: TdmkEdit;
    RGAplicacao: TdmkRadioGroup;
    RGTpCalcTrib: TdmkRadioGroup;
    Panel6: TPanel;
    RGFinanceiro: TdmkRadioGroup;
    RGTipoEmiss: TdmkRadioGroup;
    Panel7: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    SbAgrupador: TSpeedButton;
    SBModeloNF: TSpeedButton;
    EdModeloNF: TdmkEditCB;
    CBModeloNF: TdmkDBLookupComboBox;
    EdAgrupador: TdmkEditCB;
    CBAgrupador: TdmkDBLookupComboBox;
    Panel18: TPanel;
    StaticText3: TStaticText;
    dmkMemo1: TdmkMemo;
    StaticText1: TStaticText;
    MeInfAdFisco: TdmkMemo;
    Label23: TLabel;
    DBEdit7: TDBEdit;
    QrFisRegCFOPNO_OriCFOP: TWideStringField;
    QrFisRegCFOPOriCFOP: TWideStringField;
    QrFisRegUFsOriCST_ICMS: TWideStringField;
    QrFisRegUFsOriCST_IPI: TWideStringField;
    QrFisRegUFsOriCST_PIS: TWideStringField;
    QrFisRegUFsOriCST_COFINS: TWideStringField;
    QrFisRegUFsGenCtbD: TIntegerField;
    QrFisRegUFsGenCtbC: TIntegerField;
    QrFisRegCFOPGenCtbD: TIntegerField;
    QrFisRegCFOPGenCtbC: TIntegerField;
    QrPlaAllCad_D: TMySQLQuery;
    DsPlaAllCad_D: TDataSource;
    QrPlaAllCad_C: TMySQLQuery;
    DsPlaAllCad_C: TDataSource;
    QrFisRegCadGenCtbD: TIntegerField;
    QrFisRegCadGenCtbC: TIntegerField;
    QrFisRegUFsTES_ICMS: TSmallintField;
    QrFisRegUFsTES_IPI: TSmallintField;
    QrFisRegUFsTES_PIS: TSmallintField;
    QrFisRegUFsTES_COFINS: TSmallintField;
    QrFisRegUFsPISAliq: TFloatField;
    QrFisRegUFsCOFINSAliq: TFloatField;
    QrFisRegCFOPCtbCadMoF: TIntegerField;
    QrFisRegCFOPCtbPlaCta: TIntegerField;
    QrFisRegCFOPLk: TIntegerField;
    QrFisRegCFOPDataCad: TDateField;
    QrFisRegCFOPDataAlt: TDateField;
    QrFisRegCFOPUserCad: TIntegerField;
    QrFisRegCFOPUserAlt: TIntegerField;
    QrFisRegCFOPAlterWeb: TSmallintField;
    QrFisRegCFOPAWServerID: TIntegerField;
    QrFisRegCFOPAWStatSinc: TSmallintField;
    QrFisRegCFOPAtivo: TSmallintField;
    DBEdit6: TDBEdit;
    Label22: TLabel;
    QrFisRegCadNO_GenCtbD: TWideStringField;
    QrFisRegCadNO_GenCtbC: TWideStringField;
    QrFisRegUFsServico: TSmallintField;
    QrFisRegUFsOriCFOP: TWideStringField;
    QrFisRegUFsLk: TIntegerField;
    QrFisRegUFsDataCad: TDateField;
    QrFisRegUFsDataAlt: TDateField;
    QrFisRegUFsUserCad: TIntegerField;
    QrFisRegUFsUserAlt: TIntegerField;
    QrFisRegUFsAlterWeb: TSmallintField;
    QrFisRegUFsAWServerID: TIntegerField;
    QrFisRegUFsAWStatSinc: TSmallintField;
    QrFisRegUFsAtivo: TSmallintField;
    QrFisRegCadInfoEFD_PisCofins: TSmallintField;
    QrFisRegCadCreDeb_PisCofins: TSmallintField;
    QrCFOP_Frete: TMySQLQuery;
    QrCFOP_FreteCodigo: TLargeintField;
    QrCFOP_FreteNome: TWideStringField;
    DsCFOP_Frete: TDataSource;
    TabSheet9: TTabSheet;
    Panel20: TPanel;
    Label66: TLabel;
    EdFrtInnCFOP: TdmkEditCB;
    CBFrtInnCFOP: TdmkDBLookupComboBox;
    SbFrtInnCFOP: TSpeedButton;
    Label476: TLabel;
    EdFrtInnICMS_CST: TdmkEdit;
    Label25: TLabel;
    EdFrtInnPIS_CST: TdmkEdit;
    Label480: TLabel;
    EdFrtInnCOFINS_CST: TdmkEdit;
    Label26: TLabel;
    EdFrtInnIND_NAT_FRT: TdmkEdit;
    EdFrtInnNAT_BC_CRED: TdmkEdit;
    EdFrtInnIND_NAT_FRT_TXT: TdmkEdit;
    EdFrtInnNAT_BC_CRED_TXT: TdmkEdit;
    EdFrtInnALIQ_ICMS: TdmkEdit;
    EdFrtInnICMS_CST_TXT: TdmkEdit;
    EdFrtInnALIQ_PIS: TdmkEdit;
    EdFrtInnPIS_CST_TXT: TdmkEdit;
    EdFrtInnALIQ_COFINS: TdmkEdit;
    EdFrtInnCOFINS_CST_TXT: TdmkEdit;
    Label27: TLabel;
    EdFrtTES_ICMS: TdmkEdit;
    EdFrtTES_ICMS_TXT: TdmkEdit;
    EdFrtTES_PIS: TdmkEdit;
    EdFrtTES_PIS_TXT: TdmkEdit;
    EdFrtTES_COFINS: TdmkEdit;
    EdFrtTES_COFINS_TXT: TdmkEdit;
    EdFrtTES_BC_ICMS: TdmkEdit;
    Label28: TLabel;
    EdFrtTES_BC_ICMS_TXT: TdmkEdit;
    EdFrtTES_BC_PIS_TXT: TdmkEdit;
    EdFrtTES_BC_PIS: TdmkEdit;
    EdFrtTES_BC_COFINS: TdmkEdit;
    EdFrtTES_BC_COFINS_TXT: TdmkEdit;
    Label29: TLabel;
    QrFisRegUFsTES_BC_ICMS: TSmallintField;
    QrFisRegUFsTES_BC_IPI: TSmallintField;
    QrFisRegUFsTES_BC_PIS: TSmallintField;
    QrFisRegUFsTES_BC_COFINS: TSmallintField;
    QrFisRegCadLk: TIntegerField;
    QrFisRegCadDataCad: TDateField;
    QrFisRegCadDataAlt: TDateField;
    QrFisRegCadUserCad: TIntegerField;
    QrFisRegCadUserAlt: TIntegerField;
    QrFisRegCadAlterWeb: TSmallintField;
    QrFisRegCadAWServerID: TIntegerField;
    QrFisRegCadAWStatSinc: TSmallintField;
    QrFisRegCadAtivo: TSmallintField;
    QrFisRegCadFrtInnCFOP: TIntegerField;
    QrFisRegCadFrtInnICMS_CST: TIntegerField;
    QrFisRegCadFrtInnALIQ_ICMS: TFloatField;
    QrFisRegCadFrtTES_ICMS: TSmallintField;
    QrFisRegCadFrtTES_BC_ICMS: TSmallintField;
    QrFisRegCadFrtInnALIQ_PIS: TFloatField;
    QrFisRegCadFrtTES_BC_PIS: TSmallintField;
    QrFisRegCadFrtInnALIQ_COFINS: TFloatField;
    QrFisRegCadFrtTES_BC_COFINS: TSmallintField;
    QrFisRegCadFrtInnIND_NAT_FRT: TWideStringField;
    QrFisRegCadFrtInnNAT_BC_CRED: TWideStringField;
    QrFisRegCadFrtInnPIS_CST: TWideStringField;
    QrFisRegCadFrtInnCOFINS_CST: TWideStringField;
    QrFisRegCadFrtTES_PIS: TSmallintField;
    QrFisRegCadFrtTES_COFINS: TSmallintField;
    BtCorrige: TBitBtn;
    QrFRCad: TMySQLQuery;
    QrFRCadCodigo: TIntegerField;
    QrFRCfop: TMySQLQuery;
    PB1: TProgressBar;
    QrFRCfopCodigo: TIntegerField;
    QrFRCfopInterno: TSmallintField;
    QrFRCfopContribui: TSmallintField;
    QrFRCfopProprio: TSmallintField;
    QrFRCfopCFOP: TWideStringField;
    QrFRCfopServico: TSmallintField;
    QrFRCfopSubsTrib: TSmallintField;
    QrFRCfopOriCFOP: TWideStringField;
    QrFRUfs: TMySQLQuery;
    QrFRUfx: TMySQLQuery;
    QrFRUfsCodigo: TIntegerField;
    QrFRUfsInterno: TSmallintField;
    QrFRUfsContribui: TSmallintField;
    QrFRUfsProprio: TSmallintField;
    QrFRUfsUFEmit: TWideStringField;
    QrFRUfsUFDest: TWideStringField;
    QrFRUfsSubsTrib: TSmallintField;
    QrFRUfsServico: TSmallintField;
    QrFRUfsOriCFOP: TWideStringField;
    QrFRCfopControle: TIntegerField;
    QrFRUfsConta: TIntegerField;
    QrFRUfxCodigo: TIntegerField;
    QrFRUfxInterno: TSmallintField;
    QrFRUfxContribui: TSmallintField;
    QrFRUfxProprio: TSmallintField;
    QrFRUfxUFEmit: TWideStringField;
    QrFRUfxUFDest: TWideStringField;
    QrFRUfxNivel: TSmallintField;
    QrFRUfxCodNiv: TIntegerField;
    QrFRUfxSubsTrib: TSmallintField;
    QrFRUfxServico: TSmallintField;
    QrFRUfxOriCFOP: TWideStringField;
    QrFRUfxNivel4: TIntegerField;
    QrFRUfsControle: TIntegerField;
    QrFRUfxControle: TIntegerField;
    QrFRUfxConta: TIntegerField;
    QrFisRegCFOPControle: TIntegerField;
    QrFisRegUFsControle: TIntegerField;
    QrFisRegUFsConta: TIntegerField;
    QrFisRegUFxNivel4: TIntegerField;
    QrFisRegUFxControle: TIntegerField;
    QrFisRegUFxConta: TIntegerField;
    TabSheet10: TTabSheet;
    Panel21: TPanel;
    RGOriTES: TdmkRadioGroup;
    CkEFD_II_C195: TdmkCheckBox;
    QrFisRegCadOriTES: TIntegerField;
    QrFisRegCadEFD_II_C195: TSmallintField;
    QrPlaAllCad_DCodigo: TIntegerField;
    QrPlaAllCad_DNome: TWideStringField;
    QrPlaAllCad_DNivel: TSmallintField;
    QrPlaAllCad_DAnaliSinte: TIntegerField;
    QrPlaAllCad_DCredito: TSmallintField;
    QrPlaAllCad_DDebito: TSmallintField;
    QrPlaAllCad_DOrdens: TWideStringField;
    QrPlaAllCad_CCodigo: TIntegerField;
    QrPlaAllCad_CNome: TWideStringField;
    QrPlaAllCad_CNivel: TSmallintField;
    QrPlaAllCad_CAnaliSinte: TIntegerField;
    QrPlaAllCad_CCredito: TSmallintField;
    QrPlaAllCad_CDebito: TSmallintField;
    QrPlaAllCad_COrdens: TWideStringField;
    QrFisRegCadIntegraDRE: TSmallintField;
    TabSheet11: TTabSheet;
    Panel19: TPanel;
    TabSheet12: TTabSheet;
    Panel22: TPanel;
    SpeedButton14: TSpeedButton;
    SpeedButton15: TSpeedButton;
    Panel17: TPanel;
    SbGenCtbC: TSpeedButton;
    SbGenCtbD: TSpeedButton;
    Label24: TLabel;
    Label181: TLabel;
    CBGenCtbD: TdmkDBLookupComboBox;
    CBGenCtbC: TdmkDBLookupComboBox;
    EdGenCtbC: TdmkEditCB;
    EdGenCtbD: TdmkEditCB;
    DBEdOrdensD: TDBEdit;
    DBEdOrdensC: TDBEdit;
    Panel23: TPanel;
    RGInfoEFD_PisCofins: TdmkRadioGroup;
    RGIntegraDRE: TdmkRadioGroup;
    Panel24: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    EdCtbCadMoF: TdmkEditCB;
    EdCtbPlaCta: TdmkEditCB;
    CBCtbPlaCta: TdmkDBLookupComboBox;
    CBCtbCadMoF: TdmkDBLookupComboBox;
    SbCtbCadMoF: TSpeedButton;
    CtbPlaCta: TSpeedButton;
    Panel25: TPanel;
    DBRadioGroup5: TDBRadioGroup;
    DBRadioGroup6: TDBRadioGroup;
    DBRadioGroup7: TDBRadioGroup;
    Panel26: TPanel;
    Label30: TLabel;
    Label31: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    QrFisRegCadORD_GenCtbD: TWideStringField;
    QrFisRegCadORD_GenCtbC: TWideStringField;
    RGCreDeb_PisCofins: TdmkRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFisRegCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFisRegCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtRegraClick(Sender: TObject);
    procedure PMRegraPopup(Sender: TObject);
    procedure QrFisRegCadBeforeClose(DataSet: TDataSet);
    procedure QrFisRegCadAfterScroll(DataSet: TDataSet);
    procedure BtMovimentacaoClick(Sender: TObject);
    procedure QrFisRegCadCalcFields(DataSet: TDataSet);
    procedure SBModeloNFClick(Sender: TObject);
    procedure SbAgrupadorClick(Sender: TObject);
    procedure QrFisRegMvtCalcFields(DataSet: TDataSet);
    procedure DBGFisRegMvtMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Incluiitemdemovimentao1Click(Sender: TObject);
    procedure Alteraitemdemovimentaoatual1Click(Sender: TObject);
    procedure Excluiitemnsdemovimentao1Click(Sender: TObject);
    procedure BtCFOPClick(Sender: TObject);
    procedure IncluinovoCFOP1Click(Sender: TObject);
    procedure AlteraCFOPatual1Click(Sender: TObject);
    procedure RetiraCFOPAtual1Click(Sender: TObject);
    procedure QrFisRegCFOPAfterScroll(DataSet: TDataSet);
    procedure PMEmitDestPopup(Sender: TObject);
    procedure QrFisRegUFsBeforeClose(DataSet: TDataSet);
    procedure QrFisRegUFsAfterOpen(DataSet: TDataSet);
    procedure Incluiitemdealquotas1Click(Sender: TObject);
    procedure BtEmitDestClick(Sender: TObject);
    procedure Excluiumseltodos1Click(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure Incluinovaregra1Click(Sender: TObject);
    procedure Alteraregraatual1Click(Sender: TObject);
    procedure QrFisRegUFsCalcFields(DataSet: TDataSet);
    procedure BtExcecaoClick(Sender: TObject);
    procedure PMExcecaoPopup(Sender: TObject);
    procedure QrFisRegUFxAfterOpen(DataSet: TDataSet);
    procedure QrFisRegUFxBeforeClose(DataSet: TDataSet);
    procedure QrFisRegUFxCalcFields(DataSet: TDataSet);
    procedure QrFisRegUFsAfterScroll(DataSet: TDataSet);
    procedure Incluinovaexceo1Click(Sender: TObject);
    procedure Alteraexceoatual1Click(Sender: TObject);
    procedure Descrio1Click(Sender: TObject);
    procedure Cdigo1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrFisRegMvtAfterScroll(DataSet: TDataSet);
    procedure QrFisRegMvtBeforeClose(DataSet: TDataSet);
    procedure QrFisRegCFOPBeforeClose(DataSet: TDataSet);
    procedure PMMovimentacaoPopup(Sender: TObject);
    procedure PMCFOPPopup(Sender: TObject);
    procedure Alteraitemdealiquotas1Click(Sender: TObject);
    procedure Descrio2Click(Sender: TObject);
    procedure CFOP1Click(Sender: TObject);
    procedure Excluiexceoatual1Click(Sender: TObject);
    procedure BtEstricaoClick(Sender: TObject);
    procedure Incluientidade1Click(Sender: TObject);
    procedure Excluientidade1Click(Sender: TObject);
    procedure SbCtbCadMoFClick(Sender: TObject);
    procedure CtbPlaCtaClick(Sender: TObject);
    procedure SbGenCtbDClick(Sender: TObject);
    procedure SbGenCtbCClick(Sender: TObject);
    procedure EdFrtInnICMS_CSTChange(Sender: TObject);
    procedure EdFrtInnICMS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtInnPIS_CSTChange(Sender: TObject);
    procedure EdFrtInnPIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtInnCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtInnCOFINS_CSTChange(Sender: TObject);
    procedure EdFrtInnIND_NAT_FRTChange(Sender: TObject);
    procedure EdFrtInnIND_NAT_FRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtInnNAT_BC_CREDChange(Sender: TObject);
    procedure EdFrtInnNAT_BC_CREDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtTES_ICMSChange(Sender: TObject);
    procedure EdFrtTES_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtTES_PISChange(Sender: TObject);
    procedure EdFrtTES_COFINSChange(Sender: TObject);
    procedure EdFrtTES_BC_ICMSChange(Sender: TObject);
    procedure EdFrtTES_BC_PISChange(Sender: TObject);
    procedure EdFrtTES_BC_COFINSChange(Sender: TObject);
    procedure EdFrtTES_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtTES_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtTES_BC_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtTES_BC_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFrtTES_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtCorrigeClick(Sender: TObject);
    procedure RGOriTESClick(Sender: TObject);
    procedure EdGenCtbDRedefinido(Sender: TObject);
    procedure EdGenCtbCRedefinido(Sender: TObject);
  private
    F_TES_ITENS, F_TES_BC_ITENS, F_NAT_BC_CRED, F_IND_NAT_FRT: MyArrayLista;

    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure MostraFisRegUFx(SQLType: TSQLType);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //Corre��es
    procedure CorrigeFisRegMvt();
    procedure MostraFisRegUFs(SQLType: TSQLType);
    procedure ReopenCFOP_Frete();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFisRegMvt(Controle: Integer);
    procedure ReopenFisRegCFOP(CFOP: String);
    procedure ReopenFisRegUFs();
    procedure ReopenFisRegUFx();
    procedure ReopenFisRegEnt(Entidade: Integer);
  end;

var
  FmFisRegCad: TFmFisRegCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, FisRegMvt, MyDBCheck, FisAgrCad,
  {$IFDEF FmImprime} Imprime, {$ENDIF}
  {$IfNDef NAO_GFAT} ModPediVda, CFOP2003,{$EndIf}
  {$IfNDef SemNFe_0000}   NFe_PF, ModuleNFe_0000, {$EndIf}
  UnFinanceiroJan, UnFinanceiro,
  FisRegCFOP, FisRegUFs, FisRegUFx, ModuleGeral, FisRegEnt, ModuleFin;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFisRegCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFisRegCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFisRegCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFisRegCad.DefParams;
begin
  VAR_GOTOTABELA := 'FisRegCad';
  VAR_GOTOMYSQLTABLE := QrFisRegCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT frc.*, ');
{$IFDEF FmImprime}
  VAR_SQLx.Add('mnf.Nome NOMEMODELONF,');
{$Else}
  VAR_SQLx.Add('"" NOMEMODELONF,');
{$EndIf}
  // ini 2022-03-24
  VAR_SQLx.Add('cta.Nome NO_CtbPlaCta, ccm.Nome NO_CtbCadMoF, ');
  VAR_SQLx.Add('ctd.Nome NO_GenCtbD, ctc.Nome NO_GenCtbC, ');
{$IfDef UsaFinanceiro3}
  VAR_SQLx.Add('ctd.Ordens ORD_GenCtbD, ctc.Ordens ORD_GenCtbC,');
{$Else}
  VAR_SQLx.Add('"" ORD_GenCtbD, "" ORD_GenCtbC,');
{$EndIf}
  // fim 2022-03-24
  VAR_SQLx.Add('fac.Nome NOMEAGRUPADOR, fac.CodUsu CODUSU_AGRUPADOR');
  VAR_SQLx.Add('FROM fisregcad frc');
{$IFDEF FmImprime}
  VAR_SQLx.Add('LEFT JOIN imprime   mnf ON mnf.Codigo=frc.ModeloNF');
{$ENDIF}
  VAR_SQLx.Add('LEFT JOIN fisagrcad fac ON fac.Codigo=frc.Agrupador');
  // ini 2022-03-24
  VAR_SQLx.Add('LEFT JOIN ctbcadmof ccm ON ccm.Codigo=frc.CtbCadMoF');
  VAR_SQLx.Add('LEFT JOIN contas    cta ON cta.Codigo=frc.CtbPlaCta');
    {$IfDef UsaFinanceiro3}
    VAR_SQLx.Add('LEFT JOIN plaallcad ctd ON ctd.Codigo = frc.GenCtbD');
    VAR_SQLx.Add('LEFT JOIN plaallcad ctc ON ctc.Codigo = frc.GenCtbC');
    {$Else}
    VAR_SQLx.Add('LEFT JOIN contas    ctd ON ctd.Codigo=frc.GenCtbD');
    VAR_SQLx.Add('LEFT JOIN contas    ctc ON ctc.Codigo=frc.GenCtbC');
    {$EndIf}
  // fim 2022-03-24
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE frc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND frc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND frc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND frc.Nome Like :P0');
  //
end;

procedure TFmFisRegCad.Descrio1Click(Sender: TObject);
var
  Pesq: String;
begin
  Pesq := CuringaLoc.CriaForm4('Codigo', 'Nome', 'cfop2003', Dmod.MyDB, '');
  if Pesq <> '' then
    Edide_natOp.Text := VAR_NOME_X;
end;

procedure TFmFisRegCad.Descrio2Click(Sender: TObject);
begin
  LocCod(QrFisRegCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'FisRegCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFisRegCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'FisRegCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmFisRegCad.EdFrtInnCOFINS_CSTChange(Sender: TObject);
begin
  EdFrtInnCOFINS_CST_TXT.Text := UFinanceiro.CST_COFINS_Get(EdFrtInnCOFINS_CST.Text);
end;

procedure TFmFisRegCad.EdFrtInnCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdFrtInnCOFINS_CST.Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmFisRegCad.EdFrtInnICMS_CSTChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtInnICMS_CST_TXT.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdFrtInnICMS_CST.ValueVariant,
    CO_NOME_tbspedefd_ICMS_CST, Date());
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtInnICMS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', CO_NOME_tbspedefd_ICMS_CST, DModG.AllID_DB,
    ''(*Extra*), EdFrtInnICMS_CST, (*CBICMS_CST*)nil, dmktfInteger);
end;

procedure TFmFisRegCad.EdFrtInnIND_NAT_FRTChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtInnIND_NAT_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedIND_NAT_FRT,
    EdFrtInnIND_NAT_FRT.ValueVariant, F_IND_NAT_FRT);
  //
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtInnIND_NAT_FRTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtInnIND_NAT_FRT.Text := Geral.SelecionaItem(F_IND_NAT_FRT, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdFrtInnNAT_BC_CREDChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtInnNAT_BC_CRED_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedNAT_BC_CRED,
    EdFrtInnNAT_BC_CRED.ValueVariant, F_NAT_BC_CRED);
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtInnNAT_BC_CREDKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtInnNAT_BC_CRED.Text := Geral.SelecionaItem(F_NAT_BC_CRED, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdFrtInnPIS_CSTChange(Sender: TObject);
begin
  EdFrtInnPIS_CST_TXT.Text := UFinanceiro.CST_PIS_Get(EdFrtInnPIS_CST.Text);
end;

procedure TFmFisRegCad.EdFrtInnPIS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdFrtInnPIS_CST.Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmFisRegCad.EdFrtTES_BC_COFINSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtTES_BC_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,
    EdFrtTES_BC_COFINS.Text, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtTES_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtTES_BC_COFINS.Text := Geral.SelecionaItem(F_TES_BC_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdFrtTES_BC_ICMSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtTES_BC_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,
    EdFrtTES_BC_ICMS.Text, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtTES_BC_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtTES_BC_ICMS.Text := Geral.SelecionaItem(F_TES_BC_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdFrtTES_BC_PISChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtTES_BC_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,
    EdFrtTES_BC_PIS.Text, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtTES_BC_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtTES_BC_PIS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdFrtTES_COFINSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtTES_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,
    EdFrtTES_COFINS.Text, F_TES_ITENS);
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtTES_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtTES_COFINS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdFrtTES_ICMSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtTES_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,
    EdFrtTES_ICMS.Text, F_TES_ITENS);
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtTES_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtTES_ICMS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdFrtTES_PISChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdFrtTES_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,
    EdFrtTES_PIS.Text, F_TES_ITENS);
  {$EndIf}
end;

procedure TFmFisRegCad.EdFrtTES_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdFrtTES_PIS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegCad.EdGenCtbCRedefinido(Sender: TObject);
begin
  MyObjects.DefineDataFieldSelecionadoInt(EdGenCtbC, DBEdOrdensC, 'Ordens');
end;

procedure TFmFisRegCad.EdGenCtbDRedefinido(Sender: TObject);
begin
  MyObjects.DefineDataFieldSelecionadoInt(EdGenCtbD, DBEdOrdensD, 'Ordens');
end;

procedure TFmFisRegCad.Excluientidade1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFisRegEnt, DBGFisRegEnt,
  'FisRegEnt', ['Codigo', 'Entidade'], ['Codigo', 'Entidade'], istPergunta, '');
end;

procedure TFmFisRegCad.Excluiexceoatual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da exce��o selecionada?') = ID_YES then
  begin
    Dmod.MyDB.Execute(Geral.ATS(['DELETE FROM fisregufx ',
    'WHERE UFEmit="' + QrFisRegUFxUFEmit.Value + '" ',
    'AND UFDest="' + QrFisRegUFxUFDest.Value + '" ',
    'AND Nivel=' + Geral.FF0(QrFisRegUFxNivel.Value),
    'AND CodNiv=' + Geral.FF0(QrFisRegUFxCodNiv.Value),
    'AND cBenef="' + QrFisRegUFxcBenef.Value+ '"',
    '']));
    FmFisRegCad.ReopenFisRegUFx();
  end;
end;

procedure TFmFisRegCad.Excluiitemnsdemovimentao1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFisRegMvt, DBGFisRegMvt,
  'FisRegMvt', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmFisRegCad.Excluiumseltodos1Click(Sender: TObject);
begin
{ TODO : ver se pode pela exce��o! }  
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFisRegUFs,  TDBGrid(DBGFisRegUFs),
  'FisRegUFs',
  ['Codigo', 'Interno', 'Contribui', 'Proprio', 'SubsTrib', 'Servico', 'OriCFOP', 'UFEmit', 'UFDest'],
  ['Codigo', 'Interno', 'Contribui', 'Proprio', 'SubsTrib', 'Servico', 'OriCFOP', 'UFEmit', 'UFDest'],
  istPergunta, '');
end;

procedure TFmFisRegCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFisRegCad.MostraFisRegUFs(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFisRegUFs, FmFisRegUFs, afmoNegarComAviso) then
  begin
    if SQLType = stIns then
    begin
      FmFisRegUFs.EdCST_B.ValueVariant          := 0;
      FmFisRegUFs.EdCSOSN.ValueVariant          := '';
      FmFisRegUFs.RGmodBC.ItemIndex             := -1;
      FmFisRegUFs.EdpRedBC.ValueVariant         := 0;
      FmFisRegUFs.EdUFEmit.ValueVariant         := '';
      FmFisRegUFs.EdICMSAliq.ValueVariant       := 0.00;
      FmFisRegUFs.EdPISAliq.ValueVariant        := 0.00;
      FmFisRegUFs.EdCOFINSAliq.ValueVariant     := 0.00;
      FmFisRegUFs.EdpICMSInter.ValueVariant     := 0;
      FmFisRegUFs.EdUFDest.ValueVariant         := '';
      FmFisRegUFs.EdpICMSUFDest.ValueVariant    := 0;
      FmFisRegUFs.EdpFCPUFDest.ValueVariant     := 0;
      FmFisRegUFs.EdpBCUFDest.ValueVariant      := '100,0000000000';
      FmFisRegUFs.EdpICMSInterPart.ValueVariant := 0;
      FmFisRegUFs.CkUsaInterPartLei.Checked     := True;
      FmFisRegUFs.EdcBenef.ValueVariant         := '';
      FmFisRegUFs.EdpDif.ValueVariant           := 0.00;
      //FmFisRegUFs.RGOriTES.ItemIndex            := 0;
      FmFisRegUFs.EdOriCST_ICMS.Text            := '000';
      FmFisRegUFs.EdOriCST_IPI.Text             := '';
      FmFisRegUFs.EdOriCST_PIS.Text             := '';
      FmFisRegUFs.EdOriCST_COFINS.Text          := '';
      //FmFisRegUFs.CkEFD_II_C195.Checked         := False;
    end else
    begin
      FmFisRegUFs.EdCST_B.ValueVariant          := QrFisRegUFsCST_B.Value;
      FmFisRegUFs.EdCSOSN.ValueVariant          := QrFisRegUFsCSOSN.Value;
      FmFisRegUFs.RGmodBC.ItemIndex             := QrFisRegUFsmodBC.Value;
      FmFisRegUFs.EdpRedBC.ValueVariant         := QrFisRegUFspRedBC.Value;
      FmFisRegUFs.EdUFEmit.ValueVariant         := QrFisRegUFsUFEmit.Value;
      FmFisRegUFs.EdICMSAliq.ValueVariant       := QrFisRegUFsICMSAliq.Value;
      FmFisRegUFs.EdPISAliq.ValueVariant        := QrFisRegUFsPISAliq.Value;
      FmFisRegUFs.EdCOFINSAliq.ValueVariant     := QrFisRegUFsCOFINSAliq.Value;
      FmFisRegUFs.EdpICMSInter.ValueVariant     := QrFisRegUFspICMSInter.Value;
      FmFisRegUFs.EdUFDest.ValueVariant         := QrFisRegUFsUFDest.Value;
      FmFisRegUFs.EdpICMSUFDest.ValueVariant    := QrFisRegUFspICMSUFDest.Value;
      FmFisRegUFs.EdpFCPUFDest.ValueVariant     := QrFisRegUFspFCPUFDest.Value;
      FmFisRegUFs.EdpBCUFDest.ValueVariant      := QrFisRegUFspBCUFDest.Value;
      FmFisRegUFs.EdpICMSInterPart.ValueVariant := QrFisRegUFspICMSInterPart.Value;
      FmFisRegUFs.CkUsaInterPartLei.Checked     := Geral.IntToBool(QrFisRegUFsUsaInterPartLei.Value);
      FmFisRegUFs.EdcBenef.ValueVariant         := QrFisRegUFscBenef.Value;
      FmFisRegUFs.EdpDif.ValueVariant           := QrFisRegUFspDif.Value;
      //FmFisRegUFs.RGOriTES.ItemIndex            := QrFisRegUFsOriTES.Value;
      FmFisRegUFs.EdOriCST_ICMS.Text            := QrFisRegUFsOriCST_ICMS.Value;
      FmFisRegUFs.EdOriCST_IPI.Text             := QrFisRegUFsOriCST_IPI.Value;
      FmFisRegUFs.EdOriCST_PIS.Text             := QrFisRegUFsOriCST_PIS.Value;
      FmFisRegUFs.EdOriCST_COFINS.Text          := QrFisRegUFsOriCST_COFINS.Value;
      //FmFisRegUFs.CkEFD_II_C195.Checked         := Geral.IntToBool(QrFisRegUFsEFD_II_C195.Value);
      FmFisRegUFs.EdGenCtbD.ValueVariant        := QrFisRegUFsGenCtbD.Value;
      FmFisRegUFs.CBGenCtbD.KeyValue            := QrFisRegUFsGenCtbD.Value;
      FmFisRegUFs.EdGenCtbC.ValueVariant        := QrFisRegUFsGenCtbC.Value;
      FmFisRegUFs.CBGenCtbC.KeyValue            := QrFisRegUFsGenCtbC.Value;
      FmFisRegUFs.EdTES_ICMS.ValueVariant       := QrFisRegUFsTES_ICMS.Value;
      FmFisRegUFs.EdTES_IPI.ValueVariant        := QrFisRegUFsTES_IPI.Value;
      FmFisRegUFs.EdTES_PIS.ValueVariant        := QrFisRegUFsTES_PIS.Value;
      FmFisRegUFs.EdTES_COFINS.ValueVariant     := QrFisRegUFsTES_COFINS.Value;
      FmFisRegUFs.EdTES_BC_ICMS.ValueVariant    := QrFisRegUFsTES_BC_ICMS.Value;
      FmFisRegUFs.EdTES_BC_IPI.ValueVariant     := QrFisRegUFsTES_BC_IPI.Value;
      FmFisRegUFs.EdTES_BC_PIS.ValueVariant     := QrFisRegUFsTES_BC_PIS.Value;
      FmFisRegUFs.EdTES_BC_COFINS.ValueVariant  := QrFisRegUFsTES_BC_COFINS.Value;
  end;
    //
    FmFisRegUFs.ShowModal;
    FmFisRegUFs.Destroy;
    //
    ReopenFisRegUFs();
  end;
end;

procedure TFmFisRegCad.MostraFisRegUFx(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFisRegUFx, FmFisRegUFx, afmoNegarComAviso) then
  begin
    FmFisRegUFx.ImgTipo.SQLType := SQLType;
    FmFisRegUFx.EdUFEmit.Text := QrFisRegUFsUFEmit.Value;
    FmFisRegUFx.EdUFDest.Text := QrFisRegUFsUFDest.Value;
    if SQLType = stUpd then
    begin
      FmFisRegUFx.EdCST_B.ValueVariant := QrFisRegUFxCST_B.Value;
      FmFisRegUFx.EdCSOSN.ValueVariant := QrFisRegUFxCSOSN.Value;
      FmFisRegUFx.RGmodBC.ItemIndex := QrFisRegUFxmodBC.Value;
      FmFisRegUFx.EdpRedBC.ValueVariant := QrFisRegUFxpRedBC.Value;
      FmFisRegUFx.RGNivel.ItemIndex := QrFisRegUFxNivel.Value;
      FmFisRegUFx.EdCodNiv.ValueVariant := QrFisRegUFxCodNiv.Value;
      FmFisRegUFx.CBCodNiv.KeyValue := QrFisRegUFxCodNiv.Value;
      FmFisRegUFx.EdICMSAliq.ValueVariant := QrFisRegUFxICMSAliq.Value;
{     N�o implementado ainda 2011-08-23
      FmFisRegUFx.EdPISAliq.ValueVariant := QrFisRegUFxPISAliq.Value;
      FmFisRegUFx.EdCOFINSAliq.ValueVariant := QrFisRegUFxCOFINSAliq.Value;
}
      FmFisRegUFx.EdcBenef.ValueVariant         := QrFisRegUFxcBenef.Value;
      FmFisRegUFx.EdpDif.ValueVariant           := QrFisRegUFxpDif.Value;
    end else begin
      // stIns
    end;
    FmFisRegUFx.ShowModal;
    FmFisRegUFx.Destroy;
    //
    ReopenFisRegUFx();
  end;
end;

procedure TFmFisRegCad.PMRegraPopup(Sender: TObject);
begin
  Alteraregraatual1.Enabled :=
    (QrFisRegCad.State <> dsInactive) and (QrFisRegCad.RecordCount > 0) and
    (QrFisRegCadCodigo.Value <> 0);
end;

procedure TFmFisRegCad.PMCFOPPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrFisRegCFOP.State <> dsInactive) and (QrFisRegCFOP.RecordCount > 0);
  //
  AlteraCFOPatual1.Enabled := Enab;
  RetiraCFOPAtual1.Enabled := Enab;
end;

procedure TFmFisRegCad.PMEmitDestPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrFisRegUFs.State <> dsInactive) and (QrFisRegUFs.RecordCount > 0);
  //
  Alteraitemdealiquotas1.Enabled := Enab;
  Excluiumseltodos1.Enabled      := Enab;
end;

procedure TFmFisRegCad.PMExcecaoPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Incluinovaexceo1.Enabled := True;
  Habilita := (QrFisRegUFx.State <> dsInactive) and (QrFisRegUFx.RecordCount > 0);
  Alteraexceoatual1.Enabled := Habilita;
  Excluiexceoatual1.Enabled := Habilita;
end;

procedure TFmFisRegCad.PMMovimentacaoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrFisRegMvt.State <> dsInactive) and (QrFisRegMvt.RecordCount > 0);
  //
  Alteraitemdemovimentaoatual1.Enabled := Enab;
  Excluiitemnsdemovimentao1.Enabled    := Enab;
end;

procedure TFmFisRegCad.Cdigo1Click(Sender: TObject);
var
  CFOP: String;
begin
  CFOP := '';
  if InputQuery('Pesquisa por c�digo CFOP', 'Informe o c�digo CFOP', CFOP) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrAux, Dmod.MyDB, [
    'SELECT Nome ',
    'FROM cfop2003 ',
    'WHERE Codigo = "' + CFOP + '"',
    '']);
    CFOP := DModG.QrAux.FieldByName('Nome').AsString;
    if CFOP <> '' then
    begin
      Edide_natOp.Text := CFOP;
      Edide_natOp.SetFocus;
    end;
  end;
end;

procedure TFmFisRegCad.CFOP1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := CuringaLoc.CriaForm('frc.Codigo',
    'CONCAT(fco.CFOP, " - ", frc.Nome)', 'fisregcad frc', Dmod.MyDB,
    'GROUP BY frc.Codigo, fco.CFOP ', False,
    'LEFT JOIN fisregcfop fco ON fco.Codigo=frc.Codigo');
  //
  if Codigo <> 0 then
    LocCod(QrFisRegCadCodigo.Value, Codigo);
end;

procedure TFmFisRegCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFisRegCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFisRegCad.DBGFisRegMvtMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  K: Integer;
begin
  with DBGFisRegMvt do
  begin
    K := Columns[0].Width;
    if DBGTitle1.Columns[0].Width <> K then
      DBGTitle1.Columns[0].Width := K;
    //
    K := Columns[1].Width + Columns[2].Width + 1;
    if DBGTitle1.Columns[1].Width <> K then
      DBGTitle1.Columns[1].Width := K;
    //
    K := Columns[3].Width;
    if DBGTitle1.Columns[2].Width <> K then
      DBGTitle1.Columns[2].Width := K;
    //
    K := Columns[4].Width;
    if DBGTitle1.Columns[3].Width <> K then
      DBGTitle1.Columns[3].Width := K;
    //
    K := Columns[5].Width + Columns[6].Width + 1;
    if DBGTitle1.Columns[4].Width <> K then
      DBGTitle1.Columns[4].Width := K;
    //
    K := Columns[7].Width + Columns[8].Width + Columns[9].Width + 1;
    if DBGTitle1.Columns[5].Width <> K then
      DBGTitle1.Columns[5].Width := K;
    //
  end;
end;

procedure TFmFisRegCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFisRegCad.SpeedButton12Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNatOper, SpeedButton12);
end;

procedure TFmFisRegCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFisRegCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFisRegCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFisRegCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFisRegCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFisRegCadCodigo.Value;
  Close;
end;

procedure TFmFisRegCad.AlteraCFOPatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Cria(TFmFisRegCFOP, FmFisRegCFOP, afmoNegarComAviso,
    QrFisRegCFOP, stUpd);
  FmFisRegCFOP.FTemItens := QrFisRegUFs.RecordCount > 0;
  FmFisRegCFOP.HabilitaCamposIndice();
  FmFisRegCFOP.EdControle.ValueVariant := QrFisRegCFOPControle.Value;
  FmFisRegCFOP.ShowModal;
  FmFisRegCFOP.Destroy;
end;

procedure TFmFisRegCad.Alteraexceoatual1Click(Sender: TObject);
begin
  MostraFisRegUFx(stUpd);
end;

procedure TFmFisRegCad.Alteraitemdealiquotas1Click(Sender: TObject);
begin
  MostraFisRegUFs(stUpd);
end;

procedure TFmFisRegCad.Alteraitemdemovimentaoatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmFisRegMvt, FmFisRegMvt, afmoNegarComAviso,
    QrFisRegMvt, stUpd);
end;

procedure TFmFisRegCad.Alteraregraatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrFisRegCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'FisRegCad');
end;

procedure TFmFisRegCad.BtCFOPClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMCFOP, BtCFOP);
end;

procedure TFmFisRegCad.BtRegraClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMRegra, BtRegra);
end;

procedure TFmFisRegCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, OriTES: Integer;
  Nome: String;
  SemContabil, FIC: Boolean;
begin
  if MyObjects.FIC(RGTpCalcTrib.ItemIndex = 0, RGTpCalcTrib,
  'Defina uma "Forma de c�lculo de campos espec�ficos da NF-e"!') then Exit;
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Trim(Edide_natOp.Text) = '', Edide_natOp,
    'Defina a Natureza da Opera��o!') then Exit;
{$IFDEF FmImprime}
  if MyObjects.FIC(
  EdModeloNF.ValueVariant = 0, EdModeloNF, 'Defina o modelo de NF') then
    Exit;
{$ENDIF}
  //
  OriTES := RGOriTES.ItemIndex;
  //FIC := (OriTES = 0) and (PCTES.ActivePageIndex = 1);
  FIC := (OriTES = 0) and (EdTipoMov.ValueVariant = 0);
  if MyObjects.FIC(FIC, RGOriTES, 'Informe o TES de NFs de entrada!') then Exit;
  //
  // ini 2022-03-24
  if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
  begin
(*
    SemContabil := (EdCtbCadMoF.ValueVariant = 0) and (EdCtbPlaCta.ValueVariant = 0);
    MyObjects.FIC(SemContabil, nil, 'Nenhuma diretriz cont�bil foi definida!' +
    sLineBreak + '"Tipo de movimento cont�bil"' +
    sLineBreak + 'ou' +
    sLineBreak + '"Conta do plano de contas"');
*)
    //if (EdTipoMov.ValueVariant = 0) then
    if RGFinanceiro.ItemIndex > 0 then
    begin
      if MyObjects.FIC(EdGenCtbD.ValueVariant = 0, EdGenCtbD,
        'Informe a conta a d�bito!') then Exit;
      if MyObjects.FIC(EdGenCtbC.ValueVariant = 0, EdGenCtbC,
        'Informe a conta a cr�dito!') then Exit;
      if MyObjects.FIC(RGCreDeb_PisCofins.ItemIndex = 0, RGCreDeb_PisCofins,
        'Defina a "Conta a Informar no EFD PIS/COFINS"') then Exit;
    end;
  end;
  // fim 2022-03-24

  if MyObjects.FIC(
  RGInfoEFD_PisCofins.ItemIndex = 0, EdModeloNF, 'Defina "' +
  RGInfoEFD_PisCofins.Caption + '"') then
    Exit;
  //
  if MyObjects.FIC(
  RGIntegraDRE.ItemIndex = 0, EdModeloNF, 'Defina "' +
  RGIntegraDRE.Caption + '"') then
    Exit;
  //
  if (RGTpCalcTrib.ItemIndex = 1) then
    Geral.MB_Info(
    'Os c�lculos autom�ticos de campos espec�ficos da NF-e s�o realizados com ' +
    'base nas informa��es fornecidas por clientes e usu�rios do sistema.' +
    sLineBreak + sLineBreak +
    'Somente use esta forma ap�s ter certeza de que estes c�lculos ' +
    'est�o de acordo com o que sua empresa e seu contador definiram!');
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('FisRegCad', 'Codigo', ImgTipo.SQLType,
    QrFisRegCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmFisRegCad, PainelEdita,
    'FisRegCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFisRegCad.BtCorrigeClick(Sender: TObject);
var
  Codigo, Controle, Conta, Nivel4, QtdReg,
  Interno, Contribui, Proprio, Servico, SubsTrib, Nivel, CodNiv: Integer;
  OriCFOP, UFEmit, UFDest: String;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    QtdReg := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM fisregcad ',
    '']);
    QtdReg := QtdReg + Dmod.QrAux.RecordCount;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM fisregcfop ',
    '']);
    QtdReg := QtdReg + Dmod.QrAux.RecordCount;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM fisregufs ',
    '']);
    QtdReg := QtdReg + Dmod.QrAux.RecordCount;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM fisregufx ',
    '']);
    QtdReg := QtdReg + Dmod.QrAux.RecordCount;
    PB1.Position := 0;
    PB1.Max := QtdReg;
    //
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrFRCad, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM fisregcad ',
    'ORDER BY Codigo ',
    '']);
    QrFRCad.First;
    while not QrFRCad.Eof do
    begin
      //
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      Codigo := QrFRCadCodigo.Value;
      UnDmkDAC_PF.AbreMySQLQuery0(QrFRCFOP, Dmod.MyDB, [
      'SELECT * ',
      'FROM fisregcfop ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      QrFRCfop.First;
      while not QrFRCfop.Eof do
      begin
        //
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        //
        Interno    := QrFRCfopInterno.Value;
        Contribui  := QrFRCfopContribui.Value;
        Proprio    := QrFRCfopProprio.Value;
        Servico    := QrFRCfopServico.Value;
        SubsTrib   := QrFRCfopSubsTrib.Value;
        OriCFOP    := QrFRCfopOriCFOP.Value;
        Controle := QrFRCfopControle.Value;
        if Controle = 0 then
          Controle := UMyMod.BPGS1I32('fisregcfop', 'Controle', '', '', tsPos, stIns, 0);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fisregcfop', False, [
        'Controle'], [
        'Codigo', 'Interno', 'Contribui', 'Proprio', 'Servico', 'SubsTrib', 'OriCFOP'], [
        Controle], [
        Codigo, Interno, Contribui, Proprio, Servico, SubsTrib, OriCFOP], True) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrFRUFs, Dmod.MyDB, [
          'SELECT *  ',
          'FROM fisregufs ',
          'WHERE Codigo='  + Geral.FF0(QrFRCFOPCodigo.Value),
          'AND Interno='   + Geral.FF0(QrFRCFOPInterno.Value),
          'AND Contribui=' + Geral.FF0(QrFRCFOPContribui.Value),
          'AND Proprio='   + Geral.FF0(QrFRCFOPProprio.Value),
          'AND SubsTrib='  + Geral.FF0(QrFRCFOPSubsTrib.Value),
          'AND Servico='  + Geral.FF0(QrFRCFOPServico.Value),
          'AND OriCFOP="'  + QrFRCFOPOriCFOP.Value + '"',
          'ORDER BY UFEmit, UFDest ',
          '']);
          QrFRUFs.First;
          while not QrFRUFs.Eof do
          begin
            //
            MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
            //
            UFEmit := QrFRUFsUFEmit.Value;
            UFDest := QrFRUFsUFDest.Value;
            Conta := QrFRUfsConta.Value;
            if (Conta = 0) or (QrFRUFsControle.Value = 0) then
            begin
              if Conta = 0 then
                Conta := UMyMod.BPGS1I32('fisregufs', 'Conta', '', '', tsPos, stIns, 0);
              //
              Continua :=  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fisregufs', False, [
              'Controle', 'Conta'], [
              'Codigo', 'Interno', 'Contribui', 'Proprio', 'UFEmit', 'UFDest', 'SubsTrib', 'Servico', 'OriCFOP'], [
              Controle, Conta], [
              Codigo, Interno, Contribui, Proprio, UFEmit, UFDest, SubsTrib, Servico, OriCFOP], True);
            end else
              Continua := true;
            if Continua then
            begin
              UnDmkDAC_PF.AbreMySQLQuery0(QrFRUFx, Dmod.MyDB, [
              'SELECT * ',
              'FROM Fisregufx',
              'WHERE Codigo='  + Geral.FF0(QrFRUFsCodigo.Value),
              'AND Interno='   + Geral.FF0(QrFRUFsInterno.Value),
              'AND Contribui=' + Geral.FF0(QrFRUFsContribui.Value),
              'AND Proprio='   + Geral.FF0(QrFRUFsProprio.Value),
              'AND SubsTrib='  + Geral.FF0(QrFRUFsSubsTrib.Value),
              'AND Servico='   + Geral.FF0(QrFRCFOPServico.Value),
              'AND OriCFOP="'  + QrFRCFOPOriCFOP.Value + '"',
              'AND UFEmit="'   + QrFRUFsUFEmit.Value + '"',
              'AND UFDest="'   + QrFRUFsUFDest.Value + '"',
              'ORDER BY Nivel, CodNiv',
              '']);
              QrFRUFx.First;
              while not QrFRUFx.Eof do
              begin
                //
                MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                //
                Nivel  := QrFRUfxNivel.Value;
                CodNiv := QrFRUfxCodNiv.Value;
                Nivel4 := QrFRUfxNivel4.Value;
                if (Nivel4 = 0) or (QrFRUFxControle.Value = 0) or (QrFRUFxConta.Value = 0) then
                begin
                  if Nivel4 = 0 then
                    Nivel4 := UMyMod.BPGS1I32('fisregufx', 'Nivel4', '', '', tsPos, stIns, 0);
                  //
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fisregufx', False, [
                  'Controle', 'Conta', 'Nivel4'], [
                  'Codigo', 'Interno', 'Contribui', 'Proprio', 'UFEmit', 'UFDest', 'Nivel', 'CodNiv', 'SubsTrib', 'Servico', 'OriCFOP'], [
                  Controle, Conta, Nivel4], [
                  Codigo, Interno, Contribui, Proprio, UFEmit, UFDest, Nivel, CodNiv, SubsTrib, Servico, OriCFOP], True) then
                  begin

                  end;
                end;
                //
                QrFRUFx.Next;
                //
              end;
              //
              QrFRUFs.Next;
              //
            end;
          end;
        end;
        //
        QrFRCfop.Next;
        //
      end;
      //
      QrFRCad.Next;
      //
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrFRUFs, Dmod.MyDB, [
    'SELECT *  ',
    'FROM fisregufs ',
    'WHERE Conta=0 ',
    '']);
    QrFRUFs.First;
    while not QrFRUFs.Eof do
    begin
      //
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      Codigo     := QrFRUfsCodigo.Value;
      Interno    := QrFRUfsInterno.Value;
      Contribui  := QrFRUfsContribui.Value;
      Proprio    := QrFRUfsProprio.Value;
      Servico    := QrFRUfsServico.Value;
      SubsTrib   := QrFRUfsSubsTrib.Value;
      OriCFOP    := QrFRUfsOriCFOP.Value;
      Controle   := QrFRUfsControle.Value;
      UFEmit     := QrFRUFsUFEmit.Value;
      UFDest     := QrFRUFsUFDest.Value;
      //Conta      := QrFRUfsConta.Value;
      Conta := UMyMod.BPGS1I32('fisregufs', 'Conta', '', '', tsPos, stIns, 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fisregufs', False, [
      'Conta'], [
      'Codigo', 'Interno', 'Contribui', 'Proprio', 'UFEmit', 'UFDest', 'SubsTrib', 'Servico', 'OriCFOP', 'Controle', 'Conta'], [
      Conta], [
      Codigo, Interno, Contribui, Proprio, UFEmit, UFDest, SubsTrib, Servico, OriCFOP, Controle, QrFRUfsConta.Value], True);
      //
      //Geral.MB_Teste(Dmod.QrUpd.SQL.Text);
      QrFRUFs.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrFRUFx, Dmod.MyDB, [
    'SELECT *  ',
    'FROM fisregufx ',
    'WHERE Nivel4=0 ',
    '']);
    QrFRUFx.First;
    while not QrFRUFx.Eof do
    begin
      //
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      Codigo     := QrFRUfxCodigo.Value;
      Interno    := QrFRUfxInterno.Value;
      Contribui  := QrFRUfxContribui.Value;
      Proprio    := QrFRUfxProprio.Value;
      Servico    := QrFRUfxServico.Value;
      SubsTrib   := QrFRUfxSubsTrib.Value;
      OriCFOP    := QrFRUfxOriCFOP.Value;
      Controle   := QrFRUfxControle.Value;
      UFEmit     := QrFRUfxUFEmit.Value;
      UFDest     := QrFRUfxUFDest.Value;
      Conta      := QrFRUfxConta.Value;
      Nivel      := QrFRUfxNivel.Value;
      CodNiv     := QrFRUfxCodNiv.Value;
      //Nivel4 := QrFRUfxNivel4.Value;
      Nivel4 := UMyMod.BPGS1I32('fisregufx', 'Nivel4', '', '', tsPos, stIns, 0);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fisregufx', False, [
      'Nivel4'], [
      'Codigo', 'Interno', 'Contribui', 'Proprio', 'UFEmit', 'UFDest', 'Nivel', 'CodNiv', 'SubsTrib', 'Servico', 'OriCFOP', 'Controle', 'Conta', 'Nivel4'], [
      Nivel4], [
      Codigo, Interno, Contribui, Proprio, UFEmit, UFDest, Nivel, CodNiv, SubsTrib, Servico, OriCFOP, Controle, Conta, QrFRUfxNivel4.Value], True);
      QrFRUFx.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFisRegCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'FisRegCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FisRegCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FisRegCad', 'Codigo');
end;

procedure TFmFisRegCad.BtEmitDestClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMEmitDest, BtEmitDest);
end;

procedure TFmFisRegCad.BtEstricaoClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMEstricao, BtEstricao);
end;

procedure TFmFisRegCad.BtExcecaoClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMExcecao, BtExcecao);
end;

procedure TFmFisRegCad.BtMovimentacaoClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMMovimentacao, BtMovimentacao);
end;

procedure TFmFisRegCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  with THackCustomGrid(DBGFisRegMvt) do
     Options := Options - [goColMoving];
  //DBGFisRegMvt.Align := alClient;
  PageControl3.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrFisAgrCad, Dmod.MyDB);
{$IFDEF FmImprime}
  UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
{$ENDIF}
  //
//{$IfNDef SemPediVda} Erro no ToolRent
{$IfNDef NAO_GFAT}
  CBTipoMov.ListSource := DmPediVda.DsFRC_TMov;
{$EndIf}
  //
  CriaOForm;
  //
  CorrigeFisRegMvt;
  //
  PageControl3.ActivePageIndex := 0;
  //
  DBGFisRegCFOP.Align    := alClient;
  DBGFisRegCFOP2.Visible := False;
  UnDmkDAC_PF.AbreQuery(QrCtbCadMoF, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlaAllCad_D, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlaAllCad_C, Dmod.MyDB);

  ReopenCFOP_Frete();

  {$IfNDef SemNFe_0000}
  F_TES_ITENS := UnNFe_PF.ListaTES_ITENS();
  F_TES_BC_ITENS := UnNFe_PF.ListaTES_BC_ITENS();
  F_NAT_BC_CRED := UnNFe_PF.ListaNAT_BC_CRED();
  F_IND_NAT_FRT := UnNFe_PF.ListaIND_NAT_FRT();

  EdFrtInnICMS_CST_TXT.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdFrtInnICMS_CST.ValueVariant, CO_NOME_tbspedefd_ICMS_CST, Date());
  EdFrtInnPIS_CST_TXT.Text := UFinanceiro.CST_PIS_Get(EdFrtInnPIS_CST.Text);
  EdFrtInnCOFINS_CST_TXT.Text := UFinanceiro.CST_PIS_Get(EdFrtInnCOFINS_CST.Text);

  EdFrtTES_ICMS_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdFRtTES_ICMS.Text, F_TES_ITENS);
  EdFrtTES_PIS_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdFrtTES_PIS.Text, F_TES_ITENS);
  EdFrtTES_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdFrtTES_COFINS.Text, F_TES_ITENS);

  EdFrtTES_BC_ICMS_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdFRtTES_ICMS.Text, F_TES_BC_ITENS);
  EdFrtTES_BC_PIS_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdFrtTES_PIS.Text, F_TES_BC_ITENS);
  EdFrtTES_BC_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdFrtTES_COFINS.Text, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFisRegCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFisRegCad.SbAgrupadorClick(Sender: TObject);
var
  Agrupador: Integer;
begin
  VAR_CADASTRO := 0;
  Agrupador    := EdAgrupador.ValueVariant;
  //
  if DBCheck.CriaFm(TFmFisAgrCad, FmFisAgrCad, afmoNegarComAviso) then
  begin
    if Agrupador <> 0 then
      FmFisAgrCad.LocCod(Agrupador, Agrupador);
    FmFisAgrCad.ShowModal;
    FmFisAgrCad.Destroy;
  end;
  if (VAR_CADASTRO > 0) then
  begin
    UnDmkDAC_PF.AbreQuery(QrFisAgrCad, Dmod.MyDB);
    //
    EdAgrupador.ValueVariant := VAR_CADASTRO;
    CBAgrupador.KeyValue     := VAR_CADASTRO;
    EdAgrupador.SetFocus;
  end;
end;

procedure TFmFisRegCad.SbCtbCadMoFClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormCtbCadMoF(EdCtbCadMoF.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbCadMoF, CBCtbCadMoF, QrCtbCadMoF, VAR_CADASTRO);
end;

procedure TFmFisRegCad.SbGenCtbCClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  {$IfDef UsaContabil}
    DModFin.MostraFormPlaAny(EdGenCtbC, CBGenCtbC, QrPlaAllCad_C, SbGenCtbC);
  {$Else}
    FinanceiroJan.MostraFormContas(EdGenCtbC.ValueVariant);
  {$EndIf}
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdGenCtbC, CBGenCtbC, QrPlaAllCad_C, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmFisRegCad.SbGenCtbDClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  {$IfDef UsaContabil}
    DModFin.MostraFormPlaAny(EdGenCtbD, CBGenCtbD, QrPlaAllCad_D, SbGenCtbD);
  {$Else}
    FinanceiroJan.MostraFormContas(EdGenCtbD.ValueVariant);
  {$EndIf}
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdGenCtbD, CBGenCtbD, QrPlaAllCad_D, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmFisRegCad.CtbPlaCtaClick(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  //FinanceiroJan.MostraFormContas(EdCtbPlaCta.ValueVariant);
  DModFin.MostraFormPlaAny(EdCtbPlaCta, CBCtbPlaCta, QrPlaAllCad_C, SbGenCtbC);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbPlaCta, CBCtbPlaCta, Qr???, VAR_CADASTRO);
}
end;

procedure TFmFisRegCad.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmFisRegCad.SBModeloNFClick(Sender: TObject);
{$IFDEF FmImprime}
var
  ModeloNF: Integer;
begin
  VAR_IMPRIMEFMT := 0;
  ModeloNF       := EdModeloNF.ValueVariant;
  //
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    if ModeloNF <> 0 then
      FmImprime.LocCod(ModeloNF, ModeloNF);
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
  if (VAR_IMPRIMEFMT <> 0) and (VAR_IMPRIMETYP = 1) then
  begin
    QrImprime.Close;
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    //
    EdModeloNF.ValueVariant := VAR_IMPRIMEFMT;
    CBModeloNF.KeyValue     := VAR_IMPRIMEFMT;
    EdModeloNF.SetFocus;
  end;
{$ELSE}
begin
  Geral.MensagemBox('O formul�rio "FMImprime" n�o est� habilitado para este aplicativo.' +
    'AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
{$ENDIF}
end;

procedure TFmFisRegCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFisRegCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFisRegCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmFisRegCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFisRegCad.QrFisRegCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //Geral.MB_Teste(QrFisRegCad.SQL.Text);
end;

procedure TFmFisRegCad.QrFisRegCadAfterScroll(DataSet: TDataSet);
begin
  ReopenFisRegMvt(0);
  ReopenFisRegEnt(0);
end;

procedure TFmFisRegCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegCad.SbQueryClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMQuery, SbQuery);
end;

procedure TFmFisRegCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFisRegCad.Incluientidade1Click(Sender: TObject);
var
  Entidade: Integer;
begin
  if DBCheck.CriaFm(TFmFisRegEnt, FmFisRegEnt, afmoSoBoss) then
  begin
    FmFisRegEnt.ImgTipo.SQLType       := stIns;
    FmFisRegEnt.EdCodigo.ValueVariant := QrFisRegCadCodigo.Value;
    FmFisRegEnt.FQrIts                := QrFisRegEnt;
    FmFisRegEnt.ShowModal;
    Entidade := FmFisRegEnt.FEntidade;
    FmFisRegEnt.Destroy;
    //
    ReopenFisRegEnt(Entidade);
  end;
end;

procedure TFmFisRegCad.Incluiitemdealquotas1Click(Sender: TObject);
begin
  MostraFisRegUFs(stIns);
end;

procedure TFmFisRegCad.Incluiitemdemovimentao1Click(Sender: TObject);
begin
  if QrFisRegMvt.RecordCount > 0 then
  begin
    // CUIDADO!!! campo IND_MOV do registro C170 do SPED EFD usa o campo TipoCalc
    // da RegraFiscal do campo RegrFiscal da tabela nfecaba!
    Geral.MB_Info('� permitido somente uma movimenta��o por regra fiscal (SPED EFD)!');
    Exit;
  end else
  begin
    UmyMod.FormInsUpd_Show(TFmFisRegMvt, FmFisRegMvt, afmoNegarComAviso,
      QrFisRegMvt, stIns);
  end;
end;

procedure TFmFisRegCad.Incluinovaexceo1Click(Sender: TObject);
begin
  MostraFisRegUFx(stIns);
end;

procedure TFmFisRegCad.Incluinovaregra1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrFisRegCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'FisRegCad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'FisRegCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  //
  RGAplicacao.ItemIndex    := 1;
  RGTpCalcTrib.ItemIndex   := 1;
  RGTipoEmiss.ItemIndex    := 1;
  EdAgrupador.ValueVariant := 3;
  CBAgrupador.KeyValue     := 3;
  RGCreDeb_PisCofins.ItemIndex := 0;
  EdNome.SetFocus;
end;

procedure TFmFisRegCad.IncluinovoCFOP1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmFisRegCFOP, FmFisRegCFOP, afmoNegarComAviso,
    QrFisRegCFOP, stIns);
end;

procedure TFmFisRegCad.QrFisRegCadBeforeClose(DataSet: TDataSet);
begin
  QrFisRegMvt.Close;
end;

procedure TFmFisRegCad.QrFisRegCadBeforeOpen(DataSet: TDataSet);
begin
  QrFisRegCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFisRegCad.QrFisRegCadCalcFields(DataSet: TDataSet);
begin
  case QrFisRegCadTipoMov.Value of
    0: QrFisRegCadNOMETIPOMOV.Value := 'Entrada';
    1: QrFisRegCadNOMETIPOMOV.Value := 'Sa�da';
    else QrFisRegCadNOMETIPOMOV.Value := '? ? ?';
  end;
end;

procedure TFmFisRegCad.QrFisRegCFOPAfterScroll(DataSet: TDataSet);
begin
  ReopenFisRegUFs();
end;

procedure TFmFisRegCad.QrFisRegCFOPBeforeClose(DataSet: TDataSet);
begin
  QrFisRegUFs.Close;
end;

procedure TFmFisRegCad.QrFisRegMvtAfterScroll(DataSet: TDataSet);
begin
  ReopenFisRegCFOP('');
end;

procedure TFmFisRegCad.QrFisRegMvtBeforeClose(DataSet: TDataSet);
begin
  QrFisRegCFOP.Close;
end;

procedure TFmFisRegCad.QrFisRegMvtCalcFields(DataSet: TDataSet);
begin
  QrFisRegMvtSEQ.Value := QrFisRegMvt.RecNo;
end;

procedure TFmFisRegCad.QrFisRegUFsAfterOpen(DataSet: TDataSet);
begin
  BtEmitDest.Enabled := True;
end;

procedure TFmFisRegCad.QrFisRegUFsAfterScroll(DataSet: TDataSet);
begin
  ReopenFisRegUFx();
end;

procedure TFmFisRegCad.QrFisRegUFsBeforeClose(DataSet: TDataSet);
begin
  BtEmitDest.Enabled := False;
  QrFisRegUFx.Close;
end;

procedure TFmFisRegCad.QrFisRegUFsCalcFields(DataSet: TDataSet);
begin
  if QrFisRegUFsModBC.Value = -1 then
    QrFisRegUFsModBC_TXT.Value := ''
  else
    QrFisRegUFsModBC_TXT.Value := FormatFloat('0', QrFisRegUFsModBC.Value);
end;

procedure TFmFisRegCad.QrFisRegUFxAfterOpen(DataSet: TDataSet);
begin
  BtExcecao.Enabled := True;
end;

procedure TFmFisRegCad.QrFisRegUFxBeforeClose(DataSet: TDataSet);
begin
  BtExcecao.Enabled := True;
end;

procedure TFmFisRegCad.QrFisRegUFxCalcFields(DataSet: TDataSet);
begin
  if QrFisRegUFxModBC.Value = -1 then
    QrFisRegUFxModBC_TXT.Value := ''
  else
    QrFisRegUFxModBC_TXT.Value := FormatFloat('0', QrFisRegUFxModBC.Value);
end;

procedure TFmFisRegCad.ReopenCFOP_Frete();
begin
  //WHERE Codigo=CONCAT(SUBSTRING(1234, 1, 1), '.', SUBSTRING(1234, 2, 4))
  UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP_Frete, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
end;

procedure TFmFisRegCad.ReopenFisRegCFOP(CFOP: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegCFOP, Dmod.MyDB, [
  'SELECT fgc.*, ',
{$IfNDef NAO_GFAT}
  'cf1.Nome NO_CFOP, cf2.Nome NO_OriCFOP ',
{$Else}
  '"" NO_CFOP, "" NO_OriCFOP ',
{$EndIf}
  'FROM fisregcfop fgc ',
{$IfNDef NAO_GFAT}
  'LEFT JOIN cfop2003 cf1 ON cf1.Codigo=fgc.CFOP ',
  'LEFT JOIN cfop2003 cf2 ON cf2.Codigo=fgc.OriCFOP ',
{$EndIf}
  'WHERE fgc.Codigo=' + Geral.FF0(QrFisRegCadCodigo.Value),
  '']);
  //
  BtCFOP.Enabled := (QrFisRegCad.RecordCount > 0) and (QrFisRegCadCodigo.Value <> 0);
  //
  QrFisRegCFOP.Locate('CFOP', CFOP, []);
end;

procedure TFmFisRegCad.ReopenFisRegEnt(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegEnt, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'fre.*  ',
  'FROM fisregent fre ',
  'LEFT JOIN entidades ent ON ent.Codigo=fre.Entidade ',
  'WHERE fre.Codigo=' + Geral.FF0(QrFisRegCadCodigo.Value),
  '']);
  //
  if Entidade <> 0 then
    QrFisRegEnt.Locate('Entidade', Entidade, []);
end;

procedure TFmFisRegCad.ReopenFisRegMvt(Controle: Integer);
var
  Habilita: Boolean;
begin
  QrFisRegMvt.Close;
  QrFisRegMvt.Params[0].AsInteger :=   QrFisRegCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrFisRegMvt, Dmod.MyDB);
  //
  Habilita := (QrFisRegCad.RecordCount > 0) and (QrFisRegCadCodigo.Value <> 0);
  BtMovimentacao.Enabled := Habilita;
  BtEstricao.Enabled := Habilita;
  //
  if Controle <> 0 then
    QrFisRegMvt.Locate('Controle', Controle, []);
end;

procedure TFmFisRegCad.ReopenFisRegUFs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegUFs, Dmod.MyDB, [
  'SELECT *  ',
  'FROM Fisregufs ',
  'WHERE Controle='  + Geral.FF0(QrFisRegCFOPControle.Value),
(*
  'WHERE Codigo='  + Geral.FF0(QrFisRegCFOPCodigo.Value),
  'AND Interno='   + Geral.FF0(QrFisRegCFOPInterno.Value),
  'AND Contribui=' + Geral.FF0(QrFisRegCFOPContribui.Value),
  'AND Proprio='   + Geral.FF0(QrFisRegCFOPProprio.Value),
  'AND SubsTrib='  + Geral.FF0(QrFisRegCFOPSubsTrib.Value),
  'AND Servico='  + Geral.FF0(QrFisRegCFOPServico.Value),
  'AND OriCFOP="'  + QrFisRegCFOPOriCFOP.Value + '"',
  'ORDER BY UFEmit, UFDest ',
*)
  '']);
  //
end;

procedure TFmFisRegCad.ReopenFisRegUFx();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegUFx, Dmod.MyDB, [
  'SELECT * ',
  'FROM Fisregufx',
  'WHERE Conta='  + Geral.FF0(QrFisRegUFsConta.Value),
(*
  'WHERE Codigo='  + Geral.FF0(QrFisRegUFsCodigo.Value),
  'AND Interno='   + Geral.FF0(QrFisRegUFsInterno.Value),
  'AND Contribui=' + Geral.FF0(QrFisRegUFsContribui.Value),
  'AND Proprio='   + Geral.FF0(QrFisRegUFsProprio.Value),
  'AND SubsTrib='  + Geral.FF0(QrFisRegUFsSubsTrib.Value),
  'AND Servico='   + Geral.FF0(QrFisRegCFOPServico.Value),
  'AND OriCFOP="'  + QrFisRegCFOPOriCFOP.Value + '"',
  'AND UFEmit="'   + QrFisRegUFsUFEmit.Value + '"',
  'AND UFDest="'   + QrFisRegUFsUFDest.Value + '"',
  'ORDER BY Nivel, CodNiv',
*)
  '']);
end;

procedure TFmFisRegCad.RetiraCFOPAtual1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFisRegCFOP, TDBGrid(DBGFisRegCFOP),
    'FisRegCFOP', ['Codigo', 'Interno','Contribui', 'Proprio', 'Servico', 'SubsTrib', 'OriCFOP'],
    ['Codigo', 'Interno', 'Contribui', 'Proprio', 'Servico', 'SubsTrib', 'OriCFOP'], istPergunta, '');
end;

procedure TFmFisRegCad.RGOriTESClick(Sender: TObject);
begin
  CkEFD_II_C195.Checked := RGOriTES.ItemIndex = 2;
end;

//Corre��es
procedure TFmFisRegCad.CorrigeFisRegMvt;
var
  Query: TmySQLQuery;
begin
  Query := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT CorrigeFisRegMvt',
      'FROM controle',
      'WHERE CorrigeFisRegMvt=0',
      '']);
    if Query.RecordCount > 0 then
    begin
      try
        UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
          'ALTER TABLE fisregmvt',
          'DROP INDEX UNIQUE2;',
          '']);
      finally
        UnDmkDAC_PF.ExecutaMySQLQuery0(Query, Dmod.MyDB, [
          'UPDATE controle SET CorrigeFisRegMvt=1',
          '']);
      end;
    end;
  finally
    Query.Free;
  end;
end;

(*
SELECT *
FROM PRODUTO
WHERE CLASSIFICACAOFISCAL=6

https://www.confaz.fazenda.gov.br/legislacao/convenios/2021
*)

end.

