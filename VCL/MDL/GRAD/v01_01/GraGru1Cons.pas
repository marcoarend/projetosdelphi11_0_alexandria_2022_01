unit GraGru1Cons;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmGraGru1Cons = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    CBNiv1_COMP: TdmkDBLookupComboBox;
    EdNiv1_COMP: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    Label51: TLabel;
    Label52: TLabel;
    EdQtd_Comp: TdmkEdit;
    EdPerda: TdmkEdit;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    EdControle: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdNiv1_COMPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenGraGru1Cons(Niv1_COMP: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ReopenGraGru1(GGXExcluso: Integer);
  end;

  var
  FmGraGru1Cons: TFmGraGru1Cons;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmGraGru1Cons.BtOKClick(Sender: TObject);
var
  Nivel1, Niv1_COMP, Controle: Integer;
  Qtd_Comp, Perda: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Nivel1         := Geral.IMV(DBEdCodigo.Text);
  Niv1_COMP  := EdNiv1_COMP.ValueVariant;
  Qtd_Comp       := EdQtd_Comp.ValueVariant;
  Perda          := EdPerda.ValueVariant;
  Controle       := EdControle.ValueVariant;
  //
  if MyObjects.FIC(Niv1_COMP = 0, EdNiv1_COMP, 'Informe o produto de consumo!') then
    Exit;
  if MyObjects.FIC(Qtd_Comp = 0, EdQtd_Comp, 'Informe a quantidade!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('gragru1cons', 'Controle', '', '',
    tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru1cons', False, [
  'Qtd_Comp', 'Perda',
  'Nivel1', 'Niv1_COMP'], [
  'Controle'], [
  Qtd_Comp, Perda,
  Nivel1, Niv1_COMP], [
  Controle], True) then
  begin
    ReopenGraGru1Cons(Niv1_COMP);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType              := stIns;
      EdControle.ValueVariant      := 0;
      EdNiv1_COMP.ValueVariant := 0;
      CBNiv1_COMP.KeyValue     := Null;
      EdQtd_Comp.ValueVariant      := 0;
      EdPerda.ValueVariant         := 0;
      //
      EdNiv1_COMP.SetFocus;
    end else Close;
  end;
end;

procedure TFmGraGru1Cons.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGru1Cons.EdNiv1_COMPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  Aviso  = '...';
  Titulo = 'Sele��o de OP';
  Prompt = 'Informe o n�mero da OP';
  Campo  = 'Descricao';
var
  OP, Codigo, MovimCod, Controle, Ctrl1: Integer;
  Resp: Variant;
  Qry: TmySQLQuery;
  //
  Nivel1:String;
begin
  if Key = VK_F4 then
  begin
    Nivel1 := Geral.SoNumero_TT(DBEdCodigo.Text);
    //
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    ' DROP TABLE IF EXISTS _efd_0210_a; ',
    'CREATE TABLE _efd_0210_a ',
    '  SELECT k230.*',
    '  FROM ' + TMeuDB + '.efd_K230 k230',
    '  LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=k230.GraGruX',
    '  WHERE ggx.GraGru1=' + Nivel1,
    '; ',
    'DROP TABLE IF EXISTS _efd_0210_b; ',
    'CREATE TABLE _efd_0210_b ',
    'SELECT DISTINCT _235.GraGruX ',
    'FROM ' + TMeuDB + '.efd_K235 _235 ',
    'LEFT JOIN _efd_0210_a _a ON _a.LinArq=_235.K230 ',
    'AND _235.ImporExpor=_a.ImporExpor ',
    'AND _235.AnoMes=_a.AnoMes',
    'AND _235.Empresa=_a.Empresa ',
    'AND _235.K230=_a.LinArq ',
    'WHERE _a.GraGruX IS NOT NULL',
    ';',
    '',
    'DROP TABLE IF EXISTS _efd_0210_a;',
    'CREATE TABLE _efd_0210_a',
    '  SELECT k250.*',
    '  FROM ' + TMeuDB + '.efd_K250 k250',
    '  LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=k250.GraGruX',
    '  WHERE ggx.GraGru1=' + Nivel1,
    ';',
    'INSERT INTO _efd_0210_b ',
    'SELECT DISTINCT _255.GraGruX',
    'FROM ' + TMeuDB + '.efd_K255 _255',
    'LEFT JOIN _efd_0210_a _a ON _a.LinArq=_255.K250',
    'AND _255.ImporExpor=_a.ImporExpor',
    'AND _255.AnoMes=_a.AnoMes',
    'AND _255.Empresa=_a.Empresa',
    'AND _255.K250=_a.LinArq',
    'WHERE _a.GraGruX IS NOT NULL',
    ';',
    '',
    'DROP TABLE IF EXISTS _efd_0210_c;',
    'CREATE TABLE _efd_0210_c',
    '  SELECT Niv1_COMP',
    '  FROM ' + TMeuDB + '.gragru1cons',
    '  WHERE Nivel1=' + Nivel1,
    ';',
    '',
    '',
(*
    'SELECT ggx.GraGru1 gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  Descricao ',
    'FROM _efd_0210_b _b ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=_b.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  ',
    'LEFT JOIN _efd_0210_c               _c ON  _c.Niv1_COMP=gg1.Nivel1',
    'WHERE _b.GraGruX <> 0',
    'AND _c.Niv1_COMP IS NULL',
    'ORDER BY Descricao ',
*)
    'SELECT DISTINCT ggx.GraGru1 Codigo, gg1.Nome Descricao ',
    'FROM _efd_0210_b _b ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=_b.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN _efd_0210_c             _c ON  _c.Niv1_COMP=gg1.Nivel1',
    'WHERE _b.GraGruX <> 0',
    'AND _c.Niv1_COMP IS NULL',
    'ORDER BY Descricao; ',
    //
    'DROP TABLE IF EXISTS _efd_0210_a;',
    'DROP TABLE IF EXISTS _efd_0210_b;',
    'DROP TABLE IF EXISTS _efd_0210_c;',
    //
    ''], DModG.MyPID_DB, True);
    if Resp <> Null then
    begin
      EdNiv1_COMP.ValueVariant := Resp;
      CBNiv1_COMP.KeyValue := Resp;
    end;
  end;
end;

procedure TFmGraGru1Cons.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmGraGru1Cons.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmGraGru1Cons.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGru1Cons.ReopenGraGru1Cons(Niv1_COMP: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Niv1_COMP <> 0 then
      FQrIts.Locate('Niv1_COMP', Niv1_COMP, []);
  end;
end;

procedure TFmGraGru1Cons.ReopenGraGru1(GGXExcluso: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
  'SELECT gg1.Nivel1, gg1.Nome',
  'FROM gragru1 gg1',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'WHERE gg1.Nivel1 > 0',
  'AND gg1.Nivel1 <> ' + Geral.FF0(GGXExcluso),
  //REGISTRO 0210: CONSUMO ESPEC�FICO PADRONIZADO
  //Este registro somente deve existir quando o conte�do do campo 7 - TIPO_ITEM do Registro 0200 for igual a 03
  //(produto em processo) ou 04 (produto acabado).
  //'AND pgt.Tipo_Item IN (3,4)',
  'ORDER BY gg1.Nome',
  '']);
end;

end.
