object FmGraSrvValIts: TFmGraSrvValIts
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-047 :: Item de Tabela de Presta'#231#227'o de Servi'#231'o'
  ClientHeight = 436
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 602
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 554
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 506
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 490
        Height = 32
        Caption = ' Item de Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 490
        Height = 32
        Caption = ' Item de Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 490
        Height = 32
        Caption = ' Item de Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 602
    Height = 274
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 602
      Height = 274
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 602
        Height = 274
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 598
          Height = 257
          Align = alClient
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 594
            Height = 86
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 12
              Top = 4
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label2: TLabel
              Left = 12
              Top = 44
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object Label5: TLabel
              Left = 480
              Top = 4
              Width = 42
              Height = 13
              Caption = 'Controle:'
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 68
              Top = 20
              Width = 409
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DsEmpresas
              TabOrder = 1
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdEmpresa: TdmkEditCB
              Left = 12
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdClientSrv: TdmkEditCB
              Left = 12
              Top = 60
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBClientSrv
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBClientSrv: TdmkDBLookupComboBox
              Left = 68
              Top = 60
              Width = 513
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              ListSource = DsClientes
              TabOrder = 3
              dmkEditCB = EdClientSrv
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdControle: TdmkEdit
              Left = 480
              Top = 20
              Width = 101
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object Panel6: TPanel
            Left = 2
            Top = 101
            Width = 594
            Height = 128
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object LaPrdGrupTip: TLabel
              Left = 12
              Top = 4
              Width = 40
              Height = 13
              Caption = 'Produto:'
            end
            object SbGraGru1: TSpeedButton
              Left = 560
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbGraGru1Click
            end
            object LaGrade: TLabel
              Left = 12
              Top = 47
              Width = 96
              Height = 13
              Caption = 'Grade de tamanhos:'
            end
            object SBGrade: TSpeedButton
              Left = 192
              Top = 62
              Width = 23
              Height = 22
              Caption = '...'
              OnClick = SBGradeClick
            end
            object Label3: TLabel
              Left = 220
              Top = 47
              Width = 48
              Height = 13
              Caption = 'Tamanho:'
            end
            object SbGraCorCad: TSpeedButton
              Left = 560
              Top = 62
              Width = 23
              Height = 22
              Caption = '...'
              OnClick = SbGraCorCadClick
            end
            object Label4: TLabel
              Left = 320
              Top = 47
              Width = 19
              Height = 13
              Caption = 'Cor:'
            end
            object Label6: TLabel
              Left = 12
              Top = 88
              Width = 39
              Height = 13
              Caption = 'Servi'#231'o:'
            end
            object Label7: TLabel
              Left = 480
              Top = 88
              Width = 31
              Height = 13
              Caption = 'Pre'#231'o:'
            end
            object SbReduzSrv: TSpeedButton
              Left = 452
              Top = 104
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbReduzSrvClick
            end
            object EdGraGRu1: TdmkEditCB
              Left = 12
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGraGru1
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraGru1: TdmkDBLookupComboBox
              Left = 68
              Top = 20
              Width = 489
              Height = 21
              KeyField = 'Nivel1'
              ListField = 'Nome'
              ListSource = DsGraGru1
              TabOrder = 1
              dmkEditCB = EdGraGRu1
              QryCampo = 'PrdGrupTip'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGraTamCad: TdmkEditCB
              Left = 12
              Top = 63
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdGraTamCadRedefinido
              DBLookupComboBox = CBGraTamCad
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraTamCad: TdmkDBLookupComboBox
              Left = 52
              Top = 63
              Width = 141
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsGraTamCad
              TabOrder = 3
              dmkEditCB = EdGraTamCad
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGraTamIts: TdmkEditCB
              Left = 220
              Top = 63
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGraTamIts
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraTamIts: TdmkDBLookupComboBox
              Left = 260
              Top = 63
              Width = 57
              Height = 21
              KeyField = 'Controle'
              ListField = 'Nome'
              ListSource = DsGraTamIts
              TabOrder = 5
              dmkEditCB = EdGraTamIts
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGraCorCad: TdmkEditCB
              Left = 320
              Top = 63
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGraCorCad
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraCorCad: TdmkDBLookupComboBox
              Left = 376
              Top = 63
              Width = 181
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsGraCorCad
              TabOrder = 7
              dmkEditCB = EdGraCorCad
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdReduzSrv: TdmkEditCB
              Left = 12
              Top = 104
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBReduzSrv
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBReduzSrv: TdmkDBLookupComboBox
              Left = 68
              Top = 104
              Width = 381
              Height = 21
              KeyField = 'Controle'
              ListField = 'Nome'
              ListSource = DsReduzSrv
              TabOrder = 9
              dmkEditCB = EdReduzSrv
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPreco: TdmkEdit
              Left = 480
              Top = 104
              Width = 105
              Height = 21
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object Panel7: TPanel
            Left = 2
            Top = 229
            Width = 594
            Height = 26
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object CkContinuar: TCheckBox
              Left = 16
              Top = 4
              Width = 117
              Height = 17
              Caption = 'Continuar inserindo.'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 322
    Width = 602
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 598
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 366
    Width = 602
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 456
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 454
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cli.Codigo, '
      'IF(cli.Tipo=0,RazaoSocial,Cli.Nome) NOMEENT '
      'FROM entidades cli '
      'ORDER BY NOMEENT')
    Left = 192
    Top = 52
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 192
    Top = 100
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, Nome'
      'FROM GraGru1'
      'ORDER BY Nome')
    Left = 248
    Top = 52
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 248
    Top = 100
  end
  object QrGraTamCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gratamcad '
      'ORDER BY Nome')
    Left = 316
    Top = 52
    object QrGraTamCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraTamCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraTamCad: TDataSource
    DataSet = QrGraTamCad
    Left = 316
    Top = 100
  end
  object QrGraTamIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 52
    object QrGraTamItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraTamItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 5
    end
  end
  object DsGraTamIts: TDataSource
    DataSet = QrGraTamIts
    Left = 384
    Top = 100
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM gracorcad'
      'ORDER BY Nome'
      '')
    Left = 444
    Top = 52
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 444
    Top = 100
  end
  object QrReduzSrv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome '
      'FROM gragru1 gg1 '
      'RIGHT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'WHERE pgt.TipPrd=4')
    Left = 510
    Top = 52
    object QrReduzSrvControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrReduzSrvNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsReduzSrv: TDataSource
    DataSet = QrReduzSrv
    Left = 510
    Top = 100
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial, ent.CliInt, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, IE, NIRE'
      'FROM senhasits sei'
      'LEFT JOIN entidades ent ON ent.Codigo=sei.Empresa'
      'WHERE sei.Numero=:P0'
      'AND ent.Codigo < -10')
    Left = 132
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresasFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresasNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrEmpresasCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresasIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresasNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 128
    Top = 92
  end
end
