unit GraGruN;

interface

uses
  Windows, Messages, SysUtils, StrUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, DmkDAC_PF,
  dmkPermissoes, mySQLDbTables, dmkGeral, MyDBCheck, DBCGrids, Mask, DBCtrls,
  dmkDBEdit, ComCtrls, Menus, dmkEdit, dmkLabel, dmkDBGridZTO, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB, dmkCheckGroup, Variants, UnDmkProcFunc,
  dmkImage, UnDmkEnums, ModProd, UnGrade_Jan, dmkDBGrid, UnProjGroup_Consts;

type
  TFmGraGruN = class(TForm)
    Panel1: TPanel;
    QrGraGru3: TmySQLQuery;
    DsGraGru3: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    QrGraGruN: TmySQLQuery;
    QrGraGruNCodigo: TIntegerField;
    QrGraGruNCodUsu: TIntegerField;
    QrGraGruNNome: TWideStringField;
    QrGraGruNMadeBy: TSmallintField;
    QrGraGruNFracio: TSmallintField;
    QrGraGruNTipPrd: TSmallintField;
    QrGraGruNNivCad: TSmallintField;
    QrGraGruNFaixaIni: TIntegerField;
    QrGraGruNFaixaFim: TIntegerField;
    QrGraGruNNOME_MADEBY: TWideStringField;
    QrGraGruNNOME_FRACIO: TWideStringField;
    QrGraGruNNOME_TIPPRD: TWideStringField;
    DsGraGruN: TDataSource;
    PainelDados: TPanel;
    QrGraGru2: TmySQLQuery;
    DsGraGru2: TDataSource;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru2Codusu: TIntegerField;
    QrGraGru2Nivel3: TIntegerField;
    QrGraGru2Nivel2: TIntegerField;
    QrGraGru2Nome: TWideStringField;
    SplitterMeio: TSplitter;
    Panel9: TPanel;
    PCGeral: TPageControl;
    TabSheet1: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet2: TTabSheet;
    GradeE: TStringGrid;
    StaticText3: TStaticText;
    TabSheet3: TTabSheet;
    GradeP: TStringGrid;
    Panel11: TPanel;
    StaticText5: TStaticText;
    DBGrid6: TdmkDBGridZTO;
    QrGraTamI: TmySQLQuery;
    DsGraTamI: TDataSource;
    QrGraTamIControle: TAutoIncField;
    QrGraTamINome: TWideStringField;
    PMCor: TPopupMenu;
    Incluinovacor1: TMenuItem;
    Alteracoratual1: TMenuItem;
    Excluicoratual1: TMenuItem;
    QrGraGruC: TmySQLQuery;
    DsGraGruC: TDataSource;
    QrGraGruCNivel1: TIntegerField;
    QrGraGruCControle: TIntegerField;
    QrGraGruCGraCorCad: TIntegerField;
    QrGraGruCCodUsuGTC: TIntegerField;
    QrGraGruCNomeGTC: TWideStringField;
    TabSheet4: TTabSheet;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGruNTitNiv1: TWideStringField;
    QrGraGruNTitNiv2: TWideStringField;
    QrGraGruNTitNiv3: TWideStringField;
    QrGraGruNTitNiv4: TWideStringField;
    QrGraGruNTitNiv5: TWideStringField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    TabSheet6: TTabSheet;
    GradeAtrIts: TdmkDBGridZTO;
    PMAtributos: TPopupMenu;
    Incluinovoitemdeatributo1: TMenuItem;
    Alteraitemdeatributoselecionado1: TMenuItem;
    Excluiitemdeatributoselecionado1: TMenuItem;
    DsGraAtrI: TDataSource;
    QrGraAtrI: TmySQLQuery;
    Incluinovascoresvriasdeumavez1: TMenuItem;
    Incluidiversosnovositensdeatributo1: TMenuItem;
    QrGraCusPrcU: TmySQLQuery;
    DsGraCusPrcU: TDataSource;
    QrGraCusPrcUCodigo: TIntegerField;
    QrGraCusPrcUCodUsu: TIntegerField;
    QrGraCusPrcUNome: TWideStringField;
    QrGraCusPrcUCusPrc: TWideStringField;
    QrGraCusPrcUNomeTC: TWideStringField;
    QrGraCusPrcUNOMEMOEDA: TWideStringField;
    TabSheet7: TTabSheet;
    GradeX: TStringGrid;
    QrGraCusPrcUSIGLAMOEDA: TWideStringField;
    QrGraAtrIGraAtrCad: TIntegerField;
    QrGraAtrIGraAtrIts: TIntegerField;
    QrGraAtrICTRL_ATR: TIntegerField;
    QrGraAtrICTRL_ITS: TIntegerField;
    QrGraAtrICODUSU_ITS: TIntegerField;
    QrGraAtrINOME_ITS: TWideStringField;
    QrGraAtrICODUSU_CAD: TIntegerField;
    QrGraAtrINOME_CAD: TWideStringField;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Label4: TLabel;
    EdFiltroAtribCad: TEdit;
    Label5: TLabel;
    EdFiltroAtribIts: TEdit;
    QrGraGru1IPI_Alq: TFloatField;
    QrGraGru1IPI_cEnq: TWideStringField;
    QrGraGru1TipDimens: TSmallintField;
    TabSheet8: TTabSheet;
    QrGraGru1PerCuztMin: TFloatField;
    QrGraGru1PerCuztMax: TFloatField;
    QrGraGru1MedOrdem: TIntegerField;
    QrGraGru1PartePrinc: TIntegerField;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel16: TPanel;
    Label3: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit6: TDBEdit;
    TabSheet9: TTabSheet;
    Panel20: TPanel;
    Panel21: TPanel;
    RGTipDimens: TDBRadioGroup;
    Panel19: TPanel;
    Label13: TLabel;
    Label6: TLabel;
    Panel22: TPanel;
    Label15: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    DBEdit4: TDBEdit;
    QrGraGru1NO_MedOrdem: TWideStringField;
    QrGraGru1Medida1: TWideStringField;
    QrGraGru1Medida2: TWideStringField;
    QrGraGru1Medida3: TWideStringField;
    QrGraGru1Medida4: TWideStringField;
    QrGraGru1NO_PartePrinc: TWideStringField;
    DBEdit8: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label7: TLabel;
    DBEdit17: TDBEdit;
    Label8: TLabel;
    DBEdit18: TDBEdit;
    Label9: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label10: TLabel;
    QrGraGru1InfAdProd: TWideStringField;
    QrGraGru1SiglaCustm: TWideStringField;
    QrGraGru1HowBxaEstq: TSmallintField;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    QrGraGru1GerBxaEstq: TSmallintField;
    DBRadioGroup1: TDBRadioGroup;
    TabSheet10: TTabSheet;
    PageControl3: TPageControl;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    Panel23: TPanel;
    Panel24: TPanel;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label22: TLabel;
    DBEdit22: TDBEdit;
    Label18: TLabel;
    DBEdit21: TDBEdit;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    Panel25: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    GroupBox4: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    EdCST_A: TDBEdit;
    EdTextoA: TdmkEdit;
    EdCST_B: TDBEdit;
    EdTextoB: TdmkEdit;
    DBRadioGroup3: TDBRadioGroup;
    QrGraGru1PIS_AlqP: TFloatField;
    QrGraGru1PIS_AlqV: TFloatField;
    QrGraGru1PISST_AlqP: TFloatField;
    QrGraGru1PISST_AlqV: TFloatField;
    QrGraGru1COFINS_AlqP: TFloatField;
    QrGraGru1COFINS_AlqV: TFloatField;
    QrGraGru1COFINSST_AlqP: TFloatField;
    QrGraGru1COFINSST_AlqV: TFloatField;
    QrGraGru1ICMS_modBC: TSmallintField;
    QrGraGru1ICMS_modBCST: TSmallintField;
    QrGraGru1ICMS_pRedBC: TFloatField;
    QrGraGru1ICMS_pRedBCST: TFloatField;
    QrGraGru1ICMS_pMVAST: TFloatField;
    QrGraGru1ICMS_pICMSST: TFloatField;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    GroupBox2: TGroupBox;
    Label28: TLabel;
    EdIPI_CST: TDBEdit;
    EdTextoIPI_CST: TdmkEdit;
    TabSheet14: TTabSheet;
    GroupBox5: TGroupBox;
    Label16: TLabel;
    EdTextoPIS_CST: TdmkEdit;
    Panel26: TPanel;
    Label17: TLabel;
    Label20: TLabel;
    GroupBox6: TGroupBox;
    Label29: TLabel;
    Label30: TLabel;
    EdPIS_CST: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    TabSheet15: TTabSheet;
    GroupBox8: TGroupBox;
    Label31: TLabel;
    Label32: TLabel;
    Panel27: TPanel;
    Label33: TLabel;
    Label34: TLabel;
    GroupBox7: TGroupBox;
    Label35: TLabel;
    EdTextoCOFINS_CST: TdmkEdit;
    EdCOFINS_CST: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    QrGraGru1IPI_TpTrib: TSmallintField;
    QrGraGru1IPI_vUnid: TFloatField;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    DBEdit27: TDBEdit;
    DBRadioGroup4: TDBRadioGroup;
    QrGraGru1IPI_CST: TSmallintField;
    QrGraGru1PIS_CST: TSmallintField;
    QrGraGru1COFINS_CST: TSmallintField;
    QrGraGru1ICMS_Pauta: TFloatField;
    QrGraGru1ICMS_MaxTab: TFloatField;
    QrGraGru1IPI_pIPI: TFloatField;
    QrGraGru1cGTIN_EAN: TWideStringField;
    QrGraGru1EX_TIPI: TWideStringField;
    QrGraGru1PIS_pRedBC: TFloatField;
    QrGraGru1PISST_pRedBCST: TFloatField;
    QrGraGru1COFINS_pRedBC: TFloatField;
    QrGraGru1COFINSST_pRedBCST: TFloatField;
    DBEdit32: TDBEdit;
    DBEdit37: TDBEdit;
    Label19: TLabel;
    Label21: TLabel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    Label39: TLabel;
    Label40: TLabel;
    QrGraGru1ICMSRec_pRedBC: TFloatField;
    QrGraGru1IPIRec_pRedBC: TFloatField;
    QrGraGru1PISRec_pRedBC: TFloatField;
    QrGraGru1COFINSRec_pRedBC: TFloatField;
    Label41: TLabel;
    DBEdit40: TDBEdit;
    Label42: TLabel;
    DBEdit41: TDBEdit;
    Label43: TLabel;
    DBEdit42: TDBEdit;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1IPIRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    QrGraGru1ICMSRec_tCalc: TSmallintField;
    QrGraGru1IPIRec_tCalc: TSmallintField;
    QrGraGru1PISRec_tCalc: TSmallintField;
    QrGraGru1COFINSRec_tCalc: TSmallintField;
    GroupBox9: TGroupBox;
    Label45: TLabel;
    Label46: TLabel;
    DBRadioGroup5: TDBRadioGroup;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    GroupBox10: TGroupBox;
    Label47: TLabel;
    Label48: TLabel;
    DBRadioGroup6: TDBRadioGroup;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    GroupBox11: TGroupBox;
    Label49: TLabel;
    Label50: TLabel;
    DBRadioGroup7: TDBRadioGroup;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    GroupBox12: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    DBRadioGroup8: TDBRadioGroup;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    QrGraGru1ICMSAliqSINTEGRA: TFloatField;
    QrGraGru1ICMSST_BaseSINTEGRA: TFloatField;
    QrGraGru1FatorClas: TIntegerField;
    QrGraGruNImpedeCad: TSmallintField;
    PMNumero: TPopupMenu;
    Reduzido1: TMenuItem;
    PMListaPrecos: TPopupMenu;
    Atualizapreomdioeltimacompra1: TMenuItem;
    TabSheet17: TTabSheet;
    Panel29: TPanel;
    DBGrid1: TDBGrid;
    QrGraGruCST: TmySQLQuery;
    DsGraGruCST: TDataSource;
    PMEspecial: TPopupMenu;
    Incluinovoitem1: TMenuItem;
    Alteraitem1: TMenuItem;
    Excluiitemselecionado1: TMenuItem;
    QrGraGruCSTNivel1: TIntegerField;
    QrGraGruCSTUF_Orig: TWideStringField;
    QrGraGruCSTUF_Dest: TWideStringField;
    QrGraGruCSTCST_B: TSmallintField;
    IDNivel11: TMenuItem;
    CdigoNivel11: TMenuItem;
    QrLocod: TmySQLQuery;
    QrLocodPrdGrupTip: TIntegerField;
    QrLocodNivel1: TIntegerField;
    QrLocodNivel2: TIntegerField;
    QrLocodNivel3: TIntegerField;
    QrGraGru1PerComissF: TFloatField;
    QrGraGru1PerComissR: TFloatField;
    QrGraGruNPerComissF: TFloatField;
    QrGraGruNPerComissR: TFloatField;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    Label55: TLabel;
    Label56: TLabel;
    QrGraGru1PerComissZ: TSmallintField;
    Panel4: TPanel;
    PnGrades: TPanel;
    Panel5: TPanel;
    Panel8: TPanel;
    Label2: TLabel;
    DBCtrlGrid1: TDBCtrlGrid;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    PnPaineis: TPanel;
    Splitter3: TSplitter;
    Splitter2: TSplitter;
    Splitter1: TSplitter;
    PainelN: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdFiltroNiv0: TEdit;
    DBGPrdgrupTip: TdmkDBGridZTO;
    Painel1: TPanel;
    DBGNivel1: TdmkDBGridZTO;
    Panel10: TPanel;
    LaNivel1: TLabel;
    EdFiltroNiv1: TEdit;
    Painel2: TPanel;
    DBGNivel2: TdmkDBGridZTO;
    Panel12: TPanel;
    LaNivel2: TLabel;
    EdFiltroNiv2: TEdit;
    Painel3: TPanel;
    DBGNivel3: TdmkDBGridZTO;
    Panel30: TPanel;
    LaNivel3: TLabel;
    EdFiltroNiv3: TEdit;
    PMQuery: TPopupMenu;
    Nivel12: TMenuItem;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENTI: TWideStringField;
    DsClientes: TDataSource;
    QrGraGru1COD_LST: TWideStringField;
    DBEdit53: TDBEdit;
    Label57: TLabel;
    Label54: TLabel;
    QrGraGru1SPEDEFD_ALIQ_ICMS: TFloatField;
    QrGraGruEX: TmySQLQuery;
    DsGraGruEX: TDataSource;
    QrGraGruEXNO_EMBALAG: TWideStringField;
    QrGraGruEXcProd: TWideStringField;
    QrGraGruEXNivel1: TIntegerField;
    QrGraGruEXFornece: TIntegerField;
    QrGraGruEXEmbalagem: TIntegerField;
    QrGraGruEXObservacao: TWideStringField;
    QrGraGruEXGraGruX: TIntegerField;
    QrGraGruEXNO_FORNECE: TWideStringField;
    QrGraGruVinc: TmySQLQuery;
    DsGraGruVinc: TDataSource;
    QrGraGruVincNome: TWideStringField;
    QrGraGruVincGraGruX: TIntegerField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXGraCorCad: TIntegerField;
    QrGraGruXGraGruC: TIntegerField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraTamI: TIntegerField;
    Panel36: TPanel;
    GradeC: TStringGrid;
    StaticText1: TStaticText;
    SplitterQuarto: TSplitter;
    Panel33: TPanel;
    SBReduzido: TStatusBar;
    PageControl4: TPageControl;
    TabSheet18: TTabSheet;
    GradeGraGruEX: TdmkDBGridZTO;
    TabSheet19: TTabSheet;
    GradeGraGruVinc: TDBGrid;
    Panel34: TPanel;
    BtExclVinc: TBitBtn;
    Panel37: TPanel;
    DBGrid2: TDBGrid;
    DsGraGruX: TDataSource;
    Panel38: TPanel;
    StaticText6: TStaticText;
    BtReduzidoL: TBitBtn;
    PMReduzidoL: TPopupMenu;
    CadastrodoReduzido1: TMenuItem;
    Gerenciamentodoreduzido1: TMenuItem;
    QrGraGruXGraTamCad: TIntegerField;
    QrGraGru1Referencia: TWideStringField;
    Label58: TLabel;
    DBEdit54: TDBEdit;
    DBRadioGroup9: TDBRadioGroup;
    QrGraGru1DadosFisc: TSmallintField;
    Referencia1: TMenuItem;
    Painel4: TPanel;
    DBGNivel4: TdmkDBGridZTO;
    Panel40: TPanel;
    LaNivel4: TLabel;
    EdFiltroNiv4: TEdit;
    Painel5: TPanel;
    DBGNivel5: TdmkDBGridZTO;
    Panel42: TPanel;
    LaNivel5: TLabel;
    EdFiltroNiv5: TEdit;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    QrGraGru5: TmySQLQuery;
    QrGraGru4: TmySQLQuery;
    DsGraGru5: TDataSource;
    DsGraGru4: TDataSource;
    QrGraGru5Codusu: TIntegerField;
    QrGraGru5Nivel5: TIntegerField;
    QrGraGru5Nome: TWideStringField;
    QrGraGru3Codusu: TIntegerField;
    QrGraGru3Nivel4: TIntegerField;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru4Codusu: TIntegerField;
    QrGraGru4Nivel5: TIntegerField;
    QrGraGru4Nivel4: TIntegerField;
    QrGraGru4Nome: TWideStringField;
    QrLocodNivel4: TIntegerField;
    QrLocodNivel5: TIntegerField;
    QrGraGru1Nivel4: TIntegerField;
    QrGraGru1Nivel5: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel3: TPanel;
    BtCor: TBitBtn;
    BtGrade: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtDados: TBitBtn;
    BtAtributos: TBitBtn;
    BtFiscal: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel39: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraGru5PrdGrupTip: TIntegerField;
    TabSheet20: TTabSheet;
    DBGrid3: TDBGrid;
    Panel41: TPanel;
    DBGrid4: TDBGrid;
    QrGraGru1Lau: TmySQLQuery;
    DsGraGru1Lau: TDataSource;
    QrGraGru1LauNivel1: TIntegerField;
    QrGraGru1LauControle: TIntegerField;
    QrGraGru1LauNome: TWideStringField;
    QrGraGru1LauLk: TIntegerField;
    QrGraGru1LauDataCad: TDateField;
    QrGraGru1LauDataAlt: TDateField;
    QrGraGru1LauUserCad: TIntegerField;
    QrGraGru1LauUserAlt: TIntegerField;
    QrGraGru1LauAlterWeb: TSmallintField;
    QrGraGru1LauAtivo: TSmallintField;
    QrGraGru1Lot: TmySQLQuery;
    DsGraGru1Lot: TDataSource;
    QrGraGru1LotNivel1: TIntegerField;
    QrGraGru1LotControle: TIntegerField;
    QrGraGru1LotNome: TWideStringField;
    QrGraGru1LotLk: TIntegerField;
    QrGraGru1LotDataCad: TDateField;
    QrGraGru1LotDataAlt: TDateField;
    QrGraGru1LotUserCad: TIntegerField;
    QrGraGru1LotUserAlt: TIntegerField;
    QrGraGru1LotAlterWeb: TSmallintField;
    QrGraGru1LotAtivo: TSmallintField;
    PMGraGru1Lau: TPopupMenu;
    Incluinovolaudo1: TMenuItem;
    Excluilaudoatual1: TMenuItem;
    PMGraGru1Lot: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    BtOpcoesGrad: TBitBtn;
    BitBtn1: TBitBtn;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    PMGraGru5: TPopupMenu;
    PMGraGru4: TPopupMenu;
    PMGraGru3: TPopupMenu;
    PMGraGru2: TPopupMenu;
    PMGraGru1: TPopupMenu;
    IncluiGru5: TMenuItem;
    AlteraGru5: TMenuItem;
    IncluiGru4: TMenuItem;
    AlteraGru4: TMenuItem;
    IncluiGru3: TMenuItem;
    AlteraGru3: TMenuItem;
    IncluiGru2: TMenuItem;
    AlteraGru2: TMenuItem;
    IncluiGru1: TMenuItem;
    AlteraGru1: TMenuItem;
    ExcluiGru5: TMenuItem;
    ExcluiGru4: TMenuItem;
    ExcluiGru3: TMenuItem;
    ExcluiGru2: TMenuItem;
    ExcluiGru1: TMenuItem;
    Panel43: TPanel;
    BtEspecial: TBitBtn;
    N1: TMenuItem;
    Excluireduzido1: TMenuItem;
    QrGraGruNGradeado: TIntegerField;
    Splitter6: TSplitter;
    BtCodificacao: TBitBtn;
    PMCodificacao: TPopupMenu;
    ProgressBar1: TProgressBar;
    N2: TMenuItem;
    Gerenciarcodificaoprpria1: TMenuItem;
    QrGraGru1ICMS_pDif: TFloatField;
    QrGraGru1ICMS_pICMSDeson: TFloatField;
    QrGraGru1ICMS_motDesICMS: TSmallintField;
    Panel35: TPanel;
    DBRadioGroup2: TDBRadioGroup;
    Label11: TLabel;
    DBEdit23: TDBEdit;
    Label65: TLabel;
    DBEdit55: TDBEdit;
    Label66: TLabel;
    DBEdit56: TDBEdit;
    Label44: TLabel;
    EdDBICMS_motDesICMS_TXT: TEdit;
    EdDBICMS_motDesICMS: TDBEdit;
    QrGraGru1GraTabApp: TIntegerField;
    PMGrupo: TPopupMenu;
    IncluiProduto1: TMenuItem;
    AlteraProduto1: TMenuItem;
    QrGraGru1NO_GraTabApp: TWideStringField;
    QrGraGruNGraTabApp: TIntegerField;
    QrGraGruNGraTabApp_TXT: TWideStringField;
    QrTabePrCCab: TmySQLQuery;
    DsTabePrCCab: TDataSource;
    QrTabePrCCabCodigo: TIntegerField;
    QrTabePrCCabCodUsu: TIntegerField;
    QrTabePrCCabNome: TWideStringField;
    QrGraGru1prod_CEST: TIntegerField;
    TabSheet16: TTabSheet;
    GroupBox15: TGroupBox;
    BtGraGru1Cons: TBitBtn;
    PMGraGru1Cons: TPopupMenu;
    IncluiConsumoEP1: TMenuItem;
    AlteraConsumoEP1: TMenuItem;
    ExcluiConsumoEP1: TMenuItem;
    QrGraGru1Cons: TmySQLQuery;
    DsGraGru1Cons: TDataSource;
    QrGraGru1ConsNO_Niv1_COMP: TWideStringField;
    QrGraGru1ConsNivel1: TIntegerField;
    QrGraGru1ConsQtd_Comp: TFloatField;
    QrGraGru1ConsPerda: TFloatField;
    QrGraGru1ConsLk: TIntegerField;
    QrGraGru1ConsDataCad: TDateField;
    QrGraGru1ConsDataAlt: TDateField;
    QrGraGru1ConsUserCad: TIntegerField;
    QrGraGru1ConsUserAlt: TIntegerField;
    QrGraGru1ConsAlterWeb: TSmallintField;
    QrGraGru1ConsAtivo: TSmallintField;
    DBGGraGru1Cons: TdmkDBGridZTO;
    QrGraGru1ConsControle: TIntegerField;
    Alteradados1: TMenuItem;
    QrGraGru2Tipo_Item: TSmallintField;
    BtNiveis2: TBitBtn;
    AlteraGradedeTamanhos1: TMenuItem;
    QrGraGru1ConsNiv1_COMP: TIntegerField;
    QrGraGruEXxProd: TWideStringField;
    QrGraGruEXNCM: TWideStringField;
    QrGraGruEXCFOP: TIntegerField;
    QrGraGruEXuCom: TWideStringField;
    QrGraGruEXCFOP_Inn: TIntegerField;
    GerenciarcodificaodeFornecedor1: TMenuItem;
    QrGraGru1prod_indEscala: TWideStringField;
    QrGraGruYPGT: TmySQLQuery;
    DsGraGruYPGT: TDataSource;
    BtFichaDeConsumo: TBitBtn;
    TabSheet21: TTabSheet;
    PMFichaDeConsumo: TPopupMenu;
    Inclui1: TMenuItem;
    ItemNobsico1: TMenuItem;
    BasicoInclui1: TMenuItem;
    BasicoAltera1: TMenuItem;
    BasicoExclui1: TMenuItem;
    QrFiConsBas: TMySQLQuery;
    QrFiConsBasGG1Dest: TIntegerField;
    QrFiConsBasControle: TIntegerField;
    QrFiConsBasGGXSorc: TIntegerField;
    QrFiConsBasGGXSubs: TIntegerField;
    QrFiConsBasParte: TIntegerField;
    QrFiConsBasQtdUso: TFloatField;
    QrFiConsBasObrigatorio: TSmallintField;
    QrFiConsBasLk: TIntegerField;
    QrFiConsBasDataCad: TDateField;
    QrFiConsBasDataAlt: TDateField;
    QrFiConsBasUserCad: TIntegerField;
    QrFiConsBasUserAlt: TIntegerField;
    QrFiConsBasAlterWeb: TSmallintField;
    QrFiConsBasAWServerID: TIntegerField;
    QrFiConsBasAWStatSinc: TSmallintField;
    QrFiConsBasAtivo: TSmallintField;
    QrFiConsBasGGXSorc_NO_PRD_TAM_COR: TWideStringField;
    QrFiConsBasGGXSubs_NO_PRD_TAM_COR: TWideStringField;
    Panel28: TPanel;
    Panel31: TPanel;
    DBGFiConsBas: TdmkDBGridZTO;
    Panel44: TPanel;
    Label67: TLabel;
    Label68: TLabel;
    Splitter7: TSplitter;
    DsFiConsBas: TDataSource;
    QrFiConsBasNO_Parte: TWideStringField;
    QrFiConsBasNO_Obrigatorio: TWideStringField;
    QrGraGruNUsaCores: TSmallintField;
    QrFiConsOri: TMySQLQuery;
    QrFiConsOriGG1Dest: TIntegerField;
    QrFiConsOriControle: TIntegerField;
    QrFiConsOriGGXSorc: TIntegerField;
    QrFiConsOriGGXSubs: TIntegerField;
    QrFiConsOriParte: TIntegerField;
    QrFiConsOriObrigatorio: TSmallintField;
    QrFiConsOriGGXSorc_NO_PRD_TAM_COR: TWideStringField;
    QrFiConsOriSorc_SIGLAUNIDMED: TWideStringField;
    QrFiConsOriSorc_CODUSUUNIDMED: TIntegerField;
    QrFiConsOriSorc_NOMEUNIDMED: TWideStringField;
    QrFiConsOriGGXSubs_NO_PRD_TAM_COR: TWideStringField;
    QrFiConsOriSubs_SIGLAUNIDMED: TWideStringField;
    QrFiConsOriSubs_CODUSUUNIDMED: TIntegerField;
    QrFiConsOriSubs_NOMEUNIDMED: TWideStringField;
    QrFiConsOriNO_Parte: TWideStringField;
    QrFiConsOriNO_Obrigatorio: TWideStringField;
    DBGFiConsOri: TdmkDBGridZTO;
    PnEntidade: TPanel;
    dmkDBGrid1: TdmkDBGridZTO;
    Panel32: TPanel;
    Label53: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    RgTipoListaPreco: TRadioGroup;
    PnEntiMenu: TPanel;
    BtValores: TBitBtn;
    DsFiConsOri: TDataSource;
    PnDest: TPanel;
    PCGrades: TPageControl;
    TabSheet22: TTabSheet;
    GradeK7: TStringGrid;
    TabSheet23: TTabSheet;
    GradeA7: TStringGrid;
    TabSheet24: TTabSheet;
    GradeC7: TStringGrid;
    TabSheet25: TTabSheet;
    GradeX7: TStringGrid;
    QrFiConsOriSorc_GraCorCad: TIntegerField;
    QrFiConsOriSorc_GraTamI: TIntegerField;
    QrFiConsOriSorc_GraGru1: TIntegerField;
    TabSheet26: TTabSheet;
    TabSheet27: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Splitter8: TSplitter;
    GradeK9: TStringGrid;
    QrGraPckCad: TMySQLQuery;
    QrGraPckCadCodigo: TIntegerField;
    QrGraPckCadCodUsu: TIntegerField;
    QrGraPckCadNome: TWideStringField;
    QrGraPckCadGraGru1: TIntegerField;
    DsGraPckCad: TDataSource;
    BtComposicao: TBitBtn;
    BtPack: TBitBtn;
    QrGraCmpCad: TMySQLQuery;
    QrGraCmpCadCodigo: TIntegerField;
    QrGraCmpCadCodUsu: TIntegerField;
    QrGraCmpCadNome: TWideStringField;
    QrGraCmpCadLk: TIntegerField;
    QrGraCmpCadDataCad: TDateField;
    QrGraCmpCadDataAlt: TDateField;
    QrGraCmpCadUserCad: TIntegerField;
    QrGraCmpCadUserAlt: TIntegerField;
    QrGraCmpCadAlterWeb: TSmallintField;
    QrGraCmpCadAWServerID: TIntegerField;
    QrGraCmpCadAWStatSinc: TSmallintField;
    QrGraCmpCadAtivo: TSmallintField;
    DsGraCmpCad: TDataSource;
    QrGraCmpPar: TMySQLQuery;
    QrGraCmpParNO_Parte: TWideStringField;
    QrGraCmpParCodigo: TIntegerField;
    QrGraCmpParControle: TIntegerField;
    QrGraCmpParParte: TIntegerField;
    QrGraCmpParLk: TIntegerField;
    QrGraCmpParDataCad: TDateField;
    QrGraCmpParDataAlt: TDateField;
    QrGraCmpParUserCad: TIntegerField;
    QrGraCmpParUserAlt: TIntegerField;
    QrGraCmpParAlterWeb: TSmallintField;
    QrGraCmpParAWServerID: TIntegerField;
    QrGraCmpParAWStatSinc: TSmallintField;
    QrGraCmpParAtivo: TSmallintField;
    QrGraCmpParPercTot: TFloatField;
    QrGraCmpParPercImp: TFloatField;
    QrGraCmpParPercNoI: TFloatField;
    QrGraCmpParQtdeTot: TIntegerField;
    QrGraCmpParQtdeImp: TIntegerField;
    QrGraCmpParQtdeNoI: TIntegerField;
    DsGraCmpPar: TDataSource;
    QrGraCmpFib: TMySQLQuery;
    QrGraCmpFibNO_Fibra: TWideStringField;
    QrGraCmpFibCodigo: TIntegerField;
    QrGraCmpFibControle: TIntegerField;
    QrGraCmpFibConta: TIntegerField;
    QrGraCmpFibFibra: TIntegerField;
    QrGraCmpFibPercComp: TFloatField;
    QrGraCmpFibImprime: TSmallintField;
    QrGraCmpFibLk: TIntegerField;
    QrGraCmpFibDataCad: TDateField;
    QrGraCmpFibDataAlt: TDateField;
    QrGraCmpFibUserCad: TIntegerField;
    QrGraCmpFibUserAlt: TIntegerField;
    QrGraCmpFibAlterWeb: TSmallintField;
    QrGraCmpFibAWServerID: TIntegerField;
    QrGraCmpFibAWStatSinc: TSmallintField;
    QrGraCmpFibAtivo: TSmallintField;
    QrGraCmpFibNO_Imprime: TWideStringField;
    DsGraCmpFib: TDataSource;
    PnDados: TPanel;
    Splitter9: TSplitter;
    GBDados: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    PnPar: TPanel;
    Label71: TLabel;
    DBGPar: TDBGrid;
    PnFib: TPanel;
    Label72: TLabel;
    DBGFib: TDBGrid;
    PmComposicao: TPopupMenu;
    Janeladecriaoalterao1: TMenuItem;
    Atrelaexistentecorselecionada1: TMenuItem;
    QrGraGruCGraCmpCad: TIntegerField;
    QrGraGru1UsaSubsTrib: TSmallintField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    QrGraGru1CSOSN: TIntegerField;
    Label69: TLabel;
    DBEdCSOSN: TDBEdit;
    EdTextoC: TdmkEdit;
    QrGraGru1prod_cBarra: TWideStringField;
    Label70: TLabel;
    DBEdit57: TDBEdit;
    Label73: TLabel;
    DBEdit58: TDBEdit;
    QrGraGru1CtbCadGru: TIntegerField;
    QrGraGru1CtbPlaCta: TIntegerField;
    TabSheet28: TTabSheet;
    Panel7: TPanel;
    Label74: TLabel;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    Label75: TLabel;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    QrGraGru1NO_CtbPlaCta: TWideStringField;
    QrGraGru1NO_CtbCadGru: TWideStringField;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrGraGruNBeforeClose(DataSet: TDataSet);
    procedure QrGraGruNAfterScroll(DataSet: TDataSet);
    procedure QrGraGru3AfterScroll(DataSet: TDataSet);
    procedure QrGraGru2AfterScroll(DataSet: TDataSet);
    procedure QrGraGru3AfterClose(DataSet: TDataSet);
    procedure QrGraGru2AfterClose(DataSet: TDataSet);
    procedure QrGraGru1BeforeClose(DataSet: TDataSet);
    procedure QrGraGru1AfterScroll(DataSet: TDataSet);
    procedure TbGraTamNBeforePost(DataSet: TDataSet);
    procedure TbGraTamNPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure TbGraTamNAfterScroll(DataSet: TDataSet);
    procedure BtGradeClick(Sender: TObject);
    procedure PMCorPopup(Sender: TObject);
    procedure Incluinovacor1Click(Sender: TObject);
    procedure BtCorClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeAClick(Sender: TObject);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure EdFiltroNiv3Exit(Sender: TObject);
    procedure EdFiltroNiv2Exit(Sender: TObject);
    procedure EdFiltroNiv1Exit(Sender: TObject);
    procedure BtDadosClick(Sender: TObject);
    procedure EdCST_AChange(Sender: TObject);
    procedure EdCST_BChange(Sender: TObject);
    procedure BtAtributosClick(Sender: TObject);
    procedure Incluinovoitemdeatributo1Click(Sender: TObject);
    procedure Alteraitemdeatributoselecionado1Click(Sender: TObject);
    procedure Excluiitemdeatributoselecionado1Click(Sender: TObject);
    procedure Alteracoratual1Click(Sender: TObject);
    procedure Incluinovascoresvriasdeumavez1Click(Sender: TObject);
    procedure Incluidiversosnovositensdeatributo1Click(Sender: TObject);
    procedure GradePDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGraCusPrcUAfterScroll(DataSet: TDataSet);
    procedure GradePClick(Sender: TObject);
    procedure EdFiltroAtribCadExit(Sender: TObject);
    procedure GradeEDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure EdIPI_CSTChange(Sender: TObject);
    procedure Painel1Resize(Sender: TObject);
    procedure EdPIS_CSTChange(Sender: TObject);
    procedure EdCOFINS_CSTChange(Sender: TObject);
    procedure BtFiscalClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure Reduzido1Click(Sender: TObject);
    procedure GradePMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Atualizapreomdioeltimacompra1Click(Sender: TObject);
    procedure Incluinovoitem1Click(Sender: TObject);
    procedure BtEspecialClick(Sender: TObject);
    procedure IDNivel11Click(Sender: TObject);
    procedure CdigoNivel11Click(Sender: TObject);
    procedure GradeCDblClick(Sender: TObject);
    procedure PCGeralChanging(Sender: TObject; var AllowChange: Boolean);
    procedure GradeCSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure QrGraGru1AfterClose(DataSet: TDataSet);
    procedure QrGraGruNAfterClose(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure Nivel12Click(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure BtExclVincClick(Sender: TObject);
    procedure QrGraGruVincBeforeClose(DataSet: TDataSet);
    procedure QrGraGruVincAfterOpen(DataSet: TDataSet);
    procedure BtReduzidoLClick(Sender: TObject);
    procedure Gerenciamentodoreduzido1Click(Sender: TObject);
    procedure CadastrodoReduzido1Click(Sender: TObject);
    procedure QrGraGruXBeforeClose(DataSet: TDataSet);
    procedure QrGraGruXAfterOpen(DataSet: TDataSet);
    procedure Referencia1Click(Sender: TObject);
    procedure EdFiltroNiv5Exit(Sender: TObject);
    procedure EdFiltroNiv4Exit(Sender: TObject);
    procedure QrGraGru5AfterClose(DataSet: TDataSet);
    procedure QrGraGru5AfterScroll(DataSet: TDataSet);
    procedure QrGraGru4AfterClose(DataSet: TDataSet);
    procedure QrGraGru4AfterScroll(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOpcoesGradClick(Sender: TObject);
    procedure Incluinovolaudo1Click(Sender: TObject);
    procedure Excluilaudoatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure Excluiloteatual1Click(Sender: TObject);
    procedure QrGraGru5BeforeClose(DataSet: TDataSet);
    procedure QrGraGru2BeforeClose(DataSet: TDataSet);
    procedure QrGraGru3BeforeClose(DataSet: TDataSet);
    procedure QrGraGru4BeforeClose(DataSet: TDataSet);
    procedure AlteraGru5Click(Sender: TObject);
    procedure AlteraGru4Click(Sender: TObject);
    procedure AlteraGru3Click(Sender: TObject);
    procedure AlteraGru2Click(Sender: TObject);
    procedure AlteraGru1Click(Sender: TObject);
    procedure PMGraGru5Popup(Sender: TObject);
    procedure PMGraGru4Popup(Sender: TObject);
    procedure PMGraGru3Popup(Sender: TObject);
    procedure PMGraGru2Popup(Sender: TObject);
    procedure PMGraGru1Popup(Sender: TObject);
    procedure IncluiGru5Click(Sender: TObject);
    procedure IncluiGru4Click(Sender: TObject);
    procedure IncluiGru3Click(Sender: TObject);
    procedure IncluiGru2Click(Sender: TObject);
    procedure IncluiGru1Click(Sender: TObject);
    procedure ExcluiGru1Click(Sender: TObject);
    procedure ExcluiGru5Click(Sender: TObject);
    procedure ExcluiGru4Click(Sender: TObject);
    procedure ExcluiGru3Click(Sender: TObject);
    procedure ExcluiGru2Click(Sender: TObject);
    procedure PMCodificacaoPopup(Sender: TObject);
    procedure BtCodificacaoClick(Sender: TObject);
    procedure QrGraGruXAfterScroll(DataSet: TDataSet);
    procedure Gerenciarcodificaoprpria1Click(Sender: TObject);
    procedure BtValoresClick(Sender: TObject);
    procedure EdDBICMS_motDesICMSChange(Sender: TObject);
    procedure IncluiProduto1Click(Sender: TObject);
    procedure AlteraProduto1Click(Sender: TObject);
    procedure PMGrupoPopup(Sender: TObject);
    procedure PCGeralChange(Sender: TObject);
    procedure RgTipoListaPrecoClick(Sender: TObject);
    procedure ConfiguraPageIndex(Aba: Integer);
    procedure ReabreTabelasIndex(Aba: Integer);
    procedure IncluiConsumoEP1Click(Sender: TObject);
    procedure AlteraConsumoEP1Click(Sender: TObject);
    procedure ExcluiConsumoEP1Click(Sender: TObject);
    procedure BtGraGru1ConsClick(Sender: TObject);
    procedure PMGraGru1ConsPopup(Sender: TObject);
    procedure Alteradados1Click(Sender: TObject);
    procedure BtNiveis2Click(Sender: TObject);
    procedure AlteraGradedeTamanhos1Click(Sender: TObject);
    procedure GerenciarcodificaodeFornecedor1Click(Sender: TObject);
    procedure Excluicoratual1Click(Sender: TObject);
    procedure DBGNivel1DblClick(Sender: TObject);
    procedure BasicoInclui1Click(Sender: TObject);
    procedure BasicoAltera1Click(Sender: TObject);
    procedure BasicoExclui1Click(Sender: TObject);
    procedure PMFichaDeConsumoPopup(Sender: TObject);
    procedure BtFichaDeConsumoClick(Sender: TObject);
    procedure GradeK7DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrFiConsOriAfterScroll(DataSet: TDataSet);
    procedure GradeK9DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGraPckCadAfterScroll(DataSet: TDataSet);
    procedure QrGraPckCadBeforeClose(DataSet: TDataSet);
    procedure BtPackClick(Sender: TObject);
    procedure BtComposicaoClick(Sender: TObject);
    procedure QrGraCmpCadAfterScroll(DataSet: TDataSet);
    procedure QrGraCmpCadBeforeClose(DataSet: TDataSet);
    procedure QrGraCmpParAfterScroll(DataSet: TDataSet);
    procedure QrGraCmpParBeforeClose(DataSet: TDataSet);
    procedure ItemNobsico1Click(Sender: TObject);
    procedure Janeladecriaoalterao1Click(Sender: TObject);
    procedure Atrelaexistentecorselecionada1Click(Sender: TObject);
    procedure PmComposicaoPopup(Sender: TObject);
    procedure DBGParDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrGraGruCAfterScroll(DataSet: TDataSet);
    procedure QrGraGruCBeforeClose(DataSet: TDataSet);
    procedure Excluireduzido1Click(Sender: TObject);
    procedure DBEdCSOSNChange(Sender: TObject);
  private
    { Private declarations }
    FEditaGraGru1: Boolean;
    FGraGruXSel: Integer;
    function  NaoGradeado(var Msg: String): Boolean;
    function  NaoUsaCores(var Msg: String): Boolean;
    procedure InclusaoDeVariasCores();
    procedure InclusaoDeVariosAtributos();
    procedure IncluiNovaCor();
    procedure EditaDadosGerais();
    procedure EditaDadosFiscais();
    procedure IncluiNovoItemDeAtributo();
    procedure LocalizaItem(
              PrdGrupTip, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1: Integer);
    procedure AtualizaDescriNivel(PrdGrTp, Nivel: Integer; QueryNivel: TmySQLQuery);
    procedure MostraPrdGruNew(PrdGrTp, Nivel5, Nivel4, Nivel3, Nivel2: Integer;
              GraTabAppCod: Integer = 0; GraTabAppTxt: String = '';
              NewNome: String = '');
    procedure IncluiProduto();
    procedure AlteraProduto();
    procedure AtualizaGradesPrecos();
    procedure ReopenTabePrCCab(Codigo: Integer);
    procedure MostraSubForm();
    procedure ReorganizaPnNiveis();
  public
    { Public declarations }
    FEntiPrecos, FNewPrdGrupTip, FTabePr: Integer;
    FLaTipoGrupo: TSQLType;
    FEmSelecao: Boolean;
    FTipLista: TTipoListaPreco;
    FMostraSubForm: TSubForm;
    FNewNome: String;
    procedure ReopenNivelX(Qry: TmySQLQuery; Nivel, ID: Integer);
    procedure ReopenGraGru1(Nivel1: Integer; ApenasLocaliza: Boolean = False);
    procedure ReopenGraGru2(Nivel2: Integer; ApenasLocaliza: Boolean = False);
    procedure ReopenGraGru3(Nivel3: Integer; ApenasLocaliza: Boolean = False);
    procedure ReopenGraGru4(Nivel4: Integer; ApenasLocaliza: Boolean = False);
    procedure ReopenGraGru5(Nivel5: Integer; ApenasLocaliza: Boolean = False);
    procedure ReopenGraGruN(GraGruTip: Integer);
    procedure ReopenGraGruC(Controle: Integer);
    procedure ReopenGraGruX(Controle: Integer);
    procedure ReopenGraGruEX(GraGruX: Integer);
    procedure ReopenGraGruVinc();
    procedure ReopenGraAtrIts(Controle: Integer);
    procedure ReopenGraTamI();
    procedure ReopenGraGruCST(UF_Orig, UF_Dest: String);
    procedure ReopenGraGru1Lau(Controle: Integer);
    procedure ReopenGraGru1Lot(Controle: Integer);
    procedure ReopenGraGru1Cons(Niv1_COMP: Integer);
    procedure ReopenGraGruYPGT();

    procedure ReopenFiConsBas();
    procedure ReopenFiConsOri();
    procedure ReopenGraPckCad();
    procedure ReopenGraCmpCad();
    procedure ReopenGraCmpFib(Conta: Integer);
    procedure ReopenGraCmpPar(Controle: Integer);
    //procedure ReopenGraCusPrcU(Query: TmySQLQuery; Codigo: Integer);
    procedure IniciaSequenciaCadastro(PrdGrupTip: Integer);
    procedure ContinuaSequenciaCadastro();

    function  StatusItemCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
    function  StatusVariosItensCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
    //
    function  PrecoItemCorTam(GridP, GridC, GridX: TStringGrid;
              QrGraCusPrcU: TmySQLQuery): Boolean;
    function  PrecoVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid;
              QrGraCusPrcU: TmySQLQuery): Boolean;
    procedure DesSelecionaReduzido();
    procedure HabilitaBtInclui();
    procedure LocalizaIDNivel1(Codigo: Integer);
    procedure LocalizaReduzido(GraGruX: Integer);
    procedure MostraFormGraGru1Cons(SQLType: TSQLType);
  end;

  var
  FmGraGruN: TFmGraGruN;

implementation

uses
{$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
{$IfNDef SemNFe_0000} NFe_PF, {$EndIf}
{$IfDef usaGraTX} UnGraTX_Jan, {$EndIf}
  PrdGruNew, UmySQLModule, Module, GraGruT, GraGruC, (*MyVCLSkin,*)
  GraGruDados, GraGruAtr, CheckOnStringGrid, SelOnStringGrid, CheckCores,
  CheckAtributos, GraGruAti, UnInternalConsts, GraGruPrc, GraGruVal, GraAtrIts,
  UnMyObjects, GraGruFiscal, GetValor, ModuleGeral, GraImpLista,
  GraGruCST, GraGruX, GraGru1, GraGruReduzido, MeuDBUses, UnGOTOy, GraGruXEAN,
  MyListas, ModAppGraG1, GraTabAppSel, (*&&UnDmkABS_PF,*)
{$IfNDef NAO_GFAT}UnGFat_Jan, {$EndIf}
  GraGru1Cons,
  GerlShowGrid, UnGrl_Geral, MyGlyfs, GraGruY;

{$R *.DFM}

const
  CO_IDX_PCGeral_Ativos      = 0;
  CO_IDX_PCGeral_Precos      = 1;
  CO_IDX_PCGeral_Codigos     = 2;
  CO_IDX_PCGeral_DadosGrl    = 3;
  CO_IDX_PCGeral_Fiscal      = 4;
  CO_IDX_PCGeral_Atributos   = 5;
  CO_IDX_PCGeral_Estoque     = 6;
  CO_IDX_PCGeral_FchConsum   = 7;
  CO_IDX_PCGeral_Composicao  = 8;
  CO_IDX_PCGeral_Pack        = 9;
  CO_IDX_PCGeral_LotesLaudos = 10;
  CO_IDX_PCGeral_GradeX      = 11;
  //
  SplitWid                   = 5;
  HideWid                    = 0;
  //

procedure TFmGraGruN.BtDadosClick(Sender: TObject);
begin
  EditaDadosGerais();
end;

procedure TFmGraGruN.BtEspecialClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEspecial, BtEspecial);
end;

procedure TFmGraGruN.BtExclVincClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrGraGruVinc, GradeGraGruVinc,
  'gragruvinc', ['GraGruX', 'Nome'], ['GraGruX', 'Nome'], istPergunta, '');
end;

procedure TFmGraGruN.EditaDadosFiscais;
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Fiscal;
  PageControl3.ActivePageIndex := 0;
  //
  UMyMod.FormInsUpd_Show(TFmGraGruFiscal, FmGraGruFiscal, afmoNegarComAviso, QrGraGru1, stUpd);
end;

procedure TFmGraGruN.EditaDadosGerais();
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_DadosGrl;
  PageControl2.ActivePageIndex := 0;
  //
  UMyMod.FormInsUpd_Show(
    TFmGraGruDados, FmGraGruDados, afmoNegarComAviso, QrGraGru1, stUpd);
end;

procedure TFmGraGruN.AlteraGru5Click(Sender: TObject);
begin
  AtualizaDescriNivel(QrGraGruNCodigo.Value, 5, QrGraGru5);
end;

procedure TFmGraGruN.AlteraGru4Click(Sender: TObject);
begin
  AtualizaDescriNivel(QrGraGruNCodigo.Value, 4, QrGraGru4);
end;

procedure TFmGraGruN.AlteraGru3Click(Sender: TObject);
begin
  AtualizaDescriNivel(QrGraGruNCodigo.Value, 3, QrGraGru3);
end;

procedure TFmGraGruN.AlteraGru2Click(Sender: TObject);
begin
  AtualizaDescriNivel(QrGraGruNCodigo.Value, 2, QrGraGru2);
end;

procedure TFmGraGruN.AlteraGradedeTamanhos1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Msg: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(QrGraGru1Nivel1.Value),
    '']);
    if Qry.RecordCount = 0 then
    begin
      begin
        if NaoGradeado(Msg) then
        begin
          Geral.MB_Aviso(Msg);
          Exit;
        end;
        //
(*
        if (QrGraGruC.State <> dsInactive) and (QrGraGruC.RecordCount > 0) then
        begin
          Geral.MB_Aviso('Edi��o de grade abortada!' + sLineBreak +
            'Motivo: A grade atual possui cores cadastradas para ela!');
          Exit;
        end;
*)
        UMyMod.FormInsUpd_Show(TFmGraGruT, FmGraGruT, afmoNegarComAviso, QrGraGru1, stUpd);
      end;
    end else
      Geral.MB_Aviso(
      'Este produto n�o pode ter sua grade alterada pois j� possui reduzidos!');
  finally
    Qry.Free;
  end;
end;

procedure TFmGraGruN.AlteraGru1Click(Sender: TObject);
begin
  AlteraProduto();
end;

procedure TFmGraGruN.Alteracoratual1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmGraGruC, FmGraGruC, afmoNegarComAviso, QrGraGruC, stUpd);
  //
  DmProd.ConfigGrades1(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    QrGraCusPrcUCodigo.Value, FEntiPrecos, GradeA, GradeX, GradeC, GradeP, nil, GradeE);
end;

procedure TFmGraGruN.Alteradados1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormEditGraGru2(QrGraGru2Nivel2.Value, QrGraGru2Nome.Value,
  QrGraGru2Tipo_Item.Value);
  ReopenGraGru2(QrGraGru2Nivel2.Value);
end;

procedure TFmGraGruN.BasicoAltera1Click(Sender: TObject);
begin
{$IfDef usaGraTX}
  GraTX_Jan.MostraFormFiConsBas(QrFiConsBas, stUpd, QrGraGru1Nivel1.Value,
  QrFiConsBasControle.Value, QrGraGru1Nome.Value);
{$Else}
  Grl_Geral.InfoSemModulo(mdlappGraTX);
{$EndIf}
end;

procedure TFmGraGruN.AlteraConsumoEP1Click(Sender: TObject);
begin
  MostraFormGraGru1Cons(stUpd);
end;

procedure TFmGraGruN.Alteraitemdeatributoselecionado1Click(Sender: TObject);
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Atributos;
  UMyMod.FormInsUpd_Show(
    TFmGraGruAtr, FmGraGruAtr, afmoNegarComAviso, QrGraAtrI, stUpd);
end;

procedure TFmGraGruN.AlteraProduto;
begin
  if QrGraGru1GraTabApp.Value <> 0 then
    DfModAppGraG1.AlteraProdutoTabelaPersonalizada(QrGraGru1GraTabApp.Value,
      QrGraGru1Nivel1.Value, QrGraGru1Nome.Value, QrGraGru1)
  else
    AtualizaDescriNivel(QrGraGruNCodigo.Value, 1, QrGraGru1);
end;

procedure TFmGraGruN.AlteraProduto1Click(Sender: TObject);
begin
  AlteraProduto();
end;

procedure TFmGraGruN.Atualizapreomdioeltimacompra1Click(Sender: TObject);
var
  R, C: Integer;
begin
  if MyObjects.FIC(FEntiPrecos = 0, EdEntidade, 'Informe o cliente interno!') then
    Exit;
  //
  for R := 1 to GradeC.RowCount - 1 do
  begin
    for C := 1 to GradeC.ColCount - 1 do
      DmodG.AtualizaPrecosGraGruVal2(Geral.IMV(GradeC.Cells[C,R]), FEntiPrecos);
  end;
end;

procedure TFmGraGruN.BtFichaDeConsumoClick(Sender: TObject);
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_FchConsum;
  MyObjects.MostraPopUpDeBotao(PMFichaDeConsumo, BtFichaDeConsumo);
end;

procedure TFmGraGruN.BtFiscalClick(Sender: TObject);
begin
  EditaDadosFiscais();
end;

procedure TFmGraGruN.BitBtn1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0) then
    Codigo := QrGraGruNCodigo.Value
  else
    Codigo := 0;
  //
  Grade_Jan.MostraFormPrdGrupTip(Codigo);
  //
  ReopenGraGruN(0);
end;

procedure TFmGraGruN.BtAtributosClick(Sender: TObject);
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Atributos;
  //
  MyObjects.MostraPopUpDeBotao(PMAtributos, BtAtributos);
end;

procedure TFmGraGruN.BtCodificacaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCodificacao, BtCodificacao);
end;

procedure TFmGraGruN.BtComposicaoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMComposicao, BtComposicao);
end;

procedure TFmGraGruN.BtCorClick(Sender: TObject);
var
  Msg: String;
begin
  if NaoGradeado(Msg) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  MyObjects.MostraPopUpDeBotao(PMCor, BtCor);
end;

function TFmGraGruN.NaoGradeado(var Msg: String): Boolean;
var
  Res: Boolean;
begin
  if (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0) and
    (QrGraGruNGradeado.Value = 0) then
  begin
    Msg := 'Este tipo de produto n�o � gradeado!';
    Res := True;
  end else
  begin
    Msg := '';
    Res := False;
  end;
  Result := Res;
end;

function TFmGraGruN.NaoUsaCores(var Msg: String): Boolean;
var
  Res: Boolean;
begin
  if (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0) and
    (QrGraGruNUsaCores.Value = 0) then
  begin
    Msg := 'Este tipo de produto n�o usa cores!';
    Res := True;
  end else
  begin
    Msg := '';
    Res := False;
  end;
  Result := Res;
end;

procedure TFmGraGruN.BtGradeClick(Sender: TObject);
var
  Msg: String;
begin
  if NaoGradeado(Msg) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  //
  if (QrGraGruC.State <> dsInactive) and (QrGraGruC.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Edi��o de grade abortada!' + sLineBreak +
      'Motivo: A grade atual possui cores cadastradas para ela!');
    Exit;
  end;
  UMyMod.FormInsUpd_Show(TFmGraGruT, FmGraGruT, afmoNegarComAviso, QrGraGru1, stUpd);
end;

procedure TFmGraGruN.BtGraGru1ConsClick(Sender: TObject);
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Fiscal;
  PageControl3.ActivePageIndex := 5;
  //
  MyObjects.MostraPopUpDeBotao(PMGraGru1Cons, BtGraGru1Cons);
end;

procedure TFmGraGruN.Atrelaexistentecorselecionada1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Composi��o';
  Prompt = 'Informe a Composi��o';
  Campo  = 'Descricao';
var
  //Terceiro, Controle, ThisCtrl: Integer;
  Resp: Variant;
  GraCmpCad, Controle: Integer;
begin
  GraCmpCad := QrGraGruCGraCmpCad.Value;
  //
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo,
  GraCmpCad, [
    'SELECT Codigo Codigo, Nome Descricao ',
    'FROM gracmpcad ',
    'ORDER BY Nome ',
    ''], Dmod.MyDB, True);
  //
  if Resp <> Null then
  begin
    GraCmpCad := Resp;
    Controle  := QrGraGruCControle.Value;
    //
    if UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stUpd, 'gragruc', False, [
    'GraCmpCad'], [
    'Controle'], [
    GraCmpCad], [
    Controle], True) then
      ReopenGraGruC(Controle);
  end;
end;

procedure TFmGraGruN.AtualizaDescriNivel(PrdGrTp, Nivel: Integer; QueryNivel: TmySQLQuery);
var
  Controle: Integer;
  Nome, Tabela, ID: String;
begin
  case Nivel of
      1:
      begin
        ID     := 'Nivel1';
        Tabela := 'gragru1';
      end;
      2:
      begin
        ID     := 'Nivel2';
        Tabela := 'gragru2';
      end;
      3:
      begin
        ID     := 'Nivel3';
        Tabela := 'gragru3';
      end;
      4:
      begin
        ID     := 'Nivel4';
        Tabela := 'gragru4';
      end;
      5:
      begin
        ID     := 'Nivel5';
        Tabela := 'gragru5';
      end
    else
    begin
      ID     := '';
      Tabela := '';
    end;
  end;
  if Tabela = '' then
    Geral.MB_Aviso('Altera��o abortada! N�vel n�o implementado!');
  //
  if (QueryNivel.State <> dsInactive) and (QueryNivel.RecordCount > 0) and
    (Tabela <> '') then
  begin
    Controle := QueryNivel.FieldByName(ID).AsInteger;
    Nome     := QueryNivel.FieldByName('Nome').AsString;
    //
    if InputQuery('Descri��o', 'Descri��o:', Nome) then
    begin
      if Nome <> '' then
      begin
        if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + Tabela + ' SET Nome="' + Nome + '"',
          'WHERE ' + ID + '=' + Geral.FF0(Controle),
          ''])
        then
          if (PrdGrTp = -2) and (Nivel = 1) and (CO_DMKID_APP = 2) then //Bluederm PQ
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
              'UPDATE pq SET Nome="' + Nome + '"',
              'WHERE Codigo=' + Geral.FF0(Controle),
              '']);
          end;
          QueryNivel.Close;
          UnDmkDAC_PF.AbreQuery(QueryNivel, Dmod.MyDB);
          QueryNivel.Locate(ID, Controle, []);
      end;
    end;
  end;
end;

procedure TFmGraGruN.AtualizaGradesPrecos;
var
  Tipo: TTipoListaPreco;
begin
  FEntiPrecos := EdEntidade.ValueVariant;
  //
  if RgTipoListaPreco.ItemIndex = 0 then
  begin
    dmkDBGrid1.DataSource := DsGraCusPrcU;
    Tipo                  := tpGraCusPrc;
  end else
  begin
    dmkDBGrid1.DataSource := DsTabePrCCab;
    Tipo                  := tpTabePrc;
  end;
  //
  DmProd.AtualizaGradesPrecos1(QrGraGru1Nivel1.Value,
    QrGraCusPrcUCodigo.Value, FEntiPrecos, GradeP, Tipo);
end;

procedure TFmGraGruN.BtIncluiClick(Sender: TObject);
var
  PrdGrupTip: Integer;
  //
  function AtreladoGGY(): Boolean;
  begin
    Result := False;
    ReopenGraGruYPGT();
    //
    Result := QrGraGruYPGT.RecordCount > 0;
    if Result then
    begin
      Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
      FmGerlShowGrid.MeAvisos.Lines.Add('O Tipo de Grupo de Produtos ' +
        Geral.FF0(PrdGrupTip) + ' - "' + QrGraGruNNome.Value +
        '" est� atrelado aos Grupos de Estoque listados abaixo. ');
      FmGerlShowGrid.MeAvisos.Lines.Add(
        'Utilize a janela de "Grupos de Estoque" para realizar o cadastro!');
      FmGerlShowGrid.DBGSel.DataSource := DsGraGruYPGT;
      FmGerlShowGrid.ShowModal;
      FmGerlShowGrid.Destroy;
    end;
  end;
begin
  PrdGrupTip := QrGraGruNCodigo.Value;
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Ativos;
  //
  case TdmkAppID(CO_DMKID_APP) of
    dmkappB_U_G_S_T_R_O_L:
      MyObjects.MostraPopUpDeBotao(PMGrupo, BtInclui);
    else
    begin
      if AtreladoGGY() then
        Exit;
      MostraPrdGruNew(QrGraGruNCodigo.Value, 0, 0, 0, 0, 0, '', FNewNome);
    end;
  end;
end;

procedure TFmGraGruN.BtNiveis2Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruNiveis2();
end;

procedure TFmGraGruN.BtOpcoesGradClick(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
  HabilitaBtInclui();
end;

procedure TFmGraGruN.BtPackClick(Sender: TObject);
begin
{$IfDef usaGraTX}
  GraTX_Jan.MostraFormGraPckCad(0);
{$Else}
  Grl_Geral.InfoSemModulo(mdlappGraTX);
{$EndIf}
end;

procedure TFmGraGruN.BtReduzidoLClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReduzidoL, BtReduzidoL);
end;

procedure TFmGraGruN.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO  := QrGraGruXControle.Value;
  VAR_CADASTRO2 := QrGraGru1Nivel1.Value;
  Close;
end;

procedure TFmGraGruN.BtValoresClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if RgTipoListaPreco.ItemIndex = 0 then
  begin
    if (QrGraCusPrcU.State <> dsInactive) and (QrGraCusPrcU.RecordCount > 0) then
      Codigo := QrGraCusPrcUCodigo.Value
    else
      Codigo := 0;
    //
    VAR_CADASTRO := 0;
    Grade_Jan.MostraFormGraCusPrc(Codigo);
    //
    if VAR_CADASTRO <> 0 then
      DmProd.ReopenGraCusPrcU(QrGraCusPrcU, VAR_CADASTRO);
  end else
  begin
    if (QrTabePrCCab.State <> dsInactive) and (QrTabePrCCab.RecordCount > 0) then
      Codigo := QrTabePrCCabCodigo.Value
    else
      Codigo := 0;
    //
    VAR_CADASTRO := 0;
    {$IfNDef NAO_GFAT}
      GFat_Jan.MostraFormTabePrcCab(Codigo);
    {$Else}
      Geral.MB_Info('ERP sem GFAT');
    {$EndIf}
    //
    if VAR_CADASTRO <> 0 then
      ReopenTabePrCCab(VAR_CADASTRO);
  end;
end;

procedure TFmGraGruN.EdCST_AChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoA.Text := UFinanceiro.CST_A_Get(Geral.IMV(EdCST_A.Text));
{$EndIf}
end;

procedure TFmGraGruN.EdCST_BChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
{$EndIf}
end;

procedure TFmGraGruN.EdDBICMS_motDesICMSChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  EdDBICMS_motDesICMS_TXT.Text :=
    UnNFe_PF.TextoDeCodigoNFe(nfeCTmotDesICMS, Geral.IMV(EdDBICMS_motDesICMS.Text));
{$EndIf}
end;

procedure TFmGraGruN.EdEntidadeChange(Sender: TObject);
begin
  FEntiPrecos := EdEntidade.ValueVariant;

  if PCGeral.ActivePageIndex = CO_IDX_PCGeral_Estoque then //Estoque
    DmProd.AtualizaGradesEstoqueGeral(FEntiPrecos, QrGraGru1Nivel1.Value, GradeE)
  else
    AtualizaGradesPrecos;
end;

procedure TFmGraGruN.EdFiltroAtribCadExit(Sender: TObject);
begin
  ReopenGraAtrIts(QrGraAtrICTRL_ATR.Value);
end;

procedure TFmGraGruN.EdFiltroNiv1Exit(Sender: TObject);
begin
  ReopenGraGru1(QrGraGru1Nivel1.Value);
end;

procedure TFmGraGruN.EdFiltroNiv2Exit(Sender: TObject);
begin
  ReopenGraGru2(QrGraGru2Nivel2.Value);
end;

procedure TFmGraGruN.EdFiltroNiv3Exit(Sender: TObject);
begin
  ReopenGraGru3(QrGraGru3Nivel3.Value);
end;

procedure TFmGraGruN.EdFiltroNiv4Exit(Sender: TObject);
begin
  ReopenGraGru4(QrGraGru4Nivel4.Value);
end;

procedure TFmGraGruN.EdFiltroNiv5Exit(Sender: TObject);
begin
  ReopenGraGru5(QrGraGru5Nivel5.Value);
end;

procedure TFmGraGruN.ExcluiGru5Click(Sender: TObject);
var
  Nivel5: Integer;
  Nome: String;
begin
  if (QrGraGru5.State <> dsInactive) and (QrGraGru5.RecordCount > 0) then
  begin
    Nivel5 := QrGraGru5Nivel5.Value;
    Nome   := QrGraGru5Nome.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item ' + Nome + '?',
      'gragru5', 'Nivel5', Nivel5, Dmod.MyDB) <> 0 then
    begin
      QrGraGru5.Close;
      UnDmkDAC_PF.AbreQuery(QrGraGru5, Dmod.MyDB);
    end;
  end;
end;

procedure TFmGraGruN.ExcluiGru4Click(Sender: TObject);
var
  Nivel4: Integer;
  Nome: String;
begin
  if (QrGraGru4.State <> dsInactive) and (QrGraGru4.RecordCount > 0) then
  begin
    Nivel4 := QrGraGru4Nivel4.Value;
    Nome   := QrGraGru4Nome.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item ' + Nome + '?',
      'gragru4', 'Nivel4', Nivel4, Dmod.MyDB) <> 0 then
    begin
      QrGraGru4.Close;
      UnDmkDAC_PF.AbreQuery(QrGraGru4, Dmod.MyDB);
    end;
  end;
end;

procedure TFmGraGruN.ExcluiGru3Click(Sender: TObject);
var
  Nivel3: Integer;
  Nome: String;
begin
  if (QrGraGru3.State <> dsInactive) and (QrGraGru3.RecordCount > 0) then
  begin
    Nivel3 := QrGraGru3Nivel3.Value;
    Nome   := QrGraGru3Nome.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item ' + Nome + '?',
      'gragru3', 'Nivel3', Nivel3, Dmod.MyDB) <> 0 then
    begin
      QrGraGru3.Close;
      UnDmkDAC_PF.AbreQuery(QrGraGru3, Dmod.MyDB);
    end;
  end;
end;

procedure TFmGraGruN.ExcluiGru2Click(Sender: TObject);
var
  Nivel2: Integer;
  Nome: String;
begin
  if (QrGraGru2.State <> dsInactive) and (QrGraGru2.RecordCount > 0) then
  begin
    Nivel2 := QrGraGru2Nivel2.Value;
    Nome   := QrGraGru2Nome.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item ' + Nome + '?',
      'gragru2', 'Nivel2', Nivel2, Dmod.MyDB) <> 0 then
    begin
      QrGraGru2.Close;
      UnDmkDAC_PF.AbreQuery(QrGraGru2, Dmod.MyDB);
    end;
  end;
end;

procedure TFmGraGruN.ExcluiGru1Click(Sender: TObject);
var
  Nivel1, ItensA, ItensB: Integer;
begin
  Nivel1 := QrGraGru1Nivel1.Value;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT smia.*');
  Dmod.QrAux.SQL.Add('FROM stqmovitsa smia');
  Dmod.QrAux.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX');
  Dmod.QrAux.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
  Dmod.QrAux.SQL.Add('WHERE gg1.Nivel1=:P0');
  Dmod.QrAux.Params[0].AsInteger := Nivel1;
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  ItensA := Dmod.QrAux.RecordCount;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT smia.*');
  Dmod.QrAux.SQL.Add('FROM stqmovitsb smia');
  Dmod.QrAux.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX');
  Dmod.QrAux.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
  Dmod.QrAux.SQL.Add('WHERE gg1.Nivel1=:P0');
  Dmod.QrAux.Params[0].AsInteger := Nivel1;
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  ItensB := Dmod.QrAux.RecordCount;
  //
  if ItensA + ItensB > 0 then
  begin
    Geral.MB_Aviso('A grade de produtos "' + QrGraGru1Nome.Value +
    '"n�o pode ser exclu�da pois existem lan�amentos de estoque atrelados a ele!'
    + sLineBreak + 'Lan�amentos ativos: ' + IntToStr(ItensA) + sLineBreak +
    'Arquivo morto: ' + Geral.FF0(ItensB));
  end else begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT *');
    Dmod.QrAux.SQL.Add('FROM gragrux');
    Dmod.QrAux.SQL.Add('WHERE GraGru1=:P0');
    Dmod.QrAux.Params[0].AsInteger := Nivel1;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Aviso('O produto:' + slineBreak+
      QrGraGru1Nome.Value + slineBreak +
      'N�o pode ser exclu�do pois tem Reduzidos cadastrados!');
    end else
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o do produto:' + sLineBreak +
      QrGraGru1Nome.Value + '?') = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        try
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE ');
          Dmod.QrUpd.SQL.Add('FROM gragru1');
          Dmod.QrUpd.SQL.Add('WHERE Nivel1=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Nivel1;
          Dmod.QrUpd.ExecSQL;
          //
          Nivel1 := UMyMod.ProximoRegistro(QrGraGru1, 'Nivel1', Nivel1);
          ReopenGraGru1(Nivel1);
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end;
end;

procedure TFmGraGruN.BasicoExclui1Click(Sender: TObject);
begin
{$IfDef usaGraTX}
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFiConsBas, TDBGrid(DBGFiConsBas),
  'ficonsbas', ['Controle'], ['Controle'], istPergunta, '');
{$Else}
  Grl_Geral.InfoSemModulo(mdlappGraTX);
{$EndIf}
end;

procedure TFmGraGruN.ExcluiConsumoEP1Click(Sender: TObject);
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Fiscal;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrGraGru1Cons, TDBGrid(DBGGraGru1Cons),
  'gragru1cons', ['Controle'], ['Controle'], istPergunta, '');
(*
var
  Niv1_COMP: Integer;
begin
  if UMyMod.ExcluiRegistroInt2(
  'Confirma a exclus�o do item de consumo selecionado?',
  'gragru1cons', 'Nivel1', 'Niv1_COMP', QrGraGru1ConsNivel1.Value,
  QrGraGru1ConsNiv1_COMP.Value, Dmod.MyDB) = ID_YES then
  begin
    Niv1_COMP := GOTOy.LocalizaPriorNextIntQr(QrGraGru1Cons,
    QrGraGru1ConsNiv1_COMP, QrGraGru1ConsNiv1_COMP.Value);
    ReopenGraGru1Cons(Niv1_COMP);
  end;
*)
end;

procedure TFmGraGruN.Excluicoratual1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Nome: String;
  Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT gcc.CodUsu ',
      'FROM gragrux ggx ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'WHERE ggx.GraGru1=' + Geral.FF0(QrGraGruCNivel1.Value),
      'AND gcc.CodUsu=' + Geral.FF0(QrGraGruCCodUsuGTC.Value),
      '']);
    if Qry.RecordCount = 0 then
    begin
      Controle := QrGraGruCControle.Value;
      Nome     := QrGraGruCNomeGTC.Value;
      //
      if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da cor: ' + Nome + '?',
        'gragruc', 'Controle', Controle, Dmod.MyDB) = ID_YES then
      begin
        ReopenGraGru1(QrGraGruCNivel1.Value);
      end;
    end else
      Geral.MB_Aviso('Esta cor n�o pode ser exclu�da pois foi(r�o) gerado(s) reduzido(s) para ela!');
  finally
    Qry.Free;
  end;
end;

procedure TFmGraGruN.Excluiitemdeatributoselecionado1Click(Sender: TObject);
begin
  PCGeral.ActivePageIndex :=CO_IDX_PCGeral_Atributos;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrGraAtrI, TDBGrid(GradeAtrIts),
    'gragruatr', ['CTRL_ATR'], ['Controle'], istPergunta, '');
end;

procedure TFmGraGruN.Excluilaudoatual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do laudo selecionado?',
  'gragru1lau', 'Controle', QrGraGru1LauControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrGraGru1Lau, QrGraGru1LauControle,
    QrGraGru1LauControle.Value);
    ReopenGraGru1Lau(Controle);
  end;
end;

procedure TFmGraGruN.Excluiloteatual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do lote selecionado?',
  'gragru1lot', 'Controle', QrGraGru1LotControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrGraGru1Lot, QrGraGru1LotControle,
    QrGraGru1LotControle.Value);
    ReopenGraGru1Lot(Controle);
  end;
end;

procedure TFmGraGruN.Excluireduzido1Click(Sender: TObject);
var
  Controle: Integer;
  Nome: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if (QrGraGruX.State <> dsInactive) and (QrGraGruX.RecordCount > 0) then
  begin
    Controle := QrGraGruXControle.Value;
    Nome   := QrGraGru1Nome.Value + ' ' +
              QrGraGruXNO_TAM.Value + ' ' +
              QrGraGruXNO_COR.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item ' + Nome + '?',
      'GraGruX', 'Controle', Controle, Dmod.MyDB) <> 0 then
    begin
      QrGraGruX.Close;
      UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
    end;
  end;
end;

procedure TFmGraGruN.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FTabePr <> 0 then
  begin
    PCGeral.ActivePageIndex := CO_IDX_PCGeral_Precos;
    //
    case FTipLista of
      tpTabePrc:
        RgTipoListaPreco.ItemIndex := 1;
      tpGraCusPrc:
        RgTipoListaPreco.ItemIndex := 0;
    end;
    //
    ConfiguraPageIndex(CO_IDX_PCGeral_Precos);
    AtualizaGradesPrecos;
    //
    case FTipLista of
      tpTabePrc:
      begin
        if (QrTabePrCCab.State <> dsInactive) and (QrTabePrCCab.RecordCount > 0) then
          QrTabePrCCab.Locate('Codigo', FTabePr, []);
      end;
      tpGraCusPrc:
      begin
        if (QrGraCusPrcU.State <> dsInactive) and (QrGraCusPrcU.RecordCount > 0) then
          QrGraCusPrcU.Locate('Codigo', FTabePr, []);
      end;
    end;
  end;
  MostraSubForm();
end;

procedure TFmGraGruN.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FEditaGraGru1   := True;
  //
{
  // 2012-07-28 - Tirar quando atualizar em todos clientes!
  //
  Desabilitado em 2020-10-23
  //
  DmProd.ReopenOpcoesGrad();
  if DmProd.QrOpcoesGrad.FieldByName('AtuaProd').AsInteger = 0 then
  begin
    if not DmProd.AtualizaPrdGrupTip(ProgressBar1) then
      Geral.MB_Erro('Falha ao atualizar produtos!');
  end;
}
  DmProd.ReopenOpcoesGrad();
  // Fim tirar
  //
  if CO_DMKID_APP <> 2 then //Bluederm
  begin
    DmodG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    CBEntidade.ListField  := 'NOMEFILIAL';
    CBEntidade.KeyField   := 'Codigo';
    CBEntidade.ListSource := DModG.DsEmpresas;
    //
    EdEntidade.ValueVariant := DModG.QrFiliLogCodigo.Value;
    CBEntidade.KeyValue     := DModG.QrFiliLogCodigo.Value;
  end else
  begin
    UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
    //
    CBEntidade.ListField  := 'NO_ENTI';
    CBEntidade.KeyField   := 'Codigo';
    CBEntidade.ListSource := DsClientes;
  end;
  //
  FEntiPrecos                  := EdEntidade.ValueVariant;
  RgTipoListaPreco.ItemIndex   := 0; //Venda
  dmkDBGrid1.DataSource        := DsGraCusPrcU;
  PCGeral.ActivePageIndex      := CO_IDX_PCGeral_Ativos;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PnEntidade.Visible           := False;
  //
  DmProd.ReopenGraCusPrcU(QrGraCusPrcU, 0);
  ReopenTabePrCCab(0);
  ReopenGraGruN(0);
  //
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeE.ColWidths[0] := 128;
  GradeP.ColWidths[0] := 128;
  //
  GradeA7.ColWidths[0] := 128;
  GradeC7.ColWidths[0] := 128;
  GradeX7.ColWidths[0] := 128;
  GradeK7.ColWidths[0] := 128;
  //
  GradeK9.ColWidths[0] := 128;
  //
  DesSelecionaReduzido();
  //
  HabilitaBtInclui();
  //
  if TdmkAppID(CO_DMKID_APP) = dmkappB_U_G_S_T_R_O_L then
    TabSheet20.TabVisible := True
  else
    TabSheet20.TabVisible := False;
end;

procedure TFmGraGruN.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  ReorganizaPnNiveis();
end;

procedure TFmGraGruN.Gerenciamentodoreduzido1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruReduzido, FmGraGruReduzido, afmoSoBoss) then
  begin
    FmGraGruReduzido.FGraGruX := QrGraGruXControle.Value;
(*
    FmGraGruReduzido.QrGraGruX.Close;
    FmGraGruReduzido.QrGraGruX.Params[0].AsInteger := QrGraGruXControle.Value;
    UnDmkDAC_PF.AbreQuery(FmGraGruReduzido.QrGraGruX, Dmod.MyDB);
*)
    FmGraGruReduzido.ReopenGraGruX();
    FmGraGruReduzido.ShowModal;
    FmGraGruReduzido.Destroy;
  end;
end;

procedure TFmGraGruN.GerenciarcodificaodeFornecedor1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruEGerCab();
end;

procedure TFmGraGruN.Gerenciarcodificaoprpria1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruXEAN, FmGraGruXEAN, afmoNegarComAviso) then
  begin
    FmGraGruXEAN.FNivel1 := QrGraGru1Nivel1.Value;
    FmGraGruXEAN.FGrade  := QrGraGru1GraTamCad.Value;
    FmGraGruXEAN.ShowModal;
    FmGraGruXEAN.Destroy;
  end;
end;

procedure TFmGraGruN.GradeAClick(Sender: TObject);
begin
  if (GradeA.Col = 0) or (GradeA.Row = 0) then
    StatusVariosItensCorTam(GradeA, GradeC, GradeX)
  else
    StatusItemCorTam(GradeA, GradeC, GradeX);
end;

procedure TFmGraGruN.SbImprimeClick(Sender: TObject);
begin
{
  Painel1.Visible := True;
  DBGNivel1.Visible := True;
  Painel1.Align := alClient;
  DBGNivel1.Align := alClient;
  Geral.MB_Info(Geral.ATS([
  'Painel1.Width = ' + Geral.FF0(Painel1.Width),
  'Painel1.Left = ' + Geral.FF0(Painel1.Left),
  'Painel1.Top = ' + Geral.FF0(Painel1.Top),
  'DBGNivel1.Width = ' + Geral.FF0(DBGNivel1.Width),
  '']));
  ;
}
  Grade_Jan.MostraFormGraImpLista(0,0);
end;

procedure TFmGraGruN.SbNomeClick(Sender: TObject);
begin
//
end;

procedure TFmGraGruN.SbNovoClick(Sender: TObject);
begin
  //
end;

procedure TFmGraGruN.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNumero, SbNumero);
end;

procedure TFmGraGruN.SbQueryClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuery, SBQuery);
end;

function TFmGraGruN.StatusItemCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
  function ObtemGraGruY(): Integer;
  var
    Qry: TmySQLQuery;
    GraGruY, Controle: Integer;
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
(*
      'SELECT ggy.Codigo  ',
      'FROM gragru1 gg1 ',
      'LEFT JOIN gragruy ggy ON ggy.PrdGrupTip=gg1.PrdGrupTip ',
      'WHERE gg1.Nivel1=' + Geral.FF0(QrGraGru1Nivel1.Value),
*)
      'SELECT DISTINCT ggy.Codigo ',
      'FROM gragruypgt gyp ',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gyp.PrdGrupTip ',
      'LEFT JOIN gragru1 gg1 ON gg1.PrdGrupTip=pgt.Codigo ',
      'LEFT JOIN gragruy ggy ON ggy.Codigo=gyp.GraGruY ',
      'WHERE gg1.Nivel1=' + Geral.FF0(QrGraGru1Nivel1.Value),
      '']);
      Result := Qry.FieldByName('Codigo').AsInteger;
    finally
      Qry.Free;
    end;
  end;

var
  Ativo, c, l, Controle, Coluna, Linha, GraGruY: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  Ativo := Geral.IMV(GridA.Cells[Coluna, Linha]);
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  Controle := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) then Exit;
  if (Ativo = 0) and (Controle = 0) then
  begin
    Ativo := 1;
    GraGruY := ObtemGraGruY();
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'GraGruX', 'GraGruX', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO gragrux SET Ativo=:P0, GraGruC=:P1, ');
    Dmod.QrUpd.SQL.Add('GraGru1=:P2, GraTamI=:P3, GraGruY=:P4, Controle=:P5');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := l;
    Dmod.QrUpd.Params[02].AsInteger := QrGraGru1Nivel1.Value;
    Dmod.QrUpd.Params[03].AsInteger := C;
    Dmod.QrUpd.Params[04].AsInteger := GraGruY;
    Dmod.QrUpd.Params[05].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end else begin
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE gragrux SET Ativo=:P0 WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end;
  DmProd.AtualizaGradesAtivos1(QrGraGru1Nivel1.Value, GridA, GridC);
  Result := True;
  Screen.Cursor := crDefault;
end;

procedure TFmGraGruN.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel8.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel8.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    //MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
    FmMyGlyfs.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmGraGruN.GradeCDblClick(Sender: TObject);
begin
{
  if (GradeC.Col = 0) or (GradeC.Row = 0) then
    //StatusVariosItensCorTam(GradeA, GradeC, GradeX)
  else begin
    if DBCheck.CriaFm(TFmGraGruX, FmGraGruX, afmoSoMaster) then
    begin
      FmGraGruX.ShowModal;
      FmGraGruX.Destroy;
    end;
  end;
}
end;

procedure TFmGraGruN.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel8.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGruN.GradeCSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    DesSelecionaReduzido()
  else
  begin
    FGraGruXSel := Geral.IMV(GradeC.Cells[ACol, ARow]);
    if FGraGruXSel = 0 then
      DesSelecionaReduzido()
    else
    begin
      SBReduzido.Panels[1].Text := GradeC.Cells[ACol, ARow];
      SBReduzido.Panels[2].Text := QrGraGru1Nome.Value + ' ' +
                              GradeC.Cells[ACol, 0] + ' ' +
                              GradeC.Cells[0, ARow];
      ReopenGraGruVinc();
      //
      if (QrGraGruX.State <> dsInactive) and (QrGraGruX.RecordCount > 0) and
        (FGraGruXSel <> 0)
      then
        QrGraGruX.Locate('Controle', FGraGruXSel, []);
    end;
  end;  
end;

procedure TFmGraGruN.GradeEDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeE, GradeA, nil, ACol, ARow, Rect, State,
    dmkPF.FormataCasas(QrGraGruNFracio.Value), 0, 0, False);
end;

procedure TFmGraGruN.GradeK7DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Texto: String;
  TextWidth: Integer;
begin
  if (ACol <> 0) and (ARow <> 0) then
  begin
     if not (gdSelected in State) then // Duplica escrita
     begin
       Texto := GradeK7.Cells[ACol,ARow];
       TextWidth := GradeK7.Canvas.TextWidth(GradeK7.Cells[ACol,ARow]);
       //
       GradeK7.Canvas.TextRect(Rect, Rect.Right - 2 - TextWidth, Rect.Top + 2, Texto);
     end;
  end;
end;

procedure TFmGraGruN.GradeK9DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Texto: String;
  TextWidth: Integer;
begin
  if (ACol <> 0) and (ARow <> 0) then
  begin
     if not (gdSelected in State) then // Duplica escrita
     begin
       Texto := GradeK9.Cells[ACol,ARow];
       TextWidth := GradeK9.Canvas.TextWidth(GradeK9.Cells[ACol,ARow]);
       //
       GradeK9.Canvas.TextRect(Rect, Rect.Right - 2 - TextWidth, Rect.Top + 2, Texto);
     end;
  end;
end;

procedure TFmGraGruN.GradePClick(Sender: TObject);
begin
  FEntiPrecos := EdEntidade.ValueVariant;
  //
  if MyObjects.FIC(FEntiPrecos = 0, nil, 'Defina a entidade!') then Exit;
  //
  if RgTipoListaPreco.ItemIndex = 0 then //Custo
  begin
    if (GradeP.Col = 0) or (GradeP.Row = 0) then
      PrecoVariosItensCorTam(GradeP, GradeA, GradeC, GradeX, QrGraCusPrcU)
    else
      PrecoItemCorTam(GradeP, GradeC, GradeX, QrGraCusPrcU);
  end;
end;

procedure TFmGraGruN.GradePDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel8.Color
  else if GradeC.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taLeftJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taRightJustify,
      FormatFloat(Dmod.FStrFmtPrc, Geral.DMV(GradeP.Cells[Acol, ARow])),
      0, 0, False);
end;

procedure TFmGraGruN.GradePMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMListaPrecos, GradeP, X,Y);
end;

procedure TFmGraGruN.HabilitaBtInclui();
var
  Habilita, Habilita2: Boolean;
begin
  (*
  case TdmkAppID(CO_DMKID_APP) of
    dmkappB_U_G_S_T_R_O_L:
      Habilita := True;
    else
      Habilita := (QrGraGruNImpedeCad.Value = 0) and
        (DmProd.QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = 1);
  end;
  *)
  //Habilita := (QrGraGruNImpedeCad.Value = 0) and (DmProd.QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = 1);
  Habilita := (QrGraGruNImpedeCad.Value = 0);
  //
  BtInclui.Enabled := Habilita;
  FEditaGraGru1    := Habilita;
  //
  if not Habilita then
  begin
    Habilita2 := QrGraGruNGradeado.Value = 1;
    //
    BtGrade.Enabled := Habilita and Habilita2;
    BtCor.Enabled   := Habilita and Habilita2;

  end;
end;

procedure TFmGraGruN.IDNivel11Click(Sender: TObject);
var
  Cod: Variant;
begin
  Cod := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Cod, 0, 0,
  '', '', True, 'ID N�vel 1', 'Informe o "ID N�vel 1": ', 0, Cod)
  then
    LocalizaIDNivel1(Cod);
end;

procedure TFmGraGruN.IncluiGru5Click(Sender: TObject);
begin
  MostraPrdGruNew(QrGraGruNCodigo.Value, 0, 0, 0, 0);
end;

procedure TFmGraGruN.BasicoInclui1Click(Sender: TObject);
begin
{$IfDef usaGraTX}
  GraTX_Jan.MostraFormFiConsBas(QrFiConsBas, stIns, QrGraGru1Nivel1.Value,
  0(*QrFiConsBasControle.Value*), QrGraGru1Nome.Value);
{$Else}
  Grl_Geral.InfoSemModulo(mdlappGraTX);
{$EndIf}
end;

procedure TFmGraGruN.IncluiConsumoEP1Click(Sender: TObject);
begin
  MostraFormGraGru1Cons(stIns);
end;

procedure TFmGraGruN.IncluiGru4Click(Sender: TObject);
begin
  MostraPrdGruNew(QrGraGruNCodigo.Value, QrGraGru5Codusu.Value, 0, 0, 0);
end;

procedure TFmGraGruN.IncluiGru3Click(Sender: TObject);
begin
  MostraPrdGruNew(QrGraGruNCodigo.Value, QrGraGru5Codusu.Value, QrGraGru4Codusu.Value, 0, 0);
end;

procedure TFmGraGruN.IncluiGru2Click(Sender: TObject);
var
  Nivel2: Integer;
begin
  Nivel2 := 0;
  Grade_Jan.MostraFormInsGraGru2(QrGraGruNCodigo.Value, QrGraGru5Nivel5.Value,
  QrGraGru4Nivel4.Value, QrGraGru3Nivel3.Value, Nivel2, (*Nome*)'',
  (*Tipo_Item*)0);
  ReopenGraGru2(Nivel2);
end;

procedure TFmGraGruN.IncluiGru1Click(Sender: TObject);
begin
  case TdmkAppID(CO_DMKID_APP) of
    dmkappB_U_G_S_T_R_O_L:
      IncluiProduto();
    else
      MostraPrdGruNew(QrGraGruNCodigo.Value, QrGraGru5Codusu.Value,
        QrGraGru4Codusu.Value, QrGraGru3Codusu.Value, QrGraGru2Codusu.Value,
        0, FNewNome);
  end;
end;

procedure TFmGraGruN.Incluidiversosnovositensdeatributo1Click(Sender: TObject);
begin
  InclusaoDeVariosAtributos();
end;

procedure TFmGraGruN.InclusaoDeVariosAtributos();
var
  Controle: Integer;
begin
(*&&
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Atributos;
  if DBCheck.CriaFm(TFmCheckAtributos, FmCheckAtributos, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    try
      with FmCheckAtributos do
      begin
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('DROP TABLE atributos; ');
        Query.SQL.Add('CREATE TABLE atributos (');
        Query.SQL.Add('  Ativo smallint      ,');
        Query.SQL.Add('  CodUsu integer      ,');
        Query.SQL.Add('  NomeCad varchar(30) ,');
        Query.SQL.Add('  NomeIts varchar(30) ,');
        Query.SQL.Add('  Codigo integer      ,');
        Query.SQL.Add('  Controle integer     ');
        Query.SQL.Add(');');
        //
        DmProd.ReopenQrAtribIts((DmProd.QrPrdGrupJan.State = dsBrowse) and
          (DmProd.QrPrdGrupJanJanela.Value = 'PRD-ATRIB-003'),
          QrGraGruNTipPrd.Value);
        //DmProd.QrAtribIts.Close;
        //DmProd.QrAtribIts . O p e n ;
        while not DmProd.QrAtribIts.Eof do
        begin
          Query.SQL.Add('INSERT INTO atributos (' +
          'Codigo, Controle, CodUsu, NomeCad, NomeIts, Ativo) VALUES (' +
            FormatFloat('0', DmProd.QrAtribItsCodigo.Value) + ',' +
            FormatFloat('0', DmProd.QrAtribItsControle.Value) + ',' +
            FormatFloat('0', DmProd.QrAtribItsCodUsu.Value) + ',"' +
            DmProd.QrAtribItsNOMEATRIBCAD.Value + '", "' +
            DmProd.QrAtribItsNOMEATRIBITS.Value + '", 0);');
          DmProd.QrAtribIts.Next;
        end;
        //
        Query.SQL.Add('SELECT * FROM atributos;');
        DmkABS_PF.AbreQuery(Query, '');
        ShowModal;
        if FmCheckAtributos.FCadastrar then
        begin
          Query.First;
          //j := 0;
          Controle := 0;
          while not Query.Eof do
          begin
            DmProd.QrGraAtrIts.Close;
            DmProd.QrGraAtrIts.Params[00].AsInteger := QrGraGru1Nivel1.Value;
            DmProd.QrGraAtrIts.Params[01].AsInteger := QueryCodigo.Value;
            UnDmkDAC_PF.AbreQuery(DmProd.QrGraAtrIts, Dmod.MyDB);
            if DmProd.QrGraAtrIts.RecordCount > 0 then
            begin
              Dmod.QrUpd.SQL.Clear;
              Dmod.QrUpd.SQL.Add('DELETE FROM gragruatr ');
              Dmod.QrUpd.SQL.Add('WHERE Nivel1=:P0 AND GraAtrCad=:P1');
              Dmod.QrUpd.Params[00].AsInteger := QrGraGru1Nivel1.Value;
              Dmod.QrUpd.Params[01].AsInteger := QueryCodigo.Value;
              Dmod.QrUpd.ExecSQL;
              //
              UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'gragruatr',
                DmProd.QrGraAtrItsControle.Value);
            end;
            Controle := UMyMod.BuscaEmLivreY_Def('gragruatr', 'controle', stIns, 0);
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruatr', False, [
              'Nivel1', 'GraAtrCad', 'GraAtrIts'], ['Controle'], [
            QrGraGru1Nivel1.Value, QueryCodigo.Value, QueryControle.Value], [
            Controle], True);
            //
            Query.Next;
          end;
          if Controle > 0 then
            ReopenGraAtrIts(Controle);
        end;
        Destroy;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
*)
end;

procedure TFmGraGruN.Incluinovacor1Click(Sender: TObject);
begin
  IncluiNovaCor();
end;

procedure TFmGraGruN.IncluiNovaCor();
begin
  UMyMod.FormInsUpd_Show(TFmGraGruC, FmGraGruC, afmoNegarComAviso, QrGraGruC, stIns);
  //
  DmProd.ConfigGrades1(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
  QrGraCusPrcUCodigo.Value, FEntiPrecos, GradeA, GradeX, GradeC, GradeP, nil, GradeE);
end;

procedure TFmGraGruN.Incluinovascoresvriasdeumavez1Click(Sender: TObject);
begin
  InclusaoDeVariasCores();
end;

procedure TFmGraGruN.Incluinovoitem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruCST, FmGraGruCST, afmoNegarComAviso) then
  begin
    FmGraGruCST.ShowModal;
    FmGraGruCST.Destroy;
    //
    ReopenGraGruCST('', '');
  end;
end;

{
procedure TFmGraGruN.InclusaoDeVariasCores();
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmCheckCores, FmCheckCores, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    try
      with FmCheckCores do
      begin
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('DROP TABLE cores; ');
        Query.SQL.Add('CREATE TABLE cores (');
        Query.SQL.Add('  Ativo smallint    ,');
        Query.SQL.Add('  CodUsu integer    ,');
        Query.SQL.Add('  Nome varchar(30)  ,');
        Query.SQL.Add('  Codigo integer     ');
        Query.SQL.Add(');');
        //
        DmProd.QrGraCorCad.Close;
        UnDmkDAC_PF.AbreQuery(DmProd.QrGraCorCad, Dmod.MyDB);
        while not DmProd.QrGraCorCad.Eof do
        begin
          Query.SQL.Add('INSERT INTO cores (Codigo, CodUsu, Nome, Ativo) VALUES (' +
            FormatFloat('0', DmProd.QrGraCorCadCodigo.Value) + ',' +
            FormatFloat('0', DmProd.QrGraCorCadCodUsu.Value) + ',"' +
            DmProd.QrGraCorCadNome.Value + '", 0);');
          DmProd.QrGraCorCad.Next;
        end;
        //
        Query.SQL.Add('SELECT * FROM cores;');
        DmkABS_PF.AbreQuery(Query);
        ShowModal;
        if FmCheckCores.FCadastrar then
        begin
          Query.First;
          //j := 0;
          Controle := 0;
          while not Query.Eof do
          begin
            if not QrGraGruC.Locate('GraCorCad', QueryCodigo.Value, []) then
            begin
              Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
                'Nivel1', 'GraCorCad'], ['Controle'
              ], [QrGraGru1Nivel1.Value, QueryCodigo.Value], [Controle], True);
            end;
            //
            Query.Next;
          end;
          if Controle > 0 then
            ReopenGraGruC(Controle);
        end;
        Destroy;
        DmProd.ConfigGrades1(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
          QrGraCusPrcUCodigo.Value, FEntiPrecos, GradeA, GradeX, GradeC,
          GradeP, nil, GradeE);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
}

procedure TFmGraGruN.InclusaoDeVariasCores();
var
  Controle, I: Integer;
begin
  if DBCheck.CriaFm(TFmCheckCores, FmCheckCores, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    try
      with FmCheckCores do
      begin
(*
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('DROP TABLE cores; ');
        Query.SQL.Add('CREATE TABLE cores (');
        Query.SQL.Add('  Ativo smallint    ,');
        Query.SQL.Add('  CodUsu integer    ,');
        Query.SQL.Add('  Nome varchar(30)  ,');
        Query.SQL.Add('  Codigo integer     ');
        Query.SQL.Add(');');
        //
        DmProd.QrGraCorCad.Close;
        UnDmkDAC_PF.AbreQuery(DmProd.QrGraCorCad, Dmod.MyDB);
        while not DmProd.QrGraCorCad.Eof do
        begin
          Query.SQL.Add('INSERT INTO cores (Codigo, CodUsu, Nome, Ativo) VALUES (' +
            FormatFloat('0', DmProd.QrGraCorCadCodigo.Value) + ',' +
            FormatFloat('0', DmProd.QrGraCorCadCodUsu.Value) + ',"' +
            DmProd.QrGraCorCadNome.Value + '", 0);');
          DmProd.QrGraCorCad.Next;
        end;
        //
        Query.SQL.Add('SELECT * FROM cores;');
        DmkABS_PF.AbreQuery(Query);
*)
        QrGraCorCad.SQL.Text := DmProd.QrGraCorCad.SQL.Text;
        UnDmkDAC_PF.AbreQuery(QrGraCorCad, DMod.MyDB);
        ShowModal;
        if FmCheckCores.FCadastrar then
        begin
(*
          Query.First;
          //j := 0;
          Controle := 0;
          while not Query.Eof do
          begin
            if not QrGraGruC.Locate('GraCorCad', QueryCodigo.Value, []) then
            begin
              Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
                'Nivel1', 'GraCorCad'], ['Controle'
              ], [QrGraGru1Nivel1.Value, QueryCodigo.Value], [Controle], True);
            end;
            //
            Query.Next;
*)
          with DBGGCC.DataSource.DataSet do
          for I := 0 to DBGGCC.SelectedRows.Count-1 do
          begin
            GotoBookmark(DBGGCC.SelectedRows.Items[I]);
            //
            Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', stIns, 0);
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
              'Nivel1', 'GraCorCad'], ['Controle'
            ], [QrGraGru1Nivel1.Value, QrGraCorCadCodigo.Value], [Controle], True);
            //
          end;
          if Controle > 0 then
            ReopenGraGruC(Controle);
        end;
        Destroy;
        DmProd.ConfigGrades1(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
          QrGraCusPrcUCodigo.Value, FEntiPrecos, GradeA, GradeX, GradeC,
          GradeP, nil, GradeE);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmGraGruN.Incluinovoitemdeatributo1Click(Sender: TObject);
begin
  IncluiNovoItemDeAtributo();
end;

procedure TFmGraGruN.Incluinovolaudo1Click(Sender: TObject);
var
  Nome: String;
  Nivel1, Controle: Integer;
begin
  Nivel1         := QrGraGru1Nivel1.Value;
  Controle       := 0;
  Nome           := '';
  //
  if InputQuery('Laudo de Produto', 'Informe o laudo:', Nome) then
  begin
    Controle := UMyMod.BPGS1I32('gragru1lau', 'Controle', '', '', tsPos, stIns, Controle);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru1lau', False, [
    'Nivel1', 'Nome'], [
    'Controle'], [
    Nivel1, Nome], [
    Controle], True) then
      ReopenGraGru1Lau(Controle);
  end;
end;

procedure TFmGraGruN.Incluinovolote1Click(Sender: TObject);
var
  Nome: String;
  Nivel1, Controle: Integer;
begin
  Nivel1         := QrGraGru1Nivel1.Value;
  Controle       := 0;
  Nome           := '';
  //
  if InputQuery('Lote de Produto', 'Informe o lote:', Nome) then
  begin
    Controle := UMyMod.BPGS1I32('gragru1lot', 'Controle', '', '', tsPos, stIns, Controle);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru1lot', False, [
    'Nivel1', 'Nome'], [
    'Controle'], [
    Nivel1, Nome], [
    Controle], True) then
      ReopenGraGru1Lot(Controle);
  end;
end;

procedure TFmGraGruN.IncluiProduto;
var
  GraTabAppSel: Boolean;
  GraTabAppCod: Integer;
  GraTabAppTxt: String;
begin
  GraTabAppCod := QrGraGruNGraTabApp.Value;
  GraTabAppTxt := QrGraGruNGraTabApp_TXT.Value;

  if (GraTabAppCod = 0) and (DmProd.QrOpcoesGrad.FieldByName('EditGraTabApp').AsInteger = 1) then
  begin
    if DBCheck.CriaFm(TFmGraTabAppSel, FmGraTabAppSel, afmoNegarComAviso) then
    begin
      FmGraTabAppSel.ShowModal;
      //
      GraTabAppSel := FmGraTabAppSel.FGraTabAppSelSel;
      GraTabAppCod := FmGraTabAppSel.FGraTabAppSelCod;
      GraTabAppTxt := FmGraTabAppSel.FGraTabAppSelTxt;
      //
      FmGraTabAppSel.Destroy;
    end;
  end else
    GraTabAppSel := True;

  if GraTabAppSel then
  begin
    VAR_CADASTRO := 0;
    //
    MostraPrdGruNew(QrGraGruNCodigo.Value, QrGraGru5Codusu.Value,
      QrGraGru4Codusu.Value, QrGraGru3Codusu.Value, QrGraGru2Codusu.Value,
      GraTabAppCod, GraTabAppTxt);
    {
    (*
    if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
    begin
      FmGraGru1.ImgTipo.SQLType           := stIns;
      FmGraGru1.FQrGraGru1                := QrGraGru1;
      FmGraGru1.EdGraTabApp.ValueVariant  := GraTabAppCod;
      FmGraGru1.EDTXT_GraTabApp.Text      := GraTabAppTxt;
      FmGraGru1.EdCodUsu_New.ValueVariant := UMyMod.BPGS1I32('GraGru1', 'CodUsu', '', '', tsPos, stIns, 0);
      //
      FmGraGru1.ShowModal;
      FmGraGru1.Destroy;
    end;
    *)
    DmProd.FPrdGrupTip_Nome := '????';
    DmProd.FFmGraGruNew     := True;
    DmProd.FChamouGraGruNew := cggnFmGraGruN;
    DmProd.FNewGraGru5      := 0;
    DmProd.FNewGraGru4      := 0;
    DmProd.FNewGraGru3      := 0;
    DmProd.FNewGraGru2      := 0;
    DmProd.FNewGraGru1      := 0;
    DmProd.FGraTabAppMostra := True;
    DmProd.FGraTabApp       := GraTabAppCod;
    DmProd.FGraTabApp_Nome  := GraTabAppTxt;
    //
    UMyMod.FormInsUpd_Show(TFmPrdGruNew, FmPrdGruNew, afmoNegarComAviso,
      QrGraGru3, stIns);
    //FmPrdGruNew.FClicked := True;

    if VAR_CADASTRO <> 0 then
    begin
      LocalizaIDNivel1(VAR_CADASTRO);
      if QrGraGru1Nivel1.Value = VAR_CADASTRO then
        AlteraProduto1Click(Self)
      else
        Geral.MB_Erro('N�o foi poss�vel localizar o produto ' + Geral.FF0(
          VAR_CADASTRO) + ' para complementar os dados do seu cadastro!');
    end;
    }
  end;
end;

procedure TFmGraGruN.IncluiProduto1Click(Sender: TObject);
begin
  IncluiProduto();
end;

procedure TFmGraGruN.IncluiNovoItemDeAtributo();
begin
  PCGeral.ActivePageIndex := CO_IDX_PCGeral_Atributos;
  UMyMod.FormInsUpd_Show(
    TFmGraGruAtr, FmGraGruAtr, afmoNegarComAviso, nil, stIns);
end;

procedure TFmGraGruN.PCGeralChange(Sender: TObject);
begin
  ConfiguraPageIndex(PCGeral.ActivePageIndex);
  ReabreTabelasIndex(PCGeral.ActivePageIndex);
end;

procedure TFmGraGruN.PCGeralChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.Painel1Resize(Sender: TObject);
begin
  Painel1.Align := alClient;
  DBGNivel1.Align := alClient;
  MyObjects.AjustaTamanhoMaximoColunaDBGrid(TDBGrid(DBGNivel1), 'Nome', 193);
end;

procedure TFmGraGruN.PMCodificacaoPopup(Sender: TObject);
var
  Enab1: Boolean;
begin
  Enab1 := (QrGraGruX.State <> dsInactive) and (QrGraGruX.RecordCount > 0);
  //
  GerenciarcodificaodeFornecedor1.Enabled := Enab1;
  Gerenciarcodificaoprpria1.Enabled       := Enab1;
end;

procedure TFmGraGruN.PmComposicaoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(Janeladecriaoalterao1, QrGraGruC);
end;

procedure TFmGraGruN.PMCorPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrGraGruC.State <> dsInactive) and (QrGraGruC.RecordCount > 0);
  //
  AlteraCoratual1.Enabled := Habilita;
  ExcluiCoratual1.Enabled := Habilita;
  //
  Habilita := (QrGraGru1.State <> dsInactive) and (QrGraGru1.RecordCount > 0);
  IncluinovaCor1.Enabled  := Habilita;
end;

procedure TFmGraGruN.PMFichaDeConsumoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(BasicoInclui1, QrGraGru1);
  MyObjects.HabilitaMenuItemItsUpd(BasicoAltera1, QrFiConsBas);
  MyObjects.HabilitaMenuItemItsDel(BasicoExclui1, QrFiConsBas);
  //
  MyObjects.HabilitaMenuItemItsIns(ItemNobsico1, QrGraGru1);
  //
end;

procedure TFmGraGruN.PMGraGru1ConsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiConsumoEP1, QrGraGru1);
  MyObjects.HabilitaMenuItemItsUpd(AlteraConsumoEP1, QrGraGru1Cons);
  MyObjects.HabilitaMenuItemItsDel(ExcluiConsumoEP1, QrGraGru1Cons);
end;

procedure TFmGraGruN.PMGraGru1Popup(Sender: TObject);
var
  Enab1, Enab2, Enab3: Boolean;
begin
  Enab1 := (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0);
  Enab2 := (QrGraGru1.State <> dsInactive) and (QrGraGru1.RecordCount > 0);
  //
  //if Painel2.Visible then
  if Painel2.Width > HideWid then
    Enab3 := (QrGraGru2.State <> dsInactive) and (QrGraGru2.RecordCount > 0)
  else
    Enab3 := True;
  //
  IncluiGru1.Enabled := Enab3 and Enab1 and FEditaGraGru1;
  AlteraGru1.Enabled := Enab3 and Enab1 and Enab2; //Permite editar pois � somente a descri��o
  ExcluiGru1.Enabled := Enab3 and Enab1 and Enab2 and FEditaGraGru1;
end;

procedure TFmGraGruN.PMGraGru2Popup(Sender: TObject);
var
  Enab1, Enab2, Enab3, Enab4: Boolean;
begin
  Enab1 := (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0);
  Enab2 := (QrGraGru2.State <> dsInactive) and (QrGraGru2.RecordCount > 0);
  Enab3 := (QrGraGru1.State = dsInactive) or (QrGraGru1.RecordCount = 0);
  //
  //if Painel3.Visible then
  if Painel3.Width > HideWid then
    Enab4 := (QrGraGru3.State <> dsInactive) and (QrGraGru3.RecordCount > 0)
  else
    Enab4 := True;
  //
  IncluiGru2.Enabled := Enab4 and Enab1;
  AlteraGru2.Enabled := Enab4 and Enab1 and Enab2;
  ExcluiGru2.Enabled := Enab4 and Enab1 and Enab2 and Enab3;
end;

procedure TFmGraGruN.PMGraGru3Popup(Sender: TObject);
var
  Enab1, Enab2, Enab3, Enab4: Boolean;
begin
  Enab1 := (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0);
  Enab2 := (QrGraGru3.State <> dsInactive) and (QrGraGru3.RecordCount > 0);
  Enab3 := (QrGraGru2.State = dsInactive) or (QrGraGru2.RecordCount = 0);
  //
  //if Painel4.Visible then
  if Painel4.Width > HideWid then
    Enab4 := (QrGraGru4.State <> dsInactive) and (QrGraGru4.RecordCount > 0)
  else
    Enab4 := True;
  //
  IncluiGru3.Enabled := Enab4 and Enab1;
  AlteraGru3.Enabled := Enab4 and Enab1 and Enab2;
  ExcluiGru3.Enabled := Enab4 and Enab1 and Enab2 and Enab3;
end;

procedure TFmGraGruN.PMGraGru4Popup(Sender: TObject);
var
  Enab1, Enab2, Enab3, Enab4: Boolean;
begin
  Enab1 := (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0);
  Enab2 := (QrGraGru4.State <> dsInactive) and (QrGraGru4.RecordCount > 0);
  Enab3 := (QrGraGru3.State = dsInactive) or (QrGraGru3.RecordCount = 0);
  //
  //if Painel5.Visible then
  if Painel5.Width > HideWid then
    Enab4 := (QrGraGru5.State <> dsInactive) and (QrGraGru5.RecordCount > 0)
  else
    Enab4 := True;  
  //
  IncluiGru4.Enabled := Enab4 and Enab1;
  AlteraGru4.Enabled := Enab4 and Enab1 and Enab2;
  ExcluiGru4.Enabled := Enab4 and Enab1 and Enab2 and Enab3;
end;

procedure TFmGraGruN.PMGraGru5Popup(Sender: TObject);
var
  Enab1, Enab2, Enab3: Boolean;
begin
  Enab1 := (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0);
  Enab2 := (QrGraGru5.State <> dsInactive) and (QrGraGru5.RecordCount > 0);
  Enab3 := (QrGraGru4.State = dsInactive) or (QrGraGru4.RecordCount = 0);
  //
  IncluiGru5.Enabled := Enab1;
  AlteraGru5.Enabled := Enab1 and Enab2;
  ExcluiGru5.Enabled := Enab1 and Enab2 and Enab3;
end;

procedure TFmGraGruN.PMGrupoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrGraGruN.State <> dsInactive) and (QrGraGruN.RecordCount > 0);
  Enab2 := (QrGraGru1.State <> dsInactive) and (QrGraGru1.RecordCount > 0);

  IncluiProduto1.Enabled := Enab;
  AlteraProduto1.Enabled := Enab and Enab2;
end;

procedure TFmGraGruN.QrFiConsOriAfterScroll(DataSet: TDataSet);
begin
  DmProd.AtualizaGradesFiConsSpc(QrGraGru1Nivel1.Value,
  QrFiConsOriSorc_GraGru1.Value, QrFiConsOriSorc_GraCorCad.Value,
  QrFiConsOriSorc_GraTamI.Value, GradeK7);
end;

procedure TFmGraGruN.QrGraCmpCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraCmpPar(0);
end;

procedure TFmGraGruN.QrGraCmpCadBeforeClose(DataSet: TDataSet);
begin
  QrGraCmpPar.Close;
end;

procedure TFmGraGruN.QrGraCmpParAfterScroll(DataSet: TDataSet);
begin
  ReopenGraCmpFib(0);
end;

procedure TFmGraGruN.QrGraCmpParBeforeClose(DataSet: TDataSet);
begin
  QrGraCmpFib.Close;
end;

procedure TFmGraGruN.QrGraCusPrcUAfterScroll(DataSet: TDataSet);
begin
  DmProd.AtualizaGradesPrecos1(QrGraGru1Nivel1.Value,
    QrGraCusPrcUCodigo.Value, FEntiPrecos, GradeP, tpGraCusPrc);
end;

procedure TFmGraGruN.QrGraGru1AfterClose(DataSet: TDataSet);
begin
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru1AfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  ReopenGraTamI();
  ReopenGraGruC(0);
  ReopenGraAtrIts(0);
  ReopenGraGruX(0);
  DmProd.ConfigGrades1(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    QrGraCusPrcUCodigo.Value, FEntiPrecos, GradeA, GradeX, GradeC, GradeP, nil, GradeE);
  //
  Habilita := (QrGraGru1.State <> dsInactive) and (QrGraGru1.RecordCount > 0);
  //
  BtDados.Enabled       := Habilita;
  BtFiscal.Enabled      := Habilita;
  BtEspecial.Enabled    := Habilita;
  BtAtributos.Enabled   := Habilita;
  BtGraGru1Cons.Enabled := Habilita;
  BtCodificacao.Enabled := Habilita;
  BtGrade.Enabled       := Habilita and (QrGraGruNImpedeCad.Value = 0) and
                           (QrGraGruNCodigo.Value > 0) and
                           (DmProd.QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = 1);
  //Grades.Visible      := Habilita and (QrGraGru1GraTamCad.Value <> 0);
  Panel8.Visible        := Habilita and (QrGraGru1GraTamCad.Value <> 0);
  BtCor.Enabled         := BtGrade.Enabled;
  ReopenGraGruCST('','');
  DesSelecionaReduzido();
  ReopenGraGru1Lau(0);
  ReopenGraGru1Lot(0);
  ReopenGraGru1Cons(0);
  //
  DmProd.ConfigGradesFiConsSpc(QrGraGru1Nivel1.Value, QrGraGru1GraTamCad.Value,
  0, GradeA7, GradeC7, GradeX7, GradeK7);
(*
  DmProd.ConfigGradesPack1(QrGraGru1Nivel1.Value, QrGraGru1GraTamCad.Value,
  0, nil, nil, nil, GradeK9);
*)
  case PCGeral.ActivePageIndex of
    CO_IDX_PCGeral_FchConsum: (* 7? *)
    begin
      {$IfDef usaGraTX}
        ReopenFiConsBas();
        ReopenFiConsOri();
      {$Else}
        //Grl_Geral.InfoSemModulo(mdlappGraTX);
      {$EndIf}
    end;
    CO_IDX_PCGeral_Pack: (* 9? *)
    begin
      {$IfDef usaGraTX}
        ReopenGraPckCad();
      {$Else}
        //Grl_Geral.InfoSemModulo(mdlappGraTX);
      {$EndIf}
    end;
  end;
  Painel1.Visible := True;
  Painel1.Align := alClient;
  //if (QrGraGru1.State <> dsInactive) and (QrGraGru1.RecordCount > 0) then
  begin
    if (QrGraGruC.State <> dsInactive) and (QrGraGruC.RecordCount = 0) then
      BtCor.Enabled := True;
  end;
end;

procedure TFmGraGruN.QrGraGru1BeforeClose(DataSet: TDataSet);
begin
  BtGrade.Enabled     := False;
  BtCor.Enabled       := False;
  BtDados.Enabled     := False;
  BtFiscal.Enabled    := False;
  BtEspecial.Enabled  := False;
  BtAtributos.Enabled := False;
  BtGraGru1Cons.Enabled := False;
  QrGraTamI.Close;
  QrGraGruC.Close;
  QrGraAtrI.Close;
  QrGraGruCST.Close;
  QrGraGruX.Close;
  QrGraGru1Lau.Close;
  QrGraGru1Lot.Close;
  QrGraGru1Cons.Close;
  //
  MyObjects.LimpaGrade(GradeA, 0, 0, True);
  MyObjects.LimpaGrade(GradeC, 0, 0, True);
  MyObjects.LimpaGrade(GradeE, 0, 0, True);
  MyObjects.LimpaGrade(GradeP, 0, 0, True);
  MyObjects.LimpaGrade(GradeX, 0, 0, True);
  //
  MyObjects.LimpaGrade(GradeA7, 0, 0, True);
  MyObjects.LimpaGrade(GradeC7, 0, 0, True);
  MyObjects.LimpaGrade(GradeK7, 0, 0, True);
  MyObjects.LimpaGrade(GradeX7, 0, 0, True);
  //
  MyObjects.LimpaGrade(GradeK9, 0, 0, True);
  //
  //PnGrades.Visible := False;
  Panel8.Visible := False;
{$IfDef usaGraTX}
  QrFiConsBas.Close;
  QrFiConsOri.Close;
{$Else}
  //Grl_Geral.InfoSemModulo(mdlappGraTX);
{$EndIf}

end;

procedure TFmGraGruN.QrGraGru2AfterClose(DataSet: TDataSet);
begin
  //ReopenGraGru1(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru2AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru1(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru2BeforeClose(DataSet: TDataSet);
begin
  QrGraGru1.Close;
end;

procedure TFmGraGruN.QrGraGru3AfterClose(DataSet: TDataSet);
begin
  //ReopenGraGru2(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru3AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru2(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru3BeforeClose(DataSet: TDataSet);
begin
  QrGraGru2.Close;
end;

procedure TFmGraGruN.QrGraGru4AfterClose(DataSet: TDataSet);
begin
  //ReopenGraGru3(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru4AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru3(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru4BeforeClose(DataSet: TDataSet);
begin
  QrGraGru3.Close;
end;

procedure TFmGraGruN.QrGraGru5AfterClose(DataSet: TDataSet);
begin
  //ReopenGraGru4(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru5AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru4(0);
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGru5BeforeClose(DataSet: TDataSet);
begin
  QrGraGru4.Close;
end;

procedure TFmGraGruN.QrGraGruCAfterScroll(DataSet: TDataSet);
begin
  case PCGeral.ActivePageIndex of
    CO_IDX_PCGeral_Composicao: (* 8? *)
    begin
      {$IfDef usaGraTX}
        ReopenGraCmpCad();
      {$Else}
        //Grl_Geral.InfoSemModulo(mdlappGraTX);
      {$EndIf}
    end;
  end;
end;

procedure TFmGraGruN.QrGraGruCBeforeClose(DataSet: TDataSet);
begin
  QrGraCmpCad.Close;
end;

procedure TFmGraGruN.QrGraGruNAfterClose(DataSet: TDataSet);
begin
  DesSelecionaReduzido();
end;

procedure TFmGraGruN.QrGraGruNAfterScroll(DataSet: TDataSet);
begin
  LaNivel1.Caption := QrGraGruNTitNiv1.Value;
  LaNivel2.Caption := QrGraGruNTitNiv2.Value;
  LaNivel3.Caption := QrGraGruNTitNiv3.Value;
  LaNivel4.Caption := QrGraGruNTitNiv4.Value;
  LaNivel5.Caption := QrGraGruNTitNiv5.Value;
  //
  ReorganizaPnNiveis();
  //
  QrGraGru5.Close;
  QrGraGru4.Close;
  QrGraGru3.Close;
  QrGraGru2.Close;
  QrGraGru1.Close;
  //
  case QrGraGruNNivCad.Value of
    5: ReopenGraGru5(0);
    4: ReopenGraGru4(0);
    3: ReopenGraGru3(0);
    2: ReopenGraGru2(0);
    1: ReopenGraGru1(0);
  end;
  //
  HabilitaBtInclui(); //BtInclui.Enabled := QrGraGruNImpedeCad.Value = 0;
  DesSelecionaReduzido();
  //
end;

procedure TFmGraGruN.QrGraGruNBeforeClose(DataSet: TDataSet);
begin
  QrGraGru5.Close;
end;

procedure TFmGraGruN.QrGraGruVincAfterOpen(DataSet: TDataSet);
begin
  BtExclVinc.Enabled := QrGraGruVinc.RecordCount > 0;
end;

procedure TFmGraGruN.QrGraGruVincBeforeClose(DataSet: TDataSet);
begin
  BtExclVinc.Enabled := False;
end;

procedure TFmGraGruN.QrGraGruXAfterOpen(DataSet: TDataSet);
begin
  BtReduzidoL.Enabled := QrGraGruX.RecordCount > 0;
end;

procedure TFmGraGruN.QrGraGruXAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGruEx(QrGraGruXControle.Value);
end;

procedure TFmGraGruN.QrGraGruXBeforeClose(DataSet: TDataSet);
begin
  QrGraGruEX.Close;
  BtReduzidoL.Enabled := False;
end;

procedure TFmGraGruN.QrGraPckCadAfterScroll(DataSet: TDataSet);
begin
  DmProd.ConfigGradesPack1(QrGraPckCadGraGru1.Value, QrGraGru1GraTamCad.Value,
  QrGraPckCadCodigo.Value, nil, nil, nil, GradeK9);
end;

procedure TFmGraGruN.QrGraPckCadBeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrade(GradeK9, 0, 0, True);
end;

procedure TFmGraGruN.ReopenGraGruN(GraGruTip: Integer);
begin
  QrGraGruN.Close;
  UnDmkDAC_PF.AbreQuery(QrGraGruN, Dmod.MyDB);
  //
  if GraGruTip <> 0 then
    QrGraGruN.Locate('Codigo', GraGruTip, []);
end;

procedure TFmGraGruN.ReopenGraGruVinc();
begin
  QrGraGruVinc.Close;
  QrGraGruVinc.Params[0].AsInteger := FGraGruXSel;
  UnDmkDAC_PF.AbreQuery(QrGraGruVinc, Dmod.MyDB);
end;

procedure TFmGraGruN.ReopenGraGruX(Controle: Integer);
begin
  QrGraGruX.Close;
  QrGraGruX.Params[0].AsInteger := QrGraGru1Nivel1.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  QrGraGruX.Locate('Controle', Controle, []);
end;

procedure TFmGraGruN.ReopenGraGruYPGT();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruYPGT, Dmod.MyDB, [
  'SELECT ypg.GraGruY, ggy.Nome ',
  'FROM gragruypgt ypg ',
  'LEFT JOIN gragruy ggy ON ggy.Codigo=ypg.GraGruY',
  'WHERE ypg.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
  '']);
end;

procedure TFmGraGruN.ReopenGraPckCad();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraPckCad, Dmod.MyDB, [
  'SELECT gpk.* ',
  'FROM grapckcad gpk ',
  'WHERE gpk.GraGru1=' + Geral.FF0(QrGraGru1Nivel1.Value),
  EmptyStr]);
end;

procedure TFmGraGruN.ReopenGraGruC(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruC, Dmod.MyDB, [
  'SELECT ggc.Nivel1, ggc.Controle, ggc.GraCorCad, ',
  'gtc.CodUsu CodUsuGTC, gtc.Nome NomeGTC,  ',
  'ggc.GraCmpCad ',
  'FROM gragruc ggc ',
  'LEFT JOIN gracorcad gtc ON gtc.Codigo=ggc.GraCorCad ',
  'WHERE ggc.Nivel1=' + Geral.FF0(QrGraGru1Nivel1.Value),
  EmptyStr]);
  //
  if Controle <> 0 then
    QrGraGruC.Locate('Controle', Controle, []);
end;

procedure TFmGraGruN.ReopenGraGruCST(UF_Orig, UF_Dest: String);
begin
  QrGraGruCST.Close;
  QrGraGruCST.Params[0].AsInteger := QrGraGru1Nivel1.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGruCST, Dmod.MyDB);
  QrGraGruCST.Locate('UF_Orig;UF_Dest', VarArrayOf([UF_Orig,UF_Dest]), []);
end;

procedure TFmGraGruN.ReopenGraGruEX(GraGruX: Integer);
begin
  QrGraGruEX.Close;
  QrGraGruEX.Params[0].AsInteger := GraGruX;
  UnDmkDAC_PF.AbreQuery(QrGraGruEX, Dmod.MyDB);
  //
  if GraGruX <> 0 then
    QrGraGruEX.Locate('GraGruX', GraGruX, []);
end;

procedure TFmGraGruN.ReabreTabelasIndex(Aba: Integer);
begin
  case Aba of
    CO_IDX_PCGeral_FchConsum: (* 7? *) // Ficha de consumo
    begin
      {$IfDef usaGraTX}
        ReopenFiConsBas();
        ReopenFiConsOri();
      {$Else}
        //Grl_Geral.InfoSemModulo(mdlappGraTX);
      {$EndIf}
    end;
    CO_IDX_PCGeral_Composicao: (* 8? *) // Composicao
    begin
      {$IfDef usaGraTX}
        ReopenGraCmpCad();
      {$Else}
        //Grl_Geral.InfoSemModulo(mdlappGraTX);
      {$EndIf}
    end;
    CO_IDX_PCGeral_Pack: (* 9? *) // Ficha de consumo
    begin
      {$IfDef usaGraTX}
        ReopenGraPckCad();
      {$Else}
        //Grl_Geral.InfoSemModulo(mdlappGraTX);
      {$EndIf}
    end;
  end;
end;

procedure TFmGraGruN.Reduzido1Click(Sender: TObject);
var
  GraGruX: Variant;
  //Cod: Integer;
begin
  GraGruX := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX, 0, 0,
  '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, GraGruX) then
    LocalizaReduzido(GraGruX);
end;

procedure TFmGraGruN.Referencia1Click(Sender: TObject);
var
  Referencia: String;
begin
  Referencia := '';
  if InputQuery('Pesquisa por Refer�ncia', 'Informe a refer�ncia', Referencia) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocod, Dmod.MyDB, [
    'SELECT PrdGrupTip, Nivel1, Nivel2, Nivel3, Nivel4, Nivel5 ',
    'FROM gragru1',
    'WHERE Referencia="' + Referencia + '"',
    '']);
    //
    if QrLocod.RecordCount > 0 then
      LocalizaItem(QrLocodPrdGrupTip.Value, QrLocodNivel4.Value,
      QrLocodNivel4.Value, QrLocodNivel3.Value,
      QrLocodNivel2.Value, QrLocodNivel1.Value);
  end;
end;

procedure TFmGraGruN.ReopenFiConsBas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFiConsBas, Dmod.MyDB, [
  'SELECT fcb.*, ',
  ' ',
  'CONCAT(gg1a.Nome,  ',
  'IF(gtia.PrintTam=0 OR gtia.Codigo IS NULL, "", CONCAT(" ", gtia.Nome)),  ',
  'IF(gcca.PrintCor=0 OR gcca.Codigo IS NULL, "", CONCAT(" ", gcca.Nome)))  ',
  'GGXSorc_NO_PRD_TAM_COR, ',
  ' ',
  'CONCAT(gg1b.Nome,  ',
  'IF(gtib.PrintTam=0 OR gtib.Codigo IS NULL, "", CONCAT(" ", gtib.Nome)),  ',
  'IF(gccb.PrintCor=0 OR gccb.Codigo IS NULL, "", CONCAT(" ", gccb.Nome)))  ',
  'GGXSubs_NO_PRD_TAM_COR, ',
  ' ',
  'gpc.Nome NO_Parte, IF(fcb.Obrigatorio=0, "N�o", "Sim") NO_Obrigatorio ',
  ' ',
  'FROM ficonsbas fcb ',
  ' ',
  'LEFT JOIN gragrux    ggxa ON ggxa.Controle=fcb.GGXSorc ',
  'LEFT JOIN gragruc    ggca ON ggca.Controle=ggxa.GraGruC  ',
  'LEFT JOIN gracorcad  gcca ON gcca.Codigo=ggca.GraCorCad  ',
  'LEFT JOIN gratamits  gtia ON gtia.Controle=ggxa.GraTamI  ',
  'LEFT JOIN gragru1    gg1a ON gg1a.Nivel1=ggxa.GraGru1 ',
  ' ',
  'LEFT JOIN gragrux    ggxb ON ggxb.Controle=fcb.GGXSubs ',
  'LEFT JOIN gragruc    ggcb ON ggcb.Controle=ggxb.GraGruC  ',
  'LEFT JOIN gracorcad  gccb ON gccb.Codigo=ggcb.GraCorCad  ',
  'LEFT JOIN gratamits  gtib ON gtib.Controle=ggxb.GraTamI  ',
  'LEFT JOIN gragru1    gg1b ON gg1b.Nivel1=ggxb.GraGru1 ',
  ' ',
  'LEFT JOIN graparcad  gpc  ON gpc.Codigo=fcb.Parte  ',
  'WHERE fcb.GG1Dest=' + Geral.FF0(QrGraGru1Nivel1.Value),
  '']);
end;

procedure TFmGraGruN.ReopenFiConsOri();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFiConsOri, Dmod.MyDB, [
  'SELECT fco.*, ',
  ' ',
  'CONCAT(gg1a.Nome,  ',
  'IF(gtia.PrintTam=0 OR gtia.Codigo IS NULL, "", CONCAT(" ", gtia.Nome)),  ',
  'IF(gcca.PrintCor=0 OR gcca.Codigo IS NULL, "", CONCAT(" ", gcca.Nome)))  ',
  'GGXSorc_NO_PRD_TAM_COR, unma.Sigla Sorc_SIGLAUNIDMED, ',
  'unma.CodUsu Sorc_CODUSUUNIDMED, unma.Nome Sorc_NOMEUNIDMED,',
  'ggca.GraCorCad Sorc_GraCorCad, ggxa.GraTamI Sorc_GraTamI,',
  'ggxa.GraGru1 Sorc_GraGru1,',
  ' ',
  'CONCAT(gg1b.Nome,  ',
  'IF(gtib.PrintTam=0 OR gtib.Codigo IS NULL, "", CONCAT(" ", gtib.Nome)),  ',
  'IF(gccb.PrintCor=0 OR gccb.Codigo IS NULL, "", CONCAT(" ", gccb.Nome)))  ',
  'GGXSubs_NO_PRD_TAM_COR, unmb.Sigla Subs_SIGLAUNIDMED, ',
  'unmb.CodUsu Subs_CODUSUUNIDMED, unmb.Nome Subs_NOMEUNIDMED,',
  ' ',
  'gpc.Nome NO_Parte, IF(fco.Obrigatorio=0, "N�o", "Sim") NO_Obrigatorio ',
  ' ',
  'FROM ficonsori fco ',
  ' ',
  'LEFT JOIN gragrux    ggxa ON ggxa.Controle=fco.GGXSorc ',
  'LEFT JOIN gragruc    ggca ON ggca.Controle=ggxa.GraGruC  ',
  'LEFT JOIN gracorcad  gcca ON gcca.Codigo=ggca.GraCorCad  ',
  'LEFT JOIN gratamits  gtia ON gtia.Controle=ggxa.GraTamI  ',
  'LEFT JOIN gragru1    gg1a ON gg1a.Nivel1=ggxa.GraGru1 ',
  'LEFT JOIN unidmed    unma ON unma.Codigo=gg1a.UnidMed ',
  '',
  'LEFT JOIN gragrux    ggxb ON ggxb.Controle=fco.GGXSubs ',
  'LEFT JOIN gragruc    ggcb ON ggcb.Controle=ggxb.GraGruC  ',
  'LEFT JOIN gracorcad  gccb ON gccb.Codigo=ggcb.GraCorCad  ',
  'LEFT JOIN gratamits  gtib ON gtib.Controle=ggxb.GraTamI  ',
  'LEFT JOIN gragru1    gg1b ON gg1b.Nivel1=ggxb.GraGru1 ',
  'LEFT JOIN unidmed    unmb ON unmb.Codigo=gg1b.UnidMed ',
  ' ',
  'LEFT JOIN graparcad  gpc  ON gpc.Codigo=fco.Parte  ',
  'WHERE fco.GG1Dest=' + Geral.FF0(QrGraGru1Nivel1.Value),
  '']);
  //Geral.MB_SQL(self, QrFiConsSpc);
end;

procedure TFmGraGruN.ReopenGraAtrIts(Controle: Integer);
begin
  QrGraAtrI.Close;
  QrGraAtrI.Params[00].AsInteger := QrGraGru1Nivel1.Value;
  QrGraAtrI.Params[01].AsString  := '%' + EdFiltroAtribCad.Text + '%';
  QrGraAtrI.Params[02].AsString  := '%' + EdFiltroAtribIts.Text + '%';;
  UnDmkDAC_PF.AbreQuery(QrGraAtrI, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrGraAtrI.Locate('CTRL_ATR', Controle, []);
end;

procedure TFmGraGruN.ReopenGraCmpCad();
begin
  if QrGraGruCGraCmpCad.Value <> 0 then
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraCmpCad, Dmod.MyDB, [
    'SELECT gpk.* ',
    'FROM gracmpcad gpk ',
    'WHERE gpk.Codigo=' + Geral.FF0(QrGraGruCGraCmpCad.Value),
    EmptyStr])
  else
    QrGraCmpCad.Close;
end;

procedure TFmGraGruN.ReopenGraCmpFib(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraCmpFib, Dmod.MyDB, [
  'SELECT if(gcf.Imprime=1, "Sim", "N�o") NO_Imprime, gfc.Nome NO_Fibra, gcf.* ',
  'FROM gracmpfib gcf ',
  'LEFT JOIN grafibcad gfc ON gfc.Codigo=gcf.Fibra ',
  'WHERE gcf.Controle=' + Geral.FF0(QrGraCmpParControle.Value),
  'ORDER BY gcf.Controle ',
  '']);
  //
  QrGraCmpFib.Locate('Conta', Conta, []);
end;

procedure TFmGraGruN.ReopenGraCmpPar(Controle: Integer);
begin
   UnDmkDAC_PF.AbreMySQLQuery0(QrGraCmpPar, Dmod.MyDB, [
  'SELECT gpc.Nome NO_Parte, gcp.* ',
  'FROM gracmppar gcp ',
  'LEFT JOIN GraParCad gpc ON gpc.Codigo=gcp.Parte ',
  'WHERE gcp.Codigo=' + Geral.FF0(QrGraCmpCadCodigo.Value),
  'ORDER BY gcp.Controle ',
  '']);
  //
  QrGraCmpPar.Locate('Controle', Controle, []);
end;

{
procedure TFmGraGruN.ReopenGraGruT(Controle: Integer);
begin
  QrGraGruT.Close;
  QrGraGruT.Params[0].AsInteger := QrGraGru1Nivel1.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGruT, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrGraGruT.Locate('Controle', Controle, []);
end;
}

procedure TFmGraGruN.TbGraTamNAfterScroll(DataSet: TDataSet);
begin
  {
  DBCtrlGrid1.AllowInsert := TbGraTamN.RecordCount > 0;
  TbGraTamI.Filter := 'Codigo=' + FormatFloat('0', TbGraTamNCodigo.Value);
  if (TbGraTamN.RecordCount > 0) and (TbGraTamI.State = dsInactive) then
    UnDmkDAC_PF.AbreQuery(TbGraTamI, Dmod.MyDB);
  }
end;

procedure TFmGraGruN.TbGraTamNBeforePost(DataSet: TDataSet);
begin
  {
  if QrGraGru1Nivel1.Value = 0 then TbGraTamN.Cancel
  else begin
    if TbGraTamN.State = dsInsert then
    begin
      TbGraTamNNivel1.Value := QrGraGru1Nivel1.Value;
    end;
  end;
  }
end;

procedure TFmGraGruN.TbGraTamNPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if E.Message = 'Key violation.' then
  begin
    Geral.MB_Aviso('Item j� incluido');
    Action := daAbort;
  end;
end;

procedure TFmGraGruN.ReopenGraGru5(Nivel5: Integer; ApenasLocaliza: Boolean = False);
begin
  if QrGraGruNNivCad.Value >= 5 then
  begin
    if (QrGraGru5.State = dsInactive) or (QrGraGru5.RecordCount = 0) or
      (ApenasLocaliza = False) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru5, Dmod.MyDB, [
        'SELECT DISTINCT gg5.PrdGrupTip, gg5.Codusu, gg5.Nivel5, gg5.Nome ',
        'FROM gragru5 gg5 ',
        'WHERE gg5.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
        'AND gg5.Nome LIKE "%' + EdFiltroNiv5.Text + '%"',
        '']);
    end;
    if Nivel5 <> 0 then
      QrGraGru5.Locate('Nivel5', Nivel5, []);
  end else
    QrGraGru5.Close;
end;

procedure TFmGraGruN.ReopenGraGru4(Nivel4: Integer; ApenasLocaliza: Boolean = False);
var
  Nivel5: Integer;
(*
  Filtro: String;
*)
begin
(* Modificado em 2013-08-21
  ReopenNivelX(QrGraGru4, 4, Nivel4);
*)
  (*
  Filtro := EdFiltroNiv4.Text;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru4, Dmod.MyDB, [
  'SELECT DISTINCT gg4.Codusu, gg4.Nivel5, gg4.Nivel4,  ',
  'gg4.Nome  ',
  'FROM gragru4 gg4  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel4=gg4.Nivel4  ',
  'WHERE gg1.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
  'AND gg4.Nivel4 IN ',
  '( ',
  '     SELECT DISTINCT Nivel4 ',
  '     FROM gragru3 ',
  '      ',
  '     UNION  ',
  '      ',
  '     SELECT DISTINCT Nivel4 ',
  '     FROM gragru2 ',
  '      ',
  '     UNION ',
  '      ',
  '     SELECT DISTINCT Nivel4 ',
  '     FROM gragru1 ',
  ') ',
  Geral.ATS_If(Trim(Filtro) <> '', ['AND gg4.Nome LIKE "%' + Filtro + '%"']),
  '']);
  if Nivel4 <> 0 then
    QrGraGru3.Locate('Nivel4', Nivel4, []);
  *)
  if QrGraGruNNivCad.Value >= 4 then
  begin
    if (QrGraGru4.State = dsInactive) or (QrGraGru4.RecordCount = 0) or
      (ApenasLocaliza = False) then
    begin
      if (QrGraGru5.State <> dsInactive) and (QrGraGru5.RecordCount > 0) then
        Nivel5 := QrGraGru5Nivel5.Value
      else
        Nivel5 := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru4, Dmod.MyDB, [
        'SELECT DISTINCT gg4.PrdGrupTip, gg4.Codusu, ',
        'gg4.Nivel4, gg4.Nivel5, gg4.Nome ',
        'FROM gragru4 gg4 ',
        'WHERE gg4.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
        'AND gg4.Nivel5=' + Geral.FF0(Nivel5),
        'AND gg4.Nome LIKE "%' + EdFiltroNiv4.Text + '%"',
        '']);
    end;
    if Nivel4 <> 0 then
      QrGraGru4.Locate('Nivel4', Nivel4, []);
  end else
    QrGraGru4.Close;
end;

procedure TFmGraGruN.ReopenGraGru3(Nivel3: Integer; ApenasLocaliza: Boolean = False);
var
  Nivel4: Integer;
(*
  Filtro: String;
*)
begin
(* Modificado em 2013-08-21
  ReopenNivelX(QrGraGru3, 3, Nivel3);
*)
  (*
  Filtro := EdFiltroNiv3.Text;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru3, Dmod.MyDB, [
  'SELECT DISTINCT gg3.Codusu, gg3.Nivel4, gg3.Nivel3, ',
  'gg3.Nome ',
  'FROM gragru3 gg3 ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel3=gg3.Nivel3 ',
  'WHERE gg1.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
  'AND gg3.Nivel3 IN ',
  '( ',
  '     SELECT DISTINCT Nivel3 ',
  '     FROM gragru2 ',
  '      ',
  '     UNION ',
  '      ',
  '     SELECT DISTINCT Nivel3',
  '     FROM gragru1 ',
  ') ',
  Geral.ATS_If(Trim(Filtro) <> '', ['AND gg3.Nome LIKE "%' + Filtro + '%"']),
  '']);
  if Nivel3 <> 0 then
    QrGraGru3.Locate('Nivel3', Nivel3, []);
  *)
  if QrGraGruNNivCad.Value >= 3 then
  begin
    if (QrGraGru3.State = dsInactive) or (QrGraGru3.RecordCount = 0) or
      (ApenasLocaliza = False) then
    begin
      if (QrGraGru4.State <> dsInactive) and (QrGraGru4.RecordCount > 0) then
        Nivel4 := QrGraGru4Nivel4.Value
      else
        Nivel4 := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru3, Dmod.MyDB, [
        'SELECT DISTINCT gg3.PrdGrupTip, gg3.Codusu, ',
        'gg3.Nivel3, gg3.Nivel4, gg3.Nome ',
        'FROM gragru3 gg3 ',
        'WHERE gg3.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
        'AND gg3.Nivel4=' + Geral.FF0(Nivel4),
        'AND gg3.Nome LIKE "%' + EdFiltroNiv3.Text + '%"',
        '']);
    end;
    if Nivel3 <> 0 then
      QrGraGru3.Locate('Nivel3', Nivel3, []);
  end else
    QrGraGru3.Close;
end;

procedure TFmGraGruN.ReopenGraGru2(Nivel2: Integer; ApenasLocaliza: Boolean = False);
var
  Nivel3: Integer;
(*
  Filtro: String;
*)
begin
(* Modificado em 2013-08-21
  ReopenNivelX(QrGraGru2, 2, Nivel2);
*)
  (*
  Filtro := EdFiltroNiv3.Text;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru2, Dmod.MyDB, [
  'SELECT DISTINCT gg2.Codusu, gg2.Nivel3, gg2.Nivel2, ',
  'gg2.Nome ',
  'FROM gragru2 gg2 ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel2=gg2.Nivel2 ',
  'WHERE gg1.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
  'AND gg2.Nivel2 IN ',
  '( ',
  '     SELECT DISTINCT Nivel2',
  '     FROM gragru1 ',
  ') ',
  Geral.ATS_If(Trim(Filtro) <> '', ['AND gg2.Nome LIKE "%' + Filtro + '%"']),
  '']);
  if Nivel2 <> 0 then
    QrGraGru2.Locate('Nivel2', Nivel2, []);
  *)
  if QrGraGruNNivCad.Value >= 2 then
  begin
    if (QrGraGru2.State = dsInactive) or (QrGraGru2.RecordCount = 0) or
      (ApenasLocaliza = False) then
    begin
      if (QrGraGru3.State <> dsInactive) and (QrGraGru3.RecordCount > 0) then
        Nivel3 := QrGraGru3Nivel3.Value
      else
        Nivel3 := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru2, Dmod.MyDB, [
        'SELECT DISTINCT gg2.PrdGrupTip, gg2.Codusu, ',
        'gg2.Nivel2, gg2.Nivel3, gg2.Nome, gg2.Tipo_Item ',
        'FROM gragru2 gg2 ',
        'WHERE gg2.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
        'AND gg2.Nivel3=' + Geral.FF0(Nivel3),
        'AND gg2.Nome LIKE "%' + EdFiltroNiv2.Text + '%"',
        '']);
    end;
    if Nivel2 <> 0 then
      QrGraGru2.Locate('Nivel2', Nivel2, []);
  end else
    QrGraGru2.Close;
end;

procedure TFmGraGruN.ReopenGraGru1(Nivel1: Integer; ApenasLocaliza: Boolean = False);
var
  Nivel2: Integer;
begin
  if QrGraGruNNivCad.Value >= 1 then
  begin
    if (QrGraGru1.State = dsInactive) or (QrGraGru1.RecordCount = 0) or
      (ApenasLocaliza = False) then
    begin
      QrGraGru1.Close;
      if QrGraGru2.State = dsInactive then
        Nivel2 := 0
      else
        Nivel2 := QrGraGru2Nivel2.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT gg1.Codusu, gg1.Nivel5, gg1.Nivel4,  ',
      'gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, ',
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, ',
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD, ',
      'gg1.CST_A,gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso, ',
      'gg1.IPI_Alq, gg1.IPI_CST, gg1.IPI_cEnq, gg1.TipDimens, ',
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, ',
      'unm.Nome NOMEUNIDMED, gg1.PartePrinc, ',
      'gg1.PerCuztMin, gg1.PerCuztMax, gg1.MedOrdem, gg1.FatorClas, ',
      'mor.Nome NO_MedOrdem, mor.Medida1, mor.Medida2, ',
      'mor.Medida3, mor.Medida4, mpc.Nome NO_PartePrinc, ',
      'InfAdProd, SiglaCustm, HowBxaEstq, GerBxaEstq, ',
      'PIS_CST, PIS_AlqP, PIS_AlqV, PISST_AlqP, PISST_AlqV, ',
      'COFINS_CST, COFINS_AlqP, COFINS_AlqV,COFINSST_AlqP, ',
      'COFINSST_AlqV, ICMS_modBC, ICMS_modBCST, ICMS_pRedBC, ',
      'ICMS_pRedBCST, ICMS_pMVAST, ICMS_pICMSST, ',
      'IPI_pIPI, IPI_TpTrib, IPI_vUnid, EX_TIPI, ',
      'ICMS_Pauta, ICMS_MaxTab, cGTIN_EAN, ',
      'PIS_pRedBC, PISST_pRedBCST, ',
      'COFINS_pRedBC, COFINSST_pRedBCST, ',
      ' ',
      'gg1.ICMS_pDif, gg1.ICMS_pICMSDeson, gg1.ICMS_motDesICMS, ',
      ' ',
      'ICMSRec_pRedBC, IPIRec_pRedBC, ',
      'PISRec_pRedBC, COFINSRec_pRedBC, ',
      'ICMSRec_pAliq, IPIRec_pAliq, ',
      'PISRec_pAliq, COFINSRec_pAliq, ',
      'ICMSRec_tCalc, IPIRec_tCalc, ',
      'PISRec_tCalc, COFINSRec_tCalc, ',
      'ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA, ',
      'gg1.PerComissF, gg1.PerComissR, gg1.PerComissZ, ',
      'gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS, ',
      'gg1.Referencia, gg1.DadosFisc, gg1.GraTabApp,',
      'gg1.prod_indEscala, gg1.UsaSubsTrib, ',
(*
      //'gg1.EFD0210Qtd_Comp, gg1.EFD0210Perda, ',
object Label67: TLabel
  Left = 8
  Top = 20
  Width = 492
  Height = 13
  Caption =
    '03. Quantidade do item componente/insumo para se produzir uma un' +
    'idade do item composto/resultante:'
end
object DBEdit58: TDBEdit
  Left = 8
  Top = 36
  Width = 134
  Height = 21
  DataField = 'EFD0210Perda'
  DataSource = DsGraGru1
  TabOrder = 1
end
object Label68: TLabel
  Left = 8
  Top = 60
  Width = 568
  Height = 13
  Caption =
    '04. Perda/quebra normal percentual do insumo/componente para se ' +
    'produzir uma unidade do item composto/resultante:'
end
object DBEdit57: TDBEdit
  Left = 8
  Top = 76
  Width = 134
  Height = 21
  DataField = 'EFD0210Qtd_Comp'
  DataSource = DsGraGru1
  TabOrder = 0
end

//

object QrGraGru1EFD0210Qtd_Comp: TFloatField
  FieldName = 'EFD0210Qtd_Comp'
  DisplayFormat = '#,###,###,##0.000000'
end
object QrGraGru1EFD0210Perda: TFloatField
  FieldName = 'EFD0210Perda'
  DisplayFormat = '#,###,###,##0.0000'
end

*)      '',
      'gta.Nome NO_GraTabApp, gg1.prod_CEST, gg1.CSOSN, gg1.prod_cBarra,',
      'gg1.CtbCadGru, gg1.CtbPlaCta, ',
      'cta.Nome NO_CtbPlaCta, ccg.Nome NO_CtbCadGru ',
      ' ',
      'FROM gragru1 gg1 ',
      'LEFT JOIN gragru2   gg2 ON gg1.Nivel2=gg2.Nivel2 ',
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad ',
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
      'LEFT JOIN medordem mor ON mor.Codigo= gg1.MedOrdem ',
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=gg1.PartePrinc ',
      'LEFT JOIN gratabapp gta ON gta.Codigo=gg1.GraTabApp',
      'LEFT JOIN ctbcadgru ccg ON ccg.Codigo = gg1.CtbCadGru',
      //'LEFT JOIN contas    cta ON cta.Codigo = gg1.CtbPlaCta',
      {$IfDef UsaFinanceiro3}
      'LEFT JOIN plaallcad cta ON cta.Codigo = gg1.CtbPlaCta',
      {$Else}
      'LEFT JOIN contas    cta ON cta.Codigo = gg1.CtbPlaCta',
      {$EndIf}

      'WHERE gg1.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
      'AND gg1.Nivel2=' + Geral.FF0(Nivel2),
      'AND gg1.Nome LIKE "%' + EdFiltroNiv1.Text + '%" ',
      '']);
(*
      QrGraGru1.Params[0].AsInteger := QrGraGruNCodigo.Value;
      QrGraGru1.Params[1].AsInteger := Nivel2;
      QrGraGru1.Params[2].AsString  := '%' + EdFiltroNiv1.Text + '%';
      QrGraGru1. O p e n ;
*)
    end;
    if Nivel1 <> 0 then
      QrGraGru1.Locate('Nivel1', Nivel1, []);
  end else
    QrGraGru1.Close;
end;

procedure TFmGraGruN.ReopenGraGru1Cons(Niv1_COMP: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1Cons, Dmod.MyDB, [
  'SELECT gg1.Nome NO_Niv1_COMP, g1c.* ',
  'FROM gragru1cons g1c',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=g1c.Niv1_COMP',
  'WHERE g1c.Nivel1=' + Geral.FF0(QrGraGru1Nivel1.Value),
  'ORDER BY NO_Niv1_COMP',
  '']);
  //
  QrGraGru1Cons.Locate('Niv1_COMP', Niv1_COMP, []) ;
end;

procedure TFmGraGruN.ReopenGraGru1Lau(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1Lau, Dmod.MyDB, [
  'SELECT * ',
  'FROM gragru1lau ',
  'WHERE Nivel1=' + Geral.FF0(QrGraGru1Nivel1.Value),
  'ORDER BY Controle ',
  '']);
  //
  QrGraGru1Lau.Locate('Controle', Controle, []) ;
end;

procedure TFmGraGruN.ReopenGraGru1Lot(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1Lot, Dmod.MyDB, [
  'SELECT * ',
  'FROM gragru1lot ',
  'WHERE Nivel1=' + Geral.FF0(QrGraGru1Nivel1.Value),
  'ORDER BY Controle ',
  '']);
  //
  QrGraGru1Lot.Locate('Controle', Controle, []) ;
end;

procedure TFmGraGruN.ReopenGraTamI();
begin
  QrGraTamI.Close;
  QrGraTamI.Params[0].AsInteger := QrGraGru1GraTamCad.Value;
  UnDmkDAC_PF.AbreQuery(QrGraTamI, Dmod.MyDB);
  //
  {
  if Controle <> 0 then
    QrGraTamI.Locate('Controle', Controle, []);
  }
end;

procedure TFmGraGruN.ReopenNivelX(Qry: TmySQLQuery; Nivel, ID: Integer);
var
  LeftJ, NivUp, Filtro, A, B: String;
  QrUp: TmySQLQuery;
begin
  Qry.Close;
  if QrGraGruNNivCad.Value >= Nivel then
  begin
    case Nivel of
      4: QrUp := QrGraGru5;
      3: QrUp := QrGraGru4;
      2: QrUp := QrGraGru3;
      else raise EAbort.Create('Nivel n�o implementado: ' + Geral.FF0(Nivel));
    end;
    case Nivel of
      4: Filtro := EdFiltroNiv4.Text;
      3: Filtro := EdFiltroNiv3.Text;
      2: Filtro := EdFiltroNiv2.Text;
      else raise EAbort.Create('Nivel n�o implementado: ' + Geral.FF0(Nivel));
    end;
    A := Geral.FF0(Nivel + 1);
    B := Geral.FF0(Nivel);
    if QrUp.State = dsInactive then
    begin
      LeftJ := '';
      NivUp := '';
    end else
    begin
      LeftJ := 'LEFT JOIN gragru' + A + ' gg' + A + ' ON gg' + A + '.Nivel' + A + '=gg' + B + '.Nivel' + A + ' ';
      NivUp := 'AND gg' + B + '.Nivel' + A + '=' + Geral.FF0(QrUp.FieldByName('Nivel' + A).AsInteger);
    end;
    if Filtro <> '' then
      Filtro :=  'AND gg' + B + '.Nome LIKE "%' + Filtro + '%"';
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT gg' + B + '.Codusu, gg' + B + '.Nivel' + A + ', gg' + B + '.Nivel' + B + ', ',
    'gg' + B + '.Nome ',
    'FROM gragru' + B + ' gg' + B + ' ',
    LeftJ,
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel' + B + '=gg' + B + '.Nivel' + B + ' ',
    'WHERE gg1.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
    NivUp,
    Filtro,
    '']);
    //
    Geral.MB_Info('SQL Nivel ' + Geral.FF0(Nivel) + slineBreak +
    Qry.SQL.Text);
    //
    if ID <> 0 then
      Qry.Locate('Nivel' + B + '', ID, []);
  end;
{
var
  LeftJ, NivUp, Filtro, A, B: String;
  QrUp: TmySQLQuery;
begin
  Qry.Close;
  if QrGraGruNNivCad.Value >= Nivel then
  begin
    case Nivel of
      4: QrUp := QrGraGru5;
      3: QrUp := QrGraGru4;
      2: QrUp := QrGraGru3;
      else raise EAbort.Create('Nivel n�o implementado: ' + Geral.FF0(Nivel));
    end;
    case Nivel of
      4: Filtro := EdFiltroNiv4.Text;
      3: Filtro := EdFiltroNiv3.Text;
      2: Filtro := EdFiltroNiv2.Text;
      else raise EAbort.Create('Nivel n�o implementado: ' + Geral.FF0(Nivel));
    end;
    A := Geral.FF0(Nivel + 1);
    B := Geral.FF0(Nivel);
    if QrUp.State = dsInactive then
    begin
      LeftJ := '';
      NivUp := '';
    end else
    begin
      LeftJ := 'LEFT JOIN gragru' + A + ' gg' + A + ' ON gg' + A + '.Nivel' + A + '=gg' + B + '.Nivel' + A + ' ';
      NivUp := 'AND gg' + B + '.Nivel' + A + '=' + Geral.FF0(QrUp.FieldByName('Nivel' + A).AsInteger);
    end;
    if Filtro <> '' then
      Filtro :=  'AND gg' + B + '.Nome LIKE "%' + Filtro + '%"';
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT gg' + B + '.Codusu, gg' + B + '.Nivel' + A + ', gg' + B + '.Nivel' + B + ', ',
    'gg' + B + '.Nome ',
    'FROM gragru' + B + ' gg' + B + ' ',
    LeftJ,
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel' + B + '=gg' + B + '.Nivel' + B + ' ',
    'WHERE gg1.PrdGrupTip=' + Geral.FF0(QrGraGruNCodigo.Value),
    NivUp,
    Filtro,
    '']);
    //
    if ID <> 0 then
      Qry.Locate('Nivel' + B + '', ID, []);
  end;
}
end;

procedure TFmGraGruN.ReopenTabePrCCab(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreQuery(QrTabePrCCab, Dmod.MyDB);
  //
  if Codigo <> 0 then
    QrTabePrCCab.Locate('Codigo', Codigo, []);
end;

procedure TFmGraGruN.ReorganizaPnNiveis();
var
  LaPnPan, PanWid12a5, IndivWid, Paineis: Integer;
begin
  LaPnPan := PnPaineis.Width;
  PanWid12a5 := LaPnPan - PainelN.Width - Painel1.Width;
  Paineis := QrGraGruNNivCad.Value - 1;
  if Paineis > 0 then
    IndivWid := PanWid12a5 div Paineis
  else
    IndivWid := 0;
  if IndivWid < 180 then
    IndivWid := 180;
  //
  //
  // Ini 2020-09-25
{
  Splitter5.Visible := False;
  Painel5.Visible   := False;
  Splitter4.Visible := False;
  Painel4.Visible   := False;
  Splitter3.Visible := False;
  Painel3.Visible   := False;
  Splitter2.Visible := False;
  Painel2.Visible   := False;
  Splitter1.Visible := False;
  Painel1.Visible   := False;
  //
  Splitter5.Align := alNone;
  Painel5.Align   := alNone;
  Splitter4.Align := alNone;
  Painel4.Align   := alNone;
  Splitter3.Align := alNone;
  Painel3.Align   := alNone;
  Splitter2.Align := alNone;
  Painel2.Align   := alNone;
  Splitter1.Align := alNone;
  Painel1.Align   := alNone;
  //
}
  Splitter5.Width := HideWid;
  Painel5.Width   := HideWid;
  Splitter4.Width := HideWid;
  Painel4.Width   := HideWid;
  Splitter3.Width := HideWid;
  Painel3.Width   := HideWid;
  Splitter2.Width := HideWid;
  Painel2.Width   := HideWid;
  Splitter1.Width := HideWid;
  Painel1.Width   := HideWid;
  //
{
  Splitter5.Left := 2759;
  Painel5.Left   := 2762;

  Splitter4.Left := 2998;
  Painel4.Left   := 3001;

  Splitter3.Left := 3237;
  Painel3.Left   := 3240;

  Splitter2.Left := 3476;
  Painel2.Left   := 3479;

  Splitter1.Left := 3715;
  Painel1.Left   := 3718;
}
  // fim 2020-09-25
  //
  if QrGraGruNNivCad.Value >= 5 then
  begin
    (*
    Splitter5.Align   := alLeft;
    Splitter5.Visible := True;
    Painel5.Visible   := True;
    Painel5.Align     := alLeft;
    *)
    Splitter5.Width   := SplitWid;
    Painel5.Width     := IndivWid;
  end;
  if QrGraGruNNivCad.Value >= 4 then
  begin
    (*
    Splitter4.Align   := alLeft;
    Splitter4.Visible := True;
    Painel4.Visible   := True;
    Painel4.Align     := alLeft;
    *)
    Splitter4.Width   := SplitWid;
    Painel4.Width     := IndivWid;
  end;
  if QrGraGruNNivCad.Value >= 3 then
  begin
    (*
    Splitter3.Align   := alLeft;
    Splitter3.Visible := True;
    Painel3.Visible   := True;
    Painel3.Align     := alLeft;
    *)
    Splitter3.Width   := SplitWid;
    Painel3.Width     := IndivWid;
  end;
  if QrGraGruNNivCad.Value >= 2 then
  begin
    (*
    Splitter2.Align   := alLeft;
    Splitter2.Visible := True;
    Painel2.Visible   := True;
    Painel2.Align     := alLeft;
    *)
    Splitter2.Width   := SplitWid;
    Painel2.Width     := IndivWid;
  end;
  //
  if QrGraGruNNivCad.Value >= 5 then
  begin
    PainelN.Width := 222;
    DBGPrdgrupTip.Columns[1].Width := 100;
  end else
  begin
    PainelN.Width := 444;
    DBGPrdgrupTip.Columns[1].Width := 350;
  end;
  //
  (*
  Splitter2.Align   := alLeft;
  Splitter1.Visible := True;
  Painel1.Visible   := True;
  Painel1.Align     := alClient;
  *)
  //
end;

procedure TFmGraGruN.RgTipoListaPrecoClick(Sender: TObject);
begin
  AtualizaGradesPrecos;
end;

function TFmGraGruN.StatusVariosItensCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
var
  Coluna, Linha, Ativo, c, l, Controle, Ativos: Integer;
  ColI, ColF, RowI, RowF, CountC, CountL: Integer;
  AtivTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  //ShowMessage('Ativos: ' + IntToStr(Ativos));
  if Ativos > 0 then
    Ativo := 0
  else
    Ativo := 1;
  if Ativo = 0 then
    AtivTxt := ' desativa��o '
  else
    AtivTxt := 'ativa��o';
  //
  if (Coluna = 0) and (Linha = 0) then
  begin
    ItensTxt := 'todo grupo';
  end else if Coluna = 0 then
  begin
    ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
  end else if Linha = 0 then
  begin
    ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
  end;
  if Geral.MB_Pergunta('Confirma a ' + AtivTxt + ' de ' +
  ItensTxt + '?') <> ID_YES then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    Controle := Geral.IMV(GridC.Cells[c, l]);
    if (Ativo = 1) and (Controle = 0) then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'GraGruX', 'GraGruX', 'Controle');
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO gragrux SET Ativo=:P0, GraGruC=:P1, ');
      Dmod.QrUpd.SQL.Add('GraGru1=:P2, GraTamI=:P3, Controle=:P4');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(GridX.Cells[0, l]);
      Dmod.QrUpd.Params[02].AsInteger := QrGraGru1Nivel1.Value;
      Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(GridX.Cells[c, 0]);
      Dmod.QrUpd.Params[04].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE gragrux SET Ativo=:P0 WHERE Controle=:P1');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  DmProd.AtualizaGradesAtivos1(QrGraGru1Nivel1.Value, GridA, GridC);
  Result := True;
  Screen.Cursor := crDefault;
end;

{
procedure TFmGraGruN.ReopenGraCusPrcU(Query: TmySQLQuery; Codigo: Integer);
begin
  Query.Close;
  Query.SQL.Clear;
  if VAR_USUARIO > 0 then
  begin
    Query.SQL.Add('SELECT DISTINCT cpu.Codigo, gcp.CodUsu, gcp.Nome,');
    Query.SQL.Add('ELT(CustoPreco+1, "Custo", "Pre�o") CusPrc,');
    Query.SQL.Add('ELT(TipoCalc+1, "Manual", "Pre�o m�dio", "�ltima compra") NomeTC,');
    Query.SQL.Add('mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA');
    Query.SQL.Add('FROM gracusprcu cpu');
    Query.SQL.Add('LEFT JOIN gracusprc gcp ON gcp.Codigo=cpu.Codigo');
    Query.SQL.Add('LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda');
    Query.SQL.Add('WHERE Usuario=:P0');
    Query.Params[0].AsInteger := VAR_USUARIO;
  end else begin
    Query.SQL.Add('SELECT DISTINCT gcp.Codigo, gcp.CodUsu, gcp.Nome,');
    Query.SQL.Add('ELT(CustoPreco+1, "Custo", "Pre�o") CusPrc,');
    Query.SQL.Add('ELT(TipoCalc+1, "Manual", "Pre�o m�dio", "�ltima compra") NomeTC,');
    Query.SQL.Add('mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA');
    Query.SQL.Add('FROM gracusprc gcp ');
    Query.SQL.Add('LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda');
  end;
  UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
  Query.Locate('Codigo', Codigo, []);
end;
}

function TFmGraGruN.PrecoItemCorTam(GridP, GridC, GridX: TStringGrid;
QrGraCusPrcU: TmySQLQuery): Boolean;
var
  Controle, GraGruX, Nivel1, Lista, Coluna, Linha: Integer;
  Preco: Double;
  SQLType: TSQLType;
  c, l: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := GridX.Cells[Coluna, 0];
  l := GridX.Cells[0, Linha];
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = '' then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = '' then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Geral.MB_Aviso('O item nunca foi ativado!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  Nivel1 := QrGraGru1Nivel1.Value;
  Lista  := QrGraCusPrcU.FieldByName('Codigo').AsInteger;
  Preco  := Geral.DMV(GridP.Cells[Coluna, Linha]);
  if DmProd.ObtemPreco(Preco) then
  begin
    DmProd.QrLocGGV.Close;
    DmProd.QrLocGGV.Params[00].AsInteger := GraGruX;
    DmProd.QrLocGGV.Params[01].AsInteger := Nivel1;
    DmProd.QrLocGGV.Params[02].AsInteger := Lista;
    DmProd.QrLocGGV.Params[03].AsInteger := FEntiPrecos;
    UnDmkDAC_PF.AbreQuery(DmProd.QrLocGGV, Dmod.MyDB);
    //
    Controle := DmProd.QrLocGGVControle.Value;
    if Controle = 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'GraGruVal', 'GraGruVal', 'Controle');
      SQLType := stIns;
    end else SQLType := stUpd;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
    'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'], [
    GraGruX, Nivel1, Lista, Preco, FEntiPrecos], [Controle], True) then
    DmProd.AtualizaGradesPrecos1(Nivel1, Lista, FEntiPrecos, GridP, tpGraCusPrc);
  end;
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmGraGruN.PrecoVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid; QrGraCusPrcU: TmySQLQuery): Boolean;
var
  Coluna, Linha, c, l, Controle, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, Nivel1, Lista, GraGruX: Integer;
  Preco: Double;
  PrecoTxt, ItensTxt: String;
  SQLType: TSQLType;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Nivel1 := QrGraGru1Nivel1.Value;
  Lista  := QrGraCusPrcU.FieldByName('Codigo').AsInteger;
  Preco  := Geral.DMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if DmProd.ObtemPreco(Preco) then
  begin
    PrecoTxt := QrGraCusPrcU.FieldByName('SIGLAMOEDA').AsString + ' ' +
      Geral.FFT(Preco, Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Geral.MB_Pergunta('Confirma o valor de ' + PrecoTxt + ' para ' +
    ItensTxt + '?') <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
      begin
        DmProd.QrLocGGV.Close;
        DmProd.QrLocGGV.Params[00].AsInteger := GraGruX;
        DmProd.QrLocGGV.Params[01].AsInteger := Nivel1;
        DmProd.QrLocGGV.Params[02].AsInteger := Lista;
        DmProd.QrLocGGV.Params[03].AsInteger := FEntiPrecos;
        UnDmkDAC_PF.AbreQuery(DmProd.QrLocGGV, Dmod.MyDB);
        //
        Controle := DmProd.QrLocGGVControle.Value;
        if Controle = 0 then
        begin
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'GraGruVal', 'GraGruVal', 'Controle');
          SQLType := stIns;
        end else SQLType := stUpd;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
        'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'], [
        GraGruX, Nivel1, Lista, Preco, FEntiPrecos], [Controle], True);
      end;
    end;
    DmProd.AtualizaGradesPrecos1(Nivel1, Lista, FEntiPrecos, GridP, tpGraCusPrc);
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmGraGruN.IniciaSequenciaCadastro(PrdGrupTip: Integer);
begin
  DmProd.QrPrdGrupJan.Close;
  DmProd.QrPrdGrupJan.Params[0].AsInteger := PrdGrupTip;
  UnDmkDAC_PF.AbreQuery(DmProd.QrPrdGrupJan, Dmod.MyDB);
  //
  ContinuaSequenciaCadastro();
end;

procedure TFmGraGruN.ItemNobsico1Click(Sender: TObject);
begin
{$IfDef usaGraTX}
  GraTX_Jan.MostraFormFiConsSpc(QrGraGru1Nivel1.Value, QrGraGru1GraTamCad.Value,
    QrGraGru1Nome.Value);
  ReopenFiConsOri();
{$Else}
  Grl_Geral.InfoSemModulo(mdlappGraTX);
{$EndIf}
end;

procedure TFmGraGruN.Janeladecriaoalterao1Click(Sender: TObject);
begin
{$IfDef usaGraTX}
  GraTX_Jan.MostraFormGraCmpCad(0);
{$Else}
  Grl_Geral.InfoSemModulo(mdlappGraTX);
{$EndIf}
end;

procedure TFmGraGruN.LocalizaIDNivel1(Codigo: Integer);
begin
  QrLocod.Close;
  QrLocod.SQL.Clear;
  QrLocod.SQL.Add('SELECT PrdGrupTip, Nivel1, Nivel2, Nivel3, Nivel4, Nivel5 ');
  QrLocod.SQL.Add('FROM gragru1');
  QrLocod.SQL.Add('WHERE Nivel1=:P0');
  QrLocod.SQL.Add('');
  QrLocod.Params[0].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(QrLocod, Dmod.MyDB);
  //
  if QrLocod.RecordCount > 0 then
    LocalizaItem(QrLocodPrdGrupTip.Value, QrLocodNivel5.Value,
    QrLocodNivel4.Value, QrLocodNivel3.Value,
    QrLocodNivel2.Value, QrLocodNivel1.Value);
end;

procedure TFmGraGruN.LocalizaItem(PrdGrupTip, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1: Integer);
begin
  if QrGraGruN.Locate('Codigo', PrdGrupTip, []) then
  begin
    //Continua := True;
    //if Painel5.Visible then
    if Painel5.Width > HideWid then
    begin
      if not QrGraGru5.Locate('Nivel5', Nivel5, []) then
      begin
        Geral.MB_Aviso('O Nivel 5 ' + Geral.FF0(Nivel5) + ' -> "' +
        LaNivel5.Caption + '" n�o foi localizado! Verifique o filtro de pesquisa!');
        //Continua := False;
      end;
    end;
    //
    //Continua := True;
    //if Painel4.Visible then
    if Painel4.Width > HideWid then
    begin
      if not QrGraGru4.Locate('Nivel4', Nivel4, []) then
      begin
        Geral.MB_Aviso('O Nivel 4 ' + Geral.FF0(Nivel4) + ' -> "' +
        LaNivel4.Caption + '" n�o foi localizado! Verifique o filtro de pesquisa!');
        //Continua := False;
      end;
    end;
    //
    //Continua := True;
    //if Painel3.Visible then
    if Painel3.Width > HideWid then
    begin
      if not QrGraGru3.Locate('Nivel3', Nivel3, []) then
      begin
        Geral.MB_Aviso('O Nivel 3 ' + Geral.FF0(Nivel3) + ' -> "' +
        LaNivel3.Caption + '" n�o foi localizado! Verifique o filtro de pesquisa!');
        //Continua := False;
      end;
    end;
    //
    //Continua := True;
    //if Painel2.Visible then
    if Painel2.Width > HideWid then
    begin
      if not QrGraGru2.Locate('Nivel2', Nivel2, []) then
      begin
        Geral.MB_Aviso('O Nivel 2 ' + Geral.FF0(Nivel2) + ' -> "' +
        LaNivel2.Caption + '" n�o foi localizado! Verifique o filtro de pesquisa!');
        //Continua := False;
      end;
    end;
    //
    //Continua := True;
    if not QrGraGru1.Locate('Nivel1', Nivel1, []) then
    begin
        Geral.MB_Aviso('O Nivel 1 ' + Geral.FF0(Nivel1) + ' -> "' +
        LaNivel1.Caption + '" n�o foi localizado! Verifique o filtro de pesquisa!');
      //Continua := False;
    end;
    //
  end else Geral.MB_Aviso('O "Tipo de Produto" ' + FormatFloat('0',
  PrdGrupTip) + ' n�o foi localizado! Verifique o filtro de pesquisa!');
end;

procedure TFmGraGruN.LocalizaReduzido(GraGruX: Integer);
begin
  DmodG.QrGraGruX.Close;
  DmodG.QrGraGruX.Params[0].AsInteger := GraGruX;
  UnDmkDAC_PF.AbreQuery(DmodG.QrGraGruX, Dmod.MyDB);
  //
  if DmodG.QrGraGruX.RecordCount > 0 then
  begin
    LocalizaItem(DmodG.QrGraGruXPrdGrupTip.Value, DmodG.QrGraGruXNivel5.Value,
    DmodG.QrGraGruXNivel4.Value, DmodG.QrGraGruXNivel3.Value,
    DmodG.QrGraGruXNivel2.Value, DmodG.QrGraGruXNivel1.Value);
  end;
end;

procedure TFmGraGruN.MostraFormGraGru1Cons(SQLType: TSQLType);
var
  Nome: String;
  Nivel1, Controle: Integer;
begin
  Nivel1         := QrGraGru1Nivel1.Value;
  Controle       := 0;
  Nome           := '';
  //
  if DBCheck.CriaFm(TFmGraGru1Cons, FmGraGru1Cons, afmoNegarComAviso) then
  begin
    FmGraGru1Cons.ImgTipo.SQLType := SQLType;
    FmGraGru1Cons.ImgTipo.SQLType := SQLType;
    FmGraGru1Cons.FQrCab := QrGraGru1;
    FmGraGru1Cons.FQrIts := QrGraGru1Cons;
    FmGraGru1Cons.FDsCab := DsGraGru1;
    //
    FmGraGru1Cons.ReopenGraGru1(QrGraGru1Nivel1.Value);
    //
    if SQLType = stUpd then
    begin
      FmGraGru1Cons.EdControle.ValueVariant      := QrGraGru1ConsControle.Value;
      FmGraGru1Cons.EdNiv1_COMP.ValueVariant     := QrGraGru1ConsNiv1_COMP.Value;
      FmGraGru1Cons.CBNiv1_COMP.KeyValue         := QrGraGru1ConsNiv1_COMP.Value;
      FmGraGru1Cons.EdQtd_Comp.ValueVariant      := QrGraGru1ConsQtd_Comp.Value;
      FmGraGru1Cons.EdPerda.ValueVariant         := QrGraGru1ConsPerda.Value;
    end;
    FmGraGru1Cons.ShowModal;
    FmGraGru1Cons.Destroy;
    ReopenGraGru1Lau(Controle);
  end;
end;

procedure TFmGraGruN.MostraPrdGruNew(PrdGrTp, Nivel5, Nivel4, Nivel3, Nivel2: Integer;
  GraTabAppCod: Integer = 0; GraTabAppTxt: String = ''; NewNome: String = '');
(*
var
  PrdGrupTip, GraGruN, GraGru5, GraGru4, GraGru3, GraGru2, GraGru1: Integer;
*)
var
  PrdGrupTip: Integer;
begin
  if (QrGraGruN.State = dsInactive) or (QrGraGruN.RecordCount = 0) then Exit;
  //
  (*
  DmProd.FPrdGrupTip_Nome := '????';
  DmProd.FFmGraGruNew     := True;
  DmProd.FChamouGraGruNew := cggnFmGraGruN;
  DmProd.FNewGraGru5      := Nivel5;
  DmProd.FNewGraGru4      := Nivel4;
  DmProd.FNewGraGru3      := Nivel3;
  DmProd.FNewGraGru2      := Nivel2;
  DmProd.FNewGraGru1      := 0;
  //
  UMyMod.FormInsUpd_Show(TFmPrdGruNew, FmPrdGruNew, afmoNegarComAviso,
    QrGraGru3, stIns);
  //FmPrdGruNew.FClicked := True;
  *)
  DmProd.InsereItemPrdGruNew(cggnFmGraGruN, PrdGrTp, 0(*UnidMed*), (*Nome*)'',
    (*NCM*)'', (*ForcaCadastrGGX_SemCorTam*)False, Nivel5, Nivel4, Nivel3,
    Nivel2, 0, True, GraTabAppCod, GraTabAppTxt, FNewNome);
  //
  if FNewPrdGrupTip <> 0 then
  begin
    PrdGrupTip     := FNewPrdGrupTip;
    FNewPrdGrupTip := 0;
    //
    if FLaTipoGrupo = stIns then
      IniciaSequenciaCadastro(PrdGrupTip);
  end;
  (*
  06/02/2014
  � reaberto ao confirmar o cadastro do produto
  Desmarcado para otimizar o desempenho

  GraGruN := QrGraGruNCodigo.Value;
  GraGru5 := QrGraGru5Nivel5.Value;
  GraGru4 := QrGraGru4Nivel4.Value;
  GraGru3 := QrGraGru3Nivel3.Value;
  GraGru2 := QrGraGru2Nivel2.Value;
  GraGru1 := QrGraGru1Nivel1.Value;
  //
  ReopenGraGruN(GraGruN);
  ReopenGraGru5(GraGru5, True);
  ReopenGraGru4(GraGru4, True);
  ReopenGraGru3(GraGru3, True);
  ReopenGraGru2(GraGru2, True);
  ReopenGraGru1(GraGru1, True);
  *)
end;

procedure TFmGraGruN.MostraSubForm;
begin
  case FMostraSubForm of
    tsfNenhum:
      ;
    tsfDadosGerais:
      EditaDadosGerais();
    tsfDadosFiscais:
      EditaDadosFiscais();
  end;
end;

procedure TFmGraGruN.Nivel12Click(Sender: TObject);
begin
  FmMeuDBUses.PesquisaNome('gragru1', 'Nome', 'Nivel1');
  if VAR_CADASTRO <> 0 then
    LocalizaIDNivel1(VAR_CADASTRO);
end;

procedure TFmGraGruN.CadastrodoReduzido1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruX, FmGraGruX, afmoSoBoss) then
  begin
    FmGraGruX.FGraGruX := QrGraGruXControle.Value;
    FmGraGruX.QrGraGruX.Close;
    FmGraGruX.QrGraGruX.Params[0].AsInteger := QrGraGruXControle.Value;
    UnDmkDAC_PF.AbreQuery(FmGraGruX.QrGraGruX, Dmod.MyDB);

    FmGraGruX.EdGraGru1.ValueVariant := QrGraGruXGraGru1.Value;
    FmGraGruX.CBGraGru1.KeyValue := QrGraGruXGraGru1.Value;

    FmGraGruX.EdGraTamCad.ValueVariant := QrGraGruXGraTamCad.Value;
    FmGraGruX.CBGraTamCad.KeyValue := QrGraGruXGraTamCad.Value;

    FmGraGruX.EdGraCorCad.ValueVariant := QrGraGruXGraCorCad.Value;
    FmGraGruX.CBGraCorCad.KeyValue := QrGraGruXGraCorCad.Value;

    FmGraGruX.ShowModal;
    FmGraGruX.Destroy;
  end;
end;

procedure TFmGraGruN.CdigoNivel11Click(Sender: TObject);
var
  Cod: Variant;
begin
  Cod := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Cod, 0, 0,
  '', '', True, 'C�digo N�vel 1', 'Informe o "C�digo N�vel 1": ', 0, Cod) then
  begin
    QrLocod.Close;
    QrLocod.SQL.Clear;
    QrLocod.SQL.Add('SELECT PrdGrupTip, Nivel1, Nivel2, Nivel3, Nivel4, Nivel5 ');
    QrLocod.SQL.Add('FROM gragru1');
    QrLocod.SQL.Add('WHERE CodUsu=:P0');
    QrLocod.SQL.Add('');
    QrLocod.Params[0].AsInteger := Cod;
    UnDmkDAC_PF.AbreQuery(QrLocod, Dmod.MyDB);
    //
    if QrLocod.RecordCount > 0 then
      LocalizaItem(QrLocodPrdGrupTip.Value, QrLocodNivel5.Value,
      QrLocodNivel4.Value, QrLocodNivel3.Value,
      QrLocodNivel2.Value, QrLocodNivel1.Value);
  end;
end;

procedure TFmGraGruN.ConfiguraPageIndex(Aba: Integer);
begin
  if Aba = CO_IDX_PCGeral_Precos then //Pre�os
  begin
    PnEntidade.Visible       := True;
    PnEntiMenu.Visible       := True;
    dmkDBGrid1.Visible       := True;
    RgTipoListaPreco.Visible := True;
    //
    AtualizaGradesPrecos;
  end else
  if Aba = CO_IDX_PCGeral_Estoque then //Estoque
  begin
    PnEntidade.Visible       := True;
    PnEntiMenu.Visible       := False;
    dmkDBGrid1.Visible       := False;
    RgTipoListaPreco.Visible := False;
  end else
    PnEntidade.Visible := False;
end;

procedure TFmGraGruN.ContinuaSequenciaCadastro();
var
  Msg: String;
begin
  if (DmProd.QrPrdGrupJan.State <> dsInactive) and (not DmProd.QrPrdGrupJan.Eof) then
  begin
    // Grade de tamanhos
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-GRUPO-004' then
    begin
      if not NaoGradeado(Msg) then
        UMyMod.FormInsUpd_Show(TFmGraGruT, FmGraGruT, afmoNegarComAviso, QrGraGru1, stUpd);
    end else
    // Ativa��o de Produtos
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-GRUPO-008' then
    begin
      if DBCheck.CriaFm(TFmGraGruAti, FmGraGruAti, afmoNegarComAviso) then
      begin
        FmGraGruAti.ShowModal;
        FmGraGruAti.Destroy;
      end;
    end else
    // Cor
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-GRUPO-005' then
    begin
      if not NaoUsaCores(Msg) then
        IncluiNovaCor();
    end else
    // Varias cores
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-CORES-004' then
    begin
      if not NaoUsaCores(Msg) then
        InclusaoDeVariasCores();
    end else
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-GRUPO-006' then
    begin
      EditaDadosGerais();
    end else
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-GRUPO-007' then
    begin
      IncluiNovoItemDeAtributo();
    end else
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-ATRIB-003' then
    begin
      InclusaoDeVariosAtributos();
    end else
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-GRUPO-014' then
    begin
      EditaDadosFiscais();
    end else
    if (CO_DMKID_APP = 24) //Bugstrol
      and (AnsiMatchStr(UpperCase(DmProd.QrPrdGrupJanJanela.Value),
      ['PRD-GRUPO-023', 'PRD-GRUPO-025', 'PRD-GRUPO-033'])) then
    begin
      AlteraProduto();
    end else
{   N�o pode, pois pode ter mais de um cliente interno.
    if DmProd.QrPrdGrupJanJanela.Value = 'PRD-GRUPO-010' then
    begin
      if DBCheck.CriaFm(TFmGraGruPrc, FmGraGruPrc, afmoNegarComAviso) then
      begin
        FmGraGruPrc.ShowModal;
        FmGraGruPrc.Destroy;
        DmProd.AtualizaGradesPrecos1(QrGraGru1Nivel1.Value,
          QrGraCusPrcUCodigo.Value, GradeP, tpGraCusPrc);
      end;
    end else
}
    ;
    DmProd.QrPrdGrupJan.Next;
    ContinuaSequenciaCadastro();
  end else DmProd.QrPrdGrupJan.Close;
end;

procedure TFmGraGruN.DBEdCSOSNChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoC.Text :=  UFinanceiro.CSOSN_Get(1, Geral.IMV(DBEdCSOSN.Text));
{$EndIf}
end;

procedure TFmGraGruN.DBGNivel1DblClick(Sender: TObject);
begin
  EditaDadosFiscais();
end;

procedure TFmGraGruN.DBGParDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorTexto, CorFundo: TColor;
begin
  if Column.FieldName = 'PercTot' then
  begin
    CorFundo := clWhite;
    if (QrGraCmpParPercTot.Value > 99.99) then
      CorTexto := clBlue
    else
      CorTexto := clRed;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGPar), Rect, CorTexto, CorFundo,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmGraGruN.DesSelecionaReduzido();
begin
  FGraGruXSel := 0;
  SBReduzido.Panels[1].Text := '';
  SBReduzido.Panels[2].Text := '';
  QrGraGruEX.Close;
  QrGraGruVinc.Close;
end;

procedure TFmGraGruN.EdPIS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdPIS_CST.Text);
{$EndIf}
end;

procedure TFmGraGruN.EdCOFINS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdCOFINS_CST.Text);
{$EndIf}
end;

procedure TFmGraGruN.EdIPI_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdIPI_CST.Text);
{$EndIf}
end;


end.



