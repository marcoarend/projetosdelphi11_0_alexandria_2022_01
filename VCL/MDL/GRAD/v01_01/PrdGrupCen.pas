unit PrdGrupCen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DB, (*&&ABSMain,*)
  DBGrids, DBCtrls, dmkImage, UnDmkEnums, dmkDBGridZTO, mySQLDbTables;

type
  TFmPrdGrupCen = class(TForm)
    Panel1: TPanel;
    DataSource1: TDataSource;
    DBGItsToSel: TdmkDBGridZTO;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Query: TMySQLQuery;
    QueryCodUsu: TIntegerField;
    QueryCodigo: TIntegerField;
    QueryNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGItsToSelCellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmPrdGrupCen: TFmPrdGrupCen;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmPrdGrupCen.BtOKClick(Sender: TObject);
begin
  FCadastrar := True;
(*
  Query.Filter   := 'Ativo=1';
  Query.Filtered := True;
*)
  //
  Close;
end;

procedure TFmPrdGrupCen.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmPrdGrupCen.DBGItsToSelCellClick(Column: TColumn);
var
  Status: Byte;
begin
(*&&
  if Column.FieldName = 'Ativo' then
  begin
    if Query.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Ativo').Value := Status;
    Query.Post;
  end;
*)
end;

procedure TFmPrdGrupCen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrdGrupCen.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCadastrar := False;
end;

procedure TFmPrdGrupCen.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

(*&&
object Query: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 12
  Top = 12
  object QueryAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
  object QueryCodUsu: TIntegerField
    FieldName = 'CodUsu'
  end
  object QueryCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QueryNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
end
*)

end.
