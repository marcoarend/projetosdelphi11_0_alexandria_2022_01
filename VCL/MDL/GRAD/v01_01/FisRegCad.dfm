object FmFisRegCad: TFmFisRegCad
  Left = 368
  Top = 194
  Caption = 'FIS-REGRA-001 :: Regras Fiscais'
  ClientHeight = 579
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 527
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = -116
    ExplicitTop = -24
    object Splitter2: TSplitter
      Left = 0
      Top = 429
      Width = 1008
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 64
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 89
      Align = alTop
      Caption = ' Regra de Movimenta'#231#227'o: '
      TabOrder = 0
      object Label10: TLabel
        Left = 8
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label11: TLabel
        Left = 88
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label12: TLabel
        Left = 232
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label13: TLabel
        Left = 800
        Top = 21
        Width = 93
        Height = 13
        Caption = 'Tipo de movimento:'
      end
      object Label17: TLabel
        Left = 8
        Top = 44
        Width = 109
        Height = 13
        Caption = 'Natureza da opera'#231#227'o:'
      end
      object Label23: TLabel
        Left = 8
        Top = 68
        Width = 72
        Height = 13
        Caption = 'Conta a d'#233'bito:'
      end
      object Label22: TLabel
        Left = 504
        Top = 68
        Width = 75
        Height = 13
        Caption = 'Conta a cr'#233'dito:'
      end
      object DBEdCodigo: TDBEdit
        Left = 28
        Top = 16
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 148
        Top = 16
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdNome: TDBEdit
        Left = 284
        Top = 16
        Width = 513
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 900
        Top = 16
        Width = 23
        Height = 21
        DataField = 'TipoMov'
        DataSource = DsFisRegCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 927
        Top = 16
        Width = 74
        Height = 21
        DataField = 'NOMETIPOMOV'
        DataSource = DsFisRegCad
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 120
        Top = 40
        Width = 881
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'ide_natOp'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 84
        Top = 64
        Width = 416
        Height = 21
        Color = clWhite
        DataField = 'NO_GenCtbD'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 584
        Top = 64
        Width = 416
        Height = 21
        Color = clWhite
        DataField = 'NO_GenCtbC'
        DataSource = DsFisRegCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 463
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 66
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 240
        Top = 15
        Width = 766
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 652
          Top = 0
          Width = 114
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 108
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtRegra: TBitBtn
          Tag = 10104
          Left = 4
          Top = 4
          Width = 108
          Height = 40
          Cursor = crHandPoint
          Caption = '&Regra'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtRegraClick
        end
        object BtMovimentacao: TBitBtn
          Tag = 438
          Left = 112
          Top = 4
          Width = 108
          Height = 40
          Cursor = crHandPoint
          Caption = '&Movimenta'#231#227'o'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtMovimentacaoClick
        end
        object BtCFOP: TBitBtn
          Tag = 391
          Left = 220
          Top = 4
          Width = 108
          Height = 40
          Cursor = crHandPoint
          Caption = 'CF&OP'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCFOPClick
        end
        object BtEmitDest: TBitBtn
          Tag = 10105
          Left = 328
          Top = 4
          Width = 108
          Height = 40
          Cursor = crHandPoint
          Caption = 'Emit/&Dest'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtEmitDestClick
        end
        object BtExcecao: TBitBtn
          Tag = 502
          Left = 436
          Top = 4
          Width = 108
          Height = 40
          Cursor = crHandPoint
          Caption = 'E&xce'#231#227'o'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtExcecaoClick
        end
        object BtEstricao: TBitBtn
          Left = 544
          Top = 4
          Width = 108
          Height = 40
          Cursor = crHandPoint
          Caption = '&Estri'#231#227'o'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtEstricaoClick
        end
      end
    end
    object PageControl3: TPageControl
      Left = 0
      Top = 89
      Width = 1008
      Height = 340
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 2
      object TabSheet3: TTabSheet
        Caption = 'Tributa'#231#227'o'
        object PnTributacao: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 312
          Align = alClient
          TabOrder = 0
          object GroupBox8: TGroupBox
            Left = 1
            Top = 1
            Width = 67
            Height = 310
            Align = alLeft
            Caption = ' Tributo: '
            TabOrder = 0
            object dmkCheckBox1: TDBCheckBox
              Left = 8
              Top = 24
              Width = 60
              Height = 17
              Caption = 'ICMS'
              DataField = 'ICMS_Usa'
              DataSource = DsFisRegCad
              TabOrder = 0
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object dmkCheckBox2: TDBCheckBox
              Left = 8
              Top = 44
              Width = 60
              Height = 17
              Caption = 'IPI'
              DataField = 'IPI_Usa'
              DataSource = DsFisRegCad
              TabOrder = 1
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object dmkCheckBox3: TDBCheckBox
              Left = 8
              Top = 64
              Width = 60
              Height = 17
              Caption = 'PIS'
              DataField = 'PIS_Usa'
              DataSource = DsFisRegCad
              TabOrder = 2
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object dmkCheckBox4: TDBCheckBox
              Left = 8
              Top = 84
              Width = 60
              Height = 17
              Caption = 'COFINS'
              DataField = 'COFINS_Usa'
              DataSource = DsFisRegCad
              TabOrder = 3
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object dmkCheckBox5: TDBCheckBox
              Left = 8
              Top = 104
              Width = 60
              Height = 17
              Caption = 'IR'
              DataField = 'IR_Usa'
              DataSource = DsFisRegCad
              TabOrder = 4
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object dmkCheckBox6: TDBCheckBox
              Left = 8
              Top = 124
              Width = 60
              Height = 17
              Caption = 'CS'
              DataField = 'CS_Usa'
              DataSource = DsFisRegCad
              TabOrder = 5
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object dmkCheckBox7: TDBCheckBox
              Left = 8
              Top = 144
              Width = 60
              Height = 17
              Caption = 'ISS'
              DataField = 'ISS_Usa'
              DataSource = DsFisRegCad
              TabOrder = 6
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox1: TDBCheckBox
              Left = 8
              Top = 164
              Width = 60
              Height = 17
              Caption = 'II'
              DataField = 'II_Usa'
              DataSource = DsFisRegCad
              TabOrder = 7
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
          end
          object PageControl1: TPageControl
            Left = 128
            Top = 1
            Width = 871
            Height = 310
            ActivePage = TabSheet12
            Align = alClient
            TabOrder = 1
            object TabSheet1: TTabSheet
              Caption = ' Regras CFOP '
              object Splitter1: TSplitter
                Left = 0
                Top = 22
                Width = 863
                Height = 4
                Cursor = crVSplit
                Align = alBottom
                ExplicitTop = 63
                ExplicitWidth = 866
              end
              object Panel9: TPanel
                Left = 0
                Top = 259
                Width = 863
                Height = 23
                Align = alBottom
                TabOrder = 0
                object Label5: TLabel
                  Left = 4
                  Top = 4
                  Width = 136
                  Height = 13
                  Caption = 'Pref. = Ordem de prefer'#234'ncia'
                end
                object Label6: TLabel
                  Left = 148
                  Top = 4
                  Width = 177
                  Height = 13
                  Caption = 'TIE= Contribuinte estadual (Tem I.E.) '
                end
                object Label14: TLabel
                  Left = 336
                  Top = 4
                  Width = 190
                  Height = 13
                  Caption = 'VMU = Venda/compra para mesma UF?'
                end
                object Label15: TLabel
                  Left = 532
                  Top = 4
                  Width = 172
                  Height = 13
                  Caption = 'PFP = Produto de fabrica'#231#227'o pr'#243'pria'
                end
                object Label19: TLabel
                  Left = 716
                  Top = 4
                  Width = 168
                  Height = 13
                  Caption = 'PMO = M'#227'o-de-Obra para terceiros.'
                end
              end
              object DBGFisRegCFOP2: TdmkDBGridZTO
                Left = 0
                Top = 0
                Width = 863
                Height = 22
                Align = alClient
                DataSource = DsFisRegCFOP
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CFOP'
                    Width = 44
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'OrdCFOPGer'
                    Title.Alignment = taCenter
                    Title.Caption = 'Pref.'
                    Width = 35
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Contribui'
                    Title.Caption = 'TIE'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Interno'
                    Title.Caption = 'VMU'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Proprio'
                    Title.Caption = 'PFP'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Servico'
                    Title.Caption = 'PMO'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubsTrib'
                    Title.Caption = ' ST'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_CFOP'
                    Title.Caption = 'Descri'#231#227'o CFOP'
                    Width = 614
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Visible = True
                  end>
              end
              object Panel1: TPanel
                Left = 0
                Top = 26
                Width = 863
                Height = 233
                Align = alBottom
                Caption = 'Panel1'
                TabOrder = 2
                object Splitter3: TSplitter
                  Left = 569
                  Top = 1
                  Height = 231
                  Align = alRight
                  ExplicitLeft = 572
                end
                object Panel11: TPanel
                  Left = 572
                  Top = 1
                  Width = 290
                  Height = 231
                  Align = alRight
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Panel12: TPanel
                    Left = 0
                    Top = 0
                    Width = 290
                    Height = 13
                    Align = alTop
                    BevelOuter = bvNone
                    Caption = 'Exce'#231#245'es'
                    TabOrder = 0
                  end
                  object dmkDBGrid1: TdmkDBGridZTO
                    Left = 0
                    Top = 13
                    Width = 290
                    Height = 218
                    Align = alClient
                    DataSource = DsFisRegUFx
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Nivel'
                        Title.Caption = 'Niv'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CodNiv'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ICMSAliq'
                        Title.Caption = 'ICMS'
                        Width = 36
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_B'
                        Title.Caption = 'CST '#250'nico'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CSOSN'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'cBenef'
                        Title.Caption = 'C'#243'd. benef. fiscal'
                        Width = 90
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'pRedBC'
                        Title.Caption = '%RedBC'
                        Width = 48
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ModBC_TXT'
                        Width = 17
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'pDif'
                        Title.Caption = '% Diferim.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Nivel4'
                        Visible = True
                      end>
                  end
                end
                object DBGFisRegUFs: TdmkDBGridZTO
                  Left = 1
                  Top = 1
                  Width = 568
                  Height = 231
                  Align = alClient
                  DataSource = DsFisRegUFs
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'CST_B'
                      Title.Caption = 'CST '#250'nico'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CSOSN'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'cBenef'
                      Title.Caption = 'C'#243'd. benef. fiscal'
                      Width = 90
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ModBC_TXT'
                      Title.Caption = 'Mod. de det. da BC do ICMS'
                      Width = 150
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'pRedBC'
                      Title.Caption = '% Red. BC'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UFEmit'
                      Title.Caption = 'Minha UF'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ICMSAliq'
                      Title.Caption = 'ICMS'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'pICMSInter'
                      Title.Caption = '% ICMS Inter.'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UFDest'
                      Title.Caption = 'UF C/F'
                      Width = 50
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'pICMSUFDest'
                      Title.Caption = '% ICMS inter.'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'pFCPUFDest'
                      Title.Caption = '% ICMS FCP'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'pBCUFDest'
                      Title.Caption = '% BC da BC Orig.'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'pICMSInterPart'
                      Title.Caption = '% ICMS part.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'pDif'
                      Title.Caption = '% Diferim.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OriTES'
                      Title.Caption = 'TES origem'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OriCST_ICMS'
                      Title.Caption = 'Ori CST ICMS'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OriCST_IPI'
                      Title.Caption = 'Ori CST IPI'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OriCST_PIS'
                      Title.Caption = 'Ori CST PIS'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OriCST_COFINS'
                      Title.Caption = 'Ori CST COFINS'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EFD_II_C195'
                      Title.Caption = 'EFD II x195'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EFD_II_C197'
                      Title.Caption = 'EFD II x197'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Conta'
                      Visible = True
                    end>
                end
              end
              object DBGFisRegCFOP: TdmkDBGrid
                Left = 77
                Top = 149
                Width = 866
                Height = 64
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CFOP'
                    Width = 44
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'OrdCFOPGer'
                    Title.Alignment = taCenter
                    Title.Caption = 'Pref.'
                    Width = 35
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Contribui'
                    Title.Caption = 'TIE'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Interno'
                    Title.Caption = 'VMU'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Proprio'
                    Title.Caption = 'PFP'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Servico'
                    Title.Caption = 'PMO'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubsTrib'
                    Title.Caption = ' ST'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_CFOP'
                    Title.Caption = 'Descri'#231#227'o CFOP'
                    Width = 400
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'OriCFOP'
                    Title.Caption = 'Origem'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_OriCFOP'
                    Title.Caption = 'Descri'#231#227'o do CFOP de origem'
                    Width = 400
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsFisRegCFOP
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 3
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CFOP'
                    Width = 44
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'OrdCFOPGer'
                    Title.Alignment = taCenter
                    Title.Caption = 'Pref.'
                    Width = 35
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Contribui'
                    Title.Caption = 'TIE'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Interno'
                    Title.Caption = 'VMU'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Proprio'
                    Title.Caption = 'PFP'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Servico'
                    Title.Caption = 'PMO'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubsTrib'
                    Title.Caption = ' ST'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_CFOP'
                    Title.Caption = 'Descri'#231#227'o CFOP'
                    Width = 400
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'OriCFOP'
                    Title.Caption = 'Origem'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_OriCFOP'
                    Title.Caption = 'Descri'#231#227'o do CFOP de origem'
                    Width = 400
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Visible = True
                  end>
              end
            end
            object TabSheet12: TTabSheet
              Caption = 'Fiscal / Cont'#225'bil'
              ImageIndex = 2
              object Panel22: TPanel
                Left = 0
                Top = 0
                Width = 863
                Height = 282
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object SpeedButton14: TSpeedButton
                  Left = 972
                  Top = 4
                  Width = 21
                  Height = 21
                  Caption = '...'
                  Enabled = False
                  Visible = False
                  OnClick = SbCtbCadMoFClick
                end
                object SpeedButton15: TSpeedButton
                  Left = 972
                  Top = 28
                  Width = 21
                  Height = 21
                  Caption = '...'
                  Enabled = False
                  Visible = False
                  OnClick = CtbPlaCtaClick
                end
                object Panel25: TPanel
                  Left = 0
                  Top = 56
                  Width = 863
                  Height = 56
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object DBRadioGroup5: TDBRadioGroup
                    Left = 4
                    Top = 8
                    Width = 217
                    Height = 45
                    Caption = ' Informar documento no EFD PIS/COFINS:'
                    Columns = 3
                    DataField = 'InfoEFD_PisCofins'
                    DataSource = DsFisRegCad
                    Items.Strings = (
                      '??'
                      'Sim'
                      'N'#227'o')
                    TabOrder = 0
                    Values.Strings = (
                      '0'
                      '1'
                      '2'
                      '3'
                      '4'
                      '5'
                      '6'
                      '7'
                      '8'
                      '9')
                  end
                  object DBRadioGroup6: TDBRadioGroup
                    Left = 420
                    Top = 8
                    Width = 433
                    Height = 45
                    Caption = ' Integra o faturamento no DRE: '
                    Columns = 4
                    DataField = 'IntegraDRE'
                    DataSource = DsFisRegCad
                    Items.Strings = (
                      'Indefinido'
                      'N'#227'o integra'
                      'Receita (Venda)'
                      'Devolu. de venda')
                    TabOrder = 1
                    Values.Strings = (
                      '0'
                      '1'
                      '2'
                      '3'
                      '4'
                      '5'
                      '6'
                      '7'
                      '8'
                      '9')
                  end
                  object DBRadioGroup7: TDBRadioGroup
                    Left = 220
                    Top = 8
                    Width = 201
                    Height = 45
                    Caption = 'Conta a Informar no EFD PIS/COFINS:'
                    Columns = 3
                    DataField = 'CreDeb_PisCofins'
                    DataSource = DsFisRegCad
                    Items.Strings = (
                      '??'
                      'D'#233'bito'
                      'Cr'#233'dito')
                    TabOrder = 2
                    Values.Strings = (
                      '0'
                      '1'
                      '2'
                      '3'
                      '4'
                      '5'
                      '6'
                      '7'
                      '8'
                      '9')
                  end
                end
                object Panel26: TPanel
                  Left = 0
                  Top = 0
                  Width = 863
                  Height = 56
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label30: TLabel
                    Left = 4
                    Top = 36
                    Width = 142
                    Height = 13
                    Caption = 'Reduz. plano contas - cr'#233'dito:'
                  end
                  object Label31: TLabel
                    Left = 4
                    Top = 12
                    Width = 139
                    Height = 13
                    Caption = 'Reduz. plano contas - d'#233'bito:'
                  end
                  object DBEdit8: TDBEdit
                    Left = 152
                    Top = 8
                    Width = 56
                    Height = 21
                    TabStop = False
                    DataField = 'GenCtbD'
                    DataSource = DsFisRegCad
                    TabOrder = 0
                  end
                  object DBEdit9: TDBEdit
                    Left = 152
                    Top = 31
                    Width = 56
                    Height = 21
                    TabStop = False
                    DataField = 'GenCtbC'
                    DataSource = DsFisRegCad
                    TabOrder = 1
                  end
                  object DBEdit10: TDBEdit
                    Left = 212
                    Top = 8
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'ORD_GenCtbD'
                    DataSource = DsFisRegCad
                    TabOrder = 2
                  end
                  object DBEdit11: TDBEdit
                    Left = 212
                    Top = 31
                    Width = 100
                    Height = 21
                    TabStop = False
                    DataField = 'ORD_GenCtbC'
                    DataSource = DsFisRegCad
                    TabOrder = 3
                  end
                  object DBEdit12: TDBEdit
                    Left = 316
                    Top = 8
                    Width = 540
                    Height = 21
                    TabStop = False
                    DataField = 'NO_GenCtbD'
                    DataSource = DsFisRegCad
                    TabOrder = 4
                  end
                  object DBEdit13: TDBEdit
                    Left = 316
                    Top = 31
                    Width = 540
                    Height = 21
                    TabStop = False
                    DataField = 'NO_GenCtbC'
                    DataSource = DsFisRegCad
                    TabOrder = 5
                  end
                end
              end
            end
            object TabSheet2: TTabSheet
              Caption = ' F'#243'rmulas '
              ImageIndex = 1
              object DBMemo_01: TDBMemo
                Left = 4
                Top = 0
                Width = 826
                Height = 21
                DataField = 'ICMS_Frm'
                DataSource = DsFisRegCad
                TabOrder = 0
              end
              object DBMemo_02: TDBMemo
                Left = 4
                Top = 20
                Width = 826
                Height = 21
                DataField = 'IPI_Frm'
                DataSource = DsFisRegCad
                TabOrder = 1
              end
              object DBMemo_03: TDBMemo
                Left = 4
                Top = 40
                Width = 826
                Height = 21
                DataField = 'PIS_Frm'
                DataSource = DsFisRegCad
                TabOrder = 2
              end
              object DBMemo_04: TDBMemo
                Left = 4
                Top = 60
                Width = 826
                Height = 21
                DataField = 'COFINS_Frm'
                DataSource = DsFisRegCad
                TabOrder = 3
              end
              object DBMemo_05: TDBMemo
                Left = 4
                Top = 80
                Width = 826
                Height = 21
                DataField = 'IR_Frm'
                DataSource = DsFisRegCad
                TabOrder = 4
              end
              object DBMemo_06: TDBMemo
                Left = 4
                Top = 100
                Width = 826
                Height = 21
                DataField = 'CS_Frm'
                DataSource = DsFisRegCad
                TabOrder = 5
              end
              object DBMemo_07: TDBMemo
                Left = 4
                Top = 120
                Width = 826
                Height = 21
                DataField = 'ISS_Frm'
                DataSource = DsFisRegCad
                TabOrder = 6
              end
              object DBMemo3: TDBMemo
                Left = 4
                Top = 140
                Width = 826
                Height = 21
                DataField = 'II_Frm'
                DataSource = DsFisRegCad
                TabOrder = 7
              end
            end
          end
          object GroupBox9: TGroupBox
            Left = 68
            Top = 1
            Width = 60
            Height = 310
            Align = alLeft
            Caption = ' Al'#237'q. (%): '
            TabOrder = 2
            object DBEdit_01: TDBEdit
              Left = 8
              Top = 24
              Width = 40
              Height = 21
              DataField = 'ICMS_Alq'
              DataSource = DsFisRegCad
              TabOrder = 0
              Visible = False
            end
            object DBEdit_02: TDBEdit
              Left = 8
              Top = 44
              Width = 40
              Height = 21
              DataField = 'IPI_Alq'
              DataSource = DsFisRegCad
              TabOrder = 1
              Visible = False
            end
            object DBEdit_03: TDBEdit
              Left = 8
              Top = 64
              Width = 40
              Height = 21
              DataField = 'PIS_Alq'
              DataSource = DsFisRegCad
              TabOrder = 2
              Visible = False
            end
            object DBEdit_04: TDBEdit
              Left = 8
              Top = 84
              Width = 40
              Height = 21
              DataField = 'COFINS_Alq'
              DataSource = DsFisRegCad
              TabOrder = 3
              Visible = False
            end
            object DBEdit_05: TDBEdit
              Left = 8
              Top = 104
              Width = 40
              Height = 21
              DataField = 'IR_Alq'
              DataSource = DsFisRegCad
              TabOrder = 4
            end
            object DBEdit_06: TDBEdit
              Left = 8
              Top = 124
              Width = 40
              Height = 21
              DataField = 'CS_Alq'
              DataSource = DsFisRegCad
              TabOrder = 5
            end
            object DBEdit_07: TDBEdit
              Left = 8
              Top = 144
              Width = 40
              Height = 21
              DataField = 'ISS_Alq'
              DataSource = DsFisRegCad
              TabOrder = 6
            end
            object DBEdit5: TDBEdit
              Left = 8
              Top = 164
              Width = 40
              Height = 21
              DataField = 'II_Alq'
              DataSource = DsFisRegCad
              TabOrder = 7
              Visible = False
            end
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Aplica'#231#227'o'
        ImageIndex = 1
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 312
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBRadioGroup3: TDBRadioGroup
            Left = 0
            Top = 0
            Width = 1000
            Height = 65
            Align = alTop
            Caption = ' Aplica'#231#227'o: '
            Columns = 6
            DataField = 'Aplicacao'
            DataSource = DsFisRegCad
            Items.Strings = (
              'Nenhuma aplica'#231#227'o'
              'Pedidos'
              'Condicional'
              'Transfer'#234'ncia'
              'Movimento Avulso'
              'Venda Balc'#227'o'
              'Ped. Compra'
              'Consigna'#231#227'o'
              'Produ'#231#227'o'
              'Remessa p/ Indust.'
              'Devolu'#231#227'o'
              'Troca'
              'Servi'#231'o'
              'Retorno Rem. p/ Indust.'
              'Remessa Insumos'
              'Retorno Insumos'
              'Entrada de Mat'#233'ria-prima'
              'Entrada de uso e consumo')
            TabOrder = 0
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9'
              '10'
              '11'
              '12'
              '13'
              '14'
              '15'
              '16'
              '17'
              '18'
              '19'
              '20'
              '21'
              '22'
              '23'
              '24'
              '25'
              '26'
              '27'
              '28'
              '29'
              '30'
              '31'
              '32')
          end
          object DBRadioGroup4: TDBRadioGroup
            Left = 0
            Top = 65
            Width = 1000
            Height = 41
            Align = alTop
            Caption = ' Forma de c'#225'lculo de campos espec'#237'ficos da NF-e: '
            Columns = 4
            DataField = 'TpCalcTrib'
            DataSource = DsFisRegCad
            Items.Strings = (
              'Indefinido'
              'Automatico'
              'Manual'
              'Misto')
            TabOrder = 1
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9')
          end
          object DBGTitle1: TDBGrid
            Left = 0
            Top = 106
            Width = 1000
            Height = 17
            Align = alTop
            BorderStyle = bsNone
            Enabled = False
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Centro de estoque'
                Width = 286
                Visible = True
              end
              item
                Expanded = False
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Empresa'
                Width = 232
                Visible = True
              end
              item
                Expanded = False
                Title.Alignment = taCenter
                Title.Caption = 'Lista de pre'#231'os (compatibilidade)'
                Width = 244
                Visible = True
              end>
          end
          object DBGFisRegMvt: TDBGrid
            Left = 0
            Top = 123
            Width = 1000
            Height = 65
            Align = alClient
            BorderStyle = bsNone
            DataSource = DsFisRegMvt
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnMouseMove = DBGFisRegMvtMouseMove
            Columns = <
              item
                Expanded = False
                FieldName = 'SEQ'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CODUSU_STQCENCAD'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_StqCenCad'
                Title.Caption = 'Descri'#231#227'o'
                Width = 221
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_TIPOMOV'
                Title.Caption = 'Movimento'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_TIPOCALC'
                Title.Caption = 'C'#225'lculo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FILIAL'
                Title.Caption = 'Filial'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_EMP'
                Title.Caption = 'Descri'#231#227'o'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CU_GraCusPrc'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GraCusPrc'
                Title.Caption = 'Descri'#231#227'o'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GraCusApl'
                Title.Caption = 'Aplica'#231#227'o'
                Width = 52
                Visible = True
              end>
          end
          object Panel4: TPanel
            Left = 0
            Top = 188
            Width = 1000
            Height = 37
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 4
            object DBRadioGroup1: TDBRadioGroup
              Left = 0
              Top = 0
              Width = 361
              Height = 37
              Align = alLeft
              Caption = ' Financeiro: '
              Columns = 3
              DataField = 'Financeiro'
              DataSource = DsFisRegCad
              Items.Strings = (
                'Nenhum'
                'A receber'
                'A pagar')
              TabOrder = 0
              Values.Strings = (
                '0'
                '1'
                '2'
                '3')
            end
            object DBRadioGroup2: TDBRadioGroup
              Left = 361
              Top = 0
              Width = 639
              Height = 37
              Align = alClient
              Caption = 'Emiss'#227'o: '
              Columns = 5
              DataField = 'TipoEmiss'
              DataSource = DsFisRegCad
              Items.Strings = (
                'Indefinido'
                'NF-e'#9
                'Romaneio'#9
                'ECF n'#227'o concomit.'
                'ECF concomitante')
              TabOrder = 1
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4')
            end
          end
          object Panel14: TPanel
            Left = 0
            Top = 256
            Width = 1000
            Height = 56
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 5
            object GroupBox6: TGroupBox
              Left = 0
              Top = 0
              Width = 509
              Height = 56
              Align = alLeft
              Caption = 'Observa'#231#245'es:'
              TabOrder = 0
              object DBMemo1: TDBMemo
                Left = 2
                Top = 15
                Width = 505
                Height = 39
                Align = alClient
                DataField = 'Observa'
                DataSource = DsFisRegCad
                TabOrder = 0
              end
            end
            object GroupBox10: TGroupBox
              Left = 509
              Top = 0
              Width = 491
              Height = 56
              Align = alClient
              Caption = 
                'Informa'#231#245'es adicionais de interesse do fisco (na NF-e) m'#225'x. 255 ' +
                'caracteres:'
              TabOrder = 1
              object DBMemo2: TDBMemo
                Left = 2
                Top = 15
                Width = 487
                Height = 39
                Align = alClient
                DataField = 'InfAdFisco'
                DataSource = DsFisRegCad
                TabOrder = 0
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 225
            Width = 1000
            Height = 31
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 6
            object Label3: TLabel
              Left = 8
              Top = 9
              Width = 70
              Height = 13
              Caption = 'Modelo de NF:'
            end
            object Label16: TLabel
              Left = 512
              Top = 9
              Width = 52
              Height = 13
              Caption = 'Agrupador:'
            end
            object DBEdit100: TDBEdit
              Left = 84
              Top = 4
              Width = 52
              Height = 21
              DataField = 'ModeloNF'
              DataSource = DsFisRegCad
              TabOrder = 0
            end
            object DBEdit101: TDBEdit
              Left = 140
              Top = 4
              Width = 366
              Height = 21
              DataField = 'NOMEMODELONF'
              DataSource = DsFisRegCad
              TabOrder = 1
            end
            object DBEdit102: TDBEdit
              Left = 568
              Top = 4
              Width = 52
              Height = 21
              DataField = 'CODUSU_AGRUPADOR'
              DataSource = DsFisRegCad
              TabOrder = 2
            end
            object DBEdit103: TDBEdit
              Left = 624
              Top = 4
              Width = 374
              Height = 21
              DataField = 'NOMEAGRUPADOR'
              DataSource = DsFisRegCad
              TabOrder = 3
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' Entidades Estritas'
        ImageIndex = 2
        object DBGFisRegEnt: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 312
          Align = alClient
          DataSource = DsFisRegEnt
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Entidade'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENT'
              Title.Caption = 'Nome da entidade'
              Width = 295
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observacao'
              Title.Caption = 'Observa'#231#227'o'
              Width = 621
              Visible = True
            end>
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 527
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 68
      Align = alTop
      Caption = ' Regra de Movimenta'#231#227'o: '
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 88
        Top = 20
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 232
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 800
        Top = 21
        Width = 93
        Height = 13
        Caption = 'Tipo de movimento:'
      end
      object Label18: TLabel
        Left = 8
        Top = 44
        Width = 109
        Height = 13
        Caption = 'Natureza da opera'#231#227'o:'
      end
      object SpeedButton12: TSpeedButton
        Left = 976
        Top = 40
        Width = 21
        Height = 21
        Caption = '?'
        OnClick = SpeedButton12Click
      end
      object EdCodigo: TdmkEdit
        Left = 28
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 148
        Top = 16
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdNome: TdmkEdit
        Left = 284
        Top = 16
        Width = 513
        Height = 21
        MaxLength = 50
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTipoMov: TdmkEditCB
        Left = 900
        Top = 16
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TipoMov'
        UpdCampo = 'TipoMov'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTipoMov
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTipoMov: TdmkDBLookupComboBox
        Left = 925
        Top = 16
        Width = 73
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 4
        dmkEditCB = EdTipoMov
        QryCampo = 'TipoMov'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object Edide_natOp: TdmkEdit
        Left = 120
        Top = 40
        Width = 853
        Height = 21
        MaxLength = 60
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ide_natOp'
        UpdCampo = 'ide_natOp'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 463
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel15: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 68
      Width = 1008
      Height = 385
      ActivePage = TabSheet4
      Align = alTop
      TabOrder = 2
      object TabSheet4: TTabSheet
        Caption = ' Geral: '
        object RGAplicacao: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 1000
          Height = 84
          Align = alTop
          Caption = ' Aplica'#231#227'o: '
          Columns = 5
          ItemIndex = 0
          Items.Strings = (
            'Nenhuma aplica'#231#227'o'
            'Pedidos'
            'Condicional'
            'Transfer'#234'ncia'
            'Movimento Avulso'
            'Venda Balc'#227'o'
            'Ped. Compra'
            'Consigna'#231#227'o'
            'Produ'#231#227'o'
            'Remessa p/ Indust.'
            'Devolu'#231#227'o'
            'Troca'
            'Servi'#231'o'
            'Retorno Rem. p/ Indust.'
            'Remessa Insumos'
            'Retorno Insumos'
            'Entrada de Mat'#233'ria-prima'
            'Entrada de uso e consumo')
          TabOrder = 0
          QryCampo = 'Aplicacao'
          UpdCampo = 'Aplicacao'
          UpdType = utYes
          OldValor = 0
        end
        object RGTpCalcTrib: TdmkRadioGroup
          Left = 0
          Top = 84
          Width = 1000
          Height = 41
          Align = alTop
          Caption = 
            ' Forma de c'#225'lculo de campos espec'#237'ficos da NF-e (funcional somen' +
            'te a partir da NFe 3.1): '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Automatico'
            'Manual'
            'Misto')
          TabOrder = 1
          QryCampo = 'TpCalcTrib'
          UpdCampo = 'TpCalcTrib'
          UpdType = utYes
          OldValor = 0
        end
        object Panel6: TPanel
          Left = 0
          Top = 125
          Width = 1000
          Height = 36
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object RGFinanceiro: TdmkRadioGroup
            Left = 0
            Top = 0
            Width = 361
            Height = 36
            Align = alLeft
            Caption = ' Financeiro: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              #192' receber'#9
              #192' pagar'#9)
            TabOrder = 0
            QryCampo = 'Financeiro'
            UpdCampo = 'Financeiro'
            UpdType = utYes
            OldValor = 0
          end
          object RGTipoEmiss: TdmkRadioGroup
            Left = 361
            Top = 0
            Width = 639
            Height = 36
            Align = alClient
            Caption = 'Emiss'#227'o: '
            Columns = 5
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'Nota Fiscal'#9
              'Romaneio'#9
              'ECF n'#227'o concomit.'
              'ECF concomitante')
            TabOrder = 1
            QryCampo = 'TipoEmiss'
            UpdCampo = 'TipoEmiss'
            UpdType = utYes
            OldValor = 0
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 161
          Width = 1000
          Height = 32
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object Label1: TLabel
            Left = 8
            Top = 9
            Width = 70
            Height = 13
            Caption = 'Modelo de NF:'
          end
          object Label2: TLabel
            Left = 512
            Top = 9
            Width = 52
            Height = 13
            Caption = 'Agrupador:'
          end
          object SbAgrupador: TSpeedButton
            Left = 977
            Top = 4
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbAgrupadorClick
          end
          object SBModeloNF: TSpeedButton
            Left = 484
            Top = 4
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBModeloNFClick
          end
          object EdModeloNF: TdmkEditCB
            Left = 84
            Top = 4
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ModeloNF'
            UpdCampo = 'ModeloNF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBModeloNF
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBModeloNF: TdmkDBLookupComboBox
            Left = 140
            Top = 4
            Width = 345
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsImprime
            TabOrder = 1
            dmkEditCB = EdModeloNF
            QryCampo = 'ModeloNF'
            UpdType = utNil
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdAgrupador: TdmkEditCB
            Left = 568
            Top = 4
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBAgrupador
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBAgrupador: TdmkDBLookupComboBox
            Left = 624
            Top = 4
            Width = 353
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsFisAgrCad
            TabOrder = 3
            dmkEditCB = EdAgrupador
            UpdType = utNil
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object Panel18: TPanel
          Left = 0
          Top = 193
          Width = 1000
          Height = 104
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 4
          object StaticText3: TStaticText
            Left = 0
            Top = 0
            Width = 1000
            Height = 17
            Align = alTop
            Alignment = taCenter
            Caption = 'Observa'#231#245'es:'
            TabOrder = 0
          end
          object dmkMemo1: TdmkMemo
            Left = 0
            Top = 17
            Width = 1000
            Height = 31
            Align = alTop
            TabOrder = 1
            QryCampo = 'Observa'
            UpdCampo = 'Observa'
            UpdType = utYes
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 48
            Width = 1000
            Height = 17
            Align = alTop
            Alignment = taCenter
            Caption = 
              'Informa'#231#245'es adicionais de interesse do fisco (na NF-e) m'#225'x. 255 ' +
              'caracteres:'
            TabOrder = 2
          end
          object MeInfAdFisco: TdmkMemo
            Left = 0
            Top = 65
            Width = 1000
            Height = 39
            Align = alClient
            MaxLength = 255
            TabOrder = 3
            QryCampo = 'InfAdFisco'
            UpdCampo = 'InfAdFisco'
            UpdType = utYes
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = ' Tributa'#231#227'o'
        ImageIndex = 1
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 216
          Align = alTop
          Caption = ' Tributa'#231#227'o: '
          TabOrder = 0
          object GroupBox4: TGroupBox
            Left = 2
            Top = 15
            Width = 75
            Height = 199
            Align = alLeft
            Caption = ' Tributo: '
            TabOrder = 0
            object CkICMS_Usa: TdmkCheckBox
              Left = 12
              Top = 28
              Width = 60
              Height = 17
              Caption = 'ICMS'
              TabOrder = 0
              QryCampo = 'ICMS_Usa'
              UpdCampo = 'ICMS_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CkIPI_Usa: TdmkCheckBox
              Left = 12
              Top = 48
              Width = 60
              Height = 17
              Caption = 'IPI'
              TabOrder = 1
              QryCampo = 'IPI_Usa'
              UpdCampo = 'IPI_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CkPIS_Usa: TdmkCheckBox
              Left = 12
              Top = 68
              Width = 60
              Height = 17
              Caption = 'PIS'
              TabOrder = 2
              QryCampo = 'PIS_Usa'
              UpdCampo = 'PIS_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CkCOFINS_Usa: TdmkCheckBox
              Left = 12
              Top = 88
              Width = 60
              Height = 17
              Caption = 'COFINS'
              TabOrder = 3
              QryCampo = 'COFINS_Usa'
              UpdCampo = 'COFINS_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CkIR_Usa: TdmkCheckBox
              Left = 12
              Top = 108
              Width = 60
              Height = 17
              Caption = 'IR'
              TabOrder = 4
              QryCampo = 'IR_Usa'
              UpdCampo = 'IR_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CkCS_Usa: TdmkCheckBox
              Left = 12
              Top = 128
              Width = 60
              Height = 17
              Caption = 'CS'
              TabOrder = 5
              QryCampo = 'CS_Usa'
              UpdCampo = 'CS_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CkISS_Usa: TdmkCheckBox
              Left = 12
              Top = 148
              Width = 60
              Height = 17
              Caption = 'ISS'
              TabOrder = 6
              QryCampo = 'ISS_Usa'
              UpdCampo = 'ISS_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object CkII_Usa: TdmkCheckBox
              Left = 12
              Top = 168
              Width = 60
              Height = 17
              Caption = 'II'
              TabOrder = 7
              QryCampo = 'II_Usa'
              UpdCampo = 'II_Usa'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
          end
          object GroupBox5: TGroupBox
            Left = 77
            Top = 15
            Width = 84
            Height = 199
            Align = alLeft
            Caption = ' Al'#237'quota (%): '
            TabOrder = 1
            object EdICMS_Alq: TdmkEdit
              Left = 8
              Top = 28
              Width = 68
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              Visible = False
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ICMS_Alq'
              UpdCampo = 'ICMS_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdIPI_Alq: TdmkEdit
              Left = 8
              Top = 48
              Width = 68
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 1
              Visible = False
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'IPI_Alq'
              UpdCampo = 'IPI_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPIS_Alq: TdmkEdit
              Left = 8
              Top = 68
              Width = 68
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              Visible = False
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'PIS_Alq'
              UpdCampo = 'PIS_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCOFINS_Alq: TdmkEdit
              Left = 8
              Top = 88
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              Visible = False
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'COFINS_Alq'
              UpdCampo = 'COFINS_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdIR_Alq: TdmkEdit
              Left = 8
              Top = 108
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'IR_Alq'
              UpdCampo = 'IR_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCS_Alq: TdmkEdit
              Left = 8
              Top = 128
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'CS_Alq'
              UpdCampo = 'CS_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdISS_Alq: TdmkEdit
              Left = 8
              Top = 148
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ISS_Alq'
              UpdCampo = 'ISS_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdII_Alq: TdmkEdit
              Left = 8
              Top = 168
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              Visible = False
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '99,99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'II_Alq'
              UpdCampo = 'II_Alq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object PageControl4: TPageControl
            Left = 161
            Top = 15
            Width = 837
            Height = 199
            ActivePage = TabSheet8
            Align = alClient
            TabOrder = 2
            object TabSheet8: TTabSheet
              Caption = ' F'#243'rmulas '
              object Panel10: TPanel
                Left = 0
                Top = 0
                Width = 829
                Height = 171
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object SpeedButton11: TSpeedButton
                  Left = 802
                  Top = 4
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object SpeedButton10: TSpeedButton
                  Left = 802
                  Top = 24
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object SpeedButton9: TSpeedButton
                  Left = 802
                  Top = 44
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object SpeedButton8: TSpeedButton
                  Left = 802
                  Top = 64
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object SpeedButton7: TSpeedButton
                  Left = 802
                  Top = 84
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object SpeedButton5: TSpeedButton
                  Left = 802
                  Top = 104
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object SpeedButton6: TSpeedButton
                  Left = 802
                  Top = 124
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object SpeedButton13: TSpeedButton
                  Left = 802
                  Top = 144
                  Width = 21
                  Height = 21
                  Caption = '...'
                end
                object EdICMS_Frm: TdmkEdit
                  Left = 8
                  Top = 4
                  Width = 792
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'ICMS_Frm'
                  UpdCampo = 'ICMS_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdIPI_Frm: TdmkEdit
                  Left = 8
                  Top = 24
                  Width = 792
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'IPI_Frm'
                  UpdCampo = 'IPI_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdPIS_Frm: TdmkEdit
                  Left = 8
                  Top = 44
                  Width = 792
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'PIS_Frm'
                  UpdCampo = 'PIS_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCOFINS_Frm: TdmkEdit
                  Left = 8
                  Top = 64
                  Width = 792
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'COFINS_Frm'
                  UpdCampo = 'COFINS_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdIR_Frm: TdmkEdit
                  Left = 8
                  Top = 84
                  Width = 792
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'IR_Frm'
                  UpdCampo = 'IR_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCS_Frm: TdmkEdit
                  Left = 8
                  Top = 104
                  Width = 792
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'CS_Frm'
                  UpdCampo = 'CS_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdISS_Frm: TdmkEdit
                  Left = 8
                  Top = 124
                  Width = 792
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'ISS_Frm'
                  UpdCampo = 'ISS_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdII_Frm: TdmkEdit
                  Left = 8
                  Top = 144
                  Width = 792
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'II_Frm'
                  UpdCampo = 'II_Frm'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' CTe entrada'
        ImageIndex = 2
        object Panel20: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 357
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label66: TLabel
            Left = 12
            Top = 5
            Width = 31
            Height = 13
            Caption = 'CFOP:'
          end
          object SbFrtInnCFOP: TSpeedButton
            Left = 970
            Top = 18
            Width = 25
            Height = 25
            Caption = '...'
          end
          object Label476: TLabel
            Left = 12
            Top = 48
            Width = 222
            Height = 13
            Caption = 'ICMS (retorno: RedBC, BC , % e valor):'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label25: TLabel
            Left = 13
            Top = 88
            Width = 197
            Height = 13
            Caption = 'PIS (retorno: CST, BC , % e valor):'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label480: TLabel
            Left = 12
            Top = 128
            Width = 226
            Height = 13
            Caption = 'COFINS  (retorno: CST, BC , % e valor):'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label26: TLabel
            Left = 12
            Top = 168
            Width = 205
            Height = 13
            Caption = 'Indicador da Natureza do Frete Contratado:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label27: TLabel
            Left = 664
            Top = 48
            Width = 82
            Height = 13
            Caption = 'TES % do tributo:'
          end
          object Label28: TLabel
            Left = 832
            Top = 48
            Width = 144
            Height = 13
            Caption = 'TES Valor da base de c'#225'lculo:'
          end
          object Label29: TLabel
            Left = 12
            Top = 208
            Width = 182
            Height = 13
            Caption = 'C'#243'digo da Base de C'#225'lculo do Cr'#233'dito:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object EdFrtInnCFOP: TdmkEditCB
            Left = 12
            Top = 20
            Width = 55
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FrtInnCFOP'
            UpdCampo = 'FrtInnCFOP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFrtInnCFOP
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFrtInnCFOP: TdmkDBLookupComboBox
            Left = 69
            Top = 20
            Width = 896
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCFOP_Frete
            TabOrder = 1
            TabStop = False
            dmkEditCB = EdFrtInnCFOP
            QryCampo = 'FrtInnCFOP'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdFrtInnICMS_CST: TdmkEdit
            Left = 12
            Top = 64
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            QryName = 'QrNFeItsN'
            QryCampo = 'FrtInnICMS_CST'
            UpdCampo = 'FrtInnICMS_CST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFrtInnICMS_CSTChange
            OnKeyDown = EdFrtInnICMS_CSTKeyDown
          end
          object EdFrtInnPIS_CST: TdmkEdit
            Left = 12
            Top = 104
            Width = 45
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 2
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsQ'
            QryCampo = 'FrtInnPIS_CST'
            UpdCampo = 'FrtInnPIS_CST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdFrtInnPIS_CSTChange
            OnKeyDown = EdFrtInnPIS_CSTKeyDown
          end
          object EdFrtInnCOFINS_CST: TdmkEdit
            Left = 12
            Top = 144
            Width = 45
            Height = 21
            TabOrder = 16
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 2
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsS'
            QryCampo = 'FrtInnCOFINS_CST'
            UpdCampo = 'FrtInnCOFINS_CST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdFrtInnCOFINS_CSTChange
            OnKeyDown = EdFrtInnCOFINS_CSTKeyDown
          end
          object EdFrtInnIND_NAT_FRT: TdmkEdit
            Left = 12
            Top = 184
            Width = 45
            Height = 21
            TabOrder = 23
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 2
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsS'
            QryCampo = 'FrtInnIND_NAT_FRT'
            UpdCampo = 'FrtInnIND_NAT_FRT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdFrtInnIND_NAT_FRTChange
            OnKeyDown = EdFrtInnIND_NAT_FRTKeyDown
          end
          object EdFrtInnNAT_BC_CRED: TdmkEdit
            Left = 12
            Top = 224
            Width = 45
            Height = 21
            TabOrder = 25
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 2
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '99'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryName = 'QrNFeItsS'
            QryCampo = 'FrtInnNAT_BC_CRED'
            UpdCampo = 'FrtInnNAT_BC_CRED'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdFrtInnNAT_BC_CREDChange
            OnKeyDown = EdFrtInnNAT_BC_CREDKeyDown
          end
          object EdFrtInnIND_NAT_FRT_TXT: TdmkEdit
            Left = 58
            Top = 184
            Width = 939
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 24
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtInnNAT_BC_CRED_TXT: TdmkEdit
            Left = 58
            Top = 224
            Width = 939
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 26
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtInnALIQ_ICMS: TdmkEdit
            Left = 60
            Top = 64
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'FrtInnALIQ_ICMS'
            UpdCampo = 'FrtInnALIQ_ICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFrtInnICMS_CST_TXT: TdmkEdit
            Left = 101
            Top = 64
            Width = 560
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtInnALIQ_PIS: TdmkEdit
            Left = 60
            Top = 104
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'FrtInnALIQ_PIS'
            UpdCampo = 'FrtInnALIQ_PIS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFrtInnPIS_CST_TXT: TdmkEdit
            Left = 102
            Top = 104
            Width = 560
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtInnALIQ_COFINS: TdmkEdit
            Left = 60
            Top = 144
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 17
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'FrtInnALIQ_COFINS'
            UpdCampo = 'FrtInnALIQ_COFINS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFrtInnCOFINS_CST_TXT: TdmkEdit
            Left = 102
            Top = 144
            Width = 560
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 18
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtTES_ICMS: TdmkEdit
            Left = 664
            Top = 64
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '2'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrNFeItsN'
            QryCampo = 'FrtTES_ICMS'
            UpdCampo = 'FrtTES_ICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFrtTES_ICMSChange
            OnKeyDown = EdFrtTES_ICMSKeyDown
          end
          object EdFrtTES_ICMS_TXT: TdmkEdit
            Left = 689
            Top = 64
            Width = 140
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtTES_PIS: TdmkEdit
            Left = 664
            Top = 104
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '2'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrNFeItsN'
            QryCampo = 'FrtTES_PIS'
            UpdCampo = 'FrtTES_PIS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFrtTES_PISChange
            OnKeyDown = EdFrtTES_PISKeyDown
          end
          object EdFrtTES_PIS_TXT: TdmkEdit
            Left = 689
            Top = 104
            Width = 140
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtTES_COFINS: TdmkEdit
            Left = 664
            Top = 144
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 19
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '2'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrNFeItsN'
            QryCampo = 'FrtTES_COFINS'
            UpdCampo = 'FrtTES_COFINS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFrtTES_COFINSChange
            OnKeyDown = EdFrtTES_COFINSKeyDown
          end
          object EdFrtTES_COFINS_TXT: TdmkEdit
            Left = 689
            Top = 144
            Width = 140
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 20
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtTES_BC_ICMS: TdmkEdit
            Left = 832
            Top = 64
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '2'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrNFeItsN'
            QryCampo = 'FrtTES_BC_ICMS'
            UpdCampo = 'FrtTES_BC_ICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFrtTES_BC_ICMSChange
            OnKeyDown = EdFrtTES_BC_ICMSKeyDown
          end
          object EdFrtTES_BC_ICMS_TXT: TdmkEdit
            Left = 857
            Top = 64
            Width = 140
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtTES_BC_PIS_TXT: TdmkEdit
            Left = 857
            Top = 104
            Width = 140
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 15
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFrtTES_BC_PIS: TdmkEdit
            Left = 832
            Top = 104
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 14
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '2'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrNFeItsN'
            QryCampo = 'FrtTES_BC_PIS'
            UpdCampo = 'FrtTES_BC_PIS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFrtTES_BC_PISChange
            OnKeyDown = EdFrtTES_BC_PISKeyDown
          end
          object EdFrtTES_BC_COFINS: TdmkEdit
            Left = 832
            Top = 144
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 21
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '2'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FrtTES_BC_COFINS'
            UpdCampo = 'FrtTES_BC_COFINS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFrtTES_BC_COFINSChange
            OnKeyDown = EdFrtTES_BC_COFINSKeyDown
          end
          object EdFrtTES_BC_COFINS_TXT: TdmkEdit
            Left = 857
            Top = 144
            Width = 140
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 22
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object TabSheet10: TTabSheet
        Caption = 'NFe de entrada'
        ImageIndex = 3
        object Panel21: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 357
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGOriTES: TdmkRadioGroup
            Left = 0
            Top = 0
            Width = 392
            Height = 53
            Caption = ' TES: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o definido'
              'Livro fiscal: Tributa'
              'Livro fiscal: Outros')
            TabOrder = 0
            OnClick = RGOriTESClick
            QryCampo = 'OriTes'
            UpdCampo = 'OriTes'
            UpdType = utYes
            OldValor = 0
          end
          object CkEFD_II_C195: TdmkCheckBox
            Left = 4
            Top = 68
            Width = 141
            Height = 17
            Caption = 'Gera EFD II C195 / D195'
            TabOrder = 1
            QryCampo = 'EFD_II_C195'
            UpdCampo = 'EFD_II_C195'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
        end
      end
      object TabSheet11: TTabSheet
        Caption = ' Fiscal / Cont'#225'bil'
        ImageIndex = 4
        object Panel19: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 357
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 56
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object SbGenCtbC: TSpeedButton
              Left = 772
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbGenCtbCClick
            end
            object SbGenCtbD: TSpeedButton
              Left = 772
              Top = 8
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbGenCtbDClick
            end
            object Label24: TLabel
              Left = 4
              Top = 36
              Width = 142
              Height = 13
              Caption = 'Reduz. plano contas - cr'#233'dito:'
            end
            object Label181: TLabel
              Left = 4
              Top = 12
              Width = 139
              Height = 13
              Caption = 'Reduz. plano contas - d'#233'bito:'
            end
            object CBGenCtbD: TdmkDBLookupComboBox
              Left = 316
              Top = 8
              Width = 454
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPlaAllCad_D
              TabOrder = 2
              dmkEditCB = EdGenCtbD
              QryCampo = 'GenCtbD'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBGenCtbC: TdmkDBLookupComboBox
              Left = 316
              Top = 32
              Width = 454
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPlaAllCad_C
              TabOrder = 5
              dmkEditCB = EdGenCtbC
              QryCampo = 'GenCtbC'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdGenCtbC: TdmkEditCB
              Left = 152
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'GenCtbC'
              UpdCampo = 'GenCtbC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdGenCtbCRedefinido
              DBLookupComboBox = CBGenCtbC
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdGenCtbD: TdmkEditCB
              Left = 152
              Top = 8
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'GenCtbD'
              UpdCampo = 'GenCtbD'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdGenCtbDRedefinido
              DBLookupComboBox = CBGenCtbD
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object DBEdOrdensD: TDBEdit
              Left = 212
              Top = 8
              Width = 100
              Height = 21
              TabStop = False
              DataField = 'Ordens'
              DataSource = DsPlaAllCad_D
              TabOrder = 1
            end
            object DBEdOrdensC: TDBEdit
              Left = 212
              Top = 32
              Width = 100
              Height = 21
              TabStop = False
              DataField = 'Ordens'
              DataSource = DsPlaAllCad_C
              TabOrder = 4
            end
          end
          object Panel23: TPanel
            Left = 0
            Top = 56
            Width = 1000
            Height = 56
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object RGInfoEFD_PisCofins: TdmkRadioGroup
              Left = 8
              Top = 8
              Width = 237
              Height = 45
              Caption = ' Informar documento no EFD PIS/COFINS:'
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                '??'
                'Sim'
                'N'#227'o')
              TabOrder = 0
              QryCampo = 'InfoEFD_PisCofins'
              UpdCampo = 'InfoEFD_PisCofins'
              UpdType = utYes
              OldValor = 0
            end
            object RGIntegraDRE: TdmkRadioGroup
              Left = 452
              Top = 8
              Width = 541
              Height = 45
              Caption = ' Integra o faturamento no DRE: '
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                'Indefinido'
                'N'#227'o integra'
                'Receita (Venda)'
                'Devolu'#231#227'o de venda')
              TabOrder = 1
              QryCampo = 'IntegraDRE'
              UpdCampo = 'IntegraDRE'
              UpdType = utYes
              OldValor = 0
            end
            object RGCreDeb_PisCofins: TdmkRadioGroup
              Left = 248
              Top = 8
              Width = 201
              Height = 45
              Caption = 'Conta a Informar no EFD PIS/COFINS:'
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                '??'
                'D'#233'bito'
                'Cr'#233'dito')
              TabOrder = 2
              QryCampo = 'CreDeb_PisCofins'
              UpdCampo = 'CreDeb_PisCofins'
              UpdType = utYes
              OldValor = 0
            end
          end
          object Panel24: TPanel
            Left = 0
            Top = 112
            Width = 1000
            Height = 245
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object Label20: TLabel
              Left = 4
              Top = 12
              Width = 133
              Height = 13
              Caption = 'Tipo de movimento cont'#225'bil:'
              Enabled = False
              Visible = False
            end
            object Label21: TLabel
              Left = 8
              Top = 32
              Width = 125
              Height = 13
              Caption = 'Conta do plano de contas:'
              Enabled = False
              Visible = False
            end
            object SbCtbCadMoF: TSpeedButton
              Left = 728
              Top = 8
              Width = 21
              Height = 21
              Caption = '...'
              Enabled = False
              Visible = False
              OnClick = SbCtbCadMoFClick
            end
            object CtbPlaCta: TSpeedButton
              Left = 728
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              Enabled = False
              Visible = False
              OnClick = CtbPlaCtaClick
            end
            object EdCtbCadMoF: TdmkEditCB
              Left = 148
              Top = 4
              Width = 56
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              Visible = False
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CtbCadMoF'
              UpdCampo = 'CtbCadMoF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtbCadMoF
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdCtbPlaCta: TdmkEditCB
              Left = 148
              Top = 28
              Width = 56
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              Visible = False
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CtbPlaCta'
              UpdCampo = 'CtbPlaCta'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtbPlaCta
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCtbPlaCta: TdmkDBLookupComboBox
              Left = 208
              Top = 28
              Width = 513
              Height = 21
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'Nome'
              TabOrder = 3
              Visible = False
              dmkEditCB = EdCtbPlaCta
              QryCampo = 'CtbPlaCta'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBCtbCadMoF: TdmkDBLookupComboBox
              Left = 208
              Top = 4
              Width = 513
              Height = 21
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCtbCadMoF
              TabOrder = 1
              Visible = False
              dmkEditCB = EdCtbCadMoF
              QryCampo = 'CtbCadMoF'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 261
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtCorrige: TBitBtn
        Tag = 1000626
        Left = 212
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtCorrigeClick
      end
    end
    object GB_M: TGroupBox
      Left = 261
      Top = 0
      Width = 205
      Height = 52
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 182
        Height = 32
        Caption = 'Regras Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 182
        Height = 32
        Caption = 'Regras Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 182
        Height = 32
        Caption = 'Regras Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 466
      Top = 0
      Width = 494
      Height = 52
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel13: TPanel
        Left = 2
        Top = 15
        Width = 490
        Height = 35
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 1
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 0
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 0
          Top = 18
          Width = 490
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 300
    Top = 112
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFisRegCadBeforeOpen
    AfterOpen = QrFisRegCadAfterOpen
    BeforeClose = QrFisRegCadBeforeClose
    AfterScroll = QrFisRegCadAfterScroll
    OnCalcFields = QrFisRegCadCalcFields
    SQL.Strings = (
      'SELECT frc.*, '
      'mnf.Nome NOMEMODELONF,'
      'cta.Nome NO_CtbPlaCta, ccm.Nome NO_CtbCadMoF, '
      'ctd.Nome NO_GenCtbD, ctc.Nome NO_GenCtbC, '
      'ctd.Ordens ORD_GenCtbD, ctc.Ordens ORD_GenCtbC, '
      'fac.Nome NOMEAGRUPADOR, fac.CodUsu CODUSU_AGRUPADOR'
      'FROM fisregcad frc'
      'LEFT JOIN imprime   mnf ON mnf.Codigo=frc.ModeloNF'
      'LEFT JOIN fisagrcad fac ON fac.Codigo=frc.Agrupador'
      'LEFT JOIN ctbcadmof ccm ON ccm.Codigo=frc.CtbCadMoF'
      'LEFT JOIN contas    cta ON cta.Codigo=frc.CtbPlaCta'
      'LEFT JOIN plaallcad ctd ON ctd.Codigo = frc.GenCtbD'
      'LEFT JOIN plaallcad ctc ON ctc.Codigo = frc.GenCtbC')
    Left = 299
    Top = 64
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fisregcad.Codigo'
      Required = True
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fisregcad.CodUsu'
      Required = True
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'fisregcad.Nome'
      Required = True
      Size = 50
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Origin = 'fisregcad.TipoMov'
      Required = True
    end
    object QrFisRegCadICMS_Usa: TSmallintField
      FieldName = 'ICMS_Usa'
      Origin = 'fisregcad.ICMS_Usa'
      Required = True
    end
    object QrFisRegCadICMS_Frm: TWideMemoField
      FieldName = 'ICMS_Frm'
      Origin = 'fisregcad.ICMS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadIPI_Usa: TSmallintField
      FieldName = 'IPI_Usa'
      Origin = 'fisregcad.IPI_Usa'
      Required = True
    end
    object QrFisRegCadIPI_Frm: TWideMemoField
      FieldName = 'IPI_Frm'
      Origin = 'fisregcad.IPI_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadPIS_Usa: TSmallintField
      FieldName = 'PIS_Usa'
      Origin = 'fisregcad.PIS_Usa'
      Required = True
    end
    object QrFisRegCadPIS_Frm: TWideMemoField
      FieldName = 'PIS_Frm'
      Origin = 'fisregcad.PIS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadCOFINS_Usa: TSmallintField
      FieldName = 'COFINS_Usa'
      Origin = 'fisregcad.COFINS_Usa'
      Required = True
    end
    object QrFisRegCadCOFINS_Frm: TWideMemoField
      FieldName = 'COFINS_Frm'
      Origin = 'fisregcad.COFINS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadIR_Usa: TSmallintField
      FieldName = 'IR_Usa'
      Origin = 'fisregcad.IR_Usa'
      Required = True
    end
    object QrFisRegCadIR_Alq: TFloatField
      FieldName = 'IR_Alq'
      Origin = 'fisregcad.IR_Alq'
      Required = True
      DisplayFormat = '#,##0.00'
    end
    object QrFisRegCadIR_Frm: TWideMemoField
      FieldName = 'IR_Frm'
      Origin = 'fisregcad.IR_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadCS_Usa: TSmallintField
      FieldName = 'CS_Usa'
      Origin = 'fisregcad.CS_Usa'
      Required = True
    end
    object QrFisRegCadCS_Alq: TFloatField
      FieldName = 'CS_Alq'
      Origin = 'fisregcad.CS_Alq'
      Required = True
      DisplayFormat = '#,##0.00'
    end
    object QrFisRegCadCS_Frm: TWideMemoField
      FieldName = 'CS_Frm'
      Origin = 'fisregcad.CS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadISS_Usa: TSmallintField
      FieldName = 'ISS_Usa'
      Origin = 'fisregcad.ISS_Usa'
      Required = True
    end
    object QrFisRegCadISS_Alq: TFloatField
      FieldName = 'ISS_Alq'
      Origin = 'fisregcad.ISS_Alq'
      Required = True
      DisplayFormat = '#,##0.00'
    end
    object QrFisRegCadISS_Frm: TWideMemoField
      FieldName = 'ISS_Frm'
      Origin = 'fisregcad.ISS_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadAplicacao: TSmallintField
      FieldName = 'Aplicacao'
      Origin = 'fisregcad.Aplicacao'
      Required = True
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
      Origin = 'fisregcad.Financeiro'
      Required = True
    end
    object QrFisRegCadTipoEmiss: TSmallintField
      FieldName = 'TipoEmiss'
      Origin = 'fisregcad.TipoEmiss'
      Required = True
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Origin = 'fisregcad.ModeloNF'
      Required = True
    end
    object QrFisRegCadAgrupador: TIntegerField
      FieldName = 'Agrupador'
      Origin = 'fisregcad.Agrupador'
      Required = True
    end
    object QrFisRegCadObserva: TWideStringField
      FieldName = 'Observa'
      Origin = 'fisregcad.Observa'
      Size = 255
    end
    object QrFisRegCadNOMETIPOMOV: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOMOV'
      Size = 30
      Calculated = True
    end
    object QrFisRegCadNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Origin = 'imprime.Nome'
      Size = 100
    end
    object QrFisRegCadNOMEAGRUPADOR: TWideStringField
      FieldName = 'NOMEAGRUPADOR'
      Origin = 'fisagrcad.Nome'
      Size = 50
    end
    object QrFisRegCadCODUSU_AGRUPADOR: TIntegerField
      FieldName = 'CODUSU_AGRUPADOR'
      Origin = 'fisagrcad.CodUsu'
      Required = True
    end
    object QrFisRegCadInfAdFisco: TWideStringField
      FieldName = 'InfAdFisco'
      Origin = 'fisregcad.infAdFisco'
      Size = 255
    end
    object QrFisRegCadICMS_Alq: TFloatField
      FieldName = 'ICMS_Alq'
      Origin = 'fisregcad.ICMS_Alq'
      DisplayFormat = '#,##0.00'
    end
    object QrFisRegCadIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
      Origin = 'fisregcad.IPI_Alq'
      DisplayFormat = '#,##0.00'
    end
    object QrFisRegCadPIS_Alq: TFloatField
      FieldName = 'PIS_Alq'
      Origin = 'fisregcad.PIS_Alq'
      DisplayFormat = '#,##0.00'
    end
    object QrFisRegCadCOFINS_Alq: TFloatField
      FieldName = 'COFINS_Alq'
      Origin = 'fisregcad.COFINS_Alq'
      DisplayFormat = '#,##0.00'
    end
    object QrFisRegCadide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Origin = 'fisregcad.ide_natOp'
      Size = 60
    end
    object QrFisRegCadII_Usa: TSmallintField
      FieldName = 'II_Usa'
      Origin = 'fisregcad.II_Usa'
    end
    object QrFisRegCadII_Alq: TFloatField
      FieldName = 'II_Alq'
      Origin = 'fisregcad.II_Alq'
    end
    object QrFisRegCadII_Frm: TWideMemoField
      FieldName = 'II_Frm'
      Origin = 'fisregcad.II_Frm'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFisRegCadTpCalcTrib: TSmallintField
      FieldName = 'TpCalcTrib'
    end
    object QrFisRegCadCtbCadMoF: TIntegerField
      FieldName = 'CtbCadMoF'
    end
    object QrFisRegCadNO_CtbCadMoF: TWideStringField
      FieldName = 'NO_CtbCadMoF'
      Size = 60
    end
    object QrFisRegCadCtbPlaCta: TIntegerField
      FieldName = 'CtbPlaCta'
    end
    object QrFisRegCadNO_CtbPlaCta: TWideStringField
      FieldName = 'NO_CtbPlaCta'
      Size = 60
    end
    object QrFisRegCadGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrFisRegCadGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrFisRegCadNO_GenCtbD: TWideStringField
      FieldName = 'NO_GenCtbD'
      Size = 60
    end
    object QrFisRegCadNO_GenCtbC: TWideStringField
      FieldName = 'NO_GenCtbC'
      Size = 60
    end
    object QrFisRegCadInfoEFD_PisCofins: TSmallintField
      FieldName = 'InfoEFD_PisCofins'
    end
    object QrFisRegCadCreDeb_PisCofins: TSmallintField
      FieldName = 'CreDeb_PisCofins'
    end
    object QrFisRegCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrFisRegCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFisRegCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFisRegCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrFisRegCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrFisRegCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFisRegCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFisRegCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFisRegCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFisRegCadFrtInnCFOP: TIntegerField
      FieldName = 'FrtInnCFOP'
      Required = True
    end
    object QrFisRegCadFrtInnICMS_CST: TIntegerField
      FieldName = 'FrtInnICMS_CST'
      Required = True
    end
    object QrFisRegCadFrtInnALIQ_ICMS: TFloatField
      FieldName = 'FrtInnALIQ_ICMS'
      Required = True
    end
    object QrFisRegCadFrtTES_ICMS: TSmallintField
      FieldName = 'FrtTES_ICMS'
      Required = True
    end
    object QrFisRegCadFrtTES_BC_ICMS: TSmallintField
      FieldName = 'FrtTES_BC_ICMS'
      Required = True
    end
    object QrFisRegCadFrtInnALIQ_PIS: TFloatField
      FieldName = 'FrtInnALIQ_PIS'
      Required = True
    end
    object QrFisRegCadFrtTES_BC_PIS: TSmallintField
      FieldName = 'FrtTES_BC_PIS'
      Required = True
    end
    object QrFisRegCadFrtInnALIQ_COFINS: TFloatField
      FieldName = 'FrtInnALIQ_COFINS'
      Required = True
    end
    object QrFisRegCadFrtTES_BC_COFINS: TSmallintField
      FieldName = 'FrtTES_BC_COFINS'
      Required = True
    end
    object QrFisRegCadFrtInnIND_NAT_FRT: TWideStringField
      FieldName = 'FrtInnIND_NAT_FRT'
      Size = 1
    end
    object QrFisRegCadFrtInnNAT_BC_CRED: TWideStringField
      FieldName = 'FrtInnNAT_BC_CRED'
      Size = 2
    end
    object QrFisRegCadFrtInnPIS_CST: TWideStringField
      FieldName = 'FrtInnPIS_CST'
      Size = 2
    end
    object QrFisRegCadFrtInnCOFINS_CST: TWideStringField
      FieldName = 'FrtInnCOFINS_CST'
      Size = 2
    end
    object QrFisRegCadFrtTES_COFINS: TSmallintField
      FieldName = 'FrtTES_COFINS'
      Required = True
    end
    object QrFisRegCadFrtTES_PIS: TSmallintField
      FieldName = 'FrtTES_PIS'
      Required = True
    end
    object QrFisRegCadOriTES: TIntegerField
      FieldName = 'OriTES'
      Required = True
    end
    object QrFisRegCadEFD_II_C195: TSmallintField
      FieldName = 'EFD_II_C195'
    end
    object QrFisRegCadIntegraDRE: TSmallintField
      FieldName = 'IntegraDRE'
    end
    object QrFisRegCadORD_GenCtbD: TWideStringField
      FieldName = 'ORD_GenCtbD'
      Size = 255
    end
    object QrFisRegCadORD_GenCtbC: TWideStringField
      FieldName = 'ORD_GenCtbC'
      Size = 255
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 184
    Top = 392
  end
  object PMRegra: TPopupMenu
    OnPopup = PMRegraPopup
    Left = 560
    Top = 8
    object Incluinovaregra1: TMenuItem
      Caption = '&Inclui nova regra'
      OnClick = Incluinovaregra1Click
    end
    object Alteraregraatual1: TMenuItem
      Caption = '&Altera regra atual'
      OnClick = Alteraregraatual1Click
    end
    object Excluiregraatual1: TMenuItem
      Caption = '&Exclui regra atual'
      Enabled = False
    end
  end
  object DsFisRegMvt: TDataSource
    DataSet = QrFisRegMvt
    Left = 532
    Top = 8
  end
  object QrFisRegMvt: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFisRegMvtBeforeClose
    AfterScroll = QrFisRegMvtAfterScroll
    OnCalcFields = QrFisRegMvtCalcFields
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOME_EMP,'
      'emp.FILIAL, gcp.CodUsu CU_GraCusPrc, gcp.Nome NO_GraCusPrc, '
      'ELT(frm.GraCusApl+1, "Custo", "Pre'#231'o", "? ? ?") NO_GraCusApl,'
      'scc.CodUsu CODUSU_STQCENCAD, '
      'scc.Nome NOME_StqCenCad, '
      
        'ELT(frm.TipoCalc+1, "NULO", "ADICIONA", "SUBTRAI") NOME_TIPOCALC' +
        ', '
      'ELT(frm.TipoMov+1, "ENTRADA", "SA'#205'DA") NOME_TIPOMOV, frm.*'
      'FROM fisregmvt frm'
      'LEFT JOIN entidades emp ON emp.Codigo=frm.Empresa'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=frm.GraCusPrc'
      'LEFT JOIN stqcencad scc ON scc.Codigo=frm. StqCenCad'
      'WHERE frm.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 504
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFisRegMvtCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegMvtControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFisRegMvtEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrFisRegMvtStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Required = True
    end
    object QrFisRegMvtTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Required = True
    end
    object QrFisRegMvtTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrFisRegMvtNOME_EMP: TWideStringField
      FieldName = 'NOME_EMP'
      Size = 100
    end
    object QrFisRegMvtNOME_StqCenCad: TWideStringField
      FieldName = 'NOME_StqCenCad'
      Size = 50
    end
    object QrFisRegMvtNOME_TIPOCALC: TWideStringField
      FieldName = 'NOME_TIPOCALC'
      Size = 8
    end
    object QrFisRegMvtNOME_TIPOMOV: TWideStringField
      FieldName = 'NOME_TIPOMOV'
      Size = 7
    end
    object QrFisRegMvtSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrFisRegMvtFILIAL: TIntegerField
      FieldName = 'FILIAL'
    end
    object QrFisRegMvtCODUSU_STQCENCAD: TIntegerField
      FieldName = 'CODUSU_STQCENCAD'
      Required = True
    end
    object QrFisRegMvtCU_GraCusPrc: TIntegerField
      FieldName = 'CU_GraCusPrc'
      Required = True
    end
    object QrFisRegMvtNO_GraCusPrc: TWideStringField
      FieldName = 'NO_GraCusPrc'
      Size = 30
    end
    object QrFisRegMvtGraCusApl: TSmallintField
      FieldName = 'GraCusApl'
      Required = True
    end
    object QrFisRegMvtGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Required = True
    end
    object QrFisRegMvtNO_GraCusApl: TWideStringField
      FieldName = 'NO_GraCusApl'
      Size = 5
    end
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM imprime'
      'ORDER BY Nome')
    Left = 648
    Top = 8
    object QrImprimeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImprimeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 676
    Top = 8
  end
  object QrFisAgrCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM fisagrcad'
      'ORDER BY Nome')
    Left = 708
    Top = 7
    object QrFisAgrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisAgrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisAgrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdAgrupador
    Panel = PainelEdita
    QryCampo = 'Agrupador'
    UpdCampo = 'Agrupador'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 764
    Top = 8
  end
  object DsFisAgrCad: TDataSource
    DataSet = QrFisAgrCad
    Left = 736
    Top = 8
  end
  object PMMovimentacao: TPopupMenu
    OnPopup = PMMovimentacaoPopup
    Left = 588
    Top = 8
    object Incluiitemdemovimentao1: TMenuItem
      Caption = '&Inclui item de movimenta'#231#227'o'
      OnClick = Incluiitemdemovimentao1Click
    end
    object Alteraitemdemovimentaoatual1: TMenuItem
      Caption = '&Altera item de movimenta'#231#227'o atual'
      OnClick = Alteraitemdemovimentaoatual1Click
    end
    object Excluiitemnsdemovimentao1: TMenuItem
      Caption = '&Exclui item(ns) de movimenta'#231#227'o'
      OnClick = Excluiitemnsdemovimentao1Click
    end
  end
  object QrFisRegCFOP: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFisRegCFOPBeforeClose
    AfterScroll = QrFisRegCFOPAfterScroll
    SQL.Strings = (
      'SELECT fgc.*, cfp.Nome NO_CFOP'
      'FROM fisregcfop fgc'
      'LEFT JOIN cfop2003 cfp ON cfp.Codigo=fgc.CFOP'
      ''
      'WHERE fgc.Codigo=:P0')
    Left = 388
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFisRegCFOPCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCFOPInterno: TSmallintField
      FieldName = 'Interno'
      Required = True
      MaxValue = 1
    end
    object QrFisRegCFOPContribui: TSmallintField
      FieldName = 'Contribui'
      Required = True
      MaxValue = 1
    end
    object QrFisRegCFOPProprio: TSmallintField
      FieldName = 'Proprio'
      Required = True
      MaxValue = 1
    end
    object QrFisRegCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Required = True
      Size = 5
    end
    object QrFisRegCFOPNO_CFOP: TWideStringField
      FieldName = 'NO_CFOP'
      Size = 255
    end
    object QrFisRegCFOPOrdCFOPGer: TIntegerField
      FieldName = 'OrdCFOPGer'
      Required = True
    end
    object QrFisRegCFOPServico: TSmallintField
      FieldName = 'Servico'
      MaxValue = 1
    end
    object QrFisRegCFOPSubsTrib: TSmallintField
      FieldName = 'SubsTrib'
      MaxValue = 1
    end
    object QrFisRegCFOPNO_OriCFOP: TWideStringField
      FieldName = 'NO_OriCFOP'
      Size = 255
    end
    object QrFisRegCFOPOriCFOP: TWideStringField
      FieldName = 'OriCFOP'
      Size = 5
    end
    object QrFisRegCFOPGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrFisRegCFOPGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrFisRegCFOPCtbCadMoF: TIntegerField
      FieldName = 'CtbCadMoF'
      Required = True
    end
    object QrFisRegCFOPCtbPlaCta: TIntegerField
      FieldName = 'CtbPlaCta'
      Required = True
    end
    object QrFisRegCFOPLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrFisRegCFOPDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFisRegCFOPDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFisRegCFOPUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrFisRegCFOPUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrFisRegCFOPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFisRegCFOPAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFisRegCFOPAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFisRegCFOPAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFisRegCFOPControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsFisRegCFOP: TDataSource
    DataSet = QrFisRegCFOP
    Left = 388
    Top = 108
  end
  object PMCFOP: TPopupMenu
    OnPopup = PMCFOPPopup
    Left = 616
    Top = 8
    object IncluinovoCFOP1: TMenuItem
      Caption = '&Inclui novo CFOP'
      OnClick = IncluinovoCFOP1Click
    end
    object AlteraCFOPatual1: TMenuItem
      Caption = '&Altera CFOP atual'
      OnClick = AlteraCFOPatual1Click
    end
    object RetiraCFOPAtual1: TMenuItem
      Caption = '&Retira CFOP(s)'
      OnClick = RetiraCFOPAtual1Click
    end
  end
  object QrFisRegUFs: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFisRegUFsAfterOpen
    BeforeClose = QrFisRegUFsBeforeClose
    AfterScroll = QrFisRegUFsAfterScroll
    OnCalcFields = QrFisRegUFsCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM Fisregufs'
      'WHERE Codigo=:P0'
      'AND Interno=:P1'
      'AND Contribui=:P2'
      'AND Proprio=:P3'
      'ORDER BY UFEmit, UFDest'
      ' ')
    Left = 464
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrFisRegUFsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fisregufs.Codigo'
    end
    object QrFisRegUFsInterno: TSmallintField
      FieldName = 'Interno'
      Origin = 'fisregufs.Interno'
    end
    object QrFisRegUFsContribui: TSmallintField
      FieldName = 'Contribui'
      Origin = 'fisregufs.Contribui'
    end
    object QrFisRegUFsProprio: TSmallintField
      FieldName = 'Proprio'
      Origin = 'fisregufs.Proprio'
    end
    object QrFisRegUFsSubsTrib: TSmallintField
      FieldName = 'SubsTrib'
    end
    object QrFisRegUFsUFEmit: TWideStringField
      FieldName = 'UFEmit'
      Origin = 'fisregufs.UFEmit'
      Size = 2
    end
    object QrFisRegUFsUFDest: TWideStringField
      FieldName = 'UFDest'
      Origin = 'fisregufs.UFDest'
      Size = 2
    end
    object QrFisRegUFsICMSAliq: TFloatField
      FieldName = 'ICMSAliq'
      Origin = 'fisregufs.ICMSAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFisRegUFsCST_B: TWideStringField
      FieldName = 'CST_B'
      Origin = 'fisregufs.CST_B'
      Size = 2
    end
    object QrFisRegUFspRedBC: TFloatField
      FieldName = 'pRedBC'
      Origin = 'fisregufs.pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFisRegUFsmodBC: TSmallintField
      FieldName = 'modBC'
      Origin = 'fisregufs.modBC'
    end
    object QrFisRegUFsModBC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ModBC_TXT'
      Size = 1
      Calculated = True
    end
    object QrFisRegUFspBCUFDest: TFloatField
      FieldName = 'pBCUFDest'
    end
    object QrFisRegUFspFCPUFDest: TFloatField
      FieldName = 'pFCPUFDest'
    end
    object QrFisRegUFspICMSUFDest: TFloatField
      FieldName = 'pICMSUFDest'
    end
    object QrFisRegUFspICMSInter: TFloatField
      FieldName = 'pICMSInter'
    end
    object QrFisRegUFspICMSInterPart: TFloatField
      FieldName = 'pICMSInterPart'
    end
    object QrFisRegUFsUsaInterPartLei: TSmallintField
      FieldName = 'UsaInterPartLei'
    end
    object QrFisRegUFscBenef: TWideStringField
      FieldName = 'cBenef'
      Size = 10
    end
    object QrFisRegUFspDif: TFloatField
      FieldName = 'pDif'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFisRegUFsCSOSN: TWideStringField
      FieldName = 'CSOSN'
      Size = 3
    end
    object QrFisRegUFsOriCST_ICMS: TWideStringField
      FieldName = 'OriCST_ICMS'
      Required = True
      Size = 3
    end
    object QrFisRegUFsOriCST_IPI: TWideStringField
      FieldName = 'OriCST_IPI'
      Size = 2
    end
    object QrFisRegUFsOriCST_PIS: TWideStringField
      FieldName = 'OriCST_PIS'
      Size = 2
    end
    object QrFisRegUFsOriCST_COFINS: TWideStringField
      FieldName = 'OriCST_COFINS'
      Size = 2
    end
    object QrFisRegUFsGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrFisRegUFsGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
    object QrFisRegUFsTES_ICMS: TSmallintField
      FieldName = 'TES_ICMS'
    end
    object QrFisRegUFsTES_IPI: TSmallintField
      FieldName = 'TES_IPI'
    end
    object QrFisRegUFsTES_PIS: TSmallintField
      FieldName = 'TES_PIS'
    end
    object QrFisRegUFsTES_COFINS: TSmallintField
      FieldName = 'TES_COFINS'
    end
    object QrFisRegUFsPISAliq: TFloatField
      FieldName = 'PISAliq'
    end
    object QrFisRegUFsCOFINSAliq: TFloatField
      FieldName = 'COFINSAliq'
    end
    object QrFisRegUFsServico: TSmallintField
      FieldName = 'Servico'
      Required = True
    end
    object QrFisRegUFsOriCFOP: TWideStringField
      FieldName = 'OriCFOP'
      Required = True
      Size = 5
    end
    object QrFisRegUFsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrFisRegUFsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFisRegUFsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFisRegUFsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrFisRegUFsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrFisRegUFsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFisRegUFsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFisRegUFsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFisRegUFsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFisRegUFsTES_BC_ICMS: TSmallintField
      FieldName = 'TES_BC_ICMS'
      Required = True
    end
    object QrFisRegUFsTES_BC_IPI: TSmallintField
      FieldName = 'TES_BC_IPI'
      Required = True
    end
    object QrFisRegUFsTES_BC_PIS: TSmallintField
      FieldName = 'TES_BC_PIS'
      Required = True
    end
    object QrFisRegUFsTES_BC_COFINS: TSmallintField
      FieldName = 'TES_BC_COFINS'
      Required = True
    end
    object QrFisRegUFsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFisRegUFsConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object DsFisRegUFs: TDataSource
    DataSet = QrFisRegUFs
    Left = 464
    Top = 112
  end
  object PMEmitDest: TPopupMenu
    OnPopup = PMEmitDestPopup
    Left = 696
    Top = 528
    object Incluiitemdealquotas1: TMenuItem
      Caption = '&Inclui / altera item de al'#237'quotas'
      OnClick = Incluiitemdealquotas1Click
    end
    object Alteraitemdealiquotas1: TMenuItem
      Caption = '&Altera item de aliquotas'
      OnClick = Alteraitemdealiquotas1Click
    end
    object Excluiumseltodos1: TMenuItem
      Caption = '&Exclui (um / selecionados / todos)'
      OnClick = Excluiumseltodos1Click
    end
  end
  object QrFisRegUFx: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFisRegUFxAfterOpen
    BeforeClose = QrFisRegUFxBeforeClose
    OnCalcFields = QrFisRegUFxCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM Fisregufx'
      'WHERE Codigo=:P0'
      'AND Interno=:P1'
      'AND Contribui=:P2'
      'AND Proprio=:P3'
      'AND SubsTrib=:P4 '
      'AND UFEmit=:P5'
      'AND UFDest=:P6'
      'ORDER BY Nivel, CodNiv'
      ' ')
    Left = 544
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
    object QrFisRegUFxCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegUFxInterno: TSmallintField
      FieldName = 'Interno'
    end
    object QrFisRegUFxContribui: TSmallintField
      FieldName = 'Contribui'
    end
    object QrFisRegUFxProprio: TSmallintField
      FieldName = 'Proprio'
    end
    object QrFisRegUFxSubsTrib: TSmallintField
      FieldName = 'SubsTrib'
    end
    object QrFisRegUFxUFEmit: TWideStringField
      FieldName = 'UFEmit'
      Size = 2
    end
    object QrFisRegUFxUFDest: TWideStringField
      FieldName = 'UFDest'
      Size = 2
    end
    object QrFisRegUFxNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrFisRegUFxCodNiv: TIntegerField
      FieldName = 'CodNiv'
    end
    object QrFisRegUFxICMSAliq: TFloatField
      FieldName = 'ICMSAliq'
    end
    object QrFisRegUFxCST_B: TWideStringField
      FieldName = 'CST_B'
      Size = 2
    end
    object QrFisRegUFxpRedBC: TFloatField
      FieldName = 'pRedBC'
    end
    object QrFisRegUFxmodBC: TSmallintField
      FieldName = 'modBC'
    end
    object QrFisRegUFxModBC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ModBC_TXT'
      Size = 1
      Calculated = True
    end
    object QrFisRegUFxcBenef: TWideStringField
      FieldName = 'cBenef'
      Size = 10
    end
    object QrFisRegUFxCSOSN: TWideStringField
      FieldName = 'CSOSN'
      Size = 3
    end
    object QrFisRegUFxpDif: TFloatField
      FieldName = 'pDif'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFisRegUFxControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFisRegUFxConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFisRegUFxNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
  end
  object DsFisRegUFx: TDataSource
    DataSet = QrFisRegUFx
    Left = 548
    Top = 112
  end
  object PMExcecao: TPopupMenu
    OnPopup = PMExcecaoPopup
    Left = 812
    Top = 612
    object Incluinovaexceo1: TMenuItem
      Caption = '&Inclui nova exce'#231#227'o'
      Enabled = False
      OnClick = Incluinovaexceo1Click
    end
    object Alteraexceoatual1: TMenuItem
      Caption = '&Altera exce'#231#227'o atual'
      Enabled = False
      OnClick = Alteraexceoatual1Click
    end
    object Excluiexceoatual1: TMenuItem
      Caption = '&Exclui exce'#231#227'o atual'
      Enabled = False
      OnClick = Excluiexceoatual1Click
    end
  end
  object PMNatOper: TPopupMenu
    OnPopup = PMRegraPopup
    Left = 944
    Top = 112
    object Descrio1: TMenuItem
      Caption = '&Descri'#231#227'o'
      OnClick = Descrio1Click
    end
    object Cdigo1: TMenuItem
      Caption = '&C'#243'digo'
      OnClick = Cdigo1Click
    end
  end
  object PMQuery: TPopupMenu
    Left = 240
    Top = 16
    object Descrio2: TMenuItem
      Caption = '&Descri'#231#227'o'
      OnClick = Descrio2Click
    end
    object CFOP1: TMenuItem
      Caption = '&CFOP'
      OnClick = CFOP1Click
    end
  end
  object PMEstricao: TPopupMenu
    Left = 820
    Top = 672
    object Incluientidade1: TMenuItem
      Caption = '&Inclui entidade'
      OnClick = Incluientidade1Click
    end
    object Excluientidade1: TMenuItem
      Caption = '&Exclui entidade'
      OnClick = Excluientidade1Click
    end
  end
  object QrFisRegEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, '
      'fre.*  '
      'FROM fisregent fre '
      'LEFT JOIN entidades ent ON ent.Codigo=fre.Entidade ')
    Left = 664
    Top = 415
    object QrFisRegEntNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrFisRegEntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegEntEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrFisRegEntLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrFisRegEntDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFisRegEntDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFisRegEntUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrFisRegEntUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrFisRegEntAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFisRegEntAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFisRegEntAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFisRegEntAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFisRegEntObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
  end
  object DsFisRegEnt: TDataSource
    DataSet = QrFisRegEnt
    Left = 664
    Top = 460
  end
  object QrCtbCadMoF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome '
      'FROM ctbcadmof'
      'ORDER BY Nome')
    Left = 936
    Top = 448
    object QrCtbCadMoFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadMoFNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCtbCadMoF: TDataSource
    DataSet = QrCtbCadMoF
    Left = 936
    Top = 496
  end
  object QrPlaAllCad_D: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM plaallcad'
      'WHERE AnaliSinte=2'
      'AND Ativo=1'
      'ORDER BY Nome'
      ''
      '')
    Left = 252
    Top = 500
    object QrPlaAllCad_DCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCad_DNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCad_DNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCad_DAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCad_DCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCad_DDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCad_DOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object DsPlaAllCad_D: TDataSource
    DataSet = QrPlaAllCad_D
    Left = 252
    Top = 548
  end
  object QrPlaAllCad_C: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM plaallcad'
      'WHERE AnaliSinte=2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 352
    Top = 500
    object QrPlaAllCad_CCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCad_CNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCad_CNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCad_CAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCad_CCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCad_CDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCad_COrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object DsPlaAllCad_C: TDataSource
    DataSet = QrPlaAllCad_C
    Left = 352
    Top = 548
  end
  object QrCFOP_Frete: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 852
    Top = 13
    object QrCFOP_FreteCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrCFOP_FreteNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCFOP_Frete: TDataSource
    DataSet = QrCFOP_Frete
    Left = 852
    Top = 62
  end
  object QrFRCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM fisregcad'
      'ORDER BY Codigo')
    Left = 60
    Top = 320
    object QrFRCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrFRCfop: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fisregcfop'
      'WHERE Codigo=0')
    Left = 124
    Top = 320
    object QrFRCfopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFRCfopInterno: TSmallintField
      FieldName = 'Interno'
      Required = True
    end
    object QrFRCfopContribui: TSmallintField
      FieldName = 'Contribui'
      Required = True
    end
    object QrFRCfopProprio: TSmallintField
      FieldName = 'Proprio'
      Required = True
    end
    object QrFRCfopCFOP: TWideStringField
      FieldName = 'CFOP'
      Required = True
      Size = 5
    end
    object QrFRCfopServico: TSmallintField
      FieldName = 'Servico'
      Required = True
    end
    object QrFRCfopSubsTrib: TSmallintField
      FieldName = 'SubsTrib'
      Required = True
    end
    object QrFRCfopOriCFOP: TWideStringField
      FieldName = 'OriCFOP'
      Required = True
      Size = 5
    end
    object QrFRCfopControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrFRUfs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fisregufs'
      '')
    Left = 192
    Top = 320
    object QrFRUfsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFRUfsInterno: TSmallintField
      FieldName = 'Interno'
      Required = True
    end
    object QrFRUfsContribui: TSmallintField
      FieldName = 'Contribui'
      Required = True
    end
    object QrFRUfsProprio: TSmallintField
      FieldName = 'Proprio'
      Required = True
    end
    object QrFRUfsUFEmit: TWideStringField
      FieldName = 'UFEmit'
      Required = True
      Size = 2
    end
    object QrFRUfsUFDest: TWideStringField
      FieldName = 'UFDest'
      Required = True
      Size = 2
    end
    object QrFRUfsSubsTrib: TSmallintField
      FieldName = 'SubsTrib'
      Required = True
    end
    object QrFRUfsServico: TSmallintField
      FieldName = 'Servico'
      Required = True
    end
    object QrFRUfsOriCFOP: TWideStringField
      FieldName = 'OriCFOP'
      Required = True
      Size = 5
    end
    object QrFRUfsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFRUfsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrFRUfx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fisregufx'
      '')
    Left = 256
    Top = 324
    object QrFRUfxCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFRUfxInterno: TSmallintField
      FieldName = 'Interno'
      Required = True
    end
    object QrFRUfxContribui: TSmallintField
      FieldName = 'Contribui'
      Required = True
    end
    object QrFRUfxProprio: TSmallintField
      FieldName = 'Proprio'
      Required = True
    end
    object QrFRUfxUFEmit: TWideStringField
      FieldName = 'UFEmit'
      Required = True
      Size = 2
    end
    object QrFRUfxUFDest: TWideStringField
      FieldName = 'UFDest'
      Required = True
      Size = 2
    end
    object QrFRUfxNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrFRUfxCodNiv: TIntegerField
      FieldName = 'CodNiv'
      Required = True
    end
    object QrFRUfxSubsTrib: TSmallintField
      FieldName = 'SubsTrib'
      Required = True
    end
    object QrFRUfxServico: TSmallintField
      FieldName = 'Servico'
      Required = True
    end
    object QrFRUfxOriCFOP: TWideStringField
      FieldName = 'OriCFOP'
      Required = True
      Size = 5
    end
    object QrFRUfxNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrFRUfxControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFRUfxConta: TIntegerField
      FieldName = 'Conta'
    end
  end
end
