unit GraGru1CorTam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  UnInternalConsts, mySQLDbTables, Menus, UnDmkProcFunc, dmkImage, UnDmkEnums,
  dmkValUsu, dmkDBGridZTO, UnMySQLCuringa, UnUMedi_PF, Variants;

type
  TFmGraGru1CorTam = class(TForm)
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    QrGraGru3: TmySQLQuery;
    DsGraGru3: TDataSource;
    QrGraGru2: TmySQLQuery;
    DsGraGru2: TDataSource;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrLoc: TmySQLQuery;
    QrLocCodUsu: TIntegerField;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrGraGru5: TmySQLQuery;
    DsGraGru5: TDataSource;
    QrGraGru4: TmySQLQuery;
    DsGraGru4: TDataSource;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGru3CodUsu: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru2Nivel2: TIntegerField;
    QrGraGru2CodUsu: TIntegerField;
    QrGraGru2Nome: TWideStringField;
    QrGraGru4Nivel4: TIntegerField;
    QrGraGru4CodUsu: TIntegerField;
    QrGraGru4Nome: TWideStringField;
    QrGraGru5Nivel5: TIntegerField;
    QrGraGru5CodUsu: TIntegerField;
    QrGraGru5Nome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdCodUsu_Ant: TdmkEdit;
    EdNome_Ant: TdmkEdit;
    EdNivel1: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    LaPrdGrupTip: TLabel;
    LaNivel3: TLabel;
    LaNivel2: TLabel;
    LaNivel1: TLabel;
    LaNivel5: TLabel;
    LaNivel4: TLabel;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    EdNivel3: TdmkEditCB;
    CBNivel3: TdmkDBLookupComboBox;
    EdNivel2: TdmkEditCB;
    CBNivel2: TdmkDBLookupComboBox;
    EdCodUsu_New: TdmkEdit;
    EdNome_New: TdmkEdit;
    EdNivel5: TdmkEditCB;
    CBNivel5: TdmkDBLookupComboBox;
    EdNivel4: TdmkEditCB;
    CBNivel4: TdmkDBLookupComboBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CkRespeitarPai: TCheckBox;
    QrPrdGrupTipNivCad: TSmallintField;
    SbNivel5: TSpeedButton;
    SbNivel4: TSpeedButton;
    SbNivel3: TSpeedButton;
    SbNivel2: TSpeedButton;
    QrPrdGrupTipGradeado: TSmallintField;
    DBCkGradeado: TDBCheckBox;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    QrPrdGrupTipTitNiv4: TWideStringField;
    QrPrdGrupTipTitNiv5: TWideStringField;
    PMInsAlt: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Panel6: TPanel;
    Label2: TLabel;
    EdGraTabApp: TdmkEdit;
    EdTXT_GraTabApp: TdmkEdit;
    QrGraTamCad: TmySQLQuery;
    QrGraTamCadCodigo: TIntegerField;
    QrGraTamCadCodUsu: TIntegerField;
    QrGraTamCadNome: TWideStringField;
    DsGraTamCad: TDataSource;
    LaGrade: TLabel;
    EdGrade: TdmkEditCB;
    CBGrade: TdmkDBLookupComboBox;
    SBGrade: TSpeedButton;
    dmkValUsu1: TdmkValUsu;
    QrGraCorCad: TMySQLQuery;
    DsGraCorCad: TDataSource;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    GBCores: TGroupBox;
    QrGraTamIts: TmySQLQuery;
    QrGraTamItsControle: TIntegerField;
    BitBtn1: TBitBtn;
    BtCor: TBitBtn;
    SbPrdgrupTip: TSpeedButton;
    QrGG1: TmySQLQuery;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label46: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    VUUnidMed: TdmkValUsu;
    Panel7: TPanel;
    Label1: TLabel;
    Panel10: TPanel;
    DBGCores: TdmkDBGridZTO;
    Panel11: TPanel;
    CkFiltrar: TCheckBox;
    EdFiltro: TEdit;
    DBGrid1: TDBGrid;
    QrSelGCC: TMySQLQuery;
    QrSelGCCCodigo: TIntegerField;
    QrSelGCCNome: TWideStringField;
    DsSelGCC: TDataSource;
    Label3: TLabel;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
    procedure QrGraGru3BeforeClose(DataSet: TDataSet);
    procedure EdNivel3Change(Sender: TObject);
    procedure QrGraGru2BeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure EdNivel5Change(Sender: TObject);
    procedure CkRespeitarPaiClick(Sender: TObject);
    procedure SbNivel5Click(Sender: TObject);
    procedure SbNivel4Click(Sender: TObject);
    procedure SbNivel3Click(Sender: TObject);
    procedure SbNivel2Click(Sender: TObject);
    procedure CBPrdGrupTipClick(Sender: TObject);
    procedure EdNivel4Change(Sender: TObject);
    procedure EdCodUsu_NewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure EdNivel2Change(Sender: TObject);
    procedure SBGradeClick(Sender: TObject);
    procedure BtCorClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SbPrdgrupTipClick(Sender: TObject);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkFiltrarClick(Sender: TObject);
    procedure EdFiltroChange(Sender: TObject);
    procedure DBGCoresDblClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FNivCad, FNivClk: Integer;
    FEmReabertura: Boolean;
    FSelGCC: String;
    //FMultiplosGGX: array of Integer;
    //
    function  FiltroNivel(Nivel, Codigo: Integer): String;
    procedure AlteraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
              EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
    procedure CadastraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
              EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
    //
    procedure ReopenNiveisAPartirDe(Nivel: Integer);
    procedure ReopenNivelX(Nivel: Integer; Qry: TmySQLQuery;
              EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
    procedure CadastraItemUnicoDaGrade(GraGruC, GraGru1: Integer);
    procedure CadatraArtigoCorEReduzido();
    procedure CadatraSoReduzido();
    function  SemCorSelecionada(): Boolean;
    function  SemTamSelecionado(): Boolean;
    procedure ReopenGraTamIts(GraTamCad: Integer);
    procedure ReopenSelGCC(Codigo: Integer);


  public
    { Public declarations }
    FSQL_WHERE_PGT: String;
    FNivel1_JaTem: Boolean;
    FNivel1_GraGru1, FNivel1_GraGruC, FGraGruY: Integer;
    FQrGraGru1, FQrGraGruX: TmySQLQuery;
    FForcaCor, FForcaTam: Boolean;
    //
    procedure HabilitaComponentes();
    procedure ReabrePrdgrupTip();
    procedure ReopenGraCorCad(Codigo: Integer);
  end;

  var
  FmGraGru1CorTam: TFmGraGru1CorTam;

implementation

uses UnMyObjects, Module, UMySQLModule, GraGruN, DmkDAC_PF, ModProd, UnAppPF,
  UnGrade_Jan, UnGrade_PF, GraCorCad, MyDBCheck, ModuleGeral, UnGrade_Create;

{$R *.DFM}

procedure TFmGraGru1CorTam.Altera1Click(Sender: TObject);
begin
  case FNivClk of
    2: AlteraRegistroNivel(2, QrGraGru2, EdNivel2, CBNivel2);
    3: AlteraRegistroNivel(3, QrGraGru3, EdNivel3, CBNivel3);
    4: AlteraRegistroNivel(4, QrGraGru4, EdNivel4, CBNivel4);
    5: AlteraRegistroNivel(5, QrGraGru5, EdNivel5, CBNivel5);
  end;
end;

procedure TFmGraGru1CorTam.AlteraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
  EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);

var
  X, Nome: String;
  NivelX, CodUsu: Integer;
begin
  X := Geral.FF0(Nivel);
  Nome := CBNivel.Text;
  if InputQuery('Altera��o de Item no N�vel ' + X, 'Informe a descri��o:', Nome) then
  begin
    UMyMod.ObtemCodigoDeCodUsu(TdmkEditCB(EdNivel), NivelX, '', 'Nivel' + X);
    CodUsu := EdNivel.ValueVariant;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru' + X, False, [
    'Nome'], ['Nivel' + X], [Nome], [
    NivelX], True) then
    begin
      ReopenNiveisAPartirDe(Nivel);
      EdNivel.ValueVariant := CodUsu;
      CBNivel.KeyValue := CodUsu;
    end;
  end;
end;

procedure TFmGraGru1CorTam.BitBtn1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraTamCad(0);
  UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
end;

procedure TFmGraGru1CorTam.BtCorClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCorCad, FmGraCorCad, afmoNegarComAviso) then
  begin
    FmGraCorCad.ShowModal;
    FmGraCorCad.Destroy;
    //
    ReopenGraCorCad(QrGraCorCadCodigo.Value);
  end;
end;

procedure TFmGraGru1CorTam.BtOKClick(Sender: TObject);
(*
begin
  if FNivel1_JaTem then
    CadatraSoReduzido()
  else
    CadatraArtigoCorEReduzido();
*)
var
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu, CU_Ant, Nivel1, PrdGrupTip: Integer;
  Nome: String;
//GraGruC
  Controle, GraCorCad, GraTabApp, GraGruY: Integer;
// GraGruX
  GraTamCad, GraGruC, GraGru1, GraTamI, N, I, Index, UnidMed: Integer;
  Cor, Tam: array of Integer;
  Continua: Boolean;
begin
  CU_Ant      := EdCodUsu_Ant.ValueVariant;
  CodUsu      := EdCodUsu_New.ValueVariant;
  //
  Nome        := EdNome_New.Text;
  //
  if ImgTipo.SQLType = stUpd then
  begin
    if not FNivel1_JaTem and (CU_Ant <> CodUsu) then
    begin
      QrLoc.Close;
      QrLoc.Params[0].AsInteger := CodUsu;
      UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
      if QrLoc.RecordCount > 0 then
      begin
        Geral.MB_Aviso(
        'Altera��o cancelada! o c�digo ' + FormatFloat('0', CodUsu) +
        ' j� pertence a outro produto!');
        //
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
  end else
  if ImgTipo.SQLType = stIns then
  begin
    if not FNivel1_JaTem then
    begin
      if UnDmkDAC_PF.TextoJaCadastrado(Nome, 'Nome', 'GraGru1',  Dmod.MyDB) then
        Exit;
      if MyObjects.FIC(CodUsu = 0, EdCodUsu_New, 'Informe o c�digo!') then
        Exit;
    end;
    //
    if (DmProd.QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = 1) and
      (DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger = 0) and
      (DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger = 0) and
      (DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger = 0) then
    begin
      Geral.MB_Aviso('Defina a cor e o tamanho �nicos nas op��es de grade!');
      Grade_Jan.MostraFormOpcoesGrad();
      Exit;
    end;
    if FForcaCor and SemCorSelecionada then
      Exit;
    if FForcaTam and SemTamSelecionado then
      Exit;
  end;
  UnidMed := VUUnidMed.ValueVariant;
  if MyObjects.FIC(UnidMed = 0, EdUnidMed, 'Informe a unidade de medida!') then
    Exit;
  //
  Continua := False;
  if FNivel1_JaTem then
  begin
    Nivel1 := FNivel1_GraGru1;
    Continua := True;
  end else
  begin
    GraTabApp  := EdGraTabApp.ValueVariant;
    PrdGrupTip := EdPrdGrupTip.ValueVariant;
    //
    if PrdGrupTip <> 0 then
      PrdGrupTip := QrPrdGrupTipCodigo.value;
    //
    Nivel5      := EdNivel5.ValueVariant;
    if Nivel5 <> 0 then
      UMyMod.ObtemCodigoDeCodUsu(EdNivel5, Nivel5, '', 'Nivel5');
    //
    Nivel4      := EdNivel4.ValueVariant;
    if Nivel4 <> 0 then
      UMyMod.ObtemCodigoDeCodUsu(EdNivel4, Nivel4, '', 'Nivel4');
    //
    Nivel3      := EdNivel3.ValueVariant;
    if Nivel3 <> 0 then
      UMyMod.ObtemCodigoDeCodUsu(EdNivel3, Nivel3, '', 'Nivel3');
    //
    Nivel2      := EdNivel2.ValueVariant;
    if Nivel2 <> 0 then
      UMyMod.ObtemCodigoDeCodUsu(EdNivel2, Nivel2, '', 'Nivel2');
    //
    //Nome        := EdNome_New.Text; acima!
    //
    Nivel1      := EdNivel1.ValueVariant;
    Nivel1      := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', ImgTipo.SQLType, Nivel1);
    //
    Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragru1', False, [
    'Nivel5', 'Nivel4', 'Nivel3',
    'Nivel2', 'CodUsu', 'Nome',
    'PrdGrupTip', 'GraTabApp', 'UnidMed'], [
    'Nivel1'], [
    Nivel5, Nivel4, Nivel3,
    Nivel2, CodUsu, Nome,
    PrdGrupTip, GraTabApp, UnidMed], [
    Nivel1], True);
  end;
  if Continua then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      if DBCkGradeado.Checked then
      begin
        N := 0;
        // ini 2023-06-25
        with DBGCores.DataSource.DataSet do
        (*for I := 0 to DBGCores.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBGCores.SelectedRows.Items[I]));
          GotoBookmark(DBGCores.SelectedRows.Items[I]);
          N := N + 1;
          SetLength(Cor, N);
          Cor[N - 1] := QrGraCorCadCodigo.Value;
        end;*)
        //with DBGCores.DataSource.DataSet do
        QrSelGCC.First;
        while not QrSelGCC.Eof do
        begin
          N := N + 1;
          SetLength(Cor, N);
          Cor[N - 1] := QrSelGCCCodigo.Value;
          //
          QrSelGCC.Next;
        end;
        // fim 2023-06-25
        if N > 0 then
        begin
          GraTamCad := QrGraTamCadCodigo.Value;
          ReopenGraTamIts(GraTamCad);
          if QrGraTamIts.RecordCount > 0 then
          begin
            // Definir Grade de tamanhos
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
            'GraTamCad'], ['Nivel1'], [
            GraTamCad], [Nivel1], True);
            //
            if FGraGruY = 0 then
              GraGruY := DmProd.ObtemGraGruYDeGraGru1(Nivel1)
            else
              GraGruY := FGraGruY;
            //
            SetLength(Tam, QrGraTamIts.RecordCount);
            while not QrGraTamIts.Eof do
            begin
              Tam[QrGraTamIts.RecNo - 1] := QrGraTamItsControle.Value;
              QrGraTamIts.Next;
            end;
            for I := 0 to Length(Cor) - 1 do
            begin
              // Registrar a cor
              if Grade_PF.RegistrarACor(Nivel1, Cor[I], Controle) then
              (*
              Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
              GraCorCad      := Cor[I];
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
              'Nivel1', 'GraCorCad'], [
              'Controle'], [
              Nivel1, GraCorCad], [
              Controle], True) then
              *)
              begin
(*//////////////////////////////////////////////////////////////////////////////
                Eliminei o codigo abaixo fazerndo a procedure:
                VS_PF.MostraFormGraGruAtiCorTam(Nivel1, GraTamCad);
                a chamada desta procedure est� abaixo ap�s o codigo desmarcado.
                � que este codigo iria criar reduzidos de todas cores x todos
                tamanhos, e na procedure o usuario cria s� o que precisa!
////////////////////////////////////////////////////////////////////////////////
                GraGruC        := Controle;
                GraGru1        := Nivel1;
                for N := 0 to Length(Tam) - 1 do
                begin
                  // Cadastrar o item �nico da grade
                  GraTamI        := Tam[N];
                  Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
                  'GraGruC', 'GraGru1', 'GraTamI',
                  'GraGruY'], [
                  'Controle'], [
                  GraGruC, GraGru1, GraTamI,
                  GraGruY], [
                  Controle], True) then
                  begin
                    VAR_CADASTRO2 := Controle;
                    if FQrGraGruX <> nil then
                    begin
                      FQrGraGruX.Close;
                      UnDmkDAC_PF.AbreQuery(FQrGraGruX, Dmod.MyDB);
                      //
                      FQrGraGruX.Locate('Controle', Controle, []);
                      //AppPF.TerminaCadastroGrade();
                    end;
                  end;
                end;
*)//////////////////////////////////////////////////////////////////////////////
              end;
            end;
            Grade_Jan.MostraFormGraGruAtiCorTam(Nivel1, GraTamCad, GraGruY);
          end;
        end;
      end else
      begin
        // Definir Grade de tamanhos
        GraTamCad := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
        'GraTamCad'], ['Nivel1'], [
        GraTamCad], [Nivel1], True);

        // Registrar a cor
        //Nivel1         := Nivel1;
        GraCorCad      := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
        if Grade_PF.RegistrarACor(Nivel1, GraCorCad, Controle) then
        (*
        Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
        'Nivel1', 'GraCorCad'], [
        'Controle'], [
        Nivel1, GraCorCad], [
        Controle], True) then
        *)
        begin
          //
          // Cadastrar o item �nico da grade
          GraGruC        := Controle;
          GraGru1        := Nivel1;
          //CadastraItemUnicoDaGrade(GraGruC, GraGru1);
          (**)
          GraTamI        := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
          Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
          'GraGruC', 'GraGru1', 'GraTamI', 'GraGruY'], [
          'Controle'], [
          GraGruC, GraGru1, GraTamI, FGraGruY], [
          Controle], True) then
          begin
            VAR_CADASTRO2 := Controle;
            if FQrGraGruX <> nil then
            begin
              FQrGraGruX.Close;
              UnDmkDAC_PF.AbreQuery(FQrGraGruX, Dmod.MyDB);
              //
              FQrGraGruX.Locate('Controle', Controle, []);
              //AppPF.TerminaCadastroGrade();
            end;
            /////
            (*
            Index := Length(FMultiplosGGX);
            SetLength(FMultiplosGGX, Index + 1);
            FMultiplosGGX[Index] := Controle;
            *)
            ///
          end;
          (**)
          //
        end;
      end;
    end;
    if FQrGraGru1 <> nil then
    begin
      //FmGraGruN.ReopenGraGru1(Nivel1);
      FQrGraGru1.Close;
      UnDmkDAC_PF.AbreQuery(FQrGraGru1, Dmod.MyDB);
      //
      FQrGraGru1.Locate('Nivel1', Nivel1, []);
    end;
    VAR_CADASTRO := Nivel1;
    Close;
  end;
end;

procedure TFmGraGru1CorTam.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGru1CorTam.CadastraItemUnicoDaGrade(GraGruC, GraGru1: Integer);
var
  GraTamI, Controle: Integer;
begin
  // Cadastrar o item �nico da grade
  //GraGruC        := Controle;
  //GraGru1        := Nivel1;
  GraTamI        := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
  Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
  'GraGruC', 'GraGru1', 'GraTamI', 'GraGruY'], [
  'Controle'], [
  GraGruC, GraGru1, GraTamI, FGraGruY], [
  Controle], True) then
  begin
    VAR_CADASTRO2 := Controle;
    if FQrGraGruX <> nil then
    begin
      FQrGraGruX.Close;
      UnDmkDAC_PF.AbreQuery(FQrGraGruX, Dmod.MyDB);
      //
      FQrGraGruX.Locate('Controle', Controle, []);
      //AppPF.TerminaCadastroGrade();
    end;
  end;
end;

procedure TFmGraGru1CorTam.CadastraRegistroNivel(Nivel: Integer; Qry: TmySQLQuery;
  EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
var
  SQL: String;
  //
  procedure SQLNiv(Nivel: Integer; EdNivel: TdmkEditCB);
  var
    Codigo: Integer;
    Fld: String;
  begin
    Fld := 'Nivel' + IntToStr(Nivel);
    UMyMod.ObtemCodigoDeCodUsu(EdNivel, Codigo, '', Fld);

    if EdNivel.ValueVariant = 0 then
      SQL := SQL + Fld + '=0,'
    else
      SQL := SQL + Fld + '=' + Geral.FF0(Codigo) + ',';
  end;
var
  X, Nome: String;
  NivelX, CodUsu, PrdGrupTip: Integer;
begin
  X := Geral.FF0(Nivel);
  Nome := '';
  if InputQuery('Cadastro de Item no N�vel ' + X, 'Informe a descri��o:', Nome) then
  begin
    SQL := '';
    if Nivel = 5 then SQL := 'Nivel6=' + Geral.FF0(QrPrdGrupTipCodigo.Value);
    //
    if Nivel < 5 then SQLNiv(5, EdNivel5);
    if Nivel < 4 then SQLNiv(4, EdNivel4);
    if Nivel < 3 then SQLNiv(3, EdNivel3);
    //
    if Length(SQL) > 1 then
      if SQL[Length(SQL)] = ',' then
        SQL := Copy(SQL, 1, Length(SQL) - 1);
    NivelX := UMyMod.BPGS1I32(
    'GraGru' + X, 'Nivel' + X, '', '', tsPos, stIns, 0);
    CodUsu := UMyMod.BPGS1I32(
    'GraGru' + X, 'CodUsu', '', '', tsPos, stIns, 0);
    //
    PrdGrupTip := EdPrdGrupTip.ValueVariant;
    //
    if PrdGrupTip <> 0 then
      PrdGrupTip := QrPrdGrupTipCodigo.value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru' + X, False, [
    CO_JOKE_SQL, 'CodUsu', 'Nome', 'PrdGrupTip'], [
    'Nivel' + X], [
    SQL, CodUsu, Nome, PrdGrupTip], [
    NivelX], True) then
    begin
      ReopenNiveisAPartirDe(Nivel);
      EdNivel.ValueVariant := CodUsu;
      CBNivel.KeyValue := CodUsu;
    end;
  end;
end;

procedure TFmGraGru1CorTam.CadatraArtigoCorEReduzido();
var
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu, CU_Ant, Nivel1, PrdGrupTip: Integer;
  Nome: String;
//GraGruC
  Controle, GraCorCad, GraTabApp, GraGruY: Integer;
// GraGruX
  GraTamCad, GraGruC, GraGru1, GraTamI, N, I: Integer;
  Cor, Tam: array of Integer;
begin
  CU_Ant      := EdCodUsu_Ant.ValueVariant;
  CodUsu      := EdCodUsu_New.ValueVariant;
  if ImgTipo.SQLType = stUpd then
  begin
    if CU_Ant <> CodUsu then
    begin
      QrLoc.Close;
      QrLoc.Params[0].AsInteger := CodUsu;
      UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
      if QrLoc.RecordCount > 0 then
      begin
        Geral.MB_Aviso(
        'Altera��o cancelada! o c�digo ' + FormatFloat('0', CodUsu) +
        ' j� pertence a outro produto!');
        //
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
  end else
  if ImgTipo.SQLType = stIns then
  begin
    if MyObjects.FIC(CodUsu = 0, EdCodUsu_New, 'Informe o c�digo!') then
      Exit;
    //
    if (DmProd.QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = 1) and
      (DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger = 0) and
      (DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger = 0) and
      (DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger = 0) then
    begin
      Geral.MB_Aviso('Defina a cor e o tamanho �nicos nas op��es de grade!');
      Grade_Jan.MostraFormOpcoesGrad();
    end;
  end;
  //
  GraTabApp  := EdGraTabApp.ValueVariant;
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  //
  if PrdGrupTip <> 0 then
    PrdGrupTip := QrPrdGrupTipCodigo.value;
  //
  Nivel5      := EdNivel5.ValueVariant;
  if Nivel5 <> 0 then
    UMyMod.ObtemCodigoDeCodUsu(EdNivel5, Nivel5, '', 'Nivel5');
  //
  Nivel4      := EdNivel4.ValueVariant;
  if Nivel4 <> 0 then
    UMyMod.ObtemCodigoDeCodUsu(EdNivel4, Nivel4, '', 'Nivel4');
  //
  Nivel3      := EdNivel3.ValueVariant;
  if Nivel3 <> 0 then
    UMyMod.ObtemCodigoDeCodUsu(EdNivel3, Nivel3, '', 'Nivel3');
  //
  Nivel2      := EdNivel2.ValueVariant;
  if Nivel2 <> 0 then
    UMyMod.ObtemCodigoDeCodUsu(EdNivel2, Nivel2, '', 'Nivel2');
  //
  Nome        := EdNome_New.Text;
  //
  Nivel1      := EdNivel1.ValueVariant;
  Nivel1      := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', ImgTipo.SQLType, Nivel1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragru1', False, [
  'Nivel5', 'Nivel4', 'Nivel3', 'Nivel2', 'CodUsu',
  'Nome', 'PrdGrupTip', 'GraTabApp'], [
  'Nivel1'], [
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu,
  Nome, PrdGrupTip, GraTabApp], [
  Nivel1], True) then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      if DBCkGradeado.Checked then
      begin
        N := 0;
        // ini 2023-06-25
        (*
        with DBGCores.DataSource.DataSet do
        for I := 0 to DBGCores.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBGCores.SelectedRows.Items[I]));
          GotoBookmark(DBGCores.SelectedRows.Items[I]);
          N := N + 1;
          SetLength(Cor, N);
          Cor[N - 1] := QrGraCorCadCodigo.Value;
        end;
        *)
        QrSelGCC.First;
        while not QrSelGCC.Eof do
        begin
          N := N + 1;
          SetLength(Cor, N);
          Cor[N - 1] := QrSelGCCCodigo.Value;
          QrSelGCC.Next;
        end;
        // fim 2023-06-25

        if N > 0 then
        begin
          GraTamCad := QrGraTamCadCodigo.Value;
          ReopenGraTamIts(GraTamCad);
          if QrGraTamIts.RecordCount > 0 then
          begin
            // Definir Grade de tamanhos
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
            'GraTamCad'], ['Nivel1'], [
            GraTamCad], [Nivel1], True);
            //
            if FGraGruY = 0 then
              GraGruY := DmProd.ObtemGraGruYDeGraGru1(Nivel1)
            else
              GraGruY := FGraGruY;
            //
            SetLength(Tam, QrGraTamIts.RecordCount);
            while not QrGraTamIts.Eof do
            begin
              Tam[QrGraTamIts.RecNo - 1] := QrGraTamItsControle.Value;
              QrGraTamIts.Next;
            end;
            for I := 0 to Length(Cor) - 1 do
            begin
              // Registrar a cor
              Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
              GraCorCad      := Cor[I];
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
              'Nivel1', 'GraCorCad'], [
              'Controle'], [
              Nivel1, GraCorCad], [
              Controle], True) then
              begin
(*//////////////////////////////////////////////////////////////////////////////
                Eliminei o codigo abaixo fazerndo a procedure:
                VS_PF.MostraFormGraGruAtiCorTam(Nivel1, GraTamCad);
                a chamada desta procedure est� abaixo ap�s o codigo desmarcado.
                � que este codigo iria criar reduzidos de todas cores x todos
                tamanhos, e na procedure o usuario cria s� o que precisa!
////////////////////////////////////////////////////////////////////////////////
                GraGruC        := Controle;
                GraGru1        := Nivel1;
                for N := 0 to Length(Tam) - 1 do
                begin
                  // Cadastrar o item �nico da grade
                  GraTamI        := Tam[N];
                  Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
                  'GraGruC', 'GraGru1', 'GraTamI',
                  'GraGruY'], [
                  'Controle'], [
                  GraGruC, GraGru1, GraTamI,
                  GraGruY], [
                  Controle], True) then
                  begin
                    VAR_CADASTRO2 := Controle;
                    if FQrGraGruX <> nil then
                    begin
                      FQrGraGruX.Close;
                      UnDmkDAC_PF.AbreQuery(FQrGraGruX, Dmod.MyDB);
                      //
                      FQrGraGruX.Locate('Controle', Controle, []);
                      //AppPF.TerminaCadastroGrade();
                    end;
                  end;
                end;
*)//////////////////////////////////////////////////////////////////////////////
              end;
            end;
            Grade_Jan.MostraFormGraGruAtiCorTam(Nivel1, GraTamCad, GraGruY);
          end;
        end;
      end else
      begin
        // Definir Grade de tamanhos
        GraTamCad := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
        'GraTamCad'], ['Nivel1'], [
        GraTamCad], [Nivel1], True);

        // Registrar a cor
        //Nivel1         := Nivel1;
        Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
        GraCorCad      := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
        'Nivel1', 'GraCorCad'], [
        'Controle'], [
        Nivel1, GraCorCad], [
        Controle], True) then
        begin
          //
          // Cadastrar o item �nico da grade
          GraGruC        := Controle;
          GraGru1        := Nivel1;
          CadastraItemUnicoDaGrade(GraGruC, GraGru1);
          (*
          GraTamI        := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
          Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
          'GraGruC', 'GraGru1', 'GraTamI', 'GraGruY'], [
          'Controle'], [
          GraGruC, GraGru1, GraTamI, FGraGRuY], [
          Controle], True) then
          begin
            VAR_CADASTRO2 := Controle;
            if FQrGraGruX <> nil then
            begin
              FQrGraGruX.Close;
              UnDmkDAC_PF.AbreQuery(FQrGraGruX, Dmod.MyDB);
              //
              FQrGraGruX.Locate('Controle', Controle, []);
              //AppPF.TerminaCadastroGrade();
            end;
          end;
          *)
          //
        end;
      end;
    end;
    if FQrGraGru1 <> nil then
    begin
      //FmGraGruN.ReopenGraGru1(Nivel1);
      FQrGraGru1.Close;
      UnDmkDAC_PF.AbreQuery(FQrGraGru1, Dmod.MyDB);
      //
      FQrGraGru1.Locate('Nivel1', Nivel1, []);
    end;
    VAR_CADASTRO := Nivel1;
    Close;
  end;
end;

procedure TFmGraGru1CorTam.CadatraSoReduzido();
begin
(*
  CadastraItemUnicoDaGrade(FNivel1_GraGruC, FNivel1_GraGru1);
  //
  /
    if FQrGraGru1 <> nil then
    begin
      //FmGraGruN.ReopenGraGru1(Nivel1);
      FQrGraGru1.Close;
      UnDmkDAC_PF.AbreQuery(FQrGraGru1, Dmod.MyDB);
      //
      FQrGraGru1.Locate('Nivel1', Nivel1, []);
    end;
    VAR_CADASTRO := Nivel1;
    Close;
*)
end;

procedure TFmGraGru1CorTam.CBPrdGrupTipClick(Sender: TObject);
begin
  HabilitaComponentes();
end;

procedure TFmGraGru1CorTam.CkFiltrarClick(Sender: TObject);
begin
  ReopenGraCorCad(QrGraCorCadCodigo.Value);
end;

procedure TFmGraGru1CorTam.CkRespeitarPaiClick(Sender: TObject);
begin
  HabilitaComponentes();
end;

procedure TFmGraGru1CorTam.DBGCoresDblClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  if (QrGraCorCad.State <> dsInactive) and (QrGraCorCad.RecordCount > 0) then
  begin
    Codigo := QrGraCorCadCodigo.Value;
    Nome   := QrGraCorCadNome.Value;
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSelGCC, False, [
    'Nome'], [
    'Codigo'], [
    Nome], [
    Codigo], False) then
    begin
      ReopenSelGCC(Codigo);
    end;
  end;
end;

procedure TFmGraGru1CorTam.DBGrid1DblClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrSelGCC.State <> dsInactive) and (QrSelGCC.RecordCount > 0) then
  begin
    Codigo := QrSelGCCCodigo.Value;
    //
    UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB,
    ' DELETE FROM ' + FSelGCC +
    ' WHERE Codigo=' + Geral.FF0(QrSelGCCCodigo.Value) +
    '');
     ReopenSelGCC(0);
  end;
end;

procedure TFmGraGru1CorTam.EdCodUsu_NewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  CodUsu: Integer;
begin
  if Key = VK_F4 then
  begin
    CodUsu := UMyMod.BPGS1I32(
      'GraGru1', 'CodUsu', '', '', tsPos, stIns, 0);
    EdCodUsu_New.ValueVariant := CodUsu;
  end;
end;

procedure TFmGraGru1CorTam.EdFiltroChange(Sender: TObject);
begin
  ReopenGraCorCad(QrGraCorCadCodigo.Value);
end;

procedure TFmGraGru1CorTam.EdNivel2Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(1);
end;

procedure TFmGraGru1CorTam.EdNivel3Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(2);
end;

procedure TFmGraGru1CorTam.EdNivel4Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(3);
end;

procedure TFmGraGru1CorTam.EdNivel5Change(Sender: TObject);
begin
  ReopenNiveisAPartirDe(4);
end;

procedure TFmGraGru1CorTam.EdPrdGrupTipChange(Sender: TObject);
begin
  HabilitaComponentes();
end;

procedure TFmGraGru1CorTam.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraGru1CorTam.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraGru1CorTam.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmGraGru1CorTam.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmGraGru1CorTam.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

function TFmGraGru1CorTam.FiltroNivel(Nivel, Codigo: Integer): String;
begin
  if CkRespeitarPai.Checked then
    Result := 'OR Nivel' + Geral.FF0(Nivel) + '=' + Geral.FF0(Codigo)
  else
    Result := 'OR Nivel' + Geral.FF0(Nivel) + '<>0';
end;

procedure TFmGraGru1CorTam.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if EdCodUsu_New.Enabled and
  (EdCodUsu_New.ValueVariant <> 0) and (EdNome_New.Text = '') then
    EdNome_New.SetFocus;
end;

procedure TFmGraGru1CorTam.FormCreate(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FGraGruY   := 0;
  FForcaCor  := False;
  FForcaTam  := False;
  FQrGraGru1 := nil;
  FQrGraGruX := nil;
  FSQL_WHERE_PGT := '';
  //
  FEmReabertura := False;
  //
  FNivCad := 0;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  ReabrePrdGrupTip();
  UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  ReopenGraCorCad(0);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  FSelGCC := GradeCriar.RecriaTempTableNovo(ntrttVSGraCorSel, DModG.QrUpdPID1, False);
  ReopenSelGCC(0);
end;

procedure TFmGraGru1CorTam.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGru1CorTam.HabilitaComponentes();
var
  Habil: Boolean;
begin
  FNivCad := QrPrdGrupTipNivCad.Value;
  //
  Habil := FNivCad >= 5;
  LaNivel5.Enabled := Habil;
  EdNivel5.Enabled := Habil;
  CBNivel5.Enabled := Habil;
  SbNivel5.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel5.Caption := QrPrdGrupTipTitNiv5.Value;
  //
  Habil := FNivCad >= 4;
  LaNivel4.Enabled := Habil;
  EdNivel4.Enabled := Habil;
  CBNivel4.Enabled := Habil;
  SbNivel4.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel4.Caption := QrPrdGrupTipTitNiv4.Value;
  //
  Habil := FNivCad >= 3;
  LaNivel3.Enabled := Habil;
  EdNivel3.Enabled := Habil;
  CBNivel3.Enabled := Habil;
  SbNivel3.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel3.Caption := QrPrdGrupTipTitNiv3.Value;
  //
  Habil := FNivCad >= 2;
  LaNivel2.Enabled := Habil;
  EdNivel2.Enabled := Habil;
  CBNivel2.Enabled := Habil;
  SbNivel2.Enabled := Habil and CkRespeitarPai.Checked;
  LaNivel2.Caption := QrPrdGrupTipTitNiv2.Value;
  //
  LaNivel1.Caption := QrPrdGrupTipTitNiv1.Value + ' [F4]';
  //
  ReopenNiveisAPartirDe(5);
end;

procedure TFmGraGru1CorTam.Inclui1Click(Sender: TObject);
begin
  case FNivClk of
    2: CadastraRegistroNivel(2, QrGraGru2, EdNivel2, CBNivel2);
    3: CadastraRegistroNivel(3, QrGraGru3, EdNivel3, CBNivel3);
    4: CadastraRegistroNivel(4, QrGraGru4, EdNivel4, CBNivel4);
    5: CadastraRegistroNivel(5, QrGraGru5, EdNivel5, CBNivel5);
  end;
end;

procedure TFmGraGru1CorTam.QrGraGru2BeforeClose(DataSet: TDataSet);
begin
//  QrGraGru1.Close;
end;

procedure TFmGraGru1CorTam.QrGraGru3BeforeClose(DataSet: TDataSet);
begin
  //QrGraGru2.Close;
end;

procedure TFmGraGru1CorTam.ReopenNivelX(Nivel: Integer; Qry: TmySQLQuery;
EdNivel: TdmkEdit; CBNivel: TdmkDBLookupComboBox);
  function NivAcima(Nivel: Integer; EdNivel: TdmkEdit): Integer;
  begin
    if EdNivel.ValueVariant = 0 then
      Result := 0
    else
      Result := Nivel;
  end;
var
  Acima,
  Este: Integer;
  Compl, X, Y: String;
begin
  Acima := 0;
  X     := Geral.FF0(Nivel);
  Y     := Geral.FF0(Nivel + 1);
  //
  case Nivel of
    5: Acima := NivAcima(QrPrdGrupTipCodigo.Value, EdPrdGrupTip);
    4: Acima := NivAcima(QrGraGru5Nivel5.Value, EdNivel5);
    3: Acima := NivAcima(QrGraGru4Nivel4.Value, EdNivel4);
    2: Acima := NivAcima(QrGraGru3Nivel3.Value, EdNivel3);
    1: Acima := NivAcima(QrGraGru2Nivel2.Value, EdNivel2);
  end;
  if Qry.State <> dsInactive then
  begin
    //Acima := Qry.FieldByName('Nivel' + X).AsInteger;
    Compl := FiltroNivel(Nivel + 1, Acima);
  end else
  begin
    //Acima := 0;
    Compl := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Nivel' + X + ', CodUsu, Nome ',
  'FROM gragru' + X,
  'WHERE Nivel' + Y + '=0 ',
  Compl,
  'ORDER BY Nome ',
  '']);
  //
  Este := EdNivel.ValueVariant;
  if (FNivCad < Nivel) or (not QrGraGru5.Locate('CodUsu', Este, [])) then
  begin
    EdNivel.ValueVariant := 0;
    CBNivel.KeyValue := 0;
  end;
end;

procedure TFmGraGru1CorTam.SbNivel2Click(Sender: TObject);
begin
  FNivClk := 2;
  Altera1.Enabled := EdNivel2.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel2);
end;

procedure TFmGraGru1CorTam.SbNivel3Click(Sender: TObject);
begin
  FNivClk := 3;
  Altera1.Enabled := EdNivel3.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel3);
end;

procedure TFmGraGru1CorTam.SbNivel4Click(Sender: TObject);
begin
  FNivClk := 4;
  Altera1.Enabled := EdNivel4.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel4);
end;

procedure TFmGraGru1CorTam.SbNivel5Click(Sender: TObject);
begin
  FNivClk := 5;
  Altera1.Enabled := EdNivel5.ValueVariant <> 0;
  MyObjects.MostraPopUpDeBotao(PMInsAlt, SbNivel5);
end;

procedure TFmGraGru1CorTam.SbPrdgrupTipClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormPrdGrupTip(EdPrdGrupTip.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdPrdGrupTip, CBPrdGrupTip, QrPrdGrupTip, VAR_CADASTRO);
end;

procedure TFmGraGru1CorTam.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VUUnidMed.ValueVariant;
  UMedi_PF.CriaEEscolheUnidMed(Codigo, EdUnidMed, CBUnidMed, QrUnidMed);
end;

function TFmGraGru1CorTam.SemCorSelecionada(): Boolean;
var
  N, I: Integer;
begin
  // ini 2023-06-25
(*
  Result := True;
  N := 0;
  with DBGCores.DataSource.DataSet do
  for I := 0 to DBGCores.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBGCores.SelectedRows.Items[I]));
    GotoBookmark(DBGCores.SelectedRows.Items[I]);
    N := N + 1;
  end;*)
  N := QrSelGCC.RecordCount;
  // fim 2023-06-25
  Result := N = 0;
  if Result then
    Geral.MB_Aviso('Nenhuma cor foi selecionada!');
end;

function TFmGraGru1CorTam.SemTamSelecionado(): Boolean;
begin
  Result := True;
  if CBGrade.KeyValue <> Null then
  begin
     ReopenGraTamIts(EdGrade.ValueVariant);
     Result := QrGraTamIts.RecordCount = 0;
  end;
  if Result then
    Geral.MB_Aviso(
    'Nenhuma grade foi selecionada, ou a grade selecionada n�o tem tamanhos cadastrados!');
end;

procedure TFmGraGru1CorTam.SBGradeClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  Grade_Jan.MostraFormGraTamCad(dmkValUsu1.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrGraTamCad.Close;
    UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
    QrGraTamCad.Locate('Codigo', VAR_CADASTRO, []);
    //
    EdGrade.ValueVariant := QrGraTamCadCodUsu.Value;
    CBGrade.KeyValue     := QrGraTamCadCodUsu.Value;
    CBGrade.SetFocus;
  end;
end;

procedure TFmGraGru1CorTam.ReabrePrdgrupTip();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrdGrupTip, Dmod.MyDB, [
  'SELECT Codigo, CodUsu,  ',
  'Nome, NivCad, Gradeado, ',
  'TitNiv1, TitNiv2, TitNiv3, ',
  'TitNiv4, TitNiv5 ',
  'FROM prdgruptip ',
  FSQL_WHERE_PGT,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmGraGru1CorTam.ReopenSelGCC(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSelGCC, DModG.MyPID_DB, [
  'SELECT * FROM ' + FSelGCC,
  '']);
  //
  QrSelGCC.Locate('Codigo', Codigo, []);
end;

procedure TFmGraGru1CorTam.ReopenGraCorCad(Codigo: Integer);
var
  Liga, SQL_Filtro, Texto: String;
  I: Integer;
begin
(**)
  if CkFiltrar.Checked then
    SQL_Filtro := 'WHERE Nome LIKE "' + EdFiltro.Text + '"'
  else
    SQL_Filtro := '';
(**)
(*  Liga := 'WHERE ';
  for I := 0 to MeFiltro.Lines.Count - 1 do
  begin
    if Trim(MeFiltro.Lines[I]) <> EmptyStr then
    begin
      SQL_Filtro := SQL_Filtro +
        Liga + 'Nome LIKE "' + MeFiltro.Lines[I] + '"' + sLineBreak;
    end;
    //
    Liga := 'OR '
  end;
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraCorCad, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM gracorcad',
  SQL_FIltro,
  'ORDER BY Nome',
  '']);
  if Codigo <> 0 then
    QrGraCorCad.Locate('Codigo', Codigo, []);
end;

procedure TFmGraGru1CorTam.ReopenGraTamIts(GraTamCad: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraTamIts, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM gratamIts ',
  'WHERE Codigo=' + Geral.FF0(GraTamCad),
  'ORDER BY Controle ',
  '']);
end;

procedure TFmGraGru1CorTam.ReopenNiveisAPartirDe(Nivel: Integer);
begin
  if FEmReabertura then
    Exit;
  FEmReabertura := True;
  try
    if Nivel >= 5 then ReopenNivelX(5, QrGraGru5, EdNivel5, CBNivel5);
    if Nivel >= 4 then ReopenNivelX(4, QrGraGru4, EdNivel4, CBNivel4);
    if Nivel >= 3 then ReopenNivelX(3, QrGraGru3, EdNivel3, CBNivel3);
    if Nivel >= 2 then ReopenNivelX(2, QrGraGru2, EdNivel2, CBNivel2);
  finally
    FEmReabertura := False;
  end;
end;

end.

