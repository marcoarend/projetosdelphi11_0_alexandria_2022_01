unit StqManLeiIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, ComCtrls,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, Variants,
  dmkImage, UnDmkEnums;

type
  TFmStqManLeiIts = class(TForm)
    Panel1: TPanel;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    PnSeleciona: TPanel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    QrItem: TmySQLQuery;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrItemCustoPreco: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    CkContinuar: TCheckBox;
    TabSheet1: TTabSheet;
    GradeP: TStringGrid;
    StaticText3: TStaticText;
    BitBtn2: TBitBtn;
    SBGraGru1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradePDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradePKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradePSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BitBtn2Click(Sender: TObject);
    procedure SBGraGru1Click(Sender: TObject);
  private
    { Private declarations }
    FOriCtrl: Integer;
    procedure ReconfiguraGradeQ();
    procedure AtualizaListaDePrecos();
    // Qtde de Itens
    function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    function ObtemQtde(var Qtde: Integer): Boolean;
  public
    { Public declarations }
  end;

  var
  FmStqManLeiIts: TFmStqManLeiIts;

implementation

uses UnMyObjects, Module, ModProd, MyVCLSkin, GetValor, UMySQLModule,
  UnInternalConsts, ModuleGeral, StqManCad, StqManLei, StqCenLoc, UnGrade_Tabs,
  DMkDAC_PF, UnGrade_Jan;

{$R *.DFM}

procedure TFmStqManLeiIts.AtualizaListaDePrecos;
var
  Col, Row, Nivel1, GraGruX, Tabela, Empresa: Integer;
  Preco: Double;
begin
  Nivel1  := QrGraGru1Nivel1.Value;
  Tabela  := FmStqManCad.QrStqManCadGraCusPrc.Value;
  Empresa := FmStqManCad.QrStqManCadEmpresa.Value;
  //
  for Col := 1 to GradeQ.ColCount -1 do
  begin
    if GradeX.Cells[Col,0] <> '' then
    begin
      for Row := 1 to GradeQ.RowCount - 1 do
      begin
        if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
        begin
          GraGruX := Geral.IMV(GradeC.Cells[Col,Row]);
          Preco   := Geral.DMV(GradeP.Cells[Col,Row]);
          //
          if (Tabela <> 0) and (Empresa <> 0) and (GraGruX <> 0) then
            DmProd.PrecoItemCorTam(Nivel1, GraGruX, Tabela, Empresa, Preco);
        end;
      end;
    end;
  end;
end;

procedure TFmStqManLeiIts.BitBtn2Click(Sender: TObject);
var
  GraCusPrc, Nivel1: Integer;
begin
  GraCusPrc := FmStqManCad.QrStqManCadGraCusPrc.Value;
  //
  if EdGraGru1.ValueVariant <> 0 then
    Nivel1 := QrGraGru1Nivel1.Value
  else
    Nivel1 := 0;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1, GraCusPrc, tpGraCusPrc);
end;

procedure TFmStqManLeiIts.BtOKClick(Sender: TObject);
{
const
  FTipoStqMovIts = 099; //VAR_FATID_0099
}
var
  Col, Row, OriCodi, Tot, Controle, StqCenCad, GraGruX, IDCtrl,
  Empresa, OriCnta, AlterWeb, Ativo: Integer;
  Qtd, Qtde, Preco, Valor: Double;
  DataHora, Msg: String;
  Continua: Word;
  Execut, Erro: Boolean;
{
  i, Sequencia: Integer;
  Txt1, Txt2: String;
}
begin
  Screen.Cursor := crHourGlass;
  try
    Erro      := False;
    Execut    := True;
    Controle  := 0;
    DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    StqCenCad := FmStqManCad.QrStqManCadStqCenCad.Value;
    //StqCenLoc := FmStqManLei.QrStqCenLocControle.Value;
    OriCodi   := FmStqManCad.QrStqManCadCodigo.Value;
    Empresa   := FmStqManCad.QrStqManCadEmpresa.Value;
    OriCnta   := 0;
    AlterWeb  := 1;
    Ativo     := 0;
    //
    Tot    := 0;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
            Tot := Tot + Geral.IMV(GradeQ.Cells[Col, Row]);
        end;
      end;
    end;
    if Tot = 0 then
    begin
      Geral.MB_Aviso('N�o h� sele��o de itens!');
      Execut := False;
      Exit;
    end;
    PB1.Position := 0;
    PB1.Max      := Tot;
    //
    AtualizaListaDePrecos();
    //
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
          begin
            GraGruX := Geral.IMV(GradeC.Cells[Col,Row]);
            Qtd     := Geral.DMV(GradeQ.Cells[Col, Row]);
            //
            // Saldo
            Continua := DmProd.VerificaEstoqueProduto(Qtd, FmStqManCad.QrStqManCadEntraSai.Value,
                          GraGruX, StqCenCad, FmStqManCad.QrStqManCadFisRegCad.Value,
                          OriCodi, OriCnta, Empresa, FmStqManCad.QrStqManCadFatSemEstq.Value, False, Msg);
            //
            if Continua = ID_YES then
            begin
              Qtde := Qtd * FmStqManCad.QrStqManCadFator.Value;
              //
              if Qtde <> 0 then
              begin
                if GraGruX > 0 then
                begin
                  UnDmkDAC_PF.AbreMySQLQuery0(QrItem, Dmod.MyDB, [
                    'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, ',
                    'gcc.Nome NOMECOR,  gti.Nome NOMETAM, ',
                    'ggx.Controle GraGruX, ggx.GraGru1, gri.CustoPreco ',
                    'FROM gragrux ggx ',
                    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
                    'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
                    'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
                    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
                    'LEFT JOIN gragruval gri ON gri.GraGruX=ggx.Controle ',
                    'AND gri.GraCusPrc=' + Geral.FF0(FmStqManCad.QrStqManCadGraCusPrc.Value),
                    'AND gri.Entidade=' + Geral.FF0(FmStqManCad.QrStqManCadEmpresa.Value),
                    'WHERE ggx.Controle=' + Geral.FF0(GraGrux),
                    '']);
                  //
                  Preco := QrItemCustoPreco.Value;
                  Valor := Round(Preco * Qtde * 100) / 100;
                  if Valor <> 0 then
                  begin
                    FOriCtrl   := 0;
                    FOriCtrl   := DModG.BuscaProximoCodigoInt('controle', 'stqmov099', '', FOriCtrl);
                    //
                    IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
                    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
                    'DataHora', 'Tipo', 'OriCodi',
                    'OriCtrl', 'Empresa', 'StqCenCad',
                    'GraGruX', 'Qtde', 'Baixa', 'OriCnta',
                    'OriPart', 'AlterWeb', 'Ativo'], [
                    'IDCtrl'], [
                    DataHora, VAR_FATID_0099, OriCodi,
                    FOriCtrl, Empresa, StqCenCad,
                    GraGruX, Qtd, FmStqManCad.QrStqManCadFator.Value, OriCnta,
                    FOriCtrl, AlterWeb, Ativo], [
                    IDCtrl], False) then
                    begin
                      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmanits', False, [
                      'Codigo', 'GraGruX', 'Qtde', 'Preco', 'Valor'], ['Controle'], [
                      OriCodi, GraGruX, Qtde, Preco, Valor], [FOriCtrl], True) then
                      begin
                        FmStqManLei.ReopenLidos(Controle);
                        FmStqManCad.ReopenGrupos(FmStqManCad.QrGruposNivel1.Value);
                      end;
                    end;
                  end else
                  begin
                    PageControl1.ActivePageIndex := 1;
                    //
                    Geral.MB_Aviso('Valor (ou pre�o) n�o definido para o reduzido '
                      + Geral.FF0(GraGruX) + '. Ele n�o ser� incluido!');
                    //
                    Execut := False;
                  end;
                end;
              end;
            end else
              Erro := True;
          end;
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    FmStqManLei.ReopenLidos(FOriCtrl);
  end;
  if Execut = True then
  begin
    if CkContinuar.Checked = True then
    begin
      PageControl1.ActivePageIndex := 0;
      //
      if Erro = False then
      begin
        EdGraGru1.ValueVariant := 0;
        CBGraGru1.KeyValue     := Null;
      end;
      EdGraGru1.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmStqManLeiIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqManLeiIts.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmStqManLeiIts.EdGraGru1Exit(Sender: TObject);
begin
  ReconfiguraGradeQ();
end;

procedure TFmStqManLeiIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqManLeiIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  GradeA.ColWidths[0] := 128;
  GradeP.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
    'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, ',
    'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,',
    'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,',
    'gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,',
    'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, ',
    'unm.Nome NOMEUNIDMED ',
    'FROM gragru1 gg1',
    'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
    //'WHERE pgt.TipPrd=1 ', desabilitado em 2014-07-08 por MLA e MCW Usava para que?
    'ORDER BY gg1.Nome',
    '']);
end;

procedure TFmStqManLeiIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqManLeiIts.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnSeleciona.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnSeleciona.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmStqManLeiIts.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnSeleciona.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmStqManLeiIts.GradePDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Valor: String;
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else if GradeC.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;
  //
  if (ACol = 0) or (ARow = 0) then
  begin
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taLeftJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False);
  end else
  begin
    if Dmod.FStrFmtPrc <> '' then
      Valor := FormatFloat(Dmod.FStrFmtPrc, Geral.DMV(GradeP.Cells[Acol, ARow]))
    else
      Valor := GradeP.Cells[Acol, ARow];
    //
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taRightJustify,
      Valor, 0, 0, False);
  end;
end;

procedure TFmStqManLeiIts.GradePKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeP, GradeC, Key, Shift);
end;

procedure TFmStqManLeiIts.GradePSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    GradeP.Options := GradeP.Options - [goEditing]
  else
    GradeP.Options := GradeP.Options + [goEditing];
end;

procedure TFmStqManLeiIts.GradeQDblClick(Sender: TObject);
begin
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
end;

procedure TFmStqManLeiIts.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
  Texto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnSeleciona.Color
  else if GradeA.Cells[ACol, ARow] <> '' then
  begin
    Texto := GradeQ.Cells[ACol, ARow];
    Cor := clWindow;
  end else begin
    if (GradeC.Cells[ACol,ARow] = '')
    and (GradeQ.Cells[ACol,ARow] <> '')
    then  GradeQ.Cells[ACol,ARow] := '';
    Texto := '';
    Cor := clMenu;
  end;
  //


  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor, taLeftJustify,
      GradeQ.Cells[Acol, ARow], 1, 1, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor, taRightJustify,
      (*GradeQ.Cells[AcolARow]*)Texto, 1, 1, False);
end;

procedure TFmStqManLeiIts.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeQ, GradeC, Key, Shift);
end;

procedure TFmStqManLeiIts.ReconfiguraGradeQ;
var
  Grade, Nivel1, Tabela, Empresa: Integer;
begin
  Grade   := QrGraGru1GraTamCad.Value;
  Nivel1  := QrGraGru1Nivel1.Value;
  Tabela  := FmStqManCad.QrStqManCadGraCusPrc.Value;
  Empresa := FmStqManCad.QrStqManCadEmpresa.Value;
  //
  DmProd.ConfigGrades12(Grade, Nivel1, Tabela, Empresa, GradeA, GradeX, GradeC,
    GradeP, GradeQ);
end;

procedure TFmStqManLeiIts.SBGraGru1Click(Sender: TObject);
var
  Nivel1: Integer;
begin
  if EdGraGru1.ValueVariant <> 0 then
    Nivel1 := QrGraGru1Nivel1.Value
  else
    Nivel1 := 0;
  //
  VAR_CADASTRO2 := 0;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1, 0, tpGraCusPrc, tsfNenhum);
  try
    Screen.Cursor := crHourGlass;
    //
    if VAR_CADASTRO2 <> 0 then
      Nivel1 := VAR_CADASTRO2;
    //
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    QrGraGru1.Locate('Nivel1', Nivel1, []);
    //
    ReconfiguraGradeQ();
    //
    EdGraGru1.ValueVariant := Nivel1;
    CBGraGru1.KeyValue     := Nivel1;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmStqManLeiIts.ObtemQtde(var Qtde: Integer): Boolean;
var
  ResVar: Variant;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Quantidade', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;

function TFmStqManLeiIts.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde, c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Geral.MB_Aviso('O item nunca foi ativado!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde  := Geral.IMV(GridP.Cells[Coluna, Linha]);
  if ObtemQtde(Qtde) then
    GridP.Cells[Coluna, Linha] := FormatFloat('0', Qtde);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmStqManLeiIts.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX, Qtde: Integer;
  QtdeTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item com c�digo na sele��o ' +
      'para que se possa incluir / alterar o pre�o!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemQtde(Qtde) then
  begin
    QtdeTxt := Geral.FFT(Qtde, 0, siPositivo);
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
        GradeQ.Cells[c,l] := QtdeTxt;
    end;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

end.
