unit UnGrade_Create;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable_Grade = (ntrttListaIgapo,
    ntrttFatDivRef, //ntrttFatDivSum,
    ntrttGIL_Mov, // GraImpLista.Movimento
    //
    ntrttNFe_100, ntrttNFe_101, ntrttNFe_XXX, ntrttNFe_CFOP,
    //
    ntrttSintegra10, ntrttSintegra11, ntrttSintegra50, ntrttSintegra51,
    ntrttSintegra53, ntrttSintegra54, ntrttSintegra70, ntrttSintegra71,
    ntrttSintegra74, ntrttSintegra75, ntrttSintegra85, ntrttSintegra86,
    ntrttSintegra88, ntrttSintegra90, ntrttSintegraPM, ntrttSintegraEC,
    ntrttSintegraEr,
    ntrttS50Load, ntrttS54Load, ntrttS70Load, ntrttS75Load,
    ntrttStqMovValX,
    // SPED-EFD
    ntrttSPED_EFD_0_0000,
    ntrttSPED_EFD_0_0001,
    ntrttSPED_EFD_0_0005,
    ntrttSPED_EFD_0_0100,
    ntrttSPED_EFD_0_0150,
    ntrttSPED_EFD_0_0190,
    ntrttSPED_EFD_0_0200,
    ntrttSPED_EFD_0_0400,
    ntrttSPED_EFD_0_0990,
    ntrttSPED_EFD_0_C001,
    ntrttSPED_EFD_0_C100,
    ntrttSPED_EFD_0_C170,
    ntrttSPED_EFD_0_C190,
    ntrttSPED_EFD_0_C500,
    ntrttSPED_EFD_0_C590,
    ntrttSPED_EFD_0_C990,
    ntrttSPED_EFD_0_D001,
    ntrttSPED_EFD_0_D100,
    ntrttSPED_EFD_0_D190,
    ntrttSPED_EFD_0_D500,
    ntrttSPED_EFD_0_D590,
    ntrttSPED_EFD_0_D990,
    ntrttSPED_EFD_0_E001,
    ntrttSPED_EFD_0_E100,
    ntrttSPED_EFD_0_E110,
    ntrttSPED_EFD_0_E990,
    ntrttSPED_EFD_0_H001,
    ntrttSPED_EFD_0_H005,
    ntrttSPED_EFD_0_H010,
    ntrttSPED_EFD_0_H990,
    ntrttSPED_EFD_0_1001,
    ntrttSPED_EFD_0_1990,
    ntrttSPED_EFD_0_9001,
    ntrttSPED_EFD_0_9900,
    ntrttSPED_EFD_0_9990,
    ntrttSPED_EFD_0_9999,
    //
    ntrttSPEDEFD_C100,
    //
    ntrttSPEDEFD_BLCS,
    //
    ntrttSPEDESTQ_ITS,
    ntrttSPEDESTQ_SUM,
    ntrttSPEDESTQ_ERR,
    //
    ntrttSPEDINN_0200,

    ntrttGGX_SCC_Stq,
    ntrttVSGraCorSel
    );
  //TAcaoCreate = (acDrop, acCreate, acFind);
  TUnGradeCreate = class(TObject)

  private
    { Private declarations }

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable_Grade;
             Qry: TmySQLQuery; UniqueTableName: Boolean;
             Repeticoes: Integer = 1; NomeTab: String = ''): String;
    procedure Cria_ntrttListaIgapo(Qry: TmySQLQuery);
    procedure Cria_ntrttGIL_Mov(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttNFe_100(Qry: TmySQLQuery);
    procedure Cria_ntrttNFe_101(Qry: TmySQLQuery);
    procedure Cria_ntrttNFe_XXX(Qry: TmySQLQuery);
    procedure Cria_ntrttNFe_CFOP(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttSPED_EFD_0_0000(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0001(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0005(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0100(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0150(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0190(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0200(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0400(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_0990(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_C001(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_C100(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_C170(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_C190(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_C500(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_C590(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_C990(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_D001(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_D100(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_D190(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_D500(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_D590(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_D990(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_E001(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_E100(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_E110(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_E990(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_H001(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_H005(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_H010(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_H990(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_1001(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_1990(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_9001(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_9900(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_9990(Qry: TmySQLQuery);
    procedure Cria_ntrttSPED_EFD_0_9999(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttSPEDEFD_C100(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttSPEDEFD_BLCS(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttSPEDESTQ_ITS(Qry: TmySQLQuery);
    procedure Cria_ntrttSPEDESTQ_SUM(Qry: TmySQLQuery);
    procedure Cria_ntrttSPEDESTQ_ERR(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttSPEDINN_0200(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttGGX_SCC_Stq(Qry: TmySQLQuery);
    procedure Cria_ntrttVSGraCorSel(Qry: TmySQLQuery);
  end;

var
  GradeCriar: TUnGradeCreate;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TUnGradeCreate.Cria_ntrttGGX_SCC_Stq(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Nivel1               int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_PRD               varchar(511)           DEFAULT "? ? ?"     ,');
  Qry.SQL.Add('  PrdGrupTip           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  UnidMed              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_PGT               varchar(30)            DEFAULT "? ? ?"     ,');
  Qry.SQL.Add('  SIGLA                varchar(6)             DEFAULT "??"        ,');
  Qry.SQL.Add('  NO_TAM               varchar(5)             DEFAULT "??"        ,');
  Qry.SQL.Add('  NO_COR               varchar(30)            DEFAULT "? ? ?"     ,');
  Qry.SQL.Add('  GraCorCad            int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruC              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGru1              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  GraTamI              int(11)                DEFAULT "0"         ,');

  Qry.SQL.Add('  QTDE                 double(15,3)                               ,');
  Qry.SQL.Add('  PECAS                double(15,3)                               ,');
  Qry.SQL.Add('  PESO                 double(15,3)                               ,');
  Qry.SQL.Add('  AREAM2               double(15,2)                               ,');
  Qry.SQL.Add('  AREAP2               double(15,2)                               ,');
  Qry.SQL.Add('  PrcCusUni            double(15,4)                               ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenCad            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_SCC               varchar(30)            DEFAULT "? ? ?"     ,');
  //Qry.SQL.Add('  Difere               int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NCM                  varchar(10)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttGIL_Mov(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa              int(11)                                    ,');
  Qry.SQL.Add('  GraGruX              int(11)                                    ,');
  Qry.SQL.Add('  PrcCusUni            double(15,6) NOT NULL DEFAULT "0.000000"   ,');

  Qry.SQL.Add('  IniQtd               double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  IniVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  InnQtd               double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  OutQtd               double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  OutVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  BalQtd               double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  BalVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  FimQtd               double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  FimVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');

  Qry.SQL.Add('  PrdGrupTip           int(11)                                    ,');
  Qry.SQL.Add('  NO_PGT               varchar(30)                                ,');
  Qry.SQL.Add('  Nivel1               int(11)                                    ,');
  Qry.SQL.Add('  NO_PRD               varchar(511)                               ,');
  Qry.SQL.Add('  Nivel2               int(11)                                    ,');
  Qry.SQL.Add('  NO_Ni2               varchar(30)                                ,');
  Qry.SQL.Add('  Nivel3               int(11)                                    ,');
  Qry.SQL.Add('  NO_Ni3               varchar(30)                                ,');

  Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttListaIgapo(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CodTxt               varchar(60)                                ,');
  Qry.SQL.Add('  NO_PRD               varchar(511)                               ,');
  Qry.SQL.Add('  Unidad               varchar(20)                                ,');
  Qry.SQL.Add('  Preco                double(15,2)                               ,');

  Qry.SQL.Add('  PRIMARY KEY (CodTxt)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttNFe_100(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_nNF              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_serie            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_dEmi             date         NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vProd        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vST          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vFrete       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vSeg         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vIPI         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vOutro       double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vDesc        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vNF          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vBC          double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vICMS        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vPIS         double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ICMSTot_vCOFINS      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  NOME_tpEmis          varchar(30)  NOT NULL                      ,');
  Qry.SQL.Add('  NOME_tpNF            char(1)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_natOp            varchar(60)                                ,');
  //  2015-10-17
  Qry.SQL.Add('  qVol                 double(15,0)                               ,');
  Qry.SQL.Add('  esp                  varchar(60)                                ,');
  Qry.SQL.Add('  marca                varchar(60)                                ,');
  Qry.SQL.Add('  nVol                 varchar(60)                                ,');
  Qry.SQL.Add('  PesoL                double(15,3)                               ,');
  Qry.SQL.Add('  PesoB                double(15,3)                               ');
  //  FIM 2015-10-17
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttNFe_101(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_nNF              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_serie            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_AAMM_AA          char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_AAMM_MM          char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_dEmi             date         NOT NULL                      ,');
  Qry.SQL.Add('  NOME_tpEmis          varchar(30)  NOT NULL                      ,');
  Qry.SQL.Add('  NOME_tpNF            char(1)      NOT NULL                      ,');
  Qry.SQL.Add('  Id                   varchar(44)  NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_dhRecbto     datetime     NOT NULL                      ,');
  Qry.SQL.Add('  infCanc_nProt        varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  Motivo               varchar(255) NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttNFe_CFOP(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  nItem                          int(3)                 DEFAULT "0"           ,');
  Qry.SQL.Add('  prod_cProd                     varchar(60)                                  ,');
  Qry.SQL.Add('  prod_xProd                     varchar(120)                                 ,');
  Qry.SQL.Add('  prod_CFOP                      int(4)                 DEFAULT "0"           ,');
  Qry.SQL.Add('  prod_NCM                       varchar(8)                                   ,');
  Qry.SQL.Add('  prod_uCom                      varchar(6)                                   ,');
  Qry.SQL.Add('  prod_qCom                      double(12,4)           DEFAULT "0.0000"      ,');
  Qry.SQL.Add('  prod_vUnCom                    double(21,10)          DEFAULT "0.0000000000",');
  Qry.SQL.Add('  prod_vProd                     double(15,2)           DEFAULT "0.00"        ,');
  Qry.SQL.Add('  Ordem                          int(1)       NOT NULL  DEFAULT "0"           ,');
  Qry.SQL.Add('  ide_nNF                        int(9)                 DEFAULT "0"           ,');
  Qry.SQL.Add('  ide_serie                      int(3)                 DEFAULT "0"           ,');
  Qry.SQL.Add('  ide_dEmi                       date                                         ,');
  Qry.SQL.Add('  ICMSTot_vProd                  double(15,2)                                 ,');
  Qry.SQL.Add('  ICMSTot_vNF                    double(15,2)                                 ,');
  Qry.SQL.Add('  NOME_tpEmis                    varchar(30)                                  ,');
  Qry.SQL.Add('  NOME_tpNF                      varchar(1)                                   ,');
  Qry.SQL.Add('  ide_natOp                      varchar(60)            DEFAULT "?"           ,');
  Qry.SQL.Add('  qVol                           double(15,0)           DEFAULT "0"           ,');
  Qry.SQL.Add('  esp                            varchar(60)                                  ,');
  Qry.SQL.Add('  marca                          varchar(60)                                  ,');
  Qry.SQL.Add('  nVol                           varchar(60)                                  ,');
  Qry.SQL.Add('  PesoL                          double(15,3)           DEFAULT "0.000"       ,');
  Qry.SQL.Add('  PesoB                          double(15,3)           DEFAULT "0.000"        ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttNFe_XXX(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  cStat                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_nNF              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_serie            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_AAMM_AA          char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_AAMM_MM          char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_dEmi             date         NOT NULL                      ,');
  Qry.SQL.Add('  NOME_tpEmis          varchar(30)  NOT NULL                      ,');
  Qry.SQL.Add('  NOME_tpNF            char(1)      NOT NULL                      ,');
  Qry.SQL.Add('  Status               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Motivo               varchar(255) NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPEDEFD_BLCS(Qry: TmySQLQuery);
begin
{
  Qry.SQL.Add('  Bloco                char(1)      NOT NULL                      ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
  //Qry.SQL.Add('  PRIMARY KEY (Ordem,Bloco)');
}
  Qry.SQL.Add('  Bloco                char(1)      NOT NULL                      ,');
  Qry.SQL.Add('  Registro             varchar(4)   NOT NULL                      ,');
  Qry.SQL.Add('  Nivel                tinyint(3)   NOT NULL                      ,');
  Qry.SQL.Add('  RegisPai             varchar(4)   NOT NULL                      ,');
  Qry.SQL.Add('  OcorAciNiv           int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  OcorEstNiv           int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Implementd           tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  StateIndex           tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPEDEFD_C100(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  LinArq               int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_OPER             char(1)                                    ,');
  Qry.SQL.Add('  IND_EMIT             char(1)                                    ,');
  Qry.SQL.Add('  COD_PART             char(60)                                   ,');
  Qry.SQL.Add('  COD_MOD              char(2)                                    ,');
  Qry.SQL.Add('  COD_SIT              tinyint(2) NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  SER                  char(3)                                    ,');
  Qry.SQL.Add('  NUM_DOC              int(9)     NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  CHV_NFE              varchar(44)                                ,');
  Qry.SQL.Add('  DT_DOC               date                                       ,');
  Qry.SQL.Add('  DT_E_S               date                                       ,');
  Qry.SQL.Add('  VL_DOC               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_PGTO             char(1)                                    ,');
  Qry.SQL.Add('  VL_DESC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ABAT_NT           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_MERC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_FRT              char(1)                                    ,');
  Qry.SQL.Add('  VL_FRT               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SEG               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_OUT_DA            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS_ST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_ST           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_IPI               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_PIS               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_COFINS            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_PIS_ST            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_COFINS_ST         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  Entidade             int(11)                                    ,');
  //
  Qry.SQL.Add('  ICMSTot_vBC          double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vICMS        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vBCST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vST          double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vProd        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vFrete       double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vSeg         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vDesc        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vII          double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vIPI         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vPIS         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vCOFINS      double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vOutro       double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMSTot_vNF          double(15,2) NOT NULL DEFAULT "0.00"        ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPEDESTQ_ERR(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  LinArq               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Erro                 int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  LinPla               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Planilha             varchar(50)                                ,');
  Qry.SQL.Add('  Celula               varchar(30)                                ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  PRIMARY KEY (LinArq, Erro)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPEDESTQ_ITS(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  LinArq               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  LinPla               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  AnoMes               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  SeqLin               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Sit_Prod             tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Terceiro             int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  EstqIniQtd           double(15,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  EstqIniPrc           double(15,4) NOT NULL DEFAULT 0.0000       ,');
  Qry.SQL.Add('  EstqIniVal           double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  ComprasQtd           double(15,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  ComprasPrc           double(15,4) NOT NULL DEFAULT 0.0000       ,');
  Qry.SQL.Add('  ComprasVal           double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  ConsumoQtd           double(15,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  ConsumoPrc           double(15,4) NOT NULL DEFAULT 0.0000       ,');
  Qry.SQL.Add('  ConsumoVal           double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  EstqFimQtd           double(15,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  EstqFimPrc           double(15,4) NOT NULL DEFAULT 0.0000       ,');
  Qry.SQL.Add('  EstqFimVal           double(15,2) NOT NULL DEFAULT 0.00         ,');
  //Qry.SQL.Add('  Exportado            tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  AntDifQtd            double(15,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  AntDifVal            double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  AtuDifQtd            double(15,3) NOT NULL DEFAULT 0.000        ,');
  Qry.SQL.Add('  AtuDifVal            double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Erro                 int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT 0            ,');
  //
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  Planilha             varchar(50)                                ,');
  Qry.SQL.Add('  Celula               varchar(30)                                ,');
  //
  Qry.SQL.Add('  PRIMARY KEY (LinArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPEDESTQ_SUM(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  AnoMes               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  EstqIniQtd           double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  EstqIniPrc           double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  EstqIniVal           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ComprasQtd           double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  ComprasPrc           double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  ComprasVal           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ConsumoQtd           double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  ConsumoPrc           double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  ConsumoVal           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  EstqFimQtd           double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  EstqFimPrc           double(15,4) NOT NULL                      ,');
  Qry.SQL.Add('  EstqFimVal           double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (AnoMes)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPEDINN_0200(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  LinArq               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Distancia            int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Nome                 varchar(120)                               ,');
  Qry.SQL.Add('  COD_ITEM             varchar(60)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  DESCR_ITEM           varchar(255)                               ,');
  Qry.SQL.Add('  COD_ANT_ITEM         varchar(60)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  Levenshtein_Meu      varchar(120)                               ,');
  Qry.SQL.Add('  Levenshtein_Inn      varchar(120)                               ,');
  Qry.SQL.Add('  NivelExclu           int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  PRIMARY KEY (LinArq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0000(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  COD_VER              mediumint(3) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  COD_FIN              tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DT_INI               date                                       ,');
  Qry.SQL.Add('  DT_FIN               date                                       ,');
  Qry.SQL.Add('  NOME                 varchar(100)                               ,');
  Qry.SQL.Add('  CNPJ                 varchar(14)                                ,');
  Qry.SQL.Add('  CPF                  varchar(11)                                ,');
  Qry.SQL.Add('  UF                   char(2)                                    ,');
  Qry.SQL.Add('  IE                   varchar(14)                                ,');
  Qry.SQL.Add('  COD_MUN              int(7)       NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IM                   varchar(255)                               ,');
  Qry.SQL.Add('  SUFRAMA              varchar(9)                                 ,');
  Qry.SQL.Add('  IND_PERFIL           char(1)                                    ,');
  Qry.SQL.Add('  IND_ATIV             tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0001(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_MOV              tinyint(1)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0005(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  FANTASIA             varchar(60)                                ,');
  Qry.SQL.Add('  CEP                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  END                  varchar(60)                                ,');
  Qry.SQL.Add('  NUM                  varchar(10)                                ,');
  Qry.SQL.Add('  COMPL                varchar(60)                                ,');
  Qry.SQL.Add('  BAIRRO               varchar(60)                                ,');
  Qry.SQL.Add('  FONE                 varchar(10)                                ,');
  Qry.SQL.Add('  FAX                  varchar(10)                                ,');
  Qry.SQL.Add('  EMAIL                varchar(255)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0100(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  NOME                 varchar(100)                               ,');
  Qry.SQL.Add('  CPF                  varchar(11)                                ,');
  Qry.SQL.Add('  CRC                  varchar(15)                                ,');
  Qry.SQL.Add('  CNPJ                 varchar(14)                                ,');
  Qry.SQL.Add('  CEP                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  END                  varchar(60)                                ,');
  Qry.SQL.Add('  NUM                  varchar(10)                                ,');
  Qry.SQL.Add('  COMPL                varchar(60)                                ,');
  Qry.SQL.Add('  BAIRRO               varchar(60)                                ,');
  Qry.SQL.Add('  FONE                 varchar(10)                                ,');
  Qry.SQL.Add('  FAX                  varchar(10)                                ,');
  Qry.SQL.Add('  EMAIL                varchar(255)                               ,');
  Qry.SQL.Add('  COD_MUN              int(7)       NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0150(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  COD_PART             varchar(60)                                ,');
  Qry.SQL.Add('  NOME                 varchar(100)                               ,');
  Qry.SQL.Add('  COD_PAIS             int(5)       NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CNPJ                 varchar(14)                                ,');
  Qry.SQL.Add('  CPF                  varchar(11)                                ,');
  Qry.SQL.Add('  IE                   varchar(14)                                ,');
  Qry.SQL.Add('  COD_MUN              int(7)       NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SUFRAMA              varchar(9)                                 ,');
  Qry.SQL.Add('  END                  varchar(60)                                ,');
  Qry.SQL.Add('  NUM                  varchar(10)                                ,');
  Qry.SQL.Add('  COMPL                varchar(60)                                ,');
  Qry.SQL.Add('  BAIRRO               varchar(60)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0190(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  UNID                 varchar(6)                                 ,');
  Qry.SQL.Add('  DESCR                varchar(255)                               ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0200(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  COD_ITEM             varchar(60)                                ,');
  Qry.SQL.Add('  DESCR_ITEM           varchar(255)                               ,');
  Qry.SQL.Add('  COD_BARRA            varchar(255)                               ,');
  Qry.SQL.Add('  COD_ANT_ITEM         varchar(60)                                ,');
  Qry.SQL.Add('  UNID_INV             varchar(6)                                 ,');
  Qry.SQL.Add('  TIPO_ITEM            tinyint(2)                                 ,');
  Qry.SQL.Add('  COD_NCM              varchar(8)                                 ,');
  Qry.SQL.Add('  EX_IPI               varchar(3)                                 ,');
  Qry.SQL.Add('  COD_GEN              tinyint(2)                                 ,');
  Qry.SQL.Add('  COD_LST              int(4)                                     ,');
  Qry.SQL.Add('  ALIQ_ICMS            double(5,2)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0400(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  COD_NAT              varchar(10)                                ,');
  Qry.SQL.Add('  DESCR_NAT            varchar(255)                               ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_0990(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN_0            int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_1001(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_MOV              tinyint(1)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_1990(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN_1            int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_9001(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_MOV              tinyint(1)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_9900(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  REG_BLC              varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_REG_BLC          int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_9990(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN_9            int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_9999(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN              int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_C001(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_MOV              char(1)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_C100(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_OPER             char(1)                                    ,');
  Qry.SQL.Add('  IND_EMIT             char(1)                                    ,');
  Qry.SQL.Add('  COD_PART             char(60)                                   ,');
  Qry.SQL.Add('  COD_MOD              char(2)                                    ,');
  Qry.SQL.Add('  COD_SIT              tinyint(2) NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  SER                  char(3)                                    ,');
  Qry.SQL.Add('  NUM_DOC              int(9)     NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  CHV_NFE              varchar(44)                                ,');
  Qry.SQL.Add('  DT_DOC               date                                       ,');
  Qry.SQL.Add('  DT_E_S               date                                       ,');
  Qry.SQL.Add('  VL_DOC               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_PGTO             char(1)                                    ,');
  Qry.SQL.Add('  VL_DESC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ABAT_NT           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_MERC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_FRT              char(1)                                    ,');
  Qry.SQL.Add('  VL_FRT               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SEG               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_OUT_DA            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS_ST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_ST           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_IPI               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_PIS               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_COFINS            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_PIS_ST            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_COFINS_ST         double(15,2) NOT NULL DEFAULT "0.00"        ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_C170(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  _C100_               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  NUM_ITEM             mediumint(3) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  COD_ITEM             varchar(60)                                ,');
  Qry.SQL.Add('  DESCR_COMPL          varchar(255)                               ,');
  Qry.SQL.Add('  QTD                  double(15,5) NOT NULL DEFAULT "0.00000"    ,');
  Qry.SQL.Add('  UNIT                 varchar(6)                                 ,');
  Qry.SQL.Add('  VL_ITEM              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_DESC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_MOV              char(1)                                    ,');
  Qry.SQL.Add('  CST_ICMS             mediumint(3)                               ,');
  Qry.SQL.Add('  CFOP                 mediumint(4)                               ,');
  Qry.SQL.Add('  COD_NAT              varchar(10)                                ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ALIQ_ICMS            double(6,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS_ST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ALIQ_ST              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_ST           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_APUR             char(1)                                    ,');
  Qry.SQL.Add('  CST_IPI              char(2)                                    ,');
  Qry.SQL.Add('  COD_ENQ              char(3)                                    ,');
  Qry.SQL.Add('  VL_BC_IPI            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ALIQ_IPI             double(6,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_IPI               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  CST_PIS              tinyint(2)                                 ,');
  Qry.SQL.Add('  VL_BC_PIS            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ALIQ_PIS_p           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  QUANT_BC_PIS         double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  ALIQ_PIS_r           double(15,4) NOT NULL DEFAULT "0.0000"     ,');
  Qry.SQL.Add('  VL_PIS               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  CST_COFINS           tinyint(2)                                 ,');
  Qry.SQL.Add('  VL_BC_COFINS         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ALIQ_COFINS_p        double(6,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  QUANT_BC_COFINS      double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  ALIQ_COFINS_r        double(15,4) NOT NULL DEFAULT "0.0000"     ,');
  Qry.SQL.Add('  VL_COFINS            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_CTA              varchar(255)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_C190(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  _C100_               int(11)      NOT NULL DEFAULT "0"          ,'); //  s� um?
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  CST_ICMS             mediumint(3)                               ,');
  Qry.SQL.Add('  CFOP                 mediumint(4)                               ,');
  Qry.SQL.Add('  ALIQ_ICMS            double(6,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_OPR               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS_ST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_ST           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_RED_BC            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_IPI               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_OBS              varchar(6)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_C500(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_OPER             char(1)                                    ,');
  Qry.SQL.Add('  IND_EMIT             char(1)                                    ,');
  Qry.SQL.Add('  COD_PART             char(60)                                   ,');
  Qry.SQL.Add('  COD_MOD              char(2)                                    ,');
  Qry.SQL.Add('  COD_SIT              tinyint(2) NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  SER                  char(4)                                    ,');
  Qry.SQL.Add('  SUB                  mediumint(3)                               ,');
  Qry.SQL.Add('  COD_CONS             char(2)                                    ,');
  Qry.SQL.Add('  NUM_DOC              int(9)     NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  DT_DOC               date                                       ,');
  Qry.SQL.Add('  DT_E_S               date                                       ,');
  Qry.SQL.Add('  VL_DOC               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_DESC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_FORN              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SERV_NT           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_TERC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_DA                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS_ST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_ST           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_INF              varchar(6)                                 ,');
  Qry.SQL.Add('  VL_PIS               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_COFINS            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  TP_LIGACAO           tinyint(1) NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  COD_GRUPO_TENSAO     char(2)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_C590(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  _C500_               int(11)      NOT NULL DEFAULT "0"          ,'); 
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  CST_ICMS             mediumint(3)                               ,');
  Qry.SQL.Add('  CFOP                 mediumint(4)                               ,');
  Qry.SQL.Add('  ALIQ_ICMS            double(6,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_OPR               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS_ST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_ST           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_RED_BC            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_OBS              varchar(6)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_C990(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN_C            int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_D001(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_MOV              char(1)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_D100(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_OPER             char(1)                                    ,');
  Qry.SQL.Add('  IND_EMIT             char(1)                                    ,');
  Qry.SQL.Add('  COD_PART             char(60)                                   ,');
  Qry.SQL.Add('  COD_MOD              char(2)                                    ,');
  Qry.SQL.Add('  COD_SIT              tinyint(2) NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  SER                  char(4)                                    ,');
  Qry.SQL.Add('  SUB                  char(3)                                    ,');
  Qry.SQL.Add('  NUM_DOC              int(9)     NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  CHV_CTE              varchar(44)                                ,');
  Qry.SQL.Add('  DT_DOC               date                                       ,');
  Qry.SQL.Add('  DT_A_P               date                                       ,');
  Qry.SQL.Add('  TP_CTE               tinyint(1) NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  CHV_CTE_REF          varchar(44)                                ,');
  Qry.SQL.Add('  VL_DOC               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_DESC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_FRT              char(1)                                    ,');
  Qry.SQL.Add('  VL_SERV              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_NT                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_INF              varchar(6)                                 ,');
  Qry.SQL.Add('  COD_CTA              varchar(255)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_D190(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  _D100_               int(11)      NOT NULL DEFAULT "0"          ,'); 
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  CST_ICMS             mediumint(3)                               ,');
  Qry.SQL.Add('  CFOP                 mediumint(4)                               ,');
  Qry.SQL.Add('  ALIQ_ICMS            double(6,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_OPR               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_RED_BC            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_OBS              varchar(6)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_D500(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_OPER             char(1)                                    ,');
  Qry.SQL.Add('  IND_EMIT             char(1)                                    ,');
  Qry.SQL.Add('  COD_PART             char(60)                                   ,');
  Qry.SQL.Add('  COD_MOD              char(2)                                    ,');
  Qry.SQL.Add('  COD_SIT              tinyint(2) NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  SER                  char(4)                                    ,');
  Qry.SQL.Add('  SUB                  char(3)                                    ,');
  Qry.SQL.Add('  NUM_DOC              int(9)     NOT NULL DEFAULT "0"            ,');
  Qry.SQL.Add('  DT_DOC               date                                       ,');
  Qry.SQL.Add('  DT_A_P               date                                       ,');
  Qry.SQL.Add('  VL_DOC               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_DESC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SERV              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SERV_NT           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_TERC              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_DA                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_INF              varchar(6)                                 ,');
  Qry.SQL.Add('  VL_PIS               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_COFINS            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_CTA              varchar(255)                               ,');
  Qry.SQL.Add('  TP_ASSINANTE         tinyint(1)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_D590(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  _D500_               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  CST_ICMS             mediumint(3)                               ,');
  Qry.SQL.Add('  CFOP                 mediumint(4)                               ,');
  Qry.SQL.Add('  ALIQ_ICMS            double(6,2)  NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_OPR               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_BC_ICMS_ST        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_ST           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_RED_BC            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  COD_OBS              varchar(6)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_D990(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN_D            int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_E001(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_MOV              char(1)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_E100(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  DT_INI               date                                       ,');
  Qry.SQL.Add('  DT_FIN               date                                       ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_E110(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  _E100_               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  VL_TOT_DEBITOS       double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_AJ_DEBITOS        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_TOT_AJ_DEBITOS    double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ESTORNOS_CRED     double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_TOT_CREDITOS      double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_AJ_CREDITOS       double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_TOT_AJ_CREDITOS   double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ESTORNOS_DEB      double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SLD_CREDOR_ANT    double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SLD_APURADO       double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_TOT_DED           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_ICMS_RECOLHER     double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  VL_SLD_CREDOR_TRANSPORTAR double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  DEB_ESP              double(15,2) NOT NULL DEFAULT "0.00"        ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_E990(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN_E            int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_H001(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  IND_MOV              char(1)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_H005(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  DT_INV               date                                       ,');
  Qry.SQL.Add('  VL_INV               double(15,2) NOT NULL DEFAULT "0.00"        ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_H010(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  COD_ITEM             varchar(60)                                ,');
  Qry.SQL.Add('  UNID                 varchar(6)                                 ,');
  Qry.SQL.Add('  QTD                  double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  VL_UNIT              double(15,6) NOT NULL DEFAULT "0.000000"   ,');
  Qry.SQL.Add('  VL_ITEM              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  IND_PROP             char(1)                                    ,');
  Qry.SQL.Add('  COD_PART             varchar(60)                                ,');
  Qry.SQL.Add('  TXT_COMPL            varchar(255)                               ,');
  Qry.SQL.Add('  COD_CTA              varchar(255)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttSPED_EFD_0_H990(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  _lin_arq_            int(11)                                    ,');
  Qry.SQL.Add('  REG                  varchar(4)                                 ,');
  Qry.SQL.Add('  QTD_LIN_H            int(11)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUnGradeCreate.Cria_ntrttVSGraCorSel(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Nome                           varchar(30)                                ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TUnGradeCreate.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable_Grade; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrttListaIgapo:      Nome := Lowercase('_Lista_Igapo_');
      ntrttFatDivRef:       Nome := Lowercase('FatDivRef');
      //
      ntrttNFe_100:         Nome := Lowercase('NFe_100');
      ntrttNFe_101:         Nome := Lowercase('NFe_101');
      ntrttNFe_XXX:         Nome := Lowercase('NFe_XXX');
      ntrttNFe_CFOP:        Nome := Lowercase('_NFe_CFOP_');
      //
      ntrttGIL_Mov:         Nome := Lowercase('_GIL_MOV_');
      //
      ntrttSintegra10:      Nome := Lowercase('Sintegra10');
      ntrttSintegra11:      Nome := Lowercase('Sintegra11');
      ntrttSintegra50:      Nome := Lowercase('Sintegra50');
      ntrttSintegra51:      Nome := Lowercase('Sintegra51');
      ntrttSintegra53:      Nome := Lowercase('Sintegra53');
      ntrttSintegra54:      Nome := Lowercase('Sintegra54');
      ntrttSintegra70:      Nome := Lowercase('Sintegra70');
      ntrttSintegra71:      Nome := Lowercase('Sintegra71');
      ntrttSintegra74:      Nome := Lowercase('Sintegra74');
      ntrttSintegra75:      Nome := Lowercase('Sintegra75');
      ntrttSintegra85:      Nome := Lowercase('Sintegra85');
      ntrttSintegra86:      Nome := Lowercase('Sintegra86');
      ntrttSintegra88:      Nome := Lowercase('Sintegra88');
      ntrttSintegra90:      Nome := Lowercase('Sintegra90');
      ntrttSintegraPM:      Nome := Lowercase('SintegraPM');
      ntrttSintegraEC:      Nome := Lowercase('SintegraEC');
      ntrttSintegraEr:      Nome := Lowercase('SintegraEr');
      //
      ntrttS50Load:         Nome := Lowercase('Load_s50');
      ntrttS54Load:         Nome := Lowercase('Load_s54');
      ntrttS70Load:         Nome := Lowercase('Load_s70');
      ntrttS75Load:         Nome := Lowercase('Load_s75');
      //
      ntrttStqMovValX:      Nome := Lowercase('StqMovValX');
      //
      ntrttSPED_EFD_0_0000: Nome := Lowercase('SPED_EFD_0_0000');
      ntrttSPED_EFD_0_0001: Nome := Lowercase('SPED_EFD_0_0001');
      ntrttSPED_EFD_0_0005: Nome := Lowercase('SPED_EFD_0_0005');
      ntrttSPED_EFD_0_0100: Nome := Lowercase('SPED_EFD_0_0100');
      ntrttSPED_EFD_0_0150: Nome := Lowercase('SPED_EFD_0_0150');
      ntrttSPED_EFD_0_0190: Nome := Lowercase('SPED_EFD_0_0190');
      ntrttSPED_EFD_0_0200: Nome := Lowercase('SPED_EFD_0_0200');
      ntrttSPED_EFD_0_0400: Nome := Lowercase('SPED_EFD_0_0400');
      ntrttSPED_EFD_0_0990: Nome := Lowercase('SPED_EFD_0_0990');
      ntrttSPED_EFD_0_C001: Nome := Lowercase('SPED_EFD_0_C001');
      ntrttSPED_EFD_0_C100: Nome := Lowercase('SPED_EFD_0_C100');
      ntrttSPED_EFD_0_C170: Nome := Lowercase('SPED_EFD_0_C170');
      ntrttSPED_EFD_0_C190: Nome := Lowercase('SPED_EFD_0_C190');
      ntrttSPED_EFD_0_C500: Nome := Lowercase('SPED_EFD_0_C500');
      ntrttSPED_EFD_0_C590: Nome := Lowercase('SPED_EFD_0_C590');
      ntrttSPED_EFD_0_C990: Nome := Lowercase('SPED_EFD_0_C990');
      ntrttSPED_EFD_0_D001: Nome := Lowercase('SPED_EFD_0_D001');
      ntrttSPED_EFD_0_D100: Nome := Lowercase('SPED_EFD_0_D100');
      ntrttSPED_EFD_0_D190: Nome := Lowercase('SPED_EFD_0_D190');
      ntrttSPED_EFD_0_D500: Nome := Lowercase('SPED_EFD_0_D500');
      ntrttSPED_EFD_0_D590: Nome := Lowercase('SPED_EFD_0_D590');
      ntrttSPED_EFD_0_D990: Nome := Lowercase('SPED_EFD_0_D990');
      ntrttSPED_EFD_0_E001: Nome := Lowercase('SPED_EFD_0_E001');
      ntrttSPED_EFD_0_E100: Nome := Lowercase('SPED_EFD_0_E100');
      ntrttSPED_EFD_0_E110: Nome := Lowercase('SPED_EFD_0_E110');
      ntrttSPED_EFD_0_E990: Nome := Lowercase('SPED_EFD_0_E990');
      ntrttSPED_EFD_0_H001: Nome := Lowercase('SPED_EFD_0_H001');
      ntrttSPED_EFD_0_H005: Nome := Lowercase('SPED_EFD_0_H005');
      ntrttSPED_EFD_0_H010: Nome := Lowercase('SPED_EFD_0_H010');
      ntrttSPED_EFD_0_H990: Nome := Lowercase('SPED_EFD_0_H990');
      ntrttSPED_EFD_0_1001: Nome := Lowercase('SPED_EFD_0_1001');
      ntrttSPED_EFD_0_1990: Nome := Lowercase('SPED_EFD_0_1990');
      ntrttSPED_EFD_0_9001: Nome := Lowercase('SPED_EFD_0_9001');
      ntrttSPED_EFD_0_9900: Nome := Lowercase('SPED_EFD_0_9900');
      ntrttSPED_EFD_0_9990: Nome := Lowercase('SPED_EFD_0_9990');
      ntrttSPED_EFD_0_9999: Nome := Lowercase('SPED_EFD_0_9999');
      //
      ntrttSPEDEFD_C100: Nome := Lowercase('_SPEDEFD_C100_');
      //
      ntrttSPEDEFD_BLCS: Nome := Lowercase('_SPEDEFD_BLCS_');
      //
      ntrttSPEDESTQ_ITS: Nome := Lowercase('_SPEDESTQ_ITS_');
      ntrttSPEDESTQ_SUM: Nome := Lowercase('_SPEDESTQ_SUM_');
      ntrttSPEDESTQ_ERR: Nome := Lowercase('_SPEDESTQ_ERR_');
      ntrttSPEDINN_0200: Nome := Lowercase('_SPEDINN_0200_');
      //
      ntrttGGX_SCC_Stq: Nome := Lowercase('_GGX_SCC_Stq_');
      //
      ntrttVSGraCorSel:       Nome := Lowercase('_gra_cor_sel_');

      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)');
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrttListaIgapo: Cria_ntrttListaIgapo(Qry);
    ntrttFatDivRef:
    begin
{
      Qry.SQL.Add('  ID                   int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  IDCtrl               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Tipo                 tinyint(3)   NOT NULL                      ,');
      Qry.SQL.Add('  OriCodi              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  OriCtrl              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  OriCnta              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqInReduz           int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
}
      Qry.SQL.Add('  FatPedCab            int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  nItem                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  StqCenCad            int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Referencia           varchar(25)  NOT NULL DEFAULT "?"          ,');    // 2012-08-07 de 10 para 25
      Qry.SQL.Add('  GraGruX              int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  NO_GGX               varchar(100) NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  Qtde                 double(15,3) NOT NULL DEFAULT "0.000"      ,');
      Qry.SQL.Add('  PrecoOri             double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  PrecoPrz             double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  PrecoAut             double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  PrecoVen             double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  ValorTotBru          double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerDescoVal          double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValorTotLiq          double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissF           double(8,6)  NOT NULL DEFAULT "0.000000"   ,');
      //Qry.SQL.Add('  ValComisBruF         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      //Qry.SQL.Add('  ValComisDesF         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValComisLiqF         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissR           double(8,6)  NOT NULL DEFAULT "0.000000"   ,');
      //Qry.SQL.Add('  ValComisBruR         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      //Qry.SQL.Add('  ValComisDesR         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValComisLiqR         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissT           double(8,6)  NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  ValComisBruT         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValComisDesT         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValComisLiqT         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissNiv         char(1)      NOT NULL DEFAULT "?"          ,');
{
      Qry.SQL.Add('  CFOP                 varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  InfAdCuztm           int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  PercCustom           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  MedidaC              double(15,4) NOT NULL                      ,');
      Qry.SQL.Add('  MedidaL              double(15,4) NOT NULL                      ,');
      Qry.SQL.Add('  MedidaA              double(15,4) NOT NULL                      ,');
      Qry.SQL.Add('  MedidaE              double(15,6) NOT NULL                      ,');
      Qry.SQL.Add('  MedOrdem             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  TipoNF               tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  refNFe               varchar(44)  NOT NULL                      ,');
      Qry.SQL.Add('  modNF                tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  Serie                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  nNF                  int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SitDevolu            tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  Servico              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  CFOP_Contrib         tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  CFOP_MesmaUF         tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  CFOP_Proprio         tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  RefProd              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  prod_indTot          tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  CSOSN                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  pCredSN              double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  FinaliCli            tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  AlterWeb             tinyint(1)   NOT NULL                      ,');
}
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttGIL_Mov:  Cria_ntrttGIL_Mov(Qry);
    //
    ntrttNFe_100:  Cria_ntrttNFe_100(Qry);
    ntrttNFe_101:  Cria_ntrttNFe_101(Qry);
    ntrttNFe_XXX:  Cria_ntrttNFe_XXX(Qry);
    ntrttNFe_CFOP: Cria_ntrttNFe_CFOP(Qry);
    //
    {
    ntrttFatDivSum:
    begin
      Qry.SQL.Add('  FatPedCab            int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Itens                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Qtde                 double(15,3) NOT NULL DEFAULT "0.000"      ,');
      Qry.SQL.Add('  ValorTotBru          double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerDescoVal          double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValorTotLiq          double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissF           double(8,6)  NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  ValComisLiqF         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissR           double(8,6)  NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  ValComisLiqR         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissT           double(8,6)  NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  ValComisBruT         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValComisDesT         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ValComisLiqT         double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  PerComissNiv         char(1)      NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    }
    ntrttSintegra10:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  NCONTRIB             varchar(35)  NOT NULL                      ,');
      Qry.SQL.Add('  MUNICIPIO            varchar(30)  NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  FAX                  varchar(10)  NOT NULL                      ,');
      Qry.SQL.Add('  DATAINI              date         NOT NULL                      ,');
      Qry.SQL.Add('  DATAFIN              date         NOT NULL                      ,');
      Qry.SQL.Add('  CODCONV              char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  CODNAT               char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  CODFIN               char(1)      NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra11:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  LOGRADOURO           varchar(34)  NOT NULL                      ,');
      Qry.SQL.Add('  NUMERO               varchar(5)   NOT NULL                      ,');
      Qry.SQL.Add('  COMPLEMENT           varchar(22)  NOT NULL                      ,');
      Qry.SQL.Add('  BAIRRO               varchar(15)  NOT NULL                      ,');
      Qry.SQL.Add('  CEP                  varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  CONTATO              varchar(28)  NOT NULL                      ,');
      Qry.SQL.Add('  TELEFONE             varchar(12)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra50:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CODNATOP             varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  EMITENTE             char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  VALORTOT             varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  BASEICMS             varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  VALORICMS            varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRISENTO            varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  OUTROS               varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  ALIQUOTA             varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  SITUACAO             char(1)      NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra51:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CODNATOP             varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  TOTALNF              varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRTOTIPI            varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRISENTO            varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  OUTROS               varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  BRANCOS              varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  SITUACAO             char(1)      NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra53:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMERO               varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CFOP                 varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  EMITENTE             char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  BASECALC             varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMSRETIDO           varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  DESPACESS            varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  SITUACAO             char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  CODANTEC             char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  BRANCOS              varchar(29)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra54:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CODNATOP             varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  SITTRIB              char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMITEM              char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  CODPROD              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  QTDADE               varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRPROD              varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRDESC              varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  BASECALC             varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  BASESUBTRI           varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRIPI               varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  ALIQICMS             varchar(4)   NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra70:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  SUBSERIE             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CODNATOP             varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  VALORTOT             varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  BASEICMS             varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  VALORICMS            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRISENTO            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  OUTROS               varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  CIF_FOB              char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  SITUACAO             char(1)      NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra71:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAOC             date         NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  MODELOC              char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIEC               char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  SUBSERIE             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMEROC              varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  UFREM                char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJREM              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCESTREM           varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAON             date         NOT NULL                      ,');
      Qry.SQL.Add('  MODELON              char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIEN               char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  TOTALNF              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  BRANCOS              varchar(12)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra74:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  DATA                 date         NOT NULL                      ,');
      Qry.SQL.Add('  CODIPROD             varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  QTDADE               varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRPROD              varchar(13)  NOT NULL                      ,');
      Qry.SQL.Add('  CODPOSSEM            char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  BRANCOS              varchar(45)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra75:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  DATAINI              date         NOT NULL                      ,');
      Qry.SQL.Add('  DATAFIN              date         NOT NULL                      ,');
      Qry.SQL.Add('  CODPROD              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  CODNCM               varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  DESCRICAO            varchar(53)  NOT NULL                      ,');
      Qry.SQL.Add('  UNIDADE              varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  ALIQIPI              varchar(5)   NOT NULL                      ,');
      Qry.SQL.Add('  ALIQICMS             varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  REDBASEICM           varchar(5)   NOT NULL                      ,');
      Qry.SQL.Add('  BASESUBTRI           varchar(13)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra85:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  DECLARACAO           varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  DATADECLAR           date         NOT NULL                      ,');
      Qry.SQL.Add('  NATUREZA             char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  REGISTRO             varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  DATAREG              date         NOT NULL                      ,');
      Qry.SQL.Add('  CONHECIM             varchar(16)  NOT NULL                      ,');
      Qry.SQL.Add('  DATACONHEC           date         NOT NULL                      ,');
      Qry.SQL.Add('  TIPOCONHEC           char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  PAIS                 varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  RESERVADO            varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  DATAAVERB            date         NOT NULL                      ,');
      Qry.SQL.Add('  NUMNFEXP             varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  BRANCOS              varchar(19)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra86:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  REGEXPORT            varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  DATAREG              date         NOT NULL                      ,');
      Qry.SQL.Add('  CNPJREM              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  CODPROD              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  QTDADE               varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRUNIT              varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  VLRPROD              varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  RELACIONA            char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  BRANCOS              varchar(5)   NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra88:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SUBTIPO              char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMERO               varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CFOP                 varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  CST                  char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMITEM              char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  CODPROD              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  NUMSER               varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  BRANCOS              varchar(52)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegra90:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG              char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG               varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG1             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG1              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG2             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG2              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG3             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG3              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG4             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG4              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG5             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG5              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG6             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG6              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG7             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG7              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  TIPOREG8             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  TOTREG8              varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  BRANCO               varchar(5)   NOT NULL                      ,');
      Qry.SQL.Add('  NUMTIP90             char(1)      NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegraEC:
    begin
      Qry.SQL.Add('  Campo                varchar(20)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT 1             ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegraEr:
    begin
      Qry.SQL.Add('  TipoReg              tinyint(2)   NOT NULL DEFAULT 0            ,');
      Qry.SQL.Add('  ItemReg              int(11)      NOT NULL DEFAULT 0            ,');
      Qry.SQL.Add('  Campo                varchar(20)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  ValMy                varchar(255)                               ,');
      Qry.SQL.Add('  ValCo                varchar(255)                               ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT 1             ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSintegraPM:
    begin
      Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT 0            ,');
      Qry.SQL.Add('  Codigo               varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  Sigla                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  NCM                  varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  IPI_Alq              double(4,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMSAliqSINTEGRA     double(4,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pRedBC          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pRedBCST        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSST_BaseSINTEGRA  double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Nome                 varchar(53)  NOT NULL                      ,');
      Qry.SQL.Add('  Itens                int(11)      NOT NULL DEFAULT 1            ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT 1            ,');
      Qry.SQL.Add('  PRIMARY KEY (Controle, Codigo)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttS50Load:
    begin
      Qry.SQL.Add('  TIPO                 tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  IE                   varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CFOP                 varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  EMITENTE             char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  ValorNF              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  BCICMS               double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  ValICMS              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  ValIsento            double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  ValNCICMS            double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  AliqICMS             double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  Terceiro             int(11)      NOT NULL  DEFAULT 0           ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 1            ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttS54Load:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CFOP                 varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  CST                  char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMITEM              char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  CODPROD              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  QTDADE               double(15,3) NOT NULL  DEFAULT "0.000"     ,');
      Qry.SQL.Add('  VLRPROD              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  VLRDESC              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  BASECALC             double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  BASESUBTRI           double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  VLRIPI               double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  ALIQICMS             double(4,2)  NOT NULL  DEFAULT "0.00"      ,');
      //Qry.SQL.Add('  GraGruX              int(11)      NOT NULL  DEFAULT 1           ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 1            ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttS70Load:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  CNPJ                 varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  INSCEST              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  EMISSAO              date         NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  MODELO               char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  SERIE                char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  SUBSERIE             char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  NUMNF                varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  CFOP                 varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  VALORTOT             double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  BASEICMS             double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  VALORICMS            double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  VLRISENTO            double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  OUTROS               double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  CIF_FOB              tinyint(1)   NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  SITUACAO             char(1)      NOT NULL                      ,');
      Qry.SQL.Add('  Transporta           int(11)      NOT NULL  DEFAULT 1           ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 1            ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttS75Load:
    begin
      Qry.SQL.Add('  TIPO                 char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  DATAINI              date         NOT NULL                      ,');
      Qry.SQL.Add('  DATAFIN              date         NOT NULL                      ,');
      Qry.SQL.Add('  CODPROD              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  GraGruX              int(11)      NOT NULL  DEFAULT 0           ,');
      Qry.SQL.Add('  Nivel1               int(11)      NOT NULL  DEFAULT 0           ,');
      Qry.SQL.Add('  CODNCM               varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  DESCRICAO            varchar(53)  NOT NULL                      ,');
      Qry.SQL.Add('  UNIDADE              varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  ALIQIPI              double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  ALIQICMS             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  REDBASEICM           double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  BASESUBTRI           double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT 1            ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttStqMovValX:
    begin
      Qry.SQL.Add('  ID                   int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  GraGruX              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  CFOP                 varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  Qtde                 double(15,3) NOT NULL                      ,');
      Qry.SQL.Add('  Preco                double(15,6) NOT NULL                      ,');
      Qry.SQL.Add('  Total                double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  CSOSN                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  pCredSN              double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  NO_PRD               varchar(511)  NOT NULL                      ,');
      Qry.SQL.Add('  NO_TAM               varchar(5)   NOT NULL                      ,');
      Qry.SQL.Add('  NO_COR               varchar(30)  NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSPED_EFD_0_0000: Cria_ntrttSPED_EFD_0_0000(Qry);
    ntrttSPED_EFD_0_0001: Cria_ntrttSPED_EFD_0_0001(Qry);
    ntrttSPED_EFD_0_0005: Cria_ntrttSPED_EFD_0_0005(Qry);
    ntrttSPED_EFD_0_0100: Cria_ntrttSPED_EFD_0_0100(Qry);
    ntrttSPED_EFD_0_0150: Cria_ntrttSPED_EFD_0_0150(Qry);
    ntrttSPED_EFD_0_0190: Cria_ntrttSPED_EFD_0_0190(Qry);
    ntrttSPED_EFD_0_0200: Cria_ntrttSPED_EFD_0_0200(Qry);
    ntrttSPED_EFD_0_0400: Cria_ntrttSPED_EFD_0_0400(Qry);
    ntrttSPED_EFD_0_0990: Cria_ntrttSPED_EFD_0_0990(Qry);
    ntrttSPED_EFD_0_C001: Cria_ntrttSPED_EFD_0_C001(Qry);
    ntrttSPED_EFD_0_C100: Cria_ntrttSPED_EFD_0_C100(Qry);
    ntrttSPED_EFD_0_C170: Cria_ntrttSPED_EFD_0_C170(Qry);
    ntrttSPED_EFD_0_C190: Cria_ntrttSPED_EFD_0_C190(Qry);
    ntrttSPED_EFD_0_C500: Cria_ntrttSPED_EFD_0_C500(Qry);
    ntrttSPED_EFD_0_C590: Cria_ntrttSPED_EFD_0_C590(Qry);
    ntrttSPED_EFD_0_C990: Cria_ntrttSPED_EFD_0_C990(Qry);
    ntrttSPED_EFD_0_D001: Cria_ntrttSPED_EFD_0_D001(Qry);
    ntrttSPED_EFD_0_D100: Cria_ntrttSPED_EFD_0_D100(Qry);
    ntrttSPED_EFD_0_D190: Cria_ntrttSPED_EFD_0_D190(Qry);
    ntrttSPED_EFD_0_D500: Cria_ntrttSPED_EFD_0_D500(Qry);
    ntrttSPED_EFD_0_D590: Cria_ntrttSPED_EFD_0_D590(Qry);
    ntrttSPED_EFD_0_D990: Cria_ntrttSPED_EFD_0_D990(Qry);
    ntrttSPED_EFD_0_E001: Cria_ntrttSPED_EFD_0_E001(Qry);
    ntrttSPED_EFD_0_E100: Cria_ntrttSPED_EFD_0_E100(Qry);
    ntrttSPED_EFD_0_E110: Cria_ntrttSPED_EFD_0_E110(Qry);
    ntrttSPED_EFD_0_E990: Cria_ntrttSPED_EFD_0_E990(Qry);
    ntrttSPED_EFD_0_H001: Cria_ntrttSPED_EFD_0_H001(Qry);
    ntrttSPED_EFD_0_H005: Cria_ntrttSPED_EFD_0_H005(Qry);
    ntrttSPED_EFD_0_H010: Cria_ntrttSPED_EFD_0_H010(Qry);
    ntrttSPED_EFD_0_H990: Cria_ntrttSPED_EFD_0_H990(Qry);
    ntrttSPED_EFD_0_1001: Cria_ntrttSPED_EFD_0_1001(Qry);
    ntrttSPED_EFD_0_1990: Cria_ntrttSPED_EFD_0_1990(Qry);
    ntrttSPED_EFD_0_9001: Cria_ntrttSPED_EFD_0_9001(Qry);
    ntrttSPED_EFD_0_9900: Cria_ntrttSPED_EFD_0_9900(Qry);
    ntrttSPED_EFD_0_9990: Cria_ntrttSPED_EFD_0_9990(Qry);
    ntrttSPED_EFD_0_9999: Cria_ntrttSPED_EFD_0_9999(Qry);
    //
    ntrttSPEDEFD_C100: Cria_ntrttSPEDEFD_C100(Qry);
    //
    ntrttSPEDEFD_BLCS: Cria_ntrttSPEDEFD_BLCS(Qry);
    //
    ntrttSPEDESTQ_ITS: Cria_ntrttSPEDESTQ_ITS(Qry);
    ntrttSPEDESTQ_SUM: Cria_ntrttSPEDESTQ_SUM(Qry);
    ntrttSPEDESTQ_ERR: Cria_ntrttSPEDESTQ_ERR(Qry);
    ntrttSPEDINN_0200: Cria_ntrttSPEDINN_0200(Qry);
    //
    ntrttGGX_SCC_Stq    : Cria_ntrttGGX_SCC_Stq(Qry);
    //
    ntrttVSGraCorSel:       Cria_ntrttVSGraCorSel(Qry);
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

end.

