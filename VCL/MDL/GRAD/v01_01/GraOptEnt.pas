unit GraOptEnt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkRadioGroup, Mask, dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmGraOptEnt = class(TForm)
    PnMostra: TPanel;
    Panel3: TPanel;
    PnEditar: TPanel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label1: TLabel;
    PCMostra: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    DBRadioGroup1: TDBRadioGroup;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    PCEditar: TPageControl;
    TabSheet2: TTabSheet;
    GroupBox3: TGroupBox;
    RGBUCTipoPreco: TdmkRadioGroup;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    EdBUCGraCusPrc: TdmkEditCB;
    CBBUCGraCusPrc: TdmkDBLookupComboBox;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    QrGraOptEnt: TmySQLQuery;
    DsGraOptEnt: TDataSource;
    QrGraOptEntBUCTipoPreco: TSmallintField;
    QrGraOptEntBUCGraCusPrc: TIntegerField;
    QrGraOptEntCodigo: TIntegerField;
    QrGraOptEntNO_GCP: TWideStringField;
    DBEdit2: TDBEdit;
    QrGraOptEntCU_GCP: TIntegerField;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    QrGraCusPrcCusPrc: TWideStringField;
    QrGraCusPrcNomeTC: TWideStringField;
    QrGraCusPrcNOMEMOEDA: TWideStringField;
    QrGraCusPrcSIGLAMOEDA: TWideStringField;
    DsGraCusPrc: TDataSource;
    VUBUCGraCusPrc: TdmkValUsu;
    Panel5: TPanel;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    DBEdit3: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox5: TGroupBox;
    Panel2: TPanel;
    Panel8: TPanel;
    BtAltera: TBitBtn;
    BitBtn1: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure QrGraOptEntBeforeClose(DataSet: TDataSet);
    procedure QrGraOptEntAfterOpen(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure RGBUCTipoPrecoClick(Sender: TObject);
  private
    { Private declarations }
    FEntidade: Integer;
  public
    { Public declarations }
  end;

  var
  FmGraOptEnt: TFmGraOptEnt;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraOptEnt.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmGraOptEnt.BtAlteraClick(Sender: TObject);
var
  SQLType: TSQLType;
begin
  if QrGraOptEnt.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  UMyMod.ConfigPanelInsUpd(SQLType, Self, PnEditar, QrGraOptEnt, [PnMostra],
  [PnEditar], RGBUCTipoPreco, ImgTipo, 'graoptent');
  // For�ar quando n�o esta inclu�do ainda a entidade na tabela GraOptEnt
  EdCodigo.ValueVariant := EdEntidade.ValueVariant;
end;

procedure TFmGraOptEnt.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := FEntidade;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEditar,
  'graoptent', Codigo, Dmod.QrUpd, [PnEditar], [PnMostra], ImgTipo, True) then
  begin
    EdEntidadeChange(Self);
  end;
end;

procedure TFmGraOptEnt.BtSaidaClick(Sender: TObject);
begin
  UMyMod.UpdUnlockY(EdCodigo.ValueVariant, Dmod.MyDB, 'graoptent', 'Codigo');
  PnMostra.Visible := True;
  Pneditar.Visible := False;
end;

procedure TFmGraOptEnt.EdEntidadeChange(Sender: TObject);
begin
  QrGraOptEnt.Close;
  FEntidade := EdEntidade.ValueVariant;
  if FEntidade <> 0 then
  begin
    QrGraOptEnt.Params[0].AsInteger := FEntidade;
    UnDmkDAC_PF.AbreQuery(QrGraOptEnt, Dmod.MyDB);
  end;
end;

procedure TFmGraOptEnt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraOptEnt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FEntidade := 0;
  PCMostra.Align := alClient;
  PCEditar.Align := alClient;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
end;

procedure TFmGraOptEnt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraOptEnt.QrGraOptEntAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := FEntidade <> 0;
end;

procedure TFmGraOptEnt.QrGraOptEntBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
end;

procedure TFmGraOptEnt.RGBUCTipoPrecoClick(Sender: TObject);
begin
  if RGBUCTipoPreco.ItemIndex = 0 then
  begin
    Geral.MB_Info('O aplicativo n�o est� imlementado para esta op��o!' +
    sLineBreak + 'Solicite a implementa��o � DERMATK!');
    //
    RGBUCTipoPreco.ItemIndex := 1;
  end;
end;

end.
