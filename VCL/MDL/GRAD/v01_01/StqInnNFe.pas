unit StqInnNFe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkImage, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmStqInnNFe = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Label3: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    QrStqInnNFe_: TmySQLQuery;
    DsStqInnNFe: TDataSource;
    DBGrid1: TDBGrid;
    QrGraGruX: TmySQLQuery;
    QrStqInnNFe_prod_cProd: TWideStringField;
    QrStqInnNFe_prod_xProd: TWideStringField;
    QrStqInnNFe_prod_uCom: TWideStringField;
    QrStqInnNFe_prod_qCom: TFloatField;
    QrStqInnNFe_prod_vUnCom: TFloatField;
    QrStqInnNFe_prod_vProd: TFloatField;
    QrStqInnNFe_Nivel1: TIntegerField;
    QrStqInnNFe_GraGruX: TIntegerField;
    QrStqInnNFe_nItem: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrStqInnOpt: TmySQLQuery;
    DsStqInnOpt: TDataSource;
    QrStqInnOptnItem: TIntegerField;
    QrStqInnOptGraGruX: TIntegerField;
    QrStqInnOptNO_Cor: TWideStringField;
    QrStqInnOptNO_Tam: TWideStringField;
    PnOpt: TPanel;
    DBGrid2: TDBGrid;
    Label1: TLabel;
    QrErros: TmySQLQuery;
    QrStqInnNFe_Pecas: TFloatField;
    QrStqInnNFe_Peso: TFloatField;
    QrStqInnNFe_M2: TFloatField;
    QrStqInnNFe_P2: TFloatField;
    TbStqInnNFe: TmySQLTable;
    TbStqInnNFenItem: TIntegerField;
    TbStqInnNFeprod_cProd: TWideStringField;
    TbStqInnNFeprod_xProd: TWideStringField;
    TbStqInnNFeprod_uCom: TWideStringField;
    TbStqInnNFeprod_qCom: TFloatField;
    TbStqInnNFeprod_vUnCom: TFloatField;
    TbStqInnNFeprod_vProd: TFloatField;
    TbStqInnNFeNivel1: TIntegerField;
    TbStqInnNFeGraGruX: TIntegerField;
    TbStqInnNFePecas: TFloatField;
    TbStqInnNFePeso: TFloatField;
    TbStqInnNFeM2: TFloatField;
    TbStqInnNFeP2: TFloatField;
    Label2: TLabel;
    QrErrosNeedPc: TIntegerField;
    QrErrosNeedM2: TIntegerField;
    QrErrosNeedKg: TIntegerField;
    QrErrosnItem: TIntegerField;
    QrErrosPecas: TFloatField;
    QrErrosPeso: TFloatField;
    QrErrosM2: TFloatField;
    QrErrosGraGruX: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    dmkEdit1: TdmkEdit;
    BtSaida: TBitBtn;
    TbStqInnNFeprod_vFrete: TFloatField;
    TbStqInnNFeprod_vDesc: TFloatField;
    TbStqInnNFeprod_vSeg: TFloatField;
    TbStqInnNFeprod_vOutro: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrStqInnOptBeforeClose(DataSet: TDataSet);
    procedure QrStqInnOptAfterOpen(DataSet: TDataSet);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TbStqInnNFeAfterScroll(DataSet: TDataSet);
    procedure TbStqInnNFeBeforeClose(DataSet: TDataSet);
    procedure TbStqInnNFeBeforePost(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbStqInnNFeM2Change(Sender: TField);
    procedure TbStqInnNFeP2Change(Sender: TField);
  private
    { Private declarations }
    FIniciou: Boolean;
    procedure AcoesIniciais();
    procedure ReopenStqInnNFe(nItem: Integer);
    procedure SetaGraGruX(GraGruX: Integer);
  public
    { Public declarations }
    FFatID, FFatNum, FTipo, FOriCodi, FOriCnta, FEmpresa, FOriPart, FFatorClas: Integer;
    FStqInnNFe, FStqInnOpt, FDataHora: String;
  end;

  var
  FmStqInnNFe: TFmStqInnNFe;

implementation

{$R *.DFM}

uses UnMyObjects, ModuleGeral, Module, UMySQLModule, dmkGeral,
{$IfNDef semNFe_v0000}ModuleNFe_0000,{$EndIf}
StqInnCad, DmkDAC_PF;

{
procedure TFmStqInnNFe.AcoesIniciais();
begin
  FIniciou := True;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FStqInnNFe);
  DmodG.QrUpdPID1.SQL.Add('SELECT nItem, prod_cProd, prod_xProd,');
  DmodG.QrUpdPID1.SQL.Add('prod_uCom, prod_qCom, prod_vUnCom,');
  DmodG.QrUpdPID1.SQL.Add('prod_vProd, Nivel1, 0 GraGruX, ');
  DmodG.QrUpdPID1.SQL.Add('0 Pecas, 0 Peso, 0 M2, 0 P2');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + Lowercase(Dmod.MyDB.DatabaseName) + '.nfeitsi');
  DmodG.QrUpdPID1.SQL.Add('WHERE FatID=:P0');
  DmodG.QrUpdPID1.SQL.Add('AND FatNum=:P1');
  DmodG.QrUpdPID1.Params[00].AsInteger := FFatID;
  DmodG.QrUpdPID1.Params[01].AsInteger := FFatNum;
  DmodG.QrUpdPID1.ExecSQL;
  //
  ReopenStqInnNFe(0);
  QrStqInnNFe.First;
  while not QrStqInnNFe.Eof do
  begin
    QrGraGruX.Close;
    QrGraGruX.Params[0].AsInteger := QrStqInnNFeNivel1.Value;
    UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
    if QrGraGruX.RecordCount = 1 then
    begin
      UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, 'stqinnnfe', False, [
      'GraGruX'], ['nItem'], [QrGraGruXControle.Value], [
      QrStqInnNFenItem.Value], False);
    end else begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FStqInnOpt);
      DmodG.QrUpdPID1.SQL.Add('SELECT ' + FormatFloat('0', QrStqInnNFenItem.Value) + ' nItem, ggx.Controle GraGruX,');
      DmodG.QrUpdPID1.SQL.Add('gcc.Nome NO_Cor, gti.Nome NO_TAM');
      DmodG.QrUpdPID1.SQL.Add('FROM ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gragrux ggx');
      DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gragruc ggc ON ggc.Controle=ggx.GraGruC');
      DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ');
      DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gratamits gti ON gti.Controle=ggx.GraTamI');
      DmodG.QrUpdPID1.SQL.Add('WHERE GraGru1=:P0');
      DmodG.QrUpdPID1.Params[00].AsInteger := QrStqInnNFeNivel1.Value;
      DmodG.QrUpdPID1.ExecSQL;
    end;
    //
    QrStqInnNFe.Next;
  end;
  //
  ReopenStqInnNFe(QrStqInnNFenItem.Value);
  //
end;
}

procedure TFmStqInnNFe.AcoesIniciais();
begin
  FIniciou := True;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FStqInnNFe);
  DmodG.QrUpdPID1.SQL.Add('SELECT 1 & gg1.HowBxaEstq NeedPc,');
  DmodG.QrUpdPID1.SQL.Add('2 &gg1.HowBxaEstq NeedM2,');
  DmodG.QrUpdPID1.SQL.Add('4 &gg1.HowBxaEstq NeedKg,');
  DmodG.QrUpdPID1.SQL.Add('nfi.nItem, nfi.prod_cProd, nfi.prod_xProd,');
  DmodG.QrUpdPID1.SQL.Add('nfi.prod_uCom, nfi.prod_qCom, nfi.prod_vUnCom,');
  DmodG.QrUpdPID1.SQL.Add('nfi.prod_vProd, nfi.Nivel1, 0 GraGruX, gg1.HowBxaEstq,');
  DmodG.QrUpdPID1.SQL.Add('0 Pecas, 0 Peso, 0 M2, 0 P2, ');
  DmodG.QrUpdPID1.SQL.Add('nfi.prod_vFrete, nfi.prod_vSeg, ');
  DmodG.QrUpdPID1.SQL.Add('nfi.prod_vDesc, nfi.prod_vOutro ');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + Lowercase(Dmod.MyDB.DatabaseName) + '.nfeitsi nfi');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gragru1 gg1 ON gg1.Nivel1=nfi.Nivel1');
  DmodG.QrUpdPID1.SQL.Add('WHERE FatID=:P0');
  DmodG.QrUpdPID1.SQL.Add('AND FatNum=:P1');
  DmodG.QrUpdPID1.Params[00].AsInteger := FFatID;
  DmodG.QrUpdPID1.Params[01].AsInteger := FFatNum;
  DmodG.QrUpdPID1.ExecSQL;
  //
  ReopenStqInnNFe(0);
  TbStqInnNFe.First;
  while not TbStqInnNFe.Eof do
  begin
    QrGraGruX.Close;
    QrGraGruX.Params[0].AsInteger := TbStqInnNFeNivel1.Value;
    UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
    if QrGraGruX.RecordCount = 1 then
    begin
      SetaGraGruX(QrGraGruXControle.Value);
      {
      TbStqInnNFe.Edit;
      TbStqInnNFeGraGruX.Value := QrGraGruXControle.Value;
      TbStqInnNFe.Post;
      }
      //
      {
      UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, 'stqinnnfe', False, [
      'GraGruX'], ['nItem'], [QrGraGruXControle.Value], [
      TbStqInnNFenItem.Value], False);
      }
    end else begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FStqInnOpt);
      DmodG.QrUpdPID1.SQL.Add('SELECT ' + FormatFloat('0', TbStqInnNFenItem.Value) + ' nItem, ggx.Controle GraGruX,');
      DmodG.QrUpdPID1.SQL.Add('gcc.Nome NO_Cor, gti.Nome NO_TAM');
      DmodG.QrUpdPID1.SQL.Add('FROM ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gragrux ggx');
      DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gragruc ggc ON ggc.Controle=ggx.GraGruC');
      DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ');
      DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + Lowercase(Dmod.MyDB.DatabaseName) + '.gratamits gti ON gti.Controle=ggx.GraTamI');
      DmodG.QrUpdPID1.SQL.Add('WHERE GraGru1=:P0');
      DmodG.QrUpdPID1.Params[00].AsInteger := TbStqInnNFeNivel1.Value;
      DmodG.QrUpdPID1.ExecSQL;
    end;
    //
    TbStqInnNFe.Next;
  end;
  //
  //ReopenStqInnNFe(TbStqInnNFenItem.Value);
  //
end;

procedure TFmStqInnNFe.BtOKClick(Sender: TObject);
var
  IDCtrl, OriCtrl, GraGruX, StqCenCad: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, prod_vProd, prod_vFrete, prod_vSeg,
  prod_vDesc, prod_vOutro: Double;
  Texto: String;
  CustoBuy: Double;
begin
{
  QrErros.Close;
  UnDmkDAC_PF.AbreQuery(QrErros, DModG.MyPidDB);
  if QrErrosItens.Value > 0 then
  begin
    Geral.MensagemBox('Importa��o de itens cancelada! H� ' + FormatFloat('0',
    QrErrosItens.Value) + ' registros sem defini��o do reduzido!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCenCad,
    'Informe o centro de estoque!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    QrStqInnNFe.First;
    while not QrStqInnNFe.Eof do
    begin
      GraGruX := QrStqInnNFeGraGruX.Value;
      Qtde    := QrStqInnNFeprod_qCom.Value;
      Pecas   := QrStqInnNFePecas.Value;
      Peso    := QrStqInnNFePeso.Value;
      AreaM2  := QrStqInnNFeM2.Value;
      AreaP2  := QrStqInnNFeP2.Value;
      //
      IDCtrl  := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', stIns, 0);
      OriCtrl := IDCtrl;
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'Empresa',
      'StqCenCad', 'GraGruX', 'Qtde',
      'OriPart', 'Pecas', 'Peso',
      'AreaM2', 'AreaP2', 'FatorClas'], [
      'IDCtrl'], [
      FDataHora, FTipo, FOriCodi,
      OriCtrl, FOriCnta, FEmpresa,
      StqCenCad, GraGruX, Qtde,
      FOriPart, Pecas, Peso,
      AreaM2, AreaP2, FFatorClas], [
      IDCtrl], False);
      //
      QrStqInnNFe.Next;
    end;
    // Atualiza centro de estoque geral da entrada
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqinncad', True, [
    'StqCenCad'], ['Codigo'], [StqCenCad], [
    FOriCodi], True);
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
}
  QrErros.Close;
  UnDmkDAC_PF.AbreQuery(QrErros, DModG.MyPID_DB);
  //
  if QrErros.RecordCount > 0 then
  begin
    Texto := 'No item ' + FormatFloat('0', QrErrosnItem.Value) + ' falta:';
    if QrErrosGraGruX.Value = 0 then
      Texto := Texto + sLineBreak + '-> Falta informar o reduzido.';
    if (QrErrosNeedPc.Value <> 0) and (QrErrosPecas.Value = 0) then
      Texto := Texto + sLineBreak + '-> Falta informar a quantidade de pe�as.';
    if (QrErrosNeedM2.Value <> 0) and (QrErrosM2.Value = 0) then
      Texto := Texto + sLineBreak + '-> Falta informar a �rea.';
    if (QrErrosNeedKg.Value <> 0) and (QrErrosPeso.Value = 0) then
      Texto := Texto + sLineBreak + '-> Falta informar o peso.';
    //
    TbStqInnNFe.Locate('nItem', QrErrosnItem.Value, []);
    Geral.MensagemBox('Importa��o de itens cancelada! H� ' + FormatFloat('0',
    QrErros.RecordCount) + ' registros com falta de informa��o!' + sLineBreak +
    Texto, 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCenCad,
    'Informe o centro de estoque!') then
  begin
    EdStqCenCad.SetFocus;
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    TbStqInnNFe.First;
    while not TbStqInnNFe.Eof do
    begin
      GraGruX    := TbStqInnNFeGraGruX.Value;
      Qtde       := TbStqInnNFeprod_qCom.Value;
      Pecas      := TbStqInnNFePecas.Value;
      Peso       := TbStqInnNFePeso.Value;
      AreaM2     := TbStqInnNFeM2.Value;
      AreaP2     := TbStqInnNFeP2.Value;
      prod_vProd   := TbStqInnNFeprod_vProd.Value;
      prod_vFrete  := TbStqInnNFeprod_vFrete.Value;
      prod_vSeg    := TbStqInnNFeprod_vSeg.Value;
      prod_vDesc   := TbStqInnNFeprod_vDesc.Value;
      prod_vOutro  := TbStqInnNFeprod_vOutro.Value;
      //
      CustoBuy   := prod_vProd + prod_vFrete + prod_vSeg + prod_vOutro - prod_vDesc;
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT FatNum, nItem, "ICMS" Tipo, ICMS_vICMSST Valor ',
      'FROM nfeitsn  ',
      'WHERE FatID=' + Geral.FF0(FFatID),
      'AND FatNum=' + Geral.FF0(FFatNum),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      'AND nItem=' + Geral.FF0(TbStqInnNFenItem.Value),
      'AND ICMS_vICMSST > 0 ',
      ' ',
      'UNION ',
      ' ',
      'SELECT FatNum, nItem, "IPI" Tipo, IPI_vIPI  ',
      'FROM nfeitso ',
      'WHERE FatID=' + Geral.FF0(FFatID),
      'AND FatNum=' + Geral.FF0(FFatNum),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      'AND nItem=' + Geral.FF0(TbStqInnNFenItem.Value),
      'AND IPI_vIPI > 0 ',
      '']);
      Dmod.QrAux.First;
      while not Dmod.QrAux.Eof do
      begin
        CustoBuy := CustoBuy + Dmod.QrAux.FieldByName('Valor').AsFloat;
        //
        Dmod.QrAux.Next;
      end;
      //
      IDCtrl  := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', stIns, 0);
      OriCtrl := IDCtrl;
      //if
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
        'DataHora', 'Tipo', 'OriCodi',
        'OriCtrl', 'OriCnta', 'Empresa',
        'StqCenCad', 'GraGruX', 'Qtde',
        'CustoBuy', 'CustoAll',
        'OriPart', 'Pecas', 'Peso',
        'AreaM2', 'AreaP2', 'FatorClas'], [
        'IDCtrl', 'Ativo'], [
        FDataHora, FTipo, FOriCodi,
        OriCtrl, FOriCnta, FEmpresa,
        StqCenCad, GraGruX, Qtde,
        CustoBuy, CustoBuy,
        FOriPart, Pecas, Peso,
        AreaM2, AreaP2, FFatorClas], [
        IDCtrl, 0], False) then
      begin
        DmodG.AtualizaPrecosGraGruVal2(GraGruX, FEmpresa);
      end;
      //
      TbStqInnNFe.Next;
    end;
    FmStqInnCad.LocCod(FOriCodi, FOriCodi);
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmStqInnNFe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqInnNFe.DBGrid1CellClick(Column: TColumn);
begin
  dmkEdit1.Text := Column.FieldName;
end;

procedure TFmStqInnNFe.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Campo: String;
begin
  dmkEdit1.Text :=
    THackDBGrid(DBGrid1).Columns[THackDBGrid(DBGrid1).Col- 1 ].FieldName;
  if Key = VK_F4 then
  begin
    Campo := THackDBGrid(DBGrid1).Columns[THackDBGrid(DBGrid1).Col- 1 ].FieldName;
    TbStqInnNFe.Edit;
    if Campo = 'M2' then
      TbStqInnNFeM2.Value := TbStqInnNFeprod_qCom.Value;
    if Campo = 'P2' then
      TbStqInnNFeP2.Value := TbStqInnNFeprod_qCom.Value;
    if Campo = 'Peso' then
      TbStqInnNFePeso.Value := TbStqInnNFeprod_qCom.Value;
    if Campo = 'Pecas' then
      TbStqInnNFePecas.Value := TbStqInnNFeprod_qCom.Value;
    TbStqInnNFe.Post;
  end;
end;

procedure TFmStqInnNFe.DBGrid2DblClick(Sender: TObject);
begin
  {
  UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, 'stqinnnfe', False, [
  'GraGruX'], ['nItem'], [QrStqInnOptGraGruX.Value], [
  QrStqInnNFenItem.Value], False);
  }
  {
  TbStqInnNFe.Edit;
  TbStqInnNFeGraGruX.Value := QrStqInnOptGraGruX.Value;
  TbStqInnNFe.Post;
  }
  SetaGraGruX(QrStqInnOptGraGruX.Value);
  //
  //ReopenStqInnNFe(TbStqInnNFenItem.Value);
end;

procedure TFmStqInnNFe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if not FIniciou then
    AcoesIniciais();
end;

procedure TFmStqInnNFe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TbStqInnNFeprod_vProd.DisplayFormat  := Dmod.FStrFmtPrc;
  TbStqInnNFeprod_vUnCom.DisplayFormat := Dmod.FStrFmtPrc;
  //
  FIniciou := False;
  TbStqInnNFe.Database := DModG.MyPID_DB;
  //QrStqInnNFe.Database := DModG.MyPID_DB;
  QrStqInnOpt.Database := DModG.MyPID_DB;
  QrErros.Database     := DModG.MyPID_DB;
end;

procedure TFmStqInnNFe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqInnNFe.QrStqInnOptAfterOpen(DataSet: TDataSet);
begin
  PnOpt.Visible := QrStqInnOpt.RecordCount > 0;
end;

procedure TFmStqInnNFe.QrStqInnOptBeforeClose(DataSet: TDataSet);
begin
  PnOpt.Visible := False;
end;

procedure TFmStqInnNFe.ReopenStqInnNFe(nItem: Integer);
begin
{  QrStqInnNFe.Close;
  QrStqInnNFe.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrStqInnNFe, DModG.MyPidDB);
  //
  QrStqInnNFe.Locate('nItem', nItem, []);
}
  //
  TbStqInnNFe.Close;
  TbStqInnNFe.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreTable(TbStqInnNFe, DModG.MyPID_DB);
  //
  TbStqInnNFe.Locate('nItem', nItem, []);
  //
end;

procedure TFmStqInnNFe.SetaGraGruX(GraGruX: Integer);
begin
  Screen.Cursor := crHourGlass;
  TbStqInnNFeGraGruX.ReadOnly := False;
  try
    TbStqInnNFe.Edit;
    //
    TbStqInnNFeGraGruX.Value := QrGraGruXControle.Value;
    TbStqInnNFe.Post;
  finally
    TbStqInnNFeGraGruX.ReadOnly := True;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmStqInnNFe.TbStqInnNFeAfterScroll(DataSet: TDataSet);
begin
  QrStqInnOpt.Close;
  QrStqInnOpt.Params[0].AsInteger := TbStqInnNFenItem.Value;
  UnDmkDAC_PF.AbreQuery(QrStqInnOpt, DModG.MyPID_DB);
end;

procedure TFmStqInnNFe.TbStqInnNFeBeforeClose(DataSet: TDataSet);
begin
  QrStqInnOpt.Close;
end;

procedure TFmStqInnNFe.TbStqInnNFeBeforePost(DataSet: TDataSet);
begin
  if TbStqInnNFe.State = dsInsert then
    TbStqInnNFe.Cancel
end;

procedure TFmStqInnNFe.TbStqInnNFeM2Change(Sender: TField);
begin
  if THackDBGrid(DBGrid1).Columns[THackDBGrid(DBGrid1).Col- 1 ].FieldName = 'M2' then
    TbStqInnNFeP2.Value := Geral.ConverteArea(TbStqInnNFeM2.Value, ctM2toP2, cfQuarto);
end;

procedure TFmStqInnNFe.TbStqInnNFeP2Change(Sender: TField);
begin
  if THackDBGrid(DBGrid1).Columns[THackDBGrid(DBGrid1).Col- 1 ].FieldName = 'P2' then
    TbStqInnNFeM2.Value := Geral.ConverteArea(TbStqInnNFeP2.Value, ctp2toM2, cfCento);
end;

end.

