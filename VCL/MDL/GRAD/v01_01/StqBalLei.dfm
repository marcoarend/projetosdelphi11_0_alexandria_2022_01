object FmStqBalLei: TFmStqBalLei
  Left = 339
  Top = 185
  Caption = 'STQ-BALAN-002 :: Leitura de Produtos em Balan'#231'o'
  ClientHeight = 556
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 400
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 384
        Top = 4
        Width = 158
        Height = 13
        Caption = 'Localiza'#231#227'o (apenas informativo):'
      end
      object Label2: TLabel
        Left = 12
        Top = 4
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        Enabled = False
      end
      object Label11: TLabel
        Left = 808
        Top = 4
        Width = 83
        Height = 13
        Caption = 'Total quantidade:'
        FocusControl = DBEdit4
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 384
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdStqCenLocChange
        OnExit = EdStqCenLocExit
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 440
        Top = 20
        Width = 361
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenLoc
        TabOrder = 3
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 313
        Height = 21
        Enabled = False
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 1
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdStqCenCad: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdStqCenCadChange
        OnEnter = EdStqCenCadEnter
        OnExit = EdStqCenCadExit
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object DBEdit4: TDBEdit
        Left = 808
        Top = 20
        Width = 134
        Height = 21
        TabStop = False
        DataField = 'QtdLei'
        DataSource = DsTotal
        TabOrder = 4
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 49
      Width = 1008
      Height = 80
      ActivePage = TabSheet1
      Align = alTop
      TabHeight = 25
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' por &Leitura '
        object PnLeitura: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 45
          Align = alClient
          BevelOuter = bvNone
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 437
            Height = 45
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label3: TLabel
              Left = 12
              Top = 4
              Width = 89
              Height = 13
              Caption = 'Leitura / digita'#231#227'o:'
            end
            object LaQtdeLei: TLabel
              Left = 196
              Top = 4
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object Label10: TLabel
              Left = 260
              Top = 4
              Width = 53
              Height = 13
              Caption = 'Custo total:'
            end
            object EdLeitura: TEdit
              Left = 12
              Top = 20
              Width = 180
              Height = 21
              MaxLength = 20
              TabOrder = 0
              OnChange = EdLeituraChange
              OnExit = EdLeituraExit
              OnKeyDown = EdLeituraKeyDown
            end
            object EdQtdLei: TdmkEdit
              Left = 196
              Top = 20
              Width = 61
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnEnter = EdQtdLeiEnter
              OnExit = EdQtdLeiExit
              OnKeyDown = EdQtdLeiKeyDown
            end
            object BtOK: TBitBtn
              Tag = 14
              Left = 343
              Top = 3
              Width = 90
              Height = 40
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 3
              OnClick = BtOKClick
            end
            object EdValLei: TdmkEdit
              Left = 260
              Top = 20
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnEnter = EdQtdLeiEnter
              OnExit = EdQtdLeiExit
              OnKeyDown = EdQtdLeiKeyDown
            end
          end
          object PnLido: TPanel
            Left = 437
            Top = 0
            Width = 453
            Height = 45
            Align = alLeft
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 1
            Visible = False
            object Label4: TLabel
              Left = 4
              Top = 4
              Width = 86
              Height = 13
              Caption = 'Grupo de produto:'
              FocusControl = DBEdit1
            end
            object Label5: TLabel
              Left = 248
              Top = 4
              Width = 19
              Height = 13
              Caption = 'Cor:'
              FocusControl = DBEdit2
            end
            object Label6: TLabel
              Left = 392
              Top = 4
              Width = 48
              Height = 13
              Caption = 'Tamanho:'
              FocusControl = DBEdit3
            end
            object DBEdit1: TDBEdit
              Left = 4
              Top = 20
              Width = 240
              Height = 21
              DataField = 'NOMENIVEL1'
              DataSource = DsItem
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 248
              Top = 20
              Width = 140
              Height = 21
              DataField = 'NOMECOR'
              DataSource = DsItem
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 392
              Top = 20
              Width = 60
              Height = 21
              DataField = 'NOMETAM'
              DataSource = DsItem
              TabOrder = 2
            end
          end
          object PnSimu: TPanel
            Left = 890
            Top = 0
            Width = 110
            Height = 45
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            Visible = False
            object Label9: TLabel
              Left = 368
              Top = 4
              Width = 54
              Height = 13
              Caption = 'Sequ'#234'ncia:'
            end
            object Label8: TLabel
              Left = 284
              Top = 4
              Width = 48
              Height = 13
              Caption = 'Reduzido:'
            end
            object Label7: TLabel
              Left = 200
              Top = 4
              Width = 75
              Height = 13
              Caption = 'Ordem de serv.:'
            end
            object dmkEdit2: TdmkEdit
              Left = 284
              Top = 20
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000010'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 10
              ValWarn = False
              OnChange = EdLeituraChange
            end
            object dmkEdit3: TdmkEdit
              Left = 368
              Top = 20
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 8
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00000001'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
              OnChange = EdLeituraChange
            end
            object dmkEdit1: TdmkEdit
              Left = 200
              Top = 20
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdLeituraChange
            end
            object Button1: TButton
              Left = 4
              Top = 12
              Width = 185
              Height = 25
              Caption = 'Simula leitura do c'#243'digo de barras'
              TabOrder = 3
              OnClick = Button1Click
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' por &Grade '
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 45
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 45
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object BtGrade: TBitBtn
              Tag = 10054
              Left = 4
              Top = 2
              Width = 90
              Height = 40
              Caption = '&Grade'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtGradeClick
            end
          end
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 129
      Width = 1008
      Height = 271
      Align = alClient
      DataSource = DsLidos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Title.Caption = 'Seq.'
          Width = 37
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL1'
          Title.Caption = 'Descri'#231#227'o do grupo de produto'
          Width = 225
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODUSU_COR'
          Title.Caption = 'Cor'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOR'
          Title.Caption = 'Descri'#231#227'o da cor'
          Width = 107
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETAM'
          Title.Caption = 'Tamanho'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdLei'
          Title.Caption = 'Quantidade'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoAll'
          Title.Caption = 'Custo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PecasLei'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2Lei'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaP2Lei'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PesoLei'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 7
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 388
        Height = 32
        Caption = 'Leitura de Produtos em Balan'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 388
        Height = 32
        Caption = 'Leitura de Produtos em Balan'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 388
        Height = 32
        Caption = 'Leitura de Produtos em Balan'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 448
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 492
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object CkFixo: TCheckBox
        Left = 12
        Top = 16
        Width = 125
        Height = 17
        Caption = 'Quantidade fixa.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 457
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Excluir'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad scc'#10
      ''
      'ORDER BY Nome')
    Left = 148
    Top = 212
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 148
    Top = 260
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 224
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenLocNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 224
    Top = 260
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1'
      'FROM gragrux ggx'#13
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE ggx.Controle=:P0')
    Left = 288
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 288
    Top = 260
  end
  object QrLidos: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLidosCalcFields
    SQL.Strings = (
      'SELECT gg1.CodUsu CODUSU_NIVEL1, gg1.Nome NOMENIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CODUSU_COR, gcc.Nome NOMECOR,'
      'gti.Nome NOMETAM, ggx.GraGru1, sbi.*'
      'FROM stqbalits sbi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=sbi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE sbi.Codigo=:P0'
      'AND sbi.StqCenLoc=:P1'
      '')
    Left = 16
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLidosNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrLidosGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrLidosNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrLidosNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrLidosGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
    end
    object QrLidosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'stqbalits.Codigo'
      Required = True
    end
    object QrLidosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'stqbalits.Controle'
      Required = True
    end
    object QrLidosTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqbalits.Tipo'
      Required = True
    end
    object QrLidosDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'stqbalits.DataHora'
      Required = True
    end
    object QrLidosStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Origin = 'stqbalits.StqCenLoc'
      Required = True
    end
    object QrLidosGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'stqbalits.GraGruX'
      Required = True
    end
    object QrLidosQtdLei: TFloatField
      FieldName = 'QtdLei'
      Origin = 'stqbalits.QtdLei'
      Required = True
    end
    object QrLidosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      DisplayFormat = '00000'
      Calculated = True
    end
    object QrLidosCODUSU_NIVEL1: TIntegerField
      FieldName = 'CODUSU_NIVEL1'
      Required = True
    end
    object QrLidosCODUSU_COR: TIntegerField
      FieldName = 'CODUSU_COR'
      Required = True
    end
    object QrLidosPecasLei: TFloatField
      FieldName = 'PecasLei'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosPecasAnt: TFloatField
      FieldName = 'PecasAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosPesoLei: TFloatField
      FieldName = 'PesoLei'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosPesoAnt: TFloatField
      FieldName = 'PesoAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosAreaM2Lei: TFloatField
      FieldName = 'AreaM2Lei'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosAreaM2Ant: TFloatField
      FieldName = 'AreaM2Ant'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosAreaP2Lei: TFloatField
      FieldName = 'AreaP2Lei'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosAreaP2Ant: TFloatField
      FieldName = 'AreaP2Ant'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLidosCustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsLidos: TDataSource
    DataSet = QrLidos
    Left = 16
    Top = 260
  end
  object QrTotal: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLidosCalcFields
    SQL.Strings = (
      'SELECT SUM(sbi.QtdLei) QtdLei'
      'FROM stqbalits sbi'
      'WHERE sbi.Codigo=:P0'
      'AND sbi.StqCenLoc=:P1')
    Left = 76
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTotalQtdLei: TFloatField
      FieldName = 'QtdLei'
    end
  end
  object DsTotal: TDataSource
    DataSet = QrTotal
    Left = 76
    Top = 260
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Pecas) Pecas,  SUM(Peso) Peso,'
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2'
      'FROM stqmovitsa'
      'WHERE Empresa=:P0'
      'AND StqCenCad=:P1'
      'AND GraGruX=:P2')
    Left = 340
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSomaQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSomaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSomaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSomaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSomaAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSomaStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
end
