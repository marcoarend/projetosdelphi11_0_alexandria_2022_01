unit GraGruDados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkGeral, dmkLabel, Mask,
  DBCtrls, dmkDBEdit, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, DB,
  mySQLDbTables, dmkRadioGroup, dmkCheckGroup, dmkCheckBox, DmkDAC_PF, dmkImage,
  UnDmkEnums, Vcl.ComCtrls;

type
  TFmGraGruDados = class(TForm)
    VUUnidMed: TdmkValUsu;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    QrMedOrdem: TmySQLQuery;
    DsMedOrdem: TDataSource;
    QrMedOrdemCodigo: TIntegerField;
    QrMedOrdemCodUsu: TIntegerField;
    QrMedOrdemNome: TWideStringField;
    dmkValUsu2: TdmkValUsu;
    QrMatPartCad: TmySQLQuery;
    QrMatPartCadCodigo: TIntegerField;
    QrMatPartCadCodUsu: TIntegerField;
    QrMatPartCadNome: TWideStringField;
    DsMatPartCad: TDataSource;
    dmkValUsu3: TdmkValUsu;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel13: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    Label6: TLabel;
    SBUnidMed: TSpeedButton;
    Label10: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    EdUnidMed: TdmkEditCB;
    CBUnidMed: TdmkDBLookupComboBox;
    EdSigla: TdmkEdit;
    EdReferencia: TdmkEdit;
    Panel5: TPanel;
    Panel3: TPanel;
    Panel7: TPanel;
    Panel9: TPanel;
    Label5: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    EdPeso: TdmkEdit;
    EdPerComissF: TdmkEdit;
    EdPerComissR: TdmkEdit;
    CkPerComissZ: TdmkCheckBox;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    CGHowBxaEstq: TdmkCheckGroup;
    RGTipDimens: TdmkRadioGroup;
    Panel6: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    EdMedOrdem: TdmkEditCB;
    CBMedOrdem: TdmkDBLookupComboBox;
    EdPartePrinc: TdmkEditCB;
    CBPartePrinc: TdmkDBLookupComboBox;
    Panel10: TPanel;
    Label12: TLabel;
    EdPerCuztMin: TdmkEdit;
    EdPerCuztMax: TdmkEdit;
    RGGerBxaEstq: TdmkRadioGroup;
    Label9: TLabel;
    EdCOD_LST: TdmkEdit;
    Panel8: TPanel;
    Label8: TLabel;
    Label7: TLabel;
    Label11: TLabel;
    RGFatorClas: TdmkRadioGroup;
    TabSheet3: TTabSheet;
    QrCtbCadGru: TMySQLQuery;
    QrCtbCadGruCodigo: TIntegerField;
    QrCtbCadGruNome: TWideStringField;
    DsCtbCadGru: TDataSource;
    Panel11: TPanel;
    Label2: TLabel;
    EdCtbCadGru: TdmkEditCB;
    CBCtbCadGru: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdCtbPlaCta: TdmkEditCB;
    CBCtbPlaCta: TdmkDBLookupComboBox;
    SbCtbCadGru: TSpeedButton;
    SbCtbPlaCta: TSpeedButton;
    QrPlaAllCad: TMySQLQuery;
    QrPlaAllCadCodigo: TIntegerField;
    QrPlaAllCadNome: TWideStringField;
    QrPlaAllCadNivel: TSmallintField;
    QrPlaAllCadAnaliSinte: TIntegerField;
    QrPlaAllCadCredito: TSmallintField;
    QrPlaAllCadDebito: TSmallintField;
    QrPlaAllCadOrdens: TWideStringField;
    DsPlaAllCad: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure SBUnidMedClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure EdCOD_LSTChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SbCtbCadGruClick(Sender: TObject);
    procedure SbCtbPlaCtaClick(Sender: TObject);
  private
    { Private declarations }
    {
    procedure ListaDeSituacaoTributaria();
    procedure ListaDeTributacaoPeloICMS();
    procedure ListaDeCST_IPI();
    }
    function ReferenciaJaUsada(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmGraGruDados: TFmGraGruDados;

implementation

uses
  //UnFinanceiro,
  UnMyObjects, UnitListasGrade, ModProd, GraGruN, UMySQLModule, Module,
  UnMySQLCuringa, UnUMedi_PF, UnInternalConsts, MyDBCheck, SelOnStringGrid,
  {$IfNDef NO_FINANCEIRO} UnFinanceiroJan, {$EndIf}
  MedOrdem, MatPartCad, ModuleFin;

{$R *.DFM}

procedure TFmGraGruDados.BtOKClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  if MyObjects.FIC(Trim(EdReferencia.ValueVariant) = '', EdReferencia, 'Informe a refer�ncia!') then Exit;
  //
  if EdPartePrinc.ValueVariant = 0 then
    dmkValUsu3.ValueVariant := 0; //N�o pode ser Null  
  //
  if ReferenciaJaUsada() then
    Exit;
  Nivel1 := FmGraGruN.QrGraGru1Nivel1.Value;
  if UMyMod.ExecSQLInsUpdFm(FmGraGruDados, stUpd, 'gragru1', Nivel1, Dmod.QrUpd)
  then begin
    FmGraGruN.ReopenGraGru1(Nivel1);
    Close;
  end;
end;

procedure TFmGraGruDados.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruDados.EdCOD_LSTChange(Sender: TObject);
begin
  if Geral.SoNumero_TT(EdCOD_LST.Text) <> EdCOD_LST.Text then
  begin
    EdCOD_LST.ValueVariant := Geral.SoNumero_TT(EdCOD_LST.ValueVariant);
    Geral.MB_Aviso('Campo de COD_LST s� aceita n�meros!');
  end;
end;

procedure TFmGraGruDados.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraGruDados.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraGruDados.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmGraGruDados.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmGraGruDados.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraGruDados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruDados.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.ConfiguraRadioGroup(RGgerBxaEstq, sListaGerBxaEstq, 6, 0);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMedOrdem, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMatPartCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCtbCadGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlaAllCad, Dmod.MyDB);
end;

procedure TFmGraGruDados.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruDados.FormShow(Sender: TObject);
begin
  if EdReferencia.ValueVariant = '' then
    EdReferencia.ValueVariant := FmGraGruN.QrGraGru1Codusu.Value;
end;

{
procedure TFmGraGruDados.ListaDeSituacaoTributaria;
var
  i,j: Integer;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'Situa��o Tribut�ria';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      FListaA := UFinanceiro.CST_A_Lista();
      j := 0;
      for i := Low(FListaA) to High(FListaA) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := FListaA[i][0];
        Grade.Cells[2, j] := FListaA[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        EdCST_A.Text := FResult;
      Destroy;
    end;
  end;
end;
}

{
procedure TFmGraGruDados.ListaDeTributacaoPeloICMS;
var
  i,j: Integer;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'Tributa��o pelo ICMS';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      FListaB := UFinanceiro.CST_B_Lista();
      j := 0;
      for i := Low(FListaB) to High(FListaB) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := FListaB[i][0];
        Grade.Cells[2, j] := FListaB[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        EdCST_B.Text := FResult;
      Destroy;
    end;
  end;
end;

procedure TFmGraGruDados.ListaDeCST_IPI;
var
  i,j: Integer;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'CST IPI';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      FListaB := UFinanceiro.CST_IPI_Lista();
      j := 0;
      for i := Low(FListaB) to High(FListaB) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := FListaB[i][0];
        Grade.Cells[2, j] := FListaB[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        EdIPI_CST.Text := FResult;
      Destroy;
    end;
  end;
end;
}

function TFmGraGruDados.ReferenciaJaUsada(): Boolean;
var
  Ref: String;
begin
  Ref := Trim(EdReferencia.Text);
  if Ref = '' then
    Result := False
  else begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Nivel1 ',
    'FROM GraGru1 ',
    'WHERE Referencia = "' + EdReferencia.Text + '" ',
    'AND Nivel1<>' + FormatFloat('0', FmGraGruN.QrGraGru1Nivel1.Value),
    'AND Nivel1 > -900000 ',
    ' ']);
    Result := Dmod.QrAux.RecordCount > 0;
  end;
  if Result then
  begin
    Geral.MB_Aviso('A refer�ncia informada j� � usada para o pruduto ' +
    FormatFloat('0', Dmod.QrAux.FieldByName('Nivel1').AsInteger) + '. (Nivel1)');
  end;
end;

procedure TFmGraGruDados.SbCtbCadGruClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormCtbCadGru(EdCtbCadGru.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbCadGru, CBCtbCadGru, QrCtbCadGru, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmGraGruDados.SbCtbPlaCtaClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  {$IfDef UsaContabil}
    DModFin.MostraFormPlaAny(EdCtbPlaCta, CBCtbPlaCta, QrPlaAllCad, SbCtbPlaCta);
  {$Else}
    FinanceiroJan.MostraFormContas(EdCtbPlaCta.ValueVariant);
  {$EndIf}
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbPlaCta, CBCtbPlaCta, QrPlaAllCad, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmGraGruDados.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := VUUnidMed.ValueVariant;
(*
  VAR_CADASTRO := 0;
  //
  UMedi_PF.MostraUnidMed(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    CBUnidMed.SetFocus;
  end;
*)
  UMedi_PF.CriaEEscolheUnidMed(Codigo, EdUnidMed, CBUnidMed, QrUnidMed);
end;

procedure TFmGraGruDados.SpeedButton2Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := dmkValUsu2.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmMedOrdem, FmMedOrdem, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmMedOrdem.LocCod(Codigo, Codigo);
    FmMedOrdem.ShowModal;
    FmMedOrdem.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrMedOrdem.Close;
    UnDmkDAC_PF.AbreQuery(QrMedOrdem, Dmod.MyDB);
    //
    if QrMedOrdem.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdMedOrdem.ValueVariant := QrMedOrdemCodUsu.Value;
      CBMedOrdem.KeyValue     := QrMedOrdemCodUsu.Value;
      CBMedOrdem.SetFocus;
    end;
  end;
end;

procedure TFmGraGruDados.SpeedButton3Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := dmkValUsu3.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmMatPartCad, FmMatPartCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmMatPartCad.LocCod(Codigo, Codigo);
    FmMatPartCad.ShowModal;
    FmMatPartCad.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrMatPartCad.Close;
    UnDmkDAC_PF.AbreQuery(QrMatPartCad, Dmod.MyDB);
    //
    if QrMatPartCad.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdPartePrinc.ValueVariant := QrMatPartCadCodUsu.Value;
      CBPartePrinc.KeyValue     := QrMatPartCadCodUsu.Value;
      CBPartePrinc.SetFocus;
    end;
  end;
end;

end.
