unit GraImpConflitos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, frxClass, frxDBSet, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmGraImpConflitos = class(TForm)
    Panel1: TPanel;
    RGRelatorio: TRadioGroup;
    QrConfli_0: TmySQLQuery;
    DsConfli_0: TDataSource;
    frxConfli_0: TfrxReport;
    frxDsConfli_0: TfrxDBDataset;
    QrConfli_0GraGruX: TIntegerField;
    QrConfli_0CU_Nivel1: TIntegerField;
    QrConfli_0NO_PRD: TWideStringField;
    QrConfli_0Sigla: TWideStringField;
    QrConfli_0GerBxaEstq: TSmallintField;
    QrConfli_0NO_GBE: TWideStringField;
    QrConfli_0Grandeza: TSmallintField;
    QrConfli_0NO_GRAND: TWideStringField;
    DBGConfli_0: TDBGrid;
    QrConfli_0NO_RELACAO: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGRelatorioClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ImprimeConfli_0();
    procedure ImprimeConfli_1();
    procedure ImprimeConfli_2();
  public
    { Public declarations }
  end;

  var
  FmGraImpConflitos: TFmGraImpConflitos;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmGraImpConflitos.BtImprimeClick(Sender: TObject);
begin
  case RGRelatorio.ItemIndex of
    0: ImprimeConfli_0();
    1: ImprimeConfli_1();
    2: ImprimeConfli_2();
    else
  end;
end;

procedure TFmGraImpConflitos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraImpConflitos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraImpConflitos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmGraImpConflitos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraImpConflitos.ImprimeConfli_0();
begin
  QrConfli_0.Close;
  UnDmkDAC_PF.AbreQuery(QrConfli_0, Dmod.MyDB);
  MyObjects.frxMostra(frxConfli_0, RGRelatorio.Items[RGRelatorio.ItemIndex]);
end;

procedure TFmGraImpConflitos.ImprimeConfli_1();
begin
{
SELECT mpi.Codigo, mpi.Controle, smia.* 
FROM stqmovitsa smia
LEFT JOIN mpin mpi ON mpi.Codigo=smia.OriCodi 
  AND mpi.Controle=smia.OriCtrl 
WHERE smia.Tipo IN (103,113)
AND smia.GraGruX < 0
AND mpi.Controle IS NULL
}
end;

procedure TFmGraImpConflitos.ImprimeConfli_2();
begin

end;

procedure TFmGraImpConflitos.RGRelatorioClick(Sender: TObject);
begin
  BtImprime.Enabled := RGRelatorio.ItemIndex > -1;
end;

end.
