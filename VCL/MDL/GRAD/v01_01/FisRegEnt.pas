unit FisRegEnt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFisRegEnt = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    DsEntidades: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    EdObservacao: TdmkEdit;
    Label1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    QrEntidadesNO_ENT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Entidade: Integer);
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
    FEntidade: Integer;
  end;

  var
  FmFisRegEnt: TFmFisRegEnt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmFisRegEnt.BtOKClick(Sender: TObject);
var
  Observacao: String;
  Codigo, Entidade: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Entidade       := EdEntidade.ValueVariant;
  Observacao     := EdObservacao.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fisregent', False, [
  'Observacao'], [
  'Codigo', 'Entidade'], [
  Observacao], [
  Codigo, Entidade], True) then
  begin
    FEntidade := Entidade;
    ReopenCadastro_Com_Itens_ITS(Entidade);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdEntidade.ValueVariant   := 0;
      CBEntidade.KeyValue       := Null;
      EdObservacao.ValueVariant := '';
      EdEntidade.SetFocus;
    end else Close;
  end;
end;

procedure TFmFisRegEnt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFisRegEnt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegEnt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmFisRegEnt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFisRegEnt.ReopenCadastro_Com_Itens_ITS(Entidade: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Entidade <> 0 then
      FQrIts.Locate('Entidade', Entidade, []);
  end;
end;

end.
