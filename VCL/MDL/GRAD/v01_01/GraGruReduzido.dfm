object FmGraGruReduzido: TFmGraGruReduzido
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-021 :: Gerenciamento de Reduzido'
  ClientHeight = 622
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 466
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 237
      Width = 1008
      Height = 229
      ActivePage = TabSheet7
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Movimento de estoque '
        object PageControl5: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          ActivePage = TabSheet26
          Align = alClient
          TabOrder = 0
          object TabSheet15: TTabSheet
            Caption = ' Normal '
            object DBGrid13: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsStqMovItsA
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet19: TTabSheet
            Caption = ' Morto '
            ImageIndex = 1
            object DBGrid14: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsStqMovItsB
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet20: TTabSheet
            Caption = ' Balan'#231'o '
            ImageIndex = 2
            object DBGrid15: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsStqBalIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet21: TTabSheet
            Caption = ' Manual '
            ImageIndex = 3
            object DBGrid17: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsStqManIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet24: TTabSheet
            Caption = ' Evio a terceiros'
            ImageIndex = 4
            object DBGrid19: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsCMPPVaiIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet25: TTabSheet
            Caption = ' Entrada de terceiros'
            ImageIndex = 5
            object DBGrid20: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsCMPTInn
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet26: TTabSheet
            Caption = ' Entrada de MP '
            ImageIndex = 6
            object DBGrid21: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsNFeMPIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' NF-e(s) emitidas '
        ImageIndex = 1
        object PageControl3: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          ActivePage = TabSheet27
          Align = alClient
          TabOrder = 0
          object TabSheet16: TTabSheet
            Caption = ' Pedidos '
            object PageControl4: TPageControl
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              ActivePage = TabSheet10
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 990
              ExplicitHeight = 113
              object TabSheet9: TTabSheet
                Caption = ' Normais '
                object DBGrid11: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 758
                  Height = 196
                  Align = alClient
                  DataSource = DsPediVdaIts
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
              object TabSheet10: TTabSheet
                Caption = ' Customizados '
                ImageIndex = 1
                object DBGrid12: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 758
                  Height = 196
                  Align = alClient
                  DataSource = DsPediVdaCuz
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
            end
          end
          object TabSheet17: TTabSheet
            Caption = ' Faturamento '
            ImageIndex = 1
            object PageControl2: TPageControl
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              ActivePage = TabSheet14
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 990
              ExplicitHeight = 113
              object TabSheet11: TTabSheet
                Caption = ' Pedido condicional '
                ImageIndex = 2
                object DBGrid6: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 758
                  Height = 196
                  Align = alClient
                  DataSource = DsFatConIts
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
              object TabSheet12: TTabSheet
                Caption = ' Venda direta??? '
                ImageIndex = 3
                object DBGrid5: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 758
                  Height = 196
                  Align = alClient
                  DataSource = DsFatVenIts
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
              object TabSheet13: TTabSheet
                Caption = ' Venda pela refer'#234'ncia '
                ImageIndex = 4
                object DBGrid7: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 758
                  Height = 196
                  Align = alClient
                  DataSource = DsFatDivRef
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
              object TabSheet14: TTabSheet
                Caption = ' Venda de customizados '
                ImageIndex = 5
                object DBGrid8: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 758
                  Height = 196
                  Align = alClient
                  DataSource = DsFatVenCuz
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
            end
          end
          object TabSheet18: TTabSheet
            Caption = 'Valores $'
            ImageIndex = 2
            object DBGrid16: TDBGrid
              Left = 0
              Top = 0
              Width = 766
              Height = 224
              Align = alClient
              DataSource = DsStqMovValA
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet27: TTabSheet
            Caption = ' Itens NF-e'
            ImageIndex = 3
            object DBGrid22: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsNFeItsI
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'NFe Compra MP'
        ImageIndex = 2
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          Align = alClient
          DataSource = DsNFeMPInn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'CECT - Entrada'
        ImageIndex = 3
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          Align = alClient
          DataSource = DsCECTInn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'CliInt PQ'
        ImageIndex = 4
        object DBGrid3: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          Align = alClient
          DataSource = DsPQCli
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' Etiquetas '
        ImageIndex = 5
        object DBGrid4: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          Align = alClient
          DataSource = DsEtqGeraIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet7: TTabSheet
        Caption = ' Pre'#231'os  '
        ImageIndex = 6
        object PageControl6: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          ActivePage = TabSheet22
          Align = alClient
          TabOrder = 0
          object TabSheet22: TTabSheet
            Caption = ' Listas '
            object DBGrid18: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsGraGruVal
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object TabSheet23: TTabSheet
            Caption = ' Tabelas '
            ImageIndex = 1
            object DBGrid10: TDBGrid
              Left = 0
              Top = 0
              Width = 992
              Height = 173
              Align = alClient
              DataSource = DsTabePrcGrI
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Embalagens '
        ImageIndex = 7
        object DBGrid9: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 201
          Align = alClient
          DataSource = DsGraGruE
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 237
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 169
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 8
          Top = 28
          Width = 131
          Height = 13
          Caption = 'Tipo de Grupo de Produtos:'
          FocusControl = DBEdit2
        end
        object Label1: TLabel
          Left = 8
          Top = 76
          Width = 33
          Height = 13
          Caption = 'Nivel2:'
          FocusControl = DBEdit1
        end
        object Label3: TLabel
          Left = 8
          Top = 52
          Width = 33
          Height = 13
          Caption = 'Nivel3:'
          FocusControl = DBEdit5
        end
        object Label5: TLabel
          Left = 8
          Top = 100
          Width = 33
          Height = 13
          Caption = 'Nivel1:'
          FocusControl = DBEdit8
        end
        object Label4: TLabel
          Left = 144
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit9
        end
        object Label6: TLabel
          Left = 224
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdit10
        end
        object Label7: TLabel
          Left = 916
          Top = 8
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdit11
        end
        object DBText3: TDBText
          Left = 44
          Top = 52
          Width = 97
          Height = 17
          DataField = 'TitNiv3'
          DataSource = DsGraGruX
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 44
          Top = 80
          Width = 97
          Height = 17
          DataField = 'TitNiv2'
          DataSource = DsGraGruX
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText1: TDBText
          Left = 44
          Top = 100
          Width = 97
          Height = 17
          DataField = 'TitNiv1'
          DataSource = DsGraGruX
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 8
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
          FocusControl = DBEdGGXAtu
        end
        object Label10: TLabel
          Left = 8
          Top = 124
          Width = 129
          Height = 13
          Caption = 'C'#243'digo do item SPED EFD:'
          FocusControl = DBEdit13
        end
        object Label11: TLabel
          Left = 8
          Top = 148
          Width = 88
          Height = 13
          Caption = 'Grupo de estoque:'
          FocusControl = DBEdit14
        end
        object SbGraGruY: TSpeedButton
          Left = 916
          Top = 144
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SbGraGruYClick
        end
        object DBEdit2: TDBEdit
          Left = 916
          Top = 24
          Width = 76
          Height = 21
          DataField = 'PrdGrupTip'
          DataSource = DsGraGruX
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 224
          Top = 24
          Width = 688
          Height = 21
          DataField = 'NO_PGT'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 916
          Top = 72
          Width = 76
          Height = 21
          DataField = 'Nivel2'
          DataSource = DsGraGruX
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 224
          Top = 72
          Width = 688
          Height = 21
          DataField = 'NO_GG2'
          DataSource = DsGraGruX
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 916
          Top = 48
          Width = 76
          Height = 21
          DataField = 'Nivel3'
          DataSource = DsGraGruX
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 224
          Top = 48
          Width = 688
          Height = 21
          DataField = 'NO_GG3'
          DataSource = DsGraGruX
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 224
          Top = 96
          Width = 688
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsGraGruX
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Left = 916
          Top = 96
          Width = 76
          Height = 21
          DataField = 'Nivel1'
          DataSource = DsGraGruX
          TabOrder = 7
        end
        object DBEdit9: TDBEdit
          Left = 144
          Top = 24
          Width = 76
          Height = 21
          DataField = 'CU_PGT'
          DataSource = DsGraGruX
          TabOrder = 8
        end
        object DBEdit10: TDBEdit
          Left = 144
          Top = 48
          Width = 76
          Height = 21
          DataField = 'CU_GG3'
          DataSource = DsGraGruX
          TabOrder = 9
        end
        object DBEdit11: TDBEdit
          Left = 144
          Top = 72
          Width = 76
          Height = 21
          DataField = 'CU_GG2'
          DataSource = DsGraGruX
          TabOrder = 10
        end
        object DBEdit12: TDBEdit
          Left = 144
          Top = 96
          Width = 76
          Height = 21
          DataField = 'CU_GG1'
          DataSource = DsGraGruX
          TabOrder = 11
        end
        object DBEdGGXAtu: TDBEdit
          Left = 60
          Top = 4
          Width = 77
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsGraGruX
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 12
        end
        object DBEdit13: TDBEdit
          Left = 144
          Top = 120
          Width = 769
          Height = 21
          DataField = 'COD_ITEM'
          DataSource = DsGraGruX
          TabOrder = 13
        end
        object DBEdit14: TDBEdit
          Left = 224
          Top = 144
          Width = 689
          Height = 21
          DataField = 'NO_GGY'
          DataSource = DsGraGruX
          TabOrder = 14
        end
        object DBEdit15: TDBEdit
          Left = 144
          Top = 144
          Width = 76
          Height = 21
          DataField = 'GraGruY'
          DataSource = DsGraGruX
          TabOrder = 15
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 169
        Width = 1008
        Height = 68
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label9: TLabel
          Left = 8
          Top = 4
          Width = 186
          Height = 13
          Caption = 'Transferir lan'#231'amentos para o reduzido:'
        end
        object LaAviso: TLabel
          Left = 0
          Top = 38
          Width = 1008
          Height = 13
          Align = alBottom
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 13
        end
        object EdReduzido: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBReduzido
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBReduzido: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 929
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsReduzidos
          TabOrder = 1
          dmkEditCB = EdReduzido
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object PB1: TProgressBar
          Left = 0
          Top = 51
          Width = 1008
          Height = 17
          Align = alBottom
          TabOrder = 2
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 743
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 345
        Height = 32
        Caption = 'Gerenciamento de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 345
        Height = 32
        Caption = 'Gerenciamento de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 345
        Height = 32
        Caption = 'Gerenciamento de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 514
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 558
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtTransferir: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Transferir'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTransferirClick
      end
      object CkExcluir: TCheckBox
        Left = 156
        Top = 16
        Width = 273
        Height = 17
        Caption = 'Excluir Reduzido em gerenciamento.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 488
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtAlteraClick
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraGruXAfterOpen
    BeforeClose = QrGraGruXBeforeClose
    SQL.Strings = (
      'SELECT pgt.TitNiv3, pgt.TitNiv2, pgt.TitNiv1,'
      'CONCAT(gg1.Nome, IF(gti.PrintTam=0, "", '
      'CONCAT(" ", gti.Nome)), IF(gcc.PrintCor=0,"", '
      'CONCAT(" ", gcc.Nome))) NO_PRD_TAM_COR, gg1.Nivel1, '
      'gg1.CodUsu CU_GG1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, gg1.Nivel2,'
      'gg2.CodUsu CU_GG2, gg2.Nome NO_GG2, gg3.CodUsu CU_GG3,'
      'gg1.Nivel3, gg3.Nome NO_GG3, pgt.CodUsu CU_PGT,'
      'ggx.Controle GraGruX, ggx.COD_ITEM, '
      'ggx.GraGruY, ggy.Nome NO_GGY '
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2    gg2 ON gg1.Nivel2=gg2.Nivel2'
      'LEFT JOIN gragru3    gg3 ON gg1.Nivel3=gg3.Nivel3'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'WHERE ggx.Controle=:P0')
    Left = 296
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruXNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrGraGruXSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrGraGruXGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraGruXNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGruXNO_GG2: TWideStringField
      FieldName = 'NO_GG2'
      Size = 30
    end
    object QrGraGruXNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGruXNO_GG3: TWideStringField
      FieldName = 'NO_GG3'
      Size = 30
    end
    object QrGraGruXCU_GG1: TIntegerField
      FieldName = 'CU_GG1'
      Required = True
    end
    object QrGraGruXCU_GG2: TIntegerField
      FieldName = 'CU_GG2'
      Required = True
    end
    object QrGraGruXCU_GG3: TIntegerField
      FieldName = 'CU_GG3'
      Required = True
    end
    object QrGraGruXCU_PGT: TIntegerField
      FieldName = 'CU_PGT'
      Required = True
    end
    object QrGraGruXTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrGraGruXTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrGraGruXTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXCOD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrGraGruXNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 296
    Top = 96
  end
  object QrCECTInn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM CECTInn'
      'WHERE GraGruX=:P0'
      '')
    Left = 60
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCECTInnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCECTInnCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCECTInnDataE: TDateField
      FieldName = 'DataE'
    end
    object QrCECTInnNFInn: TIntegerField
      FieldName = 'NFInn'
    end
    object QrCECTInnNFRef: TIntegerField
      FieldName = 'NFRef'
    end
    object QrCECTInnInnQtdkg: TFloatField
      FieldName = 'InnQtdkg'
    end
    object QrCECTInnInnQtdPc: TFloatField
      FieldName = 'InnQtdPc'
    end
    object QrCECTInnInnQtdVal: TFloatField
      FieldName = 'InnQtdVal'
    end
    object QrCECTInnSdoQtdkg: TFloatField
      FieldName = 'SdoQtdkg'
    end
    object QrCECTInnSdoQtdPc: TFloatField
      FieldName = 'SdoQtdPc'
    end
    object QrCECTInnSdoQtdVal: TFloatField
      FieldName = 'SdoQtdVal'
    end
    object QrCECTInnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCECTInnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCECTInnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCECTInnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCECTInnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCECTInnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCECTInnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCECTInnEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCECTInnInnQtdM2: TFloatField
      FieldName = 'InnQtdM2'
    end
    object QrCECTInnSdoQtdM2: TFloatField
      FieldName = 'SdoQtdM2'
    end
    object QrCECTInnGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCECTInnTipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrCECTInnrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrCECTInnmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrCECTInnSerie: TIntegerField
      FieldName = 'Serie'
    end
  end
  object DsCECTInn: TDataSource
    DataSet = QrCECTInn
    Left = 88
    Top = 392
  end
  object QrNFeMPInn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM NFeMPIts'
      'WHERE GraGruX=:P0')
    Left = 56
    Top = 420
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeMPInnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeMPInnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeMPInnGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeMPInnPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNFeMPInnPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrNFeMPInnAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrNFeMPInnAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrNFeMPInnCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrNFeMPInnGerBxaEstq: TIntegerField
      FieldName = 'GerBxaEstq'
    end
    object QrNFeMPInnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeMPInnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeMPInnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeMPInnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeMPInnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeMPInnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeMPInnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeMPInn: TDataSource
    DataSet = QrNFeMPInn
    Left = 84
    Top = 420
  end
  object DsPQCli: TDataSource
    DataSet = QrPQCli
    Left = 84
    Top = 448
  end
  object QrPQCli: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pqcli'
      'WHERE Controle=:P0'
      '')
    Left = 56
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQCliControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPQCliPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrPQCliCI: TIntegerField
      FieldName = 'CI'
    end
    object QrPQCliMinEstq: TFloatField
      FieldName = 'MinEstq'
    end
    object QrPQCliEstSegur: TIntegerField
      FieldName = 'EstSegur'
    end
    object QrPQCliCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrPQCliMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
    end
    object QrPQCliPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrPQCliValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPQCliCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrPQCliLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQCliDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQCliDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQCliUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQCliUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQCliAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQCliAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEtqGeraIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM etqgeraits'
      'WHERE GraGruX=:P0'
      '')
    Left = 56
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqGeraItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqGeraItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEtqGeraItsSequencia: TIntegerField
      FieldName = 'Sequencia'
    end
  end
  object DsEtqGeraIts: TDataSource
    DataSet = QrEtqGeraIts
    Left = 84
    Top = 476
  end
  object QrFatConIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM fatconits'
      'WHERE GraGruX=:P0')
    Left = 116
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatConItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatConItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFatConItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFatConItsPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrFatConItsPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrFatConItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrFatConItsQuantC: TFloatField
      FieldName = 'QuantC'
    end
    object QrFatConItsQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrFatConItsValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrFatConItsDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrFatConItsDescoV: TFloatField
      FieldName = 'DescoV'
    end
    object QrFatConItsValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrFatConItsPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrFatConItsValFat: TFloatField
      FieldName = 'ValFat'
    end
    object QrFatConItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFatConItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFatConItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFatConItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFatConItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFatConItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFatConItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFatConIts: TDataSource
    DataSet = QrFatConIts
    Left = 144
    Top = 392
  end
  object QrFatDivRef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM fatdivref'
      'WHERE GraGruX=:P0')
    Left = 112
    Top = 420
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatDivRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatDivRefIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrFatDivRefStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrFatDivRefReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 10
    end
    object QrFatDivRefGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFatDivRefQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFatDivRefPrecoOri: TFloatField
      FieldName = 'PrecoOri'
    end
    object QrFatDivRefPrecoPrz: TFloatField
      FieldName = 'PrecoPrz'
    end
    object QrFatDivRefPrecoAut: TFloatField
      FieldName = 'PrecoAut'
    end
    object QrFatDivRefPrecoVen: TFloatField
      FieldName = 'PrecoVen'
    end
    object QrFatDivRefValorTotBru: TFloatField
      FieldName = 'ValorTotBru'
    end
    object QrFatDivRefPerDescoVal: TFloatField
      FieldName = 'PerDescoVal'
    end
    object QrFatDivRefValorTotLiq: TFloatField
      FieldName = 'ValorTotLiq'
    end
    object QrFatDivRefPerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrFatDivRefValComisLiqF: TFloatField
      FieldName = 'ValComisLiqF'
    end
    object QrFatDivRefPerComissR: TFloatField
      FieldName = 'PerComissR'
    end
    object QrFatDivRefValComisLiqR: TFloatField
      FieldName = 'ValComisLiqR'
    end
    object QrFatDivRefPerComissT: TFloatField
      FieldName = 'PerComissT'
    end
    object QrFatDivRefValComisBruT: TFloatField
      FieldName = 'ValComisBruT'
    end
    object QrFatDivRefValComisDesT: TFloatField
      FieldName = 'ValComisDesT'
    end
    object QrFatDivRefValComisLiqT: TFloatField
      FieldName = 'ValComisLiqT'
    end
    object QrFatDivRefPerComissNiv: TWideStringField
      FieldName = 'PerComissNiv'
      Size = 1
    end
    object QrFatDivRefVolCnta: TIntegerField
      FieldName = 'VolCnta'
    end
    object QrFatDivRefTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
    object QrFatDivRefPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrFatDivRefPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrFatDivRefAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrFatDivRefAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrFatDivRefLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFatDivRefDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFatDivRefDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFatDivRefUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFatDivRefUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFatDivRefAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFatDivRefAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFatDivRef: TDataSource
    DataSet = QrFatDivRef
    Left = 140
    Top = 420
  end
  object QrFatVenIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM fatvenits'
      'WHERE GraGruX=:P0'
      '')
    Left = 112
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatVenItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatVenItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFatVenItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFatVenItsPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrFatVenItsPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrFatVenItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrFatVenItsQuantC: TFloatField
      FieldName = 'QuantC'
    end
    object QrFatVenItsQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrFatVenItsValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrFatVenItsDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrFatVenItsDescoV: TFloatField
      FieldName = 'DescoV'
    end
    object QrFatVenItsValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrFatVenItsPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrFatVenItsMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrFatVenItsMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrFatVenItsMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrFatVenItsMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrFatVenItsPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrFatVenItsCustomizad: TSmallintField
      FieldName = 'Customizad'
    end
    object QrFatVenItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrFatVenItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFatVenItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFatVenItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFatVenItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFatVenItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFatVenItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFatVenItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFatVenIts: TDataSource
    DataSet = QrFatVenIts
    Left = 140
    Top = 448
  end
  object QrFatVenCuz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM fatvencuz'
      'WHERE GraGruX=:P0'
      '')
    Left = 112
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatVenCuzControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFatVenCuzConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFatVenCuzMatPartCad: TIntegerField
      FieldName = 'MatPartCad'
    end
    object QrFatVenCuzGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFatVenCuzMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrFatVenCuzMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrFatVenCuzMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrFatVenCuzMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrFatVenCuzQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrFatVenCuzQuantX: TFloatField
      FieldName = 'QuantX'
    end
    object QrFatVenCuzPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrFatVenCuzPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrFatVenCuzPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrFatVenCuzValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrFatVenCuzDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrFatVenCuzDescoV: TFloatField
      FieldName = 'DescoV'
    end
    object QrFatVenCuzPerCustom: TFloatField
      FieldName = 'PerCustom'
    end
    object QrFatVenCuzValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrFatVenCuzTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
    object QrFatVenCuzLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFatVenCuzDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFatVenCuzDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFatVenCuzUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFatVenCuzUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFatVenCuzAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFatVenCuzAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFatVenCuz: TDataSource
    DataSet = QrFatVenCuz
    Left = 140
    Top = 476
  end
  object QrGraGruE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM gragrue'
      'WHERE GraGruX=:P0')
    Left = 724
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruEcProd: TWideStringField
      FieldName = 'cProd'
      Size = 60
    end
    object QrGraGruENivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruEFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrGraGruEEmbalagem: TIntegerField
      FieldName = 'Embalagem'
    end
    object QrGraGruEObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrGraGruELk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruEDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruEDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruEUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruEUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGruEGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruExProd: TWideStringField
      FieldName = 'xProd'
      Size = 120
    end
    object QrGraGruENCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 8
    end
    object QrGraGruECFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrGraGruEuCom: TWideStringField
      FieldName = 'uCom'
      Required = True
      Size = 6
    end
    object QrGraGruECFOP_Inn: TIntegerField
      FieldName = 'CFOP_Inn'
    end
  end
  object DsGraGruE: TDataSource
    DataSet = QrGraGruE
    Left = 724
    Top = 392
  end
  object QrGraGruVal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM gragruval'
      'WHERE GraGruX=:P0')
    Left = 168
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruValControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruValGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruValGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruValGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
    object QrGraGruValLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruValDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruValDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruValUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruValUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruValAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruValAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsGraGruVal: TDataSource
    DataSet = QrGraGruVal
    Left = 196
    Top = 428
  end
  object QrPediVdaIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pedivdaits'
      'WHERE GraGruX=:P0')
    Left = 168
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPediVdaItsPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrPediVdaItsPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaItsQuantC: TFloatField
      FieldName = 'QuantC'
    end
    object QrPediVdaItsQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrPediVdaItsValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrPediVdaItsDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrPediVdaItsDescoV: TFloatField
      FieldName = 'DescoV'
    end
    object QrPediVdaItsValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrPediVdaItsPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrPediVdaItsMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrPediVdaItsMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrPediVdaItsMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrPediVdaItsMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrPediVdaItsPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrPediVdaItsCustomizad: TSmallintField
      FieldName = 'Customizad'
    end
    object QrPediVdaItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrPediVdaItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPediVdaItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPediVdaItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPediVdaItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPediVdaItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPediVdaItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPediVdaItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPediVdaItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPediVdaItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrPediVdaItsMulti: TIntegerField
      FieldName = 'Multi'
    end
  end
  object DsPediVdaIts: TDataSource
    DataSet = QrPediVdaIts
    Left = 196
    Top = 456
  end
  object QrPediVdaCuz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM PediVdaCuz'
      'WHERE GraGruX=:P0')
    Left = 168
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsPediVdaCuz: TDataSource
    DataSet = QrPediVdaCuz
    Left = 196
    Top = 484
  end
  object QrStqBalIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM StqBalIts'
      'WHERE GraGruX=:P0')
    Left = 224
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqBalItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqBalItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrStqBalItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrStqBalItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrStqBalItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqBalItsQtdLei: TFloatField
      FieldName = 'QtdLei'
    end
    object QrStqBalItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrStqBalItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqBalItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsStqBalIts: TDataSource
    DataSet = QrStqBalIts
    Left = 252
    Top = 400
  end
  object QrStqMovItsA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM stqmovitsa'
      'WHERE GraGruX=:P0'
      '')
    Left = 224
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqMovItsADataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrStqMovItsAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrStqMovItsATipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrStqMovItsAOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrStqMovItsAOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrStqMovItsAOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrStqMovItsAOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrStqMovItsAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqMovItsAStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrStqMovItsAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqMovItsAQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrStqMovItsAPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrStqMovItsAPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrStqMovItsAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqMovItsAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrStqMovItsAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrStqMovItsAAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrStqMovItsAFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrStqMovItsAQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrStqMovItsARetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrStqMovItsAParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrStqMovItsAParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrStqMovItsADebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrStqMovItsASMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrStqMovItsACustoAll: TFloatField
      FieldName = 'CustoAll'
    end
    object QrStqMovItsAValorAll: TFloatField
      FieldName = 'ValorAll'
    end
    object QrStqMovItsAGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrStqMovItsABaixa: TSmallintField
      FieldName = 'Baixa'
    end
    object QrStqMovItsAAntQtde: TFloatField
      FieldName = 'AntQtde'
    end
  end
  object DsStqMovItsA: TDataSource
    DataSet = QrStqMovItsA
    Left = 252
    Top = 428
  end
  object QrStqMovItsB: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM stqmovitsb'
      'WHERE GraGruX=:P0'
      '')
    Left = 224
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqMovItsBDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrStqMovItsBIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrStqMovItsBTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrStqMovItsBOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrStqMovItsBOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrStqMovItsBOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrStqMovItsBOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrStqMovItsBEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqMovItsBStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrStqMovItsBGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqMovItsBQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrStqMovItsBPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrStqMovItsBPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrStqMovItsBAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqMovItsBAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrStqMovItsBAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrStqMovItsBAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrStqMovItsBFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrStqMovItsBQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrStqMovItsBRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrStqMovItsBParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrStqMovItsBParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrStqMovItsBDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrStqMovItsBSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrStqMovItsBCustoAll: TFloatField
      FieldName = 'CustoAll'
    end
    object QrStqMovItsBValorAll: TFloatField
      FieldName = 'ValorAll'
    end
    object QrStqMovItsBGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrStqMovItsBBaixa: TSmallintField
      FieldName = 'Baixa'
    end
    object QrStqMovItsBAntQtde: TFloatField
      FieldName = 'AntQtde'
    end
  end
  object DsStqMovItsB: TDataSource
    DataSet = QrStqMovItsB
    Left = 252
    Top = 456
  end
  object QrStqMovValA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM stqmovvala'
      'WHERE GraGruX=:P0')
    Left = 224
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqMovValAID: TIntegerField
      FieldName = 'ID'
    end
    object QrStqMovValAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrStqMovValATipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrStqMovValAOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrStqMovValAOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrStqMovValAOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrStqMovValASeqInReduz: TIntegerField
      FieldName = 'SeqInReduz'
    end
    object QrStqMovValAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqMovValAStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrStqMovValAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqMovValAQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrStqMovValAPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrStqMovValATotal: TFloatField
      FieldName = 'Total'
    end
    object QrStqMovValACFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 6
    end
    object QrStqMovValAInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrStqMovValAPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrStqMovValAMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrStqMovValAMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrStqMovValAMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrStqMovValAMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrStqMovValAMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
    object QrStqMovValATipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrStqMovValArefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrStqMovValAmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrStqMovValASerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrStqMovValAnNF: TIntegerField
      FieldName = 'nNF'
    end
    object QrStqMovValASitDevolu: TSmallintField
      FieldName = 'SitDevolu'
    end
    object QrStqMovValAServico: TIntegerField
      FieldName = 'Servico'
    end
    object QrStqMovValARefProd: TIntegerField
      FieldName = 'RefProd'
    end
    object QrStqMovValACFOP_Contrib: TSmallintField
      FieldName = 'CFOP_Contrib'
    end
    object QrStqMovValACFOP_MesmaUF: TSmallintField
      FieldName = 'CFOP_MesmaUF'
    end
    object QrStqMovValACFOP_Proprio: TSmallintField
      FieldName = 'CFOP_Proprio'
    end
    object QrStqMovValAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqMovValAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrStqMovValAprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrStqMovValACSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrStqMovValApCredSN: TFloatField
      FieldName = 'pCredSN'
    end
    object QrStqMovValAFinaliCli: TSmallintField
      FieldName = 'FinaliCli'
    end
  end
  object DsStqMovValA: TDataSource
    DataSet = QrStqMovValA
    Left = 252
    Top = 484
  end
  object QrStqManIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqmanits'
      'WHERE GraGruX=:P0')
    Left = 280
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqManItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqManItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrStqManItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqManItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrStqManItsPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrStqManItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrStqManItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrStqManItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrStqManItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrStqManItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrStqManItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrStqManItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqManItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsStqManIts: TDataSource
    DataSet = QrStqManIts
    Left = 308
    Top = 400
  end
  object QrTabePrcGrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM tabeprcgri'
      'WHERE GraGruX=:P0')
    Left = 280
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTabePrcGrIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTabePrcGrIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTabePrcGrIGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrTabePrcGrITabePrcCab: TIntegerField
      FieldName = 'TabePrcCab'
    end
    object QrTabePrcGrIPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrTabePrcGrILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTabePrcGrIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTabePrcGrIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTabePrcGrIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTabePrcGrIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTabePrcGrIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTabePrcGrIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTabePrcGrI: TDataSource
    DataSet = QrTabePrcGrI
    Left = 308
    Top = 428
  end
  object QrCMPPVaiIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cmppvaiits'
      'WHERE GraGruX=:P0')
    Left = 280
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCMPPVaiItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCMPPVaiItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCMPPVaiItsOutQtdkg: TFloatField
      FieldName = 'OutQtdkg'
    end
    object QrCMPPVaiItsOutQtdPc: TFloatField
      FieldName = 'OutQtdPc'
    end
    object QrCMPPVaiItsOutQtdM2: TFloatField
      FieldName = 'OutQtdM2'
    end
    object QrCMPPVaiItsOutQtdVal: TFloatField
      FieldName = 'OutQtdVal'
    end
    object QrCMPPVaiItsGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrCMPPVaiItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCMPPVaiItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCMPPVaiItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCMPPVaiItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCMPPVaiItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCMPPVaiItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCMPPVaiItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCMPPVaiItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCMPPVaiItsMPInn: TIntegerField
      FieldName = 'MPInn'
    end
  end
  object DsCMPPVaiIts: TDataSource
    DataSet = QrCMPPVaiIts
    Left = 308
    Top = 456
  end
  object QrCMPTInn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cmptinn'
      'WHERE GraGruX=:P0')
    Left = 280
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCMPTInnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCMPTInnEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCMPTInnCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCMPTInnDataE: TDateField
      FieldName = 'DataE'
    end
    object QrCMPTInnNFInn: TIntegerField
      FieldName = 'NFInn'
    end
    object QrCMPTInnNFRef: TIntegerField
      FieldName = 'NFRef'
    end
    object QrCMPTInnInnQtdkg: TFloatField
      FieldName = 'InnQtdkg'
    end
    object QrCMPTInnInnQtdPc: TFloatField
      FieldName = 'InnQtdPc'
    end
    object QrCMPTInnInnQtdM2: TFloatField
      FieldName = 'InnQtdM2'
    end
    object QrCMPTInnInnQtdVal: TFloatField
      FieldName = 'InnQtdVal'
    end
    object QrCMPTInnSdoQtdkg: TFloatField
      FieldName = 'SdoQtdkg'
    end
    object QrCMPTInnSdoQtdPc: TFloatField
      FieldName = 'SdoQtdPc'
    end
    object QrCMPTInnSdoQtdM2: TFloatField
      FieldName = 'SdoQtdM2'
    end
    object QrCMPTInnSdoQtdVal: TFloatField
      FieldName = 'SdoQtdVal'
    end
    object QrCMPTInnGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCMPTInnTipoNF: TSmallintField
      FieldName = 'TipoNF'
    end
    object QrCMPTInnrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrCMPTInnmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrCMPTInnSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrCMPTInnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCMPTInnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCMPTInnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCMPTInnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCMPTInnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCMPTInnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCMPTInnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCMPTInnjaAtz: TSmallintField
      FieldName = 'jaAtz'
    end
  end
  object DsCMPTInn: TDataSource
    DataSet = QrCMPTInn
    Left = 308
    Top = 484
  end
  object QrNFeMPIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfempits'
      'WHERE GraGruX=:P0')
    Left = 336
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeMPItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeMPItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeMPItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeMPItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNFeMPItsPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrNFeMPItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrNFeMPItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrNFeMPItsCMPValor: TFloatField
      FieldName = 'CMPValor'
    end
    object QrNFeMPItsGerBxaEstq: TIntegerField
      FieldName = 'GerBxaEstq'
    end
    object QrNFeMPItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeMPItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeMPItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeMPItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeMPItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeMPItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeMPItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeMPIts: TDataSource
    DataSet = QrNFeMPIts
    Left = 364
    Top = 400
  end
  object QrNFeItsI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeitsi'
      'WHERE FatID=1'
      'AND prod_cProd=:P0')
    Left = 336
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeItsIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeItsIFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrNFeItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrNFeItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrNFeItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrNFeItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrNFeItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrNFeItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrNFeItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrNFeItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrNFeItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrNFeItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrNFeItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrNFeItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrNFeItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrNFeItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrNFeItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrNFeItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrNFeItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrNFeItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrNFeItsI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrNFeItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrNFeItsIEhServico: TIntegerField
      FieldName = 'EhServico'
    end
    object QrNFeItsILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeItsIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeItsIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeItsIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeItsIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeItsIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeItsIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeItsIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrNFeItsIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrNFeItsIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrNFeItsIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrNFeItsIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrNFeItsIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrNFeItsIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrNFeItsIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrNFeItsIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrNFeItsIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrNFeItsIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrNFeItsIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrNFeItsICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrNFeItsICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrNFeItsICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrNFeItsICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrNFeItsIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
    object QrNFeItsINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrNFeItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrNFeItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrNFeItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFeItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFeItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeItsIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
    end
    object QrNFeItsIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
    end
  end
  object DsNFeItsI: TDataSource
    DataSet = QrNFeItsI
    Left = 364
    Top = 428
  end
  object QrFormulasIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM formulasits'
      'WHERE EhGGX=1'
      'AND GraGruX=:P0')
    Left = 336
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsFormulasIts: TDataSource
    DataSet = QrFormulasIts
    Left = 364
    Top = 456
  end
  object mySQLQuery4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfempits'
      'WHERE GraGruX=:P0')
    Left = 496
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DataSource4: TDataSource
    DataSet = mySQLQuery4
    Left = 524
    Top = 424
  end
  object QrReduzidos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY NO_PRD_TAM_COR'
      '')
    Left = 296
    Top = 144
    object QrReduzidosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrReduzidosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsReduzidos: TDataSource
    DataSet = QrReduzidos
    Left = 296
    Top = 192
  end
  object QrGraGruDel: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragrudel'
      'WHERE Excluido=:P0')
    Left = 736
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruDelExcluido: TIntegerField
      FieldName = 'Excluido'
    end
    object QrGraGruDelReceptor: TIntegerField
      FieldName = 'Receptor'
    end
  end
  object PMGraGruY: TPopupMenu
    Left = 920
    Top = 224
    object ZeraocodigodoGrupodeEstoque1: TMenuItem
      Caption = '&Zera o codigo do Grupo de Estoque'
      OnClick = ZeraocodigodoGrupodeEstoque1Click
    end
  end
end
