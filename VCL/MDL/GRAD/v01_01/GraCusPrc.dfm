object FmGraCusPrc: TFmGraCusPrc
  Left = 368
  Top = 194
  Caption = 'PRD-PRECO-001 :: Cadastro de Lista de Pre'#231'os de Grades'
  ClientHeight = 508
  ClientWidth = 892
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 892
    Height = 412
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 892
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 633
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
    end
    object PainelEdi2: TPanel
      Left = 0
      Top = 52
      Width = 892
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Painel2: TPanel
        Left = 0
        Top = 0
        Width = 892
        Height = 52
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 440
          Top = 8
          Width = 36
          Height = 13
          Caption = 'Moeda:'
        end
        object RGCustoPreco: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 120
          Height = 52
          Align = alLeft
          Caption = ' Aplica'#231#227'o: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Custo'
            'Pre'#231'o')
          TabOrder = 0
          QryCampo = 'CustoPreco'
          UpdCampo = 'CustoPreco'
          UpdType = utYes
          OldValor = 0
        end
        object RGTipoCalc: TdmkRadioGroup
          Left = 120
          Top = 0
          Width = 313
          Height = 52
          Align = alLeft
          Caption = ' C'#225'lculo do valor: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Manual'
            'Pre'#231'o m'#233'dio'
            #218'ltima compra')
          TabOrder = 1
          QryCampo = 'TipoCalc'
          UpdCampo = 'TipoCalc'
          UpdType = utYes
          OldValor = 0
        end
        object EdMoeda: TdmkEditCB
          Left = 440
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBMoeda
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBMoeda: TdmkDBLookupComboBox
          Left = 496
          Top = 24
          Width = 281
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsCambioMda
          TabOrder = 3
          dmkEditCB = EdMoeda
          QryCampo = 'Moeda'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 348
      Width = 892
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 888
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 744
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 892
    Height = 412
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 892
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGraCusPrc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 20
        Width = 633
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsGraCusPrc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsGraCusPrc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 48
      Width = 892
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 892
        Height = 52
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 440
          Top = 8
          Width = 36
          Height = 13
          Caption = 'Moeda:'
        end
        object dmkRadioGroup1: TDBRadioGroup
          Left = 0
          Top = 0
          Width = 160
          Height = 52
          Align = alLeft
          Caption = ' Aplica'#231#227'o: (compatibilidade)'
          Columns = 2
          DataField = 'CustoPreco'
          DataSource = DsGraCusPrc
          Items.Strings = (
            'Custo'
            'Pre'#231'o')
          TabOrder = 0
          Values.Strings = (
            '0'
            '1')
        end
        object dmkRadioGroup2: TDBRadioGroup
          Left = 165
          Top = -1
          Width = 273
          Height = 52
          Caption = ' C'#225'lculo do valor: '
          Columns = 3
          DataField = 'TipoCalc'
          DataSource = DsGraCusPrc
          Items.Strings = (
            'Manual'
            'Pre'#231'o m'#233'dio'
            #218'ltima compra')
          TabOrder = 1
          Values.Strings = (
            '0'
            '1'
            '2')
        end
        object DBEdit2: TDBEdit
          Left = 476
          Top = 24
          Width = 305
          Height = 21
          DataField = 'NOMEMOEDA'
          DataSource = DsGraCusPrc
          TabOrder = 2
        end
        object DBEdit3: TDBEdit
          Left = 440
          Top = 24
          Width = 37
          Height = 21
          DataField = 'SIGLAMDA'
          DataSource = DsGraCusPrc
          TabOrder = 3
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 348
      Width = 892
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 195
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 369
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLista: TBitBtn
          Tag = 10107
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lista'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtListaClick
        end
        object BtUsuario: TBitBtn
          Tag = 10047
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Usu'#225'rio'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtUsuarioClick
        end
        object BtCusto: TBitBtn
          Tag = 10127
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Custo'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCustoClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 108
      Width = 892
      Height = 193
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = 'Usu'#225'rios'
        object DBGFunci: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 884
          Height = 165
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Usuario'
              Title.Caption = 'Usu'#225'rio'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Width = 105
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Funcionario'
              Title.Caption = 'Funcion'#225'rio'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEUSU'
              Title.Caption = 'Nome do Funcion'#225'rio'
              Width = 514
              Visible = True
            end>
          Color = clWindow
          DataSource = DsGraCusPrcU
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Usuario'
              Title.Caption = 'Usu'#225'rio'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Width = 105
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Funcionario'
              Title.Caption = 'Funcion'#225'rio'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEUSU'
              Title.Caption = 'Nome do Funcion'#225'rio'
              Width = 514
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Pre'#231'os'
        ImageIndex = 1
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 884
          Height = 81
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object LaCliInt: TLabel
            Left = 4
            Top = 8
            Width = 70
            Height = 13
            Caption = 'Cliente interno:'
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 124
            Top = 4
            Width = 390
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdEmpresa: TdmkEditCB
            Left = 76
            Top = 4
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdFiltro: TEdit
            Left = 341
            Top = 43
            Width = 173
            Height = 21
            TabOrder = 3
            OnChange = EdFiltroChange
          end
          object RGFiltro: TRadioGroup
            Left = 4
            Top = 31
            Width = 330
            Height = 40
            Caption = 'Filtrar por'
            Columns = 3
            Items.Strings = (
              'Nome do produto'
              'N'#237'vel 1'
              'Reduzido')
            TabOrder = 2
          end
        end
        object DBGCusto: TdmkDBGridZTO
          Left = 0
          Top = 98
          Width = 884
          Height = 67
          Align = alClient
          DataSource = DsGraGruVal
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          RowColors = <>
          OnDblClick = DBGCustoDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'CtrlGGV'
              Title.Caption = 'ID Pre'#231'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGru1'
              Title.Caption = 'Nivel 1'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome / tamanho / cor'
              Width = 500
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoPreco'
              Title.Caption = 'Custo / Pre'#231'o'
              Width = 88
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataIni'
              Title.Caption = 'Data inicial'
              Visible = True
            end>
        end
        object PB1: TProgressBar
          Left = 0
          Top = 81
          Width = 884
          Height = 17
          Align = alTop
          TabOrder = 2
          Visible = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 892
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 844
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 628
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 480
        Height = 32
        Caption = 'Cadastro de Lista de Pre'#231'os de Grades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 480
        Height = 32
        Caption = 'Cadastro de Lista de Pre'#231'os de Grades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 480
        Height = 32
        Caption = 'Cadastro de Lista de Pre'#231'os de Grades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 892
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 888
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 60
    Top = 56
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraCusPrcBeforeOpen
    AfterOpen = QrGraCusPrcAfterOpen
    BeforeClose = QrGraCusPrcBeforeClose
    AfterScroll = QrGraCusPrcAfterScroll
    SQL.Strings = (
      'SELECT gcp.Codigo, gcp.CodUsu, gcp.Nome,'
      'gcp.CustoPreco, gcp.TipoCalc, gcp.Moeda, '#13
      'mda.Nome NOMEMOEDA, mda.Sigla SIGLAMDA'
      'FROM gracusprc gcp'
      'LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda')
    Left = 32
    Top = 56
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraCusPrcCustoPreco: TSmallintField
      FieldName = 'CustoPreco'
      Required = True
    end
    object QrGraCusPrcTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrGraCusPrcMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrGraCusPrcNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrGraCusPrcSIGLAMDA: TWideStringField
      FieldName = 'SIGLAMDA'
      Size = 5
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtUsuario
    CanUpd01 = BtSaida
    Left = 88
    Top = 56
  end
  object QrCambioMda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Codusu, Sigla, Nome'
      'FROM cambiomda'
      'ORDER BY Nome')
    Left = 392
    Top = 72
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaCodusu: TIntegerField
      FieldName = 'Codusu'
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 420
    Top = 72
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdMoeda
    Panel = PainelEdita
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    ValueVariant = 0
    Left = 116
    Top = 56
  end
  object QrGraCusPrcU: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cpu.Controle, cpu.Usuario, '
      'sen.Login, sen.Funcionario, '
      'IF(ent.Tipo=0, RazaoSocial, Nome) NOMEUSU'
      'FROM gracusprcu cpu'
      'LEFT JOIN senhas sen ON cpu.Usuario=sen.Numero'
      'LEFT JOIN entidades ent ON ent.Codigo=sen.Funcionario'
      'WHERE cpu.Codigo=:P0')
    Left = 144
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCusPrcUControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraCusPrcUUsuario: TIntegerField
      FieldName = 'Usuario'
      Required = True
    end
    object QrGraCusPrcULogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrGraCusPrcUFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrGraCusPrcUNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 100
    end
  end
  object DsGraCusPrcU: TDataSource
    DataSet = QrGraCusPrcU
    Left = 172
    Top = 56
  end
  object PMLista: TPopupMenu
    OnPopup = PMListaPopup
    Left = 344
    Top = 416
    object Incluinovalista1: TMenuItem
      Caption = '&Inclui nova lista'
      OnClick = Incluinovalista1Click
    end
    object Alteralistaatual1: TMenuItem
      Caption = '&Altera lista atual'
      OnClick = Alteralistaatual1Click
    end
    object Excluilistaatual1: TMenuItem
      Caption = '&Exclui lista atual'
      Enabled = False
    end
  end
  object PMUsuario: TPopupMenu
    OnPopup = PMUsuarioPopup
    Left = 468
    Top = 420
    object Adicionausurio1: TMenuItem
      Caption = '&Adiciona usu'#225'rio'
      OnClick = Adicionausurio1Click
    end
    object Retirausurioatual1: TMenuItem
      Caption = '&Retira usu'#225'rios'
      OnClick = Retirausurioatual1Click
    end
  end
  object QrGraGruVal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, ggv.Controle CtrlGGV, '
      'ggv.CustoPreco, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle'
      '  AND ggv.GraCusPrc=1 AND ggv.Entidade=-11'
      ''
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 252
    Top = 64
    object QrGraGruValGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruValGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruValCtrlGGV: TIntegerField
      FieldName = 'CtrlGGV'
    end
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrGraGruValNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruValGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrGraGruValDataIni: TDateField
      FieldName = 'DataIni'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrGraGruValEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object DsGraGruVal: TDataSource
    DataSet = QrGraGruVal
    Left = 280
    Top = 64
  end
  object PMCusto: TPopupMenu
    Left = 584
    Top = 420
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
end
