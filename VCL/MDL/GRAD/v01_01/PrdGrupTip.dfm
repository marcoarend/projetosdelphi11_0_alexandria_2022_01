object FmPrdGrupTip: TFmPrdGrupTip
  Left = 368
  Top = 194
  Caption = 'PRD-GRUPO-002 :: Tipos de Grupos de Produtos'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 354
        Height = 32
        Caption = 'Tipos de Grupos de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 354
        Height = 32
        Caption = 'Tipos de Grupos de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 354
        Height = 32
        Caption = 'Tipos de Grupos de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel19: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 596
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 0
      Top = 301
      Width = 1008
      Height = 237
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Configura'#231#245'es '
        object PnGrids: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 269
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 0
            Top = 117
            Width = 1000
            Height = 10
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 160
            ExplicitWidth = 899
          end
          object Panel4: TPanel
            Left = 0
            Top = 127
            Width = 1000
            Height = 142
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object DBGAtributos: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 485
              Height = 142
              Align = alLeft
              DataSource = DsPrdGrupAtr
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Nome do Atributo'
                  Width = 380
                  Visible = True
                end>
            end
            object DBGCentros: TdmkDBGridZTO
              Left = 485
              Top = 0
              Width = 515
              Height = 142
              Align = alClient
              DataSource = DsPrdGrupCen
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'Centro'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEESTQCEN'
                  Title.Caption = 'Nome do centro de estoque'
                  Width = 307
                  Visible = True
                end>
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 117
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object DBGJanelas: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 1000
              Height = 117
              Align = alClient
              DataSource = DsPrdGrupJan
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 37
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Janela'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Width = 102
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o da janela'
                  Width = 718
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Produtos '
        ImageIndex = 1
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 209
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel8: TPanel
            Left = 461
            Top = 0
            Width = 539
            Height = 209
            Align = alClient
            Caption = 'Panel8'
            TabOrder = 0
            object DBGGraGru9: TdmkDBGridZTO
              Left = 1
              Top = 41
              Width = 537
              Height = 119
              Align = alClient
              DataSource = DsGraGru9
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnDrawColumnCell = DBGGraGru9DrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codusu'
                  Title.Caption = 'C'#243'digo'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 252
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nivel1'
                  Visible = True
                end>
            end
            object Panel9: TPanel
              Left = 1
              Top = 1
              Width = 537
              Height = 40
              Align = alTop
              BevelOuter = bvNone
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object Label19: TLabel
                Left = 0
                Top = 0
                Width = 270
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Produtos de outros tipos de grupos de produtos'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label21: TLabel
                Left = 4
                Top = 20
                Width = 24
                Height = 13
                Caption = 'Tipo:'
              end
              object SpeedButton5: TSpeedButton
                Left = 404
                Top = 16
                Width = 21
                Height = 21
                Caption = '?'
                OnClick = SpeedButton5Click
              end
              object EdTipo9: TdmkEdit
                Left = 32
                Top = 16
                Width = 38
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdTipo9Redefinido
              end
              object DBEdit14: TDBEdit
                Left = 72
                Top = 16
                Width = 329
                Height = 21
                DataField = 'Nome'
                DataSource = DsTipo9
                TabOrder = 1
              end
            end
            object Panel12: TPanel
              Left = 1
              Top = 160
              Width = 537
              Height = 48
              Align = alBottom
              TabOrder = 2
              object BtMove9: TBitBtn
                Left = 4
                Top = 4
                Width = 421
                Height = 40
                Cursor = crHandPoint
                Caption = 
                  '&Mover os itens selecionados para o Tipo de Grupo de Produtos at' +
                  'ual'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtMove9Click
              end
            end
          end
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 461
            Height = 209
            Align = alLeft
            BevelOuter = bvNone
            Caption = 'Panel8'
            TabOrder = 1
            object DBGGraGru1: TdmkDBGridZTO
              Left = 0
              Top = 20
              Width = 461
              Height = 141
              Align = alClient
              DataSource = DsGraGru1
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnDrawColumnCell = DBGGraGru1DrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Nivel1'
                  Title.Caption = 'ID N'#237'vel 1'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codusu'
                  Title.Caption = 'C.U. Niv1'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 258
                  Visible = True
                end>
            end
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 461
              Height = 20
              Align = alTop
              BevelOuter = bvNone
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHotLight
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              object Label20: TLabel
                Left = 0
                Top = 0
                Width = 316
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Produtos cadastrados neste Tipo de Grupo de Produtos'
              end
            end
            object Panel13: TPanel
              Left = 0
              Top = 161
              Width = 461
              Height = 48
              Align = alBottom
              TabOrder = 2
              object BtMove1: TBitBtn
                Left = 4
                Top = 4
                Width = 421
                Height = 40
                Cursor = crHandPoint
                Caption = 
                  '&Mover os itens selecionados para Tipo de Grupo de Produtos ao l' +
                  'ado (direito)'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtMove1Click
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Comiss'#245'es N'#237'veis 2 e 3'
        ImageIndex = 2
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 447
          Height = 209
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label26: TLabel
            Left = 0
            Top = 0
            Width = 41
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = ' N'#237'vel 3 '
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 13
            Width = 447
            Height = 196
            Align = alClient
            DataSource = DsGraComiss3
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 208
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PerComissF'
                Title.Caption = '% Faturam.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PerComissR'
                Title.Caption = '% Recebim.'
                Visible = True
              end>
          end
        end
        object Panel15: TPanel
          Left = 447
          Top = 0
          Width = 553
          Height = 209
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label27: TLabel
            Left = 0
            Top = 0
            Width = 41
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = ' N'#237'vel 2 '
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 13
            Width = 553
            Height = 196
            Align = alClient
            DataSource = DsGraComiss2
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 208
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PerComissF'
                Title.Caption = '% Faturam.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PerComissR'
                Title.Caption = '% Recebim.'
                Visible = True
              end>
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' CTB Niveis 2 e 3'
        ImageIndex = 3
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 447
          Height = 181
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 209
          object Label29: TLabel
            Left = 0
            Top = 0
            Width = 41
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = ' N'#237'vel 3 '
          end
          object DBGrid3: TDBGrid
            Left = 0
            Top = 13
            Width = 447
            Height = 168
            Align = alClient
            DataSource = DsGraPlaCta3
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 297
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Genero'
                Title.Caption = 'CTB'
                Width = 32
                Visible = True
              end>
          end
        end
        object Panel17: TPanel
          Left = 447
          Top = 0
          Width = 553
          Height = 181
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          ExplicitHeight = 209
          object Label30: TLabel
            Left = 0
            Top = 0
            Width = 41
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = ' N'#237'vel 2 '
          end
          object DBGrid4: TDBGrid
            Left = 0
            Top = 13
            Width = 553
            Height = 168
            Align = alClient
            DataSource = DsGraPlaCta2
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 297
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Genero'
                Title.Caption = 'CTB'
                Width = 32
                Visible = True
              end>
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 532
      Width = 1008
      Height = 64
      Align = alBottom
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 343
        Top = 15
        Width = 663
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel2: TPanel
          Left = 554
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 2
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtTipo: TBitBtn
          Tag = 435
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tipo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTipoClick
        end
        object BtJanelas: TBitBtn
          Tag = 10100
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Janelas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtJanelasClick
        end
        object BtAtributos: TBitBtn
          Tag = 10099
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Atributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAtributosClick
        end
        object BtCentros: TBitBtn
          Tag = 438
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Centros'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtCentrosClick
        end
        object BtComissao: TBitBtn
          Tag = 10031
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'Co&miss'#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Visible = False
          OnClick = BtComissaoClick
        end
        object BtCTB: TBitBtn
          Tag = 10101
          Left = 464
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'CT&B'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          Visible = False
          OnClick = BtCTBClick
        end
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 301
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 124
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label24: TLabel
        Left = 4
        Top = 220
        Width = 85
        Height = 13
        Caption = '% comiss'#227'o fatur.:'
      end
      object Label25: TLabel
        Left = 100
        Top = 220
        Width = 91
        Height = 13
        Caption = '% comiss'#227'o receb.:'
      end
      object Label28: TLabel
        Left = 196
        Top = 220
        Width = 300
        Height = 13
        Caption = 
          'Tipo de item - Atividades Industriais, Comerciais e Servi'#231'os [F3' +
          ']:'
        FocusControl = DBEdit9
      end
      object Label31: TLabel
        Left = 504
        Top = 220
        Width = 111
        Height = 13
        Caption = 'Conta CTB (Invent'#225'rio):'
        FocusControl = DBEdit11
      end
      object LaDBGraTabApp: TLabel
        Left = 623
        Top = 220
        Width = 172
        Height = 13
        Caption = 'Sele'#231#227'o de Agrega'#231#227'o de Produtos:'
        FocusControl = EdDBGraTabApp
      end
      object Label14: TLabel
        Left = 4
        Top = 260
        Width = 72
        Height = 13
        Caption = 'Grupo cont'#225'bil:'
      end
      object Label33: TLabel
        Left = 500
        Top = 260
        Width = 125
        Height = 13
        Caption = 'Conta do plano de contas:'
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPrdGrupTip
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 124
        Top = 20
        Width = 521
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPrdGrupTip
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 56
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsPrdGrupTip
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 4
        Top = 44
        Width = 221
        Height = 61
        Caption = ' Tipifica'#231#227'o: '
        Columns = 3
        DataField = 'TipPrd'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          'Nenhum'
          'Produto'
          'Mat. prima'
          'Uso/cons.'
          'Servi'#231'o'
          'Outros ins.')
        TabOrder = 4
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 224
        Top = 44
        Width = 137
        Height = 61
        Caption = ' Fabrica'#231#227'o: '
        Columns = 2
        DataField = 'MadeBy'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          'Nenhum'
          'Pr'#243'prio'
          'Terceiros')
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBRadioGroup3: TDBRadioGroup
        Left = 360
        Top = 44
        Width = 91
        Height = 61
        Caption = ' Fracionado: '
        Columns = 3
        DataField = 'Fracio'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
      object DBRadioGroup4: TDBRadioGroup
        Left = 450
        Top = 44
        Width = 91
        Height = 61
        Caption = ' N'#237'veis: '
        Columns = 3
        DataField = 'NivCad'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5')
        TabOrder = 7
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5')
      end
      object GroupBox2: TGroupBox
        Left = 754
        Top = 44
        Width = 163
        Height = 61
        Caption = ' Faixa de numera'#231#227'o: '
        TabOrder = 10
        object Label10: TLabel
          Left = 8
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Inicial'#185':'
        end
        object Label11: TLabel
          Left = 84
          Top = 16
          Width = 28
          Height = 13
          Caption = 'Final'#185':'
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          DataField = 'FaixaIni'
          DataSource = DsPrdGrupTip
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 84
          Top = 32
          Width = 72
          Height = 21
          DataField = 'FaixaFim'
          DataSource = DsPrdGrupTip
          TabOrder = 1
        end
      end
      object DBRadioGroup5: TDBRadioGroup
        Left = 540
        Top = 44
        Width = 65
        Height = 61
        Caption = ' Gradeado: '
        DataField = 'Gradeado'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 8
        Values.Strings = (
          '0'
          '1')
      end
      object GroupBox4: TGroupBox
        Left = 4
        Top = 105
        Width = 993
        Height = 61
        Caption = ' T'#237'tulos dos N'#237'veis: '
        TabOrder = 12
        object Label15: TLabel
          Left = 8
          Top = 16
          Width = 36
          Height = 13
          Caption = 'Nivel 1:'
          FocusControl = DBEdNome
        end
        object Label16: TLabel
          Left = 204
          Top = 16
          Width = 36
          Height = 13
          Caption = 'Nivel 2:'
          FocusControl = DBEdNome
        end
        object Label17: TLabel
          Left = 400
          Top = 16
          Width = 36
          Height = 13
          Caption = 'Nivel 3:'
          FocusControl = DBEdNome
        end
        object Label35: TLabel
          Left = 596
          Top = 16
          Width = 36
          Height = 13
          Caption = 'Nivel 4:'
          FocusControl = DBEdNome
        end
        object Label36: TLabel
          Left = 792
          Top = 16
          Width = 36
          Height = 13
          Caption = 'Nivel 5:'
          FocusControl = DBEdNome
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 32
          Width = 190
          Height = 21
          DataField = 'TitNiv1'
          DataSource = DsPrdGrupTip
          TabOrder = 0
        end
        object DBEdit5: TDBEdit
          Left = 204
          Top = 32
          Width = 190
          Height = 21
          DataField = 'TitNiv2'
          DataSource = DsPrdGrupTip
          TabOrder = 1
        end
        object DBEdit6: TDBEdit
          Left = 400
          Top = 32
          Width = 190
          Height = 21
          DataField = 'TitNiv3'
          DataSource = DsPrdGrupTip
          TabOrder = 2
        end
        object DBEdit12: TDBEdit
          Left = 596
          Top = 32
          Width = 190
          Height = 21
          DataField = 'TitNiv4'
          DataSource = DsPrdGrupTip
          TabOrder = 3
        end
        object DBEdit13: TDBEdit
          Left = 792
          Top = 32
          Width = 190
          Height = 21
          DataField = 'TitNiv5'
          DataSource = DsPrdGrupTip
          TabOrder = 4
        end
      end
      object DBRadioGroup6: TDBRadioGroup
        Left = 916
        Top = 44
        Width = 81
        Height = 61
        Caption = ' Customiz'#225'vel: '
        DataField = 'Customizav'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 11
        Values.Strings = (
          '0'
          '1')
      end
      object DBCheckBox1: TDBCheckBox
        Left = 648
        Top = 20
        Width = 249
        Height = 17
        Caption = 'Impede cadastro direto de produtos neste grupo.'
        DataField = 'ImpedeCad'
        DataSource = DsPrdGrupTip
        TabOrder = 3
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRadioGroup7: TDBRadioGroup
        Left = 4
        Top = 168
        Width = 993
        Height = 49
        Caption = ' Lista de pre'#231'os para estoque (Fiscal): '
        Columns = 9
        DataField = 'LstPrcFisc'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          '[Nenhuma]'
          'Venda'
          'Atacado'
          'Varejo'
          #218'ltima compra'
          'Pre'#231'o M'#233'dio'
          'Cont'#225'bil'
          'Reposi'#231#227'o'
          'Transfer'#234'ncia')
        TabOrder = 13
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit7: TDBEdit
        Left = 4
        Top = 236
        Width = 92
        Height = 21
        DataField = 'PerComissF'
        DataSource = DsPrdGrupTip
        TabOrder = 14
      end
      object DBEdit8: TDBEdit
        Left = 100
        Top = 236
        Width = 92
        Height = 21
        DataField = 'PerComissR'
        DataSource = DsPrdGrupTip
        TabOrder = 15
      end
      object DBEdit9: TDBEdit
        Left = 196
        Top = 236
        Width = 21
        Height = 21
        DataField = 'Tipo_Item'
        DataSource = DsPrdGrupTip
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 16
        OnChange = EdTipo_ItemChange
        OnKeyDown = EdTipo_ItemKeyDown
      end
      object DBEdit10: TDBEdit
        Left = 220
        Top = 236
        Width = 281
        Height = 21
        DataField = 'Tipo_Item_TXT'
        DataSource = DsPrdGrupTip
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 17
        OnKeyDown = EdTipo_Item_TXTKeyDown
      end
      object DBEdit11: TDBEdit
        Left = 504
        Top = 236
        Width = 116
        Height = 21
        DataField = 'Genero'
        DataSource = DsPrdGrupTip
        TabOrder = 18
      end
      object EdDBGraTabApp: TDBEdit
        Left = 623
        Top = 236
        Width = 218
        Height = 21
        DataField = 'GraTabApp_TXT'
        DataSource = DsPrdGrupTip
        TabOrder = 19
      end
      object DBCheckBox2: TDBCheckBox
        Left = 847
        Top = 238
        Width = 50
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsPrdGrupTip
        TabOrder = 20
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRadioGroup8: TDBRadioGroup
        Left = 602
        Top = 44
        Width = 67
        Height = 61
        Caption = ' Usa cores: '
        DataField = 'UsaCores'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 9
        Values.Strings = (
          '0'
          '1')
      end
      object DBEdit15: TDBEdit
        Left = 4
        Top = 276
        Width = 56
        Height = 21
        DataField = 'CtbCadGru'
        DataSource = DsPrdGrupTip
        TabOrder = 21
      end
      object DBEdit16: TDBEdit
        Left = 500
        Top = 276
        Width = 56
        Height = 21
        DataField = 'CtbPlaCta'
        DataSource = DsPrdGrupTip
        TabOrder = 22
      end
      object DBEdit17: TDBEdit
        Left = 560
        Top = 276
        Width = 432
        Height = 21
        DataField = 'NO_CtbPlaCta'
        DataSource = DsPrdGrupTip
        TabOrder = 23
      end
      object DBEdit18: TDBEdit
        Left = 60
        Top = 276
        Width = 432
        Height = 21
        DataField = 'NO_CtbCadGru'
        DataSource = DsPrdGrupTip
        TabOrder = 24
      end
      object DBRadioGroup9: TDBRadioGroup
        Left = 668
        Top = 44
        Width = 88
        Height = 61
        Caption = 'Movim.Estoque:'
        DataField = 'IND_MOV'
        DataSource = DsPrdGrupTip
        Items.Strings = (
          'Sim'
          'N'#227'o')
        TabOrder = 25
        Values.Strings = (
          '0'
          '1'
          '2')
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 596
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 533
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 876
        Top = 15
        Width = 130
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 405
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 405
        Align = alClient
        Caption = ' Edi'#231#227'o: '
        TabOrder = 0
        object Panel18: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 388
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 4
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
            FocusControl = DBEdCodigo
          end
          object Label8: TLabel
            Left = 64
            Top = 4
            Width = 57
            Height = 13
            Caption = 'C'#243'digo: [F4]'
            FocusControl = DBEdit1
          end
          object Label9: TLabel
            Left = 148
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdNome
          end
          object Label6: TLabel
            Left = 8
            Top = 260
            Width = 403
            Height = 13
            Caption = 
              #185': Deixe zerado se n'#227'o deseja separar os grupos de produtos por ' +
              'faixa de numera'#231#227'o.'
          end
          object Label18: TLabel
            Left = 8
            Top = 276
            Width = 248
            Height = 13
            Caption = #178': Fracionamento: em casas decimais ap'#243's a v'#237'rgula.'
          end
          object Label22: TLabel
            Left = 4
            Top = 220
            Width = 85
            Height = 13
            Caption = '% comiss'#227'o fatur.:'
          end
          object Label23: TLabel
            Left = 100
            Top = 220
            Width = 91
            Height = 13
            Caption = '% comiss'#227'o receb.:'
          end
          object Label208: TLabel
            Left = 196
            Top = 220
            Width = 300
            Height = 13
            Caption = 
              'Tipo de item - Atividades Industriais, Comerciais e Servi'#231'os [F3' +
              ']:'
            FocusControl = EdTipo_Item
          end
          object Label32: TLabel
            Left = 504
            Top = 220
            Width = 111
            Height = 13
            Caption = 'Conta CTB (Invent'#225'rio):'
          end
          object LaGraTabApp: TLabel
            Left = 4
            Top = 298
            Width = 172
            Height = 13
            Caption = 'Sele'#231#227'o de Agrega'#231#227'o de Produtos:'
          end
          object Label12: TLabel
            Left = 4
            Top = 340
            Width = 72
            Height = 13
            Caption = 'Grupo cont'#225'bil:'
          end
          object SbCtbCadGru: TSpeedButton
            Left = 472
            Top = 356
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbCtbCadGruClick
          end
          object Label13: TLabel
            Left = 500
            Top = 340
            Width = 142
            Height = 13
            Caption = 'Reduzido do plano de contas:'
          end
          object SbCtbPlaCta: TSpeedButton
            Left = 968
            Top = 356
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbCtbPlaCtaClick
          end
          object RGTipPrd: TdmkRadioGroup
            Left = 5
            Top = 44
            Width = 225
            Height = 61
            Caption = 'Tipifica'#231#227'o: '
            Columns = 3
            Items.Strings = (
              'Nenhum'
              'Produto'
              'Mat. prima'
              'Uso/cons.'
              'Servi'#231'o'
              'Outros ins.')
            TabOrder = 4
            QryCampo = 'TipPrd'
            UpdCampo = 'TipPrd'
            UpdType = utYes
            OldValor = 0
          end
          object EdCodigo: TdmkEdit
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 148
            Top = 20
            Width = 497
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCodUsu: TdmkEdit
            Left = 64
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CodUsu'
            UpdCampo = 'CodUsu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdCodUsuKeyDown
          end
          object RGMadeBy: TdmkRadioGroup
            Left = 232
            Top = 44
            Width = 141
            Height = 61
            Caption = ' Fabrica'#231#227'o: '
            Columns = 2
            Items.Strings = (
              'Nenhum'
              'Pr'#243'prio'
              'Terceiros')
            TabOrder = 5
            QryCampo = 'MadeBy'
            UpdCampo = 'MadeBy'
            UpdType = utYes
            OldValor = 0
          end
          object RGFracio: TdmkRadioGroup
            Left = 374
            Top = 44
            Width = 93
            Height = 61
            Caption = ' Fracionado'#178': '
            Columns = 3
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4')
            TabOrder = 6
            QryCampo = 'Fracio'
            UpdCampo = 'Fracio'
            UpdType = utYes
            OldValor = 0
          end
          object RGNivCad: TdmkRadioGroup
            Left = 468
            Top = 44
            Width = 93
            Height = 61
            Caption = 'N'#237'veis:'
            Columns = 3
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5')
            TabOrder = 7
            OnClick = RGNivCadClick
            QryCampo = 'NivCad'
            UpdCampo = 'NivCad'
            UpdType = utYes
            OldValor = 0
          end
          object GroupBox3: TGroupBox
            Left = 792
            Top = 44
            Width = 125
            Height = 61
            Caption = ' Faixa de numera'#231#227'o: '
            TabOrder = 11
            object Label4: TLabel
              Left = 4
              Top = 16
              Width = 33
              Height = 13
              Caption = 'Inicial'#185':'
            end
            object Label5: TLabel
              Left = 64
              Top = 16
              Width = 28
              Height = 13
              Caption = 'Final'#185':'
            end
            object dmkEdit1: TdmkEdit
              Left = 4
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FaixaIni'
              UpdCampo = 'FaixaIni'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object dmkEdit2: TdmkEdit
              Left = 64
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FaixaFim'
              UpdCampo = 'FaixaFim'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object RGGradeado: TdmkRadioGroup
            Left = 562
            Top = 44
            Width = 69
            Height = 61
            Caption = ' Gradeado: '
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 8
            QryCampo = 'Gradeado'
            UpdCampo = 'Gradeado'
            UpdType = utYes
            OldValor = 0
          end
          object GroupBox5: TGroupBox
            Left = 4
            Top = 104
            Width = 993
            Height = 61
            Caption = ' T'#237'tulos dos N'#237'veis: '
            TabOrder = 13
            object LaTitNiv5: TLabel
              Left = 12
              Top = 16
              Width = 38
              Height = 13
              Caption = 'N'#237'vel 5:'
              FocusControl = DBEdNome
            end
            object LaTitNiv4: TLabel
              Left = 206
              Top = 16
              Width = 38
              Height = 13
              Caption = 'N'#237'vel 4:'
              FocusControl = DBEdNome
            end
            object LaTitNiv3: TLabel
              Left = 400
              Top = 16
              Width = 38
              Height = 13
              Caption = 'N'#237'vel 3:'
              FocusControl = DBEdNome
            end
            object LaTitNiv2: TLabel
              Left = 594
              Top = 16
              Width = 38
              Height = 13
              Caption = 'N'#237'vel 2:'
              FocusControl = DBEdNome
            end
            object LaTitNiv1: TLabel
              Left = 788
              Top = 16
              Width = 38
              Height = 13
              Caption = 'N'#237'vel 1:'
              FocusControl = DBEdNome
            end
            object EdTitNiv3: TdmkEdit
              Left = 400
              Top = 32
              Width = 190
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'TitNiv3'
              UpdCampo = 'TitNiv3'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTitNiv4: TdmkEdit
              Left = 206
              Top = 32
              Width = 190
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'TitNiv4'
              UpdCampo = 'TitNiv4'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTitNiv5: TdmkEdit
              Left = 12
              Top = 32
              Width = 190
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'TitNiv5'
              UpdCampo = 'TitNiv5'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTitNiv2: TdmkEdit
              Left = 594
              Top = 32
              Width = 190
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'TitNiv2'
              UpdCampo = 'TitNiv2'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTitNiv1: TdmkEdit
              Left = 788
              Top = 32
              Width = 190
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'TitNiv1'
              UpdCampo = 'TitNiv1'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object RGCustomizav: TdmkRadioGroup
            Left = 916
            Top = 44
            Width = 81
            Height = 61
            Caption = ' Customiz'#225'vel: '
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 12
            QryCampo = 'Customizav'
            UpdCampo = 'Customizav'
            UpdType = utYes
            OldValor = 0
          end
          object CkImpedeCad: TdmkCheckBox
            Left = 648
            Top = 20
            Width = 249
            Height = 17
            Caption = 'Impede cadastro direto de produtos neste grupo.'
            TabOrder = 3
            QryCampo = 'ImpedeCad'
            UpdCampo = 'ImpedeCad'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object RGLstPrcFisc: TdmkRadioGroup
            Left = 4
            Top = 168
            Width = 993
            Height = 49
            Caption = ' Lista de pre'#231'os para estoque (Fiscal): '
            Columns = 9
            ItemIndex = 0
            Items.Strings = (
              '[Nenhuma]'
              'Venda'
              'Atacado'
              'Varejo'
              #218'ltima compra'
              'Pre'#231'o M'#233'dio'
              'Cont'#225'bil'
              'Reposi'#231#227'o'
              'Transfer'#234'ncia')
            TabOrder = 14
            QryCampo = 'LstPrcFisc'
            UpdCampo = 'LstPrcFisc'
            UpdType = utYes
            OldValor = 0
          end
          object EdPerComissF: TdmkEdit
            Left = 4
            Top = 236
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 15
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'PerComissF'
            UpdCampo = 'PerComissF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPerComissR: TdmkEdit
            Left = 100
            Top = 236
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 16
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'PerComissR'
            UpdCampo = 'PerComissR'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTipo_Item: TdmkEdit
            Left = 196
            Top = 236
            Width = 21
            Height = 21
            Alignment = taRightJustify
            TabOrder = 17
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Tipo_Item'
            UpdCampo = 'Tipo_Item'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdTipo_ItemChange
            OnKeyDown = EdTipo_ItemKeyDown
          end
          object EdTipo_Item_TXT: TdmkEdit
            Left = 220
            Top = 236
            Width = 281
            Height = 21
            ReadOnly = True
            TabOrder = 18
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Mercadoria para Revenda'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Mercadoria para Revenda'
            ValWarn = False
            OnKeyDown = EdTipo_Item_TXTKeyDown
          end
          object EdGenero: TdmkEdit
            Left = 504
            Top = 236
            Width = 117
            Height = 21
            Alignment = taRightJustify
            TabOrder = 19
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Genero'
            UpdCampo = 'Genero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdGraTabApp: TdmkEditCB
            Left = 4
            Top = 316
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 20
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GraTabApp'
            UpdCampo = 'GraTabApp'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraTabApp
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraTabApp: TdmkDBLookupComboBox
            Left = 62
            Top = 316
            Width = 439
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGraTabApp
            TabOrder = 21
            dmkEditCB = EdGraTabApp
            QryCampo = 'GraTabApp'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object RGUsaCores: TdmkRadioGroup
            Left = 632
            Top = 44
            Width = 69
            Height = 61
            Caption = ' Usa cores: '
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 9
            QryCampo = 'UsaCores'
            UpdCampo = 'UsaCores'
            UpdType = utYes
            OldValor = 0
          end
          object EdCtbCadGru: TdmkEditCB
            Left = 4
            Top = 356
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 22
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CtbCadGru'
            UpdCampo = 'CtbCadGru'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtbCadGru
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCtbCadGru: TdmkDBLookupComboBox
            Left = 60
            Top = 356
            Width = 409
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCtbCadGru
            TabOrder = 23
            dmkEditCB = EdCtbCadGru
            QryCampo = 'CtbCadGru'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCtbPlaCta: TdmkEditCB
            Left = 500
            Top = 356
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 24
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CtbPlaCta'
            UpdCampo = 'CtbPlaCta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCtbPlaCta
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCtbPlaCta: TdmkDBLookupComboBox
            Left = 556
            Top = 356
            Width = 409
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPlaAllCad
            TabOrder = 25
            dmkEditCB = EdCtbPlaCta
            QryCampo = 'CtbPlaCta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object RGIND_MOV: TdmkRadioGroup
            Left = 702
            Top = 44
            Width = 89
            Height = 61
            Caption = 'Movim.Estoque:'
            Items.Strings = (
              'Sim'
              'N'#227'o')
            TabOrder = 10
            QryCampo = 'IND_MOV'
            UpdCampo = 'IND_MOV'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 40
    Top = 12
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPrdGrupTipBeforeOpen
    AfterOpen = QrPrdGrupTipAfterOpen
    BeforeClose = QrPrdGrupTipBeforeClose
    AfterScroll = QrPrdGrupTipAfterScroll
    OnCalcFields = QrPrdGrupTipCalcFields
    SQL.Strings = (
      'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, '
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'TitNiv1, TitNiv2, TitNiv3, Customizav, '
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO, pgt.Gradeado,'
      
        'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') NOM' +
        'E_TIPPRD,'
      'pgt.ImpedeCad, pgt.LstPrcFisc, pgt.PerComissF, pgt.PerComissR,'
      'pgt.Tipo_Item, Genero'
      'FROM prdgruptip pgt'
      'WHERE pgt.Codigo > 0')
    Left = 12
    Top = 12
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrPrdGrupTipMadeBy: TSmallintField
      FieldName = 'MadeBy'
      Required = True
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
      Required = True
    end
    object QrPrdGrupTipTipPrd: TSmallintField
      FieldName = 'TipPrd'
      Required = True
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
      Required = True
    end
    object QrPrdGrupTipFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
      Required = True
    end
    object QrPrdGrupTipFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
      Required = True
    end
    object QrPrdGrupTipNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrPrdGrupTipNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrPrdGrupTipNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
    object QrPrdGrupTipGradeado: TSmallintField
      FieldName = 'Gradeado'
      Required = True
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipCustomizav: TSmallintField
      FieldName = 'Customizav'
    end
    object QrPrdGrupTipImpedeCad: TSmallintField
      FieldName = 'ImpedeCad'
    end
    object QrPrdGrupTipLstPrcFisc: TSmallintField
      FieldName = 'LstPrcFisc'
    end
    object QrPrdGrupTipPerComissF: TFloatField
      FieldName = 'PerComissF'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrPrdGrupTipPerComissR: TFloatField
      FieldName = 'PerComissR'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrPrdGrupTipTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrPrdGrupTipTipo_Item_TXT: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'Tipo_Item_TXT'
      Size = 50
      Calculated = True
    end
    object QrPrdGrupTipGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPrdGrupTipTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Size = 15
    end
    object QrPrdGrupTipTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Size = 15
    end
    object QrPrdGrupTipGraTabApp: TIntegerField
      FieldName = 'GraTabApp'
    end
    object QrPrdGrupTipGraTabApp_TXT: TWideStringField
      FieldName = 'GraTabApp_TXT'
      Size = 100
    end
    object QrPrdGrupTipAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPrdGrupTipUsaCores: TSmallintField
      FieldName = 'UsaCores'
    end
    object QrPrdGrupTipNO_CtbPlaCta: TWideStringField
      FieldName = 'NO_CtbPlaCta'
      Size = 60
    end
    object QrPrdGrupTipNO_CtbCadGru: TWideStringField
      FieldName = 'NO_CtbCadGru'
      Size = 60
    end
    object QrPrdGrupTipCtbCadGru: TIntegerField
      FieldName = 'CtbCadGru'
    end
    object QrPrdGrupTipCtbPlaCta: TIntegerField
      FieldName = 'CtbPlaCta'
    end
    object QrPrdGrupTipIND_MOV: TSmallintField
      FieldName = 'IND_MOV'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtCTB
    CanDel01 = BtComissao
    Left = 68
    Top = 12
  end
  object QrPrdGrupAtr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pga.Controle, pga.GraAtrCad, gac.CodUsu, gac.Nome '
      'FROM prdgrupatr pga'
      'LEFT JOIN graatrcad gac ON gac.Codigo=pga.GraAtrCad'
      'WHERE pga.Codigo=:P0')
    Left = 704
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrdGrupAtrControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrdGrupAtrGraAtrCad: TIntegerField
      FieldName = 'GraAtrCad'
      Required = True
    end
    object QrPrdGrupAtrCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPrdGrupAtrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPrdGrupAtr: TDataSource
    DataSet = QrPrdGrupAtr
    Left = 708
    Top = 68
  end
  object QrPrdGrupJan: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pgj.Ordem,pgj.Controle, '
      'pgj.Janela, pfj.Descricao'
      'FROM prdgrupjan pgj'
      'LEFT JOIN perfjan pfj ON pfj.Janela=pgj.Janela'
      'WHERE pgj.Codigo=:P0'
      'ORDER BY Ordem')
    Left = 616
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrdGrupJanOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrPrdGrupJanControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrdGrupJanJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 30
    end
    object QrPrdGrupJanDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object DsPrdGrupJan: TDataSource
    DataSet = QrPrdGrupJan
    Left = 616
    Top = 64
  end
  object PMTipo: TPopupMenu
    OnPopup = PMTipoPopup
    Left = 280
    Top = 604
    object Incluinovotipodegrupodeprodutos1: TMenuItem
      Caption = '&Inclui novo tipo de grupo de produtos'
      OnClick = Incluinovotipodegrupodeprodutos1Click
    end
    object Alteratipodegrupodeprodutosatual1: TMenuItem
      Caption = '&Altera tipo de grupo de produtos atual'
      OnClick = Alteratipodegrupodeprodutosatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Desativarativartipodegrupodeprodutoatual1: TMenuItem
      Caption = '&Desativar / ativar tipo de grupo de produto atual'
      OnClick = Desativarativartipodegrupodeprodutoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluitipodegrupodeprodutosatual1: TMenuItem
      Caption = '&Exclui tipo de grupo de produtos atual'
      Enabled = False
      OnClick = Excluitipodegrupodeprodutosatual1Click
    end
  end
  object QrGraAtrCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM graatrcad'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 216
    Top = 8
    object QrGraAtrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraAtrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraAtrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrPrdGrupCen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT elc.CodUsu, pgc.EstqCen, '
      'pgc.PrdGrupTip, elc.Nome NOMEESTQCEN'
      'FROM prdgrupcen pgc'
      'LEFT JOIN estqcen elc ON elc.Codigo=pgc.EstqCen'
      'WHERE pgc.PrdGrupTip=:P0')
    Left = 808
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrdGrupCenCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPrdGrupCenEstqCen: TIntegerField
      FieldName = 'EstqCen'
      Required = True
    end
    object QrPrdGrupCenNOMEESTQCEN: TWideStringField
      FieldName = 'NOMEESTQCEN'
      Size = 30
    end
    object QrPrdGrupCenPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object DsPrdGrupCen: TDataSource
    DataSet = QrPrdGrupCen
    Left = 812
    Top = 64
  end
  object QrEstqCen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ec.Codigo, ec.CodUsu, ec.Nome, '
      'pg.PrdgrupTip, pg.Ativo'
      'FROM estqcen ec'
      
        'LEFT JOIN prdgrupcen pg ON pg.estqcen=ec.Codigo AND pg.prdgrupti' +
        'p=:P0'
      'ORDER BY ec.Nome')
    Left = 280
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEstqCenCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstqCenCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEstqCenNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrEstqCenAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEstqCenPrdgrupTip: TIntegerField
      FieldName = 'PrdgrupTip'
      Required = True
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Codusu, '
      'gg1.Nome, gg1.PrdGrupTip'
      'FROM gragru1 gg1'
      'WHERE gg1.PrdGrupTip=:P0')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 124
    Top = 12
  end
  object QrGraGru9: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Codusu, gg1.Nome'
      'FROM gragru1 gg1'
      'WHERE gg1.PrdGrupTip<>:P0')
    Left = 420
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru9Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru9Codusu: TIntegerField
      FieldName = 'Codusu'
    end
    object QrGraGru9Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru9: TDataSource
    DataSet = QrGraGru9
    Left = 420
    Top = 96
  end
  object QrTipo9: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM prdgruptip'
      'WHERE Codigo <> :P0'
      'ORDER BY Nome')
    Left = 200
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTipo9Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTipo9CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTipo9Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsTipo9: TDataSource
    DataSet = QrTipo9
    Left = 200
    Top = 100
  end
  object PMJanelas: TPopupMenu
    OnPopup = PMJanelasPopup
    Left = 372
    Top = 608
    object Incluijanelas1: TMenuItem
      Caption = '&Inclui janelas'
      OnClick = Incluijanelas1Click
    end
    object Excluijanelas1: TMenuItem
      Caption = '&Exclui janelas'
      OnClick = Excluijanelas1Click
    end
  end
  object PMAtributos: TPopupMenu
    OnPopup = PMAtributosPopup
    Left = 460
    Top = 608
    object IncluiAtributos1: TMenuItem
      Caption = '&Inclui Atributos'
      OnClick = IncluiAtributos1Click
    end
    object Excluiatributos1: TMenuItem
      Caption = '&Exclui Atributos'
      OnClick = Excluiatributos1Click
    end
  end
  object PMCentros: TPopupMenu
    OnPopup = PMCentrosPopup
    Left = 548
    Top = 608
    object Incluicentro1: TMenuItem
      Caption = '&Inclui centro'
      OnClick = Incluicentro1Click
    end
    object Excluicentro1: TMenuItem
      Caption = '&Exclui centro'
      OnClick = Excluicentro1Click
    end
  end
  object QrGraComiss3: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraComiss3BeforeClose
    AfterScroll = QrGraComiss3AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg1.Nivel3, gg3.CodUsu, '
      'gg3.Nome, gco.PerComissF, gco.PerComissR'
      'FROM GraGru1 gg1'
      'LEFT JOIN gragru3 gg3 ON gg1.Nivel3=gg3.Nivel3'
      
        'LEFT JOIN gracomiss gco ON gco.Nivel3=gg3.Nivel3 AND gco.NivelX=' +
        '3 '
      '  AND gco.PrdGrupTip=:P0'
      'WHERE gg1.prdGrupTip=:P1')
    Left = 264
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraComiss3Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraComiss3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraComiss3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraComiss3PerComissF: TFloatField
      FieldName = 'PerComissF'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraComiss3PerComissR: TFloatField
      FieldName = 'PerComissR'
      DisplayFormat = '0.000000;-0.000000; '
    end
  end
  object DsGraComiss3: TDataSource
    DataSet = QrGraComiss3
    Left = 268
    Top = 100
  end
  object QrGraComiss2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT DISTINCT gg1.PrdGrupTip, gg1.Nivel2, gg1.Nivel3, gg2.CodU' +
        'su, '
      'gg2.Nome, gco.PerComissF, gco.PerComissR'
      'FROM GraGru1 gg1'
      'LEFT JOIN gragru2 gg2 ON gg1.Nivel2=gg2.Nivel2'
      'LEFT JOIN gracomiss gco ON gco.Nivel2=gg2.Nivel2 '
      '  AND gco.NivelX=2 AND gco.PrdGrupTip=:P0 AND gco.Nivel3=:P1'
      'WHERE gg1.prdGrupTip=:P2'
      'AND gg1.Nivel3=:P3')
    Left = 492
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrGraComiss2Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraComiss2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraComiss2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraComiss2PerComissF: TFloatField
      FieldName = 'PerComissF'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraComiss2PerComissR: TFloatField
      FieldName = 'PerComissR'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraComiss2Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
  end
  object DsGraComiss2: TDataSource
    DataSet = QrGraComiss2
    Left = 492
    Top = 96
  end
  object PMComissao: TPopupMenu
    OnPopup = PMComissaoPopup
    Left = 652
    Top = 604
    object ComissoNivel31: TMenuItem
      Caption = '% Comiss'#227'o Nivel &3'
      Enabled = False
      OnClick = ComissoNivel31Click
    end
    object ComissoNivel21: TMenuItem
      Caption = '% Comiss'#227'o Nivel &2'
      Enabled = False
      OnClick = ComissoNivel21Click
    end
  end
  object QrGraPlaCta3: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraPlaCta3BeforeClose
    AfterScroll = QrGraPlaCta3AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg1.Nivel3, gg3.CodUsu, '
      'gg3.Nome, gpc.Genero'
      'FROM GraGru1 gg1'
      'LEFT JOIN gragru3 gg3 ON gg1.Nivel3=gg3.Nivel3'
      
        'LEFT JOIN graplacta gpc ON gpc.Nivel3=gg3.Nivel3 AND gpc.NivelX=' +
        '3 '
      '  AND gpc.PrdGrupTip=:P0'
      'WHERE gg1.prdGrupTip=:P1')
    Left = 340
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraPlaCta3Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraPlaCta3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraPlaCta3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraPlaCta3Genero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrGraPlaCta2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT DISTINCT gg1.PrdGrupTip, gg1.Nivel2, gg1.Nivel3, gg2.CodU' +
        'su, '
      'gg2.Nome, gpc.Genero'
      'FROM GraGru1 gg1'
      'LEFT JOIN gragru2 gg2 ON gg1.Nivel2=gg2.Nivel2'
      'LEFT JOIN graplacta gpc ON gpc.Nivel2=gg2.Nivel2 '
      '  AND gpc.NivelX=2 AND gpc.PrdGrupTip=:P0 AND gpc.Nivel3=:P1'
      'WHERE gg1.prdGrupTip=:P2'
      'AND gg1.Nivel3=:P3')
    Left = 560
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrGraPlaCta2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraPlaCta2Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraPlaCta2Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraPlaCta2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraPlaCta2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraPlaCta2Genero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsGraPlaCta2: TDataSource
    DataSet = QrGraPlaCta2
    Left = 560
    Top = 100
  end
  object DsGraPlaCta3: TDataSource
    DataSet = QrGraPlaCta3
    Left = 344
    Top = 100
  end
  object PMCTB: TPopupMenu
    OnPopup = PMCTBPopup
    Left = 748
    Top = 604
    object CTBNivel31: TMenuItem
      Caption = 'CTB N'#237'vel 3'
      Enabled = False
      OnClick = CTBNivel31Click
    end
    object CTBNivel21: TMenuItem
      Caption = 'CTB Nivel 2'
      Enabled = False
      OnClick = CTBNivel21Click
    end
  end
  object QrTemNiv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Nivel1) ITENS '
      'FROM gragru1'
      'WHERE PrdGrupTip=1'
      'AND Nivel5 <> 0')
    Left = 376
    Top = 340
    object QrTemNivITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrGraTabApp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gratabapp'
      'ORDER BY Nome')
    Left = 64
    Top = 444
    object QrGraTabAppCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraTabAppNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGraTabAppTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
  end
  object DsGraTabApp: TDataSource
    DataSet = QrGraTabApp
    Enabled = False
    Left = 65
    Top = 492
  end
  object QrCtbCadGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ctbcadgru'
      'ORDER BY Nome')
    Left = 904
    Top = 50
    object QrCtbCadGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCtbCadGru: TDataSource
    DataSet = QrCtbCadGru
    Left = 904
    Top = 102
  end
  object QrPlaAllCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM plaallcad'
      'WHERE AnaliSinte=2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 148
    Top = 80
    object QrPlaAllCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCadNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCadAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCadCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCadDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCadOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object DsPlaAllCad: TDataSource
    DataSet = QrPlaAllCad
    Left = 148
    Top = 132
  end
end
