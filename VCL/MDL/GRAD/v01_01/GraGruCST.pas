unit GraGruCST;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids,
  DBGrids, dmkDBGrid, DB, mySQLDbTables, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TFmGraGruCST = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label5: TLabel;
    dmkEdit5: TdmkEdit;
    Label24: TLabel;
    EdUF_Orig: TdmkEdit;
    Label1: TLabel;
    EdUF_Dest: TdmkEdit;
    DsUFsAtoB: TDataSource;
    QrUFsAtoB: TmySQLQuery;
    QrUFsAtoBNome: TWideStringField;
    QrUFsAtoBAtivo: TSmallintField;
    dmkDBGrid1: TdmkDBGrid;
    Label11: TLabel;
    EdCST_B: TdmkEdit;
    EdTextoB: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure EdCST_BChange(Sender: TObject);
    procedure EdCST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FUFsAtoB: String;
    procedure AtivaItens(Status: Integer);
  public
    { Public declarations }
  end;

  var
  FmGraGruCST: TFmGraGruCST;

implementation


{$R *.DFM}

uses
  {$IfNDef NO_FINANCEIRO} (*UnPagtos,*) UnFinanceiro, {$EndIf}
  UnMyObjects, GraGruN, ModuleGeral, UCreate, UnInternalConsts, UMySQLModule,
  Module, dmkGeral, DmkDAC_PF;

procedure TFmGraGruCST.AtivaItens(Status: Integer);
var
  UF: String;
begin
  Screen.Cursor := crHourGlass;
  UF := QrUFsAtoBNome.Value;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FUFsAtoB);
  DmodG.QrUpdPID1.SQL.Add('SET Ativo=' + IntToStr(Status));
  DmodG.QrUpdPID1.ExecSQL;
  //
  QrUFsAtoB.Close;
  UnDmkDAC_PF.AbreQuery(QrUFsAtoB, DModG.MyPID_DB);
  QrUFsAtoB.Locate('Nome', UF, [loCaseInsensitive]);
  Screen.Cursor := crDefault;
end;

procedure TFmGraGruCST.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmGraGruCST.BtOKClick(Sender: TObject);
  procedure InsUpdAlquotas(const UF_Orig, UF_Dest: String; const
  CST_B: Integer; var Conta: Integer);
  begin
    Dmod.QrUpd.Params[00].AsInteger := FmGraGruN.QrGraGru1Nivel1.Value;
    Dmod.QrUpd.Params[01].AsString  := UF_Orig;
    Dmod.QrUpd.Params[02].AsString  := UF_Dest;
    Dmod.QrUpd.Params[03].AsInteger := CST_B;
    Dmod.QrUpd.Params[04].AsInteger := CST_B;
    UMyMod.ExecutaQuery(Dmod.QrUpd);
    //
    Conta := Conta + 1;
  end;
var
  Msg, UF_Orig, UF_Dest: String;
  CST_B, Conta: Integer;
begin
  UF_Orig := Trim(EdUF_Orig.Text);
  if UF_Orig = '' then
  begin
    Geral.MB_Aviso('Informe a UF emitente!');
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    Conta := 0;
    CST_B := EdCST_B.ValueVariant;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO gragrucst (');
    Dmod.QrUpd.SQL.Add('Nivel1,UF_Orig,UF_Dest,CST_B)');
    Dmod.QrUpd.SQL.Add('VALUES (:P0,:P1,:P2,:P3)');
    Dmod.QrUpd.SQL.Add('ON DUPLICATE KEY UPDATE CST_B=:P4');
    UF_Dest := Trim(EdUF_Dest.Text);
    if Length(UF_Dest) = 2 then
      InsUpdAlquotas(UF_Orig, UF_Dest, CST_B, Conta);
    QrUFsAtoB.First;
    while not QrUFsAtoB.Eof do
    begin
      if QrUFsAtoBAtivo.Value = 1 then
        InsUpdAlquotas(UF_Orig, QrUFsAtoBNome.Value, CST_B, Conta);
      QrUFsAtoB.Next;
    end;
    case Conta of
      0: Msg := 'N�o foi inclu�do/alterado nenhum registro!';
      1: Msg := 'Foi inclu�do/alterado um registro!';
      else Msg := 'Foram inclu�dos/alterados ' + IntToStr(Conta) + ' registros!';
    end;
    Geral.MB_Aviso(Msg);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGraGruCST.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruCST.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

procedure TFmGraGruCST.dmkDBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
  UF: String;
begin
  if Column.FieldName = 'Ativo' then
  begin
    UF := QrUFsAtoBNome.Value;
    if QrUFsAtoBAtivo.Value = 1 then
      Status := 0
    else
      Status := 1;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FUFsAtoB + ' SET Ativo=:P0');
    DmodG.QrUpdPID1.SQL.Add('WHERE Nome=:P1');
    DmodG.QrUpdPID1.Params[00].AsInteger := Status;
    DmodG.QrUpdPID1.Params[01].AsString  := UF;
    UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
    QrUFsAtoB.Close;
    UnDmkDAC_PF.AbreQuery(QrUFsAtoB, DModG.MyPID_DB);
    QrUFsAtoB.Locate('Nome', UF, []);
  end;
end;

procedure TFmGraGruCST.EdCST_BChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
{$EndIf}
end;

procedure TFmGraGruCST.EdCST_BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
{$EndIf}
end;

procedure TFmGraGruCST.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruCST.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrUFsAtoB.Database := DModG.MyPID_DB;
  FUFsAtoB := UCriar.RecriaTempTable('UFsAtoB', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FUFsAtoB);
  DmodG.QrUpdPID1.SQL.Add('SELECT Nome, 0 Ativo FROM ' + TMeuDB + '.UFs');
  DmodG.QrUpdPID1.SQL.Add('WHERE Codigo <> 0');
  DmodG.QrUpdPID1.ExecSQL;
  QrUFsAtoB.Close;
  QrUFsAtoB.SQL.Clear;
  QrUFsAtoB.SQL.Add('SELECT * FROM ' + FUFsAtoB);
  UnDmkDAC_PF.AbreQuery(QrUFsAtoB, DModG.MyPID_DB);
end;

procedure TFmGraGruCST.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
