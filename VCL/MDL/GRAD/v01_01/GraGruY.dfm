object FmGraGruY: TFmGraGruY
  Left = 368
  Top = 194
  Caption = 'PRD-GRUPO-035 :: Grupos de Estoque'
  ClientHeight = 561
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 465
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 94
        Height = 13
        Caption = 'C'#243'digo e descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 123
        Height = 13
        Caption = 'Tipo de grupo de produto:'
      end
      object SBNewPallet: TSpeedButton
        Left = 668
        Top = 72
        Width = 21
        Height = 21
        Caption = '+'
        OnClick = SBNewPalletClick
      end
      object SBPallet: TSpeedButton
        Left = 644
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBPalletClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPrdGrupTip: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PrdGrupTip'
        UpdCampo = 'PrdGrupTip'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPrdGrupTip
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPrdGrupTip: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 569
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPrdGrupTip
        TabOrder = 2
        dmkEditCB = EdPrdGrupTip
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object DBEdit1: TDBEdit
        Left = 72
        Top = 32
        Width = 569
        Height = 21
        DataField = 'Nome'
        DataSource = DsGraGruY
        TabOrder = 3
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 402
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 646
        Top = 15
        Width = 136
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 465
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 32
    ExplicitTop = 120
    object Splitter1: TSplitter
      Left = 0
      Top = 209
      Width = 784
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 138
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 227
        Height = 13
        Caption = 'Tipo de grupo de produto principal (Deprecado):'
        Enabled = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGraGruY
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsGraGruY
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'PrdGrupTip'
        DataSource = DsGraGruY
        Enabled = False
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 76
        Top = 72
        Width = 394
        Height = 21
        DataField = 'NO_PGT'
        DataSource = DsGraGruY
        Enabled = False
        TabOrder = 3
      end
      object CkFiltrar: TCheckBox
        Left = 476
        Top = 54
        Width = 97
        Height = 17
        Caption = 'Filtrar nome:'
        TabOrder = 4
        OnClick = CkFiltrarClick
      end
      object EdFiltro: TdmkEdit
        Left = 476
        Top = 72
        Width = 289
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '%%'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '%%'
        ValWarn = False
        OnChange = EdFiltroChange
        OnRedefinido = EdFiltroRedefinido
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 401
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 88
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 262
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 8
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 323
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo de Estoque'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 350
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtPrdGrupTip: TBitBtn
          Tag = 364
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tipo de Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtPrdGrupTipClick
        end
      end
    end
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 253
      Width = 784
      Height = 148
      Align = alBottom
      DataSource = DsGraGruX
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Nome do artigo'
          Width = 391
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Niv1'
          Title.Caption = 'Parte'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Niv2'
          Title.Caption = 'Tipo'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatorInt'
          Title.Caption = 'Fator int.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PrdGrupTip'
          Title.Caption = 'Nome do tipo de grupo de produto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatrClase'
          Title.Caption = 'Fator (ordem) classe'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ArtGeComodty'
          Title.Caption = 'Artigo gerado commodity'
          Width = 100
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 97
      Width = 784
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object Label5: TLabel
        Left = 0
        Top = 0
        Width = 784
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 
          'Todos "Tipo de Grupo de Produtos" e quantidade re reduzidos atre' +
          'lados'
        ExplicitWidth = 342
      end
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 0
        Top = 13
        Width = 784
        Height = 99
        Align = alClient
        DataSource = DsGraGruYPGT
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Pertence'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PrdGrupTip'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Nome do Tipo de Grupo de Produto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ITENS'
            Title.Caption = 'Qtd. Reduzidos'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 235
        Height = 32
        Caption = 'Grupos de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 235
        Height = 32
        Caption = 'Grupos de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 235
        Height = 32
        Caption = 'Grupos de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrGraGruY: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraGruYBeforeOpen
    AfterOpen = QrGraGruYAfterOpen
    BeforeClose = QrGraGruYBeforeClose
    AfterScroll = QrGraGruYAfterScroll
    SQL.Strings = (
      'SELECT pgt.Nome NO_PGT, ggy.* '
      'FROM gragruy ggy'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=ggy.PrdGrupTip'
      'WHERE ggy.Codigo > 0'
      '')
    Left = 92
    Top = 233
    object QrGraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object QrGraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrGraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrGraGruYLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruYDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruYDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruYUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruYUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruYAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruYAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGruYNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 60
    end
    object QrGraGruYPrdGrupTip: TFloatField
      FieldName = 'PrdGrupTip'
    end
  end
  object DsGraGruY: TDataSource
    DataSet = QrGraGruY
    Left = 92
    Top = 277
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, ggx.*, nv1.Nome NO_Niv1,'
      'nv1.FatorInt, nv2.Nome NO_Niv2'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle'
      'LEFT JOIN couniv1 nv1 ON nv1.Codigo=cou.CouNiv1'
      'LEFT JOIN couniv2 nv2 ON nv2.Codigo=cou.CouNiv2')
    Left = 184
    Top = 233
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrGraGruXLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruXDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruXDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruXUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruXUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruXAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruXAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGruXNO_Niv1: TWideStringField
      FieldName = 'NO_Niv1'
      Size = 60
    end
    object QrGraGruXFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrGraGruXNO_Niv2: TWideStringField
      FieldName = 'NO_Niv2'
      Size = 60
    end
    object QrGraGruXNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXNO_PrdGrupTip: TWideStringField
      FieldName = 'NO_PrdGrupTip'
      Size = 60
    end
    object QrGraGruXFatrClase: TFloatField
      FieldName = 'FatrClase'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrGraGruXNO_ArtGeComodty: TWideStringField
      FieldName = 'NO_ArtGeComodty'
      Size = 60
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 184
    Top = 277
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 436
    Top = 352
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      object Artigonovo1: TMenuItem
        Caption = '&Artigo novo'
        OnClick = Artigonovo1Click
      end
      object Itemaartigoexistente1: TMenuItem
        Caption = 'Item a artigo existente'
        OnClick = Itemaartigoexistente1Click
      end
      object Adicionarcoreoutamanhoaartigoexistente1: TMenuItem
        Caption = 'Adicionar cor e/ou tamanho a artigo existente'
        OnClick = Adicionarcoreoutamanhoaartigoexistente1Click
      end
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      object odosdados1: TMenuItem
        Caption = 'Todos dados '
        OnClick = odosdados1Click
      end
      object Dadosgenricos1: TMenuItem
        Caption = 'Dados gen'#233'ricos'
        OnClick = Dadosgenricos1Click
      end
    end
    object Alterapartedonome1: TMenuItem
      Caption = 'Altera parte do nome (&m'#250'ltiplo)'
      OnClick = Alterapartedonome1Click
    end
    object Moveparaoutrogrupodeestoque1: TMenuItem
      Caption = '&Move para outro grupo de estoque'
      OnClick = Moveparaoutrogrupodeestoque1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 312
    Top = 352
    object CabInclui1: TMenuItem
      Caption = '&Inclui novo Grupo de Estoque'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera o Grupo de Estoque selecionado'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui o Grupo de Estoque selecionado'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ForcarGrandezaPadrao: TMenuItem
      Caption = 'For'#231'ar grandeza padr'#227'o'
      OnClick = ForcarGrandezaPadraoClick
    end
    object Configurareduzidosorfos2: TMenuItem
      Caption = '&Configura reduzidos orf'#227'os'
    end
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, '
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'pgt.TitNiv1, pgt.TitNiv2, pgt.TitNiv3, pgt.TitNiv4, pgt.TitNiv5,'
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO, pgt.Gradeado,'
      
        'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') NOM' +
        'E_TIPPRD'
      'FROM prdgruptip pgt'
      'ORDER BY pgt.Nome')
    Left = 272
    Top = 228
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipMadeBy: TSmallintField
      FieldName = 'MadeBy'
      Required = True
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
      Required = True
    end
    object QrPrdGrupTipTipPrd: TSmallintField
      FieldName = 'TipPrd'
      Required = True
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
      Required = True
    end
    object QrPrdGrupTipNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrPrdGrupTipNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrPrdGrupTipNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
    object QrPrdGrupTipFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
      Required = True
    end
    object QrPrdGrupTipFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
      Required = True
    end
    object QrPrdGrupTipGradeado: TSmallintField
      FieldName = 'Gradeado'
      Required = True
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrPrdGrupTipTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Required = True
      Size = 15
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 272
    Top = 272
  end
  object PMNovo: TPopupMenu
    Left = 292
    Top = 64
    object Corrigereduzidossemgrupodeestoque1: TMenuItem
      Caption = 'Corrige reduzidos sem grupo de estoque'
      OnClick = Corrigereduzidossemgrupodeestoque1Click
    end
    object GerenciaTiposdeGruposdeprodutoXGruposdeetoque1: TMenuItem
      Caption = 'Gerencia Tipos de Grupos de produto X Grupos de etoque'
    end
  end
  object QrNaoCfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'WHERE PrdGrupTip=0'
      'AND Codigo <> 0')
    Left = 560
    Top = 164
  end
  object QrGraGruYPGT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, pgt.Nome,'
      'COUNT(ggx.Controle) ITENS'
      'FROM GraGruX ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.GraGruY=3072'
      'GROUP BY gg1.PrdGrupTip')
    Left = 352
    Top = 225
    object QrGraGruYPGTPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruYPGTNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrGraGruYPGTITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrGraGruYPGTPertence: TWideStringField
      FieldName = 'Pertence'
      Size = 3
    end
  end
  object DsGraGruYPGT: TDataSource
    DataSet = QrGraGruYPGT
    Left = 352
    Top = 269
  end
  object PMPrdGrupTip: TPopupMenu
    OnPopup = PMPrdGrupTipPopup
    Left = 376
    Top = 356
    object AdicionaTipodeGrupodeProduto1: TMenuItem
      Caption = '&Adiciona um Tipo de Grupo de Produto'
      OnClick = AdicionaTipodeGrupodeProduto1Click
    end
    object RemoveiTipodeGrupodeProduto1: TMenuItem
      Caption = '&Remove o Tipo de Grupo de Produto selecionado'
      OnClick = RemoveiTipodeGrupodeProduto1Click
    end
    object Configurareduzidosrfos1: TMenuItem
      Caption = '&Configura reduzidos '#243'rf'#227'os'
      OnClick = Configurareduzidosrfos1Click
    end
  end
  object DqPrdGrupTip: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 420
  end
end
