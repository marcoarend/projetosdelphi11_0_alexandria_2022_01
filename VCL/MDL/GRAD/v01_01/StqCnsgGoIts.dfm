object FmStqCnsgGoIts: TFmStqCnsgGoIts
  Left = 339
  Top = 185
  Caption = 'STQ-MOVIM-005 :: Consigna'#231#227'o - Itens de Remessa'
  ClientHeight = 429
  ClientWidth = 667
  Color = clBtnFace
  Constraints.MinHeight = 468
  Constraints.MinWidth = 580
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 667
    Height = 273
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnData: TPanel
      Left = 0
      Top = 56
      Width = 667
      Height = 217
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object CkContinuar: TCheckBox
        Left = 8
        Top = 192
        Width = 112
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 3
      end
      object Panel8: TPanel
        Left = 0
        Top = 128
        Width = 667
        Height = 61
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object GroupBox1: TGroupBox
          Left = 277
          Top = 0
          Width = 390
          Height = 61
          Align = alClient
          Caption = ' Quantidade e custo total: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object LaQtde: TLabel
            Left = 8
            Top = 16
            Width = 68
            Height = 13
            Caption = '??? Qtde ???:'
          end
          object Label8: TLabel
            Left = 88
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Valor total:'
            Enabled = False
          end
          object Label9: TLabel
            Left = 200
            Top = 16
            Width = 57
            Height = 13
            Caption = 'Custo Frete:'
            Enabled = False
          end
          object Label20: TLabel
            Left = 288
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Custo total:'
            Enabled = False
          end
          object SbPreco: TSpeedButton
            Left = 173
            Top = 32
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SbPrecoClick
          end
          object EdQtde: TdmkEdit
            Left = 8
            Top = 32
            Width = 77
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PLE'
            UpdCampo = 'PLE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdQtdeRedefinido
          end
          object EdCustoBuy: TdmkEdit
            Left = 88
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoBuy'
            UpdCampo = 'CustoBuy'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCustoBuyRedefinido
          end
          object EdCustoFrt: TdmkEdit
            Left = 200
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoFrt'
            UpdCampo = 'CustoFrt'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCustoFrtRedefinido
          end
          object EdValorAll: TdmkEdit
            Left = 288
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoAll'
            UpdCampo = 'CustoAll'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 0
          Width = 277
          Height = 61
          Align = alLeft
          Caption = 'Estoque: '
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
            FocusControl = DBEdit6
          end
          object Label4: TLabel
            Left = 88
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Custo total:'
            FocusControl = DBEdit7
          end
          object Label6: TLabel
            Left = 180
            Top = 16
            Width = 61
            Height = 13
            Caption = 'Custo m'#233'dio:'
            FocusControl = DBEdit4
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 32
            Width = 77
            Height = 21
            TabStop = False
            DataField = 'Qtde'
            DataSource = DsEstoque
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 88
            Top = 32
            Width = 87
            Height = 21
            TabStop = False
            DataField = 'CustoAll'
            DataSource = DsEstoque
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 180
            Top = 32
            Width = 87
            Height = 21
            TabStop = False
            DataField = 'CusMed'
            DataSource = DsEstoque
            TabOrder = 2
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 667
        Height = 64
        Align = alTop
        Caption = ' Origem da remessa: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 663
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object dmkLabel3: TdmkLabel
            Left = 4
            Top = 5
            Width = 124
            Height = 13
            Caption = 'Centro de estoque origem:'
            UpdType = utYes
            SQLType = stNil
          end
          object Label7: TLabel
            Left = 394
            Top = 5
            Width = 164
            Height = 13
            Caption = 'Localiza'#231#227'o no centro de estoque:'
          end
          object EdStqCenCadOri: TdmkEditCB
            Left = 4
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdStqCenCadOriChange
            DBLookupComboBox = CBStqCenCadOri
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenCadOri: TdmkDBLookupComboBox
            Left = 60
            Top = 21
            Width = 329
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsStqCenCadOri
            TabOrder = 1
            dmkEditCB = EdStqCenCadOri
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdStqCenLocOri: TdmkEditCB
            Left = 394
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdStqCenLocOriRedefinido
            DBLookupComboBox = CBStqCenLocOri
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenLocOri: TdmkDBLookupComboBox
            Left = 450
            Top = 21
            Width = 199
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsStqCenLocOri
            TabOrder = 3
            dmkEditCB = EdStqCenLocOri
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 64
        Width = 667
        Height = 64
        Align = alTop
        Caption = 'Destino da remessa: '
        TabOrder = 1
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 663
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object dmkLabel1: TdmkLabel
            Left = 4
            Top = 5
            Width = 127
            Height = 13
            Caption = 'Centro de estoque destino:'
            UpdType = utYes
            SQLType = stNil
          end
          object Label2: TLabel
            Left = 394
            Top = 5
            Width = 164
            Height = 13
            Caption = 'Localiza'#231#227'o no centro de estoque:'
          end
          object EdStqCenCadDst: TdmkEditCB
            Left = 4
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdStqCenCadDstChange
            DBLookupComboBox = CBStqCenCadDst
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenCadDst: TdmkDBLookupComboBox
            Left = 60
            Top = 21
            Width = 329
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsStqCenCadDst
            TabOrder = 1
            dmkEditCB = EdStqCenCadDst
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdStqCenLocDst: TdmkEditCB
            Left = 394
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLocDst
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenLocDst: TdmkDBLookupComboBox
            Left = 450
            Top = 21
            Width = 199
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsStqCenLocDst
            TabOrder = 3
            dmkEditCB = EdStqCenLocDst
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 667
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PnGGX: TPanel
        Left = 85
        Top = 0
        Width = 582
        Height = 56
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label12: TLabel
          Left = 0
          Top = 8
          Width = 76
          Height = 13
          Caption = 'Nome do grupo:'
        end
        object Label13: TLabel
          Left = 276
          Top = 8
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label14: TLabel
          Left = 372
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object Label1: TLabel
          Left = 428
          Top = 8
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object Label5: TLabel
          Left = 488
          Top = 8
          Width = 31
          Height = 13
          Caption = 'Pre'#231'o:'
          FocusControl = DBEdit8
        end
        object DBEdit1: TDBEdit
          Left = 0
          Top = 24
          Width = 273
          Height = 21
          TabStop = False
          DataField = 'NO_GG1'
          DataSource = DsGraGruX
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 276
          Top = 24
          Width = 93
          Height = 21
          TabStop = False
          DataField = 'NO_COR'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 372
          Top = 24
          Width = 53
          Height = 21
          TabStop = False
          DataField = 'NO_TAM'
          DataSource = DsGraGruX
          TabOrder = 2
        end
        object DBEdit5: TDBEdit
          Left = 427
          Top = 24
          Width = 58
          Height = 21
          DataField = 'No_SIGLA'
          DataSource = DsGraGruX
          TabOrder = 3
          OnChange = DBEdit5Change
        end
        object DBEdit8: TDBEdit
          Left = 488
          Top = 24
          Width = 77
          Height = 21
          DataField = 'Preco'
          DataSource = DsPreco
          TabOrder = 4
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 85
        Height = 56
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label11: TLabel
          Left = 8
          Top = 8
          Width = 69
          Height = 13
          Caption = 'Reduzido: [F4]'
        end
        object SpeedButton1: TSpeedButton
          Left = 64
          Top = 24
          Width = 21
          Height = 21
          Caption = '?'
          OnClick = SpeedButton1Click
        end
        object EdGraGruX: TdmkEdit
          Left = 8
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
          OnExit = EdGraGruXExit
          OnKeyDown = EdGraGruXKeyDown
          OnRedefinido = EdGraGruXRedefinido
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 667
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 619
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 571
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 405
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Remessa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 405
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Remessa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 405
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Remessa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 321
    Width = 667
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 663
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 365
    Width = 667
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 663
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 519
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX,  '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM, '
      'med.Nome, med.Sigla, med.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ')
    Left = 168
    Top = 300
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNo_SIGLA: TWideStringField
      FieldName = 'No_SIGLA'
      Size = 30
    end
    object QrGraGruXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGraGruXGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGraGruXFracio: TIntegerField
      FieldName = 'Fracio'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 168
    Top = 348
  end
  object QrStqCenLocOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM stqcenloc '
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 608
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocOriControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocOriNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLocOri: TDataSource
    DataSet = QrStqCenLocOri
    Left = 608
    Top = 108
  end
  object QrStqCenCadOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 704
    Top = 60
    object QrStqCenCadOriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadOriCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadOriNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadOri: TDataSource
    DataSet = QrStqCenCadOri
    Left = 704
    Top = 108
  end
  object QrStqCenLocDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM stqcenloc '
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 608
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocDstControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocDstNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLocDst: TDataSource
    DataSet = QrStqCenLocDst
    Left = 608
    Top = 208
  end
  object DsStqCenCadDst: TDataSource
    DataSet = QrStqCenCadDst
    Left = 708
    Top = 208
  end
  object QrStqCenCadDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 708
    Top = 160
    object QrStqCenCadDstCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadDstCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadDstNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    Left = 512
    Top = 268
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueCustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEstoqueCusMed: TFloatField
      FieldName = 'CusMed'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 580
    Top = 268
  end
  object QrPreco: TMySQLQuery
    Database = Dmod.MyDB
    Left = 516
    Top = 324
    object QrPrecoPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 516
    Top = 372
  end
end
