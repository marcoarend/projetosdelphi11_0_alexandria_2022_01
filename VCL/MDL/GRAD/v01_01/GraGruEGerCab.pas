unit GraGruEGerCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts, dmkImage,
  dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums, Vcl.Menus;

type
  TFmGraGruEGerCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtCad: TBitBtn;
    BtIts: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraGruECab: TmySQLQuery;
    DsGraGruECab: TDataSource;
    QrGraGruEIts: TmySQLQuery;
    DsGraGruEIts: TDataSource;
    QrGraGruEItscProd: TWideStringField;
    QrGraGruEItsNivel1: TIntegerField;
    QrGraGruEItsGraGruX: TIntegerField;
    QrGraGruEItsFornece: TIntegerField;
    QrGraGruEItsEmbalagem: TIntegerField;
    QrGraGruEItsObservacao: TWideStringField;
    QrGraGruEItsLk: TIntegerField;
    QrGraGruEItsDataCad: TDateField;
    QrGraGruEItsDataAlt: TDateField;
    QrGraGruEItsUserCad: TIntegerField;
    QrGraGruEItsUserAlt: TIntegerField;
    QrGraGruEItsAlterWeb: TSmallintField;
    QrGraGruEItsAtivo: TSmallintField;
    QrGraGruEItsxProd: TWideStringField;
    QrGraGruEItsNCM: TWideStringField;
    QrGraGruEItsCFOP: TIntegerField;
    QrGraGruEItsuCom: TWideStringField;
    QrGraGruEItsCFOP_Inn: TIntegerField;
    QrGraGruEItsNO_PRD_TAM_COR: TWideStringField;
    QrGraGruEItsSIGLAUNIDMED: TWideStringField;
    QrGraGruEItsNOMEUNIDMED: TWideStringField;
    QrGraGruEItsNO_FRN: TWideStringField;
    QrGraGruEItsNO_Embalagem: TWideStringField;
    QrGraGruECabcProd: TWideStringField;
    QrGraGruECabxProd: TWideStringField;
    QrGraGruECabFornece: TIntegerField;
    QrGraGruECabNO_FRN: TWideStringField;
    Panel7: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel8: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    LbItensMD: TListBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGrid1: TdmkDBGridZTO;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    Label6: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    PMCad: TPopupMenu;
    IncluiGenerico1: TMenuItem;
    AlteraGenerico1: TMenuItem;
    ExcluiGenerico1: TMenuItem;
    PMIts: TPopupMenu;
    IncluiEspecfico1: TMenuItem;
    AlteraEspecfico1: TMenuItem;
    ExcluiEspecfico1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure QrGraGruECabBeforeClose(DataSet: TDataSet);
    procedure QrGraGruECabAfterScroll(DataSet: TDataSet);
    procedure EdForneceRedefinido(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ExcluiEspecfico1Click(Sender: TObject);
    procedure AlteraEspecfico1Click(Sender: TObject);
    procedure IncluiEspecfico1Click(Sender: TObject);
    procedure BtCadClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure IncluiGenerico1Click(Sender: TObject);
    procedure AlteraGenerico1Click(Sender: TObject);
    procedure ExcluiGenerico1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraFormGraGruEIts(SQLType: TSQLType);
    procedure MostraFormGraGruECad(SQLType: TSQLType);
    procedure ReopenGraGruECab();
    procedure ReopenGraGruEIts();

  public
    { Public declarations }
    //
    procedure ReabrePesquisa();
  end;

  var
  FmGraGruEGerCab: TFmGraGruEGerCab;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, UnGrade_Jan;

{$R *.DFM}

procedure TFmGraGruEGerCab.AlteraEspecfico1Click(Sender: TObject);
begin
  MostraFormGraGruEIts(stUpd);
end;

procedure TFmGraGruEGerCab.AlteraGenerico1Click(Sender: TObject);
begin
  MostraFormGraGruECad(stUpd);
end;

procedure TFmGraGruEGerCab.BtCadClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMCad, BtCad);
end;

procedure TFmGraGruEGerCab.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
end;

procedure TFmGraGruEGerCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMIts, BtIts);
end;

procedure TFmGraGruEGerCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruEGerCab.DBGrid2DblClick(Sender: TObject);
begin
//  UnCfgCadLista.LocalizaPesquisadoInt1(TbCadLista_XXX, QrPesq, FFldID, True);
end;

procedure TFmGraGruEGerCab.EdForneceRedefinido(Sender: TObject);
begin
  ReopenGraGruECab();
end;

procedure TFmGraGruEGerCab.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmGraGruEGerCab.ExcluiEspecfico1Click(Sender: TObject);
var
  cProd, xProd, NCM, uCom: String;
  Nivel1, GraGruX, Fornece, CFOP: Integer;
begin
  cProd    := QrGraGruEItscProd.Value;
  xProd    := QrGraGruEItsxProd.Value;
  NCM      := QrGraGruEItsNCM.Value;
  uCom     := QrGraGruEItsuCom.Value;
  Nivel1   := QrGraGruEItsNivel1.Value;
  GraGruX  := QrGraGruEItsGraGruX.Value;
  Fornece  := QrGraGruEItsFornece.Value;
  CFOP     := QrGraGruEItsCFOP.Value;
  //
  if UMyMod.ExcluiRegistros('Confirma a exclus�o do item espec�fico selecionado?',
  Dmod.QrUpd, 'gragrueits',
  ['cProd', 'xProd', 'NCM', 'CFOP', 'uCom', 'Nivel1', 'GraGruX', 'Fornece'],
  ['=','=','=','=','=','=','=','='],
  [cProd, xProd, NCM, CFOP, uCom, Nivel1, GraGruX, Fornece],
  '') then
    ReopenGraGruEIts();
end;

procedure TFmGraGruEGerCab.ExcluiGenerico1Click(Sender: TObject);
var
  cProd, xProd: String;
  Fornece: Integer;
begin
  cProd    := QrGraGruEItscProd.Value;
  xProd    := QrGraGruEItsxProd.Value;
  Fornece  := QrGraGruEItsFornece.Value;
  //
  if UMyMod.ExcluiRegistros('Confirma a exclus�o do item espec�fico selecionado?',
  Dmod.QrUpd, 'gragruecad',
  ['cProd', 'xProd', 'Fornece'],
  ['=','=','='],
  [cProd, xProd, Fornece],
  '') then
    ReopenGraGruECab((*cProd, xProd, Fornece*));
end;

procedure TFmGraGruEGerCab.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruEGerCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align := alClient;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornece, Dmod.MyDB, [
  'SELECT Codigo, ',
  'IF(Tipo=0,RazaoSocial,Nome) NO_ENT ',
  'FROM entidades ',
  'ORDER BY NO_ENT ',
  '']);
end;

procedure TFmGraGruEGerCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruEGerCab.FormShow(Sender: TObject);
begin
  ReopenGraGruECab();
end;

procedure TFmGraGruEGerCab.IncluiEspecfico1Click(Sender: TObject);
begin
  MostraFormGraGruEIts(stIns);
end;

procedure TFmGraGruEGerCab.IncluiGenerico1Click(Sender: TObject);
begin
  MostraFormGraGruECad(stIns);
end;

procedure TFmGraGruEGerCab.MostraFormGraGruECad(SQLType: TSQLType);
begin
  Grade_Jan.MostraFormGraGruECad(stIns, QrGraGruECab, QrGraGruECabFornece.Value,
  QrGraGruECabcProd.Value, QrGraGruECabxProd.Value);
end;

procedure TFmGraGruEGerCab.MostraFormGraGruEIts(SQLType: TSQLType);
const
  prod_cEAN   = '';
  InfAdProd   = '';
  prod_CFOP   = 0;
  ICMS_Orig   = 0;
  ICMS_CST    = 0;
  IPI_CST     = 0;
  PIS_CST     = 0;
  COFINS_CST  = 0;
  prod_EXTIPI = '';
  IPI_cEnq    = '';
var
  GraGruX: Integer;
  NomeGGX: String;
begin
  Grade_Jan.MostraFormGraGruEIts(stIns, QrGraGruEItsFornece.Value,
  (*, PrdGrupTip*)QrGraGruEItscProd.Value, QrGraGruEItsxProd.Value, prod_cEAN,
  QrGraGruEItsNCM.Value, QrGraGruEItsuCom.Value, InfAdProd,
  QrGraGruEItsCFOP.Value, ICMS_Orig, ICMS_CST, IPI_CST, PIS_CST,
  COFINS_CST, prod_EXTIPI, IPI_cEnq, GraGruX, NomeGGX, True);
  //
  ReopenGraGruEIts();
end;

procedure TFmGraGruEGerCab.QrGraGruECabAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGruEIts();
end;

procedure TFmGraGruEGerCab.QrGraGruECabBeforeClose(DataSet: TDataSet);
begin
  QrGraGruEIts.Close;
end;

procedure TFmGraGruEGerCab.ReabrePesquisa();
begin
end;

procedure TFmGraGruEGerCab.ReopenGraGruECab();
var
  Fornecedor: Integer;
  SQL_Forn: String;
begin
  Fornecedor := EdFornece.ValueVariant;
  SQL_Forn := '';
  if Fornecedor <> 0 then
    SQL_Forn := 'WHERE Fornece=' + Geral.FF0(Fornecedor);
  if UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruECab, Dmod.MyDB, [
  'SELECT DISTINCT gge.cProd, gge.xProd, gge.Fornece,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN',
  'FROM gragrueits gge ',
  'LEFT JOIN entidades  frn ON frn.Codigo=gge.Fornece',
  SQL_Forn,
  '']) then
  begin
    //QrGraGruECab.Locate(
    //'Fornece;cProd;xProd', VarArrayOf([Fornece, cProd, xProd]), []);
  end;
end;

procedure TFmGraGruEGerCab.ReopenGraGruEIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruEIts, Dmod.MyDB, [
  'SELECT gge.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,',
  'emb.Nome NO_Embalagem',
  'FROM gragrueits gge ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=gge.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'LEFT JOIN entidades  frn ON frn.Codigo=gge.Fornece',
  'LEFT JOIN embalagens emb ON emb.Codigo=gge.Embalagem',
  'WHERE cProd="' + QrGraGruECabcProd.Value + '"',
  'AND xProd="' + QrGraGruECabxProd.Value + '"',
  'AND Fornece=' + Geral.FF0(QrGraGruECabFornece.Value),
  '']);
end;

procedure TFmGraGruEGerCab.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
