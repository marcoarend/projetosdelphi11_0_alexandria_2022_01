object FmGraGruC: TFmGraGruC
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-005 :: Seleciona Cor'
  ClientHeight = 286
  ClientWidth = 488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 488
    Height = 130
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Nivel 1:'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 96
      Top = 8
      Width = 56
      Height = 13
      Caption = 'ID Controle:'
    end
    object SpeedButton1: TSpeedButton
      Left = 444
      Top = 68
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label3: TLabel
      Left = 12
      Top = 52
      Width = 19
      Height = 13
      Caption = 'Cor:'
    end
    object EdCor: TdmkEditCB
      Left = 12
      Top = 68
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCor
      IgnoraDBLookupComboBox = False
    end
    object CBCor: TdmkDBLookupComboBox
      Left = 68
      Top = 68
      Width = 373
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsGraCorCad
      TabOrder = 1
      dmkEditCB = EdCor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object DBEdit1: TdmkDBEdit
      Left = 12
      Top = 24
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Nivel1'
      DataSource = FmGraGruN.DsGraGru1
      TabOrder = 2
      UpdCampo = 'Nivel1'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object EdControle: TdmkEdit
      Left = 96
      Top = 24
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 100
      Width = 121
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 4
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 488
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 440
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 392
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 171
        Height = 32
        Caption = 'Seleciona Cor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 171
        Height = 32
        Caption = 'Seleciona Cor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 171
        Height = 32
        Caption = 'Seleciona Cor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 178
    Width = 488
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 484
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 222
    Width = 488
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 484
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 340
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraCorCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gracorcad '
      'ORDER BY Nome')
    Left = 344
    Top = 52
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 372
    Top = 52
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdCor
    QryCampo = 'GraCorCad'
    UpdCampo = 'GraCorCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
end
