unit CheckCores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DB, DBGrids, dmkDBGridZTO,
  DBCtrls, dmkImage, UnDmkEnums, dmkDBGrid, mySQLDbTables;

type
  TFmCheckCores = class(TForm)
    Panel1: TPanel;
    DdGraCorCad: TDataSource;
    DBGGCC: TdmkDBGridZTO;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrGraCorCad: TMySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadCodUsu: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    BtCadastro: TBitBtn; 
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtCadastroClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmCheckCores: TFmCheckCores;

implementation

uses UnMyObjects, UnGrade_Jan, dmkDAC_PF, Module;

{$R *.DFM}

procedure TFmCheckCores.BtCadastroClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorCad(0);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
end;

procedure TFmCheckCores.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGGCC), False);
end;

procedure TFmCheckCores.BtOKClick(Sender: TObject);
begin
  if DBGGCC.SelectedRows.Count > 0 then
  begin
    FCadastrar := True;
    Close;
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmCheckCores.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmCheckCores.BtTodosClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGGCC), True);
end;

procedure TFmCheckCores.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCheckCores.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCadastrar := False;
end;

procedure TFmCheckCores.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
