unit GraGruAti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, ComCtrls, dmkImage, UnDmkEnums;

type
  TFmGraGruAti = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    StaticText2: TStaticText;
    GradeA: TStringGrid;
    StaticText6: TStaticText;
    GradeX: TStringGrid;
    GradeC: TStringGrid;
    StaticText1: TStaticText;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure GradeAClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruAti: TFmGraGruAti;

implementation

uses GraGruN, (*MyVCLSkin,*)MyGlyfs, ModProd, UnMyObjects;

{$R *.DFM}

procedure TFmGraGruAti.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruAti.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruAti.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  PageControl1.ActivePageIndex := 0;
  DmProd.ConfigGrades1(
    FmGraGruN.QrGraGru1GraTamCad.Value,
    FmGraGruN.QrGraGru1Nivel1.Value,
    FmGraGruN.QrGraCusPrcUCodigo.Value, FmGraGruN.FEntiPrecos,
    GradeA, GradeX, GradeC, nil, nil, nil);
end;

procedure TFmGraGruAti.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruAti.GradeAClick(Sender: TObject);
begin
  if (GradeA.Col = 0) or (GradeA.Row = 0) then
    FmGraGruN.StatusVariosItensCorTam(GradeA, GradeC, GradeX)
  else
    FmGraGruN.StatusItemCorTam(GradeA, GradeC, GradeX);

  //

  DmProd.ConfigGrades1(FmGraGruN.QrGraGru1GraTamCad.Value,
    FmGraGruN.QrGraGru1Nivel1.Value,
    FmGraGruN.QrGraCusPrcUCodigo.Value, FmGraGruN.FEntiPrecos,
    FmGraGruN.GradeA, FmGraGruN.GradeX, FmGraGruN.GradeC, nil, nil, nil);
end;

procedure TFmGraGruAti.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel1.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel1.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    //MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
    FmMyGlyfs.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmGraGruAti.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else
    Cor := clWindow;  
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGruAti.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeX, Rect, clBlack,
      Cor, taLeftJustify,
      GradeX.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeX, Rect, clBlack,
      Cor, taCenter,
      GradeX.Cells[Acol, ARow], 0, 0, False);
end;

end.
