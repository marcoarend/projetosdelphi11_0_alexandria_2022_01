unit FisRegEnPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw;

type
  TFmFisRegEnPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrFisRegEnt: TMySQLQuery;
    QrFisRegEntCodigo: TIntegerField;
    QrFisRegEntEntidade: TIntegerField;
    QrFisRegEntLk: TIntegerField;
    QrFisRegEntDataCad: TDateField;
    QrFisRegEntDataAlt: TDateField;
    QrFisRegEntUserCad: TIntegerField;
    QrFisRegEntUserAlt: TIntegerField;
    QrFisRegEntAlterWeb: TSmallintField;
    QrFisRegEntAWServerID: TIntegerField;
    QrFisRegEntAWStatSinc: TSmallintField;
    QrFisRegEntAtivo: TSmallintField;
    QrFisRegEntObservacao: TWideStringField;
    DsFisRegEnt: TDataSource;
    QrFisRegEntNO_FisRegCad: TWideStringField;
    DBGFisRegEnt: TDBGrid;
    Panel3: TPanel;
    EdPsq: TEdit;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPsqChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGFisRegEntDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure Seleciona();
  public
    { Public declarations }
    FSelecionou: Boolean;
    FFisRegEnt: Integer;
    //
    procedure ReopenPesq();
  end;

  var
  FmFisRegEnPsq: TFmFisRegEnPsq;

implementation

uses UnMyObjects, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmFisRegEnPsq.BtOKClick(Sender: TObject);
begin
  Seleciona();
end;

procedure TFmFisRegEnPsq.BtSaidaClick(Sender: TObject);
begin
  FSelecionou := False;
  Close;
end;

procedure TFmFisRegEnPsq.DBGFisRegEntDblClick(Sender: TObject);
begin
  Seleciona();
end;

procedure TFmFisRegEnPsq.EdPsqChange(Sender: TObject);
var
  Texto: String;
begin
  Texto := EdPsq.Text;
  //
  ReopenPesq();
end;

procedure TFmFisRegEnPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegEnPsq.FormCreate(Sender: TObject);
begin
  FSelecionou := False;
  ImgTipo.SQLType := stLok;
end;

procedure TFmFisRegEnPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFisRegEnPsq.ReopenPesq();
var
  Str: String;
begin
  Str := EdPsq.Text;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegEnt, Dmod.MyDB, [
  'SELECT frc.Nome NO_FisRegCad,  ',
  'fre.*   ',
  'FROM fisregent fre  ',
  'LEFT JOIN fisregcad frc ON frc.Codigo=fre.Codigo ',
  'WHERE fre.Entidade=1 ',
  'AND (',
  '  frc.Nome LIKE "%' + Str + '%"',
  '  OR ',
  '  fre.Observacao LIKE "%' + Str + '%"',
  ')',
  '']);
end;

procedure TFmFisRegEnPsq.Seleciona;
begin
  if (QrFisRegEnt.State <> dsInactive) and (QrFisRegEnt.RecordCount > 0)  then
  begin
    FSelecionou := True;
    FFisRegEnt  := QrFisRegEntCodigo.Value;
    Close;
  end;
end;

end.
