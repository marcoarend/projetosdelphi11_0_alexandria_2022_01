unit MatFichIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  DB, mySQLDbTables, Variants, Grids, dmkImage, UnDmkEnums;

type
  TFmMatFichIts = class(TForm)
    Panel1: TPanel;
    QrMatPartCad: TmySQLQuery;
    DsMatPartCad: TDataSource;
    QrMatPartCadCodigo: TIntegerField;
    QrMatPartCadCodUsu: TIntegerField;
    QrMatPartCadNome: TWideStringField;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    EdMatPartCad: TdmkEditCB;
    CBMatPartCad: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    RGGrupTip: TRadioGroup;
    GroupBox3: TGroupBox;
    EdPesqGraGru1: TEdit;
    CBGraGru1: TdmkDBLookupComboBox;
    EdGraGru1: TdmkEditCB;
    Label4: TLabel;
    Label5: TLabel;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1GraTamCad: TIntegerField;
    DsGraGru1: TDataSource;
    GroupBox5: TGroupBox;
    EdAbrange: TdmkEdit;
    EdAbrangeCodi: TdmkEdit;
    EdAbrangeNome: TdmkEdit;
    GradeA: TStringGrid;
    GradeX: TStringGrid;
    GradeC: TStringGrid;
    Label6: TLabel;
    EdPesqPrdGrupTip: TEdit;
    Label7: TLabel;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    DsPrdGrupTip: TDataSource;
    QrPrdGrupTip: TmySQLQuery;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    GroupBox6: TGroupBox;
    EdPermite: TdmkEdit;
    EdPermiteCodi: TdmkEdit;
    EdPermiteNome: TdmkEdit;
    QrGraGru1PrdGrupTip: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure RGGrupTipClick(Sender: TObject);
    procedure EdPesqGraGru1Exit(Sender: TObject);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGraGru1BeforeClose(DataSet: TDataSet);
    procedure EdGraGru1Change(Sender: TObject);
    procedure GradeCClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdMatPartCadChange(Sender: TObject);
    procedure EdPesqPrdGrupTipExit(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
  private
    { Private declarations }
    FNivPermite,
    FPermitePgt,
    FPermiteGg1,
    FPermiteGgc,
    FPermiteGti,
    FPermiteGgx: Integer;
    procedure HabilitaConfirma();
    procedure LimpaPesquisa();
    procedure ReopenPrdGrupTip(CodUsu: Integer);
    procedure ReopenGraGru1(Nivel1: Integer);
    procedure DefinePermite(NivAbrange: Integer);
  public
    { Public declarations }
  end;

  var
  FmMatFichIts: TFmMatFichIts;

implementation

uses MatPartCad, MyDBCheck, UnInternalConsts, ModProd, UnMyObjects, MatFichCab,
UMySQLModule, Module, dmkGeral;

{$R *.DFM}

procedure TFmMatFichIts.BtOKClick(Sender: TObject);
var
  NivAbrange, AbrangePgt, AbrangeGg1, AbrangeGgc, AbrangeGti, AbrangeGgx,
  MatPartCad, Controle: Integer;
  Aviso_Txt: String;
begin
  if FmMatFichCab.FAbrangeGg1 = 0 then
  begin
    Geral.MensagemBox('Grupo de produtos indefinido!! AVISE A DERMATEK',
    'Aviso', MB_OK+MB_ICONERROR);
    Exit;
  end;
  NivAbrange := FmMatFichCab.FNivAbrange;
  AbrangePgt := FmMatFichCab.FAbrangePgt;
  AbrangeGg1 := FmMatFichCab.FAbrangeGg1;
  AbrangeGgc := FmMatFichCab.FAbrangeGgc;
  AbrangeGti := FmMatFichCab.FAbrangeGti;
  AbrangeGgx := FmMatFichCab.FAbrangeGgx;
  //
  MatPartCad := EdMatPartCad.ValueVariant;
  //
  Aviso_Txt := '';
  if ((FNivPermite = 0)                      ) then Aviso_Txt := '"ERRO ?"';
  if ((FNivPermite = 5) and (FPermitePgt = 0)) then Aviso_Txt := 'Tipo de Grupo de produtos';
  if ((FNivPermite = 4) and (FPermiteGg1 = 0)) then Aviso_Txt := 'Grupode produtos';
  if ((FNivPermite = 3) and (FPermiteGgc = 0)) then Aviso_Txt := 'Cor';
  if ((FNivPermite = 2) and (FPermiteGti = 0)) then Aviso_Txt := 'Tamanho';
  if ((FNivPermite = 1) and (FPermiteGgx = 0)) then Aviso_Txt := 'Reduzido';
  //
  if Aviso_Txt <> '' then
  begin
    Application.MessageBox(PChar(Aviso_Txt + ' indefinido!! AVISE A DERMATEK'),
    'Aviso', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Controle := UMyMod.BuscaEmLivreY_Def('gramatits', 'Controle', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gramatits', False, [
    'NivAbrange', 'AbrangePgt', 'AbrangeGg1',
    'AbrangeGgc', 'AbrangeGti', 'AbrangeGgx',
    'NivPermite', 'PermitePgt', 'PermiteGg1',
    'PermiteGgc', 'PermiteGti', 'PermiteGgx',
    'MatPartCad'
  ], ['Controle'], [
    NivAbrange, AbrangePgt, AbrangeGg1,
    AbrangeGgc, AbrangeGti, AbrangeGgx,
    FNivPermite, FPermitePgt, FPermiteGg1,
    FPermiteGgc, FPermiteGti, FPermiteGgx,
    MatPartCad
  ], [Controle], True) then
  begin
    EdMatPartCad.ValueVariant := 0;
    CBMatPartCad.KeyValue     := Null;
    FmMatFichCab.ReopenGraMatIts(Controle);
  end;
end;

procedure TFmMatFichIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatFichIts.DefinePermite(NivAbrange: Integer);
var
  Ggc, Gti, Ggx: Integer;
begin
  LimpaPesquisa();
  //
  if (NivAbrange = 5) and (EdPrdGrupTip.ValueVariant > 0) then
  begin
    FNivPermite := 5;
    FPermitePgt := EdPrdGrupTip.ValueVariant;
    FPermiteGg1 := 0;
    FPermiteGgc := 0;
    FPermiteGti := 0;
    FPermiteGgx := 0;
    if FPermitePgt > 0 then
    begin
      EdPermite.ValueVariant := 'Todos grupos do tipo ';
      EdPermiteCodi.ValueVariant := QrPrdGrupTipCodUsu.Value;
      EdPermiteNome.ValueVariant := QrPrdGrupTipNome.Value;
    end;
  end else
  if ((NivAbrange = 4) and (EdGraGru1.ValueVariant > 0))
  or ((GradeC.Col = 0) and (GradeC.Row = 0) and (EdGraGru1.ValueVariant > 0)) then
  begin
    FNivPermite := 4;
    FPermitePgt := QrGraGru1PrdGrupTip.Value;
    FPermiteGg1 := QrGraGru1Nivel1.Value;
    FPermiteGgc := 0;
    FPermiteGti := 0;
    FPermiteGgx := 0;
    if QrGraGru1.RecordCount > 0 then
    begin
      EdPermite.ValueVariant := 'Todo grupo ';
      EdPermiteCodi.ValueVariant := QrGraGru1CodUsu.Value;
      EdPermiteNome.ValueVariant := QrGraGru1Nome.Value;
    end;
  end else if (GradeC.Col = 0) and (NivAbrange = 0) then
  begin
    Ggc := Geral.IMV(GradeX.Cells[GradeC.Col,GradeC.Row]);
    FNivPermite := 3;
    FPermitePgt := QrGraGru1PrDGrupTip.Value;
    FPermiteGg1 := QrGraGru1Nivel1.Value;
    FPermiteGgc := Ggc;
    FPermiteGti := 0;
    FPermiteGgx := 0;
    if Ggc <> 0 then
    begin
      EdPermite.ValueVariant := 'Todos tamanhos ativos da cor ';
      EdPermiteCodi.ValueVariant := Ggc;
      EdPermiteNome.ValueVariant := GradeC.Cells[GradeC.Col,GradeC.Row];
    end;
  end else if (GradeC.Row = 0) and (NivAbrange = 0) then
  begin
    Gti := Geral.IMV(GradeX.Cells[GradeC.Col,GradeC.Row]);
    FNivPermite := 2;
    FPermitePgt := QrGraGru1PrDGrupTip.Value;
    FPermiteGg1 := QrGraGru1Nivel1.Value;
    FPermiteGgc := 0;
    FPermiteGti := Gti;
    FPermiteGgx := 0;
    if Gti <> 0 then
    begin
      EdPermite.ValueVariant := 'Todas cores ativas do tamanho ';
      EdPermiteCodi.ValueVariant := Gti;
      EdPermiteNome.ValueVariant := GradeC.Cells[GradeC.Col,GradeC.Row];
    end;
  end else if NivAbrange = 0 then
  begin
    Ggc := Geral.IMV(GradeX.Cells[0,GradeC.Row]);
    Gti := Geral.IMV(GradeX.Cells[GradeC.Col,0]);
    Ggx := Geral.IMV(GradeC.Cells[GradeC.Col,GradeC.Row]);
    FNivPermite := 1;
    FPermitePgt := QrGraGru1PrDGrupTip.Value;
    FPermiteGg1 := QrGraGru1Nivel1.Value;
    FPermiteGgc := Ggc;
    FPermiteGti := Gti;
    FPermiteGgx := Ggx;
    if Ggx <> 0 then
    begin
      EdPermite.ValueVariant := 'Reduzido ';
      EdPermiteCodi.ValueVariant := Ggx;
      EdPermiteNome.ValueVariant := DmProd.NomeReduzido(QrGraGru1Nome.Value,
        GradeC, GradeC.Col, GradeC.Row);
    end;
  end;
  HabilitaConfirma();
end;

procedure TFmMatFichIts.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.ValueVariant > 0 then
  begin
    DmProd.ConfigGrades0(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC);
    DefinePermite(4);
  end else begin
    MyObjects.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);
    DefinePermite(5);
  end;
end;

procedure TFmMatFichIts.EdMatPartCadChange(Sender: TObject);
begin
  HabilitaConfirma();
end;

procedure TFmMatFichIts.EdPesqGraGru1Exit(Sender: TObject);
begin
  ReopenGraGru1(0);
end;

procedure TFmMatFichIts.EdPesqPrdGrupTipExit(Sender: TObject);
begin
  ReopenPrdGrupTip(0);
end;

procedure TFmMatFichIts.EdPrdGrupTipChange(Sender: TObject);
begin
  DefinePermite(5);
  ReopenGraGru1(0);
end;

procedure TFmMatFichIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMatFichIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FNivPermite := 0;
  FPermitePgt := 0;
  FPermiteGg1 := 0;
  FPermiteGgc := 0;
  FPermiteGti := 0;
  FPermiteGgx := 0;
  QrMatPartCad.Open;
end;

procedure TFmMatFichIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMatFichIts.GradeCClick(Sender: TObject);
begin
  DefinePermite(0);
end;

procedure TFmMatFichIts.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmMatFichIts.HabilitaConfirma();
begin
  BtOK.Enabled :=
    (FNivPermite > 0)
  and
    (EdPermite.ValueVariant <> '')
  and
    (EdMatPartCad.ValueVariant <> 0);
end;

procedure TFmMatFichIts.LimpaPesquisa();
begin
  FNivPermite := 0;
  FPermitePgt := 0;
  FPermiteGg1 := 0;
  FPermiteGgc := 0;
  FPermiteGti := 0;
  FPermiteGgx := 0;
  //
  EdPermite.ValueVariant := '';
  EdPermiteCodi.ValueVariant := 0;
  EdPermiteNome.ValueVariant := '';
  //
  BtOK.Enabled := False;
end;

procedure TFmMatFichIts.QrGraGru1BeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);
end;

procedure TFmMatFichIts.RGGrupTipClick(Sender: TObject);
begin
  ReopenPrdGrupTip(0);
end;

procedure TFmMatFichIts.ReopenPrdGrupTip(CodUsu: Integer);
begin
  QrPrdGrupTip.Close;
  QrPrdGrupTip.SQL.Clear;
  QrPrdGrupTip.SQL.Add('SELECT Codigo, CodUsu, Nome');
  QrPrdGrupTip.SQL.Add('FROM prdgruptip');
  case RGGrupTip.ItemIndex of
    3: QrPrdGrupTip.SQL.Add('WHERE TipPrd in (1,2)');
    else QrPrdGrupTip.SQL.Add('WHERE TipPrd=' + FormatFloat('0', RGGrupTip.ItemIndex));
  end;
  QrPrdGrupTip.SQL.Add('AND Nome LIKE "%' + EdPesqPrdGrupTip.Text + '%"');
  QrPrdGrupTip.SQL.Add('ORDER BY Nome');
  QrPrdGrupTip.Open;
  if CodUsu <> 0 then
    QrPrdGrupTip.Locate('CodUsu', CodUsu, []);
  EdPrdGrupTip.Text := '';
  CBPrdGrupTip.KeyValue := Null;
  //
  ReopenGraGru1(0);
end;

procedure TFmMatFichIts.ReopenGraGru1(Nivel1: Integer);
begin
  QrGraGru1.Close;
  QrGraGru1.SQL.Clear;
  QrGraGru1.SQL.Add('SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, ');
  QrGraGru1.SQL.Add('gg1.PrdGrupTip, gg1.GraTamCad');
  QrGraGru1.SQL.Add('FROM gragru1 gg1');
  QrGraGru1.SQL.Add('WHERE gg1.PrdGrupTip=' + FormatFloat('0', QrPrdGrupTipCodigo.Value));
  QrGraGru1.SQL.Add('AND gg1.Nome LIKE "%' + EdPesqGraGru1.Text + '%"');
  QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
  QrGraGru1.Open;
  if Nivel1 <> 0 then
    QrGraGru1.Locate('Nivel1', Nivel1, []);
  EdGraGru1.Text := '';
  CBGraGru1.KeyValue := Null;
end;

procedure TFmMatFichIts.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatPartCad, FmMatPartCad, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    FmMatPartCad.ShowModal;
    FmMatPartCad.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrMatPartCad.Close;
      QrMatPartCad.Open;
      //
      if QrMatPartCad.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMatPartCad.ValueVariant := QrMatPartCadCodUsu.Value;
        CBMatPartCad.KeyValue     := QrMatPartCadCodUsu.Value;
      end;
    end;
  end;
end;

end.

