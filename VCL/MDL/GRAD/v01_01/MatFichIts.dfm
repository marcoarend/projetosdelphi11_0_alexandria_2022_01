object FmMatFichIts: TFmMatFichIts
  Left = 339
  Top = 185
  Caption = 'MAT-FICHS-002 :: Partes de Fichas de Materiais'
  ClientHeight = 656
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 111
    ExplicitHeight = 476
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 500
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 782
      ExplicitHeight = 474
      object GroupBox1: TGroupBox
        Left = 0
        Top = 45
        Width = 784
        Height = 362
        Align = alClient
        Caption = ' Pesquisa de produtos: '
        TabOrder = 0
        ExplicitWidth = 782
        ExplicitHeight = 336
        object GradeC: TStringGrid
          Left = 2
          Top = 130
          Width = 780
          Height = 230
          TabStop = False
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 4
          OnClick = GradeCClick
          OnDrawCell = GradeCDrawCell
          ExplicitWidth = 778
          ExplicitHeight = 204
          RowHeights = (
            18
            18)
        end
        object RGGrupTip: TRadioGroup
          Left = 2
          Top = 15
          Width = 780
          Height = 42
          Align = alTop
          Caption = ' Conjunto tipos de produtos: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Nenhum'
            'Produto'
            'Mat'#233'ria-prima'
            'Ambos')
          TabOrder = 0
          OnClick = RGGrupTipClick
          ExplicitWidth = 778
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 57
          Width = 780
          Height = 73
          Align = alTop
          Caption = ' Grupo de produtos: '
          TabOrder = 1
          ExplicitWidth = 778
          object Label4: TLabel
            Left = 12
            Top = 48
            Width = 23
            Height = 13
            Caption = 'Fltro:'
          end
          object Label5: TLabel
            Left = 220
            Top = 48
            Width = 32
            Height = 13
            Caption = 'Grupo:'
          end
          object Label6: TLabel
            Left = 12
            Top = 24
            Width = 23
            Height = 13
            Caption = 'Fltro:'
          end
          object Label7: TLabel
            Left = 220
            Top = 24
            Width = 24
            Height = 13
            Caption = 'Tipo:'
          end
          object EdPesqGraGru1: TEdit
            Left = 52
            Top = 44
            Width = 160
            Height = 21
            TabOrder = 3
            OnExit = EdPesqGraGru1Exit
          end
          object CBGraGru1: TdmkDBLookupComboBox
            Left = 316
            Top = 44
            Width = 449
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsGraGru1
            TabOrder = 5
            dmkEditCB = EdGraGru1
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdGraGru1: TdmkEditCB
            Left = 260
            Top = 44
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGraGru1Change
            DBLookupComboBox = CBGraGru1
            IgnoraDBLookupComboBox = False
          end
          object EdPesqPrdGrupTip: TEdit
            Left = 52
            Top = 20
            Width = 160
            Height = 21
            TabOrder = 0
            OnExit = EdPesqPrdGrupTipExit
          end
          object EdPrdGrupTip: TdmkEditCB
            Left = 260
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPrdGrupTipChange
            DBLookupComboBox = CBPrdGrupTip
            IgnoraDBLookupComboBox = False
          end
          object CBPrdGrupTip: TdmkDBLookupComboBox
            Left = 316
            Top = 20
            Width = 449
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsPrdGrupTip
            TabOrder = 2
            dmkEditCB = EdPrdGrupTip
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object GradeA: TStringGrid
          Left = 156
          Top = 188
          Width = 225
          Height = 137
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 2
          Visible = False
          RowHeights = (
            18
            18)
        end
        object GradeX: TStringGrid
          Left = 384
          Top = 188
          Width = 205
          Height = 137
          ColCount = 2
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
          TabOrder = 3
          Visible = False
          RowHeights = (
            18
            18)
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 407
        Width = 784
        Height = 93
        Align = alBottom
        Caption = ' Dados de inclus'#227'o: '
        TabOrder = 1
        ExplicitTop = 381
        ExplicitWidth = 782
        object Label2: TLabel
          Left = 12
          Top = 68
          Width = 28
          Height = 13
          Caption = 'Parte:'
        end
        object SpeedButton1: TSpeedButton
          Left = 736
          Top = 64
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdMatPartCad: TdmkEditCB
          Left = 44
          Top = 64
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdMatPartCadChange
          DBLookupComboBox = CBMatPartCad
          IgnoraDBLookupComboBox = False
        end
        object CBMatPartCad: TdmkDBLookupComboBox
          Left = 100
          Top = 64
          Width = 633
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsMatPartCad
          TabOrder = 1
          dmkEditCB = EdMatPartCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GroupBox6: TGroupBox
          Left = 2
          Top = 15
          Width = 780
          Height = 45
          Align = alTop
          Caption = ' Sele'#231#227'o do permissor de customiza'#231#227'o (customizador): '
          Color = clBtnFace
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          ExplicitWidth = 778
          object EdPermite: TdmkEdit
            Left = 12
            Top = 16
            Width = 285
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdPermiteCodi: TdmkEdit
            Left = 296
            Top = 16
            Width = 89
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdPermiteNome: TdmkEdit
            Left = 384
            Top = 16
            Width = 372
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 45
        Align = alTop
        Caption = ' Produto customiz'#225'vel: '
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        ExplicitWidth = 782
        object EdAbrange: TdmkEdit
          Left = 12
          Top = 16
          Width = 285
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdAbrangeCodi: TdmkEdit
          Left = 296
          Top = 16
          Width = 89
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAbrangeNome: TdmkEdit
          Left = 384
          Top = 16
          Width = 389
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 361
        Height = 32
        Caption = 'Partes de Fichas de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 361
        Height = 32
        Caption = 'Partes de Fichas de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 361
        Height = 32
        Caption = 'Partes de Fichas de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrMatPartCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM matpartcad'
      'ORDER BY Nome')
    Left = 148
    Top = 408
    object QrMatPartCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMatPartCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMatPartCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMatPartCad: TDataSource
    DataSet = QrMatPartCad
    Left = 176
    Top = 408
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru1BeforeClose
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, '
      'gg1.GraTamCad, gg1.PrdGrupTip'
      'FROM gragru1 gg1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd in (:P0,:P1)'
      'AND gg1.Nome LIKE :P2'
      'ORDER BY gg1.Nome')
    Left = 64
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 92
    Top = 12
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 32
    Top = 12
  end
  object QrPrdGrupTip: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru1BeforeClose
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 4
    Top = 12
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
end
