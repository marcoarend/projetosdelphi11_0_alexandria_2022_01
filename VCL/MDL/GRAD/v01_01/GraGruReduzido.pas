unit GraGruReduzido;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, Mask, dmkImage, UnDmkEnums, UnInternalConsts,
  Vcl.Menus, UnProjGroup_Consts;

type
  TFmGraGruReduzido = class(TForm)
    Panel1: TPanel;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXNivel1: TIntegerField;
    QrGraGruXNO_PRD: TWideStringField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXNO_PGT: TWideStringField;
    QrGraGruXSIGLA: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXGraCorCad: TIntegerField;
    QrGraGruXGraGruC: TIntegerField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraTamI: TIntegerField;
    QrGraGruXNivel2: TIntegerField;
    QrGraGruXNO_GG2: TWideStringField;
    QrGraGruXNivel3: TIntegerField;
    QrGraGruXNO_GG3: TWideStringField;
    QrGraGruXCU_GG1: TIntegerField;
    QrGraGruXCU_GG2: TIntegerField;
    QrGraGruXCU_GG3: TIntegerField;
    QrGraGruXCU_PGT: TIntegerField;
    QrGraGruXTitNiv3: TWideStringField;
    QrGraGruXTitNiv2: TWideStringField;
    QrGraGruXTitNiv1: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    QrCECTInn: TmySQLQuery;
    DsCECTInn: TDataSource;
    QrNFeMPInn: TmySQLQuery;
    DsNFeMPInn: TDataSource;
    QrNFeMPInnCodigo: TIntegerField;
    QrNFeMPInnControle: TIntegerField;
    QrNFeMPInnGraGruX: TIntegerField;
    QrNFeMPInnPecas: TFloatField;
    QrNFeMPInnPLE: TFloatField;
    QrNFeMPInnAreaM2: TFloatField;
    QrNFeMPInnAreaP2: TFloatField;
    QrNFeMPInnCMPValor: TFloatField;
    QrNFeMPInnGerBxaEstq: TIntegerField;
    QrNFeMPInnLk: TIntegerField;
    QrNFeMPInnDataCad: TDateField;
    QrNFeMPInnDataAlt: TDateField;
    QrNFeMPInnUserCad: TIntegerField;
    QrNFeMPInnUserAlt: TIntegerField;
    QrNFeMPInnAlterWeb: TSmallintField;
    QrNFeMPInnAtivo: TSmallintField;
    QrCECTInnCodigo: TIntegerField;
    QrCECTInnCliente: TIntegerField;
    QrCECTInnDataE: TDateField;
    QrCECTInnNFInn: TIntegerField;
    QrCECTInnNFRef: TIntegerField;
    QrCECTInnInnQtdkg: TFloatField;
    QrCECTInnInnQtdPc: TFloatField;
    QrCECTInnInnQtdVal: TFloatField;
    QrCECTInnSdoQtdkg: TFloatField;
    QrCECTInnSdoQtdPc: TFloatField;
    QrCECTInnSdoQtdVal: TFloatField;
    QrCECTInnLk: TIntegerField;
    QrCECTInnDataCad: TDateField;
    QrCECTInnDataAlt: TDateField;
    QrCECTInnUserCad: TIntegerField;
    QrCECTInnUserAlt: TIntegerField;
    QrCECTInnAlterWeb: TSmallintField;
    QrCECTInnAtivo: TSmallintField;
    QrCECTInnEmpresa: TIntegerField;
    QrCECTInnInnQtdM2: TFloatField;
    QrCECTInnSdoQtdM2: TFloatField;
    QrCECTInnGraGruX: TIntegerField;
    QrCECTInnTipoNF: TSmallintField;
    QrCECTInnrefNFe: TWideStringField;
    QrCECTInnmodNF: TSmallintField;
    QrCECTInnSerie: TIntegerField;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DsPQCli: TDataSource;
    QrPQCli: TmySQLQuery;
    DBGrid3: TDBGrid;
    QrEtqGeraIts: TmySQLQuery;
    DsEtqGeraIts: TDataSource;
    QrFatConIts: TmySQLQuery;
    DsFatConIts: TDataSource;
    QrFatDivRef: TmySQLQuery;
    DsFatDivRef: TDataSource;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    DBGrid4: TDBGrid;
    QrFatConItsCodigo: TIntegerField;
    QrFatConItsControle: TIntegerField;
    QrFatConItsGraGruX: TIntegerField;
    QrFatConItsPrecoO: TFloatField;
    QrFatConItsPrecoR: TFloatField;
    QrFatConItsQuantP: TFloatField;
    QrFatConItsQuantC: TFloatField;
    QrFatConItsQuantV: TFloatField;
    QrFatConItsValBru: TFloatField;
    QrFatConItsDescoP: TFloatField;
    QrFatConItsDescoV: TFloatField;
    QrFatConItsValLiq: TFloatField;
    QrFatConItsPrecoF: TFloatField;
    QrFatConItsValFat: TFloatField;
    QrFatConItsLk: TIntegerField;
    QrFatConItsDataCad: TDateField;
    QrFatConItsDataAlt: TDateField;
    QrFatConItsUserCad: TIntegerField;
    QrFatConItsUserAlt: TIntegerField;
    QrFatConItsAlterWeb: TSmallintField;
    QrFatConItsAtivo: TSmallintField;
    QrFatVenIts: TmySQLQuery;
    DsFatVenIts: TDataSource;
    QrFatVenCuz: TmySQLQuery;
    DsFatVenCuz: TDataSource;
    QrGraGruE: TmySQLQuery;
    DsGraGruE: TDataSource;
    QrFatDivRefCodigo: TIntegerField;
    QrFatDivRefIDCtrl: TIntegerField;
    QrFatDivRefStqCenCad: TIntegerField;
    QrFatDivRefReferencia: TWideStringField;
    QrFatDivRefGraGruX: TIntegerField;
    QrFatDivRefQtde: TFloatField;
    QrFatDivRefPrecoOri: TFloatField;
    QrFatDivRefPrecoPrz: TFloatField;
    QrFatDivRefPrecoAut: TFloatField;
    QrFatDivRefPrecoVen: TFloatField;
    QrFatDivRefValorTotBru: TFloatField;
    QrFatDivRefPerDescoVal: TFloatField;
    QrFatDivRefValorTotLiq: TFloatField;
    QrFatDivRefPerComissF: TFloatField;
    QrFatDivRefValComisLiqF: TFloatField;
    QrFatDivRefPerComissR: TFloatField;
    QrFatDivRefValComisLiqR: TFloatField;
    QrFatDivRefPerComissT: TFloatField;
    QrFatDivRefValComisBruT: TFloatField;
    QrFatDivRefValComisDesT: TFloatField;
    QrFatDivRefValComisLiqT: TFloatField;
    QrFatDivRefPerComissNiv: TWideStringField;
    QrFatDivRefVolCnta: TIntegerField;
    QrFatDivRefTipoCalc: TSmallintField;
    QrFatDivRefPecas: TFloatField;
    QrFatDivRefPeso: TFloatField;
    QrFatDivRefAreaM2: TFloatField;
    QrFatDivRefAreaP2: TFloatField;
    QrFatDivRefLk: TIntegerField;
    QrFatDivRefDataCad: TDateField;
    QrFatDivRefDataAlt: TDateField;
    QrFatDivRefUserCad: TIntegerField;
    QrFatDivRefUserAlt: TIntegerField;
    QrFatDivRefAlterWeb: TSmallintField;
    QrFatDivRefAtivo: TSmallintField;
    QrPQCliControle: TIntegerField;
    QrPQCliPQ: TIntegerField;
    QrPQCliCI: TIntegerField;
    QrPQCliMinEstq: TFloatField;
    QrPQCliEstSegur: TIntegerField;
    QrPQCliCustoPadrao: TFloatField;
    QrPQCliMoedaPadrao: TSmallintField;
    QrPQCliPeso: TFloatField;
    QrPQCliValor: TFloatField;
    QrPQCliCusto: TFloatField;
    QrPQCliLk: TIntegerField;
    QrPQCliDataCad: TDateField;
    QrPQCliDataAlt: TDateField;
    QrPQCliUserCad: TIntegerField;
    QrPQCliUserAlt: TIntegerField;
    QrPQCliAlterWeb: TSmallintField;
    QrPQCliAtivo: TSmallintField;
    QrEtqGeraItsCodigo: TIntegerField;
    QrEtqGeraItsGraGruX: TIntegerField;
    QrEtqGeraItsSequencia: TIntegerField;
    QrFatVenItsCodigo: TIntegerField;
    QrFatVenItsControle: TIntegerField;
    QrFatVenItsGraGruX: TIntegerField;
    QrFatVenItsPrecoO: TFloatField;
    QrFatVenItsPrecoR: TFloatField;
    QrFatVenItsQuantP: TFloatField;
    QrFatVenItsQuantC: TFloatField;
    QrFatVenItsQuantV: TFloatField;
    QrFatVenItsValBru: TFloatField;
    QrFatVenItsDescoP: TFloatField;
    QrFatVenItsDescoV: TFloatField;
    QrFatVenItsValLiq: TFloatField;
    QrFatVenItsPrecoF: TFloatField;
    QrFatVenItsMedidaC: TFloatField;
    QrFatVenItsMedidaL: TFloatField;
    QrFatVenItsMedidaA: TFloatField;
    QrFatVenItsMedidaE: TFloatField;
    QrFatVenItsPercCustom: TFloatField;
    QrFatVenItsCustomizad: TSmallintField;
    QrFatVenItsInfAdCuztm: TIntegerField;
    QrFatVenItsLk: TIntegerField;
    QrFatVenItsDataCad: TDateField;
    QrFatVenItsDataAlt: TDateField;
    QrFatVenItsUserCad: TIntegerField;
    QrFatVenItsUserAlt: TIntegerField;
    QrFatVenItsAlterWeb: TSmallintField;
    QrFatVenItsAtivo: TSmallintField;
    QrFatVenCuzControle: TIntegerField;
    QrFatVenCuzConta: TIntegerField;
    QrFatVenCuzMatPartCad: TIntegerField;
    QrFatVenCuzGraGruX: TIntegerField;
    QrFatVenCuzMedidaC: TFloatField;
    QrFatVenCuzMedidaL: TFloatField;
    QrFatVenCuzMedidaA: TFloatField;
    QrFatVenCuzMedidaE: TFloatField;
    QrFatVenCuzQuantP: TFloatField;
    QrFatVenCuzQuantX: TFloatField;
    QrFatVenCuzPrecoO: TFloatField;
    QrFatVenCuzPrecoR: TFloatField;
    QrFatVenCuzPrecoF: TFloatField;
    QrFatVenCuzValBru: TFloatField;
    QrFatVenCuzDescoP: TFloatField;
    QrFatVenCuzDescoV: TFloatField;
    QrFatVenCuzPerCustom: TFloatField;
    QrFatVenCuzValLiq: TFloatField;
    QrFatVenCuzTipDimens: TSmallintField;
    QrFatVenCuzLk: TIntegerField;
    QrFatVenCuzDataCad: TDateField;
    QrFatVenCuzDataAlt: TDateField;
    QrFatVenCuzUserCad: TIntegerField;
    QrFatVenCuzUserAlt: TIntegerField;
    QrFatVenCuzAlterWeb: TSmallintField;
    QrFatVenCuzAtivo: TSmallintField;
    QrGraGruVal: TmySQLQuery;
    DsGraGruVal: TDataSource;
    QrPediVdaIts: TmySQLQuery;
    DsPediVdaIts: TDataSource;
    QrPediVdaCuz: TmySQLQuery;
    DsPediVdaCuz: TDataSource;
    QrGraGruEcProd: TWideStringField;
    QrGraGruENivel1: TIntegerField;
    QrGraGruEFornece: TIntegerField;
    QrGraGruEEmbalagem: TIntegerField;
    QrGraGruEObservacao: TWideStringField;
    QrGraGruELk: TIntegerField;
    QrGraGruEDataCad: TDateField;
    QrGraGruEDataAlt: TDateField;
    QrGraGruEUserCad: TIntegerField;
    QrGraGruEUserAlt: TIntegerField;
    QrGraGruEAlterWeb: TSmallintField;
    QrGraGruEAtivo: TSmallintField;
    QrGraGruEGraGruX: TIntegerField;
    QrGraGruValControle: TIntegerField;
    QrGraGruValGraGruX: TIntegerField;
    QrGraGruValGraGru1: TIntegerField;
    QrGraGruValGraCusPrc: TIntegerField;
    QrGraGruValCustoPreco: TFloatField;
    QrGraGruValLk: TIntegerField;
    QrGraGruValDataCad: TDateField;
    QrGraGruValDataAlt: TDateField;
    QrGraGruValUserCad: TIntegerField;
    QrGraGruValUserAlt: TIntegerField;
    QrGraGruValAlterWeb: TSmallintField;
    QrGraGruValAtivo: TSmallintField;
    PageControl3: TPageControl;
    TabSheet16: TTabSheet;
    TabSheet17: TTabSheet;
    TabSheet18: TTabSheet;
    PageControl2: TPageControl;
    TabSheet11: TTabSheet;
    DBGrid6: TDBGrid;
    TabSheet12: TTabSheet;
    DBGrid5: TDBGrid;
    TabSheet13: TTabSheet;
    DBGrid7: TDBGrid;
    TabSheet14: TTabSheet;
    DBGrid8: TDBGrid;
    PageControl4: TPageControl;
    TabSheet9: TTabSheet;
    QrPediVdaItsCodigo: TIntegerField;
    QrPediVdaItsControle: TIntegerField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsPrecoO: TFloatField;
    QrPediVdaItsPrecoR: TFloatField;
    QrPediVdaItsQuantP: TFloatField;
    QrPediVdaItsQuantC: TFloatField;
    QrPediVdaItsQuantV: TFloatField;
    QrPediVdaItsValBru: TFloatField;
    QrPediVdaItsDescoP: TFloatField;
    QrPediVdaItsDescoV: TFloatField;
    QrPediVdaItsValLiq: TFloatField;
    QrPediVdaItsPrecoF: TFloatField;
    QrPediVdaItsMedidaC: TFloatField;
    QrPediVdaItsMedidaL: TFloatField;
    QrPediVdaItsMedidaA: TFloatField;
    QrPediVdaItsMedidaE: TFloatField;
    QrPediVdaItsPercCustom: TFloatField;
    QrPediVdaItsCustomizad: TSmallintField;
    QrPediVdaItsInfAdCuztm: TIntegerField;
    QrPediVdaItsLk: TIntegerField;
    QrPediVdaItsDataCad: TDateField;
    QrPediVdaItsDataAlt: TDateField;
    QrPediVdaItsUserCad: TIntegerField;
    QrPediVdaItsUserAlt: TIntegerField;
    QrPediVdaItsAlterWeb: TSmallintField;
    QrPediVdaItsAtivo: TSmallintField;
    QrPediVdaItsOrdem: TIntegerField;
    QrPediVdaItsReferencia: TWideStringField;
    QrPediVdaItsMulti: TIntegerField;
    DBGrid11: TDBGrid;
    QrStqBalIts: TmySQLQuery;
    DsStqBalIts: TDataSource;
    QrStqMovItsA: TmySQLQuery;
    DsStqMovItsA: TDataSource;
    QrStqMovItsB: TmySQLQuery;
    DsStqMovItsB: TDataSource;
    TabSheet10: TTabSheet;
    DBGrid12: TDBGrid;
    QrStqBalItsCodigo: TIntegerField;
    QrStqBalItsControle: TIntegerField;
    QrStqBalItsTipo: TSmallintField;
    QrStqBalItsDataHora: TDateTimeField;
    QrStqBalItsGraGruX: TIntegerField;
    QrStqBalItsQtdLei: TFloatField;
    QrStqBalItsQtdAnt: TFloatField;
    QrStqBalItsAlterWeb: TSmallintField;
    QrStqBalItsAtivo: TSmallintField;
    PageControl5: TPageControl;
    TabSheet15: TTabSheet;
    TabSheet19: TTabSheet;
    TabSheet20: TTabSheet;
    QrStqMovItsADataHora: TDateTimeField;
    QrStqMovItsAIDCtrl: TIntegerField;
    QrStqMovItsATipo: TIntegerField;
    QrStqMovItsAOriCodi: TIntegerField;
    QrStqMovItsAOriCtrl: TIntegerField;
    QrStqMovItsAOriCnta: TIntegerField;
    QrStqMovItsAOriPart: TIntegerField;
    QrStqMovItsAEmpresa: TIntegerField;
    QrStqMovItsAStqCenCad: TIntegerField;
    QrStqMovItsAGraGruX: TIntegerField;
    QrStqMovItsAQtde: TFloatField;
    QrStqMovItsAPecas: TFloatField;
    QrStqMovItsAPeso: TFloatField;
    QrStqMovItsAAlterWeb: TSmallintField;
    QrStqMovItsAAtivo: TSmallintField;
    QrStqMovItsAAreaM2: TFloatField;
    QrStqMovItsAAreaP2: TFloatField;
    QrStqMovItsAFatorClas: TFloatField;
    QrStqMovItsAQuemUsou: TIntegerField;
    QrStqMovItsARetorno: TSmallintField;
    QrStqMovItsAParTipo: TIntegerField;
    QrStqMovItsAParCodi: TIntegerField;
    QrStqMovItsADebCtrl: TIntegerField;
    QrStqMovItsASMIMultIns: TIntegerField;
    QrStqMovItsACustoAll: TFloatField;
    QrStqMovItsAValorAll: TFloatField;
    QrStqMovItsAGrupoBal: TIntegerField;
    QrStqMovItsABaixa: TSmallintField;
    QrStqMovItsAAntQtde: TFloatField;
    QrStqMovValA: TmySQLQuery;
    DsStqMovValA: TDataSource;
    QrStqMovItsBDataHora: TDateTimeField;
    QrStqMovItsBIDCtrl: TIntegerField;
    QrStqMovItsBTipo: TIntegerField;
    QrStqMovItsBOriCodi: TIntegerField;
    QrStqMovItsBOriCtrl: TIntegerField;
    QrStqMovItsBOriCnta: TIntegerField;
    QrStqMovItsBOriPart: TIntegerField;
    QrStqMovItsBEmpresa: TIntegerField;
    QrStqMovItsBStqCenCad: TIntegerField;
    QrStqMovItsBGraGruX: TIntegerField;
    QrStqMovItsBQtde: TFloatField;
    QrStqMovItsBPecas: TFloatField;
    QrStqMovItsBPeso: TFloatField;
    QrStqMovItsBAlterWeb: TSmallintField;
    QrStqMovItsBAtivo: TSmallintField;
    QrStqMovItsBAreaM2: TFloatField;
    QrStqMovItsBAreaP2: TFloatField;
    QrStqMovItsBFatorClas: TFloatField;
    QrStqMovItsBQuemUsou: TIntegerField;
    QrStqMovItsBRetorno: TSmallintField;
    QrStqMovItsBParTipo: TIntegerField;
    QrStqMovItsBParCodi: TIntegerField;
    QrStqMovItsBDebCtrl: TIntegerField;
    QrStqMovItsBSMIMultIns: TIntegerField;
    QrStqMovItsBCustoAll: TFloatField;
    QrStqMovItsBValorAll: TFloatField;
    QrStqMovItsBGrupoBal: TIntegerField;
    QrStqMovItsBBaixa: TSmallintField;
    QrStqMovItsBAntQtde: TFloatField;
    DBGrid13: TDBGrid;
    DBGrid14: TDBGrid;
    DBGrid15: TDBGrid;
    QrStqManIts: TmySQLQuery;
    DsStqManIts: TDataSource;
    QrTabePrcGrI: TmySQLQuery;
    DsTabePrcGrI: TDataSource;
    QrCMPPVaiIts: TmySQLQuery;
    DsCMPPVaiIts: TDataSource;
    QrCMPTInn: TmySQLQuery;
    DsCMPTInn: TDataSource;
    QrStqMovValAID: TIntegerField;
    QrStqMovValAIDCtrl: TIntegerField;
    QrStqMovValATipo: TSmallintField;
    QrStqMovValAOriCodi: TIntegerField;
    QrStqMovValAOriCtrl: TIntegerField;
    QrStqMovValAOriCnta: TIntegerField;
    QrStqMovValASeqInReduz: TIntegerField;
    QrStqMovValAEmpresa: TIntegerField;
    QrStqMovValAStqCenCad: TIntegerField;
    QrStqMovValAGraGruX: TIntegerField;
    QrStqMovValAQtde: TFloatField;
    QrStqMovValAPreco: TFloatField;
    QrStqMovValATotal: TFloatField;
    QrStqMovValACFOP: TWideStringField;
    QrStqMovValAInfAdCuztm: TIntegerField;
    QrStqMovValAPercCustom: TFloatField;
    QrStqMovValAMedidaC: TFloatField;
    QrStqMovValAMedidaL: TFloatField;
    QrStqMovValAMedidaA: TFloatField;
    QrStqMovValAMedidaE: TFloatField;
    QrStqMovValAMedOrdem: TIntegerField;
    QrStqMovValATipoNF: TSmallintField;
    QrStqMovValArefNFe: TWideStringField;
    QrStqMovValAmodNF: TSmallintField;
    QrStqMovValASerie: TIntegerField;
    QrStqMovValAnNF: TIntegerField;
    QrStqMovValASitDevolu: TSmallintField;
    QrStqMovValAServico: TIntegerField;
    QrStqMovValARefProd: TIntegerField;
    QrStqMovValACFOP_Contrib: TSmallintField;
    QrStqMovValACFOP_MesmaUF: TSmallintField;
    QrStqMovValACFOP_Proprio: TSmallintField;
    QrStqMovValAAlterWeb: TSmallintField;
    QrStqMovValAAtivo: TSmallintField;
    QrStqMovValAprod_indTot: TSmallintField;
    QrStqMovValACSOSN: TIntegerField;
    QrStqMovValApCredSN: TFloatField;
    QrStqMovValAFinaliCli: TSmallintField;
    DBGrid16: TDBGrid;
    QrStqManItsCodigo: TIntegerField;
    QrStqManItsControle: TIntegerField;
    QrStqManItsGraGruX: TIntegerField;
    QrStqManItsQtde: TFloatField;
    QrStqManItsPreco: TFloatField;
    QrStqManItsValor: TFloatField;
    QrStqManItsLk: TIntegerField;
    QrStqManItsDataCad: TDateField;
    QrStqManItsDataAlt: TDateField;
    QrStqManItsUserCad: TIntegerField;
    QrStqManItsUserAlt: TIntegerField;
    QrStqManItsAlterWeb: TSmallintField;
    QrStqManItsAtivo: TSmallintField;
    TabSheet21: TTabSheet;
    DBGrid17: TDBGrid;
    PageControl6: TPageControl;
    TabSheet22: TTabSheet;
    TabSheet23: TTabSheet;
    QrTabePrcGrIControle: TIntegerField;
    QrTabePrcGrIGraGruX: TIntegerField;
    QrTabePrcGrIGraGru1: TIntegerField;
    QrTabePrcGrITabePrcCab: TIntegerField;
    QrTabePrcGrIPreco: TFloatField;
    QrTabePrcGrILk: TIntegerField;
    QrTabePrcGrIDataCad: TDateField;
    QrTabePrcGrIDataAlt: TDateField;
    QrTabePrcGrIUserCad: TIntegerField;
    QrTabePrcGrIUserAlt: TIntegerField;
    QrTabePrcGrIAlterWeb: TSmallintField;
    QrTabePrcGrIAtivo: TSmallintField;
    DBGrid18: TDBGrid;
    DBGrid10: TDBGrid;
    TabSheet24: TTabSheet;
    QrCMPPVaiItsCodigo: TIntegerField;
    QrCMPPVaiItsControle: TIntegerField;
    QrCMPPVaiItsOutQtdkg: TFloatField;
    QrCMPPVaiItsOutQtdPc: TFloatField;
    QrCMPPVaiItsOutQtdM2: TFloatField;
    QrCMPPVaiItsOutQtdVal: TFloatField;
    QrCMPPVaiItsGerBxaEstq: TSmallintField;
    QrCMPPVaiItsGraGruX: TIntegerField;
    QrCMPPVaiItsLk: TIntegerField;
    QrCMPPVaiItsDataCad: TDateField;
    QrCMPPVaiItsDataAlt: TDateField;
    QrCMPPVaiItsUserCad: TIntegerField;
    QrCMPPVaiItsUserAlt: TIntegerField;
    QrCMPPVaiItsAlterWeb: TSmallintField;
    QrCMPPVaiItsAtivo: TSmallintField;
    QrCMPPVaiItsMPInn: TIntegerField;
    DBGrid19: TDBGrid;
    QrNFeMPIts: TmySQLQuery;
    DsNFeMPIts: TDataSource;
    QrCMPTInnCodigo: TIntegerField;
    QrCMPTInnEmpresa: TIntegerField;
    QrCMPTInnCliente: TIntegerField;
    QrCMPTInnDataE: TDateField;
    QrCMPTInnNFInn: TIntegerField;
    QrCMPTInnNFRef: TIntegerField;
    QrCMPTInnInnQtdkg: TFloatField;
    QrCMPTInnInnQtdPc: TFloatField;
    QrCMPTInnInnQtdM2: TFloatField;
    QrCMPTInnInnQtdVal: TFloatField;
    QrCMPTInnSdoQtdkg: TFloatField;
    QrCMPTInnSdoQtdPc: TFloatField;
    QrCMPTInnSdoQtdM2: TFloatField;
    QrCMPTInnSdoQtdVal: TFloatField;
    QrCMPTInnGraGruX: TIntegerField;
    QrCMPTInnTipoNF: TSmallintField;
    QrCMPTInnrefNFe: TWideStringField;
    QrCMPTInnmodNF: TSmallintField;
    QrCMPTInnSerie: TIntegerField;
    QrCMPTInnLk: TIntegerField;
    QrCMPTInnDataCad: TDateField;
    QrCMPTInnDataAlt: TDateField;
    QrCMPTInnUserCad: TIntegerField;
    QrCMPTInnUserAlt: TIntegerField;
    QrCMPTInnAlterWeb: TSmallintField;
    QrCMPTInnAtivo: TSmallintField;
    QrCMPTInnjaAtz: TSmallintField;
    TabSheet25: TTabSheet;
    DBGrid20: TDBGrid;
    QrNFeItsI: TmySQLQuery;
    DsNFeItsI: TDataSource;
    QrFormulasIts: TmySQLQuery;
    DsFormulasIts: TDataSource;
    mySQLQuery4: TmySQLQuery;
    DataSource4: TDataSource;
    QrNFeMPItsCodigo: TIntegerField;
    QrNFeMPItsControle: TIntegerField;
    QrNFeMPItsGraGruX: TIntegerField;
    QrNFeMPItsPecas: TFloatField;
    QrNFeMPItsPLE: TFloatField;
    QrNFeMPItsAreaM2: TFloatField;
    QrNFeMPItsAreaP2: TFloatField;
    QrNFeMPItsCMPValor: TFloatField;
    QrNFeMPItsGerBxaEstq: TIntegerField;
    QrNFeMPItsLk: TIntegerField;
    QrNFeMPItsDataCad: TDateField;
    QrNFeMPItsDataAlt: TDateField;
    QrNFeMPItsUserCad: TIntegerField;
    QrNFeMPItsUserAlt: TIntegerField;
    QrNFeMPItsAlterWeb: TSmallintField;
    QrNFeMPItsAtivo: TSmallintField;
    TabSheet26: TTabSheet;
    DBGrid21: TDBGrid;
    TabSheet27: TTabSheet;
    QrNFeItsIFatID: TIntegerField;
    QrNFeItsIFatNum: TIntegerField;
    QrNFeItsIEmpresa: TIntegerField;
    QrNFeItsInItem: TIntegerField;
    QrNFeItsIprod_cProd: TWideStringField;
    QrNFeItsIprod_cEAN: TWideStringField;
    QrNFeItsIprod_xProd: TWideStringField;
    QrNFeItsIprod_NCM: TWideStringField;
    QrNFeItsIprod_EXTIPI: TWideStringField;
    QrNFeItsIprod_genero: TSmallintField;
    QrNFeItsIprod_CFOP: TIntegerField;
    QrNFeItsIprod_uCom: TWideStringField;
    QrNFeItsIprod_qCom: TFloatField;
    QrNFeItsIprod_vUnCom: TFloatField;
    QrNFeItsIprod_vProd: TFloatField;
    QrNFeItsIprod_cEANTrib: TWideStringField;
    QrNFeItsIprod_uTrib: TWideStringField;
    QrNFeItsIprod_qTrib: TFloatField;
    QrNFeItsIprod_vUnTrib: TFloatField;
    QrNFeItsIprod_vFrete: TFloatField;
    QrNFeItsIprod_vSeg: TFloatField;
    QrNFeItsIprod_vDesc: TFloatField;
    QrNFeItsITem_IPI: TSmallintField;
    QrNFeItsI_Ativo_: TSmallintField;
    QrNFeItsIInfAdCuztm: TIntegerField;
    QrNFeItsIEhServico: TIntegerField;
    QrNFeItsILk: TIntegerField;
    QrNFeItsIDataCad: TDateField;
    QrNFeItsIDataAlt: TDateField;
    QrNFeItsIUserCad: TIntegerField;
    QrNFeItsIUserAlt: TIntegerField;
    QrNFeItsIAlterWeb: TSmallintField;
    QrNFeItsIAtivo: TSmallintField;
    QrNFeItsIICMSRec_pRedBC: TFloatField;
    QrNFeItsIICMSRec_vBC: TFloatField;
    QrNFeItsIICMSRec_pAliq: TFloatField;
    QrNFeItsIICMSRec_vICMS: TFloatField;
    QrNFeItsIIPIRec_pRedBC: TFloatField;
    QrNFeItsIIPIRec_vBC: TFloatField;
    QrNFeItsIIPIRec_pAliq: TFloatField;
    QrNFeItsIIPIRec_vIPI: TFloatField;
    QrNFeItsIPISRec_pRedBC: TFloatField;
    QrNFeItsIPISRec_vBC: TFloatField;
    QrNFeItsIPISRec_pAliq: TFloatField;
    QrNFeItsIPISRec_vPIS: TFloatField;
    QrNFeItsICOFINSRec_pRedBC: TFloatField;
    QrNFeItsICOFINSRec_vBC: TFloatField;
    QrNFeItsICOFINSRec_pAliq: TFloatField;
    QrNFeItsICOFINSRec_vCOFINS: TFloatField;
    QrNFeItsIMeuID: TIntegerField;
    QrNFeItsINivel1: TIntegerField;
    QrNFeItsIprod_vOutro: TFloatField;
    QrNFeItsIprod_indTot: TSmallintField;
    QrNFeItsIprod_xPed: TWideStringField;
    QrNFeItsIprod_nItemPed: TIntegerField;
    QrNFeItsIGraGruX: TIntegerField;
    QrNFeItsIUnidMedCom: TIntegerField;
    QrNFeItsIUnidMedTrib: TIntegerField;
    DBGrid22: TDBGrid;
    QrGraGruXGraGruX: TIntegerField;
    Panel4: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBText3: TDBText;
    DBText2: TDBText;
    DBText1: TDBText;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Panel5: TPanel;
    Label8: TLabel;
    DBEdGGXAtu: TDBEdit;
    Label9: TLabel;
    EdReduzido: TdmkEditCB;
    CBReduzido: TdmkDBLookupComboBox;
    QrReduzidos: TmySQLQuery;
    DsReduzidos: TDataSource;
    QrReduzidosControle: TIntegerField;
    QrReduzidosNO_PRD_TAM_COR: TWideStringField;
    PB1: TProgressBar;
    LaAviso: TLabel;
    DBGrid9: TDBGrid;
    QrGraGruDel: TmySQLQuery;
    QrGraGruDelExcluido: TIntegerField;
    QrGraGruDelReceptor: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtTransferir: TBitBtn;
    CkExcluir: TCheckBox;
    BtSaida: TBitBtn;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    BtAltera: TBitBtn;
    QrGraGruXCOD_ITEM: TWideStringField;
    Label10: TLabel;
    DBEdit13: TDBEdit;
    QrGraGruExProd: TWideStringField;
    QrGraGruENCM: TWideStringField;
    QrGraGruECFOP: TIntegerField;
    QrGraGruEuCom: TWideStringField;
    QrGraGruECFOP_Inn: TIntegerField;
    QrGraGruXNO_GGY: TWideStringField;
    Label11: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    QrGraGruXGraGruY: TIntegerField;
    SbGraGruY: TSpeedButton;
    PMGraGruY: TPopupMenu;
    ZeraocodigodoGrupodeEstoque1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGruXAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure BtTransferirClick(Sender: TObject);
    procedure QrGraGruXBeforeClose(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure SbGraGruYClick(Sender: TObject);
    procedure ZeraocodigodoGrupodeEstoque1Click(Sender: TObject);
  private
    { Private declarations }
    FGGXSel, FGGXNew(*, FGGXWer*): String;
    FItens: Integer;
    //
    procedure AlteraGGX(SQLType: TSQLType; TabIndex: Integer; Tabela: String;
              Altera: Boolean);
    procedure ExcluiGGX();
  public
    { Public declarations }
    FGraGruX: Integer;
    procedure ReopenGraGruX();
  end;

  var
  FmGraGruReduzido: TFmGraGruReduzido;

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, DmkDAC_PF, MyListas,
  MeuDBUses, UnGrade_Jan;

{$R *.DFM}

procedure TFmGraGruReduzido.AlteraGGX(SQLType: TSQLType; TabIndex: Integer;
  Tabela: String; Altera: Boolean);
var
  I: Integer;
  Qry: TmySQLQuery;
  Achou: Boolean;
begin
  Achou := False;
  PB1.Position := PB1.Position + 1;
  MyObjects.Informa(LaAviso, True, 'Verificando Tabela: ' + Tabela);
  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TmySQLQuery then
    begin
      Qry := TmySQLQuery(Components[I]);
      if Uppercase(Qry.Name) = Uppercase('Qr' + Tabela) then
      begin
        Achou := True;
        if Qry.RecordCount > 0 then
        begin
          FItens := FItens + Qry.RecordCount;
          if Altera then
          begin
            case TabIndex of
              // NFeItsI
              0: ;// Nada
              // PQCli
              1: // Nada!
              begin
                Dmod.QrUpd.SQL.Clear;
                Dmod.QrUpd.SQL.Add('DELETE FROM pqcli');
                Dmod.QrUpd.SQL.Add('WHERE Controle=' + FormatFloat('0', FGraGruX));
                Dmod.QrUpd.ExecSQL;
              end;
              2: // GraGruVal
              begin
                Dmod.QrUpd.SQL.Clear;
                Dmod.QrUpd.SQL.Add('DELETE FROM gragruval');
                Dmod.QrUpd.SQL.Add('WHERE gragrux=' + FormatFloat('0', FGraGruX));
                Dmod.QrUpd.ExecSQL;
              end;
              else
              begin
                case SQLType of
                  //stIns: ;
                  stUpd:
                  begin
                    MyObjects.Informa(LaAviso, True, 'Atualizando Tabela: ' + Tabela);
                    Dmod.QrUpd.SQL.Clear;
                    Dmod.QrUpd.SQL.Add('UPDATE ' + Tabela);
                    Dmod.QrUpd.SQL.Add(FGGXNew);
                    Dmod.QrUpd.SQL.Add(FGGXSel);
                    try
                      UMyMod.ExecutaQuery(Dmod.QrUpd)
                    except
                      ;
                    end;
                  end;
                  //stDel:
                  //stCpy: ;
                  //stLok: ;
                  //stUnd: ;
                  //stNil: ;
                end;
              end;
            end;
          end;
        end;
      end;
    end
  end;
  if not Achou then
    Geral.MB_Erro('ERRO. A tabela ' + Tabela + ' n�o foi localizada!');
end;

procedure TFmGraGruReduzido.BtAlteraClick(Sender: TObject);
begin
  if QrGraGruXGraGruX.Value <> 0 then
  begin
    Grade_Jan.MostraFormGraGruXEdit(QrGraGruXGraGruX.Value);
    ReopenGraGruX();
  end else
    Geral.MB_Aviso('Reduzido inv�lido');
end;

procedure TFmGraGruReduzido.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruReduzido.BtTransferirClick(Sender: TObject);
const
  MaxTab = 21;
  // N�o fazer no NFeItsI !!!!!!!!!!!!
  Tabelas: Array[0..MaxTab] of String = (
    'NFeItsI', 'PQCli', 'GraGruVal', 'CECTInn', 'NFeMPInn',           // 00..04
  // N�o fazer no NFeItsI !!!!!!!!!!!!
    'FatConIts','FatDivRef', 'FatVenIts', 'FatVenCuz', 'GraGruE',     // 05..09
  // N�o fazer no NFeItsI !!!!!!!!!!!!
    'EtqGeraIts', 'PediVdaIts', 'PediVdaCuz', 'StqBalIts',            // 10..13
  // N�o fazer no NFeItsI !!!!!!!!!!!!
    'StqMovItsA','StqMovItsB', 'StqMovValA', 'StqManIts',             // 14..17
  // N�o fazer no NFeItsI !!!!!!!!!!!!
    'TabePrcGrI','CMPPVaiIts', 'CMPTInn', 'NFeMPIts');                // 18..21
  // N�o fazer no NFeItsI !!!!!!!!!!!!
var
  Reduzido, I, RecAntes: Integer;
  GraGruX: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  if QrNFeItsI.RecordCount > 0 then
  begin
    Geral.MB_Erro('Transfer�ncia de lan�amentos cancelada!' + slineBreak +
    'Existem ' + FormatFloat('0', QrNFeItsI.RecordCount) +
    ' lan�amentos em NF-e!');
    //
    Exit;
  end;
  Reduzido := EdReduzido.ValueVariant;
  if MyObjects.FIC(Reduzido=0, EdReduzido, 'Informe o novo reduzido!') then
    Exit;
  FGGXNew := 'SET GraGruX=' + FormatFloat('0', Reduzido);
  FGGXSel := 'WHERE GraGruX=' + FormatFloat('0', FGraGruX);
  PB1.Position := 0;
  PB1.Max := MaxTab;

  for I := 0 to MaxTab do
    AlteraGGX(stUpd, I, Tabelas[I], True);
  RecAntes := FItens;
  FItens := 0;
  PB1.Position := 0;
  //
  ReopenGraGruX();
  //
  for I := 0 to MaxTab do
    AlteraGGX(stUpd, I, Tabelas[I], False);
  PB1.Position := 0;
  MyObjects.Informa(LaAviso, False, '...');
  Geral.MB_Aviso(FormatFloat('0', RecAntes - FItens) + ' lan�amentos de ' +
    FormatFloat('0', RecAntes) + ' foram alterados ou exclu�dos. ' + FormatFloat('0',
    FItens) + ' n�o foram alterados nem exclu�dos!');
  if CkExcluir.Checked then
  begin
    GraGruX := FormatFloat('0', FGraGruX);
    if FItens > 0 then
      Geral.MB_Aviso('O reduzido ' + GraGruX +
      ' n�o poder� ser exclu�do!')
    else
    begin
      ExcluiGGX();
      //
      ReopenGraGruX();
      if QrGraGruX.RecordCount > 0 then
        Geral.MB_Erro('O reduzido ' + GraGruX + ' n�o foi exclu�do!')
    end;
  end;
end;

procedure TFmGraGruReduzido.ExcluiGGX();
var
  Campo: String;
  Excluido, Receptor: String;
  //
  procedure ExcluirRegistro(Tabela: String);
  begin
    MyObjects.Informa(LaAviso, True, 'Adicionando � tabela de exclu�dos.');
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrudel', False, [
    'Excluido', 'Receptor'], [
    ], [
    Excluido, Receptor], [
    ], True) then
    begin
      MyObjects.Informa(LaAviso, True, 'Excluindo da Tabela: ' + Tabela);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + Tabela);
      Dmod.QrUpd.SQL.Add('WHERE ' + Campo + '=' +  Excluido);
      try
        UMyMod.ExecutaQuery(Dmod.QrUpd)
      except
        ;
      end;
    end;
  end;
begin
  Receptor    := FormatFloat('0', EdReduzido.ValueVariant);
  Excluido := FormatFloat('0', FGraGruX);
  if MyObjects.FIC(Receptor = '0', EdReduzido, 'Informe o novo reduzido!') then
    Exit;
  Campo := 'Controle';
  ExcluirRegistro('GraGruX');
end;

procedure TFmGraGruReduzido.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if (QrGraGruX.State <> dsInactive) and (QrGraGruX.RecordCount = 0) then
    Close;
end;

procedure TFmGraGruReduzido.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrReduzidos, Dmod.MyDB);
end;

procedure TFmGraGruReduzido.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruReduzido.QrGraGruXAfterOpen(DataSet: TDataSet);
const
  Tit = 'QrGraGruXAfterOpen()';
var
  Ctrl: Integer;
  cProd: String;
begin
  if QrGraGruX.RecordCount = 0 then
  begin
    QrGraGruDel.Close;
    QrGraGruDel.Params[0].AsInteger := FGraGruX;
    UnDmkDAC_PF.AbreQuery(QrGraGruDel, Dmod.MyDB);
    if QrGraGruDel.RecordCount > 0 then
      Geral.MB_Info('O reduzido ' + FormatFloat('0', FGraGruX) +
      ' foi exclu�do!' + slineBreak +
      'Seu movimento foi transferido para o reduzido: ' +
      FormatFloat('0', QrGraGruDelReceptor.Value))
    else
      Geral.MB_Aviso('O reduzido ' + FormatFloat('0', FGraGruX) +
      ' n�o foi localizado!');
    Close;
  end else
  begin
    BtTransferir.Enabled := True;
    Ctrl := FGraGruX;
    cProd := FormatFloat('0', Ctrl);
    //
    case TdmkAppID(CO_DMKID_APP) of
      dmkappB_L_U_E_D_E_R_M:
      begin
        UMyMod.ReabreQuery(QrCECTInn, Dmod.MyDB, [Ctrl], Tit);
        UMyMod.ReabreQuery(QrNFeMPInn, Dmod.MyDB, [Ctrl], Tit);
        UMyMod.ReabreQuery(QrPQCli, Dmod.MyDB, [Ctrl], Tit);
        UMyMod.ReabreQuery(QrCMPPVaiIts, Dmod.MyDB, [Ctrl], Tit);
        UMyMod.ReabreQuery(QrCMPTInn, Dmod.MyDB, [Ctrl], Tit);
        UMyMod.ReabreQuery(QrFormulasIts, Dmod.MyDB, [cProd], Tit);
        UMyMod.ReabreQuery(QrNFeMPIts, Dmod.MyDB, [Ctrl], Tit);
      end;
    end;
    UMyMod.ReabreQuery(QrEtqGeraIts, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrFatConIts, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrFatDivRef, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrFatVenIts, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrFatVenCuz, Dmod.MyDB, [Ctrl], Tit);
    //UMyMod.ReabreQuery(QrGraGruE, Dmod.MyDB, [Ctrl], Tit);  Mudou! Ver o que fazer!
    UMyMod.ReabreQuery(QrGraGruVal, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrPediVdaIts, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrPediVdaCuz, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrStqBalIts, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrStqMovItsA, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrStqMovItsB, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrStqMovValA, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrStqManIts, Dmod.MyDB, [Ctrl], Tit);
    UMyMod.ReabreQuery(QrTabePrcGrI, Dmod.MyDB, [Ctrl], Tit);
{$IfNDef semNFe_v0000}
    UMyMod.ReabreQuery(QrNFeItsI, Dmod.MyDB, [cProd], Tit);
{$EndIf}
  end;
end;

procedure TFmGraGruReduzido.QrGraGruXBeforeClose(DataSet: TDataSet);
begin
  BtTransferir.Enabled := False;
end;

procedure TFmGraGruReduzido.ReopenGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT pgt.TitNiv3, pgt.TitNiv2, pgt.TitNiv1, ',
  'CONCAT(gg1.Nome, IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "",  ',
  'CONCAT(" ", gti.Nome)), IF(gcc.PrintCor=0,"",  ',
  'CONCAT(" ", gcc.Nome))) NO_PRD_TAM_COR, gg1.Nivel1,  ',
  'gg1.CodUsu CU_GG1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, ',
  'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, gg1.Nivel2, ',
  'gg2.CodUsu CU_GG2, gg2.Nome NO_GG2, gg3.CodUsu CU_GG3, ',
  'gg1.Nivel3, gg3.Nome NO_GG3, pgt.CodUsu CU_PGT, ',
  'ggx.Controle GraGruX, ggx.COD_ITEM, ',
  'ggx.GraGruY, ggy.Nome NO_GGY ',
  'FROM gragrux ggx  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg1.Nivel2=gg2.Nivel2 ',
  'LEFT JOIN gragru3    gg3 ON gg1.Nivel3=gg3.Nivel3 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'WHERE ggx.Controle=' + Geral.FF0(FGraGruX),
  '']);
end;

procedure TFmGraGruReduzido.SbGraGruYClick(Sender: TObject);
begin
  MyObjects.MostraPopUpdeBotao(PMGraGRuY, SbGraGruY);
end;

procedure TFmGraGruReduzido.SbQueryClick(Sender: TObject);
begin
  FmMeuDBUses.PesquisaNome('gragrux', 'Nome', 'Controle');
  if VAR_CADASTRO <> 0 then
  begin
    FGraGruX := VAR_CADASTRO;
    ReopenGraGruX();
  end;
end;

procedure TFmGraGruReduzido.ZeraocodigodoGrupodeEstoque1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if Geral.MB_Pergunta('Confirma a retirada deste reduzido do grupo de estoque '
  + Geral.FF0(QrGraGruXGraGruY.Value) + '?') = ID_YES then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
    //
    Controle := QrGraGruXGraGruX.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
    'GraGruY'], [
    'Controle'], [
    0], [
    Controle], True) then
      ReopenGraGruX();
  end;
end;

// Parei Aqui
// colocar receitas novas

end.
