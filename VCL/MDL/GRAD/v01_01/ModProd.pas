unit ModProd;

interface

uses
  Windows, Forms, Messages, SysUtils, Classes, DB, mySQLDbTables, Grids,
  Controls, StdCtrls, ComCtrls, ExtCtrls, dmkGeral, dmkLabel, dmkEdit, Dialogs,
  frxClass, frxDBSet, dmkEditCB, dmkDBLookupComboBox, Variants, UnDmkProcFunc,
  DmkDAC_PF, dmkImage, UnDmkEnums, UMySQLDB, Vcl.Graphics, Vcl.Menus;

type
  TStqMovImped = (tsmiBalanco=0, tsmiFaturamen=1, tsmiMovimenMan=2, tsmiClassifi=3);
  TChamouGraGruNew = (cggnFmGraGruN, cggnFmPQ, cggnFmGraGXPatr);
  TTipoListaPreco = (tpTabePrc, tpGraCusPrc);
  TTipoEstoque = (testqNenhum, testqProprio, testqSINTEGRA);
  TTipoPesqProd = (tpRef, tpGGX);
  TDmProd = class(TDataModule)
    QrLocta_: TmySQLQuery;
    QrLocta_Conta: TIntegerField;
    QrProdutos: TmySQLQuery;
    QrGradesCors: TMySQLQuery;
    QrGradesTams: TMySQLQuery;
    QrEstq_: TmySQLQuery;
    QrEstq_EstqQ: TFloatField;
    QrEstq_EstqV: TFloatField;
    QrPeriodoBal_: TmySQLQuery;
    QrPeriodoBal_Periodo: TIntegerField;
    QrCustoProd_: TmySQLQuery;
    QrCustoProd_Custo: TFloatField;
    QrComisPrd_: TmySQLQuery;
    QrLocProd_: TmySQLQuery;
    QrLocProd_PrecoV: TFloatField;
    QrGradesTamsTam: TAutoIncField;
    QrGradesTamsNOMETAM: TWideStringField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsNOMECOR: TWideStringField;
    QrProdutosCor: TIntegerField;
    QrProdutosTam: TIntegerField;
    QrProdutosControle: TIntegerField;
    QrProdutosAtivo: TSmallintField;
    QrGraCorCad: TmySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadCodUsu: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    QrAtribIts: TmySQLQuery;
    QrAtribItsNOMEATRIBCAD: TWideStringField;
    QrAtribItsControle: TIntegerField;
    QrAtribItsCodUsu: TIntegerField;
    QrAtribItsNOMEATRIBITS: TWideStringField;
    QrAtribItsCodigo: TIntegerField;
    QrGraAtrIts: TmySQLQuery;
    QrGraAtrItsControle: TIntegerField;
    QrGraGruVal: TmySQLQuery;
    QrGraGruValGraGruC: TIntegerField;
    QrGraGruValGraTamI: TIntegerField;
    QrGraGruValCustoPreco: TFloatField;
    QrLocGGV: TmySQLQuery;
    QrLocGGVControle: TIntegerField;
    QrPrdGrupJan: TmySQLQuery;
    QrPrdGrupJanJanela: TWideStringField;
    QrPrdGrupJanOrdem: TIntegerField;
    QrLocTPGI: TmySQLQuery;
    QrLocTPGIControle: TIntegerField;
    QrTabePrcGrI: TmySQLQuery;
    QrTabePrcGrIGraGruC: TIntegerField;
    QrTabePrcGrIGraTamI: TIntegerField;
    QrTabePrcGrIPreco: TFloatField;
    QrMaxTams: TmySQLQuery;
    QrMaxTamsItens: TLargeintField;
    QrItsLotEtq: TmySQLQuery;
    QrItsLotEtqITENS: TLargeintField;
    QrItsLotEtqGraGruC: TIntegerField;
    QrItsLotEtqGraTamI: TIntegerField;
    QrEtqPrinCmd: TmySQLQuery;
    QrEtqPrinCmdCodigo: TIntegerField;
    QrEtqPrinCmdTipo: TSmallintField;
    QrEtqPrinCmdOrdem: TIntegerField;
    QrEtqPrinCmdControle: TIntegerField;
    QrEtqPrinCmdConfig: TWideStringField;
    QrEtqGeraIts: TmySQLQuery;
    QrEtqGeraItsREDUZIDO: TIntegerField;
    QrEtqGeraItsSequencia: TIntegerField;
    QrEtqGeraItsGraGruC: TIntegerField;
    QrEtqGeraItsNIVEL1: TIntegerField;
    QrEtqGeraItsCOD_TAM: TIntegerField;
    QrEtqGeraItsNOMETAM: TWideStringField;
    QrEtqGeraItsCOD_GRADE: TIntegerField;
    QrEtqGeraItsNOMEGRADE: TWideStringField;
    QrEtqGeraItsCOD_COR: TIntegerField;
    QrEtqGeraItsNOMECOR: TWideStringField;
    QrEtqGeraItsNOMENIVEL1: TWideStringField;
    QrQtdItsPrz: TmySQLQuery;
    QrQtdItsPrzItens: TLargeintField;
    QrLocPrz: TmySQLQuery;
    QrLocPrzVariacao: TFloatField;
    QrPrcGru: TmySQLQuery;
    QrPrcGruPreco: TFloatField;
    QrPediVdaIts: TmySQLQuery;
    QrPediVdaItsGraTamI: TIntegerField;
    QrPediVdaItsGraGruC: TIntegerField;
    QrPediVdaItsGraGru1: TIntegerField;
    QrPediVdaItsCodigo: TIntegerField;
    QrPediVdaItsControle: TIntegerField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsPrecoO: TFloatField;
    QrPediVdaItsPrecoR: TFloatField;
    QrPediVdaItsQuantP: TFloatField;
    QrPediVdaItsValBru: TFloatField;
    QrPediVdaItsDescoP: TFloatField;
    QrPediVdaItsValLiq: TFloatField;
    QrPediVdaItsDescoV: TFloatField;
    QrStqPrdGer: TmySQLQuery;
    QrStqPrdGerGraGruX: TIntegerField;
    QrStqPrdGerQTDE: TFloatField;
    QrStqPrdGerGraTamI: TIntegerField;
    QrStqPrdGerGraGruC: TIntegerField;
    QrStqMovItsA: TmySQLQuery;
    QrStqMovItsAQtde: TFloatField;
    QrStqMovItsAGraGruC: TIntegerField;
    QrStqMovItsAGraTamI: TIntegerField;
    QrStqMovItsB: TmySQLQuery;
    QrStqMovItsBQtde: TFloatField;
    QrStqMovItsBGraGruC: TIntegerField;
    QrStqMovItsBGraTamI: TIntegerField;
    QrSdosPed: TmySQLQuery;
    QrSdosPedQuantF: TFloatField;
    QrSdosPedGraGruC: TIntegerField;
    QrSdosPedGraTamI: TIntegerField;
    QrOpenX: TmySQLQuery;
    QrOpenXCodigo: TIntegerField;
    QrOpenXNome: TWideStringField;
    DsOpenX: TDataSource;
    QrTabePrcGrX: TmySQLQuery;
    QrTabePrcGrXPreco: TFloatField;
    QrPrcGrX: TmySQLQuery;
    QrPrcGrXPreco: TFloatField;
    QrGraGrXVal: TmySQLQuery;
    QrGraGrXValCustoPreco: TFloatField;
    QrTabePrcGrU: TmySQLQuery;
    QrTabePrcGrUCodigo: TIntegerField;
    QrTabePrcGrUNivel1: TIntegerField;
    QrTabePrcGrUPreco: TFloatField;
    QrGradesCor2: TmySQLQuery;
    QrGradesTam2: TmySQLQuery;
    QrGradesTam2Tam: TAutoIncField;
    QrGradesTam2NOMETAM: TWideStringField;
    QrGradesCor2Cor: TIntegerField;
    QrGradesCor2NOMECOR: TWideStringField;
    QrNeed1: TmySQLQuery;
    QrNeed1Controle: TIntegerField;
    QrNeed1GraGruC: TIntegerField;
    QrNeed1GraTamI: TIntegerField;
    QrNeed1GraGru1: TIntegerField;
    QrNeed1PrdGrupTip: TIntegerField;
    QrNeed2: TmySQLQuery;
    QrNeed3_: TmySQLQuery;
    QrGraCorIdx: TmySQLQuery;
    QrGraCorIdxCodigo: TIntegerField;
    QrGraCorIdxGrupo: TWideStringField;
    QrGraCorIdxGraCorCad: TIntegerField;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpCodigo: TIntegerField;
    QrParamsEmpMoeda: TIntegerField;
    QrParamsEmpSituacao: TIntegerField;
    QrParamsEmpRegrFiscal: TIntegerField;
    QrParamsEmpFretePor: TSmallintField;
    QrParamsEmpTipMediaDD: TSmallintField;
    QrParamsEmpFatSemPrcL: TSmallintField;
    QrParamsEmpBalQtdItem: TFloatField;
    QrParamsEmpAssociada: TIntegerField;
    QrParamsEmpFatSemEstq: TSmallintField;
    QrParamsEmpAssocModNF: TIntegerField;
    QrParamsEmpCtaProdVen: TIntegerField;
    QrParamsEmpfaturasep: TWideStringField;
    QrParamsEmpfaturaseq: TSmallintField;
    QrParamsEmpFaturaDta: TSmallintField;
    QrParamsEmpTxtProdVen: TWideStringField;
    QrParamsEmpLogo3x1: TWideStringField;
    QrParamsEmpTipCalcJuro: TSmallintField;
    QrParamsEmpSimplesFed: TSmallintField;
    QrParamsEmpSimplesEst: TSmallintField;
    QrParamsEmpDirNFeGer: TWideStringField;
    QrParamsEmpDirNFeAss: TWideStringField;
    QrParamsEmpDirEnvLot: TWideStringField;
    QrParamsEmpDirRec: TWideStringField;
    QrParamsEmpDirPedRec: TWideStringField;
    QrParamsEmpDirProRec: TWideStringField;
    QrParamsEmpDirDen: TWideStringField;
    QrParamsEmpDirPedCan: TWideStringField;
    QrParamsEmpDirCan: TWideStringField;
    QrParamsEmpDirPedInu: TWideStringField;
    QrParamsEmpDirInu: TWideStringField;
    QrParamsEmpDirPedSit: TWideStringField;
    QrParamsEmpDirSit: TWideStringField;
    QrParamsEmpDirPedSta: TWideStringField;
    QrParamsEmpDirSta: TWideStringField;
    QrParamsEmpUF_WebServ: TWideStringField;
    QrParamsEmpSiglaCustm: TWideStringField;
    QrParamsEmpInfoPerCuz: TSmallintField;
    QrParamsEmpPathLogoNF: TWideStringField;
    QrParamsEmpCtaServico: TIntegerField;
    QrParamsEmpTxtServico: TWideStringField;
    QrParamsEmpDupServico: TWideStringField;
    QrParamsEmpDupProdVen: TWideStringField;
    QrParamsEmpPedVdaMudLista: TSmallintField;
    QrParamsEmpPedVdaMudPrazo: TSmallintField;
    QrFatConIts: TmySQLQuery;
    QrFatConItsGraTamI: TIntegerField;
    QrFatConItsGraGruC: TIntegerField;
    QrFatConItsGraGru1: TIntegerField;
    QrFatConItsQuantP: TFloatField;
    QrFatConItsValLiq: TFloatField;
    QrFatConItsDescoP: TFloatField;
    QrFatConItsPrecoR: TFloatField;
    QrVendidos: TmySQLQuery;
    QrVendidosQuantV: TFloatField;
    QrVendidosGraGruC: TIntegerField;
    QrVendidosGraTamI: TIntegerField;
    QrFatRetIts: TmySQLQuery;
    QrFatRetItsQuantV: TFloatField;
    QrFatRetItsValFat: TFloatField;
    QrFatRetItsDescoP: TFloatField;
    QrFatRetItsPrecoR: TFloatField;
    QrFatRetItsPrecoF: TFloatField;
    QrFatRetItsGraGruC: TIntegerField;
    QrFatRetItsGraTamI: TIntegerField;
    QrBxaA: TmySQLQuery;
    QrBxaB: TmySQLQuery;
    QrBxaAQtde: TFloatField;
    QrBxaAPecas: TFloatField;
    QrBxaAPeso: TFloatField;
    QrBxaAAreaM2: TFloatField;
    QrBxaBQtde: TFloatField;
    QrBxaBPecas: TFloatField;
    QrBxaBPeso: TFloatField;
    QrBxaAAreaP2: TFloatField;
    QrBxaBAreaM2: TFloatField;
    QrBxaBAreaP2: TFloatField;
    QrFatVenIts: TmySQLQuery;
    QrFatVenItsGraTamI: TIntegerField;
    QrFatVenItsGraGruC: TIntegerField;
    QrFatVenItsGraGru1: TIntegerField;
    QrFatVenItsCodigo: TIntegerField;
    QrFatVenItsControle: TIntegerField;
    QrFatVenItsGraGruX: TIntegerField;
    QrFatVenItsPrecoO: TFloatField;
    QrFatVenItsPrecoR: TFloatField;
    QrFatVenItsQuantP: TFloatField;
    QrFatVenItsQuantC: TFloatField;
    QrFatVenItsQuantV: TFloatField;
    QrFatVenItsValBru: TFloatField;
    QrFatVenItsDescoP: TFloatField;
    QrFatVenItsDescoV: TFloatField;
    QrFatVenItsValLiq: TFloatField;
    QrFatVenItsPrecoF: TFloatField;
    QrFatVenItsMedidaC: TFloatField;
    QrFatVenItsMedidaL: TFloatField;
    QrFatVenItsMedidaA: TFloatField;
    QrFatVenItsMedidaE: TFloatField;
    QrFatVenItsPercCustom: TFloatField;
    QrFatVenItsCustomizad: TSmallintField;
    QrFatVenItsInfAdCuztm: TIntegerField;
    DsPGT_SCC: TDataSource;
    QrPGT_SCC: TmySQLQuery;
    QrPGT_SCCPrdGrupTip: TIntegerField;
    QrPGT_SCCStqCenCad: TIntegerField;
    QrPGT_SCCLstPrcFisc: TSmallintField;
    QrPGT_SCCEmpresa: TLargeintField;
    QrPGT_SCCEntiSitio: TLargeintField;
    QrAbertura2a: TmySQLQuery;
    QrAbertura2aAbertura: TDateTimeField;
    QrAbertura2aGrupoBal: TIntegerField;
    QrAbertura2b: TmySQLQuery;
    QrAbertura2bAbertura: TDateTimeField;
    QrSMIC2: TmySQLQuery;
    QrSMIC2Nivel1: TIntegerField;
    QrSMIC2NO_PRD: TWideStringField;
    QrSMIC2PrdGrupTip: TIntegerField;
    QrSMIC2UnidMed: TIntegerField;
    QrSMIC2NCM: TWideStringField;
    QrSMIC2NO_PGT: TWideStringField;
    QrSMIC2SIGLA: TWideStringField;
    QrSMIC2NO_TAM: TWideStringField;
    QrSMIC2NO_COR: TWideStringField;
    QrSMIC2GraCorCad: TIntegerField;
    QrSMIC2GraGruC: TIntegerField;
    QrSMIC2GraGru1: TIntegerField;
    QrSMIC2GraTamI: TIntegerField;
    QrSMIC2Empresa: TIntegerField;
    QrSMIC2StqCenCad: TIntegerField;
    QrSMIC2Qtde: TFloatField;
    QrSMIC2SitProd: TSmallintField;
    QrSMIC2ENS_CNPJ: TWideStringField;
    QrSMIC2ENS_IE: TWideStringField;
    QrSMIC2ENS_NO_UF: TWideStringField;
    QrSMIC2EMP_CNPJ: TWideStringField;
    QrSMIC2EMP_IE: TWideStringField;
    QrSMIC2EMP_NO_UF: TWideStringField;
    QrSMIC2PECAS: TFloatField;
    QrSMIC2PESO: TFloatField;
    QrSMIC2AREAM2: TFloatField;
    QrSMIC2AREAP2: TFloatField;
    QrSMIC2PrcCusUni: TFloatField;
    QrSMIC2GraGruX: TIntegerField;
    QrSMIC2ValorTot: TFloatField;
    QrSMIC2My_Idx: TIntegerField;
    QrSMIC2PrintTam: TSmallintField;
    QrSMIC2PrintCor: TSmallintField;
    QrSMIC2EntiSitio: TIntegerField;
    QrSMIC2GraCusPrc: TIntegerField;
    QrSMIC2Exportado: TLargeintField;
    DsSMIC2: TDataSource;
    frxDs: TfrxDBDataset;
    QrExport2: TmySQLQuery;
    QrExport2My_Idx: TIntegerField;
    QrExport2Nivel1: TIntegerField;
    QrExport2NO_PRD: TWideStringField;
    QrExport2PrdGrupTip: TIntegerField;
    QrExport2UnidMed: TIntegerField;
    QrExport2NCM: TWideStringField;
    QrExport2NO_PGT: TWideStringField;
    QrExport2SIGLA: TWideStringField;
    QrExport2PrintTam: TSmallintField;
    QrExport2NO_TAM: TWideStringField;
    QrExport2PrintCor: TSmallintField;
    QrExport2NO_COR: TWideStringField;
    QrExport2GraCorCad: TIntegerField;
    QrExport2GraGruC: TIntegerField;
    QrExport2GraGru1: TIntegerField;
    QrExport2GraTamI: TIntegerField;
    QrExport2SitProd: TSmallintField;
    QrExport2EntiSitio: TIntegerField;
    QrExport2GraGruX: TIntegerField;
    QrExport2Empresa: TIntegerField;
    QrExport2StqCenCad: TIntegerField;
    QrExport2QTDE: TFloatField;
    QrExport2PECAS: TFloatField;
    QrExport2PESO: TFloatField;
    QrExport2AREAM2: TFloatField;
    QrExport2AREAP2: TFloatField;
    QrExport2GraCusPrc: TIntegerField;
    QrExport2PrcCusUni: TFloatField;
    QrExport2ValorTot: TFloatField;
    QrExport2Exportado: TLargeintField;
    DsExport2: TDataSource;
    frxDsExporta2: TfrxDBDataset;
    QrListErr1: TmySQLQuery;
    QrListErr1My_Idx: TIntegerField;
    QrListErr1Codigo: TWideStringField;
    QrListErr1Texto1: TWideStringField;
    DsListErr1: TDataSource;
    frxDsListErr1: TfrxDBDataset;
    QrMvt: TmySQLQuery;
    QrMvtTipoCalc: TSmallintField;
    QrStqPrdEmp: TmySQLQuery;
    QrStqPrdEmpQTDE: TFloatField;
    QrGraOptEnt: TmySQLQuery;
    QrGraOptEntBUCTipoPreco: TSmallintField;
    QrGraOptEntBUCGraCusPrc: TIntegerField;
    QrGraGruDel: TmySQLQuery;
    QrGraGruDelReceptor: TIntegerField;
    QrGraGruDelControle: TIntegerField;
    QrCTB: TmySQLQuery;
    QrCTBGenero: TIntegerField;
    QrGG1: TmySQLQuery;
    QrGG1SIGLA: TWideStringField;
    QrGGX: TmySQLQuery;
    QrGGXControle: TIntegerField;
    QrGG1UnidMed: TIntegerField;
    QrSINTEGR_50: TmySQLQuery;
    QrSINTEGR_50ImporExpor: TSmallintField;
    QrSINTEGR_50AnoMes: TIntegerField;
    QrSINTEGR_50Empresa: TIntegerField;
    QrSINTEGR_50LinArq: TIntegerField;
    QrSINTEGR_50TIPO: TSmallintField;
    QrSINTEGR_50CNPJ: TWideStringField;
    QrSINTEGR_50IE: TWideStringField;
    QrSINTEGR_50EMISSAO: TDateField;
    QrSINTEGR_50UF: TWideStringField;
    QrSINTEGR_50MODELO: TWideStringField;
    QrSINTEGR_50SERIE: TWideStringField;
    QrSINTEGR_50NUMNF: TWideStringField;
    QrSINTEGR_50CFOP: TWideStringField;
    QrSINTEGR_50EMITENTE: TWideStringField;
    QrSINTEGR_50ValorNF: TFloatField;
    QrSINTEGR_50BCICMS: TFloatField;
    QrSINTEGR_50ValICMS: TFloatField;
    QrSINTEGR_50ValIsento: TFloatField;
    QrSINTEGR_50ValNCICMS: TFloatField;
    QrSINTEGR_50AliqICMS: TFloatField;
    QrSINTEGR_50Terceiro: TIntegerField;
    QrSINTEGR_50Tabela: TSmallintField;
    QrSINTEGR_50FatID: TIntegerField;
    QrSINTEGR_50FatNum: TIntegerField;
    QrSINTEGR_50ACHOU_TXT: TWideStringField;
    QrSINTEGR_50ConfVal: TSmallintField;
    QrSINTEGR_50Situacao: TWideStringField;
    QrGraGruVinc: TmySQLQuery;
    QrGraGruVincGraGruX: TIntegerField;
    QrGraGruVincGraGru1: TIntegerField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrLocRef: TmySQLQuery;
    QrLocRefControle: TIntegerField;
    QrLocRefNO_PRD_TAM_COR: TWideStringField;
    QrLocRefGraTamcad: TIntegerField;
    QrLocRefReferencia: TWideStringField;
    QrLocRefPerComissF: TFloatField;
    QrLocRefPerComissR: TFloatField;
    QrLocRefNivel1: TIntegerField;
    QrLocRefNivel2: TIntegerField;
    QrLocRefNivel3: TIntegerField;
    QrLocRefPrdgrupTip: TIntegerField;
    DsLocRef: TDataSource;
    QrPrcNiv: TmySQLQuery;
    QrPrcNivNivel: TIntegerField;
    QrPrcNivPreco: TFloatField;
    QrEstqCen: TmySQLQuery;
    QrEstqCenQtde: TFloatField;
    QrOpcoesGrad: TmySQLQuery;
    QrPesq2: TmySQLQuery;
    QrPesq2Sigla: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    QrY: TmySQLQuery;
    QrYFatNum: TIntegerField;
    QrYFatID: TIntegerField;
    QrYEmpresa: TIntegerField;
    QrYIts: TmySQLQuery;
    QrYItsControle: TIntegerField;
    QrYLct: TmySQLQuery;
    QrYLctControle: TIntegerField;
    QrYLctSub: TSmallintField;
    QrYLctFatParcela: TIntegerField;
    QrNfeX: TmySQLQuery;
    QrNfeXFatID: TIntegerField;
    QrNfeXFatNum: TIntegerField;
    QrNfeXEmpresa: TIntegerField;
    QrEAN13: TmySQLQuery;
    QrEAN13EAN13: TWideStringField;
    QrEAN13GraTamI: TIntegerField;
    QrEAN13GraGruC: TIntegerField;
    QrNext: TmySQLQuery;
    QrNextNumero: TIntegerField;
    QrRange: TmySQLQuery;
    QrRangeFaixaIni: TIntegerField;
    QrRangeFaixaFim: TIntegerField;
    QrGG1Fisc: TmySQLQuery;
    QrGG1FiscNCM: TWideStringField;
    QrGG1FiscEX_TIPI: TWideStringField;
    QrGG1FiscUnidMed: TIntegerField;
    QrProd: TmySQLQuery;
    QrProdGraGruX: TIntegerField;
    QrProdGraGru1: TIntegerField;
    QrProdNivel3: TIntegerField;
    QrProdNivel2: TIntegerField;
    QrProdNivel1: TIntegerField;
    QrProdCodUsu: TIntegerField;
    QrProdNome: TWideStringField;
    QrProdPrdGrupTip: TIntegerField;
    QrProdGraTamCad: TIntegerField;
    QrProdUnidMed: TIntegerField;
    QrProdCST_A: TSmallintField;
    QrProdCST_B: TSmallintField;
    QrProdNCM: TWideStringField;
    QrProdPeso: TFloatField;
    QrProdIPI_Alq: TFloatField;
    QrProdIPI_CST: TSmallintField;
    QrProdIPI_cEnq: TWideStringField;
    QrProdIPI_vUnid: TFloatField;
    QrProdIPI_TpTrib: TSmallintField;
    QrProdTipDimens: TSmallintField;
    QrProdPerCuztMin: TFloatField;
    QrProdPerCuztMax: TFloatField;
    QrProdMedOrdem: TIntegerField;
    QrProdPartePrinc: TIntegerField;
    QrProdInfAdProd: TWideStringField;
    QrProdSiglaCustm: TWideStringField;
    QrProdHowBxaEstq: TSmallintField;
    QrProdGerBxaEstq: TSmallintField;
    QrProdPIS_CST: TSmallintField;
    QrProdPIS_AlqP: TFloatField;
    QrProdPIS_AlqV: TFloatField;
    QrProdPISST_AlqP: TFloatField;
    QrProdPISST_AlqV: TFloatField;
    QrProdCOFINS_CST: TSmallintField;
    QrProdCOFINS_AlqP: TFloatField;
    QrProdCOFINS_AlqV: TFloatField;
    QrProdCOFINSST_AlqP: TFloatField;
    QrProdCOFINSST_AlqV: TFloatField;
    QrProdICMS_modBC: TSmallintField;
    QrProdICMS_modBCST: TSmallintField;
    QrProdICMS_pRedBC: TFloatField;
    QrProdICMS_pRedBCST: TFloatField;
    QrProdICMS_pMVAST: TFloatField;
    QrProdICMS_pICMSST: TFloatField;
    QrProdICMS_Pauta: TFloatField;
    QrProdICMS_MaxTab: TFloatField;
    QrProdIPI_pIPI: TFloatField;
    QrProdLk: TIntegerField;
    QrProdDataCad: TDateField;
    QrProdDataAlt: TDateField;
    QrProdUserCad: TIntegerField;
    QrProdUserAlt: TIntegerField;
    QrProdAlterWeb: TSmallintField;
    QrProdAtivo: TSmallintField;
    QrProdcGTIN_EAN: TWideStringField;
    QrProdEX_TIPI: TWideStringField;
    QrProdPIS_pRedBC: TFloatField;
    QrProdPISST_pRedBCST: TFloatField;
    QrProdCOFINS_pRedBC: TFloatField;
    QrProdCOFINSST_pRedBCST: TFloatField;
    QrProdICMSAliqSINTEGRA: TFloatField;
    QrProdICMSST_BaseSINTEGRA: TFloatField;
    QrProdFatorClas: TIntegerField;
    QrProdTipo_Item: TSmallintField;
    QrProdGenero: TIntegerField;
    QrProdSigla: TWideStringField;
    QrSRC: TmySQLQuery;
    QrPsq1: TmySQLQuery;
    QrX999: TmySQLQuery;
    QrGraPckQtd: TMySQLQuery;
    QrGraPckQtdGraGruC: TIntegerField;
    QrGraPckQtdGraTamI: TIntegerField;
    QrGraPckQtdQtde: TFloatField;
    MySQLDatabase1: TMySQLDatabase;
    QrFiConsSpc: TMySQLQuery;
    QrFiConsSpcGraTamI_Dest: TIntegerField;
    QrFiConsSpcGraGruC_Dest: TIntegerField;
    QrFiConsSpcQtdUso: TFloatField;
    QrGradesTam3: TMySQLQuery;
    QrGradesTam3Tam: TAutoIncField;
    QrGradesTam3NOMETAM: TWideStringField;
    QrGradesCor3: TMySQLQuery;
    QrGradesCor3Cor: TIntegerField;
    QrGradesCor3NOMECOR: TWideStringField;
    QrFiConsSpcNO_TAM_Sorc: TWideStringField;
    QrFiConsSpcNO_COR_Sorc: TWideStringField;
    QrGradesCorsGraCorCad: TIntegerField;
    QrProdUsaSubsTrib: TSmallintField;
    QrSoliComprIts: TMySQLQuery;
    QrSoliComprItsGraTamI: TIntegerField;
    QrSoliComprItsGraGruC: TIntegerField;
    QrSoliComprItsGraGru1: TIntegerField;
    QrSoliComprItsCodigo: TIntegerField;
    QrSoliComprItsControle: TIntegerField;
    QrSoliComprItsGraGruX: TIntegerField;
    QrSoliComprItsPrecoO: TFloatField;
    QrSoliComprItsPrecoR: TFloatField;
    QrSoliComprItsQuantC: TFloatField;
    QrSoliComprItsQuantV: TFloatField;
    QrSoliComprItsValBru: TFloatField;
    QrSoliComprItsDescoP: TFloatField;
    QrSoliComprItsDescoV: TFloatField;
    QrSoliComprItsValLiq: TFloatField;
    QrSoliComprItsPrecoF: TFloatField;
    QrSoliComprItsOrdem: TIntegerField;
    QrSoliComprItsReferencia: TWideStringField;
    QrSoliComprItsQuantS: TFloatField;
    QrMovOpn: TMySQLQuery;
    QrMovOpnCodigo: TIntegerField;
    QrMovOpnMov_ID: TFloatField;
    QrMovOpnMovTXT: TWideStringField;
    DsMovOpn: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrExport2AfterScroll(DataSet: TDataSet);
    procedure QrExport2BeforeClose(DataSet: TDataSet);
    procedure QrOpcoesGradBeforeOpen(DataSet: TDataSet);
    procedure QrOpcoesGradAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    FListErr1, FLayout001: String;
    FEnabledPnTipo,
    FFmGraGruNew, FGraTabAppMostra: Boolean;
    FChamouGraGruNew: TChamouGraGruNew;
    FPrdGrupTip, FNewGraGru1, FNewGraGru2,
    FNewGraGru3, FNewGraGru4, FNewGraGru5, FGraTabApp, FNewUnidMed: Integer;
    FPrdGrupTip_Nome, FGraTabApp_Nome, FNewNCM: String;
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    ////////////////////////////////////////////////////////////////////////////
    //////////// P R O V I S � R I O ///////////////////////////////////////////
    procedure CorrigeFatID_Venda(PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
    //////////// F I M
    ////////////////////////////////////////////////////////////////////////////

    function  ObtemPrecoGraGruX_GraCusPrc(GraGruX, GraCusPrc: Integer): Double; // Reduzido
    //
    procedure ReopenProdutos(GraGruT: Integer);
    procedure ReopenGradesTams(Grade: Integer);
    procedure ReopenGradesTam2(Grade: Integer);
    procedure ReopenGradesTam3(Grade: Integer);
    procedure ReopenGradesCors(Nivel1: Integer);
    procedure ReopenGradesCor2(Nivel1: Integer);
    procedure ReopenGradesCor3(Nivel1: Integer);
    procedure ReopenGraGruVal(GraGru1, GraCusPrc, Entidade: Integer;
              TipoLista: TTipoListaPreco);
    procedure ReopenGraGrXVal(GraGruX, GraCusPrc: Integer);
    procedure ReopenTabePrcGrI(GraGru1, TabePrcCab: Integer);
    procedure ReopenTabePrcGrX(GraGruX, TabePrcCab: Integer);
    procedure ReopenTabePrcGrU(GraGruX, TabePrcCab: Integer);
    procedure ReopenItsLotEtq(Lote: Integer);
    procedure ReopenPediVdaIts(Pedido, Nivel1: Integer);
    procedure ReopenFatVenIts(FatVen, Nivel1: Integer);
    procedure ReopenFatConIts(Condicional, Nivel1: Integer);
    procedure ReopenFatRetIts(Condicional, Nivel1: Integer);
    procedure ReopenVendidos(Condicional, Nivel1: Integer);
    procedure ReopenStqMovIts(TipoMov, OriCodi, GraGru1: Integer);
    procedure ReopenEstqPrdGer(Empresa, GraGru1: Integer);
    procedure ReopenEAN13(GraGru1: Integer);
    procedure ReopenSdosPed(PediVda, GraGru1: Integer);
    procedure ReopenGraPckQtd(GraPckCad: Integer);
    procedure ReopenFiConsSpc(GraGru1Dest, OriGraGru1, OriGraCorCad,
              OriGraTamIts: Integer);
    procedure ReopenSoliComprIts(SoliComprCab, Nivel1: Integer);

    procedure MostraStqMovImped(MotivImped: TStqMovImped);
    function ExisteBalancoAberto_StqCen_Uni(PrdGrupTip, Empresa,
             StqCenCad: Integer): Boolean;
    function ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa,
             FisRegCad: Integer): Boolean;
    function ExisteFatPedAberto(Empresa, StqCenCad: Integer): Boolean;
    function ExisteStqManAberto(Empresa, StqCenCad: Integer): Boolean;
    function ExisteStqClasseAberto(Empresa, StqCenCad: Integer): Boolean;
    {
    function  PreparaGrades(Grade, Nivel1: Integer;
              GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ: TStringGrid): Boolean;
    }
    // Todas cores e tamanhos do Nivel1 (Grupo de produtos) selecionado
    function  PreparaGrades(Grade, Nivel1: Integer; StrGrids: array of TStringGrid): Boolean;
    function  PreparaGradesTripleCol(Grade, Nivel1: Integer; StrGrids: array of TStringGrid): Boolean;
    // Somente cores e tamanhos do Nivel1 permitidos na Ficha de Material (Partes)
    function  PreparaGrades1(Grade, Nivel1: Integer; StrGrids: array of TStringGrid): Boolean;
    // Somente c�digos e ativos
    procedure ConfigGrades0(Grade, Nivel1: Integer;
              GradeA, GradeX, GradeC: TStringGrid);
    // Somente c�digos e ativos - Blue Derm
    procedure ConfigGrades10(Grade, Nivel1: Integer;
              GradeA, GradeX, GradeC, GradeQI, GradeQA, GradeQK: TStringGrid);
    // Pre�os por lista e estoque
    procedure ConfigGrades1(Grade, Nivel1, Tabela, Entidade: Integer;
              GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeE: TStringGrid);
    // Pre�os por prazo
    procedure ConfigGrades2(Grade, Nivel1, Tabela: Integer;
              GradeA, GradeX, GradeC, GradeP: TStringGrid; Variacao: Double);
    // Selecionar quantidades  // Impress�o de etiquetas
    procedure ConfigGrades3(Grade, Nivel1: Integer;
              GradeA, GradeX, GradeC, GradeQ: TStringGrid);
    // Quantidades selecionadas // Impress�o de etiquetas
    procedure ConfigGrades4(Grade, Nivel1, Lote: Integer;
              GradeA, GradeX, GradeC, GradeQ: TStringGrid);
    // Selecionar quantidades  // Pedido por Tabela de Pre�o
    procedure ConfigGrades5(Grade, Nivel1, Tabela, CondicaoPG: Integer;
              GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ, GradeD: TStringGrid;
              MedDDSimpl, MedDDReal: Double; ST1, ST2, ST3: TStaticText;
              ImgTipo: TdmkImage; SemPrazo: Boolean;
              TipMediaDD, FatSemPrcL: Integer);
    // Selecionar quantidades  // Pedido por Lista de Pre�os de Grupo de produto
    procedure ConfigGrades9(Grade, Nivel1, Lista, Entidade: Integer;
              GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ, GradeD,
              Grade0, Grade1, Grade2, Grade3, Grade4: TStringGrid;
              CustoFin: Double; ST1, ST2, ST3: TStaticText;
              ImgTipo: TdmkImage);
    // Mostrar selecionados // Pedido Normal
    procedure ConfigGrades6(Grade, Nivel1, Pedido: Integer;
              GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV:
              TStringGrid);
    // Mostrar selecionados // Pedido Condicional
    procedure ConfigGrades6A(Grade, Nivel1, Condicional: Integer;
              GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV:
              TStringGrid);
    // Mostrar selecionados // Faturamento Condicional
    procedure ConfigGrades6B(Grade, Nivel1, Condicional: Integer;
              GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV:
              TStringGrid);
    // Mostrar selecionados // Venda Direta
    procedure ConfigGrades6C(Grade, Nivel1, FatVen: Integer;
              GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV:
              TStringGrid);
    // Mostrar selecionados // Faturamento Normal
    procedure ConfigGrades7(Grade, Nivel1, PedCod, FatCod: Integer;
              GradeA, GradeX, GradeC, GradeQ, GradeP: TStringGrid);
    // Mostrar selecionados // Movimento avulso
    procedure ConfigGrades8(Grade, Nivel1, StqManCad: Integer;
              GradeA, GradeX, GradeC, GradeQ: TStringGrid);
    // Selecionar quantidades  // Retorno Condicional
    procedure ConfigGrades11(Condicional, Grade, Nivel1: Integer;
              GradeA, GradeX, GradeC, GradeQ, GradeV: TStringGrid);
    // Selecionar quantidades  // Movimenta��o manual
    procedure ConfigGrades12(Grade, Nivel1, GraCusPrc, Entidade: Integer;
              GradeA, GradeX, GradeC, GradeP, GradeQ: TStringGrid);
    // Mostrar selecionados // Solicita��o Normal
    procedure ConfigGrades13(Grade, Nivel1, SoliComprCab: Integer;
              GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV:
              TStringGrid);
    //

    procedure AtualizaGradesAtivos1(Tam: Integer; GradeA, GradeC: TStringGrid);
    // Mostra pre�os da lista padr�o
    procedure AtualizaGradesPrecos1(GraGru1, GraCusPrc, Entidade: Integer;
              GradeP: TStringGrid; TipoLista: TTipoListaPreco);
    // Mostra pre�os da lista padr�o com custo financeiro em StringGrids
    procedure AtualizaGradesPrecos4(GraGru1, GraCusPrc, Entidade: Integer;
              GradeC, GradeP1, GradeP2: TStringGrid; CustoFin: Double);
    // Mostra pre�os da lista padr�o com custo financeiro em dmkDBGridDAC
    procedure AtualizaDBGridPrecos4(const GraGru1, GraCusPrc, Entidade: Integer; const
              CustoFin: Double; var PrecoO, PrecoR: Double);
    // Mostra pre�os da lista padr�o com custo financeiro em dmkEdits
    procedure Atualiza_dmkEditsPrecos4(GraGruX, GraCusPrc: Integer;
              dmkEdit1, dmkEdit2: TdmkEdit; CustoFin: Double);
    // Mostra pre�os da lista de pedido com uma varia��o pr�-defida (Custo financeiro ou desconto)
    procedure AtualizaGradesPrecos2(GraGru1, TabePrcCab: Integer;
              GradeP: TStringGrid; Variacao: Double);
    // Mostra pre�os da lista de pedido com um prazo definido
    procedure AtualizaGradesPrecos3(GraGru1, TabePrcCab, CondicaoPG: Integer;
              GradeC, GradeP1, GradeP2: TStringGrid; MedDDSimpl, MedDDReal: Double;
              ST1, ST2, ST3: TStaticText; ImgTipo: TdmkImage; SemPrazo: Boolean;
              TipMediaDD: Integer; FatSemPrcL: Integer);
    // Mostra pre�o de lista para reduzido em DBGrid          
    procedure AtualizaDBGridPrecos3(const GraGru1, TabePrcCab, CondicaoPG:
              Integer; const MedDDSimpl, MedDDReal: Double; const ST1, ST2:
              TStaticText; const SemPrazo: Boolean; const TipMediaDD,
              FatSemPrcL: Integer; var PrecoO, PrecoR: Double);
    // Mostra pre�os da lista de pedido com um prazo definido (em dmkEdit)
    procedure Atualiza_dmkEditsPrecos3(GraGruX, TabePrcCab, CondicaoPG: Integer;
              dmkPreco1, dmkPreco2: TdmkEdit; MedDDSimpl, MedDDReal: Double;
              ST1, ST2, ST3: TStaticText; ImgTipo: TdmkImage; SemPrazo: Boolean;
              TipMediaDD, FatSemPrcL: Integer);
    // Mostra pre�os da lista padr�o com juros por per�odo - Pedido
    procedure AtualizaGradesPedidos(GraGru1, Pedido: Integer;
              GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
    // Mostra pre�os da lista padr�o com juros por per�odo - Venda Direta
    procedure AtualizaGradesFatVen(GraGru1, FatVen: Integer;
              GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
    // Mostra dados do Nivel1 no pedido condicional
    procedure AtualizaGradesCondicional_Saida(GraGru1, Condicional: Integer;
              GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
    // Mostra dados do Nivel1 no faturamento condicional
    procedure AtualizaGradesCondicional_Retorno(GraGru1, Condicional: Integer;
              GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
    // Mostra quantidade de faturamento por produto
    procedure AtualizaGradesFatPed(GraGru1, PediVda, FatPedCab: Integer;
              GradeC, GradeQ, GradeP: TStringGrid);
    // Mostra quantidades de movimenta��o manual
    procedure AtualizaGradesStqMovIts(TipoMov, GraGru1, OriCodi: Integer;
              GradeC, GradeQ: TStringGrid);
    // Quantidades de etiquetas
    procedure AtualizaGradesQtdEtq1(GraGru1, Lote: Integer;
              GradeP: TStringGrid);
    // Mostra EAN-13 de produto por grade
    procedure AtualizaGradesEANGeral(GraGru1: Integer; GradeEAN: TStringGrid);
    // Mostra estoque geral de produto por grade
    procedure AtualizaGradesEstoqueGeral(Empresa, GraGru1: Integer;
              GradeE: TStringGrid);
    // Mostra quantidades de itens vendidos (que podem ser cancelados)
    procedure AtualizaGradesRetornoCondicional(GraGru1, Condicional: Integer;
      GradeV: TStringGrid);
    // Mostra Quantidades de itens na Grade Frequencia (Pack)
    procedure AtualizaGradesPack1(GraGru1, GraPckCad: Integer; GradeK:
              TStringGrid);
    procedure AtualizaGradesFiConsSpc(GG1Dst, GG1Ori, OriGraCorCad,
              OriGraTamIts: Integer; GradeK: TStringGrid);
    procedure AtualizaGradesFiConsSpcTripleCol(GraGru1Dest, OriGraGru1,
              OriGraCorCad, OriGraTamIts: Integer; GradeK: TStringGrid; IniCol,
              IniLin: Integer);
    // Mostra pre�os da lista padr�o com juros por per�odo - Pedido
    procedure AtualizaGradesSoliCompr(GraGru1, SoliComprCab: Integer;
              GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
    procedure ConfigGradesPack1(GraGru1, GraTamCad, GraPckCad: Integer; GradeA,
              GradeC, GradeX, GradeK: TStringGrid);
    procedure ConfigGradesFiConsSpc(GraGru1, GraTamCad, GG1Ori: Integer;
              GradeA, GradeC, GradeX, GradeK: TStringGrid);
    procedure ConfigGradesFiConsSpcTripleCol(GraGru1Dest, GraTamCadDest:
              Integer; GradeA, GradeC, GradeX, GradeK: TStringGrid; FixedCols,
              FixedRows: Integer);
    //
    procedure ReopenQrAtribIts(SomenteSelecionados: Boolean;
              PrdGrupTip: Integer);
    function ObtemQtdePrazos(TabePrcCab: Integer): Integer;
    function NomeReduzido(NomeGrupo: String; GradeC: TStringGrid; Coluna, Linha:
             Integer): String;

    //  Etiquetas
    procedure GeraTextoEtiqueta(Memo: TMemo; EtqPrinCad: Integer;
              QrEtqGeraLot: TmySQLQuery);
    procedure GeraLinhaEtiqueta(Memo: TMemo; Texto: String;
              QrEtqGeraLot: TmySQLQuery);
    function  ValorDeVariavel_LimitTam(Texto: String): String;
    function  ValorDeVariavel(Codigo: Integer): String;
    function  GeraSequenciaDeCodigoDeBarraProduto(): String;
    procedure ObtemCodigoDeBarraProdutoDeGraGruX(const GraGruX: Integer;
                var CodigoDeBarraProduto: String);
    procedure ObtemGraGruXDeCodigoDeBarraProduto(
                const CodigoDeBarraProduto: String; var GraGruX, Sequencia: Integer);
    // Fim Etiquetas
    function ImpedePeloPrazoMedio(TabePrcCab: Integer;
             MedDDSimpl, MedDDReal: Double; TipMediaDD: Integer): Boolean;
    function InsereItemPrdGruNew(IDForm: TChamouGraGruNew; PrdGrupTip, UnidMed:
             Integer; Nome, NCM: String;
              // ini 2020-09-24
              ForcaCadastrGGX_SemCorTam: Boolean;
              // fim 2020-09-04
             Nivel5: Integer = 0; Nivel4: Integer = 0;
             Nivel3: Integer = 0; Nivel2: Integer = 0; Nivel1: Integer = 0;
             GraGruNew: Boolean = False; GraTabAppCod: Integer = 0;
             GraTabAppTxt: String = ''; NewNome: String = ''): Boolean;
    function ObtemGraCorCadDeIdx(Nivel1: Integer; Grupo, NomeItem: String): Integer;
    function ObtemGraGruCDeIdx(Nivel1: Integer; Grupo, NomeItem: String): Integer;
    //

    function  ObtemPrecoDeCusto(Empresa, GraGruX, GraCusPrc: Integer): Double;
    function  ObtemPrecoDeLista(Empresa, GraGruX, TabePrcCab, CondicaoPG: Integer;
              MedDDSimpl, MedDDReal: Double; var PrecoO, PrecoR: Double): Boolean;
    function  ObtemQuantidadesBaixa(const TipoStqMovIts, OriCodi: Integer; var Qtde, Pecas,
              Peso, AreaM2, AreaP2: Double): Boolean;
    procedure ReopenGraCusPrcU(Query: TmySQLQuery; Codigo: Integer);
    function  FormaNomeProduto(NomeGG1, NomeTam, NomeCor: String; ImprimeTam,
              ImprimeCor: Byte): String;
    procedure GeraEstoqueEm(Empresa: Integer; DataFim: TDateTime;
              TipoEstoque: TTipoEstoque; PGT_CodUsu, PGT_Codigo, GG1_CodUsu,
              GG1_Codigo, GraCusPrc: Integer; SoPositivos: Boolean;
              LaAviso2: TLabel);
    procedure ReopenExport2(Exportados: Integer; PageControl: TPageControl;
              Avisa: Boolean);
    function  DefineTipoCalc(RegrFiscal, StqCenCad, Empresa: Integer;
              NaoExibeMsg: Boolean = False): Integer;
    function  DefineDescriGerBxaEstq(Index: Integer): String;
    function  ObtemEstqueGGXdeEntidade(GraGruX, Entidade: Integer): Double;
    procedure ConfiguraPrecoDeCusto(Entidade: Integer;
              RGTipoPreco: TRadioGroup; EdGraCusPrc: TdmkEditCB;
              CBGraCusPrc: TdmkDBLookupComboBox);
    procedure CadastraSemGrade(PrdGrupTip: Integer; NomeProd, prod_cEAN,
              prod_NCM, prod_uCom, InfAdProd, ICMS_CST_A, ICMS_CST_B, IPI_CST,
              PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq: String; QrGraGruX:
              TmySQLQuery; EdGraGruX: TdmkEditCB; CBGraGruX: TdmkDBLookupComboBox);
    procedure ObtemNovoGraGruXdeExcluido(const GGXExluido: Integer;
              var GGXSubstituto, NivelExclusao: Integer);
    function  ObtemCTB(Nivel2, Nivel3, Genero: Integer): String;
    function  ObtemCTB_peloGGX(GraGruX: Integer): String;
    function  ObtemPrimeiroGraGruXdeGraGru1(const FatID, FatNum,
              nItem: Integer; const Nome: String; const DeixaEscolherSeNaoExiste:
              Boolean; var GraGru1, GraGruX: Integer): Integer;
    function  Obtem_prod_uTribdeGraGru1(const Nivel1: Integer; var Sigla:
              String; var Codigo: Integer): Boolean;
    function  GraGruXGraGruVinc(const Nome: String; var GraGru1, GraGruX:
              Integer): Boolean;
    function  VerificaEstoqueProduto(Qtde: Double; EntraSai, GraGruX, StqCenCad,
              FisRegCad, OriCodi, OriCnta, Empresa, FatSemEstq: Integer;
              NaoExibeMsg: Boolean; var Msg: String;
              ForceTipoCalc: Integer = -1): Integer;
    function  VerificaEstoqueTodoFaturamento(FatSemEstq, OriCodi, OriCnta,
              OriTipo, FisRegCad: Integer; _TipoCalc: TTipoCalcEstq =
              tceSemMovim): Boolean;
           //GetUnidMedDeSigla
    function  ObtemUnidMedDeSigla(Sigla: String): Integer;
{$IfNDef NAO_GFAT}
    function  ReopenLocRef(const TipoPesq: TTipoPesqProd; ItemPesq: Variant;
              var Nivel1, Nivel2, Nivel3, PrdGrupTip, Controle: Integer;
              var Referencia: String;
              var PerComissF, PerComissR: Double): Integer;
{$EndIf}
    function  MontaDuplicata(FatNum, nNF, Parcela, tpFaturaNum, tpFaturaSeq:
              Integer; Prefixo, Separador: String): String;
    function  SaldoSuficiente(Empresa: Integer; QtdeSdo, QtdeLei: Double;
              GraGruX: Integer; NO_Prd, NO_Cor, NO_Tam: String;
              FatSemEstq: Integer; TipoCalc: TTipoCalcEstq; NaoExibeMsg: Boolean;
              var Msg: String): Integer;
    (* Atualizado em: 30/05/2018 �s 18:49
    function  SaldoRedudidoCen(Empresa, GraGruX, StqCenCad, OriCodi,
              OriCnta: Integer): Double;
    *)
    function  SaldoRedudidoCen(Empresa, GraGruX: Integer): Double;
    procedure ReopenOpcoesGrad();
    procedure PesquisaPorSigla(Limpa: Boolean; EdSigla: TdmkEdit;
              EdUnidMed: TdmkEditCB; CBUnidMed: TdmkDBLookupComboBox);
    procedure PesquisaPorCodigo(UnidMed: Integer; EdSigla: TdmkEdit);
    function  ObtemPreco(var Preco: Double): Boolean;
    procedure CalculaCustoProduto(EdUsoQtd, EdUsoCusUni, EdUsoCusTot:
              TdmkEdit);
    function  GeraNovoCodigoNivel(Nivel, PrdGrupTip, TipNivCad: Integer;
                MostraMsg: Boolean = True): Integer;
    procedure ConfiguraOutrasMedidas(EditQtde, EditPecas, EditPLE, EditAreaM2,
              EditAreaP2: TdmkEdit; GrandezaUnidMed: Integer);
    function  ObtemGraGru1DeGraGruX(GraGruX: Integer): Integer; //ObtemNivel1DeGraGruX
    //Janelas
    function  MostraGraGru1Xxx(const ItTxt, TabNome: String; const Nivel1:
              Integer; var Controle: Integer; var Nome: String): Boolean;
    //Atualiza��es
    function  AtualizaPrdGrupTip(ProgressBar: TProgressBar): Boolean;
    procedure AlteraNumXxxSeUserDeseja(Campo: String; Nivel1: Integer;
              Texto: String);
    procedure AtualizaGraGru1LotLau(Tabela, Nome: String; Nivel1: Integer);
    function  ObtemGraGruYDeGraGru1(Nivel1: Integer): Integer;
    function  ObtemGraGruYDeGraGruX(GraGruX: Integer): Integer;
    procedure VerificaCadastroProduto(GraGru1, UnidMed: Integer; NCM, Ex_TIPI:
              String);
    function  ObtemCodigoAreaM2(var Codigo: Integer): Boolean;
    function  ObtemCodigoPesoKg(var Codigo: Integer): Boolean;
    function  ObtemCodigoPecaPc(var Codigo: Integer): Boolean;
    function  PrecoItemCorTam(Nivel1, GraGruX, Lista, EntiPrecos: Integer;
              Preco: Double): Boolean;
    //
    procedure ReopenGraGru1(Query: TmySQLQuery; TipPrd, Codigo: Integer;
              Sinal: String; ComboBox: TdmkDBLookupComboBox);
    procedure ReopenProd(GraGruX: Integer);
    function  ObtemGraTamCadDeGraTamIts(GraTamIts: Integer): Integer;
  end;

var
  DmProd: TDmProd;

implementation

uses
  {$IFNDef semNFe_v0000}ModuleNFe_0000,{$EndIf}
  {$IfNDef NAO_GFAT} FatSelRef, {$EndIf}
  // FatPedCab, ModPediVda, TabePrcCab,
  StqInnCad, UnMyObjects, Module, StqMovImped, MyDBCheck, UMySQLModule,
  PrdGruNew, ModuleGeral, UnInternalConsts, UCreate, PrdGruNewU, Principal,
  UnGrade_Tabs, StqBalCad, GraGruRSel, GraGruX, GraGru1, GraGruVal, UnGrade_Jan;

{$R *.dfm}

const FSMIC2 = '_smic2_';

function TDmProd.AtualizaPrdGrupTip(ProgressBar: TProgressBar): Boolean;
var
  Nivel1, Nivel2, Nivel3, Nivel4, Nivel5, PrdGrupTip: Integer;
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Nivel1, Nivel2, Nivel3, ',
      'Nivel4, Nivel5, PrdGrupTip ',
      'FROM gragru1 ',
      '']);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      ProgressBar.Max      := Dmod.QrAux.RecordCount;
      ProgressBar.Position := 0;
      ProgressBar.Visible  := True;
      //
      while not Dmod.QrAux.Eof do
      begin
        ProgressBar.Position := ProgressBar.Position + 1;
        ProgressBar.Update;
        Application.ProcessMessages;
        //
        PrdGrupTip := Dmod.QrAux.FieldByName('PrdGrupTip').AsInteger;
        Nivel5     := Dmod.QrAux.FieldByName('Nivel5').AsInteger;
        Nivel4     := Dmod.QrAux.FieldByName('Nivel4').AsInteger;
        Nivel3     := Dmod.QrAux.FieldByName('Nivel3').AsInteger;
        Nivel2     := Dmod.QrAux.FieldByName('Nivel2').AsInteger;
        //
        if PrdGrupTip <> 0 then
        begin
          if Nivel5 <> 0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
              'UPDATE gragru5 SET PrdGrupTip=' + Geral.FF0(PrdGrupTip),
              'WHERE Nivel5=' + Geral.FF0(Nivel5),
              '']);
          end;
          if Nivel4 <> 0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
              'UPDATE gragru4 SET PrdGrupTip=' + Geral.FF0(PrdGrupTip),
              'WHERE Nivel4=' + Geral.FF0(Nivel4),
              ''])
          end;
          if Nivel3 <> 0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
              'UPDATE gragru3 SET PrdGrupTip=' + Geral.FF0(PrdGrupTip),
              'WHERE Nivel3=' + Geral.FF0(Nivel3),
              '']);
          end;
          if Nivel2 <> 0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
              'UPDATE gragru2 SET PrdGrupTip=' + Geral.FF0(PrdGrupTip),
              'WHERE Nivel2=' + Geral.FF0(Nivel2),
              '']);
          end;
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE OpcoesGrad SET AtuaProd=1',
      '']);
    ProgressBar.Visible := False;
    Result              := True;
  except
    Result := False;
  end;
end;

function TDmProd.ObtemCodigoAreaM2(var Codigo: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Sigla,  ',
    'IF(UPPER(Sigla) = "M�", 1,  ',
    '  IF(UPPER(Sigla) = "M2", 2, 3)) Prioridade  ',
    'FROM unidmed ',
    'WHERE Grandeza=' + Geral.FF0(Integer(TGrandezaUnidMed.gumAreaM2)), // 1
    'AND ( ',
    '  UPPER(Sigla) = "M�" ',
    '  OR UPPER(Sigla) = "M2" ',
    ') ',
    'ORDER BY Prioridade, Codigo ',
    ' ']);
    Codigo := Qry.FieldByName('Codigo').AsInteger;
    Result := Codigo > 0;
    if not Result then
    begin
      Geral.MB_ERRO(
      'N�o foi localizada nenhuma unidade de medida para �rea em m� na tabela de unidades de medida!');
      //Close;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmProd.ObtemCodigoDeBarraProdutoDeGraGruX(
  const GraGruX: Integer; var CodigoDeBarraProduto: String);
begin
  ReopenOpcoesGrad();
  //
  if QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT EAN13 ',
      'FROM gragrux ',
      'WHERE Controle=' + Geral.FF0(GraGruX),
      '']);
    if DModG.QrAux.RecordCount > 0 then
      CodigoDeBarraProduto := DModG.QrAux.FieldByName('EAN13').AsString
    else
      CodigoDeBarraProduto := '';
  end else
    CodigoDeBarraProduto := Geral.FF0(GraGruX);
end;

function TDmProd.ObtemCodigoPecaPc(var Codigo: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    ' SELECT Codigo, Sigla,  ',
    '  IF(UPPER(Sigla) = "PE�A", 1,  ',
    '  IF(UPPER(Sigla) = "PE�AS", 2,  ',
    '  IF(UPPER(Sigla) = "PECA", 3, ',
    '  IF(UPPER(Sigla) = "PECAS", 4, ',
    '  IF(UPPER(Sigla) = "PC", 5, ',
    '  IF(UPPER(Sigla) = "PCS", 6, ',
    '  IF(UPPER(Sigla) = "P�", 7, ',
    '  IF(UPPER(Sigla) = "P�S", 8, ',
    '  IF(UPPER(Sigla) = "PCA", 9, ',
    '  IF(UPPER(Sigla) = "PCAS", 10, ',
    '  IF(UPPER(Sigla) = "P�A", 11, ',
    '  IF(UPPER(Sigla) = "P�AS", 12, ',
    '  IF(UPPER(Sigla) = "PEC", 13, ',
    '  IF(UPPER(Sigla) = "PE�", 14, 15)))))))))))))) Prioridade  ',
    'FROM unidmed ',
    'WHERE Grandeza=' + Geral.FF0(Integer(TGrandezaUnidMed.gumPeca)), //0
    'AND (',
    '  UPPER(Sigla) = "PE�A"',
    '  OR UPPER(Sigla) = "PE�AS"',
    '  OR UPPER(Sigla) = "PECA"',
    '  OR UPPER(Sigla) = "PECAS"',
    '  OR UPPER(Sigla) = "PC"',
    '  OR UPPER(Sigla) = "PCS"',
    '  OR UPPER(Sigla) = "P�"',
    '  OR UPPER(Sigla) = "P�S"',
    '  OR UPPER(Sigla) = "PCA"',
    '  OR UPPER(Sigla) = "PCAS"',
    '  OR UPPER(Sigla) = "P�A"',
    '  OR UPPER(Sigla) = "P�AS"',
    '  OR UPPER(Sigla) = "PEC"',
    '  OR UPPER(Sigla) = "PE�"',
    ')',
    'ORDER BY Prioridade, Codigo ',
    ' ']);
    Codigo := Qry.FieldByName('Codigo').AsInteger;
    Result := Codigo > 0;
    if not Result then
    begin
     Geral.MB_ERRO(
     'N�o foi localizada nenhuma unidade de medida para Pe�a na tabela de unidades de medida!');
    end;
  finally
    Qry.Free;
  end;
end;

function TDmProd.ObtemCodigoPesoKg(var Codigo: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Sigla,  ',
    'IF(UPPER(Sigla) = "KG", 1,  ',
    '  IF(UPPER(Sigla) LIKE "KILO%", 2, ',
    '  IF(UPPER(Sigla) LIKE "QUILO%", 3, 4))) Prioridade  ',
    'FROM unidmed ',
    'WHERE Grandeza=' + Geral.FF0(Integer(TGrandezaUnidMed.gumPesoKg)), //2
    'AND (',
    '  UPPER(Sigla) = "KG" ',
    '  OR UPPER(Sigla) LIKE "KILO%" ',
    '  OR UPPER(Sigla) LIKE "QUILO%" ',
    ')',
    'ORDER BY Prioridade, Codigo ',
    ' ']);
    Codigo := Qry.FieldByName('Codigo').AsInteger;
    Result := Codigo > 0;
    if not Result then
    begin
     Geral.MB_ERRO(
     'N�o foi localizada nenhuma unidade de medida para peso (Kg) na tabela de unidades de medida!');
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmProd.ObtemGraGruXDeCodigoDeBarraProduto(
  const CodigoDeBarraProduto: String; var GraGruX, Sequencia: Integer);
var
  CodBarra: String;
begin
  if CodigoDeBarraProduto = '' then
  begin
    GraGruX   := 0;
    Sequencia := 0;
  end else
  begin
    ReopenOpcoesGrad();
    //
    CodBarra := StringReplace(CodigoDeBarraProduto, sLineBreak, '', [rfReplaceAll]);
    CodBarra := Trim(Geral.SoNumero_TT(Trim(CodBarra)));
    //
    if QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM gragrux ',
        'WHERE EAN13=' + CodBarra,
        '']);
      if DModG.QrAux.RecordCount > 0 then
      begin
        GraGruX   := DModG.QrAux.FieldByName('Controle').AsInteger;
        Sequencia := 0;
      end else
      begin
        GraGruX   := 0;
        Sequencia := 0;
      end;
    end else
    begin
      if Length(CodigoDeBarraProduto) <= 6 then
      begin
        GraGruX   := Geral.IMV(CodigoDeBarraProduto);
        Sequencia := 0;
      end else
      begin
        GraGruX   := Geral.IMV(Copy(CodigoDeBarraProduto, 07, 6));
        Sequencia := Geral.IMV(Copy(CodigoDeBarraProduto, 13, 8));
      end;
    end;
  end;
end;

function TDmProd.ObtemGraGruYDeGraGru1(Nivel1: Integer): Integer;
var
  Qry: TmySQLQuery;
  GraGruY, Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggy.Codigo  ',
    'FROM gragru1 gg1 ',
    'LEFT JOIN gragruy ggy ON ggy.PrdGrupTip=gg1.PrdGrupTip ',
    'WHERE gg1.Nivel1=' + Geral.FF0(Nivel1),
    '']);
    Result := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TDmProd.ObtemGraGruYDeGraGruX(GraGruX: Integer): Integer;
var
  Qry: TmySQLQuery;
  GraGruY, Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.GraGruY   ',
    'FROM gragrux ggx  ',
    //'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE Controle=' + Geral.FF0(GraGruX),
    '']);
    Result := USQLDB.v_I(Qry, 'GraGruY');
(*
    'SELECT ggy.Codigo, ggx.GraGruY  ',
    'FROM gragru1 ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN gragruy ggy ON ggy.PrdGrupTip=gg1.PrdGrupTip ',
    'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
    '']);
    Result := USQLDB.v_I(Qry, 'Codigo');
*)
  finally
    Qry.Free;
  end;
end;

function TDmProd.ObtemGraTamCadDeGraTamIts(GraTamIts: Integer): Integer;
begin
// Parei aqui! Falta fazer
end;

procedure TDmProd.AlteraNumXxxSeUserDeseja(Campo: String; Nivel1: Integer;
  Texto: String);
var
  MyFld: String;
  //
  procedure AtzTab(Tabela: String);
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
    MyFld], ['Nivel1'], [
    Texto], [Nivel1], True);
  end;
var
  DescriFld: String;
  NumLote, NumLaudo: Boolean;
begin
  NumLaudo := Lowercase(Campo) = Lowercase('NumLaudo');
  NumLote  := Lowercase(Campo) = Lowercase('NumLote');
  //
  if NumLaudo then
  begin
    DescriFld := 'Laudo';
    MyFld := 'AtuNumLaud';
  end else
  if NumLote then
  begin
    DescriFld := 'Lote';
    MyFld := 'AtuNumLot';
  end else begin
    DescriFld := '???';
    MyFld := '???';
  end;
  //
  if Geral.MB_Pergunta('Deseja atualizar o campo "' + DescriFld +
  '" deste produto nos cadastros do produto?') = ID_YES then
  begin
    AtzTab('grag1prap');
    AtzTab('grag1prmo');
    //
    if NumLaudo then
      AtualizaGraGru1LotLau('gragru1lau', Texto, Nivel1)
    else
    if NumLote then
      AtualizaGraGru1LotLau('gragru1lot', Texto, Nivel1)
    else
      ;
  end;
end;

procedure TDmProd.AtualizaDBGridPrecos3(const GraGru1, TabePrcCab, CondicaoPG:
  Integer; const MedDDSimpl, MedDDReal: Double; const ST1, ST2: TStaticText;
  const SemPrazo: Boolean; const TipMediaDD, FatSemPrcL: Integer;
  var PrecoO, PrecoR: Double);
  procedure PrecosEspecificos(StrFmt: String; Fator: Double; var Libera: Boolean);
  begin
    ReopenTabePrcGrI(GraGru1, TabePrcCab);
    Libera := Libera or (QrTabePrcGrI.RecordCount > 0);
    QrTabePrcGrI.First;
    while not QrTabePrcGrI.Eof do
    begin
      if QrGradesTams.Locate('Tam', QrTabePrcGrIGraTamI.Value, []) then
      begin
        if QrGradesCors.Locate('Cor', QrTabePrcGrIGraGruC.Value, []) then
        begin
          PrecoO :=  QrTabePrcGrIPreco.Value;
          PrecoR := dmkPF.FFF(PrecoO * Fator,
            Dmod.QrControle.FieldByName('CasasProd').AsInteger,  siNegativo);
        end;
      end;
      QrTabePrcGrI.Next;
    end;
  end;
  //
  function PrecoGeral(StrFmt: String; Fator: Double): Boolean;
  begin
    QrPrcGru.Close;
    QrPrcGru.Params[00].AsInteger := TabePrcCab;
    QrPrcGru.Params[01].AsInteger := GraGru1;
    UMyMod.AbreQuery(QrPrcGru, Dmod.MyDB, 'TDmProd.AtualizaDBGridPrecos3() > PrecoGeral()');
    Result := QrPrcGru.RecordCount > 0;
    //
    PrecoO := QrPrcGruPreco.Value;
    PrecoR := dmkPF.FFF(PrecoO * Fator, Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
  end;
  //
  function PrecoNivel(StrFmt: String; Fator: Double): Boolean;
  var
    TPC_Txt, GG1_Txt: String;
  begin
    TPC_Txt := formatFloat('0', TabePrcCab);
    GG1_Txt := formatFloat('0', GraGru1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrcNiv, Dmod.MyDB, [
      'SELECT tpn.Nivel, tpn.Preco',
      'FROM gragru1 gg1',
      'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=1 AND tpn.CodNiv=gg1.Nivel1',
      'WHERE tpn.Nivel IS NOT NULL',
      'AND tpn.Codigo=' + TPC_Txt,
      'AND gg1.Nivel1=' + GG1_Txt,
      '',
      'UNION',
      '',
      'SELECT tpn.Nivel, tpn.Preco',
      'FROM gragru1 gg1',
      'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=2 AND tpn.CodNiv=gg1.Nivel2',
      'WHERE tpn.Nivel IS NOT NULL',
      'AND tpn.Codigo=' + TPC_Txt,
      'AND gg1.Nivel1=' + GG1_Txt,
      '',
      'UNION',
      '',
      'SELECT tpn.Nivel, tpn.Preco',
      'FROM gragru1 gg1',
      'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=3 AND tpn.CodNiv=gg1.Nivel3',
      'WHERE tpn.Nivel IS NOT NULL',
      'AND tpn.Codigo=' + TPC_Txt,
      'AND gg1.Nivel1=' + GG1_Txt,
      '',
      'UNION',
      '',
      'SELECT tpn.Nivel, tpn.Preco',
      'FROM gragru1 gg1',
      'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=4 AND tpn.CodNiv=gg1.PrdGrupTip',
      'WHERE tpn.Nivel IS NOT NULL',
      'AND tpn.Codigo=' + TPC_Txt,
      'AND gg1.Nivel1=' + GG1_Txt,
      '',
      'ORDER BY Nivel',
      '']);
    Result := QrPrcNiv.RecordCount > 0;
    //
    PrecoO := QrPrcNivPreco.Value;
    PrecoR := dmkPF.FFF(PrecoO * Fator, Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
  end;
var
  //c, r: Integer;
  TipoPrzTxt: String;
  Fator, MediaDD: Double;
  Libera: Boolean;
begin
{
  ImgTipo.SQLType := stLok;
}
  ST1.Caption := '';
  ST2.Caption := '';
  ST1.Visible := False;
  ST2.Visible := False;
  //
{
  ST3.Caption := '';
  if (GradeP1 = nil) and (GradeP2 = nil) then Exit;
  //
  if GradeP1 <> nil then
  begin
    for c := 1 to GradeP1.ColCount - 1 do
      for r := 1 to GradeP1.RowCount - 1 do
        GradeP1.Cells[c, r] := '';
  end;
  //
  if GradeP2 <> nil then
  begin
    for c := 1 to GradeP2.ColCount - 1 do
      for r := 1 to GradeP2.RowCount - 1 do
        GradeP2.Cells[c, r] := '';
  end;
  //
}
  QrQtdItsPrz.Close;
  QrQtdItsPrz.Params[0].AsInteger := TabePrcCab;
  UMyMod.AbreQuery(QrQtdItsPrz, Dmod.MyDB, 'TDmProd.AtualizaDBGridPrecos3()');
  //
  MediaDD := MedDDReal;
  TipoPrzTxt := 'real';
  if QrQtdItsPrz.RecordCount > 0 then
  begin
    if SemPrazo then
    begin
      //MediaDD := 0;
      Fator   := 1;
      //
    end else begin
      case TipMediaDD(*Dm PediVda / FatVen .QrParamsEmpTipMediaDD.Value*) of
        0:
        begin
          ST1.Caption := 'Tipo de c�lculo de prazo m�dio n�o definido nos par�metros da filial!';
          ST1.Visible := True;
          MediaDD := MedDDReal;
          TipoPrzTxt := 'real';
        end;
        1:
        begin
          MediaDD := MedDDSimpl;
          TipoPrzTxt := 'simples';
        end;
        2:
        begin
          MediaDD := MedDDReal;
          TipoPrzTxt := 'real';
        end;
      end;
      QrLocPrz.Close;
      QrLocPrz.Params[00].AsInteger := TabePrcCab;
      QrLocPrz.Params[01].AsFloat   := MediaDD;
      UMyMod.AbreQuery(QrLocPrz, Dmod.MyDB, 'TDmProd.AtualizaDBGridPrecos3()');
      if (QrLocPrz.RecordCount = 0) then //and (QrQtdItsPrz.RecordCount > 0) then
      begin
        ST2.Caption := 'Falta de faixa de prazo m�dio na lista de pre�os! ' +
        'Prazo m�dio ' + TipoPrzTxt + ': ' + FloatToStr(MediaDD) + ' dias.';
        ST2.Visible := True;
        Exit;
      end;
      //
      Fator := (QrLocPrzVariacao.Value / 100) + 1;
    end;
    //
    //
    // Pre�o geral do Grupo (Nivel1)
    Libera := PrecoGeral(Dmod.FStrFmtPrc, Fator);
    //
    // Pre�os espec�ficos
    PrecosEspecificos(Dmod.FStrFmtPrc, Fator, Libera);

    // Pre�o por Nivel
    if not Libera then
      Libera := PrecoNivel(Dmod.FStrFmtPrc, Fator);

  end else begin
    ST1.Caption := 'Produto sem pre�o a prazo!';
    ST1.Visible := True;
    //
    Fator := 1; // sem custo financeiro / desconto
    Libera := PrecoGeral(Dmod.FStrFmtPrc, Fator);
  end;
{
  //Libera para edi��o / confirma��o
  if Libera or ((*Dm PediVda / FatVen .QrParamsEmpFatSemPrcL.Value*)FatSemPrcL = 1) then
    ImgTipo.SQLType := stIns;
}
end;

procedure TDmProd.AtualizaDBGridPrecos4(const GraGru1, GraCusPrc, Entidade: Integer;
  const CustoFin: Double; var PrecoO, PrecoR: Double);
var
  //c, l: Integer;
  Fator: Double;
begin
  Fator := (CustoFin / 100) + 1;
  ReopenGraGruVal(GraGru1, GraCusPrc, Entidade, tpGraCusPrc);
  QrGraGruVal.First;
  //
  while not QrGraGruVal.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrGraGruValGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrGraGruValGraGruC.Value, []) then
      begin
        {
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        }
        PrecoO := QrGraGruValCustoPreco.Value;
        PrecoR := PrecoO * Fator;
      end;
    end;
    QrGraGruVal.Next;
  end;
end;

procedure TDmProd.AtualizaGradesAtivos1(Tam: Integer; GradeA, GradeC: TStringGrid);
var
  c, l: Integer;
begin
  ReopenProdutos(Tam);
  QrProdutos.First;
  while not QrProdutos.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutosTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutosCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutosControle.Value <> 0 then
        begin
          GradeA.Cells[c, l] := IntToStr(QrProdutosAtivo.Value);
          GradeC.Cells[c, l] := FormatFloat('0', QrProdutosControle.Value);
        end else begin
          GradeA.Cells[c, l] := '';
          GradeC.Cells[c, l] := '';
        end;
      end;
    end;
    QrProdutos.Next;
  end;
  //
end;

procedure TDmProd.AtualizaGradesPrecos1(GraGru1, GraCusPrc, Entidade: Integer;
  GradeP: TStringGrid; TipoLista: TTipoListaPreco);
var
  c, l: Integer;
  Fmt: String;
begin
  if GradeP = nil then Exit;
  //
  ReopenGraGruVal(GraGru1, GraCusPrc, Entidade, TipoLista);
  //
  QrGraGruVal.First;
  for c := 1 to GradeP.ColCount - 1 do
    for l := 1 to GradeP.RowCount - 1 do
      GradeP.Cells[c, l] := '';
  //
  if GraCusPrc < 4 then
  // no futuro usar
  //if GraCusPrc.CustoPreco=1 (0=>Custo, 1=>Pre�o) then
    Fmt := Dmod.FStrFmtPrc
  else
    Fmt := Dmod.FStrFmtCus;
  //
  while not QrGraGruVal.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrGraGruValGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrGraGruValGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        GradeP.Cells[c, l] := FormatFloat(Fmt, QrGraGruValCustoPreco.Value);
      end;
    end;
    QrGraGruVal.Next;
  end;
end;

procedure TDmProd.AtualizaGradesEANGeral(GraGru1: Integer; GradeEAN: TStringGrid);
var
  c, l: Integer;
begin
  if GradeEAN = nil then Exit;
  //
  ReopenEAN13(GraGru1);
  QrEAN13.First;
  for c := 1 to GradeEAN.ColCount - 1 do
    for l := 1 to GradeEAN.RowCount - 1 do
      GradeEAN.Cells[c, l] := '';
  //
  while not QrEAN13.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrEAN13GraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrEAN13GraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        GradeEAN.Cells[c, l] := QrEAN13EAN13.Value;
      end;
    end;
    QrEAN13.Next;
  end;
  //
end;

procedure TDmProd.AtualizaGradesEstoqueGeral(Empresa, GraGru1: Integer;
  GradeE: TStringGrid);
var
  c, l: Integer;
begin
  if GradeE = nil then Exit;
  //
  ReopenEstqPrdGer(Empresa, GraGru1);
  QrStqPrdGer.First;
  for c := 1 to GradeE.ColCount - 1 do
    for l := 1 to GradeE.RowCount - 1 do
      GradeE.Cells[c, l] := '';
  //
  while not QrStqPrdGer.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrStqPrdGerGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrStqPrdGerGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        GradeE.Cells[c, l] := FloatToStr(QrStqPrdGerQTDE.Value);
      end;
    end;
    QrStqPrdGer.Next;
  end;
  //
end;

procedure TDmProd.ReopenPediVdaIts(Pedido, Nivel1: Integer);
begin
  QrPediVdaIts.Close;
  QrPediVdaIts.Params[00].AsInteger := Pedido;
  QrPediVdaIts.Params[01].AsInteger := Nivel1;
  UMyMod.AbreQuery(QrPediVdaIts, Dmod.MyDB, 'TDmProd.ReopenPediVdaIts()');
end;

procedure TDmProd.ReopenFatVenIts(FatVen, Nivel1: Integer);
begin
  QrFatVenIts.Close;
  QrFatVenIts.Params[00].AsInteger := FatVen;
  QrFatVenIts.Params[01].AsInteger := Nivel1;
  UMyMod.AbreQuery(QrFatVenIts, Dmod.MyDB, 'TDmProd.ReopenFatVenIts()');
end;

procedure TDmProd.ReopenFiConsSpc(GraGru1Dest, OriGraGru1, OriGraCorCad,
  OriGraTamIts: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFiConsSpc, Dmod.MyDB, [
  'SELECT gti1.Nome NO_TAM_Sorc, gcc1.Nome NO_COR_Sorc, ',
  'ggx2.GraTamI GraTamI_Dest, ggx2.GraGruC GraGruC_Dest, ',
  'fcs.*  ',
  'FROM ficonsspc fcs ',
  'LEFT JOIN gragrux    ggx2 ON ggx2.Controle=fcs.GGXDest ',
  'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc ',
  'LEFT JOIN gragruc    ggc1 ON ggc1.Controle=ggx1.GraGruC  ',
  'LEFT JOIN gracorcad  gcc1 ON gcc1.Codigo=ggc1.GraCorCad  ',
  'LEFT JOIN gratamits  gti1 ON gti1.Controle=ggx1.GraTamI  ',
  'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  ',
  'LEFT JOIN unidmed    unm1 ON unm1.Codigo=gg11.UnidMed  ',
  'WHERE fcs.GG1Dest=' + Geral.FF0(GraGru1Dest),
  'AND gg11.Nivel1=' + Geral.FF0(OriGraGru1),
  'AND gti1.Controle=' + Geral.FF0(OriGraTamIts),
  'AND gcc1.Codigo=' + Geral.FF0(OriGraCorCad),
  EmptyStr]);
  //Geral.MB_SQL(nil, QrFiConsSpc);
end;

procedure TDmProd.ReopenFatConIts(Condicional, Nivel1: Integer);
begin
  QrFatConIts.Close;
  QrFatConIts.Params[00].AsInteger := Condicional;
  QrFatConIts.Params[01].AsInteger := Nivel1;
  UMyMod.AbreQuery(QrFatConIts, Dmod.MyDB, 'TDmProd.ReopenFatConIts()');
end;

procedure TDmProd.ReopenFatRetIts(Condicional, Nivel1: Integer);
begin
  QrFatRetIts.Close;
  QrFatRetIts.Params[00].AsInteger := Condicional;
  QrFatRetIts.Params[01].AsInteger := Nivel1;
  UMyMod.AbreQuery(QrFatRetIts, Dmod.MyDB, 'TDmProd.ReopenFatRetIts()');
end;

procedure TDmProd.ReopenVendidos(Condicional, Nivel1: Integer);
begin
  QrVendidos.Close;
  QrVendidos.Params[00].AsInteger := Condicional;
  QrVendidos.Params[01].AsInteger := Nivel1;
  UMyMod.AbreQuery(QrVendidos, Dmod.MyDB, 'TDmProd.ReopenVendidos()');
end;

(* Atualizado em: 30/05/2018 �s 18:49
function TDmProd.SaldoRedudidoCen(Empresa, GraGruX, StqCenCad, OriCodi,
  OriCnta: Integer): Double;
var
  Qtde: Double;
begin
  QrEstqCen.Close;
  QrEstqCen.Params[00].AsInteger := GraGruX;
  QrEstqCen.Params[01].AsInteger := StqCenCad;
  UnDmkDAC_PF.AbreQuery(QrEstqCen, Dmod.MyDB);
  //
  if (OriCodi <> 0) or (OriCnta <> 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT SUM(Qtde * Baixa) Qtde ',
      'FROM stqmovitsa ',
      'WHERE Ativo=0 ',
       AND ValiStq <> 101, colocado depois para analisar se reativar o codigo! (2020-11-22)
      'AND OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCnta=' + Geral.FF0(OriCnta),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      'AND StqCenCad=' + Geral.FF0(StqCenCad),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
    Qtde := DModG.QrAux.FieldByName('Qtde').AsFloat;
  end else
    Qtde := 0;
  //
  Result := QrEstqCenQtde.Value + Qtde;
end;
*)

function TDmProd.SaldoRedudidoCen(Empresa, GraGruX: Integer): Double;
var
  Qtde: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSRC(*DModG.QrAux*), Dmod.MyDB, [
    'SELECT SUM(Qtde * Baixa) Qtde ',
    'FROM stqmovitsa ',
    'WHERE Ativo=1 ',
    'AND ValiStq <> 101',  // 2020-11-22
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
  //
  Qtde := QrSRC(*DModG.QrAux*).FieldByName('Qtde').AsFloat;
  //
  Result := QrEstqCenQtde.Value + Qtde;
end;

function TDmProd.SaldoSuficiente(Empresa: Integer; QtdeSdo, QtdeLei: Double;
  GraGruX: Integer; NO_Prd, NO_Cor, NO_Tam: String; FatSemEstq: Integer;
  TipoCalc: TTipoCalcEstq; NaoExibeMsg: Boolean; var Msg: String): Integer;
var
  QtdeNew: Double;
  Texto: String;
  FSE: Integer;
begin
  if TipoCalc <> tceBaixa then// (*2*) se for diferente de subtrai > 0=Nulo, 1=Adiciona e 2=Subtrai
  begin
    Msg    := '';
    Result := ID_YES;
  end else begin
    QtdeNew := QtdeSdo - QtdeLei;
    if QtdeNew < 0 then
    begin
      if FatSemEstq = -1 then
      begin
        DmodG.ReopenParamsEmp(Empresa);
        FSE := DModG.QrParamsEmpFatSemEstq.Value;
      end else FSE := FatSemEstq;
      case FSE of
        1:
        begin
          Msg    := '';
          Result := ID_YES;
        end;
        2:
        begin
          //Result := ID_CANCEL;
          Msg   := '';
          Texto := '(Param.Filial.Pedidos) Saldo insufiente:' + sLineBreak;
          //
          if GraGruX <> 0 then
            Texto := Texto + 'Reduzido: ' + FormatFloat('000000', GraGruX) + sLineBreak;
          if Trim(NO_Prd) <> '' then
            Texto := Texto + 'Produto: ' + NO_Prd + ' ' + NO_Cor + ' ' + NO_Tam + sLineBreak;
          //
          Msg := Msg + 'Saldo atual: ' + FormatFloat('0.000', QtdeSdo) + sLineBreak;
          Msg := Msg + 'Requisitado: ' + FormatFloat('0.000', QtdeLei) + sLineBreak;
          Msg := Msg + 'Saldo futuro: ' + FormatFloat('0.000', QtdeNew) + sLineBreak;
          //
          Texto := Texto + Msg + sLineBreak + 'Deseja confirmar assim mesmo?';
          Msg   := StringReplace(Msg, sLineBreak, ' ', [rfReplaceAll, rfIgnoreCase]);
          //
          if NaoExibeMsg then
            Result := ID_NO
          else
            Result := Geral.MB_Pergunta(Texto);
        end;
        //0:
        else begin
          Msg    := 'Saldo atual: ' + FloatToStr(QtdeSdo) + ' - sa�da: ' +
          FloatToStr(QtdeLei) + ' = Saldo futuro de ' + FloatToStr(QtdeNew) +
          '. Inclus�o abortada! (Param.Filial.Pedidos)';
          Result := ID_NO;
          //
          if not NaoExibeMsg then
            Geral.MB_Aviso(Msg);
        end;
      end;
    end else
    begin
      Msg    := '';
      Result := ID_YES;
    end;
  end;
end;

procedure TDmProd.ReopenProd(GraGruX: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProd, Dmod.MyDB, [
  'SELECT ggx.Controle GraGruX, ggx.GraGru1,  ',
  'pgt.Tipo_Item, pgt.Genero, unm.Sigla, gg1.* ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE ggx.Controle=' + Geral.FF0(GragruX),
  '']);
end;

procedure TDmProd.ReopenProdutos(GraGruT: Integer);
begin
(*
  QrProdutos.Close;
  QrProdutos.Params[00].AsInteger := GraGruT;
  UMyMod.AbreQuery(QrProdutos, Dmod.MyDB, 'TDmProd.ReopenProdutos()');
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrProdutos, Dmod.MyDB, [
  'SELECT ggx.GraGruC Cor, ggx.GraTamI Tam, ',
  'ggx.Controle, ggx.Ativo ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON ggx.GraGru1=gg1.Nivel1 ',
  'WHERE ggx.GraGru1=' + Geral.FF0(GraGruT),
  '']);
end;

procedure TDmProd.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger
end;

procedure TDmProd.ReopenEAN13(GraGru1: Integer);
begin
  QrEAN13.Close;
  QrEAN13.Params[0].AsInteger := GraGru1;
  UMyMod.AbreQuery(QrEAN13, Dmod.MyDB, 'TDmProd.ReopenEAN13(');
end;

procedure TDmProd.ReopenEstqPrdGer(Empresa, GraGru1: Integer);
begin
  QrStqPrdGer.Close;
  QrStqPrdGer.Params[0].AsInteger := GraGru1;
  QrStqPrdGer.Params[1].AsInteger := Empresa;
  UMyMod.AbreQuery(QrStqPrdGer, Dmod.MyDB, 'TDmProd.ReopenEstqPrdGer(');
end;

procedure TDmProd.ReopenExport2(Exportados: Integer; PageControl: TPageControl;
Avisa: Boolean);
var
  Txt: String;
begin
  QrExport2.Close;
  UMyMod.AbreQuery(QrExport2, DModG.MyPID_DB);
  if (QrExport2.RecordCount > 0) then
  begin
    if PageControl <> nil then
      PageControl.ActivePageIndex := 2;
    if Avisa then
    begin
      if Exportados = 0 then
        Txt := 'Nenhum item pesquisado foi exportado'
      else begin
        if Exportados = 1 then
          Txt := 'Um item dos ' + IntToStr(QrSMIC2.RecordCount) +
          ' pesquisados foi exportado!' + slineBreak +
          'Os demais itens n�o foram exportados'
        else
          Txt := IntToStr(Exportados) + ' itens dos ' +
          IntToStr(QrSMIC2.RecordCount) + ' pesquisados foram exportados!' +
          slineBreak + 'Os demais itens n�o foram exportados';
      end;
      //
      Txt := 'INVENT�RIO: ' + Txt + ' por falhas que devem ser corrigidas!';
      Geral.MensagemBox(Txt, 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

procedure TDmProd.ReopenStqMovIts(TipoMov, OriCodi, GraGru1: Integer);
begin
  QrStqMovItsA.Close;
  QrStqMovItsA.Params[00].AsInteger := TipoMov;
  QrStqMovItsA.Params[01].AsInteger := OriCodi;
  QrStqMovItsA.Params[02].AsInteger := GraGru1;
  UMyMod.AbreQuery(QrStqMovItsA, Dmod.MyDB, 'TDmProd.ReopenStqMovIts()');
  //
  QrStqMovItsB.Close;
  QrStqMovItsB.Params[00].AsInteger := TipoMov;
  QrStqMovItsB.Params[01].AsInteger := OriCodi;
  QrStqMovItsB.Params[02].AsInteger := GraGru1;
  UMyMod.AbreQuery(QrStqMovItsB, Dmod.MyDB, 'TDmProd.ReopenStqMovIts()');
end;

procedure TDmProd.ReopenGradesCors(Nivel1: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Nivel1;
  UMyMod.AbreQuery(QrGradesCors, Dmod.MyDB, 'TDmProd.ReopenGradesCors()');
end;

procedure TDmProd.ReopenGraCusPrcU(Query: TmySQLQuery; Codigo: Integer);
begin
  Query.Close;
  Query.SQL.Clear;
  if VAR_USUARIO > 0 then
  begin
    Query.SQL.Add('SELECT DISTINCT cpu.Codigo, gcp.CodUsu, gcp.Nome,');
    Query.SQL.Add('ELT(CustoPreco+1, "Custo", "Pre�o") CusPrc,');
    Query.SQL.Add('ELT(TipoCalc+1, "Manual", "Pre�o m�dio", "�ltima compra") NomeTC,');
{$IfNDef SemCotacoes}
    Query.SQL.Add('mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA');
{$Else}
    Query.SQL.Add('"" NOMEMOEDA, "" SIGLAMOEDA');
{$EndIf}
    Query.SQL.Add('FROM gracusprcu cpu');
    Query.SQL.Add('LEFT JOIN gracusprc gcp ON gcp.Codigo=cpu.Codigo');
{$IfNDef SemCotacoes}
    Query.SQL.Add('LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda');
{$EndIf}
    Query.SQL.Add('WHERE Usuario=:P0');
    Query.SQL.Add('ORDER BY gcp.Nome');
    Query.Params[0].AsInteger := VAR_USUARIO;
  end else begin
    Query.SQL.Add('SELECT DISTINCT gcp.Codigo, gcp.CodUsu, gcp.Nome,');
    Query.SQL.Add('ELT(CustoPreco+1, "Custo", "Pre�o") CusPrc,');
    Query.SQL.Add('ELT(TipoCalc+1, "Manual", "Pre�o m�dio", "�ltima compra") NomeTC,');
{$IfNDef SemCotacoes}
    Query.SQL.Add('mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA');
{$Else}
    Query.SQL.Add('"" NOMEMOEDA, "" SIGLAMOEDA');
{$EndIf}
    Query.SQL.Add('FROM gracusprc gcp ');
{$IfNDef SemCotacoes}
    Query.SQL.Add('LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda');
{$EndIf}
    Query.SQL.Add('ORDER BY gcp.Nome');
  end;
  UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
  //
  if Codigo <> 0 then
    Query.Locate('Codigo', Codigo, []);
end;

procedure TDmProd.ReopenGradesCor2(Nivel1: Integer);
begin
  QrGradesCor2.Close;
  QrGradesCor2.Params[00].AsInteger := Nivel1;
  QrGradesCor2.Params[01].AsInteger := Nivel1;
  UMyMod.AbreQuery(QrGradesCors, Dmod.MyDB, 'TDmProd.ReopenGradesCor2()');
end;

procedure TDmProd.ReopenGradesCor3(Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesCor3, Dmod.MyDB, [
  'SELECT gc.Controle Cor, cc.Nome NOMECOR ',
  'FROM gragruc gc ',
  'LEFT JOIN gracorcad cc ON cc.Codigo=gc.GraCorCad ',
  'WHERE gc.Nivel1=' + Geral.FF0(Nivel1),
  'ORDER BY NOMECOR ',
  EmptyStr]);
end;

procedure TDmProd.ReopenGradesTams(Grade: Integer);
begin
(*
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := Grade;
  UMyMod.AbreQuery(QrGradesTams, Dmod.MyDB, 'TDmProd.ReopenGradesTams()');
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesTams, Dmod.MyDB, [
  'SELECT Controle Tam, Nome NOMETAM ',
  'FROM gratamits ',
  'WHERE Codigo=' + Geral.FF0(Grade),
  // inicio 2019-01-22
  'AND Controle>0 ',
  // fim 2019-01-22
  '']);
end;

procedure TDmProd.ReopenGradesTam2(Grade: Integer);
begin
  QrGradesTam2.Close;
  QrGradesTam2.Params[00].AsInteger := Grade;
  QrGradesTam2.Params[01].AsInteger := Grade;
  UMyMod.AbreQuery(QrGradesTam2, Dmod.MyDB, 'TDmProd.ReopenGradesTam2()');
end;

procedure TDmProd.ReopenGradesTam3(Grade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesTam3, Dmod.MyDB, [
  'SELECT Controle Tam, Nome NOMETAM ',
  'FROM gratamits ',
  'WHERE Codigo=' + Geral.FF0(Grade),
  'AND Controle>0 ',
  '']);
end;

procedure TDmProd.ReopenGraGru1(Query: TmySQLQuery; TipPrd, Codigo: Integer;
  Sinal: String; ComboBox: TdmkDBLookupComboBox);
var
  RstrPrdFat: Integer;
  SQL: TStringList;
begin
  RstrPrdFat := QrOpcoesGrad.FieldByName('RstrPrdFat').AsInteger;
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,');
  Query.SQL.Add('gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, pgt.MadeBy,');
  Query.SQL.Add('gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,');
  Query.SQL.Add('gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,');
  Query.SQL.Add('gg1.UsaSubsTrib, ');
  Query.SQL.Add('unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED,');
  Query.SQL.Add('unm.Nome NOMEUNIDMED,  pgt.Fracio, gg1.prod_indTot,');
  Query.SQL.Add('CONCAT(LPAD(gg1.CodUsu, 8, "0"), " - ", gg1.Nome) NOME_EX ');
  //
  if RstrPrdFat = 0 then
  begin
    Query.SQL.Add('FROM gragru1 gg1');
  end else
  begin
    Query.SQL.Add('FROM rstrprdfat rpf');
    Query.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = rpf.Nivel1');
  end;
  Query.SQL.Add('LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad');
  Query.SQL.Add('LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed');
  Query.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
  //
  Query.SQL.Add('WHERE pgt.TipPrd ' + Sinal + ' ' + Geral.FF0(TipPrd));
  //
  Query.SQL.Add('AND gg1.Nivel1 > 0');
  Query.SQL.Add('');
  Query.SQL.Add('AND gg1.Nivel1 NOT IN (');
  Query.SQL.Add('  SELECT DISTINCT gg1.Nivel1');
  Query.SQL.Add('  FROM pedivdaits pvi');
  Query.SQL.Add('  LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX');
  Query.SQL.Add('  LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
  Query.SQL.Add('  WHERE pvi.Codigo=:P0');
  Query.SQL.Add(')');
  Query.SQL.Add('');
  Query.SQL.Add('ORDER BY gg1.Nome');
  Query.Params[0].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
  //
  SQL := TStringList.Create;
  try
    SQL.Add('SELECT gg1.Codusu, gg1.Nome ');
    //
    if RstrPrdFat = 0 then
    begin
      SQL.Add('FROM gragru1 gg1');
    end else
    begin
      SQL.Add('FROM rstrprdfat rpf');
      SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = rpf.Nivel1');
    end;
    //
    SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    SQL.Add('WHERE pgt.TipPrd ' + Sinal + ' ' + Geral.FF0(TipPrd));
    SQL.Add('AND gg1.Nivel1 > 0');
    SQL.Add('AND gg1.Nivel1 NOT IN (');
    SQL.Add('  SELECT DISTINCT gg1.Nivel1');
    SQL.Add('  FROM pedivdaits pvi');
    SQL.Add('  LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX');
    SQL.Add('  LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
    SQL.Add('  WHERE pvi.Codigo=' + Geral.FF0(Codigo));
    SQL.Add(')');
    SQL.Add('AND gg1.Nome LIKE "%$#%"');
    SQL.Add('ORDER BY gg1.Nome');
    //
    ComboBox.LocF7SQLText := SQL;
  finally
    SQL.Free;
  end;
end;

procedure TDmProd.ReopenGraGruVal(GraGru1, GraCusPrc, Entidade: Integer;
  TipoLista: TTipoListaPreco);
begin
  case TipoLista of
    tpTabePrc:
    begin
      //Venda
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, Dmod.MyDB, [
        'SELECT ggx.GraGruC, ggx.GraTamI, IF(tgi.Preco IS NULL, tgg.Preco, tgi.Preco) CustoPreco ',
        'FROM gragrux ggx ',
        'LEFT JOIN tabeprcgrg tgg ON tgg.Nivel1 = ggx.GraGru1 ',
        'LEFT JOIN tabeprcgri tgi ON tgi.GraGruX = ggx.Controle ',
        'WHERE ggx.GraGru1=' + Geral.FF0(GraGru1),
        'AND tgg.Codigo=' + Geral.FF0(GraCusPrc),
        '']);
    end;
    tpGraCusPrc:
    begin
      //Custo
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, Dmod.MyDB, [
        'SELECT ggx.GraGruC, ggx.GraTamI, ggv.CustoPreco ',
        'FROM gragruval ggv ',
        'LEFT JOIN gragrux ggx ON ggx.Controle=ggv.GraGruX ',
        'WHERE ggv.GraGru1=' + Geral.FF0(GraGru1),
        'AND ggv.GraCusPrc=' + Geral.FF0(GraCusPrc),
        'AND ggv.Entidade=' + Geral.FF0(Entidade),
        '']);
    end;
  end;
end;

procedure TDmProd.ReopenGraGrXVal(GraGruX, GraCusPrc: Integer);
begin
  QrGraGrXVal.Close;
  QrGraGrXVal.Params[00].AsInteger := GraGruX;
  QrGraGrXVal.Params[01].AsInteger := GraCusPrc;
  UMyMod.AbreQuery(QrGraGrXVal, Dmod.MyDB, 'TDmProd.ReopenGraGrXVal()');
end;

procedure TDmProd.ReopenGraPckQtd(GraPckCad: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraPckQtd, Dmod.MyDB, [
  'SELECT ggx.GraGruC, ggx.GraTamI, gpq.Qtde ',
  'FROM grapckqtd gpq ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=gpq.GraGruX ',
  'WHERE gpq.Codigo=' + Geral.FF0(GraPckCad),
  sLineBreak]);
end;

procedure TDmProd.ReopenItsLotEtq(Lote: Integer);
begin
  QrItsLotEtq.Close;
  QrItsLotEtq.Params[0].AsInteger := Lote;
  UMyMod.AbreQuery(QrItsLotEtq, Dmod.MyDB, 'TDmProd.ReopenItsLotEtq()');
end;
(*&^
       tirar daqui e colocar no ModPediVda!
*)
{$IfNDef NAO_GFAT}
function TDmProd.ReopenLocRef(const TipoPesq: TTipoPesqProd; ItemPesq: Variant;
var Nivel1, Nivel2, Nivel3, PrdGrupTip, Controle: Integer; var Referencia: String;
var PerComissF, PerComissR: Double): Integer;
  procedure DefineVariaveis();
  begin
    Nivel1 := QrLocRefNivel1.Value;
    Nivel2 := QrLocRefNivel2.Value;
    Nivel3 := QrLocRefNivel3.Value;
    PrdGrupTip := QrLocRefPrdGrupTip.Value;
    Controle := QrLocRefControle.Value;
    Referencia := QrLocRefReferencia.Value;
    PerComissF := QrLocRefPerComissF.Value;
    PerComissR := QrLocRefPerComissR.Value;
  end;
begin
  QrLocRef.Close;
  QrLocRef.SQL.Clear;
  QrLocRef.SQL.Add('SELECT gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, gg1.PrdgrupTip,');
  QrLocRef.SQL.Add('ggx.Controle, CONCAT(gg1.Nome,');
  QrLocRef.SQL.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  QrLocRef.SQL.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  QrLocRef.SQL.Add('NO_PRD_TAM_COR, gg1.PerComissF, gg1.PerComissR,');
  QrLocRef.SQL.Add('gg1.GraTamcad, gg1.Referencia, gg1.UsaSubsTrib ');
  QrLocRef.SQL.Add('FROM gragrux ggx');
  QrLocRef.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  QrLocRef.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  QrLocRef.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  QrLocRef.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrLocRef.SQL.Add('WHERE ggx.Controle > -900000');
  if TipoPesq = tpRef then
    QrLocRef.SQL.Add('AND gg1.Referencia="' + ItemPesq + '"')
  else
  if TipoPesq = tpGGX then
    QrLocRef.SQL.Add('AND ggx.Controle=' + FormatFloat('0', ItemPesq))
  else
    QrLocRef.SQL.Add('AND gg1.???=???');
  QrLocRef.SQL.Add('ORDER BY NO_PRD_TAM_COR, ggx.Controle');
  UnDmkDAC_PF.AbreQuery(QrLocRef, Dmod.MyDB);
  //
  Nivel1 := 0;
  Nivel2 := 0;
  Nivel3 := 0;
  PrdGrupTip := 0;
  Controle := 0;
  Referencia := '';
  PerComissF := 0.00;
  PerComissR := 0.00;
  //
  Result := QrLocRef.RecordCount;
  case Result of
    0:
    begin
      case TipoPesq of
        tpRef: Geral.MensagemBox(
               'N�o foi localizado nenhum produto com a refer�ncia:'
                + slineBreak + ItemPesq, 'Aviso', MB_OK+MB_ICONWARNING);
        tpGGX: Geral.MensagemBox(
               'N�o foi localizado nenhum produto para o reduzido:'
                + slineBreak + FormatFloat('0', ItemPesq), 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    1: DefineVariaveis();
    else
    begin
      //Seleciona Reduzido (quando mais de um com a mesma refer�ncia)
      MyObjects.CriaForm_AcessoTotal(TFmFatSelRef, FmFatSelRef);
      FmFatSelRef.ShowModal;
      if FmFatSelRef.FSel then
      begin
        Result := 1;
        DefineVariaveis();
      end else
        Geral.MensagemBox('Pesquisa de produto cancelado pelo usu�rio!',
        'Aviso', MB_OK+MB_ICONWARNING);
      FmFatSelRef.Destroy;
    end;
  end;
end;
{$EndIf}

procedure TDmProd.ReopenOpcoesGrad();
begin
  UMyMod.AbreQuery(QrOpcoesGrad, Dmod.MyDB);
end;

procedure TDmProd.ReopenTabePrcGrI(GraGru1, TabePrcCab: Integer);
begin
  QrTabePrcGrI.Close;
  QrTabePrcGrI.Params[00].AsInteger := GraGru1;
  QrTabePrcGrI.Params[01].AsInteger := TabePrcCab;
  UMyMod.AbreQuery(QrTabePrcGrI, Dmod.MyDB, 'TDmProd.ReopenTabePrcGrI(');
end;

procedure TDmProd.ReopenTabePrcGrU(GraGruX, TabePrcCab: Integer);
begin
  QrTabePrcGrU.Close;
  QrTabePrcGrU.Params[00].AsInteger := TabePrcCab;
  QrTabePrcGrU.Params[01].AsInteger := GraGruX;
  UMyMod.AbreQuery(QrTabePrcGrU, Dmod.MyDB, 'TDmProd.ReopenTabePrcGrU(');
end;

procedure TDmProd.ReopenTabePrcGrX(GraGruX, TabePrcCab: Integer);
begin
  QrTabePrcGrX.Close;
  QrTabePrcGrX.Params[00].AsInteger := GraGruX;
  QrTabePrcGrX.Params[01].AsInteger := TabePrcCab;
  UMyMod.AbreQuery(QrTabePrcGrX, Dmod.MyDB, 'TDmProd.ReopenTabePrcGrX()');
end;

procedure TDmProd.ReopenQrAtribIts(SomenteSelecionados: Boolean;
PrdGrupTip: Integer);
begin
  DmProd.QrAtribIts.Close;
  DmProd.QrAtribIts.SQL.Clear;
  DmProd.QrAtribIts.SQL.Add('SELECT gac.Nome NOMEATRIBCAD, gai.Controle,');
  DmProd.QrAtribIts.SQL.Add('gai.Codigo, gai.CodUsu, gai.Nome NOMEATRIBITS');
  DmProd.QrAtribIts.SQL.Add('FROM graatrits gai');
  DmProd.QrAtribIts.SQL.Add('LEFT JOIN graatrcad gac ON gac.Codigo=gai.Codigo');
  if SomenteSelecionados then
  begin
    DmProd.QrAtribIts.SQL.Add('WHERE gac.Codigo IN (');
    DmProd.QrAtribIts.SQL.Add('  SELECT GraAtrCad');
    DmProd.QrAtribIts.SQL.Add('  FROM prdgrupatr');
    DmProd.QrAtribIts.SQL.Add('  WHERE Codigo=' + FormatFloat('0', PrdGrupTip));
    DmProd.QrAtribIts.SQL.Add(')');
  end;
  DmProd.QrAtribIts.SQL.Add('ORDER BY NOMEATRIBCAD, NOMEATRIBITS, gai.Codigo, gai.Controle');
  UMyMod.AbreQuery(DmProd.QrAtribIts, Dmod.MyDB, 'TDmProd.ReopenQrAtribIts()');
end;

procedure TDmProd.ReopenSdosPed(PediVda, GraGru1: Integer);
begin
  QrSdosPed.Close;
  QrSdosPed.Params[00].AsInteger := PediVda;
  QrSdosPed.Params[01].AsInteger := GraGru1;
  UMyMod.AbreQuery(QrSdosPed, Dmod.MyDB, 'TDmProd.ReopenSdosPed()');
end;

procedure TDmProd.ReopenSoliComprIts(SoliComprCab, Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoliComprIts, Dmod.MyDB, [
  'SELECT ggx.GraTamI, ggx.GraGruC, ggx.GraGru1, sci.* ',
  'FROM solicomprits sci ',
  'LEFT JOIN gragrux   ggx ON ggx.Controle=sci.GraGruX ',
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE sci.Codigo=' + Geral.FF0(SoliComprCab),
  'AND gg1.Nivel1=' + Geral.FF0(Nivel1),
  '']);
end;

procedure TDmProd.PesquisaPorCodigo(UnidMed: Integer; EdSigla: TdmkEdit);
begin
  QrPesq2.Close;
  QrPesq2.Params[0].AsString := Geral.FF0(UnidMed);
  UnDmkDAC_PF.AbreQuery(QrPesq2, Dmod.MyDB);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdSigla.ValueVariant <> QrPesq2Sigla.Value then
      EdSigla.ValueVariant := QrPesq2Sigla.Value;
  end else EdSigla.ValueVariant := '';
end;

procedure TDmProd.PesquisaPorSigla(Limpa: Boolean; EdSigla: TdmkEdit;
  EdUnidMed: TdmkEditCB; CBUnidMed: TdmkDBLookupComboBox);
begin
  QrPesq1.Close;
  QrPesq1.Params[0].AsString := EdSigla.Text;
  UnDmkDAC_PF.AbreQuery(QrPesq1, Dmod.MyDB);
  //
  if QrPesq1.RecordCount > 0 then
  begin
    if EdUnidMed.ValueVariant <> QrPesq1CodUsu.Value then
      EdUnidMed.ValueVariant := QrPesq1CodUsu.Value;
    if CBUnidMed.KeyValue     <> QrPesq1CodUsu.Value then
      CBUnidMed.KeyValue     := QrPesq1CodUsu.Value;
  end
  else if Limpa then
    EdSigla.ValueVariant := '';
end;

function TDmProd.PrecoItemCorTam(Nivel1, GraGruX, Lista, EntiPrecos: Integer;
  Preco: Double): Boolean;
var
  Controle: Integer;
  SQLType: TSQLType;
begin
  Result        := False;
  Screen.Cursor := crHourGlass;
  try
    if GraGruX <> 0 then
    begin
      QrLocGGV.Close;
      QrLocGGV.Params[00].AsInteger := GraGruX;
      QrLocGGV.Params[01].AsInteger := Nivel1;
      QrLocGGV.Params[02].AsInteger := Lista;
      QrLocGGV.Params[03].AsInteger := EntiPrecos;
      UnDmkDAC_PF.AbreQuery(QrLocGGV, Dmod.MyDB);
      //
      Controle := QrLocGGVControle.Value;
      //
      if Controle = 0 then
      begin
        SQLType  := stIns;
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                      'GraGruVal', 'GraGruVal', 'Controle');
      end else
        SQLType := stUpd;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
        'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'],
        [GraGruX, Nivel1, Lista, Preco, EntiPrecos], [Controle], True) then
      begin
        DmodG.AtualizaPrecosGraGruVal2(GraGruX, EntiPrecos);
      end;
    end;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmProd.PreparaGrades(Grade, Nivel1: Integer; StrGrids: array of TStringGrid): Boolean;
var
  Cols, Rows, i, j, k, c, l: Integer;
begin
  //Result := False;
  j := High(StrGrids);
  k := Low(StrGrids);
  //
  Cols := StrGrids[0].ColCount;
  Rows := StrGrids[0].RowCount;
  //
  for c := 0 to Cols - 1 do
    for l := 0 to Rows - 1 do
      for i := k to j do
        if StrGrids[i] <> nil then
          StrGrids[i].Cells[c,l] := '';
  (*
  if Grade > -1 then
  begin
  *)
    ReopenGradesCors(Nivel1);
    ReopenGradesTams(Grade);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    for i := k to j do
    begin
      if StrGrids[i] <> nil then
      begin
        StrGrids[i].Cells[c,l] := '';
        StrGrids[i].ColCount   := c;
        StrGrids[i].RowCount   := l;
        StrGrids[i].FixedCols  := 1;
        StrGrids[i].FixedRows  := 1;
      end;
    end;
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      for i := k to j do
      begin
        if StrGrids[i] <> nil then
        begin
          if Copy(StrGrids[i].Name, 1, 6) = 'GradeX' then
          begin
            StrGrids[i].Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value);
            // Ver o que fazer aqui com o planning novo!
            //StrGrids[i].Cells[1, QrGradesCors.RecNo] := IntToStr(QrGradesCorsGraCorCad.Value);
          end
          else
            StrGrids[i].Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
        end;
      end;
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      for i := k to j do
        if StrGrids[i] <> nil then
        begin
          if Copy(StrGrids[i].Name, 1, 6) = 'GradeX' then
            StrGrids[i].Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value)
          else
            StrGrids[i].Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
        end;
      QrGradesTams.Next;
    end;
    Result := True;
  (*
  end else begin
    for i := k to j do
    begin
      if StrGrids[i] <> nil then
      begin
        StrGrids[i].ColCount  := 0;
        StrGrids[i].RowCount  := 0;
      end;
    end;
  end;
  *)
end;

function TDmProd.PreparaGrades1(Grade, Nivel1: Integer; StrGrids: array of TStringGrid): Boolean;
var
  Cols, Rows, i, j, k, c, l: Integer;
begin
  Result := False;
  j := High(StrGrids);
  k := Low(StrGrids);
  //
  Cols := StrGrids[0].ColCount;
  Rows := StrGrids[0].RowCount;
  //
  for c := 0 to Cols - 1 do
    for l := 0 to Rows - 1 do
      for i := k to j do
        if StrGrids[i] <> nil then
          StrGrids[i].Cells[c,l] := '';
  if Grade > -1 then
  begin
    ReopenGradesCor2(Nivel1);
    ReopenGradesTam2(Grade);
    //
    c := QrGradesTam2.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCor2.RecordCount + 1;
    if l < 2 then l := 2;
    //
    for i := k to j do
    begin
      if StrGrids[i] <> nil then
      begin
        StrGrids[i].Cells[c,l] := '';
        StrGrids[i].ColCount  := c;
        StrGrids[i].RowCount  := l;
        StrGrids[i].FixedCols := 1;
        StrGrids[i].FixedRows := 1;
      end;
    end;
    QrGradesCor2.First;
    while not QrGradesCor2.Eof do
    begin
      for i := k to j do
      begin
        if StrGrids[i] <> nil then
        begin
          if Copy(StrGrids[i].Name, 1, 6) = 'GradeX' then
            StrGrids[i].Cells[0, QrGradesCor2.RecNo] := IntToStr(QrGradesCor2Cor.Value)
          else
            StrGrids[i].Cells[0, QrGradesCor2.RecNo] := QrGradesCor2NOMECOR.Value;
        end;
      end;
      QrGradesCor2.Next;
    end;
    //
    QrGradesTam2.First;
    while not QrGradesTam2.Eof do
    begin
      for i := k to j do
        if StrGrids[i] <> nil then
        begin
          if Copy(StrGrids[i].Name, 1, 6) = 'GradeX' then
            StrGrids[i].Cells[QrGradesTam2.RecNo, 0] := IntToStr(QrGradesTam2Tam.Value)
          else
            StrGrids[i].Cells[QrGradesTam2.RecNo, 0] := QrGradesTam2NOMETAM.Value;
        end;
      QrGradesTam2.Next;
    end;
    Result := True;
  end else begin
    for i := k to j do
    begin
      if StrGrids[i] <> nil then
      begin
        StrGrids[i].ColCount  := 0;
        StrGrids[i].RowCount  := 0;
      end;
    end;
  end;
end;

function TDmProd.PreparaGradesTripleCol(Grade, Nivel1: Integer;
  StrGrids: array of TStringGrid): Boolean;
const
  nCol = 3;
var
  Cols, Rows, i, j, k, c, l, c1, l1, c3, l3: Integer;
begin
  j := High(StrGrids);
  k := Low(StrGrids);
  //
  Cols := StrGrids[0].ColCount;
  Rows := StrGrids[0].RowCount;
  //
  for c := 0 to Cols - 1 do
    for l := 0 to Rows - 1 do
      for i := k to j do
        if StrGrids[i] <> nil then
          StrGrids[i].Cells[c,l] := '';
  //
  ReopenGradesCor3(Nivel1);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesCor3, Dmod.MyDB, [
  'SELECT gc.Controle Cor, cc.Nome NOMECOR ',
  'FROM gragruc gc ',
  'LEFT JOIN gracorcad cc ON cc.Codigo=gc.GraCorCad ',
  'WHERE gc.Nivel1=' + Geral.FF0(Nivel1),
  'ORDER BY NOMECOR ',
  EmptyStr]);
*)  //
  ReopenGradesTam3(Grade);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesTam3, Dmod.MyDB, [
  'SELECT Controle Tam, Nome NOMETAM ',
  'FROM gratamits ',
  'WHERE Codigo=' + Geral.FF0(Grade),
  EmptyStr]);
*)
  //----------------------------------------------------------------------------
  // 1 coluna da cor + 3 colunas para cada tamanho...
  c1 := QrGradesTam3.RecordCount + 1;
  c3 := (QrGradesTam3.RecordCount * nCol) + 1;
  if c1 < 2 then c := 2;
  if c3 < 4 then c3 := 4;
  //----------------------------------------------------------------------------
  // 2 linhas para os tamanhos e subtitulos + 1 linha no m�nimo para dados
  l1 := QrGradesCor3.RecordCount + 1;
  l3 := QrGradesCor3.RecordCount + 2;
  if l1 < 2 then l := 2;
  if l3 < 3 then l := 3;
  //----------------------------------------------------------------------------
  //
  for i := k to j do
  begin
    if StrGrids[i] <> nil then
    begin
      if (Copy(StrGrids[i].Name, 1, 6) = 'GradeA')
      or (Copy(StrGrids[i].Name, 1, 6) = 'GradeC')
      or (Copy(StrGrids[i].Name, 1, 6) = 'GradeX') then
      begin
        StrGrids[i].Cells[c1,l1] := '';
        StrGrids[i].ColCount   := c1;
        StrGrids[i].RowCount   := l1;
      end else
      begin
        StrGrids[i].Cells[c3,l3] := '';
        StrGrids[i].ColCount   := c3;
        StrGrids[i].RowCount   := l3;
      end;
      StrGrids[i].FixedCols  := 1;
      // T�tulo e subtitulos
      //StrGrids[i].FixedRows  := 1;
      StrGrids[i].FixedRows  := 2;
    end;
  end;
  QrGradesCor3.First;
  while not QrGradesCor3.Eof do
  begin
    for i := k to j do
    begin
      if StrGrids[i] <> nil then
      begin
        if  Copy(StrGrids[i].Name, 1, 6) = 'GradeX' then
          // S�o duas linhas para os titulos...
          //StrGrids[i].Cells[0, QrGradesCor3.RecNo] := IntToStr(QrGradesCor3Cor.Value)
          StrGrids[i].Cells[0, QrGradesCor3.RecNo + 1] := IntToStr(QrGradesCor3Cor.Value)
        else
          // S�o duas linhas para os titulos...
          //StrGrids[i].Cells[0, QrGradesCor3.RecNo] := QrGradesCor3NOMECOR.Value;
          StrGrids[i].Cells[0, QrGradesCor3.RecNo + 1] := QrGradesCor3NOMECOR.Value;
      end;
    end;
    QrGradesCor3.Next;
  end;
  //
  QrGradesTam3.First;
  while not QrGradesTam3.Eof do
  begin
    for i := k to j do
      if StrGrids[i] <> nil then
      begin
        if  Copy(StrGrids[i].Name, 1, 6) = 'GradeX' then
          StrGrids[i].Cells[QrGradesTam3.RecNo, 0] := IntToStr(QrGradesTam3Tam.Value)
          //StrGrids[i].Cells[((QrGradesTam3.RecNo - 1) * 3) + 1, 0] := IntToStr(QrGradesTam3Tam.Value)
        else
        if (Copy(StrGrids[i].Name, 1, 6) = 'GradeA')
        or (Copy(StrGrids[i].Name, 1, 6) = 'GradeC') then
        begin
          StrGrids[i].Cells[QrGradesTam3.RecNo, 0] := QrGradesTam3NOMETAM.Value;
        end else
        begin
          StrGrids[i].Cells[((QrGradesTam3.RecNo - 1) * 3) + 1, 0] := QrGradesTam3NOMETAM.Value;
          //
          StrGrids[i].Cells[((QrGradesTam3.RecNo - 1) * 3) + 1, 1] := 'Tam';
          StrGrids[i].Cells[((QrGradesTam3.RecNo - 1) * 3) + 2, 1] := 'Cor';
          StrGrids[i].Cells[((QrGradesTam3.RecNo - 1) * 3) + 3, 1] := 'Qtde';
        end;
      end;
    QrGradesTam3.Next;
  end;
  Result := True;
end;

procedure TDmProd.QrExport2AfterScroll(DataSet: TDataSet);
begin
  //BtSalva2.Enabled := (QrExport2.RecordCount = 0) and (MeExp.Text <> '');
  QrListErr1.Close;
  QrListErr1.Params[0].AsInteger := QrExport2My_Idx.Value;
  UMyMod.AbreQuery(QrListErr1, DModG.MyPID_DB);
end;

procedure TDmProd.QrExport2BeforeClose(DataSet: TDataSet);
begin
  QrListErr1.Close;
end;

procedure TDmProd.QrOpcoesGradAfterOpen(DataSet: TDataSet);
begin
//  ini 2019-01-22
(*
  if (QrOpcoesGrad.FieldByName('Codigo').AsInteger = 0 )
  or (QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = -1) then
*)
  if (QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = -1) then
//  Fim 2019-01-22
  begin
    if Geral.MB_Pergunta('Modo de cadastro de grade n�o definido!' + slineBreak +
      'V� para a janela de cadastro de grade, clique no bot�o "Op�oes".' + slineBreak +
      'Na janela "OPT-GRADE-000" marque ou desmarque a caixa de sele��o "Usa grade"' +
      slineBreak + 'e confirme clicando no bot�o "OK"!' + sLineBreak +
      'Deseja configurar agora?') = ID_YES then
    begin
      Grade_Jan.MostraFormOpcoesGrad;
    end;
  end;
end;

procedure TDmProd.QrOpcoesGradBeforeOpen(DataSet: TDataSet);
begin
end;

{
function TDmProd.PreparaGrades(Grade, Nivel1: Integer;
GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ: TStringGrid): Boolean;
var
  c, l: Integer;
begin
  Result := False;
  for c := 0 to GradeA.ColCount - 1 do
    for l := 0 to GradeA.RowCount - 1 do
    begin
      if GradeA  <> nil then GradeA.Cells[c, l] := '';
      if GradeC  <> nil then GradeC.Cells[c, l] := '';
      if GradeP1 <> nil then GradeP1.Cells[c, l] := '';
      if GradeP2 <> nil then GradeP2.Cells[c, l] := '';
      if GradeQ  <> nil then GradeQ.Cells[c, l] := '';
      if GradeX  <> nil then GradeX.Cells[c, l] := '';
    end;
  if Grade > -1 then
  begin
    ReopenGradesCors(Nivel1);
    ReopenGradesTams(Grade);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    if GradeA <> nil then
    begin
      GradeA.ColCount  := c;
      GradeA.RowCount  := l;
      GradeA.FixedCols := 1;
      GradeA.FixedRows := 1;
    end;
    //
    if GradeC <> nil then
    begin
      GradeC.ColCount  := c;
      GradeC.RowCount  := l;
      GradeC.FixedCols := 1;
      GradeC.FixedRows := 1;
    end;
    //
    if GradeP1 <> nil then
    begin
      GradeP1.ColCount  := c;
      GradeP1.RowCount  := l;
      GradeP1.FixedCols := 1;
      GradeP1.FixedRows := 1;
    end;
    //
    if GradeP2 <> nil then
    begin
      GradeP2.ColCount  := c;
      GradeP2.RowCount  := l;
      GradeP2.FixedCols := 1;
      GradeP2.FixedRows := 1;
    end;
    //
    if GradeQ <> nil then
    begin
      GradeQ.ColCount  := c;
      GradeQ.RowCount  := l;
      GradeQ.FixedCols := 1;
      GradeQ.FixedRows := 1;
    end;
    //
    if GradeX <> nil then
    begin
      GradeX.ColCount  := c;
      GradeX.RowCount  := l;
    end;
    //
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      if GradeA <> nil then
        GradeA.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeC <> nil then
        GradeC.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeP1 <> nil then
        GradeP1.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeP2 <> nil then
        GradeP2.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeQ <> nil then
        GradeQ.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeX <> nil then
        GradeX.Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value);
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      if GradeA <> nil then
        GradeA.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeC <> nil then
        GradeC.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeP1 <> nil then
        GradeP1.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeP2 <> nil then
        GradeP2.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeQ <> nil then
        GradeQ.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeX <> nil then
        GradeX.Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value);
      QrGradesTams.Next;
    end;
    Result := True;
  end else begin
    GradeA.ColCount := 1;
    GradeA.RowCount := 1;
    //
    GradeC.ColCount := 1;
    GradeC.RowCount := 1;
    //
    GradeP1.ColCount := 1;
    GradeP1.RowCount := 1;
    //
    GradeP2.ColCount := 1;
    GradeP2.RowCount := 1;
    //
    GradeQ.ColCount := 1;
    GradeQ.RowCount := 1;
    //
    GradeX.ColCount := 1;
    GradeX.RowCount := 1;
    //
  end;
end;
}

procedure TDmProd.CadastraSemGrade(PrdGrupTip: Integer; NomeProd, prod_cEAN,
              prod_NCM, prod_uCom, InfAdProd, ICMS_CST_A, ICMS_CST_B, IPI_CST,
              PIS_CST, COFINS_CST, prod_EXTIPI, IPI_cEnq: String; QrGraGruX:
              TmySQLQuery; EdGraGruX: TdmkEditCB; CBGraGruX: TdmkDBLookupComboBox);
var
  GraGruX: Integer;
begin
  if DBCheck.CriaFm(TFmPrdGruNewU, FmPrdGruNewU, afmoNegarComAviso) then
  begin
    FmPrdGruNewU.ImgTipo.SQLType := stIns;
    FmPrdGruNewU.EdPrdGrupTip.ValueVariant := PrdGrupTip;
    FmPrdGruNewU.CBPrdGrupTip.KeyValue     := PrdGrupTip;
    FmPrdGruNewU.FFocused := 0;
    if FmPrdGruNewU.PnNivel3.Enabled then
    begin
      FmPrdGruNewU.EdCodUsu3.ValueVariant := PrdGrupTip;
      FmPrdGruNewU.CBCodUsu3.KeyValue     := PrdGrupTip;
      if FmPrdGruNewU.CBCodUsu3.Text = '' then
      begin
        FmPrdGruNewU.EdCodUsu3.ValueVariant := Null;
        FmPrdGruNewU.CBCodUsu3.KeyValue     := Null;
      end;
      FmPrdGruNewU.FFocused := 3;
    end;
    if FmPrdGruNewU.PnNivel2.Enabled then
    begin
      FmPrdGruNewU.EdCodUsu2.ValueVariant := PrdGrupTip;
      FmPrdGruNewU.CBCodUsu2.KeyValue     := PrdGrupTip;
      if FmPrdGruNewU.CBCodUsu2.Text = '' then
      begin
        FmPrdGruNewU.EdCodUsu2.ValueVariant := Null;
        FmPrdGruNewU.CBCodUsu2.KeyValue     := Null;
      end else
      begin
        //
      end;
      FmPrdGruNewU.FFocused := 2;
    end;
    if FmPrdGruNewU.PnNivel1.Enabled then
    begin
      FmPrdGruNewU.EdNome1.Text := NomeProd;
      FmPrdGruNewU.FFocused := 1;
      //
      if prod_NCM <> '' then
      begin
        FmPrdGruNewU.EdNCM.Text := Geral.FormataNCM(prod_NCM);
      end;
      if prod_EXTIPI <> '' then
        FmPrdGruNewU.EdEX_TIPI.Text := prod_EXTIPI;
      if prod_uCom <> '' then
      begin
        FmPrdGruNewU.EdSigla.Text := prod_uCom;
        FmPrdGruNewU.PesquisaPorSigla(False, True);
      end;
      if prod_cEAN <> '' then
        FmPrdGruNewU.EdcGTIN_EAN.Text := prod_cEAN;
      if InfAdProd <> '' then
        FmPrdGruNewU.EdInfAdProd.Text := InfAdProd;
      if ICMS_CST_A <> '' then
        FmPrdGruNewU.EdCST_A.Text := ICMS_CST_A;
      if ICMS_CST_B <> '' then
        FmPrdGruNewU.EdCST_B.Text := ICMS_CST_B;
      if IPI_CST <> '' then
        FmPrdGruNewU.EdIPI_CST.Text := IPI_CST;
      if IPI_cEnq <> '' then
        FmPrdGruNewU.EdIPI_cEnq.Text := IPI_cEnq;
      if PIS_CST <> '' then
        FmPrdGruNewU.EdPIS_CST.Text := PIS_CST;
      if COFINS_CST <> '' then
        FmPrdGruNewU.EdCOFINS_CST.Text := COFINS_CST;
    end;
    FmPrdGruNewU.ShowModal;
    GraGruX := FmPrdGruNewU.FGraGruX;
    FmPrdGruNewU.Destroy;
    //
    if GraGruX <> 0 then
    begin
      if QrGraGruX <> nil then
      begin
        QrGraGruX.Close;
        UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
      end;
      if EdGraGruX <> nil then
        EdGraGruX.ValueVariant := GraGruX;
      if CBGraGruX <> nil then
        CBGraGruX.KeyValue := GraGruX;
    end;
  end;
end;

procedure TDmProd.CalculaCustoProduto(EdUsoQtd, EdUsoCusUni, EdUsoCusTot:
TdmkEdit);
var
  UsoQtd, UsoCusUni, UsoCusTot: Double;
begin
  UsoQtd         := EdUsoQtd.ValueVariant;
  UsoCusUni      := EdUsoCusUni.ValueVariant;
  UsoCusTot      := UsoQtd * UsoCusUni;
  //
  EdUsoCusTot.ValueVariant := UsoCusTot;
end;

// Somente c�digos e ativos
procedure TDmProd.ConfigGrades0(Grade, Nivel1: Integer;
GradeA, GradeX, GradeC: TStringGrid);
begin
  if PreparaGrades(Grade, Nivel1, [GradeA, GradeX, GradeC]) then
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
end;

// Somente c�digos e ativos - Blue Derm
procedure TDmProd.ConfigGrades10(Grade, Nivel1: Integer;
GradeA, GradeX, GradeC, GradeQI, GradeQA, GradeQK: TStringGrid);
begin
  if PreparaGrades(Grade, Nivel1,
    [GradeA, GradeX, GradeC, GradeQI, GradeQA, GradeQK]) then
      AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
end;

// Pre�os por lista e estoque
procedure TDmProd.ConfigGrades1(Grade, Nivel1, Tabela, Entidade: Integer;
GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeE: TStringGrid);
begin
  if PreparaGrades(Grade, Nivel1,
    [GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeE]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    if GradeP1 <> nil then
      AtualizaGradesPrecos1(Nivel1, Tabela, Entidade, GradeP1, tpGraCusPrc);
    if GradeP2 <> nil then
      AtualizaGradesPrecos1(Nivel1, Tabela, Entidade, GradeP2, tpGraCusPrc);
    if GradeE <> nil then
      AtualizaGradesEstoqueGeral(Entidade, Nivel1, GradeE);
  end;
end;

// pre�os por lista e prazo
procedure TDmProd.ConfigGrades2(Grade, Nivel1, Tabela: Integer;
GradeA, GradeX, GradeC, GradeP: TStringGrid; Variacao: Double);
begin
  if PreparaGrades(Grade, Nivel1, [GradeA, GradeX, GradeC, GradeP]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesPrecos2(Nivel1, Tabela, GradeP, Variacao);
  end;
end;

// Selecionar quantidades  // Impress�o de etiquetas
procedure TDmProd.ConfigGrades3(Grade, Nivel1: Integer;
GradeA, GradeX, GradeC, GradeQ: TStringGrid);
begin
  if PreparaGrades(Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ]) then
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
end;

// Quantidades selecionadas // Impress�o de etiquetas
procedure TDmProd.ConfigGrades4(Grade, Nivel1, Lote: Integer;
GradeA, GradeX, GradeC, GradeQ: TStringGrid);
begin
  if PreparaGrades(Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesQtdEtq1(Nivel1, Lote, GradeQ);
  end;
end;

// Selecionar quantidades  // Pedido por Tabela de Pre�o
procedure TDmProd.ConfigGrades5(Grade, Nivel1, Tabela, CondicaoPG: Integer;
GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ, GradeD: TStringGrid;
MedDDSimpl, MedDDReal: Double; ST1, ST2, ST3: TStaticText;
ImgTipo: TdmkImage; SemPrazo: Boolean; TipMediaDD, FatSemPrcL: Integer);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ, GradeD]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesPrecos3(Nivel1, Tabela, CondicaoPG, GradeC, GradeP1, GradeP2,
    MedDDSimpl, MedDDReal, ST1, ST2, ST3, ImgTipo, SemPrazo,
    TipMediaDD, FatSemPrcL);
  end;
end;

// Selecionar quantidades  // Pedido por Lista de pre�os de produto
procedure TDmProd.ConfigGrades9(Grade, Nivel1, Lista, Entidade: Integer;
GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ, GradeD,
Grade0, Grade1, Grade2, Grade3, Grade4: TStringGrid;
CustoFin: Double; ST1, ST2, ST3: TStaticText;
ImgTipo: TdmkImage);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeP1, GradeP2, GradeQ, GradeD,
  Grade0, Grade1, Grade2, Grade3, Grade4]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesPrecos4(Nivel1, Lista, Entidade, GradeC, GradeP1, GradeP2, CustoFin);
  end;
end;

procedure TDmProd.ConfigGradesFiConsSpc(GraGru1, GraTamCad, GG1Ori: Integer; GradeA,
  GradeC, GradeX, GradeK: TStringGrid);
begin
  if (GradeA <> nil) and (GradeC <> nil) then
  begin
    PreparaGrades(GraTamCad, GraGru1, [GradeA, GradeC, GradeX, GradeK]);
    AtualizaGradesAtivos1(GraGru1, GradeA, GradeC);
  end
  else begin
    PreparaGrades(GraTamCad, GraGru1, [GradeK]);
    //AtualizaGradesAtivos1(GraGru1, nil, nil);
  end;
  //AtualizaGradesFiConsSpc(GraGru1, GG1Ori, GradeK);
end;

procedure TDmProd.ConfigGradesFiConsSpcTripleCol(GraGru1Dest, GraTamCadDest:
  Integer; GradeA, GradeC, GradeX, GradeK: TStringGrid; FixedCols, FixedRows:
  Integer);
begin
  if (GradeA <> nil) and (GradeC <> nil) then
  begin
    PreparaGradesTripleCol(GraTamCadDest, GraGru1Dest, [GradeA, GradeC, GradeX, GradeK]);
    AtualizaGradesAtivos1(GraGru1Dest, GradeA, GradeC);
  end
  else begin
    PreparaGrades(GraTamCadDest, GraGru1Dest, [GradeK]);
    //AtualizaGradesAtivos1(GraGru1, nil, nil);
  end;
  //if Preenche then
    //AtualizaGradesFiConsSpcTripleCol(GraGru1, GradeK, FixedCols, FixedRows);
end;

procedure TDmProd.ConfigGradesPack1(GraGru1, GraTamCad, GraPckCad: Integer;
  GradeA, GradeC, GradeX, GradeK: TStringGrid);
begin
  if (GradeA <> nil) and (GradeC <> nil) then
  begin
    PreparaGrades(GraTamCad, GraGru1, [GradeA, GradeC, GradeX, GradeK]);
    AtualizaGradesAtivos1(GraGru1, GradeA, GradeC);
  end
  else begin
    PreparaGrades(GraTamCad, GraGru1, [GradeK]);
    //AtualizaGradesAtivos1(GraGru1, nil, nil);
  end;
  AtualizaGradesPack1(GraGru1, GraPckCad, GradeK);
end;

procedure TDmProd.ConfiguraOutrasMedidas(EditQtde, EditPecas, EditPLE,
  EditAreaM2, EditAreaP2: TdmkEdit; GrandezaUnidMed: Integer);
begin
  case GrandezaUnidMed of
    0: //Pe�a
    begin
      if EditPecas.Enabled then
        EditPecas.ValueVariant  := EditQtde.ValueVariant;
      EditPLE.ValueVariant    := 0;
      EditAreaM2.ValueVariant := 0;
      EditAreaP2.ValueVariant := 0;
    end;
    1: //�rea (m�)
    begin
      if EditAreaM2.Enabled then
        EditAreaM2.ValueVariant := EditQtde.ValueVariant;
      EditPecas.ValueVariant  := 0;
      EditPLE.ValueVariant    := 0;
      EditAreaP2.ValueVariant := 0;
    end;
    2, 7: //Peso (kg, t)
    begin
      if EditPLE.Enabled then
        EditPLE.ValueVariant    := EditQtde.ValueVariant;
      EditPecas.ValueVariant  := 0;
      EditAreaM2.ValueVariant := 0;
      EditAreaP2.ValueVariant := 0;
    end;
    6: //�rea (ft�)
    begin
      if EditAreaP2.Enabled then
        EditAreaP2.ValueVariant := EditQtde.ValueVariant;
      EditPecas.ValueVariant  := 0;
      EditPLE.ValueVariant    := 0;
      EditAreaM2.ValueVariant := 0;
    end;
    else
    begin
      EditPecas.ValueVariant  := 0;
      EditPLE.ValueVariant    := 0;
      EditAreaM2.ValueVariant := 0;
      EditAreaP2.ValueVariant := 0;
    end;
  end;
end;

procedure TDmProd.ConfiguraPrecoDeCusto(Entidade: Integer;
  RGTipoPreco: TRadioGroup; EdGraCusPrc: TdmkEditCB;
  CBGraCusPrc: TdmkDBLookupComboBox);
begin
  QrGraOptEnt.Close;
  QrGraOptEnt.Params[0].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQuery(QrGraOptEnt, Dmod.MyDB);
  if QrGraOptEnt.RecordCount > 0 then
  begin
    RGTipoPreco.ItemIndex    := QrGraOptEntBUCTipoPreco.Value;
    EdGraCusPrc.ValueVariant := QrGraOptEntBUCGraCusPrc.Value;
    CBGraCusPrc.KeyValue     := QrGraOptEntBUCGraCusPrc.Value;
  end;
end;

procedure TDmProd.CorrigeFatID_Venda(PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
  procedure CorrigeNFe(Tabela: String);
  var
    Erros: Integer;
  begin
    Erros := 0;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + Tabela);
      Dmod.QrUpd.SQL.Add('SET FatID=1');
      Dmod.QrUpd.SQL.Add('WHERE FatID=103');
      Dmod.QrUpd.ExecSQL;
    except
      Geral.MB_ERRO('N�o foi poss�vel atualizar a tabela: ' + Tabela + slineBreak +
      'Ser� atualizado item a item!');
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFeX, Dmod.MyDB, [
      'SELECT DISTINCT FatID, FatNum, Empresa ',
      'FROM ' + Tabela,
      'WHERE FatID=103 ',
      'ORDER BY FatNum, nItem ',
      '']);
      //
      QrNFeX.First;
      PB.Position := 0;
      PB.Max := QrNFeX.RecordCount;
      while not QrNFeX.Eof do
      begin
        try
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + Tabela,
          'SET FatID=1 ',
          'WHERE FatID=103 ',
          'AND FatNum=' + Geral.FF0(QrNFeXFatNum.Value),
          'AND Empresa=' + Geral.FF0(QrNFeXEmpresa.Value),
          '']);
        except
          Erros := Erros + 1;
          MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
          'Atualizando Tabela: ' + Tabela + ' > FatNum: ' +
          Geral.FF0(QrYFatNum.Value) + '  -  ' +
          'Erros: ' + Geral.FF0(Erros));
        end;
        //
        QrNFeX.Next;
      end;
    end;
  end;
  procedure CorrigeSMI(Tabela: String);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + Tabela);
    Dmod.QrUpd.SQL.Add('SET Tipo=113');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=103');
    Dmod.QrUpd.ExecSQL;
  end;
  procedure CorrigeCabY();
  var
    Lancto, Sub, FatParcela, Controle, Match: Integer;
  begin
    Match := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(QrY, Dmod.MyDB, [
    'SELECT DISTINCT FatID, FatNum, Empresa ',
    'FROM nfecaby ',
    'WHERE FatParcela=0 ',
    'ORDER BY FatNum ',
    '']);
    //
    PB.Position := 0;
    PB.Max := QrY.RecordCount;
    QrY.First;
    while not QrY.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrYIts, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM nfecaby ',
      'WHERE FatID=' + Geral.FF0(QrYFatID.Value),
      'AND FatNum=' + Geral.FF0(QrYFatNum.Value),
      'AND Empresa=' + Geral.FF0(QrYEmpresa.Value),
      'ORDER BY Controle ',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrYLct, Dmod.MyDB, [
      'SELECT Controle, Sub, FatParcela ',
      'FROM lct0001a ',
      'WHERE FatID=' + Geral.FF0(QrYFatID.Value),
      'AND FatNum=' + Geral.FF0(QrYFatNum.Value),
      'AND CliInt=' + Geral.FF0(QrYEmpresa.Value),
      'ORDER BY FatParcela ',
      '']);
      //
      if QrYLct.RecordCount = QrYIts.RecordCount then
      begin
        Match := Match + 1;
        //
        MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
        'Atualizando FatNum: ' + Geral.FF0(QrYFatNum.Value) + '  -  ' +
        'Combinados: ' + Geral.FF0(Match));
        //
        QrYIts.First;
        QrYLct.First;
        while not QrYIts.Eof do
        begin
          Lancto     := QrYLctControle.Value;
          Sub        := QrYLctSub.Value;
          //FatParcela := QrYLctFatParcela.Value;
          FatParcela := QrYIts.RecNo;
          Controle   := QrYItsControle.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaby', False, [
          'Lancto', 'Sub', 'FatParcela'], [
          'Controle'], [
          Lancto, Sub, FatParcela], [
          Controle], True);
          //
          QrYIts.Next;
          QrYLct.Next;
        end;
      end else
      begin
        MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
        'Atualizando FatNum: ' + Geral.FF0(QrYFatNum.Value) + '  -  ' +
        'Combinados: ' + Geral.FF0(Match));
        //
        QrYIts.First;
        while not QrYIts.Eof do
        begin
          FatParcela := QrYIts.RecNo;
          Controle   := QrYItsControle.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaby', False, [
          'FatParcela'], [
          'Controle'], [
          FatParcela], [
          Controle], True);
          //
          QrYIts.Next;
          QrYLct.Next;
        end;
      end;
      QrY.Next;
    end;
    MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, False,
    'Atualiza��o finalizada! Combinados = ' + Geral.FF0(Match));
  end;
begin
  Screen.Cursor := crHourGlass;
(*
  QrErr103 . O p e n;
  PB1.Position := 0;
  PB1.Max := QrErr103.RecordCount;
  while not QrErr103.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    //
*)
    CorrigeNFe('nfecabamsg');
    CorrigeNFe('nfecabb');
    CorrigeNFe('nfecabf');
    CorrigeNFe('nfecabg');
    CorrigeNFe('nfecabxlac');
    CorrigeNFe('nfecabxreb');
    CorrigeNFe('nfecabxvol');
    CorrigeNFe('nfecaby');
    CorrigeNFe('nfecabzcon');
    CorrigeNFe('nfecabzfis');
    CorrigeNFe('nfecabzpro');

    CorrigeNFe('nfeitsi');
    CorrigeNFe('nfeitsidi');
    CorrigeNFe('nfeitsidia');
    CorrigeNFe('nfeitsm');
    CorrigeNFe('nfeitsn');
    CorrigeNFe('nfeitso');
    CorrigeNFe('nfeitsp');
    CorrigeNFe('nfeitsq');
    CorrigeNFe('nfeitsr');
    CorrigeNFe('nfeitss');
    CorrigeNFe('nfeitst');
    CorrigeNFe('nfeitsu');
    CorrigeNFe('nfeitsv');
    CorrigeNFe('nfeitsva');
    // Deixar por �ltimo, caso de errro nos anteriores
    CorrigeNFe('nfecaba');
    //
(*
    QrErr103.Next;
  end;
  PB1.Position := 0;
*)
  CorrigeSMI('stqmovitsa');
  CorrigeSMI('stqmovitsb');
  //
  // 2013-02-12
  CorrigeCabY();
  //
  Geral.MensagemBox('Corre��o finalizada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
  Screen.Cursor := crDefault;
end;

procedure TDmProd.DataModuleCreate(Sender: TObject);
begin
  FEnabledPnTipo := False;
end;

function TDmProd.DefineDescriGerBxaEstq(Index: Integer): String;
begin
  case Index of
    0: Result := '? ? ?';
    1: Result := 'Pe�a';
    2: Result := 'm�';
    3: Result := 'kg';
    4: Result := 't (ton)';
    5: Result := 'ft�';
    else Result := '#ERRO#';
  end;
end;

procedure TDmProd.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

function TDmProd.VerificaEstoqueTodoFaturamento(FatSemEstq, OriCodi, OriCnta,
  OriTipo, FisRegCad: Integer; _TipoCalc: TTipoCalcEstq = tceSemMovim): Boolean;
var
  Continua, Empresa, GraGruX, StqCenCad, Fator,
  Len, IDNivel1: Integer;
  Qtde: Double;
  Msg, Produto, Cor, Tam: String;
  Janela, Descricao: array of string;
  Qry: TmySQLQuery;
  TipoCalc: TTipoCalcEstq;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    //False := Estoque insuficiente
    //True  := Libera encerramento
    //
    Result        := False;
    Screen.Cursor := crHourGlass;
    try
      if FatSemEstq <> 1 then //Permite sem avisar
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry(*DModG.QrAux*), Dmod.MyDB, [
          'SELECT stq.OriCodi, stq.OriCnta, stq.Empresa, stq.GraGruX, ',
          'stq.StqCenCad, SUM(stq.Qtde * stq.Baixa) Qtde,  ',
          'gg1.Nome, gti.Nome Tam, gcc.Nome Cor, ggx.GraGru1 ',
          'FROM stqmovitsa stq ',
          'LEFT JOIN gragrux ggx ON ggx.Controle = stq.GraGruX ',
          'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
          'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
          'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
          'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
          'WHERE stq.Tipo=' + Geral.FF0(OriTipo),
          'AND stq.OriCodi=' + Geral.FF0(OriCodi),
          'AND stq.OriCnta=' + Geral.FF0(OriCnta),
          'GROUP BY stq.GraGruX ',
          ' ',
          'UNION ',
          ' ',
          'SELECT stq.OriCodi, stq.OriCnta, stq.Empresa, stq.GraGruX, ',
          'stq.StqCenCad, SUM(stq.Qtde * stq.Baixa) Qtde,  ',
          'gg1.Nome, gti.Nome Tam, gcc.Nome Cor, ggx.GraGru1 ',
          'FROM stqmovitsb stq',
          'LEFT JOIN gragrux ggx ON ggx.Controle = stq.GraGruX ',
          'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
          'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
          'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
          'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
          'WHERE stq.Tipo=' + Geral.FF0(OriTipo),
          'AND stq.OriCodi=' + Geral.FF0(OriCodi),
          'AND stq.OriCnta=' + Geral.FF0(OriCnta),
          'GROUP BY stq.GraGruX ',
          '']);
        if Qry(*DModG.QrAux*).RecordCount > 0 then
        begin
          Qry(*DModG.QrAux*).First;
          while not Qry(*DModG.QrAux*).Eof do
          begin
            Empresa   := Qry(*DModG.QrAux*).FieldByName('Empresa').AsInteger;
            GraGruX   := Qry(*DModG.QrAux*).FieldByName('GraGruX').AsInteger;
            StqCenCad := Qry(*DModG.QrAux*).FieldByName('StqCenCad').AsInteger;
            Qtde      := Qry(*DModG.QrAux*).FieldByName('Qtde').AsFloat;
            // ini 2023-08-10
            //TipoCalc  := DmProd.DefineTipoCalc(FisRegCad, StqCenCad, Empresa)
            if (FisRegCad <> 0) and (_TipoCalc = tceSemMovim) then
              TipoCalc  := TTipoCalcEstq(DmProd.DefineTipoCalc(FisRegCad, StqCenCad, Empresa))
            else
              TipoCalc := _TipoCalc;
            // fim 2023-08-10
            IDNivel1  := Qry(*DModG.QrAux*).FieldByName('GraGru1').AsInteger;
            Produto   := Qry(*DModG.QrAux*).FieldByName('Nome').AsString;
            Cor       := Qry(*DModG.QrAux*).FieldByName('Cor').AsString;
            Tam       := Qry(*DModG.QrAux*).FieldByName('Tam').AsString;
            //
            case TipoCalc of
                (*1*)tceAdiciona: Fator :=  1;
                (*2*)tceBaixa: Fator := -1;
              else Fator :=  0;
            end;
            Qtde := Qtde * Fator;
            //
            Continua := DmProd.VerificaEstoqueProduto(Qtde, Fator, GraGruX, StqCenCad,
                          FisRegCad, 0, 0, Empresa, FatSemEstq, True, Msg);
            //
            if Continua = ID_NO then
            begin
              Len := Length(Janela) + 1;
              //
              SetLength(Janela, Len);
              SetLength(Descricao, Len);
              //
              Janela[Len - 1]   := Geral.FF0(GraGruX) + ' - ' + Geral.FF0(IDNivel1) + ' - ' + Produto + ' ' + Tam + ' ' + Cor;
              Descricao[Len -1] := Msg;
            end;
            //
            Qry(*DModG.QrAux*).Next;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
      //
      if Len > 0 then
        DBCheck.MostraInfoSeq(Janela, Descricao, 'Reduzido - ID n�vel 1 - Nome / Tam / Cor', 'Descri��o', 450, 300);
      //
      case FatSemEstq of
          0: Result := Len = 0; //N�o permite negativo
          1: Result := True; //Permite negativo
          2: //Pergunta antes
          begin
            if Len > 0 then
            begin
              if Geral.MB_Pergunta('Deseja continuar?') = ID_YES then
                Result := True
              else
                Result := False;
            end else
              Result := True;
          end
        else
        begin
          Geral.MB_Erro('Valor da vari�vel: "FatSemEstq" inv�lido!');
          Result := False;
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmProd.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

function TDmProd.DefineTipoCalc(RegrFiscal, StqCenCad,
  Empresa: Integer; NaoExibeMsg: Boolean): Integer;
begin
  QrMvt.Close;
  QrMvt.SQL.Clear;
  QrMvt.SQL.Add('SELECT TipoCalc');
  QrMvt.SQL.Add('FROM fisregmvt frm');
  QrMvt.SQL.Add('WHERE frm.Codigo=:P0');
  QrMvt.SQL.Add('AND StqCenCad=:P1');
  QrMvt.SQL.Add('AND frm.Empresa=:P2');
  QrMvt.SQL.Add('');
  QrMvt.Params[00].AsInteger := RegrFiscal;
  QrMvt.Params[01].AsInteger := StqCenCad;
  QrMvt.Params[02].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrMvt, Dmod.MyDB);
  //
  if MyObjects.FIC((QrMvt.RecordCount = 0) and (not NaoExibeMsg), nil,
    'N�o foi poss�vel definir o tipo de movimenta��o de estoque ' + sLineBreak +
    'pelo centro de estoque da regra fiscal selecionada!')
  then
    Result := -1
  else
    Result := QrMvtTipoCalc.Value;
end;

// Mostrar selecionados // Pedido Normal
procedure TDmProd.ConfigGrades6(Grade, Nivel1, Pedido: Integer;
GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesPedidos(Nivel1, Pedido,
      GradeC, GradeQ, GradeF, GradeD, GradeV);
  end;
end;

// Mostrar selecionados // Pedido Condicional
procedure TDmProd.ConfigGrades6A(Grade, Nivel1, Condicional: Integer;
GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesCondicional_Saida(Nivel1, Condicional,
      GradeC, GradeQ, GradeF, GradeD, GradeV);
  end;
end;

// Mostrar selecionados // Faturamento Condicional
procedure TDmProd.ConfigGrades6B(Grade, Nivel1, Condicional: Integer;
GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesCondicional_Retorno(Nivel1, Condicional,
      GradeC, GradeQ, GradeF, GradeD, GradeV);
  end;
end;

// Mostrar selecionados // Venda Direta
procedure TDmProd.ConfigGrades6C(Grade, Nivel1, FatVen: Integer;
GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesFatVen(Nivel1, FatVen,
      GradeC, GradeQ, GradeF, GradeD, GradeV);
  end;
end;

// Mostrar selecionados // Faturamento
procedure TDmProd.ConfigGrades7(Grade, Nivel1, PedCod, FatCod: Integer;
GradeA, GradeX, GradeC, GradeQ, GradeP: TStringGrid);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ, GradeP]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesFatPed(Nivel1, PedCod, FatCod,
      GradeC, GradeQ, GradeP);
  end;
end;

// Mostrar selecionados // Movimento avulso
procedure TDmProd.ConfigGrades8(Grade, Nivel1, StqManCad: Integer;
GradeA, GradeX, GradeC, GradeQ: TStringGrid);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesStqMovIts(99, Nivel1, StqManCad, GradeC, GradeQ);
  end;
end;

// Selecionar quantidades  // Impress�o de etiquetas
procedure TDmProd.ConfigGrades11(Condicional, Grade, Nivel1: Integer;
GradeA, GradeX, GradeC, GradeQ, GradeV: TStringGrid);
begin
  if PreparaGrades(Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ, GradeV]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesRetornoCondicional(Nivel1, Condicional, GradeV);
  end;
end;

procedure TDmProd.ConfigGrades12(Grade, Nivel1, GraCusPrc, Entidade: Integer;
  GradeA, GradeX, GradeC, GradeP, GradeQ: TStringGrid);
begin
  if PreparaGrades(Grade, Nivel1, [GradeA, GradeX, GradeC, GradeP, GradeQ]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesPrecos1(Nivel1, GraCusPrc, Entidade, GradeP, tpGraCusPrc);
  end;
end;

procedure TDmProd.ConfigGrades13(Grade, Nivel1, SoliComprCab: Integer; GradeA,
  GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
begin
  if PreparaGrades(
  Grade, Nivel1, [GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV]) then
  begin
    AtualizaGradesAtivos1(Nivel1, GradeA, GradeC);
    AtualizaGradesSoliCompr(Nivel1, SoliComprCab,
      GradeC, GradeQ, GradeF, GradeD, GradeV);
  end;
end;

function TDmProd.ExisteBalancoAberto_StqCen_Uni(PrdGrupTip, Empresa,
  StqCenCad: Integer): Boolean;
begin
  QrOpenX.Close;
  QrOpenX.SQL.Clear;
  QrOpenX.SQL.Add('SELECT CodUsu Codigo, Nome');
  QrOpenX.SQL.Add('FROM stqbalcad');
  QrOpenX.SQL.Add('WHERE Empresa=:P0');
  QrOpenX.SQL.Add('AND PrdGrupTip=:P1');
  QrOpenX.SQL.Add('AND StqCenCad=:P2');
  QrOpenX.SQL.Add('AND Encerrou<2');
  QrOpenX.Params[00].AsInteger := Empresa;
  QrOpenX.Params[01].AsInteger := PrdGrupTip;
  QrOpenX.Params[02].AsInteger := StqCenCad;
  UMyMod.AbreQuery(QrOpenX, Dmod.MyDB, 'TDmProd.ExisteBalancoAberto_StqCen_Uni()');
  Result := QrOpenX.RecordCount > 0;
  if Result then
    MostraStqMovImped(tsmiBalanco);
end;

function TDmProd.ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa,
  fisRegCad: Integer): Boolean;
begin
  QrOpenX.Close;
  QrOpenX.SQL.Clear;
  QrOpenX.SQL.Add('SELECT CodUsu Codigo, Nome');
  QrOpenX.SQL.Add('FROM stqbalcad');
  QrOpenX.SQL.Add('WHERE Empresa=:P0');
  QrOpenX.SQL.Add('AND PrdGrupTip=:P1');
  QrOpenX.SQL.Add('AND StqCenCad IN ');
  QrOpenX.SQL.Add('(');
  QrOpenX.SQL.Add('  SELECT StqCenCad');
  QrOpenX.SQL.Add('  FROM fisregmvt');
  QrOpenX.SQL.Add('  WHERE Codigo=:P2');
  QrOpenX.SQL.Add('  AND Empresa=:P3');
  QrOpenX.SQL.Add(')');
  QrOpenX.SQL.Add('AND Encerrou<2');
  QrOpenX.Params[00].AsInteger := Empresa;
  QrOpenX.Params[01].AsInteger := PrdGrupTip;
  QrOpenX.Params[02].AsInteger := FisRegCad;
  QrOpenX.Params[03].AsInteger := Empresa;
  UMyMod.AbreQuery(QrOpenX, Dmod.MyDB, 'TDmProd.ExisteBalancoAberto_StqCen_Mul()');
  //Geral.MB_Teste(QrOpenX.SQL.Text);
  Result := QrOpenX.RecordCount > 0;
  if Result then
    MostraStqMovImped(tsmiBalanco);
end;

function TDmProd.ExisteFatPedAberto(Empresa, StqCenCad: Integer): Boolean;
begin
  QrOpenX.Close;
  QrOpenX.SQL.Clear;
  QrOpenX.SQL.Add('SELECT fpc.CodUsu Codigo,');
  QrOpenX.SQL.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) Nome');
  QrOpenX.SQL.Add('FROM fatpedcab fpc');
  QrOpenX.SQL.Add('LEFT JOIN pedivda   pvd ON pvd.Codigo=fpc.Pedido');
  QrOpenX.SQL.Add('LEFT JOIN fisregmvt frm ON frm.Codigo=pvd.RegrFiscal');
  QrOpenX.SQL.Add('  AND frm.StqCenCad=:P0');
  QrOpenX.SQL.Add('  AND frm.Empresa=:P1');
  QrOpenX.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente');
  QrOpenX.SQL.Add('WHERE pvd.Empresa=:P1');
  QrOpenX.SQL.Add('AND fpc.Encerrou<2');
  QrOpenX.Params[00].AsInteger := StqCenCad;
  QrOpenX.Params[01].AsInteger := Empresa;
  QrOpenX.Params[02].AsInteger := Empresa;
  //QrOpenX.Params[??].AsInteger := PrdGrupTip;
  UMyMod.AbreQuery(QrOpenX, Dmod.MyDB, 'TDmProd.ExisteFatPedAberto()');
  //Geral.MB_Teste(QrOpenX.SQL.Text);
  Result := QrOpenX.RecordCount > 0;
  if Result then
    MostraStqMovImped(tsmiFaturamen);
end;

function TDmProd.ExisteStqClasseAberto(Empresa, StqCenCad: Integer): Boolean;
begin
  QrOpenX.Close;
  QrOpenX.SQL.Clear;
  QrOpenX.SQL.Add('SELECT Controle Codigo, Codigo CodUsu, Marca Nome ');
  QrOpenX.SQL.Add('FROM mpin ');
  QrOpenX.SQL.Add('WHERE EmClassif > 0 ');
  UMyMod.AbreQuery(QrOpenX, Dmod.MyDB, 'TDmProd.ExisteStqClasseAberto()');
  Result := QrOpenX.RecordCount > 0;
  if Result then
    MostraStqMovImped(tsmiClassifi);
end;

function TDmProd.ExisteStqManAberto(Empresa, StqCenCad: Integer): Boolean;
begin
  QrOpenX.Close;
  QrOpenX.SQL.Clear;
  QrOpenX.SQL.Add('SELECT smc.Codigo,');
  QrOpenX.SQL.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) Nome');
  QrOpenX.SQL.Add('FROM stqmancad smc');
  QrOpenX.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo = smc.Empresa');
  QrOpenX.SQL.Add('WHERE smc.Empresa=:P0');
  QrOpenX.SQL.Add('AND smc.StqCenCad=:P1');
  QrOpenX.SQL.Add('AND smc.Encerrou<2');
  QrOpenX.Params[00].AsInteger := Empresa;
  QrOpenX.Params[01].AsInteger := StqCenCad;
  UMyMod.AbreQuery(QrOpenX, Dmod.MyDB, 'TDmProd.ExisteStqManAberto()');
  Result := QrOpenX.RecordCount > 0;
  if Result then
    MostraStqMovImped(tsmiMovimenMan);
end;

function TDmProd.FormaNomeProduto(NomeGG1, NomeTam, NomeCor: String; ImprimeTam,
ImprimeCor: Byte): String;
var
  Tam, Cor: String;
begin
  if (ImprimeTam > 0) and (Trim(NomeTam) <> '') then
    Tam := ' ' + Trim(NomeTam)
  else
    Tam := '';
  //
  if (ImprimeCor > 0) and (Trim(NomeCor) <> '') then
    Cor := ' ' + Trim(NomeCor)
  else
    Cor := '';
  //
  Result := NomeGG1 + Tam + Cor;
end;

function TDmProd.ObtemCTB(Nivel2, Nivel3, Genero: Integer): String;
begin
  Result := '';
  //
  QrCTB.Close;
  QrCTB.SQL.Clear;
  QrCTB.SQL.Add('SELECT Genero');
  QrCTB.SQL.Add('FROM graplacta');
  QrCTB.SQL.Add('WHERE NivelX=2');
  QrCTB.SQL.Add('AND Nivel2=:P0');
  QrCTB.Params[0].AsInteger := Nivel2; // QrProdNivel2.Value;
  UnDmkDAC_PF.AbreQuery(QrCTB, Dmod.MyDB);
  //
  if QrCTB.RecordCount > 0 then
    Result := FormatFloat('0', QrCTBGenero.Value)
  else begin
    QrCTB.Close;
    QrCTB.SQL.Clear;
    QrCTB.SQL.Add('SELECT Genero');
    QrCTB.SQL.Add('FROM graplacta');
    QrCTB.SQL.Add('WHERE NivelX=3');
    QrCTB.SQL.Add('AND Nivel2=:P0');
    QrCTB.SQL.Add('AND Nivel3=:P1');
    QrCTB.Params[00].AsInteger := Nivel2; // QrProdNivel2.Value;
    QrCTB.Params[01].AsInteger := Nivel3; // QrProdNivel3.Value;
    UnDmkDAC_PF.AbreQuery(QrCTB, Dmod.MyDB);
    //
    if QrCTB.RecordCount > 0 then
      Result := FormatFloat('0', QrCTBGenero.Value)
    else
    begin
      {
      if QrProdGenero.Value <> 0 then
        Result := FormatFloat('0', QrProdGenero.Value);
      }
      if Genero <> 0 then
        Result := FormatFloat('0', Genero);
    end;
  end;
  Result := FormatFloat('00000', Geral.IMV(Result));
end;

function TDmProd.ObtemCTB_peloGGX(GraGruX: Integer): String;
begin
(*
  QrProd.Close;
  QrProd.Params[0].AsInteger := QrItsIGraGruX.Value;
  QrProd. O p e n ; 2022-02-20 - Antigo . O p e n ;
*)
  ReopenProd(GraGruX);
  Result := DmProd.ObtemCTB(
        QrProdNivel2.Value, QrProdNivel3.Value, QrProdGenero.Value);
end;

function TDmProd.ObtemEstqueGGXdeEntidade(GraGruX, Entidade: Integer): Double;
begin
  QrStqPrdEmp.Close;
  QrStqPrdEmp.Params[00].AsInteger := GraGruX;
  QrStqPrdEmp.Params[01].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQuery(QrStqPrdEmp, Dmod.MyDB);
  //
  Result := QrStqPrdEmpQTDE.Value;
end;

function TDmProd.ObtemGraCorCadDeIdx(Nivel1: Integer; Grupo, NomeItem: String): Integer;
const
  Familia = 0;
  Pantone = 0;
var
  GraCorCad: Integer;
begin
  Result := 0;
  QrGraCorIdx.Close;
  QrGraCorIdx.Params[0].AsString := Grupo;
  UnDmkDAC_PF.AbreQuery(QrGraCorIdx, Dmod.MyDB);
  //
  if QrGraCorIdx.RecordCount = 0 then
  begin
    GraCorCad := DModG.BuscaProximoInteiro_Negativo(Dmod.MyDB, 'GraCorCad', 'Codigo', '', 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gracoridx', False, [
    'GraCorCad'], ['Codigo', 'Grupo'], [
    GraCorCad], [Nivel1, Grupo], True) then
    //
    Result := GraCorCad;
    try
    // Tenta criar caso n�o exista!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gracorcad', False, [
      'CodUsu', 'Nome', 'Familia', 'Pantone'], ['Codigo'], [
      GraCorCad, NomeItem, Familia, Pantone], [GraCorCad], True) then
        ;//Result := GraCorCad;
    except
      Geral.MensagemBox('N�o foi poss�vel criar a grade de cores n�mero: ' +
      IntToStr(GraCorCad) + slineBreak + 'AVISE A DERMATEK', 'Erro', MB_OK+MB_ICONERROR);
    end;
  end else Result := QrGraCorIdxGraCorCad.Value;
end;

function TDmProd.ObtemGraGru1DeGraGruX(GraGruX: Integer): Integer;
var
  Controle: Integer;
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGru1 ',
    'FROM gragrux ',
    'WHERE Controle=' + Geral.FF0(GraGruX),
    '']);
    Result := Qry.FieldByName('GraGru1').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TDmProd.ObtemGraGruCDeIdx(Nivel1: Integer; Grupo, NomeItem: String): Integer;
var
  GraCorCad: Integer;
  Controle: Integer;
begin
  //Result := 0;
  GraCorCad := ObtemGraCorCadDeIdx(Nivel1, Grupo, NomeItem);
  //
  Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', stIns, 0);
  //
  Result := Controle;
  try
    // Tenta criar caso n�o exista!
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
    'Nivel1', 'GraCorCad'], ['Controle'], [
     Nivel1, GraCorCad], [Controle], True) then
     ;//Result := Controle;
  except
    Geral.MensagemBox('N�o foi poss�vel criar o ID de cor: ' +
    IntToStr(Controle) + slineBreak + 'AVISE A DERMATEK', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TDmProd.ObtemNovoGraGruXdeExcluido(const GGXExluido: Integer;
  var GGXSubstituto, NivelExclusao: Integer);
begin
  GGXSubstituto := 0;
  NivelExclusao := 0;
  QrGraGruDel.Close;
  QrGraGruDel.Params[0].AsInteger := GGXExluido;
  UnDmkDAC_PF.AbreQuery(QrGraGruDel, Dmod.MyDB);
  //
  while (QrGraGruDelReceptor.Value <> 0) and (QrGraGruDelControle.Value = 0) do
  begin
    NivelExclusao := NivelExclusao + 1;
    GGXSubstituto := QrGraGruDelReceptor.Value;
    //
    QrGraGruDel.Close;
    QrGraGruDel.Params[0].AsInteger := GGXSubstituto;
    UnDmkDAC_PF.AbreQuery(QrGraGruDel, Dmod.MyDB);
  end;
  if QrGraGruDelReceptor.Value <> 0 then
  begin
    NivelExclusao := NivelExclusao + 1;
    GGXSubstituto := QrGraGruDelReceptor.Value;
  end;
end;

function TDmProd.ObtemPrecoGraGruX_GraCusPrc(GraGruX, GraCusPrc: Integer): Double;
begin
  //Result := 0;
  ReopenTabePrcGrX(GraGruX, GraCusPrc);
  if QrTabePrcGrX.RecordCount > 0 then
    Result := QrTabePrcGrXPreco.Value
  else begin
    ReopenTabePrcGrU(GraGruX, GraCusPrc);
      Result := QrTabePrcGrUPreco.Value
  end;
end;

function TDmProd.ObtemPrimeiroGraGruXdeGraGru1(const FatID, FatNum,
  nItem: Integer; const Nome: String; const DeixaEscolherSeNaoExiste: Boolean;
  var GraGru1, GraGruX: Integer): Integer;
var
  NomeFatID_NFe: String;
begin
  Result := 0;
  QrGGX.Close;
  QrGGX.Params[0].AsInteger := GraGru1;
  UnDmkDAC_PF.AbreQuery(QrGGX, Dmod.MyDB);
  //
  GraGruX := QrGGXControle.Value;
  if GraGruX <> 0 then
    Result := 2; // Perfeiro! Achou!
  //
  if Result = 0 then
  begin
    if DeixaEscolherSeNaoExiste then
    begin
      QrGraGruVinc.Close;
      QrGraGruVinc.Params[0].AsString := Nome;
      UnDmkDAC_PF.AbreQuery(QrGraGruVinc, Dmod.MyDB);
      GraGruX := QrGraGruVincGraGruX.Value;
      if GraGruX <> 0 then
      begin
        GraGru1 := QrGraGruVincGraGru1.Value;
        Result := 1;
      end;
      if Result = 0 then
      begin
        GraGruXGraGruVinc(Nome, GraGru1, GraGruX);
        if GraGruX <> 0 then
          Result := 1;
      end;
    end;
    if Result = 0 then
    begin
      NomeFatID_NFe := '???';
(* ini 2023-08-16
{$IFNDef semNFe_v0000}
      NomeFatID_NFe := DmNFe_0000.NomeFatID_NFe(FatID);
{$EndIf}
  *)
      NomeFatID_NFe := DmkEnums.NomeFatID_All(FatID);
// fim 2023-08-16
      Geral.MensagemBox('N�o foi localizado o "reduzido" do insumo n� ' +
      FormatFloat('0', GraGru1) + slineBreak + slineBreak +
      'Item da NF: ' + FormatFloat('0', nItem) + slineBreak +
      'Tipo: ' + NomeFatID_NFe + slineBreak +
      'C�digo: ' + FormatFloat('0', FatNum) + slineBreak +
      'Nome: ' + Nome,
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

function TDmProd.ObtemQtdePrazos(TabePrcCab: Integer): Integer;
begin
  QrMaxTams.Close;
  QrMaxTams.SQL.Clear;
  QrMaxTams.SQL.Add('SELECT COUNT(*) Itens');
  QrMaxTams.SQL.Add('FROM gratamits');
  if TabePrcCab > 0 then
  begin
    QrMaxTams.SQL.Add('WHERE Codigo IN (');
    QrMaxTams.SQL.Add('  SELECT DISTINCT gg1.GraTamCad');
    QrMaxTams.SQL.Add('  FROM tabeprcgrg tpg');
    QrMaxTams.SQL.Add('  LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpg.Nivel1');
    QrMaxTams.SQL.Add('  WHERE tpg.Codigo=' + FormatFloat('0', TabePrcCab));
    QrMaxTams.SQL.Add(')');
  end;
  QrMaxTams.SQL.Add('GROUP BY Codigo');
  QrMaxTams.SQL.Add('ORDER BY Itens Desc');
  QrMaxTams.SQL.Add('');
  UMyMod.AbreQuery(QrMaxTams, Dmod.MyDB, 'TDmProd.ObtemQtdePrazos()');
  Result := QrMaxTamsItens.Value;
end;

function TDmProd.ObtemQuantidadesBaixa(const TipoStqMovIts, OriCodi: Integer; var Qtde,
  Pecas, Peso, AreaM2, AreaP2: Double): Boolean;
begin
  //Result := False;
  Qtde   := 0;
  Pecas  := 0;
  Peso   := 0;
  AreaM2 := 0;
  AreaP2 := 0;
  //
  QrBxaA.Close;
  QrBxaA.Params[00].AsInteger := TipoStqMovIts;
  QrBxaA.Params[01].AsInteger := OriCodi;
  UnDmkDAC_PF.AbreQuery(QrBxaA, Dmod.MyDB);
  //
  QrBxaB.Close;
  QrBxaB.Params[00].AsInteger := TipoStqMovIts;
  QrBxaB.Params[01].AsInteger := OriCodi;
  UnDmkDAC_PF.AbreQuery(QrBxaB, Dmod.MyDB);
  //
  Qtde   := - (QrBxaAQtde.Value   + QrBxaBQtde.Value);
  Pecas  := - (QrBxaAPecas.Value  + QrBxaBPecas.Value);
  Peso   := - (QrBxaAPeso.Value   + QrBxaBPeso.Value);
  AreaM2 := - (QrBxaAAreaM2.Value + QrBxaBAreaM2.Value);
  AreaP2 := - (QrBxaAAreaP2.Value + QrBxaBAreaP2.Value);
  //
  Result := True;
end;

function TDmProd.ObtemUnidMedDeSigla(Sigla: String): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUnidMed, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM unidmed',
  'WHERE Sigla="' + Sigla + '"',
  '']);
  Result := QrUnidMedCodigo.Value;
end;

function TDmProd.Obtem_prod_uTribdeGraGru1(const Nivel1: Integer; var Sigla:
String; var Codigo: Integer): Boolean;
begin
  QrGG1.Close;
  QrGG1.Params[0].AsInteger := Nivel1;
  UnDmkDAC_PF.AbreQuery(QrGG1, Dmod.MyDB);
  //
  Result := QrGG1.RecordCount > 0;
  Sigla := QrGG1SIGLA.Value;
  Codigo := QrGG1UnidMed.Value;
  //
  if not Result then
    Geral.MensagemBox('N�o foi localizada a unidade de medida do produto n� ' +
    FormatFloat('0', Nivel1), 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TDmProd.AtualizaGradesPrecos2(GraGru1, TabePrcCab: Integer;
GradeP: TStringGrid; Variacao: Double);
var
  c, l: Integer;
  Fator, Valor: Double;
begin
  Fator := (Variacao / 100) + 1;
  if GradeP = nil then Exit;
  //
  ReopenTabePrcGrI(GraGru1, TabePrcCab);
  QrTabePrcGrI.First;
  for c := 1 to GradeP.ColCount - 1 do
    for l := 1 to GradeP.RowCount - 1 do
      GradeP.Cells[c, l] := '';
  //
  while not QrTabePrcGrI.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrTabePrcGrIGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrTabePrcGrIGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        Valor :=  dmkPF.FFF(QrTabePrcGrIPreco.Value * Fator,
          Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
        GradeP.Cells[c, l] := FormatFloat(Dmod.FStrFmtPrc, Valor);
      end;
    end;
    QrTabePrcGrI.Next;
  end;
  //
end;

procedure TDmProd.AtualizaGradesQtdEtq1(GraGru1, Lote: Integer;
GradeP: TStringGrid);
var
  c, l: Integer;
begin
  if GradeP = nil then Exit;
  //
  ReopenItsLotEtq(Lote);
  QrItsLotEtq.First;
  for c := 1 to GradeP.ColCount - 1 do
    for l := 1 to GradeP.RowCount - 1 do
      GradeP.Cells[c, l] := '';
  //
  while not QrItsLotEtq.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrItsLotEtqGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrItsLotEtqGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        GradeP.Cells[c, l] := dmkPF.FFP(QrItsLotEtqITENS.Value, 0);
      end;
    end;
    QrItsLotEtq.Next;
  end;
  //
end;

procedure TDmProd.GeraTextoEtiqueta(Memo: TMemo; EtqPrinCad: Integer;
QrEtqGeraLot: TmySQLQuery);
var
  Imprimir, Codigo: Integer;
begin
  Memo.Lines.Clear;
  if EtqPrinCad = 0 then
  begin
    Application.MessageBox('Informe a configura��o de impress�o!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  QrEtqPrinCmd.Close;
  QrEtqPrinCmd.Params[0].AsInteger := EtqPrinCad;
  UMyMod.AbreQuery(QrEtqPrinCmd, Dmod.MyDB, 'TDmProd.GeraTextoEtiqueta()');
  if QrEtqGeraLot <> nil then
  begin
    QrEtqGeraLot.First;
    while not QrEtqGeraLot.Eof do
    begin
      Imprimir := QrEtqGeraLot.FieldByName('Imprimir').AsInteger;
      if Imprimir = 1 then
      begin
        Codigo := QrEtqGeraLot.FieldByName('Codigo').AsInteger;
        QrEtqGeraIts.Close;
        QrEtqGeraIts.Params[0].AsInteger := Codigo;
        UMyMod.AbreQuery(QrEtqGeraIts, Dmod.MyDB, 'TDmProd.GeraTextoEtiqueta()');
        while not QrEtqGeraIts.Eof do
        begin
          QrEtqPrinCmd.First;
          while not QrEtqPrinCmd.Eof do
          begin
            GeraLinhaEtiqueta(Memo, QrEtqPrinCmdConfig.Value, QrEtqGeraLot);
            QrEtqPrinCmd.Next;
          end;
          QrEtqGeraIts.Next;
        end;
      end;
      QrEtqGeraLot.Next;
    end;
  end;
end;

function TDmProd.GraGruXGraGruVinc(const Nome: String; var GraGru1, GraGruX:
  Integer): Boolean;
var
  MD5, Soundex: String;
begin
  Result := False;
  VAR_CADASTRO := 0;
  VAR_SELCOD := GraGru1;
  MyObjects.CriaForm_AcessoTotal(TFmGraGruRSel, FmGraGruRSel);
  FmGraGruRSel.EdNomeX.Text := Nome;
  FmGraGruRSel.ShowModal;
  if VAR_CADASTRO <> 0  then
  begin
    Screen.Cursor := crHourGlass;
    try
      Result := True;
      GraGruX := VAR_CADASTRO;
      GraGru1 := VAR_SELCOD;
      //
      MD5 := FmGraGruRSel.EdMD5.Text;
      Soundex := FmGraGruRSel.QrSoundexSoundex.Value;
      if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'gragruvinc', [
      'GraGruX', 'MD5', 'Soundex', 'Nome'], ['=','=','=','='], [
      GraGruX, MD5, Soundex, Nome], '') then
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruvinc', False, [
      'GraGruX', 'MD5', 'Soundex', 'Nome'], [], [
      GraGruX, MD5, Soundex, Nome], [], False) then
      ;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  FmGraGruRSel.Destroy;
end;

function TDmProd.MontaDuplicata(FatNum, nNF, Parcela, tpFaturaNum, tpFaturaSeq:
Integer; Prefixo, Separador: String): String;
(*
var
  FaturaNum, ParcTxt: String;
begin
  if tpFaturaNum = 0 then
    FaturaNum := FormatFloat('000000', FatNum)
  else
    FaturaNum := FormatFloat('000000', nNF);
  Result := Prefixo + FaturaNum ;
  ParcTxt := dmkPF.ParcelaFatura(Parcela, tpFaturaSeq);
  if Trim(ParcTxt) <> '' then
    Result := Result + Separador + ParcTxt;
*)
begin
  Result := dmkPF.MontaDuplicata(FatNum, nNF, Parcela, tpFaturaNum, tpFaturaSeq,
            Prefixo, Separador);
end;

// Sincronismo MCW 2014-06-02
function TDmProd.MostraGraGru1Xxx(const ItTxt, TabNome: String;
  const Nivel1: Integer; var Controle: Integer; var Nome: String): Boolean;
begin
  Result := False;
  Nome   := '';
  //
  if InputQuery(ItTxt + ' de Produto', 'Informe o ' + ItTxt + ':', Nome) then
  begin
    Controle := UMyMod.BPGS1I32(TabNome, 'Controle', '', '', tsPos, stIns, Controle);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, TabNome, False, [
    'Nivel1', 'Nome'], [
    'Controle'], [
    Nivel1, Nome], [
    Controle], True);
    if not Result then
    begin
      Nome := '';
      Controle := 0;
    end;
  end;
end;

procedure TDmProd.MostraStqMovImped(MotivImped: TStqMovImped);
begin
  if DBCheck.CriaFm(TFmStqMovImped, FmStqMovImped, afmoNegarComAviso) then
  begin
    FmStqMovImped.FMotivImped := MotivImped;
    FmStqMovImped.ShowModal;
    FmStqMovImped.Destroy;
  end;
end;

function TDmProd.NomeReduzido(NomeGrupo: String; GradeC: TStringGrid; Coluna,
  Linha: Integer): String;
var
  Nome: String;
begin
  Result := NomeGrupo;
  if GradeC <> nil then
  begin
    Nome := GradeC.Cells[0, Linha];
    if Nome <> '' then Result := Result + ' ' + Trim(Nome);
    //
    Nome := GradeC.Cells[Coluna, 0];
    if Nome <> '' then Result := Result + ' ' + Trim(Nome);
  end;
end;

procedure TDmProd.GeraEstoqueEm(Empresa: Integer; DataFim: TDateTime;
  TipoEstoque: TTipoEstoque; PGT_CodUsu, PGT_Codigo, GG1_CodUsu,
  GG1_Codigo, GraCusPrc: Integer; SoPositivos: Boolean;
  LaAviso2: TLabel);
var
  //PGT, GG1: Integer;
  PrdGrupTip_Txt, StqCenCad_Txt, DiaSeguinte, Empresa_Txt, GrupoBal_Txt,
  GraCusPrc_Txt, TextoA, TextoB, TextoC, CliInt_Txt, EntiSitio_Txt: String;
  MyCursor: TCursor;
begin
  //TTipoEstoque = (testqNenhum, testqProprio, testqSINTEGRA);

  if (Int(DataFim) <> Geral.UltimoDiaDoMes(Int(DataFim)))
  and ((*RGTipoPesq2.ItemIndex = 2*) TipoEstoque = testqSINTEGRA)  then
  begin
    if Geral.MensagemBox('O bot�o de ' +
    'exporta��o n�o ser� habilitado pois a data escolhida n�o � final de mes!' +
    slineBreak + 'Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
  end;
  //
  if Empresa <> 0 then
    Empresa_Txt := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else begin
    Geral.MensagemBox('Informe a empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //PGT := (*EdPrdGrupTip.ValueVariant*) PGT_CodUsu;
    //GG1 := (*EdGraGru1.ValueVariant*) GG1_CodUsu;
    //
    DiaSeguinte := FormatDateTime('yyyy-mm-dd', DataFim + 1);
    if (*RGTipoPesq2.ItemIndex = 1*) TipoEstoque = testqProprio then
    begin
      //if CBGraCusPrc.KeyValue <> NULL then
        GraCusPrc_Txt := FormatFloat('0', GraCusPrc(*QrGraCusPrcCodigo.Value*));
      //else // N�o pode ser nulo!
        //GraCusPrc_Txt := '0';
    end;
    //
    MyObjects.Informa(LaAviso2, True, 'Tabela temporaria - Excluindo caso exista');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS stqmovitsc;');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso2, True, 'Tabela temporaria - Adicionando dados do arquivo morto');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('CREATE TABLE stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT *');
    DmodG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.stqmovitsb;');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso2, True, 'Tabela temporaria - Adicionando dados do arquivo ativo');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT * FROM '+TMeuDB+'.stqmovitsa');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso2, True, 'Tipos de Grupos de Produtos - abrindo tabela');
    QrPGT_SCC.Close;
    QrPGT_SCC.DataBase := DModG.MyPID_DB;
    QrPGT_SCC.SQL.Clear;
    case TipoEstoque of
      testqProprio:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad, ');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, ' + Empresa_Txt + ' Empresa, ' + '0' + ' EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('WHERE GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND smic.Empresa=' + Empresa_Txt);
        if PGT_CodUsu <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PGT_Codigo(*QrPrdGrupTipCodigo.Value*)));
        if GG1_CodUsu <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GG1_Codigo(*QrGraGru1Nivel1.Value*)));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');//, smic.GraGruX');
      end;
      testqSINTEGRA:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad,');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, FLOOR(smic.Empresa + 0.1) Empresa, FLOOR(scc.EntiSitio + 0.1) EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
        QrPGT_SCC.SQL.Add('WHERE (smic.Empresa=' + Empresa_Txt + ' OR scc.EntiSitio=' + Empresa_Txt + ')');
        QrPGT_SCC.SQL.Add('AND GraGruX <> 0');
        if PGT_CodUsu <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PGT_Codigo(*QrPrdGrupTipCodigo.Value*)));
        if GG1_CodUsu <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GG1_CodUsu(*QrGraGru1Nivel1.Value*)));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrPGT_SCC, DModG.MyPID_DB);
    //
    TextoA     := '';
    TextoB     := '';
    TextoC     := '';
    while not QrPGT_SCC.Eof do
    begin
      CliInt_Txt    := FormatFloat('0', QrPGT_SCCEmpresa.Value);
      EntiSitio_Txt := FormatFloat('0', QrPGT_SCCEntiSitio.Value);

      if (*RGTipoPesq2.ItemIndex = 2*)TipoEstoque = testqSINTEGRA then
      begin
        GraCusPrc_Txt := FormatFloat('0', QrPGT_SCCLstPrcFisc.Value)
      end;
      PrdGrupTip_Txt := FormatFloat('0', QrPGT_SCCPrdGrupTip.Value);
      StqCenCad_Txt  := FormatFloat('0', QrPGT_SCCStqCenCad.Value);
      TextoA := 'Tipo Prod.: ' + PrdGrupTip_Txt + '  -  ' +
                'Centro estq: ' + StqCenCad_Txt + '  -  ' +
                'Sitio: ' + EntiSitio_Txt + '  -  ' +
                'Empresa: ' + CliInt_Txt;
      TextoB := '';
      TextoC := '';
      MyObjects.Informa(LaAviso2, False, TextoA + TextoB + TextoC);
      //
      QrAbertura2b.Close;
      QrAbertura2b.SQL.Clear;
      QrAbertura2b.SQL.Add('SELECT Abertura');
      QrAbertura2b.SQL.Add('FROM stqbalcad');
      QrAbertura2b.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
      QrAbertura2b.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
      QrAbertura2b.SQL.Add('AND Abertura >= "' + DiaSeguinte + '"');
      QrAbertura2b.SQL.Add('AND Empresa=' + Empresa_Txt);
      QrAbertura2b.SQL.Add('ORDER BY Abertura DESC');
      QrAbertura2b.SQL.Add('LIMIT 1');
      UnDmkDAC_PF.AbreQuery(QrAbertura2b, Dmod.MyDB);
      if QrAbertura2b.RecordCount > 0 then
      begin
        QrAbertura2a.Close;
        QrAbertura2a.SQL.Clear;
        QrAbertura2a.SQL.Add('SELECT Abertura, GrupoBal');
        QrAbertura2a.SQL.Add('FROM stqbalcad');
        QrAbertura2a.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
        QrAbertura2a.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
        QrAbertura2a.SQL.Add('AND Abertura < "' + DiaSeguinte + '"');
        QrAbertura2a.SQL.Add('AND Empresa=' + Empresa_Txt);
        QrAbertura2a.SQL.Add('ORDER BY Abertura DESC');
        QrAbertura2a.SQL.Add('LIMIT 1');
        UnDmkDAC_PF.AbreQuery(QrAbertura2a, Dmod.MyDB);
        //
        if QrAbertura2aAbertura.Value > Int(DataFim) + 1 then
          GrupoBal_Txt := FormatFloat('0', QrAbertura2aGrupoBal.Value)
        else
          GrupoBal_Txt := '0';
      end else GrupoBal_Txt := '0';
      //
      QrSMIC2.Close;
      QrSMIC2.DataBase := DModG.MyPID_DB;
      QrSMIC2.SQL.Clear;
      if QrPGT_SCC.RecNo = 1 then
      begin
        QrSMIC2.SQL.Add('DROP TABLE IF EXISTS ' + FSMIC2 + ';');
        QrSMIC2.SQL.Add('CREATE TABLE ' + FSMIC2);
      end else begin
        QrSMIC2.SQL.Add('INSERT INTO ' + FSMIC2);
      end;
      QrSMIC2.SQL.Add('SELECT smic.IDCtrl My_Idx, gg1.Nivel1, ');
      QrSMIC2.SQL.Add('gg1.Nome NO_PRD, gg1.PrdGrupTip, gg1.UnidMed, ');
      QrSMIC2.SQL.Add('gg1.NCM, pgt.Nome NO_PGT, ');
      QrSMIC2.SQL.Add('med.Sigla SIGLA, gti.PrintTam, gti.Nome NO_TAM, ');
      QrSMIC2.SQL.Add('gcc.PrintCor, gcc.Nome NO_COR, ggc.GraCorCad, ');
      QrSMIC2.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, ');
      QrSMIC2.SQL.Add('scc.SitProd, scc.EntiSitio,');
      QrSMIC2.SQL.Add('smic.GraGruX, smic.Empresa, smic.StqCenCad,');
      QrSMIC2.SQL.Add('SUM(smic.Qtde * smic.Baixa) QTDE, SUM(smic.Pecas) PECAS,');
      QrSMIC2.SQL.Add('SUM(smic.Peso) PESO, SUM(smic.AreaM2) AREAM2,');
      QrSMIC2.SQL.Add('SUM(smic.AreaP2) AREAP2,');
      QrSMIC2.SQL.Add('ggv.GraCusPrc, TRUNCATE(ggv.CustoPreco, 4) PrcCusUni,');
      QrSMIC2.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(ggv.CustoPreco, 4),');
      QrSMIC2.SQL.Add(' 2) ValorTot, 1 Exportado');
      //QrSMIC2.SQL.Add('1 Exportado, 0 Mot_SitPrd, 0 Mot_Sitio, 0 MotQtdVal, 0 MotProduto');
      QrSMIC2.SQL.Add('FROM stqmovitsc smic');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux    ggx ON ggx.Controle=smic.GraGruX');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruc    ggc ON ggc.Controle=ggx.GraGruC');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gratamits  gti ON gti.Controle=ggx.GraTamI');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.unidmed    med ON med.Codigo=gg1.UnidMed');
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruval  ggv ON ggv.GraGruX=smic.GraGruX');
      // Evitar n�o encontrar registros sem pre�o!
      QrSMIC2.SQL.Add('          AND ggv.GraCusPrc=' + GraCusPrc_Txt);
      QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
      //
      QrSMIC2.SQL.Add('WHERE smic.GrupoBal=' + GrupoBal_Txt);
      QrSMIC2.SQL.Add('AND smic.DataHora < "' + DiaSeguinte  + '"');
      QrSMIC2.SQL.Add('AND smic.StqCenCad=' + StqCenCad_Txt);
      QrSMIC2.SQL.Add('AND gg1.PrdGrupTip=' + PrdGrupTip_Txt);
      //
      QrSMIC2.SQL.Add('AND smic.EMPRESA=' + CliInt_Txt);
      if (*RGTipoPesq2.ItemIndex = 2*) TipoEstoque = testqSINTEGRA then
        QrSMIC2.SQL.Add('AND scc.EntiSitio=' + EntiSitio_Txt);
      //
      //  Filtros de peaquisa
      if PGT_CodUsu <> 0 then
        QrSMIC2.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PGT_Codigo(*QrPrdGrupTipCodigo.Value*)));
      if GG1_CodUsu <> 0 then
        QrSMIC2.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GG1_Codigo(*QrGraGru1Nivel1.Value*)));
      //
      //
      QrSMIC2.SQL.Add('GROUP BY smic.GraGruX');
      QrSMIC2.ExecSQL;
      //
      QrPGT_SCC.Next;
    end;
    //
    MyObjects.Informa(LaAviso2, False, 'Abrindo dados gerados');
    QrSMIC2.Close;
    QrSMIC2.SQL.Clear;
    QrSMIC2.SQL.Add('SELECT sm2.*, scc.SitProd,');
    QrSMIC2.SQL.Add('ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,');
    QrSMIC2.SQL.Add('emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF');
    QrSMIC2.SQL.Add('FROM ' + FSMIC2 + ' sm2');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=sm2.StqCenCad');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades ens ON ens.Codigo=scc.EntiSitio');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades emp ON emp.Codigo=sm2.Empresa');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf1 ON uf1.Codigo=ens.EUF');
    QrSMIC2.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf2 ON uf2.Codigo=emp.EUF');
    if (*CkPositivo2.Checked*) SoPositivos then
      QrSMIC2.SQL.Add('WHERE sm2.QTDE >= 0.001');
    QrSMIC2.SQL.Add('ORDER BY NO_PGT, NO_PRD');
    UnDmkDAC_PF.AbreQuery(QrSMIC2, DModG.MyPID_DB);
    //
    FListErr1  := UCriar.RecriaTempTable('ListErr1', DmodG.QrUpdPID1, False);
    FLayout001 := UCriar.RecriaTempTable('Layout001', DmodG.QrUpdPID1, False);
    //
    MyObjects.Informa(LaAviso2, False, '...');
    //
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TDmProd.GeraLinhaEtiqueta(Memo: TMemo; Texto: String;
QrEtqGeraLot: TmySQLQuery);
var
  p: Integer;
  LinR, LinN, x, vi, vf, _i, _f: String;
begin
  vi := '@';//QrEtqPrinCadIdVarIni.Value;
  vf := '@';//QrEtqPrinCadIdVarFim.Value;
  _i := '{';
  _f := '}';
  //
  LinR := Texto;
  LinN := '';
  // in�cio da primeira vari�vel
  p := pos(vi, LinR);
  while p > 0 do
  begin
    LinN := LinN + Copy(LinR, 1, p - 1);
    LinR := Copy(LinR, p + 1);
    // Fim da vari�vel atual
    p := pos(vf, LinR);
    if p > 0 then
    begin
      x := Copy(LinR, 1, p);
      LinR := Copy(LinR, p + 1);
      //n := Geral.IMV(x);
      //x := ValorDeVariavel(n);
      if QrEtqGeraLot <> nil then
        x := ValorDeVariavel_LimitTam(x)
      else begin
        Application.MessageBox(PChar('Lote n�o definido para obten��o da ' +
        'vari�vel ' + x + '!'), 'Aviso', MB_OK+MB_ICONWARNING);
        x := '?';
      end;
      LinN := LinN + x;
      // in�cio da pr�xima vari�vel
      p := pos(vi, LinR);
    end;
    if p = 0 then
    begin
      LinN := LinN + LinR;
      LinR := '';
    end;
  end;
  LinN := LinN + LinR;


  // in�cio da primeira ascii
  LinR := LinN;
  LinN := '';
  p := pos(_i, LinR);
  while p > 0 do
  begin
    LinN := LinN + Copy(LinR, 1, p - 1);
    LinR := Copy(LinR, p + 1);
    // Fim da vari�vel atual
    p := pos(_f, LinR);
    if p > 0 then
    begin
      x := Copy(LinR, 1, p - 1);
      LinR := Copy(LinR, p + 1);
      //n := Geral.IMV(x);
      //x := ValorDeVariavel(n);
      x := Char(StrToInt(x));
      LinN := LinN + x;
      // in�cio da pr�xima vari�vel
      p := pos(_i, LinR);
    end;
    if p = 0 then
    begin
      LinN := LinN + LinR;
      LinR := '';
    end;
  end;
  LinN := LinN + LinR;
  //
  Memo.Lines.Add(LinN);
end;

function TDmProd.GeraNovoCodigoNivel(Nivel, PrdGrupTip, TipNivCad: Integer;
  MostraMsg: Boolean = True): Integer;
var
  Niv, Valor, ResMsg: Integer;
begin
  Result := 0;
  //
  if MostraMsg then
    ResMsg := Geral.MB_Pergunta('Esta a��o ir� gerar um c�digo para um novo item no n�vel selecionado!' +
                sLineBreak + 'S� continue se tiver certeza que o item n�o existe!' +
                sLineBreak + 'Se n�o tiver certeza dige F3 ao inv�s de F4 para pesquisar antes!' +
                sLineBreak + sLineBreak + 'Tem certeza que deseja gerar um novo c�digo?')
  else
    ResMsg := ID_YES;
  //
  if ResMsg = ID_YES then  
  begin
    Niv := Nivel;
    //
    QrNext.Close;
    QrNext.SQL.Clear;
    QrNext.SQL.Add('SELECT MAX(CodUsu) Numero');
    QrNext.SQL.Add('FROM gragru' + Geral.FF0(Niv));
    //
    if Niv = TipNivCad then
    begin
      QrRange.Close;
      QrRange.Params[0].AsInteger := PrdGrupTip;
      UnDmkDAC_PF.AbreQuery(QrRange, Dmod.MyDB);
      //
      if (QrRangeFaixaIni.Value <> 0) and (QrRangeFaixaFim.Value <> 0) then
      begin
        QrNext.SQL.Add('WHERE CodUsu >= :P0');
        QrNext.SQL.Add('AND CodUsu <=:P1');
        QrNext.Params[00].AsInteger := QrRangeFaixaIni.Value;
        QrNext.Params[01].AsInteger := QrRangeFaixaFim.Value;
      end;
    end;
    //
    UnDmkDAC_PF.AbreQuery(QrNext, Dmod.MyDB);
    //
    Valor := QrNextNumero.Value;
    if QrRangeFaixaFim.Value <= QrRangeFaixaIni.Value then
      Valor := Valor + 1
    else
      if (Valor >= QrRangeFaixaIni.Value) and (Valor < QrRangeFaixaFim.Value) then
        Valor := Valor + 1
      else
        Valor := QrRangeFaixaIni.Value;
    //
    Result := Valor;
  end;
end;

function TDmProd.ValorDeVariavel_LimitTam(Texto: String): String;
var
  p, Cod, Tam: Integer;
  Txt: String;
begin
  Tam := 0;
  p := pos('#', Texto);
  if p > 0 then
  begin
    Txt := Copy(Texto, 1, p-1);
    Cod := Geral.IMV(Txt);
    Txt := Copy(Texto, p+1);
    p := pos('#', Texto);
    if p > 0 then
      Txt := Copy(Txt, 1, p);
    Tam := Geral.IMV(Txt);
  end else Cod := Geral.IMV(Texto);
  Txt := ValorDeVariavel(Cod);
  if Tam > 0 then
    Result := Copy(Txt, 1, Tam)
  else
    Result := Txt;
end;

procedure TDmProd.VerificaCadastroProduto(GraGru1, UnidMed: Integer; NCM,
  Ex_TIPI: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGG1Fisc, Dmod.MyDB, [
  'SELECT NCM, EX_TIPI, UnidMed  ',
  'FROM GraGru1 ',
  'WHERE Nivel1=' + Geral.FF0(GraGru1),
  '']);
  if (UnidMed <> 0) then
  begin
    if (QrGG1FiscUnidMed.Value = 0) then
    begin
      if Geral.MB_Pergunta('O cadastro do produto n�o tem informado a unidade de medida!' +
      sLineBreak + 'Deseja atualiz�-lo?') = ID_YES then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'GraGru1', False, [
        'UnidMed'], [
        'Nivel1'], [
        UnidMed], [
        GraGru1], True);
      end;
    end;
  end;
  if (Trim(NCM) <> '') then
  begin
    if (Trim(QrGG1FiscNCM.Value) = '') then
    begin
      if Geral.MB_Pergunta('O cadastro do produto n�o tem informado a NCM!' +
      sLineBreak + 'Deseja atualiz�-lo?') = ID_YES then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'GraGru1', False, [
        'NCM'], [
        'Nivel1'], [
        Trim(NCM)], [
        GraGru1], True);
      end;
    end;
  end;
  if (Trim(Ex_TIPI) <> '') then
  begin
    if (Trim(QrGG1FiscEx_TIPI.Value) = '') then
    begin
      if Geral.MB_Pergunta('O cadastro do produto n�o tem informado a ExTIPI!' +
      sLineBreak + 'Deseja atualiz�-lo?') = ID_YES then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'GraGru1', False, [
        'Ex_TIPI'], [
        'Nivel1'], [
        Trim(Ex_TIPI)], [
        GraGru1], True);
      end;
    end;
  end;
end;

function TDmProd.VerificaEstoqueProduto(Qtde: Double; EntraSai, GraGruX,
  StqCenCad, FisRegCad, OriCodi, OriCnta, Empresa, FatSemEstq: Integer;
  NaoExibeMsg: Boolean; var Msg: String; ForceTipoCalc: Integer): Integer;
var
  Continua: Integer;
  TipoCalc: TTipoCalcEstq;
  Sdo: Double;
begin
  Continua := ID_NO;
  //
  if Qtde <> 0 then
  begin
    if EntraSai = -1 then
    begin
      //Sdo := SaldoRedudidoCen(Empresa, GraGruX, StqCenCad, OriCodi, OriCnta);
      Sdo := SaldoRedudidoCen(Empresa, GraGruX);

      if ForceTipoCalc <> -1 then
        TipoCalc := TTipoCalcEstq(ForceTipoCalc)
      else
        TipoCalc := TTipoCalcEstq(DefineTipoCalc(FisRegCad, StqCenCad, Empresa, NaoExibeMsg));

      if Sdo < Qtde then
        Continua := SaldoSuficiente(Empresa, Sdo, Qtde, GraGruX, '', '',
                      '', FatSemEstq, TipoCalc, NaoExibeMsg, Msg)
      else
        Continua := ID_YES;
    end else
      Continua := ID_YES;
    //
  end else
    Continua := ID_YES;
  //
  Result := Continua;
end;

function TDmProd.ValorDeVariavel(Codigo: Integer): String;
var
  x: String;
begin
  case Codigo of
    //001,'PRDTIPCOD', 'C�digo do tipo de produto');
    001: x := '1';
    //002,'PRDTIPNOM', 'Descri��o do tipo de produto');
    002: x := 'PRODUTO';
    //003,'PRDNI3COD', 'C�digo do 3� n�vel de produtos');
    003: x := '';
    //004,'PRDNI3NOM', 'Descri��o do 3� n�vel de produtos');
    004: x := '';
    //005,'PRDNI2COD', 'C�digo do 2� n�vel de produtos');
    005: x := '';
    //006,'PRDNI2NOM', 'Descri��o do 2� n�vel de produtos');
    006: x := '';
    //007,'PRDNI1COD', 'C�digo do 1� n�vel de produtos');
    007: x := dmkPF.FFP(QrEtqGeraItsNIVEL1.Value, 0);
    //008,'PRDNI1NOM', 'Descri��o do 1� n�vel de produtos');
    008: x := QrEtqGeraItsNOMENIVEL1.Value;
    //009,'PRDGRACOD', 'C�digo da grade de tamanhos');
    009: x := dmkPF.FFP(QrEtqGeraItsCOD_GRADE.Value, 0);
    //010,'PRDGRANOM', 'Descri��o da grade de tamanhos');
    010: x := QrEtqGeraItsNOMEGRADE.Value;
    //011,'PRDTAMCOD', 'C�digo do tamanho');
    011: x := dmkPF.FFP(QrEtqGeraItsCOD_TAM.Value, 0);
    //012,'PRDTAMNOM', 'Descri��o do tamanho');
    012: x := QrEtqGeraItsNOMETAM.Value;
    //013,'PRDCORCOD', 'C�digo da cor');
    013: x := dmkPF.FFP(QrEtqGeraItsCOD_COR.Value, 0);
    //014,'PRDCORNOM', 'Descri��o da cor');
    014: x := QrEtqGeraItsNOMECOR.Value;
    //015,'PRDREDCOD', 'C�digo reduzido');
    015: x := Geral.FTX(QrEtqGeraItsREDUZIDO.Value, 6, 0, siPositivo);
    //016,'PRDCODBAR', 'C�digo de barras do produto');
    016: x := GeraSequenciaDeCodigoDeBarraProduto();
    //017,'PRDSEQUE1', 'Sequencia de impress�o do reduzido');
    017: x := Geral.FTX(QrEtqGeraItsSequencia.Value, 8, 0, siPositivo);
    else begin
      Application.MessageBox(PChar('O c�digo ' + IntToStr(Codigo) +
      ' n�o est� implementado na gera��o de etiqueta de produtos!'), 'Aviso',
      MB_OK+MB_ICONWARNING);
      x := '';
    end;
  end;
  Result := x;
end;

function TDmProd.GeraSequenciaDeCodigoDeBarraProduto: String;
var
  Txt: String;
begin
  Txt := '000000' +
    Geral.FTX(QrEtqGeraItsREDUZIDO.Value, 6, 0, siPositivo) +
    Geral.FTX(QrEtqGeraItsSequencia.Value, 8, 0, siPositivo);
  Result := Txt;
end;

procedure TDmProd.AtualizaGradesPack1(GraGru1, GraPckCad: Integer;
  GradeK: TStringGrid);
var
  c, l: Integer;
begin
  if GradeK = nil then Exit;
  //
  //ReopenGraGruVal(GraGru1, GraCusPrc, Entidade, TipoLista);
  ReopenGraPckQtd(GraPckCad);
  //
  QrGraPckQtd.First;
  for c := 1 to GradeK.ColCount - 1 do
    for l := 1 to GradeK.RowCount - 1 do
      GradeK.Cells[c, l] := '';
  //
  while not QrGraPckQtd.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrGraPckQtdGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrGraPckQtdGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        GradeK.Cells[c, l] := Geral.FFT(QrGraPckQtdQtde.Value, 3, siNegativo);
      end;
    end;
    QrGraPckQtd.Next;
  end;
end;

procedure TDmProd.AtualizaGradesPedidos(GraGru1, Pedido: Integer;
          GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
var
  c, r: Integer;
  PrecoL: Double;
begin
  ReopenPediVdaIts(Pedido, GraGru1);
  QrPediVdaIts.First;
  while not QrPediVdaIts.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrPediVdaItsGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrPediVdaItsGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          GradeQ.Cells[c, r] := FloatToStr(QrPediVdaItsQuantP.Value);
        //
        if GradeF <> nil then
          GradeF.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, QrPediVdaItsPrecoR.Value);
        //
        if GradeD <> nil then
          GradeD.Cells[c, r] := FormatFloat('0.00', QrPediVdaItsDescoP.Value);
        //
        if GradeV <> nil then
        begin
          if QrPediVdaItsQuantP.Value = 0 then
            PrecoL := 0
          else
            PrecoL := QrPediVdaItsValLiq.Value / QrPediVdaItsQuantP.Value;
          GradeV.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, PrecoL);
        end;
        //
      end;
    end;
    QrPediVdaIts.Next;
  end;
end;

procedure TDmProd.AtualizaGradesFatVen(GraGru1, FatVen: Integer;
          GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
var
  c, r: Integer;
  PrecoL: Double;
begin
  ReopenFatVenIts(FatVen, GraGru1);
  QrFatVenIts.First;
  while not QrFatVenIts.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrFatVenItsGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrFatVenItsGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          GradeQ.Cells[c, r] := FloatToStr(QrFatVenItsQuantP.Value);
        //
        if GradeF <> nil then
          GradeF.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, QrFatVenItsPrecoR.Value);
        //
        if GradeD <> nil then
          GradeD.Cells[c, r] := FormatFloat('0.00', QrFatVenItsDescoP.Value);
        //
        if GradeV <> nil then
        begin
          if QrFatVenItsQuantP.Value = 0 then
            PrecoL := 0
          else
            PrecoL := QrFatVenItsValLiq.Value / QrFatVenItsQuantP.Value;
          GradeV.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, PrecoL);
        end;
        //
      end;
    end;
    QrFatVenIts.Next;
  end;
end;

procedure TDmProd.AtualizaGradesFiConsSpc(GG1Dst, GG1Ori, OriGraCorCad,
  OriGraTamIts: Integer; GradeK: TStringGrid);
var
  c, l: Integer;
begin
  if GradeK = nil then Exit;
  //
  //ReopenGraGruVal(GraGru1, GraCusPrc, Entidade, TipoLista);
  ReopenFiConsSpc(GG1Dst, GG1Ori, OriGraCorCad, OriGraTamIts);
  //
  QrFiConsSpc.First;
  for c := 1 to GradeK.ColCount - 1 do
    for l := 1 to GradeK.RowCount - 1 do
      GradeK.Cells[c, l] := '';
  //
  while not QrFiConsSpc.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrFiConsSpcGraTamI_Dest.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrFiConsSpcGraGruC_Dest.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        GradeK.Cells[c, l] := Geral.FFT(QrFiConsSpcQtdUso.Value, 3, siNegativo);
      end;
    end;
    QrFiConsSpc.Next;
  end;
end;

procedure TDmProd.AtualizaGradesFiConsSpcTripleCol(GraGru1Dest, OriGraGru1,
  OriGraCorCad, OriGraTamIts: Integer; GradeK: TStringGrid; IniCol, IniLin: Integer);
var
  c, l, c3: Integer;
begin
  if GradeK = nil then Exit;
  //
  ReopenFiConsSpc(GraGru1Dest, OriGraGru1, OriGraCorCad, OriGraTamIts);
  // Teste
  //GradeK.Cells[0, 0] := '';
  //
   QrFiConsSpc.First;
  for c := IniCol to GradeK.ColCount - 1 do
    for l := IniLin to GradeK.RowCount - 1 do
      GradeK.Cells[c, l] := '';
  //
  while not QrFiConsSpc.Eof do
  begin
    if QrGradesTam3.Locate('Tam', QrFiConsSpcGraTamI_Dest.Value, []) then
    begin
      if QrGradesCor3.Locate('Cor', QrFiConsSpcGraGruC_Dest.Value, []) then
      begin
        c := QrGradesTam3.RecNo;
        l := QrGradesCor3.RecNo;
        //GradeK.Cells[c, l] := Geral.FFT(QrFiConsSpcQtdeUso.Value, 3, siNegativo);
        c3 := (c - 1) * 3;
        GradeK.Cells[c3 + 1, l] := QrFiConsSpcNO_TAM_Sorc.Value;
        GradeK.Cells[c3 + 2, l] := QrFiConsSpcNO_COR_Sorc.Value;
        GradeK.Cells[c3 + 3, l] := Geral.FFT(QrFiConsSpcQtdUso.Value, 3, siNegativo);
      end;
    end;
    QrFiConsSpc.Next;
  end;
  MyObjects.LarguraAutomaticaGrade(GradeK);
end;

procedure TDmProd.AtualizaGradesCondicional_Saida(GraGru1, Condicional: Integer;
          GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
var
  c, r: Integer;
begin
  ReopenFatConIts(Condicional, GraGru1);
  QrFatConIts.First;
  while not QrFatConIts.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrFatConItsGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrFatConItsGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          GradeQ.Cells[c, r] := FloatToStr(QrFatConItsQuantP.Value);
        //
        if GradeF <> nil then
          GradeF.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, QrFatConItsPrecoR.Value);
        //
        if GradeD <> nil then
          GradeD.Cells[c, r] := FormatFloat('0.00', QrFatConItsDescoP.Value);
        //
        if GradeV <> nil then
          GradeV.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, QrFatConItsValLiq.Value);
        //
      end;
    end;
    QrFatConIts.Next;
  end;
end;

procedure TDmProd.AtualizaGradesCondicional_Retorno(GraGru1, Condicional: Integer;
          GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
var
  c, r: Integer;
begin
  ReopenFatRetIts(Condicional, GraGru1);
  QrFatRetIts.First;
  while not QrFatRetIts.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrFatRetItsGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrFatRetItsGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          GradeQ.Cells[c, r] := FloatToStr(QrFatRetItsQuantV.Value);
        //
        if GradeF <> nil then
          GradeF.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, QrFatRetItsPrecoR.Value);
        //
        if GradeD <> nil then
          GradeD.Cells[c, r] := FormatFloat('0.00', QrFatRetItsDescoP.Value);
        //
        if GradeV <> nil then
          GradeV.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, QrFatRetItsValFat.Value);
        //
      end;
    end;
    QrFatRetIts.Next;
  end;
end;

procedure TDmProd.AtualizaGradesRetornoCondicional(GraGru1, Condicional: Integer;
          GradeV: TStringGrid);
var
  c, r: Integer;
begin
  ReopenVendidos(Condicional, GraGru1);
  //
  QrVendidos.First;
  while not QrVendidos.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrVendidosGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrVendidosGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeV <> nil then
          GradeV.Cells[c, r] := FloatToStr(QrVendidosQuantV.Value);
        //
      end;
    end;
    QrVendidos.Next;
  end;
end;

procedure TDmProd.AtualizaGradesFatPed(GraGru1, PediVda, FatPedCab: Integer;
          GradeC, GradeQ, GradeP: TStringGrid);
var
  c, r: Integer;
begin
  // Itens faturados nesta fatura
  ReopenStqMovIts(1, FatPedCab, GraGru1);
  //
  QrStqMovItsA.First;
  while not QrStqMovItsA.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrStqMovItsAGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrStqMovItsAGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          // Somente A
          GradeQ.Cells[c, r] := FloatToStr(QrStqMovItsAQtde.Value);
        //
      end;
    end;
    QrStqMovItsA.Next;
  end;
  //
  QrStqMovItsB.First;
  while not QrStqMovItsB.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrStqMovItsBGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrStqMovItsBGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          // Soma de A + B
          GradeQ.Cells[c, r] := FloatToStr(
            Geral.DMV(GradeQ.Cells[c, r]) +QrStqMovItsBQtde.Value);
        //
      end;
    end;
    QrStqMovItsB.Next;
  end;

  //

  //  Itens que faltam faturar
  ReopenSdosPed(PediVda, GraGru1);
  //
  QrSdosPed.First;
  while not QrSdosPed.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrSdosPedGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrSdosPedGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeP <> nil then
          // Somente A
          GradeP.Cells[c, r] := FloatToStr(QrSdosPedQuantF.Value);
        //
      end;
    end;
    QrSdosPed.Next;
  end;
  //
end;

// Mostra quantidades de movimenta��o manual
procedure TDmProd.AtualizaGradesSoliCompr(GraGru1, SoliComprCab: Integer;
  GradeC, GradeQ, GradeF, GradeD, GradeV: TStringGrid);
var
  c, r: Integer;
  PrecoL: Double;
begin
  ReopenSoliComprIts(SoliComprCab, GraGru1);
  QrSoliComprIts.First;
  while not QrSoliComprIts.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrSoliComprItsGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrSoliComprItsGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          GradeQ.Cells[c, r] := FloatToStr(QrSoliComprItsQuantS.Value);
        //
        if GradeF <> nil then
          GradeF.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, QrSoliComprItsPrecoR.Value);
        //
        if GradeD <> nil then
          GradeD.Cells[c, r] := FormatFloat('0.00', QrSoliComprItsDescoP.Value);
        //
        if GradeV <> nil then
        begin
          if QrSoliComprItsQuantS.Value = 0 then
            PrecoL := 0
          else
            PrecoL := QrSoliComprItsValLiq.Value / QrSoliComprItsQuantS.Value;
          GradeV.Cells[c, r] := FormatFloat(Dmod.FStrFmtPrc, PrecoL);
        end;
        //
      end;
    end;
    QrSoliComprIts.Next;
  end;
end;

procedure TDmProd.AtualizaGradesStqMovIts(TipoMov, GraGru1, OriCodi: Integer;
              GradeC, GradeQ: TStringGrid);
var
  c, r: Integer;
begin
  // Itens faturados nesta fatura
  ReopenStqMovIts(TipoMov, OriCodi, GraGru1);
  //
  QrStqMovItsA.First;
  while not QrStqMovItsA.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrStqMovItsAGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrStqMovItsAGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          // Somente A
          GradeQ.Cells[c, r] := FloatToStr(QrStqMovItsAQtde.Value);
        //
      end;
    end;
    QrStqMovItsA.Next;
  end;
  //
  QrStqMovItsB.First;
  while not QrStqMovItsB.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrStqMovItsBGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrStqMovItsBGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        r := QrGradesCors.RecNo;
        //
        if GradeQ <> nil then
          // Soma de A + B
          GradeQ.Cells[c, r] := FloatToStr(
            Geral.DMV(GradeQ.Cells[c, r]) +QrStqMovItsBQtde.Value);
        //
      end;
    end;
    QrStqMovItsB.Next;
  end;
  //
end;

procedure TDmProd.AtualizaGraGru1LotLau(Tabela, Nome: String; Nivel1: Integer);
var
  Controle: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + Tabela,
    'WHERE Nome="' + Nome + '"',
    'AND Nivel1=' + Geral.FF0(Nivel1),
    '']);
    if Qry.RecordCount = 0 then
    begin
      Controle := UMyMod.BPGS1I32(Tabela, 'Controle', '', '', tsPos, stIns, 0);
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
      'Nivel1', 'Nome'], [
      'Controle'], [
      Nivel1, Nome], [
      Controle], True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmProd.AtualizaGradesPrecos3(GraGru1, TabePrcCab, CondicaoPG: Integer;
              GradeC, GradeP1, GradeP2: TStringGrid; MedDDSimpl, MedDDReal: Double;
              ST1, ST2, ST3: TStaticText; ImgTipo: TdmkImage; SemPrazo: Boolean;
              TipMediaDD: Integer; FatSemPrcL: Integer);

  procedure PrecosEspecificos(StrFmt: String; Fator: Double; var Libera: Boolean);
  var
    c, r: Integer;
    PrecoO, PrecoR: Double;
  begin
    ReopenTabePrcGrI(GraGru1, TabePrcCab);
    Libera := Libera or (QrTabePrcGrI.RecordCount > 0);
    QrTabePrcGrI.First;
    while not QrTabePrcGrI.Eof do
    begin
      if QrGradesTams.Locate('Tam', QrTabePrcGrIGraTamI.Value, []) then
      begin
        if QrGradesCors.Locate('Cor', QrTabePrcGrIGraGruC.Value, []) then
        begin
          c := QrGradesTams.RecNo;
          r := QrGradesCors.RecNo;
          //
          PrecoO := QrTabePrcGrIPreco.Value;
          PrecoR := dmkPF.FFF(PrecoO * Fator,
            Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
          //
          if GradeP1 <> nil then
            GradeP1.Cells[c, r] := FormatFloat(StrFmt, PrecoO);
          //
          if GradeP2 <> nil then
            GradeP2.Cells[c, r] := FormatFloat(StrFmt, PrecoR);
        end;
      end;
      QrTabePrcGrI.Next;
    end;
  end;

  function PrecoGeral(StrFmt: String; Fator: Double): Boolean;
  var
    c, r: Integer;
    PrecoO, PrecoR: Double;
  begin
    QrPrcGru.Close;
    QrPrcGru.Params[00].AsInteger := TabePrcCab;
    QrPrcGru.Params[01].AsInteger := GraGru1;
    UMyMod.AbreQuery(QrPrcGru, Dmod.MyDB, 'TDmProd.AtualizaGradesPrecos3() > PrecoGeral()');
    Result := QrPrcGru.RecordCount > 0;
    //
    PrecoO := QrPrcGruPreco.Value;
    PrecoR := dmkPF.FFF(PrecoO * Fator, Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
    for c := 1 to GradeC.ColCount - 1 do
      for r := 1 to GradeC.RowCount - 1 do
        if GradeC.Cells[c, r] <> '' then
        begin
          GradeP1.Cells[c, r] := FormatFloat(StrFmt,PrecoO);
          GradeP2.Cells[c, r] := FormatFloat(StrFmt,PrecoR);
        end;
  end;
var
  c, r: Integer;
  TipoPrzTxt: String;
  Fator, MediaDD: Double;
  Libera: Boolean;
begin
  ImgTipo.SQLType := stLok;
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  //
  if (GradeP1 = nil) and (GradeP2 = nil) then Exit;
  //
  if GradeP1 <> nil then
  begin
    for c := 1 to GradeP1.ColCount - 1 do
      for r := 1 to GradeP1.RowCount - 1 do
        GradeP1.Cells[c, r] := '';
  end;
  //
  if GradeP2 <> nil then
  begin
    for c := 1 to GradeP2.ColCount - 1 do
      for r := 1 to GradeP2.RowCount - 1 do
        GradeP2.Cells[c, r] := '';
  end;
  //
  QrQtdItsPrz.Close;
  QrQtdItsPrz.Params[0].AsInteger := TabePrcCab;
  UMyMod.AbreQuery(QrQtdItsPrz, Dmod.MyDB, 'TDmProd.AtualizaGradesPrecos3()');
  //
  MediaDD    := MedDDReal;
  TipoPrzTxt := 'real';
  //
  if QrQtdItsPrz.RecordCount > 0 then
  begin
    if SemPrazo then
    begin
      //MediaDD := 0;
      Fator   := 1;
      //
    end else
    begin
      case TipMediaDD(*Dm PediVda / FatVen .QrParamsEmpTipMediaDD.Value*) of
        0:
        begin
          ST1.Caption := 'Tipo de c�lculo de prazo m�dio n�o definido nos par�metros da filial!';
          MediaDD     := MedDDReal;
          TipoPrzTxt  := 'real';
        end;
        1:
        begin
          MediaDD    := MedDDSimpl;
          TipoPrzTxt := 'simples';
        end;
        2:
        begin
          MediaDD    := MedDDReal;
          TipoPrzTxt := 'real';
        end;
      end;
      QrLocPrz.Close;
      QrLocPrz.Params[00].AsInteger := TabePrcCab;
      QrLocPrz.Params[01].AsFloat   := MediaDD;
      UMyMod.AbreQuery(QrLocPrz, Dmod.MyDB, 'TDmProd.AtualizaGradesPrecos3()');
      //
      if (QrLocPrz.RecordCount = 0) then //and (QrQtdItsPrz.RecordCount > 0) then
      begin
        ST2.Caption := 'Falta de faixa de prazo m�dio na lista de pre�os! ' +
        'Prazo m�dio ' + TipoPrzTxt + ': ' + FloatToStr(MediaDD) + ' dias.';
        ST2.Font.Color := clBlue;
        //ST2.Invalidate;
        Exit;
      end;
      //
      Fator := (QrLocPrzVariacao.Value / 100) + 1;
    end;
    //
    // Pre�o geral do Grupo (Nivel1)
    Libera := PrecoGeral(Dmod.FStrFmtPrc, Fator);
    //
    // Pre�os espec�ficos
    PrecosEspecificos(Dmod.FStrFmtPrc, Fator, Libera);
  end else
  begin
    ST1.Caption := 'Produto sem pre�os a prazo!';
    //
    Fator := 1; // sem custo financeiro / desconto
    Libera := PrecoGeral(Dmod.FStrFmtPrc, Fator);
  end;
  //Libera para edi��o / confirma��o
  if Libera or ((*Dm PediVda / FatVen .QrParamsEmpFatSemPrcL.Value*)FatSemPrcL = 1) then
    ImgTipo.SQLType := stIns;
end;

procedure TDmProd.Atualiza_dmkEditsPrecos3(GraGruX, TabePrcCab, CondicaoPG: Integer;
              dmkPreco1, dmkPreco2: TdmkEdit; MedDDSimpl, MedDDReal: Double;
              ST1, ST2, ST3: TStaticText; ImgTipo: TdmkImage; SemPrazo: Boolean;
              TipMediaDD, FatSemPrcL: Integer);

  procedure PrecosEspecificos(Fator: Double; var Libera: Boolean);
  var
    PrecoO, PrecoR: Double;
  begin
    ReopenTabePrcGrX(GraGruX, TabePrcCab);
    Libera := Libera or (QrTabePrcGrX.RecordCount > 0);
    if QrTabePrcGrX.RecordCount > 0 then
    begin
      PrecoO := QrTabePrcGrXPreco.Value;
      PrecoR := dmkPF.FFF(PrecoO * Fator, Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
      if dmkPreco1 <> nil then
        dmkPreco1.ValueVariant := PrecoO;
      if dmkPreco2 <> nil then
        dmkPreco2.ValueVariant := PrecoR;
    end;
  end;

  function PrecoGeral(Fator: Double): Boolean;
  var
    PrecoO, PrecoR: Double;
  begin
    QrPrcGrX.Close;
    QrPrcGrX.Params[00].AsInteger := TabePrcCab;
    QrPrcGrX.Params[01].AsInteger := GraGruX;
    UMyMod.AbreQuery(QrPrcGrX, Dmod.MyDB, 'TDmProd.Atualiza_dmkEditsPrecos3() > PrecoGeral()');
    Result := QrPrcGrX.RecordCount > 0;
    //
    PrecoO := QrPrcGrXPreco.Value;
    PrecoR := dmkPF.FFF(PrecoO * Fator, Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
    if dmkPreco1 <> nil then
      dmkPreco1.ValueVariant := PrecoO;
    if dmkPreco2 <> nil then
      dmkPreco2.ValueVariant := PrecoR;
  end;
var
  TipoPrzTxt: String;
  Fator, MediaDD: Double;
  Libera: Boolean;
begin
  if ImgTipo <> nil then
    ImgTipo.SQLType := stLok;
  //
  if ST1 <> nil then
    ST1.Caption := '';
  if ST2 <> nil then
    ST2.Caption := '';
  if ST3 <> nil then
    ST3.Caption := '';
  //
  if (dmkPreco1 = nil) and (dmkPreco2 = nil) then Exit;
  //
  if dmkPreco1 <> nil then
    dmkPreco1.ValueVariant := 0;
  if dmkPreco2 <> nil then
    dmkPreco2.ValueVariant := 0;
  //
  QrQtdItsPrz.Close;
  QrQtdItsPrz.Params[0].AsInteger := TabePrcCab;
  UMyMod.AbreQuery(QrQtdItsPrz, Dmod.MyDB, 'TDmProd.Atualiza_dmkEditsPrecos3()');
  //
  MediaDD := MedDDReal;
  TipoPrzTxt := 'real';
  if QrQtdItsPrz.RecordCount > 0 then
  begin
    if SemPrazo then
      Fator := 1
    else begin
      case (*Dm PediVda .QrParamsEmpTipMediaDD.Value*) TipMediaDD of
        0:
        begin
          if ST1 <> nil then
            ST1.Caption := 'Tipo de c�lculo de prazo m�dio n�o definido nos par�metros da filial!';
          MediaDD := MedDDReal;
          TipoPrzTxt := 'real';
        end;
        1:
        begin
          MediaDD := MedDDSimpl;
          TipoPrzTxt := 'simples';
        end;
        2:
        begin
          MediaDD := MedDDReal;
          TipoPrzTxt := 'real';
        end;
      end;
      QrLocPrz.Close;
      QrLocPrz.Params[00].AsInteger := TabePrcCab;
      QrLocPrz.Params[01].AsFloat   := MediaDD;
      UMyMod.AbreQuery(QrLocPrz, Dmod.MyDB, 'TDmProd.Atualiza_dmkEditsPrecos3()');
      if (QrLocPrz.RecordCount = 0) then //and (QrQtdItsPrz.RecordCount > 0) then
      begin
        if ST2 <> nil then
          ST2.Caption := 'Falta de faixa de prazo m�dio na lista de pre�os! ' +
                           'Prazo m�dio ' + TipoPrzTxt + ': ' +
                           FloatToStr(MediaDD) + ' dias.';
        Exit;
      end;
      //
      Fator := (QrLocPrzVariacao.Value / 100) + 1;
    end;
    //
    // Pre�o geral do Grupo (Nivel1)
    Libera := PrecoGeral(Fator);
    //
    // Pre�os espec�ficos
    PrecosEspecificos(Fator, Libera);
  end else
  begin
    if ST1 <> nil then
      ST1.Caption := 'Produto sem pre�os a prazo!';
    //
    Fator := 1; // sem custo financeiro / desconto
    Libera := PrecoGeral(Fator);
  end;
  //Libera para edi��o / confirma��o
  if Libera or ((*Dm PediVda .QrParamsEmpFatSemPrcL.Value*)FatSemPrcL = 1) then
  begin
    if ImgTipo <> nil then
      ImgTipo.SQLType := stIns;
  end;
end;

procedure TDmProd.AtualizaGradesPrecos4(GraGru1, GraCusPrc, Entidade: Integer; GradeC,
  GradeP1, GradeP2: TStringGrid; CustoFin: Double);
var
  c, l: Integer;
  Fator: Double;
  Valor: String;
begin
  Fator := (CustoFin / 100) + 1;
  ReopenGraGruVal(GraGru1, GraCusPrc, Entidade, tpGraCusPrc);
  QrGraGruVal.First;
  for c := 1 to GradeP1.ColCount - 1 do
    for l := 1 to GradeP1.RowCount - 1 do
    begin
      GradeP1.Cells[c, l] := '';
      GradeP2.Cells[c, l] := '';
    end;
  //
  while not QrGraGruVal.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrGraGruValGraTamI.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrGraGruValGraGruC.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        Valor := FormatFloat(Dmod.FStrFmtPrc, QrGraGruValCustoPreco.Value * Fator);
        if GradeP1 <> nil then
          GradeP1.Cells[c, l] := Valor;
        if GradeP2 <> nil then
          GradeP2.Cells[c, l] := Valor;
      end;
    end;
    QrGraGruVal.Next;
  end;
  //
end;

procedure TDmProd.Atualiza_dmkEditsPrecos4(GraGruX, GraCusPrc: Integer;
  dmkEdit1, dmkEdit2: TdmkEdit; CustoFin: Double);
var
  Fator: Double;
  Valor: String;
begin
  Fator := (CustoFin / 100) + 1;
  ReopenGraGrXVal(GraGruX, GraCusPrc);
  Valor := FormatFloat(Dmod.FStrFmtPrc, QrGraGrXValCustoPreco.Value * Fator);
  if dmkEdit1 <> nil then
    dmkEdit1.Text := Valor;
  if dmkEdit2 <> nil then
    dmkEdit2.Text := Valor;
end;

function TDmProd.ImpedePeloPrazoMedio(TabePrcCab: Integer;
MedDDSimpl, MedDDReal: Double; TipMediaDD: Integer): Boolean;
var
  TipoPrzTxt: String;
  MediaDD: Double;
begin
  Result := False;
  QrQtdItsPrz.Close;
  QrQtdItsPrz.Params[0].AsInteger := TabePrcCab;
  UMyMod.AbreQuery(QrQtdItsPrz, Dmod.MyDB, 'TDmProd.ImpedePeloPrazoMedio()');
  //
  MediaDD := MedDDReal;
  TipoPrzTxt := 'real';
  if QrQtdItsPrz.RecordCount > 0 then
  begin
    case (*Dm PediVda .QrParamsEmpTipMediaDD.Value*)TipMediaDD of
      0:
      begin
        Geral.MensagemBox(
        'Tipo de c�lculo de prazo m�dio n�o definido nos par�metros da filial!',
        'Aviso', MB_OK+MB_ICONWARNING);
        MediaDD := MedDDReal;
        TipoPrzTxt := 'real';
      end;
      1:
      begin
        MediaDD := MedDDSimpl;
        TipoPrzTxt := 'simples';
      end;
      2:
      begin
        MediaDD := MedDDReal;
        TipoPrzTxt := 'real';
      end;
    end;
    QrLocPrz.Close;
    QrLocPrz.Params[00].AsInteger := TabePrcCab;
    QrLocPrz.Params[01].AsFloat   := MediaDD;
    UMyMod.AbreQuery(QrLocPrz, Dmod.MyDB, 'TDmProd.Atualiza_dmkEditsPrecos3()');
    if (QrLocPrz.RecordCount = 0) then //and (QrQtdItsPrz.RecordCount > 0) then
    begin
      Result := True;
      Geral.MensagemBox(
      'Falta de faixa de prazo m�dio na lista de pre�os! ' +
      'Prazo m�dio ' + TipoPrzTxt + ': ' + FloatToStr(MediaDD) + ' dias.',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end else
    Geral.MensagemBox('Produto sem pre�os a prazo!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

function TDmProd.InsereItemPrdGruNew(IDForm: TChamouGraGruNew; PrdGrupTip,
  UnidMed: Integer; Nome, NCM: String;
  // ini 2020-09-24
  ForcaCadastrGGX_SemCorTam: Boolean;
  // fim 2020-09-04
  Nivel5: Integer = 0; Nivel4: Integer = 0;
  Nivel3: Integer = 0; Nivel2: Integer = 0; Nivel1: Integer = 0; GraGruNew:
  Boolean = False; GraTabAppCod: Integer = 0; GraTabAppTxt: String = '';
  NewNome: String = ''): Boolean;
var
  Continua: Boolean;
  Msg: String;
begin
  FChamouGraGruNew := IDForm;
  FFmGraGruNew     := GraGruNew; //Mostra a janela
  FPrdGrupTip      := PrdgrupTip;
  FPrdGrupTip_Nome := Nome;
  FEnabledPnTipo   := True;
  //
  if GraTabAppCod <> 0 then
  begin
    FGraTabAppMostra := True;
    FGraTabApp       := GraTabAppCod;
    FGraTabApp_Nome  := GraTabAppTxt;
  end else
  begin
    FGraTabAppMostra := False;
    FGraTabApp       := 0;
    FGraTabApp_Nome  := '';
  end;
  //
  DmProd.FNewGraGru5 := Nivel5;
  DmProd.FNewGraGru4 := Nivel4;
  DmProd.FNewGraGru3 := Nivel3;
  DmProd.FNewGraGru2 := Nivel2;
  DmProd.FNewGraGru1 := Nivel1;
  DmProd.FNewUnidMed := UnidMed;
  DmProd.FNewNCM     := NCM;
  //
  (*
  UMyMod.FormInsUpd_Show(TFmPrdGruNew, FmPrdGruNew, afmoNegarComAviso,
    nil, stIns);
  *)
  if UMyMod.FormInsUpd_Cria(TFmPrdGruNew, FmPrdGruNew, afmoNegarComAviso,
    nil, stIns) then
  begin
    FmPrdGruNew.FForcaCadastrGGX_SemCorTam := ForcaCadastrGGX_SemCorTam;
    (* � executado no onActivate do Form
    FmPrdGruNew.AtivaOForm(FFmGraGruNew, Msg);
    *)
    if (FFmGraGruNew = False) then
    begin
      Continua := FmPrdGruNew.AtivaOForm(False, Msg);
      //
      if Continua = True then
      begin
        Continua := FmPrdGruNew.ConfirmaCadastro(False, Msg);
        //
        if Continua = False then
          Geral.MB_Aviso(Msg);
      end else
        Geral.MB_Aviso(Msg);
    end else
      Continua := True;
    //
    if (Continua = True) and (FFmGraGruNew = True) then
    begin
      // ini 2020-09-24
      FEnabledPnTipo := True;
      FmPrdGruNew.PnTipo.Enabled := True;
      // fim 2020-09-24
      //FFmGraGruNew := False;
      FmPrdGruNew.FNewNome := NewNome;
      FmPrdGruNew.EdNome1.Text := NewNome;
      FmPrdGruNew.ShowModal;
    end;
    FmPrdGruNew.Destroy;
  end;
  FEnabledPnTipo := False;
  Result         := Continua;
end;

function TDmProd.ObtemPreco(var Preco: Double): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruVal, FmGraGruVal, afmoNegarComAviso) then
  begin
    FmGraGruVal.EdValor.ValueVariant := Preco;
    FmGraGruVal.ShowModal;
    Result := FmGraGruVal.FConfirmou;
    Preco := FmGraGruVal.EdValor.ValueVariant;
    FmGraGruVal.Destroy;
  end;
end;

function TDmProd.ObtemPrecoDeCusto(Empresa, GraGruX, GraCusPrc: Integer): Double;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT CustoPreco ',
      'FROM gragruval',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      'AND GraCusPrc=' + Geral.FF0(GraCusPrc),
      'AND Entidade=' + Geral.FF0(Empresa),
      '']);
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('CustoPreco').AsFloat;
  finally
    Qry.Free;
  end;
end;

function TDmProd.ObtemPrecoDeLista(Empresa, GraGruX, TabePrcCab, CondicaoPG: Integer;
            MedDDSimpl, MedDDReal: Double; var PrecoO, PrecoR: Double): Boolean;
var
  TipoPrzTxt: String;
  Fator, MediaDD: Double;
  Msg: String;
begin
  Result := False;
  //
  QrQtdItsPrz.Close;
  QrQtdItsPrz.Params[0].AsInteger := TabePrcCab;
  UMyMod.AbreQuery(QrQtdItsPrz, Dmod.MyDB, 'TDmProd.ObtemPrecoDeLista()');
  //
  MediaDD := MedDDReal;
  TipoPrzTxt := 'real';
  Msg := '';
  if QrQtdItsPrz.RecordCount > 0 then
  begin
    if (QrParamsEmp.State = dsInactive) or
    (QrParamsEmp.Params[0].AsInteger <> Empresa) then
    begin
      QrParamsEmp.Close;
      QrParamsEmp.Params[0].AsInteger := Empresa;
      UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
    end;
    case QrParamsEmpTipMediaDD.Value of
      0:
      begin
        Msg := Msg +
          'Tipo de c�lculo de prazo m�dio n�o definido nos par�metros da filial!'
          + slineBreak;
        MediaDD := MedDDReal;
        TipoPrzTxt := 'real';
      end;
      1:
      begin
        MediaDD := MedDDSimpl;
        TipoPrzTxt := 'simples';
      end;
      2:
      begin
        MediaDD := MedDDReal;
        TipoPrzTxt := 'real';
      end;
    end;
    QrLocPrz.Close;
    QrLocPrz.Params[00].AsInteger := TabePrcCab;
    QrLocPrz.Params[01].AsFloat   := MediaDD;
    UMyMod.AbreQuery(QrLocPrz, Dmod.MyDB, 'TDmProd.ObtemPrecoDeLista()');
    if (QrLocPrz.RecordCount = 0) then //and (QrQtdItsPrz.RecordCount > 0) then
    begin
      Msg := Msg + 'Falta de faixa de prazo m�dio na lista de pre�os! ' +
      'Prazo m�dio ' + TipoPrzTxt + ': ' + FloatToStr(MediaDD) + ' dias.' +
      sLineBreak;
      Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //
    Fator := (QrLocPrzVariacao.Value / 100) + 1;
    //
    PrecoO := DmProd.ObtemPrecoGraGruX_GraCusPrc(GraGruX, TabePrcCab);
    PrecoR := dmkPF.FFF(PrecoO * Fator, Dmod.QrControle.FieldByName('CasasProd').AsInteger, siNegativo);
  end else begin
    Msg := Msg + 'Produto sem pre�os a prazo!';
    //
    Fator := 1; // sem custo financeiro / desconto
    PrecoO := DmProd.ObtemPrecoGraGruX_GraCusPrc(GraGruX, TabePrcCab);
    PrecoR := PrecoO * Fator;
  end;
  if Msg <> '' then
    Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING);
end;

{Desabilitado pelo Marcelo
SELECT smia.GraGruX, GraTamI, GraGruC, SUM(smia.Qtde) QTDE
FROM stqmovitsa smia
LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX
WHERE smia.Ativo=1
GROUP BY smia.GraGruX
}

end.
