object FmStqCnsgBkIts: TFmStqCnsgBkIts
  Left = 339
  Top = 185
  Caption = 'STQ-MOVIM-007 :: Consigna'#231#227'o - Itens de Retorno'
  ClientHeight = 429
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 468
  Constraints.MinWidth = 580
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 657
    Height = 273
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object PnData: TPanel
      Left = 0
      Top = 56
      Width = 657
      Height = 217
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      ExplicitWidth = 667
      object CkContinuar: TCheckBox
        Left = 8
        Top = 192
        Width = 112
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 3
      end
      object Panel8: TPanel
        Left = 0
        Top = 128
        Width = 657
        Height = 61
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitWidth = 667
        object GroupBox1: TGroupBox
          Left = 277
          Top = 0
          Width = 380
          Height = 61
          Align = alClient
          Caption = ' Quantidade e custo total: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          ExplicitWidth = 390
          object LaQtde: TLabel
            Left = 8
            Top = 16
            Width = 68
            Height = 13
            Caption = '??? Qtde ???:'
          end
          object Label8: TLabel
            Left = 88
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Valor total:'
            Enabled = False
          end
          object Label9: TLabel
            Left = 200
            Top = 16
            Width = 57
            Height = 13
            Caption = 'Custo Frete:'
            Enabled = False
          end
          object Label20: TLabel
            Left = 288
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Custo total:'
            Enabled = False
          end
          object SbPreco: TSpeedButton
            Left = 173
            Top = 32
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SbPrecoClick
          end
          object EdQtde: TdmkEdit
            Left = 8
            Top = 32
            Width = 77
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PLE'
            UpdCampo = 'PLE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdQtdeRedefinido
          end
          object EdCustoBuy: TdmkEdit
            Left = 88
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoBuy'
            UpdCampo = 'CustoBuy'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCustoBuyRedefinido
          end
          object EdCustoFrt: TdmkEdit
            Left = 200
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoFrt'
            UpdCampo = 'CustoFrt'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCustoFrtRedefinido
          end
          object EdValorAll: TdmkEdit
            Left = 288
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoAll'
            UpdCampo = 'CustoAll'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 0
          Width = 277
          Height = 61
          Align = alLeft
          Caption = 'Estoque: '
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
            FocusControl = DBEdit6
          end
          object Label4: TLabel
            Left = 88
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Custo total:'
            FocusControl = DBEdit7
          end
          object Label6: TLabel
            Left = 180
            Top = 16
            Width = 61
            Height = 13
            Caption = 'Custo m'#233'dio:'
            FocusControl = DBEdit4
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 32
            Width = 77
            Height = 21
            TabStop = False
            DataField = 'Qtde'
            DataSource = DsEstoque
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 88
            Top = 32
            Width = 87
            Height = 21
            TabStop = False
            DataField = 'CustoAll'
            DataSource = DsEstoque
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 180
            Top = 32
            Width = 87
            Height = 21
            TabStop = False
            DataField = 'CusMed'
            DataSource = DsEstoque
            TabOrder = 2
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 657
        Height = 64
        Align = alTop
        Caption = ' Origem da remessa: '
        TabOrder = 0
        ExplicitWidth = 667
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 653
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 663
          object dmkLabel3: TdmkLabel
            Left = 4
            Top = 5
            Width = 124
            Height = 13
            Caption = 'Centro de estoque origem:'
            UpdType = utYes
            SQLType = stNil
          end
          object Label7: TLabel
            Left = 394
            Top = 5
            Width = 164
            Height = 13
            Caption = 'Localiza'#231#227'o no centro de estoque:'
          end
          object EdStqCenCadOri: TdmkEditCB
            Left = 4
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdStqCenCadOriChange
            DBLookupComboBox = CBStqCenCadOri
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenCadOri: TdmkDBLookupComboBox
            Left = 60
            Top = 21
            Width = 329
            Height = 21
            DropDownRows = 2
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsStqCenCadOri
            TabOrder = 1
            dmkEditCB = EdStqCenCadOri
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdStqCenLocOri: TdmkEditCB
            Left = 394
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdStqCenLocOriRedefinido
            DBLookupComboBox = CBStqCenLocOri
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenLocOri: TdmkDBLookupComboBox
            Left = 450
            Top = 21
            Width = 199
            Height = 21
            DropDownRows = 2
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsStqCenLocOri
            TabOrder = 3
            dmkEditCB = EdStqCenLocOri
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 64
        Width = 657
        Height = 64
        Align = alTop
        Caption = 'Destino da remessa: '
        TabOrder = 1
        ExplicitWidth = 667
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 653
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 663
          object dmkLabel1: TdmkLabel
            Left = 4
            Top = 5
            Width = 127
            Height = 13
            Caption = 'Centro de estoque destino:'
            UpdType = utYes
            SQLType = stNil
          end
          object Label2: TLabel
            Left = 394
            Top = 5
            Width = 164
            Height = 13
            Caption = 'Localiza'#231#227'o no centro de estoque:'
          end
          object EdStqCenCadDst: TdmkEditCB
            Left = 4
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdStqCenCadDstChange
            DBLookupComboBox = CBStqCenCadDst
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenCadDst: TdmkDBLookupComboBox
            Left = 60
            Top = 21
            Width = 329
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsStqCenCadDst
            TabOrder = 1
            dmkEditCB = EdStqCenCadDst
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdStqCenLocDst: TdmkEditCB
            Left = 394
            Top = 21
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLocDst
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenLocDst: TdmkDBLookupComboBox
            Left = 450
            Top = 21
            Width = 199
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsStqCenLocDst
            TabOrder = 3
            dmkEditCB = EdStqCenLocDst
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 657
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 667
      object PnGGX: TPanel
        Left = 85
        Top = 0
        Width = 572
        Height = 56
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        ExplicitWidth = 582
        object Label12: TLabel
          Left = 0
          Top = 8
          Width = 76
          Height = 13
          Caption = 'Nome do grupo:'
        end
        object Label13: TLabel
          Left = 276
          Top = 8
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label14: TLabel
          Left = 372
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object Label1: TLabel
          Left = 428
          Top = 8
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object Label5: TLabel
          Left = 488
          Top = 8
          Width = 31
          Height = 13
          Caption = 'Pre'#231'o:'
          FocusControl = DBEdit8
        end
        object DBEdit1: TDBEdit
          Left = 0
          Top = 24
          Width = 273
          Height = 21
          TabStop = False
          DataField = 'NO_GG1'
          DataSource = DsGraGruX
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 276
          Top = 24
          Width = 93
          Height = 21
          TabStop = False
          DataField = 'NO_COR'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 372
          Top = 24
          Width = 53
          Height = 21
          TabStop = False
          DataField = 'NO_TAM'
          DataSource = DsGraGruX
          TabOrder = 2
        end
        object DBEdit5: TDBEdit
          Left = 427
          Top = 24
          Width = 58
          Height = 21
          DataField = 'No_SIGLA'
          DataSource = DsGraGruX
          TabOrder = 3
          OnChange = DBEdit5Change
        end
        object DBEdit8: TDBEdit
          Left = 488
          Top = 24
          Width = 77
          Height = 21
          DataField = 'Preco'
          DataSource = DsPreco
          TabOrder = 4
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 85
        Height = 56
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label11: TLabel
          Left = 8
          Top = 8
          Width = 69
          Height = 13
          Caption = 'Reduzido: [F4]'
        end
        object SpeedButton1: TSpeedButton
          Left = 64
          Top = 24
          Width = 21
          Height = 21
          Caption = '?'
          OnClick = SpeedButton1Click
        end
        object EdGraGruX: TdmkEdit
          Left = 8
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
          OnExit = EdGraGruXExit
          OnKeyDown = EdGraGruXKeyDown
          OnRedefinido = EdGraGruXRedefinido
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 667
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 619
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        TabStop = False
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 571
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 387
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 387
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 387
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Retorno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 321
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 667
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 663
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 365
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 667
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 663
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 519
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 657
    Top = 48
    Width = 351
    Height = 273
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    ExplicitLeft = 840
    ExplicitTop = 164
    ExplicitWidth = 185
    ExplicitHeight = 41
    object Label10: TLabel
      Left = 0
      Top = 260
      Width = 351
      Height = 13
      Align = alBottom
      Caption = 
        'Aten'#231#227'o: Os estoques acima j'#225' consideram as baixas deste movimen' +
        'to!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 337
    end
    object DBGPrdEstq: TdmkDBGridZTO
      Left = 0
      Top = 0
      Width = 351
      Height = 260
      Align = alClient
      DataSource = DsPrdEstq
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBGPrdEstqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Produto'
          Width = 154
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QTDE'
          Title.Caption = 'Quantidade'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTot'
          Title.Caption = '$ Estoque'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrcCusUni'
          Title.Caption = '$ Unit'#225'rio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA'
          Title.Caption = 'Sigla'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PGT'
          Title.Caption = 'Tipo de Produto'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel1'
          Title.Caption = 'ID'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SCC'
          Title.Caption = 'Centro de estoque'
          Width = 96
          Visible = True
        end>
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX,  '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM, '
      'med.Nome, med.Sigla, med.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ')
    Left = 168
    Top = 300
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNo_SIGLA: TWideStringField
      FieldName = 'No_SIGLA'
      Size = 30
    end
    object QrGraGruXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGraGruXGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGraGruXFracio: TIntegerField
      FieldName = 'Fracio'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 168
    Top = 348
  end
  object QrStqCenLocOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM stqcenloc '
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 608
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocOriControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocOriNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLocOri: TDataSource
    DataSet = QrStqCenLocOri
    Left = 608
    Top = 108
  end
  object QrStqCenCadOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 704
    Top = 60
    object QrStqCenCadOriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadOriCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadOriNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadOri: TDataSource
    DataSet = QrStqCenCadOri
    Left = 704
    Top = 108
  end
  object QrStqCenLocDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM stqcenloc '
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 608
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocDstControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocDstNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLocDst: TDataSource
    DataSet = QrStqCenLocDst
    Left = 608
    Top = 208
  end
  object DsStqCenCadDst: TDataSource
    DataSet = QrStqCenCadDst
    Left = 708
    Top = 208
  end
  object QrStqCenCadDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 708
    Top = 160
    object QrStqCenCadDstCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadDstCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadDstNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    Left = 512
    Top = 268
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueCustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEstoqueCusMed: TFloatField
      FieldName = 'CusMed'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 580
    Top = 268
  end
  object QrPreco: TMySQLQuery
    Database = Dmod.MyDB
    Left = 336
    Top = 300
    object QrPrecoPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 336
    Top = 348
  end
  object QrPrdEstq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,'
      'SUM(smia.Qtde) QTDE, SUM(smia.Pecas) PECAS, '
      'SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2, '
      'SUM(smia.AreaP2) AREAP2, smia.GraGruX, gg1.NCM'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE smia.Ativo=1'
      'AND smia.Empresa=-11'
      'AND gg1.PrdGrupTip=2'
      'GROUP BY smia.GraGruX'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX'
      '')
    Left = 252
    Top = 300
    object QrPrdEstqNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPrdEstqNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 511
    end
    object QrPrdEstqPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrPrdEstqUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrPrdEstqNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrPrdEstqSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrPrdEstqNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrPrdEstqNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrPrdEstqGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrPrdEstqGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrPrdEstqGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrPrdEstqGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrPrdEstqQTDE: TFloatField
      FieldName = 'QTDE'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPrdEstqPECAS: TFloatField
      FieldName = 'PECAS'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrPrdEstqPESO: TFloatField
      FieldName = 'PESO'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrPrdEstqAREAM2: TFloatField
      FieldName = 'AREAM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPrdEstqAREAP2: TFloatField
      FieldName = 'AREAP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPrdEstqGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPrdEstqNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrPrdEstqPrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPrdEstqValorTot: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ValorTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
    object QrPrdEstqNO_SCC: TWideStringField
      FieldName = 'NO_SCC'
      Size = 30
    end
    object QrPrdEstqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdEstqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 511
    end
  end
  object DsPrdEstq: TDataSource
    DataSet = QrPrdEstq
    Left = 252
    Top = 348
  end
end
