unit PrdGrupAtr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, DB, Grids,
  DBGrids, dmkImage, UnDmkEnums, mySQLDbTables,
  dmkDBGridZTO;

type
  THackDBGrid = class(TDBGrid);
  TFmPrdGrupAtr = class(TForm)
    Panel1: TPanel;
    DataSource1: TDataSource;
    DBGItsToSel: TdmkDBGridZTO;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Query: TMySQLQuery;
    QueryCodigo: TIntegerField;
    QueryCodUsu: TIntegerField;
    QueryNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGItsToSelColExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmPrdGrupAtr: TFmPrdGrupAtr;

implementation

uses UnMyObjects(*, MyVCLSkin*);

{$R *.DFM}

procedure TFmPrdGrupAtr.BtOKClick(Sender: TObject);
begin
  FCadastrar := True;
  Close;
end;

procedure TFmPrdGrupAtr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdGrupAtr.DBGItsToSelColExit(Sender: TObject);
begin
(*&&
  DBGrid1.Options := DBGrid1.Options - [dgAlwaysShowEditor];
*)
end;

procedure TFmPrdGrupAtr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrdGrupAtr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPrdGrupAtr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

