object FmGraGruEPat: TFmGraGruEPat
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-049 :: Estoque de Patrim'#244'nio'
  ClientHeight = 629
  ClientWidth = 927
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 927
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 879
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 831
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 274
        Height = 32
        Caption = 'Estoque de Patrim'#244'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 274
        Height = 32
        Caption = 'Estoque de Patrim'#244'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 274
        Height = 32
        Caption = 'Estoque de Patrim'#244'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 927
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 927
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
      end
      object Label2: TLabel
        Left = 92
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Referencia:'
      end
      object Label3: TLabel
        Left = 216
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object BitBtn1: TBitBtn
        Left = 696
        Top = 20
        Width = 75
        Height = 25
        Caption = 'Pesquisa'
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object EdReduzido: TdmkEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdReferencia: TdmkEdit
        Left = 92
        Top = 20
        Width = 121
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 216
        Top = 20
        Width = 453
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object DBGPsq: TdmkDBGridZTO
      Left = 0
      Top = 49
      Width = 927
      Height = 418
      Align = alClient
      DataSource = DsPsq
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBGPsqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Patrimonio'
          Title.Caption = 'Patrim'#244'nio'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Referencia'
          Title.Caption = 'Refer'#234'ncia'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Nome'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqAnt'
          Title.Caption = 'Estq.Ant.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqEnt'
          Title.Caption = 'Entradas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqLoc'
          Title.Caption = 'Locado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqSai'
          Title.Caption = 'Sa'#237'das'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqSdo'
          Title.Caption = 'Saldo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Empresa'
          Width = 46
          Visible = True
        end>
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 927
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 923
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 927
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 781
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 779
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAlteraClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 24
    Top = 7
  end
  object QrPsq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPsqAfterOpen
    BeforeClose = QrPsqBeforeClose
    SQL.Strings = (
      'SELECT pat.EstqAnt, pat.EstqEnt, pat.EstqSai, pat.EstqSdo, '
      'ggx.Controle Reduzido, ggx.GraGru1 Produto, '
      'CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e)))  '
      'NO_PRD_TAM_COR, gg1.Patrimonio, gg1.Referencia   '
      'FROM gragrux ggx  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruepat pat ON pat.GraGruX=ggx.Controle')
    Left = 460
    Top = 304
    object QrPsqEstqAnt: TFloatField
      FieldName = 'EstqAnt'
    end
    object QrPsqEstqEnt: TFloatField
      FieldName = 'EstqEnt'
    end
    object QrPsqEstqLoc: TFloatField
      FieldName = 'EstqLoc'
    end
    object QrPsqEstqSai: TFloatField
      FieldName = 'EstqSai'
    end
    object QrPsqEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrPsqReduzido: TIntegerField
      FieldName = 'Reduzido'
      Required = True
    end
    object QrPsqProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrPsqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPsqPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrPsqReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrPsqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object DsPsq: TDataSource
    DataSet = QrPsq
    Left = 456
    Top = 356
  end
  object QrEPat: TMySQLQuery
    Database = Dmod.MyDB
    Left = 208
    Top = 320
    object QrEPatGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
end
