unit GraGru2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, Vcl.Mask,
  mySQLDbTables, dmkDBLookupComboBox, dmkEditCB;

type
  TFmGraGru2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    EdNivel2: TdmkEdit;
    EdNome: TdmkEdit;
    Label28: TLabel;
    EdTipo_Item: TdmkEdit;
    EdTipo_Item_TXT: TdmkEdit;
    QrCtbCadGru: TMySQLQuery;
    QrCtbCadGruCodigo: TIntegerField;
    QrCtbCadGruNome: TWideStringField;
    DsCtbCadGru: TDataSource;
    QrContas: TMySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label1: TLabel;
    EdCtbCadGru: TdmkEditCB;
    CBCtbCadGru: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdCtbPlaCta: TdmkEditCB;
    CBCtbPlaCta: TdmkDBLookupComboBox;
    SbCtbCadGru: TSpeedButton;
    SbCtbPlaCta: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTipo_ItemChange(Sender: TObject);
    procedure EdTipo_ItemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNivel2Change(Sender: TObject);
    procedure SbCtbPlaCtaClick(Sender: TObject);
    procedure SbCtbCadGruClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FPrdGrupTip, FNivel5, FNivel4, FNivel3: Integer;
  end;

  var
  FmGraGru2: TFmGraGru2;

implementation

uses UnMyObjects,
{$IfNDef NO_FINANCEIRO}UnFinanceiro, UnFinanceiroJan, {$EndIf}
Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGru2.BtOKClick(Sender: TObject);
var
  Nome: String;
  //Nivel5, Nivel4, Nivel3,
  CodUsu, Nivel2(*, ContaCTB, PrdGrupTip*), Tipo_Item: Integer;
  SQLType: TSQLType;
  CtbPlaCta, CtbCadGru: Integer;
begin
  SQLType := ImgTipo.SQLType;
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
(*  Nivel5         := ;
  Nivel4         := ;
  Nivel3         := ;*)
  Nivel2         := EdNivel2.ValueVariant;
  //CodUsu         := ;
  Nome           := EdNome.Text;
  (*ContaCTB       := ;
  PrdGrupTip     := ;*)
  Tipo_Item      := EdTipo_Item.ValueVariant;
  CtbPlaCta      := EdCtbPlaCta.ValueVariant;
  CtbCadGru      := EdCtbCadGru.ValueVariant;
  if SQLType = stIns then
  begin
    Nivel2 := UMyMod.BuscaEmLivreY_Def('gragru2', 'Nivel2', SQLType, Nivel2);
    EdNivel2.ValueVariant := Nivel2;
    CodUsu := Nivel2;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru2', False, [
    'Nivel5', 'Nivel4', 'Nivel3',
    'CodUsu', 'Nome', (*'ContaCTB',*)
    'PrdGrupTip', 'Tipo_Item',
    'CtbPlaCta', 'CtbCadGru'], [
    'Nivel2'], [
    FNivel5, FNivel4, FNivel3,
    CodUsu, Nome, (* ContaCTB,*)
    FPrdGrupTip, Tipo_Item,
    CtbPlaCta, CtbCadGru], [
    Nivel2], True) then
    begin
      //ReopenCadastro_Com_Itens_ITS(Controle);
    end;
  end else
  begin
    Nivel2 := UMyMod.BuscaEmLivreY_Def('gragru2', 'Nivel2', SQLType, Nivel2);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru2', False, [
    (*'Nivel5', 'Nivel4', 'Nivel3',
    'CodUsu',*) 'Nome', (*'ContaCTB',
    'PrdGrupTip',*) 'Tipo_Item',
    'CtbPlaCta', 'CtbCadGru'], [
    'Nivel2'], [
    (*Nivel5, Nivel4, Nivel3,
    CodUsu,*) Nome(*, ContaCTB,
    PrdGrupTip*), Tipo_Item,
    CtbPlaCta, CtbCadGru], [
    Nivel2], True) then
    begin
      //ReopenCadastro_Com_Itens_ITS(Controle);
    end;
  end;
  //
  Close;
end;

procedure TFmGraGru2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGru2.EdNivel2Change(Sender: TObject);
begin
  EdNome.Enabled := EdNivel2.ValueVariant > 0;
end;

procedure TFmGraGru2.EdTipo_ItemChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTipo_Item_TXT.Text := UFinanceiro.SPED_Tipo_Item_Get(EdTipo_Item.ValueVariant);
{$EndIf}
end;

procedure TFmGraGru2.EdTipo_ItemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdTipo_Item.Text := UFinanceiro.ListaDeSPED_Tipo_Item();
{$EndIf}
end;

procedure TFmGraGru2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGru2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FNivel5 := 0;
  FNivel4 := 0;
  FNivel3 := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrCtbCadGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmGraGru2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGru2.SbCtbCadGruClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormCtbCadGru(EdCtbCadGru.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbCadGru, CBCtbCadGru, QrCtbCadGru, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmGraGru2.SbCtbPlaCtaClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormContas(EdCtbPlaCta.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbPlaCta, CBCtbPlaCta, QrContas, VAR_CADASTRO);
{$EndIf}
end;

end.
