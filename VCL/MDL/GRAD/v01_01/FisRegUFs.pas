unit FisRegUFs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids, UnDmkProcFunc, DBGrids,
  dmkDBGrid, DB, mySQLDbTables, dmkDBGridDAC, dmkRadioGroup, dmkImage,
  UnDmkEnums, UnMySQLCuringa, SPED_Listas, dmkGeral,
  Vcl.ComCtrls, dmkCheckBox, Vcl.DBCtrls, dmkDBLookupComboBox, dmkEditCB;

type
  TFmFisRegUFs = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    DBGFisRegCFOP: TdmkDBGrid;
    Panel5: TPanel;
    DsUFsAtoB: TDataSource;
    QrUFsAtoB: TmySQLQuery;
    QrUFsAtoBNome: TWideStringField;
    QrUFsAtoBAtivo: TSmallintField;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox5: TGroupBox;
    Panel7: TPanel;
    Label24: TLabel;
    EdUFEmit: TdmkEdit;
    Label2: TLabel;
    EdICMSAliq: TdmkEdit;
    Label3: TLabel;
    EdPISAliq: TdmkEdit;
    Label4: TLabel;
    EdCOFINSAliq: TdmkEdit;
    GroupBox6: TGroupBox;
    Panel9: TPanel;
    EdUFDest: TdmkEdit;
    Label1: TLabel;
    Label5: TLabel;
    EdpICMSUFDest: TdmkEdit;
    Label8: TLabel;
    EdpFCPUFDest: TdmkEdit;
    Label9: TLabel;
    EdpBCUFDest: TdmkEdit;
    Label10: TLabel;
    EdpICMSInter: TdmkEdit;
    LapICMSInterPart: TLabel;
    EdpICMSInterPart: TdmkEdit;
    Panel10: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Panel11: TPanel;
    CkUsaInterPartLei: TCheckBox;
    SbAjuda: TBitBtn;
    PCTES: TPageControl;
    TabSheet1: TTabSheet;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label14: TLabel;
    EdCST_B: TdmkEdit;
    EdTextoB: TdmkEdit;
    EdCSOSN: TdmkEdit;
    EdTextoC: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel12: TPanel;
    BtNaoSel: TBitBtn;
    RGmodBC: TdmkRadioGroup;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    Label13: TLabel;
    EdpRedBC: TdmkEdit;
    EdpDif: TdmkEdit;
    TabSheet2: TTabSheet;
    Panel14: TPanel;
    Panel15: TPanel;
    Label476: TLabel;
    EdOriCST_ICMS: TdmkEdit;
    EdOriTextoB: TdmkEdit;
    Label477: TLabel;
    EdOriCST_IPI: TdmkEdit;
    EdOriTextoIPI_CST: TdmkEdit;
    Label478: TLabel;
    EdOriCST_PIS: TdmkEdit;
    EdOriTextoPIS_CST: TdmkEdit;
    Label480: TLabel;
    EdOriCST_COFINS: TdmkEdit;
    EdOriTextoCOFINS_CST: TdmkEdit;
    Panel16: TPanel;
    Label181: TLabel;
    EdGenCtbD: TdmkEditCB;
    CBGenCtbD: TdmkDBLookupComboBox;
    SbGenCtbD: TSpeedButton;
    Label12: TLabel;
    EdcBenef: TdmkEdit;
    Label15: TLabel;
    EdGenCtbC: TdmkEditCB;
    CBGenCtbC: TdmkDBLookupComboBox;
    SbGenCtbC: TSpeedButton;
    EdTES_ICMS: TdmkEdit;
    EdTES_ICMS_TXT: TdmkEdit;
    EdTES_IPI: TdmkEdit;
    EdTES_IPI_TXT: TdmkEdit;
    EdTES_PIS: TdmkEdit;
    EdTES_COFINS: TdmkEdit;
    EdTES_PIS_TXT: TdmkEdit;
    EdTES_COFINS_TXT: TdmkEdit;
    EdTES_BC_COFINS_TXT: TdmkEdit;
    EdTES_BC_COFINS: TdmkEdit;
    EdTES_BC_PIS: TdmkEdit;
    EdTES_BC_IPI: TdmkEdit;
    EdTES_BC_ICMS: TdmkEdit;
    EdTES_BC_PIS_TXT: TdmkEdit;
    EdTES_BC_IPI_TXT: TdmkEdit;
    EdTES_BC_ICMS_TXT: TdmkEdit;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    QrGet: TMySQLQuery;
    QrGetConta: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure EdCST_BChange(Sender: TObject);
    procedure EdCST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtNaoSelClick(Sender: TObject);
    procedure CkUsaInterPartLeiClick(Sender: TObject);
    procedure SbAjudaClick(Sender: TObject);
    procedure EdCSOSNChange(Sender: TObject);
    procedure EdCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdOriCST_ICMSChange(Sender: TObject);
    procedure EdOriCST_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdOriCST_IPIChange(Sender: TObject);
    procedure EdOriCST_IPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdOriCST_PISChange(Sender: TObject);
    procedure EdOriCST_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdOriCST_COFINSChange(Sender: TObject);
    procedure EdOriCST_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGOriTESClick(Sender: TObject);
    procedure EdTES_ICMSChange(Sender: TObject);
    procedure EdTES_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTES_IPIChange(Sender: TObject);
    procedure EdTES_PISChange(Sender: TObject);
    procedure EdTES_COFINSChange(Sender: TObject);
    procedure EdTES_IPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTES_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTES_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTES_BC_ICMSChange(Sender: TObject);
    procedure EdTES_BC_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTES_BC_IPIChange(Sender: TObject);
    procedure EdTES_BC_PISChange(Sender: TObject);
    procedure EdTES_BC_COFINSChange(Sender: TObject);
    procedure EdTES_BC_IPIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTES_BC_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTES_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FUFsAtoB: String;
    FData: TDateTime;
    F_TES_ITENS, F_TES_BC_ITENS: MyArrayLista;
    procedure AtivaItens(Status: Integer);
  public
    { Public declarations }
  end;

  var
  FmFisRegUFs: TFmFisRegUFs;

implementation

uses
  {$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
  {$IfNDef SemNFe_0000} ModuleNFe_0000, NFe_PF, {$EndIf}
  UnMyObjects, FisRegCad, ModuleGeral, UCreate, UnInternalConsts, UMySQLModule,
  Module, DmkDAC_PF, UnDmkWeb, UnFinanceiroJan;

{$R *.DFM}


procedure TFmFisRegUFs.AtivaItens(Status: Integer);
var
  UF: String;
begin
  Screen.Cursor := crHourGlass;
  UF := QrUFsAtoBNome.Value;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FUFsAtoB);
  DmodG.QrUpdPID1.SQL.Add('SET Ativo=' + IntToStr(Status));
  DmodG.QrUpdPID1.ExecSQL;
  //
  QrUFsAtoB.Close;
  UnDmkDAC_PF.AbreQuery(QrUFsAtoB, DModG.MyPID_DB);
  QrUFsAtoB.Locate('Nome', UF, [loCaseInsensitive]);
  Screen.Cursor := crDefault;
end;

procedure TFmFisRegUFs.BtNaoSelClick(Sender: TObject);
begin
  RGmodBC.ItemIndex := -1;
end;

procedure TFmFisRegUFs.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmFisRegUFs.BtOKClick(Sender: TObject);
{
  procedure InsUpdAlquotas(const UFEmit, UFDest: String; const modBC: Integer;
  const ICMSAliq, PISALiq, COFINSAliq, pRedBC: Double; CST_B: String;
  var Conta: Integer);
  begin
    Dmod.QrUpd.Params[00].AsInteger := FmFisRegCad.QrFisRegCFOPCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmFisRegCad.QrFisRegCFOPInterno.Value;
    Dmod.QrUpd.Params[02].AsInteger := FmFisRegCad.QrFisRegCFOPContribui.Value;
    Dmod.QrUpd.Params[03].AsInteger := FmFisRegCad.QrFisRegCFOPProprio.Value;
    Dmod.QrUpd.Params[04].AsString  := UFEmit;
    Dmod.QrUpd.Params[05].AsString  := UFDest;
    Dmod.QrUpd.Params[06].AsFloat   := ICMSAliq;
    Dmod.QrUpd.Params[07].AsString  := Trim(CST_B);
    Dmod.QrUpd.Params[08].AsFloat   := pRedBC;
    Dmod.QrUpd.Params[09].AsInteger := modBC;
    //
    Dmod.QrUpd.Params[10].AsFloat   := ICMSAliq;
    Dmod.QrUpd.Params[11].AsString  := Trim(CST_B);
    Dmod.QrUpd.Params[12].AsFloat   := pRedBC;
    Dmod.QrUpd.Params[13].AsInteger := modBC;
    UMyMod.ExecutaQuery(Dmod.QrUpd);
    //
    Conta := Conta + 1;
  end;
var
  Msg, UFEmit, UFDest, CST_B: String;
  ICMSAliq, PISALiq, COFINSAliq, pRedBC: Double;
  modBC, Conta: Integer;
begin
  UFEmit := Trim(EdUFEmit.Text);
  if UFEmit = '' then
  begin
    Geral.MB_Aviso('Informe a UF emitente!');
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    Conta := 0;
    ICMSALiq   := EdICMSAliq.ValueVariant;
    PISALiq    := EdPISAliq.ValueVariant;
    COFINSALiq := EdCOFINSAliq.ValueVariant;
    CST_B      := Trim(EdCST_B.Text);
    if CST_B = '0' then
      CST_B := '00';
    if CST_B <> '' then
    begin
      try
        Geral.IMV(EdCST_B.Text);
      except
        if MyObjects.FIC(True, EdCST_B, 'O CST informado para o ICMS � inv�lido!') then
          Exit;
      end;
    end;
    pRedBC := EdpRedBC.ValueVariant;
    modBC := RGmodBC.ItemIndex;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fisregufs (');
    Dmod.QrUpd.SQL.Add('Codigo,interno,Contribui,Proprio,UFEmit,');
    Dmod.QrUpd.SQL.Add('UFDest,ICMSAliq,CST_B,pRedBC,modBC) VALUES (');
    Dmod.QrUpd.SQL.Add(':P0,:P1,:P2,:P3,:P4,:P5,:P6,:P7,:P8,:P9)');
    Dmod.QrUpd.SQL.Add('ON DUPLICATE KEY UPDATE ICMSAliq=:P10, CST_B=:P11, ');
    Dmod.QrUpd.SQL.Add('pRedBC=:P12, modBC=:P13');
    UFDest := Trim(EdUFDest.Text);
    if Length(UFDest) = 2 then
      InsUpdAlquotas(UFEmit, UFDest, modBC, ICMSAliq, PISALiq, COFINSAliq,
        pRedBC, CST_B, Conta);
    QrUFsAtoB.First;
    while not QrUFsAtoB.Eof do
    begin
      if QrUFsAtoBAtivo.Value = 1 then
        InsUpdAlquotas(UFEmit, QrUFsAtoBNome.Value, modBC, ICMSAliq, PISALiq,
          COFINSAliq, pRedBC, CST_B, Conta);
      QrUFsAtoB.Next;
    end;
    case Conta of
      0: Msg := 'N�o foi inclu�do/alterado nenhum registro!';
      1: Msg := 'Foi inclu�do/alterado um registro!';
      else Msg := 'Foram inclu�dos/alterados ' + IntToStr(Conta) + ' registros!';
    end;
    Geral.MB_Aviso(Msg);
  finally
    Screen.Cursor := crDefault;
  end;
end;
}
var
  Codigo, Controle, Interno, Contribui, Proprio, Servico, SubsTrib: Integer;
  OriCFOP: String;
  //
  procedure InsUpdAlquotas(const UFEmit, UFDest: String; const modBC: Integer;
  const ICMSAliq, PISALiq, COFINSAliq, pRedBC: Double; CST_B, CSOSN: String;
  pBCUFDest, pFCPUFDest, pICMSUFDest, pICMSInter, pICMSInterPart: Double;
  const UsaInterPartLei: Integer; const cBenef: String; const pDif: Double;
  (*const OriTES: Integer;*) const OriCST_ICMS, OriCST_IPI, OriCST_PIS,
  OriCST_COFINS: String; const (*EFD_II_C195,*) GenCtbD, GenCtbC,
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
  TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
  var QtdInseridos: Integer);
  var
    Conta: Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGet, Dmod.MyDB, [
    'SELECT Conta  ',
    'FROM Fisregufs ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Controle=' + Geral.FF0(Controle),
    'AND Interno=' + Geral.FF0(Interno),
    'AND Contribui=' + Geral.FF0(Contribui),
    'AND Proprio=' + Geral.FF0(Proprio),
    'AND SubsTrib=' + Geral.FF0(SubsTrib),
    'AND Servico=' + Geral.FF0(Servico),
    'AND OriCFOP="' + OriCFOP + '"',
    'AND UFEmit="' + UFEmit + '"',
    'AND UFDest="' + UFDest + '"',
    '']);
     if QrGetConta.Value > 0 then
       Conta := QrGetConta.Value
     else
       Conta := UMyMod.BPGS1I32('fisregufs', 'Conta', '', '', tsPos, stIns, 0);
     //
     if UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'fisregufs', False, [
    'ICMSAliq', 'PISAliq', 'COFINSAliq',
    'CST_B', 'CSOSN', 'pRedBC', 'pDif',
    'modBC', 'pBCUFDest', 'pFCPUFDest',
    'pICMSUFDest', 'pICMSInter', 'pICMSInterPart',
    'UsaInterPartLei', 'cBenef', (*'OriTES',*)
    'OriCST_ICMS', 'OriCST_IPI', 'OriCST_PIS',
    'OriCST_COFINS', (*'EFD_II_C195',*)
    'GenCtbD', 'GenCtbC', 'TES_ICMS',
    'TES_IPI', 'TES_PIS', 'TES_COFINS',
    'TES_BC_ICMS',
    'TES_BC_IPI', 'TES_BC_PIS', 'TES_BC_COFINS'

    ], [

    'Conta', 'Controle',

    'Codigo', 'Interno', 'Contribui',
    'Proprio', 'Servico', 'SubsTrib',
    'OriCFOP', 'UFEmit', 'UFDest'], [

    'ICMSAliq', 'PISAliq', 'COFINSAliq',
    'CST_B', 'CSOSN', 'pRedBC', 'pDif',
    'modBC', 'pBCUFDest', 'pFCPUFDest',
    'pICMSUFDest', 'pICMSInter', 'pICMSInterPart',
    'UsaInterPartLei', 'cBenef', (*'OriTES',*)
    'OriCST_ICMS', 'OriCST_IPI', 'OriCST_PIS',
    'OriCST_COFINS', (*'EFD_II_C195',*)
    'GenCtbD', 'GenCtbC', 'TES_ICMS',
    'TES_IPI', 'TES_PIS', 'TES_COFINS',
    'TES_BC_ICMS',
    'TES_BC_IPI', 'TES_BC_PIS', 'TES_BC_COFINS'
    ], [

    ICMSAliq, PISAliq, COFINSAliq,
    CST_B, CSOSN, pRedBC, pDif,
    modBC, pBCUFDest, pFCPUFDest,
    pICMSUFDest, pICMSInter, pICMSInterPart,
    UsaInterPartLei, cBenef, (*OriTES,*)
    OriCST_ICMS, OriCST_IPI, OriCST_PIS,
    OriCST_COFINS, (*EFD_II_C195,*)
    GenCtbD, GenCtbC, TES_ICMS,
    TES_IPI, TES_PIS, TES_COFINS,
    TES_BC_ICMS,
    TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS
    ], [

    Conta, Controle,

    Codigo, Interno, Contribui,
    Proprio, Servico, SubsTrib,
    OriCFOP, UFEmit, UFDest], [


    ICMSAliq, PISAliq, COFINSAliq,
    CST_B, CSOSN, pRedBC, pDif,
    modBC, pBCUFDest, pFCPUFDest,
    pICMSUFDest, pICMSInter, pICMSInterPart,
    UsaInterPartLei, cBenef, (*OriTES,*)
    OriCST_ICMS, OriCST_IPI, OriCST_PIS,
    OriCST_COFINS, (*EFD_II_C195,*)
    GenCtbD, GenCtbC, TES_ICMS,
    TES_IPI, TES_PIS, TES_COFINS,
    TES_BC_ICMS,
    TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS
    ], True) then
(*
             SQLCampos, SQLIndex, SQLUpdate: array of String;
             ValCampos, ValIndex, ValUpdate: array of Variant;
             UserDataAlterweb: Boolean;
             CampoIncrementa: String = '';
             Incremento: Integer = 1): Boolean;
    if UMyMod.SQLInsUpd(Dmod.QrUpd?, ImgTipo.SQLType?, 'fisregufs', auto_increment?[
    'ICMSAliq', 'CST_B', 'pRedBC',
    'modBC', 'pBCUFDest', 'pFCPUFDest',
    'pICMSUFDest', 'pICMSInter', 'pICMSInterPart'], [
    'Codigo', 'Interno', 'Contribui', 'Proprio', 'UFEmit', 'UFDest'], [
    ICMSAliq, CST_B, pRedBC,
    modBC, pBCUFDest, pFCPUFDest,
    pICMSUFDest, pICMSInter, pICMSInterPart], [
    Codigo, Interno, Contribui, Proprio, UFEmit, UFDest], UserDataAlterweb?, IGNORE?
*)
    QtdInseridos := QtdInseridos + 1;
  end;
var
  Msg: String;
  PISALiq, COFINSAliq: Double;
  Conta: Integer;
var
  UFEmit, UFDest, CST_B, CSOSN, cBenef, OriCST_ICMS, OriCST_IPI, OriCST_PIS,
  OriCST_COFINS: String;
  modBC, UsaInterPartLei, (*OriTES,*) (*EFD_II_C195,*) GenCtbD, GenCtbC,
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
  TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS: Integer;
  ICMSAliq, pRedBC, pBCUFDest, pFCPUFDest, pICMSUFDest, pICMSInter,
  pICMSInterPart, pDif: Double;
  //
  //FIC: Boolean;
begin
  Geral.MB_Aviso('Este procedimento dever� ser realizado para cada CFOP da regra fiscal atual!');
  //
  Screen.Cursor := crHourGlass;
  try
    Codigo          := FmFisRegCad.QrFisRegCFOPCodigo.Value;
    Controle        := FmFisRegCad.QrFisRegCFOPControle.Value;
    Interno         := FmFisRegCad.QrFisRegCFOPInterno.Value;
    Contribui       := FmFisRegCad.QrFisRegCFOPContribui.Value;
    Proprio         := FmFisRegCad.QrFisRegCFOPProprio.Value;
    SubsTrib        := FmFisRegCad.QrFisRegCFOPSubsTrib.Value;
    OriCFOP         := FmFisRegCad.QrFisRegCFOPOriCFOP.Value;
    UFEmit          := Trim(EdUFEmit.Text);
    UFDest          := Trim(EdUFDest.Text);
    ICMSAliq        := EdICMSAliq.ValueVariant;
    CST_B           := Trim(EdCST_B.Text);
    CSOSN           := Trim(EdCSOSN.Text);
    pRedBC          := EdpRedBC.ValueVariant;
    modBC           := RGmodBC.ItemIndex;
    pBCUFDest       := EdpBCUFDest.ValueVariant;
    pFCPUFDest      := EdpFCPUFDest.ValueVariant;
    pICMSUFDest     := EdpICMSUFDest.ValueVariant;
    pICMSInter      := EdpICMSInter.ValueVariant;
    pICMSInterPart  := EdpICMSInterPart.ValueVariant;
    UsaInterPartLei := Geral.BoolToInt(CkUsaInterPartLei.Checked);
    cBenef          := Trim(EdcBenef.Text);
    pDif            := EdpDif.ValueVariant;
    //
    //OriTES          := RGOriTES.ItemIndex;
    OriCST_ICMS     := EdOriCST_ICMS.Text;
    OriCST_IPI      := EdOriCST_IPI.Text;
    OriCST_PIS      := EdOriCST_PIS.Text;
    OriCST_COFINS   := EdOriCST_COFINS.Text;
    //EFD_II_C195     := Geral.BoolToInt(CkEFD_II_C195.Checked);
    GenCtbD         := EdGenCtbD.ValueVariant;
    GenCtbC         := EdGenCtbC.ValueVariant;
    TES_ICMS        := EdTES_ICMS.ValueVariant;
    TES_IPI         := EdTES_IPI.ValueVariant;
    TES_PIS         := EdTES_PIS.ValueVariant;
    TES_COFINS      := EdTES_COFINS.ValueVariant;
    PISALiq         := EdPisAliq.ValueVariant;
    TES_BC_ICMS     := EdTES_BC_ICMS.ValueVariant;
    TES_BC_IPI      := EdTES_BC_IPI.ValueVariant;
    TES_BC_PIS      := EdTES_BC_PIS.ValueVariant;
    TES_BC_COFINS   := EdTES_BC_COFINS.ValueVariant;
    PISALiq         := EdPisAliq.ValueVariant;
    COFINSAliq      := EdCofinsAliq.ValueVariant;
    //
(*
    FIC := (OriTES = 0) and (PCTES.ActivePageIndex = 1);
    if MyObjects.FIC(FIC, RGOriTES, 'Informe o TES de NFs de entrada!') then Exit;
*)
    //
    Conta := 0;
    if UFEmit = '' then
    begin
      Geral.MB_Aviso('Informe a UF emitente!');
      Exit;
    end;
    if CST_B = '0' then
      CST_B := '00';
    if CST_B <> '' then
    begin
      try
        Geral.IMV(EdCST_B.Text);
      except
        if MyObjects.FIC(True, EdCST_B, 'O CST informado para o ICMS � inv�lido!') then
          Exit;
      end;
    end;
    if Length(UFDest) = 2 then
      InsUpdAlquotas(UFEmit, UFDest, modBC, ICMSAliq, PISALiq,
      COFINSAliq, pRedBC, CST_B, CSOSN, pBCUFDest, pFCPUFDest, pICMSUFDest,
      pICMSInter, pICMSInterPart, UsaInterPartLei, cBenef, pDif,
      (*OriTES,*) OriCST_ICMS, OriCST_IPI, OriCST_PIS, OriCST_COFINS, (*EFD_II_C195,*)
      GenCtbD, GenCtbC, TES_ICMS,
      TES_IPI, TES_PIS, TES_COFINS,
      TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS,
      Conta);
    QrUFsAtoB.First;
    while not QrUFsAtoB.Eof do
    begin
      if QrUFsAtoBAtivo.Value = 1 then
        InsUpdAlquotas(UFEmit, QrUFsAtoBNome.Value, modBC, ICMSAliq, PISALiq,
        COFINSAliq, pRedBC, CST_B, CSOSN, pBCUFDest, pFCPUFDest, pICMSUFDest,
        pICMSInter, pICMSInterPart, UsaInterPartLei, cBenef, pDif,
        (*OriTES,*) OriCST_ICMS, OriCST_IPI, OriCST_PIS, OriCST_COFINS, (*EFD_II_C195,*)
        GenCtbD, GenCtbC,
        TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
        TES_BC_ICMS, TES_BC_IPI, TES_BC_PIS, TES_BC_COFINS,
        Conta);
      QrUFsAtoB.Next;
    end;
    case Conta of
      0: Msg := 'N�o foi inclu�do/alterado nenhum registro!';
      1: Msg := 'Foi inclu�do/alterado um registro!';
      else Msg := 'Foram inclu�dos/alterados ' + IntToStr(Conta) + ' registros!';
    end;
    Geral.MB_Info(Msg);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFisRegUFs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFisRegUFs.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

procedure TFmFisRegUFs.CkUsaInterPartLeiClick(Sender: TObject);
begin
  LapICMSInterPart.Enabled := not CkUsaInterPartLei.Checked;
  EdpICMSInterPart.Enabled := not CkUsaInterPartLei.Checked;
end;

procedure TFmFisRegUFs.dmkDBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
  UF: String;
begin
  if Column.FieldName = 'Ativo' then
  begin
    UF := QrUFsAtoBNome.Value;
    if QrUFsAtoBAtivo.Value = 1 then
      Status := 0
    else
      Status := 1;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FUFsAtoB + ' SET Ativo=:P0');
    DmodG.QrUpdPID1.SQL.Add('WHERE Nome=:P1');
    DmodG.QrUpdPID1.Params[00].AsInteger := Status;
    DmodG.QrUpdPID1.Params[01].AsString  := UF;
    UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
    QrUFsAtoB.Close;
    UnDmkDAC_PF.AbreQuery(QrUFsAtoB, DModG.MyPID_DB);
    QrUFsAtoB.Locate('Nome', UF, []);
  end;
end;

procedure TFmFisRegUFs.EdTES_BC_COFINSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_BC_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,
    EdTES_BC_COFINS.ValueVariant, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_BC_COFINS.Text := Geral.SelecionaItem(F_TES_BC_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdTES_BC_ICMSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_BC_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,
    EdTES_BC_ICMS.ValueVariant, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_BC_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_BC_ICMS.Text := Geral.SelecionaItem(F_TES_BC_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdTES_BC_IPIChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_BC_IPI_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,
    EdTES_BC_IPI.ValueVariant, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_BC_IPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_BC_IPI.Text := Geral.SelecionaItem(F_TES_BC_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdTES_BC_PISChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_BC_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,
    EdTES_BC_PIS.ValueVariant, F_TES_BC_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_BC_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_BC_PIS.Text := Geral.SelecionaItem(F_TES_BC_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdTES_COFINSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSped_Txt2(spedTES_ITENS,
  EdTES_COFINS.ValueVariant, F_TES_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_COFINS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdCSOSNChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  EdTextoC.Text := UFinanceiro.CSOSN_Get(1, Geral.IMV(EdCSOSN.Text));
  {$EndIf}
end;

procedure TFmFisRegUFs.EdCSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();
  {$EndIf}
end;

procedure TFmFisRegUFs.EdCST_BChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
  {$EndIf}
end;

procedure TFmFisRegUFs.EdCST_BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
  {$EndIf}
end;

procedure TFmFisRegUFs.EdOriCST_COFINSChange(Sender: TObject);
begin
  EdOriTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdOriCST_COFINS.Text);
end;

procedure TFmFisRegUFs.EdOriCST_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdOriCST_COFINS.Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmFisRegUFs.EdOriCST_IPIChange(Sender: TObject);
begin
  EdOriTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdOriCST_IPI.Text);
end;

procedure TFmFisRegUFs.EdOriCST_IPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdOriCST_IPI.Text := UFinanceiro.ListaDeCST_IPI();
end;

procedure TFmFisRegUFs.EdOriCST_PISChange(Sender: TObject);
begin
  EdOriTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdOriCST_PIS.Text);
end;

procedure TFmFisRegUFs.EdOriCST_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdOriCST_PIS.Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmFisRegUFs.EdTES_ICMSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdTES_ICMS.ValueVariant, F_TES_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_ICMS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdTES_IPIChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_IPI_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdTES_IPI.ValueVariant, F_TES_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_IPIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_IPI.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdTES_PISChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdTES_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdTES_PIS.ValueVariant, F_TES_ITENS);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdTES_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTES_PIS.Text := Geral.SelecionaItem(F_TES_ITENS, 0,
      'SEL-LISTA-000 :: Forma de Tributa��o',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFisRegUFs.EdOriCST_ICMSChange(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  EdOriTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdOriCST_ICMS.ValueVariant,
    CO_NOME_tbspedefd_ICMS_CST, FData);
  {$EndIf}
end;

procedure TFmFisRegUFs.EdOriCST_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', CO_NOME_tbspedefd_ICMS_CST, DModG.AllID_DB,
    ''(*Extra*), EdOriCST_ICMS, (*CBICMS_CST*)nil, dmktfInteger);
end;

procedure TFmFisRegUFs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegUFs.FormCreate(Sender: TObject);
begin
  QrUFsAtoB.Database := DModG.MyPID_DB;
  FData := Now();
  FUFsAtoB := UCriar.RecriaTempTable('UFsAtoB', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FUFsAtoB);
  DmodG.QrUpdPID1.SQL.Add('SELECT Nome, 0 Ativo FROM ' + TMeuDB + '.UFs');
  DmodG.QrUpdPID1.SQL.Add('WHERE Codigo <> 0');
  DmodG.QrUpdPID1.ExecSQL;
  QrUFsAtoB.Close;
  QrUFsAtoB.SQL.Clear;
  QrUFsAtoB.SQL.Add('SELECT * FROM ' + FUFsAtoB);
  UnDmkDAC_PF.AbreQuery(QrUFsAtoB, DModG.MyPID_DB);
  //
  {$IfNDef SemNFe_0000}
  EdOriTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdOriCST_ICMS.ValueVariant,
    CO_NOME_tbspedefd_ICMS_CST, FData);
  PCTES.ActivePageIndex := 0;
  F_TES_ITENS := UnNFe_PF.ListaTES_ITENS();
  F_TES_BC_ITENS := UnNFe_PF.ListaTES_BC_ITENS();


  EdOriTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdOriCST_ICMS.ValueVariant, CO_NOME_tbspedefd_ICMS_CST, FData);
  EdOriTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdOriCST_IPI.Text);
  EdOriTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdOriCST_PIS.Text);
  EdOriTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdOriCST_COFINS.Text);

  EdTES_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdTES_ICMS.ValueVariant, F_TES_ITENS);
  EdTES_IPI_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdTES_IPI.ValueVariant, F_TES_ITENS);
  EdTES_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdTES_PIS.ValueVariant, F_TES_ITENS);
  EdTES_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_ITENS,  EdTES_COFINS.ValueVariant, F_TES_ITENS);

  EdTES_BC_ICMS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdTES_BC_ICMS.ValueVariant, F_TES_BC_ITENS);
  EdTES_BC_IPI_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdTES_BC_IPI.ValueVariant, F_TES_BC_ITENS);
  EdTES_BC_PIS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdTES_BC_PIS.ValueVariant, F_TES_BC_ITENS);
  EdTES_BC_COFINS_Txt.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedTES_BC_ITENS,  EdTES_BC_COFINS.ValueVariant, F_TES_BC_ITENS);
  {$EndIf}

end;

procedure TFmFisRegUFs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFisRegUFs.RGOriTESClick(Sender: TObject);
begin
  //CkEFD_II_C195.Checked := RGOriTES.ItemIndex = 2;
end;

procedure TFmFisRegUFs.SbAjudaClick(Sender: TObject);
begin
  DmkWeb.MostraWebBrowser('http://www.dermatek.net.br/?page=wfaqview&id=230', True, False, 0, 0);
end;

end.
