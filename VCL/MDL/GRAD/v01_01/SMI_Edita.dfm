object FmSMI_Edita: TFmSMI_Edita
  Left = 339
  Top = 185
  Caption = 'STQ-MOVIM-001 :: Edi'#231#227'o de Lan'#231'amento no Estoque'
  ClientHeight = 593
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 545
    Width = 544
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 437
    ExplicitWidth = 504
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 432
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 392
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object Panel1: TPanel
    Left = 123
    Top = 48
    Width = 504
    Height = 389
    TabOrder = 0
    object Label14: TLabel
      Left = 1
      Top = 369
      Width = 502
      Height = 16
      Align = alTop
      Alignment = taCenter
      Caption = 'Digite sempre valores positivos ou zero mesmo quando for baixa!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 454
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 89
      Width = 502
      Height = 140
      Align = alTop
      Caption = ' Dados atuais do lan'#231'amento: '
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 16
        Width = 139
        Height = 13
        Caption = 'Centro de estoque de origem:'
      end
      object Label1: TLabel
        Left = 8
        Top = 56
        Width = 154
        Height = 13
        Caption = 'Reduzido do Produto de Origem:'
      end
      object LaPecas: TLabel
        Left = 8
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object LaAreaM2: TLabel
        Left = 96
        Top = 96
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object LaAreaP2: TLabel
        Left = 184
        Top = 96
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object LaPeso: TLabel
        Left = 264
        Top = 96
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label13: TLabel
        Left = 352
        Top = 96
        Width = 114
        Height = 13
        Caption = 'Quantidade no estoque:'
        FocusControl = DBEdit15
      end
      object DBEdit4: TDBEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'StqCenCad'
        DataSource = DsSMIA
        TabOrder = 0
      end
      object DBEdit5: TDBEdit
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'GraGruX'
        DataSource = DsSMIA
        TabOrder = 2
      end
      object DBEdit6: TDBEdit
        Left = 68
        Top = 72
        Width = 424
        Height = 21
        TabStop = False
        DataField = 'NO_PRD'
        DataSource = DsSMIA
        TabOrder = 3
      end
      object DBEdit7: TDBEdit
        Left = 68
        Top = 32
        Width = 424
        Height = 21
        TabStop = False
        DataField = 'NO_STQCENCAD'
        DataSource = DsSMIA
        TabOrder = 1
      end
      object DBEdit8: TDBEdit
        Left = 8
        Top = 112
        Width = 84
        Height = 21
        TabStop = False
        DataField = 'Pecas'
        DataSource = DsSMIA
        TabOrder = 4
      end
      object DBEdit9: TDBEdit
        Left = 96
        Top = 112
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'AreaM2'
        DataSource = DsSMIA
        TabOrder = 5
      end
      object DBEdit10: TDBEdit
        Left = 180
        Top = 112
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'AreaP2'
        DataSource = DsSMIA
        TabOrder = 6
      end
      object DBEdit11: TDBEdit
        Left = 264
        Top = 112
        Width = 84
        Height = 21
        TabStop = False
        DataField = 'Peso'
        DataSource = DsSMIA
        TabOrder = 7
      end
      object DBEdit14: TDBEdit
        Left = 408
        Top = 112
        Width = 84
        Height = 21
        TabStop = False
        DataField = 'Qtde'
        DataSource = DsSMIA
        TabOrder = 9
      end
      object DBEdit15: TDBEdit
        Left = 352
        Top = 112
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'SIGLA'
        DataSource = DsSMIA
        TabOrder = 8
      end
    end
    object GBNovosDados: TGroupBox
      Left = 1
      Top = 229
      Width = 502
      Height = 140
      Align = alTop
      Caption = ' Novos dados do lan'#231'amento: '
      Enabled = False
      TabOrder = 2
      object Label4: TLabel
        Left = 8
        Top = 16
        Width = 142
        Height = 13
        Caption = 'Centro de estoque de destino:'
      end
      object Label5: TLabel
        Left = 8
        Top = 56
        Width = 83
        Height = 13
        Caption = 'Produto (destino):'
      end
      object Label8: TLabel
        Left = 8
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label9: TLabel
        Left = 96
        Top = 96
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label10: TLabel
        Left = 184
        Top = 96
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label11: TLabel
        Left = 264
        Top = 96
        Width = 42
        Height = 13
        Caption = 'Peso kg:'
      end
      object Label6: TLabel
        Left = 356
        Top = 96
        Width = 114
        Height = 13
        Caption = 'Quantidade no estoque:'
        FocusControl = DBEdit13
      end
      object EdStqCenCad: TdmkEditCB
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 68
        Top = 32
        Width = 424
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 1
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 68
        Top = 72
        Width = 424
        Height = 21
        KeyField = 'GraGruX'
        ListField = 'Nome'
        ListSource = DsGraGruX
        TabOrder = 3
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPecas: TdmkEdit
        Left = 8
        Top = 112
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPecasChange
      end
      object EdAreaM2: TdmkEditCalc
        Left = 96
        Top = 112
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdAreaM2Change
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 180
        Top = 112
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdAreaP2Change
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPeso: TdmkEdit
        Left = 264
        Top = 112
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdPesoChange
      end
      object DBEdit13: TDBEdit
        Left = 356
        Top = 112
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'SIGLAUNIDMED'
        DataSource = DsGraGruX
        TabOrder = 8
      end
      object EdQtde: TdmkEdit
        Left = 412
        Top = 112
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 502
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 89
      ExplicitTop = 5
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object Label12: TLabel
        Left = 68
        Top = 4
        Width = 188
        Height = 13
        Caption = 'Data e hora do lan'#231'amento no estoque:'
      end
      object Label2: TLabel
        Left = 8
        Top = 44
        Width = 97
        Height = 13
        Caption = 'Tipo de lan'#231'amento:'
        FocusControl = DBEdit3
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 20
        Width = 193
        Height = 21
        TabStop = False
        DataField = 'DataHora'
        DataSource = DsSMIA
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        DataSource = DsSMIA
        TabOrder = 0
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 60
        Width = 40
        Height = 21
        TabStop = False
        DataField = 'Tipo'
        DataSource = DsSMIA
        TabOrder = 2
        OnChange = DBEdit3Change
      end
      object EdFatID_Nome: TdmkEdit
        Left = 52
        Top = 60
        Width = 441
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = -131
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 496
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 448
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 424
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amento no Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 424
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amento no Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 424
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amento no Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 437
    Width = 544
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitLeft = -131
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 481
    Width = 544
    Height = 64
    Align = alBottom
    TabOrder = 4
    ExplicitLeft = -131
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 396
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
      end
    end
  end
  object QrGraGruXA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,' +
        ' '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 264
    Top = 52
    object QrGraGruXAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXANivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGruXANivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGruXANivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruXANome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGruXAPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGruXAGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGruXANOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXACODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXACST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGruXACST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGruXAUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGruXANCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGruXAPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGruXASIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGruXACODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXANOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXAHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXAGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXAFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
  end
  object DsGraGruXA: TDataSource
    DataSet = QrGraGruXA
    Left = 292
    Top = 52
  end
  object QrStqCenCadA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 320
    Top = 52
    object QrStqCenCadACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadACodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadANome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCadA: TDataSource
    DataSet = QrStqCenCadA
    Left = 348
    Top = 52
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,' +
        ' '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 264
    Top = 108
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGruXNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruXNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGruXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXCODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXCST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrGraGruXCST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrGraGruXFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 292
    Top = 108
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 320
    Top = 108
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 348
    Top = 108
  end
  object QrSMIA: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSMIAAfterOpen
    BeforeClose = QrSMIABeforeClose
    SQL.Strings = (
      'SELECT scc.Nome NO_STQCENCAD,'
      'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN stqcencad scc ON scc.Codigo=smia.StqCenCad'
      'WHERE IDCtrl=:P0')
    Left = 264
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSMIANO_STQCENCAD: TWideStringField
      FieldName = 'NO_STQCENCAD'
      Size = 50
    end
    object QrSMIANivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrSMIANO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 50
    end
    object QrSMIAPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrSMIAUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSMIANO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrSMIASIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSMIANO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSMIANO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSMIAGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSMIAGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSMIAGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSMIAGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSMIADataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSMIAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrSMIATipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSMIAOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrSMIAOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrSMIAOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrSMIAOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrSMIAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSMIAStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrSMIAGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMIAQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMIAPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object QrSMIAPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSMIAAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSMIAAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSMIAFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrSMIAQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrSMIARetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrSMIAParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrSMIAParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrSMIADebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrSMIAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSMIAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSMIASMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrSMIACustoAll: TFloatField
      FieldName = 'CustoAll'
    end
    object QrSMIAValorAll: TFloatField
      FieldName = 'ValorAll'
    end
    object QrSMIAGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrSMIABaixa: TSmallintField
      FieldName = 'Baixa'
    end
  end
  object DsSMIA: TDataSource
    DataSet = QrSMIA
    Left = 292
    Top = 80
  end
end
