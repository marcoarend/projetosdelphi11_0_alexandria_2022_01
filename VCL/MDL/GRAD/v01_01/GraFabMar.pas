
unit GraFabMar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnProjGroup_Consts;

type
  TFmGraFabMar = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    LaNomeCad: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdGraFabMCd: TdmkEditCB;
    CBGraFabMCd: TdmkDBLookupComboBox;
    QrGraFabMCd: TMySQLQuery;
    DsGraFabMCd: TDataSource;
    SbGraFabMCd: TSpeedButton;
    QrGraFabMCdCodigo: TIntegerField;
    QrGraFabMCdNome: TWideStringField;
    EdNome: TdmkEdit;
    LaNomeStr: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGraFabMCdClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraFabMar(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmGraFabMar: TFmGraFabMar;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, GraFabCad,
  DmkDAC_PF, CfgCadLista, MyListas;

{$R *.DFM}

procedure TFmGraFabMar.BtOKClick(Sender: TObject);
var
  Codigo, Controle, GraFabMCd: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  GraFabMCd := EdGraFabMCd.ValueVariant;
  if MyObjects.FIC((GraFabMCd = 0) and (Nome = EmptyStr), EdGraFabMCd, 'Informe uma Marca!') then
    Exit;
  Codigo    := Geral.IMV(DBEdCodigo.Text);
  //
  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BuscaEmLivreY_Def('grafabmar', 'Controle', ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'grafabmar', False, [
  'Codigo', 'GraFabMCd', 'Nome'], [
  'Controle'], [
  Codigo, GraFabMCd, Nome], [
  Controle], True) then
  begin
    ReopenGraFabMar(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdGraFabMCd.ValueVariant := 0;
      CBGraFabMCd.KeyValue     := 0;
      EdNome.ValueVariant      := EmptyStr;
      if CO_SIGLA_APP = 'TREN' then
        EdNOme.SetFocus
      else
        EdGraFabMCd.SetFocus;
    end else Close;
  end;
end;

procedure TFmGraFabMar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraFabMar.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmGraFabMar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraFabMCd, Dmod.MyDB);
  //
end;

procedure TFmGraFabMar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraFabMar.FormShow(Sender: TObject);
var
  EhTRen: Boolean;
begin
  EhTRen := CO_SIGLA_APP = 'TREN';
  //
  LaNomeStr.Enabled := EhTRen;
  EdNome.Enabled := EhTRen;
  LaNomeCad.Enabled := not EhTRen;
  EdGraFabMCd.Enabled := not EhTRen;
  CBGraFabMCd.Enabled := not EhTRen;
  SbGraFabMCd.Enabled := not EhTRen;
end;

procedure TFmGraFabMar.ReopenGraFabMar(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmGraFabMar.SbGraFabMCdClick(Sender: TObject);
  procedure MostraFormGraFabMCd();
  begin
    UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'GraFabMCd', 255, ncGerlSeq1,
    'Marcas',
    [], False, Null(*Maximo*), [], [], False);
  end;
begin
  VAR_CADASTRO := 0;
  MostraFormGraFabMCd();
  UMyMod.SetaCodigoPesquisado(EdGraFabMCd, CBGraFabMCd, QrGraFabMCd, VAR_CADASTRO);
end;

end.
