unit GraPlaCta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Mask, dmkImage, UnDmkEnums;

type
  TFmGraPlaCta = class(TForm)
    Panel1: TPanel;
    PnNivel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DsGraPlaCta3: TDataSource;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    PnNivel2: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DsGraPlaCta2: TDataSource;
    Panel4: TPanel;
    EdGenero: TdmkEdit;
    Label8: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraPlaCta: TFmGraPlaCta;

implementation

uses UnMyObjects, PrdGrupTip, Module;

{$R *.DFM}

procedure TFmGraPlaCta.BtOKClick(Sender: TObject);
begin
  if PnNivel3.Visible then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM graplacta');
    Dmod.QrUpd.SQL.Add('WHERE NivelX=3');
    Dmod.QrUpd.SQL.Add('AND PrdGrupTip=:P0');
    Dmod.QrUpd.SQL.Add('AND Nivel3=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraPlaCta3Nivel3.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO graplacta');
    Dmod.QrUpd.SQL.Add('SET NivelX=3, Nivel2=0,');
    Dmod.QrUpd.SQL.Add('PrdGrupTip=:P0, ');
    Dmod.QrUpd.SQL.Add('Nivel3=:P1,');
    Dmod.QrUpd.SQL.Add('Genero=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraPlaCta3Nivel3.Value;
    Dmod.QrUpd.Params[02].AsFloat   := EdGenero.ValueVariant;
    Dmod.QrUpd.ExecSQL;
    //
    FmPrdGrupTip.ReopenGraPlaCta3(FmPrdGrupTip.QrGraPlaCta3CodUsu.Value);
    //
  end else
  if PnNivel2.Visible then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM graplacta');
    Dmod.QrUpd.SQL.Add('WHERE NivelX=2');
    Dmod.QrUpd.SQL.Add('AND PrdGrupTip=:P0');
    Dmod.QrUpd.SQL.Add('AND Nivel3=:P1');
    Dmod.QrUpd.SQL.Add('AND Nivel2=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraPlaCta2Nivel3.Value;
    Dmod.QrUpd.Params[02].AsInteger := FmPrdGrupTip.QrGraPlaCta2Nivel2.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO graplacta');
    Dmod.QrUpd.SQL.Add('SET NivelX=2,');
    Dmod.QrUpd.SQL.Add('PrdGrupTip=:P0, ');
    Dmod.QrUpd.SQL.Add('Nivel3=:P1,');
    Dmod.QrUpd.SQL.Add('Nivel2=:P2,');
    Dmod.QrUpd.SQL.Add('Genero=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraPlaCta2Nivel3.Value;
    Dmod.QrUpd.Params[02].AsInteger := FmPrdGrupTip.QrGraPlaCta2Nivel2.Value;
    Dmod.QrUpd.Params[03].AsInteger := EdGenero.ValueVariant;
    Dmod.QrUpd.ExecSQL;
    //
    FmPrdGrupTip.ReopenGraPlaCta2(FmPrdGrupTip.QrGraPlaCta2CodUsu.Value);
    //
  end;
  //
  Close;
end;

procedure TFmGraPlaCta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraPlaCta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraPlaCta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmGraPlaCta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
