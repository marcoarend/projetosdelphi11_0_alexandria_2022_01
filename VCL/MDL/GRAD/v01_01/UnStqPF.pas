unit UnStqPF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ComCtrls, StdCtrls, ExtCtrls, Db, DbCtrls, Buttons, UnMsgInt,
  UnInternalConsts, UnInternalConsts2, dmkGeral, AppListas, mySQLDBTables,
  dmkEdit, UnDmkEnums, UnAppEnums, dmkLabel, Mask, dmkRadioGroup, dmkEditCB,
  dmkDBLookupComboBox;

type
  TUnStqPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // Cabe�alho
    procedure AtivaItensNoEstoque_1(FatID, IDCtrl, Status: Integer);
    procedure AtivaItensNoEstoque_2(FatID1, FatID2, IDCtrl, Status: Integer);
    function  DefineCarteira(Entidade: Integer): Integer;
    procedure QrForneCalcFields(Qry: TmySQLQuery);
    procedure RateioCustoDoFrete();
    procedure ReopenForne(Qry: TmySQLQuery; Fornece: Integer);
    procedure ReopenTabePrcCab(Qry: TmySQLQuery; Aplicacao: Integer);
    procedure ReopenPagtos(Qry: TmySQLQuery; Tabela, Codigos: String; FatID: Integer);
    procedure ReopenStqMovIts(Qry: TmySQLQuery; FatID, Codigo, IDCtrl: Integer);
    // Item
    procedure AbrePesquisa(QrGraGruX: TmySQLQuery; EdGraGruX: TdmkEdit;
              PnGGX, PnData: TPanel);
    procedure CalculaValorAll(EdCustoBuy, EdCustoFrt, EdValorAll: TdmkEdit);
    procedure DefineCustoRem(QrPreco: TmySQLQuery; EdQtde, EdCustoBuy: TdmkEdit);
    procedure DefineCustoSel(QrEstoque: TmySQLQuery; EdQtde, EdCustoBuy: TdmkEdit);
    procedure DefinePreco(QrPreco: TmySQLQuery; GragruX, TabePrcCab: Integer);
    procedure MostraGraGruPesq1(Nivel1: Integer; EdGraGruX: TdmkEdit);
    procedure ReopenEstoque(Qry: TmySQLQuery; EdGraGruX: TdmkEdit; EdStqCenLoc:
              TdmkEditCB);
    procedure ReopenGraGruX(Qry: TmySQLQuery);
    procedure ReopenStqCenCad(Qry: TmySQLQuery; EntiSitio: Integer; EdStqCenCad:
              TdmkEditCB; CBStqCenCad: TdmkDBLookupComboBox; Comparacao: TCompara);
    procedure ReopenStqCenLoc(StqCenCad: Integer; Qry: TmySQLQuery; EdStqCenLoc:
              TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox);
    procedure VerificaPainelGGX(QrGraGruX: TmySQLQuery; EdGraGruX: TdmkEdit;
              PnGGX, PnData: TPanel);


  end;

var
  StqPF: TUnStqPF;


implementation

uses
  Module, DmkDAC_PF, MyDBCheck, UnDmkProcFunc, ModProd, ModuleGeral, GraGruPesq1;

{ TUnStqPF }

procedure TUnStqPF.AbrePesquisa(QrGraGruX: TmySQLQuery; EdGraGruX: TdmkEdit;
  PnGGX, PnData: TPanel);
var
  GraGruX, Nivel1: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    Nivel1 := DmProd.ObtemGraGru1deGraGruX(GraGruX)
  else
    Nivel1 := 0;
  //
  MostraGraGruPesq1(Nivel1, EdGraGruX);
  //StqPF.ReopenGraGruX(QrGraGruX);
  VerificaPainelGGX(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;

procedure TUnStqPF.AtivaItensNoEstoque_1(FatID, IDCtrl, Status: Integer);
var
  Ativo: Integer;
begin
  if Status > 0 then // Existe status maior que 1?
    Ativo := 1
  else
    Ativo := 0;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE stqmovitsa ',
  'SET Ativo=' + Geral.FF0(Status),
  'WHERE Tipo="' + Geral.FF0(FatID) + '"',
  'AND OriCodi="' + Geral.FF0(IDCtrl) + '"',
  '']);
end;

procedure TUnStqPF.AtivaItensNoEstoque_2(FatID1, FatID2, IDCtrl,
  Status: Integer);
var
  Ativo: Integer;
begin
  if Status > 0 then // Existe status maior que 1?
    Ativo := 1
  else
    Ativo := 0;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE stqmovitsa ',
  'SET Ativo=' + Geral.FF0(Status),
  'WHERE Tipo IN (' + Geral.FF0(FatID1) + ',' + Geral.FF0(FatID2) + ')',
  'AND OriCodi="' + Geral.FF0(IDCtrl) + '"',
  '']);
end;

procedure TUnStqPF.CalculaValorAll(EdCustoBuy, EdCustoFrt,
  EdValorAll: TdmkEdit);
var
  ValorAll, CustoBuy, CustoFrt: Double;
begin
  CustoBuy := EdCustoBuy.ValueVariant;
  CustoFrt := EdCustoFrt.ValueVariant;
  //
  ValorAll := CustoBuy + CustoFrt;
  EdValorAll.ValueVariant := ValorAll;
end;

function TUnStqPF.DefineCarteira(Entidade: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM carteiras',
  'WHERE FornecePadrao=' + Geral.FF0(Entidade),
  'AND Ativo=1',
  'ORDER BY Codigo DESC',
  '']);
  if Dmod.QrAux.RecordCount > 0 then
    Result := Dmod.QrAux.FieldByName('Codigo').AsInteger
  else
    Result := 0;
end;

procedure TUnStqPF.DefineCustoRem(QrPreco: TmySQLQuery; EdQtde, EdCustoBuy:
  TdmkEdit);
var
  Preco, Qtde, CustoBuy: Double;
begin
  if QrPreco.State <> dsInactive then
    Preco := QrPreco.FieldByName('Preco').AsFloat // QrPrecoPreco.Value
  else
    Preco := 0.00;
  Qtde := EdQtde.ValueVariant;
  CustoBuy := Qtde * Preco;
  EdCustoBuy.ValueVariant := CustoBuy;
end;

procedure TUnStqPF.DefineCustoSel(QrEstoque: TmySQLQuery; EdQtde, EdCustoBuy:
  TdmkEdit);
var
  Preco, Qtde, CustoBuy: Double;
begin
  if (QrEstoque.State <> dsInactive)  then
    Preco := QrEstoque.FieldByName('CustoAll').AsFloat
  else
    Preco := 0.00;
  Qtde := EdQtde.ValueVariant;
  CustoBuy := Qtde * Preco;
  EdCustoBuy.ValueVariant := CustoBuy;
end;

procedure TUnStqPF.DefinePreco(QrPreco: TmySQLQuery; GragruX,
  TabePrcCab: Integer);
begin
 if GraGruX > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPreco, Dmod.MyDB, [
    'SELECT IF(tpi.Preco > 0, tpi.Preco, tpc.Preco) Preco ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN tabeprcgrg  tpc ON tpc.Nivel1=ggx.GraGru1 ',
    '          AND tpc.Codigo=' + Geral.FF0(TabePrcCab),
    'LEFT JOIN tabeprcgri  tpi ON tpi.GraGruX=ggx.Controle ',
    '          AND tpi.TabePrcCab=' + Geral.FF0(TabePrcCab),
    'WHERE ggx.Controle=' + Geral.FF0(GragruX),
    '']);
  end else
    QrPreco.Close;
end;

procedure TUnStqPF.MostraGraGruPesq1(Nivel1: Integer; EdGraGruX: TdmkEdit);
begin
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FmGraGruPesq1.QrGraGru1, Dmod.MyDB, [
      'SELECT gg1.CodUsu, gg1.Nivel1, gg1.UsaSubsTrib, ',
      'gg1.GraTamCad, gg1.Nome, gg1.PrdGrupTip, pgt.Fracio ',
      'FROM gragru1 gg1',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
      'ORDER BY gg1.Nome',
      '']);
    //
    FmGraGruPesq1.VUGraGru1.ValueVariant := Nivel1;
    //
    FmGraGruPesq1.ShowModal;
    //
    EdGraGruX.Text := FmGraGruPesq1.FGraGruX;
    //FFracio
    //
    FmGraGruPesq1.Destroy;
  end;
end;

procedure TUnStqPF.QrForneCalcFields(Qry: TmySQLQuery);
begin
  Qry.FieldByName('TE1_TXT').AsString :=
    Geral.FormataTelefone_TT_Curto(Qry.FieldByName('Te1').AsString);
  Qry.FieldByName('FAX_TXT').AsString :=
    Geral.FormataTelefone_TT_Curto(Qry.FieldByName('Fax').AsString);
  Qry.FieldByName('CNPJ_TXT').AsString :=
    Geral.FormataCNPJ_TT(Qry.FieldByName('CNPJ_CPF').AsString);
  Qry.FieldByName('IE_TXT').AsString :=
    Geral.Formata_IE(Qry.FieldByName('IE_RG').AsString, Qry.FieldByName('UF').AsString, '??', Qry.FieldByName('Tipo').AsInteger);
  Qry.FieldByName('NUMERO_TXT').AsString :=
    Geral.FormataNumeroDeRua(Qry.FieldByName('Rua').AsString, Trunc(Qry.FieldByName('Numero').AsFloat), False);
  //
  Qry.FieldByName('E_ALL').AsString := UpperCase(Qry.FieldByName('NOMELOGRAD').AsString);
  if Trim(Qry.FieldByName('E_ALL').AsString) <> '' then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ' ';
  Qry.FieldByName('E_ALL').AsString := Qry.FieldByName('E_ALL').AsString + Uppercase(Qry.FieldByName('Rua').AsString);
  if Trim(Qry.FieldByName('Rua').AsString) <> '' then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ', ' + Qry.FieldByName('NUMERO_TXT').AsString;
  if Trim(Qry.FieldByName('Compl').AsString) <>  '' then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ' ' + Uppercase(Qry.FieldByName('Compl').AsString);
  if Trim(Qry.FieldByName('Bairro').AsString) <>  '' then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ' - ' + Uppercase(Qry.FieldByName('Bairro').AsString);
  if Qry.FieldByName('CEP').AsFloat > 0 then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ' CEP ' +Geral.FormataCEP_NT(Qry.FieldByName('CEP').AsFloat);
  if Trim(Qry.FieldByName('Cidade').AsString) <>  '' then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ' - ' + Uppercase(Qry.FieldByName('Cidade').AsString);
  if Trim(Qry.FieldByName('NOMEUF').AsString) <>  '' then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ', ' + Qry.FieldByName('NOMEUF').AsString;
  if Trim(Qry.FieldByName('Pais').AsString) <>  '' then Qry.FieldByName('E_ALL').AsString :=
    Qry.FieldByName('E_ALL').AsString + ' - ' + Qry.FieldByName('Pais').AsString;
end;

procedure TUnStqPF.RateioCustoDoFrete();
begin
(*
var
  Codigo, IDCtrl, GraGruX, Empresa: Integer;
  Fator, SumQtde, ValTotFrete, CustoFrt, CustoAll: Double;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  Codigo  := QrStqCnsgVeCabCodigo.Value;
  Empresa := QrStqCnsgVeCabEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumFrt, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito',
  'FROM ' + FTabLctA + ' la',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgVeCabCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  '']);
  //
  ValTotFrete := QrSumFrtDebito.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(CustoBuy) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Tipo=' + Geral.FF0(FThisFatID_Ven),
  'AND OriCodi=' + Geral.FF0(QrStqCnsgVeCabCodigo.Value),
  '']);
  //
  SumQtde := QrSumItsQtde.Value;
  //
  if SumQtde > 0 then
    Fator := ValTotFrete / SumQtde
  else
    Fator := 0;
  //
  ReopenStqMovIts(0);
  QrStqMovIts.DisableControls;
  try
    QrStqMovIts.First;
    while not QrStqMovIts.Eof do
    begin
      CustoFrt := QrStqMovItsCustoBuy.Value * Fator;
      CustoAll := QrStqMovItsCustoBuy.Value + CustoFrt;
      //
      GraGruX  := QrStqMovItsGraGruX.Value;
      IDCtrl   := QrStqMovItsIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
      'CustoFrt', 'CustoAll'], [
      'IDCtrl'], [
      CustoFrt, CustoAll], [
      IDCtrl], False);
      //
      DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
      //
      QrStqMovIts.Next;
    end;
  finally
    QrStqMovIts.EnableControls;
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqcnsgvecab', False, [
  'ValTotFrete'], [
  'Codigo'], [
  ValTotFrete], [
  Codigo], False);
  //
  LocCod(Codigo, Codigo);
end;
*)
end;

procedure TUnStqPF.ReopenEstoque(Qry: TmySQLQuery; EdGraGruX: TdmkEdit; EdStqCenLoc:
  TdmkEditCB);
var
  GraGruX, StqCenLoc: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  StqCenLoc := EdStqCenLoc.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT SUM(Qtde * Baixa) Qtde,',
  'SUM(CustoAll * Baixa) CustoAll,',
  'IF(SUM(Qtde * Baixa) = 0.00, 0.00, ',
  '  SUM(CustoAll * Baixa) / ',
  '  SUM(Qtde * Baixa)) CusMed',
  'FROM stqmovitsa',
  'WHERE GraGruX=' + Geral.FF0(GraGruX),
  'AND StqCenLoc=' + Geral.FF0(StqCenLoc),
  'AND Ativo=1',
  'AND ValiStq <> 101',
  '']);
  //Geral.MB_Teste(QrEstoque.SQL.Text);
end;

procedure TUnStqPF.ReopenForne(Qry: TmySQLQuery; Fornece: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT en.Codigo, Tipo, CodUsu, IE, ',
  'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT,',
  'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF,',
  'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO2_ENT,',
  'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG,',
  'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA,',
  'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0.000 NUMERO,',
  'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL,',
  'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO,',
  'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CIDADE,',
  'CASE WHEN en.Tipo=0 THEN en.EUF         ELSE en.PUF      END + 0.000 UF,',
  'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD,',
  'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF,',
  'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais,',
  'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0.000 Lograd,',
  'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0.000 CEP,',
  'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF,',
  'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1,',
  'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX,',
  'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL',
  'FROM entidades en',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd',
  'WHERE en.Codigo=' + Geral.FF0(Fornece),
  '']);
end;

procedure TUnStqPF.ReopenGraGruX(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ggx.Controle GraGruX,  ',
  'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  ',
  'gcc.Nome NO_COR,  gti.Nome NO_TAM, pgt.Fracio, ',
  'med.Nome No_SIGLA, med.Sigla, med.Grandeza ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
  '']);
end;

procedure TUnStqPF.ReopenPagtos(Qry: TmySQLQuery; Tabela, Codigos: String; FatID: Integer);
{$IfNDef NO_FINANCEIRO}
var
  ATT_SIT_LCT: String;
{$EndIf}
begin
{$IfNDef NO_FINANCEIRO}
  ATT_SIT_LCT := dmkPF.ArrayToTexto('la.Sit', 'NO_SIT', pvPos, True,
    sSitLct);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Debito, la.Credito, ',
  ATT_SIT_LCT,
  'IF(Compensado <2, "", DATE_FORMAT(Compensado, "%d/%m/%y")) Compensado_TXT, ',
  'la.Banco, la.Agencia, la.FatID, la.ContaCorrente, ',
  'la.Documento, la.Descricao, la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI,',
  'ca.Tipo CARTEIRATIPO',
  'FROM ' + Tabela + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
  'WHERE FatNum IN (' + Codigos + ')',
  'AND FatID=' + Geral.FF0(FatID),
  'AND ID_Pgto=0 ', // n�o incluir quita��es!
  'ORDER BY la.FatParcela, la.Vencimento',
  '']);
{$EndIf}
end;

procedure TUnStqPF.ReopenStqCenCad(Qry: TmySQLQuery; EntiSitio: Integer;
  EdStqCenCad: TdmkEditCB; CBStqCenCad: TdmkDBLookupComboBox; Comparacao: TCompara);
var
  SQL_EntiSitio, sSinal: String;
  StqCenCad: Integer;
begin
  if EntiSitio = 0 then
    SQL_EntiSitio := ''
  else
    SQL_EntiSitio := 'WHERE EntiSitio' + DmkEnums.ComparaToString(Comparacao) +
      Geral.FF0(EntiSitio);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome',
  'FROM stqcencad',
  SQL_EntiSitio,
  'ORDER BY Nome',
  '']);
  //
  if Qry.RecordCount = 1 then
  begin
    StqCenCad := Qry.FieldByName('Codigo').AsInteger;
    EdStqCenCad.ValueVariant := StqCenCad;
    CBStqCenCad.KeyValue     := StqCenCad;
  end;
end;

procedure TUnStqPF.ReopenStqCenLoc(StqCenCad: Integer; Qry: TmySQLQuery;
  EdStqCenLoc: TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox);
var
  StqCenloc: Integer;
begin
  UnDmkDac_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM stqcenloc ',
  'WHERE Codigo=' + Geral.FF0(StqCenCad),
  'ORDER BY Nome ',
  '']);
  if Qry.RecordCount = 1 then
  begin
    StqCenloc := Qry.FieldByName('Controle').Value;
    EdStqCenLoc.ValueVariant := StqCenloc;
    CBStqCenLoc.KeyValue     := StqCenloc;
  end else
  begin
    EdStqCenLoc.ValueVariant := 0;
    CBStqCenLoc.KeyValue     := 0;
  end;
end;

procedure TUnStqPF.ReopenStqMovIts(Qry: TmySQLQuery; FatID, Codigo, IDCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ValorAll/Qtde ValorUni, ',
  'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,  ',
  'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*  ',
  'FROM stqmovitsa smia ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'WHERE Tipo=' + Geral.FF0(FatID),
  'AND OriCodi=' + Geral.FF0(Codigo),
  'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora ',
  '']);
  //Geral.MB_Teste(Qry.SQL.Text);
  //
  Qry.Locate('IDCtrl', IDCtrl, []);
end;

procedure TUnStqPF.ReopenTabePrcCab(Qry: TmySQLQuery; Aplicacao: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM tabeprccab',
  'WHERE Codigo=0',
  'OR',
  '(',
  '  ' + Geral.FF0(1) + ' & Aplicacao',
  '  AND "' + Geral.FDT(DModG.ObtemAgora(), Aplicacao) + '" BETWEEN DataI and DataF',
  ')',
  'ORDER BY Nome',
  '']);
end;

procedure TUnStqPF.VerificaPainelGGX(QrGraGruX: TmySQLQuery; EdGraGruX: TdmkEdit;
  PnGGX, PnData: TPanel);
var
  GraGruX: Integer;
begin
  if QrGraGruX.State <> dsInactive then
  begin
    GraGruX       := Geral.IMV(EdGraGruX.Text);
    PnGGX.Visible := QrGraGruX.Locate('GraGruX', GraGruX, []);
    PnData.Visible := PnGGX.Visible;
  end;
end;

end.
