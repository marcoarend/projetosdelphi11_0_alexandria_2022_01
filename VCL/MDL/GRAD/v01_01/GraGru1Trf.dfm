object FmGraGru1Trf: TFmGraGru1Trf
  Left = 339
  Top = 185
  Caption = 
    'PRD-GRUPO-036 :: Transfer'#234'ncia de Itens de Grupos de Produtos (N' +
    'ivel 1)'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 667
        Height = 32
        Caption = 'Transfer'#234'ncia de Itens de Grupos de Produtos (Nivel 1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 667
        Height = 32
        Caption = 'Transfer'#234'ncia de Itens de Grupos de Produtos (Nivel 1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 667
        Height = 32
        Caption = 'Transfer'#234'ncia de Itens de Grupos de Produtos (Nivel 1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 46
        Width = 812
        Height = 421
        Align = alClient
        Caption = ' Itens do Nivel 1 de origem: '
        TabOrder = 0
        ExplicitLeft = 4
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 291
          Height = 404
          Align = alLeft
          DataSource = DsGraGruC
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsuGTC'
              Title.Caption = 'ID Usuario'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeGTC'
              Title.Caption = 'Nome da Cor'
              Width = 192
              Visible = True
            end>
        end
        object Panel37: TPanel
          Left = 293
          Top = 15
          Width = 517
          Height = 404
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 252
          ExplicitHeight = 541
          object DBGrid2: TDBGrid
            Left = 0
            Top = 65
            Width = 517
            Height = 339
            Align = alClient
            DataSource = DsGraGruX
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'Reduzido'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Produto / tamanho / cor'
                Width = 421
                Visible = True
              end>
          end
          object Panel38: TPanel
            Left = 0
            Top = 0
            Width = 517
            Height = 65
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            Visible = False
            ExplicitWidth = 252
            object StaticText6: TStaticText
              Left = 0
              Top = 48
              Width = 63
              Height = 17
              Align = alBottom
              BorderStyle = sbsSunken
              Caption = 'Reduzidos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
            end
            object BtReduzidoL: TBitBtn
              Tag = 14
              Left = 128
              Top = 4
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Reduzido'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object BtCodificacao: TBitBtn
              Tag = 41
              Left = 4
              Top = 4
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = 'Codifica'#231#227'o'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
            end
          end
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 812
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 2
        ExplicitTop = 15
        ExplicitWidth = 808
        object LaNivel2: TLabel
          Left = 12
          Top = 0
          Width = 85
          Height = 13
          Caption = 'Nivel 1 de origem:'
        end
        object Label1: TLabel
          Left = 404
          Top = 0
          Width = 88
          Height = 13
          Caption = 'Nivel 1 de destino:'
        end
        object EdNivel1Src: TdmkEditCB
          Left = 12
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBNivel1Src
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel1Src: TdmkDBLookupComboBox
          Left = 68
          Top = 16
          Width = 332
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru1Src
          TabOrder = 1
          dmkEditCB = EdNivel1Src
          QryCampo = 'Nivel2'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdNivel1Dst: TdmkEditCB
          Left = 404
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBNivel1Dst
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel1Dst: TdmkDBLookupComboBox
          Left = 460
          Top = 16
          Width = 332
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGraGru1Dst
          TabOrder = 3
          dmkEditCB = EdNivel1Dst
          QryCampo = 'Nivel2'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGru1Src: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru1SrcBeforeClose
    AfterScroll = QrGraGru1SrcAfterScroll
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome'
      'FROM GraGru1'
      'ORDER BY Nome')
    Left = 24
    Top = 156
    object QrGraGru1SrcNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1SrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1SrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1Src: TDataSource
    DataSet = QrGraGru1Src
    Left = 24
    Top = 204
  end
  object QrGraGru1Dst: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome'
      'FROM GraGru1'
      'ORDER BY Nome')
    Left = 104
    Top = 156
    object QrGraGru1DstNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1DstCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1DstNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1Dst: TDataSource
    DataSet = QrGraGru1Dst
    Left = 104
    Top = 204
  end
  object QrGraGruC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGruCBeforeClose
    AfterScroll = QrGraGruCAfterScroll
    SQL.Strings = (
      'SELECT ggc.Nivel1, ggc.Controle, ggc.GraCorCad,'
      'gtc.CodUsu CodUsuGTC, gtc.Nome NomeGTC'
      'FROM gragruc ggc'
      'LEFT JOIN gracorcad gtc ON gtc.Codigo=ggc.GraCorCad'
      'WHERE ggc.Nivel1=:P0')
    Left = 176
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruCNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraGruCGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
    end
    object QrGraGruCCodUsuGTC: TIntegerField
      FieldName = 'CodUsuGTC'
      Required = True
    end
    object QrGraGruCNomeGTC: TWideStringField
      FieldName = 'NomeGTC'
      Size = 50
    end
  end
  object DsGraGruC: TDataSource
    DataSet = QrGraGruC
    Left = 176
    Top = 204
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR  '
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'WHERE ggx.GraGruC=1  '
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 240
    Top = 156
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 240
    Top = 204
  end
end
