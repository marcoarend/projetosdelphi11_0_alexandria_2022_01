unit StqCnsgBkCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy,UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, dmkDBGrid, frxClass, frxDBSet,
  UnInternalConsts3, Variants, UnDmkProcFunc, dmkImage, UnDmkEnums, UnGrl_Geral,
  dmkCheckBox, Vcl.ComCtrls, dmkLabelRotate, UnStqPF;

type
  TFmStqCnsgBkCab = class(TForm)
    PnDados: TPanel;
    DsStqCnsgBkCab: TDataSource;
    QrStqCnsgBkCab: TMySQLQuery;
    PnEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrStqMovIts: TmySQLQuery;
    DsStqMovIts: TDataSource;
    PMCab: TPopupMenu;
    PMItens: TPopupMenu;
    DBGStqMovIts: TDBGrid;
    N1: TMenuItem;
    QrStqCnsgBkCabCodigo: TIntegerField;
    QrStqCnsgBkCabCodUsu: TIntegerField;
    QrStqCnsgBkCabNome: TWideStringField;
    QrStqCnsgBkCabEmpresa: TIntegerField;
    QrStqCnsgBkCabAbertura: TDateTimeField;
    QrStqCnsgBkCabEncerrou: TDateTimeField;
    QrStqCnsgBkCabStatus: TSmallintField;
    QrStqCnsgBkCabLk: TIntegerField;
    QrStqCnsgBkCabDataCad: TDateField;
    QrStqCnsgBkCabDataAlt: TDateField;
    QrStqCnsgBkCabUserCad: TIntegerField;
    QrStqCnsgBkCabUserAlt: TIntegerField;
    QrStqCnsgBkCabAlterWeb: TSmallintField;
    QrStqCnsgBkCabAtivo: TSmallintField;
    QrStqMovItsNivel1: TIntegerField;
    QrStqMovItsNO_PRD: TWideStringField;
    QrStqMovItsPrdGrupTip: TIntegerField;
    QrStqMovItsUnidMed: TIntegerField;
    QrStqMovItsNO_PGT: TWideStringField;
    QrStqMovItsSIGLA: TWideStringField;
    QrStqMovItsNO_TAM: TWideStringField;
    QrStqMovItsNO_COR: TWideStringField;
    QrStqMovItsGraCorCad: TIntegerField;
    QrStqMovItsGraGruC: TIntegerField;
    QrStqMovItsGraGru1: TIntegerField;
    QrStqMovItsGraTamI: TIntegerField;
    QrStqMovItsDataHora: TDateTimeField;
    QrStqMovItsIDCtrl: TIntegerField;
    QrStqMovItsTipo: TIntegerField;
    QrStqMovItsOriCodi: TIntegerField;
    QrStqMovItsOriCtrl: TIntegerField;
    QrStqMovItsOriCnta: TIntegerField;
    QrStqMovItsEmpresa: TIntegerField;
    QrStqMovItsStqCenCad: TIntegerField;
    QrStqMovItsGraGruX: TIntegerField;
    QrStqMovItsQtde: TFloatField;
    QrStqMovItsAlterWeb: TSmallintField;
    QrStqMovItsAtivo: TSmallintField;
    QrStqMovItsOriPart: TIntegerField;
    QrStqMovItsPecas: TFloatField;
    QrStqMovItsPeso: TFloatField;
    QrStqMovItsAreaM2: TFloatField;
    QrStqMovItsAreaP2: TFloatField;
    QrStqMovItsFatorClas: TFloatField;
    QrStqMovItsQuemUsou: TIntegerField;
    QrStqMovItsRetorno: TSmallintField;
    QrStqMovItsParTipo: TIntegerField;
    QrStqMovItsParCodi: TIntegerField;
    QrStqMovItsDebCtrl: TIntegerField;
    QrStqMovItsSMIMultIns: TIntegerField;
    Incluiitem1: TMenuItem;
    Excluiitem1: TMenuItem;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    QrStqCnsgBkCabNOMEFILIAL: TWideStringField;
    QrStqCnsgBkCabNO_FORNECE: TWideStringField;
    QrStqCnsgBkCabBalQtdItem: TFloatField;
    QrStqCnsgBkCabFatSemEstq: TSmallintField;
    QrStqCnsgBkCabFornece: TIntegerField;
    QrStqCnsgBkCabENCERROU_TXT: TWideStringField;
    GroupBox1: TGroupBox;
    QrFornece: TMySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    GroupBox5: TGroupBox;
    dmkLabel4: TdmkLabel;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    GroupBox2: TGroupBox;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    Label13: TLabel;
    EdNome: TdmkEdit;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    LaFornece: TdmkLabel;
    SbFornece: TSpeedButton;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    VUEmpresa: TdmkValUsu;
    QrStqMovItsCustoAll: TFloatField;
    QrStqMovItsValorAll: TFloatField;
    N2: TMenuItem;
    frxSTQ_MOVIM_006: TfrxReport;
    frxDsStqMovIts: TfrxDBDataset;
    frxDsStqCnsgBkCab: TfrxDBDataset;
    SpeedButton5: TSpeedButton;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCab: TBitBtn;
    BtItens: TBitBtn;
    QrStqCnsgBkCabCodCliInt: TIntegerField;
    Panel8: TPanel;
    Label5: TLabel;
    Ednfe_serie: TdmkEdit;
    Ednfe_nNF: TdmkEdit;
    Label6: TLabel;
    EdNFe_Id: TdmkEdit;
    Label192: TLabel;
    GroupBox4: TGroupBox;
    DBEdit3: TDBEdit;
    QrStqCnsgBkCabnfe_serie: TIntegerField;
    QrStqCnsgBkCabnfe_nNF: TIntegerField;
    QrStqCnsgBkCabnfe_Id: TWideStringField;
    Label9: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit4: TDBEdit;
    DBEdNFe_Id: TDBEdit;
    PMNovo: TPopupMenu;
    ID1: TMenuItem;
    Pesquisadetalhada1: TMenuItem;
    CkAtzPrcMed: TdmkCheckBox;
    DBCkAtzPrcMed: TDBCheckBox;
    QrStqCnsgBkCabAtzPrcMed: TSmallintField;
    QrStqCnsgBkCabTransporta: TIntegerField;
    QrStqCnsgBkCabNO_TRANSPORTA: TWideStringField;
    dmkLabel2: TdmkLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrTransporta: TMySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNO_ENT: TWideStringField;
    DsTransporta: TDataSource;
    dmkLabel3: TdmkLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    SbTransporta: TSpeedButton;
    QrStqCnsgBkCabValTotFrete: TFloatField;
    QrStqMovItsCustoBuy: TFloatField;
    QrStqMovItsCustoFrt: TFloatField;
    Fracionamento1: TMenuItem;
    N4: TMenuItem;
    QrStqMovItsPackGGX: TIntegerField;
    QrStqMovItsPackUnMed: TIntegerField;
    QrStqMovItsPackQtde: TFloatField;
    Incluinovoretorno1: TMenuItem;
    Alteraretornoatual1: TMenuItem;
    Excluiretornoatual1: TMenuItem;
    Encerraretorno1: TMenuItem;
    EdTabePrcCab: TdmkEditCB;
    dmkLabel6: TdmkLabel;
    CBTabePrcCab: TdmkDBLookupComboBox;
    SbTabePrcCab: TSpeedButton;
    QrTabePrcCab: TMySQLQuery;
    DsTabePrcCab: TDataSource;
    QrTabePrcCabCodigo: TIntegerField;
    QrTabePrcCabNome: TWideStringField;
    QrStqCnsgBkCabNO_TabePrcCab: TWideStringField;
    DBEdit9: TDBEdit;
    Label16: TLabel;
    QrStqCnsgBkCabTabePrcCab: TIntegerField;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    QrStqMovItsTwnCtrl: TIntegerField;
    QrStqMovItsTwnBrth: TIntegerField;
    QrForne: TMySQLQuery;
    QrForneE_ALL: TWideStringField;
    QrForneCNPJ_TXT: TWideStringField;
    QrForneNOME_TIPO_DOC: TWideStringField;
    QrForneTE1_TXT: TWideStringField;
    QrForneFAX_TXT: TWideStringField;
    QrForneNUMERO_TXT: TWideStringField;
    QrForneCEP_TXT: TWideStringField;
    QrForneCodigo: TIntegerField;
    QrForneTipo: TSmallintField;
    QrForneCodUsu: TIntegerField;
    QrForneNOME_ENT: TWideStringField;
    QrForneCNPJ_CPF: TWideStringField;
    QrForneIE_RG: TWideStringField;
    QrForneRUA: TWideStringField;
    QrForneCOMPL: TWideStringField;
    QrForneBAIRRO: TWideStringField;
    QrForneCIDADE: TWideStringField;
    QrForneNOMELOGRAD: TWideStringField;
    QrForneNOMEUF: TWideStringField;
    QrFornePais: TWideStringField;
    QrForneENDEREF: TWideStringField;
    QrForneTE1: TWideStringField;
    QrForneFAX: TWideStringField;
    QrForneIE: TWideStringField;
    QrForneCAD_FEDERAL: TWideStringField;
    QrForneCAD_ESTADUAL: TWideStringField;
    QrForneIE_TXT: TWideStringField;
    QrForneNO2_ENT: TWideStringField;
    QrForneNUMERO: TFloatField;
    QrForneUF: TFloatField;
    QrForneLOGRAD: TFloatField;
    QrForneCEP: TFloatField;
    frxDsForne: TfrxDBDataset;
    QrStqCnsgBkCabStqCiclCab: TIntegerField;
    Label18: TLabel;
    DBEdit16: TDBEdit;
    EdStqCiclCab: TdmkEdit;
    Label19: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqCnsgBkCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqCnsgBkCabBeforeOpen(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrStqCnsgBkCabBeforeClose(DataSet: TDataSet);
    procedure QrStqCnsgBkCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluiitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure QrStqCnsgBkCabCalcFields(DataSet: TDataSet);
    procedure SbForneceClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrForneCalcFields(DataSet: TDataSet);
    procedure QrStqCnsgBkCabAfterClose(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure ID1Click(Sender: TObject);
    procedure Pesquisadetalhada1Click(Sender: TObject);
    procedure Incluinovoretorno1Click(Sender: TObject);
    procedure Alteraretornoatual1Click(Sender: TObject);
    procedure Excluiretornoatual1Click(Sender: TObject);
    procedure Encerraretorno1Click(Sender: TObject);
    procedure SbTabePrcCabClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure HabilitaBotoes();
    //procedure AtivaItensNoEstoque(IDCtrl, Status: Integer);
  public
    { Public declarations }
    FThisFatID_Ret_Bxa,
    FThisFatID_Ret_Inn,
    FThisFatID_Frt,
    FStqCiclCab,
    FFornece,
    FEmpresa: Integer;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenStqMovIts(IDCtrl: Integer);
    procedure RateioCustoDoFrete();
    procedure HabilitaComps(SQLType: TSQLType);
  end;

var
  FmStqCnsgBkCab: TFmStqCnsgBkCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF,  UnDmkWeb, StqInnPesq,
  {$IfNDef semNFe_v0000}ModuleNFe_0000, NFeImporta_0400,{$EndIf}
  {$IFDEF FmPlacasAdd}PlacasAdd,{$ENDIF}
  {$IfNDef NO_FINANCEIRO}ModuleFin, UnPagtos,{$EndIf}
  UnGrade_Tabs, ModuleGeral, UCreate, StqInnNFe, Entidade2, StqCnsgBkIts,
  UMySQLDB, UnApp_Jan, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqCnsgBkCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqCnsgBkCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqCnsgBkCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqCnsgBkCab.DefParams;
begin
  VAR_GOTOTABELA := 'StqCnsgBkCab';
  VAR_GOTOMYSQLTABLE := QrStqCnsgBkCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('tpc.Nome NO_TabePrcCab, ');
  VAR_SQLx.Add('pem.BalQtdItem, pem.FatSemEstq, sic.*, ei.CodCliInt');
  VAR_SQLx.Add('FROM stqcnsgbkcab sic');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades fil  ON fil.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem  ON pem.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece');
  VAR_SQLx.Add('LEFT JOIN entidades tsp ON tsp.Codigo=sic.Transporta');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=sic.TabePrcCab');
  VAR_SQLx.Add('WHERE sic.Codigo > -1000');
  VAR_SQLx.Add('AND sic.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND sic.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sic.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sic.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Codigo IN (SELECT Codigo FROM stqcnsgbkcab WHERE Empresa IN (' + VAR_LIB_EMPRESAS + '))';
end;

procedure TFmStqCnsgBkCab.RateioCustoDoFrete();
{
var
  Codigo, IDCtrl, GraGruX, Empresa: Integer;
  Fator, SumQtde, ValTotFrete, CustoFrt, CustoAll: Double;
  SQLType: TSQLType;
}
begin
{
  SQLType := stUpd;
  Codigo  := QrStqCnsgBkCabCodigo.Value;
  Empresa := QrStqCnsgBkCabEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumFrt, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito',
  'FROM ' + FTabLctA + ' la',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgBkCabCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  '']);
  //
  ValTotFrete := QrSumFrtDebito.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(CustoBuy) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Tipo=' + Geral.FF0(FThisFatID_Buy),
  'AND OriCodi=' + Geral.FF0(QrStqCnsgBkCabCodigo.Value),
  '']);
  //
  SumQtde := QrSumItsQtde.Value;
  //
  if SumQtde > 0 then
    Fator := ValTotFrete / SumQtde
  else
    Fator := 0;
  //
  ReopenStqMovIts(0);
  QrStqMovIts.DisableControls;
  try
    QrStqMovIts.First;
    while not QrStqMovIts.Eof do
    begin
      CustoFrt := QrStqMovItsCustoBuy.Value * Fator;
      CustoAll := QrStqMovItsCustoBuy.Value + CustoFrt;
      //
      GraGruX  := QrStqMovItsGraGruX.Value;
      IDCtrl   := QrStqMovItsIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
      'CustoFrt', 'CustoAll'], [
      'IDCtrl'], [
      CustoFrt, CustoAll], [
      IDCtrl], False);
      //
      DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
      //
      QrStqMovIts.Next;
    end;
  finally
    QrStqMovIts.EnableControls;
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqcnsgbkcab', False, [
  'ValTotFrete'], [
  'Codigo'], [
  ValTotFrete], [
  Codigo], False);
  //
  LocCod(Codigo, Codigo);
}
end;

procedure TFmStqCnsgBkCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCnsgBkCab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqCnsgBkCab.Encerraretorno1Click(Sender: TObject);
var
  LastCod, Codigo, Empresa, Status: Integer;
  ListaGraGruX: TStringList;
  //
  Encerrou: String;
begin
  Codigo := QrStqCnsgBkCabCodigo.Value;
  //
  if QrStqCnsgBkCabEncerrou.Value = 0 then
  begin
    Encerrou := Geral.FDT(DModG.ObtemAgora(), 109);
    Status := 1;
  end else
  begin
    if MyObjects.FIC(QrStqCnsgBkCabStqCiclCab.Value > 0, nil,
    'Este retorno j� pertence ao ciclo n� ' + Geral.FF0(
    QrStqCnsgBkCabStqCiclCab.Value) + ' e n�o pode mais ser reaberto!' +
    sLineBreak + 'Para reabr�-lo, remova-o do ciclo.') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrUpdM, Dmod.MyDB, [
    'SELECT MAX(Codigo) Codigo',
    'FROM stqcnsgbkcab',
    'WHERE Fornece=' + Geral.FF0(QrStqCnsgBkCabFornece.Value),
    '']);
    LastCod := USQLDB.v_i(Dmod.QrUpdM, 'Codigo');
    if LastCod <> QrStqCnsgBkCabCodigo.Value then
    begin
      Geral.MB_Aviso('Somente o �ltimo retorno do vendedor/representante ' +
      QrStqCnsgBkCabNO_FORNECE.Value + ' pode ser desfeita!');
      Exit;
    end else
    begin
      Encerrou := '0';
      Status := 1;
    end;
    //
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqcnsgbkcab', False, [
  'Encerrou', 'Status'], ['Codigo'], [
  Encerrou, Status], [Codigo], True);
  //
  StqPF.AtivaItensNoEstoque_2(FThisFatID_Ret_Bxa, FThisFatID_Ret_Inn,
    QrStqCnsgBkCabCodigo.Value, Status);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmStqCnsgBkCab.Excluiitem1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrStqMovIts, DBGStqMovIts,
  'StqMovItsA', ['OriCodi', 'TwnCtrl'], ['OriCodi', 'TwnCtrl'], istPergunta, '');
end;

procedure TFmStqCnsgBkCab.Excluiretornoatual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrStqMovIts.RecordCount > 0 then
    Geral.MB_Aviso(
    'Este retorno n�o pode ser exclu�do, pois existem itens lan�ados nele.')
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o do retorno ID n�mero ' +
      Geral.FF0(QrStqCnsgBkCabCodigo.Value) + '?') <> ID_YES then Exit;
    //
    Codigo := QrStqCnsgBkCabCodigo.Value;
    //
    if DBCheck.ExcluiRegistro(Dmod.QrUpd, QrStqCnsgBkCab, 'StqCnsgBkCab', ['Codigo'],
      ['Codigo'], True, 'Confirma a exclus�o deste retorno?') then
    begin
      LocCod(Codigo, Codigo);
      Va(vpLast);
    end;
  end;
end;

procedure TFmStqCnsgBkCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqCnsgBkCab.Pesquisadetalhada1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmStqInnPesq, FmStqInnPesq, afmoNegarComAviso) then
  begin
    FmStqInnPesq.EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.FThisFatID             := FThisFatID_Ret_Inn;
    FmStqInnPesq.ShowModal;
    FmStqInnPesq.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
    LocCod(VAR_CADASTRO, VAR_CADASTRO);
end;

procedure TFmStqCnsgBkCab.PMCabPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrStqCnsgBkCab.State <> dsInactive) and (QrStqCnsgBkCab.RecordCount > 0);
  //
  if Enab then
    Enab2 := QrStqCnsgBkCabEncerrou.Value = 0
  else
    Enab2 := False;
  //
  AlteraRetornoatual1.Enabled := Enab and Enab2;
  ExcluiRetornoatual1.Enabled := Enab and Enab2;
  EncerraRetorno1.Enabled     := Enab;
end;

procedure TFmStqCnsgBkCab.PMItensPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrStqCnsgBkCab.State <> dsInactive) and (QrStqCnsgBkCab.RecordCount > 0);
  Enab2 := (QrStqMovIts.State <> dsInactive) and (QrStqMovIts.RecordCount > 0);
  //
  Incluiitem1.Enabled := Enab;
  Fracionamento1.Enabled := Enab and Enab2;
  Excluiitem1.Enabled := Enab and Enab2;
end;

procedure TFmStqCnsgBkCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqCnsgBkCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqCnsgBkCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqCnsgBkCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqCnsgBkCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqCnsgBkCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqCnsgBkCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqCnsgBkCab.SpeedButton5Click(Sender: TObject);
begin
  {$IFDEF FmPlacasAdd}
  if DBCheck.CriaFm(TFmPlacasAdd, FmPlacasAdd, afmoNegarComAviso) then
  begin
    FmPlacasAdd.ShowModal;
    EdNome.ValueVariant := FmPlacasAdd.FPlacas;
    FmPlacasAdd.Destroy;
  end;
  {$ENDIF}
end;

procedure TFmStqCnsgBkCab.SbForneceClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdFornece.ValueVariant;

  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFornece, CBFornece, QrFornece, VAR_CADASTRO);
    EdFornece.SetFocus;
  end;
end;

procedure TFmStqCnsgBkCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqCnsgBkCabCodigo.Value;
  Close;
end;

procedure TFmStqCnsgBkCab.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmStqCnsgBkCab.Alteraretornoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrStqCnsgBkCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'StqCnsgBkCab');
  //
  HabilitaComps(stUpd);
  //
  GroupBox2.Enabled := QrStqMovIts.RecordCount = 0;
  //
  EdFornece.Enabled := QrStqMovIts.RecordCount = 0;
  CBFornece.Enabled := QrStqMovIts.RecordCount = 0;
end;

(*
procedure TFmStqCnsgBkCab.AtivaItensNoEstoque(IDCtrl, Status: Integer);
var
  Ativo: Integer;
begin
  if Status > 0 then // Existe status maior que 1?
    Ativo := 1
  else
    Ativo := 0;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE stqmovitsa ',
  'SET Ativo=' + Geral.FF0(Status),
  'WHERE Tipo IN (' + Geral.FF0(FThisFatID_Ret_Bxa) + ',' +
    Geral.FF0(FThisFatID_Ret_Inn) + ')',
  'AND OriCodi="' + Geral.FF0(IDCtrl) + '"',
  '']);
end;
*)

procedure TFmStqCnsgBkCab.BtConfirmaClick(Sender: TObject);
var
  Nome, Abertura, (*Encerrou,*) nfe_Id: String;
  Codigo, CodUsu, Empresa, (*Status,*) Fornece, Cliente, nfe_serie,
  nfe_nNF, AtzPrcMed, Transporta, TabePrcCab, StqCiclCab: Integer;
  //ValTotFrete: Double;
  SQLType: TSQLType;
begin
  //
  if MyObjects.FIC(EdFilial.ValueVariant = 0, EdFilial, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdFornece.ValueVariant = 0, EdFornece, 'Informe o vendedor/representante!') then Exit;
  //if MyObjects.FIC(EdCodigo.ValueVariant = 0, nil, 'Informe o c�digo!') then Exit;
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  CodUsu         := EdCodUsu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  Abertura       := Geral.FDT(EdAbertura.ValueVariant, 109);
  //Encerrou       := ;
  //Status         := ;
  Fornece        := EdFornece.ValueVariant;
  Cliente        := 0;
  nfe_serie      := Ednfe_serie.ValueVariant;
  nfe_nNF        := Ednfe_nNF.ValueVariant;;
  nfe_Id         := Ednfe_Id.ValueVariant;;
  AtzPrcMed      := 0;
  Transporta     := EdTransporta.ValueVariant;
  TabePrcCab     := EdTabePrcCab.ValueVariant;
  StqCiclCab     := EdStqCiclCab.ValueVariant;

  //ValTotFrete    := ;
  //
  Codigo := UMyMod.BPGS1I32('stqcnsgbkcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if SQLType = stIns then
    CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqcnsgbkcab', False, [
  'CodUsu', 'Nome', 'Empresa',
  'Abertura', (*'Encerrou', 'Status',*)
  'Fornece', 'Cliente', 'nfe_serie',
  'nfe_nNF', 'nfe_Id', 'AtzPrcMed',
  'Transporta'(*, 'ValTotFrete'*), 'TabePrcCab',
  'StqCiclCab'], [
  'Codigo'], [
  CodUsu, Nome, Empresa,
  Abertura, (*Encerrou, Status,*)
  Fornece, Cliente, nfe_serie,
  nfe_nNF, nfe_Id, AtzPrcMed,
  Transporta(*, ValTotFrete*), TabePrcCab,
  StqCiclCab], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmStqCnsgBkCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'StqCnsgBkCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCnsgBkCab', 'Codigo');
  MostraEdicao(0, stlok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCnsgBkCab', 'Codigo');
end;

procedure TFmStqCnsgBkCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmStqCnsgBkCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FStqCiclCab := 0;
  FFornece := 0;
  FEmpresa := 0;
  FThisFatID_Ret_Bxa := VAR_FATID_4203;
  FThisFatID_Ret_Inn := VAR_FATID_4204;
  FThisFatID_Frt := VAR_FATID_4213;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBFilial.ListSource := DModG.DsEmpresas;
  PnDados.Align := alClient;
  DBGStqMovIts.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrTabePrcCab, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM tabeprccab',
  'WHERE Codigo=0',
  'OR',
  '(',
  '  ' + Geral.FF0(1) + ' & Aplicacao',
  '  AND "' + Geral.FDT(DModG.ObtemAgora(), 1) + '" BETWEEN DataI and DataF',
  ')',
  'ORDER BY Nome',
  '']);
*)
  StqPF.ReopenTabePrcCab(QrTabePrcCab, (*Aplicacao*)1);
  //
  if UpperCase(Application.Title) = 'SAFECAR' then
  begin
    EdNome.Width         := 817;
    SpeedButton5.Visible := True;
  end else
  begin
    EdNome.Width         := 845;
    SpeedButton5.Visible := False;    
  end;
end;

procedure TFmStqCnsgBkCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqCnsgBkCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCnsgBkCab.SbImprimeClick(Sender: TObject);
begin
  DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  DModG.ReopenEndereco(Geral.IMV(VAR_LIB_EMPRESAS));
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrForne, Dmod.MyDB, [
    'SELECT en.Codigo, Tipo, CodUsu, IE, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF,',
    'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO2_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG,',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA,',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0.000 NUMERO,',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL,',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO,',
    'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CIDADE,',
    'CASE WHEN en.Tipo=0 THEN en.EUF         ELSE en.PUF      END + 0.000 UF,',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD,',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF,',
    'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais,',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0.000 Lograd,',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0.000 CEP,',
    'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF,',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1,',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX,',
    'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL',
    'FROM entidades en',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd',
    'WHERE en.Codigo=' + Geral.FF0(QrStqCnsgBkCabFornece.Value),
    '']);
*)
  StqPF.ReopenForne(QrForne, QrStqCnsgBkCabFornece.Value);
  //
  MyObjects.frxDefineDataSets(frxSTQ_MOVIM_006, [
    DModG.frxDsMaster,
    DModG.frxDsEndereco,
    frxDsForne,
    frxDsStqCnsgBkCab,
    frxDsStqMovIts
    ]);
  //
  MyObjects.frxMostra(frxSTQ_MOVIM_006, 'Informe da Retorno de Consigna��o n� ' +
    FormatFloat('000000', QrStqCnsgBkCabCodUsu.Value));
end;

procedure TFmStqCnsgBkCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqCnsgBkCab.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, SbNovo);
end;

procedure TFmStqCnsgBkCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqCnsgBkCab.QrForneCalcFields(DataSet: TDataSet);
begin
(*
  QrForneTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneTe1.Value);
  QrForneFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneFax.Value);
  QrForneCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrForneCNPJ_CPF.Value);
  QrForneIE_TXT.Value :=
    Geral.Formata_IE(QrForneIE_RG.Value, QrForneUF.Value, '??', QrForneTipo.Value);
  QrForneNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrForneRua.Value, Trunc(QrForneNumero.Value), False);
  //
  QrForneE_ALL.Value := UpperCase(QrForneNOMELOGRAD.Value);
  if Trim(QrForneE_ALL.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ';
  QrForneE_ALL.Value := QrForneE_ALL.Value + Uppercase(QrForneRua.Value);
  if Trim(QrForneRua.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNUMERO_TXT.Value;
  if Trim(QrForneCompl.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ' + Uppercase(QrForneCompl.Value);
  if Trim(QrForneBairro.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneBairro.Value);
  if QrForneCEP.Value > 0 then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrForneCEP.Value);
  if Trim(QrForneCidade.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneCidade.Value);
  if Trim(QrForneNOMEUF.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNOMEUF.Value;
  if Trim(QrFornePais.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + QrFornePais.Value;
*)
  StqPF.QrForneCalcFields(QrForne);
end;

procedure TFmStqCnsgBkCab.QrStqCnsgBkCabAfterClose(DataSet: TDataSet);
begin
  HabilitaBotoes;
end;

procedure TFmStqCnsgBkCab.QrStqCnsgBkCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  HabilitaBotoes;
end;

procedure TFmStqCnsgBkCab.QrStqCnsgBkCabAfterScroll(DataSet: TDataSet);
begin
  ReopenStqMovIts(0);
  //
  BtItens.Enabled := QrStqCnsgBkCabEncerrou.Value = 0;
end;

procedure TFmStqCnsgBkCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdNFe_Id.CharCase   := ecNormal;
  DBEdNFe_Id.CharCase := ecNormal;
end;

procedure TFmStqCnsgBkCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqCnsgBkCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'StqCnsgBkCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqCnsgBkCab.SbTabePrcCabClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_jan.MostraFormTabePrcCab();
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTabePrcCab, CBTabePrcCab, QrTabePrcCab, VAR_CADASTRO);
    EdTabePrcCab.SetFocus;
  end;
end;

procedure TFmStqCnsgBkCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCnsgBkCab.HabilitaBotoes;
var
  Enab: Boolean;
  Texto: string;
begin
  if (QrStqCnsgBkCabStatus.Value > 0) and (QrStqCnsgBkCabEncerrou.Value <> 0) and
    (QrStqCnsgBkCab.RecordCount > 0) then
  begin
    Enab  := False;
    Texto := 'Desfaz encerramento da retorno';
  end else
  begin
    Enab  := True;
    Texto := 'Encerra a retorno';
  end;
  //
  BtItens.Enabled             := Enab;
  Alteraretornoatual1.Enabled := Enab;
  Excluiretornoatual1.Enabled := Enab;
  Encerraretorno1.Caption     := Texto;
end;

procedure TFmStqCnsgBkCab.HabilitaComps(SQLType: TSQLType);
var
  Habilita: Boolean;
begin
  Habilita := (SQLType = stIns) or ((QrStqMovIts.State <> dsInactive) and (QrStqMovIts.RecordCount = 0));
  GroupBox2.Enabled := Habilita;
  LaFornece.Enabled := Habilita;
  EdFornece.Enabled := Habilita;
  CBFornece.Enabled := Habilita;
  SbFornece.Enabled := Habilita;
end;

procedure TFmStqCnsgBkCab.ID1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqCnsgBkCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmStqCnsgBkCab.Incluiitem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqCnsgBkIts, FmStqCnsgBkIts, afmoNegarComAviso) then
  begin
    FmStqCnsgBkIts.ImgTipo.SQLType := stIns;
    FmStqCnsgBkIts.FTabePrcCab := QrStqCnsgBkCabTabePrcCab.Value;
    //
    FmStqCnsgBkIts.ShowModal;
    FmStqCnsgBkIts.Destroy;
    //
    ReopenStqMovIts(0);
  end;
end;

procedure TFmStqCnsgBkCab.Incluinovoretorno1Click(Sender: TObject);
begin
  if AppPF.ImpedePorMovimentoAberto() then
    Exit;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrStqCnsgBkCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'StqCnsgBkCab');
  //
  HabilitaComps(stIns);
  EdAbertura.ValueVariant := DModG.ObtemAgora();
  EdFilial.ValueVariant   := DModG.QrFiliLogFilial.Value;
  CBFilial.KeyValue       := DModG.QrFiliLogFilial.Value;
  EdStqCiclCab.ValueVariant := FStqCiclCab;
  if FFornece <> 0 then
  begin
    EdFornece.ValueVariant := FFornece;
    CBFornece.KeyValue := FFornece;
    //
    EdFornece.Enabled := False;
    CBFornece.Enabled := False;
  end;
  if FEmpresa <> 0 then
  begin
    DmodG.QrEmpresas.Locate('Codigo', FEmpresa, []);
    EdFilial.ValueVariant := DModG.QrEmpresasFilial.Value;
    CBFilial.KeyValue := DModG.QrEmpresasFilial.Value;
    //
    EdFilial.Enabled := False;
    CBFilial.Enabled := False;
  end;
end;

procedure TFmStqCnsgBkCab.QrStqCnsgBkCabBeforeClose(DataSet: TDataSet);
begin
  QrStqMovIts.Close;
  //
  BtItens.Enabled := False;
end;

procedure TFmStqCnsgBkCab.QrStqCnsgBkCabBeforeOpen(DataSet: TDataSet);
begin
  QrStqCnsgBkCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqCnsgBkCab.QrStqCnsgBkCabCalcFields(DataSet: TDataSet);
begin
  if QrStqCnsgBkCabEncerrou.Value = 0 then
    QrStqCnsgBkCabENCERROU_TXT.Value := CO_MovimentoAberto
  else
    QrStqCnsgBkCabENCERROU_TXT.Value := Geral.FDT(QrStqCnsgBkCabEncerrou.Value, 0);
  //
end;

procedure TFmStqCnsgBkCab.ReopenStqMovIts(IDCtrl: Integer);
begin
  StqPF.ReopenStqMovIts(QrStqMovIts, FThisFatID_Ret_Inn, QrStqCnsgBkCabCodigo.Value, IDCtrl);
end;

end.

