object FmGraGruNiveis2: TFmGraGruNiveis2
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-041 :: N'#237'veis 2 de Grade'
  ClientHeight = 452
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 217
        Height = 32
        Caption = 'N'#237'veis 2 de Grade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 217
        Height = 32
        Caption = 'N'#237'veis 2 de Grade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 217
        Height = 32
        Caption = 'N'#237'veis 2 de Grade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 92
    Width = 1008
    Height = 217
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 147
      Width = 1008
      Height = 70
      Align = alBottom
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 862
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 860
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtInclui: TBitBtn
          Tag = 10
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 136
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 260
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 147
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 147
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBGrid1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 584
          Height = 147
          Align = alClient
          DataSource = DsGraGru2
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Nivel2'
              ReadOnly = True
              Title.Caption = 'N'#237'vel 2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 298
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Tipo_Item'
              Title.Caption = 'Tipo de item (SPED EFD)'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 808
          Top = 0
          Width = 200
          Height = 147
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 200
            Height = 85
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 46
              Height = 13
              Caption = 'Pesquisa:'
            end
            object EdPesq: TEdit
              Left = 8
              Top = 20
              Width = 185
              Height = 21
              TabOrder = 0
              OnChange = EdPesqChange
            end
            object TBTam: TTrackBar
              Left = 4
              Top = 40
              Width = 193
              Height = 41
              Min = 1
              ParentShowHint = False
              Position = 4
              SelStart = 3
              ShowHint = True
              TabOrder = 1
              OnChange = TBTamChange
            end
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 85
            Width = 200
            Height = 62
            Align = alClient
            DataSource = DsPesq
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid2DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'd.'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 131
                Visible = True
              end>
          end
        end
        object LbItensMD: TListBox
          Left = 584
          Top = 0
          Width = 224
          Height = 147
          Align = alRight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ItemHeight = 14
          ParentFont = False
          TabOrder = 2
          Visible = False
          OnDblClick = LbItensMDDblClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 309
    Width = 1008
    Height = 143
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 501
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 80
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel8: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object TbGraGru2: TMySQLTable
    Database = Dmod.MyDB
    BeforeOpen = TbGraGru2BeforeOpen
    AfterInsert = TbGraGru2AfterInsert
    BeforePost = TbGraGru2BeforePost
    AfterPost = TbGraGru2AfterPost
    OnCalcFields = TbGraGru2CalcFields
    OnNewRecord = TbGraGru2NewRecord
    OnDeleting = TbGraGru2Deleting
    TableName = 'gragru2'
    Left = 120
    Top = 132
    object TbGraGru2Nivel5: TIntegerField
      FieldName = 'Nivel5'
      Required = True
    end
    object TbGraGru2Nivel4: TIntegerField
      FieldName = 'Nivel4'
      Required = True
    end
    object TbGraGru2Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object TbGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object TbGraGru2CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object TbGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object TbGraGru2ContaCTB: TIntegerField
      FieldName = 'ContaCTB'
      Required = True
    end
    object TbGraGru2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object TbGraGru2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object TbGraGru2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbGraGru2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbGraGru2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbGraGru2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbGraGru2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbGraGru2Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbGraGru2Tipo_ITEM: TSmallintField
      FieldName = 'Tipo_ITEM'
    end
    object TbGraGru2NO_Tipo_Item: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Tipo_Item'
      Size = 255
      Calculated = True
    end
  end
  object DsGraGru2: TDataSource
    DataSet = TbGraGru2
    Left = 120
    Top = 176
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 192
    Top = 136
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 192
    Top = 180
  end
end
