object FmFisRegEnPsq: TFmFisRegEnPsq
  Left = 339
  Top = 185
  Caption = 'FIS-REGRA-007 :: Estri'#231#227'o de Regra Fiscal'
  ClientHeight = 447
  ClientWidth = 1002
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1002
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 954
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 906
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 299
        Height = 32
        Caption = 'Estri'#231#227'o de Regra Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 32
        Caption = 'Estri'#231#227'o de Regra Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 299
        Height = 32
        Caption = 'Estri'#231#227'o de Regra Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 333
    Width = 1002
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 998
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 377
    Width = 1002
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 856
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 854
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1002
    Height = 285
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object DBGFisRegEnt: TDBGrid
      Left = 0
      Top = 41
      Width = 1002
      Height = 244
      Align = alClient
      DataSource = DsFisRegEnt
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGFisRegEntDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Width = 46
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FisRegCad'
          Title.Caption = 'Nome da Regra Fiscal'
          Width = 295
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Observacao'
          Title.Caption = 'Observa'#231#227'o'
          Width = 621
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1002
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 204
      ExplicitTop = 8
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 348
        Height = 13
        Caption = 
          'Informe uma parte do nome da regra fiscal ou da observa'#231#227'o da es' +
          'tri'#231#227'o: '
      end
      object EdPsq: TEdit
        Left = 372
        Top = 12
        Width = 289
        Height = 21
        TabOrder = 0
        OnChange = EdPsqChange
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrFisRegEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Nome NO_FisRegCad, '
      'fre.*  '
      'FROM fisregent fre '
      'LEFT JOIN fisregcad frc ON frc.Codigo=fre.Codigo'
      'WHERE fre.Entidade=1')
    Left = 35
    Top = 71
    object QrFisRegEntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegEntEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrFisRegEntLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrFisRegEntDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFisRegEntDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFisRegEntUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrFisRegEntUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrFisRegEntAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrFisRegEntAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrFisRegEntAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrFisRegEntAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFisRegEntObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrFisRegEntNO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Size = 50
    end
  end
  object DsFisRegEnt: TDataSource
    DataSet = QrFisRegEnt
    Left = 35
    Top = 127
  end
end
