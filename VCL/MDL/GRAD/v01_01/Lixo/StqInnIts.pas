unit StqInnIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DB,
  mySQLDbTables, dmkRadioGroup, Mask, DBCtrls, dmkEdit, dmkCheckGroup,
  dmkGeral, dmkEditCalc, dmkImage, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox,
  dmkValUsu;

type
  TFmStqInnIts = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    PainelGGX: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    QrGraGruX: TmySQLQuery;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    DsGraGruX: TDataSource;
    Panel4: TPanel;
    CGHowBxaEstq: TdmkDBCheckGroup;
    RGGerBxaEstq: TDBRadioGroup;
    QrGraGruXHowBxaEstq: TSmallintField;
    QrGraGruXGerBxaEstq: TSmallintField;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrGraGruXNo_SIGLA: TWideStringField;
    QrGraGruXSigla: TWideStringField;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    Label2: TLabel;
    DBEdit5: TDBEdit;
    Panel2: TPanel;
    Label11: TLabel;
    EdGraGruX: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    GBOutrasUnidades: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdPecas: TdmkEdit;
    EdPLE: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdCustoAll: TdmkEdit;
    Label20: TLabel;
    QrStqCenLoc: TmySQLQuery;
    DsStqCenLoc: TDataSource;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNome: TWideStringField;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdStqCenLoc: TdmkEditCB;
    QrGraGruXGrandeza: TIntegerField;
    Panel9: TPanel;
    dmkLabel2: TdmkLabel;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    dmkLabel3: TdmkLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    RGCasasProd: TdmkRadioGroup;
    QrPrdGrupTip: TmySQLQuery;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    DsPrdGrupTip: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    VUPrdGrupTip: TdmkValUsu;
    VUStqCenCad: TdmkValUsu;
    QrPrdGrupTipFracio: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGGerBxaEstqChange(Sender: TObject);
    procedure DBEdit4Change(Sender: TObject);
    procedure EdQtdeExit(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
    procedure RGCasasProdClick(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaPainelGGX();
    procedure MostraGraGruPesq1(Nivel1: Integer);
    procedure MostraEdicao(Tipo: TSQLType);
    procedure ReopenGraGruX();
    procedure ReopenStqCenCad(StqCenCad: Integer);
    procedure ConfiguraFracioProd(Casas: Integer);
  public
    { Public declarations }
  end;

  var
  FmStqInnIts: TFmStqInnIts;

implementation

uses UnMyObjects, StqInnCad, UMySQLModule, Module, GraGruPesq1, MyDBCheck,
  ModuleGeral, DmkDAC_PF, ModProd, MyListas;

{$R *.DFM}

procedure TFmStqInnIts.BtOKClick(Sender: TObject);
const
  OriCnta = 0;
  FatorClas = 1;
  OriPart = 0;
var
  DataHora, Texto: String;
  GerBxaEstq,
  t, IDCtrl, Tipo, OriCodi, OriCtrl, Empresa, StqCenCad, StqCenLoc,
  GraGruX: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll: Double;
begin
  //Aparentemente usa apenas no Bluederm
  if CO_DMKID_APP = 2 then //Bluederm
  begin
    GerBxaEstq := RGGerBxaEstq.ItemIndex;
    //
    if MyObjects.FIC(GerBxaEstq < 1, nil, 'Defina uma grandeza!') then Exit;
  end;
  //
  DataHora  := DModG.ObtemAgoraTxt();
  Tipo      := FmStqInnCad.FThisFatID;
  OriCodi   := FmStqInnCad.QrStqInnCadCodigo.Value;
  Empresa   := FmStqInnCad.QrStqInnCadEmpresa.Value;
  StqCenCad := EdStqCenCad.ValueVariant;
  StqCenLoc := EdStqCenLoc.ValueVariant;
  //
  if MyObjects.FIC(StqCenCad = 0, EdStqCenCad, 'Defina o centro de estoque!') then Exit;
  //
  GraGruX   := EdGraGruX.ValueVariant;
  Pecas     := EdPecas.ValueVariant;
  Peso      := EdPLE.ValueVariant;
  AreaM2    := EdAreaM2.ValueVariant;
  AreaP2    := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  CustoAll  := EdCustoAll.ValueVariant;
(*  case RGGerBxaEstq.ItemIndex of
    0: Qtde  := 0;
    1: Qtde  := Pecas;
    2: Qtde  := AreaM2;
    3: Qtde  := Peso;
    else Qtde  := 0;
  end;
*)
  Qtde := EdQtde.ValueVariant;
  //
  if MyObjects.FIC(Qtde <= 0, nil, 'Quantidade n�o definida!') then Exit;
  if MyObjects.FIC(CustoAll <= 0, nil, 'Custo total n�o definido!') then Exit;
  //
  //if QrErros.RecordCount > 0 then
  //begin
    Texto := '';
    t := QrGraGruXHowBxaEstq.Value;
    if GraGruX = 0 then
      Texto := Texto + sLineBreak + '-> Falta informar o reduzido.';
    if (Geral.IntInConjunto(1, t) and (Pecas = 0)) then
      Texto := Texto + sLineBreak + '-> Falta informar a quantidade de pe�as.';
    if (Geral.IntInConjunto(2, t) and (AreaM2 = 0)) then
      Texto := Texto + sLineBreak + '-> Falta informar a �rea.';
    if (Geral.IntInConjunto(4, t) and (Peso = 0)) then
      Texto := Texto + sLineBreak + '-> Falta informar o peso.';
    //
    if Texto <> '' then
    begin
      Texto := 'Neste item falta:' + Texto;
      Geral.MensagemBox(Texto, 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  //end;  
  //
  //OBS.: Ativo 0 para atualizar o estoque somente no encerramento
  //
  IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  OriCtrl  := IDCtrl;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
  'DataHora', 'Tipo', 'OriCodi',
  'OriCtrl', 'OriCnta', 'Empresa',
  'StqCenCad', 'StqCenLoc',
  'GraGruX', 'Qtde',
  'OriPart', 'Pecas', 'Peso',
  'AreaM2', 'AreaP2', 'FatorClas', 'CustoAll', 'Ativo'
  {, 'QuemUsou', 'Retorno', 'ParTipo',
  'ParCodi', 'DebCtrl', 'SMIMultIns'}], [
  'IDCtrl'], [
  DataHora, Tipo, OriCodi,
  OriCtrl, OriCnta, Empresa,
  StqCenCad, StqCenLoc,
  GraGruX, Qtde,
  OriPart, Pecas, Peso,
  AreaM2, AreaP2, FatorClas, CustoAll, 0
  {, QuemUsou, Retorno, ParTipo,
  ParCodi, DebCtrl, SMIMultIns}], [
  IDCtrl], False) then
  begin
    DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
{ N�o precisa?
? := UMyMod.BuscaEmLivreY_Def('stqmovvala', ''ID', ImgTipo.SQLType, CodAtual);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovvala', auto_increment?[
'IDCtrl', 'Tipo', 'OriCodi', 
'OriCtrl', 'OriCnta', 'SeqInReduz', 
'Empresa', 'StqCenCad', 'GraGruX', 
'Qtde', 'Preco', 'Total', 
'CFOP', 'InfAdCuztm', 'PercCustom', 
'MedidaC', 'MedidaL', 'MedidaA', 
'MedidaE', 'MedOrdem', 'TipoNF', 
'refNFe', 'modNF', 'Serie', 
'nNF', 'SitDevolu', 'Servico', 
'RefProd', 'CFOP_Contrib', 'CFOP_MesmaUF', 
'CFOP_Proprio'], [
'ID'], [
IDCtrl, Tipo, OriCodi, 
OriCtrl, OriCnta, SeqInReduz, 
Empresa, StqCenCad, GraGruX, 
Qtde, Preco, Total, 
CFOP, InfAdCuztm, PercCustom, 
MedidaC, MedidaL, MedidaA, 
MedidaE, MedOrdem, TipoNF, 
refNFe, modNF, Serie, 
nNF, SitDevolu, Servico,
RefProd, CFOP_Contrib, CFOP_MesmaUF,
CFOP_Proprio], [
ID], UserDataAlterweb?, IGNORE?
}
  //
  end;
  if not CkContinuar.Checked then
  begin
    FmStqInnCad.LocCod(
      FmStqInnCad.QrStqInnCadCodigo.Value, FmStqInnCad.QrStqInnCadCodigo.Value);
    Close;
  end else
    MostraEdicao(stIns);
end;

procedure TFmStqInnIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqInnIts.ConfiguraFracioProd(Casas: Integer);
var
  CasasProd: Integer;
begin
  CasasProd := DmodG.QrControle.FieldByName('CasasProd').AsInteger;
  //
  EdQtde.DecimalSize   := Casas;
  EdPecas.DecimalSize  := Casas;
  EdPLE.DecimalSize    := Casas;
  EdAreaM2.DecimalSize := Casas;
  EdAreaP2.DecimalSize := Casas;
  //
  EdCustoAll.DecimalSize := CasasProd;
end;

procedure TFmStqInnIts.DBEdit4Change(Sender: TObject);
begin
  LaQtde.Caption := DBEdit4.Text + ':';
end;

procedure TFmStqInnIts.EdGraGruXChange(Sender: TObject);
begin
  VerificaPainelGGX();
end;

procedure TFmStqInnIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  GraGruX, Nivel1: Integer;
begin
  if Key=VK_F4 then
  begin
    GraGruX := EdGraGruX.ValueVariant;
    //
    if GraGruX <> 0 then
      Nivel1 := DmProd.ObtemGraGru1deGraGruX(GraGruX)
    else
      Nivel1 := 0;
    //
    MostraGraGruPesq1(Nivel1);
  end;
end;

procedure TFmStqInnIts.EdPrdGrupTipChange(Sender: TObject);
var
  Fracio, PrdGrupTip: Integer;
begin
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  //
  if PrdGrupTip <> 0 then
  begin
    Fracio := QrPrdGrupTipFracio.Value;
    //
    RGCasasProd.ItemIndex := Fracio;
    //
    ConfiguraFracioProd(Fracio);
  end;
end;

procedure TFmStqInnIts.EdQtdeExit(Sender: TObject);
begin
  if EdQtde.ValueVariant <> 0 then
    DmProd.ConfiguraOutrasMedidas(EdQtde, EdPecas, EdPLE, EdAreaM2, EdAreaP2, QrGraGruXGrandeza.Value);
end;

procedure TFmStqInnIts.EdStqCenCadChange(Sender: TObject);
var
  StqCenCad: Integer;
begin
  StqCenCad := EdStqCenCad.ValueVariant;
  //
  if StqCenCad <> 0 then
    ReopenStqCenCad(VUStqCenCad.ValueVariant);
end;

procedure TFmStqInnIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqInnIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  //
  ReopenGraGruX();
  //
  CkContinuar.Checked := ImgTipo.SQLType = stIns;
end;

procedure TFmStqInnIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqInnIts.MostraEdicao(Tipo: TSQLType);
begin
  case Tipo of
    stIns:
    begin
      EdGraGruX.ValueVariant  := '';
      EdPecas.ValueVariant    := 0;
      EdPLE.ValueVariant      := 0;
      EdAreaM2.ValueVariant   := 0;
      EdAreaP2.ValueVariant   := 0;
      EdCustoAll.ValueVariant := 0;
      //
      VerificaPainelGGX();
    end;
  end;
  EdGraGruX.SetFocus;
end;

procedure TFmStqInnIts.MostraGraGruPesq1(Nivel1: Integer);
var
  PrdGrupTip: Integer;
  PGT: String;
begin
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  //
  if MyObjects.FIC(PrdGrupTip = 0, EdPrdGrupTip, 'Defina o tipo de grupo de produtos!') then Exit;
  //
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    PGT := Geral.FF0(VUPrdGrupTip.ValueVariant);
    //
    FmGraGruPesq1.QrGraGru1.Close;
    FmGraGruPesq1.QrGraGru1.SQL.Clear;
    FmGraGruPesq1.QrGraGru1.SQL.Add('SELECT gg1.CodUsu, gg1.Nivel1, ');
    FmGraGruPesq1.QrGraGru1.SQL.Add('gg1.GraTamCad, gg1.Nome');
    FmGraGruPesq1.QrGraGru1.SQL.Add('FROM gragru1 gg1');
    FmGraGruPesq1.QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    FmGraGruPesq1.QrGraGru1.SQL.Add('WHERE pgt.Codigo=' + PGT);
    FmGraGruPesq1.QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
    UnDmkDAC_PF.AbreQuery(FmGraGruPesq1.QrGraGru1, DMod.MyDB);
    //
    FmGraGruPesq1.VUGraGru1.ValueVariant := Nivel1;
    //
    FmGraGruPesq1.ShowModal;
    //
    EdGraGruX.Text := FmGraGruPesq1.FGraGruX;
    //
    FmGraGruPesq1.Destroy;
  end;
end;

procedure TFmStqInnIts.ReopenGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle GraGruX,  ',
  'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  ',
  'gcc.Nome NO_COR,  gti.Nome NO_TAM, ',
  'med.Nome No_SIGLA, med.Sigla, med.Grandeza ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
  '']);
end;

procedure TFmStqInnIts.ReopenStqCenCad(StqCenCad: Integer);
begin
  UnDmkDac_PF.AbreMySQLQuery0(QrStqCenLoc, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM stqcenloc ',
  'WHERE Codigo=' + Geral.FF0(StqCenCad),
  'ORDER BY Nome ',
  '']);
end;

procedure TFmStqInnIts.RGCasasProdClick(Sender: TObject);
var
  Fracio: Integer;
begin
  Fracio := RGCasasProd.ItemIndex;
  //
  ConfiguraFracioProd(Fracio);
end;

procedure TFmStqInnIts.RGGerBxaEstqChange(Sender: TObject);
var
  Enab: Integer;
begin
  Enab := 0;
  case RGGerBxaEstq.ItemIndex of
    1: Enab := 1;
    2: Enab := 2;
    3: Enab := 3;
  end;
  EdPecas.Enabled  := Enab = 1;
  EdPLE.Enabled    := Enab = 3;
  EdAreaM2.Enabled := Enab = 2;
  EdAreaP2.Enabled := Enab = 2;
end;

procedure TFmStqInnIts.SpeedButton1Click(Sender: TObject);
var
  GraGruX, Nivel1: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  //
  if GraGruX <> 0 then
    Nivel1 := DmProd.ObtemGraGru1deGraGruX(GraGruX)
  else
    Nivel1 := 0;
  //
  MostraGraGruPesq1(Nivel1);
  //
  ReopenGraGruX;
end;

procedure TFmStqInnIts.VerificaPainelGGX();
var
  GraGruX: Integer;
begin
  GraGruX                  := Geral.IMV(EdGraGruX.Text);
  PainelGGX.Visible        := QrGraGruX.Locate('GraGruX', GraGruX, []);
  GBOutrasUnidades.Visible := QrGraGruXHowBxaEstq.Value > 0;
end;

end.
