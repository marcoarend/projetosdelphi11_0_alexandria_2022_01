unit PrdGruNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, dmkLabel, Mask, dmkGeral,
  Menus, dmkImage, UnDmkEnums;

type
  TFmPrdGruNew = class(TForm)
    PnTipo: TPanel;
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    Panel3: TPanel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    QrPrdGrupTipMadeBy: TSmallintField;
    QrPrdGrupTipFracio: TSmallintField;
    QrPrdGrupTipTipPrd: TSmallintField;
    QrPrdGrupTipNivCad: TSmallintField;
    QrPrdGrupTipNOME_MADEBY: TWideStringField;
    QrPrdGrupTipNOME_FRACIO: TWideStringField;
    QrPrdGrupTipNOME_TIPPRD: TWideStringField;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    QrPrdGrupTipFaixaIni: TIntegerField;
    QrPrdGrupTipFaixaFim: TIntegerField;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    PnNiveis: TPanel;
    PnNivel3: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    EdNome3: TdmkEdit;
    StaticText3: TStaticText;
    PnNivel2: TPanel;
    Label11: TLabel;
    EdCodUsu2: TdmkEdit;
    EdNome2: TdmkEdit;
    StaticText2: TStaticText;
    PnNivel1: TPanel;
    Label13: TLabel;
    EdCodUsu1: TdmkEdit;
    EdNome1: TdmkEdit;
    StaticText1: TStaticText;
    QrGraGru5: TmySQLQuery;
    DsGraGru5: TDataSource;
    Label10: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    DBEdNivel3: TDBEdit;
    DBEdNivel2: TDBEdit;
    Label15: TLabel;
    DBEdNivel1: TDBEdit;
    Label16: TLabel;
    QrGraGru2: TmySQLQuery;
    DsGraGru2: TDataSource;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru2Nivel2: TIntegerField;
    QrGraGru2Nome: TWideStringField;
    QrGraGru2CodUsu: TIntegerField;
    QrPrdGrupTipGradeado: TSmallintField;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv4: TWideStringField;
    QrPrdGrupTipTitNiv5: TWideStringField;
    QrGraGru2PrdGrupTip: TIntegerField;
    QrGraGru2Nivel3: TIntegerField;
    QrGraGru2NOMENIVEL3: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1NOMENIVEL2: TWideStringField;
    EdCodUsu3: TdmkEdit;
    PMAltera: TPopupMenu;
    AlteraCdigodoNivel3: TMenuItem;
    AlteraCdigodoNivel2: TMenuItem;
    AlteraCdigodoNivel1: TMenuItem;
    PnMudaTipo: TPanel;
    Label1: TLabel;
    EdPrdGruTip: TdmkEditCB;
    CBPrdGruTip: TdmkDBLookupComboBox;
    PnNivel4: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    EdNome4: TdmkEdit;
    StaticText4: TStaticText;
    DBEdNivel4: TDBEdit;
    EdCodUsu4: TdmkEdit;
    QrGraGru4: TmySQLQuery;
    DsGraGru4: TDataSource;
    QrGraGru4PrdGrupTip: TIntegerField;
    QrGraGru4CodUsu: TIntegerField;
    QrGraGru4Nivel4: TIntegerField;
    QrGraGru4Nome: TWideStringField;
    QrGraGru4Nivel5: TIntegerField;
    QrGraGru4NOMENIVEL5: TWideStringField;
    QrGraGru3: TmySQLQuery;
    DsGraGru3: TDataSource;
    QrGraGru5PrdGrupTip: TIntegerField;
    QrGraGru5CodUsu: TIntegerField;
    QrGraGru5Nivel5: TIntegerField;
    QrGraGru5Nome: TWideStringField;
    QrGraGru5NOMEPGT: TWideStringField;
    PnNivel5: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdNome5: TdmkEdit;
    StaticText5: TStaticText;
    DBEdNivel5: TDBEdit;
    EdCodUsu5: TdmkEdit;
    QrGraGru3PrdGrupTip: TIntegerField;
    QrGraGru3CodUsu: TIntegerField;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru3Nivel4: TIntegerField;
    QrGraGru3NOMENIVEL4: TWideStringField;
    AlteraCdigodoNivel4: TMenuItem;
    AlteraCdigodoNivel5: TMenuItem;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtAltera: TBitBtn;
    Label17: TLabel;
    Label18: TLabel;
    BtSaida: TBitBtn;
    PnGraTabApp: TPanel;
    Label25: TLabel;
    EdGraTabApp: TdmkEdit;
    EdGraTabApp_TXT: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCodUsu3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure EdNome3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodUsu2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNome2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodUsu1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNome1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodUsu3Change(Sender: TObject);
    procedure EdCodUsu2Change(Sender: TObject);
    procedure EdCodUsu1Change(Sender: TObject);
    procedure QrPrdGrupTipAfterScroll(DataSet: TDataSet);
    procedure EdCodUsu2Exit(Sender: TObject);
    procedure EdCodUsu2Enter(Sender: TObject);
    procedure EdCodUsu1Enter(Sender: TObject);
    procedure EdCodUsu1Exit(Sender: TObject);
    procedure EdCodUsu3Enter(Sender: TObject);
    procedure EdCodUsu3Exit(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure AlteraCdigodoNivel1Click(Sender: TObject);
    procedure AlteraCdigodoNivel2Click(Sender: TObject);
    procedure AlteraCdigodoNivel3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdCodUsu5Change(Sender: TObject);
    procedure EdCodUsu5Enter(Sender: TObject);
    procedure EdCodUsu5Exit(Sender: TObject);
    procedure EdCodUsu5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNome5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNome4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodUsu4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodUsu4Exit(Sender: TObject);
    procedure EdCodUsu4Enter(Sender: TObject);
    procedure EdCodUsu4Change(Sender: TObject);
    procedure AlteraCdigodoNivel4Click(Sender: TObject);
    procedure AlteraCdigodoNivel5Click(Sender: TObject);
  private
    { Private declarations }
    FHeiIniA, FHeiIniB, FHeiIniN: Integer;
    FClicked: Boolean;
    FNivel5Ant, FNivel4Ant, FNivel3Ant, FNivel2Ant, FNivel1Ant, FNivel1_Nome: String;
    function  VerificaNivel5(Definitivo: Boolean; var Msg: String): Boolean;
    function  VerificaNivel4(Definitivo: Boolean; var Msg: String): Boolean;
    function  VerificaNivel3(Definitivo: Boolean; var Msg: String): Boolean;
    function  VerificaNivel2(Definitivo: Boolean; var Msg: String): Boolean;
    function  VerificaNivel1(Definitivo: Boolean; var Msg: String): Boolean;
    function ObtemNovoCodUsu(var CodUsu: Integer): Boolean;
    procedure KeyPressed(Key: Word; Shift: TShiftState;
              dmkEd, dmkEdFocus: TdmkEdit; dmkCB: TdmkDBLookupCombobox);
  public
    { Public declarations }
    FNivel1, FNivel2, FNivel3, FNivel4, FNivel5: Integer;
    function ConfirmaCadastro(MostraForm: Boolean; var Msg: String): Boolean;
    function AtivaOForm(MostraJanela: Boolean; var Msg: String): Boolean;
  end;

  var
  FmPrdGruNew: TFmPrdGruNew;

implementation

uses UnMyObjects, Module, UMySQLModule, UnMySQLCuringa, GraGruN, GetValor,
  ModProd, DmkDAC_PF, MyListas, ModAppGraG1, UnGrade_PF;

{$R *.DFM}

procedure TFmPrdGruNew.AlteraCdigodoNivel1Click(Sender: TObject);
(*
var
  CodUsu, Nivel1: Integer;
*)
begin
  (*
  Nivel1 := QrGraGru1Nivel1.Value;
  if ObtemNovoCodUsu(CodUsu) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
    'CodUsu'], ['Nivel1'], [CodUsu], [Nivel1], True) then
    begin
      EdCodUsu1.ValueVariant := CodUsu;
    end;
  end;
  *)
end;

procedure TFmPrdGruNew.AlteraCdigodoNivel2Click(Sender: TObject);
begin
//
end;

procedure TFmPrdGruNew.AlteraCdigodoNivel3Click(Sender: TObject);
begin
//
end;

procedure TFmPrdGruNew.AlteraCdigodoNivel4Click(Sender: TObject);
begin
//
end;

procedure TFmPrdGruNew.AlteraCdigodoNivel5Click(Sender: TObject);
begin
//
end;

procedure TFmPrdGruNew.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmPrdGruNew.BtOKClick(Sender: TObject);
var
  Msg: String;
begin
  if ConfirmaCadastro(True, Msg) then
    Close
  else
    Geral.MB_Aviso(Msg);
end;

function TFmPrdGruNew.ConfirmaCadastro(MostraForm: Boolean; var Msg: String): Boolean;
var
  PrdGruTip, CodUsu1, CodUsu2, CodUsu3, CodUsu4, CodUsu5, Extra, Controle,
  GraGruX, GraTabApp: Integer;
  Nome1, Nome2, Nome3, Nome4, Nome5: String;
  SQLType: TSQLType;
  ResNiv: Boolean;
begin
  //N�o mostrar mensagens utilizar a var MSG para isso
  //Motivo: Esta fun��o � chamada no ModProd
  //
  Result := False;
  Msg    := '';
  //
  if PnGraTabApp.Visible then
    GraTabApp := EdGraTabApp.ValueVariant
  else
    GraTabApp := 0;
  //
  if EdNome1.Text = '' then
  begin
    if FNivel1_Nome <> '' then
      EdNome1.Text := FNivel1_Nome
  end;
  PrdGruTip := 0;
  if (QrPrdGrupTipNivCad.Value < 1) and (QrPrdGrupTipNivCad.Value > 5) then
  begin
    Msg                := 'N�vel de cadastro fora do esperado!';
    Height             := FHeiIniB;
    PnNiveis.Visible   := False;
    PnMudaTipo.Enabled := True;
    Exit;
  end;
  if PnMudaTipo.Enabled then
  begin
    PnNivel5.Visible   := QrPrdGrupTipNivCad.Value >= 5;
    PnNivel4.Visible   := QrPrdGrupTipNivCad.Value >= 4;
    PnNivel3.Visible   := QrPrdGrupTipNivCad.Value >= 3;
    PnNivel2.Visible   := QrPrdGrupTipNivCad.Value >= 2;
    PnNivel1.Visible   := QrPrdGrupTipNivCad.Value >= 1;
    Extra              := QrPrdGrupTipNivCad.Value * FHeiIniN;
    PnMudaTipo.Enabled := False;
    PnNiveis.Visible   := True;
    Height             := FHeiIniB + Extra;
    Top                := Top - Trunc(Extra / 2);
    //
    if MostraForm then
    begin
      case QrPrdGrupTipNivCad.Value of
        1: EdCodUsu1.SetFocus;
        2: EdCodUsu2.SetFocus;
        3: EdCodUsu3.SetFocus;
        4: EdCodUsu4.SetFocus;
        5: EdCodUsu5.SetFocus;
      end;
      Exit;
    end else
    begin
      Msg    := '';
      ResNiv := False;
      //
      case QrPrdGrupTipNivCad.Value of
        1: ResNiv := VerificaNivel1(True, Msg);
        2: ResNiv := VerificaNivel2(True, Msg);
        3: ResNiv := VerificaNivel3(True, Msg);
        4: ResNiv := VerificaNivel4(True, Msg);
        5: ResNiv := VerificaNivel5(True, Msg);
      end;
      if not ResNiv then
        Exit;
    end;
  end else begin
    FNivel1 := 0;
    FNivel2 := 0;
    FNivel3 := 0;
    FNivel4 := 0;
    FNivel5 := 0;
    //
    if not PnNivel5.Visible then
    begin
      CodUsu5 := 0;
      Nome5   := '';
    end else
    begin
      CodUsu5 := EdCodUsu5.ValueVariant;
      Nome5   := EdNome5.ValueVariant;
    end;
    if not PnNivel4.Visible then
    begin
      CodUsu4 := 0;
      Nome4   := '';
    end else
    begin
      CodUsu4 := EdCodUsu4.ValueVariant;
      Nome4   := EdNome4.ValueVariant;
    end;
    if not PnNivel3.Visible then
    begin
      CodUsu3 := 0;
      Nome3   := '';
    end else
    begin
      CodUsu3 := EdCodUsu3.ValueVariant;
      Nome3   := EdNome3.ValueVariant;
    end;
    if not PnNivel2.Visible then
    begin
      CodUsu2 := 0;
      Nome2   := '';
    end else
    begin
      CodUsu2 := EdCodUsu2.ValueVariant;
      Nome2   := EdNome2.ValueVariant;
    end;
    if not PnNivel1.Visible then
    begin
      CodUsu1 := 0;
      Nome1   := '';
    end else
    begin
      CodUsu1 := EdCodUsu1.ValueVariant;
      Nome1   := EdNome1.ValueVariant;
    end;
    //
    if PnNivel5.Visible then
    begin
      if CodUsu5 = 0 then
      begin
        Msg := 'Informe a refer�ncia do n�vel 5!';
        Exit;
      end;
      if Nome5 = '' then
      begin
        Msg := 'Informe uma descri��o para o n�vel 5!';
        Exit;
      end;
    end;
    if PnNivel4.Visible then
    begin
      if CodUsu4 = 0 then
      begin
        Msg := 'Informe a refer�ncia do n�vel 4!';
        Exit;
      end;
      if Nome4 = '' then
      begin
        Msg := 'Informe uma descri��o para o n�vel 4!';
        Exit;
      end;
    end;
    if PnNivel3.Visible then
    begin
      if CodUsu3 = 0 then
      begin
        Msg := 'Informe a refer�ncia do n�vel 3!';
        Exit;
      end;
      if Nome3 = '' then
      begin
        Msg := 'Informe uma descri��o para o n�vel 3!';
        Exit;
      end;
    end;
    if PnNivel2.Visible then
    begin
      if CodUsu2 = 0 then
      begin
        Msg := 'Informe a refer�ncia do n�vel 2!';
        Exit;
      end;
      if Nome2 = '' then
      begin
        Msg := 'Informe uma descri��o para o n�vel 2!';
        Exit;
      end;
    end;
    if PnNivel1.Visible then
    begin
      if CodUsu1 = 0 then
      begin
        Msg := 'Informe a refer�ncia do n�vel 1!';
        Exit;
      end;
      if Nome1 = '' then
      begin
        Msg := 'Informe uma descri��o para o n�vel 1!';
        Exit;
      end;
    end;
    //
    UMyMod.ObtemCodigoDeCodUsu(EdPrdGruTip, PrdGruTip, '');
    //
    if PnNivel5.Visible then
    begin
      if QrGraGru5CodUsu.Value = CodUsu5 then
        SQLType := stUpd
      else
        SQLType := stIns;
      //
      QrGraGru5.Close;
      QrGraGru5.Params[0].AsInteger := CodUsu5;
      UnDmkDAC_PF.AbreQuery(QrGraGru5, Dmod.MyDB);
      //
      (*
      FNivel5 := UMyMod.BuscaEmLivreY_Def('gragru5', 'Nivel5', SQLType,
        QrGraGru5Nivel5.Value);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru5', False,
        ['CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel5'],
        [CodUsu5, EdNome5.Text, PrdGruTip], [FNivel5], True);
        *)
      FNivel5 := Grade_PF.InsUpdProdNivel5(SQLType, PrdGruTip,
                   QrGraGru5Nivel5.Value, CodUsu5, EdNome5.Text);
      //
      if FNivel5 = 0 then
      begin
        Geral.MB_Erro('Falha ao atualizar o n�vel 5!');
        Exit;
      end;
    end;
    //
    if PnNivel4.Visible then
    begin
      if QrGraGru4CodUsu.Value = CodUsu4 then
        SQLType := stUpd
      else
        SQLType := stIns;
      //
      QrGraGru4.Close;
      QrGraGru4.Params[0].AsInteger := CodUsu4;
      UnDmkDAC_PF.AbreQuery(QrGraGru4, Dmod.MyDB);
      //
      (*
      FNivel4 := UMyMod.BuscaEmLivreY_Def('gragru4', 'Nivel4', SQLType,
        QrGraGru4Nivel4.Value);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru4', False,
        ['Nivel5', 'CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel4'],
        [FNivel5, CodUsu4, EdNome4.Text, PrdGruTip], [FNivel4], True);
      *)
      FNivel4 := Grade_PF.InsUpdProdNivel4(SQLType, PrdGruTip, FNivel5,
                   QrGraGru4Nivel4.Value, CodUsu4, EdNome4.Text);
      //
      if FNivel4 = 0 then
      begin
        Geral.MB_Erro('Falha ao atualizar o n�vel 4!');
        Exit;
      end;
    end;
    //
    if PnNivel3.Visible then
    begin
      if QrGraGru3CodUsu.Value = CodUsu3 then
        SQLType := stUpd
      else
        SQLType := stIns;
      //
      QrGraGru3.Close;
      QrGraGru3.Params[0].AsInteger := CodUsu3;
      UnDmkDAC_PF.AbreQuery(QrGraGru3, Dmod.MyDB);
      //
      (*
      FNivel3 := UMyMod.BuscaEmLivreY_Def('gragru3', 'Nivel3', SQLType,
        QrGraGru3Nivel3.Value);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru3', False,
        ['Nivel5', 'Nivel4', 'CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel3'],
        [FNivel5, FNivel4, CodUsu3, EdNome3.Text, PrdGruTip], [FNivel3], True);
      *)
      FNivel3 := Grade_PF.InsUpdProdNivel3(SQLType, PrdGruTip, FNivel5, FNivel4,
                   QrGraGru3Nivel3.Value, CodUsu3, EdNome3.Text);
      //
      if FNivel3 = 0 then
      begin
        Geral.MB_Erro('Falha ao atualizar o n�vel 3!');
        Exit;
      end;
    end;
    //
    if PnNivel2.Visible then
    begin
      if QrGraGru2CodUsu.Value = CodUsu2 then
        SQLType := stUpd
      else
        SQLType := stIns;
      //
      QrGraGru2.Close;
      QrGraGru2.Params[0].AsInteger := CodUsu2;
      UnDmkDAC_PF.AbreQuery(QrGraGru2, Dmod.MyDB);
      //
      (*
      FNivel2 := UMyMod.BuscaEmLivreY_Def('gragru2', 'Nivel2', SQLType,
        QrGraGru2Nivel2.Value);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru2', False,
        ['Nivel5', 'Nivel4', 'Nivel3', 'CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel2'],
        [FNivel5, FNivel4, FNivel3, CodUsu2, EdNome2.Text, PrdGruTip], [FNivel2], True);
      *)
      FNivel2 := Grade_PF.InsUpdProdNivel2(SQLType, PrdGruTip, FNivel5, FNivel4,
                   FNivel3, QrGraGru2Nivel2.Value, CodUsu2, EdNome2.Text);
      //
      if FNivel2 = 0 then
      begin
        Geral.MB_Erro('Falha ao atualizar o n�vel 2!');
        Exit;
      end;
    end;
    //
    if PnNivel1.Visible then
    begin
      if EdNome1.Text = '' then
      begin
        Msg := 'Defina o nome do "' + StaticText1.Caption + '"';
        Exit;
      end;
      if QrGraGru1CodUsu.Value = CodUsu1 then
        SQLType := stUpd
      else
        SQLType := stIns;
      //
      QrGraGru1.Close;
      QrGraGru1.Params[0].AsInteger := CodUsu1;
      UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
      //
      (*
      FNivel1 := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', SQLType,
        QrGraGru1Nivel1.Value);
      //
      //PrdGruTip := Geral.IMV(EdPrdGruTip.Text);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru1', False, [
        'Nivel5', 'Nivel4', 'Nivel3',
        'Nivel2', 'CodUsu', 'Nome', 'PrdGrupTip',
        'GraTabApp'
      ], ['Nivel1'], [
        FNivel5, FNivel4, FNivel3,
        FNivel2, CodUsu1, EdNome1.Text, PrdGruTip,
        GraTabApp
      ], [FNivel1], True) then
      begin
        if (SQLType = stIns) and (QrPrdGrupTipGradeado.Value = 0) then
        begin
          DmProd.ReopenOpcoesGrad;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, ['GraTamCad'],
            ['Nivel1'], [DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger], [FNivel1], True) then
          begin
            Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', stIns, 0);
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, ['GraCorCad',
              'Nivel1'], ['Controle'], [DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger,
              FNivel1], [Controle], True) then
            begin
              GraGruX := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                'GraGruX', 'GraGruX', 'Controle');
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, ['Ativo',
                'GraGruC', 'GraGru1', 'GraTamI'], ['Controle'], [1, Controle,
                FNivel1, DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger],
                [GraGruX], True);
            end;
          end;
        end;
      end;
      *)
      FNivel1 := Grade_PF.InsUpdProdNivel1(SQLType, GraTabApp, PrdGruTip,
                   QrPrdGrupTipGradeado.Value, FNivel5, FNivel4, FNivel3,
                   FNivel2, QrGraGru1Nivel1.Value, CodUsu1, EdNome1.Text);

      //
      if FNivel1 = 0 then
      begin
        Geral.MB_Erro('Falha ao atualizar o n�vel 1!');
        Exit;
      end;
      //
      if DmProd.FFmGraGruNew and (DmProd.FChamouGraGruNew = cggnFmGraGruN) then
      begin
        FmGraGruN.ReopenGraGruN(PrdGruTip);
        FmGraGruN.ReopenGraGru5(FNivel3, True);
        FmGraGruN.ReopenGraGru4(FNivel4, True);
        FmGraGruN.ReopenGraGru3(FNivel3, True);
        FmGraGruN.ReopenGraGru2(FNivel2, True);
        FmGraGruN.ReopenGraGru1(FNivel1, True);
      end;
    end;
    if DmProd.FChamouGraGruNew = cggnFmGraGruN then
      FmGraGruN.FNewPrdGrupTip := PrdGruTip;
    //
    DmProd.FNewGraGru1      := FNivel1;
    DmProd.FNewGraGru2      := FNivel2;
    DmProd.FNewGraGru3      := FNivel3;
    DmProd.FNewGraGru4      := FNivel4;
    DmProd.FNewGraGru5      := FNivel5;
    DmProd.FPrdGrupTip_Nome := '';
    //
    case TdmkAppID(CO_DMKID_APP) of
      dmkappB_U_G_S_T_R_O_L:
      begin
        if (GraTabApp <> 0) and (FNivel1 <> 0) and (Nome1 <> '') then
        begin
          DfModAppGraG1.AlteraProdutoTabelaPersonalizada(GraTabApp, FNivel1,
            Nome1, True);
        end;
      end;
    end;
    Result := True;
  end;
end;

procedure TFmPrdGruNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdGruNew.EdCodUsu4Change(Sender: TObject);
var
  Msg: String;
  Definitivo: Boolean;
begin
  EdNome4.Text := '';
  Definitivo   := (not EdCodUsu4.Focused) and (DmProd.FNewGraGru4 = 0);
  //
  if not VerificaNivel4(Definitivo, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    EdCodUsu4.SetFocus;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu4Enter(Sender: TObject);
begin
  FNivel4Ant := EdCodUsu4.Text;
end;

procedure TFmPrdGruNew.EdCodUsu4Exit(Sender: TObject);
var
  Msg: String;
begin
  if FNivel4Ant <> EdCodUsu4.Text then
  begin
    if not VerificaNivel4(True, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      EdCodUsu4.SetFocus;
    end;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu4, EdNome4, nil);
end;

procedure TFmPrdGruNew.EdCodUsu1Change(Sender: TObject);
var
  Msg: String;
  Definitivo: Boolean;
begin
  //EdNome1.Text := '';  n�o pode por causa do pq
  Definitivo := not EdCodUsu1.Focused;
  if not VerificaNivel1(Definitivo, Msg) then
  begin
    if Geral.MB_Pergunta(Msg) <> ID_YES then
    begin
      EdCodUsu1.ValueVariant := 0;
      EdNome1.Text := '';
      EdCodUsu1.SetFocus;
    end;
  end;
  if (ImgTipo.SQLType = stIns) and (DmProd.FFmGraGruNew = False)
  and (DmProd.FPrdGrupTip > 0) and (DmProd.FPrdGrupTip_Nome <> '')
  and (DmProd.FChamouGraGruNew = cggnFmGraGruN) then
  begin
    FmGraGruN.FLaTipoGrupo := stUpd;
    EdNome1.Text := DmProd.FPrdGrupTip_Nome;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu1Enter(Sender: TObject);
begin
  FNivel1Ant := EdCodUsu1.Text;
end;

procedure TFmPrdGruNew.EdCodUsu1Exit(Sender: TObject);
var
  Msg: String;
begin
  if FNivel1Ant <> EdCodUsu1.Text then
  begin
    if not VerificaNivel1(True, Msg) then
    begin
      if Geral.MB_Pergunta(Msg) <> ID_YES then
      begin
        EdCodUsu1.ValueVariant := 0;
        EdNome1.Text := '';
        EdCodUsu1.SetFocus;
      end;
    end;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu1, EdNome1, nil);
end;

procedure TFmPrdGruNew.EdCodUsu2Change(Sender: TObject);
var
  Msg: String;
  Definitivo: Boolean;
begin
  EdNome2.Text := '';
  Definitivo   := (not EdCodUsu2.Focused) and (DmProd.FNewGraGru2 = 0);
  //
  if not VerificaNivel2(Definitivo, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    EdCodUsu2.SetFocus;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu2Enter(Sender: TObject);
begin
  FNivel2Ant := EdCodUsu2.Text;
end;

procedure TFmPrdGruNew.EdCodUsu2Exit(Sender: TObject);
var
  Msg: String;
begin
  if FNivel2Ant <> EdCodUsu2.Text then
  begin
    if not VerificaNivel2(True, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      EdCodUsu2.SetFocus;
    end;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu2, EdNome2, nil);
end;

procedure TFmPrdGruNew.EdCodUsu3Change(Sender: TObject);
var
  Msg: String;
  Definitivo: Boolean;
begin
  EdNome3.Text := '';
  Definitivo   := (not EdCodUsu3.Focused) and (DmProd.FNewGraGru3 = 0);
  //
  if not VerificaNivel3(Definitivo, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    EdCodUsu3.SetFocus;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu3Enter(Sender: TObject);
begin
  FNivel3Ant := EdCodUsu3.Text;
end;

procedure TFmPrdGruNew.EdCodUsu3Exit(Sender: TObject);
var
  Msg: String;
begin
  if FNivel3Ant <> EdCodUsu3.Text then
  begin
    if not VerificaNivel3(True, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      EdCodUsu3.SetFocus;
    end;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu3, EdNome3, nil);
end;

procedure TFmPrdGruNew.EdCodUsu5Change(Sender: TObject);
var
  Definitivo: Boolean;
  Msg: String;
begin
  EdNome5.Text := '';
  Definitivo   := (not EdCodUsu5.Focused) and (DmProd.FNewGraGru5 = 0);
  //
  if not VerificaNivel5(Definitivo, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    EdCodUsu5.SetFocus;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu5Enter(Sender: TObject);
begin
  FNivel5Ant := EdCodUsu5.Text;
end;

procedure TFmPrdGruNew.EdCodUsu5Exit(Sender: TObject);
var
  Msg: String;
begin
  if FNivel5Ant <> EdCodUsu5.Text then
  begin
    if not VerificaNivel5(True, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      EdCodUsu5.SetFocus;
    end;
  end;
end;

procedure TFmPrdGruNew.EdCodUsu5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu5, EdNome5, nil);
end;

procedure TFmPrdGruNew.EdNome1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu1, EdNome1, nil);
end;

procedure TFmPrdGruNew.EdNome2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu2, EdNome2, nil);
end;

procedure TFmPrdGruNew.EdNome3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu3, EdNome3, nil);
end;

procedure TFmPrdGruNew.EdNome4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu4, EdNome4, nil);
end;

procedure TFmPrdGruNew.EdNome5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu5, EdNome5, nil);
end;

procedure TFmPrdGruNew.FormActivate(Sender: TObject);
var
  Msg: String;
begin
  MyObjects.CorIniComponente();
  //
  if not AtivaOForm(DmProd.FFmGraGruNew, Msg) then
    Geral.MB_Aviso(Msg);
end;

function TFmPrdGruNew.AtivaOForm(MostraJanela: Boolean; var Msg: String): Boolean;
begin
  Result := False;
  Msg    := '';
  //
  if ImgTipo.SQLType = stIns then
  begin
    if EdPrdGruTip.ValueVariant = 0 then
    begin
      if DmProd.FFmGraGruNew and (DmProd.FChamouGraGruNew = cggnFmGraGruN) then
      begin
        EdPrdGruTip.ValueVariant := FmGraGruN.QrGraGruNCodUsu.Value;
        CBPrdGruTip.KeyValue     := FmGraGruN.QrGraGruNCodUsu.Value;
      end else
      begin
        if QrPrdGrupTip.Locate('Codigo', DmProd.FPrdGrupTip, []) then
        begin
          EdPrdGruTip.ValueVariant := QrPrdGrupTipCodUsu.Value;
          CBPrdGruTip.KeyValue     := QrPrdGrupTipCodUsu.Value;
          //
          EdNome1.Text := DmProd.FPrdgrupTip_Nome;
        end else
          Msg := 'N�o foi poss�vel setar o Tipo de Grupo de Produtos!';
      end;
    end;
  end;
  if DmProd.FGraTabAppMostra then
  begin
    PnGraTabApp.Visible          := True;
    EdGraTabApp.ValueVariant     := DmProd.FGraTabApp;
    EdGraTabApp_Txt.ValueVariant := DmProd.FGraTabApp_Nome;
  end else
    PnGraTabApp.Visible := False;
  //
  EdCodUsu5.ValueVariant := DmProd.FNewGraGru5;
  EdCodUsu4.ValueVariant := DmProd.FNewGraGru4;
  EdCodUsu3.ValueVariant := DmProd.FNewGraGru3;
  EdCodUsu2.ValueVariant := DmProd.FNewGraGru2;
  EdCodUsu1.ValueVariant := DmProd.FNewGraGru1;
  //
  DmProd.FNewGraGru1 := 0;
  DmProd.FNewGraGru2 := 0;
  DmProd.FNewGraGru3 := 0;
  DmProd.FNewGraGru4 := 0;
  DmProd.FNewGraGru5 := 0;
  //
  if FClicked = False then
  begin
    if ConfirmaCadastro(MostraJanela, Msg) then
      FClicked := True;
  end;
  try
    if DmProd.FPrdGrupTip_Nome <> '' then
    begin
      EdNome1.Text := DmProd.FPrdGrupTip_Nome;
      FNivel1_Nome := DmProd.FPrdGrupTip_Nome;
      DmProd.FPrdGrupTip_Nome := '';
    end;
  except
    //
  end;
  if (EdCodUsu2.ValueVariant <> 0) and ((EdCodUsu1.ValueVariant = 0) and (PnNivel1.Visible = True)) then
  begin
    if MostraJanela then
      EdCodUsu1.SetFocus;
  end else
  if (EdCodUsu3.ValueVariant <> 0) and ((EdCodUsu2.ValueVariant = 0) and (PnNivel2.Visible = True)) then
  begin
    if MostraJanela then
      EdCodUsu2.SetFocus;
  end else
  if (EdCodUsu4.ValueVariant <> 0) and ((EdCodUsu3.ValueVariant = 0) and (PnNivel3.Visible = True)) then
  begin
    if MostraJanela then
      EdCodUsu3.SetFocus;
  end else
  if (EdCodUsu5.ValueVariant <> 0) and ((EdCodUsu4.ValueVariant = 0) and (PnNivel4.Visible = True)) then
  begin
    if MostraJanela then
      EdCodUsu4.SetFocus;
  end;
  Result := True;
end;

procedure TFmPrdGruNew.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Evitar engano em nova inclus�o
  DmProd.FPrdGrupTip := 0;
  DmProd.FPrdGrupTip_Nome := '';
  // N�o podem ser zerados, pois v�o ser usados
  //FNewGraGru1 := 0;
  //FNewGraGru2 := 0;
  //FNewGraGru3 := 0;
end;

procedure TFmPrdGruNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FNivel1_Nome       := '';
  FHeiIniA           := FmPrdGruNew.Height;
  FHeiIniB           := FmPrdGruNew.Height - PnNiveis.Height;
  FHeiIniN           := PnNivel1.Height;
  FmPrdGruNew.Height := FHeiIniB;
  PnMudaTipo.Enabled := DmProd.FEnabledPnTipo;
  //
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  if DmProd.FChamouGraGruNew = cggnFmGraGruN then
  begin
    try
      FmGraGruN.FLaTipoGrupo := stIns;
    except
    ;// Nada
    end;
  end;  
end;

procedure TFmPrdGruNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdGruNew.KeyPressed(Key: Word; Shift: TShiftState;
  dmkEd, dmkEdFocus: TdmkEdit; dmkCB: TdmkDBLookupCombobox);
var
  Valor, PrdGrupTip: Integer;
  Nivel, Extra: String;
begin
  if Key in ([VK_F3,VK_F4]) then
  begin
    Nivel := dmkEd.Name[Length(dmkEd.Name)];
    //
    if Key = VK_F4 then
    begin
      Valor := DmProd.GeraNovoCodigoNivel(Geral.IMV(Nivel),
                 QrPrdGrupTipCodigo.Value, QrPrdGrupTipNivCad.Value);
      if Valor <> 0 then
      begin
        dmkEd.ValueVariant := Valor;
        dmkEdFocus.SetFocus;
      end;
    end else
    if Key = VK_F3 then
    begin
      if EdPrdGruTip.ValueVariant <> 0 then
      begin
        PrdGrupTip := QrPrdGrupTipCodigo.Value;
        Extra      := 'AND PrdGrupTip=' + Geral.FF0(PrdGrupTip);
      end else
        Extra := '';
      //
      CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraGru'+Nivel, Dmod.MyDB,
        Extra, dmkEd, nil, dmktfInteger, True);
    end;
  end;
end;

function TFmPrdGruNew.ObtemNovoCodUsu(var CodUsu: Integer): Boolean;
var
  ResVar: Variant;
begin
  CodUsu := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Grupo de Produtos', 'Informe o novo c�digo: ',
  0, ResVar) then
  begin
    CodUsu := Geral.IMV(ResVar);
    Result := True;
  end;
end;

procedure TFmPrdGruNew.PMAlteraPopup(Sender: TObject);
begin
  AlteraCdigodoNivel1.Enabled := (QrGraGru1.State <> dsInactive) and (QrGraGru1.RecordCount > 0);
  AlteraCdigodoNivel2.Enabled := (QrGraGru2.State <> dsInactive) and (QrGraGru2.RecordCount > 0);
  AlteraCdigodoNivel3.Enabled := (QrGraGru3.State <> dsInactive) and (QrGraGru3.RecordCount > 0);
  AlteraCdigodoNivel4.Enabled := (QrGraGru4.State <> dsInactive) and (QrGraGru4.RecordCount > 0);
  AlteraCdigodoNivel5.Enabled := (QrGraGru5.State <> dsInactive) and (QrGraGru5.RecordCount > 0);
end;

procedure TFmPrdGruNew.QrPrdGrupTipAfterScroll(DataSet: TDataSet);
const
  Txt = 'Altera C�digo de: ';
begin
  StaticText1.Caption := QrPrdGrupTipTitNiv1.Value;
  StaticText2.Caption := QrPrdGrupTipTitNiv2.Value;
  StaticText3.Caption := QrPrdGrupTipTitNiv3.Value;
  StaticText4.Caption := QrPrdGrupTipTitNiv4.Value;
  StaticText5.Caption := QrPrdGrupTipTitNiv5.Value;
  //
  AlteraCdigodoNivel1.Caption := Txt + StaticText1.Caption;
  AlteraCdigodoNivel2.Caption := Txt + StaticText2.Caption;
  AlteraCdigodoNivel3.Caption := Txt + StaticText3.Caption;
  AlteraCdigodoNivel4.Caption := Txt + StaticText4.Caption;
  AlteraCdigodoNivel5.Caption := Txt + StaticText5.Caption;
end;

function TFmPrdGruNew.VerificaNivel1(Definitivo: Boolean; var Msg: String): Boolean;
var
  CodUsu, Nivel2: Integer;
begin
  Msg    := '';
  Result := False;
  //
  QrGraGru1.Close;
  CodUsu := Geral.IMV(EdCodUsu1.Text);
  if CodUsu <> 0 then
  begin
    QrGraGru1.Params[0].AsInteger := CodUsu;
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    //
    if QrGraGru1.RecordCount > 0 then
    begin
      if Definitivo then
      begin
        if PnNivel2.Visible then
          Nivel2 := Geral.IMV(EdCodUsu2.Text)
        else
          Nivel2 := 0;
        if QrGraGru1Nivel2.Value <> Nivel2 then
        begin
          Msg := 'O cadastro ' + FormatFloat('0', QrGraGru1CodUsu.Value) +
                 ' - ' + QrGraGru1Nome.Value + ' j� pertence ao ' +
                 StaticText2.Caption + ' ' + FormatFloat('0',
                 QrGraGru1Nivel2.Value) + ' "' + QrGraGru1NOMENIVEL2.Value + '"!' +
                 ' Deseja realmente alter�-lo?';
          Exit;
        end else
          EdNome1.Text := QrGraGru1Nome.Value;
      end else
        EdNome1.Text := QrGraGru1Nome.Value;
    end else
      EdNome1.Text := '';
  end;
  Result := True;
end;

function TFmPrdGruNew.VerificaNivel2(Definitivo: Boolean; var Msg: String): Boolean;
var
  CodUsu, Nivel3: Integer;
begin
  Msg    := '';
  Result := False;
  //
  QrGraGru2.Close;
  CodUsu := Geral.IMV(EdCodUsu2.Text);
  if CodUsu <> 0 then
  begin
    QrGraGru2.Params[0].AsInteger := CodUsu;
    UnDmkDAC_PF.AbreQuery(QrGraGru2, Dmod.MyDB);
    //
    if QrGraGru2.RecordCount > 0 then
    begin
      if Definitivo then
      begin
        if PnNivel3.Visible then
          Nivel3 := Geral.IMV(EdCodUsu3.Text)
        else
          Nivel3 := 0;
        if QrGraGru2Nivel3.Value <> Nivel3 then
        begin
          Msg := 'O cadastro ' + Geral.FF0(QrGraGru2CodUsu.Value) +
                 ' - ' + QrGraGru2Nome.Value + ' j� pertence ao ' +
                 StaticText3.Caption + ' ' + Geral.FF0(QrGraGru2Nivel3.Value) + ' "'
                 + QrGraGru2NOMENIVEL3.Value + '"!';
          //
          EdCodUsu2.ValueVariant := 0;
          EdNome2.Text := '';
          Exit;
        end else
          EdNome2.Text := QrGraGru2Nome.Value;
      end else
        EdNome2.Text := QrGraGru2Nome.Value;
    end else
      EdNome2.Text := '';
  end;
  if DmProd.FFmGraGruNew then
    EdCodUsu1.ValueVariant := 0;
  Result                 := True;
  // N�o por causa do PQ
  //EdNome1.Text           := '';
end;

function TFmPrdGruNew.VerificaNivel3(Definitivo: Boolean; var Msg: String): Boolean;
var
  CodUsu, Nivel4: Integer;
begin
  Msg    := '';
  Result := False;
  //
  QrGraGru3.Close;
  CodUsu := Geral.IMV(EdCodUsu3.Text);
  if CodUsu <> 0 then
  begin
    QrGraGru3.Params[0].AsInteger := CodUsu;
    UnDmkDAC_PF.AbreQuery(QrGraGru3, Dmod.MyDB);
    //
    if QrGraGru3.RecordCount > 0 then
    begin
      if Definitivo then
      begin
        if PnNivel4.Visible then
          Nivel4 := Geral.IMV(EdCodUsu4.Text)
        else
          Nivel4 := 0;
        if QrGraGru3Nivel4.Value <> Nivel4 then
        begin
          Msg := 'O cadastro ' + Geral.FF0(QrGraGru3CodUsu.Value) +
                 ' - ' + QrGraGru3Nome.Value + ' j� pertence ao ' +
                 StaticText4.Caption + ' ' + Geral.FF0(QrGraGru3Nivel4.Value) +
                 ' "' + QrGraGru3NOMENIVEL4.Value + '"!';
          //
          EdCodUsu3.ValueVariant := 0;
          EdNome3.Text := '';
          Exit;
        end else
          EdNome3.Text := QrGraGru3Nome.Value;
      end else EdNome3.Text := QrGraGru3Nome.Value;
    end else
      EdNome3.Text := '';
  end;
  if DmProd.FFmGraGruNew then
    EdCodUsu1.ValueVariant := 0;
  Result                 := True;
  // N�o por causa do PQ
  //EdNome1.Text           := '';
end;

function TFmPrdGruNew.VerificaNivel4(Definitivo: Boolean; var Msg: String): Boolean;
var
  CodUsu, Nivel5: Integer;
begin
  Msg    := '';
  Result := False;
  //
  QrGraGru4.Close;
  CodUsu := Geral.IMV(EdCodUsu4.Text);
  //
  if CodUsu <> 0 then
  begin
    QrGraGru4.Params[0].AsInteger := CodUsu;
    UnDmkDAC_PF.AbreQuery(QrGraGru4, Dmod.MyDB);
    //
    if QrGraGru4.RecordCount > 0 then
    begin
      if Definitivo then
      begin
        if PnNivel5.Visible then
          Nivel5 := Geral.IMV(EdCodUsu5.Text)
        else
          Nivel5 := 0;
        if QrGraGru4Nivel5.Value <> Nivel5 then
        begin
          Msg := 'O cadastro ' + Geral.FF0(QrGraGru4CodUsu.Value) +
                 ' - ' + QrGraGru4Nome.Value + ' j� pertence ao ' +
                 StaticText5.Caption + ' ' + Geral.FF0(QrGraGru4Nivel5.Value) +
                 ' "' + QrGraGru4NOMENIVEL5.Value + '"!';
          //
          EdCodUsu4.ValueVariant := 0;
          EdNome4.Text := '';
          Exit;
        end else
          EdNome4.Text := QrGraGru4Nome.Value;
      end else
        EdNome4.Text := QrGraGru4Nome.Value;
    end else
      EdNome4.Text := '';
  end;
  if DmProd.FFmGraGruNew then
    EdCodUsu1.ValueVariant := 0;
  Result                 := True;
  // N�o por causa do PQ
  //EdNome1.Text           := '';
end;

function TFmPrdGruNew.VerificaNivel5(Definitivo: Boolean; var Msg: String): Boolean;
var
  CodUsu: Integer;
begin
  Msg    := '';
  Result := False;
  //
  QrGraGru5.Close;
  CodUsu := Geral.IMV(EdCodUsu5.Text);
  if CodUsu <> 0 then
  begin
    QrGraGru5.Params[0].AsInteger := CodUsu;
    UnDmkDAC_PF.AbreQuery(QrGraGru5, Dmod.MyDB);
    //
    if QrGraGru5.RecordCount > 0 then
    begin
      if Definitivo then
      begin
        if (QrGraGru5PrdGrupTip.Value > 0) and
        (QrGraGru5PrdGrupTip.Value <> QrPrdGrupTipCodigo.Value) then
        begin
          Msg := 'O cadastro ' + Geral.FF0(QrGraGru5CodUsu.Value) +
                 ' - ' + QrGraGru5Nome.Value + ' j� pertence ao Tipo de Grupo ' +
                 Geral.FF0(QrGraGru5PrdGrupTip.Value) + ' "' +
                 QrGraGru5NOMEPGT.Value + '"!';
          //
          EdCodUsu5.ValueVariant := 0;
          EdNome5.Text := '';
          Exit;
        end else
          EdNome5.Text := QrGraGru5Nome.Value;
      end else
        EdNome5.Text := QrGraGru5Nome.Value;
    end else
      EdNome5.Text := '';
  end;
  if DmProd.FFmGraGruNew then
    EdCodUsu1.ValueVariant := 0;
  Result                 := True;
  // N�o por causa do PQ
  //EdNome1.Text           := '';
end;

end.

