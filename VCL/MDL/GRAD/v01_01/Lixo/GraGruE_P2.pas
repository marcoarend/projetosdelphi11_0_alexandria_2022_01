unit GraGruE_P2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, dmkRadioGroup, dmkEditCB, dmkDBLookupComboBox, dmkGeral,
  Principal, Menus, Variants, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmGraGruE_P2 = class(TForm)
    DsEmbalagens: TDataSource;
    QrEmbalagens: TmySQLQuery;
    QrEmbalagensCodigo: TIntegerField;
    QrEmbalagensNome: TWideStringField;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    Panel1: TPanel;
    Memo1: TMemo;
    Panel3: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    SbEmbalagem: TSpeedButton;
    Label4: TLabel;
    SbGraGruX: TSpeedButton;
    CBEmbalagem: TdmkDBLookupComboBox;
    EdEmbalagem: TdmkEditCB;
    EdObservacao: TdmkEdit;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXNivel1: TIntegerField;
    PMGraGruX: TPopupMenu;
    CadastrosemGrade1: TMenuItem;
    CadastroComGrade1: TMenuItem;
    Usoeconsumo1: TMenuItem;
    Matriaprima1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    CkContinuar: TCheckBox;
    Panel2: TPanel;
    Label2: TLabel;
    Label5: TLabel;
    EdxProd: TdmkEdit;
    EdcProd: TdmkEdit;
    Label6: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    SbFornece: TSpeedButton;
    Label7: TLabel;
    EdCFOP: TdmkEdit;
    Label8: TLabel;
    EduCom: TdmkEdit;
    EdNCM: TdmkEdit;
    Label9: TLabel;
    QrGraGruECad: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbEmbalagemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure SbForneceClick(Sender: TObject);
    procedure Usoeconsumo1Click(Sender: TObject);
    procedure Matriaprima1Click(Sender: TObject);
    procedure CadastroComGrade1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    //FFormChamou,
    FNomeGGX: String;
    FGraGruX: Integer;
    // Dados do produto
    F_prod_cEAN,
    F_prod_NCM,
    F_prod_uCom,
    F_InfAdProd,
    F_ICMS_CST_A,
    F_ICMS_CST_B,
    F_IPI_CST,
    F_PIS_CST,
    F_COFINS_CST,
    F_prod_EXTIPI,
    F_IPI_cEnq,
    FTxtPesq: String;
    // FIM dados do produto
  end;

  var
  FmGraGruE_P2: TFmGraGruE_P2;

implementation

uses UnMyObjects, MyDBCheck, UMySQLModule, Module, UnInternalConsts, Entidade2,
  //{$IFDEF FmPQE} PQ, {$ENDIF}
  GraGruN, Embalagens, ModProd, PrdGruNewU;

{$R *.DFM}

procedure TFmGraGruE_P2.BtOKClick(Sender: TObject);
var
  cProd, Observacao, xProd, NCM, uCom: String;
  Nivel1, GraGruX, Fornece, Embalagem, CFOP, CFOP_Inn: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  cProd          := EdcProd.Text;
  Nivel1         := QrGraGruXNivel1.Value;
  GraGruX        := EdGraGruX.ValueVariant;
  Fornece        := EdFornece.ValueVariant;
  Embalagem      := EdEmbalagem.ValueVariant;
  Observacao     := EdObservacao.Text;
  xProd          := EdxProd.Text;
  NCM            := EdNCM.Text;
  CFOP           := EdCFOP.ValueVariant;
  uCom           := EduCom.Text;
  CFOP_Inn       := 0;

  //
  if MyObjects.FIC(Fornece = 0, nil, 'Fornecedor n�o informado!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o produto (Reduzido)!') then Exit;
  //if MyObjects.FIC(Embalagem = 0, EdEmbalagem, 'Informe a embalagem!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrueits', False, [
  'Embalagem', 'Observacao', 'CFOP_Inn'], [
  'cProd', 'Nivel1', 'GraGruX', 'Fornece', 'xProd', 'NCM', 'CFOP', 'uCom'], [
  Embalagem, Observacao, CFOP_Inn], [
  cProd, Nivel1, GraGruX, Fornece, xProd, NCM, CFOP, uCom], True) then
  begin
    FGraGruX := GraGruX;
    FNomeGGX := CBGraGruX.Text;
(*
    if FFormChamou = 'FmGraGruN' then
      FmGraGruN.ReopenGraGruEX(FGraGruX);
*)
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdObservacao.Text        := '';
      EdGraGruX.ValueVariant   := 0;
      CBGraGruX.KeyValue       := 0;
      EdEmbalagem.ValueVariant := 0;
      CBEmbalagem.KeyValue     := 0;
    end else
      Close;
  end;
end;

procedure TFmGraGruE_P2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruE_P2.CadastroComGrade1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdxProd.Text;
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
end;

procedure TFmGraGruE_P2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruE_P2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FTxtPesq := '';
  //
  FGraGruX := 0;
  FNomeGGX := '';
  UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  //
  ReopenGraGruX();
end;

procedure TFmGraGruE_P2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruE_P2.Matriaprima1Click(Sender: TObject);
begin
  DmProd.CadastraSemGrade(-1, EdxProd.Text, F_prod_cEAN, F_prod_NCM, F_prod_uCom,
  F_InfAdProd, F_ICMS_CST_A, F_ICMS_CST_B, F_IPI_CST, F_PIS_CST, F_COFINS_CST,
  F_prod_EXTIPI, F_IPI_cEnq, QrGraGruX, EdGraGruX, CBGraGruX);
end;

procedure TFmGraGruE_P2.ReopenGraGruX();
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle, CONCAT(gg1.Nome, " ",  ',
  '  IF(gti.PrintTam=1, gti.Nome, ""), " ",  ',
  '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR, ',
  'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM,  ',
  'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR,  ',
  'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN pqcli      pqc ON pqc.PQ=gg1.Nivel1 ',
  'WHERE gg1.PrdGrupTip > -3 ',
  'AND NOT (pqc.PQ IS NULL) ',
  'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle ',
  '']);
  //Geral.MB_SQL(Self, QrGraGrux);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle, CONCAT(gg1.Nome, " ",  ',
  '  IF(gti.PrintTam=1, gti.Nome, ""), " ",  ',
  '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR, ',
  'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM,  ',
  'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR,  ',
  'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  //'LEFT JOIN pqcli      pqc ON pqc.PQ=gg1.Nivel1 ',
  'WHERE gg1.PrdGrupTip > -3 ',
  //'AND NOT (pqc.PQ IS NULL) ',
  'AND ggx.Controle > 0 ',
  'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle ',
  '']);
end;

procedure TFmGraGruE_P2.SbEmbalagemClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrEmbalagens.Close;
      UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
      EdEmbalagem.ValueVariant := VAR_CADASTRO;
      CBEmbalagem.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmGraGruE_P2.SbForneceClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrFornece.Close;
      UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
      EdFornece.ValueVariant := VAR_CADASTRO;
      CBFornece.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmGraGruE_P2.SbGraGruXClick(Sender: TObject);
begin

(*
  VAR_CADASTRO := 0;
  {$IFDEF FmPQE}
  //Fazer assim por causa do PQ!
  if FFormChamou = 'FmPQE' then
  begin
    if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
    begin
      DmProd.FPrdGrupTip_Nome := EdxProd.Text;
      FmPQ.ShowModal;
      FmPQ.Destroy;
    end;
  end else begin
    if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
    begin
      DmProd.FPrdGrupTip_Nome := EdxProd.Text;
      FmGraGruN.ShowModal;
      FmGraGruN.Destroy;
    end;
  end;
  {$ELSE}
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdxProd.Text;
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
  {$ENDIF}
  //
  QrGraGruX.Close;
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdGraGruX.ValueVariant := VAR_CADASTRO;
    CBGraGruX.KeyValue     := VAR_CADASTRO;
  end;
*)
(*
  if DBCheck.CriaFm(TFmPrdGruNewU, FmPrdGruNewU, afmoNegarComAviso) then
  begin
    FmPrdGruNewU.ImgTipo.SQLType := stIns;
    FmPrdGruNewU.ShowModal;
    FmPrdGruNewU.Destroy;
  end;
*)
  MyObjects.MostraPopOnControlXY(PMGraGruX, SbGraGruX, 0, 0);
  // MyObjects.MostraPopUpDeBotaoObject(PMGraGruX, SbGraGruX, 21, 21);
end;

procedure TFmGraGruE_P2.Usoeconsumo1Click(Sender: TObject);
begin
  DmProd.CadastraSemGrade(-2, EdxProd.Text, F_prod_cEAN, F_prod_NCM, F_prod_uCom,
  F_InfAdProd, F_ICMS_CST_A, F_ICMS_CST_B, F_IPI_CST, F_PIS_CST, F_COFINS_CST,
  F_prod_EXTIPI, F_IPI_cEnq, QrGraGruX, EdGraGruX, CBGraGruX);
end;

end.
