unit GraGruE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, dmkRadioGroup, dmkEditCB, dmkDBLookupComboBox, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmGraGruE = class(TForm)
    DsEmbalagens: TDataSource;
    QrEmbalagens: TmySQLQuery;
    Panel3: TPanel;
    CBEmbalagem: TdmkDBLookupComboBox;
    EdEmbalagem: TdmkEditCB;
    Label1: TLabel;
    QrEmbalagensCodigo: TIntegerField;
    QrEmbalagensNome: TWideStringField;
    EdcProd: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdObservacao: TdmkEdit;
    SbEmbalagem: TSpeedButton;
    Label4: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    SbFornece: TSpeedButton;
    QrFornece: TmySQLQuery;
    DsFornece: TDataSource;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbEmbalagemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbForneceClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGraGruN, FGraGruX: Integer;
    FNomeForm: String;
  end;

  var
  FmGraGruE: TFmGraGruE;

implementation

uses UnMyObjects, MyDBCheck, UMySQLModule, Module, Embalagens, Entidade2,
  UnInternalConsts, GraGruN, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruE.BtOKClick(Sender: TObject);
var
  cProd, Observacao: String;
  Embalagem, Nivel1, Fornece: Integer;
begin
  Fornece := EdFornece.ValueVariant;
  if MyObjects.FIC(Fornece = 0, EdFornece, 'Informe o fornecedor!') then Exit;
  // Reativei em 2010-06-29 - ver porque tinha sido cancelado!!
  Embalagem := EdEmbalagem.ValueVariant;
  if MyObjects.FIC(Embalagem = 0, EdEmbalagem, 'Informe a embalagem!') then Exit;
  // Fim reativa��o
  cProd := EdcProd.ValueVariant;
  if MyObjects.FIC(cProd = '', EdcProd, 'Informe o c�digo do fornecedor!') then Exit;
  //
  Observacao := EdObservacao.ValueVariant;
  Nivel1     := FGraGruN;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragrue', False,
    ['Observacao', 'Embalagem'], ['cProd', 'Nivel1', 'Fornece', 'GraGruX'],
    [Observacao, Embalagem], [cProd, Nivel1, Fornece, FGraGruX], True) then
  begin
    if FNomeForm = 'FmGraGruN' then
      FmGraGruN.ReopenGraGruEX(FGraGruX);
    Close;
  end;
end;

procedure TFmGraGruE.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmGraGruE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruE.SbEmbalagemClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := EdEmbalagem.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEmbalagens.LocCod(Codigo, Codigo);
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrEmbalagens.Close;
      UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
      EdEmbalagem.ValueVariant := VAR_CADASTRO;
      CBEmbalagem.KeyValue     := VAR_CADASTRO;
      CBEmbalagem.SetFocus;
    end;
  end;
end;

procedure TFmGraGruE.SbForneceClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := EdFornece.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEntidade2.LocCod(Codigo, Codigo);
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrFornece.Close;
      UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
      EdFornece.ValueVariant := VAR_CADASTRO;
      CBFornece.KeyValue     := VAR_CADASTRO;
      CBFornece.SetFocus;
    end;
  end;
end;

end.
