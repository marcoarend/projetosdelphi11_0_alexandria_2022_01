object FmGraGruE_P: TFmGraGruE_P
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-016 :: Codifica'#231#227'o por Fornecedor'
  ClientHeight = 420
  ClientWidth = 583
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 583
    Height = 264
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Memo1: TMemo
      Left = 0
      Top = 0
      Width = 583
      Height = 33
      Align = alTop
      Alignment = taCenter
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Lines.Strings = (
        'N'#227'o foi encontrado cadastro para o item abaixo.'
        'Selecione o cadastro correspondente para associ'#225'-lo!')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 33
      Width = 583
      Height = 231
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 128
        Width = 58
        Height = 13
        Caption = 'Embalagem:'
      end
      object Label2: TLabel
        Left = 8
        Top = 4
        Width = 85
        Height = 13
        Caption = 'C'#243'd. Fornecedor: '
        Enabled = False
      end
      object Label3: TLabel
        Left = 8
        Top = 88
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
      end
      object SbEmbalagem: TSpeedButton
        Left = 552
        Top = 144
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbEmbalagemClick
      end
      object Label4: TLabel
        Left = 8
        Top = 168
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object SbGraGruN: TSpeedButton
        Left = 552
        Top = 184
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbGraGruNClick
      end
      object Label5: TLabel
        Left = 172
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Descri'#231#227'o: '
        Enabled = False
      end
      object Label6: TLabel
        Left = 8
        Top = 44
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        Enabled = False
      end
      object SbFornece: TSpeedButton
        Left = 552
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
        OnClick = SbForneceClick
      end
      object CBEmbalagem: TdmkDBLookupComboBox
        Left = 64
        Top = 144
        Width = 485
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmbalagens
        TabOrder = 6
        dmkEditCB = EdEmbalagem
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmbalagem: TdmkEditCB
        Left = 8
        Top = 144
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmbalagem
        IgnoraDBLookupComboBox = False
      end
      object EdcProd: TdmkEdit
        Left = 8
        Top = 20
        Width = 161
        Height = 21
        Enabled = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdObservacao: TdmkEdit
        Left = 8
        Top = 104
        Width = 565
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdGraGru1: TdmkEditCB
        Left = 8
        Top = 184
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdCampo = 'Embalagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 64
        Top = 184
        Width = 485
        Height = 21
        KeyField = 'Nivel1'
        ListField = 'Nome'
        ListSource = DsGraGru1
        TabOrder = 8
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdxProd: TdmkEdit
        Left = 172
        Top = 20
        Width = 401
        Height = 21
        Enabled = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFornece: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Embalagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 485
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsFornece
        TabOrder = 3
        dmkEditCB = EdFornece
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 583
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 535
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 487
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 334
        Height = 32
        Caption = 'Codifica'#231#227'o por Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 334
        Height = 32
        Caption = 'Codifica'#231#227'o por Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 334
        Height = 32
        Caption = 'Codifica'#231#227'o por Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 312
    Width = 583
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 356
    Width = 583
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 435
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsEmbalagens: TDataSource
    DataSet = QrEmbalagens
    Left = 348
    Top = 76
  end
  object QrEmbalagens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM embalagens'
      'ORDER BY Nome')
    Left = 320
    Top = 76
    object QrEmbalagensCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmbalagensNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, Nome '
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 376
    Top = 76
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 404
    Top = 76
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 432
    Top = 76
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 460
    Top = 76
  end
end
