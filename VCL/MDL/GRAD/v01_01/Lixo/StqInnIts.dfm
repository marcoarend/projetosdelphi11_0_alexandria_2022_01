object FmStqInnIts: TFmStqInnIts
  Left = 339
  Top = 185
  Caption = 'STQ-ENTRA-003 :: Itens de Entrada'
  ClientHeight = 408
  ClientWidth = 882
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 882
    Height = 252
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 882
      Height = 252
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 91
        Width = 882
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object PainelGGX: TPanel
          Left = 113
          Top = 0
          Width = 769
          Height = 56
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Label12: TLabel
            Left = 4
            Top = 8
            Width = 76
            Height = 13
            Caption = 'Nome do grupo:'
          end
          object Label13: TLabel
            Left = 248
            Top = 8
            Width = 19
            Height = 13
            Caption = 'Cor:'
            FocusControl = DBEdit2
          end
          object Label14: TLabel
            Left = 392
            Top = 8
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
            FocusControl = DBEdit3
          end
          object Label1: TLabel
            Left = 464
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Unidade:'
            FocusControl = DBEdit4
          end
          object Label2: TLabel
            Left = 552
            Top = 8
            Width = 107
            Height = 13
            Caption = 'Descri'#231#227'o da unidade:'
            FocusControl = DBEdit5
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 24
            Width = 240
            Height = 21
            TabStop = False
            DataField = 'NO_GG1'
            DataSource = DsGraGruX
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 248
            Top = 24
            Width = 140
            Height = 21
            TabStop = False
            DataField = 'NO_COR'
            DataSource = DsGraGruX
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 392
            Top = 24
            Width = 69
            Height = 21
            TabStop = False
            DataField = 'NO_TAM'
            DataSource = DsGraGruX
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 464
            Top = 24
            Width = 82
            Height = 21
            DataField = 'Sigla'
            DataSource = DsGraGruX
            TabOrder = 3
            OnChange = DBEdit4Change
          end
          object DBEdit5: TDBEdit
            Left = 551
            Top = 24
            Width = 208
            Height = 21
            DataField = 'No_SIGLA'
            DataSource = DsGraGruX
            TabOrder = 4
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 113
          Height = 56
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label11: TLabel
            Left = 8
            Top = 8
            Width = 69
            Height = 13
            Caption = 'Reduzido: [F4]'
          end
          object SpeedButton1: TSpeedButton
            Left = 88
            Top = 24
            Width = 21
            Height = 21
            Caption = '?'
            OnClick = SpeedButton1Click
          end
          object EdGraGruX: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GraGruX'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGraGruXChange
            OnKeyDown = EdGraGruXKeyDown
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 50
        Width = 882
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 1
        object CGHowBxaEstq: TdmkDBCheckGroup
          Left = 297
          Top = 0
          Width = 585
          Height = 41
          Align = alClient
          Caption = ' Grandezas obrigat'#243'rias na gera'#231#227'o, entrada e baixa: '
          Columns = 3
          DataField = 'HowBxaEstq'
          DataSource = DsGraGruX
          Items.Strings = (
            'Pe'#231'as'
            #193'rea (m'#178', in'#178')'
            'Peso (kg)')
          ParentBackground = False
          TabOrder = 1
        end
        object RGGerBxaEstq: TDBRadioGroup
          Left = 0
          Top = 0
          Width = 297
          Height = 41
          Align = alLeft
          Caption = ' Grandeza: '
          Columns = 4
          DataField = 'GerBxaEstq'
          DataSource = DsGraGruX
          Items.Strings = (
            '? ? ? '
            'Pe'#231'as'
            #193'rea (m'#178')'
            'Peso (kg)')
          ParentBackground = True
          TabOrder = 0
          Values.Strings = (
            '0'
            '1'
            '2'
            '3')
          OnChange = RGGerBxaEstqChange
        end
      end
      object CkContinuar: TCheckBox
        Left = 8
        Top = 220
        Width = 112
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 4
      end
      object Panel8: TPanel
        Left = 0
        Top = 147
        Width = 882
        Height = 68
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 177
          Height = 68
          Align = alLeft
          Caption = ' Quantidade e custo total: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object LaQtde: TLabel
            Left = 8
            Top = 16
            Width = 68
            Height = 13
            Caption = '??? Qtde ???:'
          end
          object Label20: TLabel
            Left = 100
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Custo total:'
          end
          object EdQtde: TdmkEdit
            Left = 8
            Top = 32
            Width = 89
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PLE'
            UpdCampo = 'PLE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdQtdeExit
          end
          object EdCustoAll: TdmkEdit
            Left = 100
            Top = 32
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoAll'
            UpdCampo = 'CustoAll'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GBOutrasUnidades: TGroupBox
          Left = 577
          Top = 0
          Width = 305
          Height = 68
          Align = alClient
          Caption = 'Outras medidas espec'#237'ficas:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          object Label3: TLabel
            Left = 8
            Top = 16
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label4: TLabel
            Left = 72
            Top = 16
            Width = 27
            Height = 13
            Caption = 'Peso:'
          end
          object Label5: TLabel
            Left = 148
            Top = 16
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label6: TLabel
            Left = 224
            Top = 16
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object EdPecas: TdmkEdit
            Left = 8
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 1
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPLE: TdmkEdit
            Left = 72
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PLE'
            UpdCampo = 'PLE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAreaM2: TdmkEditCalc
            Left = 148
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdAreaP2
            CalcType = ctM2toP2
            CalcFrac = cfCento
          end
          object EdAreaP2: TdmkEditCalc
            Left = 224
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdAreaM2
            CalcType = ctP2toM2
            CalcFrac = cfQuarto
          end
        end
        object GroupBox2: TGroupBox
          Left = 177
          Top = 0
          Width = 400
          Height = 68
          Align = alLeft
          Caption = ' Local de estoqe (apenas informativo):'
          TabOrder = 1
          object Label7: TLabel
            Left = 8
            Top = 16
            Width = 60
            Height = 13
            Caption = 'Localiza'#231#227'o:'
          end
          object CBStqCenLoc: TdmkDBLookupComboBox
            Left = 64
            Top = 32
            Width = 325
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsStqCenLoc
            TabOrder = 1
            dmkEditCB = EdStqCenLoc
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdStqCenLoc: TdmkEditCB
            Left = 8
            Top = 32
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBStqCenLoc
            IgnoraDBLookupComboBox = False
          end
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 882
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object dmkLabel2: TdmkLabel
          Left = 8
          Top = 8
          Width = 126
          Height = 13
          Caption = 'Tipo de Grupo de Produto:'
          UpdType = utYes
          SQLType = stNil
        end
        object dmkLabel3: TdmkLabel
          Left = 340
          Top = 8
          Width = 90
          Height = 13
          Caption = 'Centro de estoque:'
          UpdType = utYes
          SQLType = stNil
        end
        object EdPrdGrupTip: TdmkEditCB
          Left = 8
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPrdGrupTipChange
          DBLookupComboBox = CBPrdGrupTip
          IgnoraDBLookupComboBox = False
        end
        object CBPrdGrupTip: TdmkDBLookupComboBox
          Left = 64
          Top = 24
          Width = 268
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsPrdGrupTip
          TabOrder = 1
          dmkEditCB = EdPrdGrupTip
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdStqCenCad: TdmkEditCB
          Left = 340
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdStqCenCadChange
          DBLookupComboBox = CBStqCenCad
          IgnoraDBLookupComboBox = False
        end
        object CBStqCenCad: TdmkDBLookupComboBox
          Left = 396
          Top = 24
          Width = 236
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsStqCenCad
          TabOrder = 3
          dmkEditCB = EdStqCenCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RGCasasProd: TdmkRadioGroup
          Left = 636
          Top = 13
          Width = 128
          Height = 37
          Caption = ' Casas decimais: '
          Columns = 4
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2'
            '3')
          TabOrder = 4
          OnClick = RGCasasProdClick
          QryCampo = 'CasasProd'
          UpdCampo = 'CasasProd'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 882
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 834
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 786
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 201
        Height = 32
        Caption = 'Itens de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 201
        Height = 32
        Caption = 'Itens de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 201
        Height = 32
        Caption = 'Itens de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 300
    Width = 882
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 878
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 344
    Width = 882
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 878
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 734
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX,  '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM, '
      'med.Nome, med.Sigla, med.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ')
    Left = 392
    Top = 264
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNo_SIGLA: TWideStringField
      FieldName = 'No_SIGLA'
      Size = 30
    end
    object QrGraGruXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGraGruXGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 392
    Top = 312
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM stqcenloc '
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 460
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 460
    Top = 312
  end
  object QrPrdGrupTip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Fracio'
      'FROM prdgruptip'
      'WHERE Codigo<>0'
      'ORDER BY Nome')
    Left = 480
    Top = 12
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 508
    Top = 12
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 536
    Top = 12
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 564
    Top = 12
  end
  object VUPrdGrupTip: TdmkValUsu
    dmkEditCB = EdPrdGrupTip
    Panel = Panel9
    QryCampo = 'StqCenCad'
    UpdCampo = 'StqCenCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 620
    Top = 12
  end
  object VUStqCenCad: TdmkValUsu
    dmkEditCB = EdStqCenCad
    Panel = Panel9
    QryCampo = 'PrdGrupTip'
    UpdCampo = 'PrdGrupTip'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 592
    Top = 12
  end
end
