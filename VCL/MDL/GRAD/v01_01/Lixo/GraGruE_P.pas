unit GraGruE_P;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, dmkRadioGroup, dmkEditCB, dmkDBLookupComboBox, dmkGeral,
  Principal, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmGraGruE_P = class(TForm)
    DsEmbalagens: TDataSource;
    QrEmbalagens: TmySQLQuery;
    QrEmbalagensCodigo: TIntegerField;
    QrEmbalagensNome: TWideStringField;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    Panel1: TPanel;
    Memo1: TMemo;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SbEmbalagem: TSpeedButton;
    Label4: TLabel;
    SbGraGruN: TSpeedButton;
    Label5: TLabel;
    CBEmbalagem: TdmkDBLookupComboBox;
    EdEmbalagem: TdmkEditCB;
    EdcProd: TdmkEdit;
    EdObservacao: TdmkEdit;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    EdxProd: TdmkEdit;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    Label6: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    SbFornece: TSpeedButton;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbEmbalagemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGraGruNClick(Sender: TObject);
    procedure SbForneceClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFormChamou: String;
    FGraGruX: Integer;
  end;

  var
  FmGraGruE_P: TFmGraGruE_P;

implementation

uses UnMyObjects, MyDBCheck, UMySQLModule, Module, UnInternalConsts, Entidade2,
  {$IFDEF FmPQE} PQ, {$ENDIF}
  GraGruN, Embalagens, ModProd;

{$R *.DFM}

procedure TFmGraGruE_P.BtOKClick(Sender: TObject);
var
  cProd, Observacao: String;
  Embalagem, Nivel1, Fornece: Integer;
begin
  Fornece    := EdFornece.ValueVariant;
  if MyObjects.FIC(Fornece = 0, nil, 'Fornecedor n�o informado!') then Exit;
  Nivel1     := EdGraGru1.ValueVariant;
  if MyObjects.FIC(Nivel1 = 0, EdGraGru1, 'Informe o produto!') then Exit;
  Embalagem  := EdEmbalagem.ValueVariant;
  //if MyObjects.FIC(Embalagem = 0, EdEmbalagem, 'Informe a embalagem!') then Exit;
  cProd      := EdcProd.Text;
  Observacao := EdObservacao.Text;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragrue', False, ['Observacao'], [
  'cProd', 'Nivel1', 'Fornece', 'Embalagem'], [Observacao], [
  cProd, Nivel1, Fornece, Embalagem], True) then
  begin
    if FFormChamou = 'FmGraGruN' then
      FmGraGruN.ReopenGraGruEX(FGraGruX);
    Close;
  end;
end;

procedure TFmGraGruE_P.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruE_P.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruE_P.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
end;

procedure TFmGraGruE_P.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruE_P.SbEmbalagemClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrEmbalagens.Close;
      UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
      EdEmbalagem.ValueVariant := VAR_CADASTRO;
      CBEmbalagem.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmGraGruE_P.SbForneceClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrFornece.Close;
      UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
      EdFornece.ValueVariant := VAR_CADASTRO;
      CBFornece.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmGraGruE_P.SbGraGruNClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  {$IFDEF FmPQE}
  //Fazer assim por causa do PQ!
  if FFormChamou = 'FmPQE' then
  begin
    if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
    begin
      DmProd.FPrdGrupTip_Nome := EdxProd.Text;
      FmPQ.ShowModal;
      FmPQ.Destroy;
    end;
  end else begin
    if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
    begin
      DmProd.FPrdGrupTip_Nome := EdxProd.Text;
      FmGraGruN.ShowModal;
      FmGraGruN.Destroy;
    end;
  end;
  {$ELSE}
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdxProd.Text;
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
  {$ENDIF}
  //
  QrGraGru1.Close;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdGraGru1.ValueVariant := VAR_CADASTRO;
    CBGraGru1.KeyValue     := VAR_CADASTRO;
  end;
end;

end.
