object FmGraGruE_P2: TFmGraGruE_P2
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-016 :: Codifica'#231#227'o por Fornecedor'
  ClientHeight = 461
  ClientWidth = 583
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 583
    Height = 305
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Memo1: TMemo
      Left = 0
      Top = 0
      Width = 583
      Height = 33
      Align = alTop
      Alignment = taCenter
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Lines.Strings = (
        'N'#227'o foi encontrado cadastro para o item abaixo.'
        'Selecione o cadastro correspondente para associ'#225'-lo!')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 161
      Width = 583
      Height = 144
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Label1: TLabel
        Left = 8
        Top = 44
        Width = 58
        Height = 13
        Caption = 'Embalagem:'
      end
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
      end
      object SbEmbalagem: TSpeedButton
        Left = 552
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbEmbalagemClick
      end
      object Label4: TLabel
        Left = 8
        Top = 84
        Width = 89
        Height = 13
        Caption = 'Produto (reduzido):'
      end
      object SbGraGruX: TSpeedButton
        Left = 552
        Top = 100
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbGraGruXClick
      end
      object CBEmbalagem: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 485
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmbalagens
        TabOrder = 2
        dmkEditCB = EdEmbalagem
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmbalagem: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmbalagem
        IgnoraDBLookupComboBox = False
      end
      object EdObservacao: TdmkEdit
        Left = 8
        Top = 20
        Width = 565
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'gragrux'
        QryCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 64
        Top = 100
        Width = 485
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 4
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7CodiFldName = 'Controle'
        LocF7NameFldName = 'NO_PRD_TAM_COR'
        LocF7TableName = 'gragrux'
        LocF7SQLMasc = '$#'
      end
      object CkContinuar: TCheckBox
        Left = 8
        Top = 124
        Width = 125
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 5
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 33
      Width = 583
      Height = 128
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 8
        Top = 4
        Width = 85
        Height = 13
        Caption = 'C'#243'd. Fornecedor: '
      end
      object Label5: TLabel
        Left = 172
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Descri'#231#227'o: '
      end
      object Label6: TLabel
        Left = 8
        Top = 44
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        Enabled = False
      end
      object SbFornece: TSpeedButton
        Left = 552
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
        OnClick = SbForneceClick
      end
      object Label7: TLabel
        Left = 112
        Top = 84
        Width = 34
        Height = 13
        Caption = 'CFOP: '
      end
      object Label8: TLabel
        Left = 184
        Top = 84
        Width = 94
        Height = 13
        Caption = 'Unidade comercial: '
      end
      object Label9: TLabel
        Left = 8
        Top = 84
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object EdxProd: TdmkEdit
        Left = 172
        Top = 20
        Width = 401
        Height = 21
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdcProd: TdmkEdit
        Left = 8
        Top = 20
        Width = 161
        Height = 21
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFornece: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Embalagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 485
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsFornece
        ReadOnly = True
        TabOrder = 3
        dmkEditCB = EdFornece
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCFOP: TdmkEdit
        Left = 112
        Top = 100
        Width = 69
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EduCom: TdmkEdit
        Left = 184
        Top = 100
        Width = 93
        Height = 21
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNCM: TdmkEdit
        Left = 8
        Top = 100
        Width = 100
        Height = 21
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NCM'
        UpdCampo = 'NCM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 583
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 535
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 487
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 336
        Height = 32
        Caption = 'Codifica'#231#227'o por Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 336
        Height = 32
        Caption = 'Codifica'#231#227'o por Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 336
        Height = 32
        Caption = 'Codifica'#231#227'o por Fornecedor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 353
    Width = 583
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 397
    Width = 583
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 435
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsEmbalagens: TDataSource
    DataSet = QrEmbalagens
    Left = 184
    Top = 392
  end
  object QrEmbalagens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM embalagens'
      'ORDER BY Nome')
    Left = 184
    Top = 344
    object QrEmbalagensCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmbalagensNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, " ", '
      '  IF(gti.PrintTam=1, gti.Nome, ""), " ", '
      '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR,'
      'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM, '
      'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR, '
      'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI'
      'FROM      gragrux    ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN pqcli pqc ON pqc.Controle=ggx.Controle'
      'WHERE gg1.PrdGrupTip > -3'
      'AND NOT (pqc.PQ IS NULL)'
      'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle')
    Left = 272
    Top = 344
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 87
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 272
    Top = 396
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 348
    Top = 344
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 348
    Top = 392
  end
  object PMGraGruX: TPopupMenu
    Left = 512
    Top = 244
    object CadastrosemGrade1: TMenuItem
      Caption = 'Cadastro &Sem Grade'
      object Usoeconsumo1: TMenuItem
        Caption = '&Uso e consumo'
        OnClick = Usoeconsumo1Click
      end
      object Matriaprima1: TMenuItem
        Caption = '&Mat'#233'ria-prima'
        OnClick = Matriaprima1Click
      end
    end
    object CadastroComGrade1: TMenuItem
      Caption = 'Cadastro &Com Grade'
      OnClick = CadastroComGrade1Click
    end
  end
  object QrGraGruECad: TmySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 120
  end
end
