unit StqCnsgVeCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy,UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, dmkDBGrid, frxClass, frxDBSet,
  UnInternalConsts3, Variants, UnDmkProcFunc, dmkImage, UnDmkEnums, UnGrl_Geral,
  dmkCheckBox, Vcl.ComCtrls, dmkLabelRotate, UnStqPF;

type
  TFmStqCnsgVeCab = class(TForm)
    PnDados: TPanel;
    DsStqCnsgVeCab: TDataSource;
    QrStqCnsgVeCab: TMySQLQuery;
    PnEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrStqMovIts: TmySQLQuery;
    DsStqMovIts: TDataSource;
    PMCab: TPopupMenu;
    PMItens: TPopupMenu;
    DBGStqMovIts: TDBGrid;
    N1: TMenuItem;
    QrStqCnsgVeCabCodigo: TIntegerField;
    QrStqCnsgVeCabCodUsu: TIntegerField;
    QrStqCnsgVeCabNome: TWideStringField;
    QrStqCnsgVeCabEmpresa: TIntegerField;
    QrStqCnsgVeCabAbertura: TDateTimeField;
    QrStqCnsgVeCabEncerrou: TDateTimeField;
    QrStqCnsgVeCabStatus: TSmallintField;
    QrStqCnsgVeCabAtivo: TSmallintField;
    QrStqMovItsNivel1: TIntegerField;
    QrStqMovItsNO_PRD: TWideStringField;
    QrStqMovItsPrdGrupTip: TIntegerField;
    QrStqMovItsUnidMed: TIntegerField;
    QrStqMovItsNO_PGT: TWideStringField;
    QrStqMovItsSIGLA: TWideStringField;
    QrStqMovItsNO_TAM: TWideStringField;
    QrStqMovItsNO_COR: TWideStringField;
    QrStqMovItsGraCorCad: TIntegerField;
    QrStqMovItsGraGruC: TIntegerField;
    QrStqMovItsGraGru1: TIntegerField;
    QrStqMovItsGraTamI: TIntegerField;
    QrStqMovItsDataHora: TDateTimeField;
    QrStqMovItsIDCtrl: TIntegerField;
    QrStqMovItsTipo: TIntegerField;
    QrStqMovItsOriCodi: TIntegerField;
    QrStqMovItsOriCtrl: TIntegerField;
    QrStqMovItsOriCnta: TIntegerField;
    QrStqMovItsEmpresa: TIntegerField;
    QrStqMovItsStqCenCad: TIntegerField;
    QrStqMovItsGraGruX: TIntegerField;
    QrStqMovItsQtde: TFloatField;
    QrStqMovItsAlterWeb: TSmallintField;
    QrStqMovItsAtivo: TSmallintField;
    QrStqMovItsOriPart: TIntegerField;
    QrStqMovItsPecas: TFloatField;
    QrStqMovItsPeso: TFloatField;
    QrStqMovItsAreaM2: TFloatField;
    QrStqMovItsAreaP2: TFloatField;
    QrStqMovItsFatorClas: TFloatField;
    QrStqMovItsQuemUsou: TIntegerField;
    QrStqMovItsRetorno: TSmallintField;
    QrStqMovItsParTipo: TIntegerField;
    QrStqMovItsParCodi: TIntegerField;
    QrStqMovItsDebCtrl: TIntegerField;
    QrStqMovItsSMIMultIns: TIntegerField;
    Incluiitem1: TMenuItem;
    Excluiitem1: TMenuItem;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    QrStqCnsgVeCabNOMEFILIAL: TWideStringField;
    QrStqCnsgVeCabNO_FORNECE: TWideStringField;
    QrStqCnsgVeCabBalQtdItem: TFloatField;
    QrStqCnsgVeCabFatSemEstq: TSmallintField;
    QrStqCnsgVeCabFornece: TIntegerField;
    QrStqCnsgVeCabENCERROU_TXT: TWideStringField;
    GroupBox1: TGroupBox;
    QrFornece: TMySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    GroupBox5: TGroupBox;
    dmkLabel4: TdmkLabel;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    GroupBox2: TGroupBox;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    Label13: TLabel;
    EdNome: TdmkEdit;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    LaFornece: TdmkLabel;
    SbFornece: TSpeedButton;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    VUEmpresa: TdmkValUsu;
    QrStqMovItsCustoAll: TFloatField;
    QrStqMovItsValorAll: TFloatField;
    N2: TMenuItem;
    frxSTQ_MOVIM_008: TfrxReport;
    frxDsStqMovIts: TfrxDBDataset;
    QrForne: TmySQLQuery;
    QrForneE_ALL: TWideStringField;
    QrForneCNPJ_TXT: TWideStringField;
    QrForneNOME_TIPO_DOC: TWideStringField;
    QrForneTE1_TXT: TWideStringField;
    QrForneFAX_TXT: TWideStringField;
    QrForneNUMERO_TXT: TWideStringField;
    QrForneCEP_TXT: TWideStringField;
    QrForneCodigo: TIntegerField;
    QrForneTipo: TSmallintField;
    QrForneCodUsu: TIntegerField;
    QrForneNOME_ENT: TWideStringField;
    QrForneCNPJ_CPF: TWideStringField;
    QrForneIE_RG: TWideStringField;
    QrForneRUA: TWideStringField;
    QrForneCOMPL: TWideStringField;
    QrForneBAIRRO: TWideStringField;
    QrForneCIDADE: TWideStringField;
    QrForneNOMELOGRAD: TWideStringField;
    QrForneNOMEUF: TWideStringField;
    QrFornePais: TWideStringField;
    QrForneENDEREF: TWideStringField;
    QrForneTE1: TWideStringField;
    QrForneFAX: TWideStringField;
    QrForneIE: TWideStringField;
    QrForneCAD_FEDERAL: TWideStringField;
    QrForneCAD_ESTADUAL: TWideStringField;
    QrForneIE_TXT: TWideStringField;
    QrForneNO2_ENT: TWideStringField;
    frxDsForne: TfrxDBDataset;
    frxDsStqCnsgVeCab: TfrxDBDataset;
    PMPagto: TPopupMenu;
    Panel4: TPanel;
    Splitter1: TSplitter;
    IncluiVen1: TMenuItem;
    ExcluiVen1: TMenuItem;
    QrPgtVen: TMySQLQuery;
    QrPgtVenData: TDateField;
    QrPgtVenVencimento: TDateField;
    QrPgtVenBanco: TIntegerField;
    QrPgtVenSEQ: TIntegerField;
    QrPgtVenFatID: TIntegerField;
    QrPgtVenContaCorrente: TWideStringField;
    QrPgtVenDocumento: TFloatField;
    QrPgtVenDescricao: TWideStringField;
    QrPgtVenFatParcela: TIntegerField;
    QrPgtVenFatNum: TFloatField;
    QrPgtVenNOMECARTEIRA: TWideStringField;
    QrPgtVenNOMECARTEIRA2: TWideStringField;
    QrPgtVenBanco1: TIntegerField;
    QrPgtVenAgencia1: TIntegerField;
    QrPgtVenConta1: TWideStringField;
    QrPgtVenTipoDoc: TSmallintField;
    QrPgtVenControle: TIntegerField;
    QrPgtVenNOMEFORNECEI: TWideStringField;
    QrPgtVenCARTEIRATIPO: TIntegerField;
    DsPgtVen: TDataSource;
    SpeedButton5: TSpeedButton;
    QrPgtVenAgencia: TIntegerField;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCab: TBitBtn;
    BtItens: TBitBtn;
    BtPagto: TBitBtn;
    QrForneNUMERO: TFloatField;
    QrForneUF: TFloatField;
    QrForneLOGRAD: TFloatField;
    QrForneCEP: TFloatField;
    QrStqCnsgVeCabCodCliInt: TIntegerField;
    Panel8: TPanel;
    Label5: TLabel;
    Ednfe_serie: TdmkEdit;
    Ednfe_nNF: TdmkEdit;
    Label6: TLabel;
    EdNFe_Id: TdmkEdit;
    Label192: TLabel;
    GroupBox4: TGroupBox;
    DBEdit3: TDBEdit;
    QrStqCnsgVeCabnfe_serie: TIntegerField;
    QrStqCnsgVeCabnfe_nNF: TIntegerField;
    QrStqCnsgVeCabnfe_Id: TWideStringField;
    Label9: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit4: TDBEdit;
    DBEdNFe_Id: TDBEdit;
    PMNovo: TPopupMenu;
    ID1: TMenuItem;
    Pesquisadetalhada1: TMenuItem;
    CkAtzPrcMed: TdmkCheckBox;
    DBCkAtzPrcMed: TDBCheckBox;
    QrStqCnsgVeCabAtzPrcMed: TSmallintField;
    QrPgtFrt: TMySQLQuery;
    QrPgtFrtData: TDateField;
    QrPgtFrtVencimento: TDateField;
    QrPgtFrtBanco: TIntegerField;
    QrPgtFrtSEQ: TIntegerField;
    QrPgtFrtFatID: TIntegerField;
    QrPgtFrtContaCorrente: TWideStringField;
    QrPgtFrtDocumento: TFloatField;
    QrPgtFrtDescricao: TWideStringField;
    QrPgtFrtFatParcela: TIntegerField;
    QrPgtFrtFatNum: TFloatField;
    QrPgtFrtNOMECARTEIRA: TWideStringField;
    QrPgtFrtNOMECARTEIRA2: TWideStringField;
    QrPgtFrtBanco1: TIntegerField;
    QrPgtFrtAgencia1: TIntegerField;
    QrPgtFrtConta1: TWideStringField;
    QrPgtFrtTipoDoc: TSmallintField;
    QrPgtFrtControle: TIntegerField;
    QrPgtFrtNOMEFORNECEI: TWideStringField;
    QrPgtFrtCARTEIRATIPO: TIntegerField;
    QrPgtFrtDebito: TFloatField;
    QrPgtFrtAgencia: TIntegerField;
    DsPgtFrt: TDataSource;
    Produtos1: TMenuItem;
    Frete1: TMenuItem;
    IncluiFrt1: TMenuItem;
    ExcluiFrt1: TMenuItem;
    QrStqCnsgVeCabTransporta: TIntegerField;
    QrStqCnsgVeCabNO_TRANSPORTA: TWideStringField;
    dmkLabel2: TdmkLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrTransporta: TMySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNO_ENT: TWideStringField;
    DsTransporta: TDataSource;
    dmkLabel3: TdmkLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    SbTransporta: TSpeedButton;
    QrSumFrt: TMySQLQuery;
    QrStqCnsgVeCabValTotFrete: TFloatField;
    QrSumFrtDebito: TFloatField;
    QrSumIts: TMySQLQuery;
    QrSumItsQtde: TFloatField;
    QrStqMovItsCustoBuy: TFloatField;
    QrStqMovItsCustoFrt: TFloatField;
    DBGVen: TDBGrid;
    dmkLabelRotate1: TdmkLabelRotate;
    dmkLabelRotate2: TdmkLabelRotate;
    DBGFrt: TDBGrid;
    Fracionamento1: TMenuItem;
    N4: TMenuItem;
    QrStqMovItsPackGGX: TIntegerField;
    QrStqMovItsPackUnMed: TIntegerField;
    QrStqMovItsPackQtde: TFloatField;
    Incluinovavenda1: TMenuItem;
    Alteravendaatual1: TMenuItem;
    Excluivendaatual1: TMenuItem;
    Encerravenda1: TMenuItem;
    EdTabePrcCab: TdmkEditCB;
    dmkLabel6: TdmkLabel;
    CBTabePrcCab: TdmkDBLookupComboBox;
    SbTabePrcCab: TSpeedButton;
    QrTabePrcCab: TMySQLQuery;
    DsTabePrcCab: TDataSource;
    QrTabePrcCabCodigo: TIntegerField;
    QrTabePrcCabNome: TWideStringField;
    QrStqCnsgVeCabNO_TabePrcCab: TWideStringField;
    DBEdit9: TDBEdit;
    Label16: TLabel;
    QrStqCnsgVeCabTabePrcCab: TIntegerField;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    QrStqMovItsTwnCtrl: TIntegerField;
    QrStqMovItsTwnBrth: TIntegerField;
    QrCliente: TMySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNO_ENT: TWideStringField;
    DsCliente: TDataSource;
    DBEdit14: TDBEdit;
    dmkLabel5: TdmkLabel;
    DBEdit15: TDBEdit;
    QrStqCnsgVeCabNO_CLIENTE: TWideStringField;
    EdCliente: TdmkEditCB;
    dmkLabel7: TdmkLabel;
    CBCliente: TdmkDBLookupComboBox;
    SbCliente: TSpeedButton;
    QrStqCnsgVeCabCliente: TIntegerField;
    QrPgtVenCredito: TFloatField;
    QrPgtVenNO_SIT: TWideStringField;
    QrPgtFrtNO_SIT: TWideStringField;
    QrPgtFrtCompensado_TXT: TWideStringField;
    QrPgtVenCompensado_TXT: TWideStringField;
    Splitter2: TSplitter;
    QrStqCnsgVeCabStqCiclCab: TIntegerField;
    Label18: TLabel;
    DBEdit16: TDBEdit;
    QrSumItsCustoBuy: TFloatField;
    QrSumItsValorAll: TFloatField;
    QrSumVen: TMySQLQuery;
    QrSumVenValor: TFloatField;
    QrStqCnsgVeCabOPNValorAll: TFloatField;
    QrStqCnsgVeCabTotalQtde: TFloatField;
    QrStqCnsgVeCabTotValorAll: TFloatField;
    QrStqCnsgVeCabPagValorAll: TFloatField;
    Label19: TLabel;
    DBEdit17: TDBEdit;
    Label20: TLabel;
    DBEdit18: TDBEdit;
    Label21: TLabel;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    EdStqCiclCab: TdmkEdit;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqCnsgVeCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqCnsgVeCabBeforeOpen(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrStqCnsgVeCabBeforeClose(DataSet: TDataSet);
    procedure QrStqCnsgVeCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluiitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure QrStqCnsgVeCabCalcFields(DataSet: TDataSet);
    procedure SbForneceClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrForneCalcFields(DataSet: TDataSet);
    procedure BtPagtoClick(Sender: TObject);
    procedure IncluiVen1Click(Sender: TObject);
    procedure ExcluiVen1Click(Sender: TObject);
    procedure QrStqCnsgVeCabAfterClose(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure PMPagtoPopup(Sender: TObject);
    procedure ID1Click(Sender: TObject);
    procedure Pesquisadetalhada1Click(Sender: TObject);
    procedure QrPgtVenCalcFields(DataSet: TDataSet);
    procedure IncluiFrt1Click(Sender: TObject);
    procedure ExcluiFrt1Click(Sender: TObject);
    procedure QrPgtFrtCalcFields(DataSet: TDataSet);
    procedure Incluinovavenda1Click(Sender: TObject);
    procedure Alteravendaatual1Click(Sender: TObject);
    procedure Excluivendaatual1Click(Sender: TObject);
    procedure Encerravenda1Click(Sender: TObject);
    procedure SbTabePrcCabClick(Sender: TObject);
    procedure SbClienteClick(Sender: TObject);
  private
    FTabLctA: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure HabilitaBotoes();
    //procedure AtivaItensNoEstoque(IDCtrl, Status: Integer);
    procedure ReopenSumIts(Codigo: Integer);
  public
    { Public declarations }
    FThisFatID_Ven,
    FThisFatID_Frt,
    FStqCiclCab, FFornece, FEmpresa: Integer;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenStqMovIts(IDCtrl: Integer);
    //procedure ReopenPagtosBuy();
    //procedure ReopenPagtosFrt();
    procedure RateioCustoDoFrete();
    procedure HabilitaComps(SQLType: TSQLType);
    procedure AtualizaTotais(Codigo: Integer);
  end;

var
  FmStqCnsgVeCab: TFmStqCnsgVeCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF,  UnDmkWeb, StqInnPesq,
  {$IfNDef semNFe_v0000}ModuleNFe_0000, NFeImporta_0400,{$EndIf}
  {$IFDEF FmPlacasAdd}PlacasAdd,{$ENDIF}
  {$IfNDef NO_FINANCEIRO}ModuleFin, UnPagtos,{$EndIf}
  UnGrade_Tabs, ModuleGeral, UCreate, StqInnNFe, Entidade2, StqCnsgVeIts,
  UMySQLDB, UnApp_Jan, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqCnsgVeCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqCnsgVeCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqCnsgVeCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqCnsgVeCab.DefParams;
begin
  VAR_GOTOTABELA := 'StqCnsgVeCab';
  VAR_GOTOMYSQLTABLE := QrStqCnsgVeCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('tpc.Nome NO_TabePrcCab, pem.BalQtdItem, pem.FatSemEstq, ');
  VAR_SQLx.Add('sic.TotValorAll - sic.PagValorAll OPNValorAll, sic.*, ');
  VAR_SQLx.Add('ei.CodCliInt');
  VAR_SQLx.Add('FROM stqcnsgvecab sic');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades fil  ON fil.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem  ON pem.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=sic.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades tsp ON tsp.Codigo=sic.Transporta');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=sic.TabePrcCab');
  VAR_SQLx.Add('WHERE sic.Codigo > -1000');
  VAR_SQLx.Add('AND sic.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND sic.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sic.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sic.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Codigo IN (SELECT Codigo FROM stqcnsgvecab WHERE Empresa IN (' + VAR_LIB_EMPRESAS + '))';
end;

procedure TFmStqCnsgVeCab.RateioCustoDoFrete();
var
  Codigo, IDCtrl, GraGruX, Empresa: Integer;
  Fator, SumCustoBuy, ValTotFrete, CustoFrt, ValorAll: Double;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  Codigo  := QrStqCnsgVeCabCodigo.Value;
  Empresa := QrStqCnsgVeCabEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumFrt, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito',
  'FROM ' + FTabLctA + ' la',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgVeCabCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  '']);
  //
  ValTotFrete := QrSumFrtDebito.Value;
  ReopenSumIts(QrStqCnsgVeCabCodigo.Value);
  SumCustoBuy := QrSumItsCustoBuy.Value;
  if SumCustoBuy > 0 then
    Fator := ValTotFrete / SumCustoBuy
  else
    Fator := 0;
  //
  ReopenStqMovIts(0);
  QrStqMovIts.DisableControls;
  try
    QrStqMovIts.First;
    while not QrStqMovIts.Eof do
    begin
      CustoFrt := QrStqMovItsCustoBuy.Value * Fator;
      ValorAll := QrStqMovItsCustoBuy.Value + CustoFrt;
      //
      GraGruX  := QrStqMovItsGraGruX.Value;
      IDCtrl   := QrStqMovItsIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
      'CustoFrt', 'ValorAll'], [
      'IDCtrl'], [
      CustoFrt, ValorAll], [
      IDCtrl], False);
      //
      DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
      //
      QrStqMovIts.Next;
    end;
  finally
    QrStqMovIts.EnableControls;
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqcnsgvecab', False, [
  'ValTotFrete'], [
  'Codigo'], [
  ValTotFrete], [
  Codigo], False);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmStqCnsgVeCab.ReopenStqMovIts(IDCtrl: Integer);
begin
  StqPF.ReopenStqMovIts(QrStqMovIts, FThisFatID_Ven, QrStqCnsgVeCabCodigo.Value, IDCtrl);
end;

procedure TFmStqCnsgVeCab.ReopenSumIts(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(Qtde) Qtde, SUM(CustoBuy) CustoBuy, SUM(ValorAll) ValorAll ',
  'FROM stqmovitsa ',
  'WHERE Tipo=' + Geral.FF0(FThisFatID_Ven),
  'AND OriCodi=' + Geral.FF0(QrStqCnsgVeCabCodigo.Value),
  '']);
  //

end;

procedure TFmStqCnsgVeCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCnsgVeCab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqCnsgVeCab.Encerravenda1Click(Sender: TObject);
var
  LastCod, Codigo, Empresa, Status: Integer;
  ListaGraGruX: TStringList;
  //
  Encerrou: String;
begin
  Codigo := QrStqCnsgVeCabCodigo.Value;
  //
  if QrStqCnsgVeCabEncerrou.Value = 0 then
  begin
    Encerrou := Geral.FDT(DModG.ObtemAgora(), 109);
    Status := 1;
  end else
  begin
    if MyObjects.FIC(QrStqCnsgVeCabStqCiclCab.Value > 0, nil,
    'Esta venda j� pertence ao ciclo n� ' + Geral.FF0(
    QrStqCnsgVeCabStqCiclCab.Value) + ' e n�o pode mais ser reaberta!' +
    sLineBreak + 'Para reabr�-la, remova-a do ciclo.') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrUpdM, Dmod.MyDB, [
    'SELECT MAX(Codigo) Codigo',
    'FROM stqcnsgvecab',
    'WHERE Fornece=' + Geral.FF0(QrStqCnsgVeCabFornece.Value),
    '']);
    LastCod := USQLDB.v_i(Dmod.QrUpdM, 'Codigo');
    if LastCod <> QrStqCnsgVeCabCodigo.Value then
    begin
      Geral.MB_Aviso('Somente a �ltima venda do vendedor/representante ' +
      QrStqCnsgVeCabNO_FORNECE.Value + ' pode ser desfeita!');
      Exit;
    end else
    begin
      Encerrou := '0';
      Status := 1;
    end;
    //
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqcnsgvecab', False, [
  'Encerrou', 'Status'], ['Codigo'], [
  Encerrou, Status], [Codigo], True);
  //
  StqPF.AtivaItensNoEstoque_1(FThisFatID_Ven, QrStqCnsgVeCabCodigo.Value, Status);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmStqCnsgVeCab.ExcluiVen1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados, Codigo: Integer;
begin
  Codigo := QrStqCnsgVeCabCodigo.Value;
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID_Ven, Codigo);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPgtVen, DBGVen,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  //
  AtualizaTotais(Codigo);
  LocCod(Codigo, Codigo);
  StqPF.ReopenPagtos(QrPgtVen, FTabLctA, Geral.FF0(Codigo), FThisFatID_Ven);
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmStqCnsgVeCab.ExcluiFrt1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados: Integer;
begin
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID_Frt, QrStqCnsgVeCabCodigo.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPgtFrt, DBGFrt,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  RateioCustoDoFrete();
  StqPF.ReopenPagtos(QrPgtFrt, FTabLctA, Geral.FF0(QrStqCnsgVeCabCodigo.Value), FThisFatID_Frt);
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmStqCnsgVeCab.Excluiitem1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrStqMovIts, DBGStqMovIts,
  'StqMovItsA', ['IDCtrl'], ['IDCtrl'], istPergunta, '');
end;

procedure TFmStqCnsgVeCab.Excluivendaatual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrStqMovIts.RecordCount > 0 then
    Geral.MB_Aviso(
    'Esta venda n�o pode ser exclu�da, pois existem itens lan�ados nela.')
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o da venda ID n�mero ' +
      Geral.FF0(QrStqCnsgVeCabCodigo.Value) + '?') <> ID_YES then Exit;
    //
    Codigo := QrStqCnsgVeCabCodigo.Value;
    //
    if DBCheck.ExcluiRegistro(Dmod.QrUpd, QrStqCnsgVeCab, 'StqCnsgVeCab', ['Codigo'],
      ['Codigo'], True, 'Confirma a exclus�o desta venda?') then
    begin
      LocCod(Codigo, Codigo);
      Va(vpLast);
    end;
  end;
end;

procedure TFmStqCnsgVeCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqCnsgVeCab.Pesquisadetalhada1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmStqInnPesq, FmStqInnPesq, afmoNegarComAviso) then
  begin
    FmStqInnPesq.EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.FThisFatID             := FThisFatID_Ven;
    FmStqInnPesq.ShowModal;
    FmStqInnPesq.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
    LocCod(VAR_CADASTRO, VAR_CADASTRO);
end;

procedure TFmStqCnsgVeCab.PMCabPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrStqCnsgVeCab.State <> dsInactive) and (QrStqCnsgVeCab.RecordCount > 0);
  //
  if Enab then
    Enab2 := QrStqCnsgVeCabEncerrou.Value = 0
  else
    Enab2 := False;
  //
  Alteravendaatual1.Enabled := Enab and Enab2;
  Excluivendaatual1.Enabled := Enab and Enab2;
  Encerravenda1.Enabled     := Enab;
end;

procedure TFmStqCnsgVeCab.PMItensPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrStqCnsgVeCab.State <> dsInactive) and (QrStqCnsgVeCab.RecordCount > 0);
  Enab2 := (QrStqMovIts.State <> dsInactive) and (QrStqMovIts.RecordCount > 0);
  //
  Incluiitem1.Enabled := Enab;
  Fracionamento1.Enabled := Enab and Enab2;
  Excluiitem1.Enabled := Enab and Enab2;
end;

procedure TFmStqCnsgVeCab.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrStqCnsgVeCab.State <> dsInactive) and (QrStqCnsgVeCab.RecordCount > 0);
  Enab2 := (QrPgtVen.State <> dsInactive) and (QrPgtVen.RecordCount > 0);
  Enab3 := (QrPgtFrt.State <> dsInactive) and (QrPgtFrt.RecordCount > 0);
  //
  IncluiVen1.Enabled := Enab;
  ExcluiVen1.Enabled := Enab and Enab2;
  //
  IncluiFrt1.Enabled := Enab;
  ExcluiFrt1.Enabled := Enab and Enab3;
  //
end;

procedure TFmStqCnsgVeCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqCnsgVeCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqCnsgVeCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqCnsgVeCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqCnsgVeCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqCnsgVeCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqCnsgVeCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqCnsgVeCab.SpeedButton5Click(Sender: TObject);
begin
  {$IFDEF FmPlacasAdd}
  if DBCheck.CriaFm(TFmPlacasAdd, FmPlacasAdd, afmoNegarComAviso) then
  begin
    FmPlacasAdd.ShowModal;
    EdNome.ValueVariant := FmPlacasAdd.FPlacas;
    FmPlacasAdd.Destroy;
  end;
  {$ENDIF}
end;

procedure TFmStqCnsgVeCab.SbClienteClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdCliente.ValueVariant;

  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrCliente, VAR_CADASTRO);
    EdCliente.SetFocus;
  end;
end;

procedure TFmStqCnsgVeCab.SbForneceClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdFornece.ValueVariant;

  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFornece, CBFornece, QrFornece, VAR_CADASTRO);
    EdFornece.SetFocus;
  end;
end;

procedure TFmStqCnsgVeCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqCnsgVeCabCodigo.Value;
  Close;
end;

procedure TFmStqCnsgVeCab.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmStqCnsgVeCab.BtPagtoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagto);
end;

procedure TFmStqCnsgVeCab.Alteravendaatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrStqCnsgVeCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'StqCnsgVeCab');
  //
  HabilitaComps(stUpd);
  //
  GroupBox2.Enabled := QrStqMovIts.RecordCount = 0;
  //
  EdFornece.Enabled := QrStqMovIts.RecordCount = 0;
  CBFornece.Enabled := QrStqMovIts.RecordCount = 0;
end;

(*
procedure TFmStqCnsgVeCab.AtivaItensNoEstoque(IDCtrl, Status: Integer);
var
  Ativo: Integer;
begin
  if Status > 0 then // Existe status maior que 1?
    Ativo := 1
  else
    Ativo := 0;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE stqmovitsa ',
  'SET Ativo=' + Geral.FF0(Status),
  'WHERE Tipo="' + Geral.FF0(FThisFatID_Ven) + '"',
  'AND OriCodi="' + Geral.FF0(IDCtrl) + '"',
  '']);
end;
*)

procedure TFmStqCnsgVeCab.BtConfirmaClick(Sender: TObject);
var
  Nome, Abertura, (*Encerrou,*) nfe_Id: String;
  Codigo, CodUsu, Empresa, (*Status,*) Fornece, Cliente, nfe_serie,
  nfe_nNF, AtzPrcMed, Transporta, TabePrcCab, StqCiclCab: Integer;
  //ValTotFrete: Double;
  SQLType: TSQLType;
begin
  //
  if MyObjects.FIC(EdFilial.ValueVariant = 0, EdFilial, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdFornece.ValueVariant = 0, EdFornece, 'Informe o vendedor/representante!') then Exit;
  //if MyObjects.FIC(EdCodigo.ValueVariant = 0, nil, 'Informe o c�digo!') then Exit;
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  CodUsu         := EdCodUsu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  Abertura       := Geral.FDT(EdAbertura.ValueVariant, 109);
  //Encerrou       := ;
  //Status         := ;
  Fornece        := EdFornece.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  nfe_serie      := Ednfe_serie.ValueVariant;
  nfe_nNF        := Ednfe_nNF.ValueVariant;;
  nfe_Id         := Ednfe_Id.ValueVariant;;
  AtzPrcMed      := 0;
  Transporta     := EdTransporta.ValueVariant;
  TabePrcCab     := EdTabePrcCab.ValueVariant;
  StqCiclCab     := EdStqCiclCab.ValueVariant;

  //ValTotFrete    := ;
  //
  Codigo := UMyMod.BPGS1I32('stqcnsgvecab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if SQLType = stIns then
    CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqcnsgvecab', False, [
  'CodUsu', 'Nome', 'Empresa',
  'Abertura', (*'Encerrou', 'Status',*)
  'Fornece', 'Cliente', 'nfe_serie',
  'nfe_nNF', 'nfe_Id', 'AtzPrcMed',
  'Transporta'(*, 'ValTotFrete'*), 'TabePrcCab',
  'StqCiclCab'], [
  'Codigo'], [
  CodUsu, Nome, Empresa,
  Abertura, (*Encerrou, Status,*)
  Fornece, Cliente, nfe_serie,
  nfe_nNF, nfe_Id, AtzPrcMed,
  Transporta(*, ValTotFrete*), TabePrcCab,
  StqCiclCab], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmStqCnsgVeCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'StqCnsgVeCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCnsgVeCab', 'Codigo');
  MostraEdicao(0, stlok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCnsgVeCab', 'Codigo');
end;

procedure TFmStqCnsgVeCab.AtualizaTotais(Codigo: Integer);
var
  TotalQtde, TotValorAll, PagValorAll: Double;
begin
  ReopenSumIts(Codigo);
  //
  TotalQtde   := QrSumItsQtde.Value;
  TotValorAll := QrSumItsValorAll.Value;
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumVen, Dmod.MyDB, [
  'SELECT SUM(Credito-Debito) Valor',
  'FROM ' + FTabLctA + ' la',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgVeCabCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Ven),
  '']);
  //
  PagValorAll := QrSumVenValor.Value;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqcnsgvecab', False, [
  'TotalQtde', 'TotValorAll', 'PagValorAll'], [
  'Codigo'], [
  TotalQtde, TotValorAll, PagValorAll], [
  Codigo], True);
end;

procedure TFmStqCnsgVeCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmStqCnsgVeCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FStqCiclCab := 0;
  FFornece := 0;
  FEmpresa := 0;
  FThisFatID_Ven := VAR_FATID_4205;
  FThisFatID_Frt := VAR_FATID_4215;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBFilial.ListSource := DModG.DsEmpresas;
  DBGStqMovIts.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrTabePrcCab, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM tabeprccab',
  'WHERE Codigo=0',
  'OR',
  '(',
  '  ' + Geral.FF0(1) + ' & Aplicacao',
  '  AND "' + Geral.FDT(DModG.ObtemAgora(), 1) + '" BETWEEN DataI and DataF',
  ')',
  'ORDER BY Nome',
  '']);
*)
  StqPF.ReopenTabePrcCab(QrTabePrcCab, (*Aplicacao*)1);
  //
  if UpperCase(Application.Title) = 'SAFECAR' then
  begin
    EdNome.Width         := 817;
    SpeedButton5.Visible := True;
  end else
  begin
    EdNome.Width         := 845;
    SpeedButton5.Visible := False;    
  end;
end;

procedure TFmStqCnsgVeCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqCnsgVeCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCnsgVeCab.SbImprimeClick(Sender: TObject);
begin
  DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  DModG.ReopenEndereco(Geral.IMV(VAR_LIB_EMPRESAS));
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrForne, Dmod.MyDB, [
    'SELECT en.Codigo, Tipo, CodUsu, IE, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF,',
    'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO2_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG,',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA,',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0.000 NUMERO,',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL,',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO,',
    'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CIDADE,',
    'CASE WHEN en.Tipo=0 THEN en.EUF         ELSE en.PUF      END + 0.000 UF,',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD,',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF,',
    'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais,',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0.000 Lograd,',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0.000 CEP,',
    'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF,',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1,',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX,',
    'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL',
    'FROM entidades en',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd',
    'WHERE en.Codigo=' + Geral.FF0(QrStqCnsgVeCabFornece.Value),
    '']);
*)
  StqPF.ReopenForne(QrForne, QrStqCnsgVeCabFornece.Value);
  //
  MyObjects.frxDefineDataSets(frxSTQ_MOVIM_008, [
    DModG.frxDsMaster,
    DModG.frxDsEndereco,
    frxDsForne,
    frxDsStqCnsgVeCab,
    frxDsStqMovIts
    ]);
  //
  MyObjects.frxMostra(frxSTQ_MOVIM_008, 'Informe da Venda de Consignado  n� ' +
    FormatFloat('000000', QrStqCnsgVeCabCodUsu.Value));
end;

procedure TFmStqCnsgVeCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqCnsgVeCab.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, SbNovo);
end;

procedure TFmStqCnsgVeCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqCnsgVeCab.QrForneCalcFields(DataSet: TDataSet);
begin
(*
  QrForneTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneTe1.Value);
  QrForneFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneFax.Value);
  QrForneCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrForneCNPJ_CPF.Value);
  QrForneIE_TXT.Value :=
    Geral.Formata_IE(QrForneIE_RG.Value, QrForneUF.Value, '??', QrForneTipo.Value);
  QrForneNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrForneRua.Value, Trunc(QrForneNumero.Value), False);
  //
  QrForneE_ALL.Value := UpperCase(QrForneNOMELOGRAD.Value);
  if Trim(QrForneE_ALL.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ';
  QrForneE_ALL.Value := QrForneE_ALL.Value + Uppercase(QrForneRua.Value);
  if Trim(QrForneRua.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNUMERO_TXT.Value;
  if Trim(QrForneCompl.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ' + Uppercase(QrForneCompl.Value);
  if Trim(QrForneBairro.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneBairro.Value);
  if QrForneCEP.Value > 0 then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrForneCEP.Value);
  if Trim(QrForneCidade.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneCidade.Value);
  if Trim(QrForneNOMEUF.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNOMEUF.Value;
  if Trim(QrFornePais.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + QrFornePais.Value;
*)
  StqPF.QrForneCalcFields(QrForne);
end;

procedure TFmStqCnsgVeCab.QrPgtVenCalcFields(DataSet: TDataSet);
begin
  QrPgtVenSEQ.Value := QrPgtVen.RecNo;
end;

procedure TFmStqCnsgVeCab.QrPgtFrtCalcFields(DataSet: TDataSet);
begin
  QrPgtFrtSEQ.Value := QrPgtFrt.RecNo;
end;

procedure TFmStqCnsgVeCab.QrStqCnsgVeCabAfterClose(DataSet: TDataSet);
begin
  HabilitaBotoes;
end;

procedure TFmStqCnsgVeCab.QrStqCnsgVeCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtPagto.Enabled := True;
  HabilitaBotoes;
end;

procedure TFmStqCnsgVeCab.QrStqCnsgVeCabAfterScroll(DataSet: TDataSet);
begin
  ReopenStqMovIts(0);
  //
  BtItens.Enabled := QrStqCnsgVeCabEncerrou.Value = 0;
  if QrStqCnsgVeCabCodCliInt.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrStqCnsgVeCabCodCliInt.Value)
  else
    FTabLctA := sTabLctErr;
  //
  StqPF.ReopenPagtos(QrPgtVen, FTabLctA, Geral.FF0(QrStqCnsgVeCabCodigo.Value), FThisFatID_Ven);
  StqPF.ReopenPagtos(QrPgtFrt, FTabLctA, Geral.FF0(QrStqCnsgVeCabCodigo.Value), FThisFatID_Frt);
end;

procedure TFmStqCnsgVeCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdNFe_Id.CharCase   := ecNormal;
  DBEdNFe_Id.CharCase := ecNormal;
end;

procedure TFmStqCnsgVeCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqCnsgVeCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'StqCnsgVeCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqCnsgVeCab.SbTabePrcCabClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  App_jan.MostraFormTabePrcCab();
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTabePrcCab, CBTabePrcCab, QrTabePrcCab, VAR_CADASTRO);
    EdTabePrcCab.SetFocus;
  end;
end;

procedure TFmStqCnsgVeCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCnsgVeCab.HabilitaBotoes;
var
  Enab: Boolean;
  Texto: string;
begin
  if (QrStqCnsgVeCabStatus.Value > 0) and (QrStqCnsgVeCabEncerrou.Value <> 0) and
    (QrStqCnsgVeCab.RecordCount > 0) then
  begin
    Enab  := False;
    Texto := 'Desfaz encerramento da venda';
  end else
  begin
    Enab  := True;
    Texto := 'Encerra a venda';
  end;
  //
  BtItens.Enabled             := Enab;
  Alteravendaatual1.Enabled := Enab;
  Excluivendaatual1.Enabled := Enab;
  Encerravenda1.Caption     := Texto;
end;

procedure TFmStqCnsgVeCab.HabilitaComps(SQLType: TSQLType);
var
  Habilita: Boolean;
begin
  Habilita := (SQLType = stIns) or ((QrStqMovIts.State <> dsInactive) and (QrStqMovIts.RecordCount = 0));
  GroupBox2.Enabled := Habilita;
  LaFornece.Enabled := Habilita;
  EdFornece.Enabled := Habilita;
  CBFornece.Enabled := Habilita;
  SbFornece.Enabled := Habilita;
end;

procedure TFmStqCnsgVeCab.ID1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqCnsgVeCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmStqCnsgVeCab.IncluiVen1Click(Sender: TObject);
var
  Terceiro, Codigo, Empresa, NF, Carteira, Genero: Integer;
  Valor: Double;
  Descricao: String;
begin
 {$IfNDef NO_FINANCEIRO}
  Codigo        := QrStqCnsgVeCabCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Terceiro      := QrStqCnsgVeCabFornece.Value;
  Descricao     := 'Venda de Consignado n� ' + Geral.FF0(QrStqCnsgVeCabCodUsu.Value);
  Empresa       := QrStqCnsgVeCabEmpresa.Value;
  NF            := QrStqCnsgVeCabnfe_nNF.Value;
  Carteira      := StqPF.DefineCarteira(Terceiro);
  Valor         := QrStqCnsgVeCabOPNValorAll.Value;
  //
  if Dmod.QrOpcoesApp.State = dsInactive then
    Dmod.ReopenOpcoesApp();
  Genero := Dmod.QrOpcoesAppPlaConCtaVen.Value;
  //
  UPagtos.Pagto(QrPgtVen, tpCred, Codigo, Terceiro, FThisFatID_Ven, Genero, 0, stIns,
  'Venda', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0, True, True,
  Carteira, 0, 0, 0, NF, FTabLctA, Descricao);
  //
  AtualizaTotais(Codigo);
  LocCod(codigo, Codigo);
  StqPF.ReopenPagtos(QrPgtVen, FTabLctA, Geral.FF0(Codigo), FThisFatID_Ven);
 {$Else}
   Geral.MB_Info('ERP sem pagamento de Venda de Consignado');
 {$EndIf}
end;

procedure TFmStqCnsgVeCab.IncluiFrt1Click(Sender: TObject);
var
  Terceiro, Codigo, Empresa, NF, Carteira: Integer;
  Valor: Double;
  Descricao: String;
begin
 {$IfNDef NO_FINANCEIRO}
  Codigo        := QrStqCnsgVeCabCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Terceiro      := QrStqCnsgVeCabTransporta.Value;
  Descricao     := 'FC n� ' + Geral.FF0(QrStqCnsgVeCabCodUsu.Value);
  Empresa       := QrStqCnsgVeCabEmpresa.Value;
  NF            := QrStqCnsgVeCabnfe_nNF.Value;
  Carteira      := 0; //StqPF.DefineCarteira(Terceiro);
  //
  UPagtos.Pagto(QrPgtFrt, tpDeb, Codigo, Terceiro, FThisFatID_Frt, 0, 0, stIns,
  'Frete de venda', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0, True, True,
  Carteira, 0, 0, 0, NF, FTabLctA, Descricao);
  //
  RateioCustoDoFrete();
  StqPF.ReopenPagtos(QrPgtVen, FTabLctA, Geral.FF0(QrStqCnsgVeCabCodigo.Value), FThisFatID_Ven);
 {$Else}
   Geral.MB_Info('ERP sem pagamento de Venda de Consignado');
 {$EndIf}
end;

procedure TFmStqCnsgVeCab.Incluiitem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqCnsgVeIts, FmStqCnsgVeIts, afmoNegarComAviso) then
  begin
    FmStqCnsgVeIts.ImgTipo.SQLType := stIns;
    FmStqCnsgVeIts.FTabePrcCab := QrStqCnsgVeCabTabePrcCab.Value;
    //
    FmStqCnsgVeIts.ShowModal;
    FmStqCnsgVeIts.Destroy;
    //
    ReopenStqMovIts(0);
  end;
end;

procedure TFmStqCnsgVeCab.Incluinovavenda1Click(Sender: TObject);
begin
  if AppPF.ImpedePorMovimentoAberto() then
    Exit;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrStqCnsgVeCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'StqCnsgVeCab');
  //
  HabilitaComps(stIns);
  EdAbertura.ValueVariant := DModG.ObtemAgora();
  EdFilial.ValueVariant   := DModG.QrFiliLogFilial.Value;
  CBFilial.KeyValue       := DModG.QrFiliLogFilial.Value;
  EdStqCiclCab.ValueVariant := FStqCiclCab;
  if FFornece <> 0 then
  begin
    EdFornece.ValueVariant := FFornece;
    CBFornece.KeyValue := FFornece;
    //
    EdFornece.Enabled := False;
    CBFornece.Enabled := False;
  end;
  if FEmpresa <> 0 then
  begin
    DmodG.QrEmpresas.Locate('Codigo', FEmpresa, []);
    EdFilial.ValueVariant := DModG.QrEmpresasFilial.Value;
    CBFilial.KeyValue := DModG.QrEmpresasFilial.Value;
    //
    EdFilial.Enabled := False;
    CBFilial.Enabled := False;
  end;
end;

procedure TFmStqCnsgVeCab.QrStqCnsgVeCabBeforeClose(DataSet: TDataSet);
begin
  QrStqMovIts.Close;
  QrPgtVen.Close;
  //
  BtItens.Enabled := False;
  BtPagto.Enabled := False;
end;

procedure TFmStqCnsgVeCab.QrStqCnsgVeCabBeforeOpen(DataSet: TDataSet);
begin
  QrStqCnsgVeCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqCnsgVeCab.QrStqCnsgVeCabCalcFields(DataSet: TDataSet);
begin
  if QrStqCnsgVeCabEncerrou.Value = 0 then
    QrStqCnsgVeCabENCERROU_TXT.Value := CO_MovimentoAberto
  else
    QrStqCnsgVeCabENCERROU_TXT.Value := Geral.FDT(QrStqCnsgVeCabEncerrou.Value, 0);
  //
end;

(*
procedure TFmStqCnsgVeCab.ReopenPagtosBuy();
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgtVen, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
  'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, ',
  'la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI,',
  'ca.Tipo CARTEIRATIPO',
  'FROM ' + FTabLctA + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgVeCabCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Ven),
  'ORDER BY la.FatParcela, la.Vencimento',
  '']);
{$EndIf}
end;
*)

(*
procedure TFmStqCnsgVeCab.ReopenPagtosFrt();
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgtFrt, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
  'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, ',
  'la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI,',
  'ca.Tipo CARTEIRATIPO',
  'FROM ' + FTabLctA + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgVeCabCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  'ORDER BY la.FatParcela, la.Vencimento',
  '']);
{$EndIf}
end;
*)

end.

