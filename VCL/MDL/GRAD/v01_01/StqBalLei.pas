unit StqBalLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, Variants, Grids, DBGrids,
  ComCtrls, dmkImage, UnDmkEnums, UnProjGroup_Consts;

type
  TFmStqBalLei = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label1: TLabel;
    CBStqCenCad: TdmkDBLookupComboBox;
    EdStqCenCad: TdmkEditCB;
    Label2: TLabel;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    DsStqCenLoc: TDataSource;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocCodUsu: TIntegerField;
    QrStqCenLocNome: TWideStringField;
    QrItem: TmySQLQuery;
    DsItem: TDataSource;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrLidos: TmySQLQuery;
    DsLidos: TDataSource;
    QrLidosNOMENIVEL1: TWideStringField;
    QrLidosGraCorCad: TIntegerField;
    QrLidosNOMECOR: TWideStringField;
    QrLidosNOMETAM: TWideStringField;
    QrLidosGraGru1: TIntegerField;
    QrLidosCodigo: TIntegerField;
    QrLidosControle: TIntegerField;
    QrLidosTipo: TSmallintField;
    QrLidosDataHora: TDateTimeField;
    QrLidosStqCenLoc: TIntegerField;
    QrLidosGraGruX: TIntegerField;
    QrLidosQtdLei: TFloatField;
    QrLidosSEQ: TIntegerField;
    QrLidosCODUSU_NIVEL1: TIntegerField;
    QrLidosCODUSU_COR: TIntegerField;
    QrTotal: TmySQLQuery;
    DsTotal: TDataSource;
    QrTotalQtdLei: TFloatField;
    Label11: TLabel;
    DBEdit4: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdLeitura: TEdit;
    EdQtdLei: TdmkEdit;
    PnLido: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    PnSimu: TPanel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit1: TdmkEdit;
    Button1: TButton;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    BtGrade: TBitBtn;
    BtOK: TBitBtn;
    QrLidosPecasLei: TFloatField;
    QrLidosPecasAnt: TFloatField;
    QrLidosPesoLei: TFloatField;
    QrLidosPesoAnt: TFloatField;
    QrLidosAreaM2Lei: TFloatField;
    QrLidosAreaM2Ant: TFloatField;
    QrLidosAreaP2Lei: TFloatField;
    QrLidosAreaP2Ant: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    CkFixo: TCheckBox;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    QrLidosCustoAll: TFloatField;
    EdValLei: TdmkEdit;
    Label10: TLabel;
    QrSoma: TMySQLQuery;
    QrSomaQtde: TFloatField;
    QrSomaPecas: TFloatField;
    QrSomaPeso: TFloatField;
    QrSomaAreaM2: TFloatField;
    QrSomaAreaP2: TFloatField;
    QrSomaStqCenLoc: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdStqCenCadExit(Sender: TObject);
    procedure EdStqCenCadEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure EdStqCenLocChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdStqCenLocExit(Sender: TObject);
    procedure QrLidosCalcFields(DataSet: TDataSet);
    procedure EdLeituraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure BtGradeClick(Sender: TObject);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure EdQtdLeiExit(Sender: TObject);
    procedure EdLeituraExit(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
    FStqCenCad: Integer;
    FTam20: Boolean;
    function ReopenItem(GraGruX: Integer): Boolean;
    procedure InsereItem();
    procedure ReopenStqCenLoc(Controle: Integer);
  public
    { Public declarations }
    procedure ReopenLidos(Controle: Integer);
  end;

  var
    FmStqBalLei: TFmStqBalLei;

implementation

uses UnMyObjects, Module, UMySQLModule, dmkGeral, StqBalCad, ModuleGeral,
  UnInternalConsts, MyDBCheck, GetValor, StqBalLeiIts, StqCenCad, ModProd,
  DmkDAC_PF, MyListas;

{$R *.DFM}

procedure TFmStqBalLei.BitBtn1Click(Sender: TObject);
begin
  if (QrLidos.State <> dsInactive) and (QrLidos.RecordCount > 0) then
  begin
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLidos, DBGrid1, 'stqbalits',
      ['Codigo', 'Controle', 'Tipo'], ['Codigo', 'Controle', 'Tipo'],
      istPergunta, '');
  end;
end;

procedure TFmStqBalLei.BtGradeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqBalLeiIts, FmStqBalLeiIts, afmoNegarComAviso) then
  begin
    FmStqBalLeiIts.ShowModal;
    FmStqBalLeiIts.Destroy;
    ReopenLidos(0);
  end;
end;

procedure TFmStqBalLei.BtOKClick(Sender: TObject);
var
  Tam, GraGruX, Seq: Integer;
begin
  Tam := Length(EdLeitura.Text);
  //
  if (Tam <= 9) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    if ReopenItem(GraGruX) then
      InsereItem();
  end;
end;

procedure TFmStqBalLei.InsereItem();
const
  Tipo = 1;
var
  //Centro,
  Codigo, Controle, StqCenCad, StqCenLoc, GraGruX: Integer;
  DataHora: String;
  QtdLei, QtdAnt, CustoAll: Double;
begin
  //Centro  := Geral.IMV(EdStqCenCad.Text);
  if (QrItem.State = dsInactive) or (QrItem.RecordCount = 0) then
  begin
    Application.MessageBox('Reduzido n�o definido!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;

  QtdLei := EdQtdLei.ValueVariant;
  CustoAll := EdValLei.ValueVariant;
  if (QtdLei <= 0) then
  begin
    Application.MessageBox('Informe a quantidade!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  StqCenLoc := QrStqCenLocControle.Value;//Geral.IMV(EdStqCenLoc.Text);
  StqCenCad := FmStqBalCad.QrStqBalCadStqCenCad.Value;
  GraGruX   := QrItemGraGruX.Value;
  Codigo    := FmStqBalCad.QrStqBalCadCodigo.Value;


  //verificar se n�o est� duplicando ??? Fazer caso precise

  // Saldo Anterior
  // ini 2023-08-12
(*
  FmStqBalCad.QrSoma.Close;
  FmStqBalCad.QrSoma.Params[00].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
  FmStqBalCad.QrSoma.Params[01].AsInteger := StqCenCad;
  FmStqBalCad.QrSoma.Params[02].AsInteger := GraGruX;
  UnDmkDAC_PF.AbreQuery(FmStqBalCad.QrSoma, Dmod.MyDB);
  QtdAnt := FmStqBalCad.QrSomaQtde.Value;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
  'SELECT StqCenLoc, SUM(Qtde) Qtde, SUM(Pecas) Pecas,  SUM(Peso) Peso,',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2',
  'FROM stqmovitsa',
  'WHERE Empresa=' + Geral.FF0(FmStqBalCad.QrStqBalCadEmpresa.Value),
  'AND StqCenCad=' + Geral.FF0(StqCenCad),
  'AND GraGruX=' + Geral.FF0(GraGruX),
  'AND StqCenLoc=' + Geral.FF0(StqCenLoc),
  'GROUP BY StqCenLoc',
  'ORDER BY StqCenLoc',
  '']);
  // fim 2023-08-12
  QtdAnt := QrSomaQtde.Value;
  //

  //
  Controle := DModG.BuscaProximoInteiro('stqbalits', 'Controle', 'Tipo', Tipo);
  //

  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalits', False, [
  'Tipo', 'DataHora', 'StqCenLoc',
  'GraGruX', 'QtdLei', 'QtdAnt',
  'CustoAll'], [ // 2023-08-12
  'Codigo', 'Controle'], [
  Tipo, DataHora, StqCenLoc,
  GraGruX, QtdLei, QtdAnt,
  CustoAll], [  // 2023-08-12
  Codigo, Controle], False) then
  begin
    EdLeitura.Text := '';
    EdQtdLei.ValueVariant := FmStqBalCad.QrStqBalCadBalQtdItem.Value;
    EdValLei.ValueVariant := 0.00;  // 2023-08-12
    EdLeitura.SetFocus;
    ReopenLidos(Controle);
  end;
end;

procedure TFmStqBalLei.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 0 then
    EdLeitura.SetFocus;
end;

procedure TFmStqBalLei.QrItemBeforeClose(DataSet: TDataSet);
begin
  PnSimu.Visible := True;
  PnLido.Visible := False;
end;

procedure TFmStqBalLei.QrLidosCalcFields(DataSet: TDataSet);
begin
  QrLidosSEQ.Value := QrLidos.RecNo;
end;

procedure TFmStqBalLei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqBalLei.Button1Click(Sender: TObject);
const
  Tick = 25;
var
  i: Integer;
begin
  EdLeitura.Text := '';
  //
  for i := 1 to Length(dmkEdit1.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit1.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit2.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit2.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit3.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit3.Text[i];
    sleep(Tick);
  end;
end;

procedure TFmStqBalLei.EdLeituraChange(Sender: TObject);
(*
var
  Tam: Integer;
*)
begin
  (*

  A leitora n�o mostra as mensagens no onChange mudado para o onExit

  Tam := Length(EdLeitura.Text);
  if Tam = 20 then
  begin
    if ReopenItem(Geral.IMV(Copy(EdLeitura.Text, 7, 6))) then
      if CkFixo.Checked then
      begin
        FTam20 := True;
        InsereItem();
        EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end;
  end else if Tam = 0 then QrItem.Close;
  *)
end;

procedure TFmStqBalLei.EdLeituraExit(Sender: TObject);
var
  Tam, GraGruX, Seq: Integer;
begin
  Tam := Length(EdLeitura.Text);
  //
  if (Tam = 20) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    if ReopenItem(GraGruX) then
      if CkFixo.Checked then
      begin
        FTam20 := True;
        InsereItem();
        EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end;
  end else
  if (Tam > 0) and (Tam <= 9) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    ReopenItem(GraGruX);
  end else
  if Tam = 0 then
    QrItem.Close;
end;

procedure TFmStqBalLei.EdLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  (*
  if Key = VK_RETURN then
  begin
    if Length(EdLeitura.Text) <= 9 then
      ReopenItem(Geral.IMV(EdLeitura.Text))
    else Application.MessageBox(PChar('Quantidade de caracteres inv�lida para ' +
    'localiza��o pelo reduzido!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  *)
end;

procedure TFmStqBalLei.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeitura.Text = '') and FTam20 then
  begin
    FTam20 := False;
    EdLeitura.SetFocus;
  end;
end;

procedure TFmStqBalLei.EdQtdLeiExit(Sender: TObject);
begin
  //EdQtdLei.Text := Geral.TFT(EdQtdLei.Text,
    //FmStqBalCad.QrStqBalCadCasasProd.Value, siPositivo);
end;

procedure TFmStqBalLei.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereItem();
end;

procedure TFmStqBalLei.EdStqCenCadChange(Sender: TObject);
begin
  EdStqCenLoc.ValueVariant := 0;
  CBStqCenLoc.KeyValue     := 0;
  if not EdStqCenCad.Focused then
    ReopenStqCenLoc(0);
end;

procedure TFmStqBalLei.EdStqCenCadEnter(Sender: TObject);
begin
  FStqCenCad := EdStqCenCad.ValueVariant;
end;

procedure TFmStqBalLei.EdStqCenCadExit(Sender: TObject);
begin
  if FStqCenCad <> EdStqCenCad.ValueVariant then
    ReopenStqCenLoc(0);
end;

procedure TFmStqBalLei.EdStqCenLocChange(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdStqCenLoc.ValueVariant <> Null)
  and
    (EdStqCenLoc.ValueVariant <> 0);
  //
  PnLeitura.Enabled := Habilita;
  BtGrade.Enabled   := Habilita;
  //
  if not EdStqCenLoc.Focused then
    ReopenLidos(0);
end;

procedure TFmStqBalLei.EdStqCenLocExit(Sender: TObject);
begin
  ReopenLidos(0);
end;

procedure TFmStqBalLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if PageControl1.ActivePageIndex = 0 then
    EdLeitura.SetFocus;
end;

procedure TFmStqBalLei.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  //EdLeitura.OnKeyDown := nil;
  //
  if CO_DMKID_APP = 6 then //Planning
    PageControl1.ActivePageIndex := 0
  else
    PageControl1.ActivePageIndex := 1;
end;

procedure TFmStqBalLei.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmStqBalLei.ReopenItem(GraGruX: Integer): Boolean;
begin
  Result := False;
  QrItem.Close;
  if GraGruX > 0 then
  begin
    QrItem.Params[0].AsInteger := GraGrux;
    UnDmkDAC_PF.AbreQuery(QrItem, Dmod.MyDB);
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      EdQtdLei.SetFocus;
      PnLido.Visible := True;
      PnSimu.Visible := False;
    end else
      Application.MessageBox('Reduzido n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmStqBalLei.ReopenLidos(Controle: Integer);
var
  StqCenLoc: Integer;
begin
  QrLidos.Close;
  QrTotal.Close;
  StqCenLoc := Geral.IMV(EdStqCenLoc.Text);
  if StqCenLoc <> 0 then
  begin
    StqCenLoc := QrStqCenLocControle.Value;
    //
    QrLidos.Params[00].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
    QrLidos.Params[01].AsInteger := StqCenLoc;
    UnDmkDAC_PF.AbreQuery(QrLidos, Dmod.MyDB);
    QrLidos.Locate('Controle', Controle, []);
    //
    QrTotal.Params[00].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
    QrTotal.Params[01].AsInteger := StqCenLoc;
    UnDmkDAC_PF.AbreQuery(QrTotal, Dmod.MyDB);
    //
  end;
end;

procedure TFmStqBalLei.ReopenStqCenLoc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqCenLoc, Dmod.MyDB, [
  'SELECT Controle, CodUsu, Nome ',
  'FROM stqcenloc scl ',
  'WHERE Codigo=' + Geral.FF0(QrStqCenCadCodigo.Value),
  'ORDER BY Nome ',
  '']);
  //
  if Controle <> 0 then
  begin
    QrStqCenLoc.Locate('Controle', Controle, []);
  end else
  begin
    if QrStqCenLoc.RecordCount = 1 then
    begin
      EdStqCenLoc.ValueVariant := QrStqCenLocControle.Value;
      CBStqCenLoc.KeyValue     := QrStqCenLocControle.Value;
    end;
  end;
end;

end.
