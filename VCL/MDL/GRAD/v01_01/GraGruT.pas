unit GraGruT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkLabel, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask, dmkDBEdit, MyDBCheck,
  Variants, dmkImage, UnDmkEnums;

type
  TFmGraGruT = class(TForm)
    Panel1: TPanel;
    EdGrade: TdmkEditCB;
    CBGrade: TdmkDBLookupComboBox;
    QrGraTamCad: TmySQLQuery;
    DsGraTamCad: TDataSource;
    QrGraTamCadCodigo: TIntegerField;
    QrGraTamCadCodUsu: TIntegerField;
    QrGraTamCadNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    Label1: TLabel;
    DBEdit1: TdmkDBEdit;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruT: TFmGraGruT;

implementation

uses UnMyObjects, Module, UMySQLModule, GraGruN, UnInternalConsts, UnGrade_Jan,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruT.BtOKClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  Nivel1 := FmGraGruN.QrGraGru1Nivel1.Value;
  if UMyMod.ExecSQLInsUpdFm(FmGraGruT, ImgTipo.SQLType, 'gragru1', Nivel1,
    Dmod.QrUpd) then
  begin
    FmGraGruN.ReopenGraGru1(Nivel1);
    Close;
  end;
end;

procedure TFmGraGruT.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruT.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if UpperCase(Application.Title) = 'SAFECAR' then
  begin
    EdGrade.ValueVariant := 0;
    CBGrade.KeyValue     := Null;
  end;
end;

procedure TFmGraGruT.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
end;

procedure TFmGraGruT.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruT.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  Grade_Jan.MostraFormGraTamCad(dmkValUsu1.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrGraTamCad.Close;
    UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
    QrGraTamCad.Locate('Codigo', VAR_CADASTRO, []);
    //
    EdGrade.ValueVariant := QrGraTamCadCodUsu.Value;
    CBGrade.KeyValue     := QrGraTamCadCodUsu.Value;
    CBGrade.SetFocus;
  end;
end;

end.
