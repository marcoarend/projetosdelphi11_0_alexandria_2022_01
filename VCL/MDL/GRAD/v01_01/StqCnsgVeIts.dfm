object FmStqCnsgVeIts: TFmStqCnsgVeIts
  Left = 339
  Top = 185
  Caption = 'STQ-MOVIM-009 :: Consigna'#231#227'o - Itens de Venda'
  ClientHeight = 361
  ClientWidth = 667
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 580
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 667
    Height = 205
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnData: TPanel
      Left = 0
      Top = 56
      Width = 667
      Height = 149
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object CkContinuar: TCheckBox
        Left = 8
        Top = 128
        Width = 112
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 2
      end
      object Panel8: TPanel
        Left = 0
        Top = 64
        Width = 667
        Height = 61
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 277
          Top = 0
          Width = 390
          Height = 61
          Align = alClient
          Caption = ' Quantidade e custo total: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object LaQtde: TLabel
            Left = 8
            Top = 16
            Width = 68
            Height = 13
            Caption = '??? Qtde ???:'
          end
          object Label8: TLabel
            Left = 88
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Valor total:'
            Enabled = False
          end
          object Label9: TLabel
            Left = 200
            Top = 16
            Width = 57
            Height = 13
            Caption = 'Custo Frete:'
            Enabled = False
          end
          object Label20: TLabel
            Left = 288
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Custo total:'
            Enabled = False
          end
          object SbPreco: TSpeedButton
            Left = 173
            Top = 32
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SbPrecoClick
          end
          object EdQtde: TdmkEdit
            Left = 8
            Top = 32
            Width = 77
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PLE'
            UpdCampo = 'PLE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdQtdeRedefinido
          end
          object EdCustoBuy: TdmkEdit
            Left = 88
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoBuy'
            UpdCampo = 'CustoBuy'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCustoBuyRedefinido
          end
          object EdCustoFrt: TdmkEdit
            Left = 200
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoFrt'
            UpdCampo = 'CustoFrt'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCustoFrtRedefinido
          end
          object EdValorAll: TdmkEdit
            Left = 288
            Top = 32
            Width = 86
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'CustoAll'
            UpdCampo = 'CustoAll'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 0
          Width = 277
          Height = 61
          Align = alLeft
          Caption = 'Estoque: '
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 16
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
            FocusControl = DBEdit6
          end
          object Label4: TLabel
            Left = 88
            Top = 16
            Width = 53
            Height = 13
            Caption = 'Custo total:'
            FocusControl = DBEdit7
          end
          object Label2: TLabel
            Left = 176
            Top = 16
            Width = 61
            Height = 13
            Caption = 'Custo m'#233'dio:'
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 32
            Width = 77
            Height = 21
            TabStop = False
            DataField = 'Qtde'
            DataSource = DsEstoque
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 88
            Top = 32
            Width = 87
            Height = 21
            TabStop = False
            DataField = 'CustoAll'
            DataSource = DsEstoque
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 180
            Top = 32
            Width = 87
            Height = 21
            TabStop = False
            DataField = 'CusMed'
            DataSource = DsEstoque
            TabOrder = 2
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 667
        Height = 64
        Align = alTop
        Caption = 'Centro de sa'#237'da: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 663
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object dmkLabel3: TdmkLabel
            Left = 4
            Top = 1
            Width = 90
            Height = 13
            Caption = 'Centro de estoque:'
            UpdType = utYes
            SQLType = stNil
          end
          object Label7: TLabel
            Left = 390
            Top = 1
            Width = 164
            Height = 13
            Caption = 'Localiza'#231#227'o no centro de estoque:'
          end
          object EdStqCenCad: TdmkEditCB
            Left = 4
            Top = 17
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdStqCenCadChange
            DBLookupComboBox = CBStqCenCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenCad: TdmkDBLookupComboBox
            Left = 60
            Top = 17
            Width = 325
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsStqCenCad
            TabOrder = 1
            dmkEditCB = EdStqCenCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdStqCenLoc: TdmkEditCB
            Left = 390
            Top = 17
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdStqCenLocRedefinido
            DBLookupComboBox = CBStqCenLoc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBStqCenLoc: TdmkDBLookupComboBox
            Left = 446
            Top = 17
            Width = 203
            Height = 21
            DropDownRows = 2
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsStqCenLoc
            TabOrder = 3
            dmkEditCB = EdStqCenLoc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 667
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PnGGX: TPanel
        Left = 85
        Top = 0
        Width = 582
        Height = 56
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label12: TLabel
          Left = 0
          Top = 8
          Width = 76
          Height = 13
          Caption = 'Nome do grupo:'
        end
        object Label13: TLabel
          Left = 268
          Top = 8
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label14: TLabel
          Left = 368
          Top = 8
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object Label1: TLabel
          Left = 424
          Top = 8
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object Label5: TLabel
          Left = 484
          Top = 8
          Width = 31
          Height = 13
          Caption = 'Pre'#231'o:'
          FocusControl = DBEdit8
        end
        object DBEdit1: TDBEdit
          Left = 0
          Top = 24
          Width = 265
          Height = 21
          TabStop = False
          DataField = 'NO_GG1'
          DataSource = DsGraGruX
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 268
          Top = 24
          Width = 97
          Height = 21
          TabStop = False
          DataField = 'NO_COR'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 368
          Top = 24
          Width = 53
          Height = 21
          TabStop = False
          DataField = 'NO_TAM'
          DataSource = DsGraGruX
          TabOrder = 2
        end
        object DBEdit5: TDBEdit
          Left = 423
          Top = 24
          Width = 58
          Height = 21
          DataField = 'No_SIGLA'
          DataSource = DsGraGruX
          TabOrder = 3
          OnChange = DBEdit5Change
        end
        object DBEdit8: TDBEdit
          Left = 484
          Top = 24
          Width = 77
          Height = 21
          DataField = 'Preco'
          DataSource = DsPreco
          TabOrder = 4
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 85
        Height = 56
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label11: TLabel
          Left = 8
          Top = 8
          Width = 69
          Height = 13
          Caption = 'Reduzido: [F4]'
        end
        object SpeedButton1: TSpeedButton
          Left = 64
          Top = 24
          Width = 21
          Height = 21
          Caption = '?'
          OnClick = SpeedButton1Click
        end
        object EdGraGruX: TdmkEdit
          Left = 8
          Top = 24
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
          OnExit = EdGraGruXExit
          OnKeyDown = EdGraGruXKeyDown
          OnRedefinido = EdGraGruXRedefinido
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 667
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 619
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 571
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 366
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 366
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 366
        Height = 32
        Caption = 'Consigna'#231#227'o - Itens de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 253
    Width = 667
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 663
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 297
    Width = 667
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 663
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 519
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX,  '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM, '
      'med.Nome, med.Sigla, med.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ')
    Left = 468
    Top = 232
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNo_SIGLA: TWideStringField
      FieldName = 'No_SIGLA'
      Size = 30
    end
    object QrGraGruXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGraGruXGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGraGruXFracio: TIntegerField
      FieldName = 'Fracio'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 468
    Top = 280
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM stqcenloc '
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 172
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 172
    Top = 276
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 252
    Top = 228
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 252
    Top = 276
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    Left = 324
    Top = 228
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueCustoAll: TFloatField
      FieldName = 'CustoAll'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEstoqueCusMed: TFloatField
      FieldName = 'CusMed'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 324
    Top = 276
  end
  object QrPreco: TMySQLQuery
    Database = Dmod.MyDB
    Left = 388
    Top = 228
    object QrPrecoPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 388
    Top = 276
  end
end
