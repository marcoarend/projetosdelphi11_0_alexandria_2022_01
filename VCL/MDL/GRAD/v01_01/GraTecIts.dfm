object FmGraTecIts: TFmGraTecIts
  Left = 339
  Top = 185
  Caption = 'PRD-TECID-002 :: Composi'#231#227'o de Tecido'
  ClientHeight = 450
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 224
    Width = 494
    Height = 162
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object SpeedButton1: TSpeedButton
      Left = 359
      Top = 68
      Width = 21
      Height = 22
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label4: TLabel
      Left = 384
      Top = 52
      Width = 66
      Height = 13
      Caption = 'Porcentagem:'
    end
    object Label5: TLabel
      Left = 12
      Top = 8
      Width = 50
      Height = 13
      Caption = 'ID Tecido:'
    end
    object Label6: TLabel
      Left = 112
      Top = 8
      Width = 74
      Height = 13
      Caption = 'ID composi'#231#227'o:'
    end
    object Label8: TLabel
      Left = 12
      Top = 52
      Width = 47
      Height = 13
      Caption = 'Fibra [F3]:'
    end
    object EdGraFibCad: TdmkEditCB
      Left = 12
      Top = 68
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraFibCadChange
      OnKeyDown = EdGraFibCadKeyDown
      DBLookupComboBox = CBGraFibCad
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdSigla: TdmkEdit
      Left = 68
      Top = 68
      Width = 40
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdSiglaChange
      OnExit = EdSiglaExit
      OnKeyDown = EdSiglaKeyDown
    end
    object CBGraFibCad: TdmkDBLookupComboBox
      Left = 108
      Top = 68
      Width = 249
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsGraFibCad
      TabOrder = 4
      OnKeyDown = CBGraFibCadKeyDown
      dmkEditCB = EdGraFibCad
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPorcentagem: TdmkEdit
      Left = 384
      Top = 68
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Percent'
      UpdCampo = 'Percent'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 112
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object dmkDBEdit1: TdmkDBEdit
      Left = 12
      Top = 24
      Width = 97
      Height = 21
      DataField = 'Codigo'
      DataSource = FmGraTecCad.DsGraTecCad
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 98
      Width = 494
      Height = 64
      Align = alBottom
      TabOrder = 6
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 490
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 372
          Top = 0
          Width = 118
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 10
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 20
          Top = 4
          Width = 90
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 48
    Width = 494
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 1
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 148
      Top = 4
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label3: TLabel
      Left = 64
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label7: TLabel
      Left = 424
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Percentual:'
      FocusControl = DBEdPercent
    end
    object DBEdCodigo: TDBEdit
      Left = 4
      Top = 20
      Width = 56
      Height = 21
      Hint = 'N'#186' do banco'
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmGraTecCad.DsGraTecCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
    end
    object DBEdNome: TDBEdit
      Left = 148
      Top = 20
      Width = 273
      Height = 21
      Hint = 'Nome do banco'
      Color = clWhite
      DataField = 'Nome'
      DataSource = FmGraTecCad.DsGraTecCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object DBEdit1: TDBEdit
      Left = 64
      Top = 20
      Width = 80
      Height = 21
      DataField = 'CodUsu'
      DataSource = FmGraTecCad.DsGraTecCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object DBEdPercent: TDBEdit
      Left = 424
      Top = 20
      Width = 56
      Height = 21
      DataField = 'Percent'
      DataSource = FmGraTecCad.DsGraTecCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnChange = DBEdPercentChange
    end
  end
  object DBGItens: TdmkDBGrid
    Left = 0
    Top = 138
    Width = 494
    Height = 86
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'CODUSUFIB'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Sigla'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Percent'
        Title.Caption = 'Percentual'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEFIB'
        Title.Caption = 'Fibra (descri'#231#227'o)'
        Width = 205
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODUSUFIB'
        Title.Caption = 'ID'
        Visible = True
      end>
    Color = clWindow
    DataSource = FmGraTecCad.DsGraTecIts
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODUSUFIB'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Sigla'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Percent'
        Title.Caption = 'Percentual'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEFIB'
        Title.Caption = 'Fibra (descri'#231#227'o)'
        Width = 205
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODUSUFIB'
        Title.Caption = 'ID'
        Visible = True
      end>
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 494
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 446
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 398
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 275
        Height = 32
        Caption = 'Composi'#231#227'o de Tecido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 275
        Height = 32
        Caption = 'Composi'#231#227'o de Tecido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 275
        Height = 32
        Caption = 'Composi'#231#227'o de Tecido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 97
    Width = 494
    Height = 41
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 490
      Height = 24
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBControla: TGroupBox
    Left = 0
    Top = 386
    Width = 494
    Height = 64
    Align = alBottom
    TabOrder = 5
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 490
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 380
        Top = 0
        Width = 110
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 112
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 204
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
    end
  end
  object QrGraFibCad: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Sigla'
      'FROM grafibcad'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    object QrGraFibCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraFibCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraFibCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraFibCadSigla: TWideStringField
      DisplayWidth = 5
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object DsGraFibCad: TDataSource
    DataSet = QrGraFibCad
    Left = 40
    Top = 12
  end
  object QrPesq1: TMySQLQuery
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM grafibcad'
      'WHERE Sigla = :P0')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrPesq2: TMySQLQuery
    SQL.Strings = (
      'SELECT Sigla'
      'FROM grafibcad'
      'WHERE CodUsu = :P0')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdGraFibCad
    Panel = FmGraTecCad.PainelEdita
    QryCampo = 'GraFibCad'
    UpdCampo = 'GraFibCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 124
    Top = 12
  end
  object QrSum1: TMySQLQuery
    SQL.Strings = (
      'SELECT SUM(gti.Percent) Percent'
      'FROM gratecits gti'
      'WHERE gti.Codigo=:P0'
      '')
    Left = 416
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum1Percent: TFloatField
      FieldName = 'Percent'
    end
  end
end
