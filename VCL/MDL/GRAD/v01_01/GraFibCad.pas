unit GraFibCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, unDmkProcFunc, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkImage, UnDmkEnums;

type
  TFmGraFibCad = class(TForm)
    PainelDados: TPanel;
    DsGraFibCad: TDataSource;
    QrGraFibCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrGraFibCadCodigo: TIntegerField;
    QrGraFibCadCodUsu: TIntegerField;
    QrGraFibCadNome: TWideStringField;
    EdSigla: TdmkEdit;
    Label4: TLabel;
    QrGraFibCadSigla: TWideStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraFibCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraFibCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmGraFibCad: TFmGraFibCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraFibCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraFibCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraFibCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraFibCad.DefParams;
begin
  VAR_GOTOTABELA := 'GraFibCad';
  VAR_GOTOMYSQLTABLE := QrGraFibCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Sigla');
  VAR_SQLx.Add('FROM grafibcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraFibCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraFibCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmGraFibCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraFibCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraFibCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraFibCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraFibCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraFibCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraFibCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraFibCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraFibCad.BtAlteraClick(Sender: TObject);
begin
  if (QrGraFibCad.State = dsInactive) or (QrGraFibCad.RecordCount = 0) then Exit;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraFibCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'grafibcad');
end;

procedure TFmGraFibCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraFibCadCodigo.Value;
  Close;
end;

procedure TFmGraFibCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = 'Defina uma descri��o!', EdNome, '') then Exit;
  //
(*
  Codigo := UMyMod.BuscaEmLivreY_Def('GraFibCad', 'Codigo', ImgTipo.SQLType,
    QrGraFibCadCodigo.Value);
*)
  //
  Codigo := EdCodigo.ValueVariant;
  Codigo := UMyMod.BPGS1I32('grafibcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraFibCad, PainelEdit,
    'GraFibCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, true) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraFibCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraFibCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraFibCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraFibCad', 'Codigo');
end;

procedure TFmGraFibCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrGraFibCad, [PainelDados],
    [PainelEdita], EdSigla, ImgTipo, 'grafibcad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraFibCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdSigla.ValueVariant := '';
  EdNome.ValueVariant  := '';
end;

procedure TFmGraFibCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmGraFibCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraFibCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraFibCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmGraFibCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraFibCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraFibCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraFibCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraFibCad.QrGraFibCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraFibCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraFibCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraFibCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GraFibCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraFibCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraFibCad.QrGraFibCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraFibCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

