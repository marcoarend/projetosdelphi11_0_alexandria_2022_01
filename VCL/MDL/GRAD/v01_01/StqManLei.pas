unit StqManLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, Variants, Grids, DBGrids,
  ComCtrls, dmkImage, UnDmkEnums, UnProjGroup_Consts;

type
  TFmStqManLei = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    DsStqCenLoc: TDataSource;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocCodUsu: TIntegerField;
    QrStqCenLocNome: TWideStringField;
    QrItem: TmySQLQuery;
    DsItem: TDataSource;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrLidos: TmySQLQuery;
    DsLidos: TDataSource;
    QrTotal: TmySQLQuery;
    DsTotal: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdLeitura: TEdit;
    EdQtdLei: TdmkEdit;
    PnLido: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    BtGrade: TBitBtn;
    BtOK: TBitBtn;
    QrItemCustoPreco: TFloatField;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    QrLidosCODUSU_NIVEL1: TIntegerField;
    QrLidosNOMENIVEL1: TWideStringField;
    QrLidosGraCorCad: TIntegerField;
    QrLidosCODUSU_COR: TIntegerField;
    QrLidosNOMECOR: TWideStringField;
    QrLidosNOMETAM: TWideStringField;
    QrLidosGraGru1: TIntegerField;
    QrLidosCodigo: TIntegerField;
    QrLidosControle: TIntegerField;
    QrLidosGraGruX: TIntegerField;
    QrLidosQtde: TFloatField;
    QrLidosValor: TFloatField;
    QrLidosLk: TIntegerField;
    QrLidosDataCad: TDateField;
    QrLidosDataAlt: TDateField;
    QrLidosUserCad: TIntegerField;
    QrLidosUserAlt: TIntegerField;
    QrLidosAlterWeb: TSmallintField;
    QrLidosAtivo: TSmallintField;
    QrLidosPreco: TFloatField;
    QrTotalQtdLei: TFloatField;
    QrTotalValor: TFloatField;
    QrLidosSEQ: TIntegerField;
    PnSimu: TPanel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit1: TdmkEdit;
    Button1: TButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    CkFixo: TCheckBox;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label1: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    Label11: TLabel;
    DBEdit4: TDBEdit;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    BitBtn2: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdStqCenCadEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrLidosCalcFields(DataSet: TDataSet);
    procedure EdLeituraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure BtGradeClick(Sender: TObject);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure EdLeituraExit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
    FOriCtrl: Integer;
    FTam20: Boolean;
    function ReopenItem(GraGruX: Integer): Boolean;
    procedure InsereItem();
    procedure ReopenStqCenLoc(Controle: Integer);
  public
    { Public declarations }
    procedure ReopenLidos(Controle: Integer);
  end;

  var
  FmStqManLei: TFmStqManLei;


implementation

uses UnMyObjects, Module, UMySQLModule, dmkGeral, StqManCad, ModuleGeral,
  UnInternalConsts, MyDBCheck, GetValor, StqManLeiIts, StqCenCad, UnGrade_Tabs,
  ModProd, DmkDAC_PF, UnGrade_Jan, MyListas;

{$R *.DFM}

{
const
  FTipoStqMovIts = 099; VAR_FATID_0099
}

procedure TFmStqManLei.BitBtn1Click(Sender: TObject);
  function ExcluiItemAtual(): Boolean;
  var
    OriCodi, OriCtrl: Integer;
  begin
    OriCodi := QrLidosCodigo.Value;
    OriCtrl := QrLidosControle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 AND OriCtrl=:P2 ');
    Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0099;
    Dmod.QrUpd.Params[01].AsInteger := OriCodi;
    Dmod.QrUpd.Params[02].AsInteger := OriCtrl;
    UMyMod.ExecutaQuery(Dmod.QrUpd);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsb ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 AND OriCtrl=:P2 ');
    Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0099;
    Dmod.QrUpd.Params[01].AsInteger := OriCodi;
    Dmod.QrUpd.Params[02].AsInteger := OriCtrl;
    UMyMod.ExecutaQuery(Dmod.QrUpd);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmanits ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Controle=:P1 ');
    Dmod.QrUpd.Params[00].AsInteger := OriCodi;
    Dmod.QrUpd.Params[01].AsInteger := OriCtrl;
    UMyMod.ExecutaQuery(Dmod.QrUpd);
    //
    Result := True;
  end;
var
  i: integer;
begin
  if (QrLidos.State <> dsInactive) and (QrLidos.RecordCount > 0) then
  begin
    if DBGrid1.SelectedRows.Count > 1 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos itens selecionados?') = ID_YES then
      begin
        with DBGrid1.DataSource.DataSet do
        for i:= 0 to DBGrid1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBGrid1.SelectedRows.Items[i]));
          GotoBookmark(DBGrid1.SelectedRows.Items[i]);
          ExcluiItemAtual()
        end;
      end;
    end else begin
      if Geral.MB_Pergunta('Confirma a exclus�o do item selecionado?') = ID_YES then
        ExcluiItemAtual();
    end;
    ReopenLidos(0);
    FmStqManCad.ReopenGrupos(FmStqManCad.QrGruposNivel1.Value);
  end;
end;

procedure TFmStqManLei.BitBtn2Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN(0, FmStqManCad.QrStqManCadGraCusPrc.Value, tpGraCusPrc);
end;

procedure TFmStqManLei.BtGradeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqManLeiIts, FmStqManLeiIts, afmoNegarComAviso) then
  begin
    FmStqManLeiIts.ShowModal;
    FmStqManLeiIts.Destroy;
    ReopenLidos(0);
    FmStqManCad.ReopenGrupos(FmStqManCad.QrGruposNivel1.Value);
  end;
end;

procedure TFmStqManLei.BtOKClick(Sender: TObject);
var
  Tam, GraGruX, Seq: Integer;
begin
  Tam := Length(EdLeitura.Text);
  //
  if (Tam <= 6) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    if ReopenItem(GraGruX) then
      InsereItem();
  end;
end;

procedure TFmStqManLei.InsereItem();
var
  Controle, StqCenCad, StqCenLoc, GraGruX,
  IDCtrl, OriCodi, Empresa, OriCnta, AlterWeb, Ativo: Integer;
  DataHora, Msg: String;
  Qtd, Qtde, Preco, Valor: Double;
  Continua: Word;
begin
  Controle := 0;
  //
  if (QrItem.State = dsInactive) or (QrItem.RecordCount = 0) then
  begin
    Geral.MB_Aviso('Reduzido n�o definido!');
    Exit;
  end;
  // FmStqManCad.QrStqManCadEntraSai.Value > definir sinal (+ ou -)
  //  dados gerais
  DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  OriCodi   := FmStqManCad.QrStqManCadCodigo.Value;
  Empresa   := FmStqManCad.QrStqManCadEmpresa.Value;
  OriCnta   := 0;//Volume no faturamento. aqui n�o usa ainda (est� livre).
  StqCenCad := FmStqManCad.QrStqManCadStqCenCad.Value;
  StqCenLoc := EdStqCenLoc.ValueVariant;
  GraGruX   := QrItemGraGruX.Value;
  AlterWeb  := 1;
  Ativo     := 0;
  //
  if StqCenLoc <> 0 then
    StqCenLoc := QrStqCenCadCodigo.Value;
  // Saldo
  Continua := DmProd.VerificaEstoqueProduto(Geral.DMV(EdQtdLei.Text),
                FmStqManCad.QrStqManCadEntraSai.Value, GraGruX, StqCenCad,
                FmStqManCad.QrStqManCadFisRegCad.Value, OriCodi, OriCnta,
                Empresa, FmStqManCad.QrStqManCadFatSemEstq.Value, False, Msg);
  //
  Qtd  := Geral.DMV(EdQtdLei.Text);
  Qtde := Qtd * FmStqManCad.QrStqManCadFator.Value;
  if (Qtde = 0) then
  begin
    Geral.MB_Aviso('Informe a quantidade!');
    Exit;
  end;
  //
  Preco := QrItemCustoPreco.Value;
  Valor := Round(Preco * Qtde * 100) / 100;
  //
  if Valor = 0 then
  begin
    Geral.MB_Aviso('Valor (ou pre�o) n�o definido!');
    Exit;
  end;
  if Continua = ID_YES then
  begin
    FOriCtrl   := 0;
    FOriCtrl   := DModG.BuscaProximoCodigoInt('controle', 'stqmov099', '', FOriCtrl);
    //
    IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
    'DataHora', 'Tipo', 'OriCodi',
    'OriCtrl', 'Empresa',
    'StqCenCad', 'StqCenLoc',
    'GraGruX', 'Qtde', 'Baixa', 'OriCnta',
    'OriPart', 'AlterWeb', 'Ativo'], [
    'IDCtrl'], [
    DataHora, VAR_FATID_0099, OriCodi,
    FOriCtrl, Empresa,
    StqCenCad, StqCenLoc,
    GraGruX, Qtd, FmStqManCad.QrStqManCadEntraSai.Value, OriCnta,
    FOriCtrl, AlterWeb, Ativo], [
    IDCtrl], False) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmanits', False, [
      'Codigo', 'GraGruX', 'Qtde', 'Preco', 'Valor'], ['Controle'], [
      OriCodi, GraGruX, Qtde, Preco, Valor], [FOriCtrl], True) then
      begin
        EdLeitura.Text := '';
        EdQtdLei.ValueVariant := 1;
        EdLeitura.SetFocus;
        ReopenLidos(Controle);
        FmStqManCad.ReopenGrupos(FmStqManCad.QrGruposNivel1.Value);
      end;
    end;
  end;
  {
  Parei aqui!!!
  mostrar grades
  ativo=1 quando encerrar
  estoque somente de ativo=1
  exclus�o de itens
  }
end;

procedure TFmStqManLei.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 0 then
    EdLeitura.SetFocus;
end;

procedure TFmStqManLei.QrItemBeforeClose(DataSet: TDataSet);
begin
  //PnSimu.Visible := True;
  PnLido.Visible := False;
end;

procedure TFmStqManLei.QrLidosCalcFields(DataSet: TDataSet);
begin
  QrLidosSEQ.Value := QrLidos.RecNo;
end;

procedure TFmStqManLei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqManLei.Button1Click(Sender: TObject);
const
  Tick = 25;
var
  i: Integer;
begin
  EdLeitura.Text := '';
  //
  for i := 1 to Length(dmkEdit1.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit1.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit2.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit2.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit3.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit3.Text[i];
    sleep(Tick);
  end;
end;

procedure TFmStqManLei.EdLeituraChange(Sender: TObject);
(*
var
  Tam: Integer;
*)
begin
(*
  A leitora n�o mostra as mensagens no onChange mudado para o onExit

  Tam := Length(EdLeitura.Text);
  if Tam = 20 then
  begin
    if ReopenItem(Geral.IMV(Copy(EdLeitura.Text, 7, 6))) then
      if CkFixo.Checked then
      begin
        FTam20 := True;
        InsereItem();
        EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end;
  end else if Tam = 0 then QrItem.Close;
*)
end;

procedure TFmStqManLei.EdLeituraExit(Sender: TObject);
var
  Tam, GraGruX, Seq: Integer;
begin
  Tam := Length(EdLeitura.Text);
  //
  if (Tam = 20) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    if ReopenItem(GraGruX) then
    begin
      if CkFixo.Checked then
      begin
        FTam20 := True;
        InsereItem();
        EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end;
    end;
  end else
  if Tam = 0 then
    QrItem.Close;
end;

procedure TFmStqManLei.EdLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  (*
  N�o � necess�rio

  if Key = VK_RETURN then
  begin
    if Length(EdLeitura.Text) <= 6 then
      ReopenItem(Geral.IMV(EdLeitura.Text))
    else Geral.MB_Aviso('Quantidade de caracteres inv�lida para ' +
    'localiza��o pelo reduzido!');
  end;
  *)
end;

procedure TFmStqManLei.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeitura.Text = '') and FTam20 then
  begin
    FTam20 := False;
    EdLeitura.SetFocus;
  end;
end;

procedure TFmStqManLei.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereItem();
end;

procedure TFmStqManLei.EdStqCenCadChange(Sender: TObject);
begin
{
  EdStqCenLoc.ValueVariant := 0;
  CBStqCenLoc.KeyValue     := 0;
  if not EdStqCenCad.Focused then
}
    ReopenStqCenLoc(0);
end;

procedure TFmStqManLei.EdStqCenCadEnter(Sender: TObject);
begin
{
  FStqCenCad := EdStqCenCad.ValueVariant;
}
end;

procedure TFmStqManLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if PageControl1.ActivePageIndex = 0 then
    EdLeitura.SetFocus;
end;

procedure TFmStqManLei.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  //
  if CO_DMKID_APP = 6 then //Planning
    PageControl1.ActivePageIndex := 0
  else
    PageControl1.ActivePageIndex := 1;
  //
  PnLeitura.Enabled := True;
  BtGrade.Enabled   := True;
  ReopenLidos(0);
end;

procedure TFmStqManLei.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmStqManLei.ReopenItem(GraGruX: Integer): Boolean;
begin
  Result := False;
  QrItem.Close;
  if GraGruX > 0 then
  begin
(*
    QrItem.Params[00].AsInteger := FmStqManCad.QrStqManCadGraCusPrc.Value;
    QrItem.Params[01].AsInteger := GraGrux;
    UnDmkDAC_PF.AbreQuery(QrItem, Dmod.MyDB);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrItem, Dmod.MyDB, [
    'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, ',
    'gcc.Nome NOMECOR,  gti.Nome NOMETAM, ',
    'ggx.Controle GraGruX, ggx.GraGru1, gri.Preco CustoPreco ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN tabeprcgri gri ON gri.GraGruX=ggx.Controle ',
    '  AND gri.TabePrcCab=' + Geral.FF0(FmStqManCad.QrStqManCadGraCusPrc.Value),
    'WHERE ggx.Controle=' + Geral.FF0(GraGrux),
    '']);
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      EdQtdLei.SetFocus;
      PnLido.Visible := True;
      //PnSimu.Visible := False;
    end else
      Geral.MB_Aviso('Reduzido n�o localizado!');
  end;
end;

procedure TFmStqManLei.ReopenLidos(Controle: Integer);
var
  StqCenLoc: Integer;
begin
  QrLidos.Close;
  QrTotal.Close;
  StqCenLoc := QrStqCenCadCodigo.Value;
  if StqCenLoc <> 0 then
  begin
    QrLidos.Params[00].AsInteger := FmStqManCad.QrStqManCadCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrLidos, Dmod.MyDB);
    QrLidos.Locate('Controle', Controle, []);
    //
    QrTotal.Params[00].AsInteger := FmStqManCad.QrStqManCadCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrTotal, Dmod.MyDB);
    //
  end;
end;

procedure TFmStqManLei.ReopenStqCenLoc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqCenLoc, Dmod.MyDB, [
    'SELECT Controle, CodUsu, Nome ',
    'FROM stqcenloc scl ',
    'WHERE Codigo=' + Geral.FF0(QrStqCenCadCodigo.Value),
    'ORDER BY Nome ',
    '']);
  if Controle <> 0 then
  begin
    QrStqCenLoc.Locate('Controle', Controle, []);
  end else
  begin
    if QrStqCenLoc.RecordCount = 1 then
    begin
      EdStqCenLoc.ValueVariant := QrStqCenLocControle.Value;
      CBStqCenLoc.KeyValue     := QrStqCenLocControle.Value;
    end;
  end;
end;

end.
