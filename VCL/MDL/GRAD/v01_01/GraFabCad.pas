unit GraFabCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  dmkGeral, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  Variants, UnDmkProcFunc, UnDmkEnums, dmkCheckBox, UnProjGroup_Consts;

type
  TFmGraFabCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrGraFabCad: TmySQLQuery;
    DsGraFabCad: TDataSource;
    QrGraFabMar: TmySQLQuery;
    DsGraFabMar: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrGraFabCadCodigo: TIntegerField;
    QrGraFabCadCodUsu: TIntegerField;
    QrGraFabCadNome: TWideStringField;
    Label3: TLabel;
    EdCodUsu: TdmkEdit;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    CkAtivo: TdmkCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrGraFabCadAtivo: TIntegerField;
    Importa1: TMenuItem;
    Importa2: TMenuItem;
    QrGraFabMarNO_Marca: TWideStringField;
    QrGraFabMarCodigo: TIntegerField;
    QrGraFabMarControle: TIntegerField;
    QrGraFabMarGraFabMCd: TIntegerField;
    QrGraFabMarNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraFabCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraFabCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraFabCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure Importa1Click(Sender: TObject);
    procedure Importa2Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCadastroMarca(SQLType: TSQLType);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure LocCodByControle(Controle: Integer);
    procedure ReopenGraFabMar(Controle: Integer);
  end;

var
  FmGraFabCad: TFmGraFabCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, GraFabMar, DmkDAC_PF, CfgCadLista,
  MyListas;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraFabCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraFabCad.LocCodByControle(Controle: Integer);
var
  Codigo: Integer;
  Query: TmySQLQuery;
begin
  Query := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM grafabmar ',
      'WHERE Controle = ' + Geral.FF0(COntrole),
      '']);
    if Query.RecordCount > 0 then
    begin
      Codigo := Query.FieldByName('Codigo').AsInteger;

      LocCod(Codigo, Codigo);
      ReopenGraFabMar(Controle);
    end;
  finally
    Query.Free;
  end;
end;

procedure TFmGraFabCad.MostraCadastroMarca(SQLType: TSQLType);
begin
  if CO_SIGLA_APP <> 'TREN' then
  begin
    if QrGraFabCadCodigo.Value = 0 then
      Exit;
    if (SQLType = stUpd) and (QrGraFabMarCodigo.Value = 0) then
      Exit;
  end;
  //
  if DBCheck.CriaFm(TFmGraFabMar, FmGraFabMar, afmoNegarComAviso) then
  begin
    FmGraFabMar.ImgTipo.SQLType := SQLType;
    FmGraFabMar.FQrCab := QrGraFabCad;
    FmGraFabMar.FDsCab := DsGraFabCad;
    FmGraFabMar.FQrIts := QrGraFabMar;
    if SQLType = stIns then
      //?
    else
    begin
      FmGraFabMar.EdControle.ValueVariant := QrGraFabMarControle.Value;
      //FmGraFabMar.EdNome.Text := QrGraFabMarNome.Value;
      FmGraFabMar.EdGraFabMCd.ValueVariant := QrGraFabMarGraFabMCd.Value;
      FmGraFabMar.CBGraFabMCd.KeyValue := QrGraFabMarGraFabMCd.Value;
      FmGraFabMar.EdNome.ValueVariant := QrGraFabMarNome.Value;
    end;
    FmGraFabMar.ShowModal;
    FmGraFabMar.Destroy;
  end;
end;

procedure TFmGraFabCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrGraFabCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrGraFabCad, QrGraFabMar);
end;

procedure TFmGraFabCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrGraFabCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrGraFabMar);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrGraFabMar);
end;

procedure TFmGraFabCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraFabCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraFabCad.DefParams;
begin
  VAR_GOTOTABELA := 'grafabcad';
  VAR_GOTOMYSQLTABLE := QrGraFabCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM grafabcad');
  VAR_SQLx.Add('WHERE Codigo > -999999999');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraFabCad.EdCodigoChange(Sender: TObject);
begin
  EdCodUsu.ValueVariant := EdCodigo.ValueVariant;
end;

procedure TFmGraFabCad.Importa1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'GraFabCad', 120, ncGerlSeq1,
  'Fabricantes',
  [], False, Null(*Maximo*), [], [], False, 0, ', CodUsu=Codigo');
end;

procedure TFmGraFabCad.Importa2Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'GraFabMar', 120, ncGerlSeq1,
  'Marcas de Fabricates',
  [], False, Null(*Maximo*), [], [], False, 0, '');
end;

procedure TFmGraFabCad.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastroMarca(stUpd);
end;

procedure TFmGraFabCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + slineBreak +
    Caption + slineBreak + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmGraFabCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraFabCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraFabCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'grafabmar', 'Controle', QrGraFabMarControle.Value, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrGraFabMar,
      QrGraFabMarControle, QrGraFabMarControle.Value);
    ReopenGraFabMar(Controle);
  end;
end;

procedure TFmGraFabCad.ReopenGraFabMar(Controle: Integer);
begin
(*
  QrGraFabMar.Close;
  QrGraFabMar.Params[0].AsInteger := QrGraFabCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraFabMar, Dmod.MyDB);
*)
  UnDMkDAC_PF.AbreMySQLQuery0(QrGraFabMar, Dmod.MyDB, [
  'SELECT gfm.Nome, gmc.Nome NO_Marca, gmc.Codigo,',
  'gfm.Controle, gfm.GraFabMCd',
  'FROM grafabmar gfm ',
  'LEFT JOIN grafabmcd gmc ON gmc.Codigo=gfm.GraFabMCd ',
  'WHERE gfm.Codigo=' + Geral.FF0(QrGraFabCadCodigo.Value),
  'ORDER BY NO_Marca, gfm.Nome ',
  '']);
  //
  QrGraFabMar.Locate('Controle', Controle, []);
end;


procedure TFmGraFabCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraFabCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraFabCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraFabCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraFabCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraFabCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraFabCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO  := QrGraFabCadCodigo.Value;
  VAR_CADASTRO2 := QrGraFabMarGraFabMCd.Value;
  Close;
end;

procedure TFmGraFabCad.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastroMarca(stIns);
end;

procedure TFmGraFabCad.CabAltera1Click(Sender: TObject);
begin
  if QrGraFabCadCodigo.Value = 0 then
    Exit;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraFabCad, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'grafabcad');
end;

procedure TFmGraFabCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('grafabcad', 'Codigo', ImgTipo.SQLType,
    QrGraFabCadCodigo.Value);
  EdCodigo.ValueVariant := Codigo;  
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'grafabcad',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
      Close;
  end;
end;

procedure TFmGraFabCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'grafabcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'grafabcad', 'Codigo');
end;

procedure TFmGraFabCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmGraFabCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmGraFabCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  if CO_SIGLA_APP = 'TREN' then
    BtCab.Enabled := False;
end;

procedure TFmGraFabCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraFabCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraFabCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraFabCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraFabCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraFabCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if QrGraFabMar.State <> dsInactive then
    VAR_CADASTRO2 := QrGraFabMarGraFabMCd.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraFabCad.QrGraFabCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraFabCad.QrGraFabCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraFabMar(0);
end;

procedure TFmGraFabCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrGraFabCadCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmGraFabCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraFabCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'grafabcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraFabCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraFabCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrGraFabCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'grafabcad');
end;

procedure TFmGraFabCad.QrGraFabCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraFabCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

