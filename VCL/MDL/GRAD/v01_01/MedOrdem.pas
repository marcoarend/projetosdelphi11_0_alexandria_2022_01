unit MedOrdem;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, UnDmkProcFunc, dmkImage,
  UnDmkEnums;

type
  TFmMedOrdem = class(TForm)
    PainelDados: TPanel;
    DsMedOrdem: TDataSource;
    QrMedOrdem: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrMedOrdemCodigo: TIntegerField;
    QrMedOrdemCodUsu: TIntegerField;
    QrMedOrdemNome: TWideStringField;
    QrMedOrdemMedida1: TWideStringField;
    QrMedOrdemMedida2: TWideStringField;
    QrMedOrdemMedida3: TWideStringField;
    QrMedOrdemMedida4: TWideStringField;
    EdMedida1: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdMedida2: TdmkEdit;
    Label6: TLabel;
    EdMedida3: TdmkEdit;
    Label10: TLabel;
    EdMedida4: TdmkEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    EdSigla1: TdmkEdit;
    Label15: TLabel;
    EdSigla3: TdmkEdit;
    Label16: TLabel;
    Label17: TLabel;
    EdSigla2: TdmkEdit;
    Label18: TLabel;
    EdSigla4: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    QrMedOrdemSigla1: TWideStringField;
    QrMedOrdemSigla2: TWideStringField;
    QrMedOrdemSigla3: TWideStringField;
    QrMedOrdemSigla4: TWideStringField;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMedOrdemAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMedOrdemBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmMedOrdem: TFmMedOrdem;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMedOrdem.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMedOrdem.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMedOrdemCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMedOrdem.DefParams;
begin
  VAR_GOTOTABELA := 'MedOrdem';
  VAR_GOTOMYSQLTABLE := QrMedOrdem;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, ');
  VAR_SQLx.Add('Medida1, Medida2, Medida3, Medida4, ');
  VAR_SQLx.Add('Sigla1, Sigla2, Sigla3, Sigla4');
  VAR_SQLx.Add('FROM medordem');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMedOrdem.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'medordem', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmMedOrdem.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMedOrdem.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmMedOrdem.AlteraRegistro;
var
  MedOrdem : Integer;
begin
  MedOrdem := QrMedOrdemCodigo.Value;
  if QrMedOrdemCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(MedOrdem, Dmod.MyDB, 'MedOrdem', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(MedOrdem, Dmod.MyDB, 'MedOrdem', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMedOrdem.IncluiRegistro;
var
  Cursor : TCursor;
  MedOrdem : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MedOrdem := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'MedOrdem', 'MedOrdem', 'Codigo');
    if Length(FormatFloat(FFormatFloat, MedOrdem))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, MedOrdem);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmMedOrdem.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMedOrdem.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMedOrdem.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMedOrdem.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMedOrdem.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMedOrdem.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMedOrdem.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrMedOrdem, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'medordem');
end;

procedure TFmMedOrdem.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMedOrdemCodigo.Value;
  Close;
end;

procedure TFmMedOrdem.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('MedOrdem', 'Codigo', ImgTipo.SQLType,
    QrMedOrdemCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmMedOrdem, PainelEdit,
    'MedOrdem', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmMedOrdem.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'MedOrdem', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MedOrdem', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MedOrdem', 'Codigo');
end;

procedure TFmMedOrdem.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrMedOrdem, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'medordem');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'medordem', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmMedOrdem.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmMedOrdem.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMedOrdemCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMedOrdem.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmMedOrdem.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMedOrdem.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrMedOrdemCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmMedOrdem.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMedOrdem.QrMedOrdemAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMedOrdem.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMedOrdem.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMedOrdemCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'MedOrdem', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMedOrdem.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMedOrdem.QrMedOrdemBeforeOpen(DataSet: TDataSet);
begin
  QrMedOrdemCodigo.DisplayFormat := FFormatFloat;
end;

end.


