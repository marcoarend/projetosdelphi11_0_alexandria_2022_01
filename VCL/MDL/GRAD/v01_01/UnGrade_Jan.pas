unit UnGrade_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, DmkGeral, DmkDAC_PF,
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus, Variants,
  Vcl.Forms, Vcl.Controls,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums, ModProd, UnGrl_Vars,
  mySQLDirectQuery;

type
  TSubForm = (tsfNenhum=0, tsfDadosGerais=1, tsfDadosFiscais=2);
  TUnGrade_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }

    procedure CadastraProdutoGrade(GraGruY, PrdGrupTip, GraGru1, GraGruC:
              Integer; JaTemN1: Boolean; NewNome, SQL_WHERE_PGT: String);
    function  CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
              ShowForm: Boolean; IDCtrl: Integer): Boolean;
    function  MostraFormUnidMed(Codigo: Integer): Boolean;
    procedure MostraFormEntrada(FatID, IDCtrl: Integer);
    //
    procedure MostraFormFisRegCad(Codigo: Integer);
    procedure MostraFormGraCorCad(Codigo: Integer);
    procedure MostraFormGraCorFam(Codigo: Integer);
    procedure MostraFormGraCorPan(Codigo: Integer);
    procedure MostraFormGraCusPrc(Codigo: Integer);
    procedure MostraFormGraFabCad();
    procedure MostraFormGraGruEGerCab();
    function  MostraFormGraGruECad(const SQLType: TSQLType; const QrCad:
              TmySQLQuery; const CodInfoEmit: Integer; const prod_cProd,
              prod_xProd: String): Boolean;
    function  MostraFormGraGruEIts(const SQLType: TSQLType; const CodInfoEmit
              (*, PrdGrupTip*): Integer; const prod_cProd, prod_xProd,
              prod_cEAN, prod_NCM, prod_uCom, InfAdProd: String; const prod_CFOP,
              ICMS_Orig, ICMS_CST, IPI_CST, PIS_CST, COFINS_CST: Integer; const
              prod_EXTIPI, IPI_cEnq: String; var GraGruX: Integer; var NomeGGX:
              String; Continuar: Boolean = False): Boolean;
    procedure MostraFormGraGruN(Nivel1: Integer; TabePr: Integer = 0; TipoLista:
              TTipoListaPreco = tpGraCusPrc; MostraSubForm: TSubForm = tsfNenhum);
    procedure MostraFormGraGruN_GGX(GraGruX: Integer; NewNome: String);
    procedure MostraFormGraGruReduzido();
    procedure MostraFormGraGruXEdit(GraGruX: Integer);
    procedure MostraFormGraImpLista(PrdGrupTip, GraGru1: Integer; Aba:
              Integer = 0; StqCenCad: Integer = 0);
    procedure MostraFormGraOptEnt();
    procedure MostraFormGraTamCad(Codigo: Integer);
    procedure MostraFormOpcoesGrad();
    procedure MostraFormPrdGrupTip(Codigo: Integer);
    procedure MostraFormStqBalCad(Codigo: Integer);
    procedure MostraFormStqCenCab();
    procedure MostraFormStqCenCad(StqCenCad: Integer; StqCenLoc: Integer = 0);
    procedure MostraFormStqInnCad(Codigo: Integer);
    procedure MostraFormStqManCad(Codigo: Integer);
    procedure MostraFormCFOP2003();
    procedure MostraFormNCMs();
    function  MostraFormNCM_Mul(SQL: array of String; DB: TmySQLDataBase): Boolean;
    procedure MostraFormModelosImp();
    procedure MostraFormGraAtrCad(Codigo: Integer);
    procedure MostraGraTecCad(Codigo: Integer);
    function  MostraFormClasFisc(): String;
    function  MostraFormGraGruPesq1(SQL: String): Integer;
    //
    procedure MostraFormEditGraGru2(Nivel2: Integer; Nome: String;
              Tipo_Item: Integer);
    procedure MostraFormInsGraGru2(const PrdGrupTip, Nivel5, Nivel4, Nivel3:
              Integer; var Nivel2: Integer; const Nome: String; const Tipo_Item:
              Integer);
    procedure MostraFormGraGruNiveis2();
    procedure MostraFormGraGruN_Dados(GraGru1: Integer; SubForm: TSubForm);
    procedure MostraFormGraGruYIncorpora(GraGruY, PrdGrupTip: Integer);
    procedure MostraFormGraGruAtiCorTam(Nivel1, GraTamCad, GraGruY: Integer);
    procedure MostraFormGraGruY(GraGruY, GragruX: Integer; NewNome: String);
    procedure MostraFormGraGruY1(GraGruY, GragruX: Integer; NewNome: String);
    procedure MostraFormGraSrvVal();
    procedure MostraFormGraGruEPat(GraGruX: Integer);
    function  ObtemGraGruXSemY(Tabela: Integer): Integer;

  end;

var
  Grade_Jan: TUnGrade_Jan;

implementation

uses MyDBCheck, ModuleGeral, UnMyObjects, GetValor, Module, GraAtrCad, GraFibCad,
  {$IfNDef SemNFe_0000} EntradaCab, {$EndIf}
  {$IfNDef NAO_GFAT} CFOP2003, NCMs, Imprime, {$EndIf}
  GraGruPesq1, CfgCadLista,
  PrdGrupTip, GraTamCad, GraCorCad, GraCorFam, GraCorPan, GraFabCad, GraGruN,
  GraImpLista, GraCusPrc, GraGruReduzido, StqBalCad, StqCenCab, StqManCad,
  FisRegCad, StqCenCad, StqInnCad, GraOptEnt, UnidMed,
  GraTecCad, OpcoesGrad, GraGruXEdit, ClasFisc, GraGru2, GraGruNiveis2, GraGruY,
  GraGruEGerCab, GraGruEIts, GraGruECad, GraGruYIncorpora, GraGruAtiCorTam,
  NCMsMul, GraGruXSelToY, GraSrvVal, GraGruEPat(*, StqAlocGer*);

{ TUnGrade_Jan }

procedure TUnGrade_Jan.CadastraProdutoGrade(GraGruY, PrdGrupTip, GraGru1,
  GraGruC: Integer; JaTemN1: Boolean; NewNome, SQL_WHERE_PGT: String);
begin
// Copiar de VS_PF ?
end;

function TUnGrade_Jan.CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
  ShowForm: Boolean; IDCtrl: Integer): Boolean;
var
  TipoEntradTitu: String;
  TipoEntradaDig, TipoEntradaNFe, TipoEntradaEFD: Integer;
begin
{$IfNDef SemNFe_0000}
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  case MaterialNF of
    tnfMateriaPrima:
    begin
      TipoEntradaDig := VAR_FATID_0113;
      TipoEntradaNFE := VAR_FATID_0013;
      TipoEntradaEFD := VAR_FATID_0213;
      TipoEntradTitu := 'Mat�ria-prima';
    end;
    tnfUsoEConsumo:
    begin
      TipoEntradaDig := VAR_FATID_0151;
      TipoEntradaNFe := VAR_FATID_0051;
      TipoEntradaEFD := VAR_FATID_0251;
      TipoEntradTitu := 'Uso e Consumo';
    end;
  end;
  Result := DBCheck.CriaFm(TFmEntradaCab, FmEntradaCab, afmoNegarComAviso);
  FmEntradaCab.IniciaForm(TipoEntradTitu, TipoEntradaDig, TipoEntradaNFe,
    TipoEntradaEFD);
  if Result and ShowForm then
  begin
    if IDCtrl <> 0 then
    begin
      FmEntradaCab.LocCod(IDCtrl, IDCtrl);
      if FmEntradaCab.QrNFeCabAIDCtrl.Value <> IDCtrl then
        Geral.MensagemBox('N�o foi poss�vel localizar o lan�amento solicitado!',
        'Aviso', MB_OK+MB_ICONINFORMATION);
    end;
    FmEntradaCab.ShowModal;
    FmEntradaCab.Destroy;
  end;
{$Else}
  Geral.MB_Aviso('Aplicativo sem "EntradaCab". Avise a Dermatek!');
{$EndIf}
end;

function TUnGrade_Jan.MostraFormClasFisc: String;
begin
  Result := '';
  //
  if DBCheck.CriaFm(TFmClasFisc, FmClasFisc, afmoNegarComAviso) then
  begin
    FmClasFisc.BtSeleciona.Visible := True;
    FmClasFisc.ShowModal;
    if FmClasFisc.FNCMSelecio <> '' then
      Result := FmClasFisc.FNCMSelecio;
    FmClasFisc.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormCFOP2003();
begin
  {$IfNDef NAO_GFAT}
    if DBCheck.CriaFm(TFmCFOP2003, FmCFOP2003, afmoNegarComAviso) then
  begin
    FmCFOP2003.ShowModal;
    FmCFOP2003.Destroy;
  end;
  {$EndIf}
end;

procedure TUnGrade_Jan.MostraFormEditGraGru2(Nivel2: Integer; Nome: String;
  Tipo_Item: Integer);
var
  CtbPlaCta, CtbCadGru: Integer;
begin
  if DBCheck.CriaFm(TFmGraGru2, FmGraGru2, afmoNegarComAviso) then
  begin
    FmGraGru2.ImgTipo.SQLType := stUpd;
    FmGraGru2.EdNivel2.ValueVariant := Nivel2;
    FmGraGru2.EdNome.ValueVariant := Nome;
    FmGraGru2.EdTipo_item.ValueVariant := Tipo_Item;
    //
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT * ',
      'FROM GraGru2',
      'WHERE Nivel2=' + Geral.FF0(Nivel2),
      '']);
      CtbCadGru := Dmod.QrAux.FieldByName('CtbCadGru').AsInteger;
      CtbPlaCta := Dmod.QrAux.FieldByName('CtbPlaCta').AsInteger;
      //
      FmGraGru2.EdCtbCadGru.ValueVariant := CtbCadGru;
      FmGraGru2.CBCtbCadGru.KeyValue     := CtbCadGru;
      FmGraGru2.EdCtbPlaCta.ValueVariant := CtbPlaCta;
      FmGraGru2.CBCtbPlaCta.KeyValue     := CtbPlaCta;
      //
      Dmod.QrAux.Close;
    except
      // nada
    end;
    //
    FmGraGru2.ShowModal;
    FmGraGru2.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormInsGraGru2(const PrdGrupTip, Nivel5, Nivel4,
  Nivel3: Integer; var Nivel2: Integer; const Nome: String;
  const Tipo_Item: Integer);
begin
  if DBCheck.CriaFm(TFmGraGru2, FmGraGru2, afmoNegarComAviso) then
  begin
    FmGraGru2.ImgTipo.SQLType := stIns;
    FmGraGru2.FPrdGrupTip := PrdGrupTip;
    FmGraGru2.FNivel5 := Nivel5;
    FmGraGru2.FNivel4 := Nivel4;
    FmGraGru2.FNivel3 := Nivel3;
    //
    FmGraGru2.EdNivel2.ValueVariant := Nivel2;
    FmGraGru2.EdNome.ValueVariant := Nome;
    FmGraGru2.EdTipo_item.ValueVariant := Tipo_Item;
    //
    FmGraGru2.ShowModal;
    Nivel2 := FmGraGru2.EdNivel2.ValueVariant;
    FmGraGru2.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormEntrada(FatID, IDCtrl: Integer);
begin
  case FatID of
    VAR_FATID_0013,
    VAR_FATID_0113,
    VAR_FATID_0213:
      CriaFormEntradaCab(tnfMateriaPrima, True, IDCtrl);
    VAR_FATID_0051,
    VAR_FATID_0151,
    VAR_FATID_0251:
      CriaFormEntradaCab(tnfUsoEConsumo, True, IDCtrl);
  end;
end;

procedure TUnGrade_Jan.MostraFormFisRegCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFisRegCad, FmFisRegCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmFisRegCad.LocCod(Codigo, Codigo);
      if FmFisRegCad.QrFisRegCadCodigo.Value <> Codigo then
        if VAR_FisRegCad_ToAlt <> 0 then
          FmFisRegCad.LocCod(VAR_FisRegCad_ToAlt, VAR_FisRegCad_ToAlt);
    end;
    FmFisRegCad.ShowModal;
    VAR_FisRegCad_ToAlt := 0;
    FmFisRegCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraCorCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraCorCad, FmGraCorCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraCorCad.LocCod(Codigo, Codigo);
    FmGraCorCad.ShowModal;
    FmGraCorCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraCorFam(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraCorFam, FmGraCorFam, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraCorFam.LocCod(Codigo, Codigo);
    FmGraCorFam.ShowModal;
    FmGraCorFam.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraCorPan(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraCorPan, FmGraCorPan, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraCorPan.LocCod(Codigo, Codigo);
    FmGraCorPan.ShowModal;
    FmGraCorPan.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraCusPrc(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraCusPrc, FmGraCusPrc, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraCusPrc.LocCod(Codigo, Codigo);
    FmGraCusPrc.ShowModal;
    FmGraCusPrc.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraFabCad();
begin
  if DBCheck.CriaFm(TFmGraFabCad, FmGraFabCad, afmoNegarComAviso) then
  begin
    FmGraFabCad.ShowModal;
    FmGraFabCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruAtiCorTam(Nivel1, GraTamCad,
  GraGruY: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruAtiCorTam, FmGraGruAtiCorTam, afmoNegarComAviso) then
  begin
    FmGraGruAtiCorTam.FNivel1           := Nivel1;
    FmGraGruAtiCorTam.FGraTamCad        := GraTamCad;
    FmGraGruAtiCorTam.FGraCusPrcUCodigo := 0; // ????
    FmGraGruAtiCorTam.FEntiPrecos       := 0; // ????
    FmGraGruAtiCorTam.FGraGruY          := GraGruY;
    //
    FmGraGruAtiCorTam.ConfiguraGrades();
    //
    FmGraGruAtiCorTam.ShowModal;
    FmGraGruAtiCorTam.Destroy;
  end;
end;

function TUnGrade_Jan.MostraFormGraGruECad(const SQLType: TSQLType;
  const QrCad: TmySQLQuery; const CodInfoEmit: Integer; const prod_cProd,
  prod_xProd: String): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruECad, FmGraGruECad, afmoNegarComAviso) then
  begin
    FmGraGruECad.ImgTipo.SQLType         := SQLType;
    //
    FmGraGruECad.FQrCad                 := QrCad;
    FmGraGruECad.FxProd                 := prod_xProd;
    FmGraGruECad.FcProd                 := prod_cProd;
    FmGraGruECad.FFornece               := CodInfoEmit;
    //
    FmGraGruECad.EdxProd.Text            := prod_xProd;
    FmGraGruECad.EdcProd.Text            := prod_cProd;
    FmGraGruECad.EdFornece.ValueVariant  := CodInfoEmit;
    FmGraGruECad.CBFornece.KeyValue      := CodInfoEmit;
    //
    FmGraGruECad.ShowModal;
    FmGraGruECad.Destroy;
    //
    Result := True;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruEGerCab;
begin
  if DBCheck.CriaFm(TFmGraGruEGerCab, FmGraGruEGerCab, afmoNegarComAviso) then
  begin
    FmGraGruEGerCab.ShowModal;
    FmGraGruEGerCab.Destroy;
  end;
end;

function TUnGrade_Jan.MostraFormGraGruEIts(const SQLType: TSQLType; const
  CodInfoEmit(*, PrdGrupTip*): Integer; const prod_cProd, prod_xProd, prod_cEAN,
  prod_NCM, prod_uCom, InfAdProd: String; const prod_CFOP, ICMS_Orig, ICMS_CST,
  IPI_CST, PIS_CST, COFINS_CST: Integer; const prod_EXTIPI, IPI_cEnq: String;
  var GraGruX: Integer; var NomeGGX: String; Continuar: Boolean = False): Boolean;
begin
  Result := False;
  case SQLType of
    stIns: ;
    stUpd: ;
    else
    begin
      Geral.MB_Erro('SQLType <> stIns/stUpd');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmGraGruEIts, FmGraGruEIts, afmoNegarComAviso) then
  begin
    FmGraGruEIts.ImgTipo.SQLType         := SQLType;
    //
    FmGraGruEIts.EdxProd.Text            := prod_xProd;
    FmGraGruEIts.EdcProd.Text            := prod_cProd;
    //FmGraGruEIts.FFormChamou             := FormChamou;
    FmGraGruEIts.EdFornece.ValueVariant  := CodInfoEmit;
    FmGraGruEIts.CBFornece.KeyValue      := CodInfoEmit;
    FmGraGruEIts.EdNCM.Text              := prod_NCM;
    FmGraGruEIts.EdCFOP.ValueVariant     := prod_CFOP;
    FmGraGruEIts.EduCom.Text             := prod_uCOM;

    // Dados do produto
    FmGraGruEIts.F_prod_NCM              := prod_NCM;
    FmGraGruEIts.F_prod_EXTIPI           := prod_EXTIPI;
    FmGraGruEIts.F_prod_uCom             := prod_uCom;
    FmGraGruEIts.F_prod_cEAN             := prod_cEAN;
    FmGraGruEIts.F_InfAdProd             := InfAdProd;
    FmGraGruEIts.F_ICMS_CST_A            := FormatFloat('0', ICMS_Orig);
    FmGraGruEIts.F_ICMS_CST_B            := FormatFloat('00', ICMS_CST);
    FmGraGruEIts.F_IPI_CST               := FormatFloat('00', IPI_CST);
    FmGraGruEIts.F_PIS_CST               := FormatFloat('00', PIS_CST);
    FmGraGruEIts.F_COFINS_CST            := FormatFloat('00', COFINS_CST);
    FmGraGruEIts.F_prod_EXTIPI           := prod_EXTIPI;
    FmGraGruEIts.F_IPI_cEnq              := IPI_cEnq;
    // FIM dados do produto
    if SQLType = stUpd then
    begin
      FmGraGruEIts.EdGraGruX.ValueVariant := GraGruX;
      FmGraGruEIts.CBGraGruX.KeyValue := GraGruX;
      //
      FmGraGruEIts.EdGraGruX.Enabled := False;
      FmGraGruEIts.CBGraGruX.Enabled := False;
    end else
      FmGraGruEIts.EdGraGruX.TxtPesq       := prod_xProd;

    //
    if Continuar = True then
    begin
      FmGraGruEIts.CkContinuar.Visible := True;
      FmGraGruEIts.CkContinuar.Checked := False;
    end else
    begin
      FmGraGruEIts.CkContinuar.Visible := False;
      FmGraGruEIts.CkContinuar.Checked := False;
    end;
    //
    FmGraGruEIts.ShowModal;
    GraGruX := FmGraGruEIts.FGraGruX;
    NomeGGX := FmGraGruEIts.FNomeGGX;
    FmGraGruEIts.Destroy;
    //
    Result := GraGruX <> 0;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruEPat(GraGruX: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruEPat, FmGraGruEPat, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmGraGruEPat.EdReduzido.ValueVariant := GraGruX;
      FmGraGruEPat.Pesquisa();
    end;
    FmGraGruEPat.ShowModal;
    FmGraGruEPat.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruN(Nivel1: Integer; TabePr: Integer = 0;
  TipoLista: TTipoListaPreco = tpGraCusPrc; MostraSubForm: TSubForm = tsfNenhum);
begin
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    if Nivel1 <> 0 then
      FmGraGruN.LocalizaIDNivel1(Nivel1);
    //
    if TabePr <> 0 then
    begin
      FmGraGruN.FTabePr   := TabePr;
      FmGraGruN.FTipLista := TipoLista;
    end else
      FmGraGruN.FTabePr := 0;
    //
    FmGraGruN.FMostraSubForm := MostraSubForm;
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruN_Dados(GraGru1: Integer;
  SubForm: TSubForm);
//{$IfNDef NAO_GFAT}
var
  SubFrm: TSubForm;
  //Nivel1: Integer;
begin
  if SubForm = TSubForm.tsfDadosGerais then
    SubFrm := tsfDadosGerais
  else if SubForm = TSubForm.tsfDadosFiscais then
    SubFrm := tsfDadosFiscais
  else
    SubFrm := tsfNenhum;
  //
  //Grade_Jan.MostraFormGraGruN(Nivel1, 0, tpGraCusPrc, SubFrm);
  Grade_Jan.MostraFormGraGruN(GraGru1, 0, tpGraCusPrc, SubFrm);
  try
    Screen.Cursor := crHourGlass;
    //
  finally
    Screen.Cursor := crDefault;
  end;
(*
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
*)
end;

procedure TUnGrade_Jan.MostraFormGraGruN_GGX(GraGruX: Integer; NewNome: String);
begin
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
      FmGraGruN.LocalizaReduzido(GraGruX);
    FmGraGruN.FNewNome := NewNome;
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
end;

function TUnGrade_Jan.MostraFormGraGruPesq1(SQL: String): Integer;
var
  Res: String;
begin
  Res := '';
  //
  if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
  begin
    FmGraGruPesq1.QrGraGru1.Close;
    FmGraGruPesq1.QrGraGru1.SQL.Clear;
    FmGraGruPesq1.QrGraGru1.SQL.Text := SQL;
    UnDmkDAC_PF.AbreQuery(FmGraGruPesq1.QrGraGru1, Dmod.MyDB);
    FmGraGruPesq1.ShowModal;
    //
    Res := FmGraGruPesq1.FGraGruX;
    //
    FmGraGruPesq1.Destroy;
  end;
  if Res = '' then
    Result := 0
  else
    Result := Geral.IMV(Res);
end;

procedure TUnGrade_Jan.MostraFormGraGruNiveis2();
begin
  if DBCheck.CriaFm(TFmGraGruNiveis2, FmGraGruNiveis2, afmoNegarComAviso) then
  begin
    FmGraGruNiveis2.ShowModal;
    FmGraGruNiveis2.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruReduzido();
var
  GraGruX: Variant;
begin
  GraGruX := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX, 0, 0,
  '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, GraGruX) then
  begin
    if DBCheck.CriaFm(TFmGraGruReduzido, FmGraGruReduzido, afmoSoAdmin) then
    begin
      FmGraGruReduzido.FGraGruX := GraGruX;
      FmGraGruReduzido.QrGraGruX.Close;
      FmGraGruReduzido.QrGraGruX.Params[0].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(FmGraGruReduzido.QrGraGruX, Dmod.MyDB);
      FmGraGruReduzido.ShowModal;
      FmGraGruReduzido.Destroy;
    end;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruXEdit(GraGruX: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruXEdit, FmGraGruXEdit, afmoNegarComAviso) then
  begin
    FmGraGruXEdit.ReopengraGruX(GraGruX);
    FmGraGruXEdit.ShowModal;
    FmGraGruXEdit.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruY(GraGruY, GragruX: Integer;
  NewNome: String);
begin
  if DBCheck.CriaFm(TFmGraGruY, FmGraGruY, afmoNegarComAviso) then
  begin
    if GraGruY <> 0 then
    begin
      FmGraGruY.LocCod(GraGruY, GraGruY);
      FmGraGruY.QrGraGruX.Locate('Controle', GraGruX, []);
    end;
    FmGraGruY.FGraGruX := GraGruX;
    FmGraGruY.FNewNome := NewNome;
    FmGraGruY.ShowModal;
    FmGraGruY.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruY1(GraGruY, GragruX: Integer;
  NewNome: String);
begin
  if DBCheck.CriaFm(TFmGraGruY, FmGraGruY1, afmoNegarComAviso) then
  begin
    if GraGruY <> 0 then
      FmGraGruY1.LocCod(GraGruY, GraGruY);
    FmGraGruY1.FGraGruX := GraGruX;
    FmGraGruY1.FNewNome := NewNome;
    FmGraGruY1.ShowModal;
    FmGraGruY1.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraGruYIncorpora(GraGruY, PrdGrupTip: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruYIncorpora, FmGraGruYIncorpora, afmoNegarComAviso) then
  begin
    FmGraGruYIncorpora.FGraGruY := GraGruY;
    FmGraGruYIncorpora.FPrdGrupTip := PrdGrupTip;
    FmGraGruYIncorpora.ReopenGG1Orfaos();
    if FmGraGruYIncorpora.QrGG1Orfaos.RecordCount > 0 then
      FmGraGruYIncorpora.ShowModal
    else
      Geral.MB_Info(
      'N�o existem reduzidos orf�os para o Tipo de Grupo de Produto n�mero ' +
      Geral.FF0(PrdGrupTip));
    FmGraGruYIncorpora.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraImpLista(PrdGrupTip, GraGru1: Integer;
  Aba: Integer = 0; StqCenCad: Integer = 0);
begin
  if DBCheck.CriaFm(TFmGraImpLista, FmGraImpLista, afmoNegarComAviso) then
  begin
    if PrdGrupTip <> 0 then
    begin
      FmGraImpLista.EdPrdGrupTip.ValueVariant := PrdGrupTip;
      FmGraImpLista.CBPrdGrupTip.KeyValue     := PrdGrupTip;
    end;
    if GraGru1 <> 0 then
    begin
      FmGraImpLista.EdGraGru1.ValueVariant := GraGru1;
      FmGraImpLista.CBGraGru1.KeyValue     := GraGru1;
    end;
    //
    if StqCenCad <> 0 then
    begin
      FmGraImpLista.EdStqCenCad.ValueVariant := StqCenCad;
      FmGraImpLista.CBStqCenCad.KeyValue     := StqCenCad;
    end;
    //
    try
      try
        FmGraImpLista.PageControl1.ActivePageIndex := Aba;
      except
        // nada
      end;
      //
    finally
      FmGraImpLista.ShowModal;
      FmGraImpLista.Destroy;
    end;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraOptEnt;
begin
  if DBCheck.CriaFm(TFmGraOptEnt, FmGraOptEnt, afmoNegarComAviso) then
  begin
    FmGraOptEnt.ShowModal;
    FmGraOptEnt.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraSrvVal();
begin
  if DBCheck.CriaFm(TFmGraSrvVal, FmGraSrvVal, afmoNegarComAviso) then
  begin
    FmGraSrvVal.ShowModal;
    FmGraSrvVal.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraTamCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraTamCad, FmGraTamCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraTamCad.LocCod(Codigo, Codigo);
    FmGraTamCad.ShowModal;
    FmGraTamCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormModelosImp;
begin
{$IfNDef NAO_GFAT}
    if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
{$EndIf}
end;

procedure TUnGrade_Jan.MostraFormNCMs;
begin
{$IfNDef NAO_GFAT}
  if DBCheck.CriaFm(TFmNCMs, FmNCMs, afmoNegarComAviso) then
  begin
    FmNCMs.ShowModal;
    FmNCMs.Destroy;
  end;
{$EndIf}
end;

function TUnGrade_Jan.MostraFormNCM_Mul(SQL: array of String; DB:
  TmySQLDataBase): Boolean;
begin
//{$IfNDef ???}
  Result := False;
  if DBCheck.CriaFm(TFmNCMsMul, FmNCMsMul, afmoNegarComAviso) then
  begin
    FmNCMsMul.ReopenErrNCMs(Geral.ATS(SQL), DB);
    FmNCMsMul.ShowModal;
    FmNCMsMul.Destroy;
    Result := True;
  end;
//{$EndIf}
end;

procedure TUnGrade_Jan.MostraFormOpcoesGrad();
begin
  if DmProd.QrOpcoesGrad.State = dsInactive then
    DmProd.ReopenOpcoesGrad();
  //
  if DBCheck.CriaFm(TFmOpcoesGrad, FmOpcoesGrad, afmoNegarComAviso) then
  begin
    FmOpcoesGrad.ImgTipo.SQLType := stUpd;
    //
    FmOpcoesGrad.CkUsaGrade.Checked := DmProd.QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = 1;
    //  Dever ser antes do UnicoTam
    FmOpcoesGrad.EdUnicaGra.ValueVariant := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
    FmOpcoesGrad.CBUnicaGra.KeyValue     := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
    //
    FmOpcoesGrad.ReopenGraTamIts(DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger);
    //
    FmOpcoesGrad.EdUnicoTam.ValueVariant := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
    FmOpcoesGrad.CBUnicoTam.KeyValue     := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
    //
    FmOpcoesGrad.EdUnicaCor.ValueVariant := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
    FmOpcoesGrad.CBUnicaCor.KeyValue     := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
    //
    FmOpcoesGrad.CkUsaCodFornece.Checked := Geral.IntToBool(DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger);
    //
    FmOpcoesGrad.RGCasasProd.ItemIndex   := DModG.QrControle.FieldByName('CasasProd').AsInteger;
    //
    FmOpcoesGrad.CkEditGraTabApp.Checked := Geral.IntToBool(DmProd.QrOpcoesGrad.FieldByName('EditGraTabApp').AsInteger);
    //
    FmOpcoesGrad.CkRstrPrdFat.Checked := Geral.IntToBool(DmProd.QrOpcoesGrad.FieldByName('RstrPrdFat').AsInteger);
    //

    FmOpcoesGrad.RGNFeUsaMarca.ItemIndex            := DmProd.QrOpcoesGrad.FieldByName('NFeUsaMarca').AsInteger;
    FmOpcoesGrad.EdNFePreMarca.ValueVariant         := DmProd.QrOpcoesGrad.FieldByName('NFePreMarca').AsString;
    FmOpcoesGrad.EdNFePosMarca.ValueVariant         := DmProd.QrOpcoesGrad.FieldByName('NFePosMarca').AsString;
    FmOpcoesGrad.RGNFeUsaReferencia.ItemIndex       := DmProd.QrOpcoesGrad.FieldByName('NFeUsaReferencia').AsInteger;
    FmOpcoesGrad.EdNFePreReferencia.ValueVariant    := DmProd.QrOpcoesGrad.FieldByName('NFePreReferencia').AsString;
    FmOpcoesGrad.EdNFePosReferencia.ValueVariant    := DmProd.QrOpcoesGrad.FieldByName('NFePosReferencia').AsString;
    //
    FmOpcoesGrad.CkLoadXMLPorLoteFab.Checked := Geral.IntToBool(DmProd.QrOpcoesGrad.FieldByName('LoadXMLPorLoteFab').AsInteger);
    //

    FmOpcoesGrad.ShowModal;
    FmOpcoesGrad.Destroy;
    //
    DmProd.ReopenOpcoesGrad();
  end;
end;

procedure TUnGrade_Jan.MostraFormPrdGrupTip(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPrdGrupTip, FmPrdGrupTip, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPrdGrupTip.LocCod(Codigo, Codigo);
    FmPrdGrupTip.ShowModal;
    FmPrdGrupTip.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormStqBalCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmStqBalCad, FmStqBalCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmStqBalCad.LocCod(Codigo, Codigo);
    FmStqBalCad.ShowModal;
    FmStqBalCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormStqCenCab();
begin
  if DBCheck.CriaFm(TFmStqCenCab, FmStqCenCab, afmoNegarComAviso) then
  begin
    FmStqCenCab.ShowModal;
    FmStqCenCab.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormStqCenCad(StqCenCad: Integer; StqcenLoc: Integer);
var
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  if DBCheck.CriaFm(TFmStqCenCad, FmStqCenCad, afmoNegarComAviso) then
  begin
    if StqCenCad <> 0 then
      Codigo := StqCenCad
    else
    if StqCenloc <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Codigo  ',
        'FROM stqcenloc ',
        'WHERE Controle=' + Geral.FF0(StqCenLoc),
        '']);
        Codigo := Qry.FieldByName('Codigo').AsInteger;
      finally
        Qry.Free;
      end;
    end;
    FmStqCenCad.LocCod(Codigo, Codigo);
    FmStqCenCad.ShowModal;
    if FmStqCenCad.QrStqCenLoc.State <> dsInactive then
      VAR_CAD_STQCENLOC := FmStqCenCad.QrStqCenLocControle.Value
    else
      VAR_CAD_STQCENLOC := 0;
    FmStqCenCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormStqInnCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmStqInnCad, FmStqInnCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmStqInnCad.LocCod(Codigo, Codigo);
    FmStqInnCad.ShowModal;
    FmStqInnCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraFormStqManCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmStqManCad, FmStqManCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmStqManCad.LocCod(Codigo, Codigo);
    FmStqManCad.ShowModal;
    FmStqManCad.Destroy;
  end;
end;

function TUnGrade_Jan.MostraFormUnidMed(Codigo: Integer): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmUnidMed.LocCod(Codigo, Codigo);
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    Result := True;
  end;
end;

procedure TUnGrade_Jan.MostraFormGraAtrCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraAtrCad, FmGraAtrCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraAtrCad.LocCod(Codigo, Codigo);
    FmGraAtrCad.ShowModal;
    FmGraAtrCad.Destroy;
  end;
end;

procedure TUnGrade_Jan.MostraGraTecCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraTecCad, FmGraTecCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraTecCad.LocCod(Codigo, Codigo);
    FmGraTecCad.ShowModal;
    FmGraTecCad.Destroy;
  end;
end;

function TUnGrade_Jan.ObtemGraGruXSemY(Tabela: Integer): Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmGraGruXSelToY, FmGraGruXSelToY, afmoNegarComAviso) then
  begin
    FmGraGruXSelToY.FGraGruY := Tabela;
    //
    FmGraGruXSelToY.ShowModal;
    Result := FmGraGruXSelToY.FGraGruX;
    FmGraGruXSelToY.Destroy;
  end;
end;

end.
