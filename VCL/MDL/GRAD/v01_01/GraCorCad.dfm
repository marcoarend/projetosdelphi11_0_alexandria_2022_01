object FmGraCorCad: TFmGraCorCad
  Left = 368
  Top = 194
  Caption = 'PRD-CORES-001 :: Cadastro de Cores'
  ClientHeight = 387
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 291
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 136
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 792
        Height = 64
        Align = alTop
        Caption = ' Cadastro: '
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 16
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label8: TLabel
          Left = 68
          Top = 16
          Width = 57
          Height = 13
          Caption = 'C'#243'digo: [F4]'
        end
        object Label9: TLabel
          Left = 152
          Top = 16
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCodUsu: TdmkEdit
          Left = 68
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdCodUsuKeyDown
        end
        object EdNome: TdmkEdit
          Left = 152
          Top = 32
          Width = 548
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkPrintCor: TdmkCheckBox
          Left = 704
          Top = 36
          Width = 73
          Height = 17
          Caption = 'Imprimir cor.'
          TabOrder = 3
          QryCampo = 'PrintCor'
          UpdCampo = 'PrintCor'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 64
        Width = 792
        Height = 64
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox5: TGroupBox
          Left = 0
          Top = 0
          Width = 369
          Height = 64
          Align = alLeft
          Caption = ' Pantone: '
          TabOrder = 0
          object Label4: TLabel
            Left = 8
            Top = 16
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label5: TLabel
            Left = 92
            Top = 16
            Width = 72
            Height = 13
            Caption = 'Descri'#231#227'o: [F3]'
          end
          object SpeedButton5: TSpeedButton
            Left = 344
            Top = 32
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object EdPantone: TdmkEditCB
            Left = 8
            Top = 32
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = CBFamiliaKeyDown
            DBLookupComboBox = CBPantone
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPantone: TdmkDBLookupComboBox
            Left = 92
            Top = 32
            Width = 249
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsGraCorPan
            TabOrder = 1
            OnKeyDown = CBPantoneKeyDown
            dmkEditCB = EdPantone
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object GroupBox6: TGroupBox
          Left = 369
          Top = 0
          Width = 423
          Height = 64
          Align = alClient
          Caption = ' Fam'#237'lia de cores: '
          TabOrder = 1
          object Label15: TLabel
            Left = 8
            Top = 16
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label16: TLabel
            Left = 92
            Top = 16
            Width = 72
            Height = 13
            Caption = 'Descri'#231#227'o: [F3]'
          end
          object SpeedButton6: TSpeedButton
            Left = 392
            Top = 32
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton6Click
          end
          object CBFamilia: TdmkDBLookupComboBox
            Left = 92
            Top = 32
            Width = 297
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsGraCorFam
            TabOrder = 1
            OnKeyDown = CBFamiliaKeyDown
            dmkEditCB = EdFamilia
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdFamilia: TdmkEditCB
            Left = 8
            Top = 32
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = CBFamiliaKeyDown
            DBLookupComboBox = CBFamilia
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 221
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 291
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 132
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 792
        Height = 64
        Align = alTop
        Caption = ' Cadastro: '
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label3: TLabel
          Left = 68
          Top = 16
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label2: TLabel
          Left = 152
          Top = 16
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsGraCorCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit1: TDBEdit
          Left = 68
          Top = 32
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsGraCorCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 152
          Top = 32
          Width = 548
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsGraCorCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 64
        Width = 792
        Height = 64
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 369
          Height = 64
          Align = alLeft
          Caption = ' Pantone: '
          TabOrder = 0
          object Label6: TLabel
            Left = 8
            Top = 16
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label13: TLabel
            Left = 76
            Top = 16
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label14: TLabel
            Left = 160
            Top = 16
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object DBEdit2: TDBEdit
            Left = 8
            Top = 32
            Width = 64
            Height = 21
            DataField = 'Pantone'
            DataSource = DsGraCorCad
            TabOrder = 0
          end
          object DBEdit6: TDBEdit
            Left = 76
            Top = 32
            Width = 80
            Height = 21
            DataField = 'REF_PANTONE'
            DataSource = DsGraCorCad
            TabOrder = 1
          end
          object DBEdit7: TDBEdit
            Left = 160
            Top = 32
            Width = 201
            Height = 21
            DataField = 'NOMEPENTONE'
            DataSource = DsGraCorCad
            TabOrder = 2
          end
        end
        object GroupBox2: TGroupBox
          Left = 369
          Top = 0
          Width = 423
          Height = 64
          Align = alClient
          Caption = ' Fam'#237'lia de cores: '
          TabOrder = 1
          object Label10: TLabel
            Left = 8
            Top = 16
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label11: TLabel
            Left = 76
            Top = 16
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label12: TLabel
            Left = 160
            Top = 16
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object DBEdit4: TDBEdit
            Left = 8
            Top = 32
            Width = 64
            Height = 21
            DataField = 'Familia'
            DataSource = DsGraCorCad
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 160
            Top = 32
            Width = 253
            Height = 21
            DataField = 'NOMEFAMILIA'
            DataSource = DsGraCorCad
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 76
            Top = 32
            Width = 80
            Height = 21
            DataField = 'REF_FAMILIA'
            DataSource = DsGraCorCad
            TabOrder = 1
          end
        end
      end
      object DBCheckBox1: TDBCheckBox
        Left = 704
        Top = 36
        Width = 76
        Height = 17
        Caption = 'Imprime cor.'
        DataField = 'PrintCor'
        DataSource = DsGraCorCad
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 227
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BitBtn1: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object BitBtn2: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object BitBtn3: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object BitBtn4: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 95
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel9: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel10: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BitBtn5: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 230
        Height = 32
        Caption = 'Cadastro de Cores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 230
        Height = 32
        Caption = 'Cadastro de Cores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 230
        Height = 32
        Caption = 'Cadastro de Cores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 360
    Top = 60
  end
  object QrGraCorCad: TMySQLQuery
    BeforeOpen = QrGraCorCadBeforeOpen
    AfterOpen = QrGraCorCadAfterOpen
    SQL.Strings = (
      'SELECT gcc.Codigo, gcc.CodUsu, gcc.Nome, gcc.Pantone, '
      'gcc.Familia, gcf.Nome NOMEFAMILIA, gcf.CodUsu REF_FAMILIA,'
      'gcp.Nome NOMEPENTONE, gcp.CodUsu REF_PANTONE,'
      'gcc.PrintCor'
      'FROM gracorcad gcc'
      'LEFT JOIN gracorfam gcf ON gcc.Familia=gcf.Codigo'
      'LEFT JOIN gracorpan gcp ON gcc.Pantone=gcp.Codigo'
      '')
    Left = 332
    Top = 60
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraCorCadPantone: TWideStringField
      FieldName = 'Pantone'
    end
    object QrGraCorCadFamilia: TIntegerField
      FieldName = 'Familia'
      Required = True
    end
    object QrGraCorCadNOMEFAMILIA: TWideStringField
      FieldName = 'NOMEFAMILIA'
      Size = 30
    end
    object QrGraCorCadREF_FAMILIA: TIntegerField
      FieldName = 'REF_FAMILIA'
      Required = True
    end
    object QrGraCorCadNOMEPENTONE: TWideStringField
      FieldName = 'NOMEPENTONE'
      Size = 30
    end
    object QrGraCorCadREF_PANTONE: TIntegerField
      FieldName = 'REF_PANTONE'
      Required = True
    end
    object QrGraCorCadPrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanDel01 = BtExclui
    Left = 388
    Top = 60
  end
  object QrGraCorFam: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gracorfam'
      'ORDER BY Nome')
    Left = 476
    Top = 8
    object QrGraCorFamCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorFamCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCorFamNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCorFam: TDataSource
    DataSet = QrGraCorFam
    Left = 504
    Top = 8
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdFamilia
    Panel = PainelEdit
    QryCampo = 'Familia'
    UpdCampo = 'Familia'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 532
    Top = 8
  end
  object QrGraCorPan: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gracorpan'
      'ORDER BY Nome')
    Left = 564
    Top = 8
    object QrGraCorPanCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorPanCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCorPanNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCorPan: TDataSource
    DataSet = QrGraCorPan
    Left = 592
    Top = 8
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdPantone
    Panel = PainelEdit
    QryCampo = 'Pantone'
    UpdCampo = 'Pantone'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 620
    Top = 8
  end
end
