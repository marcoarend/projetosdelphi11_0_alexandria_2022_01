object FmStqBalTrf: TFmStqBalTrf
  Left = 339
  Top = 185
  Caption = 'STQ-BALAN-004 :: Encerra Balan'#231'o'
  ClientHeight = 640
  ClientWidth = 734
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 77
    Width = 734
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 37
      Width = 734
      Height = 418
      ActivePage = TabSheet2
      Align = alClient
      TabHeight = 25
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Listagem dos reduzidos '
        object dmkDBGrid3: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 726
          Height = 383
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'CU_GG1'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GG1'
              Title.Caption = 'Grupo de produto'
              Width = 264
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_COR'
              Title.Caption = 'Cor'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TAM'
              Title.Caption = 'Tamanho'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsAll
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CU_GG1'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GG1'
              Title.Caption = 'Grupo de produto'
              Width = 264
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_COR'
              Title.Caption = 'Cor'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TAM'
              Title.Caption = 'Tamanho'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Por grupo de produto'
        ImageIndex = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 726
          Height = 383
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel4: TPanel
            Left = 288
            Top = 0
            Width = 438
            Height = 383
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object StaticText1: TStaticText
              Left = 0
              Top = 0
              Width = 438
              Height = 17
              Align = alTop
              Caption = 
                '  Itens do grupo selecionado que tinham estoque e n'#227'o foram lido' +
                's'
              TabOrder = 0
            end
            object dmkDBGrid1: TdmkDBGrid
              Left = 0
              Top = 17
              Width = 438
              Height = 366
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_COR'
                  Title.Caption = 'Cor'
                  Width = 264
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TAM'
                  Title.Caption = 'Tamanho'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Title.Caption = 'Quantidade'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsGGX
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_COR'
                  Title.Caption = 'Cor'
                  Width = 264
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TAM'
                  Title.Caption = 'Tamanho'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Title.Caption = 'Quantidade'
                  Visible = True
                end>
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 288
            Height = 383
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object StaticText2: TStaticText
              Left = 0
              Top = 0
              Width = 288
              Height = 17
              Align = alTop
              Caption = '  Grupos de produtos que sofreram balan'#231'os  '
              TabOrder = 0
            end
            object dmkDBGrid2: TdmkDBGrid
              Left = 0
              Top = 17
              Width = 288
              Height = 366
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 185
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsGG1
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 185
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object StaticText3: TStaticText
      Left = 0
      Top = 0
      Width = 734
      Height = 20
      Align = alTop
      Alignment = taCenter
      Caption = 
        '  Itens com estoque que ter'#227'o seu estoque zerado por n'#227'o estarem' +
        ' neste balan'#231'o!'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
    object PB1: TProgressBar
      Left = 0
      Top = 20
      Width = 734
      Height = 17
      Align = alTop
      TabOrder = 2
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 734
    Height = 29
    Align = alTop
    ParentBackground = False
    TabOrder = 1
    object Label10: TLabel
      Left = 10
      Top = 8
      Width = 90
      Height = 13
      Caption = 'Encerramento [F4]:'
    end
    object EdEncerrou: TdmkEdit
      Left = 105
      Top = 4
      Width = 117
      Height = 21
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfDateTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfLong
      Texto = '30/12/1899 00:00:00'
      QryCampo = 'Abertura'
      UpdCampo = 'Abertura'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdEncerrouKeyDown
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 29
    Width = 734
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 686
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 638
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 201
        Height = 32
        Caption = 'Encerra Balan'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 201
        Height = 32
        Caption = 'Encerra Balan'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 201
        Height = 32
        Caption = 'Encerra Balan'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 532
    Width = 734
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 730
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 576
    Width = 734
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 730
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 586
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrGG1: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrGG1AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ggx.GraGru1, gg1.CodUsu, gg1.Nome'
      'FROM stqbalits sbi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=sbi.Gragrux'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE sbi.Codigo=:P0')
    Left = 8
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGG1GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGG1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGG1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGG1: TDataSource
    DataSet = QrGG1
    Left = 36
    Top = 4
  end
  object QrBalIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT scl.Codigo StqCenCad, sbi.*'
      'FROM stqbalits sbi'
      'LEFT JOIN stqcenloc scl ON scl.Controle=sbi.StqCenLoc'
      'WHERE sbi.Codigo=:P0')
    Left = 164
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBalItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBalItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBalItsTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrBalItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrBalItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrBalItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrBalItsQtdLei: TFloatField
      FieldName = 'QtdLei'
      Required = True
    end
    object QrBalItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      Required = True
    end
    object QrBalItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrBalItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrBalItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrBalItsPecasLei: TFloatField
      FieldName = 'PecasLei'
    end
    object QrBalItsPecasAnt: TFloatField
      FieldName = 'PecasAnt'
    end
    object QrBalItsPesoLei: TFloatField
      FieldName = 'PesoLei'
    end
    object QrBalItsPesoAnt: TFloatField
      FieldName = 'PesoAnt'
    end
    object QrBalItsAreaM2Lei: TFloatField
      FieldName = 'AreaM2Lei'
    end
    object QrBalItsAreaM2Ant: TFloatField
      FieldName = 'AreaM2Ant'
    end
    object QrBalItsAreaP2Lei: TFloatField
      FieldName = 'AreaP2Lei'
    end
    object QrBalItsAreaP2Ant: TFloatField
      FieldName = 'AreaP2Ant'
    end
    object QrBalItsCustoAll: TFloatField
      FieldName = 'CustoAll'
    end
  end
  object QrGGX: TMySQLQuery
    Database = Dmod.MyDB
    Filter = 'Qtde <> 0'
    Filtered = True
    SQL.Strings = (
      'SELECT smia.GraGruX, SUM(smia.Qtde) Qtde,'
      'gcc.CodUsu CU_COR, gcc.Nome NO_COR, gti.Nome NO_TAM'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX'
      
        'LEFT JOIN stqbalits sbi ON sbi.GraGruX=smia.GraGruX AND sbi.Codi' +
        'go=:P0'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE sbi.GraGruX IS NULL'
      'AND smia.Empresa=:P1'
      'AND smia.StqCenCad=:P2'
      'AND ggx.GraGru1=:P3'
      'GROUP BY smia.GraGruX')
    Left = 72
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object DsGGX: TDataSource
    DataSet = QrGGX
    Left = 96
    Top = 4
  end
  object QrAll: TMySQLQuery
    Database = Dmod.MyDB
    Filter = 'Qtde <> 0'
    Filtered = True
    SortFieldNames = 'NO_COR ASC'
    SQL.Strings = (
      'SELECT smia.GraGruX, SUM(smia.Qtde) Qtde,'
      'gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gti.Nome NO_TAM,'
      'gg1.CodUsu CU_GG1, gg1.Nome NO_GG1'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX'
      
        'LEFT JOIN stqbalits sbi ON sbi.GraGruX=smia.GraGruX AND sbi.Codi' +
        'go=:P0'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE sbi.GraGruX IS NULL'
      'AND smia.Empresa=:P1'
      'AND smia.StqCenCad=:P2'
      'AND ggx.GraGru1 in ('
      'SELECT DISTINCT ggx.GraGru1'
      '  FROM stqbalits sbi'
      '  LEFT JOIN gragrux ggx ON ggx.Controle=sbi.Gragrux'
      '  WHERE sbi.Codigo=:P3'
      ')'
      'GROUP BY smia.GraGruX'
      '')
    Left = 128
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrAllGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrAllQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrAllCU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
    object QrAllNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrAllNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrAllCU_GG1: TIntegerField
      FieldName = 'CU_GG1'
    end
    object QrAllNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
  end
  object DsAll: TDataSource
    DataSet = QrAll
    Left = 156
    Top = 4
  end
  object QrA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT smia.*'
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX'
      'WHERE smia.Empresa=:P0'
      'AND smia.StqCenCad=:P1'
      'AND ggx.GraGru1 IN('
      '  SELECT DISTINCT ggx.GraGru1'
      '  FROM stqbalits sbi'
      '  LEFT JOIN gragrux ggx ON ggx.Controle=sbi.Gragrux'
      '  WHERE sbi.Codigo=:P2'
      ''
      ') '
      ' ')
    Left = 164
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDescribeA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SHOW FIELDS FROM stqmovitsa ')
    Left = 164
    Top = 300
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    Left = 352
    Top = 377
    object QrItensGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Pecas) Pecas,  SUM(Peso) Peso,'
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2'
      'FROM stqmovitsa'
      'WHERE Empresa=:P0'
      'AND StqCenCad=:P1'
      'AND GraGruX=:P2')
    Left = 588
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSomaQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSomaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSomaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSomaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSomaAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSomaStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
  end
end
