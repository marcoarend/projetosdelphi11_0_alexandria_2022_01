unit GraGruAtiCorTam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, ComCtrls, dmkImage, UnDmkEnums,
  Data.DB, mySQLDbTables;

type
  TFmGraGruAtiCorTam = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    StaticText2: TStaticText;
    GradeA: TStringGrid;
    StaticText6: TStaticText;
    GradeX: TStringGrid;
    GradeC: TStringGrid;
    StaticText1: TStaticText;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtSaida: TBitBtn;
    mySQLQuery1: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure GradeAClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function  StatusVariosItensCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
    function  StatusItemCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
  public
    { Public declarations }
    FNivel1, FGraTamCad, FGraCusPrcUCodigo, FEntiPrecos, FGraGruY: Integer;
    //
    procedure ConfiguraGrades();
  end;

  var
  FmGraGruAtiCorTam: TFmGraGruAtiCorTam;

implementation

uses (*GraGruN, *)MyVCLSkin, ModProd, UnMyObjects, Module, UmySQLModule,
DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruAtiCorTam.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruAtiCorTam.ConfiguraGrades();
begin
  DmProd.ConfigGrades1(FGraTamCad, FNivel1, FGraCusPrcUCodigo, FEntiPrecos,
    GradeA, GradeX, GradeC, nil, nil, nil);
end;

procedure TFmGraGruAtiCorTam.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruAtiCorTam.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FGraGruY := 0;
  //
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmGraGruAtiCorTam.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruAtiCorTam.GradeAClick(Sender: TObject);
begin
  if (GradeA.Col = 0) or (GradeA.Row = 0) then
    StatusVariosItensCorTam(GradeA, GradeC, GradeX)
  else
    StatusItemCorTam(GradeA, GradeC, GradeX);

  //

  ConfiguraGrades();
end;

procedure TFmGraGruAtiCorTam.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel1.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel1.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmGraGruAtiCorTam.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else
    Cor := clWindow;  
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGruAtiCorTam.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeX, Rect, clBlack,
      Cor, taLeftJustify,
      GradeX.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeX, Rect, clBlack,
      Cor, taCenter,
      GradeX.Cells[Acol, ARow], 0, 0, False);
end;

function TFmGraGruAtiCorTam.StatusItemCorTam(GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Ativo, c, l, Controle, Coluna, Linha, GraGruY: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  Ativo := Geral.IMV(GridA.Cells[Coluna, Linha]);
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  Controle := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) then Exit;
  if (Ativo = 0) and (Controle = 0) then
  begin
    Ativo := 1;

    //GraGruY := DmProd.ObtemGraGruYDeGraGru1(FNivel1);
    if FGraGruY = 0 then
      GraGruY := DmProd.ObtemGraGruYDeGraGru1(FNivel1)
    else
      GraGruY := FGraGruY;
    //

    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'GraGruX', 'GraGruX', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO gragrux SET Ativo=:P0, GraGruC=:P1, ');
    Dmod.QrUpd.SQL.Add('GraGru1=:P2, GraTamI=:P3, GraGruY=:P4, Controle=:P5');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := l;
    Dmod.QrUpd.Params[02].AsInteger := FNivel1;
    Dmod.QrUpd.Params[03].AsInteger := C;
    Dmod.QrUpd.Params[04].AsInteger := GraGruY;
    Dmod.QrUpd.Params[05].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end else begin
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE gragrux SET Ativo=:P0 WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end;
  DmProd.AtualizaGradesAtivos1(FNivel1, GridA, GridC);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmGraGruAtiCorTam.StatusVariosItensCorTam(GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Coluna, Linha, Ativo, c, l, Controle, Ativos: Integer;
  ColI, ColF, RowI, RowF, CountC, CountL: Integer;
  AtivTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  //ShowMessage('Ativos: ' + IntToStr(Ativos));
  if Ativos > 0 then
    Ativo := 0
  else
    Ativo := 1;
  if Ativo = 0 then
    AtivTxt := ' desativa��o '
  else
    AtivTxt := 'ativa��o';
  //
  if (Coluna = 0) and (Linha = 0) then
  begin
    ItensTxt := 'todo grupo';
  end else if Coluna = 0 then
  begin
    ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
  end else if Linha = 0 then
  begin
    ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
  end;
  if Geral.MB_Pergunta('Confirma a ' + AtivTxt + ' de ' +
  ItensTxt + '?') <> ID_YES then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    Controle := Geral.IMV(GridC.Cells[c, l]);
    if (Ativo = 1) and (Controle = 0) then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'GraGruX', 'GraGruX', 'Controle');
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO gragrux SET Ativo=:P0, GraGruC=:P1, ');
      Dmod.QrUpd.SQL.Add('GraGru1=:P2, GraTamI=:P3, Controle=:P4');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(GridX.Cells[0, l]);
      Dmod.QrUpd.Params[02].AsInteger := FNivel1;
      Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(GridX.Cells[c, 0]);
      Dmod.QrUpd.Params[04].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE gragrux SET Ativo=:P0 WHERE Controle=:P1');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  DmProd.AtualizaGradesAtivos1(FNivel1, GridA, GridC);
  Result := True;
  Screen.Cursor := crDefault;
end;

end.
