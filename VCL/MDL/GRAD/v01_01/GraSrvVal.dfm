object FmGraSrvVal: TFmGraSrvVal
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-048 :: Tabela de Presta'#231#227'o de Servi'#231'o'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 383
        Height = 32
        Caption = 'Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 383
        Height = 32
        Caption = 'Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 383
        Height = 32
        Caption = 'Tabela de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object Label5: TLabel
          Left = 2
          Top = 149
          Width = 1004
          Height = 316
          Margins.Top = 56
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -21
          Font.Name = 'MS Reference Specialty'
          Font.Style = []
          Font.Quality = fqClearType
          ParentFont = False
          Layout = tlCenter
          ExplicitTop = 109
          ExplicitWidth = 284
          ExplicitHeight = 24
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 357
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEmpresa: TdmkEditCB
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdEmpresaRedefinido
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdClientSrv: TdmkEditCB
            Left = 432
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdClientSrvRedefinido
            DBLookupComboBox = CBClientSrv
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBClientSrv: TdmkDBLookupComboBox
            Left = 488
            Top = 20
            Width = 500
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENT'
            ListSource = DsClientes
            TabOrder = 3
            dmkEditCB = EdClientSrv
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkEmpresa: TCheckBox
            Left = 12
            Top = 2
            Width = 97
            Height = 17
            Caption = 'Empresa:'
            Checked = True
            State = cbChecked
            TabOrder = 4
            OnClick = CkEmpresaClick
          end
          object CkClientSrv: TCheckBox
            Left = 432
            Top = 2
            Width = 97
            Height = 17
            Caption = 'Cliente:'
            Checked = True
            State = cbChecked
            TabOrder = 5
            OnClick = CkEmpresaClick
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 61
          Width = 1004
          Height = 88
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object SbGraGru1: TSpeedButton
            Left = 400
            Top = 19
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbGraGru1Click
          end
          object SBGrade: TSpeedButton
            Left = 604
            Top = 18
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SBGradeClick
          end
          object SpeedButton2: TSpeedButton
            Left = 972
            Top = 18
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object SbReduzSrv: TSpeedButton
            Left = 444
            Top = 60
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbReduzSrvClick
          end
          object EdGraGru1: TdmkEditCB
            Left = 12
            Top = 19
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdGraGru1Redefinido
            DBLookupComboBox = CBGraGru1
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGru1: TdmkDBLookupComboBox
            Left = 68
            Top = 19
            Width = 329
            Height = 21
            KeyField = 'Nivel1'
            ListField = 'Nome'
            ListSource = DsGraGru1
            TabOrder = 1
            dmkEditCB = EdGraGru1
            QryCampo = 'PrdGrupTip'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdReduzSrv: TdmkEditCB
            Left = 12
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdGraGru1Redefinido
            DBLookupComboBox = CBReduzSrv
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBReduzSrv: TdmkDBLookupComboBox
            Left = 68
            Top = 60
            Width = 373
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsReduzSrv
            TabOrder = 3
            dmkEditCB = EdReduzSrv
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdGraTamCad: TdmkEditCB
            Left = 424
            Top = 19
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdGraTamCadRedefinido
            DBLookupComboBox = CBGraTamCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraTamCad: TdmkDBLookupComboBox
            Left = 464
            Top = 19
            Width = 141
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGraTamCad
            TabOrder = 5
            dmkEditCB = EdGraTamCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdGraTamIts: TdmkEditCB
            Left = 632
            Top = 19
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraTamIts
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraTamIts: TdmkDBLookupComboBox
            Left = 672
            Top = 19
            Width = 57
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsGraTamIts
            TabOrder = 7
            dmkEditCB = EdGraTamIts
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdGraCorCad: TdmkEditCB
            Left = 732
            Top = 19
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraCorCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraCorCad: TdmkDBLookupComboBox
            Left = 788
            Top = 19
            Width = 181
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGraCorCad
            TabOrder = 9
            dmkEditCB = EdGraCorCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkGraGru1: TCheckBox
            Left = 12
            Top = 2
            Width = 97
            Height = 17
            Caption = 'Produto:'
            TabOrder = 10
            OnClick = CkEmpresaClick
          end
          object CkGraTamCad: TCheckBox
            Left = 424
            Top = 2
            Width = 125
            Height = 17
            Caption = 'Grade de tamanhos:'
            TabOrder = 11
            OnClick = CkEmpresaClick
          end
          object CkGraTamIts: TCheckBox
            Left = 632
            Top = 2
            Width = 97
            Height = 17
            Caption = 'Tamanho:'
            TabOrder = 12
            OnClick = CkEmpresaClick
          end
          object CkGraCorCad: TCheckBox
            Left = 732
            Top = 2
            Width = 97
            Height = 17
            Caption = 'Cor:'
            TabOrder = 13
            OnClick = CkEmpresaClick
          end
          object CkReduzSrv: TCheckBox
            Left = 12
            Top = 42
            Width = 97
            Height = 17
            Caption = 'Servi'#231'o:'
            TabOrder = 14
            OnClick = CkEmpresaClick
          end
          object BtPesquisa: TBitBtn
            Tag = 18
            Left = 472
            Top = 44
            Width = 521
            Height = 40
            Cursor = crHandPoint
            Caption = '&Reabre pesquisa (F2)'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 15
            OnClick = BtPesquisaClick
          end
        end
        object DBGGraSrvVal: TdmkDBGridZTO
          Left = 2
          Top = 149
          Width = 1004
          Height = 316
          Align = alClient
          DataSource = DsGraSrvVal
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 2
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Empresa'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ClientSrv'
              Title.Caption = 'Cliente'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReduzSrv'
              Title.Caption = 'Servi'#231'o'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ReduzSrv'
              Title.Caption = 'Descri'#231#227'o do servi'#231'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Title.Caption = 'Refer'#234'ncia'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GG1'
              Title.Caption = 'Nome do artigo'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_COR'
              Title.Caption = 'Cor'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Tam'
              Title.Caption = 'Tamanho'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_UnidMed'
              Title.Caption = 'Unidade'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Preco'
              Title.Caption = 'Pre'#231'o'
              Width = 72
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 128
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 276
    Top = 55
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cli.Codigo, '
      'IF(cli.Tipo=0,RazaoSocial,Cli.Nome) NOMEENT '
      'FROM entidades cli '
      'ORDER BY NOMEENT')
    Left = 532
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 532
    Top = 48
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, Nome'
      'FROM GraGru1'
      'ORDER BY Nome')
    Left = 588
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 588
    Top = 48
  end
  object QrGraTamIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 724
    object QrGraTamItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraTamItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 5
    end
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM gracorcad'
      'ORDER BY Nome'
      '')
    Left = 792
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 793
    Top = 48
  end
  object DsGraTamIts: TDataSource
    DataSet = QrGraTamIts
    Left = 724
    Top = 48
  end
  object QrGraTamCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gratamcad '
      'ORDER BY Nome')
    Left = 656
    object QrGraTamCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraTamCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraTamCad: TDataSource
    DataSet = QrGraTamCad
    Left = 656
    Top = 48
  end
  object QrGraSrvVal: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraSrvValAfterOpen
    BeforeClose = QrGraSrvValBeforeClose
    SQL.Strings = (
      'SELECT gg1s.Nome NO_ReduzSrv, gg1p.UnidMed, '
      'gg1p.Referencia, gg1p.Nome NO_GG1, '
      'gtip.Nome NO_Tam, gccp.Nome NO_COR, '
      'unmp.Nome NO_UnidMed, gsv.* '
      'FROM grasrvval gsv'
      'LEFT JOIN gragrux   ggxs ON ggxs.Controle=gsv.ReduzSrv'
      'LEFT JOIN gragru1   gg1s ON ggxs.GraGru1=gg1s.Nivel1'
      'LEFT JOIN gragru1   gg1p ON gg1p.Nivel1=gsv.GraGru1'
      'LEFT JOIN gratamits gtip ON gtip.Controle=gsv.GraTamIts'
      'LEFT JOIN gracorcad gccp ON gccp.Codigo=gsv.GraCorCad'
      'LEFT JOIN unidmed   unmp ON unmp.Codigo=gg1p.UnidMed')
    Left = 276
    Top = 268
    object QrGraSrvValNO_ReduzSrv: TWideStringField
      FieldName = 'NO_ReduzSrv'
      Size = 120
    end
    object QrGraSrvValUnidMed: TIntegerField
      FieldName = 'UnidMed'
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraSrvValNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraSrvValNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 5
    end
    object QrGraSrvValNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraSrvValNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
    object QrGraSrvValEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValClientSrv: TIntegerField
      FieldName = 'ClientSrv'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValReduzSrv: TIntegerField
      FieldName = 'ReduzSrv'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValGraTamIts: TIntegerField
      FieldName = 'GraTamIts'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrGraSrvValPreco: TFloatField
      FieldName = 'Preco'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000;0'
    end
  end
  object DsGraSrvVal: TDataSource
    DataSet = QrGraSrvVal
    Left = 276
    Top = 312
  end
  object QrReduzSrv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome '
      'FROM gragru1 gg1 '
      'RIGHT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'WHERE pgt.TipPrd=4')
    Left = 864
    object QrReduzSrvControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrReduzSrvNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsReduzSrv: TDataSource
    DataSet = QrReduzSrv
    Left = 864
    Top = 48
  end
end
