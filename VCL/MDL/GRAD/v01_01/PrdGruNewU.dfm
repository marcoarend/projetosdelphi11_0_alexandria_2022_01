object FmPrdGruNewU: TFmPrdGruNewU
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-017 :: Cadastro de Produto sem Tamanho / Cor'
  ClientHeight = 702
  ClientWidth = 652
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 652
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnMudaTipo: TPanel
      Left = 0
      Top = 0
      Width = 652
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 108
        Height = 13
        Caption = 'Tipo de grupo produto:'
      end
      object EdPrdGrupTip: TdmkEditCB
        Left = 12
        Top = 20
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPrdGrupTipChange
        DBLookupComboBox = CBPrdGrupTip
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPrdGrupTip: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 557
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPrdGrupTip
        TabOrder = 1
        dmkEditCB = EdPrdGrupTip
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 48
      Width = 652
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label2: TLabel
        Left = 12
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdit1
      end
      object Label3: TLabel
        Left = 96
        Top = 4
        Width = 49
        Height = 13
        Caption = 'Produ'#231#227'o:'
        FocusControl = DBEdit2
      end
      object Label4: TLabel
        Left = 376
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Fracionado:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 220
        Top = 4
        Width = 68
        Height = 13
        Caption = 'Aplicabilidade:'
        FocusControl = DBEdit4
      end
      object Label6: TLabel
        Left = 440
        Top = 4
        Width = 29
        Height = 13
        Caption = 'N'#237'vel:'
        FocusControl = DBEdit5
      end
      object Label7: TLabel
        Left = 476
        Top = 4
        Width = 99
        Height = 13
        Caption = 'Faixa de numera'#231#227'o:'
        FocusControl = DBEdit6
      end
      object DBEdit1: TDBEdit
        Left = 12
        Top = 20
        Width = 80
        Height = 21
        DataField = 'Codigo'
        DataSource = DsPrdGrupTip
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 96
        Top = 20
        Width = 121
        Height = 21
        DataField = 'NOME_MADEBY'
        DataSource = DsPrdGrupTip
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 376
        Top = 20
        Width = 61
        Height = 21
        DataField = 'NOME_FRACIO'
        DataSource = DsPrdGrupTip
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 220
        Top = 20
        Width = 153
        Height = 21
        DataField = 'NOME_TIPPRD'
        DataSource = DsPrdGrupTip
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 440
        Top = 20
        Width = 33
        Height = 21
        DataField = 'NivCad'
        DataSource = DsPrdGrupTip
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 476
        Top = 20
        Width = 80
        Height = 21
        DataField = 'FaixaIni'
        DataSource = DsPrdGrupTip
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 556
        Top = 20
        Width = 80
        Height = 21
        DataField = 'FaixaFim'
        DataSource = DsPrdGrupTip
        TabOrder = 6
      end
    end
    object PnNiveis: TPanel
      Left = 0
      Top = 98
      Width = 652
      Height = 448
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object PnNivel3: TPanel
        Left = 0
        Top = 0
        Width = 652
        Height = 65
        Align = alTop
        TabOrder = 0
        object Label8: TLabel
          Left = 12
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label9: TLabel
          Left = 78
          Top = 20
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label14: TLabel
          Left = 556
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdNivel3
        end
        object StaticText3: TStaticText
          Left = 1
          Top = 1
          Width = 650
          Height = 17
          Align = alTop
          Alignment = taCenter
          BevelKind = bkSoft
          Caption = 'N'#205'VEL 3 (DOIS N'#205'VEIS ACIMA DO GRUPO DE PRODUTOS)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdNivel3: TDBEdit
          Left = 556
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Nivel3'
          DataSource = DsGraGru3
          Enabled = False
          TabOrder = 2
        end
        object EdCodUsu3: TdmkEditCB
          Left = 12
          Top = 36
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodUsu3Change
          DBLookupComboBox = CBCodUsu3
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCodUsu3: TdmkDBLookupComboBox
          Left = 80
          Top = 36
          Width = 472
          Height = 21
          KeyField = 'Nivel3'
          ListField = 'Nome'
          ListSource = DsGraGru3
          TabOrder = 1
          dmkEditCB = EdCodUsu3
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object PnNivel2: TPanel
        Left = 0
        Top = 65
        Width = 652
        Height = 65
        Align = alTop
        TabOrder = 1
        object Label11: TLabel
          Left = 78
          Top = 20
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label15: TLabel
          Left = 556
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdNivel2
        end
        object Label10: TLabel
          Left = 12
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object EdCodUsu2: TdmkEditCB
          Left = 12
          Top = 36
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodUsu2Change
          DBLookupComboBox = CBCodUsu2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object StaticText2: TStaticText
          Left = 1
          Top = 1
          Width = 650
          Height = 17
          Align = alTop
          Alignment = taCenter
          BevelKind = bkSoft
          Caption = 'N'#205'VEL 2 (UM N'#205'VEL ACIMA DO GRUPO DE PRODUTOS)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdNivel2: TDBEdit
          Left = 556
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Nivel2'
          DataSource = DsGraGru2
          Enabled = False
          TabOrder = 2
        end
        object CBCodUsu2: TdmkDBLookupComboBox
          Left = 80
          Top = 36
          Width = 472
          Height = 21
          KeyField = 'Nivel2'
          ListField = 'Nome'
          ListSource = DsGraGru2
          TabOrder = 1
          dmkEditCB = EdCodUsu2
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object PnNivel1: TPanel
        Left = 0
        Top = 130
        Width = 652
        Height = 187
        Align = alTop
        TabOrder = 2
        ExplicitTop = 134
        object Label13: TLabel
          Left = 94
          Top = 20
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label12: TLabel
          Left = 12
          Top = 20
          Width = 75
          Height = 13
          Caption = 'C'#243'digo [F3][F4]:'
          FocusControl = DBEdit1
        end
        object Label16: TLabel
          Left = 556
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdNivel1
        end
        object DBText1: TDBText
          Left = 152
          Top = 20
          Width = 397
          Height = 17
          DataField = 'Nome'
          DataSource = DsGraGru1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label17: TLabel
          Left = 12
          Top = 64
          Width = 117
          Height = 13
          Caption = 'Unidade de Medida [F3]:'
        end
        object SpeedButton1: TSpeedButton
          Left = 360
          Top = 80
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object Label18: TLabel
          Left = 384
          Top = 64
          Width = 27
          Height = 13
          Caption = 'NCM:'
        end
        object Label30: TLabel
          Left = 512
          Top = 64
          Width = 115
          Height = 13
          Caption = 'GTIN(EAN, DUN, UPC):'
        end
        object SpeedButton2: TSpeedButton
          Left = 484
          Top = 80
          Width = 23
          Height = 22
          Caption = '?'
          OnClick = SpeedButton2Click
        end
        object Label19: TLabel
          Left = 12
          Top = 104
          Width = 197
          Height = 13
          Caption = 'Informa'#231#245'es adicionais do produto (NF-e):'
        end
        object Label25: TLabel
          Left = 12
          Top = 144
          Width = 55
          Height = 13
          Caption = 'Refer'#234'ncia:'
        end
        object Label26: TLabel
          Left = 72
          Top = 144
          Width = 53
          Height = 13
          Caption = 'Fabricante:'
          FocusControl = DBEdit1
        end
        object SpeedButton3: TSpeedButton
          Left = 612
          Top = 160
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SpeedButton3Click
        end
        object EdCodUsu1: TdmkEdit
          Left = 12
          Top = 38
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodUsu1Change
          OnEnter = EdCodUsu1Enter
          OnExit = EdCodUsu1Exit
          OnKeyDown = EdCodUsu1KeyDown
        end
        object EdNome1: TdmkEdit
          Left = 93
          Top = 38
          Width = 459
          Height = 21
          MaxLength = 120
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnKeyDown = EdNome1KeyDown
        end
        object StaticText1: TStaticText
          Left = 1
          Top = 1
          Width = 650
          Height = 17
          Align = alTop
          Alignment = taCenter
          BevelKind = bkSoft
          Caption = 'N'#205'VEL 1 (GRUPO DE PRODUTOS)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 12
        end
        object DBEdNivel1: TDBEdit
          Left = 556
          Top = 38
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Nivel1'
          DataSource = DsGraGru1
          Enabled = False
          TabOrder = 2
        end
        object EdUnidMed: TdmkEditCB
          Left = 12
          Top = 80
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedChange
          OnKeyDown = EdUnidMedKeyDown
          DBLookupComboBox = CBUnidMed
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdSigla: TdmkEdit
          Left = 68
          Top = 80
          Width = 40
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaChange
          OnExit = EdSiglaExit
          OnKeyDown = EdSiglaKeyDown
        end
        object CBUnidMed: TdmkDBLookupComboBox
          Left = 108
          Top = 80
          Width = 252
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMed
          TabOrder = 5
          OnKeyDown = CBUnidMedKeyDown
          dmkEditCB = EdUnidMed
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNCM: TdmkEdit
          Left = 384
          Top = 80
          Width = 100
          Height = 21
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NCM'
          UpdCampo = 'NCM'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdNCMExit
        end
        object EdcGTIN_EAN: TdmkEdit
          Left = 512
          Top = 80
          Width = 121
          Height = 21
          MaxLength = 14
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'cGTIN_EAN'
          UpdCampo = 'cGTIN_EAN'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdInfAdProd: TdmkEdit
          Left = 13
          Top = 120
          Width = 620
          Height = 21
          MaxLength = 255
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfAdProd'
          UpdCampo = 'InfAdProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdNCMExit
        end
        object EdReferencia: TdmkEdit
          Left = 13
          Top = 160
          Width = 56
          Height = 21
          MaxLength = 25
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfAdProd'
          UpdCampo = 'InfAdProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdNCMExit
        end
        object EdFabricante: TdmkEditCB
          Left = 72
          Top = 160
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodUsu2Change
          DBLookupComboBox = CBFabricante
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBFabricante: TdmkDBLookupComboBox
          Left = 140
          Top = 160
          Width = 470
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsFabricas
          TabOrder = 11
          dmkEditCB = EdFabricante
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 317
        Width = 652
        Height = 131
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 3
        object TabSheet1: TTabSheet
          Caption = ' ICMS '
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 644
            Height = 103
            Align = alClient
            Caption = 
              ' CST - ICMS (Imposto sobre Circula'#231#227'o de Mercadorias e Servi'#231'os)' +
              ': '
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 640
              Height = 86
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label21: TLabel
                Left = 8
                Top = 4
                Width = 156
                Height = 13
                Caption = 'N11 - Origem da mercadoria: [F3]'
              end
              object Label22: TLabel
                Left = 8
                Top = 44
                Width = 202
                Height = 13
                Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
              end
              object EdCST_A: TdmkEdit
                Left = 8
                Top = 20
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CST_A'
                UpdCampo = 'CST_A'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCST_AChange
                OnKeyDown = EdCST_AKeyDown
              end
              object EdTextoA: TdmkEdit
                Left = 56
                Top = 20
                Width = 705
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdTextoAKeyDown
              end
              object EdCST_B: TdmkEdit
                Left = 8
                Top = 60
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '00'
                QryCampo = 'CST_B'
                UpdCampo = 'CST_B'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCST_BChange
                OnKeyDown = EdCST_BKeyDown
              end
              object EdTextoB: TdmkEdit
                Left = 56
                Top = 60
                Width = 705
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdTextoBKeyDown
              end
            end
            object Panel13: TPanel
              Left = 2
              Top = 101
              Width = 640
              Height = 0
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' IPI '
          ImageIndex = 1
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 644
            Height = 61
            Align = alTop
            Caption = ' CST - IPI (Imposto sobre produtos industrializados): '
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            object Label24: TLabel
              Left = 8
              Top = 16
              Width = 199
              Height = 13
              Caption = 'O09: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
            end
            object EdIPI_CST: TdmkEdit
              Left = 8
              Top = 32
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00'
              QryCampo = 'IPI_CST'
              UpdCampo = 'IPI_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdIPI_CSTChange
              OnKeyDown = EdIPI_CSTKeyDown
            end
            object EdTextoIPI_CST: TdmkEdit
              Left = 56
              Top = 32
              Width = 573
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdTextoIPI_CSTKeyDown
            end
          end
          object Panel4: TPanel
            Left = 0
            Top = 61
            Width = 644
            Height = 42
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label20: TLabel
              Left = 4
              Top = 16
              Width = 119
              Height = 13
              Caption = 'O06: C'#243'd. enq. legal IPI'#185':'
            end
            object Label31: TLabel
              Left = 236
              Top = 16
              Width = 112
              Height = 13
              Caption = 'I06: C'#243'digo EX da TIPI:'
            end
            object Label23: TLabel
              Left = 476
              Top = 16
              Width = 109
              Height = 13
              Caption = 'O13: % Aliquota de IPI:'
            end
            object EdIPI_cEnq: TdmkEdit
              Left = 128
              Top = 12
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '999'
              QryCampo = 'IPI_cEnq'
              UpdCampo = 'IPI_cEnq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 999
              ValWarn = False
            end
            object EdEX_TIPI: TdmkEdit
              Left = 356
              Top = 12
              Width = 44
              Height = 21
              Alignment = taRightJustify
              MaxLength = 3
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              QryCampo = 'EX_TIPI'
              UpdCampo = 'EX_TIPI'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdIPI_pIPI: TdmkEdit
              Left = 588
              Top = 12
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'IPI_pIPI'
              UpdCampo = 'IPI_pIPI'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' PIS '
          ImageIndex = 2
          object GroupBox5: TGroupBox
            Left = 0
            Top = 0
            Width = 644
            Height = 61
            Align = alTop
            Caption = ' CST - PIS (Programa de Integra'#231#227'o Social): '
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            object Label34: TLabel
              Left = 8
              Top = 16
              Width = 199
              Height = 13
              Caption = 'Q06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
            end
            object EdPIS_CST: TdmkEdit
              Left = 8
              Top = 32
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '1'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '01'
              QryCampo = 'PIS_CST'
              UpdCampo = 'PIS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
              OnChange = EdPIS_CSTChange
              OnKeyDown = EdPIS_CSTKeyDown
            end
            object EdTextoPIS_CST: TdmkEdit
              Left = 56
              Top = 32
              Width = 709
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdTextoPIS_CSTKeyDown
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 61
            Width = 644
            Height = 42
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            ExplicitHeight = 135
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' COFINS '
          ImageIndex = 3
          object GroupBox7: TGroupBox
            Left = 0
            Top = 0
            Width = 644
            Height = 61
            Align = alTop
            Caption = 
              ' CST - COFINS (Contribui'#231#227'o para o Financiamento da Seguridade S' +
              'ocial): '
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 0
            object Label50: TLabel
              Left = 8
              Top = 16
              Width = 198
              Height = 13
              Caption = 'S06: C'#243'digo situa'#231#227'o tribut'#225'ria (CST): [F3]'
            end
            object EdCOFINS_CST: TdmkEdit
              Left = 8
              Top = 32
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '1'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '01'
              QryCampo = 'COFINS_CST'
              UpdCampo = 'COFINS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
              OnChange = EdCOFINS_CSTChange
              OnKeyDown = EdCOFINS_CSTKeyDown
            end
            object EdTextoCOFINS_CST: TdmkEdit
              Left = 56
              Top = 32
              Width = 709
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdTextoCOFINS_CSTKeyDown
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 61
            Width = 644
            Height = 42
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            ExplicitHeight = 135
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 652
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 604
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 556
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 496
        Height = 32
        Caption = 'Cadastro de Produto sem Tamanho / Cor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 496
        Height = 32
        Caption = 'Cadastro de Produto sem Tamanho / Cor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 496
        Height = 32
        Caption = 'Cadastro de Produto sem Tamanho / Cor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 594
    Width = 652
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 638
    Width = 652
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 504
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, '
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'pgt.TitNiv1, pgt.TitNiv2, pgt.TitNiv3,'
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO, pgt.Gradeado,'
      
        'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') NOM' +
        'E_TIPPRD'
      'FROM prdgruptip pgt'
      'ORDER BY pgt.Nome')
    Left = 8
    Top = 8
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipMadeBy: TSmallintField
      FieldName = 'MadeBy'
      Required = True
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
      Required = True
    end
    object QrPrdGrupTipTipPrd: TSmallintField
      FieldName = 'TipPrd'
      Required = True
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
      Required = True
    end
    object QrPrdGrupTipNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrPrdGrupTipNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrPrdGrupTipNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
    object QrPrdGrupTipFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
      Required = True
    end
    object QrPrdGrupTipFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
      Required = True
    end
    object QrPrdGrupTipGradeado: TSmallintField
      FieldName = 'Gradeado'
      Required = True
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Required = True
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Required = True
      Size = 15
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 36
    Top = 8
  end
  object QrGraGru3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg3.PrdGrupTip, gg3.CodUsu, gg3.Nivel3, gg3.Nome, '
      'IF(pgt.Nome <> "", pgt.Nome, "???") NOMEPGT'
      'FROM gragru3 gg3'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg3.PrdGrupTip'
      'WHERE pgt.Codigo=:P0'
      '')
    Left = 588
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru3PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru3CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru3NOMEPGT: TWideStringField
      FieldName = 'NOMEPGT'
      Size = 30
    end
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 612
    Top = 152
  end
  object QrGraGru2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg2.CodUsu, gg2.Nivel2, '
      'gg2.Nome, gg2.Nivel3, gg3.Nome NOMENIVEL3'
      'FROM gragru2 gg2'
      'LEFT JOIN gragru3 gg3 ON gg2.Nivel3=gg3.Nivel3'
      'WHERE gg2.Nivel3=:P0'
      'ORDER BY gg2.Nome')
    Left = 584
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGru2Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru2NOMENIVEL3: TWideStringField
      FieldName = 'NOMENIVEL3'
      Size = 30
    end
  end
  object DsGraGru2: TDataSource
    DataSet = QrGraGru2
    Left = 612
    Top = 220
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, gg1.CodUsu, gg1.Nivel1, '
      'gg1.Nome, gg1.Nivel2, gg2.Nome NOMENIVEL2'
      'FROM gragru1 gg1'
      'LEFT JOIN gragru2 gg2 ON gg1.Nivel2=gg2.Nivel2'
      'WHERE gg1.Nivel2=:P0')
    Left = 584
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      DisplayWidth = 120
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru1NOMENIVEL2: TWideStringField
      FieldName = 'NOMENIVEL2'
      Size = 30
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 612
    Top = 284
  end
  object QrNext: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CodUsu) Numero'
      'FROM gragru3'
      'WHERE CodUsu >= 1000'
      'AND CodUsu <=2000')
    Left = 64
    Top = 8
    object QrNextNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
  end
  object QrRange: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FaixaIni, FaixaFim'
      'FROM prdgruptip'
      'WHERE CodUsu=:P0'
      '')
    Left = 100
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRangeFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
    end
    object QrRangeFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
    end
  end
  object dmkValUsu3: TdmkValUsu
    dmkEditCB = EdCodUsu3
    Panel = Panel1
    QryCampo = 'Nivel3'
    UpdCampo = 'Nivel3'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 504
    Top = 176
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdCodUsu2
    Panel = Panel1
    QryCampo = 'Nivel2'
    UpdCampo = 'Nivel2'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 504
    Top = 244
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 448
    Top = 528
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 476
    Top = 528
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM unidmed'
      'WHERE Sigla = :P0')
    Left = 504
    Top = 528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla'
      'FROM unidmed'
      'WHERE CodUsu = :P0')
    Left = 532
    Top = 528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object QrFabricas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM Fabricas'
      'ORDER BY Nome'
      '')
    Left = 424
    Top = 8
    object QrFabricasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFabricasCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFabricasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsFabricas: TDataSource
    DataSet = QrFabricas
    Left = 452
    Top = 8
  end
end
