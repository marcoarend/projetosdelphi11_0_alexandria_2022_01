unit StqInnPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkProcFunc, dmkRadioGroup, UnDmkEnums, dmkValUsu;

type
  TFmStqInnPesq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    DsFornece: TDataSource;
    Label3: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    DBGrid1: TDBGrid;
    QrStqInnCad: TmySQLQuery;
    DsStqInnCad: TDataSource;
    QrForneceFornece_Txt: TWideStringField;
    Label6: TLabel;
    Edide_nNF: TdmkEdit;
    EdNFe_Id: TdmkEdit;
    Label192: TLabel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VUEmpresa: TdmkValUsu;
    BtPesquisa: TBitBtn;
    BtOk: TBitBtn;
    QrStqInnCadCodigo: TIntegerField;
    QrStqInnCadCodUsu: TIntegerField;
    QrStqInnCadNome: TWideStringField;
    QrStqInnCadValor: TFloatField;
    QrStqInnCadnfe_serie: TIntegerField;
    QrStqInnCadnfe_nNF: TIntegerField;
    QrStqInnCadnfe_Id: TWideStringField;
    QrStqInnCadNOMEFILIAL: TWideStringField;
    QrStqInnCadNO_FORNECE: TWideStringField;
    QrStqInnCadENCERROU_TXT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrStqInnCadBeforeClose(DataSet: TDataSet);
    procedure QrStqInnCadAfterOpen(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtOkClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenStqInnPesq();
    procedure SelecionaItem();
  public
    { Public declarations }
    FThisFatID: Integer;
  end;

  var
  FmStqInnPesq: TFmStqInnPesq;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmStqInnPesq.BtOkClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmStqInnPesq.BtPesquisaClick(Sender: TObject);
begin
  ReopenStqInnPesq();
end;

procedure TFmStqInnPesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqInnPesq.DBGrid1DblClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmStqInnPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  QrStqInnCad.Close;
end;

procedure TFmStqInnPesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  CBFornece.ListSource := DsFornece;
  //
  QrStqInnCadValor.DisplayFormat := Dmod.FStrFmtPrc;
  //
  UMyMod.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmStqInnPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqInnPesq.QrStqInnCadAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrStqInnCad.RecordCount > 0;
end;

procedure TFmStqInnPesq.QrStqInnCadBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

procedure TFmStqInnPesq.ReopenStqInnPesq();
var
  Empresa, Fornece, NF: Integer;
  NFeID, SQLEmpresa, SQLFornece, SQLNF, SQLNFeID: String;
begin
  Empresa := EdEmpresa.ValueVariant;
  Fornece := EdFornece.ValueVariant;
  NF      := Edide_nNF.ValueVariant;
  NFeID   := EdNFe_Id.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  //
  SQLEmpresa := 'WHERE sic.Empresa=' + Geral.FF0(VUEmpresa.ValueVariant);
  //
  if Fornece <> 0 then
    SQLFornece := 'AND sic.Fornece=' + Geral.FF0(Fornece);
  if NF <> 0 then
    SQLNF := 'AND sic.nfe_nNF=' + Geral.FF0(NF);
  if NFeID <> '' then
    SQLNFeID := 'AND sic.nfe_Id="' + NFeID + '"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqInnCad, Dmod.MyDB, [
    'SELECT sic.Codigo, sic.CodUsu, sic.Nome, SUM(stq.CustoAll) Valor, ',
    'sic.nfe_serie, sic.nfe_nNF, sic.nfe_Id, ',
    'IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL, ',
    'IF(frn.Tipo=0,frn.RazaoSocial,frn.Nome) NO_FORNECE, ',
    'IF(sic.Encerrou < "1900-01-01", "'+ CO_MovimentoAberto +'", DATE_FORMAT(sic.Encerrou, "%d/%m/%Y %H:%i:%s")) ENCERROU_TXT',
    'FROM stqinncad sic ',
    'LEFT JOIN entidades fil  ON fil.Codigo=sic.Empresa ',
    'LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece ',
    'LEFT JOIN stqmovitsa stq ON (stq.Tipo="'+ Geral.FF0(FThisFatID) +'" AND stq.OriCodi=sic.Codigo) ',
    SQLEmpresa,
    SQLFornece,
    SQLNF,
    SQLNFeID,
    'GROUP BY sic.Codigo ',
    'ORDER BY sic.Codigo ',
    '']);
end;

procedure TFmStqInnPesq.SelecionaItem;
begin
  if (QrStqInnCad.State <> dsInactive) and (QrStqInnCad.RecordCount > 0) then
  begin
    VAR_CADASTRO := QrStqInnCadCodigo.Value;
    Close;
  end;
end;

end.
