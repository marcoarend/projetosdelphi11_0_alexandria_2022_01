object FmGraGruRSel: TFmGraGruRSel
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-022 :: Sele'#231#227'o de Reduzido'
  ClientHeight = 620
  ClientWidth = 588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 588
    Height = 452
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 588
      Height = 76
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 32
        Width = 26
        Height = 13
        Caption = 'MD5:'
      end
      object Label2: TLabel
        Left = 276
        Top = 32
        Width = 45
        Height = 13
        Caption = 'Soundex:'
      end
      object EdNomeX: TEdit
        Left = 12
        Top = 4
        Width = 564
        Height = 24
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        OnChange = EdNomeXChange
      end
      object EdMD5: TEdit
        Left = 12
        Top = 48
        Width = 260
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 276
        Top = 48
        Width = 300
        Height = 21
        DataField = 'Soundex'
        DataSource = DataSource1
        TabOrder = 2
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 76
      Width = 588
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object Label4: TLabel
        Left = 12
        Top = 4
        Width = 89
        Height = 13
        Caption = 'Produto (reduzido):'
      end
      object SbGraGruX: TSpeedButton
        Left = 556
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbGraGruXClick
      end
      object EdGraGruX: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdCampo = 'Embalagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 485
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 1
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7CodiFldName = 'Nivel1'
        LocF7NameFldName = 'Nome'
        LocF7TableName = 'gragrux'
        LocF7SQLMasc = '$#'
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 128
      Width = 588
      Height = 324
      Align = alClient
      DataSource = DsLevenshtein
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Dist'#226'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Nome do Produto'
          Width = 415
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 588
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 540
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 492
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 257
        Height = 32
        Caption = 'Sele'#231#227'o de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 257
        Height = 32
        Caption = 'Sele'#231#227'o de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 257
        Height = 32
        Caption = 'Sele'#231#227'o de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 500
    Width = 588
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 584
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 584
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 556
    Width = 588
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 584
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 440
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtDistancia: TBitBtn
        Tag = 20
        Left = 214
        Top = 3
        Width = 159
        Height = 40
        Cursor = crHandPoint
        Caption = '&Dist'#226'ncia entre nomes'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtDistanciaClick
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, " ", '
      '  IF(gti.PrintTam=1, gti.Nome, ""), " ", '
      '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR,'
      'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM, '
      'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR, '
      'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI'
      'FROM      gragrux    ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE gg1.PrdGrupTip > -3'
      'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle')
    Left = 64
    Top = 12
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 87
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 92
    Top = 12
  end
  object PMGraGruX: TPopupMenu
    Left = 396
    Top = 104
    object CadastrosemGrade1: TMenuItem
      Caption = 'Cadastro &Sem Grade'
      object Usoeconsumo1: TMenuItem
        Caption = '&Uso e consumo'
        OnClick = Usoeconsumo1Click
      end
      object Matriaprima1: TMenuItem
        Caption = '&Mat'#233'ria-prima'
        OnClick = Matriaprima1Click
      end
    end
    object CadastroComGrade1: TMenuItem
      Caption = 'Cadastro &Com Grade'
      OnClick = CadastroComGrade1Click
    end
  end
  object QrSoundex: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SOUNDEX(:P0) Soundex')
    Left = 148
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoundexSoundex: TWideStringField
      FieldName = 'Soundex'
      Size = 4
    end
  end
  object Timer0: TTimer
    Enabled = False
    Interval = 250
    OnTimer = Timer0Timer
    Left = 20
    Top = 272
  end
  object QrGGX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY ggx.Controle')
    Left = 236
    Top = 276
    object QrGGXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object QrLevenshtein: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _cod_qtd_'
      'ORDER BY Codigo, Nome')
    Left = 352
    Top = 284
    object QrLevenshteinCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLevenshteinControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLevenshteinNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsLevenshtein: TDataSource
    DataSet = QrLevenshtein
    Left = 380
    Top = 284
  end
  object DataSource1: TDataSource
    DataSet = QrSoundex
    Left = 288
    Top = 292
  end
end
