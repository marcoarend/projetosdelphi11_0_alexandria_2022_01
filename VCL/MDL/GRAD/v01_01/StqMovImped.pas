unit StqMovImped;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids, Data.DB, dmkImage,
  UnDmkEnums, ModProd, mySQLDbTables, DmkDAC_PF, Vcl.Menus, UnProjGroup_Consts;

type
  TFmStqMovImped = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    StaticText1: TStaticText;
    StImpedimento: TStaticText;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtLocaliza: TBitBtn;
    BtSaida: TBitBtn;
    QrAux: TmySQLQuery;
    PMLocaliza: TPopupMenu;
    Faturamento1: TMenuItem;
    Locacao1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Faturamento1Click(Sender: TObject);
    procedure Locacao1Click(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
  private
    { Private declarations }
    FDataSemHora: String;
    //
    procedure ConfiguraTituloImped(MotivImped: TStqMovImped);
    procedure LocalizaFaturamento();
    procedure EncerraFatPedCab();

  public
    { Public declarations }
    FMotivImped: TStqMovImped;
  end;

  var
  FmStqMovImped: TFmStqMovImped;

implementation

{$R *.DFM}

uses UnMyObjects, UnGrade_Jan, MyListas,
{$IfNDef NAO_GFAT}UnGFat_Jan, {$EndIf}
Module, ModuleGeral, UnAppPF;

procedure TFmStqMovImped.BtEncerraClick(Sender: TObject);
var
  I: Integer;
begin
  DmProd.QrOpenX.DisableControls;
  try
    Screen.Cursor := crHourGlass;
    FDataSemHora := Geral.FDT(Trunc(DModG.ObtemAgora()), 109);
    if DBGrid1.SelectedRows.Count > 1 then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to DBGrid1.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGrid1.SelectedRows.Items[I]);
        //
        EncerraFatPedCab();
      end;
    end else
      EncerraFatPedCab();
    //
    UnDmkDAC_PF.AbreQuery(DmProd.QrOpenX, Dmod.MyDB);
    //
  finally
    DmProd.QrOpenX.EnableControls;
    Screen.Cursor := crDefault;
    // continua varios itens selecionados!!!
    DBGrid1.SelectedRows.Clear;
  end;
end;

procedure TFmStqMovImped.BtLocalizaClick(Sender: TObject);
begin
  if Uppercase(CO_SIGLA_APP) = Uppercase('TREN') then
  begin
    Locacao1.Visible := True;
    MyObjects.MostraPopUpDeBotao(PMLocaliza, BtLocaliza);
  end else
    LocalizaFaturamento();
end;

procedure TFmStqMovImped.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqMovImped.ConfiguraTituloImped(MotivImped: TStqMovImped);
var
  Txt: String;
begin
  case MotivImped of
    tsmiBalanco:
      Txt := 'BALANÇO';
    tsmiFaturamen:
      Txt := 'FATURAMENTO DE PEDIDO';
    tsmiMovimenMan:
      Txt := 'MOVIMENTAÇÃO MANUAL';
    tsmiClassifi:
      Txt := 'CLASSIFICAÇÃO';
  end;
  StImpedimento.Caption := Txt;
end;

procedure TFmStqMovImped.EncerraFatPedCab();
begin
  Dmod.MyDB.Execute(Geral.ATS([
  'UPDATE fatpedcab ',
  'SET Encerrou = "' + FDataSemHora + '" ',
  'WHERE CodUsu=' + Geral.FF0(DmProd.QrOpenXCodigo.Value),
  '']));
end;

procedure TFmStqMovImped.Faturamento1Click(Sender: TObject);
begin
  LocalizaFaturamento();
end;

procedure TFmStqMovImped.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqMovImped.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBGrid1.DataSource := DmProd.DsOpenX;
end;

procedure TFmStqMovImped.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqMovImped.FormShow(Sender: TObject);
begin
  ConfiguraTituloImped(FMotivImped);
end;

procedure TFmStqMovImped.LocalizaFaturamento();
var
  Codigo: Integer;
begin
  if (DmProd.QrOpenX.State <> dsInactive) and (DmProd.QrOpenX.RecordCount > 0) then
  begin
    Codigo := DmProd.QrOpenXCodigo.Value;
    //
    case FMotivImped of
      tsmiBalanco:
        Grade_Jan.MostraFormStqBalCad(Codigo);
{$IfNDef NAO_GFAT}
      tsmiFaturamen:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
          'SELECT fat.Codigo, ped.CodUsu ',
          'FROM fatpedcab fat ',
          'LEFT JOIN pedivda ped ON ped.Codigo = fat.Pedido ',
          'WHERE fat.Codigo=' + Geral.FF0(Codigo),
          '']);
        if Dmod.QrAux.RecordCount > 0 then
        begin
          if Dmod.QrAux.FieldByName('CodUsu').AsInteger < 1 then
            GFat_Jan.MostraFormFatDivGer(Codigo) //Sem pedido
          else
            GFat_Jan.MostraFormFatPedCab(Codigo);
        end;
        Dmod.QrAux.Close;
      end;
{$EndIf}
      tsmiMovimenMan:
        Grade_Jan.MostraFormStqManCad(Codigo);
      tsmiClassifi:
        Geral.MB_Aviso('Localização não implementada para este tipo de movimentação!');
    end;
    Close;
  end else
    Geral.MB_Aviso('Nenhum registro foi selecionado!');
end;

procedure TFmStqMovImped.Locacao1Click(Sender: TObject);
begin
  AppPF.LocalizaOrigemDoFatPedCab(DmProd.QrOpenXCodigo.Value);
end;

end.
