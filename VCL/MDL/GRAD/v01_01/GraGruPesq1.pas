unit GraGruPesq1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, ComCtrls, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums,
  dmkValUsu;

type
  TFmGraGruPesq1 = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GradeC: TStringGrid;
    TabSheet2: TTabSheet;
    GradeA: TStringGrid;
    TabSheet3: TTabSheet;
    GradeX: TStringGrid;
    Panel3: TPanel;
    Label1: TLabel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1GraTamCad: TIntegerField;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    VUGraGru1: TdmkValUsu;
    QrGraGru1UsaSubsTrib: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeCSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeCDblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReconfiguraGrade();
  public
    { Public declarations }
    FFormChamou: String;
    FGraGruX: String;
  end;

  var
  FmGraGruPesq1: TFmGraGruPesq1;

implementation

uses UnMyObjects, ModProd, GraGruN, UnInternalConsts,
  {$IFDEF FmPQE}PQ, {$ENDIF}
  MyDBCheck, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmGraGruPesq1.BtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruPesq1.BtSaidaClick(Sender: TObject);
begin
  FGraGruX := '0';
  Close;
end;

procedure TFmGraGruPesq1.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGrade();
end;

procedure TFmGraGruPesq1.EdGraGru1Exit(Sender: TObject);
begin
  ReconfiguraGrade();
end;

procedure TFmGraGruPesq1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruPesq1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  FGraGrux                     := '';
  //
  MyObjects.ConfiguraF7AppCBGraGru1(CBGraGru1);
end;

procedure TFmGraGruPesq1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruPesq1.GradeCDblClick(Sender: TObject);
begin
  if FGraGruX <> '0' then Close;
end;

procedure TFmGraGruPesq1.GradeCSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  BtOK.Enabled := (ACol > 0) and (ARow > 0);
  FGraGruX     := GradeC.Cells[ACol,ARow];
end;

procedure TFmGraGruPesq1.ReconfiguraGrade();
begin
  DmProd.ConfigGrades0(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC);
end;

procedure TFmGraGruPesq1.SpeedButton1Click(Sender: TObject);
var
  Nivel1: Integer;
begin
  VAR_CADASTRO2 := 0;
  //
  if EdGraGru1.ValueVariant <> 0 then
    Nivel1 := QrGraGru1Nivel1.Value
  else
    Nivel1 := 0;
  //
  {$IFDEF FmPQE}
  //Fazer assim por causa do PQ!
  if FFormChamou = 'FmPQE' then
  begin
    if DBCheck.CriaFm(TFmPQ, FmPQ, afmoNegarComAviso) then
    begin
      DmProd.FPrdGrupTip_Nome := EdGraGru1.Text;
      FmPQ.ShowModal;
      FmPQ.Destroy;
    end;
  end else begin
    if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
    begin
      DmProd.FPrdGrupTip_Nome := EdGraGru1.Text;
      if Nivel1 <> 0 then
        FmGraGruN.LocalizaIDNivel1(Nivel1);
      //
      FmGraGruN.ShowModal;
      FmGraGruN.Destroy;
    end;
  end;
  {$ELSE}
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdGraGru1.Text;
    if Nivel1 <> 0 then
      FmGraGruN.LocalizaIDNivel1(Nivel1);
    //
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
  {$ENDIF}
  //
  QrGraGru1.Close;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  if VAR_CADASTRO2 <> 0 then
  begin
    VUGraGru1.ValueVariant := VAR_CADASTRO2;
  end;
end;

end.
