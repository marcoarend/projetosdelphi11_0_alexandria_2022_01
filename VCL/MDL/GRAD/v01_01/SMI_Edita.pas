unit SMI_Edita;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, dmkEditCalc, Mask, dmkImage, UnDmkEnums;

type
  TFmSMI_Edita = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrGraGruXA: TmySQLQuery;
    QrGraGruXAGraGruX: TIntegerField;
    QrGraGruXANivel3: TIntegerField;
    QrGraGruXANivel2: TIntegerField;
    QrGraGruXANivel1: TIntegerField;
    QrGraGruXANome: TWideStringField;
    QrGraGruXAPrdGrupTip: TIntegerField;
    QrGraGruXAGraTamCad: TIntegerField;
    QrGraGruXANOMEGRATAMCAD: TWideStringField;
    QrGraGruXACODUSUGRATAMCAD: TIntegerField;
    QrGraGruXACST_A: TSmallintField;
    QrGraGruXACST_B: TSmallintField;
    QrGraGruXAUnidMed: TIntegerField;
    QrGraGruXANCM: TWideStringField;
    QrGraGruXAPeso: TFloatField;
    QrGraGruXASIGLAUNIDMED: TWideStringField;
    QrGraGruXACODUSUUNIDMED: TIntegerField;
    QrGraGruXANOMEUNIDMED: TWideStringField;
    QrGraGruXAHowBxaEstq: TSmallintField;
    QrGraGruXAGerBxaEstq: TSmallintField;
    QrGraGruXAFatorClas: TIntegerField;
    DsGraGruXA: TDataSource;
    QrStqCenCadA: TmySQLQuery;
    QrStqCenCadACodigo: TIntegerField;
    QrStqCenCadACodUsu: TIntegerField;
    QrStqCenCadANome: TWideStringField;
    DsStqCenCadA: TDataSource;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXNivel3: TIntegerField;
    QrGraGruXNivel2: TIntegerField;
    QrGraGruXNivel1: TIntegerField;
    QrGraGruXNome: TWideStringField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruXGraTamCad: TIntegerField;
    QrGraGruXHowBxaEstq: TSmallintField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNOMEGRATAMCAD: TWideStringField;
    QrGraGruXCODUSUGRATAMCAD: TIntegerField;
    QrGraGruXCST_A: TSmallintField;
    QrGraGruXCST_B: TSmallintField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXPeso: TFloatField;
    QrGraGruXFatorClas: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    GBNovosDados: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Panel3: TPanel;
    Label7: TLabel;
    Label12: TLabel;
    QrSMIA: TmySQLQuery;
    DBEdit1: TDBEdit;
    DsSMIA: TDataSource;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    EdFatID_Nome: TdmkEdit;
    QrSMIANO_STQCENCAD: TWideStringField;
    QrSMIANivel1: TIntegerField;
    QrSMIANO_PRD: TWideStringField;
    QrSMIAPrdGrupTip: TIntegerField;
    QrSMIAUnidMed: TIntegerField;
    QrSMIANO_PGT: TWideStringField;
    QrSMIASIGLA: TWideStringField;
    QrSMIANO_TAM: TWideStringField;
    QrSMIANO_COR: TWideStringField;
    QrSMIAGraCorCad: TIntegerField;
    QrSMIAGraGruC: TIntegerField;
    QrSMIAGraGru1: TIntegerField;
    QrSMIAGraTamI: TIntegerField;
    QrSMIADataHora: TDateTimeField;
    QrSMIAIDCtrl: TIntegerField;
    QrSMIATipo: TIntegerField;
    QrSMIAOriCodi: TIntegerField;
    QrSMIAOriCtrl: TIntegerField;
    QrSMIAOriCnta: TIntegerField;
    QrSMIAOriPart: TIntegerField;
    QrSMIAEmpresa: TIntegerField;
    QrSMIAStqCenCad: TIntegerField;
    QrSMIAGraGruX: TIntegerField;
    QrSMIAQtde: TFloatField;
    QrSMIAPecas: TFloatField;
    QrSMIAPeso: TFloatField;
    QrSMIAAreaM2: TFloatField;
    QrSMIAAreaP2: TFloatField;
    QrSMIAFatorClas: TFloatField;
    QrSMIAQuemUsou: TIntegerField;
    QrSMIARetorno: TSmallintField;
    QrSMIAParTipo: TIntegerField;
    QrSMIAParCodi: TIntegerField;
    QrSMIADebCtrl: TIntegerField;
    QrSMIAAlterWeb: TSmallintField;
    QrSMIAAtivo: TSmallintField;
    QrSMIASMIMultIns: TIntegerField;
    QrSMIACustoAll: TFloatField;
    QrSMIAValorAll: TFloatField;
    QrSMIAGrupoBal: TIntegerField;
    QrSMIABaixa: TSmallintField;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit14: TDBEdit;
    Label13: TLabel;
    DBEdit15: TDBEdit;
    Label8: TLabel;
    EdPecas: TdmkEdit;
    Label9: TLabel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    Label10: TLabel;
    Label11: TLabel;
    EdPeso: TdmkEdit;
    Label6: TLabel;
    DBEdit13: TDBEdit;
    EdQtde: TdmkEdit;
    Label14: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBEdit3Change(Sender: TObject);
    procedure QrSMIABeforeClose(DataSet: TDataSet);
    procedure QrSMIAAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPecasChange(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdAreaP2Change(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
  private
    { Private declarations }
    procedure DefineQtde();
  public
    { Public declarations }
    FNegativos: Boolean;
  end;

  var
  FmSMI_Edita: TFmSMI_Edita;

implementation

uses
{$IfNDef SemNFe_0000} ModuleNFe_0000, {$EndIf}
UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmSMI_Edita.BtOKClick(Sender: TObject);
var
  StqCenCad, GraGruX, IDCtrl: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, AntQtde: Double;
  Fator: Double;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCenCad,
    'Informe o centro de estoque!') then Exit;
  if MyObjects.FIC_Edit_Int(EdGraGruX, 'Informe o produto!', GraGruX) then Exit;
  (*if*) MyObjects.FIC_Edit_Flu(EdQtde,
    'CUIDADO! A quantidade no estoque n�o foi definida!', Qtde) (*then Exit*);
  //
  IDCtrl  := QrSMIAIDCtrl.Value;
  AntQtde := QrSMIAQtde.Value;
  //
  if FNegativos then
    Fator := -1
  else
    Fator := 1;
  Qtde    := Qtde * Fator;
  Pecas   := EdPecas.ValueVariant * Fator;
  Peso    := EdPeso.ValueVariant * Fator;
  AreaM2  := EdAreaM2.ValueVariant * Fator;
  AreaP2  := EdAreaP2.ValueVariant * Fator;
  //


  Screen.Cursor := crHourGlass;
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
    {'DataHora', 'Tipo', 'OriCodi',
    'OriCtrl', 'OriCnta', 'OriPart',
    'Empresa',} 'StqCenCad', 'GraGruX',
    'Qtde', 'Pecas', 'Peso',
    'AreaM2', 'AreaP2', {'FatorClas',
    'QuemUsou', 'Retorno', 'ParTipo',
    'ParCodi', 'DebCtrl', 'SMIMultIns',
    'CustoAll', 'ValorAll', 'GrupoBal',
    'Baixa',} 'AntQtde'], [
    'IDCtrl'], [
    {DataHora, Tipo, OriCodi,
    OriCtrl, OriCnta, OriPart,
    Empresa,} StqCenCad, GraGruX,
    Qtde, Pecas, Peso,
    AreaM2, AreaP2, {FatorClas,
    QuemUsou, Retorno, ParTipo,
    ParCodi, DebCtrl, SMIMultIns,
    CustoAll, ValorAll, GrupoBal,
    Baixa,} AntQtde], [
    IDCtrl], False) then Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSMI_Edita.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSMI_Edita.DBEdit3Change(Sender: TObject);
begin
(* ini 2023-08-16
{$IFNDef semNFe_v0000}
  EdFatID_Nome.Text := DmNFe_0000.NomeFatID_NFe(QrSMIATipo.Value);
{$EndIf}
  *)
  EdFatID_Nome.Text := DmkEnums.NomeFatID_All(QrSMIATipo.Value);
// fim 2023-08-16

end;

procedure TFmSMI_Edita.DefineQtde();
begin
  case QrGraGruXGerBxaEstq.Value of
    //0: ? ? ?
    1: EdQtde.ValueVariant := EdPecas.ValueVariant;  // Pe�a
    2: EdQtde.ValueVariant := EdAreaM2.ValueVariant; // m�
    3: EdQtde.ValueVariant := EdPeso.ValueVariant;   // kg
    4: EdQtde.ValueVariant := EdPeso.ValueVariant / 1000;   // t (ton)
    5: EdQtde.ValueVariant := EdAreaM2.ValueVariant; // ft�
    else EdQtde.ValueVariant := 0;
  end;
end;

procedure TFmSMI_Edita.EdAreaM2Change(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmSMI_Edita.EdAreaP2Change(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmSMI_Edita.EdGraGruXChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmSMI_Edita.EdPecasChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmSMI_Edita.EdPesoChange(Sender: TObject);
begin
  DefineQtde();
end;

procedure TFmSMI_Edita.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSMI_Edita.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
end;

procedure TFmSMI_Edita.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSMI_Edita.QrSMIAAfterOpen(DataSet: TDataSet);
begin
  GBNovosDados.Enabled := QrSMIA.RecordCount > 0;
end;

procedure TFmSMI_Edita.QrSMIABeforeClose(DataSet: TDataSet);
begin
  GBNovosDados.Enabled := False;
end;

end.
