unit GraGruEPat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBGridZTO, mySQLDbTables, AppListas, Variants;

type
  TFmGraGruEPat = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAltera: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DBGPsq: TdmkDBGridZTO;
    BitBtn1: TBitBtn;
    QrPsq: TMySQLQuery;
    DsPsq: TDataSource;
    Label1: TLabel;
    EdReduzido: TdmkEdit;
    EdReferencia: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdNome: TdmkEdit;
    QrPsqEstqAnt: TFloatField;
    QrPsqEstqEnt: TFloatField;
    QrPsqEstqSai: TFloatField;
    QrPsqEstqSdo: TFloatField;
    QrPsqReduzido: TIntegerField;
    QrPsqProduto: TIntegerField;
    QrPsqNO_PRD_TAM_COR: TWideStringField;
    QrPsqPatrimonio: TWideStringField;
    QrPsqReferencia: TWideStringField;
    QrPsqEstqLoc: TFloatField;
    QrPsqEmpresa: TIntegerField;
    QrEPat: TMySQLQuery;
    QrEPatGraGruX: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure QrPsqAfterOpen(DataSet: TDataSet);
    procedure DBGPsqDblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure Pesquisa();
  end;

  var
  FmGraGruEPat: TFmGraGruEPat;

implementation

uses UnMyObjects, DmkDAC_PF, Module, GetValor, UMySQLModule, UnAppPF;

{$R *.DFM}

procedure TFmGraGruEPat.BitBtn1Click(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmGraGruEPat.BtAlteraClick(Sender: TObject);
var
  GraGruX, Empresa, EstqAnt: Integer;
  SQLType: TSQLType;
var
  Qtd: Variant;
begin
  SQLType := stUpd;
  Qtd     := QrPsqEstqAnt.Value;
  GraGruX := QrPsqReduzido.Value;
  Empresa := QrPsqEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEPat, Dmod.MyDB, [
  'SELECT GraGruX ',
  'FROM gragruepat ',
  'WHERE GraGruX=' + Geral.FF0(GraGruX),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  if QrEPat.RecordCount = 0 then
  begin
    SQLType := stIns;
    Empresa := -11;
  end;
  QrEPat.Close;
  //
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Qtd, 0, 0,
  '', '', True, 'Estoque anterior', 'Informe o estoque anterior: ', 0, Qtd) then
  begin
    EstqAnt := Integer(Qtd);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruepat', False, [
    'EstqAnt'], [
    'GraGruX', 'Empresa'], [
    EstqAnt], [
    GraGruX, Empresa], True) then
    begin
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa, True);
      UnDMkDAC_PF.AbreQuery(QrPsq, Dmod.MyDB);
      QrPsq.Locate('Reduzido;Empresa', VarArrayOf([GraGruX, Empresa]), []);
    end;
  end;
end;

procedure TFmGraGruEPat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruEPat.DBGPsqDblClick(Sender: TObject);
begin
  if BtAltera.Enabled then
    BtAlteraClick(BtAltera);
end;

procedure TFmGraGruEPat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruEPat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmGraGruEPat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruEPat.Pesquisa();
var
  Reduzido: Integer;
  Referencia, Nome: String;
  SQL_Reduzido, SQL_Referencia, SQL_Nome: String;
begin
  Reduzido   := EdReduzido.ValueVariant;
  Referencia := EdReferencia.ValueVariant;
  Nome       := EdNome.ValueVariant;
  //
  SQL_Reduzido   := '';
  SQL_Referencia := '';
  SQL_Nome       := '';
  //
  if Reduzido <> 0 then
    SQL_Reduzido   := 'AND ggx.Controle=' + Geral.FF0(Reduzido);
  //
  if Trim(Referencia) <> EmptyStr then
    SQL_Referencia   := 'AND gg1.Referencia="' + ReFerencia + '"';
  //
  if Trim(Nome) <> EmptyStr then
    SQL_Nome   := Geral.ATS([
      'AND CONCAT(gg1.Nome,  ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'LIKE "%' + Nome + '%"  ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
  'SELECT pat.EstqAnt, pat.EstqEnt, pat.EstqLoc, pat.EstqSai, pat.EstqSdo, ',
  'pat.Empresa, ggx.Controle Reduzido, ggx.GraGru1 Produto, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, gg1.Patrimonio, gg1.Referencia   ',
  'FROM gragrux ggx  ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN gragruepat pat ON pat.GraGruX=ggx.Controle ',
  'WHERE ggx.GraGruY IN( ' + CO_CONTROLE_ESTOQUE_GRAGRUY + ')',
  SQL_Reduzido,
  SQL_Referencia,
  SQL_Nome,
  'ORDER BY NO_PRD_TAM_COR  ',
  '']);
  //Geral.MB_Teste(QrPsq.SQL.Text);
end;

procedure TFmGraGruEPat.QrPsqAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrPsq.RecordCount > 0;
end;

procedure TFmGraGruEPat.QrPsqBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
end;

end.
