object FmStqManLei: TFmStqManLei
  Left = 339
  Top = 185
  Caption = 'STQ-MANUA-002 :: Leitura de Movimenta'#231#227'o Manual'
  ClientHeight = 462
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 993
    Height = 308
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 556
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 4
          Width = 90
          Height = 13
          Caption = 'Centro de estoque:'
          Enabled = False
        end
        object Label11: TLabel
          Left = 378
          Top = 4
          Width = 83
          Height = 13
          Caption = 'Total quantidade:'
          FocusControl = DBEdit4
        end
        object Label12: TLabel
          Left = 465
          Top = 4
          Width = 53
          Height = 13
          Caption = 'Total valor:'
          FocusControl = DBEdit6
        end
        object EdStqCenCad: TdmkEditCB
          Left = 12
          Top = 20
          Width = 55
          Height = 20
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdStqCenCadChange
          OnEnter = EdStqCenCadEnter
          DBLookupComboBox = CBStqCenCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenCad: TdmkDBLookupComboBox
          Left = 67
          Top = 20
          Width = 308
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsStqCenCad
          TabOrder = 1
          dmkEditCB = EdStqCenCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object DBEdit4: TDBEdit
          Left = 378
          Top = 20
          Width = 84
          Height = 21
          TabStop = False
          DataField = 'QtdLei'
          DataSource = DsTotal
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 465
          Top = 20
          Width = 84
          Height = 21
          TabStop = False
          DataField = 'Valor'
          DataSource = DsTotal
          TabOrder = 3
        end
      end
      object Panel9: TPanel
        Left = 556
        Top = 0
        Width = 437
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 1
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 158
          Height = 13
          Caption = 'Localiza'#231#227'o (apenas informativo):'
        end
        object EdStqCenLoc: TdmkEditCB
          Left = 4
          Top = 20
          Width = 55
          Height = 20
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBStqCenLoc
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenLoc: TdmkDBLookupComboBox
          Left = 59
          Top = 20
          Width = 355
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsStqCenLoc
          TabOrder = 1
          dmkEditCB = EdStqCenLoc
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 48
      Width = 993
      Height = 79
      ActivePage = TabSheet2
      Align = alTop
      TabHeight = 20
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' por &Leitura '
        object PnLeitura: TPanel
          Left = 0
          Top = 0
          Width = 985
          Height = 49
          Align = alClient
          BevelOuter = bvNone
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 347
            Height = 49
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label3: TLabel
              Left = 12
              Top = 4
              Width = 89
              Height = 13
              Caption = 'Leitura / digita'#231#227'o:'
            end
            object LaQtdeLei: TLabel
              Left = 193
              Top = 4
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object EdLeitura: TEdit
              Left = 12
              Top = 20
              Width = 177
              Height = 21
              MaxLength = 20
              TabOrder = 0
              OnChange = EdLeituraChange
              OnExit = EdLeituraExit
              OnKeyDown = EdLeituraKeyDown
            end
            object EdQtdLei: TdmkEdit
              Left = 193
              Top = 20
              Width = 60
              Height = 20
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnEnter = EdQtdLeiEnter
              OnKeyDown = EdQtdLeiKeyDown
            end
            object BtOK: TBitBtn
              Tag = 14
              Left = 255
              Top = 3
              Width = 89
              Height = 39
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtOKClick
            end
          end
          object PnLido: TPanel
            Left = 347
            Top = 0
            Width = 591
            Height = 49
            Align = alLeft
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 1
            Visible = False
            object Label4: TLabel
              Left = 4
              Top = 4
              Width = 86
              Height = 13
              Caption = 'Grupo de produto:'
              FocusControl = DBEdit1
            end
            object Label5: TLabel
              Left = 264
              Top = 4
              Width = 19
              Height = 13
              Caption = 'Cor:'
              FocusControl = DBEdit2
            end
            object Label6: TLabel
              Left = 441
              Top = 4
              Width = 48
              Height = 13
              Caption = 'Tamanho:'
              FocusControl = DBEdit3
            end
            object Label10: TLabel
              Left = 512
              Top = 4
              Width = 31
              Height = 13
              Caption = 'Pre'#231'o:'
              FocusControl = DBEdit5
            end
            object DBEdit1: TDBEdit
              Left = 4
              Top = 20
              Width = 256
              Height = 21
              DataField = 'NOMENIVEL1'
              DataSource = DsItem
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 264
              Top = 20
              Width = 174
              Height = 21
              DataField = 'NOMECOR'
              DataSource = DsItem
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 441
              Top = 20
              Width = 68
              Height = 21
              DataField = 'NOMETAM'
              DataSource = DsItem
              TabOrder = 2
            end
            object DBEdit5: TDBEdit
              Left = 512
              Top = 20
              Width = 71
              Height = 21
              DataField = 'CustoPreco'
              DataSource = DsItem
              TabOrder = 3
            end
          end
          object PnSimu: TPanel
            Left = 938
            Top = 0
            Width = 47
            Height = 49
            Align = alClient
            TabOrder = 2
            Visible = False
            object Label9: TLabel
              Left = 362
              Top = 4
              Width = 54
              Height = 13
              Caption = 'Sequ'#234'ncia:'
            end
            object Label8: TLabel
              Left = 280
              Top = 4
              Width = 48
              Height = 13
              Caption = 'Reduzido:'
            end
            object Label7: TLabel
              Left = 197
              Top = 4
              Width = 75
              Height = 13
              Caption = 'Ordem de serv.:'
            end
            object dmkEdit2: TdmkEdit
              Left = 280
              Top = 20
              Width = 79
              Height = 20
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000010'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 10
              ValWarn = False
              OnChange = EdLeituraChange
            end
            object dmkEdit3: TdmkEdit
              Left = 362
              Top = 20
              Width = 80
              Height = 20
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 8
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00000001'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
              OnChange = EdLeituraChange
            end
            object dmkEdit1: TdmkEdit
              Left = 197
              Top = 20
              Width = 80
              Height = 20
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdLeituraChange
            end
            object Button1: TButton
              Left = 4
              Top = 12
              Width = 182
              Height = 25
              Caption = 'Simula leitura do c'#243'digo de barras'
              TabOrder = 3
              OnClick = Button1Click
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' por &Grade '
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 985
          Height = 49
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 985
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object BtGrade: TBitBtn
              Tag = 10054
              Left = 4
              Top = 2
              Width = 89
              Height = 40
              Caption = '&Grade'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtGradeClick
            end
          end
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 127
      Width = 993
      Height = 181
      Align = alClient
      DataSource = DsLidos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Title.Caption = 'Seq.'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODUSU_NIVEL1'
          Title.Caption = 'C'#243'digo'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL1'
          Title.Caption = 'Descri'#231#227'o do grupo de produto'
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODUSU_COR'
          Title.Caption = 'Cor'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOR'
          Title.Caption = 'Descri'#231#227'o da cor'
          Width = 171
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETAM'
          Title.Caption = 'Tamanho'
          Width = 43
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 59
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 377
        Height = 31
        Caption = 'Leitura de Movimenta'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 377
        Height = 31
        Caption = 'Leitura de Movimenta'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 377
        Height = 31
        Caption = 'Leitura de Movimenta'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 398
    Width = 993
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 846
        Top = 0
        Width = 143
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object CkFixo: TCheckBox
        Left = 12
        Top = 16
        Width = 123
        Height = 17
        Caption = 'Quantidade fixa.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 450
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Excluir'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn1Click
      end
      object BitBtn2: TBitBtn
        Tag = 10107
        Left = 329
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Lista de pre'#231'os'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 355
    Width = 993
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad scc'#10
      ''
      ''
      'ORDER BY Nome')
    Left = 112
    Top = 216
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 112
    Top = 264
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 188
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenLocNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 184
    Top = 264
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, gri.Preco CustoPreco'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      
        'LEFT JOIN tabeprcgri gri ON gri.GraGruX=ggx.Controle AND gri.Tab' +
        'ePrcCab=:P0'
      'WHERE ggx.Controle=:P1')
    Left = 248
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 248
    Top = 264
  end
  object QrLidos: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLidosCalcFields
    SQL.Strings = (
      'SELECT gg1.CodUsu CODUSU_NIVEL1, gg1.Nome NOMENIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CODUSU_COR, gcc.Nome NOMECOR,'
      'gti.Nome NOMETAM, ggx.GraGru1, sbi.*'
      'FROM stqmanits sbi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=sbi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE sbi.Codigo=:P0'
      'ORDER BY Controle DESC')
    Left = 12
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLidosCODUSU_NIVEL1: TIntegerField
      FieldName = 'CODUSU_NIVEL1'
      Required = True
    end
    object QrLidosNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Size = 30
    end
    object QrLidosGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrLidosCODUSU_COR: TIntegerField
      FieldName = 'CODUSU_COR'
      Required = True
    end
    object QrLidosNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
    object QrLidosNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrLidosGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLidosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLidosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLidosGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrLidosQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrLidosValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;; '
    end
    object QrLidosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLidosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLidosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLidosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLidosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLidosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLidosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLidosPreco: TFloatField
      FieldName = 'Preco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;; '
    end
    object QrLidosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsLidos: TDataSource
    DataSet = QrLidos
    Left = 12
    Top = 264
  end
  object QrTotal: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLidosCalcFields
    SQL.Strings = (
      'SELECT SUM(sbi.Qtde) QtdLei, SUM(sbi.Valor) Valor'
      'FROM stqmanits sbi'
      'WHERE sbi.Codigo=:P0')
    Left = 56
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotalQtdLei: TFloatField
      FieldName = 'QtdLei'
    end
    object QrTotalValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsTotal: TDataSource
    DataSet = QrTotal
    Left = 56
    Top = 264
  end
end
