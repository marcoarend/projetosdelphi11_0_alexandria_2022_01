unit GraSrvValIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkEditCB, dmkDBLookupComboBox;

type
  TFmGraSrvValIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrClientes: TMySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    DsClientes: TDataSource;
    QrGraGru1: TMySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    DsGraGru1: TDataSource;
    QrGraTamCad: TMySQLQuery;
    QrGraTamCadCodigo: TIntegerField;
    QrGraTamCadNome: TWideStringField;
    DsGraTamCad: TDataSource;
    QrGraTamIts: TMySQLQuery;
    QrGraTamItsControle: TIntegerField;
    QrGraTamItsNome: TWideStringField;
    DsGraTamIts: TDataSource;
    QrGraCorCad: TMySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    EdClientSrv: TdmkEditCB;
    CBClientSrv: TdmkDBLookupComboBox;
    Panel6: TPanel;
    LaPrdGrupTip: TLabel;
    SbGraGru1: TSpeedButton;
    LaGrade: TLabel;
    SBGrade: TSpeedButton;
    Label3: TLabel;
    SbGraCorCad: TSpeedButton;
    Label4: TLabel;
    Label6: TLabel;
    EdGraGRu1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    EdGraTamCad: TdmkEditCB;
    CBGraTamCad: TdmkDBLookupComboBox;
    EdGraTamIts: TdmkEditCB;
    CBGraTamIts: TdmkDBLookupComboBox;
    EdGraCorCad: TdmkEditCB;
    CBGraCorCad: TdmkDBLookupComboBox;
    EdReduzSrv: TdmkEditCB;
    CBReduzSrv: TdmkDBLookupComboBox;
    EdPreco: TdmkEdit;
    Label7: TLabel;
    QrReduzSrv: TMySQLQuery;
    QrReduzSrvControle: TIntegerField;
    QrReduzSrvNome: TWideStringField;
    DsReduzSrv: TDataSource;
    Panel7: TPanel;
    CkContinuar: TCheckBox;
    Label5: TLabel;
    EdControle: TdmkEdit;
    SbReduzSrv: TSpeedButton;
    QrEmpresas: TMySQLQuery;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasFilial: TIntegerField;
    QrEmpresasNOMEFILIAL: TWideStringField;
    QrEmpresasCNPJ_CPF: TWideStringField;
    QrEmpresasIE: TWideStringField;
    QrEmpresasNIRE: TWideStringField;
    DsEmpresas: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbReduzSrvClick(Sender: TObject);
    procedure SbGraGru1Click(Sender: TObject);
    procedure SBGradeClick(Sender: TObject);
    procedure EdGraTamCadRedefinido(Sender: TObject);
    procedure SbGraCorCadClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure ReopenGraTamIts(GraTamCad: Integer);


  public
    { Public declarations }
    FQrIts: TmySQLQuery;
  end;

  var
  FmGraSrvValIts: TFmGraSrvValIts;

implementation

uses UnMyObjects, UMySQLModule, DmkDAC_PF, UnGrade_Jan, ModuleGeral, Module;

{$R *.DFM}

procedure TFmGraSrvValIts.BtOKClick(Sender: TObject);
var
  Empresa, ClientSrv, Controle, ReduzSrv, GraGru1, GraTamIts, GraCorCad: Integer;
  Preco: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Empresa        := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
  Controle       := EdControle.ValueVariant;
  ClientSrv      := EdClientSrv.ValueVariant;
  ReduzSrv       := EdReduzSrv.ValueVariant;
  GraGru1        := EdGraGru1.ValueVariant;
  GraTamIts      := EdGraTamIts.ValueVariant;
  GraCorCad      := EdGraCorCad.ValueVariant;
  Preco          := EdPreco.ValueVariant;
  //
  if MyObjects.FIC(ClientSrv = 0, EdClientSrv, 'Informe o cliente!') then Exit;
  if MyObjects.FIC(GraGru1 = 0, EdGraGru1, 'Informe o produto!') then Exit;
  if MyObjects.FIC(ReduzSrv = 0, EdGraGru1, 'Informe o servi�o!') then Exit;
  if MyObjects.FIC(Preco = 0, EdPreco, 'Informe o pre�o!') then Exit;

  //
  Controle := UMyMod.BPGS1I32('grasrvval', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'grasrvval', False, [
  'Empresa', 'ClientSrv', 'ReduzSrv',
  'GraGru1', 'GraTamIts', 'GraCorCad',
  'Preco'], [
  'Controle'], [
  Empresa, ClientSrv, ReduzSrv,
  GraGru1, GraTamIts, GraCorCad,
  Preco], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      //
      (*
      EdClientSrv.ValueVariant   := 0;
      CBClientSrv.KeyValue       := 0;
      EdReduzSrv.ValueVariant    := 0;
      CBReduzSrv.KeyValue        := 0;
      EdGraGru1.ValueVariant     := 0;
      CBGraGru1.KeyValue         := 0;
      *)
      EdGraTamIts.ValueVariant   := 0;
      CBGraTamIts.KeyValue       := 0;
      EdGraCorCad.ValueVariant   := 0;
      CBGraCorCad.KeyValue       := 0;
      EdPreco.ValueVariant       := 0;
      //
      EdClientSrv.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmGraSrvValIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraSrvValIts.EdGraTamCadRedefinido(Sender: TObject);
var
  GraTamCad: Integer;
begin
  EdGraTamIts.ValueVariant := 0;
  CBGraTamIts.KeyValue := 0;
  GraTamCad := EdGraTamCad.ValueVariant;
  if GraTamCad = 0 then
  begin
    QrGraTamIts.Close;
  end else
  begin
    ReopenGraTamIts(GraTamCad);
  end;
  //ReopenGraSrvVal();
end;

procedure TFmGraSrvValIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraSrvValIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DModG.ReopenEmpresas(VAR_Usuario, (*Empresa*)0, EdEmpresa,
  CBEmpresa, (*FiltroSQL*)'', (*Inverte*)False, QrEmpresas);
  CBEmpresa.ListSource := DsEmpresas;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReduzSrv, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmGraSrvValIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraSrvValIts.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    if FQrIts.State <> dsInactive then
    begin
      UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
      //
      if Controle <> 0 then
        FQrIts.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmGraSrvValIts.ReopenGraTamIts(GraTamCad: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraTamIts, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM gratamIts ',
  'WHERE Codigo=' + Geral.FF0(GraTamCad),
  'ORDER BY Controle ',
  '']);
end;

procedure TFmGraSrvValIts.SbGraCorCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormGraCorCad(EdGraCorCad.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdGraCorCad, CBGraCorCad, QrGraCorCad, VAR_CADASTRO);
end;

procedure TFmGraSrvValIts.SBGradeClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormGraTamCad(EdGraTamCad.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdGraTamCad, CBGraTamCad, QrGraTamCad, VAR_CADASTRO);
end;

procedure TFmGraSrvValIts.SbGraGru1Click(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  Grade_Jan.MostraFormGraGruN(EdGraGru1.ValueVariant);
  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdGraGru1, CBGraGru1, QrGraGru1, VAR_CADASTRO2, 'Nivel1');
end;

procedure TFmGraSrvValIts.SbReduzSrvClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  Grade_Jan.MostraFormGraGruN(EdReduzSrv.ValueVariant);
  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdReduzSrv, CBReduzSrv, QrReduzSrv, VAR_CADASTRO2, 'Nivel1');
end;

end.
