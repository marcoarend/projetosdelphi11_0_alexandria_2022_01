object FmGraGruYPrecos: TFmGraGruYPrecos
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-053 :: Pre'#231'os por Grupo de Estoque'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 857
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 809
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 761
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 360
        Height = 32
        Caption = 'Pre'#231'os por Grupo de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 360
        Height = 32
        Caption = 'Pre'#231'os por Grupo de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 360
        Height = 32
        Caption = 'Pre'#231'os por Grupo de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitWidth = 857
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 853
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitWidth = 857
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 711
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 709
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 857
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 53
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 857
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 88
        Height = 13
        Caption = 'Grupo de estoque:'
      end
      object EdGraGruY: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdGraGruYRedefinido
        DBLookupComboBox = CBGraGruY
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruY: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 773
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraGruY
        TabOrder = 1
        dmkEditCB = EdGraGruY
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 53
      Width = 1008
      Height = 414
      Align = alClient
      DataSource = DsGraGruXCou
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          ReadOnly = True
          Title.Caption = 'Reduzido'
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          ReadOnly = True
          Title.Caption = 'Nome do Artigo'
          Width = 264
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BaseValCusto'
          Title.Caption = '$ Custo base'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BaseCliente'
          Title.Caption = 'Cliente Padr'#227'o'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BaseValVenda'
          Title.Caption = '$ Padr'#227'o venda'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BaseImpostos'
          Title.Caption = '% Impostos'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BasePerComis'
          Title.Caption = '% Comiss'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BasFrteVendM2'
          Title.Caption = '$ frete m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BaseValLiq'
          ReadOnly = True
          Title.Caption = '$ Padr'#227'o liquido'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrGraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggy.Codigo, ggy.Nome '
      'FROM gragruy ggy'
      'ORDER BY ggy.Nome')
    Left = 92
    Top = 233
    object QrGraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsGraGruY: TDataSource
    DataSet = QrGraGruY
    Left = 92
    Top = 277
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,'
      'ggx.GraGruY'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 300
    Top = 388
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object TbGraGruXCou: TMySQLTable
    Database = Dmod.MyDB
    Filtered = True
    BeforePost = TbGraGruXCouBeforePost
    TableName = 'gragruxcou'
    Left = 308
    Top = 320
    object TbGraGruXCouGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object TbGraGruXCouBaseValVenda: TFloatField
      FieldName = 'BaseValVenda'
      Required = True
      DisplayFormat = '#,###,###,##0.000000'
    end
    object TbGraGruXCouBaseCliente: TWideStringField
      FieldName = 'BaseCliente'
      Size = 60
    end
    object TbGraGruXCouBaseImpostos: TFloatField
      FieldName = 'BaseImpostos'
      Required = True
      DisplayFormat = '#,###,###,##0.000000'
    end
    object TbGraGruXCouBasePerComis: TFloatField
      FieldName = 'BasePerComis'
      Required = True
      DisplayFormat = '#,###,###,##0.000000'
    end
    object TbGraGruXCouBasFrteVendM2: TFloatField
      FieldName = 'BasFrteVendM2'
      Required = True
      DisplayFormat = '#,###,###,##0.000000'
    end
    object TbGraGruXCouBaseValLiq: TFloatField
      FieldName = 'BaseValLiq'
      Required = True
      DisplayFormat = '#,###,###,##0.000000'
    end
    object TbGraGruXCouNO_PRD_TAM_COR: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_PRD_TAM_COR'
      LookupDataSet = QrGraGruX
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'GraGruX'
      Size = 255
      Lookup = True
    end
    object TbGraGruXCouGraGruY: TIntegerField
      FieldKind = fkLookup
      FieldName = 'GraGruY'
      LookupDataSet = QrGraGruX
      LookupKeyFields = 'Controle'
      LookupResultField = 'GraGruY'
      KeyFields = 'GraGruX'
      Lookup = True
    end
    object TbGraGruXCouBaseValCusto: TFloatField
      FieldName = 'BaseValCusto'
      Required = True
      DisplayFormat = '#,###,###,##0.000000'
    end
  end
  object DsGraGruXCou: TDataSource
    DataSet = TbGraGruXCou
    Left = 68
    Top = 400
  end
  object QrGGXs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 424
    Top = 320
    object QrGGXsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
