unit PrdGruNewU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Mask, dmkValUsu, Variants, ComCtrls, dmkImage, UnDmkEnums;

type
  TFmPrdGruNewU = class(TForm)
    Panel1: TPanel;
    QrPrdGrupTip: TmySQLQuery;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrPrdGrupTipMadeBy: TSmallintField;
    QrPrdGrupTipFracio: TSmallintField;
    QrPrdGrupTipTipPrd: TSmallintField;
    QrPrdGrupTipNivCad: TSmallintField;
    QrPrdGrupTipNOME_MADEBY: TWideStringField;
    QrPrdGrupTipNOME_FRACIO: TWideStringField;
    QrPrdGrupTipNOME_TIPPRD: TWideStringField;
    QrPrdGrupTipFaixaIni: TIntegerField;
    QrPrdGrupTipFaixaFim: TIntegerField;
    QrPrdGrupTipGradeado: TSmallintField;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    DsPrdGrupTip: TDataSource;
    PnMudaTipo: TPanel;
    Label1: TLabel;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    QrGraGru3: TmySQLQuery;
    QrGraGru3PrdGrupTip: TIntegerField;
    QrGraGru3CodUsu: TIntegerField;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru3NOMEPGT: TWideStringField;
    DsGraGru3: TDataSource;
    QrGraGru2: TmySQLQuery;
    QrGraGru2CodUsu: TIntegerField;
    QrGraGru2Nivel2: TIntegerField;
    QrGraGru2Nome: TWideStringField;
    QrGraGru2Nivel3: TIntegerField;
    QrGraGru2NOMENIVEL3: TWideStringField;
    DsGraGru2: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1NOMENIVEL2: TWideStringField;
    DsGraGru1: TDataSource;
    PnNiveis: TPanel;
    PnNivel3: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label14: TLabel;
    StaticText3: TStaticText;
    DBEdNivel3: TDBEdit;
    EdCodUsu3: TdmkEditCB;
    PnNivel2: TPanel;
    Label11: TLabel;
    Label15: TLabel;
    EdCodUsu2: TdmkEditCB;
    StaticText2: TStaticText;
    DBEdNivel2: TDBEdit;
    Label10: TLabel;
    CBCodUsu3: TdmkDBLookupComboBox;
    CBCodUsu2: TdmkDBLookupComboBox;
    PnNivel1: TPanel;
    Label13: TLabel;
    Label12: TLabel;
    Label16: TLabel;
    EdCodUsu1: TdmkEdit;
    EdNome1: TdmkEdit;
    StaticText1: TStaticText;
    DBEdNivel1: TDBEdit;
    QrNext: TmySQLQuery;
    QrNextNumero: TIntegerField;
    QrRange: TmySQLQuery;
    QrRangeFaixaIni: TIntegerField;
    QrRangeFaixaFim: TIntegerField;
    dmkValUsu3: TdmkValUsu;
    dmkValUsu2: TdmkValUsu;
    DBText1: TDBText;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label17: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrPesq1: TmySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Sigla: TWideStringField;
    Label18: TLabel;
    EdNCM: TdmkEdit;
    EdcGTIN_EAN: TdmkEdit;
    Label30: TLabel;
    SpeedButton2: TSpeedButton;
    EdInfAdProd: TdmkEdit;
    Label19: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    EdCST_A: TdmkEdit;
    EdTextoA: TdmkEdit;
    EdCST_B: TdmkEdit;
    EdTextoB: TdmkEdit;
    Panel13: TPanel;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    Label24: TLabel;
    EdIPI_CST: TdmkEdit;
    EdTextoIPI_CST: TdmkEdit;
    TabSheet3: TTabSheet;
    GroupBox5: TGroupBox;
    Label34: TLabel;
    EdPIS_CST: TdmkEdit;
    EdTextoPIS_CST: TdmkEdit;
    TabSheet4: TTabSheet;
    GroupBox7: TGroupBox;
    Label50: TLabel;
    EdCOFINS_CST: TdmkEdit;
    EdTextoCOFINS_CST: TdmkEdit;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    Label20: TLabel;
    EdIPI_cEnq: TdmkEdit;
    Label31: TLabel;
    EdEX_TIPI: TdmkEdit;
    Label23: TLabel;
    EdIPI_pIPI: TdmkEdit;
    Label25: TLabel;
    EdReferencia: TdmkEdit;
    EdFabricante: TdmkEditCB;
    Label26: TLabel;
    CBFabricante: TdmkDBLookupComboBox;
    QrFabricas: TmySQLQuery;
    QrFabricasCodigo: TIntegerField;
    QrFabricasCodUsu: TIntegerField;
    QrFabricasNome: TWideStringField;
    SpeedButton3: TSpeedButton;
    DsFabricas: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
    procedure EdCodUsu3Change(Sender: TObject);
    procedure EdCodUsu2Change(Sender: TObject);
    procedure EdCodUsu1Change(Sender: TObject);
    procedure EdCodUsu1Enter(Sender: TObject);
    procedure EdCodUsu1Exit(Sender: TObject);
    procedure EdCodUsu1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNome1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdNCMExit(Sender: TObject);
    procedure EdCST_AChange(Sender: TObject);
    procedure EdCST_AKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTextoAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_BChange(Sender: TObject);
    procedure EdCST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTextoBKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIPI_CSTChange(Sender: TObject);
    procedure EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTextoIPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPIS_CSTChange(Sender: TObject);
    procedure EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTextoPIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCOFINS_CSTChange(Sender: TObject);
    procedure EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTextoCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton3Click(Sender: TObject);
  private
    { Private declarations }
    FJaFocou: Boolean;
    FNivel1Ant: Integer;
    //
    procedure ReopenGraGru3(Nivel3: Integer);
    procedure ReopenGraGru2(Nivel2: Integer);
    procedure ReopenGraGru1(Nivel1: Integer);
    //
    procedure KeyPressed(Key: Word; Shift: TShiftState;
              dmkEd: TdmkEdit; dmkCB: TdmkDBLookupCombobox);
    procedure VerificaNivel1(Definitivo: Boolean);
  public
    { Public declarations }
    FFocused, FGraGruX: Integer;
    procedure PesquisaPorSigla(Limpa, Avisa: Boolean);
    procedure PesquisaPorCodigo();
  end;

  var
  FmPrdGruNewU: TFmPrdGruNewU;

implementation

uses
{$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
  UnMyObjects, ModProd, GraGruN, UnMySQLCuringa, Module, UMySQLModule,
  UnInternalConsts, UnidMed, MyDBCheck, ClasFisc, Fabricas, DmkDAC_PF;

{$R *.DFM}

procedure TFmPrdGruNewU.BtOKClick(Sender: TObject);
const
  GraTamCad = -1;
  GraCorCad = -1;
  GraTamI   = -1;
var
  CodUsu1, Nivel1, Nivel2, Nivel3, PrdGrupTip, CST_A, CST_B, IPI_CST, PIS_CST,
  COFINS_CST, Fabricante, UnidMed: Integer;
  SQLType: TSQLType;
  Nome, NCM, IPI_cEnq, cGTIN_EAN, EX_TIPI, Referencia, InfAdProd: String;
  IPI_pIPI: Double;
  Controle: Integer;
  //
  GraGruC, GraGru1: Integer;
begin
  CodUsu1 := EdCodUsu1.ValueVariant;
  if QrGraGru1CodUsu.Value = CodUsu1 then
    SQLType := stUpd else SQLType := stIns;
  if SQLType <> ImgTipo.SQLType then
  begin
    case SQLType of
      stIns: Geral.MensagemBox('Altera��o cancelada!' + slineBreak + 'O Codigo ('+
        FormatFloat('0', CodUsu1) + ') n�o pode ser diferente do ID (' +
        DBEdNivel1.Text + ')!', 'Aviso', MB_OK+MB_ICONWARNING);
      stUpd: Geral.MensagemBox('Inclus�o cancelada!' + slineBreak + 'O Codigo ('+
        FormatFloat('0', CodUsu1) + ') j� est� sendo usado para o ID (' +
        DBEdNivel1.Text + ')!', 'Aviso', MB_OK+MB_ICONWARNING);
      else Geral.MensagemBox('Inclus�o / altera��o cancelada!' + slineBreak +
        'Status de Janela desconhecido!', 'ERRO', MB_OK+MB_ICONERROR);
    end;
    //
    Exit;
  end;
  Nome := EdNome1.Text;
  if Nome = '' then
  begin
    Geral.MensagemBox('Defina o nome do "' + StaticText1.Caption + '"',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  QrGraGru1.Close;
  QrGraGru1.Params[0].AsInteger := CodUsu1;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  //
  //PrdGruTip := Geral.IMV(EdPrdGruTip.Text);
  if not UMyMod.ObtemCodigoDeCodUsu(EdPrdGrupTip, PrdGrupTip,
    'Informe o tipo de grupo de produto!') then
      Exit;
  //
  if PnNivel3.Enabled then
  begin
    if EdCodUsu3.ValueVariant <> 0 then
      Nivel3 := QrGraGru3Nivel3.Value
    else
      Nivel3 := 0;
{
    if not UMyMod.ObtemCodigoDeCodUsu(EdCodUsu3, Nivel3,
      'Informe o ' + StaticText3.Caption) then
        Exit;
}
  end else
    Nivel3 := 0;
  //
  if PnNivel2.Enabled then
  begin
    if EdCodUsu2.ValueVariant <> 0 then
      Nivel2 := QrGraGru2Nivel2.Value
    else
      Nivel2 := 0;
{
    if not UMyMod.ObtemCodigoDeCodUsu(EdCodUsu2, Nivel2,
      'Informe o ' + StaticText2.Caption) then
        Exit;
}
  end else Nivel2 := 0;
  //
{
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru1', False, [
    'Nivel3', 'Nivel2', 'CodUsu', 'Nome', 'PrdGrupTip'
  ], ['Nivel1'], [
    Nivel3, Nivel2, CodUsu1, EdNome1.Text, PrdGrupTip
  ], [Nivel1], True) then
  begin
    if (SQLType = stIns) and (QrPrdGrupTipGradeado.Value = 0) then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
        'GraTamCad'], ['Nivel1'], [0], [Nivel1], True);
    if DmProd.FFmGraGruNew and (DmProd.FChamouGraGruNew = cggnFmGraGruN) then
    begin
      FmGraGruN.ReopenGraGruN(PrdGruTip);
      FmGraGruN.ReopenGraGru3(FNivel3);
      FmGraGruN.ReopenGraGru2(FNivel2);
      FmGraGruN.ReopenGraGru1(FNivel1);
    end;
  end;
}
  CST_A      := EdCST_A.ValueVariant;
  CST_B      := EdCST_B.ValueVariant;
  NCM        := EdNCM.Text;
  IPI_CST    := EdIPI_CST.ValueVariant;
  IPI_cEnq   := EdIPI_cEnq.Text;
  PIS_CST    := EdPIS_CST.ValueVariant;
  COFINS_CST := EdCOFINS_CST.ValueVariant;
  InfAdProd  := EdInfAdProd.Text;
  IPI_pIPI   := EdIPI_pIPI.ValueVariant;
  cGTIN_EAN  := EdcGTIN_EAN.Text;
  EX_TIPI    := EdEX_TIPI.Text;
  UnidMed    := EdUnidMed.ValueVariant;
  Referencia := EdReferencia.ValueVariant;
  Fabricante := EdFabricante.ValueVariant;
  //
  Nivel1 := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', SQLType,
    QrGraGru1Nivel1.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragru1', False, [
  'Nivel3', 'Nivel2', 'CodUsu',
  'Nome', 'PrdGrupTip', 'GraTamCad',
  'UnidMed', 'CST_A', 'CST_B', 'NCM',
  'IPI_CST', 'IPI_cEnq',
  'InfAdProd', 'PIS_CST', 'COFINS_CST',
  'IPI_pIPI', 'cGTIN_EAN', 'EX_TIPI',
  'Referencia', 'Fabricante'], [
  'Nivel1'], [
  Nivel3, Nivel2, CodUsu1,
  Nome, PrdGrupTip, GraTamCad,
  UnidMed, CST_A, CST_B, NCM,
  IPI_CST, IPI_cEnq,
  InfAdProd, PIS_CST, COFINS_CST,
  IPI_pIPI, cGTIN_EAN, EX_TIPI,
  Referencia, Fabricante], [
  Nivel1], True) then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
      'Nivel1', 'GraCorCad'], ['Controle'], [
      Nivel1, GraCorCad], [
      Controle], True) then ;
      //
      GraGruC := Controle;
      GraGru1 := Nivel1;
      Controle := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'gragrux', False, [
      'GraGruC', 'GraGru1', 'GraTamI'], ['Controle'], [
      GraGruC, GraGru1, GraTamI], [
      Controle], True) then FGraGruX := Controle;
    end;
    //
    Close;
  end;
end;

procedure TFmPrdGruNewU.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdGruNewU.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmPrdGruNewU.EdCodUsu1Change(Sender: TObject);
var
  Definitivo: Boolean;
begin
  //EdNome1.Text := '';  n�o pode por causa do pq
  Definitivo := not EdCodUsu1.Focused;
  VerificaNivel1(Definitivo);
{
  if (ImgTipo.SQLType = stIns) and (DmProd.FFmGraGruNew = False)
  and (DmProd.FPrdGrupTip > 0) and (DmProd.FPrdGrupTip_Nome <> '')
  and (DmProd.FChamouGraGruNew = cggnFmGraGruN) then
  begin
    FmGraGruN.FLaTipoGrupo := stUpd;
    EdNome1.Text := DmProd.FPrdGrupTip_Nome;
  end;
}
end;

procedure TFmPrdGruNewU.EdCodUsu1Enter(Sender: TObject);
begin
  FNivel1Ant := EdCodUsu1.ValueVariant;
end;

procedure TFmPrdGruNewU.EdCodUsu1Exit(Sender: TObject);
begin
  if FNivel1Ant <> EdCodUsu1.ValueVariant then
    VerificaNivel1(True);
end;

procedure TFmPrdGruNewU.EdCodUsu1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu1, nil);
end;

procedure TFmPrdGruNewU.EdCodUsu2Change(Sender: TObject);
begin
  ReopenGraGru1(0);
end;

procedure TFmPrdGruNewU.EdCodUsu3Change(Sender: TObject);
begin
  ReopenGraGru2(0);
end;

procedure TFmPrdGruNewU.EdCOFINS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdCOFINS_CST.Text);
{$EndIf}
end;

procedure TFmPrdGruNewU.EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCOFINS_CST.Text := UFinanceiro.ListaDeCST_COFINS();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdCST_AChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoA.Text := UFinanceiro.CST_A_Get(Geral.IMV(EdCST_A.Text));
{$EndIf}
end;

procedure TFmPrdGruNewU.EdCST_AKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_A.Text := UFinanceiro.ListaDeSituacaoTributaria();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdCST_BChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
{$EndIf}
end;

procedure TFmPrdGruNewU.EdCST_BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdIPI_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdIPI_CST.Text);
{$EndIf}
end;

procedure TFmPrdGruNewU.EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdIPI_CST.Text := UFinanceiro.ListaDeCST_IPI();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdNCMExit(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmPrdGruNewU.EdNome1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPressed(Key, Shift, EdCodUsu1, nil);
end;

procedure TFmPrdGruNewU.EdPIS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdPIS_CST.Text);
{$EndIf}
end;

procedure TFmPrdGruNewU.EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdPis_CST.Text := UFinanceiro.ListaDeCST_PIS();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdPrdGrupTipChange(Sender: TObject);
begin
  ReopenGraGru3(0);
end;

procedure TFmPrdGruNewU.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    PesquisaPorSigla(False, False);
end;

procedure TFmPrdGruNewU.EdSiglaExit(Sender: TObject);
begin
  PesquisaPorSigla(True, True);
end;

procedure TFmPrdGruNewU.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmPrdGruNewU.EdTextoAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_A.Text := UFinanceiro.ListaDeSituacaoTributaria();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdTextoBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdTextoCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCOFINS_CST.Text := UFinanceiro.ListaDeCST_COFINS();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdTextoIPI_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdIPI_CST.Text := UFinanceiro.ListaDeCST_IPI();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdTextoPIS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdPis_CST.Text := UFinanceiro.ListaDeCST_PIS();
{$EndIf}
end;

procedure TFmPrdGruNewU.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    PesquisaPorCodigo();
end;

procedure TFmPrdGruNewU.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmPrdGruNewU.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if not FJaFocou then
  begin
    case FFocused of
      0: if PnMudaTipo.Enabled then EdPrdGrupTip.SetFocus;
      1: if PnNivel1.Enabled then EdCodUsu1.SetFocus;
      2: if PnNivel2.Enabled then EdCodUsu2.SetFocus;
      3: if PnNivel3.Enabled then EdCodUsu3.SetFocus;
    end;
    FJaFocou := True;
  end;
end;

procedure TFmPrdGruNewU.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FJaFocou := False;
  FFocused := -1;
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFabricas, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
  FGraGruX := 0;
end;

procedure TFmPrdGruNewU.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdGruNewU.KeyPressed(Key: Word; Shift: TShiftState;
  dmkEd: TdmkEdit; dmkCB: TdmkDBLookupCombobox);
var
  Valor: Integer;
  Nivel: String;
begin
  if Key in ([VK_F3,VK_F4]) then
  begin
    Nivel := dmkEd.Name[Length(dmkEd.Name)];
    //
    if Key = VK_F4 then
    begin
      QrNext.Close;
      QrNext.SQL.Clear;
      QrNext.SQL.Add('SELECT MAX(CodUsu) Numero');
      QrNext.SQL.Add('FROM gragru'+nivel);
      if Geral.IMV(Nivel) = QrPrdGrupTipNivCad.Value then
      begin
        QrRange.Close;
        QrRange.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
        UnDmkDAC_PF.AbreQuery(QrRange, Dmod.MyDB);
        if (QrRangeFaixaIni.Value <> 0) and (QrRangeFaixaFim.Value <> 0) then
        begin
          QrNext.SQL.Add('WHERE CodUsu >= :P0');
          QrNext.SQL.Add('AND CodUsu <=:P1');
          QrNext.Params[00].AsInteger := QrRangeFaixaIni.Value;
          QrNext.Params[01].AsInteger := QrRangeFaixaFim.Value;
        end;
      end;
      //
      UnDmkDAC_PF.AbreQuery(QrNext, Dmod.MyDB);
      //
      Valor := QrNextNumero.Value;
      if QrRangeFaixaFim.Value <= QrRangeFaixaIni.Value then
        Valor := Valor + 1
      else
        if (Valor >= QrRangeFaixaIni.Value)
        and (Valor < QrRangeFaixaFim.Value) then
          Valor := Valor + 1
        else
          Valor := QrRangeFaixaIni.Value;
      //
      dmkEd.ValueVariant := Valor;
    end else
    if Key = VK_F3 then
      CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraGru'+Nivel, Dmod.MyDB,
      ''(*Extra*), dmkEd, nil, dmktfInteger);
  end;
end;

procedure TFmPrdGruNewU.PesquisaPorCodigo();
begin
  QrPesq2.Close;
  QrPesq2.Params[0].AsString := EdUnidMed.Text;
  UnDmkDAC_PF.AbreQuery(QrPesq2, Dmod.MyDB);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdSigla.ValueVariant <> QrPesq2Sigla.Value then
      EdSigla.ValueVariant := QrPesq2Sigla.Value;
  end else EdSigla.ValueVariant := '';
end;

procedure TFmPrdGruNewU.PesquisaPorSigla(Limpa, Avisa: Boolean);
var
  Sigla: String;
begin
  Sigla := EdSigla.Text;
  QrPesq1.Close;
  QrPesq1.Params[0].AsString := Sigla;
  UnDmkDAC_PF.AbreQuery(QrPesq1, Dmod.MyDB);
  if QrPesq1.RecordCount > 0 then
  begin
    if EdUnidMed.ValueVariant <> QrPesq1CodUsu.Value then
      EdUnidMed.ValueVariant := QrPesq1CodUsu.Value;
    if CBUnidMed.KeyValue     <> QrPesq1CodUsu.Value then
      CBUnidMed.KeyValue     := QrPesq1CodUsu.Value;
  end else
  begin
    if Limpa then
      EdSigla.ValueVariant := '';
    if Avisa then Geral.MensagemBox(
    'N�o foi localizado nenhum cadastro para a sigla de unidade:' + slineBreak+
    Sigla, 'Aviso', MB_OK+MB_ICONWARNING)
  end;
end;

procedure TFmPrdGruNewU.ReopenGraGru1(Nivel1: Integer);
begin
{
  EdCodUsu1.ValueVariant := Null;
  EdNome1.Text := '';
}
  //
  QrGraGru1.Close;
  if (EdCodUsu2.ValueVariant <> 0) or (PnNivel2.Enabled = False) then
  begin
    QrGraGru1.Params[0].AsInteger := QrGraGru2Nivel2.Value;
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    //
    QrGraGru1.Locate('Nivel1', Nivel1, []);
  end;
end;

procedure TFmPrdGruNewU.ReopenGraGru2(Nivel2: Integer);
begin
  EdCodUsu2.ValueVariant := Null;
  CBCodUsu2.KeyValue     := Null;
  //
  QrGraGru2.Close;
  if (EdCodUsu3.ValueVariant <> 0) or (PnNivel3.Enabled = False) then
  begin
    QrGraGru2.Params[0].AsInteger := QrGraGru3Nivel3.Value;
    UnDmkDAC_PF.AbreQuery(QrGraGru2, Dmod.MyDB);
    //
    QrGraGru2.Locate('Nivel2', Nivel2, []);
  end;
end;

procedure TFmPrdGruNewU.ReopenGraGru3(Nivel3: Integer);
begin
  EdCodUsu3.ValueVariant := Null;
  CBCodUsu3.KeyValue     := Null;
  //
  QrGraGru3.Close;
  if EdPrdGrupTip.ValueVariant <> 0 then
  begin
    QrGraGru3.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrGraGru3, Dmod.MyDB);
    //
    QrGraGru3.Locate('Nivel3', Nivel3, []);
    //
    PnNivel3.Enabled := QrPrdGrupTipNivCad.Value >= 3;
    PnNivel2.Enabled := QrPrdGrupTipNivCad.Value >= 2;
    PnNivel1.Enabled := QrPrdGrupTipNivCad.Value >= 1; // deveria ser obigat�rio?
    //
    if (PnNivel3.Enabled = False) and (PnNivel2.Enabled = True) then
      ReopenGraGru2(0);
    //
    if (PnNivel2.Enabled = False) and (PnNivel1.Enabled = True) then
      ReopenGraGru1(0);
    //
    PnNiveis.Visible := True;
  end else
  begin
    PnNiveis.Visible := False;
  end;
end;

procedure TFmPrdGruNewU.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

procedure TFmPrdGruNewU.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmClasFisc, FmClasFisc, afmoNegarComAviso) then
  begin
    FmClasFisc.BtSeleciona.Visible := True;
    FmClasFisc.ShowModal;
    if FmClasFisc.FNCMSelecio <> '' then
      EdNCM.Text := FmClasFisc.FNCMSelecio;
    FmClasFisc.Destroy;
  end;
end;

procedure TFmPrdGruNewU.SpeedButton3Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmFabricas, FmFabricas, afmoNegarComAviso) then
  begin
    FmFabricas.ShowModal;
    FmFabricas.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdFabricante, CBFabricante,
      QrFabricas, VAR_CADASTRO, 'Codigo', 'CodUsu');
    end;
  end;
end;

procedure TFmPrdGruNewU.VerificaNivel1(Definitivo: Boolean);
var
  CodUsu, Nivel2: Integer;
begin
  QrGraGru1.Close;
  CodUsu := Geral.IMV(EdCodUsu1.Text);
  if CodUsu <> 0 then
  begin
    QrGraGru1.Params[0].AsInteger := CodUsu;
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    //
    if QrGraGru1.RecordCount > 0 then
    begin
      if Definitivo then
      begin
        if PnNivel2.Visible then
          Nivel2 := Geral.IMV(EdCodUsu2.Text)
        else
          Nivel2 := 0;
        if QrGraGru1Nivel2.Value <> Nivel2 then
        begin
          Application.MessageBox(PChar('O cadastro ' + FormatFloat('0',
          QrGraGru1CodUsu.Value) + ' - ' + QrGraGru1Nome.Value +
          ' j� pertence ao ' + StaticText2.Caption + ' ' + FormatFloat('0',
          QrGraGru1Nivel2.Value) + ' "' + QrGraGru1NOMENIVEL2.Value + '"!'),
          'Aviso', MB_OK+MB_ICONWARNING);
          {
          EdCodUsu1.ValueVariant := 0;
          EdNome1.Text := '';
          }
          EdCodUsu1.SetFocus;
        end else
          (*EdNome1.Text := QrGraGru1Nome.Value*);
      end else (*EdNome1.Text := QrGraGru1Nome.Value*);
    end;
  end;
end;

end.

