object FmStqManCad: TFmStqManCad
  Left = 368
  Top = 194
  Caption = 'STQ-MANUA-001 :: Movimenta'#231#245'es Manuais'
  ClientHeight = 643
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 547
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    object PnCabeca: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 185
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 68
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 588
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 8
        Top = 44
        Width = 128
        Height = 13
        Caption = 'Tipo de grupo de produtos:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 336
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        FocusControl = DBEdit4
      end
      object Label11: TLabel
        Left = 768
        Top = 44
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label12: TLabel
        Left = 884
        Top = 44
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object Label16: TLabel
        Left = 8
        Top = 132
        Width = 59
        Height = 13
        Caption = 'Regra fiscal:'
        FocusControl = DBEdit7
      end
      object Label17: TLabel
        Left = 448
        Top = 132
        Width = 55
        Height = 13
        Caption = 'Movimento:'
        FocusControl = DBEdit9
      end
      object Label18: TLabel
        Left = 548
        Top = 132
        Width = 38
        Height = 13
        Caption = 'C'#225'lculo:'
        FocusControl = DBEdit10
      end
      object Label19: TLabel
        Left = 656
        Top = 132
        Width = 27
        Height = 13
        Caption = 'Fator:'
      end
      object Label20: TLabel
        Left = 8
        Top = 86
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit11
      end
      object Label21: TLabel
        Left = 636
        Top = 86
        Width = 80
        Height = 13
        Caption = 'Tabela de custo:'
        FocusControl = DBEdit14
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsStqManCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 152
        Top = 20
        Width = 432
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsStqManCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsStqManCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 588
        Top = 20
        Width = 408
        Height = 21
        DataField = 'NOMEFILIAL'
        DataSource = DsStqManCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 60
        Width = 324
        Height = 21
        DataField = 'NOMEPRDGRUPTIP'
        DataSource = DsStqManCad
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 336
        Top = 60
        Width = 296
        Height = 21
        DataField = 'MOMESTQCENCAD'
        DataSource = DsStqManCad
        TabOrder = 5
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 636
        Top = 44
        Width = 128
        Height = 37
        Caption = ' Casas decimais: '
        Columns = 4
        DataField = 'CasasProd'
        DataSource = DsStqManCad
        Items.Strings = (
          '0'
          '1'
          '2'
          '3')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
      object DBEdit5: TDBEdit
        Left = 768
        Top = 60
        Width = 112
        Height = 21
        DataField = 'Abertura'
        DataSource = DsStqManCad
        TabOrder = 7
      end
      object DBEdit6: TDBEdit
        Left = 884
        Top = 60
        Width = 112
        Height = 21
        DataField = 'ENCERROU_TXT'
        DataSource = DsStqManCad
        TabOrder = 8
        OnChange = DBEdit6Change
      end
      object DBEdit7: TDBEdit
        Left = 64
        Top = 148
        Width = 377
        Height = 21
        DataField = 'NO_FisRegCad'
        DataSource = DsStqManCad
        TabOrder = 14
      end
      object DBEdit8: TDBEdit
        Left = 8
        Top = 148
        Width = 56
        Height = 21
        DataField = 'CU_FisRegCad'
        DataSource = DsStqManCad
        TabOrder = 13
      end
      object DBEdit9: TDBEdit
        Left = 448
        Top = 148
        Width = 95
        Height = 21
        TabStop = False
        DataField = 'NO_TipoMov'
        DataSource = DsStqManCad
        TabOrder = 15
      end
      object DBEdit10: TDBEdit
        Left = 547
        Top = 148
        Width = 108
        Height = 21
        TabStop = False
        DataField = 'NO_TipoCalc'
        DataSource = DsStqManCad
        TabOrder = 16
      end
      object DBEdit12: TDBEdit
        Left = 656
        Top = 148
        Width = 33
        Height = 21
        DataField = 'FATOR'
        DataSource = DsStqManCad
        TabOrder = 17
      end
      object DBEdit11: TDBEdit
        Left = 8
        Top = 102
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsStqManCad
        TabOrder = 9
      end
      object DBEdit13: TDBEdit
        Left = 64
        Top = 102
        Width = 568
        Height = 21
        DataField = 'NO_CLIENTE'
        DataSource = DsStqManCad
        TabOrder = 10
      end
      object DBEdit14: TDBEdit
        Left = 636
        Top = 102
        Width = 56
        Height = 21
        DataField = 'TabelaPrc'
        DataSource = DsStqManCad
        TabOrder = 11
      end
      object DBEdit15: TDBEdit
        Left = 692
        Top = 102
        Width = 304
        Height = 21
        DataField = 'TabelaPrc_TXT'
        DataSource = DsStqManCad
        TabOrder = 12
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 216
      Width = 1008
      Height = 267
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 396
        Height = 267
        Align = alLeft
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Nome'
            Width = 217
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 75
            Visible = True
          end>
        Color = clWindow
        DataSource = DsGrupos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Nome'
            Width = 217
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 75
            Visible = True
          end>
      end
      object PageControl1: TPageControl
        Left = 396
        Top = 0
        Width = 612
        Height = 267
        ActivePage = TabSheet5
        Align = alClient
        TabOrder = 1
        TabPosition = tpBottom
        object TabSheet5: TTabSheet
          Caption = ' Sele'#231#227'o '
          ImageIndex = 2
          object GradeQ: TStringGrid
            Left = 0
            Top = 0
            Width = 604
            Height = 224
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeQDrawCell
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 224
            Width = 604
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
              'a incluir / alterar a quantidade de etiquetas.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' C'#243'digos '
          ImageIndex = 3
          object GradeC: TStringGrid
            Left = 0
            Top = 0
            Width = 606
            Height = 227
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            RowCount = 2
            TabOrder = 0
            OnDrawCell = GradeCDrawCell
            RowHeights = (
              18
              19)
          end
          object StaticText6: TStaticText
            Left = 0
            Top = 227
            Width = 502
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
              'dente na guia "Ativos".'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Ativos '
          object GradeA: TStringGrid
            Left = 0
            Top = 0
            Width = 606
            Height = 227
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeADrawCell
            RowHeights = (
              18
              18)
          end
          object StaticText2: TStaticText
            Left = 0
            Top = 227
            Width = 475
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
              'esativar o produto.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet9: TTabSheet
          Caption = ' X '
          ImageIndex = 6
          object GradeX: TStringGrid
            Left = 0
            Top = 0
            Width = 606
            Height = 243
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            RowHeights = (
              18
              18)
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 483
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtMovim: TBitBtn
          Tag = 521
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Movim.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtMovimClick
        end
        object BtLeitura: TBitBtn
          Tag = 525
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtLeituraClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 547
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 217
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object dmkLabel1: TdmkLabel
        Left = 588
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel2: TdmkLabel
        Left = 8
        Top = 44
        Width = 126
        Height = 13
        Caption = 'Tipo de Grupo de Produto:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel3: TdmkLabel
        Left = 336
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label10: TLabel
        Left = 768
        Top = 44
        Width = 43
        Height = 13
        Caption = 'Abertura:'
        Enabled = False
      end
      object dmkLabel4: TdmkLabel
        Left = 8
        Top = 134
        Width = 438
        Height = 13
        Caption = 
          'Regra fiscal (precisa ser aplica'#231#227'o igual a "Movimento avulso" e' +
          ' c'#225'lculo diferente de "Nulo":'
        UpdType = utYes
        SQLType = stNil
      end
      object Label13: TLabel
        Left = 755
        Top = 134
        Width = 55
        Height = 13
        Caption = 'Movimento:'
        FocusControl = EdDBTipoMov
      end
      object Label14: TLabel
        Left = 855
        Top = 134
        Width = 38
        Height = 13
        Caption = 'C'#225'lculo:'
        FocusControl = EdDBTipoCalc
      end
      object Label15: TLabel
        Left = 963
        Top = 134
        Width = 27
        Height = 13
        Caption = 'Fator:'
      end
      object SpeedButton5: TSpeedButton
        Left = 731
        Top = 150
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object LaCliente: TdmkLabel
        Left = 8
        Top = 84
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        Enabled = False
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel5: TdmkLabel
        Left = 8
        Top = 177
        Width = 189
        Height = 13
        Caption = 'Lista de valores para custo de produtos:'
        UpdType = utYes
        SQLType = stNil
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 152
        Top = 20
        Width = 433
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdFilial: TdmkEditCB
        Left = 588
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFilialChange
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 644
        Top = 20
        Width = 352
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DsFiliais
        TabOrder = 4
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPrdGrupTip: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPrdGrupTipChange
        DBLookupComboBox = CBPrdGrupTip
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPrdGrupTip: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 268
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPrdGrupTip
        TabOrder = 6
        dmkEditCB = EdPrdGrupTip
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdStqCenCad: TdmkEditCB
        Left = 336
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdStqCenCadChange
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 396
        Top = 60
        Width = 236
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 8
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGCasasProd: TdmkRadioGroup
        Left = 636
        Top = 44
        Width = 128
        Height = 37
        Caption = ' Casas decimais: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          '0'
          '1'
          '2'
          '3')
        TabOrder = 9
        QryCampo = 'CasasProd'
        UpdCampo = 'CasasProd'
        UpdType = utYes
        OldValor = 0
      end
      object EdAbertura: TdmkEdit
        Left = 768
        Top = 60
        Width = 228
        Height = 21
        Enabled = False
        TabOrder = 10
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'Abertura'
        UpdCampo = 'Abertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFisRegCad: TdmkEditCB
        Left = 8
        Top = 150
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFisRegCadChange
        DBLookupComboBox = CBFisRegCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFisRegCad: TdmkDBLookupComboBox
        Left = 64
        Top = 150
        Width = 663
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 14
        dmkEditCB = EdFisRegCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdDBTipoMov: TDBEdit
        Left = 755
        Top = 150
        Width = 95
        Height = 21
        TabStop = False
        DataField = 'NO_TipoMov'
        DataSource = DsFisRegCad
        TabOrder = 15
      end
      object EdDBTipoCalc: TDBEdit
        Left = 855
        Top = 150
        Width = 108
        Height = 21
        TabStop = False
        DataField = 'NO_TipoCalc'
        DataSource = DsFisRegCad
        TabOrder = 16
      end
      object EdEntraSai: TdmkEdit
        Left = 963
        Top = 150
        Width = 33
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'EntraSai'
        UpdCampo = 'EntraSai'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPrdGrupTipChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 64
        Top = 100
        Width = 932
        Height = 21
        Enabled = False
        KeyField = 'CodUsu'
        ListField = 'NOMEENT'
        TabOrder = 12
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTabelaPrc: TdmkEditCB
        Left = 8
        Top = 193
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TabelaPrc'
        UpdCampo = 'TabelaPrc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFisRegCadChange
        DBLookupComboBox = CBTabelaPrc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTabelaPrc: TdmkDBLookupComboBox
        Left = 65
        Top = 193
        Width = 662
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraCusPrc
        TabOrder = 19
        dmkEditCB = EdTabelaPrc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 483
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 1
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 1
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 301
        Height = 32
        Caption = 'Movimenta'#231#245'es Manuais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 301
        Height = 32
        Caption = 'Movimenta'#231#245'es Manuais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 301
        Height = 32
        Caption = 'Movimenta'#231#245'es Manuais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsStqManCad: TDataSource
    DataSet = QrStqManCad
    Left = 332
    Top = 100
  end
  object QrStqManCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrStqManCadBeforeOpen
    AfterOpen = QrStqManCadAfterOpen
    BeforeClose = QrStqManCadBeforeClose
    AfterScroll = QrStqManCadAfterScroll
    OnCalcFields = QrStqManCadCalcFields
    SQL.Strings = (
      'SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,'
      'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLIENTE,'
      'pgt.Nome NOMEPRDGRUPTIP, scc.Nome MOMESTQCENCAD,'
      'pem.BalQtdItem, pem.FatSemEstq, frc.Nome NO_FisRegCad,'
      'frc.CodUsu CU_FisRegCad, frm.TipoMov, '
      'ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov,'
      'frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",'
      '"Subtrai","?") NO_TipoCalc, sbc.TabelaPrc GraCusPrc, sbc.*'
      'FROM stqmancad sbc'
      'LEFT JOIN entidades fil  ON fil.Codigo=sbc.Empresa'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=sbc.PrdGrupTip'
      'LEFT JOIN stqcencad scc  ON scc.Codigo=sbc.StqCenCad'
      'LEFT JOIN paramsemp pem  ON pem.Codigo=sbc.Empresa'
      'LEFT JOIN fisregcad frc ON frc.Codigo=sbc.FisRegCad'
      'LEFT JOIN entidades cli ON cli.Codigo=sbc.Cliente'
      'LEFT JOIN fisregmvt frm ON frm.Codigo=frc.Codigo'
      '     AND frm.TipoCalc > 0'
      '     AND frm.Empresa=sbc.Empresa'
      'WHERE sbc.Codigo > -1000')
    Left = 304
    Top = 100
    object QrStqManCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqManCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqManCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrStqManCadEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrStqManCadPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrStqManCadStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Required = True
    end
    object QrStqManCadCasasProd: TSmallintField
      FieldName = 'CasasProd'
      Required = True
    end
    object QrStqManCadAbertura: TDateTimeField
      FieldName = 'Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrStqManCadEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrStqManCadNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrStqManCadNOMEPRDGRUPTIP: TWideStringField
      FieldName = 'NOMEPRDGRUPTIP'
      Size = 30
    end
    object QrStqManCadMOMESTQCENCAD: TWideStringField
      FieldName = 'MOMESTQCENCAD'
      Size = 50
    end
    object QrStqManCadBalQtdItem: TFloatField
      FieldName = 'BalQtdItem'
    end
    object QrStqManCadENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 50
      Calculated = True
    end
    object QrStqManCadFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Required = True
    end
    object QrStqManCadStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrStqManCadFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
    end
    object QrStqManCadNO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Size = 50
    end
    object QrStqManCadCU_FisRegCad: TIntegerField
      FieldName = 'CU_FisRegCad'
      Required = True
    end
    object QrStqManCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
    end
    object QrStqManCadNO_TipoMov: TWideStringField
      FieldName = 'NO_TipoMov'
      Size = 7
    end
    object QrStqManCadTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
    object QrStqManCadNO_TipoCalc: TWideStringField
      FieldName = 'NO_TipoCalc'
      Size = 8
    end
    object QrStqManCadEntraSai: TSmallintField
      FieldName = 'EntraSai'
      Required = True
    end
    object QrStqManCadFATOR: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'FATOR'
      Calculated = True
    end
    object QrStqManCadGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrStqManCadCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrStqManCadNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrStqManCadTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrStqManCadTabelaPrc_TXT: TWideStringField
      FieldName = 'TabelaPrc_TXT'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = DBEdit11
    CanUpd01 = DBEdit12
    Left = 356
    Top = 100
  end
  object PMMovim: TPopupMenu
    OnPopup = PMMovimPopup
    Left = 752
    Top = 20
    object Incluinovomovimento1: TMenuItem
      Caption = '&Inclui novo movimento'
      OnClick = Incluinovomovimento1Click
    end
    object Alteramovimentoatual1: TMenuItem
      Caption = '&Altera movimento atual'
      Enabled = False
      OnClick = Alteramovimentoatual1Click
    end
    object Encerramovimentoatual1: TMenuItem
      Caption = '&Encerra movimento atual'
      Enabled = False
      OnClick = Encerramovimentoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object EXcluimovimentoatual1: TMenuItem
      Caption = 'E&Xclui movimento atual'
      Enabled = False
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 356
    Top = 40
  end
  object QrFiliais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Filial, Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL'
      'FROM entidades'
      'WHERE Codigo<-10'
      'ORDER BY NOMEFILIAL')
    Left = 328
    Top = 40
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliaisNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdFilial
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    ValueVariant = 0
    Left = 388
    Top = 100
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Fracio, LstPrcFisc'
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 272
    Top = 40
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrPrdGrupTipLstPrcFisc: TIntegerField
      FieldName = 'LstPrcFisc'
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 300
    Top = 40
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdPrdGrupTip
    Panel = PainelEdita
    QryCampo = 'PrdGrupTip'
    UpdCampo = 'PrdGrupTip'
    RefCampo = 'Codigo'
    UpdType = utYes
    ValueVariant = 0
    Left = 416
    Top = 100
  end
  object dmkValUsu3: TdmkValUsu
    dmkEditCB = EdStqCenCad
    Panel = PainelEdita
    QryCampo = 'StqCenCad'
    UpdCampo = 'StqCenCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    ValueVariant = 0
    Left = 444
    Top = 100
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 384
    Top = 40
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 412
    Top = 40
  end
  object QrNew: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM stqmancad'
      'WHERE Encerrou=0'
      'AND Empresa=:P0'
      'AND PrdGrupTip=:P1'
      'AND StqCenCad=:P2'
      'ORDER BY Codigo')
    Left = 440
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde'
      'FROM stqmovitsa'
      'WHERE Empresa=:P0'
      'AND StqCenCad=:P1'
      'AND GraGruX=:P2'
      '')
    Left = 476
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSomaQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.Nome, frm.TipoMov,'
      'ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov, '
      'frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",'
      '"Subtrai","?") NO_TipoCalc '
      'FROM fisregmvt frm'
      'LEFT JOIN fisregcad frc ON frc.Codigo=frm.Codigo'
      'WHERE frc.Aplicacao=4'
      'AND frm.TipoCalc > 0'
      'AND frm.Empresa=:P0'
      'AND frm.StqCenCad=:P1')
    Left = 512
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Required = True
    end
    object QrFisRegCadNO_TipoMov: TWideStringField
      FieldName = 'NO_TipoMov'
      Size = 7
    end
    object QrFisRegCadTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrFisRegCadNO_TipoCalc: TWideStringField
      FieldName = 'NO_TipoCalc'
      Size = 8
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 540
    Top = 40
  end
  object dmkValUsu4: TdmkValUsu
    dmkEditCB = EdFisRegCad
    Panel = PainelEdita
    QryCampo = 'FisRegCad'
    UpdCampo = 'FisRegCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    ValueVariant = 0
    Left = 472
    Top = 100
  end
  object QrGrupos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGruposBeforeClose
    AfterScroll = QrGruposAfterScroll
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gg1.Nivel1, gti.Codigo GraTamCad, SUM(smi.Valor) Valor,'
      'smi.Controle smiCtrl'
      'FROM stqmanits smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE smi.Codigo=:P0'
      'GROUP BY ggx.GraGru1')
    Left = 512
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGruposCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrGruposNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrGruposNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGruposGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGruposValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;#,###,##0.00; '
    end
    object QrGrupossmiCtrl: TIntegerField
      FieldName = 'smiCtrl'
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 540
    Top = 364
  end
  object dmkValUsu5: TdmkValUsu
    dmkEditCB = EdCliente
    Panel = PainelEdita
    QryCampo = 'Cliente'
    UpdCampo = 'Cliente'
    RefCampo = 'Codigo'
    UpdType = utYes
    ValueVariant = 0
    Left = 500
    Top = 100
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM tabeprccab'
      'WHERE :P0 BETWEEN DataI AND DataF'
      'AND Aplicacao & 8 <> 0'
      'ORDER BY Nome')
    Left = 612
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 640
    Top = 36
  end
end
