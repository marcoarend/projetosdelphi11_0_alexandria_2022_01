unit UnGrade_PF;

interface

uses
  Vcl.ComCtrls, Vcl.ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Forms, Dialogs, Buttons, Grids, DBGrids, DBCtrls, StdCtrls,
  Variants, DB, mySQLDbTables, DmkGeral, DmkDAC_PF, UnInternalConsts,
  UnDmkProcFunc, frxClass, frxDBSet, Menus, dmkDBLookupComboBox, dmkEditCB,
  dmkEdit, dmkValUsu, UnDmkEnums, dmkRadioGroup, UnAppPF, AppListas,
  UnAppEnums, UnProjGroup_Consts, UMySQLDB, UnGrl_Vars, UnProjGroup_Vars;

type
  TUnGrade_PF = class(TObject)
  private
    { Private declarations }
    function  DefineContasContabeisNFe_PelosItens_UmItem( const TypCtbCadMoF:
              TTypCtbCadMoF; const Tipo, OriCodi, Empresa: Integer; var GenCtbD,
              GenCtbC: Integer; var Descricao: String): Boolean;
    function  DefineContasContabeisNFe_PelosItens_VariosItens( const TypCtbCadMoF:
              TTypCtbCadMoF; const Tipo, OriCodi, Empresa: Integer; var GenCtbD,
              GenCtbC: Integer; var Descricao: String): Boolean;

  public
    { Public declarations }
    procedure AlteraSMIA(Qry: TmySQLQuery);
    procedure GeraEstoqueEm_2(Empresa_Txt: String; DataIni, DataFim: TDateTime;
              PGT, GG1, GG2, GG3, StqCenCad, TipoPesq: Integer; GraCusPrc,
              TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo,
              PrdGrupTipCodigo, GraGru1Nivel1, GraGru2Nivel2,
              GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
              QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
              LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery;
              var TxtSQL_Part1, TxtSQL_Part2: String);
    procedure CadastroEDefinicaoDeUnidMed(VUUnidMed: TdmkValUsu; EdUnidMed:
              TdmkEditCB; CBUnidMed: TDmkDBLookupComboBox; QrUnidMed:
              TmySQLQuery);
    procedure ReopenStqCenLoc(Qry: TmySQLQuery; Centro: Variant; Controle:
              Integer; MovimID: TEstqMovimID = TEstqMovimID.emidAjuste);
    function  ReopenFatStqCenCad(RegrFiscal, Empresa: Integer; NomeEmp,
              NomeFisRegCad: String; QueryStqCenCad: TMySQLQuery): Boolean;
    procedure InsereFatPedFis(Tmp_FatPedFis: String; GraGru1, IDCtrl, Tipo,
              OriCodi, OriCtrl, OriCnta, OriPart, FisRegCad: Integer);
    function  ValidaPainelFisRegCad(CFOP, UFEmit, UFDest, CST_B: String;
              var Mensagem: String): Boolean;
    function  InsUpdProdNivel5(SQLTipo: TSQLType; PrdGrupTip, Nivel5,
              CodUsu: Integer; Nome: String; var Msg: String): Integer;
    function  InsUpdProdNivel4(SQLTipo: TSQLType; PrdGrupTip, Nivel5, Nivel4,
              CodUsu: Integer; Nome: String; var Msg: String): Integer;
    function  InsUpdProdNivel3(SQLTipo: TSQLType; PrdGrupTip, Nivel5, Nivel4,
              Nivel3, CodUsu: Integer; Nome: String; var Msg: String): Integer;
    function  InsUpdProdNivel2(SQLTipo: TSQLType; PrdGrupTip, Nivel5, Nivel4,
              Nivel3, Nivel2, CodUsu: Integer; Nome: String; var Msg: String):
              Integer;
    function  InsUpdProdNivel1(SQLTipo: TSQLType; GraTabApp, PrdGrupTip,
              Gradeado, UsaCores, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1,
              CodUsu: Integer; Nome: String; UnidMed: Integer; NCM: String;
              var Msg: String; ForcaCadastrGGX_SemCorTam: Boolean): Integer;
    function  RegistrarACor(const Nivel1, GraCorCad: Integer; var Controle:
              Integer): Boolean;
    function  ValidaGrandeza(TipoCalc, HowBxaEstq: Integer; Pecas, AreaM2,
              AreaP2, Peso: Double; var Msg: String): Boolean;
    //function  ComparaUnidMed2GGX(GGX1, GGX2: Integer): Boolean;
    function  SelecionaGraGruXDeGraGruE(const Fornece: Integer; const cProd,
              xProd, NCM, uCom: String; const CFOP: Integer; var GraGruX,
              GraGru1: Integer; var NomeGGX: String): TKindLocSelGGXE;
    function  ObtemFisRegMvtTipoCalc(RegrFiscal, StqCenCad, Empresa: Integer): Integer;
    function  ObtemStqMovValAPrecoF(OriCodi, OriCtrl, OriCnta, IDCtrl,
              Empresa: Integer): Double;
    function  ObtemStqMovValA_Many(const OriCodi, OriCtrl, OriCnta, IDCtrl,
              Empresa: Integer; var Qtde, Preco, Total: Double; var
              obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto:
              String): Boolean;
    function  ObtemStqMovValA_SUM(const OriCodi, Tipo, Empresa: Integer;
              var Qtde, Total: Double): Boolean;
    function  ObtemGrandezaDeGraGruX(const GraGruX: Integer; var Grandeza:
              TGrandezaUnidMed; var Tipo_Item: Integer): Boolean;
    procedure CadastraProdutoGrade(GraGruY, PrdGrupTip, GraGru1, GraGruC:
              Integer; JaTemN1: Boolean; NewNome, SQL_WHERE_PGT: String;
              ForcaCor, ForcaTam: Boolean);
    procedure CorrigeGraGruYdeGraGruX();
    procedure GraGru1_AlteraNome(Nivel1: Integer; Nome: String);
    procedure AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
    procedure DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor: TdmkEdit; GraGruX:
              Integer);
    function  DefineContasContabeisNFe_PelosItens(const TypCtbCadMoF: TTypCtbCadMoF;
              const Tipo, OriCodi, Empresa: Integer; var GenCtbD, GenCtbC:
              Integer; var Descricao: String): Boolean;
    function  SQL_Estoque(const Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip,
              StqCenCad, GraCusPrc, CGSitEstq_Value: Integer;
              const CkStqCenCad_Checked, CkSelecao0_Enabled, CkSelecao0_Checked,
              CkPrdGrupo_Checked: Boolean; InatIncTipo, InatIncOriCodi: Integer;
              const LaAviso1, LaAviso2: TLabel;
              var SQL_Sum, SQL_Its: String): Boolean;
  end;

var
  Grade_PF: TUnGrade_PF;


implementation

uses MyDBCheck, ModuleGeral, UnMyObjects, ModProd, Module, UMySQLModule,
  {$IfDef UsaContabil}
  UnContabil_PF,
  {$EndIf}
  SMI_Edita, UnidMed, GraGru1CorTam, ModuleFin, UnGrade_Create;

{ TUnGrade_PF }

procedure TUnGrade_PF.AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, gg1.UnidMed, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
  'IF(xco.CouNiv2 IS NULL, -1.000, xco.CouNiv2+0.000) CouNiv2, ',
  'IF(unm.Grandeza IS NULL, -1.000, unm.Grandeza+0.000) Grandeza, ',
  'ggx.GraGruY, xco.UsaSifDipoa, nv2.Codigo Niv2Cod ',
  'FROM gragrux ggx',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
  'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
  'WHERE ggx.Ativo=1 ',
  _AND,
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
end;

procedure TUnGrade_PF.AlteraSMIA(Qry: TmySQLQuery);
var
  Tipo, IDCtrl, GraGruX, StqCenCad: Integer;
  Qtde, Pecas, AreaM2, AreaP2, Peso: Double;
begin
  Tipo := Qry.FieldByName('Tipo').AsInteger;
  IDCtrl    := Qry.FieldByName('IDCtrl').AsInteger;
  GraGruX   := Qry.FieldByName('GraGruX').AsInteger;
  StqCenCad := Qry.FieldByName('StqCenCad').AsInteger;
  case Tipo of
    VAR_FATID_0101:
    begin
      Qtde      := Qry.FieldByName('Qtde').AsFloat;
      Pecas     := Qry.FieldByName('Pecas').AsFloat;
      AreaM2    := Qry.FieldByName('AreaM2').AsFloat;
      AreaP2    := Qry.FieldByName('AreaP2').AsFloat;
      Peso      := Qry.FieldByName('Peso').AsFloat;
    end;
    VAR_FATID_0104:
    begin
      Qtde      := - Qry.FieldByName('Qtde').AsFloat;
      Pecas     := - Qry.FieldByName('Pecas').AsFloat;
      AreaM2    := - Qry.FieldByName('AreaM2').AsFloat;
      AreaP2    := - Qry.FieldByName('AreaP2').AsFloat;
      Peso      := - Qry.FieldByName('Peso').AsFloat;
    end;
    else begin
      Geral.MB_Erro('Altera��o cancelada!' + slineBreak +
      'Tipo de lan�amento n�o implementado: ' + FormatFloat('0', Tipo) + slineBreak
      + 'AVISE A DERMATEK!');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmSMI_Edita, FmSMI_Edita, afmoNegarComAviso) then
  begin
    FmSMI_Edita.FNegativos := Tipo = VAR_FATID_0104;
    //
    FmSMI_Edita.QrSMIA.Close;
    FmSMI_Edita.QrSMIA.Params[0].AsInteger := IDCtrl;
    UnDmkDAC_PF.AbreQuery(FmSMI_Edita.QrSMIA, Dmod.MyDB);
    //
    FmSMI_Edita.EdGraGruX.ValueVariant   := GraGruX;
    FmSMI_Edita.CBGraGruX.KeyValue       := GraGruX;
    //
    FmSMI_Edita.EdStqCenCad.ValueVariant := StqCenCad;
    FmSMI_Edita.CBStqCenCad.KeyValue     := StqCenCad;

    FmSMI_Edita.EdQtde.ValueVariant      := Qtde;
    FmSMI_Edita.EdPecas.ValueVariant     := Pecas;
    FmSMI_Edita.EdAreaM2.ValueVariant    := AreaM2;
    FmSMI_Edita.EdAreaP2.ValueVariant    := AreaP2;
    FmSMI_Edita.EdPeso.ValueVariant      := Peso;
    //
    FmSMI_Edita.ShowModal;
    FmSMI_Edita.Destroy;
  end;
end;

procedure TUnGrade_PF.CadastraProdutoGrade(GraGruY, PrdGrupTip, GraGru1,
  GraGruC: Integer; JaTemN1: Boolean; NewNome, SQL_WHERE_PGT: String; ForcaCor,
  ForcaTam: Boolean);
const
  QrGraGru1 = nil;
var
  GraTabAppSel: Boolean;
  GraTabAppCod, UnidMed, GraTamCad: Integer;
  GraTabAppTxt: String;
  //
  Nivel1, GraGruX: Integer;
  Tabela: String;
  Qry: TmySQLQuery;
  Continua: Boolean;
  MultiplosGGX: array of Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Continua     := False;
    GraTabAppSel := False;
    GraTabAppCod := 0;
    GraTabAppTxt := '';
(*
  if DBCheck.CriaFm(TFmGraTabAppSel, FmGraTabAppSel, afmoNegarComAviso) then
  begin
    FmGraTabAppSel.ShowModal;
    //
    GraTabAppSel := FmGraTabAppSel.FGraTabAppSelSel;
    GraTabAppCod := FmGraTabAppSel.FGraTabAppSelCod;
    GraTabAppTxt := FmGraTabAppSel.FGraTabAppSelTxt;
    //
    FmGraTabAppSel.Destroy;
  end;
  if GraTabAppSel then
  begin
*)
    VAR_CADASTRO := 0;
    if DBCheck.CriaFm(TFmGraGru1CorTam, FmGraGru1CorTam, afmoNegarComAviso) then
    begin
      FmGraGru1CorTam.ImgTipo.SQLType          := stIns;
      FmGraGru1CorTam.FQrGraGru1               := QrGraGru1;
      FmGraGru1CorTam.EdGraTabApp.ValueVariant := GraTabAppCod;
      FmGraGru1CorTam.EDTXT_GraTabApp.Text     := GraTabAppTxt;
      FmGraGru1CorTam.EdNome_New.Text          := NewNome;
      FmGraGru1CorTam.FGraGruY                 := GraGruY;
      FmGraGru1CorTam.FForcaCor                := ForcaCor;
      FmGraGru1CorTam.FForcaTam                := ForcaTam;
      //
      if SQL_WHERE_PGT <> '' then
      begin
        FmGraGru1CorTam.FSQL_WHERE_PGT := SQL_WHERE_PGT;
        //
        FmGraGru1CorTam.ReabrePrdgrupTip();
      end;
      //
      if JaTemN1 then
      begin
        FmGraGru1CorTam.EdCodUsu_New.Enabled := False;
        FmGraGru1CorTam.LaNivel1.Enabled     := False;
      end else
      begin
        FmGraGru1CorTam.LaNivel1.Enabled     := True;
        FmGraGru1CorTam.EdCodUsu_New.Enabled := True;
        (*
        2017-03-03 => Desmarcado para evitar perder c�digos quando desiste;
        Usar o F4 da janela.

        FmGraGru1CorTam.EdCodUsu_New.ValueVariant :=
          UMyMod.BPGS1I32('GraGru1', 'CodUsu', '', '', tsPos, stIns, 0);
        *)
      end;
      //
      if PrdGrupTip <> 0 then
      begin
        // Ini 2019-06-13
        //FmGraGru1CorTam.EdPrdGrupTip.ValueVariant := PrdGrupTip;
        //FmGraGru1CorTam.CBPrdGrupTip.KeyValue     := PrdGrupTip;
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Gradeado, CodUsu ',
          'FROM prdgruptip ',
          'WHERE Codigo=' + Geral.FF0(PrdGrupTip),
          '']);
        FmGraGru1CorTam.EdPrdGrupTip.ValueVariant := USQLDB.v_i(Qry, 'CodUsu');
        FmGraGru1CorTam.CBPrdGrupTip.KeyValue     := USQLDB.v_i(Qry, 'CodUsu');
        //
        FmGraGru1CorTam.EdPrdGrupTip.Enabled      := False;
        FmGraGru1CorTam.CBPrdGrupTip.Enabled      := False;
        FmGraGru1CorTam.LaPrdGrupTip.Enabled      := False;
        //
(*
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Gradeado ',
          'FROM prdgruptip ',
          'WHERE Codigo=' + Geral.FF0(PrdGrupTip),
          '']);
*)
        // fim 2019-06-13
        if Qry.FieldByName('Gradeado').AsInteger = 1 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT UnidMed, GraTamCad ',
            'FROM gragru1 ',
            'WHERE Nivel1 = ' + Geral.FF0(GraGru1),
            '']);
          //
          UnidMed   := Qry.FieldByName('UnidMed').AsInteger;
          GraTamCad := Qry.FieldByName('GraTamCad').AsInteger;
          //
          FmGraGru1CorTam.EdGrade.ValueVariant   := GraTamCad;
          FmGraGru1CorTam.CBGrade.KeyValue       := GraTamCad;
          FmGraGru1CorTam.EdUnidMed.ValueVariant := UnidMed;
          FmGraGru1CorTam.CBUnidMed.KeyValue     := UnidMed;
          //
          FmGraGru1CorTam.LaGrade.Enabled := True;
          FmGraGru1CorTam.EdGrade.Enabled := True;
          FmGraGru1CorTam.CBGrade.Enabled := True;
          //
          FmGraGru1CorTam.GBCores.Enabled  := True;
          FmGraGru1CorTam.DBGCores.Enabled := True;
        end;
      end;
      //
      FmGraGru1CorTam.FNivel1_JaTem   := JaTemN1;
      FmGraGru1CorTam.FNivel1_GraGru1 := GraGru1;
      FmGraGru1CorTam.FNivel1_GraGruC := GraGruC;
      FmGraGru1CorTam.ShowModal;
      //
      FmGraGru1CorTam.Destroy;
    end;
    if VAR_CADASTRO <> 0 then
    begin
      Nivel1 := VAR_CADASTRO;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      'WHERE GraGru1=' + Geral.FF0(Nivel1),
      '']);
      Qry.First;
      GraGruX := Qry.FieldByName('Controle').AsInteger;
      VAR_CADASTRO2 := GraGruY;
      Tabela := AppPF.ObtemNomeTabelaGraGruY(GraGruY);
      if Tabela <> '' then
      begin
        //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
        Qry.First;
        while not Qry.Eof do
        begin
          UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, Tabela, False, [
          ], [
          'GraGruX'], ['Ativo'], [
          ], [
          Qry.FieldByName('Controle').AsInteger], [1], True);
          //
          Qry.Next;
        end;
        //
        Continua := AppPF.CorrigeGraGruYdeGraGruX(GraGruY, GraGruX);
        //
        if Continua (*and (not JaTemN1)*) then
        begin
          AppPF.MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX, True, Qry(*, MultiplosGGX*));
        end;
      end;
    end else
      Geral.MB_Erro('N�o foi poss�vel localizar o produto ' + Geral.FF0(
      VAR_CADASTRO) + ' para complementar os dados do seu cadastro!');
  finally
    Qry.Free;
  end;
end;

procedure TUnGrade_PF.CadastroEDefinicaoDeUnidMed(VUUnidMed: TdmkValUsu;
EdUnidMed: TdmkEditCB; CBUnidMed: TDmkDBLookupComboBox; QrUnidMed: TmySQLQuery);
var
  Codigo: Integer;
begin
  Codigo       := VUUnidMed.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmUnidMed.LocCod(Codigo, Codigo);
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    { N�o presisa no SetaCodUsoDeCodigo
    QrUnidMed.Close;
    UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
    }
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
        'Codigo', 'CodUsu');
      EdUnidMed.SetFocus;
    end;
  end;
end;

procedure TUnGrade_PF.CorrigeGraGruYdeGraGruX();
var
  Qry: TmySQLQuery;
  GraGruY, Controle: Integer;
begin
  // Parei aqui SPEDICMSIPI
  // Ver o que fazer!!!!
{}
  Qry := TmySQLQuery.Create(Dmod);
  try
    // Provis�rio! Reativar se necess�rio!
{
    if CO_SIGLA_APP = 'BDER' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, ggy.Codigo  ',
      'FROM gragrux ggx ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN gragruy ggy ON ggy.PrdGrupTip=gg1.PrdGrupTip ',
      'WHERE ggx.GraGruY <> ggy.Codigo ',
      'AND ggx.GraGruY = 0 ',
      '']);
    end else
    begin
}
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, gyp.GraGruY Codigo ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
      'LEFT JOIN gragruypgt gyp ON gyp.PrdGrupTip=pgt.Codigo ',
      'WHERE ggx.GraGruY=0 ',
      'AND gyp.GraGruY<>0 ',
      '']);
    //end;
    Qry.First;
    while not Qry.Eof do
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
      GraGruY  := Qry.FieldByName('Codigo').AsInteger;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
      'GraGruY'], ['Controle'], [GraGruY], [Controle], True);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
{}
end;

function TUnGrade_PF.DefineContasContabeisNFe_PelosItens(
  const TypCtbCadMoF: TTypCtbCadMoF; const Tipo, OriCodi, Empresa: Integer;
  var GenCtbD, GenCtbC: Integer; var Descricao: String): Boolean;
const
  sProcName = 'TUnGrade_PF.DefineContasContabeisNFe_PelosItens()';
var
  QtdItens: Integer;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrAux, Dmod.MyDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM stqmovvala ',
  'WHERE Tipo=' + Geral.FF0(Tipo),
  'AND OriCodi=' + Geral.FF0(OriCodi),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  QtdItens := DModFin.QrAux.FieldByName('ITENS').AsInteger;
  case QtdItens of
    0: Geral.MB_Aviso('N�o h� itens para defini��o de contas cont�beis em ' +
       sProcName);
    1: Result := DefineContasContabeisNFe_PelosItens_UmItem(TypCtbCadMoF, Tipo,
      OriCodi, Empresa, GenCtbD, GenCtbC, Descricao);
    else
      Result := DefineContasContabeisNFe_PelosItens_VariosItens(TypCtbCadMoF,
      Tipo, OriCodi, Empresa, GenCtbD, GenCtbC, Descricao);
  end;
end;

function TUnGrade_PF.DefineContasContabeisNFe_PelosItens_UmItem(
  const TypCtbCadMoF: TTypCtbCadMoF; const Tipo, OriCodi, Empresa: Integer;
  var GenCtbD, GenCtbC: Integer; var Descricao: String): Boolean;
var
  CtbPlaCta, CtbCadGru: Integer;
begin
  Result := False;
  GenCtbD := 0;
  GenCtbC := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGraCtbs1, Dmod.MyDB, [
  'SELECT  ',
  'gg1.CtbPlaCta gg1_CtbPlaCta,  ',
  'gg1.CtbCadGru gg1_CtbCadGru, ',
  ' ',
  'gg2.CtbPlaCta gg2_CtbPlaCta,  ',
  'gg2.CtbCadGru gg2_CtbCadGru, ',
  ' ',
  'pgt.CtbPlaCta pgt_CtbPlaCta,  ',
  'pgt.CtbCadGru pgt_CtbCadGru ,  ',
  ' ',
  'IF(gg2.Nome <> "", gg2.Nome, pgt.Nome) NO_GG2, pgt.Nome NO_PGT ',
  ' ',
  'FROM stqmovvala smia',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
  'LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'WHERE smia.Tipo=' + Geral.FF0(Tipo),
  'AND smia.OriCodi=' + Geral.FF0(OriCodi),
  'AND smia.Empresa=' + Geral.FF0(Empresa),
  '']);
  //
  CtbCadGru := DModFin.QrGraCtbs1gg1_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
  {$IfDef UsaContabil}
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
      Result := True;
      Exit;
    end;
  //{$Else}
    //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
  end;
  CtbPlaCta := DModFin.QrGraCtbs1gg1_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    begin
      Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
      Result := True;
      Exit;
    end;
  end;
  //
  CtbCadGru := DModFin.QrGraCtbs1gg2_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
  {$IfDef UsaContabil}
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
      Result := True;
      Exit;
    end;
  //{$Else}
    //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
  end;
  CtbPlaCta := DModFin.QrGraCtbs1gg2_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    begin
      Descricao := DModFin.QrGraCtbs1NO_GG2.Value;
      Result := True;
      Exit;
    end;
  end;
  //
  CtbCadGru := DModFin.QrGraCtbs1pgt_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
   {$IfDef UsaContabil}
   if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbs1NO_PGT.Value;
      Result := True;
      Exit;
    end;
  //{$Else}
    //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
  end;
  CtbPlaCta := DModFin.QrGraCtbs1pgt_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    begin
      Descricao := DModFin.QrGraCtbs1NO_PGT.Value;
      Result := True;
      Exit;
    end;
  end;
end;

function TUnGrade_PF.DefineContasContabeisNFe_PelosItens_VariosItens(
  const TypCtbCadMoF: TTypCtbCadMoF; const Tipo, OriCodi, Empresa: Integer;
  var GenCtbD, GenCtbC: Integer; var Descricao: String): Boolean;
var
  CtbPlaCta, CtbCadGru: Integer;
begin
  Result := False;
  GenCtbD := 0;
  GenCtbC := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGraCtbsN, Dmod.MyDB, [
  'SELECT  SUM(smia.Total) SumValor,',
  ' ',
  'gg2.CtbPlaCta gg2_CtbPlaCta,  ',
  'gg2.CtbCadGru gg2_CtbCadGru, ',
  ' ',
  'pgt.CtbPlaCta pgt_CtbPlaCta,  ',
  'pgt.CtbCadGru pgt_CtbCadGru, ',
  ' ',
  'IF(gg2.Nome <> "", gg2.Nome, pgt.Nome) NO_GG2, pgt.Nome NO_PGT ',
  ' ',
  'FROM stqmovvala smia',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
  'LEFT JOIN prdGrupTip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'WHERE smia.Tipo=' + Geral.FF0(Tipo),
  'AND smia.OriCodi=' + Geral.FF0(OriCodi),
  'AND smia.Empresa=' + Geral.FF0(Empresa),
  'ORDER BY SumValor DESC',
  '']);
  //
  CtbCadGru := DModFin.QrGraCtbsNgg2_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
  {$IfDef UsaContabil}
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbsNNO_GG2.Value;
      Result := True;
      Exit;
    end;
  //{$Else}
    //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
  end;
  CtbPlaCta := DModFin.QrGraCtbsNgg2_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    begin
      Descricao := DModFin.QrGraCtbsNNO_GG2.Value;
      Result := True;
      Exit;
    end;
  end;
  //
  CtbCadGru := DModFin.QrGraCtbsNpgt_CtbCadGru.Value;
  if CtbCadGru <> 0 then
  begin
  {$IfDef UsaContabil}
    if Contabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF, CtbCadGru, GenCtbD,
    GenCtbC) then
    begin
      Descricao := DModFin.QrGraCtbsNNO_PGT.Value;
      Result := True;
      Exit;
    end;
  //{$Else}
    //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
  end;
  CtbPlaCta := DModFin.QrGraCtbsNpgt_CtbPlaCta.Value;
  if CtbPlaCta <> 0 then
  begin
    GenCtbD := CtbPlaCta;
    begin
      Descricao := DModFin.QrGraCtbsNNO_PGT.Value;
      Result := True;
      Exit;
    end;
  end;
end;

procedure TUnGrade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor: TdmkEdit;
  GraGruX: Integer);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    'gg1.Nome NO_GG1, gti.Nome NO_Tam, gcc.Nome NO_Cor',
    'FROM gragrux ggx',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
    'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
    '']);
    if EdNome <> nil then
      EdNome.Text := Qry.FieldByName('NO_GG1').AsString;
    if EdTam <> nil then
      EdTam.Text := Qry.FieldByName('NO_Tam').AsString;
    if EdCor <> nil then
      EdCor.Text := Qry.FieldByName('NO_Cor').AsString;
  finally
    Qry.Free;
  end;
end;

procedure TUnGrade_PF.GeraEstoqueEm_2(Empresa_Txt: String; DataIni,
  DataFim: TDateTime; PGT, GG1, GG2, GG3, StqCenCad, TipoPesq: Integer; GraCusPrc,
  TabePrcCab: Variant; GraCusPrcCodigo, TabePrcCabCodigo, PrdGrupTipCodigo,
  GraGru1Nivel1, GraGru2Nivel2, GraGru3Nivel3: Integer; QrPGT_SCC, QrAbertura2a,
  QrAbertura2b: TmySQLQuery; UsaTabePrcCab, SoPositivos: Boolean;
  LaAviso: TLabel; NomeTb_SMIC: String; QrSMIC_X: TmySQLQuery; var TxtSQL_Part1,
  TxtSQL_Part2: String);
var
  PrdGrupTip_Txt, StqCenCad_Txt, DiaSeguinte, GrupoBal_Txt,
  GraCusPrc_Txt, TabePrcCab_Txt, TextoA, TextoB, TextoC, CliInt_Txt,
  EntiSitio_Txt: String;
begin
  Screen.Cursor := crHourGlass;
  try
    //
    //DiaSeguinte := FormatDateTime('yyyy-mm-dd', TPDataFim2.Date + 1);
    DiaSeguinte := FormatDateTime('yyyy-mm-dd', DataFim + 1);
    if TipoPesq = 1 then
    begin
      if GraCusPrc <> NULL then
        GraCusPrc_Txt := FormatFloat('0', GraCusPrcCodigo)
      else // N�o pode ser nulo!
        GraCusPrc_Txt := '0';
      //
    end;
    if TabePrcCab <> NULL then
      TabePrcCab_Txt := FormatFloat('0', TabePrcCabCodigo)
    else // N�o pode ser nulo?
      TabePrcCab_Txt := '0';
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Excluindo caso exista');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS stqmovitsc;');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Adicionando dados do arquivo morto');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('CREATE TABLE stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT smi.*');
    DmodG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.stqmovitsb smi');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smi.GraGruX');
    DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    DmodG.QrUpdPID1.SQL.Add('WHERE '(*NOT *)+'gg1.PrdGrupTip IN');
    DmodG.QrUpdPID1.SQL.Add('(');
    DmodG.QrUpdPID1.SQL.Add('  SELECT DISTINCT prdgruptip');
    DmodG.QrUpdPID1.SQL.Add('  FROM '+TMeuDB+'.stqbalcad');
    DmodG.QrUpdPID1.SQL.Add('  WHERE Abertura >= "' + DiaSeguinte + '"');
    DmodG.QrUpdPID1.SQL.Add(')');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tabela temporaria - Adicionando dados do arquivo ativo');
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO stqmovitsc');
    DmodG.QrUpdPID1.SQL.Add('SELECT * FROM '+TMeuDB+'.stqmovitsa');
    DmodG.QrUpdPID1.ExecSQL;
    //
    MyObjects.Informa(LaAviso, True, 'Tipos de Grupos de Produtos - abrindo tabela');
    QrPGT_SCC.Close;
    QrPGT_SCC.SQL.Clear;
    case TipoPesq of
      1:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad, ');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, ' + Empresa_Txt + ' Empresa, ' + '0' + ' EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('WHERE GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        QrPGT_SCC.SQL.Add('AND smic.Empresa=' + Empresa_Txt);
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1));
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2));
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3));
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
        if StqCenCad <> 0 then
          QrPGT_SCC.SQL.Add('AND smic.StqCenCad=' + FormatFloat('0', StqCenCad));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');//, smic.GraGruX');
      end;
      2:
      begin
        QrPGT_SCC.SQL.Add('SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad,');
        QrPGT_SCC.SQL.Add('pgt.LstPrcFisc, FLOOR(smic.Empresa + 0.1) Empresa, FLOOR(scc.EntiSitio + 0.1) EntiSitio');
        QrPGT_SCC.SQL.Add('FROM stqmovitsc smic');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
        QrPGT_SCC.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
        QrPGT_SCC.SQL.Add('WHERE (smic.Empresa=' + Empresa_Txt + ' OR scc.EntiSitio=' + Empresa_Txt + ')');
        QrPGT_SCC.SQL.Add('AND GraGruX <> 0');
        QrPGT_SCC.SQL.Add('AND Baixa = 1');
        if GG1 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1));
        if GG2 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2));
        if GG3 <> 0 then
          QrPGT_SCC.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3));
        if PGT <> 0 then
          QrPGT_SCC.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
        if StqCenCad <> 0 then
          QrPGT_SCC.SQL.Add('AND smic.StqCenCad=' + FormatFloat('0', StqCenCad));
        QrPGT_SCC.SQL.Add('ORDER BY gg1.PrdGrupTip, smic.StqCenCad');
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrPGT_SCC, Dmod.MyDB);
    //
    TextoA     := '';
    TextoB     := '';
    TextoC     := '';
    // Caso n�o ache nada!
    if QrPGT_SCC.RecordCount = 0 then
    begin
      QrSMIC_X.Close;
      QrSMIC_X.SQL.Clear;
      QrSMIC_X.SQL.Add('DELETE FROM ' + NomeTb_SMIC + ';');
      QrSMIC_X.ExecSQL;
    end;
    //
    while not QrPGT_SCC.Eof do
    begin
      CliInt_Txt    := FormatFloat('0', QrPGT_SCC.FieldByName('Empresa').AsInteger);
      EntiSitio_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('EntiSitio').AsInteger);
      if TipoPesq = 2 then
      begin
        GraCusPrc_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('LstPrcFisc').AsInteger)
      end;
      PrdGrupTip_Txt := FormatFloat('0', QrPGT_SCC.FieldByName('PrdGrupTip').AsInteger);
      StqCenCad_Txt  := FormatFloat('0', QrPGT_SCC.FieldByName('StqCenCad').AsInteger);
      TextoA := 'Tipo Prod.: ' + PrdGrupTip_Txt + '  -  ' +
                'Centro estq: ' + StqCenCad_Txt + '  -  ' +
                'Sitio: ' + EntiSitio_Txt + '  -  ' +
                'Empresa: ' + CliInt_Txt;
      TextoB := '';
      TextoC := '';
      MyObjects.Informa(LaAviso, False, TextoA + TextoB + TextoC);
      //
      QrAbertura2b.Close;
      QrAbertura2b.SQL.Clear;
      QrAbertura2b.SQL.Add('SELECT Abertura');
      QrAbertura2b.SQL.Add('FROM stqbalcad');
      QrAbertura2b.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
      QrAbertura2b.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
      QrAbertura2b.SQL.Add('AND Abertura >= "' + DiaSeguinte + '"');
      QrAbertura2b.SQL.Add('AND Empresa=' + Empresa_Txt);
      QrAbertura2b.SQL.Add('ORDER BY Abertura DESC');
      QrAbertura2b.SQL.Add('LIMIT 1');
      UnDmkDAC_PF.AbreQuery(QrAbertura2b, Dmod.MyDB);
      if QrAbertura2b.RecordCount > 0 then
      begin
        QrAbertura2a.Close;
        QrAbertura2a.SQL.Clear;
        QrAbertura2a.SQL.Add('SELECT Abertura, GrupoBal');
        QrAbertura2a.SQL.Add('FROM stqbalcad');
        QrAbertura2a.SQL.Add('WHERE PrdGrupTip=' + PrdGrupTip_Txt);
        QrAbertura2a.SQL.Add('AND StqCenCad=' + StqCenCad_Txt);
        QrAbertura2a.SQL.Add('AND Abertura < "' + DiaSeguinte + '"');
        QrAbertura2a.SQL.Add('AND Empresa=' + Empresa_Txt);
        QrAbertura2a.SQL.Add('ORDER BY Abertura DESC');
        QrAbertura2a.SQL.Add('LIMIT 1');
        UnDmkDAC_PF.AbreQuery(QrAbertura2a, Dmod.MyDB);
        //
        //if QrAbertura2aAbertura.Value > Int(TPDataFim2.Date) + 1 then
        if QrAbertura2a.FieldByName('Abertura').AsDateTime > Int(DataFim) + 1 then
          GrupoBal_Txt := FormatFloat('0', QrAbertura2a.FieldByName('GrupoBal').AsInteger)
        else
          GrupoBal_Txt := '0';
      end else GrupoBal_Txt := '0';
      //
      QrSMIC_X.Close;
      QrSMIC_X.SQL.Clear;
      if QrPGT_SCC.RecNo = 1 then
      begin
        QrSMIC_X.SQL.Add('DROP TABLE IF EXISTS ' + NomeTb_SMIC + ';');
        QrSMIC_X.SQL.Add('CREATE TABLE ' + NomeTb_SMIC);
      end else begin
        QrSMIC_X.SQL.Add('INSERT INTO ' + NomeTb_SMIC);
      end;
      QrSMIC_X.SQL.Add('SELECT smic.IDCtrl My_Idx, gg1.Nivel1, ');
      QrSMIC_X.SQL.Add('gg1.Nome NO_PRD, gg1.PrdGrupTip, gg1.UnidMed, ');
      QrSMIC_X.SQL.Add('gg1.NCM, pgt.Nome NO_PGT, ');
      QrSMIC_X.SQL.Add('med.Sigla SIGLA, gti.PrintTam, gti.Nome NO_TAM, ');
      QrSMIC_X.SQL.Add('gcc.PrintCor, gcc.Nome NO_COR, ggc.GraCorCad, ');
      QrSMIC_X.SQL.Add('ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, ');
      QrSMIC_X.SQL.Add('scc.SitProd, scc.EntiSitio,');
      QrSMIC_X.SQL.Add('smic.GraGruX, smic.Empresa, smic.StqCenCad,');
      QrSMIC_X.SQL.Add('SUM(smic.Qtde * smic.Baixa) QTDE, SUM(smic.Pecas) PECAS,');
      QrSMIC_X.SQL.Add('SUM(smic.Peso) PESO, SUM(smic.AreaM2) AREAM2,');
      QrSMIC_X.SQL.Add('SUM(smic.AreaP2) AREAP2,');
      if UsaTabePrcCab then
      begin
        // Por tabela de pre�o de GraGru1 (TPC_GCP_ID = 2) ou de GraGruX (TPC_GCP_ID = 3)
        QrSMIC_X.SQL.Add('TRUNCATE(IF(tpi.Preco IS NULL, 2, 3), 0) TPC_GCP_ID,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, tpc.Codigo, tpi.TabePrcCab) TPC_GCP_TAB,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(tpc.Preco, 6), TRUNCATE(tpi.Preco, 6)) PrcCusUni,');
        QrSMIC_X.SQL.Add('IF(tpi.Preco IS NULL, TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpc.Preco, 6), 2),');
        QrSMIC_X.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * TRUNCATE(tpi.Preco, 6), 2)) ValorTot,');
        QrSMIC_X.SQL.Add('');
      end else
      begin
        // Por lista de Pre�o (TPC_GCP_ID = 1)
        QrSMIC_X.SQL.Add('TRUNCATE(1,0) TPC_GCP_ID, ggv.GraCusPrc TPC_GCP_TAB, ');
        QrSMIC_X.SQL.Add('TRUNCATE(ggv.CustoPreco, 6) PrcCusUni, ');
        QrSMIC_X.SQL.Add('TRUNCATE(SUM(smic.Qtde*smic.Baixa) * ');
        QrSMIC_X.SQL.Add('TRUNCATE(ggv.CustoPreco, 6), 2) ValorTot, ');
      end;
      QrSMIC_X.SQL.Add('1 Exportado ');
      //QrSMIC_X.SQL.Add('1 Exportado, 0 Mot_SitPrd, 0 Mot_Sitio, 0 MotQtdVal, 0 MotProduto');
      QrSMIC_X.SQL.Add('FROM stqmovitsc smic');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragrux    ggx ON ggx.Controle=smic.GraGruX');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruc    ggc ON ggc.Controle=ggx.GraGruC');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gratamits  gti ON gti.Controle=ggx.GraTamI');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.unidmed    med ON med.Codigo=gg1.UnidMed');
      if UsaTabePrcCab then
      begin
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgrg  tpc ON tpc.Nivel1=ggx.GraGru1');
        QrSMIC_X.SQL.Add('          AND tpc.Codigo=' + TabePrcCab_Txt);
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.tabeprcgri  tpi ON tpi.GraGruX=smic.GraGruX');
        QrSMIC_X.SQL.Add('          AND tpi.TabePrcCab=' + TabePrcCab_Txt);
      end else begin
        QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.gragruval  ggv ON ggv.GraGruX=smic.GraGruX');
        // Evitar n�o encontrar registros sem pre�o!
        QrSMIC_X.SQL.Add('          AND ggv.GraCusPrc=' + GraCusPrc_Txt);
      end;
      QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad');
      //
      QrSMIC_X.SQL.Add('WHERE smic.Baixa> 0');

      // Ativar? 2020-11-22
      //QrSMIC_X.SQL.Add('AND smic.Ativo = 1');
      //QrSMIC_X.SQL.Add('AND smic.ValiStq <> 101');

      //QrSMIC_X.SQL.Add('AND smic.GrupoBal=' + GrupoBal_Txt);
      QrSMIC_X.SQL.Add('AND smic.DataHora < "' + DiaSeguinte  + '"');
      QrSMIC_X.SQL.Add('AND smic.StqCenCad=' + StqCenCad_Txt);
      QrSMIC_X.SQL.Add('AND gg1.PrdGrupTip=' + PrdGrupTip_Txt);
      //
      QrSMIC_X.SQL.Add('AND smic.EMPRESA=' + CliInt_Txt);
      if TipoPesq = 2 then
        QrSMIC_X.SQL.Add('AND scc.EntiSitio=' + EntiSitio_Txt);
      //
      //  Filtros de pesquisa
      if GG1 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1));
      if GG2 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2));
      if GG3 <> 0 then
        QrSMIC_X.SQL.Add('AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3));
      if PGT <> 0 then
        QrSMIC_X.SQL.Add('AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo));
      if StqCenCad <> 0 then
        QrSMIC_X.SQL.Add('AND smic.StqCenCad=' + FormatFloat('0', StqCenCad));
      //
      QrSMIC_X.SQL.Add('GROUP BY smic.GraGruX');
      //dmkPF.LeMeuTexto(QrSMIC_X.SQL.Text);
      QrSMIC_X.ExecSQL;
      //
      QrPGT_SCC.Next;
    end;
    //
    MyObjects.Informa(LaAviso, False, 'Abrindo dados gerados');
    QrSMIC_X.Close;
    QrSMIC_X.SQL.Clear;
    QrSMIC_X.SQL.Add('SELECT sm2.*, scc.SitProd,');
    QrSMIC_X.SQL.Add('ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,');
    QrSMIC_X.SQL.Add('emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF');
    QrSMIC_X.SQL.Add('FROM ' + NomeTb_SMIC + ' sm2');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=sm2.StqCenCad');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades ens ON ens.Codigo=scc.EntiSitio');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades emp ON emp.Codigo=sm2.Empresa');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf1 ON uf1.Codigo=ens.EUF');
    QrSMIC_X.SQL.Add('LEFT JOIN '+TMeuDB+'.ufs uf2 ON uf2.Codigo=emp.EUF');
    if SoPositivos then
      QrSMIC_X.SQL.Add('WHERE sm2.QTDE >= 0.001');
    QrSMIC_X.SQL.Add('ORDER BY NO_PGT, NO_PRD');
    UnDmkDAC_PF.AbreQuery(QrSMIC_X, Dmod.MyDB);
    //
    TXTSQL_Part1 := 'SELECT SUM(smic.Qtde) QtdSum,' + slineBreak +
    'SUM(IF(smic.Qtde>0, smic.Qtde * smic.Baixa, 0)) QtdPos,' + slineBreak +
    'SUM(IF(smic.Qtde<0, smic.Qtde * smic.Baixa, 0)) QtdNeg' + slineBreak +
    'FROM stqmovitsc smic' + slineBreak +
    'LEFT JOIN '+TMeuDB+'.gragrux ggx ON ggx.Controle=smic.GraGruX' + slineBreak +
    'LEFT JOIN '+TMeuDB+'.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1' + slineBreak +
    'LEFT JOIN '+TMeuDB+'.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip' + slineBreak +
    'LEFT JOIN '+TMeuDB+'.stqcencad scc ON scc.Codigo=smic.StqCenCad';
    //TXTSQL_Part := 'WHERE Tipo > 0' + slineBreak +
    TXTSQL_Part2 := dmkPF.SQL_Periodo('AND smic.DataHora ', DataIni, DataFim, True, True) + slineBreak +
    //
    'AND smic.EMPRESA=' + CliInt_Txt + slineBreak;

{
    if TipoPesq = 2 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND scc.EntiSitio=' + EntiSitio_Txt + slineBreak;
}
    //
    //  Filtros de pesquisa
    if GG1 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel1=' + FormatFloat('0', GraGru1Nivel1) + slineBreak;
    if GG2 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel2=' + FormatFloat('0', GraGru2Nivel2) + slineBreak;
    if GG3 <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND gg1.Nivel3=' + FormatFloat('0', GraGru3Nivel3) + slineBreak;
    if PGT <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND pgt.Codigo=' + FormatFloat('0', PrdGrupTipCodigo) + slineBreak;
    if StqCenCad <> 0 then
      TXTSQL_Part2 := TXTSQL_Part2 + 'AND smic.StqCenCad=' + FormatFloat('0', StqCenCad) + sLineBreak;
    //
    TXTSQL_Part2 := TXTSQL_Part2 + 'AND GraGruX=:P0';
    //
    MyObjects.Informa(LaAviso, False, '...');
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnGrade_PF.GraGru1_AlteraNome(Nivel1: Integer; Nome: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
  'Nome'], ['Nivel1'], [Nome], [Nivel1], True);
end;

procedure TUnGrade_PF.InsereFatPedFis(Tmp_FatPedFis: String; GraGru1, IDCtrl,
  Tipo, OriCodi, OriCtrl, OriCnta, OriPart, FisRegCad: Integer);
var
  QryTmpTable: TmySQLQuery;
  modBC, UsaInterPartLei: Integer;
  pRedBC, ICMSAliq, pICMSUFDest, pICMSInter, pFCPUFDest, pICMSInterPart,
  pBCUFDest: Double;
  CFOP, CST_B, UFEmit, UFDest: String;
begin
  QryTmpTable := TmySQLQuery.Create(TDataModule(DModG.MyPID_DB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryTmpTable, DModG.MyPID_DB, [
      'SELECT * ',
      'FROM ' + Tmp_FatPedFis,
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCnta=' + Geral.FF0(OriCnta),
      'AND GraGru1=' + Geral.FF0(GraGru1),
      '']);
    if QryTmpTable.RecordCount > 0 then
    begin
      CFOP            := QryTmpTable.FieldByName('CFOP').AsString;
      CST_B           := Trim(QryTmpTable.FieldByName('CST_B').AsString);
      modBC           := QryTmpTable.FieldByName('modBC').AsInteger;
      pRedBC          := QryTmpTable.FieldByName('pRedBC').AsFloat;
      UFEmit          := QryTmpTable.FieldByName('UFEmit').AsString;
      UFDest          := QryTmpTable.FieldByName('UFDest').AsString;
      ICMSAliq        := QryTmpTable.FieldByName('ICMSAliq').AsFloat;
      pICMSUFDest     := QryTmpTable.FieldByName('pICMSUFDest').AsFloat;
      pICMSInter      := QryTmpTable.FieldByName('pICMSInter').AsFloat;
      pFCPUFDest      := QryTmpTable.FieldByName('pICMSUFDest').AsFloat;
      pBCUFDest       := QryTmpTable.FieldByName('pBCUFDest').AsFloat;
      pICMSInterPart  := QryTmpTable.FieldByName('pICMSInterPart').AsFloat;
      UsaInterPartLei := QryTmpTable.FieldByName('UsaInterPartLei').AsInteger;
      //
      if CST_B = '0' then
        CST_B := '00';
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'fatpedfis', False,
        ['Tipo', 'OriCodi', 'OriCtrl', 'OriCnta', 'OriPart', 'FisRegCad', 'CFOP',
        'CST_B', 'modBC', 'pRedBC', 'UFEmit', 'UFDest', 'ICMSAliq', 'pICMSUFDest',
        'pICMSInter', 'pFCPUFDest', 'pBCUFDest', 'pICMSInterPart',
        'UsaInterPartLei'], ['IDCtrl'],
        [Tipo, OriCodi, OriCtrl, OriCnta, OriPart, FisRegCad, CFOP,
        CST_B, modBC, pRedBC, UFEmit, UFDest, ICMSAliq, pICMSUFDest,
        pICMSInter, pFCPUFDest, pBCUFDest, pICMSInterPart,
        UsaInterPartLei], [IDCtrl], True) then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(QryTmpTable, DModG.MyPID_DB, [
          'DELETE FROM ' + Tmp_FatPedFis,
          'WHERE OriCodi=' + Geral.FF0(OriCodi),
          'AND OriCnta=' + Geral.FF0(OriCnta),
          'AND GraGru1=' + Geral.FF0(GraGru1),
          '']);
      end else
      begin
        Geral.MB_Erro('Falha ao atualizar dados fiscais!' + sLineBreak +
          'Exclua o produto atual e insira novamente!');
      end;
    end;
  finally
    QryTmpTable.Free;
  end;
end;

function TUnGrade_PF.InsUpdProdNivel1(SQLTipo: TSQLType; GraTabApp, PrdGrupTip,
  Gradeado, UsaCores, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu: Integer;
  Nome: String; UnidMed: Integer; NCM: String; var Msg: String;
  ForcaCadastrGGX_SemCorTam: Boolean): Integer;
//
var
  Niv1, Controle, GraGruX, UnicaGra, UnicaCor, UnicoTam: Integer;
  //
  function CadastraGrGruX(): Boolean;
  begin
    Result := False;
    Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False,
      ['GraCorCad', 'Nivel1'], ['Controle'],
      [UnicaCor, Niv1], [Controle], True) then
    begin
      if VAR_APP_EMPARELHA_CODIGO_DO_GG1_COM_O_CONTROLE_DO_GGX then
        GraGruX := Niv1
      else
        GraGruX := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                   'GraGruX', 'GraGruX', 'Controle');
      //
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False,
        ['Ativo', 'GraGruC', 'GraGru1', 'GraTamI'], ['Controle'],
        [1, Controle, Niv1, UnicoTam], [GraGruX], True)
      then
        Exit;
      VAR_CADASTRO2 := GraGruX;
      Result := True;
    end else
      Exit;
  end;
begin
  Result := 0;
  //
  if PrdGrupTip = 0 then
  begin
    Msg := 'Defina o tipo de grupo de produto para n�vel 1!';
    Exit;
  end else
  if Nome = '' then
  begin
    Msg := 'Defina uma descri��o para o n�vel 1!';
    Exit;
  end else
  if CodUsu = 0 then
  begin
    Msg := 'Defina um ID para o n�vel 1!';
    Exit;
  end;
  //
  DmProd.ReopenOpcoesGrad;
  //
  UnicaGra := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
  UnicaCor := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
  UnicoTam := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
  //
  // 2020-10-05
  if VAR_APP_EMPARELHA_CODIGO_DO_GG1_COM_O_CONTROLE_DO_GGX then
    Niv1 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                   'GraGruX', 'GraGruX', 'Controle')
  else
  // 2020-10-05
    Niv1 := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', SQLTipo, Nivel1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'gragru1', False, [
    'Nivel5', 'Nivel4', 'Nivel3',
    'Nivel2', 'CodUsu', 'Nome',
    'PrdGrupTip', 'GraTabApp', 'GraTamCad',
    'UnidMed', 'NCM'], ['Nivel1'], [
    Nivel5, Nivel4, Nivel3,
    Nivel2, CodUsu, Nome,
    PrdGrupTip, GraTabApp, UnicaGra,
    UnidMed, NCM], [Niv1], True) then
  begin
    VAR_CADASTRO := Niv1;
    //
    // tesyat no blue derm
    if (SQLTipo = stIns) and (Gradeado = 0) and (UsaCores = 0) then
      CadastraGrGruX()
    else
    if ForcaCadastrGGX_SemCorTam then
      CadastraGrGruX();
    Result := Niv1;
  end;
end;

function TUnGrade_PF.InsUpdProdNivel2(SQLTipo: TSQLType; PrdGrupTip, Nivel5,
  Nivel4, Nivel3, Nivel2, CodUsu: Integer; Nome: String; var Msg: String): Integer;
var
  Niv2: Integer;
begin
  Result := 0;
  //
  if PrdGrupTip = 0 then
  begin
    Msg := 'Defina o tipo de grupo de produto para n�vel 2!';
    Exit;
  end else
  if Nome = '' then
  begin
    Msg := 'Defina uma descri��o para o n�vel 2!';
    Exit;
  end else
  if CodUsu = 0 then
  begin
    Msg := 'Defina um ID para o n�vel 2!';
    Exit;
  end;
  //
  Niv2 := UMyMod.BuscaEmLivreY_Def('gragru2', 'Nivel2', SQLTipo, Nivel2);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'gragru2', False,
    ['Nivel5', 'Nivel4', 'Nivel3', 'CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel2'],
    [Nivel5, Nivel4, Nivel3, CodUsu, Nome, PrdGrupTip], [Niv2], True)
  then
    Result := Niv2;
end;

function TUnGrade_PF.InsUpdProdNivel3(SQLTipo: TSQLType; PrdGrupTip, Nivel5,
  Nivel4, Nivel3, CodUsu: Integer; Nome: String; var Msg: String): Integer;
var
  Niv3: Integer;
begin
  Result := 0;
  //
  if PrdGrupTip = 0 then
  begin
    Msg := 'Defina o tipo de grupo de produto para n�vel 3!';
    Exit;
  end else
  if Nome = '' then
  begin
    Msg := 'Defina uma descri��o para o n�vel 3!';
    Exit;
  end else
  if CodUsu = 0 then
  begin
    Msg := 'Defina um ID para o n�vel 3!';
    Exit;
  end;
  //
  Niv3 := UMyMod.BuscaEmLivreY_Def('gragru3', 'Nivel3', SQLTipo, Nivel3);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'gragru3', False,
    ['Nivel5', 'Nivel4', 'CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel3'],
    [Nivel5, Nivel4, CodUsu, Nome, PrdGrupTip], [Niv3], True)
  then
    Result := Niv3;
end;

function TUnGrade_PF.InsUpdProdNivel4(SQLTipo: TSQLType; PrdGrupTip, Nivel5,
  Nivel4, CodUsu: Integer; Nome: String; var Msg: String): Integer;
var
  Niv4: Integer;
begin
  Result := 0;
  //
  if PrdGrupTip = 0 then
  begin
    Msg := 'Defina o tipo de grupo de produto para n�vel 4!';
    Exit;
  end else
  if Nome = '' then
  begin
    Msg := 'Defina uma descri��o para o n�vel 4!';
    Exit;
  end else
  if CodUsu = 0 then
  begin
    Msg := 'Defina um ID para o n�vel 4!';
    Exit;
  end;
  //
  Niv4 := UMyMod.BuscaEmLivreY_Def('gragru4', 'Nivel4', SQLTipo, Nivel4);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'gragru4', False,
    ['Nivel5', 'CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel4'],
    [Nivel5, CodUsu, Nome, PrdGrupTip], [Niv4], True)
  then
    Result := Niv4;
end;

function TUnGrade_PF.InsUpdProdNivel5(SQLTipo: TSQLType; PrdGrupTip, Nivel5,
  CodUsu: Integer; Nome: String; var Msg: String): Integer;
var
  Niv5: Integer;
begin
  Result := 0;
  //
  if PrdGrupTip = 0 then
  begin
    Msg := 'Defina o tipo de grupo de produto para n�vel 5!';
    Exit;
  end else
  if Nome = '' then
  begin
    Msg := 'Defina uma descri��o para o n�vel 5!';
    Exit;
  end else
  if CodUsu = 0 then
  begin
    Msg := 'Defina um ID para o n�vel 5!';
    Exit;
  end;
  //
  Niv5 := UMyMod.BuscaEmLivreY_Def('gragru5', 'Nivel5', SQLTipo, Nivel5);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'gragru5', False,
    ['CodUsu', 'Nome', 'PrdGrupTip'], ['Nivel5'],
    [CodUsu, Nome, PrdGrupTip], [Niv5], True)
  then
    Result := Niv5;
end;

function TUnGrade_PF.ObtemGrandezaDeGraGruX(const GraGruX: Integer;
  var Grandeza: TGrandezaUnidMed; var Tipo_Item: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    unDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT med.Grandeza, ',
    'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
    '']);
    Grandeza  := TGrandezaUnidMed(Qry.FieldByName('Grandeza').AsInteger);
    Tipo_Item := Qry.FieldByName('Tipo_Item').AsInteger;
    Result    := True;
  finally
    Qry.Free;
  end;
end;


function TUnGrade_PF.ObtemFisRegMvtTipoCalc(RegrFiscal, StqCenCad,
  Empresa: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT TipoCalc ',
      'FROM fisregmvt ',
      'WHERE Codigo=' + Geral.FF0(RegrFiscal),
      'AND StqCenCad=' + Geral.FF0(StqCenCad),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
    //
    Result := Qry.FieldByName('TipoCalc').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnGrade_PF.ObtemStqMovValAPrecoF(OriCodi, OriCtrl, OriCnta, IDCtrl,
  Empresa: Integer): Double;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Preco ',
      'FROM stqmovvala ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriCnta=' + Geral.FF0(OriCnta),
      'AND IDCtrl=' + Geral.FF0(IDCtrl),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
    //
    Result := Qry.FieldByName('Preco').AsFloat;
  finally
    Qry.Free;
  end;
end;

function TUnGrade_PF.ObtemStqMovValA_Many(const OriCodi, OriCtrl, OriCnta,
  IDCtrl, Empresa: Integer; var Qtde, Preco, Total: Double; var obsCont_xCampo,
  obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qtde   := 0.000;
  Preco  := 0.0000000;
  Total  := 0.00;
  Qry    := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Qtde, Preco, Total, ',
      'obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto ',
      'FROM stqmovvala ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriCnta=' + Geral.FF0(OriCnta),
      'AND IDCtrl=' + Geral.FF0(IDCtrl),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
    //
    //Geral.MB_Teste(Qry.SQL.Text);
    //
    Qtde := Qry.FieldByName('Qtde').AsFloat;
    Preco := Qry.FieldByName('Preco').AsFloat;
    Total := Qry.FieldByName('Total').AsFloat;
    obsCont_xCampo   := Qry.FieldByName('obsCont_xCampo').AsString;
    obsCont_xTexto   := Qry.FieldByName('obsCont_xTexto').AsString;
    obsFisco_xCampo  := Qry.FieldByName('obsFisco_xCampo').AsString;
    obsFisco_xTexto  := Qry.FieldByName('obsFisco_xTexto').AsString;
    //
    Result := True;
  finally
    Qry.Free;
  end;
end;

function TUnGrade_PF.ObtemStqMovValA_SUM(const OriCodi, Tipo, Empresa: Integer;
var Qtde, Total: Double): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qtde   := 0.000;
  Total  := 0.00;
  Qry    := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Qtde) Qtde, SUM(Total) Total ',
      'FROM stqmovvala ',
      'WHERE OriCodi=' + Geral.FF0(OriCodi),
      'AND Tipo =' + Geral.FF0(Tipo),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
    //
    //Geral.MB_Teste(Qry.SQL.Text);
    //
    Qtde := Qry.FieldByName('Qtde').AsFloat;
    Total := Qry.FieldByName('Total').AsFloat;
    //
    Result := True;
  finally
    Qry.Free;
  end;
end;

function TUnGrade_PF.RegistrarACor(const Nivel1, GraCorCad: Integer;
  var Controle: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    // Ver se a cor ja existe!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragruc ',
    'WHERE Nivel1=' + Geral.FF0(Nivel1),
    'AND GraCorCad=' + Geral.FF0(GraCorCad),
    '']);
    if Qry.RecordCount > 0 then
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
    end else
      Controle := 0;
    if Controle = 0 then
    begin
      // Registrar a cor
      Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
      //GraCorCad      := Cor[I];
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
      'Nivel1', 'GraCorCad'], [
      'Controle'], [
      Nivel1, GraCorCad], [
      Controle], True);
    end else
      Result := True;
  finally
    Qry.Free;
  end;
end;

function TUnGrade_PF.ReopenFatStqCenCad(RegrFiscal, Empresa: Integer; NomeEmp,
  NomeFisRegCad: String; QueryStqCenCad: TMySQLQuery): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryStqCenCad, Dmod.MyDB, [
    'SELECT Codigo, CodUsu, Nome ',
    'FROM stqcencad scc ',
    'WHERE Codigo IN ',
    '( ',
    '  SELECT StqCenCad ',
    '  FROM fisregmvt ',
    '  WHERE Codigo=' + Geral.FF0(RegrFiscal),
    '  AND Empresa=' + Geral.FF0(Empresa),
    ') ',
    'ORDER BY Nome ',
    '']);
  if QueryStqCenCad.RecordCount = 0 then
  begin
    Geral.MB_Aviso('A empresa ' + NomeEmp +
      ' n�o possui nenhuma movimenta��o para a regra fiscal: ' + sLineBreak +
      'Nome: ' + NomeFisRegCad + sLineBreak +
      'ID: ' + Geral.FF0(RegrFiscal) + sLineBreak +
      'Defina a movimenta��o para que voc� possa faturar os itens!');
  end else
    Result := True;
end;

procedure TUnGrade_PF.ReopenStqCenLoc(Qry: TmySQLQuery; Centro: Variant;
  Controle: Integer; MovimID: TEstqMovimID);
  procedure Abre(MovID: TEstqMovimID);
  var
    SQL_StqCenCad, SQL_MovimID_LJ, SQL_MovimID_WH: String;
  begin
    if (Centro <> Null) and (Centro <> Unassigned) then
      SQL_StqCenCad := 'WHERE scl.Codigo=' + Geral.FF0(Centro)
    else
      SQL_StqCenCad := 'WHERE scl.Codigo>-999999999';
    //
    if MovID = TEstqMovimID.emidAjuste then
    begin
      SQL_MovimID_LJ := '';
      SQL_MovimID_WH := '';
    end else
    begin                              // vsmovidloc << UnProjGroup_Consts
      SQL_MovimID_LJ := 'LEFT JOIN ' + CO_TAB_XX_MOV_ID_LOC + ' mil ON mil.StqCenLoc=scl.Controle';
      SQL_MovimID_WH := 'AND mil.MovimID=' + Geral.FF0(Integer(MovID));
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT scl.Controle, scl.CodUsu,  scc.EntiSitio, ',
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
      'FROM stqcenloc scl ',
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
      SQL_MovimID_LJ,
      SQL_StqCenCad,
      SQL_MovimID_WH,
      'ORDER BY NO_LOC_CEN ',
      '']);
    if Controle <> 0 then
      Qry.Locate('Controle', Controle, []);
    //Geral.MB_SQL(nil, Qry);
  end;
begin
  if Qry.Params.Count = 0 then
  begin
    Qry.Params.Add;
    Qry.Params[0].Value := Centro;
  end;
  Abre(MovimID);
  if (Qry.RecordCount = 0) and (MovimID <> TEstqMovimID.emidAjuste) then
  begin
    Abre(TEstqMovimID.emidAjuste);
  end;
end;

function TUnGrade_PF.SelecionaGraGruXDeGraGruE(const Fornece: Integer; const
  cProd, xProd, NCM, uCom: String; const CFOP: Integer; var GraGruX, GraGru1:
  Integer; var NomeGGX: String): TKindLocSelGGXE;
var
  Qry: TmySQLQuery;
begin
  Result  := TKindLocSelGGXE.klsggxeNotLoc;
  GraGruX := 0;
  GraGru1 := 0;
  NomeGGX := '';
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cad.* ',
    'FROM gragruecad  cad',
    'WHERE cad.Fornece=' + Geral.FF0(Fornece),
    'AND cad.cProd="' + cProd + '"',
    'AND cad.xProd="' + xProd + '"',
    '']);
    //
    if Qry.RecordCount > 0 then
    begin
      //GraGruX := ???;
      Result  := TKindLocSelGGXE.klsggxeLocMul;
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Nivel1, GraGruX  ',
      'FROM gragrueits  ',
      'WHERE Fornece=' + Geral.FF0(Fornece),
      'AND cProd="' + cProd + '"',
      'AND xProd="' + xProd + '"',
      'AND NCM="' + NCM + '"',
      'AND CFOP=' + Geral.FF0(CFOP),
      'AND uCom="' + uCom + '"',
      'ORDER BY GraGruX  ',
      '']);
      //
      if Qry.RecordCount > 0 then
      begin
        GraGruX := Qry.FieldByName('GraGruX').AsInteger;
        GraGru1 := Qry.FieldByName('Nivel1').AsInteger;
        Result  := TKindLocSelGGXE.klsggxeLocUni;
        //Result  := GraGruX <> 0;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT CONCAT(gg1.Nome, " ",  ',
        '  IF(gti.PrintTam=1, gti.Nome, ""), " ",  ',
        '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR ',
        'FROM gragrux ggx ',
        'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
        'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
        'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
        'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
        '']);
        NomeGGX := Qry.FieldByName('NO_PRD_TAM_COR').AsString;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnGrade_PF.SQL_Estoque(const Empresa, GraGru1, GraGru2, GraGru3,
PrdGrupTip, StqCenCad, GraCusPrc, CGSitEstq_Value: Integer;
const CkStqCenCad_Checked, CkSelecao0_Enabled, CkSelecao0_Checked,
CkPrdGrupo_Checked: Boolean; InatIncTipo, InatIncOriCodi: Integer; const LaAviso1, LaAviso2: TLabel;
var SQL_Sum, SQL_Its: String): Boolean;
var
  SQL_StqCenCad_Nome, SQL_Empresa, SQL_Selecao0, SQL_GraGru1, SQL_GraGru2,
  SQL_GraGru3, SQL_PrdGrupTip, SQL_StqCenCad, SQL_GraCusPrc, FGGX_SCC_Stq,
  SQL_GroupBy, SQL_Where2, SQL_SitsAtivo: String;
begin
  Result := False;
  if MyObjects.FIC(CGSitEstq_Value = 0, nil,
  'Informe pelo menos uma "Situa��o do estoque"!') then Exit;
  //
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a empresa!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando');
    //
    SQL_Empresa := '';
    SQL_GraGru1 := '';
    SQL_GraGru2 := '';
    SQL_GraGru3 := '';
    SQL_PrdGrupTip := '';
    SQL_StqCenCad := '';
    SQL_GraCusPrc := '';
    SQL_Where2 := '';
    //
    if (CkStqCenCad_Checked) or (StqCenCad <> 0) then
      SQL_StqCenCad_Nome := 'scc.Nome, '
    else
      SQL_StqCenCad_Nome := '"TODOS", ';
    //
    if Empresa <> 0 then
      SQL_Empresa := 'AND smia.Empresa=' + Geral.FF0(Empresa);
    //
    if CkSelecao0_Enabled and CkSelecao0_Checked then
    begin
      SQL_Selecao0 := Geral.ATS([
      'AND ggx.GraGru1 IN (',
      '  SELECT Codigo ',
      '  FROM ' + VAR_MyPID_DB_NOME + '.pesqesel_sel',
      ')']);
    end else
    begin
      if GraGru1 <> 0 then
        SQL_GraGru1 := 'AND ggx.GraGru1=' +Geral.FF0(GraGru1);
      if GraGru2 <> 0 then
        SQL_GraGru2 := 'AND gg1.Nivel2=' +Geral.FF0(GraGru2);
      if GraGru3 <> 0 then
        SQL_GraGru3 := 'AND gg1.Nivel3=' +Geral.FF0(GraGru3);
      if PrdGrupTip <> 0 then
        SQL_PrdGrupTip := 'AND gg1.PrdGrupTip=' +Geral.FF0(PrdGrupTip);
      if StqCenCad <> 0 then
        SQL_StqCenCad := 'AND smia.StqCenCad=' +Geral.FF0(StqCenCad);
    end;
    if CkPrdGrupo_Checked = True then
    begin
      if CkStqCenCad_Checked then
        SQL_GroupBy := 'GROUP BY gg1.Nivel1, smia.StqCenCad'
      else
        SQL_GroupBy := 'GROUP BY gg1.Nivel1';
    end else
    begin
      if CkStqCenCad_Checked then
        SQL_GroupBy := 'GROUP BY smia.GraGruX, smia.StqCenCad'
      else
        SQL_GroupBy := 'GROUP BY smia.GraGruX';
    end;
    //
    case CGSitEstq_Value of
      1: SQL_Where2 := 'WHERE QTDE < 0';
      2: SQL_Where2 := 'WHERE QTDE = 0';
      3: SQL_Where2 := 'WHERE QTDE <= 0';
      4: SQL_Where2 := 'WHERE QTDE > 0';
      5: SQL_Where2 := 'WHERE QTDE <> 0';
      6: SQL_Where2 := 'WHERE QTDE >= 0';
      7: SQL_Where2 := ''; // Tudo
    end;
    //
    if InatIncOriCodi = 0 then
      SQL_SitsAtivo := 'WHERE smia.Ativo=1'
    else
      SQL_SitsAtivo := 'WHERE (smia.Ativo=1 OR (smia.Tipo=' +
      Geral.FF0(InatIncTipo) + ' AND smia.OriCodi=' + Geral.FF0(InatIncOriCodi) +
      '))';
    //
    FGGX_SCC_Stq := GradeCriar.RecriaTempTableNovo(ntrttGGX_SCC_Stq, DModG.QrUpdPID1, False);
    SQL_Sum := Geral.ATS([
    'INSERT INTO ' + DmodG.MyPID_DB.DataBaseName + '.' + FGGX_SCC_Stq,
    'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,',
    'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,',
    'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,',
    'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,',
    'SUM(smia.Qtde * smia.Baixa) QTDE, SUM(smia.Pecas) PECAS,', //N�o estava funcionando agora est�
    'SUM(smia.Peso) PESO, SUM(smia.AreaM2) AREAM2,',
    'SUM(smia.AreaP2) AREAP2, ggv.CustoPreco PrcCusUni, ',
    'smia.GraGruX, smia.StqCenCad, ',
    //
    SQL_StqCenCad_Nome,
    //
    'gg1.NCM',
    'FROM stqmovitsa smia',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN stqcencad  scc ON scc.Codigo=smia.StqCenCad',
    'LEFT JOIN gragruval  ggv ON ggv.GraGruX=smia.GraGruX',
    // Evitar n�o encontrar registros sem pre�o!
    '          AND ggv.GraCusPrc=' + Geral.FF0(GraCusPrc),
    '          AND ggv.Entidade=' + Geral.FF0(Empresa),
    //
    //'WHERE smia.Ativo=1',
    //'AND smia.Baixa=1', ver acima baixa * Qtde!!!
    (*
    Alterado em 22/04/2014 foi corrigido o campo baixa
    'WHERE smia.Baixa=1',
    *)
    SQL_SitsAtivo,
    'AND smia.ValiStq <> 101',  // 2020-11-22
    //
    SQL_Empresa,
    //
    SQL_Selecao0,
    SQL_GraGru1,
    SQL_GraGru2,
    SQL_GraGru3,
    SQL_PrdGrupTip,
    SQL_StqCenCad,
    SQL_GraCusPrc,
    SQL_GroupBy,
    'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX',
    ';']);
    //Geral.MB_Teste(SQL_Sum);
    //
    SQL_Its := Geral.ATS([
    'SELECT CONCAT(NO_PRD, " ", NO_TAM, " ", NO_COR) NO_PRD_TAM_COR, ',
    'stq.*, gg1.CodUsu ',
    'FROM ' + FGGX_SCC_Stq + ' stq',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1 = stq.Nivel1 ',
    SQL_Where2,
    '']);
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnGrade_PF.ValidaGrandeza(TipoCalc, HowBxaEstq: Integer; Pecas,
  AreaM2, AreaP2, Peso: Double; var Msg: String): Boolean;
var
  NeedQI, NeedQA, NeedQK: Boolean;
  Erro: String;
begin
  Result := True;
  Msg    := '';
  //
  if TipoCalc > 0 then
  begin
    NeedQI := Geral.IntInConjunto(1, HowBxaEstq);
    NeedQA := Geral.IntInConjunto(2, HowBxaEstq);
    NeedQK := Geral.IntInConjunto(4, HowBxaEstq);
    //
    Erro   := '';
    //
    if (Pecas  = 0) and NeedQI then Erro := 'Pe�as';
    if (AreaM2 = 0) and NeedQA then Erro := '�rea';
    if (Peso   = 0) and NeedQK then Erro := 'Peso';
    //
    if Erro <> '' then
    begin
      Msg := 'Grandeza obrigat�ria: "' + Erro + '".';
      Result := False;
    end;
  end;
end;

function TUnGrade_PF.ValidaPainelFisRegCad(CFOP, UFEmit, UFDest, CST_B: String;
  var Mensagem: String): Boolean;
var
  CST_B_Txt: String;
begin
  Result := False;
  //
  if (Trim(CFOP) = '') or (Trim(CFOP) = '0') then
  begin
    Mensagem := 'O campo "CFOP" n�o foi informado!';
    Exit;
  end;
  if Trim(UFEmit) = '' then
  begin
    Mensagem := 'O campo "Minha UF (origem)" n�o foi informado!';
    Exit;
  end;
  if Trim(UFDest) = '' then
  begin
    Mensagem := 'O campo "UF (destino)" n�o foi informado!';
    Exit;
  end;
  //
  CST_B_Txt := Trim(CST_B);
  //
  if CST_B_Txt = '0' then
    CST_B_Txt := '00';
  if CST_B_Txt <> '' then
  begin
    try
      Geral.IMV(CST_B_Txt);
    except
      Mensagem := 'O campo "CST" n�o foi informado para o ICMS � inv�lido!!';
      Exit;
    end;
  end;
  Result := True;
end;

end.
