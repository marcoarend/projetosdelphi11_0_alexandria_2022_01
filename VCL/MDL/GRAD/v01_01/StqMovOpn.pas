unit StqMovOpn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmStqMovOpn = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DBGMovOpn: TdmkDBGridZTO;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGMovOpnDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmStqMovOpn: TFmStqMovOpn;

implementation

uses
{$IfDef uStqCnsg}
StqCnsgGoCab, StqCnsgBkCab, StqCnsgVeCab,
{$EndIf}
UnMyObjects, Module, DmkDAC_PF, ModProd, UnGrade_Jan, UnApp_Jan;

{$R *.DFM}

procedure TFmStqMovOpn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqMovOpn.DBGMovOpnDblClick(Sender: TObject);
const
  sProcName = 'TFmStqMovOpn.DBGMovOpnDblClick()';
  StqCiclCab = 0;
  Fornece = 0;
  Empresa = 0;
var
  Codigo, Fat_ID: Integer;
begin
  if (DmProd.QrMovOpn.State <> dsInactive)
  and (DmProd.QrMovOpn.RecordCount > 0) then
  begin
    Codigo := DmProd.QrMovOpnCodigo.Value;
    Fat_ID := Trunc(DmProd.QrMovOpnMov_ID.Value);
    //
    case Fat_ID of
      0: Grade_Jan.MostraFormStqBalCad(Codigo);
      5: Grade_Jan.MostraFormStqInnCad(Codigo);
      99:   Grade_Jan.MostraFormStqManCad(Codigo);
{$IfDef uStqCnsg}
      4201:  App_jan.MostraFormStqCnsgGoCab(Codigo, StqCiclCab, Fornece, Empresa);
      4203:  App_jan.MostraFormStqCnsgBkCab(Codigo, StqCiclCab, Fornece, Empresa);
      4205:  App_jan.MostraFormStqCnsgVeCab(Codigo, StqCiclCab, Fornece, Empresa);
{$EndIf}
      else
        Geral.MB_Erro('FatID "' + Geral.FF0(Fat_ID) + '" n�o implementado em '
        + sProcName);
    end;
  end;
end;

procedure TFmStqMovOpn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqMovOpn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  DBGMovOpn.DataSource := DmProd.DsMovOpn;
end;

procedure TFmStqMovOpn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
