unit StqBalCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, ComCtrls,
  dmkEditDateTimePicker, frxClass, frxDBSet, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmStqBalCad = class(TForm)
    PainelDados: TPanel;
    DsStqBalCad: TDataSource;
    QrStqBalCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdGrupoBal: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMBalanco: TPopupMenu;
    Panel4: TPanel;
    Incluinovobalano1: TMenuItem;
    Alterabalanoatual1: TMenuItem;
    Excluibalanoatual1: TMenuItem;
    QrStqBalCadCodigo: TIntegerField;
    QrStqBalCadCodUsu: TIntegerField;
    QrStqBalCadNome: TWideStringField;
    QrStqBalCadEmpresa: TIntegerField;
    QrStqBalCadPrdGrupTip: TIntegerField;
    QrStqBalCadStqCenCad: TIntegerField;
    QrStqBalCadCasasProd: TSmallintField;
    QrStqBalCadAbertura: TDateTimeField;
    QrStqBalCadEncerrou: TDateTimeField;
    DsFiliais: TDataSource;
    QrFiliais: TmySQLQuery;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkValUsu1: TdmkValUsu;
    QrStqBalCadNOMEFILIAL: TWideStringField;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    dmkLabel2: TdmkLabel;
    dmkValUsu2: TdmkValUsu;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrStqBalCadNOMEPRDGRUPTIP: TWideStringField;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    QrStqBalCadMOMESTQCENCAD: TWideStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    EdStqCenCad: TdmkEditCB;
    dmkLabel3: TdmkLabel;
    CBStqCenCad: TdmkDBLookupComboBox;
    dmkValUsu3: TdmkValUsu;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    RGCasasProd: TdmkRadioGroup;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    Label11: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit5: TDBEdit;
    QrStqBalCadBalQtdItem: TFloatField;
    QrLocCenLoc: TmySQLQuery;
    DsLocCenLoc: TDataSource;
    QrLocCenLocControle: TIntegerField;
    QrLocCenLocCodUsu: TIntegerField;
    QrLocCenLocNome: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    DsLocBalIts: TDataSource;
    QrLocBalIts: TmySQLQuery;
    QrLocBalItsNOMENIVEL1: TWideStringField;
    QrLocBalItsGraCorCad: TIntegerField;
    QrLocBalItsNOMECOR: TWideStringField;
    QrLocBalItsNOMETAM: TWideStringField;
    QrLocBalItsGraGru1: TIntegerField;
    QrLocBalItsCodigo: TIntegerField;
    QrLocBalItsControle: TIntegerField;
    QrLocBalItsTipo: TSmallintField;
    QrLocBalItsDataHora: TDateTimeField;
    QrLocBalItsStqCenLoc: TIntegerField;
    QrLocBalItsGraGruX: TIntegerField;
    QrLocBalItsSEQ: TIntegerField;
    QrLocBalItsCODUSU_NIVEL1: TIntegerField;
    QrLocBalItsCODUSU_COR: TIntegerField;
    dmkDBGrid2: TdmkDBGrid;
    frxSTQ_BALAN_001_01: TfrxReport;
    frxDsLocCenLoc: TfrxDBDataset;
    frxDsLocBalIts: TfrxDBDataset;
    QrLocBalItsQtdLei: TFloatField;
    QrListaLoc: TmySQLQuery;
    TabSheet2: TTabSheet;
    dmkDBGrid3: TdmkDBGrid;
    DsListaLoc: TDataSource;
    PMImprime: TPopupMenu;
    porLocal1: TMenuItem;
    peloReduzido1: TMenuItem;
    QrListaLocCODUSU_STQCENLOC: TIntegerField;
    QrListaLocNOMESTQCENLOC: TWideStringField;
    QrListaLocStqCenLoc: TIntegerField;
    QrListaLocCODUSU_NIVEL1: TIntegerField;
    QrListaLocNOMENIVEL1: TWideStringField;
    QrListaLocGraCorCad: TIntegerField;
    QrListaLocCODUSU_COR: TIntegerField;
    QrListaLocNOMECOR: TWideStringField;
    QrListaLocNOMETAM: TWideStringField;
    QrListaLocGraGru1: TIntegerField;
    QrListaLocLEITURAS: TLargeintField;
    QrListaLocDH_INI: TDateTimeField;
    QrListaLocDH_FIM: TDateTimeField;
    QrListaLocQtdLei: TFloatField;
    QrListaLocQtdAnt: TFloatField;
    QrListaLocQTDDIF: TFloatField;
    TabSheet3: TTabSheet;
    QrListaCen: TmySQLQuery;
    DsListaCen: TDataSource;
    QrListaCenCODUSU_NIVEL1: TIntegerField;
    QrListaCenNOMENIVEL1: TWideStringField;
    QrListaCenGraCorCad: TIntegerField;
    QrListaCenCODUSU_COR: TIntegerField;
    QrListaCenNOMECOR: TWideStringField;
    QrListaCenNOMETAM: TWideStringField;
    QrListaCenGraGru1: TIntegerField;
    QrListaCenLEITURAS: TLargeintField;
    QrListaCenDH_INI: TDateTimeField;
    QrListaCenDH_FIM: TDateTimeField;
    QrListaCenQtdLei: TFloatField;
    QrListaCenQtdAnt: TFloatField;
    QrListaCenQTDDIF: TFloatField;
    dmkDBGrid4: TdmkDBGrid;
    QrListaCenQTD_IGUAL: TFloatField;
    QrListaCenQTD_MAIS: TFloatField;
    QrListaCenQTD_MENOS: TFloatField;
    EncerraBalanoatual1: TMenuItem;
    N1: TMenuItem;
    QrNew: TmySQLQuery;
    QrNewCodigo: TIntegerField;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    QrStqBalCadENCERROU_TXT: TWideStringField;
    QrStqBalCadGrupoBal: TSmallintField;
    EdCodigo: TdmkEdit;
    Label13: TLabel;
    RGForcada: TdmkRadioGroup;
    QrStqBalCadStatus: TSmallintField;
    QrStqBalCadLk: TIntegerField;
    QrStqBalCadDataCad: TDateField;
    QrStqBalCadDataAlt: TDateField;
    QrStqBalCadUserCad: TIntegerField;
    QrStqBalCadUserAlt: TIntegerField;
    QrStqBalCadAlterWeb: TSmallintField;
    QrStqBalCadAtivo: TSmallintField;
    QrStqBalCadForcada: TSmallintField;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtBalanco: TBitBtn;
    BtLeitura: TBitBtn;
    QrPrdGrupTipFracio: TSmallintField;
    QrLocBalItsCustoAll: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqBalCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqBalCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtBalancoClick(Sender: TObject);
    procedure PMBalancoPopup(Sender: TObject);
    procedure QrStqBalCadBeforeClose(DataSet: TDataSet);
    procedure QrStqBalCadAfterScroll(DataSet: TDataSet);
    procedure BtLeituraClick(Sender: TObject);
    procedure Incluinovobalano1Click(Sender: TObject);
    procedure Alterabalanoatual1Click(Sender: TObject);
    procedure QrLocCenLocAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure porLocal1Click(Sender: TObject);
    procedure QrStqBalCadCalcFields(DataSet: TDataSet);
    procedure DBEdit6Change(Sender: TObject);
    procedure QrLocCenLocBeforeClose(DataSet: TDataSet);
    procedure EncerraBalanoatual1Click(Sender: TObject);
    procedure EdAberturaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPrdGrupTipChange(Sender: TObject);
  private
    FFmtQtde: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FMultiGrandeza: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenLocCenLoc(Controle: Integer);
    procedure ReopenListaLoc();
    procedure ReopenListaCen();
    procedure ReopenLocBalIts();
  end;

var
  FmStqBalCad: TFmStqBalCad;
const
  FFormatFloat = '00000';
  SBalancoAberto = 'BALAN�O ABERTO';

implementation

uses
//{$IfNDef NAO_EMPRESA} MasterSelFilial, {$EndIf}
  UnMyObjects, Module, MyDBCheck, StqBalLei, StqBalTrf, ModuleGeral,
  ModProd, DmkDAC_PF, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqBalCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqBalCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqBalCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqBalCad.DefParams;
begin
  VAR_GOTOTABELA := 'StqBalCad';
  VAR_GOTOMYSQLTABLE := QrStqBalCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('pgt.Nome NOMEPRDGRUPTIP, scc.Nome MOMESTQCENCAD, ');
  VAR_SQLx.Add('pem.BalQtdItem, sbc.*');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM stqbalcad sbc');
  VAR_SQLx.Add('LEFT JOIN entidades fil ON fil.Codigo=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=sbc.PrdGrupTip');
  VAR_SQLx.Add('LEFT JOIN stqcencad scc ON scc.Codigo=sbc.StqCenCad');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem  ON pem.Codigo=sbc.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE sbc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND sbc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sbc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sbc.Nome Like :P0');
  //
end;

procedure TFmStqBalCad.EdAberturaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Data: TDateTime;
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    if not DBCheck.ObtemData(Date, Data, 0, 84540/84600, True) then Exit;
    RGForcada.ItemIndex := 1;
    EdAbertura.ValueVariant := Data;
  end;
end;

procedure TFmStqBalCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqBalCad', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqBalCad.EdPrdGrupTipChange(Sender: TObject);
var
  Casas: Integer;
begin
  if EdPrdGrupTip.ValueVariant <> 0 then
    Casas := QrPrdGrupTipFracio.Value
  else
    Casas := -1;
  //
  RGCasasProd.ItemIndex := Casas;
end;

procedure TFmStqBalCad.EncerraBalanoatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqBalTrf, FmStqBalTrf, afmoNegarComAviso) then
  begin
    FmStqBalTrf.ShowModal;
    FmStqBalTrf.Destroy;
  end;
end;

procedure TFmStqBalCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqBalCad.PMBalancoPopup(Sender: TObject);
var
  Habil1: Boolean;
begin
  Habil1 := (QrStqBalCad.State <> dsInactive) and (QrStqBalCad.RecordCount > 0)
              and (QrStqBalCadEncerrou.Value = 0);
  //
  Alterabalanoatual1.Enabled  := Habil1;
  EncerraBalanoatual1.Enabled := Habil1;
end;

procedure TFmStqBalCad.porLocal1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxSTQ_BALAN_001_01, 'Resultado de balan�o');
end;

procedure TFmStqBalCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqBalCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqBalCad.DBEdit6Change(Sender: TObject);
begin
  if DBEdit6.Text = SBalancoAberto then
  begin
    DBEdit6.Font.Color := clRed;
    DBEdit6.Font.Style := [];//[fsBold];
  end else begin
    DBEdit6.Font.Color := clWindowText;
    DBEdit6.Font.Style := [];
  end;
end;

procedure TFmStqBalCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqBalCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqBalCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqBalCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqBalCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqBalCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqBalCadCodigo.Value;
  Close;
end;

procedure TFmStqBalCad.Alterabalanoatual1Click(Sender: TObject);
var
  Habilita: Boolean;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrStqBalCad, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'StqBalCad');
  Habilita := QrLocCenLoc.RecordCount = 0;
  EdPrdGrupTip.Enabled := Habilita;
  CBPrdGrupTip.Enabled := Habilita;
  EdStqCenCad.Enabled  := Habilita;
  CBStqCenCad.Enabled  := Habilita;
  EdFilial.Enabled     := Habilita;
  CBFilial.Enabled     := Habilita;
end;

procedure TFmStqBalCad.BtBalancoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBalanco, BtBalanco);
end;

procedure TFmStqBalCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, Empresa, PrdGrupTip, StqCenCad, GrupoBal: Integer;
  Nome: String;
begin
  if MyObjects.FIC(Geral.IMV(EdFilial.Text) = 0, EdFilial, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdPrdGrupTip.Text) = 0, EdPrdGrupTip, 'Informe o tipo de grupo de produtos!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdStqCenCad.Text) = 0, EdStqCenCad, 'Informe o centro de estoque!') then Exit;
  //
  Empresa    := QrFiliaisCodigo.Value;
  PrdGrupTip := QrPrdGrupTipCodigo.Value;
  StqCenCad  := QrStqCenCadCodigo.Value;
  //verifica se n�o h� faturamanto aberto
  if DmProd.ExisteFatPedAberto(Empresa, StqCenCad) then
    Exit;
  if DmProd.ExisteStqManAberto(Empresa, StqCenCad) then
    Exit;
  Nome := EdNome.Text;
  //
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  QrNew.Close;
  QrNew.Params[00].AsInteger := Empresa;
  QrNew.Params[01].AsInteger := PrdGrupTip;
  QrNew.Params[02].AsInteger := StqCenCad;
  QrNew.Params[02].AsInteger := StqCenCad;
  QrNew.Params[03].AsInteger := QrStqBalCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrNew, Dmod.MyDB);
  //
  if MyObjects.FIC(QrNew.RecordCount > 0, nil,
    'Inclus�o abortada!' + sLineBreak + 'Existe um balan�o aberto para esta ' +
    'configura��o (ID n� ' + IntToStr(QrNewCodigo.Value) + ').') then
  begin
    LocCod(QrStqBalCadCodigo.Value, QrNewCodigo.Value);
    Exit;
  end;
  //
  //
  if ImgTipo.SQLType = stIns then
  begin
    if not DModG.BuscaProximoCodigoInt_Novo('Controle', 'GrupoBal', '', 0, '', GrupoBal) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    EdGrupoBal.ValueVariant := GrupoBal;
  end;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('StqBalCad', 'Codigo', ImgTipo.SQLType,
    QrStqBalCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmStqBalCad, PainelEdit,
    'StqBalCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmStqBalCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'StqBalCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqBalCad', 'Codigo');
  MostraEdicao(0, stlok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqBalCad', 'Codigo');
end;

procedure TFmStqBalCad.BtLeituraClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqBalLei, FmStqBalLei, afmoNegarComAviso) then
  begin
    FmStqBalLei.QrLidosQtdLei.DisplayFormat := FFmtQtde;
    FmStqBalLei.QrTotalQtdLei.DisplayFormat := FFmtQtde;
    FmStqBalLei.EdQtdLei.DecimalSize        := QrStqBalCadCasasProd.Value;
    FmStqBalLei.EdQtdLei.ValueVariant       := QrStqBalCadBalQtdItem.Value;
    FmStqBalLei.EdStqCenCad.ValueVariant    := QrStqBalCadStqCenCad.Value;
    FmStqBalLei.CBStqCenCad.KeyValue        := QrStqBalCadStqCenCad.Value;
    FmStqBalLei.ReopenLidos(0);
    FmStqBalLei.ShowModal;
    LocCod(QrStqBalCadCodigo.Value, QrStqBalCadCodigo.Value);
    FmStqBalLei.Destroy;
  end;
end;

procedure TFmStqBalCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FMultiGrandeza := False;
  // n�o usa!!!
  TabSheet2.TabVisible := false;
  //
  PainelEdit.Align             := alClient;
  Panel4.Align                 := alClient;
  PageControl1.ActivePageIndex := 0;
  UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmStqBalCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqBalCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqBalCad.SbImprimeClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmStqBalCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqBalCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqBalCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmStqBalCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqBalCad.QrLocCenLocAfterScroll(DataSet: TDataSet);
begin
  ReopenLocBalIts();
end;

procedure TFmStqBalCad.QrLocCenLocBeforeClose(DataSet: TDataSet);
begin
  QrLocBalIts.Close;
end;

procedure TFmStqBalCad.QrStqBalCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtLeitura.Enabled := (QrStqBalCad.RecordCount > 0)
    and (QrStqBalCadEncerrou.Value = 0);
end;

procedure TFmStqBalCad.QrStqBalCadAfterScroll(DataSet: TDataSet);
begin
  FFmtQtde := dmkPF.FormataCasas(QrStqBalCadCasasProd.Value);
  ReopenLocCenLoc(0);
  ReopenListaLoc();
  ReopenListaCen();
end;

procedure TFmStqBalCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqBalCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqBalCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'StqBalCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqBalCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqBalCad.Incluinovobalano1Click(Sender: TObject);
begin
  if AppPF.ImpedePorMovimentoAberto() then
    Exit;
  RGForcada.ItemIndex := 0;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrStqBalCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'StqBalCad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqBalCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  //
  EdAbertura.ValueVariant :=  DModG.ObtemAgora();
  EdFilial.ValueVariant   := DmodG.QrFiliLogFilial.Value;
  CBFilial.KeyValue       := DmodG.QrFiliLogFilial.Value;
end;

procedure TFmStqBalCad.QrStqBalCadBeforeClose(DataSet: TDataSet);
begin
  BtLeitura.Enabled := False;
  QrLocCenLoc.Close;
  QrListaLoc.Close;
  QrListacen.Close;
end;

procedure TFmStqBalCad.QrStqBalCadBeforeOpen(DataSet: TDataSet);
begin
  QrStqBalCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqBalCad.QrStqBalCadCalcFields(DataSet: TDataSet);
begin
  if QrStqBalCadEncerrou.Value = 0 then
    QrStqBalCadENCERROU_TXT.Value := SBalancoAberto
  else
    QrStqBalCadENCERROU_TXT.Value := Geral.FDT(QrStqBalCadEncerrou.Value, 0);
end;

procedure TFmStqBalCad.ReopenLocBalIts;
begin
  QrLocBalIts.Close;
  QrLocBalIts.Params[00].AsInteger := QrStqBalCadCodigo.Value;
  QrLocBalIts.Params[01].AsInteger := QrLocCenLocControle.Value;
  UnDmkDAC_PF.AbreQuery(QrLocBalIts, Dmod.MyDB);
  //
  //if Controle <> 0 then
    //QrLocBalIts.Locate('Controle', Controle, []);
end;

procedure TFmStqBalCad.ReopenLocCenLoc(Controle: Integer);
begin
  QrLocCenLoc.Close;
  QrLocBalItsQtdLei.DisplayFormat := FFmtQtde;
  QrLocCenLoc.Params[0].AsInteger := QrStqBalCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrLocCenLoc, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrLocCenLoc.Locate('Controle', Controle, []);
end;

procedure TFmStqBalCad.ReopenListaLoc();
begin
  Exit;
  // N�o usa!!!
  QrListaLoc.Close;
  QrListaLocQtdLei.DisplayFormat := FFmtQtde;
  QrListaLocQtdAnt.DisplayFormat := FFmtQtde;
  QrListaLocQTDDIF.DisplayFormat := FFmtQtde;
  QrListaLoc.Params[0].AsInteger := QrStqBalCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrListaLoc, Dmod.MyDB);
end;

procedure TFmStqBalCad.ReopenListaCen();
begin
  QrListaCen.Close;
  QrListaCenQtdLei.DisplayFormat    := FFmtQtde;
  QrListaCenQtdAnt.DisplayFormat    := FFmtQtde;
  QrListaCenQTDDIF.DisplayFormat    := FFmtQtde;
  QrListaCenQTD_IGUAL.DisplayFormat := FFmtQtde;
  QrListaCenQTD_MAIS.DisplayFormat  := FFmtQtde;
  QrListaCenQTD_MENOS.DisplayFormat := FFmtQtde;
  QrListaCen.Params[0].AsInteger    := QrStqBalCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrListaCen, Dmod.MyDB);
end;

end.

