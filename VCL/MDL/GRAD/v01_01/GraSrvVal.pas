unit GraSrvVal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCB, dmkDBLookupComboBox, mySQLDbTables, dmkDBGridZTO;

type
  TFmGraSrvVal = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrClientes: TMySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    DsClientes: TDataSource;
    Panel5: TPanel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    EdClientSrv: TdmkEditCB;
    CBClientSrv: TdmkDBLookupComboBox;
    Panel6: TPanel;
    QrGraGru1: TMySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    DsGraGru1: TDataSource;
    QrGraTamIts: TMySQLQuery;
    QrGraTamItsControle: TIntegerField;
    QrGraCorCad: TMySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    DsGraTamIts: TDataSource;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    SbGraGru1: TSpeedButton;
    QrGraTamCad: TMySQLQuery;
    QrGraTamCadCodigo: TIntegerField;
    QrGraTamCadNome: TWideStringField;
    DsGraTamCad: TDataSource;
    QrGraTamItsNome: TWideStringField;
    DBGGraSrvVal: TdmkDBGridZTO;
    QrGraSrvVal: TMySQLQuery;
    DsGraSrvVal: TDataSource;
    QrGraSrvValUnidMed: TIntegerField;
    QrGraSrvValReferencia: TWideStringField;
    QrGraSrvValNO_GG1: TWideStringField;
    QrGraSrvValNO_Tam: TWideStringField;
    QrGraSrvValNO_COR: TWideStringField;
    QrGraSrvValNO_UnidMed: TWideStringField;
    QrGraSrvValEmpresa: TIntegerField;
    QrGraSrvValClientSrv: TIntegerField;
    QrGraSrvValControle: TIntegerField;
    QrGraSrvValReduzSrv: TIntegerField;
    QrGraSrvValGraGru1: TIntegerField;
    QrGraSrvValGraTamIts: TIntegerField;
    QrGraSrvValGraCorCad: TIntegerField;
    QrGraSrvValPreco: TFloatField;
    QrGraSrvValNO_ReduzSrv: TWideStringField;
    Label5: TLabel;
    EdReduzSrv: TdmkEditCB;
    CBReduzSrv: TdmkDBLookupComboBox;
    QrReduzSrv: TMySQLQuery;
    DsReduzSrv: TDataSource;
    QrReduzSrvControle: TIntegerField;
    QrReduzSrvNome: TWideStringField;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    EdGraTamCad: TdmkEditCB;
    CBGraTamCad: TdmkDBLookupComboBox;
    SBGrade: TSpeedButton;
    EdGraTamIts: TdmkEditCB;
    CBGraTamIts: TdmkDBLookupComboBox;
    EdGraCorCad: TdmkEditCB;
    CBGraCorCad: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    SbReduzSrv: TSpeedButton;
    CkEmpresa: TCheckBox;
    CkClientSrv: TCheckBox;
    CkGraGru1: TCheckBox;
    CkGraTamCad: TCheckBox;
    CkGraTamIts: TCheckBox;
    CkGraCorCad: TCheckBox;
    CkReduzSrv: TCheckBox;
    BtPesquisa: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraTamCadRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdClientSrvRedefinido(Sender: TObject);
    procedure EdGraGru1Redefinido(Sender: TObject);
    procedure EdGraTamItsRedefinido(Sender: TObject);
    procedure EdGraCorCadRedefinido(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrGraSrvValBeforeClose(DataSet: TDataSet);
    procedure QrGraSrvValAfterOpen(DataSet: TDataSet);
    procedure SbGraGru1Click(Sender: TObject);
    procedure SBGradeClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SbReduzSrvClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtPesquisaClick(Sender: TObject);
    procedure CkEmpresaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraTamIts(GraTamCad: Integer);
    procedure FechaPesquisa();
    procedure RealizaPesquisa();
    procedure InsAltItem(SQLType: TSQLType);
  public
    { Public declarations }
  end;

  var
  FmGraSrvVal: TFmGraSrvVal;

implementation

uses UnMyObjects, DmkDAC_PF, UnMySQLCuringa, MyDBCheck, UnGrade_Jan, UMySQLModule,
  Module, ModuleGeral, GraSrvValIts;

{$R *.DFM}

procedure TFmGraSrvVal.BtAlteraClick(Sender: TObject);
begin
  if DBGGraSrvVal.SelectedRows.Count > 1 then
    Geral.MB_Aviso('Selecione apenas um item!')
  else
    InsAltItem(stUpd);
end;

procedure TFmGraSrvVal.BtExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrGraSrvVal, TDBGrid(DBGGraSrvVal),
    'GraSrvVal', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmGraSrvVal.BtIncluiClick(Sender: TObject);
begin
  InsAltItem(stIns);
end;

procedure TFmGraSrvVal.BtPesquisaClick(Sender: TObject);
begin
  RealizaPesquisa();
end;

procedure TFmGraSrvVal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraSrvVal.CkEmpresaClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraSrvVal.EdGraCorCadRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraSrvVal.EdClientSrvRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraSrvVal.EdEmpresaRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraSrvVal.EdGraGru1Redefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraSrvVal.EdGraTamCadRedefinido(Sender: TObject);
var
  GraTamCad: Integer;
begin
  EdGraTamIts.ValueVariant := 0;
  CBGraTamIts.KeyValue := 0;
  GraTamCad := EdGraTamCad.ValueVariant;
  if GraTamCad = 0 then
  begin
    QrGraTamIts.Close;
  end else
  begin
    ReopenGraTamIts(GraTamCad);
  end;
  //FechaPesquisa();
end;

procedure TFmGraSrvVal.EdGraTamItsRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmGraSrvVal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraSrvVal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReduzSrv, Dmod.MyDB);
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmGraSrvVal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F2 then
    RealizaPesquisa();
end;

procedure TFmGraSrvVal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraSrvVal.InsAltItem(SQLType: TSQLType);
var
  Filial: Double;
begin
  if DBCheck.CriaFm(TFmGraSrvValIts, FmGraSrvValIts, afmoNegarComAviso) then
  begin
    FmGraSrvValIts.ImgTipo.SQLType := SQLType;
    FmGraSrvValIts.FQrIts          := QrGraSrvVal;
    if SQLType = stIns then
    begin
      //
    end else
    begin
      Filial := DModG.ObtemFilialDeEntidade(QrGraSrvValEmpresa.Value);
      FmGraSrvValIts.EdEmpresa.ValueVariant     := Filial;
      FmGraSrvValIts.EdControle.ValueVariant    := QrGraSrvValControle.Value;
      FmGraSrvValIts.EdClientSrv.ValueVariant   := QrGraSrvValClientSrv.Value;
      FmGraSrvValIts.EdReduzSrv.ValueVariant    := QrGraSrvValReduzSrv.Value;
      FmGraSrvValIts.EdGraGru1.ValueVariant     := QrGraSrvValGraGru1.Value;
      Geral.MB_Info(
        '/ arrumar  FmGraSrvValIts.EdGraTamIts.ValueVariant   := QrGraSrvValGraTamIts.Value;' +
        sLineBreak +
        'TDmProd.ObtemGraTamCadDeGraTamIts(GraTamIts: Integer): Integer;'
      );
      FmGraSrvValIts.EdGraTamIts.ValueVariant   := QrGraSrvValGraTamIts.Value;
      FmGraSrvValIts.EdGraCorCad.ValueVariant   := QrGraSrvValGraCorCad.Value;
      FmGraSrvValIts.EdPreco.ValueVariant       := QrGraSrvValPreco.Value;
      //
      FmGraSrvValIts.CkContinuar.Checked        := False;
    end;
    FmGraSrvValIts.ShowModal;
    FmGraSrvValIts.Destroy;
  end;
end;

procedure TFmGraSrvVal.QrGraSrvValAfterOpen(DataSet: TDataSet);
begin
  BtExclui.Enabled := QrGraSrvVal.RecordCount > 0;
end;

procedure TFmGraSrvVal.QrGraSrvValBeforeClose(DataSet: TDataSet);
begin
  BtExclui.Enabled := False;
end;

procedure TFmGraSrvVal.FechaPesquisa();
begin
  QrGraSrvVal.Close;
  DBGGraSrvVal.Visible := False;
end;

procedure TFmGraSrvVal.RealizaPesquisa();
var
  //Mostra: Boolean;
  Empresa, ClientSrv, ReduzSrv, GraGru1, GraTamIts, GraCorCad: Integer;
  SQL_Empresa, SQL_ClientSrv, SQL_ReduzSrv, SQL_GraGru1, SQL_GraTamIts,
  SQL_GraCorCad: String;
begin
  Empresa   := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
  ClientSrv := EdClientSrv.ValueVariant;
  ReduzSrv  := EdReduzSrv.ValueVariant;
  GraGru1   := EdGraGru1.ValueVariant;
  GraTamIts := EdGraTamIts.ValueVariant;
  GraCorCad := EdGraCorCad.ValueVariant;
  //
  QrGraSrvVal.Close;
(*
  Mostra := (Empresa <> 0) and (Cliente <> 0);
  DBGGraSrvVal.Visible := Mostra;
  if not Mostra then Exit;
*)
  //
  DBGGraSrvVal.Visible := True; //Mostra;
  SQL_Empresa   := '';
  SQL_ClientSrv := '';
  SQL_ReduzSrv  := '';
  SQL_GraGru1   := '';
  SQL_GraTamIts := '';
  SQL_GraCorCad := '';
  if CkEmpresa.Checked then
    SQL_Empresa  := 'AND gsv.Empresa=' + Geral.FF0(Empresa);
  if CkClientSrv.Checked then
    SQL_ClientSrv  := 'AND gsv.ClientSrv=' + Geral.FF0(ClientSrv);
  if CkReduzSrv.Checked then
    SQL_ReduzSrv  := 'AND gsv.ReduzSrv=' + Geral.FF0(ReduzSrv);
  if CkGraGru1.Checked then
    SQL_ReduzSrv  := 'AND gsv.GraGru1=' + Geral.FF0(GraGru1);
  if CkGraTamIts.Checked then
    SQL_GraTamIts  := 'AND gsv.GraTamIts=' + Geral.FF0(GraTamIts);
  if CkGraCorCad.Checked then
    SQL_GraCorCad  := 'AND gsv.GraCorCad=' + Geral.FF0(GraCorCad);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraSrvVal, Dmod.MyDB, [
  'SELECT gg1s.Nome NO_ReduzSrv, gg1p.UnidMed,  ',
  'gg1p.Referencia, gg1p.Nome NO_GG1,  ',
  'gtip.Nome NO_Tam, gccp.Nome NO_COR,  ',
  'unmp.Nome NO_UnidMed, gsv.*  ',
  'FROM grasrvval gsv ',
  'LEFT JOIN gragrux   ggxs ON ggxs.Controle=gsv.ReduzSrv ',
  'LEFT JOIN gragru1   gg1s ON ggxs.GraGru1=gg1s.Nivel1 ',
  'LEFT JOIN gragru1   gg1p ON gg1p.Nivel1=gsv.GraGru1 ',
  'LEFT JOIN gratamits gtip ON gtip.Controle=gsv.GraTamIts ',
  'LEFT JOIN gracorcad gccp ON gccp.Codigo=gsv.GraCorCad ',
  'LEFT JOIN unidmed   unmp ON unmp.Codigo=gg1p.UnidMed ',
  'WHERE gsv.Ativo<>0 ',
  SQL_Empresa,
  SQL_ClientSrv,
  SQL_ReduzSrv,
  SQL_GraGru1,
  SQL_GraTamIts,
  SQL_GraCorCad,
  '']);
end;

procedure TFmGraSrvVal.ReopenGraTamIts(GraTamCad: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraTamIts, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM gratamIts ',
  'WHERE Codigo=' + Geral.FF0(GraTamCad),
  'ORDER BY Controle ',
  '']);
end;

procedure TFmGraSrvVal.SBGradeClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormGraTamCad(EdGraTamCad.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdGraTamCad, CBGraGru1, QrGraGru1, VAR_CADASTRO);
end;

procedure TFmGraSrvVal.SbGraGru1Click(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  Grade_Jan.MostraFormGraGruN(EdGraGru1.ValueVariant);
  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdGraGru1, CBGraGru1, QrGraGru1, VAR_CADASTRO2, 'Nivel1');
end;

procedure TFmGraSrvVal.SbReduzSrvClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  Grade_Jan.MostraFormGraGruN(EdReduzSrv.ValueVariant);
  if VAR_CADASTRO2 <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdReduzSrv, CBReduzSrv, QrReduzSrv, VAR_CADASTRO2, 'Nivel1');
end;

procedure TFmGraSrvVal.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormGraCorCad(EdGraCorCad.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdGraCorCad, CBGraGru1, QrGraGru1, VAR_CADASTRO);
end;

end.
