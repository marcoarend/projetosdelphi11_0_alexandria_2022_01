object FmStqCenCab: TFmStqCenCab
  Left = 368
  Top = 194
  Caption = 'STQ-MANUA-004 :: Movimenta'#231#227'o Entre Centros'
  ClientHeight = 502
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 406
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 60
    ExplicitTop = 136
    object Splitter1: TSplitter
      Left = 573
      Top = 185
      Width = 5
      Height = 157
      ExplicitTop = 137
      ExplicitHeight = 154
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitLeft = 48
      ExplicitTop = -4
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 45
        Height = 13
        Caption = 'Entidade:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 62
        Height = 13
        Caption = 'Regra Fiscal:'
        FocusControl = DBEdit3
      end
      object Label21: TLabel
        Left = 16
        Top = 138
        Width = 80
        Height = 13
        Caption = 'Tabela de custo:'
        FocusControl = DBEdit14
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsStqCenCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 688
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsStqCenCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Entidade'
        DataSource = DsStqCenCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 72
        Width = 688
        Height = 21
        DataField = 'NO_ENT'
        DataSource = DsStqCenCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'FisRegCad'
        DataSource = DsStqCenCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 76
        Top = 112
        Width = 688
        Height = 21
        DataField = 'NO_FisRegCad'
        DataSource = DsStqCenCab
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 76
        Top = 154
        Width = 688
        Height = 21
        DataField = 'TabelaPrc_TXT'
        DataSource = DsStqCenCab
        TabOrder = 6
      end
      object DBEdit14: TDBEdit
        Left = 16
        Top = 154
        Width = 56
        Height = 21
        DataField = 'TabelaPrc'
        DataSource = DsStqCenCab
        TabOrder = 7
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 342
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 521
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 525
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 185
      Width = 573
      Height = 157
      Align = alLeft
      DataSource = DsStqCenIts
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / hora'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_GG1'
          Title.Caption = 'Descri'#231#227'o'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Qtde.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoAll'
          Title.Caption = 'Total $'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUSTOUNIT'
          Title.Caption = 'Unit'#225'rio $'
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 592
      Top = 200
      Width = 320
      Height = 120
      DataSource = DsCentros
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'TpTRANSF'
          Title.Caption = 'Fonte'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CENTRO'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 406
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 277
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label25: TLabel
        Left = 16
        Top = 56
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        ParentColor = False
      end
      object dmkLabel4: TdmkLabel
        Left = 16
        Top = 98
        Width = 245
        Height = 13
        Caption = 'Regra fiscal: (Aplica'#231#227'o deve ser de Transfer'#234'ncia):'
        UpdType = utYes
        SQLType = stNil
      end
      object SpeedButton5: TSpeedButton
        Left = 688
        Top = 114
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object dmkLabel5: TdmkLabel
        Left = 16
        Top = 139
        Width = 189
        Height = 13
        Caption = 'Lista de valores para custo de produtos:'
        UpdType = utYes
        SQLType = stNil
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Filial'
        UpdCampo = 'Filial'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 637
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        QryCampo = 'Filial'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFisRegCad: TdmkEditCB
        Left = 16
        Top = 114
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFisRegCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFisRegCad: TdmkDBLookupComboBox
        Left = 72
        Top = 114
        Width = 612
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 5
        dmkEditCB = EdFisRegCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTabelaPrc: TdmkEditCB
        Left = 16
        Top = 155
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TabelaPrc'
        UpdCampo = 'TabelaPrc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTabelaPrc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTabelaPrc: TdmkDBLookupComboBox
        Left = 72
        Top = 155
        Width = 637
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraCusPrc
        TabOrder = 7
        dmkEditCB = EdTabelaPrc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 343
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 353
        Height = 32
        Caption = 'Movimenta'#231#227'o Entre Centros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 353
        Height = 32
        Caption = 'Movimenta'#231#227'o Entre Centros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 353
        Height = 32
        Caption = 'Movimenta'#231#227'o Entre Centros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrStqCenCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrStqCenCabBeforeOpen
    AfterOpen = QrStqCenCabAfterOpen
    BeforeClose = QrStqCenCabBeforeClose
    AfterScroll = QrStqCenCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_cab'
      'WHERE Codigo > 0')
    Left = 180
    Top = 276
    object QrStqCenCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrStqCenCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrStqCenCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrStqCenCabNO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Size = 50
    end
    object QrStqCenCabFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrStqCenCabTabelaPrc_TXT: TWideStringField
      FieldName = 'TabelaPrc_TXT'
      Size = 50
    end
    object QrStqCenCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrStqCenCabFilial: TIntegerField
      FieldName = 'Filial'
    end
  end
  object DsStqCenCab: TDataSource
    DataSet = QrStqCenCab
    Left = 180
    Top = 324
  end
  object QrStqCenIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrStqCenItsBeforeClose
    AfterScroll = QrStqCenItsAfterScroll
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, smi.* '
      'FROM stqmovitsa smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE smi.Tipo=61'
      'AND smi.OriCodi=1'
      'AND smi.OriCnta=1')
    Left = 256
    Top = 276
    object QrStqCenItsNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrStqCenItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrStqCenItsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrStqCenItsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrStqCenItsOriCodi: TIntegerField
      FieldName = 'OriCodi'
    end
    object QrStqCenItsOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrStqCenItsOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrStqCenItsOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrStqCenItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqCenItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrStqCenItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqCenItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrStqCenItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrStqCenItsPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrStqCenItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrStqCenItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrStqCenItsFatorClas: TFloatField
      FieldName = 'FatorClas'
    end
    object QrStqCenItsQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
    end
    object QrStqCenItsRetorno: TSmallintField
      FieldName = 'Retorno'
    end
    object QrStqCenItsParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrStqCenItsParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrStqCenItsDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
    end
    object QrStqCenItsSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
    end
    object QrStqCenItsCustoAll: TFloatField
      FieldName = 'CustoAll'
    end
    object QrStqCenItsValorAll: TFloatField
      FieldName = 'ValorAll'
    end
    object QrStqCenItsGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
    object QrStqCenItsBaixa: TSmallintField
      FieldName = 'Baixa'
    end
    object QrStqCenItsAntQtde: TFloatField
      FieldName = 'AntQtde'
    end
    object QrStqCenItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrStqCenItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrStqCenItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrStqCenItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrStqCenItsValiStq: TIntegerField
      FieldName = 'ValiStq'
    end
    object QrStqCenItsCUSTOUNIT: TFloatField
      FieldName = 'CUSTOUNIT'
    end
  end
  object DsStqCenIts: TDataSource
    DataSet = QrStqCenIts
    Left = 256
    Top = 324
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 380
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.Nome'
      'FROM fisregmvt frm'
      'LEFT JOIN fisregcad frc ON frc.Codigo=frm.Codigo'
      'WHERE frc.Aplicacao=3'
      'AND frm.Empresa<>0')
    Left = 324
    Top = 276
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 324
    Top = 324
  end
  object QrCentros: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT smi.OriCnta,  smi.OriCtrl, smi.StqCencad,'
      'ELT(OriCnta, "Origem", "Destino") TpTRANSF, scc.Nome NO_CENTRO'
      'FROM stqmovitsa smi'
      'LEFT JOIN stqcencad scc ON scc.Codigo=smi.StqCenCad'
      'WHERE smi.Tipo=61'
      'AND smi.OriCodi=1'
      'AND smi.OriCtrl=133')
    Left = 404
    Top = 276
    object QrCentrosOriCnta: TIntegerField
      FieldName = 'OriCnta'
    end
    object QrCentrosOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrCentrosStqCencad: TIntegerField
      FieldName = 'StqCencad'
    end
    object QrCentrosTpTRANSF: TWideStringField
      FieldName = 'TpTRANSF'
      Size = 7
    end
    object QrCentrosNO_CENTRO: TWideStringField
      FieldName = 'NO_CENTRO'
      Size = 50
    end
  end
  object DsCentros: TDataSource
    DataSet = QrCentros
    Left = 404
    Top = 324
  end
  object QrLocOD: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 524
    Top = 188
    object QrLocODIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrLocODStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM gracusprc '
      'ORDER BY Nome ')
    Left = 556
    Top = 308
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 584
    Top = 308
  end
end
