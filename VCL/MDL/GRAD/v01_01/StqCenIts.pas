unit StqCenIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmStqCenIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXCtrlGGV: TIntegerField;
    QrGraGruXCustoPreco: TFloatField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    DsGraGruX: TDataSource;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label4: TLabel;
    QrStqCenCadSorc: TmySQLQuery;
    DsStqCenCadSorc: TDataSource;
    QrStqCenCadDest: TmySQLQuery;
    DsStqCenCadDest: TDataSource;
    Label1: TLabel;
    EdCencadSorc: TdmkEditCB;
    CBCencadSorc: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdCencadDest: TdmkEditCB;
    CBCencadDest: TdmkDBLookupComboBox;
    QrStqCenCadDestCodigo: TIntegerField;
    QrStqCenCadDestNome: TWideStringField;
    QrStqCenCadSorcCodigo: TIntegerField;
    QrStqCenCadSorcNome: TWideStringField;
    EdQtde: TdmkEdit;
    Label7: TLabel;
    EdIDCtrlSorc: TdmkEdit;
    Label8: TLabel;
    EdIDCtrlDest: TdmkEdit;
    Label9: TLabel;
    QrFRC: TmySQLQuery;
    QrFRCGraCusPrc: TIntegerField;
    EdPreco: TdmkEdit;
    Label6: TLabel;
    EdCustoAll: TdmkEdit;
    Label10: TLabel;
    DsFRC: TDataSource;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    QrFRCNO_GCP: TWideStringField;
    DBEdit2: TDBEdit;
    QrGraGruXFracio: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdCustoAllChange(Sender: TObject);
    procedure EdQtdeChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXExit(Sender: TObject);
    procedure EdGraGruXEnter(Sender: TObject);
    procedure EdCencadSorcEnter(Sender: TObject);
    procedure EdCencadSorcExit(Sender: TObject);
    procedure EdCencadSorcChange(Sender: TObject);
  private
    { Private declarations }
    FLastGraGruX, FLastCencadSorc: Integer;
    //
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure RedefineCusto();
    procedure CalculaCustoAll();
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FCodigo, FEmpresa, FFisRegCad, FTabelaPrc, FLastOriCtrl: Integer;
  end;

  var
  FmStqCenIts: TFmStqCenIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
ModuleGeral, ModProd;

{$R *.DFM}

procedure TFmStqCenIts.BtOKClick(Sender: TObject);
const
   EntraSai = -1;
   FatSemStq = -1;
   NaoExibeMsg = False;
var
  DataHora: String;
  //QuemUsou, Retorno, ParTipo, ParCodi, DebCtrl, SMIMultIns, GrupoBal, UnidMed, StqCenLoc, ValiStq
  IDCtrl, Tipo, OriCodi, OriCtrl, OriCnta, OriPart, Empresa, StqCenCad, Baixa,
  GraGruX: Integer;
  //ValorAll, AntQtde, Pecas, Peso, AreaM2, AreaP2,
  Qtde, FatorClas, CustoAll: Double;
  //
  Continua, Sorc, Dest: Integer;
  Msg: String;
  //
  function ExecSQL(): Boolean;
  begin
    Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
    'DataHora', 'Tipo', 'OriCodi',
    'OriCtrl', 'OriCnta', 'OriPart',
    'Empresa', 'StqCenCad', 'GraGruX',
    'Qtde', (*'Pecas', 'Peso',
    'AreaM2', 'AreaP2',*) 'FatorClas',
    (*'QuemUsou', 'Retorno', 'ParTipo',
    'ParCodi', 'DebCtrl', 'SMIMultIns',*)
    'CustoAll', (*'ValorAll', 'GrupoBal',*)
    'Baixa'(*, 'AntQtde', 'UnidMed',
    'StqCenLoc', 'ValiStq'*)], [
    'IDCtrl'], [
    DataHora, Tipo, OriCodi,
    OriCtrl, OriCnta, OriPart,
    Empresa, StqCenCad, GraGruX,
    Qtde, (*Pecas, Peso,
    AreaM2, AreaP2,*) FatorClas,
    (*QuemUsou, Retorno, ParTipo,
    ParCodi, DebCtrl, SMIMultIns,*)
    CustoAll, (*ValorAll, GrupoBal,*)
    Baixa(*, AntQtde, UnidMed,
    StqCenLoc, ValiStq*)], [
    IDCtrl], False);
  end;
begin
  Sorc := EdCencadSorc.ValueVariant;
  dest := EdCencadDest.ValueVariant;
  //
  DataHora       := DModG.ObtemAgoraTxt();
  Tipo           := VAR_FATID_0061;
  OriCodi        := FCodigo;
  OriPart        := 0;
  Empresa        := FEmpresa;
  GraGruX        := EdGraGruX.ValueVariant;
  FatorClas      := 1;
  Qtde           := EdQtde.ValueVariant;
  CustoAll       := EdCustoAll.ValueVariant;
  (*Pecas          := ;
  Peso           := ;
  AreaM2         := ;
  AreaP2         := ;
  QuemUsou       := ;
  Retorno        := ;
  ParTipo        := ;
  ParCodi        := ;
  DebCtrl        := ;
  SMIMultIns     := ;
  ValorAll       := ;
  GrupoBal       := ;
  AntQtde        := ;
  UnidMed        := ;
  StqCenLoc      := ;
  ValiStq        := ;*)
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o produto!') then
    Exit;
  if MyObjects.FIC(Sorc = 0, EdCencadSorc, 'Informe o centro de origem!') then
    Exit;
  if MyObjects.FIC(Dest = 0, EdCencadDest, 'Informe o centro de destino') then
    Exit;
  if MyObjects.FIC(Sorc = Dest, nil, 'Centros de origem e destino n�o podem ser iguais!') then
    Exit;
  if MyObjects.FIC(Qtde = 0, EdQtde, 'Informe a quantidade!') then
    Exit;
  //
  OriCnta  := 1;
  Msg      := '';
  Continua := DmProd.VerificaEstoqueProduto(Qtde, EntraSai, GraGruX, Sorc,
                FFisRegCad, OriCodi, OriCnta, Empresa, FatSemStq, NaoExibeMsg, Msg, 2);
  //
  if Continua = ID_YES then
  begin
    // Baixa na Origem
    IDCtrl         := UMyMod.Busca_IDCtrl_NFe(ImgTipo.SQLType, EdIDCtrlSorc.ValueVariant);
    OriCtrl        := IDCtrl;
    StqCenCad      := EdCencadSorc.ValueVariant;
    OriCnta        := 1;
    Baixa          := -1;
    ExecSQL();
    // Entra no destino
    IDCtrl         := UMyMod.Busca_IDCtrl_NFe(ImgTipo.SQLType, EdIDCtrlDest.ValueVariant);
    //OriCtrl        := IDCtrl;  o mesmo da origem!
    StqCenCad      := EdCencadDest.ValueVariant;
    OriCnta        := 2;
    Baixa          := 1;
    ExecSQL();
    //
    FLastOriCtrl := OriCtrl;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Item inclu�do com sucesso!');
      //
      EdGraGruX.ValueVariant := 0;
      CBGraGruX.KeyValue := Null;
      //
      EdGraGruX.SetFocus;
    end;
  end;
end;

procedure TFmStqCenIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqCenIts.CalculaCustoAll();
begin
  EdCustoAll.ValueVariant := EdQtde.ValueVariant * EdPreco.ValueVariant;
end;

procedure TFmStqCenIts.EdCencadSorcChange(Sender: TObject);
begin
  if not EdCencadSorc.Focused then
    RedefineCusto();
end;

procedure TFmStqCenIts.EdCencadSorcEnter(Sender: TObject);
begin
  FLastCencadSorc := EdCencadSorc.ValueVariant;
end;

procedure TFmStqCenIts.EdCencadSorcExit(Sender: TObject);
begin
  if EdCencadSorc.ValueVariant <> FLastCencadSorc then
    RedefineCusto();
end;

procedure TFmStqCenIts.EdCustoAllChange(Sender: TObject);
var
  Qtde: Double;
begin
  Qtde := EdQtde.ValueVariant;
  //
  if EdCustoAll.Focused and (Qtde > 0) then
    EdPreco.ValueVariant := EdCustoAll.ValueVariant / Qtde;
end;

procedure TFmStqCenIts.EdGraGruXChange(Sender: TObject);
begin
  if not EdGraGruX.Focused then
    RedefineCusto();
  //
  EdQtde.DecimalSize := QrGraGruXFracio.Value;
end;

procedure TFmStqCenIts.EdGraGruXEnter(Sender: TObject);
begin
  FLastGraGruX := EdGraGruX.ValueVariant;
end;

procedure TFmStqCenIts.EdGraGruXExit(Sender: TObject);
begin
  if EdGraGruX.ValueVariant <> FLastGraGruX then
    RedefineCusto();
end;

procedure TFmStqCenIts.EdPrecoChange(Sender: TObject);
begin
  if EdCustoAll.Focused then
    Exit;
  CalculaCustoAll();
end;

procedure TFmStqCenIts.EdQtdeChange(Sender: TObject);
begin
  CalculaCustoAll();
end;

procedure TFmStqCenIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmStqCenIts.FormCreate(Sender: TObject);
var
  CasasProd: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenGraGruX;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCadSorc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCadDest, Dmod.MyDB);
  //
  CasasProd := DmodG.QrControle.FieldByName('CasasProd').AsInteger;
  //
  EdPreco.DecimalSize    := CasasProd;
  EdCustoAll.DecimalSize := CasasProd;
end;

procedure TFmStqCenIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCenIts.RedefineCusto();
var
  StqCenCad, GraGruX: Integer;
  CustoPreco: Double;
begin
  StqCenCad := EdCencadSorc.ValueVariant;
  GraGruX   := EdGraGruX.ValueVariant;
  //
  if (StqCenCad <> 0) and (GraGruX <> 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrFRC, Dmod.MyDB, [
      'SELECT gcp.Codigo GraCusPrc, gcp.Nome NO_GCP ',
      'FROM gracusprc gcp ',
      'WHERE gcp.Codigo=' + Geral.FF0(FTabelaPrc),
      '']);
    //
    CustoPreco := DmProd.ObtemPrecoDeCusto(FEmpresa, GraGruX, QrFRCGraCusPrc.Value);
    //
    EdPreco.ValueVariant := CustoPreco;
    //
    if CustoPreco < 0.000001 then
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Pre�o n�o definido na tabela de custo definida na movimenta��o da regra fiscal selecionada!')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'O pre�o de custo � oriundo da tabela conforme preenchido na janela anterior.');
  end;
end;

procedure TFmStqCenIts.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmStqCenIts.ReopenGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle GraGruX, ggv.Controle CtrlGGV, ',
    'ggv.CustoPreco, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, pgt.Fracio ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN prdgruptip pgt ON gg1.PrdGrupTip=pgt.Codigo ',
    'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle ',
    '  AND ggv.GraCusPrc=' + Geral.FF0(FTabelaPrc),
    '  AND ggv.Entidade=' + Geral.FF0(FEmpresa),
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
end;

procedure TFmStqCenIts.SpeedButton1Click(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
}
end;

end.
