unit GraCusPrc;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, dmkDBGrid,
  dmkImage, Vcl.ComCtrls, dmkDBGridZTO, UnDmkEnums, UnProjGroup_Consts;

type
  TFmGraCusPrc = class(TForm)
    PainelDados: TPanel;
    DsGraCusPrc: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    PainelEdi2: TPanel;
    RGCustoPreco: TdmkRadioGroup;
    RGTipoCalc: TdmkRadioGroup;
    EdMoeda: TdmkEditCB;
    CBMoeda: TdmkDBLookupComboBox;
    Label4: TLabel;
    Painel2: TPanel;
    QrGraCusPrcCustoPreco: TSmallintField;
    QrGraCusPrcTipoCalc: TSmallintField;
    QrGraCusPrcMoeda: TIntegerField;
    QrGraCusPrcNOMEMOEDA: TWideStringField;
    Panel4: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    dmkRadioGroup1: TDBRadioGroup;
    dmkRadioGroup2: TDBRadioGroup;
    DBEdit2: TDBEdit;
    QrCambioMda: TmySQLQuery;
    DsCambioMda: TDataSource;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodusu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    QrGraCusPrcSIGLAMDA: TWideStringField;
    DBEdit3: TDBEdit;
    QrGraCusPrcU: TmySQLQuery;
    DsGraCusPrcU: TDataSource;
    PMLista: TPopupMenu;
    Incluinovalista1: TMenuItem;
    Alteralistaatual1: TMenuItem;
    Excluilistaatual1: TMenuItem;
    PMUsuario: TPopupMenu;
    Adicionausurio1: TMenuItem;
    Retirausurioatual1: TMenuItem;
    QrGraCusPrcUControle: TIntegerField;
    QrGraCusPrcUUsuario: TIntegerField;
    QrGraCusPrcULogin: TWideStringField;
    QrGraCusPrcUFuncionario: TIntegerField;
    QrGraCusPrcUNOMEUSU: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLista: TBitBtn;
    BtUsuario: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGFunci: TdmkDBGrid;
    Panel1: TPanel;
    CBEmpresa: TdmkDBLookupComboBox;
    LaCliInt: TLabel;
    EdEmpresa: TdmkEditCB;
    DBGCusto: TdmkDBGridZTO;
    QrGraGruVal: TmySQLQuery;
    DsGraGruVal: TDataSource;
    QrGraGruValGraGru1: TIntegerField;
    QrGraGruValGraGruX: TIntegerField;
    QrGraGruValCtrlGGV: TIntegerField;
    QrGraGruValCustoPreco: TFloatField;
    QrGraGruValNO_PRD_TAM_COR: TWideStringField;
    BtCusto: TBitBtn;
    PMCusto: TPopupMenu;
    EdFiltro: TEdit;
    RGFiltro: TRadioGroup;
    PB1: TProgressBar;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    QrGraGruValGraCusPrc: TIntegerField;
    QrGraGruValDataIni: TDateField;
    QrGraGruValEntidade: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraCusPrcAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraCusPrcBeforeOpen(DataSet: TDataSet);
    procedure BtListaClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrGraCusPrcBeforeClose(DataSet: TDataSet);
    procedure QrGraCusPrcAfterScroll(DataSet: TDataSet);
    procedure Incluinovalista1Click(Sender: TObject);
    procedure Alteralistaatual1Click(Sender: TObject);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Adicionausurio1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtCustoClick(Sender: TObject);
    procedure DBGCustoDblClick(Sender: TObject);
    procedure EdFiltroChange(Sender: TObject);
    procedure PMListaPopup(Sender: TObject);
    procedure PMUsuarioPopup(Sender: TObject);
    procedure Retirausurioatual1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure InsAltSohPreco();
    procedure InsAltPrecoEData(SQLType: TSQLType);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenGraCusPrcU(Controle: Integer);
    procedure ReopenGraGruVal(CtrlGGV: Integer);
  end;

var
  FmGraCusPrc: TFmGraCusPrc;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, GraCusPrcU, MyDBCheck, ModuleGeral, DmkDAC_PF,
  ModProd, MyListas, GraGruValDta;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraCusPrc.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraCusPrc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraCusPrcCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraCusPrc.DefParams;
begin
  VAR_GOTOTABELA := 'GraCusPrc';
  VAR_GOTOMYSQLTABLE := QrGraCusPrc;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT gcp.Codigo, gcp.CodUsu, gcp.Nome,');
  VAR_SQLx.Add('gcp.CustoPreco, gcp.TipoCalc, gcp.Moeda,');
{$IfNDef SemCotacoes}
  VAR_SQLx.Add('mda.Nome NOMEMOEDA, mda.Sigla SIGLAMDA');
{$Else}
  VAR_SQLx.Add('"" NOMEMOEDA, "" SIGLAMDA');
{$EndIf}
  VAR_SQLx.Add('FROM gracusprc gcp');
{$IfNDef SemCotacoes}
  VAR_SQLx.Add('LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda');
{$EndIf}
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE gcp.Codigo > -1000');
  //
  VAR_SQL1.Add('AND gcp.Codigo=:P0');
  //
  VAR_SQL2.Add('AND gcp.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND gcp.Nome Like :P0');
  //
end;

procedure TFmGraCusPrc.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraCusPrc', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmGraCusPrc.EdEmpresaChange(Sender: TObject);
begin
  ReopenGraGruVal(0);
end;

procedure TFmGraCusPrc.EdFiltroChange(Sender: TObject);
begin
  ReopenGraGruVal(0);
end;

procedure TFmGraCusPrc.Exclui1Click(Sender: TObject);
begin
// Parei aqui
end;

procedure TFmGraCusPrc.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraCusPrc.PMListaPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrGraCusPrc.State <> dsInactive) and (QrGraCusPrc.RecordCount > 0);
  //
  Alteralistaatual1.Enabled := Enab;
end;

procedure TFmGraCusPrc.PMUsuarioPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrGraCusPrc.State <> dsInactive) and (QrGraCusPrc.RecordCount > 0);
  Enab2 := (QrGraCusPrcU.State <> dsInactive) and (QrGraCusPrcU.RecordCount > 0);
  //
  Adicionausurio1.Enabled    := Enab1;
  Retirausurioatual1.Enabled := Enab1 and Enab2;
end;

procedure TFmGraCusPrc.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmGraCusPrc.AlteraRegistro;
var
  GraCusPrc : Integer;
begin
  GraCusPrc := QrGraCusPrcCodigo.Value;
  if QrGraCusPrcCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(GraCusPrc, Dmod.MyDB, 'GraCusPrc', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GraCusPrc, Dmod.MyDB, 'GraCusPrc', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGraCusPrc.IncluiRegistro;
var
  Cursor : TCursor;
  GraCusPrc : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    GraCusPrc := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GraCusPrc', 'GraCusPrc', 'Codigo');
    if Length(FormatFloat(FFormatFloat, GraCusPrc))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, GraCusPrc);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmGraCusPrc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraCusPrc.DBGCustoDblClick(Sender: TObject);
begin
  if BtCusto.Enabled then
    BtCustoClick(Self);
end;

procedure TFmGraCusPrc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraCusPrc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraCusPrc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraCusPrc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraCusPrc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraCusPrc.Adicionausurio1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraCusPrcU, FmGraCusPrcU, afmoNegarComAviso) then
  begin
    FmGraCusPrcU.ShowModal;
    FmGraCusPrcU.Destroy;
  end;
end;

procedure TFmGraCusPrc.Altera1Click(Sender: TObject);
begin
  InsAltPrecoEData(stUpd);
end;

procedure TFmGraCusPrc.Alteralistaatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraCusPrc, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'gracusprc');
end;

procedure TFmGraCusPrc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraCusPrcCodigo.Value;
  Close;
end;

procedure TFmGraCusPrc.BtUsuarioClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmGraCusPrc.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('GraCusPrc', 'Codigo', ImgTipo.SQLType,
    QrGraCusPrcCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraCusPrc, PainelEdita,
    'GraCusPrc', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraCusPrc.BtCustoClick(Sender: TObject);
begin
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
    MyObjects.MostraPopUpDeBotao(PMCusto, BtCusto)
  else
    InsAltSohPreco();
end;

procedure TFmGraCusPrc.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraCusPrc', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraCusPrc', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraCusPrc', 'Codigo');
end;

procedure TFmGraCusPrc.BtListaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLista, BtLista);
end;

procedure TFmGraCusPrc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource   := DModG.DsEmpresas;
  EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
  //
  PainelEdi2.Align  := alClient;
  //Panel4.Align      := alClient;
  //DBGFunci.Align    := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
{$IfNDef SemCotacoes}
  UnDmkDAC_PF.AbreQuery(QrCambioMda, Dmod.MyDB);
{$EndIf}
  //
  PB1.Visible := False;
end;

procedure TFmGraCusPrc.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraCusPrcCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraCusPrc.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmGraCusPrc.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraCusPrc.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraCusPrcCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraCusPrc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraCusPrc.QrGraCusPrcAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraCusPrc.QrGraCusPrcAfterScroll(DataSet: TDataSet);
begin
  ReopenGraCusPrcU(0);
  ReopenGraGruVal(0);
  // Evitar mudar listas de Pr~�o M�dio e �ltima compra (4 e 5)
  BtLista.Enabled := not (QrGraCusPrcCodigo.Value in ([4,5]));
end;

procedure TFmGraCusPrc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraCusPrc.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraCusPrcCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GraCusPrc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraCusPrc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraCusPrc.Inclui1Click(Sender: TObject);
begin
  InsAltPrecoEData(stIns);
end;

procedure TFmGraCusPrc.Incluinovalista1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrGraCusPrc, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'gracusprc');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraCusPrc', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
end;

procedure TFmGraCusPrc.InsAltPrecoEData(SQLType: TSQLType);
var
  Entidade: Integer;
begin
  Entidade := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Entidade = 0, EdEmpresa, 'Informe o cliente interno!') then Exit;
  Entidade := DModG.QrEmpresasCodigo.Value;
  //
  if DBCheck.CriaFm(TFmGraGruValDta, FmGraGruValDta, afmoNegarComAviso) then
  begin
    FmGraGruValDta.ImgTipo.SQLType := SQLType;
    //
    FmGraGruValDta.FControle   := QrGraGruValCtrlGGV.Value;
    FmGraGruValDta.FGraCusPrc  := QrGraCusPrcCodigo.Value;
    FmGraGruValDta.FGraGruX    := QrGraGruValGraGruX.Value;
    FmGraGruValDta.FGraGru1    := QrGraGruValGraGru1.Value;
    FmGraGruValDta.FEntidade   := Entidade;
    //
    if SQLType = stUpd then
    begin
      FmGraGruValDta.FEntidade   := QrGraGruValEntidade.Value;
      FmGraGruValDta.EdCustoPreco.ValueVariant := QrGraGruValCustoPreco.Value;
      FmGraGruValDta.TPDataIni.Date            := QrGraGruValDataIni.Value;
    end;
    //
    FmGraGruValDta.ShowModal;
    FmGraGruValDta.Destroy;
  end;
end;

procedure TFmGraCusPrc.InsAltSohPreco();
  procedure AtualizaPrecos(CustoPreco: Double);
  var
    GraCusPrc, GraGruX, GraGru1, Controle, Entidade: Integer;
    SQLType: TSQLType;
  begin
    Controle := QrGraGruValCtrlGGV.Value;
    Entidade := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
    //
    if Controle <> 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    //
    Controle  := UMyMod.BuscaEmLivreY_Def('GraGruVal', 'Controle', SQLType, Controle);
    GraGruX   := QrGraGruValGraGruX.Value;
    GraGru1   := QrGraGruValGraGru1.Value;
    GraCusPrc := QrGraCusPrcCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
      'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'], [
      GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [Controle], True) then
    ReopenGraGruVal(Controle);
  end;
var
  i: Integer;
  CustoPreco: Double;
begin
  PageControl1.ActivePageIndex := 1;
  //
  if (QrGraGruVal.State = dsInactive) or (QrGraGruVal.RecordCount = 0) then Exit;
  //
  CustoPreco := QrGraGruValCustoPreco.Value;
  //
  if DBGCusto.SelectedRows.Count > 1 then
  begin
    if DmProd.ObtemPreco(CustoPreco) then
    begin
      try
        Screen.Cursor    := crHourGlass;
        DBGCusto.Enabled := False;
        PB1.Position     := 0;
        PB1.Max          := DBGCusto.SelectedRows.Count;
        PB1.Visible      := True;
        //
        QrGraGruVal.DisableControls;
        //
        with DBGCusto.DataSource.DataSet do
        for i:= 0 to DBGCusto.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBGCusto.SelectedRows.Items[i]));
          GotoBookmark(DBGCusto.SelectedRows.Items[i]);
          //
          AtualizaPrecos(CustoPreco);
          //
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
        end;
      finally
        QrGraGruVal.EnableControls;
        //
        PB1.Visible      := False;
        DBGCusto.Enabled := True;
        Screen.Cursor    := crDefault;
      end;
    end;
  end else
  begin
    if DmProd.ObtemPreco(CustoPreco) then
      AtualizaPrecos(CustoPreco);
  end;
end;

procedure TFmGraCusPrc.QrGraCusPrcBeforeClose(DataSet: TDataSet);
begin
  QrGraCusPrcU.Close;
  ReopenGraGruVal(0);
  BtLista.Enabled := False;
end;

procedure TFmGraCusPrc.QrGraCusPrcBeforeOpen(DataSet: TDataSet);
begin
  QrGraCusPrcCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGraCusPrc.ReopenGraCusPrcU(Controle: Integer);
begin
  QrGraCusPrcU.Close;
  QrGraCusPrcU.Params[0].AsInteger := QrGraCusPrcCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraCusPrcU, Dmod.MyDB);
  //
  QrGraCusPrcU.Locate('Controle', Controle, []);
end;

procedure TFmGraCusPrc.ReopenGraGruVal(CtrlGGV: Integer);
var
  Entidade: Integer;
  SQL_Filtro: String;
begin
  QrGraGruVal.Close;
  //
  Entidade := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
  if (QrGraCusPrc.State <> dsInactive)
  and (QrGraCusPrc.RecordCount > 0)
  and (Entidade <> 0) then
  begin
    if Trim(EdFiltro.Text) <> '' then
    begin
      case RGFiltro.ItemIndex of
          1: SQL_Filtro := 'WHERE gg1.Nivel1=' + EdFiltro.Text;
          2: SQL_Filtro := 'WHERE ggx.Controle=' + EdFiltro.Text;
        else SQL_Filtro := 'WHERE gg1.Nome LIKE "%' + EdFiltro.Text + '%"';
      end;
    end else
      SQL_Filtro := '';
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle GraGruX,  ',
    'ggv.Controle CtrlGGV, ggv.CustoPreco,  ',
    'ggv.GraCusPrc, Entidade, DataIni, ',
    'CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR  ',
    'FROM gragrux ggx  ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle',
    '  AND ggv.GraCusPrc=' + Geral.FF0(QrGraCusPrcCodigo.Value),
    '  AND ggv.Entidade=' + Geral.FF0(Entidade),
    SQL_Filtro,
    'AND ggx.Ativo = 1 ',
    'ORDER BY NO_PRD_TAM_COR, DataIni, ggx.Controle ',
    '']);
    //Geral.MB_SQL(Self, QrGraGruVal);
    //
(*
SELECT GraGruX, GraCusPrc,
Entidade, MAX(DataIni) DataIni
FROM GraGruVal
WHERE DataIni < "2016-10-04"
GROUP BY GraGruX, GraCusPrc, Entidade
*)
    if CtrlGGV <> 0 then
      QrGraGruVal.Locate('CtrlGGV', CtrlGGV, []);
    //
  end;
  DBGCusto.Visible := QrGraGruVal.State <> dsInactive;
  BtCusto.Enabled := DBGCusto.Visible and (QrGraGruVal.RecordCount > 0);
end;

procedure TFmGraCusPrc.Retirausurioatual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrGraCusPrcUControle.Value;
  //
  if UMyMod.ExcluiRegistroInt1('', 'gracusprcu', 'Controle', Controle, Dmod.MyDB) <> 0 then
    ReopenGraCusPrcU(0);
end;

end.

