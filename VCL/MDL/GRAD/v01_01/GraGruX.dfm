object FmGraGruX: TFmGraGruX
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-019 :: Cadastro de Reduzido'
  ClientHeight = 322
  ClientWidth = 480
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 480
    Height = 166
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 155
    object DBText1: TDBText
      Left = 0
      Top = 0
      Width = 480
      Height = 17
      Align = alTop
      Alignment = taCenter
      DataField = 'NO_PRD_TAM_COR'
      DataSource = DsGraGruX
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ExplicitTop = 4
    end
    object Panel3: TPanel
      Left = 0
      Top = 17
      Width = 480
      Height = 149
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 18
      ExplicitWidth = 478
      ExplicitHeight = 136
      object Label1: TLabel
        Left = 12
        Top = 8
        Width = 40
        Height = 13
        Caption = 'Produto:'
        Enabled = False
      end
      object Label2: TLabel
        Left = 12
        Top = 48
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label3: TLabel
        Left = 12
        Top = 88
        Width = 19
        Height = 13
        Caption = 'Cor:'
      end
      object EdGraGru1: TdmkEditCB
        Left = 12
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 68
        Top = 24
        Width = 400
        Height = 21
        Enabled = False
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraGru1
        TabOrder = 1
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraTamCad: TdmkEditCB
        Left = 12
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraTamCad
        IgnoraDBLookupComboBox = False
      end
      object CBGraTamCad: TdmkDBLookupComboBox
        Left = 68
        Top = 64
        Width = 400
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraTamCad
        TabOrder = 3
        dmkEditCB = EdGraTamCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraCorCad: TdmkEditCB
        Left = 12
        Top = 104
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraCorCad
        IgnoraDBLookupComboBox = False
      end
      object CBGraCorCad: TdmkDBLookupComboBox
        Left = 68
        Top = 104
        Width = 400
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraCorCad
        TabOrder = 5
        dmkEditCB = EdGraCorCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 480
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -155
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 432
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 384
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 269
        Height = 32
        Caption = 'Cadastro de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 269
        Height = 32
        Caption = 'Cadastro de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 269
        Height = 32
        Caption = 'Cadastro de Reduzido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 214
    Width = 480
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -155
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 258
    Width = 480
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -155
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 332
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.*, gg1.Nome NO_PRD, gti.Nome NO_TAM,'
      'gcc.Nome NO_COR, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle=:P0')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 32
    Top = 4
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 60
    Top = 4
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 88
    Top = 4
  end
  object QrGraCorCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gratamcad'
      'ORDER BY Nome')
    Left = 116
    Top = 4
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCorCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraCorCad: TDataSource
    DataSet = QrGraCorCad
    Left = 144
    Top = 4
  end
  object QrGraTamCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gracorcad'
      'ORDER BY Nome')
    Left = 172
    Top = 4
    object QrGraTamCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraTamCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraTamCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraTamCad: TDataSource
    DataSet = QrGraTamCad
    Left = 200
    Top = 4
  end
end
