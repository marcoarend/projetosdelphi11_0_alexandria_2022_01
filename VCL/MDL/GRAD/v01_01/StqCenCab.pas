unit StqCenCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, dmkDBGridZTO;

type
  TFmStqCenCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrStqCenCab: TmySQLQuery;
    DsStqCenCab: TDataSource;
    QrStqCenIts: TmySQLQuery;
    DsStqCenIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrStqCenCabCodigo: TIntegerField;
    QrStqCenCabNome: TWideStringField;
    Label25: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrStqCenCabEntidade: TIntegerField;
    QrStqCenCabNO_ENT: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrFisRegCad: TmySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    DsFisRegCad: TDataSource;
    dmkLabel4: TdmkLabel;
    EdFisRegCad: TdmkEditCB;
    CBFisRegCad: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrStqCenCabNO_FisRegCad: TWideStringField;
    QrStqCenCabFisRegCad: TIntegerField;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrStqCenItsNO_GG1: TWideStringField;
    QrStqCenItsDataHora: TDateTimeField;
    QrStqCenItsIDCtrl: TIntegerField;
    QrStqCenItsTipo: TIntegerField;
    QrStqCenItsOriCodi: TIntegerField;
    QrStqCenItsOriCtrl: TIntegerField;
    QrStqCenItsOriCnta: TIntegerField;
    QrStqCenItsOriPart: TIntegerField;
    QrStqCenItsEmpresa: TIntegerField;
    QrStqCenItsStqCenCad: TIntegerField;
    QrStqCenItsGraGruX: TIntegerField;
    QrStqCenItsQtde: TFloatField;
    QrStqCenItsPecas: TFloatField;
    QrStqCenItsPeso: TFloatField;
    QrStqCenItsAreaM2: TFloatField;
    QrStqCenItsAreaP2: TFloatField;
    QrStqCenItsFatorClas: TFloatField;
    QrStqCenItsQuemUsou: TIntegerField;
    QrStqCenItsRetorno: TSmallintField;
    QrStqCenItsParTipo: TIntegerField;
    QrStqCenItsParCodi: TIntegerField;
    QrStqCenItsDebCtrl: TIntegerField;
    QrStqCenItsSMIMultIns: TIntegerField;
    QrStqCenItsCustoAll: TFloatField;
    QrStqCenItsValorAll: TFloatField;
    QrStqCenItsGrupoBal: TIntegerField;
    QrStqCenItsBaixa: TSmallintField;
    QrStqCenItsAntQtde: TFloatField;
    QrStqCenItsUnidMed: TIntegerField;
    QrStqCenItsAlterWeb: TSmallintField;
    QrStqCenItsAtivo: TSmallintField;
    QrStqCenItsStqCenLoc: TIntegerField;
    QrStqCenItsValiStq: TIntegerField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrStqCenItsCUSTOUNIT: TFloatField;
    DBGrid1: TDBGrid;
    QrCentros: TmySQLQuery;
    DsCentros: TDataSource;
    QrCentrosOriCnta: TIntegerField;
    QrCentrosOriCtrl: TIntegerField;
    QrCentrosStqCencad: TIntegerField;
    QrCentrosTpTRANSF: TWideStringField;
    QrCentrosNO_CENTRO: TWideStringField;
    QrLocOD: TmySQLQuery;
    QrLocODIDCtrl: TIntegerField;
    QrLocODStqCenCad: TIntegerField;
    Splitter1: TSplitter;
    EdTabelaPrc: TdmkEditCB;
    CBTabelaPrc: TdmkDBLookupComboBox;
    dmkLabel5: TdmkLabel;
    QrStqCenCabTabelaPrc: TIntegerField;
    DBEdit15: TDBEdit;
    DBEdit14: TDBEdit;
    Label21: TLabel;
    QrStqCenCabTabelaPrc_TXT: TWideStringField;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    QrStqCenCabFilial: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqCenCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqCenCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrStqCenCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrStqCenCabBeforeClose(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrStqCenItsBeforeClose(DataSet: TDataSet);
    procedure QrStqCenItsAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormStqCenIts(SQLType: TSQLType);
    procedure ReopenCentros(OriCnta: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenStqCenIts(OriCtrl: Integer);
  end;

var
  FmStqCenCab: TFmStqCenCab;
const
  FFormatFloat = '00000';
  CO_SMIA_VAR_FATID_0061_OriCnta_Sorc = 1;
  CO_SMIA_VAR_FATID_0061_OriCnta_Dest = 2;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, StqCenIts, ModuleGeral,
  UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqCenCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqCenCab.MostraFormStqCenIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmStqCenIts, FmStqCenIts, afmoNegarComAviso) then
  begin
    FmStqCenIts.ImgTipo.SQLType := SQLType;
    FmStqCenIts.FQrCab     := QrStqCenCab;
    FmStqCenIts.FDsCab     := DsStqCenCab;
    FmStqCenIts.FQrIts     := QrStqCenIts;
    FmStqCenIts.FCodigo    := QrStqCenCabCodigo.Value;
    FmStqCenIts.FEmpresa   := QrStqCenCabEntidade.Value;
    FmStqCenIts.FFisRegCad := QrStqCenCabFisRegCad.Value;
    FmStqCenIts.FTabelaPrc := QrStqCenCabTabelaPrc.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmStqCenIts.EdGraGruX.ValueVariant    := QrStqCenItsGraGruX.Value;
      FmStqCenIts.CBGraGruX.KeyValue        := QrStqCenItsGraGruX.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLocOD, Dmod.MyDB, [
        'SELECT smi.IDCtrl, smi.StqCenCad ',
        'FROM stqmovitsa smi ',
        'WHERE smi.Tipo=' + Geral.FF0(VAR_FATID_0061),
        'AND smi.OriCodi=' + Geral.FF0(QrStqCenCabCodigo.Value),
        'AND smi.OriCtrl=' + Geral.FF0(QrStqCenItsOriCtrl.Value),
        'AND OriCnta=' + Geral.FF0(CO_SMIA_VAR_FATID_0061_OriCnta_Sorc), // Cuidado!! Aqui Muda
        '']);
      FmStqCenIts.EdIDCtrlSorc.ValueVariant := QrLocODIDCtrl.Value;
      FmStqCenIts.EdCenCadSorc.ValueVariant := QrLocODStqCenCad.Value;
      FmStqCenIts.CBCenCadSorc.KeyValue     := QrLocODStqCenCad.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLocOD, Dmod.MyDB, [
        'SELECT smi.IDCtrl, smi.StqCenCad ',
        'FROM stqmovitsa smi ',
        'WHERE smi.Tipo=' + Geral.FF0(VAR_FATID_0061),
        'AND smi.OriCodi=' + Geral.FF0(QrStqCenCabCodigo.Value),
        'AND smi.OriCtrl=' + Geral.FF0(QrStqCenItsOriCtrl.Value),
        'AND OriCnta=' + Geral.FF0(CO_SMIA_VAR_FATID_0061_OriCnta_Dest), // Cuidado!! Aqui Muda
        '']);
      FmStqCenIts.EdIDCtrlDest.ValueVariant := QrLocODIDCtrl.Value;
      FmStqCenIts.EdCenCadDest.ValueVariant := QrLocODStqCenCad.Value;
      FmStqCenIts.CBCenCadDest.KeyValue     := QrLocODStqCenCad.Value;
      //
      FmStqCenIts.EdQtde.ValueVariant       := QrStqCenItsQtde.Value;
      FmStqCenIts.EdPreco.ValueVariant      := QrStqCenItsCUSTOUNIT.Value; // Inocuo!
      // Deve ser o ultimo!
      FmStqCenIts.EdCustoAll.ValueVariant   := QrStqCenItsCustoAll.Value;
    end;
    FmStqCenIts.ShowModal;
    ReopenStqCenIts(FmStqCenIts.FLastOriCtrl);
    FmStqCenIts.Destroy;
  end;
end;

procedure TFmStqCenCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrStqCenCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrStqCenCab, QrStqCenIts);
end;

procedure TFmStqCenCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrStqCenCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrStqCenIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrStqCenIts);
end;

procedure TFmStqCenCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqCenCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqCenCab.DefParams;
begin
  VAR_GOTOTABELA := 'stqcencab';
  VAR_GOTOMYSQLTABLE := QrStqCenCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT scc.*, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,');
  VAR_SQLx.Add('frc.Nome NO_FisRegCad, cli.CodFilial Filial, ');
{$IfNDef NAO_GFAT}
  VAR_SQLx.Add('prc.Nome TabelaPrc_TXT ');
{$Else}
  VAR_SQLx.Add('"" TabelaPrc_TXT');
{$EndIf}
  VAR_SQLx.Add('FROM stqcencab scc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=scc.Entidade');
  VAR_SQLx.Add('LEFT JOIN fisregcad frc ON frc.Codigo=scc.FisRegCad');
  VAR_SQLx.Add('LEFT JOIN enticliint cli ON cli.CodEnti=scc.Entidade');
{$IfNDef NAO_GFAT}
  VAR_SQLx.Add('LEFT JOIN gracusprc prc ON prc.Codigo=scc.TabelaPrc');
{$EndIf}
  VAR_SQLx.Add('WHERE scc.Codigo > 0');

  //
  VAR_SQL1.Add('AND scc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND scc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND scc.Nome Like :P0');
  //
end;

procedure TFmStqCenCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormStqCenIts(stUpd);
end;

procedure TFmStqCenCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmStqCenCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqCenCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqCenCab.ItsExclui1Click(Sender: TObject);
const
  Pergunta = 'Confirma a exclus�o da transfer�ncia selecionada?';
  Tabela = 'stqmovitsa';
var
  Tipo, OriCodi, OriCtrl: Integer;
begin
  if QrStqCenIts.RecordCount > 0 then
  begin
    Tipo    := QrStqCenItsTipo.Value;
    OriCodi := QrStqCenItsOriCodi.Value;
    OriCtrl := QrStqCenItsOriCtrl.Value;
    //
    if UMyMod.ExcluiRegistroIntArr(Pergunta, Tabela, [
    'Tipo', 'OriCodi', 'OriCtrl'], [
    Tipo, OriCodi, OriCtrl]) = ID_YES then
    begin
      OriCtrl := UMyMod.ProximoRegistro(QrStqCenIts, 'OriCtrl',
        QrStqCenItsOriCtrl.Value);
      //
      QrStqCenIts.Next;
      ReopenStqCenIts(OriCtrl);
    end;
  end;
end;

procedure TFmStqCenCab.ReopenCentros(OriCnta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCentros, Dmod.MyDB, [
  'SELECT smi.OriCnta,  smi.OriCtrl, smi.StqCencad,',
  'ELT(OriCnta, "Origem", "Destino") TpTRANSF, scc.Nome NO_CENTRO',
  'FROM stqmovitsa smi',
  'LEFT JOIN stqcencad scc ON scc.Codigo=smi.StqCenCad',
  'WHERE smi.Tipo=' + Geral.FF0(VAR_FATID_0061),
  'AND smi.OriCodi=' + Geral.FF0(QrStqCenCabCodigo.Value),
  'AND smi.OriCtrl=' + Geral.FF0(QrStqCenItsOriCtrl.Value),
  '']);
end;

procedure TFmStqCenCab.ReopenStqCenIts(OriCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqCenIts, Dmod.MyDB, [
  'SELECT gg1.Nome NO_GG1, ',
  'IF(smi.Qtde > 0, smi.CustoAll / smi.Qtde, 0) CUSTOUNIT, smi.* ',
  'FROM stqmovitsa smi',
  'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE smi.Tipo=' + Geral.FF0(VAR_FATID_0061),
  'AND smi.OriCodi=' + Geral.FF0(QrStqCenCabCodigo.Value),
  'AND smi.OriCnta=1',
  '']);
  //
  QrStqCenIts.Locate('OriCtrl', OriCtrl, []);
end;


procedure TFmStqCenCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqCenCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqCenCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqCenCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqCenCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqCenCab.SpeedButton5Click(Sender: TObject);
var
  FisRegCad: Integer;
begin
  VAR_CADASTRO := 0;
  FisRegCad    := EdFisRegCad.ValueVariant;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
    if QrFisRegCad.Locate('Codigo', QrFisRegCadCodigo.Value, []) then
    begin
      EdFisRegCad.ValueVariant := QrFisRegCadCodUsu.Value;
      CBFisRegCad.KeyValue     := QrFisRegCadCodUsu.Value;
      EdFisRegCad.SetFocus;
    end;
  end;
end;

procedure TFmStqCenCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqCenCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqCenCabCodigo.Value;
  Close;
end;

procedure TFmStqCenCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormStqCenIts(stIns);
end;

procedure TFmStqCenCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrStqCenCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'stqcencab');
end;

procedure TFmStqCenCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, Entidade, FisRegCad, TabelaPrc: Integer;
  Nome: String;
begin
  Nome      := EdNome.ValueVariant;
  Codigo    := EdCodigo.ValueVariant;
  FisRegCad := EdFisRegCad.ValueVariant;
  TabelaPrc := EdTabelaPrc.ValueVariant;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdEmpresa, Entidade, 'Informe a empresa',
    'Codigo', 'FILIAL') then Exit;
  //
  if MyObjects.FIC(FisregCad = 0, EdFisRegCad, 'Defina uma regra fiscal!') then Exit;
  if MyObjects.FIC(TabelaPrc = 0, EdTabelaPrc, 'Defina a lista de custos!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('stqcencab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
(*
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'stqcencab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
*)
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqcencab', False, [
  'Nome', 'Entidade', 'FisRegCad', 'TabelaPrc'], [
  'Codigo'], [
  Nome, Entidade, FisRegCad, TabelaPrc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmStqCenCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'stqcencab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'stqcencab', 'Codigo');
end;

procedure TFmStqCenCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmStqCenCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmStqCenCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  CriaOForm;
  FSeq := 0;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  UnDMkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  UnDMkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
end;

procedure TFmStqCenCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqCenCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCenCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqCenCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqCenCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCenCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqCenCab.QrStqCenCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmStqCenCab.QrStqCenCabAfterScroll(DataSet: TDataSet);
begin
  ReopenStqCenIts(0);
end;

procedure TFmStqCenCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrStqCenCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmStqCenCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqCenCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'stqcencab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqCenCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCenCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrStqCenCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'stqcencab');
  //
  EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
end;

procedure TFmStqCenCab.QrStqCenCabBeforeClose(
  DataSet: TDataSet);
begin
  QrStqCenIts.Close;
end;

procedure TFmStqCenCab.QrStqCenCabBeforeOpen(DataSet: TDataSet);
begin
  QrStqCenCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqCenCab.QrStqCenItsAfterScroll(DataSet: TDataSet);
begin
  ReopenCentros(0);
end;

procedure TFmStqCenCab.QrStqCenItsBeforeClose(DataSet: TDataSet);
begin
  QrCentros.Close;
end;

end.

