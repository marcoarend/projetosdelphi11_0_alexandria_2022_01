�
 TFMSTQBALCAD 0բ  TPF0TFmStqBalCadFmStqBalCadLeftpTop� Caption0   STQ-BALAN-001 :: Balanços de Grupos de ProdutosClientHeightClientWidth�Color	clBtnFaceConstraints.MinHeight Constraints.MinWidthvFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenter
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreateOnResize
FormResizePixelsPerInch`
TextHeight TPanelPainelEditaLeft TopMWidth�Height�AlignalClient
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentBackground
ParentFontTabOrderVisible TPanel
PainelEditLeft Top Width�Height]Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalTop
BevelOuterbvNoneParentBackgroundTabOrder  TLabelLabel7Left
TopWidth HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionGrupo:FocusControl
DBEdCodigo  TLabelLabel8Left~TopWidth9HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Código: [F4]FocusControlDBEdit1  TLabelLabel9Left� TopWidth3HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Descrição:FocusControlDBEdNome  	TdmkLabel	dmkLabel1LefttTopWidth,HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionEmpresa:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel2Left
Top,Width~HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionTipo de Grupo de Produto:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel3Left.Top,WidthZHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionCentro de estoque:UpdTypeutYesSQLTypestNil  TLabelLabel10Left�Top,Width@HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionAbertura [F4]:  TLabelLabel13LeftDTopWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionID:FocusControl
DBEdCodigo  TdmkEdit
EdGrupoBalLeft
TopWidth8HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder 
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCampoGrupoBalUpdCampoGrupoBalUpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  TdmkEditEdNomeLeft� TopWidth�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabOrder
FormatTypedmktfStringMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortQryCampoNomeUpdCampoNomeUpdTypeutYesObrigatorioPermiteNuloValueVariant    ValWarn  TdmkEditEdCodUsuLeft~TopWidthHHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCampoCodUsuUpdCampoCodUsuUpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn	OnKeyDownEdCodUsuKeyDown  
TdmkEditCBEdFilialLefttTopWidth8HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBFilialIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBoxCBFilialLeft�TopWidth0HeightMargins.LeftMargins.TopMargins.RightMargins.BottomKeyFieldFilial	ListField
NOMEFILIAL
ListSource	DsFiliaisTabOrder	dmkEditCBEdFilialUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone  
TdmkEditCBEdPrdGrupTipLeft
Top<Width8HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnOnChangeEdPrdGrupTipChangeDBLookupComboBoxCBPrdGrupTipIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBoxCBPrdGrupTipLeftCTop<Width� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomKeyFieldCodUsu	ListFieldNome
ListSourceDsPrdGrupTipTabOrder	dmkEditCBEdPrdGrupTipUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone  
TdmkEditCBEdStqCenCadLeft.Top<Width8HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBStqCenCadIgnoraDBLookupComboBoxAutoSetIfOnlyOneRegsetregOnlyManual  TdmkDBLookupComboBoxCBStqCenCadLeftgTop<Width� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomKeyFieldCodUsu	ListFieldNome
ListSourceDsStqCenCadTabOrder		dmkEditCBEdStqCenCadUpdTypeutYesLocF7SQLMasc$#LocF7PreDefProcf7pNone  TdmkRadioGroupRGCasasProdLeftHTop,Width� Height.Margins.LeftMargins.TopMargins.RightMargins.BottomCaption Casas decimais: Columns	ItemIndexItems.Strings0123 TabOrder
QryCampo	CasasProdUpdCampo	CasasProdUpdTypeutYesOldValor   TdmkEdit
EdAberturaLeft�Top<WidthtHeightMargins.LeftMargins.TopMargins.RightMargins.BottomReadOnly	TabOrder
FormatTypedmktfDateTimeMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat	dmkhfLongTexto30/12/1899 00:00:00QryCampoAberturaUpdCampoAberturaUpdTypeutYesObrigatorioPermiteNuloValueVariant          ValWarn	OnKeyDownEdAberturaKeyDown  TdmkEditEdCodigoLeftDTopWidth8HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCampoCodigoUpdCampoCodigoUpdTypeutIncObrigatorioPermiteNuloValueVariant ValWarn  TdmkRadioGroup	RGForcadaLeftYTop,Width� Height.Margins.LeftMargins.TopMargins.RightMargins.BottomCaption    Forçada: ColumnsEnabled	ItemIndex Items.Strings   NãoSim TabOrderQryCampoForcadaUpdCampoForcadaUpdTypeutYesOldValor    	TGroupBoxGBRodaPeLeft TopvWidth�Height@Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomTabOrder TPanelPanel8LeftTopWidth�Height/Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelOuterbvNoneTabOrder  TPanel
PnSaiDesisLeft:Top Width� Height/Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight
BevelOuterbvNoneTabOrder TBitBtn	BtDesisteTagLeft	TopWidthxHeight(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.BottomCaption&DesisteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrder OnClickBtDesisteClick   TBitBtn
BtConfirmaTagLeft
TopWidthxHeight(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.BottomCaption	&ConfirmaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrder OnClickBtConfirmaClick     TPanelPainelDadosLeft TopMWidth�Height�AlignalClient
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentBackground
ParentFontTabOrder  TPanelPnCabecaLeft Top Width�HeightUMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalTop
BevelOuterbvNoneEnabledTabOrder  TLabelLabel1Left
TopWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionID:FocusControl
DBEdCodigo  TLabelLabel2Left� TopWidth3HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Descrição:FocusControlDBEdNome  TLabelLabel3LeftDTopWidth$HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Código:FocusControlDBEdit1  TLabelLabel4Left�TopWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionFilial:FocusControlDBEdit2  TLabelLabel5Left
Top,Width� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionTipo de grupo de produtos:FocusControlDBEdit3  TLabelLabel6Left:Top,WidthZHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionCentro de estoque:FocusControlDBEdit4  TLabelLabel11Left�Top,Width+HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption	Abertura:  TLabelLabel12Left�Top,WidthEHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionEncerramento:  TDBEdit
DBEdCodigoLeft
TopWidth8HeightHint   Nº do bancoMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	DataFieldCodigo
DataSourceDsStqBalCadFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintReadOnly	ShowHint	TabOrder   TDBEditDBEdNomeLeft� TopWidthHeightHintNome do bancoMargins.LeftMargins.TopMargins.RightMargins.BottomColorclWhite	DataFieldNome
DataSourceDsStqBalCadFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrder  TDBEditDBEdit1LeftDTopWidthHHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	DataFieldCodUsu
DataSourceDsStqBalCadFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditDBEdit2Left�TopWidthIHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	DataField
NOMEFILIAL
DataSourceDsStqBalCadTabOrder  TDBEditDBEdit3Left
Top<Width,HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	DataFieldNOMEPRDGRUPTIP
DataSourceDsStqBalCadTabOrder  TDBEditDBEdit4Left:Top<WidthHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	DataFieldMOMESTQCENCAD
DataSourceDsStqBalCadTabOrder  TDBRadioGroupDBRadioGroup1Left[Top,Width� Height(Margins.LeftMargins.TopMargins.RightMargins.BottomCaption Casas decimais: Columns	DataField	CasasProd
DataSourceDsStqBalCadItems.Strings0123 TabOrderValues.Strings0123   TDBEditDBEdit5Left�Top<WidthpHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	DataFieldAbertura
DataSourceDsStqBalCadTabOrder  TDBEditDBEdit6LeftpTop<WidthpHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	DataFieldENCERROU_TXT
DataSourceDsStqBalCadTabOrderOnChangeDBEdit6Change   TPanelPanel4Left TopZWidth�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottom
BevelOuterbvNoneTabOrder TPageControlPageControl1Left Top Width�HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom
ActivePage	TabSheet1AlignalClientTabOrder  	TTabSheet	TabSheet1Margins.LeftMargins.TopMargins.RightMargins.BottomCaption Pela leitura por local 
TdmkDBGrid
dmkDBGrid1Left Top WidthHeight Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeftColumnsExpanded	FieldNameCodUsuTitle.Caption   CódigoWidth(Visible	 Expanded	FieldNameNomeTitle.Caption   Descrição do localWidth� Visible	  ColorclWindow
DataSourceDsLocCenLocOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCodUsuTitle.Caption   CódigoWidth(Visible	 Expanded	FieldNameNomeTitle.Caption   Descrição do localWidth� Visible	    
TdmkDBGrid
dmkDBGrid2LeftTop Width�Height Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientColumnsExpanded	FieldNameGraGruXTitle.CaptionReduzidoWidth7Visible	 Expanded	FieldName
NOMENIVEL1Title.CaptionGrupo de produtosWidth� Visible	 Expanded	FieldName
CODUSU_CORTitle.CaptionCorWidth'Visible	 Expanded	FieldNameNOMECORTitle.Caption   Descrição da corWidthVisible	 Expanded	FieldNameNOMETAMTitle.CaptionTamanhoWidth5Visible	 Expanded	FieldNameQtdLeiTitle.Caption
QuantidadeVisible	 Expanded	FieldNameCustoAllTitle.CaptionCustoWidthTVisible	  ColorclWindow
DataSourceDsLocBalItsOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameGraGruXTitle.CaptionReduzidoWidth7Visible	 Expanded	FieldName
NOMENIVEL1Title.CaptionGrupo de produtosWidth� Visible	 Expanded	FieldName
CODUSU_CORTitle.CaptionCorWidth'Visible	 Expanded	FieldNameNOMECORTitle.Caption   Descrição da corWidthVisible	 Expanded	FieldNameNOMETAMTitle.CaptionTamanhoWidth5Visible	 Expanded	FieldNameQtdLeiTitle.Caption
QuantidadeVisible	 Expanded	FieldNameCustoAllTitle.CaptionCustoWidthTVisible	     	TTabSheet	TabSheet3Margins.LeftMargins.TopMargins.RightMargins.BottomCaption    Por divergências no centro 
ImageIndex 
TdmkDBGrid
dmkDBGrid4Left Top Width�Height Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientColumnsExpanded	FieldNameCODUSU_NIVEL1Title.Caption   CódigoWidthHVisible	 Expanded	FieldName
NOMENIVEL1Title.CaptionGrupo de produtosWidth� Visible	 Expanded	FieldName
CODUSU_CORTitle.CaptionCorVisible	 Expanded	FieldNameNOMECORTitle.Caption   Descrição da corWidth� Visible	 Expanded	FieldNameNOMETAMTitle.CaptionTamanhoWidth5Visible	 Expanded	FieldNameQtdAntTitle.Caption	Qtd. sdo.Visible	 Expanded	FieldNameQtdLeiTitle.CaptionQtd lidoVisible	 Expanded	FieldNameQTDDIFTitle.Caption	Qtd. dif.Visible	 Expanded	FieldName	QTD_IGUALTitle.CaptionIguaisVisible	 Expanded	FieldName	QTD_MENOSTitle.CaptionFaltasVisible	 Expanded	FieldNameQTD_MAISTitle.CaptionSobrasVisible	  ColorclWindow
DataSource
DsListaCenOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCODUSU_NIVEL1Title.Caption   CódigoWidthHVisible	 Expanded	FieldName
NOMENIVEL1Title.CaptionGrupo de produtosWidth� Visible	 Expanded	FieldName
CODUSU_CORTitle.CaptionCorVisible	 Expanded	FieldNameNOMECORTitle.Caption   Descrição da corWidth� Visible	 Expanded	FieldNameNOMETAMTitle.CaptionTamanhoWidth5Visible	 Expanded	FieldNameQtdAntTitle.Caption	Qtd. sdo.Visible	 Expanded	FieldNameQtdLeiTitle.CaptionQtd lidoVisible	 Expanded	FieldNameQTDDIFTitle.Caption	Qtd. dif.Visible	 Expanded	FieldName	QTD_IGUALTitle.CaptionIguaisVisible	 Expanded	FieldName	QTD_MENOSTitle.CaptionFaltasVisible	 Expanded	FieldNameQTD_MAISTitle.CaptionSobrasVisible	     	TTabSheet	TabSheet2Margins.LeftMargins.TopMargins.RightMargins.BottomCaption    Por divergências no local 
ImageIndex 
TdmkDBGrid
dmkDBGrid3Left Top Width�Height Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientColumnsExpanded	FieldNameNOMESTQCENLOCTitle.CaptionLocal Width� Visible	 Expanded	FieldNameCODUSU_NIVEL1Title.Caption   CódigoWidthHVisible	 Expanded	FieldName
NOMENIVEL1Title.CaptionGrupo de produtosWidth� Visible	 Expanded	FieldName
CODUSU_CORTitle.CaptionCorVisible	 Expanded	FieldNameNOMECORTitle.Caption   Descrição da corWidth� Visible	 Expanded	FieldNameNOMETAMTitle.CaptionTamanhoWidth5Visible	 Expanded	FieldNameQtdAntTitle.Caption	Qtd. sdo.Visible	 Expanded	FieldNameQtdLeiTitle.CaptionQtd lidoVisible	 Expanded	FieldNameQTDDIFTitle.Caption	Qtd. dif.Visible	  ColorclWindow
DataSource
DsListaLocOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style VisibleColumnsExpanded	FieldNameNOMESTQCENLOCTitle.CaptionLocal Width� Visible	 Expanded	FieldNameCODUSU_NIVEL1Title.Caption   CódigoWidthHVisible	 Expanded	FieldName
NOMENIVEL1Title.CaptionGrupo de produtosWidth� Visible	 Expanded	FieldName
CODUSU_CORTitle.CaptionCorVisible	 Expanded	FieldNameNOMECORTitle.Caption   Descrição da corWidth� Visible	 Expanded	FieldNameNOMETAMTitle.CaptionTamanhoWidth5Visible	 Expanded	FieldNameQtdAntTitle.Caption	Qtd. sdo.Visible	 Expanded	FieldNameQtdLeiTitle.CaptionQtd lidoVisible	 Expanded	FieldNameQTDDIFTitle.Caption	Qtd. dif.Visible	       	TGroupBoxGBCntrlLeft TopvWidth�Height@Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomTabOrder TPanelPanel5LeftTopWidth� Height/Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeft
BevelOuterbvNoneParentColor	TabOrder TBitBtnSpeedButton4TagLeft� TopWidth(Height(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrder OnClickSpeedButton4Click  TBitBtnSpeedButton3TagLeft\TopWidth(Height(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton3Click  TBitBtnSpeedButton2TagLeft3TopWidth(Height(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton2Click  TBitBtnSpeedButton1TagLeft
TopWidth(Height(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton1Click   TStaticText
LaRegistroLeft� TopWidth� Height/Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelInner	bvLowered	BevelKindbkFlatCaption[N]: 0TabOrder  TPanelPanel3LeftmTopWidth�Height/Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight
BevelOuterbvNoneParentColor	TabOrder  TPanelPanel2Left�Top Width� Height/Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight	AlignmenttaRightJustify
BevelOuterbvNoneTabOrder TBitBtnBtSaidaTagLeftTopWidthxHeight(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   &Saída	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtSaidaClick   TBitBtn	BtBalancoTag'LeftTopWidthxHeight(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.BottomCaption	   &Balanço	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtBalancoClick  TBitBtn	BtLeituraTagLeft� TopWidthxHeight(CursorcrHandPointMargins.LeftMargins.TopMargins.RightMargins.BottomCaption&LeituraEnabled	NumGlyphsParentShowHintShowHint	TabOrderOnClickBtLeituraClick     TPanelPanel6Left Top Width�Height0AlignalTop
BevelOuterbvNoneTabOrder 	TGroupBoxGB_RLeft�Top Width4Height0Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRightTabOrder  	TdmkImageImgTipoLeftTopWidth Height Margins.LeftMargins.TopMargins.RightMargins.BottomTransparent	SQLTypestNil   	TGroupBoxGB_LLeft Top Width� Height0Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeftTabOrder TBitBtn	SbImprimeTagLeftTopWidth(Height(Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrder OnClickSbImprimeClick  TBitBtnSbNovoTagLeft.TopWidth(Height(Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbNovoClick  TBitBtnSbNumeroTagLeftXTopWidth(Height(Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbNumeroClick  TBitBtnSbNomeTagLeft� TopWidth(Height(Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbNomeClick  TBitBtnSbQueryTag	Left� TopWidth(Height(Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbQueryClick   	TGroupBoxGB_MLeft� Top Width�Height0Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientTabOrder TLabel
LaTitulo1ALeft	TopWidth�Height Margins.LeftMargins.TopMargins.RightMargins.BottomCaption   Balanços de Grupos de ProdutosColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclGradientActiveCaptionFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFontVisible  TLabel
LaTitulo1BLeftTopWidth�Height Margins.LeftMargins.TopMargins.RightMargins.BottomCaption   Balanços de Grupos de ProdutosColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont  TLabel
LaTitulo1CLeft
TopWidth�Height Margins.LeftMargins.TopMargins.RightMargins.BottomCaption   Balanços de Grupos de ProdutosColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.Color
clHotLightFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont    	TGroupBox	GBAvisos1Left Top0Width�HeightAlignalTopCaption	 Avisos: TabOrder TPanelPanel7LeftTopWidth�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelOuterbvNoneTabOrder  TLabelLaAviso1LeftTopWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption...Font.CharsetDEFAULT_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	  TLabelLaAviso2LeftTopWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption...Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	    TDataSourceDsStqBalCadDataSetQrStqBalCadLeftDTop  TMySQLQueryQrStqBalCadDatabase	Dmod.MyDB
BeforeOpenQrStqBalCadBeforeOpen	AfterOpenQrStqBalCadAfterOpenBeforeCloseQrStqBalCadBeforeCloseAfterScrollQrStqBalCadAfterScrollOnCalcFieldsQrStqBalCadCalcFieldsSQL.Strings;SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL, 1pgt.Nome NOMEPRDGRUPTIP, scc.Nome MOMESTQCENCAD, pem.BalQtdItem, sbc.*FROM stqbalcad sbc2LEFT JOIN entidades fil  ON fil.Codigo=sbc.Empresa5LEFT JOIN prdgruptip pgt ON pgt.Codigo=sbc.PrdGrupTip4LEFT JOIN stqcencad scc  ON scc.Codigo=sbc.StqCenCad2LEFT JOIN paramsemp pem  ON pem.Codigo=sbc.Empresa Left(Top TIntegerFieldQrStqBalCadCodigo	FieldNameCodigoRequired	  TIntegerFieldQrStqBalCadCodUsu	FieldNameCodUsuRequired	  TWideStringFieldQrStqBalCadNome	FieldNameNomeRequired	Size2  TIntegerFieldQrStqBalCadEmpresa	FieldNameEmpresaRequired	  TIntegerFieldQrStqBalCadPrdGrupTip	FieldName
PrdGrupTipRequired	  TIntegerFieldQrStqBalCadStqCenCad	FieldName	StqCenCadRequired	  TSmallintFieldQrStqBalCadCasasProd	FieldName	CasasProdRequired	  TDateTimeFieldQrStqBalCadAbertura	FieldNameAberturaRequired	DisplayFormatdd/mm/yyyy hh:nn:ss  TDateTimeFieldQrStqBalCadEncerrou	FieldNameEncerrouRequired	DisplayFormatdd/mm/yyyy hh:nn:ss  TWideStringFieldQrStqBalCadNOMEFILIAL	FieldName
NOMEFILIALSized  TWideStringFieldQrStqBalCadNOMEPRDGRUPTIP	FieldNameNOMEPRDGRUPTIPSize  TWideStringFieldQrStqBalCadMOMESTQCENCAD	FieldNameMOMESTQCENCADSize2  TFloatFieldQrStqBalCadBalQtdItem	FieldName
BalQtdItem  TWideStringFieldQrStqBalCadENCERROU_TXT	FieldKindfkCalculated	FieldNameENCERROU_TXTSize2
Calculated	  TSmallintFieldQrStqBalCadGrupoBal	FieldNameGrupoBal  TSmallintFieldQrStqBalCadStatus	FieldNameStatus  TIntegerFieldQrStqBalCadLk	FieldNameLk  
TDateFieldQrStqBalCadDataCad	FieldNameDataCad  
TDateFieldQrStqBalCadDataAlt	FieldNameDataAlt  TIntegerFieldQrStqBalCadUserCad	FieldNameUserCad  TIntegerFieldQrStqBalCadUserAlt	FieldNameUserAlt  TSmallintFieldQrStqBalCadAlterWeb	FieldNameAlterWeb  TSmallintFieldQrStqBalCadAtivo	FieldNameAtivo  TSmallintFieldQrStqBalCadForcada	FieldNameForcada   TdmkPermissoesdmkPermissoes1Left`Top  
TPopupMenu	PMBalancoOnPopupPMBalancoPopupLeft8Top� 	TMenuItemIncluinovobalano1Caption   &Inclui novo balançoOnClickIncluinovobalano1Click  	TMenuItemAlterabalanoatual1Caption   &Altera balanço atualEnabledOnClickAlterabalanoatual1Click  	TMenuItemEncerraBalanoatual1Caption   Encerra &Balanço atualEnabledOnClickEncerraBalanoatual1Click  	TMenuItemN1Caption-  	TMenuItemExcluibalanoatual1Caption   E&Xclui balanço atualEnabled   TDataSource	DsFiliaisDataSet	QrFiliaisLeftdTop(  TMySQLQuery	QrFiliaisDatabase	Dmod.MyDBSQL.StringsSELECT Filial, Codigo, (IF(Tipo=0, RazaoSocial, Nome) NOMEFILIALFROM entidadesWHERE Codigo<-10ORDER BY NOMEFILIAL LeftHTop( TIntegerFieldQrFiliaisFilial	FieldNameFilial  TIntegerFieldQrFiliaisCodigo	FieldNameCodigo  TWideStringFieldQrFiliaisNOMEFILIAL	FieldName
NOMEFILIALRequired	Sized   
TdmkValUsu
dmkValUsu1	dmkEditCBEdFilialPanelPainelEditaQryCampoEmpresaUpdCampoEmpresaRefCampoCodigoUpdTypeutYesLeft|Top  TMySQLQueryQrPrdGrupTipDatabase	Dmod.MyDBSQL.Strings#SELECT Codigo, CodUsu, Nome, FracioFROM prdgruptipWHERE Codigo <> 0ORDER BY Nome LeftTop( TIntegerFieldQrPrdGrupTipCodigo	FieldNameCodigo  TIntegerFieldQrPrdGrupTipCodUsu	FieldNameCodUsu  TWideStringFieldQrPrdGrupTipNome	FieldNameNomeSize  TSmallintFieldQrPrdGrupTipFracio	FieldNameFracio   TDataSourceDsPrdGrupTipDataSetQrPrdGrupTipLeft,Top(  
TdmkValUsu
dmkValUsu2	dmkEditCBEdPrdGrupTipPanelPainelEditaQryCampo
PrdGrupTipUpdCampo
PrdGrupTipRefCampoCodigoUpdTypeutYesLeft� Top  
TdmkValUsu
dmkValUsu3	dmkEditCBEdStqCenCadPanelPainelEditaQryCampo	StqCenCadUpdCampo	StqCenCadRefCampoCodigoUpdTypeutYesLeft� Top  TMySQLQueryQrStqCenCadDatabase	Dmod.MyDBSQL.StringsSELECT Codigo, CodUsu, NomeFROM stqcencadORDER BY Nome Left�Top( TIntegerFieldQrStqCenCadCodigo	FieldNameCodigo  TIntegerFieldQrStqCenCadCodUsu	FieldNameCodUsu  TWideStringFieldQrStqCenCadNome	FieldNameNomeSize2   TDataSourceDsStqCenCadDataSetQrStqCenCadLeft�Top(  TMySQLQueryQrLocCenLocDatabase	Dmod.MyDBBeforeCloseQrLocCenLocBeforeCloseAfterScrollQrLocCenLocAfterScrollSQL.StringsSELECT Controle, CodUsu, Nome FROM stqcenlocWHERE Controle in (  SELECT DISTINCT StqCenLoc   FROM stqbalits  WHERE Codigo=:P0 )     Left�Top� 	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrLocCenLocControle	FieldNameControle  TIntegerFieldQrLocCenLocCodUsu	FieldNameCodUsu  TWideStringFieldQrLocCenLocNome	FieldNameNomeSize2   TDataSourceDsLocCenLocDataSetQrLocCenLocLeft�Top  TDataSourceDsLocBalItsDataSetQrLocBalItsLeftTop  TMySQLQueryQrLocBalItsDatabase	Dmod.MyDBSQL.Strings5SELECT gg1.CodUsu CODUSU_NIVEL1, gg1.Nome NOMENIVEL1,7ggc.GraCorCad, gcc.CodUsu CODUSU_COR, gcc.Nome NOMECOR,$gti.Nome NOMETAM, ggx.GraGru1, sbi.*FROM stqbalits sbi3LEFT JOIN gragrux   ggx ON ggx.Controle=sbi.GraGruX1LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru13LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC3LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad3LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamIWHERE sbi.Codigo=:P0AND sbi.StqCenLoc=:P1     LeftTop� 	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown DataType	ftUnknownNameP1	ParamType	ptUnknown   TWideStringFieldQrLocBalItsNOMENIVEL1	FieldName
NOMENIVEL1Origingragru1.NomeSize  TIntegerFieldQrLocBalItsGraCorCad	FieldName	GraCorCadOrigingragruc.GraCorCad  TWideStringFieldQrLocBalItsNOMECOR	FieldNameNOMECOROrigingracorcad.NomeSize  TWideStringFieldQrLocBalItsNOMETAM	FieldNameNOMETAMOrigingratamits.NomeSize  TIntegerFieldQrLocBalItsGraGru1	FieldNameGraGru1Origingragrux.GraGru1  TIntegerFieldQrLocBalItsCodigo	FieldNameCodigoOriginstqbalits.CodigoRequired	  TIntegerFieldQrLocBalItsControle	FieldNameControleOriginstqbalits.ControleRequired	  TSmallintFieldQrLocBalItsTipo	FieldNameTipoOriginstqbalits.TipoRequired	  TDateTimeFieldQrLocBalItsDataHora	FieldNameDataHoraOriginstqbalits.DataHoraRequired	  TIntegerFieldQrLocBalItsStqCenLoc	FieldName	StqCenLocOriginstqbalits.StqCenLocRequired	  TIntegerFieldQrLocBalItsGraGruX	FieldNameGraGruXOriginstqbalits.GraGruXRequired	  TIntegerFieldQrLocBalItsSEQ	FieldKindfkCalculated	FieldNameSEQDisplayFormat00000
Calculated	  TIntegerFieldQrLocBalItsCODUSU_NIVEL1	FieldNameCODUSU_NIVEL1Required	  TIntegerFieldQrLocBalItsCODUSU_COR	FieldName
CODUSU_CORRequired	  TFloatFieldQrLocBalItsQtdLei	FieldNameQtdLeiRequired	  TFloatFieldQrLocBalItsCustoAll	FieldNameCustoAllDisplayFormat#,###,##0.00;-#,###,##0.00;    
TfrxReportfrxSTQ_BALAN_001_01Version2022.1DotMatrixReportEngineOptions.DoublePass	IniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate PS�뭬�@ReportOptions.LastChange PS�뭬�@ScriptLanguagePascalScriptScriptText.StringsbeginD  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Name));end. Left8TopDatasets DataSetfrxDsLocBalItsDataSetNamefrxDsLocBalIts DataSetfrxDsLocCenLocDataSetNamefrxDsLocCenLoc  	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight      ��@	PaperSize	
LeftMargin       �@RightMargin       �@	TopMargin       �@BottomMargin       �@	Frame.Typ 
MirrorMode  TfrxPageHeaderPageHeader1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height��	K<�L�@Top��O����@WidthQk�w��ͮ@ TfrxShapeViewShape1AllowVectorExport	WidthQk�w��ͮ@Height�	�c.�@	Frame.Typ Frame.Width���������?ShapeskRoundRectangle  TfrxMemoViewMemo13AllowVectorExport	Left�j����@Width�1ZGU�@Height�	�c.�@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W[frxDsDono."NOMEDONO"] 
ParentFontVAlignvaCenter  TfrxLineViewLine1AllowVectorExport	Topc	�c.�@WidthQk�w��ͮ@ColorclBlack	Frame.TypftTop Frame.Width���������?  TfrxMemoViewMemo14AllowVectorExport	Left�	�c.�@Topc	�c.�@Width��+e�l�@Height�	�c.�@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	Frame.Typ HAlignhaCenter
Memo.UTF8W   RESULTADO DE BALANÇO 
ParentFontVAlignvaCenter  TfrxMemoViewMemo15AllowVectorExport	Left�j����@Topc	�c.�@Width���)�D��@Height�	�c.�@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ 
Memo.UTF8Wimpresso em [Date], [Time] 
ParentFontVAlignvaCenter  TfrxMemoViewMemo16AllowVectorExport	Left�*��	�@Topc	�c.�@Width���)�D��@Height�	�c.�@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaRight
Memo.UTF8W   Página [Page] de [TotalPages] 
ParentFontVAlignvaCenter   TfrxReportTitleReportTitle1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�˷�N��@Top�	�c.�@WidthQk�w��ͮ@  TfrxMasterDataMasterData1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�	�c.�@Top6v�ꭁ��@WidthQk�w��ͮ@DataSetfrxDsLocCenLocDataSetNamefrxDsLocCenLocRowCount  TfrxMemoViewMemo3AllowVectorExport	Width�	�c.�@Height�	�c.�@	DataFieldCodUsuDataSetfrxDsLocCenLocDataSetNamefrxDsLocCenLocDisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8W[frxDsLocCenLoc."CodUsu"] 
ParentFontWordWrap  TfrxMemoViewMemo21AllowVectorExport	Left�	�c.�@Width�JY�8��@Height�	�c.�@	DataFieldNomeDataSetfrxDsLocCenLocDataSetNamefrxDsLocCenLocDisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	Frame.TypftLeftftRightftTopftBottom Frame.Width���������?
Memo.UTF8W[frxDsLocCenLoc."Nome"] 
ParentFont   TfrxPageFooterPageFooter1FillTypeftBrushFillGap.Top FillGap.Left FillGap.Bottom FillGap.Right 	Frame.Typ Height�˷�N��@TopQ1�߄B�@WidthQk�w��ͮ@ TfrxMemoViewMemo1AllowVectorExport	Left�j����@Width>�
Y�j�@Height�˷�N��@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ 
Memo.UTF8W*www.dermatek.com.br - software customizado 
ParentFontVAlignvaCenter  TfrxMemoViewMeVARF_CODI_FRXAllowVectorExport	Left�	K<�l2�@Width���QI��@Height�˷�N��@DisplayFormat.DecimalSeparator,Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameUnivers Light Condensed
Font.Style 	Frame.Typ HAlignhaRight
Memo.UTF8WVARF_CODI_FRX 
ParentFontVAlignvaCenter     TfrxDBDatasetfrxDsLocCenLocUserNamefrxDsLocCenLocCloseDataSourceDataSetQrLocCenLocBCDToCurrencyDataSetOptions Left�TopL  TfrxDBDatasetfrxDsLocBalItsUserNamefrxDsLocBalItsCloseDataSourceDataSetQrLocBalItsBCDToCurrencyDataSetOptions LeftTopL  TMySQLQuery
QrListaLocDatabase	Dmod.MyDBSQL.Strings#SELECT scl.CodUsu CODUSU_STQCENLOC,&scl.Nome NOMESTQCENLOC, sbi.StqCenLoc,.gg1.CodUsu CODUSU_NIVEL1, gg1.Nome NOMENIVEL1,7ggc.GraCorCad, gcc.CodUsu CODUSU_COR, gcc.Nome NOMECOR,<gti.Nome NOMETAM, ggx.GraGru1, COUNT(sbi.Controle) LEITURAS,3MIN(sbi.DataHora) DH_INI, MAX(sbi.DataHora) DH_FIM,BSUM(sbi.QtdLei) QtdLei, QtdAnt, (SUM(sbi.QtdLei) - QtdAnt) QTDDIF FROM stqbalits sbi5LEFT JOIN stqcenloc scl ON scl.Controle=sbi.StqCenLoc3LEFT JOIN gragrux   ggx ON ggx.Controle=sbi.GraGruX1LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru13LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC3LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad3LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamIWHERE sbi.Codigo=:P0#GROUP BY sbi.StqCenLoc, sbi.GraGruX4ORDER BY NOMESTQCENLOC, NOMENIVEL1, NOMECOR, NOMETAM LeftTTop	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrListaLocCODUSU_STQCENLOC	FieldNameCODUSU_STQCENLOC  TWideStringFieldQrListaLocNOMESTQCENLOC	FieldNameNOMESTQCENLOCSize2  TIntegerFieldQrListaLocStqCenLoc	FieldName	StqCenLocRequired	  TIntegerFieldQrListaLocCODUSU_NIVEL1	FieldNameCODUSU_NIVEL1  TWideStringFieldQrListaLocNOMENIVEL1	FieldName
NOMENIVEL1Size  TIntegerFieldQrListaLocGraCorCad	FieldName	GraCorCad  TIntegerFieldQrListaLocCODUSU_COR	FieldName
CODUSU_COR  TWideStringFieldQrListaLocNOMECOR	FieldNameNOMECORSize  TWideStringFieldQrListaLocNOMETAM	FieldNameNOMETAMSize  TIntegerFieldQrListaLocGraGru1	FieldNameGraGru1  TLargeintFieldQrListaLocLEITURAS	FieldNameLEITURASRequired	  TDateTimeFieldQrListaLocDH_INI	FieldNameDH_INIRequired	  TDateTimeFieldQrListaLocDH_FIM	FieldNameDH_FIMRequired	  TFloatFieldQrListaLocQtdLei	FieldNameQtdLei  TFloatFieldQrListaLocQtdAnt	FieldNameQtdAntRequired	  TFloatFieldQrListaLocQTDDIF	FieldNameQTDDIFRequired	   TDataSource
DsListaLocDataSet
QrListaLocLeftpTop  
TPopupMenu	PMImprimeLeftTop 	TMenuItem	porLocal1Caption
por &LocalOnClickporLocal1Click  	TMenuItempeloReduzido1Captionpelo &Reduzido   TMySQLQuery
QrListaCenDatabase	Dmod.MyDBSQL.Strings5SELECT gg1.CodUsu CODUSU_NIVEL1, gg1.Nome NOMENIVEL1,7ggc.GraCorCad, gcc.CodUsu CODUSU_COR, gcc.Nome NOMECOR,<gti.Nome NOMETAM, ggx.GraGru1, COUNT(sbi.Controle) LEITURAS,3MIN(sbi.DataHora) DH_INI, MAX(sbi.DataHora) DH_FIM,BSUM(sbi.QtdLei) QtdLei, QtdAnt, (SUM(sbi.QtdLei) - QtdAnt) QTDDIF,<IF(SUM(sbi.QtdLei) = QtdAnt, SUM(sbi.QtdLei), 0) QTD_IGUAL, DIF(SUM(sbi.QtdLei) > QtdAnt, SUM(sbi.QtdLei) - QtdAnt, 0) QTD_MAIS, CIF(SUM(sbi.QtdLei) < QtdAnt, QtdAnt - SUM(sbi.QtdLei), 0) QTD_MENOSFROM stqbalits sbi5LEFT JOIN stqcenloc scl ON scl.Controle=sbi.StqCenLoc3LEFT JOIN gragrux   ggx ON ggx.Controle=sbi.GraGruX1LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru13LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC3LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad3LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamIWHERE sbi.Codigo=:P0GROUP BY sbi.GraGruX%ORDER BY NOMENIVEL1, NOMECOR, NOMETAM Left�Top	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrListaCenCODUSU_NIVEL1	FieldNameCODUSU_NIVEL1  TWideStringFieldQrListaCenNOMENIVEL1	FieldName
NOMENIVEL1Size  TIntegerFieldQrListaCenGraCorCad	FieldName	GraCorCad  TIntegerFieldQrListaCenCODUSU_COR	FieldName
CODUSU_COR  TWideStringFieldQrListaCenNOMECOR	FieldNameNOMECORSize  TWideStringFieldQrListaCenNOMETAM	FieldNameNOMETAMSize  TIntegerFieldQrListaCenGraGru1	FieldNameGraGru1  TLargeintFieldQrListaCenLEITURAS	FieldNameLEITURASRequired	  TDateTimeFieldQrListaCenDH_INI	FieldNameDH_INIRequired	  TDateTimeFieldQrListaCenDH_FIM	FieldNameDH_FIMRequired	  TFloatFieldQrListaCenQtdLei	FieldNameQtdLei  TFloatFieldQrListaCenQtdAnt	FieldNameQtdAntRequired	  TFloatFieldQrListaCenQTDDIF	FieldNameQTDDIFRequired	  TFloatFieldQrListaCenQTD_IGUAL	FieldName	QTD_IGUALRequired	  TFloatFieldQrListaCenQTD_MAIS	FieldNameQTD_MAISRequired	  TFloatFieldQrListaCenQTD_MENOS	FieldName	QTD_MENOSRequired	   TDataSource
DsListaCenDataSet
QrListaCenLeft�Top  TMySQLQueryQrNewDatabase	Dmod.MyDBSQL.StringsSELECT CodigoFROM stqbalcadWHERE Encerrou=0AND Empresa=:P0AND PrdGrupTip=:P1AND StqCenCad=:P2AND Codigo <> 0 AND Codigo <> :P3ORDER BY Codigo Left�Top(	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown DataType	ftUnknownNameP1	ParamType	ptUnknown DataType	ftUnknownNameP2	ParamType	ptUnknown DataType	ftUnknownNameP3	ParamType	ptUnknown   TIntegerFieldQrNewCodigo	FieldNameCodigo    