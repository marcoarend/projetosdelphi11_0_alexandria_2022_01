unit GraG1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGridZTO, UnDmkEnums;

type
  TFmGraG1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    EdGraTabApp: TdmkEditCB;
    CBGraTabApp: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrGraTabApp: TmySQLQuery;
    QrGraTabAppCodigo: TIntegerField;
    QrGraTabAppNome: TWideStringField;
    QrGraTabAppTabela: TWideStringField;
    DsGraTabApp: TDataSource;
    DBGGraGru1: TdmkDBGridZTO;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Nivel5: TIntegerField;
    QrGraGru1Nivel4: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1IPI_Alq: TFloatField;
    QrGraGru1IPI_CST: TSmallintField;
    QrGraGru1IPI_cEnq: TWideStringField;
    QrGraGru1IPI_vUnid: TFloatField;
    QrGraGru1IPI_TpTrib: TSmallintField;
    QrGraGru1TipDimens: TSmallintField;
    QrGraGru1PerCuztMin: TFloatField;
    QrGraGru1PerCuztMax: TFloatField;
    QrGraGru1MedOrdem: TIntegerField;
    QrGraGru1PartePrinc: TIntegerField;
    QrGraGru1InfAdProd: TWideStringField;
    QrGraGru1SiglaCustm: TWideStringField;
    QrGraGru1HowBxaEstq: TSmallintField;
    QrGraGru1GerBxaEstq: TSmallintField;
    QrGraGru1PIS_CST: TSmallintField;
    QrGraGru1PIS_AlqP: TFloatField;
    QrGraGru1PIS_AlqV: TFloatField;
    QrGraGru1PISST_AlqP: TFloatField;
    QrGraGru1PISST_AlqV: TFloatField;
    QrGraGru1COFINS_CST: TSmallintField;
    QrGraGru1COFINS_AlqP: TFloatField;
    QrGraGru1COFINS_AlqV: TFloatField;
    QrGraGru1COFINSST_AlqP: TFloatField;
    QrGraGru1COFINSST_AlqV: TFloatField;
    QrGraGru1ICMS_modBC: TSmallintField;
    QrGraGru1ICMS_modBCST: TSmallintField;
    QrGraGru1ICMS_pRedBC: TFloatField;
    QrGraGru1ICMS_pRedBCST: TFloatField;
    QrGraGru1ICMS_pMVAST: TFloatField;
    QrGraGru1ICMS_pICMSST: TFloatField;
    QrGraGru1ICMS_Pauta: TFloatField;
    QrGraGru1ICMS_MaxTab: TFloatField;
    QrGraGru1IPI_pIPI: TFloatField;
    QrGraGru1cGTIN_EAN: TWideStringField;
    QrGraGru1EX_TIPI: TWideStringField;
    QrGraGru1PIS_pRedBC: TFloatField;
    QrGraGru1PISST_pRedBCST: TFloatField;
    QrGraGru1COFINS_pRedBC: TFloatField;
    QrGraGru1COFINSST_pRedBCST: TFloatField;
    QrGraGru1ICMSRec_pRedBC: TFloatField;
    QrGraGru1IPIRec_pRedBC: TFloatField;
    QrGraGru1PISRec_pRedBC: TFloatField;
    QrGraGru1COFINSRec_pRedBC: TFloatField;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1IPIRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    QrGraGru1ICMSRec_tCalc: TSmallintField;
    QrGraGru1IPIRec_tCalc: TSmallintField;
    QrGraGru1PISRec_tCalc: TSmallintField;
    QrGraGru1COFINSRec_tCalc: TSmallintField;
    QrGraGru1ICMSAliqSINTEGRA: TFloatField;
    QrGraGru1ICMSST_BaseSINTEGRA: TFloatField;
    QrGraGru1FatorClas: TIntegerField;
    QrGraGru1prod_indTot: TSmallintField;
    QrGraGru1Referencia: TWideStringField;
    QrGraGru1Fabricante: TIntegerField;
    QrGraGru1PerComissF: TFloatField;
    QrGraGru1PerComissR: TFloatField;
    QrGraGru1PerComissZ: TSmallintField;
    QrGraGru1NomeTemp: TWideStringField;
    QrGraGru1COD_LST: TWideStringField;
    QrGraGru1SPEDEFD_ALIQ_ICMS: TFloatField;
    QrGraGru1DadosFisc: TSmallintField;
    QrGraGru1Lk: TIntegerField;
    QrGraGru1DataCad: TDateField;
    QrGraGru1DataAlt: TDateField;
    QrGraGru1UserCad: TIntegerField;
    QrGraGru1UserAlt: TIntegerField;
    QrGraGru1AlterWeb: TSmallintField;
    QrGraGru1Ativo: TSmallintField;
    QrGraGru1GraTabApp: TIntegerField;
    QrGTA: TmySQLQuery;
    QrGTACodigo: TIntegerField;
    QrGTANome: TWideStringField;
    QrGTATabela: TWideStringField;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BitBtn1: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    BtOK: TBitBtn;
    BtRegulariza: TBitBtn;
    BtNiveis: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraTabAppChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtRegularizaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrGraGru1BeforeClose(DataSet: TDataSet);
    procedure QrGraGru1AfterOpen(DataSet: TDataSet);
    procedure BtNiveisClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure FechaPesquisa();
  public
    { Public declarations }
    procedure ReopenGraGru1(Nivel1: Integer);
  end;

  var
  FmGraG1: TFmGraG1;

implementation

uses UnMyObjects, Module, DmkDAC_PF, GraGru1, MyDBCheck, UMySQLModule,
ModAppGraG1;

{$R *.DFM}

procedure TFmGraG1.BitBtn1Click(Sender: TObject);
  procedure UploadAtual();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragrux ',
    'WHERE gragru1=' + Geral.FF0(QrGraGru1Nivel1.Value),
    '']);
    if QrGraGruX.RecordCount = 0 then
    begin
      Geral.MB_Aviso('O produto n�mero ' + Geral.FF0(QrGraGru1Nivel1.Value) +
      ' n�o possui reduzido e n�o poder� ser subido para a web!');
    end else
    begin
       //Parei aqui
    end;
  end;
var
  Txt: String;
  I, N: Integer;
begin
  N := DBGGraGru1.SelectedRows.Count;
  if N < 2 then
    Txt := 'do item selecionado'
  else
    Txt := 'dos ' + Geral.FF0(DBGGraGru1.SelectedRows.Count) + ' itens selecionados';
  //
  if Geral.MB_Pergunta('Confirma o upload ' + Txt + '?') = ID_YES then
  begin
    if N > 1 then
    begin
      with DBGGraGru1.DataSource.DataSet do
      for I:= 0 to DBGGraGru1.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGGraGru1.SelectedRows.Items[i]));
        UploadAtual();
      end;
    end else
      UploadAtual();
  end;
end;

procedure TFmGraG1.BtAlteraClick(Sender: TObject);
begin
  DfModAppGraG1.AlteraProdutoTabelaPersonalizada(EdGraTabApp.ValueVariant,
    QrGraGru1Nivel1.Value, QrGraGru1Nome.Value, nil);
  //
  ReopenGraGru1(QrGraGru1Nivel1.Value);
end;

procedure TFmGraG1.BtIncluiClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType           := stIns;
    FmGraGru1.FQrGraGru1                := QrGraGru1;
    FmGraGru1.EdGraTabApp.ValueVariant  := EdGraTabApp.ValueVariant;
    FmGraGru1.EDTXT_GraTabApp.Text      := CBGraTabApp.Text;
    FmGraGru1.EdCodUsu_New.ValueVariant := UMyMod.BPGS1I32('GraGru1', 'CodUsu', '', '', tsPos, stIns, 0);
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    if QrGraGru1.Locate('Nivel1', VAR_CADASTRO, []) then
      BtAlteraClick(Self)
    else
      Geral.MB_ERRO('N�o foi poss�vel localizar o produto ' + Geral.FF0(
      VAR_CADASTRO) + ' para complementar os dados do seu cadastro!');
  end;
end;

procedure TFmGraG1.BtNiveisClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stUpd;
    FmGraGru1.FQrGraGru1 := QrGraGru1;
    //
    FmGraGru1.EdGraTabApp.ValueVariant := EdGraTabApp.ValueVariant;
    FmGraGru1.EDTXT_GraTabApp.Text     := CBGraTabApp.Text;
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdPrdGrupTip, FmGraGru1.CBPrdGrupTip,
      FmGraGru1.QrPrdGrupTip, QrGraGru1PrdGrupTip.Value);
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel5, FmGraGru1.CBNivel5,
      FmGraGru1.QrGraGru5, QrGraGru1Nivel5.Value, 'Nivel5', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel4, FmGraGru1.CBNivel4,
      FmGraGru1.QrGraGru4, QrGraGru1Nivel4.Value, 'Nivel4', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel3, FmGraGru1.CBNivel3,
      FmGraGru1.QrGraGru3, QrGraGru1Nivel3.Value, 'Nivel3', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel2, FmGraGru1.CBNivel2,
      FmGraGru1.QrGraGru2, QrGraGru1Nivel2.Value, 'Nivel2', 'CodUsu');
    //
    FmGraGru1.EdNivel1.ValueVariant     := QrGraGru1Nivel1.Value;
    FmGraGru1.EdCodUsu_Ant.ValueVariant := QrGraGru1CodUsu.Value;
    FmGraGru1.EdCodUsu_New.ValueVariant := QrGraGru1CodUsu.Value;
    FmGraGru1.EdNome_Ant.ValueVariant   := QrGraGru1Nome.Value;
    FmGraGru1.EdNome_New.ValueVariant   := QrGraGru1Nome.Value;
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
    //
    ReopenGraGru1(QrGraGru1Nivel1.Value);
  end;
end;

procedure TFmGraG1.ReopenGraGru1(Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
    'SELECT * ',
    'FROM gragru1 ',
    'WHERE GraTabApp=' + Geral.FF0(EdGraTabApp.ValueVariant),
    '']);
  //
  if (QrGraGru1.RecordCount > 0) and (Nivel1 <> 0) then
    QrGraGru1.Locate('Nivel1', Nivel1, []);
end;

procedure TFmGraG1.BtOKClick(Sender: TObject);
begin
  ReopenGraGru1(0);
end;

procedure TFmGraG1.BtRegularizaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrGTA, Dmod.MyDB, [
    'SELECT * ',
    'FROM gratabapp ',
    'ORDER BY Nome ',
    '']);
    QrGTA.First;
    while not QrGTA.Eof do
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE gragru1 ',
      'SET GraTabApp=' + Geral.FF0(QrGTACodigo.Value),
      'WHERE Nivel1 IN ( ',
      '     SELECT Nivel1 ',
      '     FROM ' + QrGTATabela.Value,
      ') ',
      '']);
      QrGTA.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGraG1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraG1.EdGraTabAppChange(Sender: TObject);
begin
  FechaPesquisa();
  BtOKClick(Self);
end;

procedure TFmGraG1.FechaPesquisa();
begin
  QrGraGru1.Close;
  BtInclui.Enabled := EdGraTabApp.ValueVariant > 0;
end;

procedure TFmGraG1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraG1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraTabApp, Dmod.MyDB, [
  'SELECT * ',
  'FROM gratabapp ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmGraG1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraG1.QrGraGru1AfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrGraGru1.RecordCount > 0;
  BtNiveis.Enabled := QrGraGru1.RecordCount > 0;
end;

procedure TFmGraG1.QrGraGru1BeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtNiveis.Enabled := False;
end;

end.
