unit GraGruYIncompleto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables;

type
  TFmGraGruYIncompleto = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DsIncompleto: TDataSource;
    QrIncompleto: TmySQLQuery;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenIncompleto(QryOrig: TmySQLQuery);
  end;

  var
  FmGraGruYIncompleto: TFmGraGruYIncompleto;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnGrade_Jan;

{$R *.DFM}

procedure TFmGraGruYIncompleto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruYIncompleto.DBGrid1DblClick(Sender: TObject);
var
  GraGruX, GraGruY: Integer;
begin
  if QrIncompleto.RecordCount > 0 then
  begin
    try
      GraGruX := QrIncompleto.FieldByName('GraGruX').AsInteger;
      GraGruY := QrIncompleto.FieldByName('GraGruY').AsInteger;
    except
      GraGruY := 0;
    end;
  end;
  if GraGruY <> 0 then
    //AppPF.MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX, True, nil);
    Grade_Jan.MostraFormGraGruY(GraGruY, GragruX, '');
end;

procedure TFmGraGruYIncompleto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruYIncompleto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmGraGruYIncompleto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruYIncompleto.ReopenIncompleto(QryOrig: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIncompleto, QryOrig.Database,
    [QryOrig.SQL.Text]);
  //Geral.MB_Info(QryOrig.SQL.Text);
end;

end.
