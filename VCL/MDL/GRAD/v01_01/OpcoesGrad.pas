unit OpcoesGrad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkCheckBox, dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, dmkDBGrid,
  frxClass, Vcl.Menus, frxDBSet, UnProjGroup_Consts;

type
  TFmOpcoesGrad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraTamCad: TmySQLQuery;
    QrGraTamCadCodigo: TIntegerField;
    QrGraTamCadCodUsu: TIntegerField;
    QrGraTamCadNome: TWideStringField;
    DsGraTamCad: TDataSource;
    QrGraCorCad: TmySQLQuery;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadCodUsu: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    DsGraCorCad: TDataSource;
    QrGraTamIts: TmySQLQuery;
    DsGraTamIts: TDataSource;
    QrGraTamItsControle: TIntegerField;
    QrGraTamItsNome: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    SbUnicoTam: TSpeedButton;
    Label3: TLabel;
    SbUnicaCor: TSpeedButton;
    Label1: TLabel;
    SbUnicoGra: TSpeedButton;
    EdUnicoTam: TdmkEditCB;
    CBUnicoTam: TdmkDBLookupComboBox;
    EdUnicaCor: TdmkEditCB;
    CBUnicaCor: TdmkDBLookupComboBox;
    EdUnicaGra: TdmkEditCB;
    CBUnicaGra: TdmkDBLookupComboBox;
    CkUsaCodFornece: TdmkCheckBox;
    RGCasasProd: TdmkRadioGroup;
    CkEditGraTabApp: TdmkCheckBox;
    CkUsaGrade: TdmkCheckBox;
    PnDados: TPanel;
    CkRstrPrdFat: TdmkCheckBox;
    PnRstrPrdFat: TPanel;
    Panel5: TPanel;
    Label4: TLabel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    CkNome_ex: TCheckBox;
    BtExcluiRstr: TBitBtn;
    BtIncluiRstr: TBitBtn;
    DBGRstrPrdFat: TdmkDBGrid;
    QrRstrPrdFat: TmySQLQuery;
    QrRstrPrdFatCodusu: TIntegerField;
    QrRstrPrdFatNivel1: TIntegerField;
    QrRstrPrdFatNome: TWideStringField;
    DsRstrPrdFat: TDataSource;
    DsGraGru1: TDataSource;
    QrGraGru1: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    VUGraGru1: TdmkValUsu;
    BtImprime: TBitBtn;
    frxOPT_GRADE_000_01: TfrxReport;
    PMImprime: TPopupMenu;
    Produtosporgrupo1: TMenuItem;
    Produtosporgrade1: TMenuItem;
    QrProdutosImp: TmySQLQuery;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    QrProdutosImpNome: TWideStringField;
    QrProdutosImpCor: TWideStringField;
    QrProdutosImpTam: TWideStringField;
    frxDsProdutosImp: TfrxDBDataset;
    frxOPT_GRADE_000_02: TfrxReport;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    EdNFePreMarca: TdmkEdit;
    Label5: TLabel;
    EdNFePosMarca: TdmkEdit;
    Label6: TLabel;
    RGNFeUsaMarca: TRadioGroup;
    Panel10: TPanel;
    Panel11: TPanel;
    RGNFeUsaReferencia: TRadioGroup;
    Panel12: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    EdNFePreReferencia: TdmkEdit;
    EdNFePosReferencia: TdmkEdit;
    CkLoadXMLPorLoteFab: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbUnicoTamClick(Sender: TObject);
    procedure SbUnicaCorClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrGraTamCadBeforeClose(DataSet: TDataSet);
    procedure EdUnicaGraChange(Sender: TObject);
    procedure CkNome_exClick(Sender: TObject);
    procedure BtIncluiRstrClick(Sender: TObject);
    procedure BtExcluiRstrClick(Sender: TObject);
    procedure CkRstrPrdFatClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Produtosporgrade1Click(Sender: TObject);
    procedure Produtosporgrupo1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrGraTamCadAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenGraGru1();
    procedure ReopenRstrPrdFat();
    procedure ConfiguraRstrPrdFat(Mostra: Boolean);
    procedure ImprimeListaProdutos(AgruparProduto: Boolean);
  public
    { Public declarations }
    procedure ReopenGraTamIts(Codigo: Integer);
  end;

  var
  FmOpcoesGrad: TFmOpcoesGrad;

implementation

uses UnMyObjects, UMySQLModule, Module, MyDBCheck, GraTamCad, GraCorCad,
  ModProd, ModuleGeral, DmkDAC_PF, MyListas;

{$R *.DFM}

procedure TFmOpcoesGrad.BtExcluiRstrClick(Sender: TObject);
var
  Nivel1, CodUsu: Integer;
begin
  if (QrRstrPrdFat.State <> dsInactive) and (QrRstrPrdFat.RecordCount > 0) then
  begin
    CodUsu := QrRstrPrdFatCodUsu.Value;
    Nivel1 := QrRstrPrdFatNivel1.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do produto ' + Geral.FF0(CodUsu) +
      '?', 'rstrprdfat', 'Nivel1', Nivel1, Dmod.MyDB) = ID_YES then
    begin
      ReopenRstrPrdFat;
      ReopenGraGru1;
    end;
  end;
end;

procedure TFmOpcoesGrad.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmOpcoesGrad.BtIncluiRstrClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  if EdGraGru1.ValueVariant <> 0 then
    Nivel1 := VUGraGru1.ValueVariant
  else
    Nivel1 := 0;
  //
  if MyObjects.FIC(Nivel1 = 0, EdGraGru1, 'Informe o produto!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'rstrprdfat', False,
    [], ['Nivel1'], [], [Nivel1], True) then
  begin
    ReopenRstrPrdFat;
    ReopenGraGru1;
    //
    EdGraGru1.ValueVariant := 0;
    CBGraGru1.KeyValue     := Null;
    EdGraGru1.SetFocus;
  end;
end;

procedure TFmOpcoesGrad.BtOKClick(Sender: TObject);
var
  Codigo, UsaGrade, UnicaCor, UnicaGra, UnicoTam, UsaCodFornece, EditGraTabApp,
  RstrPrdFat, NFeUsaMarca, NFeUsaReferencia, LoadXMLPorLoteFab: Integer;
  NFePreMarca, NFePosMarca, NFePreReferencia, NFePosReferencia: String;
begin
  Codigo         := 1;
  UsaGrade       := Geral.BoolToInt(CkUsaGrade.Checked);
  // ini 2022-05-24
  //UMyMod.ObtemCodigoDeCodUsu(EdUnicaCor, UnicaCor, '');
  //UMyMod.ObtemCodigoDeCodUsu(EdUnicaGra, UnicaGra, '');
  UnicaCor       := EdUnicaCor.ValueVariant;
  UnicaGra       := EdUnicaGra.ValueVariant;
  // fim 2022-05-24
  UnicoTam       := EdUnicoTam.ValueVariant;
  UsaCodFornece  := Geral.BoolToInt(CkUsaCodFornece.Checked);
  EditGraTabApp  := Geral.BoolToInt(CkEditGraTabApp.Checked);
  RstrPrdFat     := Geral.BoolToInt(CkRstrPrdFat.Checked);
  //
  NFeUsaMarca      := RGNFeUsaMarca.ItemIndex;
  NFePreMarca      := EdNFePreMarca.ValueVariant;
  NFePosMarca      := EdNFePosMarca.ValueVariant;
  NFeUsaReferencia := RGNFeUsaReferencia.ItemIndex;
  NFePreReferencia := EdNFePreReferencia.ValueVariant;
  NFePosReferencia := EdNFePosReferencia.ValueVariant;
  LoadXMLPorLoteFab := Geral.BoolToInt(CkLoadXMLPorLoteFab.Checked);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'opcoesgrad', False,
    ['UsaGrade', 'UnicaCor', 'UnicaGra', 'UnicoTam', 'UsaCodFornece',
    'EditGraTabApp', 'RstrPrdFat',
    'NFeUsaMarca', 'NFePreMarca', 'NFePosMarca',
    'NFeUsaReferencia', 'NFePreReferencia', 'NFePosReferencia',
    'LoadXMLPorLoteFab'], ['Codigo'],
    [UsaGrade, UnicaCor, UnicaGra, UnicoTam, UsaCodFornece,
    EditGraTabApp, RstrPrdFat,
    NFeUsaMarca, NFePreMarca, NFePosMarca,
    NFeUsaReferencia, NFePreReferencia, NFePosReferencia,
    LoadXMLPorLoteFab], [Codigo], True) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Controle', False,
      ['CasasProd'], ['Codigo'], [RGCasasProd.ItemIndex], ['1'], False) then
    begin
      UMyMod.AbreQuery(DmodG.QrControle, Dmod.MyDB);
      DmProd.ReopenOpcoesGrad();
      Close;
    end;
  end;
end;

procedure TFmOpcoesGrad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesGrad.CkNome_exClick(Sender: TObject);
begin
  if CkNome_ex.Checked then
    CBGraGru1.ListField := 'NOME_EX'
  else
    CBGraGru1.ListField := 'Nome';
end;

procedure TFmOpcoesGrad.ConfiguraRstrPrdFat(Mostra: Boolean);
begin
  if Mostra then
  begin
    PnRstrPrdFat.Visible := True;
    //
    ReopenGraGru1;
    ReopenRstrPrdFat;
  end else
  begin
    PnRstrPrdFat.Visible := False;
    //
    QrGraGru1.Close;
    QrRstrPrdFat.Close;
  end;
end;

procedure TFmOpcoesGrad.CkRstrPrdFatClick(Sender: TObject);
begin
  ConfiguraRstrPrdFat(CkRstrPrdFat.Checked);
end;

procedure TFmOpcoesGrad.EdUnicaGraChange(Sender: TObject);
begin
  ReopenGraTamIts(QrGraTamCadCodigo.Value);
end;

procedure TFmOpcoesGrad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesGrad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CkEditGraTabApp.Visible := CO_DMKID_APP = 24; //Bugstrol => Habilitar somente para os aplicativo que usam
  //
  PageControl1.ActivePageIndex := 0;
  BtImprime.Visible            := False;
  //
  UMyMod.AbreQuery(QrGraTamCad, Dmod.MyDB);
  UMyMod.AbreQuery(QrGraCorCad, Dmod.MyDB);
  UMyMod.AbreQuery(DmodG.QrControle, Dmod.MyDB);
  //
  CBGraGru1.ListSource     := DsGraGru1;
  DBGRstrPrdFat.DataSource := DsRstrPrdFat;
  //
end;

procedure TFmOpcoesGrad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesGrad.FormShow(Sender: TObject);
begin
  ConfiguraRstrPrdFat(CkRstrPrdFat.Checked);
end;

procedure TFmOpcoesGrad.ImprimeListaProdutos(AgruparProduto: Boolean);
var
  SQL: String;
  frxRep: TfrxReport;
begin
  if AgruparProduto = True then
  begin
    SQL    := 'GROUP BY rpf.Nivel1 ';
    frxRep := frxOPT_GRADE_000_02;
  end else
  begin
    SQL    := '';
    frxRep := frxOPT_GRADE_000_01;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProdutosImp, Dmod.MyDB, [
    'SELECT gg1.CodUsu, gg1.Nivel1, gg1.Nome, gcc.Nome Cor, gti.Nome Tam ',
    'FROM rstrprdfat rpf ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1 = rpf.Nivel1 ',
    'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    SQL,
    'ORDER BY Nome, Cor, Tam ',
    '']);
  if QrProdutosImp.RecordCount > 0 then
  begin
    MyObjects.frxDefineDataSets(frxRep, [
      DModG.frxDsDono,
      frxDsProdutosImp
      ]);
    //
    MyObjects.frxMostra(frxRep, Caption);
  end;
end;

procedure TFmOpcoesGrad.PageControl1Change(Sender: TObject);
begin
  BtImprime.Visible := PageControl1.ActivePageIndex = 1;
end;

procedure TFmOpcoesGrad.Produtosporgrade1Click(Sender: TObject);
begin
  ImprimeListaProdutos(False);
end;

procedure TFmOpcoesGrad.Produtosporgrupo1Click(Sender: TObject);
begin
  ImprimeListaProdutos(True);
end;

procedure TFmOpcoesGrad.QrGraTamCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraTamIts(QrGraTamCadCodigo.Value);
end;

procedure TFmOpcoesGrad.QrGraTamCadBeforeClose(DataSet: TDataSet);
begin
  QrGraTamIts.Close;
end;

procedure TFmOpcoesGrad.ReopenGraTamIts(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraTamIts, Dmod.MyDB, [
    'SELECT Controle, Nome ',
    'FROM gratamits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Nome ',
    '']);
  //
  EdUnicoTam.ValueVariant := 0;
  CBUnicoTam.KeyValue := 0;
end;

procedure TFmOpcoesGrad.ReopenRstrPrdFat;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRstrPrdFat, Dmod.MyDB, [
    'SELECT gg1.CodUsu, gg1.Nivel1, gg1.Nome ',
    'FROM rstrprdfat rtf ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1 = rtf.Nivel1 ',
    'ORDER BY gg1.Nome ',
    '']);
end;

procedure TFmOpcoesGrad.ReopenGraGru1;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
    'SELECT gg1.CodUsu, gg1.Nivel1, gg1.Nome, ',
    'CONCAT(LPAD(gg1.CodUsu, 8, "0"), " - ", gg1.Nome) NOME_EX ',
    'FROM gragru1 gg1 ',
    'WHERE gg1.Nivel1 > 0 ',
    'AND gg1.Nivel1 NOT IN ',
    '( ',
    'SELECT Nivel1 ',
    'FROM RstrPrdFat ',
    ') ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmOpcoesGrad.SbUnicaCorClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraCorCad, FmGraCorCad, afmoNegarComAviso) then
  begin
    FmGraCorCad.ShowModal;
    FmGraCorCad.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdUnicaCor, CBUnicaCor, QrGraCorCad, VAR_CADASTRO);
end;

procedure TFmOpcoesGrad.SbUnicoTamClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraTamCad, FmGraTamCad, afmoNegarComAviso) then
  begin
    FmGraTamCad.ShowModal;
    FmGraTamCad.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdUnicaGra, CBUnicaGra, QrGraTamCad, VAR_CADASTRO);
  UMyMod.SetaCodigoPesquisado(EdUnicoTam, CBUnicoTam, QrGraTamIts, VAR_CAD_ITEM, 'Controle');
end;

end.

