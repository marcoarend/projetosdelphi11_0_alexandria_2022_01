object FmStqInnPack: TFmStqInnPack
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-047 :: Item de Tabela de Presta'#231#227'o de Servi'#231'o'
  ClientHeight = 315
  ClientWidth = 964
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 964
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 916
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 868
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 460
        Height = 32
        Caption = ' Fracionamento - Entrada por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 460
        Height = 32
        Caption = ' Fracionamento - Entrada por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 460
        Height = 32
        Caption = ' Fracionamento - Entrada por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 201
    Width = 964
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 960
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 245
    Width = 964
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 818
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 816
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 964
    Height = 153
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 964
      Height = 70
      Align = alTop
      Caption = ' Item j'#225' FRACIONADO: '
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 960
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PainelGGXFrac: TPanel
          Left = 221
          Top = 0
          Width = 739
          Height = 53
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Label12: TLabel
            Left = 4
            Top = 8
            Width = 76
            Height = 13
            Caption = 'Nome do grupo:'
          end
          object Label13: TLabel
            Left = 248
            Top = 8
            Width = 19
            Height = 13
            Caption = 'Cor:'
            FocusControl = DBEdit2
          end
          object Label14: TLabel
            Left = 392
            Top = 8
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
            FocusControl = DBEdit3
          end
          object Label1: TLabel
            Left = 464
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Unidade:'
            FocusControl = DBEdit4
          end
          object Label2: TLabel
            Left = 552
            Top = 8
            Width = 107
            Height = 13
            Caption = 'Descri'#231#227'o da unidade:'
            FocusControl = DBEdit5
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 24
            Width = 240
            Height = 21
            TabStop = False
            DataField = 'NO_GG1'
            DataSource = DsGGXFrac
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 248
            Top = 24
            Width = 140
            Height = 21
            TabStop = False
            DataField = 'NO_COR'
            DataSource = DsGGXFrac
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 392
            Top = 24
            Width = 69
            Height = 21
            TabStop = False
            DataField = 'NO_TAM'
            DataSource = DsGGXFrac
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 464
            Top = 24
            Width = 82
            Height = 21
            DataField = 'Sigla'
            DataSource = DsGGXFrac
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 551
            Top = 24
            Width = 112
            Height = 21
            DataField = 'No_SIGLA'
            DataSource = DsGGXFrac
            TabOrder = 4
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 221
          Height = 53
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label11: TLabel
            Left = 8
            Top = 8
            Width = 69
            Height = 13
            Caption = 'Reduzido: [F4]'
          end
          object SpeedButton1: TSpeedButton
            Left = 88
            Top = 24
            Width = 21
            Height = 21
            Caption = '?'
            OnClick = SpeedButton1Click
          end
          object Label9: TLabel
            Left = 112
            Top = 8
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object EdGraGruX: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GraGruX'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGraGruXChange
            OnKeyDown = EdGraGruXKeyDown
          end
          object EdQtde: TdmkEdit
            Left = 112
            Top = 24
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 70
      Width = 964
      Height = 70
      Align = alTop
      Caption = ' PACOTE antes de fracionar: '
      TabOrder = 1
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 960
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object PainelGGXPack: TPanel
          Left = 221
          Top = 0
          Width = 739
          Height = 56
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Label3: TLabel
            Left = 4
            Top = 8
            Width = 76
            Height = 13
            Caption = 'Nome do grupo:'
          end
          object Label4: TLabel
            Left = 248
            Top = 8
            Width = 19
            Height = 13
            Caption = 'Cor:'
            FocusControl = DBEdit7
          end
          object Label5: TLabel
            Left = 392
            Top = 8
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
            FocusControl = DBEdit8
          end
          object Label6: TLabel
            Left = 464
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Unidade:'
            FocusControl = DBEdit9
          end
          object Label7: TLabel
            Left = 552
            Top = 8
            Width = 107
            Height = 13
            Caption = 'Descri'#231#227'o da unidade:'
            FocusControl = DBEdit10
          end
          object DBEdit6: TDBEdit
            Left = 4
            Top = 24
            Width = 240
            Height = 21
            TabStop = False
            DataField = 'NO_GG1'
            DataSource = DsGGXPack
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 248
            Top = 24
            Width = 140
            Height = 21
            TabStop = False
            DataField = 'NO_COR'
            DataSource = DsGGXPack
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 392
            Top = 24
            Width = 69
            Height = 21
            TabStop = False
            DataField = 'NO_TAM'
            DataSource = DsGGXPack
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 464
            Top = 24
            Width = 82
            Height = 21
            DataField = 'Sigla'
            DataSource = DsGGXPack
            TabOrder = 3
          end
          object DBEdit10: TDBEdit
            Left = 551
            Top = 24
            Width = 112
            Height = 21
            DataField = 'No_SIGLA'
            DataSource = DsGGXPack
            TabOrder = 4
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 221
          Height = 56
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label8: TLabel
            Left = 8
            Top = 8
            Width = 69
            Height = 13
            Caption = 'Reduzido: [F4]'
          end
          object SpeedButton2: TSpeedButton
            Left = 88
            Top = 24
            Width = 21
            Height = 21
            Caption = '?'
            OnClick = SpeedButton2Click
          end
          object Label10: TLabel
            Left = 112
            Top = 8
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object EdPackGGX: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GraGruX'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdPackGGXChange
            OnKeyDown = EdPackGGXKeyDown
          end
          object EdPackQtde: TdmkEdit
            Left = 112
            Top = 24
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrGGXFrac: TMySQLQuery
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX,  '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM, '
      'med.Nome, med.Sigla, med.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ')
    Left = 224
    Top = 48
    object QrGGXFracNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGGXFracNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGGXFracNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGGXFracGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGGXFracHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGGXFracGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGGXFracNo_SIGLA: TWideStringField
      FieldName = 'No_SIGLA'
      Size = 30
    end
    object QrGGXFracSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGGXFracGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGGXFracFracio: TIntegerField
      FieldName = 'Fracio'
    end
    object QrGGXFracUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object DsGGXFrac: TDataSource
    DataSet = QrGGXFrac
    Left = 224
    Top = 96
  end
  object QrGGXPack: TMySQLQuery
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX,  '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.Nome NO_GG1,  '
      'gcc.Nome NO_COR,  gti.Nome NO_TAM, '
      'med.Nome, med.Sigla, med.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ')
    Left = 312
    Top = 52
    object QrGGXPackNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 30
    end
    object QrGGXPackNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGGXPackNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGGXPackGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGGXPackHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGGXPackGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGGXPackNo_SIGLA: TWideStringField
      FieldName = 'No_SIGLA'
      Size = 30
    end
    object QrGGXPackSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrGGXPackGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGGXPackFracio: TIntegerField
      FieldName = 'Fracio'
    end
    object QrGGXPackUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object DsGGXPack: TDataSource
    DataSet = QrGGXPack
    Left = 312
    Top = 100
  end
end
