object FmMatFichCab: TFmMatFichCab
  Left = 339
  Top = 185
  Caption = 'MAT-FICHS-001 :: Fichas de Materiais'
  ClientHeight = 588
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 432
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 111
    ExplicitHeight = 398
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 281
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 244
        Height = 281
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitHeight = 279
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 244
          Height = 93
          Align = alTop
          DataSource = DsPrdGrupTip
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 140
              Visible = True
            end>
        end
        object Panel4: TPanel
          Left = 0
          Top = 93
          Width = 244
          Height = 188
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitLeft = 1
          ExplicitTop = 94
          ExplicitWidth = 242
          ExplicitHeight = 184
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 244
            Height = 36
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 242
            object LaNivel1: TLabel
              Left = 0
              Top = 0
              Width = 89
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 'Grupo de Produtos'
            end
            object EdFiltroNiv1: TEdit
              Left = 0
              Top = 15
              Width = 244
              Height = 21
              Align = alBottom
              TabOrder = 0
              OnExit = EdFiltroNiv1Exit
              ExplicitWidth = 242
            end
          end
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 36
            Width = 244
            Height = 152
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 140
                Visible = True
              end>
            Color = clWindow
            DataSource = DsGraGru1
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = dmkDBGrid1CellClick
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 140
                Visible = True
              end>
          end
        end
      end
      object Panel7: TPanel
        Left = 244
        Top = 0
        Width = 764
        Height = 281
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 245
        ExplicitTop = 1
        ExplicitWidth = 762
        ExplicitHeight = 279
        object GroupBox5: TGroupBox
          Left = 0
          Top = 236
          Width = 764
          Height = 45
          Align = alBottom
          Caption = ' Sele'#231#227'o: '
          Color = clBtnFace
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          ExplicitLeft = 1
          ExplicitTop = 233
          ExplicitWidth = 760
          object EdSelecao: TdmkEdit
            Left = 12
            Top = 16
            Width = 285
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSelCodi: TdmkEdit
            Left = 296
            Top = 16
            Width = 89
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdSelNome: TdmkEdit
            Left = 384
            Top = 16
            Width = 372
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PageControl1: TPageControl
          Left = 0
          Top = 0
          Width = 764
          Height = 236
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 1
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitWidth = 760
          ExplicitHeight = 232
          object TabSheet1: TTabSheet
            Caption = ' Grade '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 752
            ExplicitHeight = 204
            object GradeC: TStringGrid
              Left = 0
              Top = 0
              Width = 756
              Height = 208
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnClick = GradeCClick
              OnDrawCell = GradeCDrawCell
              ExplicitWidth = 752
              ExplicitHeight = 204
              RowHeights = (
                18
                18)
            end
          end
          object TabSheet2: TTabSheet
            Caption = ' Ativos '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeA: TStringGrid
              Left = 0
              Top = 0
              Width = 756
              Height = 208
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeADrawCell
              RowHeights = (
                18
                18)
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' X '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeX: TStringGrid
              Left = 0
              Top = 0
              Width = 756
              Height = 208
              Align = alClient
              ColCount = 2
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
              TabOrder = 0
              OnDrawCell = GradeXDrawCell
              RowHeights = (
                18
                18)
            end
          end
        end
      end
    end
    object GradeItens: TdmkDBGrid
      Left = 0
      Top = 281
      Width = 1008
      Height = 151
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_Parte'
          Title.Caption = 'Parte'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MatPartCad'
          Title.Caption = 'Descri'#231#227'o'
          Width = 152
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PERMISSOES'
          Title.Caption = 'Material que poder'#225' ser usado'
          Width = 370
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ABRANGENCIA'
          Title.Caption = 'Material que receber'#225' outros materiais'
          Width = 370
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsGraMatIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_Parte'
          Title.Caption = 'Parte'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MatPartCad'
          Title.Caption = 'Descri'#231#227'o'
          Width = 152
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PERMISSOES'
          Title.Caption = 'Material que poder'#225' ser usado'
          Width = 370
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ABRANGENCIA'
          Title.Caption = 'Material que receber'#225' outros materiais'
          Width = 370
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 237
        Height = 32
        Caption = 'Fichas de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 237
        Height = 32
        Caption = 'Fichas de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 237
        Height = 32
        Caption = 'Fichas de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 480
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 524
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtExcluiClick
      end
    end
  end
  object QrPrdGrupTip: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPrdGrupTipBeforeClose
    AfterScroll = QrPrdGrupTipAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM prdgruptip'
      'WHERE TipPrd = 1')
    Left = 8
    Top = 8
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 36
    Top = 8
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru1BeforeClose
    BeforeScroll = QrGraGru1BeforeScroll
    AfterScroll = QrGraGru1AfterScroll
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome, GraTamCad'
      'FROM gragru1'
      'WHERE PrdGrupTip=:P0'
      'AND Nome LIKE :P1')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 92
    Top = 8
  end
  object QrGraMatIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraMatItsAfterOpen
    BeforeClose = QrGraMatItsBeforeClose
    OnCalcFields = QrGraMatItsCalcFields
    SQL.Strings = (
      'SELECT ELT(mpc.Parte, "Secund'#225'ria", "Principal",'
      '"? ? ?" ) NO_Parte, mpc.Nome NO_MatPartCad,'
      
        'ELT(NivAbrange, "Reduzido","Tamanho", "Cor", "Grupo", "Tipo", "?' +
        '") Abrange_TXT,'
      ''
      'pgt1.CodUsu CU1_PrdGrupTip, pgt1.Nome NO1_PrdGrupTip,'
      'gg11.CodUsu CU1_GraGru1, gg11.Nome NO1_GraGru1, ggc1.GraCorCad, '
      'gcc1.CodUsu CU1_COR, gcc1.Nome NO1_COR, gti1.Nome NO1_TAM, '
      ''
      'pgt2.CodUsu CU2_PrdGrupTip, pgt2.Nome NO2_PrdGrupTip,'
      'gg12.CodUsu CU2_GraGru1, gg12.Nome NO2_GraGru1, ggc2.GraCorCad, '
      'gcc2.CodUsu CU2_COR, gcc2.Nome NO2_COR, gti2.Nome NO2_TAM, '
      'gmi.*'
      'FROM gramatits gmi'
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=gmi.MatPartCad'
      ''
      'LEFT JOIN gragrux   ggx1 ON ggx1.Controle=gmi.AbrangeGgx'
      'LEFT JOIN gragru1   gg11 ON gg11.Nivel1=gmi.AbrangeGg1'
      'LEFT JOIN gragruc   ggc1 ON ggc1.Controle=gmi.AbrangeGgc'
      'LEFT JOIN gracorcad gcc1 ON gcc1.Codigo=ggc1.GraCorCad'
      'LEFT JOIN gratamits gti1 ON gti1.Controle=gmi.AbrangeGti'
      'LEFT JOIN prdgruptip pgt1 ON pgt1.Codigo=gmi.AbrangePgt'
      ''
      'LEFT JOIN gragrux ggx2 ON ggx2.Controle=gmi.PermiteGgx'
      'LEFT JOIN gragru1   gg12 ON gg12.Nivel1=gmi.PermiteGg1'
      'LEFT JOIN gragruc   ggc2 ON ggc2.Controle=gmi.PermiteGgc'
      'LEFT JOIN gracorcad gcc2 ON gcc2.Codigo=ggc2.GraCorCad'
      'LEFT JOIN gratamits gti2 ON gti2.Controle=gmi.PermiteGti'
      'LEFT JOIN prdgruptip pgt2 ON pgt2.Codigo=gmi.PermitePgt'
      ''
      ''
      ''
      '')
    Left = 120
    Top = 8
    object QrGraMatItsNO_Parte: TWideStringField
      FieldName = 'NO_Parte'
      Size = 10
    end
    object QrGraMatItsNO_MatPartCad: TWideStringField
      FieldName = 'NO_MatPartCad'
      Size = 50
    end
    object QrGraMatItsAbrange_TXT: TWideStringField
      FieldName = 'Abrange_TXT'
      Size = 8
    end
    object QrGraMatItsCU1_PrdGrupTip: TIntegerField
      FieldName = 'CU1_PrdGrupTip'
      Required = True
    end
    object QrGraMatItsNO1_PrdGrupTip: TWideStringField
      FieldName = 'NO1_PrdGrupTip'
      Size = 30
    end
    object QrGraMatItsCU1_GraGru1: TIntegerField
      FieldName = 'CU1_GraGru1'
      Required = True
    end
    object QrGraMatItsNO1_GraGru1: TWideStringField
      FieldName = 'NO1_GraGru1'
      Size = 30
    end
    object QrGraMatItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrGraMatItsCU1_COR: TIntegerField
      FieldName = 'CU1_COR'
      Required = True
    end
    object QrGraMatItsNO1_COR: TWideStringField
      FieldName = 'NO1_COR'
      Size = 30
    end
    object QrGraMatItsNO1_TAM: TWideStringField
      FieldName = 'NO1_TAM'
      Size = 5
    end
    object QrGraMatItsCU2_PrdGrupTip: TIntegerField
      FieldName = 'CU2_PrdGrupTip'
      Required = True
    end
    object QrGraMatItsNO2_PrdGrupTip: TWideStringField
      FieldName = 'NO2_PrdGrupTip'
      Size = 30
    end
    object QrGraMatItsCU2_GraGru1: TIntegerField
      FieldName = 'CU2_GraGru1'
      Required = True
    end
    object QrGraMatItsNO2_GraGru1: TWideStringField
      FieldName = 'NO2_GraGru1'
      Size = 30
    end
    object QrGraMatItsGraCorCad_1: TIntegerField
      FieldName = 'GraCorCad_1'
    end
    object QrGraMatItsCU2_COR: TIntegerField
      FieldName = 'CU2_COR'
      Required = True
    end
    object QrGraMatItsNO2_COR: TWideStringField
      FieldName = 'NO2_COR'
      Size = 30
    end
    object QrGraMatItsNO2_TAM: TWideStringField
      FieldName = 'NO2_TAM'
      Size = 5
    end
    object QrGraMatItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraMatItsNivAbrange: TSmallintField
      FieldName = 'NivAbrange'
    end
    object QrGraMatItsAbrangePgt: TIntegerField
      FieldName = 'AbrangePgt'
    end
    object QrGraMatItsAbrangeGg1: TIntegerField
      FieldName = 'AbrangeGg1'
    end
    object QrGraMatItsAbrangeGgc: TIntegerField
      FieldName = 'AbrangeGgc'
    end
    object QrGraMatItsAbrangeGti: TIntegerField
      FieldName = 'AbrangeGti'
    end
    object QrGraMatItsAbrangeGgx: TIntegerField
      FieldName = 'AbrangeGgx'
    end
    object QrGraMatItsNivPermite: TSmallintField
      FieldName = 'NivPermite'
    end
    object QrGraMatItsPermitePgt: TIntegerField
      FieldName = 'PermitePgt'
    end
    object QrGraMatItsPermiteGg1: TIntegerField
      FieldName = 'PermiteGg1'
    end
    object QrGraMatItsPermiteGgc: TIntegerField
      FieldName = 'PermiteGgc'
    end
    object QrGraMatItsPermiteGti: TIntegerField
      FieldName = 'PermiteGti'
    end
    object QrGraMatItsPermiteGgx: TIntegerField
      FieldName = 'PermiteGgx'
    end
    object QrGraMatItsMatPartCad: TIntegerField
      FieldName = 'MatPartCad'
    end
    object QrGraMatItsABRANGENCIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ABRANGENCIA'
      Size = 255
      Calculated = True
    end
    object QrGraMatItsPERMISSOES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERMISSOES'
      Size = 255
      Calculated = True
    end
  end
  object DsGraMatIts: TDataSource
    DataSet = QrGraMatIts
    Left = 148
    Top = 8
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 132
    Top = 364
    object Alteraparte1: TMenuItem
      Caption = 'Altera &Parte'
      OnClick = Alteraparte1Click
    end
    object AlteraAbrangncia1: TMenuItem
      Caption = '&Altera abrang'#234'cia (pela sele'#231#227'o da grade)'
      Enabled = False
    end
  end
end
