object FmFisRegUFx: TFmFisRegUFx
  Left = 339
  Top = 185
  Caption = 'FIS-REGRA-005 :: Exce'#231#227'o de Regra Fiscal'
  ClientHeight = 580
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 479
    Height = 418
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label6: TLabel
      Left = 0
      Top = 44
      Width = 479
      Height = 13
      Align = alTop
      Caption = 
        'Aten'#231#227'o! As informa'#231#245'es aqui cadastradas sobrep'#245'e qualquer outra' +
        '!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 389
    end
    object Panel3: TPanel
      Left = 0
      Top = 265
      Width = 479
      Height = 153
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 479
        Height = 101
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object RGNivel: TRadioGroup
          Left = 0
          Top = 0
          Width = 479
          Height = 54
          Align = alTop
          Caption = ' N'#237'vel de exce'#231#227'o: '
          Columns = 6
          ItemIndex = 0
          Items.Strings = (
            'Nenhum'
            'Produto'
            'Nivel 2'
            'Nivel 3'
            'Tipo'
            'Tudo')
          TabOrder = 0
          OnClick = RGNivelClick
        end
        object Panel9: TPanel
          Left = 0
          Top = 54
          Width = 479
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label5: TLabel
            Left = 8
            Top = 4
            Width = 175
            Height = 13
            Caption = 'C'#243'digo do item do n'#237'vel selecionado:'
          end
          object EdCodNiv: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCodNiv
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCodNiv: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 405
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCodNivel
            TabOrder = 1
            dmkEditCB = EdCodNiv
            UpdType = utYes
            LocF7SQLText.Strings = (
              '')
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 101
        Width = 479
        Height = 52
        Align = alBottom
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label24: TLabel
          Left = 8
          Top = 8
          Width = 49
          Height = 13
          Caption = 'Minha UF:'
          Enabled = False
        end
        object Label1: TLabel
          Left = 64
          Top = 8
          Width = 41
          Height = 13
          Caption = 'UF terc.:'
          Enabled = False
        end
        object Label2: TLabel
          Left = 120
          Top = 8
          Width = 40
          Height = 13
          Caption = '% ICMS:'
        end
        object Label3: TLabel
          Left = 184
          Top = 8
          Width = 31
          Height = 13
          Caption = '% PIS:'
          Enabled = False
          Visible = False
        end
        object Label4: TLabel
          Left = 248
          Top = 8
          Width = 53
          Height = 13
          Caption = '% COFINS:'
          Enabled = False
          Visible = False
        end
        object Label12: TLabel
          Left = 312
          Top = 9
          Width = 131
          Height = 13
          Caption = 'I105f - C'#243'd. beneficio fiscal:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object EdUFEmit: TdmkEdit
          Left = 9
          Top = 24
          Width = 52
          Height = 21
          CharCase = ecUpperCase
          Enabled = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtUF
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'EUF'
          UpdCampo = 'EUF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdUFDest: TdmkEdit
          Left = 65
          Top = 24
          Width = 52
          Height = 21
          CharCase = ecUpperCase
          Enabled = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtUF
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'EUF'
          UpdCampo = 'EUF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdICMSAliq: TdmkEdit
          Left = 120
          Top = 24
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPISAliq: TdmkEdit
          Left = 184
          Top = 24
          Width = 60
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCOFINSAliq: TdmkEdit
          Left = 248
          Top = 24
          Width = 60
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdcBenef: TdmkEdit
          Left = 312
          Top = 24
          Width = 157
          Height = 21
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'cBenef'
          UpdCampo = 'cBenef'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 479
      Height = 44
      Align = alTop
      Caption = 'Panel4'
      TabOrder = 2
      object DBGFisRegCFOP: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 477
        Height = 42
        TabStop = False
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CFOP'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contribui'
            Title.Caption = 'TIE'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Interno'
            Title.Caption = 'VMU'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Proprio'
            Title.Caption = 'PFP'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubsTrib'
            Title.Caption = ' ST'
            Width = 30
            Visible = True
          end>
        Color = clWindow
        DataSource = FmFisRegCad.DsFisRegCFOP
        Enabled = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CFOP'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contribui'
            Title.Caption = 'TIE'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Interno'
            Title.Caption = 'VMU'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Proprio'
            Title.Caption = 'PFP'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubsTrib'
            Title.Caption = ' ST'
            Width = 30
            Visible = True
          end>
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 57
      Width = 479
      Height = 208
      Align = alTop
      Caption = '  Informa'#231#245'es exclusivas: '
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 2
        Top = 15
        Width = 475
        Height = 90
        Align = alTop
        Caption = ' N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST ICMS): [F3] '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label9: TLabel
          Left = 8
          Top = 44
          Width = 356
          Height = 13
          Caption = 
            'N12a - C'#243'digo de Situa'#231#227'o da Opera'#231#227'o - Simples Nacional (CSOSN)' +
            ': [F3] '
        end
        object EdCST_B: TdmkEdit
          Left = 8
          Top = 20
          Width = 29
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CST_B'
          UpdCampo = 'CST_B'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdCST_BChange
          OnKeyDown = EdCST_BKeyDown
        end
        object EdTextoB: TdmkEdit
          Left = 39
          Top = 20
          Width = 426
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCSOSN: TdmkEdit
          Left = 8
          Top = 60
          Width = 29
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CSOSN'
          UpdCampo = 'CSOSN'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdCSOSNChange
          OnKeyDown = EdCSOSNKeyDown
        end
        object EdTextoC: TdmkEdit
          Left = 39
          Top = 60
          Width = 426
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 2
        Top = 161
        Width = 475
        Height = 45
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        object Label7: TLabel
          Left = 8
          Top = 20
          Width = 204
          Height = 13
          Caption = 'Percentual de redu'#231#227'o da base de c'#225'lculo:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 280
          Top = 20
          Width = 123
          Height = 13
          Caption = 'Percentual de diferimento:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object EdpRedBC: TdmkEdit
          Left = 216
          Top = 16
          Width = 57
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'pRedBC'
          UpdCampo = 'pRedBC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdCST_BChange
          OnKeyDown = EdCST_BKeyDown
        end
        object EdpDif: TdmkEdit
          Left = 412
          Top = 16
          Width = 57
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'pRedBC'
          UpdCampo = 'pRedBC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdCST_BChange
          OnKeyDown = EdCST_BKeyDown
        end
      end
      object RGmodBC: TdmkRadioGroup
        Left = 2
        Top = 105
        Width = 475
        Height = 56
        Align = alTop
        Caption = 'Modalidade de determina'#231#227'o da BC do ICMS: '
        Columns = 2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemIndex = 3
        Items.Strings = (
          'Margem valor agregado (%)'
          'Pauta (valor)'
          'Pre'#231'o tabelado m'#225'ximo (valor)'
          'Valor da opera'#231#227'o')
        ParentFont = False
        TabOrder = 2
        QryCampo = 'modBC'
        UpdCampo = 'modBC'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 431
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 383
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 303
        Height = 32
        Caption = 'Exce'#231#227'o de Regra Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 303
        Height = 32
        Caption = 'Exce'#231#227'o de Regra Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 303
        Height = 32
        Caption = 'Exce'#231#227'o de Regra Fiscal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 510
    Width = 479
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 368
      Top = 15
      Width = 109
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 366
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 466
    Width = 479
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCodNivel: TMySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 24
  end
  object DsCodNivel: TDataSource
    DataSet = QrCodNivel
    Left = 428
    Top = 24
  end
end
