unit FisRegMvt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, dmkImage, UnDmkEnums;

type
  TFmFisRegMvt = class(TForm)
    Panel1: TPanel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    Label1: TLabel;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    dmkValUsu1: TdmkValUsu;
    DsStqCenCad: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    QrFiliais: TmySQLQuery;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    DsFiliais: TDataSource;
    dmkLabel1: TdmkLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    SbStqCenCad: TSpeedButton;
    RGTipoMov: TdmkRadioGroup;
    RGTipoCalc: TdmkRadioGroup;
    Panel4: TPanel;
    dmkValUsu2: TdmkValUsu;
    Label2: TLabel;
    EdControle: TdmkEdit;
    dmkEdit1: TdmkEdit;
    Label6: TLabel;
    CkContinuar: TCheckBox;
    dmkValUsu3: TdmkValUsu;
    QrGraCusPrc: TmySQLQuery;
    DsGraCusPrc: TDataSource;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    QrStqCenCadEntiSitio: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GBValor: TGroupBox;
    Label7: TLabel;
    SpeedButton1: TSpeedButton;
    RGGraCusApl: TdmkRadioGroup;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbStqCenCadClick(Sender: TObject);
    procedure RGGraCusAplClick(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraCusPrc();
  public
    { Public declarations }
  end;

  var
  FmFisRegMvt: TFmFisRegMvt;

implementation

uses UnMyObjects, Module, FisRegCad, UMySQLModule, MyDBCheck, UnInternalConsts,
  UnGrade_Jan, DmkDAC_PF;

{$R *.DFM}

procedure TFmFisRegMvt.BtOKClick(Sender: TObject);
var
  Controle, StqCenCad, Empresa, GraCusPrc: Integer;
begin
  StqCenCad := Geral.IMV(EdStqCenCad.Text);
  if StqCenCad = 0 then
  begin
    Application.MessageBox('Informe o centro de estoque!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    EdStqCenCad.SetFocus;
    Exit;
  end;
  //
  if dmkValUsu3.ValueVariant = Null then
    dmkValUsu3.ValueVariant := 0;
  //
  GraCusPrc := Geral.IMV(EdGraCusPrc.Text);
  (*
  Desativado em 18/09/2015 - INI
  if GraCusPrc = 0 then
  begin
    Application.MessageBox('Informe a lista de pre�os!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    EdGraCusPrc.SetFocus;
    Exit;
  end;
  Desativado em 18/09/2015 - FIM
  *)
  //
  Empresa := Geral.IMV(EdEmpresa.Text);
  if Empresa = 0 then
  begin
    Application.MessageBox('Informe a empresa!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  //
  Controle := Geral.IMV(EdControle.Text);
  Controle := UMyMod.BuscaEmLivreY_Def('fisregmvt', 'Controle', ImgTipo.SQLType,
    Controle);
  EdControle.ValueVariant := Controle;
  if UMyMod.ExecSQLInsUpdFm(FmFisRegMvt, ImgTipo.SQLType, 'FisRegMvt', Controle,
    Dmod.QrUpd) then
  begin
    FmFisRegCad.ReopenFisRegMvt(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant   := 0;
      EdStqCenCad.ValueVariant  := Null;
      CBStqCenCad.KeyValue      := 0;
      CBEmpresa.KeyValue        := 0;
      EdEmpresa.ValueVariant    := Null;
      RGTipoMov.ItemIndex       := 0;
      RGTipoCalc.ItemIndex      := 0;
    end else Close;
  end;
end;

procedure TFmFisRegMvt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFisRegMvt.EdStqCenCadChange(Sender: TObject);
begin
  if EdStqCenCad.ValueVariant <> 0 then
    dmkValUsu2.ValueVariant := QrStqCenCadEntiSitio.Value;
end;

procedure TFmFisRegMvt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegMvt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
  //
  GBValor.Visible := False;
end;

procedure TFmFisRegMvt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFisRegMvt.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Visible := True;
    CkContinuar.Checked := True;
  end else
  begin
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
  end;
end;

procedure TFmFisRegMvt.ReopenGraCusPrc;
var
  CodUsu: Integer;
begin
  QrGraCusPrc.Close;
  QrGraCusPrc.Params[0].AsInteger := RGGraCusApl.ItemIndex;
  UnDmkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
  //
  CodUsu := Geral.IMV(EdGraCusPrc.Text);
  if not QrGraCusPrc.Locate('CodUsu', CodUsu, []) then
  begin
    EdGraCusPrc.Text := '0';
    CBGraCusPrc.KeyValue := Null;
  end;  
end;

procedure TFmFisRegMvt.RGGraCusAplClick(Sender: TObject);
begin
  ReopenGraCusPrc();
end;

procedure TFmFisRegMvt.SbStqCenCadClick(Sender: TObject);
var
  StqCenCad: Integer;
begin
  VAR_CADASTRO := 0;
  StqCenCad    := EdStqCenCad.ValueVariant;
  //
  Grade_Jan.MostraFormStqCenCad(StqCenCad);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
    //
    EdStqCenCad.ValueVariant := VAR_CADASTRO;
    CBStqCenCad.KeyValue     := VAR_CADASTRO;
    EdStqCenCad.SetFocus;
    //
    dmkValUsu2.ValueVariant  := QrStqCenCadEntiSitio.Value;
  end;
end;

end.
