unit FisRegUFx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids, DBGrids, dmkDBGrid,
  DB, mySQLDbTables, dmkDBGridDAC, dmkRadioGroup, dmkImage, DmkDAC_PF,
  DBCtrls, dmkDBLookupComboBox, dmkEditCB, Variants, UnDmkEnums;

type
  TFmFisRegUFx = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    DBGFisRegCFOP: TdmkDBGrid;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdCST_B: TdmkEdit;
    EdTextoB: TdmkEdit;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    EdpRedBC: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel5: TPanel;
    Panel7: TPanel;
    Label24: TLabel;
    EdUFEmit: TdmkEdit;
    EdUFDest: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdICMSAliq: TdmkEdit;
    EdPISAliq: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdCOFINSAliq: TdmkEdit;
    RGNivel: TRadioGroup;
    Panel9: TPanel;
    EdCodNiv: TdmkEditCB;
    CBCodNiv: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrCodNivel: TmySQLQuery;
    DsCodNivel: TDataSource;
    Label6: TLabel;
    RGmodBC: TdmkRadioGroup;
    Label12: TLabel;
    EdcBenef: TdmkEdit;
    Label8: TLabel;
    EdpDif: TdmkEdit;
    EdCSOSN: TdmkEdit;
    EdTextoC: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCST_BChange(Sender: TObject);
    procedure EdCST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGNivelClick(Sender: TObject);
    procedure EdCSOSNChange(Sender: TObject);
    procedure EdCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FNivel: Integer;
  public
    { Public declarations }
  end;

  var
  FmFisRegUFx: TFmFisRegUFx;

implementation

uses
  {$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
  UnMyObjects, FisRegCad, ModuleGeral, UCreate, UnInternalConsts, UMySQLModule,
  Module, dmkGeral;

{$R *.DFM}


procedure TFmFisRegUFx.BtOKClick(Sender: TObject);
{
  procedure InsUpdAlquotas(const UFEmit, UFDest: String; const modBC: Integer;
  const ICMSAliq, PISALiq, COFINSAliq, pRedBC: Double; CST_B: String;
  var Conta: Integer);
  begin
    Dmod.QrUpd.Params[00].AsInteger := FmFisRegCad.QrFisRegCFOPCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmFisRegCad.QrFisRegCFOPInterno.Value;
    Dmod.QrUpd.Params[02].AsInteger := FmFisRegCad.QrFisRegCFOPContribui.Value;
    Dmod.QrUpd.Params[03].AsInteger := FmFisRegCad.QrFisRegCFOPProprio.Value;
    Dmod.QrUpd.Params[04].AsString  := UFEmit;
    Dmod.QrUpd.Params[05].AsString  := UFDest;
    Dmod.QrUpd.Params[06].AsFloat   := ICMSAliq;
    Dmod.QrUpd.Params[07].AsString  := Trim(CST_B);
    Dmod.QrUpd.Params[08].AsFloat   := pRedBC;
    Dmod.QrUpd.Params[09].AsInteger := modBC;
    //
    Dmod.QrUpd.Params[10].AsFloat   := ICMSAliq;
    Dmod.QrUpd.Params[11].AsString  := Trim(CST_B);
    Dmod.QrUpd.Params[12].AsFloat   := pRedBC;
    Dmod.QrUpd.Params[13].AsInteger := modBC;
    UMyMod.ExecutaQuery(Dmod.QrUpd);
    //
    Conta := Conta + 1;
  end;
}
var
  UFEmit, UFDest, CST_B, CSOSN, cBenef: String;
  ICMSAliq, (*PISALiq, COFINSAliq,*) pRedBC, pDif: Double;
  modBC, Codigo, Interno, Contribui, Proprio, SubsTrib, Nivel, CodNiv: Integer;
  Executou: Boolean;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  if MyObjects.FIC(Trim(EdCST_B.Text) = '', EdCST_B, 'Informe o CST do ICMS!') then
    Exit;
  try
    Geral.IMV(EdCST_B.ValueVariant);
  except
    if MyObjects.FIC(True, EdCST_B, 'Informe um CST v�lido do ICMS!') then
      Exit;
  end;
  if MyObjects.FIC(RGmodBC.ItemIndex < 0, RGmodBC,
  'Informe a modalidade de determina��o da base de c�lculo do ICMS!') then
    Exit;
  if MyObjects.FIC(RGNivel.ItemIndex < 1, RGNivel,
  'Informe o n�vel de exce��o!') then
    Exit;
  if RGNivel.ItemIndex < 5 then
    if MyObjects.FIC(EdCodNiv.ValueVariant = 0, EdCodNiv,
    'Informe o c�digo do item do n�vel de exce��o!') then
      Exit;
  //
  UFEmit := EdUFEmit.ValueVariant;
  UFDest := EdUFDest.ValueVariant;
  CST_B  := EdCST_B.ValueVariant;
  CSOSN  := EdCSOSN.Text;
  ICMSAliq := EdICMSAliq.ValueVariant;
  //PISALiq := EdPISALiq.ValueVariant;
  //COFINSAliq := EdCOFINSAliq.ValueVariant;
  pRedBC := EdpRedBC.ValueVariant;
  modBC := RGmodBC.ItemIndex;
  Codigo := FmFisRegCad.QrFisRegUFsCodigo.Value;
  Interno := FmFisRegCad.QrFisRegUFsInterno.Value;
  Contribui := FmFisRegCad.QrFisRegUFsContribui.Value;
  Proprio := FmFisRegCad.QrFisRegUFsProprio.Value;
  SubsTrib := FmFisRegCad.QrFisRegUFsSubsTrib.Value;
  Nivel := RGNivel.ItemIndex;
  CodNiv := EdCodNiv.ValueVariant;
  cBenef := EdcBenef.ValueVariant;
  pDif := EdpDif.ValueVariant;
  //
  if SQLType = stIns then
  begin
    Executou := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fisregufx', False, [
    'ICMSAliq', 'CST_B', 'CSOSN', 'pRedBC', 'modBC', 'pDif'], [
    'Codigo', 'Interno', 'Contribui', 'Proprio', 'SubsTrib',
    'UFEmit', 'UFDest', 'Nivel', 'CodNiv', 'cBenef'], [
    ICMSAliq, CST_B, CSOSN, pRedBC, modBC, pDif], [
    Codigo, Interno, Contribui, Proprio, SubsTrib,
    UFEmit, UFDest, Nivel, CodNiv, cBenef], True);
  end else
  if SQLType = stUpd then
  begin
    Executou := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fisregufx', False, [
    'ICMSAliq', 'CST_B',  'CSOSN', 'pRedBC', 'modBC', 'pDif',
    'Codigo', 'Interno', 'Contribui', 'Proprio', 'SubsTrib',
    'UFEmit', 'UFDest', 'Nivel', 'CodNiv', 'cBenef'], [
    'Codigo', 'Interno', 'Contribui', 'Proprio', 'SubsTrib',
    'UFEmit', 'UFDest', 'Nivel', 'CodNiv', 'cBenef'], [
    ICMSAliq, CST_B,  CSOSN, pRedBC, modBC, pDif,
     Codigo, Interno, Contribui, Proprio, SubsTrib,
    UFEmit, UFDest, Nivel, CodNiv, cBenef], [
    FmFisRegCad.QrFisRegUFxCodigo.Value, FmFisRegCad.QrFisRegUFxInterno.Value,
    FmFisRegCad.QrFisRegUFxContribui.Value, FmFisRegCad.QrFisRegUFxProprio.Value,
    FmFisRegCad.QrFisRegUFxSubsTrib.Value,
    FmFisRegCad.QrFisRegUFxUFEmit.Value, FmFisRegCad.QrFisRegUFxUFDest.Value,
    FmFisRegCad.QrFisRegUFxNivel.Value, FmFisRegCad.QrFisRegUFxCodNiv.Value,
    FmFisRegCad.QrFisRegUFxcBenef.Value], True);
  end;
  if Executou then
  begin
    FmFisRegCad.ReopenFisRegUFx();
    //
    Close;
  end;
{
  UFEmit := Trim(EdUFEmit.Text);
  if UFEmit = '' then
  begin
    Geral.MensagemBox('Informe a UF emitente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    Conta := 0;
    ICMSALiq   := EdICMSAliq.ValueVariant;
    PISALiq    := EdPISAliq.ValueVariant;
    COFINSALiq := EdCOFINSAliq.ValueVariant;
    CST_B      := Trim(EdCST_B.Text);
    if CST_B = '0' then
      CST_B := '00';
    pRedBC := EdpRedBC.ValueVariant;
    modBC := RGmodBC.ItemIndex;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fisregufs (');
    Dmod.QrUpd.SQL.Add('Codigo,interno,Contribui,Proprio,UFEmit,');
    Dmod.QrUpd.SQL.Add('UFDest,ICMSAliq,CST_B,pRedBC,modBC) VALUES (');
    Dmod.QrUpd.SQL.Add(':P0,:P1,:P2,:P3,:P4,:P5,:P6,:P7,:P8,:P9)');
    Dmod.QrUpd.SQL.Add('ON DUPLICATE KEY UPDATE ICMSAliq=:P10, CST_B=:P11, ');
    Dmod.QrUpd.SQL.Add('pRedBC=:P12, modBC=:P13');
    UFDest := Trim(EdUFDest.Text);
    if Length(UFDest) = 2 then
      InsUpdAlquotas(UFEmit, UFDest, modBC, ICMSAliq, PISALiq, COFINSAliq,
        pRedBC, CST_B, Conta);
    QrUFsAtoB.First;
    while not QrUFsAtoB.Eof do
    begin
      if QrUFsAtoBAtivo.Value = 1 then
        InsUpdAlquotas(UFEmit, QrUFsAtoBNome.Value, modBC, ICMSAliq, PISALiq,
          COFINSAliq, pRedBC, CST_B, Conta);
      QrUFsAtoB.Next;
    end;
    case Conta of
      0: Msg := 'N�o foi inclu�do/alterado nenhum registro!';
      1: Msg := 'Foi inclu�do/alterado um registro!';
      else Msg := 'Foram inclu�dos/alterados ' + IntToStr(Conta) + ' registros!';
    end;
    Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmFisRegUFx.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFisRegUFx.EdCSOSNChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  EdTextoC.Text := UFinanceiro.CSOSN_Get(1, Geral.IMV(EdCSOSN.Text));
  {$EndIf}
end;

procedure TFmFisRegUFx.EdCSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();
  {$EndIf}
end;

procedure TFmFisRegUFx.EdCST_BChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
{$EndIf}
end;

procedure TFmFisRegUFx.EdCST_BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
{$EndIf}
end;

procedure TFmFisRegUFx.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegUFx.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFisRegUFx.RGNivelClick(Sender: TObject);
var
  F7SQL: TStrings;
begin
  QrCodNivel.Close;
  //
  case RGNivel.ItemIndex of
    1:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Nivel1 Codigo, Nome  ',
        'FROM gragru1',
        'ORDER BY Nome ',
        '']);
    2:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Nivel2 Codigo, Nome  ',
        'FROM gragru2',
        'ORDER BY Nome ',
        '']);
    3:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Nivel3 Codigo, Nome  ',
        'FROM gragru3',
        'ORDER BY Nome ',
        '']);
    4:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Codigo, Nome  ',
        'FROM prdgruptip',
        'ORDER BY Nome ',
        '']);
  end;

  case RGNivel.ItemIndex of
    1:   CBCodNiv.LocF7SQLText.Text := Geral.ATS([
        'SELECT Nivel1 _Codigo, Nome _Nome ',
        'FROM gragru1 ',
        'WHERE Nome LIKE "%$#%" ']);
    2:   CBCodNiv.LocF7SQLText.Text := Geral.ATS([
        'SELECT Nivel2 _Codigo, Nome _Nome ',
        'FROM gragru2 ',
        'WHERE Nome LIKE "%$#%" ']);
    3:   CBCodNiv.LocF7SQLText.Text := Geral.ATS([
        'SELECT Nivel3 _Codigo, Nome _Nome ',
        'FROM gragru3 ',
        'WHERE Nome LIKE "%$#%" ']);
    4:   CBCodNiv.LocF7SQLText.Text := Geral.ATS([
        'SELECT Codigo _Codigo, Nome _Nome ',
        'FROM prdgruptip ',
        'WHERE Nome LIKE "%$#%" ']);
    else CBCodNiv.LocF7SQLText.Text := '';
  end;

  if (FNivel <> RGNivel.ItemIndex) or (RGNivel.ItemIndex = 0) then
  begin
    EdCodNiv.ValueVariant := 0;
    CBCodNiv.KeyValue := Null;
    //
    FNivel := RGNivel.ItemIndex;
  end;
end;

end.
