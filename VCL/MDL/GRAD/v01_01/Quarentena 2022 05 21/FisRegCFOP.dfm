object FmFisRegCFOP: TFmFisRegCFOP
  Left = 339
  Top = 185
  Caption = 'FIS-REGRA-003 :: Regras CFOP'
  ClientHeight = 576
  ClientWidth = 984
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 101
    Width = 984
    Height = 327
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 984
      Height = 65
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 15
        Top = 1
        Width = 52
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CFOP: [F7]'
      end
      object Label2: TLabel
        Left = 668
        Top = 47
        Width = 249
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de prefer'#234'ncia para defini'#231#227'o do CFOP Geral:'
      end
      object SpeedButton1: TSpeedButton
        Left = 957
        Top = 17
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object EdCFOP: TdmkEditCB
        Left = 15
        Top = 17
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CFOP'
        UpdCampo = 'CFOP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
        DBLookupComboBox = CBCFOP
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCFOP: TdmkDBLookupComboBox
        Left = 84
        Top = 17
        Width = 869
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCFOP
        TabOrder = 1
        dmkEditCB = EdCFOP
        QryCampo = 'CFOP'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkContribui: TdmkCheckBox
        Left = 15
        Top = 43
        Width = 126
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contribuinte UF (I.E.)'
        TabOrder = 2
        QryCampo = 'Contribui'
        UpdCampo = 'Contribui'
        UpdType = utIdx
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkInterno: TdmkCheckBox
        Left = 146
        Top = 43
        Width = 151
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Venda/compra mesma UF.'
        TabOrder = 3
        QryCampo = 'Interno'
        UpdCampo = 'Interno'
        UpdType = utIdx
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkProprio: TdmkCheckBox
        Left = 298
        Top = 43
        Width = 111
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fabrica'#231#227'o pr'#243'pria.'
        TabOrder = 4
        QryCampo = 'Proprio'
        UpdCampo = 'Proprio'
        UpdType = utIdx
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdOrdCFOPGer: TdmkEdit
        Left = 925
        Top = 42
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '999'
        QryCampo = 'OrdCFOPGer'
        UpdCampo = 'OrdCFOPGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 999
        ValWarn = False
      end
      object CkServico: TdmkCheckBox
        Left = 410
        Top = 43
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Presta'#231#227'o de servi'#231'o.'
        TabOrder = 6
        QryCampo = 'Servico'
        UpdCampo = 'Servico'
        UpdType = utIdx
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkSubsTrib: TdmkCheckBox
        Left = 538
        Top = 43
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Substituto tribut'#225'rio.'
        TabOrder = 7
        QryCampo = 'SubsTrib'
        UpdCampo = 'SubsTrib'
        UpdType = utIdx
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
    object DBMemo1: TDBMemo
      Left = 0
      Top = 65
      Width = 984
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Align = alTop
      DataField = 'Descricao'
      DataSource = DsCFOP
      TabOrder = 1
    end
    object DBMemo2: TDBMemo
      Left = 0
      Top = 114
      Width = 984
      Height = 134
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Align = alClient
      DataField = 'Complementacao'
      DataSource = DsCFOP
      TabOrder = 2
    end
    object TPanel
      Left = 0
      Top = 248
      Width = 984
      Height = 79
      Align = alBottom
      TabOrder = 3
      object Label6: TLabel
        Left = 7
        Top = 8
        Width = 231
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CFOP constante na NFe )NFe-s de entrada): [F7]'
      end
      object SpeedButton2: TSpeedButton
        Left = 956
        Top = 3
        Width = 23
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object Label181: TLabel
        Left = 192
        Top = 32
        Width = 72
        Height = 13
        Caption = 'Conta a d'#233'bito:'
        Enabled = False
      end
      object Label15: TLabel
        Left = 192
        Top = 60
        Width = 75
        Height = 13
        Caption = 'Conta a cr'#233'dito:'
        Enabled = False
      end
      object SbGenCtbD: TSpeedButton
        Left = 956
        Top = 27
        Width = 23
        Height = 23
        Caption = '...'
        Enabled = False
        OnClick = SbGenCtbDClick
      end
      object SbGenCtbC: TSpeedButton
        Left = 956
        Top = 51
        Width = 23
        Height = 23
        Caption = '...'
        Enabled = False
        OnClick = SbGenCtbCClick
      end
      object EdOriCFOP: TdmkEditCB
        Left = 276
        Top = 4
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'OriCFOP'
        UpdCampo = 'OriCFOP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
        DBLookupComboBox = CBOriCFOP
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBOriCFOP: TdmkDBLookupComboBox
        Left = 336
        Top = 4
        Width = 618
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOriCFOP
        TabOrder = 1
        dmkEditCB = EdOriCFOP
        QryCampo = 'OriCFOP'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGenCtbD: TdmkEditCB
        Left = 276
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GenCtbD'
        UpdCampo = 'GenCtbD'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGenCtbD
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdGenCtbC: TdmkEditCB
        Left = 276
        Top = 52
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GenCtbC'
        UpdCampo = 'GenCtbC'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGenCtbC
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGenCtbD: TdmkDBLookupComboBox
        Left = 336
        Top = 28
        Width = 618
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContasD
        TabOrder = 4
        dmkEditCB = EdGenCtbD
        QryCampo = 'GenCtbD'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBGenCtbC: TdmkDBLookupComboBox
        Left = 336
        Top = 52
        Width = 618
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContasC
        TabOrder = 5
        dmkEditCB = EdGenCtbC
        QryCampo = 'GenCtbC'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 52
    Width = 984
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 1
    object Label3: TLabel
      Left = 192
      Top = 5
      Width = 51
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label4: TLabel
      Left = 89
      Top = 5
      Width = 36
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label5: TLabel
      Left = 15
      Top = 5
      Width = 14
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 15
      Top = 25
      Width = 69
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmFisRegCad.DsFisRegCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utIdx
      Alignment = taRightJustify
    end
    object DBEdit1: TDBEdit
      Left = 89
      Top = 25
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'CodUsu'
      DataSource = FmFisRegCad.DsFisRegCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 192
      Top = 25
      Width = 785
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      DataField = 'Nome'
      DataSource = FmFisRegCad.DsFisRegCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 428
    Width = 984
    Height = 24
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 19
      Top = 1
      Width = 144
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 984
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 925
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 866
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 175
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Regras CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 175
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Regras CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 175
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Regras CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 452
    Width = 984
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 980
      Height = 37
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 506
    Width = 984
    Height = 70
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 5
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 980
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 802
        Top = 0
        Width = 178
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 5
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cfop2003'
      'ORDER BY Nome')
    Left = 348
    Top = 24
    object QrCFOPCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 348
    Top = 72
  end
  object QrOriCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cfop2003'
      'ORDER BY Nome')
    Left = 456
    Top = 20
    object QrOriCFOPCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrOriCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrOriCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOriCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsOriCFOP: TDataSource
    DataSet = QrOriCFOP
    Left = 456
    Top = 68
  end
  object QrContasD: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 560
    Top = 216
    object QrContasDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasDNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsContasD: TDataSource
    DataSet = QrContasD
    Left = 560
    Top = 264
  end
  object QrContasC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 644
    Top = 216
    object QrContasCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasCNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsContasC: TDataSource
    DataSet = QrContasC
    Left = 644
    Top = 264
  end
end
