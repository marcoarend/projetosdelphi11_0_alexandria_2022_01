object FmFisRegUFs: TFmFisRegUFs
  Left = 339
  Top = 185
  Caption = 'FIS-REGRA-004 :: Impostos entre UFs por CFOP'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 499
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label6: TLabel
      Left = 0
      Top = 44
      Width = 1008
      Height = 13
      Align = alTop
      Caption = 
        'Aten'#231#227'o! Ao informar agum campo exclus'#237'vo, ele sobrepor'#225' qualque' +
        'r outro!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 429
    end
    object Panel3: TPanel
      Left = 0
      Top = 309
      Width = 1008
      Height = 190
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 433
        Height = 190
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox5: TGroupBox
          Left = 0
          Top = 0
          Width = 217
          Height = 190
          Align = alLeft
          Caption = 'Minha UF:'
          TabOrder = 0
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 213
            Height = 173
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label24: TLabel
              Left = 4
              Top = 4
              Width = 49
              Height = 13
              Caption = 'Minha UF:'
            end
            object Label2: TLabel
              Left = 4
              Top = 44
              Width = 40
              Height = 13
              Caption = '% ICMS:'
            end
            object Label3: TLabel
              Left = 108
              Top = 44
              Width = 31
              Height = 13
              Caption = '% PIS:'
            end
            object Label4: TLabel
              Left = 4
              Top = 84
              Width = 53
              Height = 13
              Caption = '% COFINS:'
            end
            object Label10: TLabel
              Left = 108
              Top = 84
              Width = 103
              Height = 13
              Caption = '% ICMS interestadual:'
            end
            object Label12: TLabel
              Left = 6
              Top = 128
              Width = 131
              Height = 13
              Caption = 'I105f - C'#243'd. beneficio fiscal:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object EdUFEmit: TdmkEdit
              Left = 5
              Top = 20
              Width = 44
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'EUF'
              UpdCampo = 'EUF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdICMSAliq: TdmkEdit
              Left = 4
              Top = 60
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPISAliq: TdmkEdit
              Left = 108
              Top = 60
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCOFINSAliq: TdmkEdit
              Left = 4
              Top = 100
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdpICMSInter: TdmkEdit
              Left = 108
              Top = 100
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdcBenef: TdmkEdit
              Left = 4
              Top = 144
              Width = 133
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'cBenef'
              UpdCampo = 'cBenef'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object GroupBox6: TGroupBox
          Left = 217
          Top = 0
          Width = 216
          Height = 190
          Align = alClient
          Caption = 'UF Destino emiss'#227'o / origem entrada:'
          TabOrder = 1
          object Panel9: TPanel
            Left = 2
            Top = 15
            Width = 212
            Height = 173
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 3
              Top = 4
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label5: TLabel
              Left = 4
              Top = 44
              Width = 75
              Height = 13
              Caption = '% ICMS interno:'
            end
            object Label8: TLabel
              Left = 108
              Top = 44
              Width = 67
              Height = 13
              Caption = '% ICMS FCP*:'
            end
            object Label9: TLabel
              Left = 4
              Top = 84
              Width = 85
              Height = 13
              Caption = '% BC da BC Orig.:'
            end
            object LapICMSInterPart: TLabel
              Left = 108
              Top = 84
              Width = 77
              Height = 13
              Caption = '% ICMS partilha:'
            end
            object EdUFDest: TdmkEdit
              Left = 4
              Top = 20
              Width = 44
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'EUF'
              UpdCampo = 'EUF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdpICMSUFDest: TdmkEdit
              Left = 4
              Top = 60
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdpFCPUFDest: TdmkEdit
              Left = 108
              Top = 60
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdpBCUFDest: TdmkEdit
              Left = 4
              Top = 100
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 10
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '100,0000000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 100.000000000000000000
              ValWarn = False
            end
            object EdpICMSInterPart: TdmkEdit
              Left = 108
              Top = 100
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object Panel10: TPanel
        Left = 433
        Top = 0
        Width = 575
        Height = 190
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 237
          Height = 190
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'UF'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Adiciona'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsUFsAtoB
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'UF'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Adiciona'
              Visible = True
            end>
        end
        object Panel11: TPanel
          Left = 237
          Top = 0
          Width = 338
          Height = 190
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object CkUsaInterPartLei: TCheckBox
            Left = 0
            Top = 0
            Width = 338
            Height = 190
            Align = alClient
            Caption = 
              'Usar % ICMS partilha conforme emenda constitucional 87/2015.'#13#10'40' +
              '% em 2016  -  60% em 2017'#13#10'80% em 2018  -  100% a partir de 2019' +
              '.'
            TabOrder = 0
            WordWrap = True
            OnClick = CkUsaInterPartLeiClick
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 44
      Align = alTop
      Caption = 'Panel4'
      TabOrder = 1
      object DBGFisRegCFOP: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 1006
        Height = 42
        TabStop = False
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CFOP'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contribui'
            Title.Caption = 'TIE'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Interno'
            Title.Caption = 'VMU'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Proprio'
            Title.Caption = 'PFP'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubsTrib'
            Title.Caption = ' ST'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Servico'
            Title.Caption = 'Servi'#231'o'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OriCFOP'
            Title.Caption = 'CFOP origem'
            Width = 70
            Visible = True
          end>
        Color = clWindow
        DataSource = FmFisRegCad.DsFisRegCFOP
        Enabled = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CFOP'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contribui'
            Title.Caption = 'TIE'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Interno'
            Title.Caption = 'VMU'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Proprio'
            Title.Caption = 'PFP'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubsTrib'
            Title.Caption = ' ST'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Servico'
            Title.Caption = 'Servi'#231'o'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OriCFOP'
            Title.Caption = 'CFOP origem'
            Width = 70
            Visible = True
          end>
      end
    end
    object PCTES: TPageControl
      Left = 0
      Top = 57
      Width = 1008
      Height = 252
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'NFs emitidas'
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 224
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 224
            Align = alTop
            Caption = '  Informa'#231#245'es exclusivas: '
            TabOrder = 0
            object GroupBox2: TGroupBox
              Left = 2
              Top = 15
              Width = 996
              Height = 98
              Align = alTop
              Caption = 
                'Caso n'#227'o queira um CST '#250'nico para o ICMS deixe o campo em branco' +
                '!'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              object Label11: TLabel
                Left = 8
                Top = 16
                Width = 234
                Height = 13
                Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST ICMS): [F3] '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label14: TLabel
                Left = 8
                Top = 56
                Width = 356
                Height = 13
                Caption = 
                  'N12a - C'#243'digo de Situa'#231#227'o da Opera'#231#227'o - Simples Nacional (CSOSN)' +
                  ': [F3] '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object EdCST_B: TdmkEdit
                Left = 8
                Top = 32
                Width = 29
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'CST_B'
                UpdCampo = 'CST_B'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCST_BChange
                OnKeyDown = EdCST_BKeyDown
              end
              object EdTextoB: TdmkEdit
                Left = 39
                Top = 32
                Width = 518
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCSOSN: TdmkEdit
                Left = 8
                Top = 72
                Width = 29
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'CSOSN'
                UpdCampo = 'CSOSN'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCSOSNChange
                OnKeyDown = EdCSOSNKeyDown
              end
              object EdTextoC: TdmkEdit
                Left = 39
                Top = 72
                Width = 518
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox3: TGroupBox
              Left = 2
              Top = 113
              Width = 996
              Height = 72
              Align = alTop
              Caption = 
                'Caso n'#227'o queira um modBC '#250'nico para o ICMS clique no bot'#227'o "Desm' +
                'arcar"!'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              object Panel12: TPanel
                Left = 2
                Top = 15
                Width = 100
                Height = 55
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object BtNaoSel: TBitBtn
                  Left = 3
                  Top = 8
                  Width = 90
                  Height = 40
                  Hint = 'Desmarca todos itens'
                  Caption = 'Desmarcar'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  TabOrder = 0
                  OnClick = BtNaoSelClick
                end
              end
              object RGmodBC: TdmkRadioGroup
                Left = 102
                Top = 15
                Width = 892
                Height = 55
                Align = alClient
                Caption = 'Modalidade de determina'#231#227'o da BC do ICMS: '
                Columns = 2
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Margem valor agregado (%)'
                  'Pauta (valor)'
                  'Pre'#231'o tabelado m'#225'ximo (valor)'
                  'Valor da opera'#231#227'o')
                ParentFont = False
                TabOrder = 1
                QryCampo = 'modBC'
                UpdCampo = 'modBC'
                UpdType = utYes
                OldValor = 0
              end
            end
            object GroupBox4: TGroupBox
              Left = 2
              Top = 185
              Width = 996
              Height = 37
              Align = alClient
              Caption = 
                'Caso n'#227'o queira um % red. BC ou dif. '#250'nico para o ICMS deixe o c' +
                'ampo zerado!'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              object Label7: TLabel
                Left = 8
                Top = 20
                Width = 204
                Height = 13
                Caption = 'Percentual de redu'#231#227'o da base de c'#225'lculo:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label13: TLabel
                Left = 284
                Top = 20
                Width = 123
                Height = 13
                Caption = 'Percentual de diferimento:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object EdpRedBC: TdmkEdit
                Left = 220
                Top = 16
                Width = 57
                Height = 21
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'pRedBC'
                UpdCampo = 'pRedBC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdCST_BChange
                OnKeyDown = EdCST_BKeyDown
              end
              object EdpDif: TdmkEdit
                Left = 412
                Top = 16
                Width = 57
                Height = 21
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'pRedBC'
                UpdCampo = 'pRedBC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdCST_BChange
                OnKeyDown = EdCST_BKeyDown
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'NFs recebidas (de entrada)'
        ImageIndex = 1
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 224
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel15: TPanel
            Left = 1
            Top = 42
            Width = 998
            Height = 181
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label476: TLabel
              Left = 10
              Top = 20
              Width = 35
              Height = 13
              Caption = 'ICMS:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label477: TLabel
              Left = 8
              Top = 44
              Width = 13
              Height = 13
              Caption = 'IPI'
            end
            object Label478: TLabel
              Left = 8
              Top = 68
              Width = 21
              Height = 13
              Caption = 'PIS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label480: TLabel
              Left = 8
              Top = 96
              Width = 46
              Height = 13
              Caption = 'COFINS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label181: TLabel
              Left = 8
              Top = 116
              Width = 72
              Height = 13
              Caption = 'Conta a d'#233'bito:'
              Enabled = False
            end
            object SbGenCtbD: TSpeedButton
              Left = 772
              Top = 112
              Width = 21
              Height = 21
              Caption = '...'
              Enabled = False
              OnClick = SbGenCtbDClick
            end
            object Label15: TLabel
              Left = 8
              Top = 140
              Width = 75
              Height = 13
              Caption = 'Conta a cr'#233'dito:'
              Enabled = False
            end
            object SbGenCtbC: TSpeedButton
              Left = 772
              Top = 136
              Width = 21
              Height = 21
              Caption = '...'
              Enabled = False
              OnClick = SbGenCtbCClick
            end
            object Label16: TLabel
              Left = 60
              Top = 0
              Width = 24
              Height = 13
              Caption = 'CST:'
            end
            object Label17: TLabel
              Left = 656
              Top = 0
              Width = 82
              Height = 13
              Caption = 'TES % do tributo:'
            end
            object Label18: TLabel
              Left = 824
              Top = 0
              Width = 144
              Height = 13
              Caption = 'TES Valor da base de c'#225'lculo:'
            end
            object EdOriCST_ICMS: TdmkEdit
              Left = 60
              Top = 16
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '999'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'UC_ICMS_CST_B'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdOriCST_ICMSChange
              OnKeyDown = EdOriCST_ICMSKeyDown
            end
            object EdOriTextoB: TdmkEdit
              Left = 109
              Top = 16
              Width = 544
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdOriCST_IPI: TdmkEdit
              Left = 60
              Top = 40
              Width = 45
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsO'
              QryCampo = 'UC_IPI_CST'
              UpdCampo = 'UC_IPI_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdOriCST_IPIChange
              OnKeyDown = EdOriCST_IPIKeyDown
            end
            object EdOriTextoIPI_CST: TdmkEdit
              Left = 109
              Top = 40
              Width = 544
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdOriCST_PIS: TdmkEdit
              Left = 60
              Top = 64
              Width = 45
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsQ'
              QryCampo = 'UC_PIS_CST'
              UpdCampo = 'UC_PIS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdOriCST_PISChange
              OnKeyDown = EdOriCST_PISKeyDown
            end
            object EdOriTextoPIS_CST: TdmkEdit
              Left = 109
              Top = 64
              Width = 544
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdOriCST_COFINS: TdmkEdit
              Left = 60
              Top = 88
              Width = 45
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsS'
              QryCampo = 'UC_COFINS_CST'
              UpdCampo = 'UC_COFINS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdOriCST_COFINSChange
              OnKeyDown = EdOriCST_COFINSKeyDown
            end
            object EdOriTextoCOFINS_CST: TdmkEdit
              Left = 109
              Top = 88
              Width = 544
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdGenCtbD: TdmkEditCB
              Left = 92
              Top = 112
              Width = 56
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'GenCtbD'
              UpdCampo = 'GenCtbD'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGenCtbD
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGenCtbD: TdmkDBLookupComboBox
              Left = 152
              Top = 112
              Width = 618
              Height = 21
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContasD
              TabOrder = 9
              dmkEditCB = EdGenCtbD
              QryCampo = 'GenCtbD'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdGenCtbC: TdmkEditCB
              Left = 92
              Top = 136
              Width = 56
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 10
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'GenCtbC'
              UpdCampo = 'GenCtbC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGenCtbC
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGenCtbC: TdmkDBLookupComboBox
              Left = 152
              Top = 136
              Width = 618
              Height = 21
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContasC
              TabOrder = 11
              dmkEditCB = EdGenCtbC
              QryCampo = 'GenCtbC'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTES_ICMS: TdmkEdit
              Left = 656
              Top = 16
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_ICMS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_ICMSChange
              OnKeyDown = EdTES_ICMSKeyDown
            end
            object EdTES_ICMS_TXT: TdmkEdit
              Left = 681
              Top = 16
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTES_IPI: TdmkEdit
              Left = 656
              Top = 40
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 14
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_IPI'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_IPIChange
              OnKeyDown = EdTES_IPIKeyDown
            end
            object EdTES_IPI_TXT: TdmkEdit
              Left = 681
              Top = 40
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 15
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTES_PIS: TdmkEdit
              Left = 656
              Top = 64
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 16
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_PIS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_PISChange
              OnKeyDown = EdTES_PISKeyDown
            end
            object EdTES_COFINS: TdmkEdit
              Left = 656
              Top = 88
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 17
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_COFINS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_COFINSChange
              OnKeyDown = EdTES_COFINSKeyDown
            end
            object EdTES_PIS_TXT: TdmkEdit
              Left = 681
              Top = 64
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 18
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTES_COFINS_TXT: TdmkEdit
              Left = 681
              Top = 88
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 19
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTES_BC_COFINS_TXT: TdmkEdit
              Left = 849
              Top = 88
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 20
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTES_BC_COFINS: TdmkEdit
              Left = 824
              Top = 88
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 21
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_COFINS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_BC_COFINSChange
              OnKeyDown = EdTES_BC_COFINSKeyDown
            end
            object EdTES_BC_PIS: TdmkEdit
              Left = 824
              Top = 64
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 22
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_PIS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_BC_PISChange
              OnKeyDown = EdTES_BC_PISKeyDown
            end
            object EdTES_BC_IPI: TdmkEdit
              Left = 824
              Top = 40
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 23
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_IPI'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_BC_IPIChange
              OnKeyDown = EdTES_BC_IPIKeyDown
            end
            object EdTES_BC_ICMS: TdmkEdit
              Left = 824
              Top = 16
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 24
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 1
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '2'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'TES_ICMS'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTES_BC_ICMSChange
              OnKeyDown = EdTES_BC_ICMSKeyDown
            end
            object EdTES_BC_PIS_TXT: TdmkEdit
              Left = 849
              Top = 64
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 25
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTES_BC_IPI_TXT: TdmkEdit
              Left = 849
              Top = 40
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 26
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTES_BC_ICMS_TXT: TdmkEdit
              Left = 849
              Top = 16
              Width = 140
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 27
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object Panel16: TPanel
            Left = 1
            Top = 1
            Width = 998
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object RGOriTES: TdmkRadioGroup
              Left = 0
              Top = 0
              Width = 392
              Height = 41
              Align = alLeft
              Caption = ' TES: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'N'#227'o definido'
                'Livro fiscal: Tributa'
                'Livro fiscal: Outros')
              TabOrder = 0
              OnClick = RGOriTESClick
              UpdType = utYes
              OldValor = 0
            end
            object CkEFD_II_C195: TdmkCheckBox
              Left = 400
              Top = 16
              Width = 141
              Height = 17
              Caption = 'Gera EFD II C195 / D195'
              TabOrder = 1
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbAjuda: TBitBtn
        Tag = 615
        Left = 4
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbAjudaClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 369
        Height = 32
        Caption = 'Impostos entre UFs por CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 369
        Height = 32
        Caption = 'Impostos entre UFs por CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 369
        Height = 32
        Caption = 'Impostos entre UFs por CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 591
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 897
      Top = 15
      Width = 109
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 895
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 139
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Marca todos itens'
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 231
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Desmarca todos itens'
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 547
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 307
        Height = 16
        Caption = '* FCP: Fundo de combate '#224' pobreza. M'#225'ximo 2,00%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 307
        Height = 16
        Caption = '* FCP: Fundo de combate '#224' pobreza. M'#225'ximo 2,00%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsUFsAtoB: TDataSource
    DataSet = QrUFsAtoB
    Left = 228
    Top = 52
  end
  object QrUFsAtoB: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ufsatob')
    Left = 200
    Top = 52
    object QrUFsAtoBNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrUFsAtoBAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object QrContasD: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 560
    Top = 216
    object QrContasDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasDNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsContasD: TDataSource
    DataSet = QrContasD
    Left = 560
    Top = 264
  end
  object QrContasC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 644
    Top = 216
    object QrContasCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasCNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsContasC: TDataSource
    DataSet = QrContasC
    Left = 644
    Top = 264
  end
end
