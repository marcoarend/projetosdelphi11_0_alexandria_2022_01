object FmFisRegMvt: TFmFisRegMvt
  Left = 339
  Top = 185
  Caption = 'FIS-REGRA-002 :: Tipos de Movimentos de Regras Fiscais'
  ClientHeight = 435
  ClientWidth = 674
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 103
    Width = 674
    Height = 190
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 90
      Height = 13
      Caption = 'Centro de estoque:'
    end
    object dmkLabel1: TdmkLabel
      Left = 12
      Top = 43
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      UpdType = utYes
      SQLType = stNil
    end
    object SbStqCenCad: TSpeedButton
      Left = 626
      Top = 20
      Width = 21
      Height = 20
      Caption = '...'
      OnClick = SbStqCenCadClick
    end
    object Label6: TLabel
      Left = 630
      Top = 90
      Width = 34
      Height = 13
      Caption = 'Ordem:'
      Visible = False
    end
    object EdStqCenCad: TdmkEditCB
      Left = 12
      Top = 20
      Width = 55
      Height = 20
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdStqCenCadChange
      DBLookupComboBox = CBStqCenCad
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStqCenCad: TdmkDBLookupComboBox
      Left = 67
      Top = 20
      Width = 559
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsStqCenCad
      TabOrder = 1
      dmkEditCB = EdStqCenCad
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 59
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 67
      Top = 59
      Width = 559
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DsFiliais
      TabOrder = 3
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object RGTipoMov: TdmkRadioGroup
      Left = 12
      Top = 86
      Width = 182
      Height = 42
      Caption = ' Tipo de movimento: '
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Entrada'
        'Sa'#237'da')
      TabOrder = 4
      QryCampo = 'TipoMov'
      UpdCampo = 'TipoMov'
      UpdType = utYes
      OldValor = 0
    end
    object RGTipoCalc: TdmkRadioGroup
      Left = 197
      Top = 86
      Width = 430
      Height = 42
      Caption = ' Movimenta'#231#227'o de estoque: '
      Columns = 3
      ItemIndex = 2
      Items.Strings = (
        'Nulo'
        'Adiciona'
        'Subtrai ')
      TabOrder = 5
      QryCampo = 'TipoCalc'
      UpdCampo = 'TipoCalc'
      UpdType = utYes
      OldValor = 0
    end
    object dmkEdit1: TdmkEdit
      Left = 630
      Top = 107
      Width = 36
      Height = 21
      TabOrder = 6
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '0'
      ValWarn = False
    end
    object GBValor: TGroupBox
      Left = 12
      Top = 131
      Width = 615
      Height = 56
      Caption = ' Valor: '
      TabOrder = 7
      object Label7: TLabel
        Left = 146
        Top = 13
        Width = 25
        Height = 13
        Caption = 'Lista:'
      end
      object SpeedButton1: TSpeedButton
        Left = 590
        Top = 27
        Width = 21
        Height = 21
        Caption = '...'
        Visible = False
      end
      object RGGraCusApl: TdmkRadioGroup
        Left = 2
        Top = 15
        Width = 137
        Height = 39
        Align = alLeft
        Caption = 'Aplica'#231#227'o: '
        Columns = 2
        Items.Strings = (
          'Custo'
          'Pre'#231'o')
        TabOrder = 0
        OnClick = RGGraCusAplClick
        QryCampo = 'GraCusApl'
        UpdCampo = 'GraCusApl'
        UpdType = utYes
        OldValor = 0
      end
      object EdGraCusPrc: TdmkEditCB
        Left = 146
        Top = 27
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraCusPrc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraCusPrc: TdmkDBLookupComboBox
        Left = 202
        Top = 27
        Width = 386
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraCusPrc
        TabOrder = 2
        dmkEditCB = EdGraCusPrc
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 47
    Width = 674
    Height = 56
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 1
    object Label3: TLabel
      Left = 154
      Top = 4
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label4: TLabel
      Left = 71
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label5: TLabel
      Left = 12
      Top = 4
      Width = 46
      Height = 13
      Caption = 'ID Regra:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 571
      Top = 4
      Width = 69
      Height = 13
      Caption = 'ID Movimento:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 20
      Width = 55
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmFisRegCad.DsFisRegCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdit1: TDBEdit
      Left = 71
      Top = 20
      Width = 79
      Height = 21
      DataField = 'CodUsu'
      DataSource = FmFisRegCad.DsFisRegCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 154
      Top = 20
      Width = 414
      Height = 21
      Color = clWhite
      DataField = 'Nome'
      DataSource = FmFisRegCad.DsFisRegCad
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object EdControle: TdmkEdit
      Left = 571
      Top = 20
      Width = 79
      Height = 20
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '0'
      ValWarn = False
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 293
    Width = 674
    Height = 36
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object CkContinuar: TCheckBox
      Left = 16
      Top = 6
      Width = 115
      Height = 16
      Caption = 'Continuar inserindo.'
      Enabled = False
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 674
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 627
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 580
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 459
        Height = 31
        Caption = 'Tipos de Movimentos de Regras Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 459
        Height = 31
        Caption = 'Tipos de Movimentos de Regras Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 459
        Height = 31
        Caption = 'Tipos de Movimentos de Regras Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 329
    Width = 674
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 670
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 372
    Width = 674
    Height = 63
    Align = alBottom
    TabOrder = 5
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 670
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 529
        Top = 0
        Width = 141
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdStqCenCad
    QryCampo = 'StqCenCad'
    UpdCampo = 'StqCenCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 284
    Top = 56
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 256
    Top = 56
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu, Codigo, Nome, EntiSitio'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 228
    Top = 56
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrStqCenCadEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object QrFiliais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Filial, Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL'
      'FROM entidades'
      'WHERE Codigo<-10'
      'ORDER BY NOMEFILIAL')
    Left = 312
    Top = 56
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliaisNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 340
    Top = 56
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdEmpresa
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 368
    Top = 56
  end
  object dmkValUsu3: TdmkValUsu
    dmkEditCB = EdGraCusPrc
    QryCampo = 'GraCusPrc'
    UpdCampo = 'GraCusPrc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 452
    Top = 56
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gracusprc'
      'WHERE CustoPreco=:P0'
      'ORDER BY Nome')
    Left = 396
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 424
    Top = 56
  end
end
