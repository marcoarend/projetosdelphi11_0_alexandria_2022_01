unit GraAtrIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, dmkImage, UnDmkEnums;

type
  TFmGraAtrIts = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    DBEdit1: TdmkDBEdit;
    EdNome: TdmkEdit;
    CkContinuar: TCheckBox;
    EdCodUsu: TdmkEdit;
    Label2: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    EdControle: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraAtrIts: TFmGraAtrIts;

implementation

uses UnMyObjects, Module, UMySQLModule, GraAtrCad, UnInternalConsts, dmkGeral;

{$R *.DFM}

procedure TFmGraAtrIts.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('graatrits', 'Controle', ImgTipo.SQLType,
    FmGraAtrCad.QrGraAtrItsControle.Value);
  EdControle.ValueVariant := Controle;
  if UMyMod.ExecSQLInsUpdFm(FmGraAtrIts, ImgTipo.SQLType, 'graatrits', Controle,
  Dmod.QrUpd) then
  begin
    FmGraAtrCad.ReopenGraAtrIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType        := stIns;
      EdCodUsu.ValueVariant := 0;
      EdNome.Text           := '';
      EdCodUsu.SetFocus;
    end else Close;
  end;
end;

procedure TFmGraAtrIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraAtrIts.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'graatrits', 'CodUsu', ['Codigo'],
      [FmGraAtrCad.QrGraAtrCadCodigo.Value], stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmGraAtrIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraAtrIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmGraAtrIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraAtrIts.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'graatrits', 'CodUsu', ['Codigo'],
      [FmGraAtrCad.QrGraAtrCadCodigo.Value], stIns, 0, siPositivo, EdCodUsu);
    CkContinuar.Checked := False;
    CkContinuar.Visible := True;
    EdNome.SetFocus;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
    EdCodUsu.SetFocus;
  end;
end;

end.
