unit GraCorCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB, dmkValUsu, MyDBCheck, dmkCheckBox, dmkImage,
  UnDmkEnums;

type
  TFmGraCorCad = class(TForm)
    PainelDados: TPanel;
    DsGraCorCad: TDataSource;
    QrGraCorCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadCodUsu: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    QrGraCorCadPantone: TWideStringField;
    QrGraCorCadFamilia: TIntegerField;
    QrGraCorCadNOMEFAMILIA: TWideStringField;
    QrGraCorFam: TmySQLQuery;
    DsGraCorFam: TDataSource;
    QrGraCorFamCodigo: TIntegerField;
    QrGraCorFamCodUsu: TIntegerField;
    QrGraCorFamNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    QrGraCorCadREF_FAMILIA: TIntegerField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    Panel4: TPanel;
    GroupBox3: TGroupBox;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    EdNome: TdmkEdit;
    Panel6: TPanel;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    CBFamilia: TdmkDBLookupComboBox;
    EdFamilia: TdmkEditCB;
    QrGraCorPan: TmySQLQuery;
    DsGraCorPan: TDataSource;
    QrGraCorPanCodigo: TIntegerField;
    QrGraCorPanCodUsu: TIntegerField;
    QrGraCorPanNome: TWideStringField;
    EdPantone: TdmkEditCB;
    Label4: TLabel;
    Label5: TLabel;
    CBPantone: TdmkDBLookupComboBox;
    dmkValUsu2: TdmkValUsu;
    QrGraCorCadNOMEPENTONE: TWideStringField;
    QrGraCorCadREF_PANTONE: TIntegerField;
    DBEdit6: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    QrGraCorCadPrintCor: TSmallintField;
    DBCheckBox1: TDBCheckBox;
    CkPrintCor: TdmkCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel8: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    LaRegistro: TStaticText;
    Panel9: TPanel;
    Panel10: TPanel;
    BitBtn5: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraCorCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraCorCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure CBFamiliaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBPantoneKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmGraCorCad: TFmGraCorCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, GraCorPan, GraCorFam, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraCorCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraCorCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraCorCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraCorCad.DefParams;
begin
  VAR_GOTOTABELA := 'GraCorCad';
  VAR_GOTOMYSQLTABLE := QrGraCorCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT gcc.Codigo, gcc.CodUsu, gcc.Nome, gcc.Pantone,');
  VAR_SQLx.Add('gcc.Familia, gcf.Nome NOMEFAMILIA, gcf.CodUsu REF_FAMILIA,');
  VAR_SQLx.Add('gcp.Nome NOMEPENTONE, gcp.CodUsu REF_PANTONE, gcc.PrintCor');
  VAR_SQLx.Add('FROM gracorcad gcc');
  VAR_SQLx.Add('LEFT JOIN gracorfam gcf ON gcc.Familia=gcf.Codigo');
  VAR_SQLx.Add('LEFT JOIN gracorpan gcp ON gcc.Pantone=gcp.Codigo');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE gcc.Codigo > -1000000');
  //
  VAR_SQL1.Add('AND gcc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND gcc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND gcc.Nome Like :P0');
  //
end;

procedure TFmGraCorCad.CBFamiliaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraCorFam', Dmod.MyDB,
    ''(*Extra*), EdFamilia, CBFamilia, dmktfInteger);
end;

procedure TFmGraCorCad.CBPantoneKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraCorPan', Dmod.MyDB,
    ''(*Extra*), EdPantone, CBPantone, dmktfInteger);
end;

procedure TFmGraCorCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
  begin
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraCorCad', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
  end;
end;

procedure TFmGraCorCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraCorCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;
{
procedure TFmGraCorCad.AlteraRegistro;
var
  GraCorCad : Integer;
begin
  GraCorCad := QrGraCorCadCodigo.Value;
  if QrGraCorCadCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(GraCorCad, Dmod.MyDB, 'GraCorCad', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GraCorCad, Dmod.MyDB, 'GraCorCad', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGraCorCad.IncluiRegistro;
var
  Cursor : TCursor;
  GraCorCad : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    GraCorCad := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GraCorCad', 'GraCorCad', 'Codigo');
    if Length(FormatFloat(FFormatFloat, GraCorCad))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, GraCorCad);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}
procedure TFmGraCorCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraCorCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraCorCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraCorCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraCorCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraCorCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraCorCad.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmGraCorPan, FmGraCorPan, afmoNegarComAviso) then
  begin
    FmGraCorPan.ShowModal;
    FmGraCorPan.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrGraCorPan.Close;
      UnDmkDAC_PF.AbreQuery(QrGraCorPan, Dmod.MyDB);
      if QrGraCorPan.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdPantone.ValueVariant := QrGraCorPanCodUsu.Value;
        CBPantone.KeyValue     := QrGraCorPanCodUsu.Value;
      end;
    end;
  end;
end;

procedure TFmGraCorCad.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmGraCorFam, FmGraCorFam, afmoNegarComAviso) then
  begin
    FmGraCorFam.ShowModal;
    FmGraCorFam.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrGraCorFam.Close;
      UnDmkDAC_PF.AbreQuery(QrGraCorFam, Dmod.MyDB);
      if QrGraCorFam.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdFamilia.ValueVariant := QrGraCorFamCodUsu.Value;
        CBFamilia.KeyValue     := QrGraCorFamCodUsu.Value;
      end;
    end;
  end;
end;

procedure TFmGraCorCad.BtAlteraClick(Sender: TObject);
begin
  if (QrGraCorCad.State = dsInactive) or (QrGraCorCad.RecordCount = 0) or
    (QrGraCorCadCodigo.Value = 0) then Exit;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraCorCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'gracorcad');
end;

procedure TFmGraCorCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraCorCadCodigo.Value;
  Close;
end;

procedure TFmGraCorCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, CodUsu: Integer;
  Nome: String;
begin
  CodUsu := EdCodUsu.ValueVariant;
  Nome   := EdNome.ValueVariant;
  //
  if MyObjects.FIC(CodUsu = 0, EdCodUsu, 'Defina o c�digo!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('GraCorCad', 'Codigo', ImgTipo.SQLType,
    QrGraCorCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraCorCad, PainelEdit,
    'GraCorCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraCorCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraCorCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraCorCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraCorCad', 'Codigo');
end;

procedure TFmGraCorCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrGraCorCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'gracorcad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraCorCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
end;

procedure TFmGraCorCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  UnDmkDAC_PF.AbreQuery(QrGraCorFam, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCorPan, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmGraCorCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraCorCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraCorCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmGraCorCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraCorCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraCorCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraCorCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraCorCad.QrGraCorCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraCorCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraCorCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraCorCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GraCorCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraCorCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraCorCad.QrGraCorCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraCorCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

