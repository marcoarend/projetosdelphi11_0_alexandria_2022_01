unit CheckAtributos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids,
  dmkDBGrid, DBCtrls, dmkGeral, dmkImage, UnDmkEnums(*&&, ABSMain*);

type
  TFmCheckAtributos = class(TForm)
    Panel1: TPanel;
    DataSource1: TDataSource;
    DBGrid1: TdmkDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmCheckAtributos: TFmCheckAtributos;

implementation

uses UnMyObjects(*&&, UnDmkABS_PF*);

{$R *.DFM}

procedure TFmCheckAtributos.BtOKClick(Sender: TObject);
begin
(*&&
  FCadastrar := True;
  Query.Filter   := 'Ativo=1';
  Query.Filtered := True;
  //
  if Query.RecordCount > 0 then
    Close
  else begin
    Query.Filtered := False;
    Application.MessageBox('Nenhum item foi selecionado!', 'Avido',
    MB_OK+MB_ICONWARNING);
  end;
*)
end;

procedure TFmCheckAtributos.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmCheckAtributos.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
  Codigo, Controle: Integer;
begin
(*&&
  if Column.FieldName = 'Ativo' then
  begin
    if Query.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Ativo').Value := Status;
    Query.Post;
    //
    if Status = 1 then
    begin
      Codigo   := QueryCodigo.Value;
      Controle := QueryControle.Value;
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('UPDATE atributos SET Ativo = 0 ');
      Query.SQL.Add('WHERE Codigo = ' + FormatFloat('0', Codigo));
      Query.SQL.Add('AND Controle <> ' + FormatFloat('0', Controle) + ';');
      Query.SQL.Add('SELECT * FROM atributos;');
      DmkABS_PF.AbreQuery(Query);
      //
      Query.Locate('Controle', Controle, []);
    end;
  end;
*)
end;

procedure TFmCheckAtributos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCheckAtributos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCadastrar := False;
end;

procedure TFmCheckAtributos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

(*&&
object Query: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 12
  Top = 12
  object QueryAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
  object QueryCodUsu: TIntegerField
    FieldName = 'CodUsu'
  end
  object QueryNomeCad: TWideStringField
    FieldName = 'NomeCad'
    Size = 30
  end
  object QueryNomeIts: TWideStringField
    FieldName = 'NomeIts'
    Size = 30
  end
  object QueryControle: TIntegerField
    FieldName = 'Controle'
  end
  object QueryCodigo: TIntegerField
    FieldName = 'Codigo'
  end
end
*)
end.

