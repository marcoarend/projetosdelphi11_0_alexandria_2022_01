unit StqInnCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy,UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, dmkDBGrid, frxClass, frxDBSet,
  UnInternalConsts3, Variants, UnDmkProcFunc, dmkImage, UnDmkEnums, UnGrl_Geral,
  dmkCheckBox, Vcl.ComCtrls, dmkLabelRotate;

type
  TFmStqInnCad = class(TForm)
    PainelDados: TPanel;
    DsStqInnCad: TDataSource;
    QrStqInnCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrStqMovIts: TmySQLQuery;
    DsStqMovIts: TDataSource;
    PMEntrada: TPopupMenu;
    PMItens: TPopupMenu;
    DBGStqMovIts: TDBGrid;
    N1: TMenuItem;
    IncluiporXML1: TMenuItem;
    QrStqInnCadCodigo: TIntegerField;
    QrStqInnCadCodUsu: TIntegerField;
    QrStqInnCadNome: TWideStringField;
    QrStqInnCadEmpresa: TIntegerField;
    QrStqInnCadAbertura: TDateTimeField;
    QrStqInnCadEncerrou: TDateTimeField;
    QrStqInnCadStatus: TSmallintField;
    QrStqInnCadLk: TIntegerField;
    QrStqInnCadDataCad: TDateField;
    QrStqInnCadDataAlt: TDateField;
    QrStqInnCadUserCad: TIntegerField;
    QrStqInnCadUserAlt: TIntegerField;
    QrStqInnCadAlterWeb: TSmallintField;
    QrStqInnCadAtivo: TSmallintField;
    QrStqMovItsNivel1: TIntegerField;
    QrStqMovItsNO_PRD: TWideStringField;
    QrStqMovItsPrdGrupTip: TIntegerField;
    QrStqMovItsUnidMed: TIntegerField;
    QrStqMovItsNO_PGT: TWideStringField;
    QrStqMovItsSIGLA: TWideStringField;
    QrStqMovItsNO_TAM: TWideStringField;
    QrStqMovItsNO_COR: TWideStringField;
    QrStqMovItsGraCorCad: TIntegerField;
    QrStqMovItsGraGruC: TIntegerField;
    QrStqMovItsGraGru1: TIntegerField;
    QrStqMovItsGraTamI: TIntegerField;
    QrStqMovItsDataHora: TDateTimeField;
    QrStqMovItsIDCtrl: TIntegerField;
    QrStqMovItsTipo: TIntegerField;
    QrStqMovItsOriCodi: TIntegerField;
    QrStqMovItsOriCtrl: TIntegerField;
    QrStqMovItsOriCnta: TIntegerField;
    QrStqMovItsEmpresa: TIntegerField;
    QrStqMovItsStqCenCad: TIntegerField;
    QrStqMovItsGraGruX: TIntegerField;
    QrStqMovItsQtde: TFloatField;
    QrStqMovItsAlterWeb: TSmallintField;
    QrStqMovItsAtivo: TSmallintField;
    QrStqMovItsOriPart: TIntegerField;
    QrStqMovItsPecas: TFloatField;
    QrStqMovItsPeso: TFloatField;
    QrStqMovItsAreaM2: TFloatField;
    QrStqMovItsAreaP2: TFloatField;
    QrStqMovItsFatorClas: TFloatField;
    QrStqMovItsQuemUsou: TIntegerField;
    QrStqMovItsRetorno: TSmallintField;
    QrStqMovItsParTipo: TIntegerField;
    QrStqMovItsParCodi: TIntegerField;
    QrStqMovItsDebCtrl: TIntegerField;
    QrStqMovItsSMIMultIns: TIntegerField;
    Incluiitem1: TMenuItem;
    Excluiitem1: TMenuItem;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    QrStqInnCadNOMEFILIAL: TWideStringField;
    QrStqInnCadNO_FORNECE: TWideStringField;
    QrStqInnCadBalQtdItem: TFloatField;
    QrStqInnCadFatSemEstq: TSmallintField;
    QrStqInnCadFornece: TIntegerField;
    QrStqInnCadENCERROU_TXT: TWideStringField;
    GroupBox1: TGroupBox;
    QrFornece: TMySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    GroupBox5: TGroupBox;
    dmkLabel4: TdmkLabel;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    Incluinovaentrada1: TMenuItem;
    Alteraentradaatual1: TMenuItem;
    Excluientradaatual1: TMenuItem;
    GroupBox2: TGroupBox;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    Label13: TLabel;
    EdNome: TdmkEdit;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    dmkLabel5: TdmkLabel;
    SpeedButton6: TSpeedButton;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    VUEmpresa: TdmkValUsu;
    QrStqMovItsCustoAll: TFloatField;
    QrStqMovItsValorAll: TFloatField;
    N2: TMenuItem;
    frxSTQ_ENTRA_001_1: TfrxReport;
    frxDsStqMovIts: TfrxDBDataset;
    QrForne: TmySQLQuery;
    QrForneE_ALL: TWideStringField;
    QrForneCNPJ_TXT: TWideStringField;
    QrForneNOME_TIPO_DOC: TWideStringField;
    QrForneTE1_TXT: TWideStringField;
    QrForneFAX_TXT: TWideStringField;
    QrForneNUMERO_TXT: TWideStringField;
    QrForneCEP_TXT: TWideStringField;
    QrForneCodigo: TIntegerField;
    QrForneTipo: TSmallintField;
    QrForneCodUsu: TIntegerField;
    QrForneNOME_ENT: TWideStringField;
    QrForneCNPJ_CPF: TWideStringField;
    QrForneIE_RG: TWideStringField;
    QrForneRUA: TWideStringField;
    QrForneCOMPL: TWideStringField;
    QrForneBAIRRO: TWideStringField;
    QrForneCIDADE: TWideStringField;
    QrForneNOMELOGRAD: TWideStringField;
    QrForneNOMEUF: TWideStringField;
    QrFornePais: TWideStringField;
    QrForneENDEREF: TWideStringField;
    QrForneTE1: TWideStringField;
    QrForneFAX: TWideStringField;
    QrForneIE: TWideStringField;
    QrForneCAD_FEDERAL: TWideStringField;
    QrForneCAD_ESTADUAL: TWideStringField;
    QrForneIE_TXT: TWideStringField;
    QrForneNO2_ENT: TWideStringField;
    frxDsForne: TfrxDBDataset;
    frxDsStqInnCad: TfrxDBDataset;
    PMPagto: TPopupMenu;
    Panel4: TPanel;
    Splitter1: TSplitter;
    IncluiBuy1: TMenuItem;
    ExcluiBuy1: TMenuItem;
    QrPgtBuy: TMySQLQuery;
    QrPgtBuyData: TDateField;
    QrPgtBuyVencimento: TDateField;
    QrPgtBuyBanco: TIntegerField;
    QrPgtBuySEQ: TIntegerField;
    QrPgtBuyFatID: TIntegerField;
    QrPgtBuyContaCorrente: TWideStringField;
    QrPgtBuyDocumento: TFloatField;
    QrPgtBuyDescricao: TWideStringField;
    QrPgtBuyFatParcela: TIntegerField;
    QrPgtBuyFatNum: TFloatField;
    QrPgtBuyNOMECARTEIRA: TWideStringField;
    QrPgtBuyNOMECARTEIRA2: TWideStringField;
    QrPgtBuyBanco1: TIntegerField;
    QrPgtBuyAgencia1: TIntegerField;
    QrPgtBuyConta1: TWideStringField;
    QrPgtBuyTipoDoc: TSmallintField;
    QrPgtBuyControle: TIntegerField;
    QrPgtBuyNOMEFORNECEI: TWideStringField;
    QrPgtBuyCARTEIRATIPO: TIntegerField;
    DsPgtBuy: TDataSource;
    QrPgtBuyDebito: TFloatField;
    N3: TMenuItem;
    Encerraentrada1: TMenuItem;
    SpeedButton5: TSpeedButton;
    QrPgtBuyAgencia: TIntegerField;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtEntrada: TBitBtn;
    BtItens: TBitBtn;
    BtPagto: TBitBtn;
    QrForneNUMERO: TFloatField;
    QrForneUF: TFloatField;
    QrForneLOGRAD: TFloatField;
    QrForneCEP: TFloatField;
    QrStqInnCadCodCliInt: TIntegerField;
    Panel8: TPanel;
    Label5: TLabel;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Label6: TLabel;
    EdNFe_Id: TdmkEdit;
    Label192: TLabel;
    GroupBox4: TGroupBox;
    DBEdit3: TDBEdit;
    QrStqInnCadnfe_serie: TIntegerField;
    QrStqInnCadnfe_nNF: TIntegerField;
    QrStqInnCadnfe_Id: TWideStringField;
    Label9: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit4: TDBEdit;
    DBEdNFe_Id: TDBEdit;
    PMNovo: TPopupMenu;
    ID1: TMenuItem;
    Pesquisadetalhada1: TMenuItem;
    CkAtzPrcMed: TdmkCheckBox;
    DBCkAtzPrcMed: TDBCheckBox;
    QrStqInnCadAtzPrcMed: TSmallintField;
    MeAvisos: TMemo;
    QrPgtFrt: TMySQLQuery;
    QrPgtFrtData: TDateField;
    QrPgtFrtVencimento: TDateField;
    QrPgtFrtBanco: TIntegerField;
    QrPgtFrtSEQ: TIntegerField;
    QrPgtFrtFatID: TIntegerField;
    QrPgtFrtContaCorrente: TWideStringField;
    QrPgtFrtDocumento: TFloatField;
    QrPgtFrtDescricao: TWideStringField;
    QrPgtFrtFatParcela: TIntegerField;
    QrPgtFrtFatNum: TFloatField;
    QrPgtFrtNOMECARTEIRA: TWideStringField;
    QrPgtFrtNOMECARTEIRA2: TWideStringField;
    QrPgtFrtBanco1: TIntegerField;
    QrPgtFrtAgencia1: TIntegerField;
    QrPgtFrtConta1: TWideStringField;
    QrPgtFrtTipoDoc: TSmallintField;
    QrPgtFrtControle: TIntegerField;
    QrPgtFrtNOMEFORNECEI: TWideStringField;
    QrPgtFrtCARTEIRATIPO: TIntegerField;
    QrPgtFrtDebito: TFloatField;
    QrPgtFrtAgencia: TIntegerField;
    DsPgtFrt: TDataSource;
    Produtos1: TMenuItem;
    Frete1: TMenuItem;
    IncluiFrt1: TMenuItem;
    ExcluiFrt1: TMenuItem;
    QrStqInnCadTransporta: TIntegerField;
    QrStqInnCadNO_TRANSPORTA: TWideStringField;
    dmkLabel2: TdmkLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrTransporta: TMySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNO_ENT: TWideStringField;
    DsTransporta: TDataSource;
    dmkLabel3: TdmkLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    SbTransporta: TSpeedButton;
    QrSumFrt: TMySQLQuery;
    QrStqInnCadValTotFrete: TFloatField;
    QrSumFrtDebito: TFloatField;
    QrSumIts: TMySQLQuery;
    QrSumItsQtde: TFloatField;
    QrStqMovItsCustoBuy: TFloatField;
    QrStqMovItsCustoFrt: TFloatField;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    DBGBuy: TDBGrid;
    dmkLabelRotate1: TdmkLabelRotate;
    dmkLabelRotate2: TdmkLabelRotate;
    DBGFrt: TDBGrid;
    Fracionamento1: TMenuItem;
    N4: TMenuItem;
    QrStqMovItsPackGGX: TIntegerField;
    QrStqMovItsPackUnMed: TIntegerField;
    QrStqMovItsPackQtde: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqInnCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqInnCadBeforeOpen(DataSet: TDataSet);
    procedure BtEntradaClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrStqInnCadBeforeClose(DataSet: TDataSet);
    procedure QrStqInnCadAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure IncluiporXML1Click(Sender: TObject);
    procedure Incluiitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure QrStqInnCadCalcFields(DataSet: TDataSet);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Incluinovaentrada1Click(Sender: TObject);
    procedure Alteraentradaatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrForneCalcFields(DataSet: TDataSet);
    procedure BtPagtoClick(Sender: TObject);
    procedure IncluiBuy1Click(Sender: TObject);
    procedure Encerraentrada1Click(Sender: TObject);
    procedure ExcluiBuy1Click(Sender: TObject);
    procedure QrStqInnCadAfterClose(DataSet: TDataSet);
    procedure Excluientradaatual1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMEntradaPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure PMPagtoPopup(Sender: TObject);
    procedure ID1Click(Sender: TObject);
    procedure Pesquisadetalhada1Click(Sender: TObject);
    procedure QrPgtBuyCalcFields(DataSet: TDataSet);
    procedure IncluiFrt1Click(Sender: TObject);
    procedure ExcluiFrt1Click(Sender: TObject);
    procedure QrPgtFrtCalcFields(DataSet: TDataSet);
    procedure Fracionamento1Click(Sender: TObject);
  private
    FTabLctA: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure HabilitaBotoes();
    procedure AtivaItensNoEstoque(IDCtrl: Integer);
    procedure ObtemListaGraGruXFromStqInnIts(var Lista: TStringList);
    procedure AtualizaPrecosStqInnIts(Lista: TStringList; Empresa: Integer);
  public
    { Public declarations }
    FThisFatID_Buy: Integer;
    FThisFatID_Frt: Integer;
    FStqInnNFe, FStqInnOpt: String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenStqMovIts(IDCtrl: Integer);
    procedure ReopenPagtosBuy();
    procedure ReopenPagtosFrt();
    procedure RateioCustoDoFrete();

  end;

var
  FmStqInnCad: TFmStqInnCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF,  UnDmkWeb, StqInnPesq,
  {$IfNDef semNFe_v0000}ModuleNFe_0000, NFeImporta_0400,{$EndIf}
  {$IFDEF FmPlacasAdd}PlacasAdd,{$ENDIF}
  {$IfNDef NO_FINANCEIRO}ModuleFin, UnPagtos,{$EndIf}
  UnGrade_Tabs, ModuleGeral, UCreate, StqInnNFe, Entidade2, StqInnIts,
  StqInnPack, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqInnCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqInnCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqInnCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqInnCad.DefParams;
begin
  VAR_GOTOTABELA := 'StqInnCad';
  VAR_GOTOMYSQLTABLE := QrStqInnCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('pem.BalQtdItem, pem.FatSemEstq, sic.*, ei.CodCliInt');
  VAR_SQLx.Add('FROM stqinncad sic');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades fil  ON fil.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem  ON pem.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece');
  VAR_SQLx.Add('LEFT JOIN entidades tsp ON tsp.Codigo=sic.Transporta');
  VAR_SQLx.Add('WHERE sic.Codigo > -1000');
  VAR_SQLx.Add('AND sic.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND sic.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sic.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sic.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Codigo IN (SELECT Codigo FROM stqinncad WHERE Empresa IN (' + VAR_LIB_EMPRESAS + '))';
end;

procedure TFmStqInnCad.RateioCustoDoFrete();
var
  Codigo, IDCtrl, GraGruX, Empresa: Integer;
  Fator, SumQtde, ValTotFrete, CustoFrt, CustoAll: Double;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  Codigo  := QrStqInnCadCodigo.Value;
  Empresa := QrStqInnCadEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumFrt, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito',
  'FROM ' + FTabLctA + ' la',
  'WHERE FatNum=' + Geral.FF0(QrStqInnCadCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  '']);
  //
  ValTotFrete := QrSumFrtDebito.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(CustoBuy) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Tipo=' + Geral.FF0(FThisFatID_Buy),
  'AND OriCodi=' + Geral.FF0(QrStqInnCadCodigo.Value),
  '']);
  //
  SumQtde := QrSumItsQtde.Value;
  //
  if SumQtde > 0 then
    Fator := ValTotFrete / SumQtde
  else
    Fator := 0;
  //
  ReopenStqMovIts(0);
  QrStqMovIts.DisableControls;
  try
    QrStqMovIts.First;
    while not QrStqMovIts.Eof do
    begin
      CustoFrt := QrStqMovItsCustoBuy.Value * Fator;
      CustoAll := QrStqMovItsCustoBuy.Value + CustoFrt;
      //
      GraGruX  := QrStqMovItsGraGruX.Value;
      IDCtrl   := QrStqMovItsIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
      'CustoFrt', 'CustoAll'], [
      'IDCtrl'], [
      CustoFrt, CustoAll], [
      IDCtrl], False);
      //
      DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
      //
      QrStqMovIts.Next;
    end;
  finally
    QrStqMovIts.EnableControls;
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqinncad', False, [
  'ValTotFrete'], [
  'Codigo'], [
  ValTotFrete], [
  Codigo], False);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmStqInnCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqInnCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqInnCad.Encerraentrada1Click(Sender: TObject);
var
  Codigo, Empresa: Integer;
  ListaGraGruX: TStringList;
begin
  Codigo := QrStqInnCadCodigo.Value;
  //
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqinncad SET Encerrou=:P0, Status=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2');
  //
  if QrStqInnCadEncerrou.Value = 0 then
  begin
    Dmod.QrUpd.Params[0].AsDateTime := DModG.ObtemAgora();
    Dmod.QrUpd.Params[1].AsInteger  := 1;
  end else
  begin
    Dmod.QrUpdM.Close;
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('SELECT MAX(Codigo) Codigo');
    Dmod.QrUpdM.SQL.Add('FROM stqinncad');
    Dmod.QrUpdM.SQL.Add('WHERE Fornece=:P0');
    Dmod.QrUpdM.Params[0].AsInteger := QrStqInnCadFornece.Value;
    UnDmkDAC_PF.AbreQuery(Dmod.QrUpdM, Dmod.MyDB);
    if Dmod.QrUpdM.FieldByName('Codigo').Value <> Null then
    begin
      if Dmod.QrUpdM.FieldByName('Codigo').Value <> QrStqInnCadCodigo.Value then
      begin
        Geral.MensagemBox('Somente a �ltima entrada do fornecedor ' +
          sLineBreak + QrStqInnCadNO_FORNECE.Value + '.' + sLineBreak + 'Pode ser desfeita!',
          'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;  
    //
    Dmod.QrUpd.Params[0].AsDateTime := 0;
    Dmod.QrUpd.Params[1].AsInteger  := 0;
  end;
  //
  Dmod.QrUpd.Params[2].AsInteger  := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  AtivaItensNoEstoque(QrStqInnCadCodigo.Value);
  //
  //Atualiza lista de pre�os
  Screen.Cursor := crHourGlass;
  ListaGraGruX  := TStringList.Create;
  Empresa       := QrStqInnCadEmpresa.Value;
  try
    ObtemListaGraGruXFromStqInnIts(ListaGraGruX);
    //
    AtualizaPrecosStqInnIts(ListaGraGruX, Empresa);
  finally
    ListaGraGruX.Free;
    //
    Screen.Cursor := crDefault;
  end;
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmStqInnCad.ExcluiBuy1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados: Integer;
begin
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID_Buy, QrStqInnCadCodigo.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPgtBuy, DBGBuy,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  ReopenPagtosBuy();
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmStqInnCad.Excluientradaatual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrStqMovIts.RecordCount > 0 then
    Geral.MensagemBox('Esta entrada n�o pode ser exclu�da. Pois existem itens lan�ados nela.',
      'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o da entrada ID n�mero ' +
      Geral.FF0(QrStqInnCadCodigo.Value) + '?') <> ID_YES then Exit;
    //
    Codigo := QrStqInnCadCodigo.Value;
    //
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqinncad WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    LocCod(Codigo, Codigo);
    Va(vpLast);
  end;
end;

procedure TFmStqInnCad.ExcluiFrt1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados: Integer;
begin
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID_Frt, QrStqInnCadCodigo.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPgtFrt, DBGFrt,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  RateioCustoDoFrete();
  ReopenPagtosFrt();
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmStqInnCad.Excluiitem1Click(Sender: TObject);
var
  Continua: Boolean;
  Empresa, GraGruX: Integer;
  ListaGraGruX: TStringList;
begin
  Continua     := False;
  ListaGraGruX := TStringList.Create;
  Empresa      := QrStqInnCadEmpresa.Value;
  try
    ObtemListaGraGruXFromStqInnIts(ListaGraGruX);
    //
    Continua := DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrStqMovIts, DBGStqMovIts,
                  'StqMovItsA', ['IDCtrl'], ['IDCtrl'], istPergunta, '') > 0;
  finally
    if (Continua) and (ListaGraGruX.Count > 0) then
      AtualizaPrecosStqInnIts(ListaGraGruX, Empresa);
    //
    ListaGraGruX.Free;
  end;
end;

procedure TFmStqInnCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqInnCad.Pesquisadetalhada1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmStqInnPesq, FmStqInnPesq, afmoNegarComAviso) then
  begin
    FmStqInnPesq.EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.FThisFatID             := FThisFatID_Buy;
    FmStqInnPesq.ShowModal;
    FmStqInnPesq.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
    LocCod(VAR_CADASTRO, VAR_CADASTRO);
end;

procedure TFmStqInnCad.PMEntradaPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrStqInnCad.State <> dsInactive) and (QrStqInnCad.RecordCount > 0);
  //
  if Enab then
    Enab2 := QrStqInnCadEncerrou.Value = 0
  else
    Enab2 := False;
  //
  Alteraentradaatual1.Enabled := Enab and Enab2;
  Excluientradaatual1.Enabled := Enab and Enab2;
  Encerraentrada1.Enabled     := Enab;
end;

procedure TFmStqInnCad.PMItensPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrStqInnCad.State <> dsInactive) and (QrStqInnCad.RecordCount > 0);
  Enab2 := (QrStqMovIts.State <> dsInactive) and (QrStqMovIts.RecordCount > 0);
  //
  Incluiitem1.Enabled := Enab;
  Fracionamento1.Enabled := Enab and Enab2;
  Excluiitem1.Enabled := Enab and Enab2;
end;

procedure TFmStqInnCad.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrStqInnCad.State <> dsInactive) and (QrStqInnCad.RecordCount > 0);
  Enab2 := (QrPgtBuy.State <> dsInactive) and (QrPgtBuy.RecordCount > 0);
  Enab3 := (QrPgtFrt.State <> dsInactive) and (QrPgtFrt.RecordCount > 0);
  //
  IncluiBuy1.Enabled := Enab;
  ExcluiBuy1.Enabled := Enab and Enab2;
  //
  IncluiFrt1.Enabled := Enab;
  ExcluiFrt1.Enabled := Enab and Enab3;
  //
end;

procedure TFmStqInnCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqInnCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqInnCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqInnCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqInnCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqInnCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqInnCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqInnCad.SpeedButton5Click(Sender: TObject);
begin
  {$IFDEF FmPlacasAdd}
  if DBCheck.CriaFm(TFmPlacasAdd, FmPlacasAdd, afmoNegarComAviso) then
  begin
    FmPlacasAdd.ShowModal;
    EdNome.ValueVariant := FmPlacasAdd.FPlacas;
    FmPlacasAdd.Destroy;
  end;
  {$ENDIF}
end;

procedure TFmStqInnCad.SpeedButton6Click(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdFornece.ValueVariant;

  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFornece, CBFornece, QrFornece, VAR_CADASTRO);
    EdFornece.SetFocus;
  end;
end;

procedure TFmStqInnCad.Alteraentradaatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrStqInnCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'StqInnCad');
  //
  GroupBox2.Enabled := QrStqMovIts.RecordCount = 0;
end;

procedure TFmStqInnCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqInnCadCodigo.Value;
  Close;
end;

procedure TFmStqInnCad.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmStqInnCad.BtPagtoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagto);
end;

procedure TFmStqInnCad.AtivaItensNoEstoque(IDCtrl: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE stqmovitsa ',
    'SET Ativo = 1 ',
    'WHERE Tipo="' + Geral.FF0(FThisFatID_Buy) + '"',
    'AND OriCodi="' + Geral.FF0(IDCtrl) + '"',
    '']);
end;

procedure TFmStqInnCad.AtualizaPrecosStqInnIts(Lista: TStringList; Empresa: Integer);
var
  I, GraGruX: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    for I := 0 to Lista.Count - 1 do
    begin
      GraGruX := Geral.IMV(Lista[I]);
      //
      DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmStqInnCad.ObtemListaGraGruXFromStqInnIts(var Lista: TStringList);
var
  J, GraGruX: Integer;
begin
  Screen.Cursor        := crHourGlass;
  DBGStqMovIts.Enabled := False;
  //
  QrStqMovIts.DisableControls;
  try
    while not QrStqMovIts.Eof do
    begin
      GraGruX := QrStqMovItsGraGruX.Value;
      //
      if (GraGruX <> 0) and (Lista.Find(Geral.FF0(GraGruX), J) = False) then
        Lista.Add(Geral.FF0(GraGruX));
      //
      QrStqMovIts.Next;
    end;
  finally
    QrStqMovIts.EnableControls;
    //
    DBGStqMovIts.Enabled := True;
    Screen.Cursor        := crDefault;
  end;
end;

procedure TFmStqInnCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(EdFilial.ValueVariant = 0, EdFilial, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdFornece.ValueVariant = 0, EdFornece, 'Informe o fornecedor!') then Exit;
  if MyObjects.FIC(EdCodUsu.ValueVariant = 0, EdCodUsu, 'Informe o c�digo!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('StqInnCad', 'Codigo', ImgTipo.SQLType, QrStqInnCadCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmStqInnCad, PainelEdita,
    'StqInnCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmStqInnCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'StqInnCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqInnCad', 'Codigo');
  MostraEdicao(0, stlok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqInnCad', 'Codigo');
end;

procedure TFmStqInnCad.BtEntradaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntrada, BtEntrada);
end;

procedure TFmStqInnCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FThisFatID_Buy := VAR_FATID_0005;
  FThisFatID_Frt := VAR_FATID_0006;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBFilial.ListSource := DModG.DsEmpresas;
  DBGStqMovIts.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  //
  if UpperCase(Application.Title) = 'SAFECAR' then
  begin
    EdNome.Width         := 817;
    SpeedButton5.Visible := True;
  end else
  begin
    EdNome.Width         := 845;
    SpeedButton5.Visible := False;    
  end;
end;

procedure TFmStqInnCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqInnCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqInnCad.SbImprimeClick(Sender: TObject);
begin
  DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  DModG.ReopenEndereco(Geral.IMV(VAR_LIB_EMPRESAS));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrForne, Dmod.MyDB, [
    'SELECT en.Codigo, Tipo, CodUsu, IE, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF,',
    'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO2_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG,',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA,',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0.000 NUMERO,',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL,',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO,',
    'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CIDADE,',
    'CASE WHEN en.Tipo=0 THEN en.EUF         ELSE en.PUF      END + 0.000 UF,',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD,',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF,',
    'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais,',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0.000 Lograd,',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0.000 CEP,',
    'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF,',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1,',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX,',
    'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL',
    'FROM entidades en',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd',
    'WHERE en.Codigo=' + Geral.FF0(QrStqInnCadFornece.Value),
    '']);
  //
  MyObjects.frxDefineDataSets(frxSTQ_ENTRA_001_1, [
    DModG.frxDsMaster,
    DModG.frxDsEndereco,
    frxDsForne,
    frxDsStqInnCad,
    frxDsStqMovIts
    ]);
  //
  MyObjects.frxMostra(frxSTQ_ENTRA_001_1, 'Informe da Compra n� ' +
    FormatFloat('000000', QrStqInnCadCodUsu.Value));
end;

procedure TFmStqInnCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqInnCad.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, SbNovo);
end;

procedure TFmStqInnCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqInnCad.QrForneCalcFields(DataSet: TDataSet);
begin
  QrForneTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneTe1.Value);
  QrForneFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneFax.Value);
  QrForneCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrForneCNPJ_CPF.Value);
  QrForneIE_TXT.Value :=
    Geral.Formata_IE(QrForneIE_RG.Value, QrForneUF.Value, '??', QrForneTipo.Value);
  QrForneNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrForneRua.Value, Trunc(QrForneNumero.Value), False);
  //
  QrForneE_ALL.Value := UpperCase(QrForneNOMELOGRAD.Value);
  if Trim(QrForneE_ALL.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ';
  QrForneE_ALL.Value := QrForneE_ALL.Value + Uppercase(QrForneRua.Value);
  if Trim(QrForneRua.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNUMERO_TXT.Value;
  if Trim(QrForneCompl.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ' + Uppercase(QrForneCompl.Value);
  if Trim(QrForneBairro.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneBairro.Value);
  if QrForneCEP.Value > 0 then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrForneCEP.Value);
  if Trim(QrForneCidade.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneCidade.Value);
  if Trim(QrForneNOMEUF.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNOMEUF.Value;
  if Trim(QrFornePais.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + QrFornePais.Value;
end;

procedure TFmStqInnCad.QrPgtBuyCalcFields(DataSet: TDataSet);
begin
  QrPgtBuySEQ.Value := QrPgtBuy.RecNo;
end;

procedure TFmStqInnCad.QrPgtFrtCalcFields(DataSet: TDataSet);
begin
  QrPgtFrtSEQ.Value := QrPgtFrt.RecNo;
end;

procedure TFmStqInnCad.QrStqInnCadAfterClose(DataSet: TDataSet);
begin
  HabilitaBotoes;
end;

procedure TFmStqInnCad.QrStqInnCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  HabilitaBotoes;
end;

procedure TFmStqInnCad.QrStqInnCadAfterScroll(DataSet: TDataSet);
begin
  ReopenStqMovIts(0);
  //
  BtItens.Enabled := QrStqInnCadEncerrou.Value = 0;
  if QrStqInnCadCodCliInt.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrStqInnCadCodCliInt.Value)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenPagtosBuy();
  ReopenPagtosFrt();
  BtPagto.Enabled := QrStqInnCadEncerrou.Value = 0;
end;

procedure TFmStqInnCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdNFe_Id.CharCase   := ecNormal;
  DBEdNFe_Id.CharCase := ecNormal;
end;

procedure TFmStqInnCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqInnCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'StqInnCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqInnCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqInnCad.Fracionamento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqInnPack, FmStqInnPack, afmoNegarComAviso) then
  begin
    FmStqInnPack.ImgTipo.SQLType := stUpd;
    FmStqInnPack.FIDCtrl := QrStqMovItsIDCtrl.Value;
    if (QrStqMovItsPackGGX.Value > 0) or (QrStqMovItsPackQtde.Value > 0) then
    begin
      FmStqInnPack.EdGraGruX.ValueVariant := QrStqMovItsGraGruX.Value;
      FmStqInnPack.EdQtde.ValueVariant := QrStqMovItsQtde.Value;
      FmStqInnPack.EdPackGGX.ValueVariant := QrStqMovItsPackGGX.Value;
      FmStqInnPack.EdPackQtde.ValueVariant := QrStqMovItsPackQtde.Value;
    end else
    begin
      FmStqInnPack.EdGraGruX.ValueVariant := QrStqMovItsGraGruX.Value;;
      FmStqInnPack.EdQtde.ValueVariant := QrStqMovItsQtde.Value;
      FmStqInnPack.EdPackGGX.ValueVariant := QrStqMovItsGraGruX.Value;
      FmStqInnPack.EdPackQtde.ValueVariant := QrStqMovItsQtde.Value;
    end;
    FmStqInnPack.ShowModal;
    FmStqInnPack.Destroy;
    //
    ReopenStqMovIts(0);
  end;

end;

procedure TFmStqInnCad.HabilitaBotoes;
var
  Enab: Boolean;
  Texto: string;
begin
  if (QrStqInnCadStatus.Value > 0) and (QrStqInnCadEncerrou.Value <> 0) and
    (QrStqInnCad.RecordCount > 0) then
  begin
    Enab  := False;
    Texto := 'Desfaz encerramento da entrada';
  end else
  begin
    Enab  := True;
    Texto := 'Encerra entrada';
  end;
  //
  BtItens.Enabled             := Enab;
  BtPagto.Enabled             := Enab;
  Alteraentradaatual1.Enabled := Enab;
  Excluientradaatual1.Enabled := Enab;
  Encerraentrada1.Caption     := Texto;
end;

procedure TFmStqInnCad.ID1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqInnCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmStqInnCad.IncluiBuy1Click(Sender: TObject);
var
  Terceiro, Codigo, Empresa, NF: Integer;
  Valor: Double;
  Descricao: String;
begin
 {$IfNDef NO_FINANCEIRO}
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('SELECT SUM(CustoAll) CustoAll');
  Dmod.QrUpd.SQL.Add('FROM stqmovitsa');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0');
  Dmod.QrUpd.SQL.Add('AND OriCodi=:P1');
  Dmod.QrUpd.Params[0].AsInteger := FThisFatID_Buy;
  Dmod.QrUpd.Params[1].AsInteger := QrStqInnCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(Dmod.QrUpd, Dmod.MyDB);
  if Dmod.QrUpd.FieldByName('CustoAll').Value <> Null then
    Valor := Dmod.QrUpd.FieldByName('CustoAll').Value
  else
    Valor := 0;
  Dmod.QrUpd.Close;
  //
  Codigo        := QrStqInnCadCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Terceiro      := QrStqInnCadFornece.Value;
  Descricao     := 'Compra n� ' + Geral.FF0(QrStqInnCadCodUsu.Value);
  Empresa       := QrStqInnCadEmpresa.Value;
  NF            := QrStqInnCadnfe_nNF.Value;
  //
  UPagtos.Pagto(QrPgtBuy, tpDeb, Codigo, Terceiro, FThisFatID_Buy, 0, 0, stIns,
    'Compra', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0, True, True, 0, 0,
    0, 0, NF, FTabLctA, Descricao);
  //
  ReopenPagtosBuy();
 {$Else}
   Geral.MB_Info('ERP sem pagamento de compra');
 {$EndIf}
end;

procedure TFmStqInnCad.IncluiFrt1Click(Sender: TObject);
var
  Terceiro, Codigo, Empresa, NF: Integer;
  Valor: Double;
  Descricao: String;
begin
 {$IfNDef NO_FINANCEIRO}
  Codigo        := QrStqInnCadCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Terceiro      := QrStqInnCadTransporta.Value;
  Descricao     := 'FC n� ' + Geral.FF0(QrStqInnCadCodUsu.Value);
  Empresa       := QrStqInnCadEmpresa.Value;
  NF            := QrStqInnCadnfe_nNF.Value;
  //
  UPagtos.Pagto(QrPgtFrt, tpDeb, Codigo, Terceiro, FThisFatID_Frt, 0, 0, stIns,
    'Frete', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0, True, True, 0, 0,
    0, 0, NF, FTabLctA, Descricao);
  //
  RateioCustoDoFrete();
  ReopenPagtosBuy();
 {$Else}
   Geral.MB_Info('ERP sem pagamento de compra');
 {$EndIf}
end;

procedure TFmStqInnCad.Incluiitem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqInnIts, FmStqInnIts, afmoNegarComAviso) then
  begin
    FmStqInnIts.ImgTipo.SQLType := stIns;
    FmStqInnIts.ShowModal;
    FmStqInnIts.Destroy;
    //
    ReopenStqMovIts(0);
  end;
end;

procedure TFmStqInnCad.Incluinovaentrada1Click(Sender: TObject);
begin
  if AppPF.ImpedePorMovimentoAberto() then
    Exit;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrStqInnCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'StqInnCad');
  // Deve ser depois do UMyMod.ConfigPanelInsUpd
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqInnCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  //
  EdAbertura.ValueVariant := DModG.ObtemAgora();
  GroupBox2.Enabled       := True;
  EdFilial.ValueVariant   := DModG.QrFiliLogFilial.Value;
  CBFilial.KeyValue       := DModG.QrFiliLogFilial.Value;
end;

procedure TFmStqInnCad.IncluiporXML1Click(Sender: TObject);
const
  Nome   = '';
  Status = 0;
  // StqMovItsA
  OriCnta = 1;
  OriPart = 1;
var
  cab_qVol, IDCtrl, FatID, FatNum, Empresa, CliFor, Transporta: Integer;
  cab_PesoB, cab_PesoL, SumPesoLIts: Double;
  cab_Data, cab_DataE, cab_DataS, cab_Cancelado, cab_refNFe: String;
  //
  DataFiscal: TDateTime;
  CodUsu, Codigo: Integer;
  Abertura: String;
  //
  cab_Serie, cab_nNF: Integer;
begin
  if AppPF.ImpedePorMovimentoAberto() then
    Exit;
{$IfNDef semNFe_v0000}
  FatID      := FThisFatID_Buy;
  Abertura   := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  FStqInnNFe := UCriar.RecriaTempTable('StqInnNFe', DmodG.QrUpdPID1, False);
  FStqInnOpt := UCriar.RecriaTempTable('StqInnOpt', DmodG.QrUpdPID1, False);
  FatNum     := UMyMod.BuscaEmLivreY_Def('StqInnCad', 'Codigo', stIns, 0);
  CodUsu     := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqInnCad', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
  cab_PesoB  := 0;
  cab_PesoL  := 0;
  //
  if UnNFeImporta_0400.ImportaDadosNFeDeXML(tpemiTerceiros, tpnfEntrada,
    MeAvisos, MeAvisos, MeAvisos, '', FmStqInnCad, FatID, FatNum,
    Empresa, IDCtrl, CliFor, Transporta, cab_qVol, cab_PesoB, cab_PesoL,
    SumPesoLIts, DataFiscal, FTabLcta) then
  begin
    cab_Data          := FormatDateTime('yyyy-mm-dd', Date); // Entrada
    //cab_TipoNF        := 1; // Nota Fiscal Eletr�nica
    //
    //cab_Pedido        := 0;
    //cab_Juros         := 0;
    //cab_RICMS         := 0;
    //cab_RICMSF        := 0;
    //cab_Conhecimento  := 0;
    cab_Cancelado     := 'V';
    //
    DmNFe_0000.ReopenQrA(FatID, FatNum, Empresa);
    //
    //cab_NF      := DmNFe_0000.QrAide_nNF.Value;
    cab_dataS   := Geral.FDT(DmNFe_0000.QrAide_dSaiEnt.Value, 1);
    cab_dataE   := Geral.FDT(DmNFe_0000.QrAide_dEmi.Value, 1);
    cab_refNFe  := DmNFe_0000.QrAId.Value;
    cab_Serie   := DmNFe_0000.QrAide_serie.Value;
    cab_nNF     := DmNFe_0000.QrAide_nNF.Value;
    {cab_modNF   := DmNFe_0000.QrAide_mod.Value;
    cab_ICMS    := DmNFe_0000.QrAICMSTot_vICMS.Value;
    cab_ValProd := DmNFe_0000.QrAICMSTot_vProd.Value;
    cab_Frete   := DmNFe_0000.QrAICMSTot_vFrete.Value;
    cab_Seguro  := DmNFe_0000.QrAICMSTot_vSeg.Value;
    cab_Desconto:= DmNFe_0000.QrAICMSTot_vDesc.Value;
    cab_IPI     := DmNFe_0000.QrAICMSTot_vIPI.Value;
    cab_PIS     := DmNFe_0000.QrAICMSTot_vPIS.Value;
    cab_COFINS  := DmNFe_0000.QrAICMSTot_vCOFINS.Value;
    cab_Outros  := DmNFe_0000.QrAICMSTot_vOutro.Value;
    cab_ValorNF := DmNFe_0000.QrAICMSTot_vNF.Value;}
    //

    Codigo := FatNum;
    Abertura := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqinncad', False, [
    'CodUsu', 'Nome', 'Empresa', 'Fornece',
    'nfe_Id', 'nfe_serie', 'nfe_nNF',
    'Abertura', 'Status', 'Transporta'], [
    'Codigo'], [
    CodUsu, Nome, Empresa, CliFor,
    cab_refNFe, cab_Serie, cab_nNF,
    Abertura, Status, Transporta], [
    Codigo], True) then
    begin
      if DBCheck.CriaFm(TFmStqInnNFe, FmStqInnNFe, afmoNegarComAviso) then
      begin
        FmStqInnNFe.FFatID     := FatID;
        FmStqInnNFe.FFatNum    := FatNum;
        FmStqInnNFe.FStqInnNFe := FStqInnNFe;
        FmStqInnNFe.FStqInnOpt := FStqInnOpt;
        FmStqInnNFe.FDataHora  := Abertura;
        FmStqInnNFe.FTipo      := FatID;
        FmStqInnNFe.FOriCodi   := FatNum;
        FmStqInnNFe.FOriCnta   := OriCnta;
        FmStqInnNFe.FEmpresa   := Empresa;
        FmStqInnNFe.FOriPart   := OriPart;
        FmStqInnNFe.FFatorClas := 1;
        //
        FmStqInnNFe.ShowModal;
        FmStqInnNFe.Destroy;
      end;
    end;
  end;
{$EndIf}  
end;

procedure TFmStqInnCad.QrStqInnCadBeforeClose(DataSet: TDataSet);
begin
  QrStqMovIts.Close;
  QrPgtBuy.Close;
  //
  BtItens.Enabled := False;
  BtPagto.Enabled := False;
end;

procedure TFmStqInnCad.QrStqInnCadBeforeOpen(DataSet: TDataSet);
begin
  QrStqInnCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqInnCad.QrStqInnCadCalcFields(DataSet: TDataSet);
begin
  if QrStqInnCadEncerrou.Value = 0 then
    QrStqInnCadENCERROU_TXT.Value := CO_MovimentoAberto
  else
    QrStqInnCadENCERROU_TXT.Value := Geral.FDT(QrStqInnCadEncerrou.Value, 0);
  //
end;

procedure TFmStqInnCad.ReopenPagtosBuy();
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgtBuy, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
  'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, ',
  'la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI,',
  'ca.Tipo CARTEIRATIPO',
  'FROM ' + FTabLctA + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
  'WHERE FatNum=' + Geral.FF0(QrStqInnCadCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Buy),
  'ORDER BY la.FatParcela, la.Vencimento',
  '']);
{$EndIf}
end;

procedure TFmStqInnCad.ReopenPagtosFrt();
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgtFrt, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
  'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, ',
  'la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI,',
  'ca.Tipo CARTEIRATIPO',
  'FROM ' + FTabLctA + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
  'WHERE FatNum=' + Geral.FF0(QrStqInnCadCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  'ORDER BY la.FatParcela, la.Vencimento',
  '']);
{$EndIf}
end;

procedure TFmStqInnCad.ReopenStqMovIts(IDCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqMovIts, Dmod.MyDB, [
    'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
    'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
    'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,  ',
    'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*  ',
    'FROM stqmovitsa smia ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'WHERE Tipo=' + Geral.FF0(FThisFatID_Buy),
    'AND OriCodi=' + Geral.FF0(QrStqInnCadCodigo.Value),
    'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora ',
    '']);
  //
  QrStqMovIts.Locate('IDCtrl', IDCtrl, []);
end;

end.

