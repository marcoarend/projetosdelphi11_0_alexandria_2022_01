unit StqManCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, ComCtrls,
  dmkEditDateTimePicker, frxClass, frxDBSet, Variants, UnDmkProcFunc, dmkImage,
  UnDmkEnums, UnGrl_Geral;

type
  TFmStqManCad = class(TForm)
    DsStqManCad: TDataSource;
    QrStqManCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMMovim: TPopupMenu;
    QrStqManCadCodigo: TIntegerField;
    QrStqManCadCodUsu: TIntegerField;
    QrStqManCadNome: TWideStringField;
    QrStqManCadEmpresa: TIntegerField;
    QrStqManCadPrdGrupTip: TIntegerField;
    QrStqManCadStqCenCad: TIntegerField;
    QrStqManCadCasasProd: TSmallintField;
    QrStqManCadAbertura: TDateTimeField;
    QrStqManCadEncerrou: TDateTimeField;
    DsFiliais: TDataSource;
    QrFiliais: TmySQLQuery;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkValUsu1: TdmkValUsu;
    QrStqManCadNOMEFILIAL: TWideStringField;
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    dmkLabel2: TdmkLabel;
    dmkValUsu2: TdmkValUsu;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrStqManCadNOMEPRDGRUPTIP: TWideStringField;
    QrStqManCadMOMESTQCENCAD: TWideStringField;
    EdStqCenCad: TdmkEditCB;
    dmkLabel3: TdmkLabel;
    CBStqCenCad: TdmkDBLookupComboBox;
    dmkValUsu3: TdmkValUsu;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    RGCasasProd: TdmkRadioGroup;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    QrStqManCadBalQtdItem: TFloatField;
    N1: TMenuItem;
    QrNew: TmySQLQuery;
    QrNewCodigo: TIntegerField;
    QrStqManCadENCERROU_TXT: TWideStringField;
    QrSoma: TmySQLQuery;
    QrSomaQtde: TFloatField;
    Incluinovomovimento1: TMenuItem;
    Alteramovimentoatual1: TMenuItem;
    Encerramovimentoatual1: TMenuItem;
    EXcluimovimentoatual1: TMenuItem;
    EdFisRegCad: TdmkEditCB;
    dmkLabel4: TdmkLabel;
    CBFisRegCad: TdmkDBLookupComboBox;
    QrFisRegCad: TmySQLQuery;
    DsFisRegCad: TDataSource;
    dmkValUsu4: TdmkValUsu;
    QrStqManCadFisRegCad: TIntegerField;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadTipoMov: TSmallintField;
    QrFisRegCadNO_TipoMov: TWideStringField;
    QrFisRegCadTipoCalc: TSmallintField;
    QrFisRegCadNO_TipoCalc: TWideStringField;
    Label13: TLabel;
    EdDBTipoMov: TDBEdit;
    Label14: TLabel;
    EdDBTipoCalc: TDBEdit;
    Label15: TLabel;
    QrStqManCadStatus: TSmallintField;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    QrStqManCadFatSemEstq: TSmallintField;
    QrGruposCU_NIVEL1: TIntegerField;
    QrGruposNO_NIVEL1: TWideStringField;
    QrGruposNivel1: TIntegerField;
    QrGruposGraTamCad: TIntegerField;
    QrStqManCadNO_FisRegCad: TWideStringField;
    QrStqManCadCU_FisRegCad: TIntegerField;
    SpeedButton5: TSpeedButton;
    QrStqManCadTipoMov: TSmallintField;
    QrStqManCadNO_TipoMov: TWideStringField;
    QrStqManCadTipoCalc: TSmallintField;
    QrStqManCadNO_TipoCalc: TWideStringField;
    QrStqManCadEntraSai: TSmallintField;
    QrStqManCadFATOR: TSmallintField;
    QrStqManCadGraCusPrc: TIntegerField;
    EdEntraSai: TdmkEdit;
    QrGruposValor: TFloatField;
    EdCliente: TdmkEditCB;
    LaCliente: TdmkLabel;
    CBCliente: TdmkDBLookupComboBox;
    QrStqManCadCliente: TIntegerField;
    dmkValUsu5: TdmkValUsu;
    QrStqManCadNO_CLIENTE: TWideStringField;
    QrGrupossmiCtrl: TIntegerField;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelDados: TPanel;
    PnCabeca: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    Panel4: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtMovim: TBitBtn;
    BtLeitura: TBitBtn;
    QrStqManCadTabelaPrc: TIntegerField;
    QrStqManCadTabelaPrc_TXT: TWideStringField;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label21: TLabel;
    QrPrdGrupTipFracio: TSmallintField;
    EdTabelaPrc: TdmkEditCB;
    CBTabelaPrc: TdmkDBLookupComboBox;
    dmkLabel5: TdmkLabel;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    QrPrdGrupTipLstPrcFisc: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqManCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqManCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtMovimClick(Sender: TObject);
    procedure PMMovimPopup(Sender: TObject);
    procedure QrStqManCadBeforeClose(DataSet: TDataSet);
    procedure QrStqManCadAfterScroll(DataSet: TDataSet);
    procedure BtLeituraClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrStqManCadCalcFields(DataSet: TDataSet);
    procedure DBEdit6Change(Sender: TObject);
    procedure Incluinovomovimento1Click(Sender: TObject);
    procedure Alteramovimentoatual1Click(Sender: TObject);
    procedure Encerramovimentoatual1Click(Sender: TObject);
    procedure EdFisRegCadChange(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGruposAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrGruposBeforeClose(DataSet: TDataSet);
  private
    FFmtQtde: String;
    //FEmInclusao: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReconfiguraGradeQ();
    procedure ReopenTabePrcCab();
  public
    { Public declarations }
    FTipoMov: Integer;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFisRegCad();
    procedure ReopenGrupos(Nivel1: Integer);
  end;

var
  FmStqManCad: TFmStqManCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MasterSelFilial, StqManLei, ModuleGeral,
{$IfNDef SemPediVda} ModPediVda, {$EndIf}
{$IfNDef NAO_GPED} FatPedImp, {$EndIf}
ModProd, MyVCLSkin, Principal, DmkDAC_PF, UnGrade_Jan, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqManCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqManCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqManCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqManCad.DefParams;
begin
  VAR_GOTOTABELA := 'StqManCad';
  VAR_GOTOMYSQLTABLE := QrStqManCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  //
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('sbc.TabelaPrc, ');
{$IfNDef NAO_GFAT}
  VAR_SQLx.Add('prc.Nome TabelaPrc_TXT,');
{$Else}
  VAR_SQLx.Add('"" TabelaPrc_TXT,');
{$EndIf}
  VAR_SQLx.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('pgt.Nome NOMEPRDGRUPTIP, scc.Nome MOMESTQCENCAD,');
  VAR_SQLx.Add('pem.BalQtdItem, pem.FatSemEstq, frc.Nome NO_FisRegCad,');
  VAR_SQLx.Add('frc.CodUsu CU_FisRegCad, frm.TipoMov,');
  VAR_SQLx.Add('ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov,');
  VAR_SQLx.Add('frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",');
  VAR_SQLx.Add('"Subtrai","?") NO_TipoCalc, sbc.TabelaPrc GraCusPrc, sbc.*');
  VAR_SQLx.Add('FROM stqmancad sbc');
  VAR_SQLx.Add('LEFT JOIN entidades fil  ON fil.Codigo=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=sbc.PrdGrupTip');
  VAR_SQLx.Add('LEFT JOIN stqcencad scc  ON scc.Codigo=sbc.StqCenCad');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem  ON pem.Codigo=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN fisregcad frc ON frc.Codigo=sbc.FisRegCad');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=sbc.Cliente');
{$IfNDef NAO_GFAT}
  VAR_SQLx.Add('LEFT JOIN gracusprc prc ON prc.Codigo=sbc.TabelaPrc');
{$EndIf}
  VAR_SQLx.Add('LEFT JOIN fisregmvt frm ON frm.Codigo=frc.Codigo');
  VAR_SQLx.Add('     AND frm.TipoCalc > 0');
  VAR_SQLx.Add('     AND frm.Empresa=sbc.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE sbc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND sbc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sbc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sbc.Nome Like :P0');
  //
end;

procedure TFmStqManCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqManCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqManCad.EdFilialChange(Sender: TObject);
begin
  ReopenFisRegCad();
end;

procedure TFmStqManCad.EdFisRegCadChange(Sender: TObject);
var
  FisRegCad: Integer;
begin
  FisRegCad := EdFisRegCad.ValueVariant;
  //
  if FisRegCad <> 0 then
  begin
    EdDBTipoMov.DataSource := DsFisRegCad;
    EdDBTipoMov.DataSource := DsFisRegCad;
    (*
    if ImgTipo.SQLType = stIns then
    begin
    *)
    case QrFisRegCadTipoCalc.Value of
      1: EdEntraSai.ValueVariant := 1;
      2: EdEntraSai.ValueVariant := -1;
      else EdEntraSai.ValueVariant := 0;
    end;
    //end;
  end else begin
    EdDBTipoMov.DataSource := nil;
    EdDBTipoMov.DataSource := nil;
    EdEntraSai.ValueVariant := 0;
  end;
end;

procedure TFmStqManCad.EdPrdGrupTipChange(Sender: TObject);
begin
  ReopenFisRegCad();
  //
  RGCasasProd.ItemIndex    := QrPrdGrupTipFracio.Value;
  EdTabelaPrc.ValueVariant := QrPrdGrupTipLstPrcFisc.Value;
  CBTabelaPrc.KeyValue     := QrPrdGrupTipLstPrcFisc.Value;
end;

procedure TFmStqManCad.EdStqCenCadChange(Sender: TObject);
begin
  ReopenFisRegCad();
end;

procedure TFmStqManCad.Encerramovimentoatual1Click(Sender: TObject);
var
  Codigo: Integer;
  Agora: String;
begin
  if Geral.MB_Pergunta('Confirma o encerramento da movimenta��o atual!' +
    sLineBreak + 'ATEN��O: Este procedimento n�o poder� ser desfeito!') <> ID_YES
  then
    Exit;
  //
  Agora := Geral.FDT(DModG.ObtemAgora(), 105);
  Codigo := QrStqManCadCodigo.Value;
  //
  if not DmProd.VerificaEstoqueTodoFaturamento(QrStqManCadFatSemEstq.Value,
    Codigo, 0, 99, QrStqManCadFisRegCad.Value)
  then
    Exit;
  //
  // Ativa itens no movimento
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=99 AND OriCodi=:P0 ');
  Dmod.QrUpd.Params[00].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  // Encerra definivamente movimento manual
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqmancad SET ');
  Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 ');
  Dmod.QrUpd.Params[00].AsString  := Agora;
  Dmod.QrUpd.Params[01].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmStqManCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqManCad.PMMovimPopup(Sender: TObject);
var
  Habil1: Boolean;
begin
  Habil1 := (QrStqManCad.State <> dsInactive) and (QrStqManCad.RecordCount > 0) and (QrStqManCadEncerrou.Value = 0);
  Alteramovimentoatual1.Enabled  := Habil1;
  Encerramovimentoatual1.Enabled := Habil1;
end;

procedure TFmStqManCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqManCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqManCad.DBEdit6Change(Sender: TObject);
begin
  if DBEdit6.Text = CO_MovimentoAberto then
  begin
    DBEdit6.Font.Color := clRed;
    DBEdit6.Font.Style := [];//[fsBold];
  end else begin
    DBEdit6.Font.Color := clWindowText;
    DBEdit6.Font.Style := [];
  end;
end;

procedure TFmStqManCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqManCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqManCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqManCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqManCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqManCad.SpeedButton5Click(Sender: TObject);
var
  FisRegCad: Integer;
begin
  VAR_CADASTRO := 0;
  FisRegCad    := EdFisRegCad.ValueVariant;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
    if QrFisRegCad.Locate('Codigo', QrFisRegCadCodigo.Value, []) then
    begin
      EdFisRegCad.ValueVariant := QrFisRegCadCodUsu.Value;
      CBFisRegCad.KeyValue     := QrFisRegCadCodUsu.Value;
      EdFisRegCad.SetFocus;
    end;
  end;
end;

procedure TFmStqManCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqManCadCodigo.Value;
  Close;
end;

procedure TFmStqManCad.BtMovimClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMovim, BtMovim);
end;

procedure TFmStqManCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, Empresa, PrdGrupTip, FisRegCad: Integer;
  Nome: String;
begin
  if MyObjects.FIC(EdFilial.ValueVariant = 0, EdFilial, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdPrdGrupTip.ValueVariant = 0, EdPrdGrupTip, 'Informe o tipo de grupo de produtos!') then Exit;
  if MyObjects.FIC(EdStqCenCad.ValueVariant = 0, EdStqCenCad, 'Informe o centro de estoque!') then Exit;
  if MyObjects.FIC(EdFisRegCad.ValueVariant = 0, EdFisRegCad, 'Informe a regra fiscal!') then Exit;
  //
  Empresa    := QrFiliaisCodigo.Value;
  PrdGrupTip := QrPrdGrupTipCodigo.Value;
  FisRegCad  := QrFisRegCadCodigo.Value;
  //StqCenCad  := QrStqCenCadCodigo.Value;
  //
  // Verifica se existe balan�o aberto
  if DmProd.ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa, FisRegCad) then
    Exit;
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  {  precisa?
  QrNew.Close;
  QrNew.Params[00].AsInteger := Empresa;
  QrNew.Params[01].AsInteger := PrdGrupTip;
  QrNew.Params[02].AsInteger := StqCenCad;
  UnDmkDAC_PF.AbreQuery(QrNew, Dmod.MyDB);
  if MyObjects.FIC(QrNew.RecordCount > 0, nil,
  'Inclus�o abortada!' + sLineBreak + 'Existe um balan�o aberto para esta ' +
  'configura��o (ID n� ' + IntToStr(QrNewCodigo.Value) + ').') then
  begin
    LocCod(QrStqManCadCodigo.Value, QrNewCodigo.Value);
    Exit;
  end;
  }
  Codigo := UMyMod.BuscaEmLivreY_Def('StqManCad', 'Codigo', ImgTipo.SQLType,
    QrStqManCadCodigo.Value);
  //
  //FEmInclusao := True;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmStqManCad, PainelEdit,
    'StqManCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
  //FEmInclusao := False;
end;

procedure TFmStqManCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'StqManCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqManCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqManCad', 'Codigo');
end;

procedure TFmStqManCad.BtLeituraClick(Sender: TObject);
begin
  if QrStqManCadFATOR.Value = 0 then
  begin
    Geral.MB_Aviso('Fator n�o definido! ' + sLineBreak +
      'O c�lculo deve ser diferente de nulo na regra fiscal!');
    Exit;
  end;
  if DBCheck.CriaFm(TFmStqManLei, FmStqManLei, afmoNegarComAviso) then
  begin
    FmStqManLei.QrLidosQtde.DisplayFormat   := FFmtQtde;
    FmStqManLei.QrTotalQtdLei.DisplayFormat := FFmtQtde;
    FmStqManLei.EdQtdLei.DecimalSize        := QrStqManCadCasasProd.Value;
    FmStqManLei.EdQtdLei.ValueVariant       := QrStqManCadBalQtdItem.Value;
    FmStqManLei.EdStqCenCad.ValueVariant    := QrStqManCadStqCenCad.Value;
    FmStqManLei.CBStqCenCad.KeyValue        := QrStqManCadStqCenCad.Value;
    FmStqManLei.ReopenLidos(0);
    FmStqManLei.ShowModal;
    LocCod(QrStqManCadCodigo.Value, QrStqManCadCodigo.Value);
    FmStqManLei.Destroy;
  end;
end;

procedure TFmStqManCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  FTipoMov := 99;
  PainelEdit.Align             := alClient;
  Panel4.Align                 := alClient;
  UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  {$IfNDef SemPediVda}
  DmPediVda.ReopenClientes();
  DmPediVda.ReopenTabePrcCab(1, FormatDateTime('YYYY-MM-DD', Date));
  LaCliente.Enabled := True;
  EdCliente.Enabled := True;
  CBCliente.Enabled := True;
  //
  CBCliente.ListSource   := DmPediVda.DsClientes;
  {$EndIf}
  CriaOForm;
  EdDBTipoMov.DataSource := nil;
  EdDBTipoMov.DataSource := nil;
  //
  ReopenTabePrcCab;
end;

procedure TFmStqManCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqManCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqManCad.SbImprimeClick(Sender: TObject);
begin
{$IfNDef NAO_GPED}
  //MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
  if DBCheck.CriaFm(TFmFatPedImp, FmFatPedImp, afmoNegarComAviso) then
  begin
    FmFatPedImp.FFatID          := 99;
    FmFatPedImp.FEmpresa        := QrStqManCadEmpresa.Value;
    FmFatPedImp.FOriCodigo      := QrStqManCadCodigo.Value;
    FmFatPedImp.FTituloPedido   := 'Pedido Manual';
    FmFatPedImp.FPedido         := 0;
    FmFatPedImp.FCliente        := QrStqManCadCliente.Value;
    FmFatPedImp.FNomeCondicaoPg := '';
    FmFatPedImp.FTipoMov        := QrStqManCadTipoMov.Value; // entrada ou saida

    FmFatPedImp.ShowModal;
    FmFatPedImp.Destroy;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmStqManCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqManCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqManCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmStqManCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqManCad.QrGruposAfterScroll(DataSet: TDataSet);
begin
  ReconfiguraGradeQ();
end;

procedure TFmStqManCad.QrGruposBeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeQ, GradeC, GradeA, GradeX], 0, 0, True);
end;

procedure TFmStqManCad.QrStqManCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtLeitura.Enabled := (QrStqManCad.RecordCount > 0)
    and (QrStqManCadEncerrou.Value = 0);
end;

procedure TFmStqManCad.QrStqManCadAfterScroll(DataSet: TDataSet);
begin
  FFmtQtde := dmkPF.FormataCasas(QrStqManCadCasasProd.Value);
  ReopenGrupos(0);
end;

procedure TFmStqManCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqManCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqManCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'StqManCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqManCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqManCad.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PainelDados.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PainelDados.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmStqManCad.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PainelDados.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmStqManCad.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
  Texto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PainelDados.Color
  else if GradeA.Cells[ACol, ARow] <> '' then
  begin
    Texto := GradeQ.Cells[ACol, ARow];
    Cor := clWindow;
  end else begin
    if (GradeC.Cells[ACol,ARow] = '')
    and (GradeQ.Cells[ACol,ARow] <> '')
    then  GradeQ.Cells[ACol,ARow] := '';
    Texto := '';
    Cor := clMenu;
  end;
  //


  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor, taLeftJustify,
      GradeQ.Cells[Acol, ARow], 1, 1, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor, taRightJustify,
      (*GradeQ.Cells[AcolARow]*)Texto, 1, 1, False);
end;

procedure TFmStqManCad.Incluinovomovimento1Click(Sender: TObject);
begin
  if AppPF.ImpedePorMovimentoAberto() then
    Exit;
  //FEmInclusao := False;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrStqManCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'StqManCad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqManCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  //
  EdAbertura.ValueVariant  := DModG.ObtemAgora();
  EdFilial.ValueVariant    := DmodG.QrFiliLogFilial.Value;
  CBFilial.KeyValue        := DmodG.QrFiliLogFilial.Value;
  EdFisRegCad.ValueVariant := 0;
  CBFisRegCad.KeyValue     := Null;
  EdFilial.Enabled         := True;
  CBFilial.Enabled         := True;
  EdPrdGrupTip.Enabled     := True;
  CBPrdGrupTip.Enabled     := True;
  EdStqCenCad.Enabled      := True;
  CBStqCenCad.Enabled      := True;
  EdFisRegCad.Enabled      := True;
  CBFisRegCad.Enabled      := True;
end;

procedure TFmStqManCad.QrStqManCadBeforeClose(DataSet: TDataSet);
begin
  BtLeitura.Enabled := False;
  QrGrupos.Close;
end;

procedure TFmStqManCad.QrStqManCadBeforeOpen(DataSet: TDataSet);
begin
  QrStqManCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqManCad.QrStqManCadCalcFields(DataSet: TDataSet);
begin
  if QrStqManCadEncerrou.Value = 0 then
    QrStqManCadENCERROU_TXT.Value := CO_MovimentoAberto
  else
    QrStqManCadENCERROU_TXT.Value := Geral.FDT(QrStqManCadEncerrou.Value, 0);
  //
  case QrStqManCadTipoCalc.Value of
    1: QrStqManCadFATOR.Value := 1;
    2: QrStqManCadFATOR.Value := -1;
    else QrStqManCadFATOR.Value := 0;
  end;
end;

procedure TFmStqManCad.ReconfiguraGradeQ;
begin
  DmProd.ConfigGrades3(QrGruposGraTamCad.Value, QrGruposNivel1.Value,
    GradeA, GradeX, GradeC, GradeQ);
  DmProd.ConfigGrades8(QrGruposGraTamCad.Value, QrGruposNivel1.Value,
    QrStqManCadCodigo.Value, GradeA, GradeX, GradeC, GradeQ);
end;

procedure TFmStqManCad.ReopenFisRegCad;
var
  Empresa, StqCenCad: Integer;
begin
  //if FEmInclusao then Exit;
  Empresa   := Geral.IMV(EdFilial.Text);
  StqCenCad := Geral.IMV(EdStqCenCad.Text);
  if (Empresa <> 0) and (StqCenCad <> 0) then
  begin
    Empresa   := QrFiliaisCodigo.Value;
    StqCenCad := QrStqCenCadCodigo.Value;
    if //(QrFisRegCad.State = dsInactive) or
    (Empresa <> QrFisRegCad.Params[0].AsInteger) or
    (StqCenCad <> QrFisRegCad.Params[1].AsInteger) then
    begin
      QrFisRegCad.Close;
      QrFisRegCad.Params[00].AsInteger := Empresa;
      QrFisRegCad.Params[01].AsInteger := StqCenCad;
      UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
      EdFisRegCad.ValueVariant := 0;
      CBFisRegCad.KeyValue     := Null;
    end;
  end else QrFisRegCad.Close;
end;

procedure TFmStqManCad.ReopenGrupos(Nivel1: Integer);
begin
{
SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,
gg1.Nivel1, gti.Codigo GraTamCad
FROM stqmovitsa smi
LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX
LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1
LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI
WHERE smi.Tipo=99
AND smi.OriCodi=:P0

UNION

SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,
gg1.Nivel1, gti.Codigo GraTamCad
FROM stqmovitsb smi
LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX
LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1
LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI
WHERE smi.Tipo=99
AND smi.OriCodi=:P0
}
  QrGrupos.Close;
  QrGrupos.ParamByName('P0').AsInteger := QrStqManCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
  //
  if Nivel1 <> 0 then
    QrGrupos.Locate('Nivel1', Nivel1, []);
end;

procedure TFmStqManCad.ReopenTabePrcCab;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraCusPrc, Dmod.MyDB, [
    'SELECT Codigo, CodUsu, Nome ',
    'FROM gracusprc ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmStqManCad.Alteramovimentoatual1Click(Sender: TObject);
var
  Habilita: Boolean;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrStqManCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'StqManCad');
  //
  Habilita             := QrGrupos.RecordCount = 0;
  EdFilial.Enabled     := Habilita;
  CBFilial.Enabled     := Habilita;
  EdPrdGrupTip.Enabled := Habilita;
  CBPrdGrupTip.Enabled := Habilita;
  EdStqCenCad.Enabled  := Habilita;
  CBStqCenCad.Enabled  := Habilita;
  EdFisRegCad.Enabled  := Habilita;
  CBFisRegCad.Enabled  := Habilita;
end;

end.

