unit GraTabAppSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables;

type
  TFmGraTabAppSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DsGraTabApp: TDataSource;
    QrGraTabApp: TmySQLQuery;
    QrGraTabAppCodigo: TIntegerField;
    QrGraTabAppNome: TWideStringField;
    QrGraTabAppTabela: TWideStringField;
    Panel5: TPanel;
    Label1: TLabel;
    EdGraTabApp: TdmkEditCB;
    CBGraTabApp: TdmkDBLookupComboBox;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGraTabAppSelSel: Boolean;
    FGraTabAppSelCod: Integer;
    FGraTabAppSelTxt: String;
  end;

  var
  FmGraTabAppSel: TFmGraTabAppSel;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraTabAppSel.BtDesisteClick(Sender: TObject);
begin
  FGraTabAppSelSel := False;
  FGraTabAppSelCod := 0;
  FGraTabAppSelTxt := '';
  Close;
end;

procedure TFmGraTabAppSel.BtOKClick(Sender: TObject);
begin
  FGraTabAppSelSel := True;
  FGraTabAppSelCod := EdGraTabApp.ValueVariant;
  FGraTabAppSelTxt := CBGraTabApp.Text;
  //
  Close;
end;

procedure TFmGraTabAppSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraTabAppSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FGraTabAppSelSel := False;
  FGraTabAppSelCod := 0;
  FGraTabAppSelTxt := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraTabApp, Dmod.MyDB, [
    'SELECT * ',
    'FROM gratabapp ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmGraTabAppSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
