object FmGraSrv1: TFmGraSrv1
  Left = 368
  Top = 194
  Caption = 'PRD-SERVI-001 :: Cadastro de Servi'#231'os'
  ClientHeight = 467
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 371
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 260
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 197
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label3: TLabel
          Left = 64
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label2: TLabel
          Left = 148
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label6: TLabel
          Left = 728
          Top = 4
          Width = 54
          Height = 13
          Caption = 'LC 116/03:'
          FocusControl = DBEdit3
        end
        object Label5: TLabel
          Left = 4
          Top = 116
          Width = 159
          Height = 13
          Caption = 'V - Informa'#231#245'es adicionais (NF-e):'
          FocusControl = DBEdNome
        end
        object Label18: TLabel
          Left = 4
          Top = 156
          Width = 95
          Height = 13
          Caption = 'Unidade de medida:'
        end
        object DBEdCodigo: TDBEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsGraSrv1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit1: TDBEdit
          Left = 64
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsGraSrv1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 148
          Top = 20
          Width = 577
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsGraSrv1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object DBEdit3: TDBEdit
          Left = 728
          Top = 20
          Width = 54
          Height = 21
          DataField = 'cListServ'
          DataSource = DsGraSrv1
          TabOrder = 3
        end
        object DBMemo1: TDBMemo
          Left = 4
          Top = 44
          Width = 777
          Height = 69
          DataField = 'Descricao'
          DataSource = DsGraSrv1
          TabOrder = 4
        end
        object DBEdit2: TDBEdit
          Left = 4
          Top = 132
          Width = 777
          Height = 21
          DataField = 'infAdProd'
          DataSource = DsGraSrv1
          TabOrder = 5
        end
        object DBEdit9: TDBEdit
          Left = 4
          Top = 172
          Width = 56
          Height = 21
          DataField = 'CODUSUUNIDMED'
          DataSource = DsGraSrv1
          TabOrder = 6
        end
        object DBEdit10: TDBEdit
          Left = 60
          Top = 172
          Width = 43
          Height = 21
          DataField = 'SIGLAUNIDMED'
          DataSource = DsGraSrv1
          TabOrder = 7
        end
        object DBEdit7: TDBEdit
          Left = 104
          Top = 172
          Width = 677
          Height = 21
          DataField = 'NOMEUNIDMED'
          DataSource = DsGraSrv1
          TabOrder = 8
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 197
        Width = 792
        Height = 63
        Align = alClient
        TabOrder = 1
        object GroupBox2: TGroupBox
          Left = 1
          Top = 1
          Width = 790
          Height = 62
          Align = alTop
          Caption = ' Pre'#231'os unit'#225'rios por grandeza: '
          TabOrder = 0
          object Label14: TLabel
            Left = 12
            Top = 16
            Width = 37
            Height = 13
            Caption = '$ Pe'#231'a:'
          end
          object Label15: TLabel
            Left = 96
            Top = 16
            Width = 54
            Height = 13
            Caption = '$ '#193'rea (m'#178'):'
          end
          object Label16: TLabel
            Left = 180
            Top = 16
            Width = 57
            Height = 13
            Caption = '$ Peso (kg):'
          end
          object DBEdit4: TDBEdit
            Left = 12
            Top = 32
            Width = 80
            Height = 21
            DataField = 'PrecoPc'
            DataSource = DsGraSrv1
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 96
            Top = 32
            Width = 80
            Height = 21
            DataField = 'PrecoM2'
            DataSource = DsGraSrv1
            TabOrder = 1
          end
          object DBEdit6: TDBEdit
            Left = 180
            Top = 32
            Width = 80
            Height = 21
            DataField = 'Precokg'
            DataSource = DsGraSrv1
            TabOrder = 2
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 307
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 371
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 168
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 4
        Top = 84
        Width = 159
        Height = 13
        Caption = 'V - Informa'#231#245'es adicionais (NF-e):'
      end
      object Label10: TLabel
        Left = 4
        Top = 44
        Width = 224
        Height = 13
        Caption = 'U06 - C'#243'digo da lista de servi'#231'os da LC 116/03'
      end
      object SpeedButton5: TSpeedButton
        Left = 760
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label17: TLabel
        Left = 4
        Top = 124
        Width = 117
        Height = 13
        Caption = 'Unidade de Medida [F3]:'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 633
        Height = 21
        MaxLength = 120
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdInfAdProd: TdmkEdit
        Left = 4
        Top = 100
        Width = 777
        Height = 21
        MaxLength = 255
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'infAdProd'
        UpdCampo = 'infAdProd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdcListServ: TdmkEditCB
        Left = 4
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'cListServ'
        UpdCampo = 'cListServ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBcListServ
        IgnoraDBLookupComboBox = False
      end
      object CBcListServ: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 693
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsListServ
        TabOrder = 4
        dmkEditCB = EdcListServ
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdUnidMed: TdmkEditCB
        Left = 4
        Top = 140
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdUnidMedChange
        OnKeyDown = EdUnidMedKeyDown
        DBLookupComboBox = CBUnidMed
        IgnoraDBLookupComboBox = False
      end
      object EdSigla: TdmkEdit
        Left = 60
        Top = 140
        Width = 40
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdSiglaChange
        OnExit = EdSiglaExit
        OnKeyDown = EdUnidMedKeyDown
      end
      object CBUnidMed: TdmkDBLookupComboBox
        Left = 100
        Top = 140
        Width = 681
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsUnidMed
        TabOrder = 8
        OnKeyDown = EdUnidMedKeyDown
        dmkEditCB = EdUnidMed
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBMemo2: TDBMemo
      Left = 0
      Top = 268
      Width = 792
      Height = 33
      Align = alBottom
      DataField = 'Descricao'
      DataSource = DsListServ
      TabOrder = 1
    end
    object Panel4: TPanel
      Left = 0
      Top = 168
      Width = 792
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 792
        Height = 64
        Align = alClient
        Caption = ' Pre'#231'os unit'#225'rios por grandeza: '
        TabOrder = 0
        object Label11: TLabel
          Left = 12
          Top = 16
          Width = 37
          Height = 13
          Caption = '$ Pe'#231'a:'
        end
        object Label12: TLabel
          Left = 96
          Top = 16
          Width = 54
          Height = 13
          Caption = '$ '#193'rea (m'#178'):'
        end
        object Label13: TLabel
          Left = 180
          Top = 16
          Width = 57
          Height = 13
          Caption = '$ Peso (kg):'
        end
        object EdPrecoPc: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'PrecoPc'
          UpdCampo = 'PrecoPc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPrecoM2: TdmkEdit
          Left = 96
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'PrecoM2'
          UpdCampo = 'PrecoM2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPrecokg: TdmkEdit
          Left = 180
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'Precokg'
          UpdCampo = 'Precokg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 301
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 3
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 260
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 260
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 260
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsGraSrv1: TDataSource
    DataSet = QrGraSrv1
    Left = 40
    Top = 12
  end
  object QrGraSrv1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraSrv1BeforeOpen
    AfterOpen = QrGraSrv1AfterOpen
    SQL.Strings = (
      'SELECT gs1.Codigo, gs1.CodUsu, gs1.Nome, '
      'gs1.infAdProd, gs1.cListServ, lse.Descricao,'
      'gs1.PrecoPc, gs1.PrecoM2, gs1.Precokg,'
      'gs1.UnidMed, med.Sigla SIGLAUNIDMED,'
      'med.CodUsu CODUSUUNIDMED,'
      'med.Nome NOMEUNIDMED'
      'FROM grasrv1 gs1'
      'LEFT JOIN listserv lse ON lse.Codigo=gs1.cListServ'
      'LEFT JOIN unidmed med ON med.Codigo=gs1.UnidMed')
    Left = 12
    Top = 12
    object QrGraSrv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraSrv1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraSrv1Nome: TWideStringField
      DisplayWidth = 120
      FieldName = 'Nome'
      Size = 120
    end
    object QrGraSrv1infAdProd: TWideStringField
      FieldName = 'infAdProd'
      Size = 255
    end
    object QrGraSrv1cListServ: TIntegerField
      FieldName = 'cListServ'
    end
    object QrGraSrv1Descricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGraSrv1PrecoPc: TFloatField
      FieldName = 'PrecoPc'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrGraSrv1PrecoM2: TFloatField
      FieldName = 'PrecoM2'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrGraSrv1Precokg: TFloatField
      FieldName = 'Precokg'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrGraSrv1UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraSrv1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraSrv1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraSrv1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtExclui
    CanDel01 = BtInclui
    Left = 68
    Top = 12
  end
  object QrListServ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodTxt, Codigo, Nome, Descricao'
      'FROM listserv')
    Left = 520
    Top = 8
    object QrListServCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 4
    end
    object QrListServCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListServNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrListServDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsListServ: TDataSource
    DataSet = QrListServ
    Left = 548
    Top = 8
  end
  object QrUnidMed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 576
    Top = 8
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 604
    Top = 8
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdUnidMed
    Panel = PainelEdita
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 96
    Top = 12
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM unidmed'
      'WHERE Sigla = :P0')
    Left = 124
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla'
      'FROM unidmed'
      'WHERE CodUsu = :P0')
    Left = 152
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
end
