unit Embalagens;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmEmbalagens = class(TForm)
    PainelDados: TPanel;
    DsEmbalagens: TDataSource;
    QrEmbalagens: TmySQLQuery;
    QrEmbalagensLk: TIntegerField;
    QrEmbalagensDataCad: TDateField;
    QrEmbalagensDataAlt: TDateField;
    QrEmbalagensUserCad: TIntegerField;
    QrEmbalagensUserAlt: TIntegerField;
    QrEmbalagensCodigo: TSmallintField;
    QrEmbalagensNome: TWideStringField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Label27: TLabel;
    EdBruto: TdmkEdit;
    Label3: TLabel;
    EdLiqui: TdmkEdit;
    QrEmbalagensBruto: TFloatField;
    QrEmbalagensLiqui: TFloatField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    LaRegistro: TStaticText;
    Panel2: TPanel;
    Panel6: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEmbalagensAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrEmbalagensAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEmbalagensBeforeOpen(DataSet: TDataSet);
    procedure EdBrutoExit(Sender: TObject);
    procedure EdLiquiExit(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEmbalagens: TFmEmbalagens;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEmbalagens.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEmbalagens.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEmbalagensCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEmbalagens.DefParams;
begin
  VAR_GOTOTABELA := 'Embalagens';
  VAR_GOTOMYSQLTABLE := QrEmbalagens;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM embalagens');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEmbalagens.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      if SQLType = stIns then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
        EdBruto.Text  := '';
        EdLiqui.Text  := '';
      end else begin
        EdCodigo.Text := IntToStr(QrEmbalagensCodigo.Value);
        EdNome.Text   := QrEmbalagensNome.Value;
        EdBruto.Text  := Geral.FFT(QrEmbalagensBruto.Value, 3, siPositivo);
        EdLiqui.Text  := Geral.FFT(QrEmbalagensLiqui.Value, 3, siPositivo);
      end;
      EdNome.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmEmbalagens.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEmbalagens.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEmbalagens.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEmbalagens.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEmbalagens.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEmbalagens.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEmbalagens.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEmbalagens.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmEmbalagens.BtAlteraClick(Sender: TObject);
var
  Embalagens : Integer;
begin
  Embalagens := QrEmbalagensCodigo.Value;
  if not UMyMod.SelLockY(Embalagens, Dmod.MyDB, 'Embalagens', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Embalagens, Dmod.MyDB, 'Embalagens', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEmbalagens.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEmbalagensCodigo.Value;
  Close;
end;

procedure TFmEmbalagens.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaEmLivreY_Def('embalagens', 'Codigo', stIns, Codigo, nil)
  else
    Codigo := QrEmbalagensCodigo.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'embalagens', False, [
    'Nome', 'Bruto', 'Liqui'], ['Codigo'],
    [Nome, EdBruto.ValueVariant, EdLiqui.ValueVariant], [Codigo], True) then
  begin
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmEmbalagens.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Embalagens', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Embalagens', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Embalagens', 'Codigo');
end;

procedure TFmEmbalagens.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmEmbalagens.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEmbalagensCodigo.Value,LaRegistro.Caption);
end;

procedure TFmEmbalagens.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmEmbalagens.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEmbalagens.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmEmbalagens.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEmbalagens.QrEmbalagensAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEmbalagens.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Embalagens', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmEmbalagens.QrEmbalagensAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrEmbalagensCodigo.Value, False);
  BtExclui.Enabled := GOTOy.BtEnabled(QrEmbalagensCodigo.Value, False);
end;

procedure TFmEmbalagens.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEmbalagensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Embalagens', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEmbalagens.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmbalagens.QrEmbalagensBeforeOpen(DataSet: TDataSet);
begin
  QrEmbalagensCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEmbalagens.EdBrutoExit(Sender: TObject);
begin
  EdBruto.Text := Geral.TFT(EdBruto.Text, 3, siPositivo);
end;

procedure TFmEmbalagens.EdLiquiExit(Sender: TObject);
begin
  EdLiqui.Text := Geral.TFT(EdLiqui.Text, 3, siPositivo);
end;

end.

