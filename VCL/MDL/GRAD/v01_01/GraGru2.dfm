object FmGraGru2: TFmGraGru2
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-040 :: Cadastro de N'#237'vel 2'
  ClientHeight = 377
  ClientWidth = 621
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 621
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 573
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 525
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 239
        Height = 32
        Caption = 'Cadastro de N'#237'vel 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 239
        Height = 32
        Caption = 'Cadastro de N'#237'vel 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 239
        Height = 32
        Caption = 'Cadastro de N'#237'vel 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 621
    Height = 215
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 621
      Height = 215
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 621
        Height = 215
        Align = alClient
        TabOrder = 0
        object Label28: TLabel
          Left = 16
          Top = 80
          Width = 300
          Height = 13
          Caption = 
            'Tipo de item - Atividades Industriais, Comerciais e Servi'#231'os [F3' +
            ']:'
        end
        object Label1: TLabel
          Left = 16
          Top = 120
          Width = 72
          Height = 13
          Caption = 'Grupo cont'#225'bil:'
        end
        object Label2: TLabel
          Left = 16
          Top = 160
          Width = 125
          Height = 13
          Caption = 'Conta do plano de contas:'
        end
        object SbCtbCadGru: TSpeedButton
          Left = 588
          Top = 136
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbCtbCadGruClick
        end
        object SbCtbPlaCta: TSpeedButton
          Left = 588
          Top = 176
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbCtbPlaCtaClick
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 617
          Height = 62
          Align = alTop
          Caption = ' Dados do item: '
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 16
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label7: TLabel
            Left = 96
            Top = 16
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object EdNivel2: TdmkEdit
            Left = 12
            Top = 32
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNivel2Change
          end
          object EdNome: TdmkEdit
            Left = 96
            Top = 32
            Width = 513
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object EdTipo_Item: TdmkEdit
          Left = 16
          Top = 97
          Width = 21
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Tipo_Item'
          UpdCampo = 'Tipo_Item'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdTipo_ItemChange
          OnKeyDown = EdTipo_ItemKeyDown
        end
        object EdTipo_Item_TXT: TdmkEdit
          Left = 40
          Top = 97
          Width = 569
          Height = 21
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'Mercadoria para Revenda'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'Mercadoria para Revenda'
          ValWarn = False
        end
        object EdCtbCadGru: TdmkEditCB
          Left = 16
          Top = 136
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCtbCadGru
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCtbCadGru: TdmkDBLookupComboBox
          Left = 72
          Top = 136
          Width = 513
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCtbCadGru
          TabOrder = 4
          dmkEditCB = EdCtbCadGru
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCtbPlaCta: TdmkEditCB
          Left = 16
          Top = 176
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCtbPlaCta
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCtbPlaCta: TdmkDBLookupComboBox
          Left = 72
          Top = 176
          Width = 513
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 6
          dmkEditCB = EdCtbPlaCta
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 263
    Width = 621
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 617
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 307
    Width = 621
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 475
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 473
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCtbCadGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ctbcadgru'
      'ORDER BY Nome')
    Left = 364
    Top = 66
    object QrCtbCadGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCtbCadGru: TDataSource
    DataSet = QrCtbCadGru
    Left = 364
    Top = 118
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 476
    Top = 72
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 476
    Top = 120
  end
end
