unit StqCenLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask,
  dmkDBEdit, dmkGeral, Variants, dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmStqCenLoc = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    CkContinuar: TCheckBox;
    Label2: TLabel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    EdCodUsu: TdmkEdit;
    EdNome: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrCarteiras: TMySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasTipo: TIntegerField;
    DsCarteiras: TDataSource;
    EdCarteira: TdmkEditCB;
    Label13: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    SbCarteira: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SbCarteiraClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmStqCenLoc: TFmStqCenLoc;

implementation

uses UnMyObjects, Module, StqCenCad, UMySQLModule, DmkDAC_PF, UnFinanceiroJan;

{$R *.DFM}

procedure TFmStqCenLoc.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, CodUsu, Carteira: Integer;
begin
  Codigo         := FmStqCenCad.QrStqCenCadCodigo.Value;
  Controle       := EdControle.ValueVariant;
  CodUsu         := EdCodUsu.ValueVariant;
  Nome           := EdNome.Text;
  Carteira       := EdCarteira.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe a descri��o!') then
    Exit;
  Controle := UMyMod.BuscaEmLivreY_Def('stqcenloc', 'Controle', ImgTipo.SQLType,
    Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqcenloc', False, [
  'Codigo', 'CodUsu', 'Nome',
  'Carteira'], [
  'Controle'], [
  Codigo, CodUsu, Nome,
  Carteira], [
  Controle], True) then
  begin
    FmStqCenCad.ReopenStqCenLoc(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType         := stIns;
      EdControle.ValueVariant := 0;
      EdCodUsu.ValueVariant   := 0;
      EdNome.Text             := '';
      //
      EdCodUsu.SetFocus;
    end else Close;
  end;
end;

procedure TFmStqCenLoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqCenLoc.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCenLoc', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqCenLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqCenLoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
{$EndIf}
end;

procedure TFmStqCenLoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCenLoc.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCenLoc', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
    CkContinuar.Visible := True;
    CkContinuar.Checked := False;
    EdNome.SetFocus;
  end else
  begin
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
  end;
end;

procedure TFmStqCenLoc.SbCarteiraClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  FinanceiroJan.CadastroESelecaoDeCarteira(QrCarteiras, EdCarteira, CBCarteira);
{$EndIf}
end;

end.
