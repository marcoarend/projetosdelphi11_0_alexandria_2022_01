unit GraGruECad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, Variants, dmkRadioGroup, dmkEditCB, dmkDBLookupComboBox,
  dmkGeral, Menus, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmGraGruECad = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Label5: TLabel;
    EdxProd: TdmkEdit;
    EdcProd: TdmkEdit;
    Label6: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    SbFornece: TSpeedButton;
    QrGraGruECad: TmySQLQuery;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CadastroComGrade1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrCad: TmySQLQuery;
    FxProd, FcProd: String;
    FFornece: Integer;
  end;

  var
  FmGraGruECad: TFmGraGruECad;

implementation

uses UnMyObjects, MyDBCheck, UMySQLModule, Module, UnInternalConsts,
  GraGruN, ModProd;

{$R *.DFM}

procedure TFmGraGruECad.BtOKClick(Sender: TObject);
var
  cProd, xProd: String;
  Fornece: Integer;
  SQLType: TSQLType;
  Continua: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  cProd          := EdcProd.Text;
  Fornece        := EdFornece.ValueVariant;
  xProd          := EdxProd.Text;
  //
  if MyObjects.FIC(Fornece = 0, nil, 'Fornecedor n�o informado!') then Exit;
  //
  if SQLType = stIns then
  begin
    Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruecad', False, [
    ], ['cProd', 'Fornece', 'xProd'], [
    ], [cProd, Fornece, xProd], True);
  end else
  begin
    Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruecad', False, [
    'cProd', 'Fornece', 'xProd'], [
    'cProd', 'Fornece', 'xProd'], [
    cProd, Fornece, xProd], [
    FcProd, FFornece, FxProd], True);
  end;
  //
  if Continua then
  begin
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
    end else
    begin
      if FQrCad <> nil then
      begin
        UnDMkDAC_PF.AbreQuery(FQrCad, Dmod.MyDB);
        FQrCad.Locate(
          'Fornece;cProd;xProd', VarArrayOf([Fornece, cProd, xProd]), []);
      end;
      Close;
    end;
  end;
end;

procedure TFmGraGruECad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruECad.CadastroComGrade1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdxProd.Text;
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
end;

procedure TFmGraGruECad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruECad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmGraGruECad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
