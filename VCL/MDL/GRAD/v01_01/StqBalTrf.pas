unit StqBalTrf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, DBGrids,
  dmkDBGrid, dmkGeral, ComCtrls, dmkEdit, UnInternalConsts, dmkImage, UnDmkEnums;

type
  TFmStqBalTrf = class(TForm)
    QrGG1: TmySQLQuery;
    DsGG1: TDataSource;
    QrGG1GraGru1: TIntegerField;
    QrGG1CodUsu: TIntegerField;
    QrGG1Nome: TWideStringField;
    QrBalIts: TmySQLQuery;
    QrBalItsCodigo: TIntegerField;
    QrBalItsControle: TIntegerField;
    QrBalItsTipo: TSmallintField;
    QrBalItsDataHora: TDateTimeField;
    QrBalItsStqCenLoc: TIntegerField;
    QrBalItsGraGruX: TIntegerField;
    QrBalItsQtdLei: TFloatField;
    QrBalItsQtdAnt: TFloatField;
    QrBalItsAlterWeb: TSmallintField;
    QrBalItsAtivo: TSmallintField;
    QrBalItsStqCenCad: TIntegerField;
    QrGGX: TmySQLQuery;
    DsGGX: TDataSource;
    QrAll: TmySQLQuery;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Panel4: TPanel;
    StaticText1: TStaticText;
    dmkDBGrid1: TdmkDBGrid;
    Panel5: TPanel;
    StaticText2: TStaticText;
    dmkDBGrid2: TdmkDBGrid;
    StaticText3: TStaticText;
    DsAll: TDataSource;
    dmkDBGrid3: TdmkDBGrid;
    QrAllGraGruX: TIntegerField;
    QrAllQtde: TFloatField;
    QrAllCU_COR: TIntegerField;
    QrAllNO_COR: TWideStringField;
    QrAllNO_TAM: TWideStringField;
    QrAllCU_GG1: TIntegerField;
    QrAllNO_GG1: TWideStringField;
    QrA: TmySQLQuery;
    QrDescribeA: TmySQLQuery;
    Panel6: TPanel;
    EdEncerrou: TdmkEdit;
    Label10: TLabel;
    QrBalItsPecasLei: TFloatField;
    QrBalItsPecasAnt: TFloatField;
    QrBalItsPesoLei: TFloatField;
    QrBalItsPesoAnt: TFloatField;
    QrBalItsAreaM2Lei: TFloatField;
    QrBalItsAreaM2Ant: TFloatField;
    QrBalItsAreaP2Lei: TFloatField;
    QrBalItsAreaP2Ant: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    QrItens: TMySQLQuery;
    QrItensGraGru1: TIntegerField;
    QrBalItsCustoAll: TFloatField;
    QrSoma: TMySQLQuery;
    QrSomaQtde: TFloatField;
    QrSomaPecas: TFloatField;
    QrSomaPeso: TFloatField;
    QrSomaAreaM2: TFloatField;
    QrSomaAreaP2: TFloatField;
    QrSomaStqCenLoc: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrGG1AfterScroll(DataSet: TDataSet);
    procedure EdEncerrouKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmStqBalTrf: TFmStqBalTrf;

implementation

uses UnMyObjects, Module, StqBalCad, UMySQLModule, ModuleGeral, UnGrade_Tabs,
  MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmStqBalTrf.BtOKClick(Sender: TObject);
{
const
  FTipoStqMovIts = dmkteBalanco; // VAR_FATID_0000
}
var
  OriCodi, OriCtrl, StqCenCad, StqCenLoc, Empresa, GraGruX, Codigo, Controle,
  TipoPrd, IDCtrl: Integer;
  DataHora: TDateTime;
  Qtde, Pecas, Peso, AreaM2, AreaP2, QtdLei, QtdAnt, PecasLei, PecasAnt,
  PesoLei, PesoAnt, AreaM2Lei, AreaM2Ant, AreaP2Lei, AreaP2Ant, CustoAll: Double;
  Agora, Abertura: String;
  //
  Corda1: String;
begin
  Screen.Cursor := crHourGlass;
  StqCenLoc := 0;
  StqCenCad := FmStqBalCad.QrStqBalCadStqCenCad.Value;
  TipoPrd   := FmStqBalCad.QrStqBalCadPrdGrupTip.Value;
  Agora     := FormatDateTime('yyyy-mm-dd hh:nn:ss', EdEncerrou.ValueVariant);
  Abertura  := FormatDateTime('yyyy-mm-dd hh:nn:ss', FmStqBalCad.QrStqBalCadAbertura.Value);
  //
  QrAll.First;
  if QrAll.RecordCount > 0 then
  begin
    // Zerar reduzidos com estoque n�o lidos dos produtos com reduzidos lidos!
    QtdLei    := 0;
    PecasLei  := 0;
    PesoLei   := 0;
    AreaM2Lei := 0;
    AreaP2Lei := 0;
    Codigo    := FmStqBalCad.QrStqBalCadCodigo.Value;
    QrAll.First;
    //
    PB1.Position := 0;
    PB1.Max      := QrAll.RecordCount;
    //
    while not QrAll.Eof do
    begin
      GraGruX := QrAllGraGruX.Value;
      // Saldo Anterior
      // ini 2023-08-12
(*
      FmStqBalCad.QrSoma.Close;
      FmStqBalCad.QrSoma.Params[00].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
      FmStqBalCad.QrSoma.Params[01].AsInteger := StqCenCad;
      FmStqBalCad.QrSoma.Params[02].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(FmStqBalCad.QrSoma, Dmod.MyDB);
*)
      UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
      'SELECT StqCenLoc, SUM(Qtde) Qtde, SUM(Pecas) Pecas,  SUM(Peso) Peso,',
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2',
      'FROM stqmovitsa',
      'WHERE Empresa=' + Geral.FF0(FmStqBalCad.QrStqBalCadEmpresa.Value),
      'AND StqCenCad=' + Geral.FF0(StqCenCad),
      'AND GraGruX=' + Geral.FF0(GraGruX),
      'GROUP BY StqCenLoc',
      'ORDER BY StqCenLoc',
      '']);
      QrSoma.First;
      while not QrSoma.Eof do
      begin
      // fim 2023-08-12
        QtdAnt := QrSomaQtde.Value;
        PecasAnt := QrSomaPecas.Value;
        PesoAnt := QrSomaPeso.Value;
        AreaM2Ant := QrSomaAreaM2.Value;
        AreaP2Ant := QrSomaAreaP2.Value;
        StqCenLoc := QrSomaStqCenLoc.Value;
        //
        Controle :=
          DModG.BuscaProximoInteiro('stqbalits', 'Controle', 'Tipo', TipoPrd);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalits', False, [
        'Tipo', 'DataHora', 'StqCenLoc', 'GraGruX', 'QtdLei', 'QtdAnt',
        'PecasLei', 'PecasAnt', 'PesoLei', 'PesoAnt', 'AreaM2Lei', 'AreaM2Ant',
        'AreaP2Lei', 'AreaP2Ant'], ['Codigo', 'Controle'], [
        TipoPrd, Agora, StqCenLoc, GraGruX, QtdLei, QtdAnt,
        PecasLei, PecasAnt, PesoLei, PesoAnt, AreaM2Lei, AreaM2Ant,
        AreaP2Lei, AreaP2Ant], [Codigo, Controle], False) then
        //
        // ini 2023-08-12
        QrSoma.Next;
      end;
      // ini 2023-08-12
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrAll.Next;
    end;
  end;
  //
  // Verifica de novo.
  QrAll.Close;
  UnDmkDAC_PF.AbreQuery(QrAll, DmodG.MyPID_DB);
  if QrAll.RecordCount > 0 then
  begin
    Geral.MB_Erro('N�o foi poss�vel encerrar o balan�o!' + sLineBreak +
    'Ainda existem itens com estoque n�o zerados!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Transferir da tabela stqmovitsa para a tabela stqmovitsb
{  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('REPLACE INTO stqmovitsb');
  Dmod.QrUpd.SQL.Add('SELECT smia.*');
  Dmod.QrUpd.SQL.Add('FROM stqmovitsa smia');
  Dmod.QrUpd.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX');
  Dmod.QrUpd.SQL.Add('WHERE smia.Empresa=:P0');
  Dmod.QrUpd.SQL.Add('AND smia.StqCenCad=:P1');
  Dmod.QrUpd.SQL.Add('AND smia.DataHora<=:P2');
  Dmod.QrUpd.SQL.Add('AND ggx.GraGru1 in (');
  Dmod.QrUpd.SQL.Add('SELECT DISTINCT ggx.GraGru1');
  Dmod.QrUpd.SQL.Add('  FROM stqbalits sbi');
  Dmod.QrUpd.SQL.Add('  LEFT JOIN gragrux ggx ON ggx.Controle=sbi.Gragrux');
  Dmod.QrUpd.SQL.Add('  WHERE sbi.Codigo=:P3');
  Dmod.QrUpd.SQL.Add(')');
  Dmod.QrUpd.Params[00].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
  Dmod.QrUpd.Params[01].AsInteger := FmStqBalCad.QrStqBalCadStqCenCad.Value;
  Dmod.QrUpd.Params[02].AsString  := Abertura;
  Dmod.QrUpd.Params[03].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
  Dmod.QrUpd.ExecSQL;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrItens, DMod.MyDB, [
  '  SELECT DISTINCT ggx.GraGru1 ',
  '  FROM stqbalits sbi ',
  '  LEFT JOIN gragrux ggx ON ggx.Controle=sbi.Gragrux ',
  '  WHERE sbi.Codigo=' + Geral.FF0(FmStqBalCad.QrStqBalCadCodigo.Value),
  ' ']);
  Corda1 := MyObjects.CordaDeQuery(QrItens, 'GraGru1', EmptyStr);
  if Corda1 <> EmptyStr then
  begin
    DMod.MyDB.Execute(Geral.ATS([
    'REPLACE INTO stqmovitsb ',
    'SELECT smia.* ',
    'FROM stqmovitsa smia ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX ',
    'WHERE smia.Empresa=' + Geral.FF0(FmStqBalCad.QrStqBalCadEmpresa.Value),
    'AND smia.StqCenCad=' + Geral.FF0(FmStqBalCad.QrStqBalCadStqCenCad.Value),
    'AND smia.DataHora<="' + Abertura + '"',
    'AND ggx.GraGru1 in ( ' + Corda1 + ') ',
    '']));
    //
    // Excluir dados transferidos
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE smia FROM stqmovitsa smia, gragrux ggx');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smia.GraGruX');
    Dmod.QrUpd.SQL.Add('AND smia.Empresa=:P0');
    Dmod.QrUpd.SQL.Add('AND smia.StqCenCad=:P1');
    Dmod.QrUpd.SQL.Add('AND smia.DataHora<=:P2');
    Dmod.QrUpd.SQL.Add('AND ggx.GraGru1 in (');
    Dmod.QrUpd.SQL.Add('SELECT DISTINCT ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('  FROM stqbalits sbi');
    Dmod.QrUpd.SQL.Add('  LEFT JOIN gragrux ggx ON ggx.Controle=sbi.Gragrux');
    Dmod.QrUpd.SQL.Add('  WHERE sbi.Codigo=:P3');
    Dmod.QrUpd.SQL.Add(')');
    Dmod.QrUpd.Params[00].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmStqBalCad.QrStqBalCadStqCenCad.Value;
    Dmod.QrUpd.Params[02].AsString  := Abertura;
    Dmod.QrUpd.Params[03].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
    Dmod.QrUpd.ExecSQL;
*)
    DMod.MyDB.Execute(Geral.ATS([
    'DELETE smia FROM stqmovitsa smia, gragrux ggx ',
    'WHERE ggx.Controle=smia.GraGruX ',
    'AND smia.Empresa=' + Geral.FF0(FmStqBalCad.QrStqBalCadEmpresa.Value),
    'AND smia.StqCenCad=' + Geral.FF0(FmStqBalCad.QrStqBalCadStqCenCad.Value),
    'AND smia.DataHora<="' + Abertura + '"',
    'AND ggx.GraGru1 in ( ' + Corda1 + ') ',
    '']));

      // Setar c�digo do GrupoBal no stqmovitsb
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE stqmovitsb smib, gragrux ggx');
    Dmod.QrUpd.SQL.Add('SET smib.GrupoBal=:P0');
    Dmod.QrUpd.SQL.Add('WHERE ggx.Controle=smib.GraGruX');
    Dmod.QrUpd.SQL.Add('AND smib.Empresa=:P1');
    Dmod.QrUpd.SQL.Add('AND smib.StqCenCad=:P2');
    Dmod.QrUpd.SQL.Add('AND smib.DataHora<=:P3');
    Dmod.QrUpd.SQL.Add('AND ggx.GraGru1 in (');
    Dmod.QrUpd.SQL.Add('SELECT DISTINCT ggx.GraGru1');
    Dmod.QrUpd.SQL.Add('  FROM stqbalits sbi');
    Dmod.QrUpd.SQL.Add('  LEFT JOIN gragrux ggx ON ggx.Controle=sbi.Gragrux');
    Dmod.QrUpd.SQL.Add('  WHERE sbi.Codigo=:P4');
    Dmod.QrUpd.SQL.Add(')');
    Dmod.QrUpd.Params[00].AsInteger := FmStqBalCad.QrStqBalCadGrupoBal.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
    Dmod.QrUpd.Params[02].AsInteger := FmStqBalCad.QrStqBalCadStqCenCad.Value;
    Dmod.QrUpd.Params[03].AsString  := Abertura;
    Dmod.QrUpd.Params[04].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
    Dmod.QrUpd.ExecSQL;
*)
    DMod.MyDB.Execute(Geral.ATS([
    'UPDATE stqmovitsb smib, gragrux ggx ',
    'SET smib.GrupoBal=' + Geral.FF0(FmStqBalCad.QrStqBalCadGrupoBal.Value),
    'WHERE ggx.Controle=smib.GraGruX ',
    'AND smib.Empresa=' + Geral.FF0(FmStqBalCad.QrStqBalCadEmpresa.Value),
    'AND smib.StqCenCad=' + Geral.FF0(FmStqBalCad.QrStqBalCadStqCenCad.Value),
    'AND smib.DataHora<="' + Abertura + '"',
    'AND ggx.GraGru1 in ( ' + Corda1 + ') ',
    '']));
  end;
  // Replicar dados do balan�o na tabela de estoque ativa
  Empresa   := FmStqBalCad.QrStqBalCadEmpresa.Value;
  OriCodi   := FmStqBalCad.QrStqBalCadCodigo.Value;
  QrBalIts.Close;
  QrBalIts.Params[0].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrBalIts, Dmod.MyDB);
  QrBalIts.First;
  //
  PB1.Position := 0;
  PB1.Max      := QrBalIts.RecordCount;
  //
  while not QrBalIts.Eof do
  begin
    DataHora  := QrBalItsDataHora.Value;
    GraGruX   := QrBalItsGraGruX.Value;
    Qtde      := QrBalItsQtdLei.Value;
    Pecas     := QrBalItsPecasLei.Value;
    Peso      := QrBalItsPesoLei.Value;
    AreaM2    := QrBalItsAreaM2Lei.Value;
    AreaP2    := QrBalItsAreaP2Lei.Value;
    OriCtrl   := QrBalItsControle.Value;
    CustoAll  := QrBalItsCustoAll.Value;
    StqCenLoc := QrBalItsStqCenLoc.Value;
    //
    IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
    UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'stqmovitsa', False, [
    'Tipo', 'DataHora', 'Empresa',
    'StqCenCad', 'GraGruX', 'Qtde',
    'Pecas', 'Peso', 'AreaM2',
    'AreaP2', 'IDCtrl', 'CustoAll',
    'StqCenLoc'], [
    'OriCodi', 'OriCtrl'], [
    'OriCodi', 'OriCtrl'], [
    VAR_FATID_0000, DataHora, Empresa,
    StqCenCad, GraGruX, Qtde,
    Pecas, Peso, AreaM2,
    AreaP2, IDCtrl, CustoAll,
    StqCenLoc], [
    OriCodi, OriCtrl], [
    OriCodi, OriCtrl], False);
    //
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    QrBalIts.Next;
  end;
  // Encerra Balan�o
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE stqbalcad SET ');
  Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 ');
  Dmod.QrUpd.Params[00].AsString  := Agora;
  Dmod.QrUpd.Params[01].AsInteger := OriCodi;
  Dmod.QrUpd.ExecSQL;
  //
  FmStqBalCad.LocCod(OriCodi, OriCodi);
  //
  PB1.Position  := 0;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmStqBalTrf.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqBalTrf.EdEncerrouKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Data: TDateTime;
begin
  if Key = VK_F4 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    if not DBCheck.ObtemData(Date, Data, 0, 84599.1/84600, True) then Exit;
    //RGForcada.ItemIndex := 1;
    EdEncerrou.ValueVariant := Data;
  end;
end;

procedure TFmStqBalTrf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqBalTrf.FormCreate(Sender: TObject);
var
  sCodigo: String;
begin
  ImgTipo.SQLType := stLok;
  //
  if FmStqBalCad.QrStqBalCadForcada.Value = 1 then
    EdEncerrou.ValueVariant :=  FmStqBalCad.QrStqBalCadAbertura.Value
  else
    EdEncerrou.ValueVariant := DModG.ObtemAgora();
  //  
  QrGG1.Close;
  QrGG1.Params[0].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGG1, Dmod.MyDB);
  //
{
  QrAll.Close;
  QrAll.Params[00].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
  QrAll.Params[01].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
  QrAll.Params[02].AsInteger := FmStqBalCad.QrStqBalCadStqCenCad.Value;
  QrAll.Params[03].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrAll, Dmod.MyDB);
}
  sCodigo := Geral.FF0(FmStqBalCad.QrStqBalCadCodigo.Value);
  DModG.MyPID_DB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _stq_bal_its_; ',
  'CREATE TABLE _stq_bal_its_ ',
  'SELECT DISTINCT ggx.GraGru1 ',
  '  FROM ' + TMeuDB + '.stqbalits sbi ',
  '  LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=sbi.Gragrux ',
  '  WHERE sbi.Codigo=' + sCodigo + '; ',
  ' ']));
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, DModG.MyPID_DB, [
  'SELECT smia.GraGruX, SUM(smia.Qtde) Qtde, ',
  'gcc.CodUsu CU_COR, gcc.Nome NO_COR, ',
  'gti.Nome NO_TAM, ',
  'gg1.CodUsu CU_GG1, gg1.Nome NO_GG1 ',
  'FROM ' + TMeuDB + '.stqmovitsa smia ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=smia.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.stqbalits sbi ON sbi.GraGruX=smia.GraGruX AND sbi.Codigo=' + sCodigo,
  'LEFT JOIN ' + TMeuDB + '.gragruc ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE sbi.GraGruX IS NULL ',
  'AND smia.Empresa=' + Geral.FF0(FmStqBalCad.QrStqBalCadEmpresa.Value),
  'AND smia.StqCenCad=' + Geral.FF0(FmStqBalCad.QrStqBalCadStqCenCad.Value),
  'AND ggx.GraGru1 in ( ',
  '  SELECT GraGru1 FROM _stq_bal_its_ ',
  ') ',
  'GROUP BY smia.GraGruX; ',
  '']);
  //
  if QrGG1.RecordCount > 0 then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
end;

procedure TFmStqBalTrf.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqBalTrf.QrGG1AfterScroll(DataSet: TDataSet);
begin
  QrGGX.Close;
  QrGGX.Params[00].AsInteger := FmStqBalCad.QrStqBalCadCodigo.Value;
  QrGGX.Params[01].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
  QrGGX.Params[02].AsInteger := FmStqBalCad.QrStqBalCadStqCenCad.Value;
  QrGGX.Params[03].AsInteger := QrGG1GraGru1.Value;
  UnDmkDAC_PF.AbreQuery(QrGGX, Dmod.MyDB);
end;

end.

