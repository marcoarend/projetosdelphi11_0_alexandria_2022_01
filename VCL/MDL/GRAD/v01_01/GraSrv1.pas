unit GraSrv1;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmGraSrv1 = class(TForm)
    PainelDados: TPanel;
    DsGraSrv1: TDataSource;
    QrGraSrv1: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrGraSrv1Codigo: TIntegerField;
    QrGraSrv1CodUsu: TIntegerField;
    QrGraSrv1Nome: TWideStringField;
    EdInfAdProd: TdmkEdit;
    Label4: TLabel;
    QrGraSrv1infAdProd: TWideStringField;
    QrGraSrv1cListServ: TIntegerField;
    QrGraSrv1Descricao: TWideMemoField;
    EdcListServ: TdmkEditCB;
    CBcListServ: TdmkDBLookupComboBox;
    Label10: TLabel;
    DBMemo2: TDBMemo;
    QrListServ: TmySQLQuery;
    QrListServCodTxt: TWideStringField;
    QrListServCodigo: TIntegerField;
    QrListServNome: TWideStringField;
    QrListServDescricao: TWideMemoField;
    DsListServ: TDataSource;
    SpeedButton5: TSpeedButton;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    EdPrecoPc: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdPrecoM2: TdmkEdit;
    Label13: TLabel;
    EdPrecokg: TdmkEdit;
    QrGraSrv1PrecoPc: TFloatField;
    QrGraSrv1PrecoM2: TFloatField;
    QrGraSrv1Precokg: TFloatField;
    Panel7: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    DBMemo1: TDBMemo;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    Label17: TLabel;
    CBUnidMed: TdmkDBLookupComboBox;
    dmkValUsu1: TdmkValUsu;
    QrPesq1: TmySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Sigla: TWideStringField;
    Label18: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit7: TDBEdit;
    QrGraSrv1UnidMed: TIntegerField;
    QrGraSrv1SIGLAUNIDMED: TWideStringField;
    QrGraSrv1CODUSUUNIDMED: TIntegerField;
    QrGraSrv1NOMEUNIDMED: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraSrv1AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraSrv1BeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure PesquisaPorSigla(Limpa: Boolean);
    procedure PesquisaPorCodigo();
  public
    { Public declarations }
  end;

var
  FmGraSrv1: TFmGraSrv1;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ListServ, MyDBCheck, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraSrv1.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraSrv1.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraSrv1Codigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraSrv1.DefParams;
begin
  VAR_GOTOTABELA := 'GraSrv1';
  VAR_GOTOMYSQLTABLE := QrGraSrv1;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT gs1.Codigo, gs1.CodUsu, gs1.Nome,');
  VAR_SQLx.Add('gs1.infAdProd, gs1.cListServ, lse.Descricao,');
  VAR_SQLx.Add('gs1.PrecoPc, gs1.PrecoM2, gs1.Precokg, ');
  VAR_SQLx.Add('gs1.UnidMed, med.Sigla SIGLAUNIDMED,');
  VAR_SQLx.Add('med.CodUsu CODUSUUNIDMED,');
  VAR_SQLx.Add('med.Nome NOMEUNIDMED');
  VAR_SQLx.Add('FROM grasrv1 gs1');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.listserv lse ON lse.Codigo=gs1.cListServ');
  VAR_SQLx.Add('LEFT JOIN unidmed med ON med.Codigo=gs1.UnidMed');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE gs1.Codigo > 0');
  //
  VAR_SQL1.Add('AND gs1.Codigo=:P0');
  //
  VAR_SQL2.Add('AND gs1.TxtUsu=:P0');
  //
  VAR_SQLa.Add('AND gsi.Descricao Like :P0');
  //
end;

procedure TFmGraSrv1.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'grasrv1', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmGraSrv1.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    PesquisaPorSigla(False);
end;

procedure TFmGraSrv1.EdSiglaExit(Sender: TObject);
begin
  PesquisaPorSigla(True);
end;

procedure TFmGraSrv1.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    PesquisaPorCodigo();
end;

procedure TFmGraSrv1.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraSrv1.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraSrv1.PesquisaPorCodigo();
begin
  QrPesq2.Close;
  QrPesq2.Params[0].AsString := EdUnidMed.Text;
  QrPesq2.Open;
  if QrPesq2.RecordCount > 0 then
  begin
    if EdSigla.ValueVariant <> QrPesq2Sigla.Value then
      EdSigla.ValueVariant := QrPesq2Sigla.Value;
  end else EdSigla.ValueVariant := '';
end;

procedure TFmGraSrv1.PesquisaPorSigla(Limpa: Boolean);
begin
  QrPesq1.Close;
  QrPesq1.Params[0].AsString := EdSigla.Text;
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdUnidMed.ValueVariant <> QrPesq1CodUsu.Value then
      EdUnidMed.ValueVariant := QrPesq1CodUsu.Value;
    if CBUnidMed.KeyValue     <> QrPesq1CodUsu.Value then
      CBUnidMed.KeyValue     := QrPesq1CodUsu.Value;
  end else if Limpa then
    EdSigla.ValueVariant := '';
end;

procedure TFmGraSrv1.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmGraSrv1.AlteraRegistro;
var
  GraSrv1 : Integer;
begin
  GraSrv1 := QrGraSrv1Codigo.Value;
  if QrGraSrv1Codigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(GraSrv1, Dmod.MyDB, 'GraSrv1', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GraSrv1, Dmod.MyDB, 'GraSrv1', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGraSrv1.IncluiRegistro;
var
  Cursor : TCursor;
  GraSrv1 : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    GraSrv1 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GraSrv1', 'GraSrv1', 'Codigo');
    if Length(FormatFloat(FFormatFloat, GraSrv1))>Length(FFormatFloat) then
    begin
      Geral.MB_(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, GraSrv1);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmGraSrv1.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraSrv1.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraSrv1.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraSrv1.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraSrv1.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraSrv1.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraSrv1.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmListServ, FmListServ, afmoNegarComAviso) then
  begin
    FmListServ.ShowModal;
    FmListServ.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    EdcListserv.ValueVariant := VAR_CADASTRO;
    CBcListserv.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmGraSrv1.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraSrv1, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'grasrv1');
end;

procedure TFmGraSrv1.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraSrv1Codigo.Value;
  Close;
end;

procedure TFmGraSrv1.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('GraSrv1', 'Codigo', ImgTipo.SQLType,
    QrGraSrv1Codigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraSrv1, PainelEdita,
    'GraSrv1', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraSrv1.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraSrv1', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraSrv1', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraSrv1', 'Codigo');
end;

procedure TFmGraSrv1.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrGraSrv1, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'grasrv1');
end;

procedure TFmGraSrv1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelData.Align  := alClient;
  DBMemo2.Align     := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrListServ, DModG.AllID_DB);
  QrUnidmed.Open;
end;

procedure TFmGraSrv1.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraSrv1Codigo.Value, LaRegistro.Caption);
end;

procedure TFmGraSrv1.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmGraSrv1.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraSrv1.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraSrv1CodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraSrv1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraSrv1.QrGraSrv1AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraSrv1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraSrv1.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraSrv1Codigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GraSrv1', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraSrv1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraSrv1.QrGraSrv1BeforeOpen(DataSet: TDataSet);
begin
  QrGraSrv1Codigo.DisplayFormat := FFormatFloat;
end;

end.

