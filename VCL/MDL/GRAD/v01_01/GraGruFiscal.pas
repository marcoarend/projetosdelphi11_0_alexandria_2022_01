unit GraGruFiscal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkGeral, dmkLabel, Mask, DBCtrls,
  dmkDBEdit, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, DB, mySQLDbTables,
  dmkRadioGroup, dmkCheckGroup, ComCtrls, dmkImage, UnDmkEnums, Vcl.Menus,
  dmkCheckBox;

type
  TFmGraGruFiscal = class(TForm)
    Panel1: TPanel;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    QrPesq1: TmySQLQuery;
    QrPesq1CodUsu: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Sigla: TWideStringField;
    Panel4: TPanel;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Panel5: TPanel;
    Panel3: TPanel;
    QrMedOrdem: TmySQLQuery;
    DsMedOrdem: TDataSource;
    QrMedOrdemCodigo: TIntegerField;
    QrMedOrdemCodUsu: TIntegerField;
    QrMedOrdemNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    Panel7: TPanel;
    Panel8: TPanel;
    Label2: TLabel;
    QrMatPartCad: TmySQLQuery;
    QrMatPartCadCodigo: TIntegerField;
    QrMatPartCadCodUsu: TIntegerField;
    QrMatPartCadNome: TWideStringField;
    DsMatPartCad: TDataSource;
    Panel9: TPanel;
    Label15: TLabel;
    EdInfAdProd: TdmkEdit;
    Label5: TLabel;
    Label13: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    EdIPI_CST: TdmkEdit;
    EdTextoIPI_CST: TdmkEdit;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GroupBox5: TGroupBox;
    Label7: TLabel;
    EdPIS_CST: TdmkEdit;
    EdTextoPIS_CST: TdmkEdit;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    EdPIS_AlqV: TdmkEdit;
    Label19: TLabel;
    EdPIS_AlqP: TdmkEdit;
    Label18: TLabel;
    Label20: TLabel;
    EdPISST_AlqP: TdmkEdit;
    EdPISST_AlqV: TdmkEdit;
    Label21: TLabel;
    GroupBox7: TGroupBox;
    Label22: TLabel;
    EdCOFINS_CST: TdmkEdit;
    EdTextoCOFINS_CST: TdmkEdit;
    Panel11: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    EdCOFINS_AlqV: TdmkEdit;
    EdCOFINS_AlqP: TdmkEdit;
    GroupBox8: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    EdCOFINSST_AlqP: TdmkEdit;
    EdCOFINSST_AlqV: TdmkEdit;
    EdNCM: TdmkEdit;
    Label4: TLabel;
    Label30: TLabel;
    EdcGTIN_EAN: TdmkEdit;
    Panel12: TPanel;
    RGIPI_TpTrib: TdmkRadioGroup;
    Label27: TLabel;
    Label17: TLabel;
    Label31: TLabel;
    Label9: TLabel;
    EdIPI_vUnid: TdmkEdit;
    EdIPI_pIPI: TdmkEdit;
    EdIPI_cEnq: TdmkEdit;
    EdEX_TIPI: TdmkEdit;
    EdPIS_pRedBC: TdmkEdit;
    Label32: TLabel;
    EdPISST_pRedBCST: TdmkEdit;
    Label33: TLabel;
    EdCOFINS_pRedBC: TdmkEdit;
    Label34: TLabel;
    EdCOFINSST_pRedBCST: TdmkEdit;
    Label35: TLabel;
    GroupBox10: TGroupBox;
    Label6: TLabel;
    EdIPIRec_pRedBC: TdmkEdit;
    GroupBox11: TGroupBox;
    Label14: TLabel;
    EdPISRec_pRedBC: TdmkEdit;
    GroupBox12: TGroupBox;
    Label39: TLabel;
    EdCOFINSRec_pRedBC: TdmkEdit;
    EdIPIRec_pAliq: TdmkEdit;
    RgIPIRec_tCalc: TdmkRadioGroup;
    EdPISRec_pAliq: TdmkEdit;
    RgPISRec_tCalc: TdmkRadioGroup;
    EdCOFINSRec_pAliq: TdmkEdit;
    RgCOFINSRec_tCalc: TdmkRadioGroup;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    SbNCM: TSpeedButton;
    RGDadosFisc: TdmkRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel15: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    EdIPI_cEnqGrupo: TdmkEdit;
    MeIPI_cEnqTXT: TMemo;
    TabSheet5: TTabSheet;
    PMNCM: TPopupMenu;
    CadastrodeNCM1: TMenuItem;
    N1: TMenuItem;
    PesquisaemNFes1: TMenuItem;
    Ckprod_indEscala: TdmkCheckBox;
    EdSiglaCustm: TdmkEdit;
    Label16: TLabel;
    Label52: TLabel;
    Edprod_cBarra: TdmkEdit;
    RGUsaSubsTrib: TdmkRadioGroup;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    Panel17: TPanel;
    Label8: TLabel;
    Label47: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label48: TLabel;
    RGICMS_modBC: TdmkRadioGroup;
    EdICMS_pRedBC: TdmkEdit;
    EdICMS_pDif: TdmkEdit;
    EdICMS_Pauta: TdmkEdit;
    EdICMS_MaxTab: TdmkEdit;
    EdICMS_pICMSDeson: TdmkEdit;
    Panel16: TPanel;
    Label49: TLabel;
    EdICMS_motDesICMS: TdmkEdit;
    EdICMS_motDesICMS_TXT: TEdit;
    Panel6: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    Label51: TLabel;
    EdCST_A: TdmkEdit;
    EdTextoA: TdmkEdit;
    EdCST_B: TdmkEdit;
    EdTextoB: TdmkEdit;
    EdCSOSN: TdmkEdit;
    EdTextoC: TdmkEdit;
    GroupBox15: TGroupBox;
    GroupBox9: TGroupBox;
    Panel2: TPanel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label50: TLabel;
    dmkRadioGroup1: TdmkRadioGroup;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    Edprod_CEST: TdmkEdit;
    Panel13: TPanel;
    GroupBox4: TGroupBox;
    Label12: TLabel;
    Label40: TLabel;
    EdICMSRec_pRedBC: TdmkEdit;
    EdICMSRec_pAliq: TdmkEdit;
    RGICMSRec_tCalc: TdmkRadioGroup;
    GroupBox13: TGroupBox;
    Label44: TLabel;
    Label45: TLabel;
    EdICMSAliqSINTEGRA: TdmkEdit;
    EdICMSST_BaseSINTEGRA: TdmkEdit;
    GroupBox14: TGroupBox;
    Label46: TLabel;
    EdSPEDEFD_ALIQ_ICMS: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCST_AChange(Sender: TObject);
    procedure EdCST_BChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCST_AKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIPI_CSTChange(Sender: TObject);
    procedure EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPIS_CSTChange(Sender: TObject);
    procedure EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCOFINS_CSTChange(Sender: TObject);
    procedure EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNCMClick(Sender: TObject);
    procedure EdICMS_motDesICMSChange(Sender: TObject);
    procedure EdIPI_cEnqChange(Sender: TObject);
    procedure EdIPI_cEnqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CadastrodeNCM1Click(Sender: TObject);
    procedure PesquisaemNFes1Click(Sender: TObject);
    procedure EdNCMRedefinido(Sender: TObject);
    procedure EdCSOSNChange(Sender: TObject);
    procedure EdCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FEnquadramentoLegalIPI: MyArrayLista;
  public
    { Public declarations }
  end;

  var
  FmGraGruFiscal: TFmGraGruFiscal;

implementation

uses
  {$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
  {$IfNDef SemNFe_0000} NFe_PF, UnNFeListas, {$EndIf}
  UnMyObjects, GraGruN, UMySQLModule, Module, UnMySQLCuringa,
  UnidMed, UnInternalConsts, MyDBCheck, SelOnStringGrid, MedOrdem, MatPartCad,
  UnGrade_Jan, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruFiscal.BtOKClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  if (EdPIS_AlqP.ValueVariant > 0) and (EdPIS_AlqV.ValueVariant > 0) then
  begin
    Geral.MB_Aviso('Informe apenas a aliquota em % ou em $ para o PIS!');
    Exit;
  end;
  if (EdPISST_AlqP.ValueVariant > 0) and (EdPISST_AlqV.ValueVariant > 0) then
  begin
    Geral.MB_Aviso('Informe apenas a aliquota em % ou em $ para o PIS ST!');
    Exit;
  end;
  if (EdCOFINS_AlqP.ValueVariant > 0) and (EdCOFINS_AlqV.ValueVariant > 0) then
  begin
    Geral.MB_Aviso('Informe apenas a aliquota em % ou em $ para o COFINS!');
    Exit;
  end;
  if (EdCOFINSST_AlqP.ValueVariant > 0) and (EdCOFINSST_AlqV.ValueVariant > 0) then
  begin
    Geral.MB_Aviso('Informe apenas a aliquota em % ou em $ para o COFINS ST!');
    Exit;
  end;
  Nivel1 := FmGraGruN.QrGraGru1Nivel1.Value;
  if UMyMod.ExecSQLInsUpdFm(FmGraGruFiscal, stUpd, 'gragru1', Nivel1, Dmod.QrUpd)
  then begin
    FmGraGruN.ReopenGraGru1(Nivel1);
    Close;
  end;
end;

procedure TFmGraGruFiscal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruFiscal.CadastrodeNCM1Click(Sender: TObject);
var
  NCMSel: String;
begin
  VAR_CADASTRO := 0;
  NCMSel       := Grade_Jan.MostraFormClasFisc();
  //
  if NCMSel <> '' then
    EdNCM.Texto := NCMSel;
end;

procedure TFmGraGruFiscal.EdPIS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdPIS_CST.Text);
{$EndIf}
end;

procedure TFmGraGruFiscal.EdPIS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdPis_CST.Text := UFinanceiro.ListaDeCST_PIS();
{$EndIf}
end;

procedure TFmGraGruFiscal.EdNCMRedefinido(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmGraGruFiscal.EdCOFINS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdCOFINS_CST.Text);
{$EndIf}
end;

procedure TFmGraGruFiscal.EdCOFINS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCOFINS_CST.Text := UFinanceiro.ListaDeCST_COFINS();
{$EndIf}
end;

procedure TFmGraGruFiscal.EdCSOSNChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  EdTextoC.Text := UFinanceiro.CSOSN_Get(1, Geral.IMV(EdCSOSN.Text));
  {$EndIf}
end;

procedure TFmGraGruFiscal.EdCSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  EdTextoC.Text := UFinanceiro.CSOSN_Get(1, Geral.IMV(EdCSOSN.Text));
  {$EndIf}
end;

procedure TFmGraGruFiscal.EdCST_AChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoA.Text := UFinanceiro.CST_A_Get(Geral.IMV(EdCST_A.Text));
{$EndIf}
end;

procedure TFmGraGruFiscal.EdCST_AKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_A.Text := UFinanceiro.ListaDeSituacaoTributaria();
{$EndIf}
end;

procedure TFmGraGruFiscal.EdCST_BChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
{$EndIf}
end;

procedure TFmGraGruFiscal.EdCST_BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
{$EndIf}
end;

procedure TFmGraGruFiscal.EdICMS_motDesICMSChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  EdICMS_motDesICMS_TXT.Text :=
    UnNFe_PF.TextoDeCodigoNFe(nfeCTmotDesICMS, EdICMS_motDesICMS.ValueVariant);
{$EndIf}
end;

procedure TFmGraGruFiscal.EdIPI_cEnqChange(Sender: TObject);
var
  Texto1, Texto2: String;
begin
  Geral.DescricaoDeArrStrStr2(EdIPI_cEnq.Text, FEnquadramentoLegalIPI, Texto1, Texto2, 0, 1, 2);
  EdIPI_cEnqGrupo.Text := Texto1;
  MeIPI_cEnqTXT.Text   := Texto2;
end;

procedure TFmGraGruFiscal.EdIPI_cEnqKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if (Key = VK_F4) then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIPI_cEnq.Text := Geral.SelecionaItem(FEnquadramentoLegalIPI, 0,
    'SEL-LISTA-000 :: Sele��o do C�digo de Enquadramento Legal do IPI', TitCols, Screen.Width)
  end;
end;

procedure TFmGraGruFiscal.EdIPI_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdIPI_CST.Text);
{$EndIf}
end;

procedure TFmGraGruFiscal.EdIPI_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdIPI_CST.Text := UFinanceiro.ListaDeCST_IPI();
{$EndIf}
end;

procedure TFmGraGruFiscal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
{$IfNDef NO_FINANCEIRO}
  EdTextoA.Text := UFinanceiro.CST_A_Get(Geral.IMV(EdCST_A.Text));
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
  EdTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdIPI_CST.Text);
  EdTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdCOFINS_CST.Text);
  EdTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdPIS_CST.Text);
{$EndIf}
end;

procedure TFmGraGruFiscal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMedOrdem, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMatPartCad, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
  //
  {$IfNDef SemNFe_0000}
    FEnquadramentoLegalIPI := NFeListas.ArrayEnquadramentoLegalIPI();
  {$EndIf}
end;

procedure TFmGraGruFiscal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruFiscal.PesquisaemNFes1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de NCM';
  Prompt = 'Selecione a NCM';
var
  Qry: TmySQLQuery;
  Ds: TDataSource;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Ds := TDataSource.Create(Dmod);
    try
      Ds.DataSet := Qry;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT nfi.prod_NCM, nfi.prod_cProd, nfi.prod_xProd ',
      'FROM nfeitsi nfi',
      'LEFT JOIN gragrux ggx ON ggx.Controle=nfi.GraGruX',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
      'WHERE gg1.Nivel1=' + Geral.FF0(FmGraGruN.QrGraGru1Nivel1.Value),
      'AND nfi.prod_NCM <> ""',
      '']);
      if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, Ds, False, False) then
        EdNCM.Text := Qry.FieldByName('prod_NCM').AsString;
    finally
      Ds.Free;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmGraGruFiscal.SbNCMClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNCM, SbNCM);
end;

(*
Substitui��o tribut�ria

Com�rcios
1. utilize os c�digos de situa��o tribut�ria 010 ou 060 nos casos de substitui��o tribut�ria.
2. Para vendas dentro do estado utiliza-se a al�quota normal do ICMS, por�m a base do ICMS ser� igual ao custo de reposi��o * 1,35 quando pre�o de venda � superior a 35% do custo. Caso contr�rio a base do ICMS n�o se altera.
3. Para vendas fora do estado a base do ICMS n�o se altera e � utilizada a al�quota de ICMS de Substitui��o Tribut�ria
4. O Genesis permite identificar CFOP com substitui��o tribut�ria.

Ind�strias
1. Cliente deve ser revenda
2. Classe fiscal com lucro estimado maior que zero
3. Al�q�ota de ST do estado maior que zero
4. C�lculos:
    Base de C�lculo do ICMS Substitui��o (BCS) = (Valor Mercadoria + IPI + Frete) + ((Valor Mercadoria + IPI + Frete) * Margem Lucro
    Valor ICMS Substitui��o (VIS) = BCS * 17% (ICMS RS) � Valor ICMS
    Valor Total da NF = Valor Mercadoria + Frete + IPI + VIS
5. O modo de cobran�a do ICMS com substitui��o tribut�ria depende do padr�o de pagamento
--------------------------------------------------------------------------------
Margem de Lucro, depende do par�metro Margem Simples definido na Unidade da Federa��o
   Se par�metro ligado = Lucro Estimado / 100
   Se par�metro desligado = { (1 + Lucro Estimado ) / 100 * [((1 - ICMS) / 100) / ((1 - ICMSST) / 100)] } - 1
*)

(*
object GroupBox15: TGroupBox
  Left = 0
  Top = 0
  Width = 776
  Height = 117
  Align = alTop
  Caption = 'Registro SPED EFD 0210: Consumo espec'#237'fico padronizado: '
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object Label51: TLabel
    Left = 8
    Top = 24
    Width = 492
    Height = 13
    Caption =
      '03. Quantidade do item componente/insumo para se produzir uma un' +
      'idade do item composto/resultante:'
  end
  object Label52: TLabel
    Left = 8
    Top = 48
    Width = 568
    Height = 13
    Caption =
      '04. Perda/quebra normal percentual do insumo/componente para se ' +
      'produzir uma unidade do item composto/resultante:'
  end
  object EdEFD0210Qtd_Comp: TdmkEdit
    Left = 632
    Top = 20
    Width = 132
    Height = 21
    Alignment = taRightJustify
    TabOrder = 0
    FormatType = dmktfDouble
    MskType = fmtNone
    DecimalSize = 6
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    Texto = '0,000000'
    QryCampo = 'EFD0210Qtd_Comp'
    UpdCampo = 'EFD0210Qtd_Comp'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = 0.000000000000000000
    ValWarn = False
  end
  object EdEFD0210Perda: TdmkEdit
    Left = 632
    Top = 44
    Width = 132
    Height = 21
    Alignment = taRightJustify
    TabOrder = 1
    FormatType = dmktfDouble
    MskType = fmtNone
    DecimalSize = 4
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    Texto = '0,0000'
    QryCampo = 'EFD0210Perda'
    UpdCampo = 'EFD0210Perda'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = 0.000000000000000000
    ValWarn = False
  end
end
*)

end.
