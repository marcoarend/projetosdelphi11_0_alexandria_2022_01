object FmNCMsMul: TFmNCMsMul
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-048 :: Defini'#231#227'o M'#250'ltipla de NCM'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 323
        Height = 32
        Caption = 'Defini'#231#227'o M'#250'ltipla de NCM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 323
        Height = 32
        Caption = 'Defini'#231#227'o M'#250'ltipla de NCM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 323
        Height = 32
        Caption = 'Defini'#231#227'o M'#250'ltipla de NCM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label6: TLabel
            Left = 8
            Top = 4
            Width = 27
            Height = 13
            Caption = 'NCM:'
          end
          object EdNCM: TdmkEdit
            Left = 8
            Top = 20
            Width = 100
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'NCM'
            UpdCampo = 'NCM'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnRedefinido = EdNCMRedefinido
          end
        end
        object DBGErrGrandz: TdmkDBGridZTO
          Left = 2
          Top = 61
          Width = 1004
          Height = 404
          Align = alClient
          DataSource = DsErrNCMs
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Nivel1'
              Title.Caption = 'N'#237'vel 1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome / tamanho / cor'
              Width = 513
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NCM'
              Width = 106
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 373
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 497
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object VUUnidMed: TdmkValUsu
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
  object QrErrNCMs: TMySQLQuery
    SQL.Strings = (
      'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido,  '
      'gg1.Nome NO_GG1, med.Sigla, med.Grandeza, '
      'xco.Grandeza xco_Grandeza,  '
      'IF(xco.Grandeza > 0, xco.Grandeza,  '
      '  IF((ggx.GraGruY<2048 OR  '
      '  xco.CouNiv2<>1), 2,  '
      '  IF(xco.CouNiv2=1, 1, -1)))  dif_Grandeza '
      ' '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed '
      
        'WHERE vmi.DataHora BETWEEN "2016-01-01" AND "2016-01-31 23:59:59' +
        '" '
      'AND vmi.GraGruX <> 0 '
      'AND ((IF(xco.Grandeza > 0, xco.Grandeza,  '
      '  IF((ggx.GraGruY<2048 OR  '
      '  xco.CouNiv2<>1), 2,  '
      '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza)) ')
    Left = 28
    Top = 256
    object QrErrNCMsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrErrNCMsNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrErrNCMsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object DsErrNCMs: TDataSource
    DataSet = QrErrNCMs
    Left = 28
    Top = 304
  end
end
