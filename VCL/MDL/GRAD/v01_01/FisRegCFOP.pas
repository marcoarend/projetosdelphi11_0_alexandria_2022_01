unit FisRegCFOP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DmkDAC_PF,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, 
  dmkDBEdit, dmkGeral, Variants, dmkValUsu, dmkCheckBox, dmkImage, UnDmkEnums,
  dmkRadioGroup, UnGrl_Geral;

type
  TFmFisRegCFOP = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Panel4: TPanel;
    CkContinuar: TCheckBox;
    QrCFOP: TMySQLQuery;
    QrCFOPCodigo: TWideStringField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    DsCFOP: TDataSource;
    Panel5: TPanel;
    Label1: TLabel;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    CkContribui: TdmkCheckBox;
    CkInterno: TdmkCheckBox;
    CkProprio: TdmkCheckBox;
    Label2: TLabel;
    EdOrdCFOPGer: TdmkEdit;
    SpeedButton1: TSpeedButton;
    CkServico: TdmkCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    CkSubsTrib: TdmkCheckBox;
    Label6: TLabel;
    EdOriCFOP: TdmkEditCB;
    CBOriCFOP: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    QrOriCFOP: TMySQLQuery;
    QrOriCFOPCodigo: TWideStringField;
    QrOriCFOPNome: TWideStringField;
    QrOriCFOPDescricao: TWideMemoField;
    QrOriCFOPComplementacao: TWideMemoField;
    DsOriCFOP: TDataSource;
    Label181: TLabel;
    EdGenCtbD: TdmkEditCB;
    Label15: TLabel;
    EdGenCtbC: TdmkEditCB;
    CBGenCtbD: TdmkDBLookupComboBox;
    CBGenCtbC: TdmkDBLookupComboBox;
    SbGenCtbD: TSpeedButton;
    SbGenCtbC: TSpeedButton;
    Label7: TLabel;
    EdControle: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTemItens: Boolean;
    procedure HabilitaCamposIndice();
  end;

  var
  FmFisRegCFOP: TFmFisRegCFOP;

implementation

uses
  {$IfNDef NAO_GFAT} UnGrade_Jan, {$EndIf}
  UnMyObjects, Module, FisRegCad, UMySQLModule, UnInternalConsts, MyDBCheck,
  UnDmkProcFunc, UnFinanceiroJan;

{$R *.DFM}

procedure TFmFisRegCFOP.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
  TIE_Txt, VMU_Txt, PFP_Txt, PMO_Txt, CFOP, ST_Txt, OriCFOP: String;
  Controle, Interno, Contribui, Proprio, Servico, SubsTrib: Integer;
begin
  if CBCFOP.KeyValue = Null then
    CFOP := ''
  else
    CFOP := EdCFOP.Text;
  if Trim(CFOP) = '' then
  begin
    Geral.MensagemBox('Informe o CFOP!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdCFOP.SetFocus;
    Exit;
  end;
  //
  Codigo := FmFisRegCad.QrFisRegCadCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Interno, Contribui, Proprio, Servico, SubsTrib, CFOP, OriCFOP ',
  'FROM fisregcfop',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  'AND Contribui=' + FormatFloat('0', Geral.BoolToInt(CkContribui.Checked)),
  'AND Interno=' + FormatFloat('0', Geral.BoolToInt(CkInterno.Checked)),
  'AND Proprio=' + FormatFloat('0', Geral.BoolToInt(CkProprio.Checked)),
  'AND Servico=' + FormatFloat('0', Geral.BoolToInt(CkServico.Checked)),
  'AND SubsTrib=' + FormatFloat('0', Geral.BoolToInt(CkSubsTrib.Checked)),
  'AND OriCFOP="' + EdOriCFOP.Text + '"',
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Interno   := Dmod.QrAux.FieldByName('Interno').AsInteger;
    Contribui := Dmod.QrAux.FieldByName('Contribui').AsInteger;
    Proprio   := Dmod.QrAux.FieldByName('Proprio').AsInteger;
    Servico   := Dmod.QrAux.FieldByName('Servico').AsInteger;
    SubsTrib  := Dmod.QrAux.FieldByName('SubsTrib').AsInteger;
    OriCFOP   := Dmod.QrAux.FieldByName('OriCFOP').AsString;
    //
    if (Interno = Geral.BoolToInt(CkInterno.Checked)) and
       (Contribui = Geral.BoolToInt(CkContribui.Checked)) and
       (Proprio = Geral.BoolToInt(CkProprio.Checked)) and
       (Servico = Geral.BoolToInt(CkServico.Checked)) and
       (SubsTrib = Geral.BoolToInt(CkSubsTrib.Checked)) then
    begin
      if ImgTipo.SQLType = stIns then
      begin
        if CkContribui.Checked then TIE_Txt := 'Sim' else TIE_Txt := 'N�o';
        if CkInterno.Checked then VMU_Txt := 'Sim' else VMU_Txt := 'N�o';
        if CkProprio.Checked then PFP_Txt := 'Sim' else PFP_Txt := 'N�o';
        if CkServico.Checked then PMO_Txt := 'Sim' else PMO_Txt := 'N�o';
        if CkSubsTrib.Checked then ST_Txt := 'Sim' else ST_Txt := 'N�o';
        //
        Geral.MB_Aviso('Para esta configura��o:' + sLineBreak + sLineBreak +
          'Contribuinte UF (I.E.): ' + TIE_Txt + sLineBreak +
          'Venda/compra mesma UF.: ' + VMU_Txt + sLineBreak +
          'Fabrica��o pr�pria.: '    + PFP_Txt + sLineBreak +
          'Presta��o de servi�o.: '  + PMO_Txt + sLineBreak +
          'Substituto tribut�rio.: '  + ST_Txt + sLineBreak +
          'CFOP de Origem.: '  + OriCFOP + sLineBreak +
          'J� est� cadastrado o CFOP ' + Dmod.QrAux.FieldByName('CFOP').AsString
          + ' e n�o � permitido duplica��o!');
        Exit;
      end;
    end;
  end;
  //
  Controle := EdControle.ValueVariant;
  if Controle = 0 then
    Controle := UMyMod.BPGS1I32('fisregcfop', 'Controle', '', '', tsPos, stIns, Controle);
  EdControle.ValueVariant := Controle;
  //
  if UMyMod.ExecSQLInsUpdFm(FmFisRegCFOP, ImgTipo.SQLType, 'FisRegCFOP', Codigo,
  Dmod.QrUpd) then
  begin
    FmFisRegCad.ReopenFisRegCFOP(CFOP);
    Interno   := Geral.BoolToInt(CkInterno.Checked);
    Contribui := Geral.BoolToInt(CkContribui.Checked);
    Proprio   := Geral.BoolToInt(CkProprio.Checked);
    Servico   := Geral.BoolToInt(CkServico.Checked);
    SubsTrib  := Geral.BoolToInt(CkSubsTrib.Checked);
    OriCFOP   := EdOriCFOP.Text;
    FmFisRegCad.QrFisRegCFOP.Locate(
    'CFOP; Interno; Contribui; Proprio; Servico; SubsTrib; OriCFOP',
    VarArrayOf([CFOP, Interno, Contribui, Proprio, Servico, SubsTrib, OriCFOP]), [loCaseInsensitive]);

    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType       := stIns;
      HabilitaCamposIndice();
      EdCFOP.Text           := '';
      EdControle.ValueVariant := 0;
      CBCFOP.KeyValue       := Null;
      CkContribui.Checked   := False;
      CkInterno.Checked     := False;
      CkProprio.Checked     := False;
      CkSubsTrib.Checked    := False;
      EdCFOP.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmFisRegCFOP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFisRegCFOP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFisRegCFOP.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FTemItens := False;
  //
  UnDmkDAC_PF.AbreQuery(QrCFOP, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOriCFOP, Dmod.MyDB);
end;

procedure TFmFisRegCFOP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFisRegCFOP.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Visible := True;
    CkContinuar.Checked := True;
  end else
  begin
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
  end;
end;

procedure TFmFisRegCFOP.HabilitaCamposIndice();
var
  Habilita: Boolean;
begin
  Habilita := (ImgTipo.SQLType = stIns) or (FTemItens = False);
  //
  CkContribui.Enabled := Habilita;
  CkInterno.Enabled   := Habilita;
  CkProprio.Enabled   := Habilita;
  CkServico.Enabled   := Habilita;
  CkSubsTrib.Enabled  := Habilita;
  EdOriCFOP.Enabled   := Habilita;
  CBOriCFOP.Enabled   := Habilita;
end;

procedure TFmFisRegCFOP.ImgTipoChange(Sender: TObject);
begin
  HabilitaCamposIndice();
end;

procedure TFmFisRegCFOP.SpeedButton1Click(Sender: TObject);
begin
{$IfNDef NAO_GFAT}
  VAR_CADTEXTO := '';
  //
  Grade_Jan.MostraFormCFOP2003();
  //
  if VAR_CADTEXTO <> '' then
  begin
    QrCFOP.Close;
    UnDmkDAC_PF.AbreQuery(QrCFOP, Dmod.MyDB);
    EdCFOP.ValueVariant := VAR_CADTEXTO;
    CBCFOP.KeyValue     := VAR_CADTEXTO;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

end.
