object FmStqCnsgVeCab: TFmStqCnsgVeCab
  Left = 368
  Top = 194
  Caption = 'STQ-MOVIM-008 :: Consigna'#231#227'o - Venda'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 570
  Constraints.MinWidth = 1024
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 89
    Width = 1008
    Height = 542
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 493
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 4
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 870
        Top = 1
        Width = 135
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        Enabled = False
      end
      object Label13: TLabel
        Left = 200
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object SpeedButton5: TSpeedButton
        Left = 972
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label23: TLabel
        Left = 136
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Ciclo:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 20
        Width = 61
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdNome: TdmkEdit
        Left = 200
        Top = 20
        Width = 769
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdStqCiclCab: TdmkEdit
        Left = 136
        Top = 20
        Width = 61
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'StqCiclCab'
        UpdCampo = 'StqCiclCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 97
      Width = 1006
      Height = 48
      Align = alTop
      TabOrder = 2
      object LaFornece: TdmkLabel
        Left = 8
        Top = 4
        Width = 130
        Height = 13
        Caption = 'Vendedor / Representante:'
        UpdType = utYes
        SQLType = stNil
      end
      object SbFornece: TSpeedButton
        Left = 313
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbForneceClick
      end
      object dmkLabel3: TdmkLabel
        Left = 668
        Top = 4
        Width = 69
        Height = 13
        Caption = 'Transportador:'
        UpdType = utYes
        SQLType = stNil
      end
      object SbTransporta: TSpeedButton
        Left = 973
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbForneceClick
      end
      object dmkLabel7: TdmkLabel
        Left = 336
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        UpdType = utYes
        SQLType = stNil
      end
      object SbCliente: TSpeedButton
        Left = 645
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteClick
      end
      object EdFornece: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornece'
        UpdCampo = 'Fornece'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 248
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsFornece
        TabOrder = 1
        dmkEditCB = EdFornece
        QryCampo = 'Fornece'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransporta: TdmkEditCB
        Left = 668
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 724
        Top = 20
        Width = 248
        Height = 21
        DropDownRows = 2
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsTransporta
        TabOrder = 5
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pEntidades
      end
      object EdCliente: TdmkEditCB
        Left = 336
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 392
        Top = 20
        Width = 253
        Height = 21
        DropDownRows = 2
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsCliente
        TabOrder = 3
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 49
      Width = 1006
      Height = 48
      Align = alTop
      TabOrder = 1
      object dmkLabel1: TdmkLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label10: TLabel
        Left = 767
        Top = 4
        Width = 43
        Height = 13
        Caption = 'Abertura:'
        Enabled = False
      end
      object EdFilial: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 695
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 1
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdAbertura: TdmkEdit
        Left = 765
        Top = 20
        Width = 228
        Height = 21
        Enabled = False
        TabOrder = 2
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'Abertura'
        UpdCampo = 'Abertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel8: TPanel
      Left = 1
      Top = 145
      Width = 1006
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object Label5: TLabel
        Left = 316
        Top = 4
        Width = 98
        Height = 13
        Caption = 'S'#233'rie da Nota Fiscal:'
      end
      object Label6: TLabel
        Left = 421
        Top = 4
        Width = 81
        Height = 13
        Caption = 'N'#186' da nota fiscal:'
      end
      object Label192: TLabel
        Left = 548
        Top = 4
        Width = 264
        Height = 13
        Caption = 'Chave NF-e p/ cosulta no site www.nfe.fazenda.gov.br:'
        FocusControl = EdNFe_Id
      end
      object dmkLabel6: TdmkLabel
        Left = 8
        Top = 5
        Width = 129
        Height = 13
        Caption = 'Tabela de pre'#231'o de venda:'
        UpdType = utYes
        SQLType = stNil
      end
      object SbTabePrcCab: TSpeedButton
        Left = 293
        Top = 21
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTabePrcCabClick
      end
      object Ednfe_serie: TdmkEdit
        Left = 316
        Top = 20
        Width = 100
        Height = 21
        Alignment = taRightJustify
        MaxLength = 5
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'nfe_serie'
        UpdCampo = 'nfe_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Ednfe_nNF: TdmkEdit
        Left = 421
        Top = 20
        Width = 120
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'nfe_nNF'
        UpdCampo = 'nfe_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFe_Id: TdmkEdit
        Left = 548
        Top = 20
        Width = 270
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'nfe_Id'
        UpdCampo = 'nfe_Id'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtzPrcMed: TdmkCheckBox
        Left = 824
        Top = 24
        Width = 185
        Height = 17
        Caption = 'Atualiza custo m'#233'dio dos itens.'
        TabOrder = 5
        Visible = False
        QryCampo = 'AtzPrcMed'
        UpdCampo = 'AtzPrcMed'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdTabePrcCab: TdmkEditCB
        Left = 8
        Top = 21
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TabePrcCab'
        UpdCampo = 'TabePrcCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTabePrcCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTabePrcCab: TdmkDBLookupComboBox
        Left = 64
        Top = 21
        Width = 229
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTabePrcCab
        TabOrder = 1
        dmkEditCB = EdTabePrcCab
        QryCampo = 'TabePrcCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 89
    Width = 1008
    Height = 542
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 366
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 361
      ExplicitWidth = 1004
    end
    object DBGStqMovIts: TDBGrid
      Left = 0
      Top = 189
      Width = 1008
      Height = 92
      Align = alTop
      DataSource = DsStqMovIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PGT'
          Title.Caption = 'Grupo'
          Width = 119
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Produto'
          Width = 167
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_COR'
          Title.Caption = 'Cor'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TAM'
          Title.Caption = 'Tamanho'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoBuy'
          Title.Caption = '$ Mercad.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoFrt'
          Title.Caption = '$ Frete'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorAll'
          Title.Caption = '$ Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID Ctrl'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGru1'
          Title.Caption = 'Nivel 1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoAll'
          Visible = True
        end>
    end
    object PnCabeca: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 192
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 68
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label18: TLabel
        Left = 936
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Ciclo:'
        FocusControl = DBEdit16
      end
      object Label19: TLabel
        Left = 608
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        FocusControl = DBEdit17
      end
      object Label20: TLabel
        Left = 684
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Valor vendas:'
        FocusControl = DBEdit18
      end
      object Label21: TLabel
        Left = 768
        Top = 4
        Width = 71
        Height = 13
        Caption = 'Valor recebido:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 852
        Top = 4
        Width = 75
        Height = 13
        Caption = 'Valor a receber:'
        FocusControl = DBEdit20
      end
      object Label24: TLabel
        Left = 132
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Ciclo:'
        FocusControl = DBEdit21
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsStqCnsgVeCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 192
        Top = 20
        Width = 413
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsStqCnsgVeCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 20
        Width = 61
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsStqCnsgVeCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit16: TDBEdit
        Left = 936
        Top = 20
        Width = 56
        Height = 21
        DataField = 'StqCiclCab'
        DataSource = DsStqCnsgVeCab
        TabOrder = 3
      end
      object DBEdit17: TDBEdit
        Left = 608
        Top = 20
        Width = 72
        Height = 21
        DataField = 'TotalQtde'
        DataSource = DsStqCnsgVeCab
        TabOrder = 4
      end
      object DBEdit18: TDBEdit
        Left = 684
        Top = 20
        Width = 80
        Height = 21
        DataField = 'TotValorAll'
        DataSource = DsStqCnsgVeCab
        TabOrder = 5
      end
      object DBEdit19: TDBEdit
        Left = 768
        Top = 20
        Width = 80
        Height = 21
        DataField = 'PagValorAll'
        DataSource = DsStqCnsgVeCab
        TabOrder = 6
      end
      object DBEdit20: TDBEdit
        Left = 852
        Top = 20
        Width = 80
        Height = 21
        DataField = 'OPNValorAll'
        DataSource = DsStqCnsgVeCab
        TabOrder = 7
      end
      object DBEdit21: TDBEdit
        Left = 132
        Top = 20
        Width = 56
        Height = 21
        DataField = 'StqCiclCab'
        TabOrder = 8
      end
    end
    object GroupBox5: TGroupBox
      Left = 0
      Top = 93
      Width = 1008
      Height = 48
      Align = alTop
      TabOrder = 3
      object dmkLabel4: TdmkLabel
        Left = 8
        Top = 4
        Width = 130
        Height = 13
        Caption = 'Vendedor / Representante:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel2: TdmkLabel
        Left = 616
        Top = 4
        Width = 69
        Height = 13
        Caption = 'Transportador:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label16: TLabel
        Left = 892
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Frete:'
        FocusControl = DBEdit9
      end
      object dmkLabel5: TdmkLabel
        Left = 304
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        UpdType = utYes
        SQLType = stNil
      end
      object DBEdit11: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Fornece'
        DataSource = DsStqCnsgVeCab
        TabOrder = 0
      end
      object DBEdit13: TDBEdit
        Left = 64
        Top = 20
        Width = 237
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsStqCnsgVeCab
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 616
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsStqCnsgVeCab
        TabOrder = 4
      end
      object DBEdit8: TDBEdit
        Left = 672
        Top = 20
        Width = 217
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsStqCnsgVeCab
        TabOrder = 5
      end
      object DBEdit9: TDBEdit
        Left = 892
        Top = 20
        Width = 105
        Height = 21
        DataField = 'ValTotFrete'
        DataSource = DsStqCnsgVeCab
        TabOrder = 6
      end
      object DBEdit14: TDBEdit
        Left = 304
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsStqCnsgVeCab
        TabOrder = 2
      end
      object DBEdit15: TDBEdit
        Left = 360
        Top = 20
        Width = 253
        Height = 21
        DataField = 'NO_CLIENTE'
        DataSource = DsStqCnsgVeCab
        TabOrder = 3
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 45
      Width = 1008
      Height = 48
      Align = alTop
      TabOrder = 2
      object Label4: TLabel
        Left = 8
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit2
      end
      object Label11: TLabel
        Left = 766
        Top = 8
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label12: TLabel
        Left = 882
        Top = 8
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object DBEdit2: TDBEdit
        Left = 8
        Top = 24
        Width = 752
        Height = 21
        DataField = 'NOMEFILIAL'
        DataSource = DsStqCnsgVeCab
        TabOrder = 0
      end
      object DBEdit5: TDBEdit
        Left = 766
        Top = 24
        Width = 112
        Height = 21
        DataField = 'Abertura'
        DataSource = DsStqCnsgVeCab
        TabOrder = 1
      end
      object DBEdit6: TDBEdit
        Left = 882
        Top = 24
        Width = 112
        Height = 21
        DataField = 'ENCERROU_TXT'
        DataSource = DsStqCnsgVeCab
        TabOrder = 2
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 371
      Width = 1008
      Height = 107
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 5
      object dmkLabelRotate1: TdmkLabelRotate
        Left = 0
        Top = 0
        Width = 17
        Height = 107
        Angle = ag90
        Caption = 'Mercadorias'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Align = alLeft
      end
      object dmkLabelRotate2: TdmkLabelRotate
        Left = 548
        Top = 0
        Width = 17
        Height = 107
        Angle = ag90
        Caption = 'Frete'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Align = alLeft
        ExplicitLeft = 617
      end
      object Splitter2: TSplitter
        Left = 545
        Top = 0
        Height = 107
        ExplicitLeft = 542
      end
      object DBGVen: TDBGrid
        Left = 17
        Top = 0
        Width = 528
        Height = 107
        Align = alLeft
        DataSource = DsPgtVen
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SEQ'
            Title.Caption = 'N'#186
            Width = 22
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECARTEIRA'
            Title.Caption = 'Carteira'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SIT'
            Title.Caption = 'Situa'#231#227'o'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Compensado_TXT'
            Title.Caption = 'Compensado'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'Lan'#231'amento'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 182
            Visible = True
          end>
      end
      object DBGFrt: TDBGrid
        Left = 565
        Top = 0
        Width = 443
        Height = 107
        Align = alClient
        DataSource = DsPgtFrt
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SEQ'
            Title.Caption = 'N'#186
            Width = 22
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECARTEIRA'
            Title.Caption = 'Carteira'
            Width = 147
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SIT'
            Title.Caption = 'Situa'#231#227'o'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Compensado_TXT'
            Title.Caption = 'Compensado'
            Width = 93
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 250
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'Lan'#231'amento'
            Width = 75
            Visible = True
          end>
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 478
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 6
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 266
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 440
        Top = 15
        Width = 566
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 433
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 291
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Venda'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtItens: TBitBtn
          Tag = 525
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItensClick
        end
        object BtPagto: TBitBtn
          Tag = 187
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pagto'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtPagtoClick
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 141
      Width = 1008
      Height = 48
      Align = alTop
      TabOrder = 4
      object Label9: TLabel
        Left = 316
        Top = 8
        Width = 98
        Height = 13
        Caption = 'S'#233'rie da Nota Fiscal:'
      end
      object Label14: TLabel
        Left = 421
        Top = 8
        Width = 81
        Height = 13
        Caption = 'N'#186' da nota fiscal:'
      end
      object Label15: TLabel
        Left = 546
        Top = 8
        Width = 264
        Height = 13
        Caption = 'Chave NF-e p/ cosulta no site www.nfe.fazenda.gov.br:'
        FocusControl = EdNFe_Id
      end
      object Label17: TLabel
        Left = 8
        Top = 8
        Width = 134
        Height = 13
        Caption = 'Tabele de pre'#231'os de venda:'
        FocusControl = DBEdit10
      end
      object DBEdit3: TDBEdit
        Left = 316
        Top = 24
        Width = 100
        Height = 21
        DataField = 'nfe_serie'
        DataSource = DsStqCnsgVeCab
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 421
        Top = 24
        Width = 120
        Height = 21
        DataField = 'nfe_nNF'
        DataSource = DsStqCnsgVeCab
        TabOrder = 3
      end
      object DBEdNFe_Id: TDBEdit
        Left = 546
        Top = 24
        Width = 270
        Height = 21
        DataField = 'nfe_Id'
        DataSource = DsStqCnsgVeCab
        TabOrder = 4
      end
      object DBCkAtzPrcMed: TDBCheckBox
        Left = 828
        Top = 28
        Width = 165
        Height = 17
        Caption = 'Atualiza custo m'#233'dio dos itens.'
        DataField = 'AtzPrcMed'
        DataSource = DsStqCnsgVeCab
        TabOrder = 5
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit10: TDBEdit
        Left = 64
        Top = 24
        Width = 249
        Height = 21
        DataField = 'NO_TabePrcCab'
        DataSource = DsStqCnsgVeCab
        TabOrder = 1
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 24
        Width = 56
        Height = 21
        DataField = 'TabePrcCab'
        DataSource = DsStqCnsgVeCab
        TabOrder = 0
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 260
        Height = 32
        Caption = 'Consigna'#231#227'o - Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 260
        Height = 32
        Caption = 'Consigna'#231#227'o - Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 260
        Height = 32
        Caption = 'Consigna'#231#227'o - Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 37
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 20
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 433
        Height = 16
        Caption = 
          'O estoque e os custos ser'#227'o atualizados somente ap'#243's encerrar a ' +
          'entrada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 433
        Height = 16
        Caption = 
          'O estoque e os custos ser'#227'o atualizados somente ap'#243's encerrar a ' +
          'entrada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsStqCnsgVeCab: TDataSource
    DataSet = QrStqCnsgVeCab
    Left = 228
    Top = 148
  end
  object QrStqCnsgVeCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrStqCnsgVeCabBeforeOpen
    AfterOpen = QrStqCnsgVeCabAfterOpen
    BeforeClose = QrStqCnsgVeCabBeforeClose
    AfterClose = QrStqCnsgVeCabAfterClose
    AfterScroll = QrStqCnsgVeCabAfterScroll
    OnCalcFields = QrStqCnsgVeCabCalcFields
    SQL.Strings = (
      'SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE,'
      'IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSPORTA,'
      'tpc.Nome NO_TabePrcCab, pem.BalQtdItem, pem.FatSemEstq, '
      'sic.TotValorAll - sic.PagValorAll OPNValorAll, sic.*, '
      'ei.CodCliInt'
      'FROM stqcnsgvecab sic'
      'LEFT JOIN enticliint ei ON ei.CodEnti=sic.Empresa'
      'LEFT JOIN entidades fil  ON fil.Codigo=sic.Empresa'
      'LEFT JOIN paramsemp pem  ON pem.Codigo=sic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece'
      'LEFT JOIN entidades cli ON cli.Codigo=sic.Cliente'
      'LEFT JOIN entidades tsp ON tsp.Codigo=sic.Transporta'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=sic.TabePrcCab'
      'WHERE sic.Codigo > -1000')
    Left = 228
    Top = 100
    object QrStqCnsgVeCabENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 50
      Calculated = True
    end
    object QrStqCnsgVeCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'stqinncad.Codigo'
    end
    object QrStqCnsgVeCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'stqinncad.CodUsu'
    end
    object QrStqCnsgVeCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'stqinncad.Nome'
      Size = 50
    end
    object QrStqCnsgVeCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqinncad.Empresa'
    end
    object QrStqCnsgVeCabAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'stqinncad.Abertura'
    end
    object QrStqCnsgVeCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'stqinncad.Encerrou'
    end
    object QrStqCnsgVeCabStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'stqinncad.Status'
    end
    object QrStqCnsgVeCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqinncad.Ativo'
    end
    object QrStqCnsgVeCabNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrStqCnsgVeCabNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrStqCnsgVeCabBalQtdItem: TFloatField
      FieldName = 'BalQtdItem'
      Origin = 'paramsemp.BalQtdItem'
    end
    object QrStqCnsgVeCabFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
    end
    object QrStqCnsgVeCabFornece: TIntegerField
      FieldName = 'Fornece'
      Origin = 'stqinncad.Fornece'
    end
    object QrStqCnsgVeCabCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrStqCnsgVeCabnfe_serie: TIntegerField
      FieldName = 'nfe_serie'
    end
    object QrStqCnsgVeCabnfe_nNF: TIntegerField
      FieldName = 'nfe_nNF'
    end
    object QrStqCnsgVeCabnfe_Id: TWideStringField
      FieldName = 'nfe_Id'
      Size = 44
    end
    object QrStqCnsgVeCabAtzPrcMed: TSmallintField
      FieldName = 'AtzPrcMed'
    end
    object QrStqCnsgVeCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrStqCnsgVeCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 120
    end
    object QrStqCnsgVeCabValTotFrete: TFloatField
      FieldName = 'ValTotFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrStqCnsgVeCabNO_TabePrcCab: TWideStringField
      FieldName = 'NO_TabePrcCab'
      Size = 50
    end
    object QrStqCnsgVeCabTabePrcCab: TIntegerField
      FieldName = 'TabePrcCab'
    end
    object QrStqCnsgVeCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 120
    end
    object QrStqCnsgVeCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrStqCnsgVeCabStqCiclCab: TIntegerField
      FieldName = 'StqCiclCab'
    end
    object QrStqCnsgVeCabOPNValorAll: TFloatField
      FieldName = 'OPNValorAll'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrStqCnsgVeCabTotalQtde: TFloatField
      FieldName = 'TotalQtde'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrStqCnsgVeCabTotValorAll: TFloatField
      FieldName = 'TotValorAll'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrStqCnsgVeCabPagValorAll: TFloatField
      FieldName = 'PagValorAll'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65524
  end
  object QrStqMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora')
    Left = 640
    Top = 65532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqMovItsNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrStqMovItsNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = 'gragru1.Nome'
      Size = 50
    end
    object QrStqMovItsPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrStqMovItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
    end
    object QrStqMovItsNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = 'prdgruptip.Nome'
      Size = 30
    end
    object QrStqMovItsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrStqMovItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrStqMovItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrStqMovItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrStqMovItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = 'gragrux.GraGruC'
    end
    object QrStqMovItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
    end
    object QrStqMovItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = 'gragrux.GraTamI'
    end
    object QrStqMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'stqmovitsa.DataHora'
    end
    object QrStqMovItsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovitsa.IDCtrl'
    end
    object QrStqMovItsTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'stqmovitsa.Tipo'
    end
    object QrStqMovItsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovitsa.OriCodi'
    end
    object QrStqMovItsOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'stqmovitsa.OriCtrl'
    end
    object QrStqMovItsOriCnta: TIntegerField
      FieldName = 'OriCnta'
      Origin = 'stqmovitsa.OriCnta'
    end
    object QrStqMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovitsa.Empresa'
    end
    object QrStqMovItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'stqmovitsa.StqCenCad'
    end
    object QrStqMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'stqmovitsa.GraGruX'
    end
    object QrStqMovItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'stqmovitsa.Qtde'
    end
    object QrStqMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovitsa.AlterWeb'
    end
    object QrStqMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovitsa.Ativo'
    end
    object QrStqMovItsOriPart: TIntegerField
      FieldName = 'OriPart'
      Origin = 'stqmovitsa.OriPart'
    end
    object QrStqMovItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'stqmovitsa.Pecas'
    end
    object QrStqMovItsPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'stqmovitsa.Peso'
    end
    object QrStqMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = 'stqmovitsa.AreaM2'
    end
    object QrStqMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      Origin = 'stqmovitsa.AreaP2'
    end
    object QrStqMovItsFatorClas: TFloatField
      FieldName = 'FatorClas'
      Origin = 'stqmovitsa.FatorClas'
    end
    object QrStqMovItsQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
      Origin = 'stqmovitsa.QuemUsou'
    end
    object QrStqMovItsRetorno: TSmallintField
      FieldName = 'Retorno'
      Origin = 'stqmovitsa.Retorno'
    end
    object QrStqMovItsParTipo: TIntegerField
      FieldName = 'ParTipo'
      Origin = 'stqmovitsa.ParTipo'
    end
    object QrStqMovItsParCodi: TIntegerField
      FieldName = 'ParCodi'
      Origin = 'stqmovitsa.ParCodi'
    end
    object QrStqMovItsDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
      Origin = 'stqmovitsa.DebCtrl'
    end
    object QrStqMovItsSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
      Origin = 'stqmovitsa.SMIMultIns'
    end
    object QrStqMovItsCustoAll: TFloatField
      FieldName = 'CustoAll'
      Origin = 'stqmovitsa.CustoAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsValorAll: TFloatField
      FieldName = 'ValorAll'
      Origin = 'stqmovitsa.ValorAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsCustoBuy: TFloatField
      FieldName = 'CustoBuy'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsCustoFrt: TFloatField
      FieldName = 'CustoFrt'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsPackGGX: TIntegerField
      FieldName = 'PackGGX'
    end
    object QrStqMovItsPackUnMed: TIntegerField
      FieldName = 'PackUnMed'
    end
    object QrStqMovItsPackQtde: TFloatField
      FieldName = 'PackQtde'
    end
    object QrStqMovItsTwnCtrl: TIntegerField
      FieldName = 'TwnCtrl'
    end
    object QrStqMovItsTwnBrth: TIntegerField
      FieldName = 'TwnBrth'
    end
  end
  object DsStqMovIts: TDataSource
    DataSet = QrStqMovIts
    Left = 640
    Top = 44
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 568
    Top = 336
    object Incluinovavenda1: TMenuItem
      Caption = '&Inclui nova venda'
      OnClick = Incluinovavenda1Click
    end
    object Alteravendaatual1: TMenuItem
      Caption = '&Altera venda atual'
      OnClick = Alteravendaatual1Click
    end
    object Excluivendaatual1: TMenuItem
      Caption = '&Exclui venda atual'
      Enabled = False
      OnClick = Excluivendaatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Encerravenda1: TMenuItem
      Caption = 'Encerra venda'
      OnClick = Encerravenda1Click
    end
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 660
    Top = 336
    object Incluiitem1: TMenuItem
      Caption = '&Inclui item'
      OnClick = Incluiitem1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Fracionamento1: TMenuItem
      Caption = '&Fracionamento'
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Excluiitem1: TMenuItem
      Caption = '&Exclui item'
      OnClick = Excluiitem1Click
    end
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT '
      'FROM entidades'
      'WHERE'
      '      Fornece5="V"'
      'OR Fornece6="V"'
      'ORDER BY NO_ENT')
    Left = 204
    Top = 304
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 204
    Top = 356
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdFilial
    Panel = PnEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 136
    Top = 65516
  end
  object frxSTQ_MOVIM_008: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 40322.707901088000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin            '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));  '
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);'
      'end.')
    Left = 396
    Top = 60
    Datasets = <
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsForne
        DataSetName = 'frxDsForne'
      end
      item
      end
      item
        DataSet = frxDsStqCnsgVeCab
        DataSetName = 'frxDsStqInnCad'
      end
      item
        DataSet = frxDsStqMovIts
        DataSetName = 'frxDsStqMovIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 173.968528350000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 2.779530000000000000
          Top = 3.779530000000001000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoTE1: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Top = 151.181200000000000000
          Width = 699.212598430000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'INFORME DE VENDA DE CONSIGNA'#199#195'O N'#186' [frxDsStqInnCad."CodUsu"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 68.031540000000010000
          Width = 699.213050000000000000
          Height = 75.590600000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 616.063390000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsForne."E_ALL"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 68.031540000000010000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 124.724490000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsForne."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsForne."CAD_FEDERAL"]:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 124.724490000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsForne."IE_TXT"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsForne."CAD_ESTADUAL"]:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 124.724490000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsForne."TE1_TXT"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 68.031540000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsForne."CodUsu"] - [frxDsForne."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 495.118430000000000000
        Width = 699.213050000000000000
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 253.228510000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsStqInnCad."Codigo"'
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 245.669291338582700000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 64.251968500000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Custo')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456697800000000000
        Top = 336.378170000000000000
        Width = 699.213050000000000000
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 7.559060000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsStqMovIts."Qtde">)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 7.559060000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsStqMovIts."CustoAll">)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 510.236508500000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637795275590000
        Top = 294.803340000000000000
        Width = 699.213050000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsStqMovIts
        DataSetName = 'frxDsStqMovIts'
        RowCount = 0
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."NO_TAM"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."NO_COR"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."SIGLA"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsStqMovIts."Qtde"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 245.669291338582700000
          Height = 18.897637800000000000
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."NO_PRD"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Width = 64.251968500000000000
          Height = 18.897637795275590000
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          DisplayFormat.FormatStr = '%g'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsStqMovIts."GraGruX"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 94.488188980000000000
          Height = 18.897637800000000000
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsStqMovIts."CustoAll"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133890000000000000
        Top = 423.307360000000000000
        Width = 699.213050000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 30.236240000000000000
          Width = 544.252320000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsForne."NOME_ENT"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsStqMovIts: TfrxDBDataset
    UserName = 'frxDsStqMovIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'DataHora=DataHora'
      'IDCtrl=IDCtrl'
      'Tipo=Tipo'
      'OriCodi=OriCodi'
      'OriCtrl=OriCtrl'
      'OriCnta=OriCnta'
      'Empresa=Empresa'
      'StqCenCad=StqCenCad'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'OriPart=OriPart'
      'Pecas=Pecas'
      'Peso=Peso'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'FatorClas=FatorClas'
      'QuemUsou=QuemUsou'
      'Retorno=Retorno'
      'ParTipo=ParTipo'
      'ParCodi=ParCodi'
      'DebCtrl=DebCtrl'
      'SMIMultIns=SMIMultIns'
      'CustoAll=CustoAll'
      'ValorAll=ValorAll')
    DataSet = QrStqMovIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 396
    Top = 249
  end
  object QrForne: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrForneCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO2_ENT' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 396
    Top = 105
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrForneE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrForneCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrForneNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrForneTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrForneFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrForneNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrForneCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrForneCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrForneTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrForneCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
      Required = True
    end
    object QrForneNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrForneCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrForneIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrForneRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrForneCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrForneBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrForneCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrForneNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrForneNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrFornePais: TWideStringField
      FieldName = 'Pais'
    end
    object QrForneENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrForneTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrForneFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrForneIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
    end
    object QrForneCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrForneCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrForneIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrForneNO2_ENT: TWideStringField
      FieldName = 'NO2_ENT'
      Size = 60
    end
    object QrForneNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrForneUF: TFloatField
      FieldName = 'UF'
    end
    object QrForneLOGRAD: TFloatField
      FieldName = 'LOGRAD'
    end
    object QrForneCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object frxDsForne: TfrxDBDataset
    UserName = 'frxDsForne'
    CloseDataSource = False
    FieldAliases.Strings = (
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'TE1_TXT=TE1_TXT'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'NUMERO=NUMERO'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'Lograd=Lograd'
      'CEP=CEP'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'IE=IE'
      'UF=UF'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'IE_TXT=IE_TXT'
      'NO2_ENT=NO2_ENT')
    DataSet = QrForne
    BCDToCurrency = False
    DataSetOptions = []
    Left = 396
    Top = 153
  end
  object frxDsStqCnsgVeCab: TfrxDBDataset
    UserName = 'frxDsStqInnCad'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ENCERROU_TXT=ENCERROU_TXT'
      'Codigo=Codigo'
      'CodUsu=CodUsu'
      'Nome=Nome'
      'Empresa=Empresa'
      'PrdGrupTip=PrdGrupTip'
      'StqCenCad=StqCenCad'
      'CasasProd=CasasProd'
      'Abertura=Abertura'
      'Encerrou=Encerrou'
      'Status=Status'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NOMEFILIAL=NOMEFILIAL'
      'NO_FORNECE=NO_FORNECE'
      'NOMEPRDGRUPTIP=NOMEPRDGRUPTIP'
      'MOMESTQCENCAD=MOMESTQCENCAD'
      'BalQtdItem=BalQtdItem'
      'FatSemEstq=FatSemEstq'
      'Fornece=Fornece')
    DataSet = QrStqCnsgVeCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 396
    Top = 201
  end
  object PMPagto: TPopupMenu
    OnPopup = PMPagtoPopup
    Left = 748
    Top = 336
    object Produtos1: TMenuItem
      Caption = '&Mercadorias'
      object IncluiVen1: TMenuItem
        Caption = '&Inclui '
        OnClick = IncluiVen1Click
      end
      object ExcluiVen1: TMenuItem
        Caption = '&Exclui'
        OnClick = ExcluiVen1Click
      end
    end
    object Frete1: TMenuItem
      Caption = 'Frete'
      object IncluiFrt1: TMenuItem
        Caption = '&Inclui'
        OnClick = IncluiFrt1Click
      end
      object ExcluiFrt1: TMenuItem
        Caption = '&Exclui'
        OnClick = ExcluiFrt1Click
      end
    end
  end
  object QrPgtVen: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPgtVenCalcFields
    Left = 745
    Top = 65533
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPgtVenData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtVenVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtVenBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtVenSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPgtVenFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPgtVenContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPgtVenDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPgtVenDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPgtVenFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPgtVenFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPgtVenNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPgtVenNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPgtVenBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPgtVenAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPgtVenConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPgtVenTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPgtVenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPgtVenNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrPgtVenCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPgtVenAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPgtVenCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPgtVenNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
    end
    object QrPgtVenCompensado_TXT: TWideStringField
      FieldName = 'Compensado_TXT'
      Size = 10
    end
  end
  object DsPgtVen: TDataSource
    DataSet = QrPgtVen
    Left = 745
    Top = 45
  end
  object PMNovo: TPopupMenu
    Left = 296
    Top = 136
    object ID1: TMenuItem
      Caption = '&ID'
      OnClick = ID1Click
    end
    object Pesquisadetalhada1: TMenuItem
      Caption = '&Pesquisa detalhada'
      OnClick = Pesquisadetalhada1Click
    end
  end
  object QrPgtFrt: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPgtFrtCalcFields
    Left = 813
    Top = 65533
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPgtFrtData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPgtFrtVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPgtFrtBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPgtFrtSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPgtFrtFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPgtFrtContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPgtFrtDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPgtFrtDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPgtFrtFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPgtFrtFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPgtFrtNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPgtFrtNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPgtFrtBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPgtFrtAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPgtFrtConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPgtFrtTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPgtFrtControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPgtFrtNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrPgtFrtCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPgtFrtDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPgtFrtAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPgtFrtNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
    end
    object QrPgtFrtCompensado_TXT: TWideStringField
      FieldName = 'Compensado_TXT'
      Size = 10
    end
  end
  object DsPgtFrt: TDataSource
    DataSet = QrPgtFrt
    Left = 813
    Top = 45
  end
  object QrTransporta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT '
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Fornece7="V"'
      'OR Fornece8="V"'
      'ORDER BY NO_ENT')
    Left = 300
    Top = 304
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 296
    Top = 356
  end
  object QrSumFrt: TMySQLQuery
    Database = Dmod.MyDB
    Left = 440
    Top = 401
    object QrSumFrtDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrSumIts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 440
    Top = 449
    object QrSumItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumItsCustoBuy: TFloatField
      FieldName = 'CustoBuy'
    end
    object QrSumItsValorAll: TFloatField
      FieldName = 'ValorAll'
    end
  end
  object QrTabePrcCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tabeprccab'
      'WHERE Codigo=0'
      'OR'
      '('
      '  :P0 & Aplicacao'
      '  AND :P1 BETWEEN DataI and DataF'
      ')'
      'ORDER BY Nome')
    Left = 108
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTabePrcCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTabePrcCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsTabePrcCab: TDataSource
    DataSet = QrTabePrcCab
    Left = 108
    Top = 356
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT '
      'FROM entidades'
      'WHERE'
      '      Cliente1="V"'
      'OR Cliente2="V"'
      'ORDER BY NO_ENT')
    Left = 380
    Top = 304
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 380
    Top = 356
  end
  object QrSumVen: TMySQLQuery
    Database = Dmod.MyDB
    Left = 440
    Top = 501
    object QrSumVenValor: TFloatField
      FieldName = 'Valor'
    end
  end
end
