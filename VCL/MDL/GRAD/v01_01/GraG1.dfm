object FmGraG1: TFmGraG1
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-030 :: Produtos de Tabelas Personalizadas'
  ClientHeight = 606
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 167
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtOK: TBitBtn
        Tag = 18
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtRegulariza: TBitBtn
        Tag = 95
        Left = 59
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtRegularizaClick
      end
      object BtNiveis: TBitBtn
        Tag = 364
        Left = 113
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtNiveisClick
      end
    end
    object GB_M: TGroupBox
      Left = 167
      Top = 0
      Width = 1015
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 518
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Produtos de Tabelas Personalizadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 518
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Produtos de Tabelas Personalizadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 518
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Produtos de Tabelas Personalizadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 406
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 406
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 80
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = ' Filtros de Pesquisa: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 1237
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 10
            Top = 5
            Width = 136
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tabela personalizada:'
          end
          object EdGraTabApp: TdmkEditCB
            Left = 10
            Top = 25
            Width = 69
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGraTabAppChange
            DBLookupComboBox = CBGraTabApp
            IgnoraDBLookupComboBox = False
          end
          object CBGraTabApp: TdmkDBLookupComboBox
            Left = 79
            Top = 25
            Width = 872
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGraTabApp
            TabOrder = 1
            dmkEditCB = EdGraTabApp
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object DBGGraGru1: TdmkDBGridZTO
        Left = 0
        Top = 80
        Width = 1241
        Height = 326
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsGraGru1
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Nivel1'
            Title.Caption = 'Nivel 1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 444
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel2'
            Title.Caption = 'Nivel 2'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel3'
            Title.Caption = 'Nivel 3'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel4'
            Title.Caption = 'Nivel 4'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel5'
            Title.Caption = 'Nivel 5'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PrdGrupTip'
            Title.Caption = 'Tipo'
            Width = 56
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 465
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 519
    Width = 1241
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 5
        Top = 4
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 155
        Top = 4
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BitBtn1: TBitBtn
        Tag = 364
        Left = 310
        Top = 4
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Subir web'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BitBtn1Click
      end
    end
  end
  object QrGraTabApp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gratabapp'
      'ORDER BY Nome')
    Left = 312
    Top = 176
    object QrGraTabAppCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraTabAppNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGraTabAppTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
  end
  object DsGraTabApp: TDataSource
    DataSet = QrGraTabApp
    Left = 312
    Top = 224
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraGru1AfterOpen
    BeforeClose = QrGraGru1BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM gragru1')
    Left = 556
    Top = 12
    object QrGraGru1Nivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGru1Nivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
    end
    object QrGraGru1IPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrGraGru1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrGraGru1IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrGraGru1IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrGraGru1IPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object QrGraGru1TipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
    object QrGraGru1PerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
    end
    object QrGraGru1PerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
    end
    object QrGraGru1MedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
    object QrGraGru1PartePrinc: TIntegerField
      FieldName = 'PartePrinc'
    end
    object QrGraGru1InfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Size = 255
    end
    object QrGraGru1SiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
    object QrGraGru1HowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGru1GerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGru1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrGraGru1PIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
    end
    object QrGraGru1PIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
    end
    object QrGraGru1PISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
    end
    object QrGraGru1PISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
    end
    object QrGraGru1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrGraGru1COFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
    end
    object QrGraGru1COFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
    end
    object QrGraGru1COFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
    end
    object QrGraGru1COFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
    end
    object QrGraGru1ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrGraGru1ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrGraGru1ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrGraGru1ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrGraGru1ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrGraGru1ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrGraGru1ICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
    end
    object QrGraGru1ICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
    end
    object QrGraGru1IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrGraGru1cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrGraGru1EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrGraGru1PIS_pRedBC: TFloatField
      FieldName = 'PIS_pRedBC'
    end
    object QrGraGru1PISST_pRedBCST: TFloatField
      FieldName = 'PISST_pRedBCST'
    end
    object QrGraGru1COFINS_pRedBC: TFloatField
      FieldName = 'COFINS_pRedBC'
    end
    object QrGraGru1COFINSST_pRedBCST: TFloatField
      FieldName = 'COFINSST_pRedBCST'
    end
    object QrGraGru1ICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrGraGru1IPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrGraGru1PISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrGraGru1COFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrGraGru1IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrGraGru1ICMSRec_tCalc: TSmallintField
      FieldName = 'ICMSRec_tCalc'
    end
    object QrGraGru1IPIRec_tCalc: TSmallintField
      FieldName = 'IPIRec_tCalc'
    end
    object QrGraGru1PISRec_tCalc: TSmallintField
      FieldName = 'PISRec_tCalc'
    end
    object QrGraGru1COFINSRec_tCalc: TSmallintField
      FieldName = 'COFINSRec_tCalc'
    end
    object QrGraGru1ICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrGraGru1ICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object QrGraGru1FatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGru1prod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrGraGru1Referencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGru1Fabricante: TIntegerField
      FieldName = 'Fabricante'
    end
    object QrGraGru1PerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrGraGru1PerComissR: TFloatField
      FieldName = 'PerComissR'
    end
    object QrGraGru1PerComissZ: TSmallintField
      FieldName = 'PerComissZ'
    end
    object QrGraGru1NomeTemp: TWideStringField
      FieldName = 'NomeTemp'
      Size = 120
    end
    object QrGraGru1COD_LST: TWideStringField
      FieldName = 'COD_LST'
      Size = 4
    end
    object QrGraGru1SPEDEFD_ALIQ_ICMS: TFloatField
      FieldName = 'SPEDEFD_ALIQ_ICMS'
    end
    object QrGraGru1DadosFisc: TSmallintField
      FieldName = 'DadosFisc'
    end
    object QrGraGru1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGru1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGru1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGru1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGru1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGru1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGru1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGru1GraTabApp: TIntegerField
      FieldName = 'GraTabApp'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 584
    Top = 12
  end
  object QrGTA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gratabapp'
      'ORDER BY Nome')
    Left = 612
    Top = 12
    object QrGTACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGTANome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGTATabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 164
    Top = 232
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
