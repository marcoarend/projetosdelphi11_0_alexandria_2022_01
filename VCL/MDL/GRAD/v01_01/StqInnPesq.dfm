object FmStqInnPesq: TFmStqInnPesq
  Left = 339
  Top = 185
  Caption = 'STQ-ENTRA-003 :: Pesquisa - Entrada por Compra'
  ClientHeight = 513
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 382
        Height = 32
        Caption = 'Pesquisa - Entrada por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 382
        Height = 32
        Caption = 'Pesquisa - Entrada por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 382
        Height = 32
        Caption = 'Pesquisa - Entrada por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 351
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 351
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 351
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 140
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 48
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
          end
          object Label6: TLabel
            Left = 8
            Top = 92
            Width = 81
            Height = 13
            Caption = 'N'#186' da nota fiscal:'
          end
          object Label192: TLabel
            Left = 135
            Top = 92
            Width = 60
            Height = 13
            Caption = 'Chave NF-e:'
            FocusControl = EdNFe_Id
          end
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object EdFornece: TdmkEditCB
            Left = 8
            Top = 64
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Contratada'
            UpdCampo = 'Contratada'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornece
            IgnoraDBLookupComboBox = False
          end
          object CBFornece: TdmkDBLookupComboBox
            Left = 64
            Top = 64
            Width = 466
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Fornece_Txt'
            ListSource = DsFornece
            TabOrder = 3
            dmkEditCB = EdFornece
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object Edide_nNF: TdmkEdit
            Left = 8
            Top = 108
            Width = 120
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'nfe_nNF'
            UpdCampo = 'nfe_nNF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFe_Id: TdmkEdit
            Left = 135
            Top = 108
            Width = 270
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'nfe_Id'
            UpdCampo = 'nfe_Id'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 24
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Contratada'
            UpdCampo = 'Contratada'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 24
            Width = 466
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BtPesquisa: TBitBtn
            Tag = 22
            Left = 410
            Top = 92
            Width = 120
            Height = 40
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 6
            OnClick = BtPesquisaClick
          end
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 155
          Width = 780
          Height = 194
          Align = alClient
          DataSource = DsStqInnCad
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ENCERROU_TXT'
              Title.Caption = 'Encerramento'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Caption = 'Valor Total'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nfe_serie'
              Title.Caption = 'S'#233'rie'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nfe_nNF'
              Title.Caption = 'Nota fiscal'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFILIAL'
              Title.Caption = 'Empresa'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nfe_Id'
              Title.Caption = 'Chave NF-e'
              Width = 250
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 399
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 443
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOk: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial,  Nome) Fornece_Txt'
      'FROM Entidades'
      'ORDER BY Fornece_Txt')
    Left = 486
    Top = 15
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceFornece_Txt: TWideStringField
      FieldName = 'Fornece_Txt'
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 515
    Top = 15
  end
  object QrStqInnCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrStqInnCadAfterOpen
    BeforeClose = QrStqInnCadBeforeClose
    SQL.Strings = (
      
        'SELECT sic.Codigo, sic.CodUsu, sic.Nome, SUM(stq.CustoAll) Valor' +
        ','
      'sic.nfe_serie, sic.nfe_nNF, sic.nfe_Id,'
      'IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,'
      'IF(frn.Tipo=0,frn.RazaoSocial,frn.Nome) NO_FORNECE,'
      
        'IF(sic.Encerrou < "1900-01-01", "MOVIM. ABERTO", DATE_FORMAT(sic' +
        '.Encerrou, "%d/%m/%Y %H:%i:%s")) ENCERROU_TXT'
      'FROM stqinncad sic'
      'LEFT JOIN entidades fil  ON fil.Codigo=sic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece'
      
        'LEFT JOIN stqmovitsa stq ON (stq.Tipo="0005" AND stq.OriCodi=sic' +
        '.Codigo)'
      'GROUP BY sic.Codigo'
      'ORDER BY sic.Codigo')
    Left = 228
    Top = 280
    object QrStqInnCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqInnCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqInnCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrStqInnCadValor: TFloatField
      FieldName = 'Valor'
    end
    object QrStqInnCadnfe_serie: TIntegerField
      FieldName = 'nfe_serie'
    end
    object QrStqInnCadnfe_nNF: TIntegerField
      FieldName = 'nfe_nNF'
    end
    object QrStqInnCadnfe_Id: TWideStringField
      FieldName = 'nfe_Id'
      Size = 44
    end
    object QrStqInnCadNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrStqInnCadNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrStqInnCadENCERROU_TXT: TWideStringField
      FieldName = 'ENCERROU_TXT'
      Size = 50
    end
  end
  object DsStqInnCad: TDataSource
    DataSet = QrStqInnCad
    Left = 256
    Top = 280
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = Panel5
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 636
    Top = 76
  end
end
