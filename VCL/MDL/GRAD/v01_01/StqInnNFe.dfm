object FmStqInnNFe: TFmStqInnNFe
  Left = 339
  Top = 185
  Caption = 'STQ-ENTRA-002 :: Entrada por Compra - NF-e'
  ClientHeight = 562
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 406
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
      end
      object Label2: TLabel
        Left = 452
        Top = 24
        Width = 450
        Height = 13
        Caption = 
          '[F4] nos campos Pe'#231'as, '#193'rea ou Peso copia o conte'#250'do do campo qu' +
          'antidade.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdStqCenCad: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 373
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 1
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 48
      Width = 773
      Height = 358
      Align = alClient
      DataSource = DsStqInnNFe
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnKeyDown = DBGrid1KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'prod_xProd'
          Title.Caption = 'Nome do item (dado pelo fornecedor)'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'prod_uCom'
          Title.Caption = 'Unidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'prod_qCom'
          Title.Caption = 'Quantidade'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'prod_vUnCom'
          Title.Caption = 'Pre'#231'o'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'prod_vProd'
          Title.Caption = 'Total'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'M2'
          Title.Caption = #193'rea m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'P2'
          Title.Caption = #193'rea ft'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nItem'
          Title.Caption = 'Item'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'prod_cProd'
          Title.Caption = 'C'#243'digo do fornecedor'
          Width = 128
          Visible = True
        end>
    end
    object PnOpt: TPanel
      Left = 773
      Top = 48
      Width = 235
      Height = 358
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 193
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Duplo clique seleciona o reduzido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 13
        Width = 235
        Height = 345
        Align = alClient
        DataSource = DsStqInnOpt
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGrid2DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Cor'
            Title.Caption = 'Cor'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Tam'
            Title.Caption = 'Tamanho'
            Width = 48
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 331
        Height = 32
        Caption = 'Entrada por Compra - NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 331
        Height = 32
        Caption = 'Entrada por Compra - NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 331
        Height = 32
        Caption = 'Entrada por Compra - NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 454
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 498
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 13
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object dmkEdit1: TdmkEdit
        Left = 160
        Top = 16
        Width = 253
        Height = 21
        TabOrder = 2
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 68
    Top = 8
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 96
    Top = 8
  end
  object QrStqInnNFe_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqinnnfe')
    Left = 704
    Top = 136
    object QrStqInnNFe_prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrStqInnNFe_prod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrStqInnNFe_prod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrStqInnNFe_prod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrStqInnNFe_prod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrStqInnNFe_prod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrStqInnNFe_Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrStqInnNFe_GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqInnNFe_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrStqInnNFe_Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrStqInnNFe_Peso: TFloatField
      FieldName = 'Peso'
    end
    object QrStqInnNFe_M2: TFloatField
      FieldName = 'M2'
    end
    object QrStqInnNFe_P2: TFloatField
      FieldName = 'P2'
    end
  end
  object DsStqInnNFe: TDataSource
    DataSet = TbStqInnNFe
    Left = 676
    Top = 136
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM gragrux'
      'WHERE GraGru1=:P0')
    Left = 352
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrStqInnOpt: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrStqInnOptAfterOpen
    BeforeClose = QrStqInnOptBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM stqinnopt'
      'WHERE nitem=:P0')
    Left = 648
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStqInnOptnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrStqInnOptGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrStqInnOptNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 30
    end
    object QrStqInnOptNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 5
    end
  end
  object DsStqInnOpt: TDataSource
    DataSet = QrStqInnOpt
    Left = 676
    Top = 164
  end
  object QrErros: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM stqinnnfe'
      'WHERE GraGruX = 0'
      'OR '
      ' (NeedPC<>0 AND Pecas=0)'
      'OR '
      ' (NeedM2<>0 AND M2=0)'
      'OR '
      ' (NeedKg<>0 AND Peso=0)'
      'ORDER BY nItem')
    Left = 648
    Top = 196
    object QrErrosNeedPc: TIntegerField
      FieldName = 'NeedPc'
    end
    object QrErrosNeedM2: TIntegerField
      FieldName = 'NeedM2'
    end
    object QrErrosNeedKg: TIntegerField
      FieldName = 'NeedKg'
    end
    object QrErrosnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrErrosPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrErrosPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrErrosM2: TFloatField
      FieldName = 'M2'
    end
    object QrErrosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object TbStqInnNFe: TMySQLTable
    Database = Dmod.MyDB
    BeforeClose = TbStqInnNFeBeforeClose
    BeforePost = TbStqInnNFeBeforePost
    AfterScroll = TbStqInnNFeAfterScroll
    TableName = 'stqinnnfe'
    Left = 648
    Top = 136
    object TbStqInnNFenItem: TIntegerField
      FieldName = 'nItem'
      ReadOnly = True
    end
    object TbStqInnNFeprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      ReadOnly = True
      Size = 60
    end
    object TbStqInnNFeprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      ReadOnly = True
      Size = 120
    end
    object TbStqInnNFeprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      ReadOnly = True
      Size = 6
    end
    object TbStqInnNFeprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      ReadOnly = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object TbStqInnNFeprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      ReadOnly = True
    end
    object TbStqInnNFeprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      ReadOnly = True
    end
    object TbStqInnNFeNivel1: TIntegerField
      FieldName = 'Nivel1'
      ReadOnly = True
    end
    object TbStqInnNFeGraGruX: TIntegerField
      FieldName = 'GraGruX'
      ReadOnly = True
    end
    object TbStqInnNFePecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object TbStqInnNFePeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object TbStqInnNFeM2: TFloatField
      FieldName = 'M2'
      OnChange = TbStqInnNFeM2Change
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbStqInnNFeP2: TFloatField
      FieldName = 'P2'
      OnChange = TbStqInnNFeP2Change
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbStqInnNFeprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object TbStqInnNFeprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object TbStqInnNFeprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object TbStqInnNFeprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
  end
end
