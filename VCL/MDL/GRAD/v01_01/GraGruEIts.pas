unit GraGruEIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, Mask, DBCtrls, dmkEdit, DB,
  mySQLDbTables, dmkRadioGroup, dmkEditCB, dmkDBLookupComboBox, dmkGeral,
  Principal, Menus, Variants, dmkImage, UnDmkEnums, UnProjGroup_Consts,
  DmkDAC_PF, UnApp_Jan;

type
  TFmGraGruEIts = class(TForm)
    DsEmbalagens: TDataSource;
    QrEmbalagens: TmySQLQuery;
    QrEmbalagensCodigo: TIntegerField;
    QrEmbalagensNome: TWideStringField;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    Panel1: TPanel;
    Memo1: TMemo;
    Panel3: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    SbEmbalagem: TSpeedButton;
    Label4: TLabel;
    SbGraGruX: TSpeedButton;
    CBEmbalagem: TdmkDBLookupComboBox;
    EdEmbalagem: TdmkEditCB;
    EdObservacao: TdmkEdit;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXNivel1: TIntegerField;
    PMGraGruX: TPopupMenu;
    CadastrosemGrade1: TMenuItem;
    CadastroComGrade1: TMenuItem;
    Usoeconsumo1: TMenuItem;
    Matriaprima1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    CkContinuar: TCheckBox;
    Panel2: TPanel;
    Label2: TLabel;
    Label5: TLabel;
    EdxProd: TdmkEdit;
    EdcProd: TdmkEdit;
    Label6: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    SbFornece: TSpeedButton;
    Label7: TLabel;
    EdCFOP: TdmkEdit;
    Label8: TLabel;
    EduCom: TdmkEdit;
    EdNCM: TdmkEdit;
    Label9: TLabel;
    QrGraGruECad: TmySQLQuery;
    PMProduto: TPopupMenu;
    SpeedButton1: TSpeedButton;
    CkNCM: TCheckBox;
    InsumoUsoeConsumo1: TMenuItem;
    Matriaprima2: TMenuItem;
    N1: TMenuItem;
    EdFatorQtd: TdmkEdit;
    Label10: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbEmbalagemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure SbForneceClick(Sender: TObject);
    procedure Usoeconsumo1Click(Sender: TObject);
    procedure Matriaprima1Click(Sender: TObject);
    procedure CadastroComGrade1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure InsumoUsoeConsumo1Click(Sender: TObject);
    procedure Matriaprima2Click(Sender: TObject);
    procedure EdGraGruXEnter(Sender: TObject);
    procedure CBGraGruXEnter(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    //FFormChamou,
    FNomeGGX: String;
    FGraGruX: Integer;
    // Dados do produto
    F_prod_cEAN,
    F_prod_NCM,
    F_prod_uCom,
    F_InfAdProd,
    F_ICMS_CST_A,
    F_ICMS_CST_B,
    F_IPI_CST,
    F_PIS_CST,
    F_COFINS_CST,
    F_prod_EXTIPI,
    F_IPI_cEnq,
    FTxtPesq: String;
    // FIM dados do produto
    FPM: TPopupMenu;
    //
  end;

  var
  FmGraGruEIts: TFmGraGruEIts;

implementation

uses UnMyObjects, MyDBCheck, UMySQLModule, Module, UnInternalConsts, Entidade2,
  //{$IFDEF FmPQE} PQ, {$ENDIF}
  GraGruN, Embalagens, ModProd, PrdGruNewU, UnAppPF, MyListas;

{$R *.DFM}

procedure TFmGraGruEIts.BtOKClick(Sender: TObject);
var
  cProd, Observacao, xProd, NCM, uCom: String;
  Nivel1, GraGruX, Fornece, Embalagem, CFOP, CFOP_Inn: Integer;
  SQLType: TSQLType;
  FatorQtd: Double;
begin
  SQLType        := ImgTipo.SQLType;
  cProd          := EdcProd.Text;
  Nivel1         := QrGraGruXNivel1.Value;
  GraGruX        := EdGraGruX.ValueVariant;
  Fornece        := EdFornece.ValueVariant;
  Embalagem      := EdEmbalagem.ValueVariant;
  Observacao     := EdObservacao.Text;
  xProd          := EdxProd.Text;
  NCM            := EdNCM.Text;
  CFOP           := EdCFOP.ValueVariant;
  uCom           := EduCom.Text;
  CFOP_Inn       := 0;
  FatorQtd       := EdFatorQtd.ValueVariant;
  //
  if MyObjects.FIC(Fornece = 0, nil, 'Fornecedor n�o informado!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o produto (Reduzido)!') then Exit;
  //if MyObjects.FIC(Embalagem = 0, EdEmbalagem, 'Informe a embalagem!') then Exit;
  if MyObjects.FIC(FatorQtd < 0.0000000001, EdFatorQtd, 'Informe o Fator qtde!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrueits', False, [
  'Embalagem', 'Observacao', 'CFOP_Inn', 'FatorQtd'], [
  'cProd', 'Nivel1', 'GraGruX', 'Fornece', 'xProd', 'NCM', 'CFOP', 'uCom'], [
  Embalagem, Observacao, CFOP_Inn, FatorQtd], [
  cProd, Nivel1, GraGruX, Fornece, xProd, NCM, CFOP, uCom], True) then
  begin
    FGraGruX := GraGruX;
    FNomeGGX := CBGraGruX.Text;
    // ini 2022-04-23
    if CkNCM.Checked then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE gragru1',
      'SET NCM="' + Geral.FormataNCM(Geral.SoNumero_TT(NCM)) + '" ',
      'WHERE Nivel1=' + Geral.FF0(Nivel1),
      'AND',
      '  (NCM IS NULL',
      '  OR',
      '  NCM = "")',
      '']);
    end;
    // fim 2022-04-23
    //
(*
    if FFormChamou = 'FmGraGruN' then
      FmGraGruN.ReopenGraGruEX(FGraGruX);
*)
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdObservacao.Text        := '';
      EdGraGruX.ValueVariant   := 0;
      CBGraGruX.KeyValue       := 0;
      EdEmbalagem.ValueVariant := 0;
      CBEmbalagem.KeyValue     := 0;
    end else
      Close;
  end;
end;

procedure TFmGraGruEIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruEIts.CadastroComGrade1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    DmProd.FPrdGrupTip_Nome := EdxProd.Text;
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
end;

procedure TFmGraGruEIts.CBGraGruXEnter(Sender: TObject);
begin
  if EdGraGruX.ValueVariant = 0 then
    ReopenGraGruX();
end;

procedure TFmGraGruEIts.EdGraGruXEnter(Sender: TObject);
(*var
  GraGruX: Integer;
*)
begin
  if EdGraGruX.ValueVariant = 0 then
    ReopenGraGruX();

{
   if VAR_CADASTRO <> 0 then
   begin
     GraGruX := Dmod.ObtemGraGruXDeGraGru1(VAR_CADASTRO);
     ReopenGraGruX();
     EdGraGruX.ValueVariant := GraGruX;
     CBGraGruX.KeyValue     := GraGruX;
     VAR_CADASTRO := 0;
   end;
}
end;

procedure TFmGraGruEIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruEIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FTxtPesq := '';
  //
  VAR_CADASTRO2 := 0;
  FGraGruX := 0;
  FNomeGGX := '';
  UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  //
  ReopenGraGruX();
end;

procedure TFmGraGruEIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruEIts.InsumoUsoeConsumo1Click(Sender: TObject);
begin
  App_Jan.MostraFormUsoEConsumo();
end;

procedure TFmGraGruEIts.Matriaprima1Click(Sender: TObject);
begin
  DmProd.CadastraSemGrade(-1, EdxProd.Text, F_prod_cEAN, F_prod_NCM, F_prod_uCom,
  F_InfAdProd, F_ICMS_CST_A, F_ICMS_CST_B, F_IPI_CST, F_PIS_CST, F_COFINS_CST,
  F_prod_EXTIPI, F_IPI_cEnq, QrGraGruX, EdGraGruX, CBGraGruX);
end;

procedure TFmGraGruEIts.Matriaprima2Click(Sender: TObject);
begin
  App_Jan.MostraFormMateriaPrima();
end;

procedure TFmGraGruEIts.ReopenGraGruX();
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle, CONCAT(gg1.Nome, " ",  ',
  '  IF(gti.PrintTam=1, gti.Nome, ""), " ",  ',
  '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR, ',
  'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM,  ',
  'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR,  ',
  'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  'LEFT JOIN pqcli      pqc ON pqc.PQ=gg1.Nivel1 ',
  'WHERE gg1.PrdGrupTip > -3 ',
  'AND NOT (pqc.PQ IS NULL) ',
  'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle ',
  '']);
  //Geral.MB_SQL(Self, QrGraGrux);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle, CONCAT(gg1.Nome, " ",  ',
  '  IF(gti.PrintTam=1, gti.Nome, ""), " ",  ',
  '  IF(gcc.PrintCor=1, gcc.Nome, "")) NO_PRD_TAM_COR, ',
  'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
  'IF(gti.PrintTam=1, gti.Nome, "") NO_TAM,  ',
  'IF(gcc.PrintCor=1, gcc.Nome, "") NO_COR,  ',
  'ggc.GraCorCad, ggx.GraGruC, ggx.GraGru1, ggx.GraTamI ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  //'LEFT JOIN pqcli      pqc ON pqc.PQ=gg1.Nivel1 ',
  'WHERE gg1.PrdGrupTip > -3 ',
  //'AND NOT (pqc.PQ IS NULL) ',
  'AND ggx.Controle > 0 ',
  'ORDER BY NO_PRD, NO_TAM, NO_COR, ggx.Controle ',
  '']);
end;

procedure TFmGraGruEIts.SbEmbalagemClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEmbalagens, FmEmbalagens, afmoNegarComAviso) then
  begin
    FmEmbalagens.ShowModal;
    FmEmbalagens.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrEmbalagens.Close;
      UnDmkDAC_PF.AbreQuery(QrEmbalagens, Dmod.MyDB);
      EdEmbalagem.ValueVariant := VAR_CADASTRO;
      CBEmbalagem.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmGraGruEIts.SbForneceClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrFornece.Close;
      UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
      EdFornece.ValueVariant := VAR_CADASTRO;
      CBFornece.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmGraGruEIts.SbGraGruXClick(Sender: TObject);
begin
(* BlueDerm
  MyObjects.MostraPopOnControlXY(PMGraGruX, SbGraGruX, 0, 0);
*)
  CBGraGruX.SetFocus;
  if CO_DMKID_APP = 2 then
    AppPF.MostraPopupDeCadsatroEspecificoDeGraGruXY(FmGraGruEIts, SbGraGruX,
      PMGraGruX)
  else
    AppPF.MostraPopupDeCadsatroEspecificoDeGraGruXY(FmGraGruEIts, SbGraGruX,
      PMProduto);

end;

procedure TFmGraGruEIts.SpeedButton1Click(Sender: TObject);
begin
  if VAR_CADASTRO2 <> 0 then
    EdGraGruX.ValueVariant := VAR_CADASTRO2;
end;

procedure TFmGraGruEIts.Usoeconsumo1Click(Sender: TObject);
begin
  DmProd.CadastraSemGrade(-2, EdxProd.Text, F_prod_cEAN, F_prod_NCM, F_prod_uCom,
  F_InfAdProd, F_ICMS_CST_A, F_ICMS_CST_B, F_IPI_CST, F_PIS_CST, F_COFINS_CST,
  F_prod_EXTIPI, F_IPI_cEnq, QrGraGruX, EdGraGruX, CBGraGruX);
end;

end.

