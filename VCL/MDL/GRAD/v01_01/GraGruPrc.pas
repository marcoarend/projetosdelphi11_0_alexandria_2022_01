unit GraGruPrc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, ComCtrls, DBGrids, dmkDBGrid,
  DB, mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmGraGruPrc = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    GradeX: TStringGrid;
    StaticText1: TStaticText;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    dmkDBGrid1: TdmkDBGrid;
    GradeP: TStringGrid;
    QrGraCusPrcU: TmySQLQuery;
    QrGraCusPrcUCodigo: TIntegerField;
    QrGraCusPrcUCodUsu: TIntegerField;
    QrGraCusPrcUNome: TWideStringField;
    QrGraCusPrcUCusPrc: TWideStringField;
    QrGraCusPrcUNomeTC: TWideStringField;
    QrGraCusPrcUNOMEMOEDA: TWideStringField;
    QrGraCusPrcUSIGLAMOEDA: TWideStringField;
    DsGraCusPrcU: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure GradeAClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure GradePClick(Sender: TObject);
    procedure GradePDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGraCusPrcUAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruPrc: TFmGraGruPrc;

implementation

uses UnMyObjects, GraGruN, MyVCLSkin, ModProd;

{$R *.DFM}

procedure TFmGraGruPrc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruPrc.FormActivate(Sender: TObject);
begin
  if FmGraGruN.FEntiPrecos = 0 then
  begin
    Geral.MB_Erro('Cliente interno n�o definido!');
    Close;
    Exit;
  end;
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruPrc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DmProd.ReopenGraCusPrcU(QrGraCusPrcU, 0);
  QrGraCusPrcU.Locate('Codigo', FmGraGruN.QrGraCusPrcUCodigo.Value, []);
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeP.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  PageControl1.ActivePageIndex := 0;
  DmProd.ConfigGrades1(
    FmGraGruN.QrGraGru1GraTamCad.Value,
    FmGraGruN.QrGraGru1Nivel1.Value,
    QrGraCusPrcUCodigo.Value, FmGraGruN.FEntiPrecos,
    GradeA, GradeX, GradeC, GradeP, nil, nil);
end;

procedure TFmGraGruPrc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruPrc.GradeAClick(Sender: TObject);
begin
  if (GradeA.Col = 0) or (GradeA.Row = 0) then
    FmGraGruN.StatusVariosItensCorTam(GradeA, GradeC, GradeX)
  else
    FmGraGruN.StatusItemCorTam(GradeA, GradeC, GradeX);

  //

  DmProd.ConfigGrades1(FmGraGruN.QrGraGru1GraTamCad.Value,
    FmGraGruN.QrGraGru1Nivel1.Value, QrGraCusPrcUCodigo.Value, FmGraGruN.FEntiPrecos,
    FmGraGruN.GradeA, FmGraGruN.GradeX, FmGraGruN.GradeC, nil, nil, nil);
end;

procedure TFmGraGruPrc.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel1.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          Panel1.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmGraGruPrc.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else
    Cor := clWindow;  
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGruPrc.GradePClick(Sender: TObject);
begin
  if (GradeP.Col = 0) or (GradeP.Row = 0) then
    FmGraGruN.PrecoVariosItensCorTam(GradeP, GradeA, GradeC, GradeX, QrGraCusPrcU)
  else
    FmGraGruN.PrecoItemCorTam(GradeP, GradeC, GradeX, QrGraCusPrcU);
end;

procedure TFmGraGruPrc.GradePDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else if GradeC.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taLeftJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taRightJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGruPrc.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel1.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeX, Rect, clBlack,
      Cor, taLeftJustify,
      GradeX.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeX, Rect, clBlack,
      Cor, taCenter,
      GradeX.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGruPrc.QrGraCusPrcUAfterScroll(DataSet: TDataSet);
begin
  DmProd.AtualizaGradesPrecos1(FmGraGruN.QrGraGru1Nivel1.Value,
    QrGraCusPrcUCodigo.Value, FmGraGruN.FEntiPrecos, GradeP, tpGraCusPrc);
end;

end.
