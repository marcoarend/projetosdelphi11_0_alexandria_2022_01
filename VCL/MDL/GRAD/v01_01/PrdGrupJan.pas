unit PrdGrupJan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, DB, (*&&ABSMain,*) Grids,
  DBGrids, dmkImage, UnDmkEnums, mySQLDbTables, dmkDBGridZTO;

type
  THackDBGrid = class(TDBGrid);
  TFmPrdGrupJan = class(TForm)
    Panel1: TPanel;
    DataSource1: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Query: TMySQLQuery;
    QueryOrdem: TIntegerField;
    QueryAdded: TIntegerField;
    QueryJanela: TWideStringField;
    QueryTexto: TWideStringField;
    DBGItsToSel: TdmkDBGridZTO;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGItsToSelDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGItsToSelCellClick(Column: TColumn);
    procedure DBGItsToSelColEnter(Sender: TObject);
    procedure DBGItsToSelColExit(Sender: TObject);
    procedure QueryAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmPrdGrupJan: TFmPrdGrupJan;

implementation

uses UnMyObjects, (*MyVCLSkin,*) ModuleGeral;

{$R *.DFM}

procedure TFmPrdGrupJan.BtOKClick(Sender: TObject);
begin
  FCadastrar := True;
  Close;
end;

procedure TFmPrdGrupJan.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdGrupJan.DBGItsToSelCellClick(Column: TColumn);
begin
(*&&
  if (DBGrid1.SelectedField.Name = 'QueryAtivo') then
  begin
    Query.Edit;
    if QueryAtivo.Value = 0 then
      QueryAtivo.Value := 1
    else
      QueryAtivo.Value := 0;
    Query.Post;
  end;
*)
end;

procedure TFmPrdGrupJan.DBGItsToSelColEnter(Sender: TObject);
begin
(*&&
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Ordem' then
    DBGrid1.Options := DBGrid1.Options + [dgEditing] else
    DBGrid1.Options := DBGrid1.Options - [dgEditing];
*)
end;

procedure TFmPrdGrupJan.DBGItsToSelColExit(Sender: TObject);
begin
(*&&
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
*)
end;

procedure TFmPrdGrupJan.DBGItsToSelDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
(*&&
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QueryAtivo.Value);
*)
end;

procedure TFmPrdGrupJan.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrdGrupJan.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Query.Database := DModG.MyPID_DB;
end;

procedure TFmPrdGrupJan.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdGrupJan.QueryAfterScroll(DataSet: TDataSet);
begin
(*&
  if Query.State = dsInsert then
    Query.Prior;
*)
end;

(*&&
object Query: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  AfterScroll = QueryAfterScroll
  RequestLive = True
  Left = 148
  Top = 136
  object QueryAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
  object QueryOrdem: TIntegerField
    FieldName = 'Ordem'
  end
  object QueryAdded: TIntegerField
    FieldName = 'Added'
  end
  object QueryJanela: TWideStringField
    FieldName = 'Janela'
    Size = 30
  end
  object QueryTexto: TWideStringField
    FieldName = 'Texto'
    Size = 255
  end
end
*)

end.
