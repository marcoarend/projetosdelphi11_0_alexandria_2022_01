unit GraGruAtr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMySQLCuringa, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu,
  dmkLabel, Mask, dmkDBEdit, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmGraGruAtr = class(TForm)
    Panel1: TPanel;
    EdGraAtrCad: TdmkEditCB;
    CBGraAtrCad: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrGraAtrCad: TmySQLQuery;
    DsGraAtrCad: TDataSource;
    QrGraAtrCadCodigo: TIntegerField;
    QrGraAtrCadCodUsu: TIntegerField;
    QrGraAtrCadNome: TWideStringField;
    CBGraAtrIts: TdmkDBLookupComboBox;
    EdGraAtrIts: TdmkEditCB;
    Label2: TLabel;
    QrGraAtrIts: TmySQLQuery;
    DsGraAtrIts: TDataSource;
    QrGraAtrItsControle: TIntegerField;
    QrGraAtrItsCodUsu: TIntegerField;
    QrGraAtrItsNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    dmkValUsu2: TdmkValUsu;
    DBEdit1: TdmkDBEdit;
    Label3: TLabel;
    EdControle: TdmkEdit;
    Label4: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraAtrCadChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraAtrCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraAtrCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraAtrItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraAtrItsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraAtrIts(Controle: Integer);
    procedure CadastraAtrCad(Qual: Integer);
  public
    { Public declarations }
  end;

  var
  FmGraGruAtr: TFmGraGruAtr;

implementation

uses UnMyObjects, Module, GraGruN, UMySQLModule, MyDBCheck, GraAtrCad,
UnInternalConsts, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruAtr.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('gragruatr', 'Controle', ImgTipo.SQLType,
    FmGraGruN.QrGraAtrICTRL_ATR.Value);
  EdControle.ValueVariant := Controle;
  if UMyMod.ExecSQLInsUpdFm(FmGraGruAtr, ImgTipo.SQLType, 'gragruatr', Controle,
  Dmod.QrUpd) then
  begin
    FmGraGruN.ReopenGraAtrIts(Controle);
    Close;
  end;
end;

procedure TFmGraGruAtr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruAtr.CBGraAtrCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
  begin
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraAtrCad', Dmod.MyDB,
    ''(*Extra*), EdGraAtrCad, CBGraAtrCad, dmktfInteger);
    ReopenGraAtrIts(0);
  end;
end;

procedure TFmGraGruAtr.CBGraAtrItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraAtrIts', Dmod.MyDB,
    'AND Codigo = ' + FormatFloat('0', Geral.IMV(EdGraAtrCad.Text)),
    EdGraAtrIts, CBGraAtrIts, dmktfInteger);
end;

procedure TFmGraGruAtr.EdGraAtrCadChange(Sender: TObject);
begin
  ReopenGraAtrIts(0);
end;

procedure TFmGraGruAtr.EdGraAtrCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
  begin
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraAtrCad', Dmod.MyDB,
    ''(*Extra*), EdGraAtrCad, CBGraAtrCad, dmktfInteger);
    ReopenGraAtrIts(0);
  end;
end;

procedure TFmGraGruAtr.EdGraAtrItsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'GraAtrIts', Dmod.MyDB,
    'AND Codigo = ' + FormatFloat('0', Geral.IMV(EdGraAtrCad.Text)),
    EdGraAtrIts, CBGraAtrIts, dmktfInteger);
end;

procedure TFmGraGruAtr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruAtr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraAtrCad, Dmod.MyDB);
end;

procedure TFmGraGruAtr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruAtr.ReopenGraAtrIts(Controle: Integer);
begin
  QrGraAtrIts.Close;
  QrGraAtrIts.Params[0].AsInteger := QrGraAtrCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraAtrIts, Dmod.MyDB);
  if Controle <> 0 then
    QrGraAtrIts.Locate('Controle', Controle, []);
end;

procedure TFmGraGruAtr.SpeedButton1Click(Sender: TObject);
begin
  CadastraAtrCad(1);
end;

procedure TFmGraGruAtr.SpeedButton2Click(Sender: TObject);
begin
  CadastraAtrCad(2);
end;

procedure TFmGraGruAtr.CadastraAtrCad(Qual: Integer);
begin
  VAR_CADASTRO := 0;
  VAR_CAD_ITEM := 0;
  if DBCheck.CriaFm(TFmGraAtrCad, FmGraAtrCad, afmoNegarComAviso) then
  begin
    FmGraAtrCad.ShowModal;
    FmGraAtrCad.Destroy;
    //
    if Qual = 1 then
    begin
      if VAR_CADASTRO <> 0 then
      begin
        QrGraAtrCad.Close;
        UnDmkDAC_PF.AbreQuery(QrGraAtrCad, Dmod.MyDB);
        //
        EdGraAtrCad.ValueVariant := VAR_CADASTRO;
        CBGraAtrCad.KeyValue     := VAR_CADASTRO;
      end;
    end else begin
      if VAR_CAD_ITEM <> 0 then
      begin
        QrGraAtrIts.Close;
        UnDmkDAC_PF.AbreQuery(QrGraAtrIts, Dmod.MyDB);
        //
        EdGraAtrIts.ValueVariant := VAR_CAD_ITEM;
        CBGraAtrIts.KeyValue     := VAR_CAD_ITEM;
      end;
    end;
  end;
end;

end.
