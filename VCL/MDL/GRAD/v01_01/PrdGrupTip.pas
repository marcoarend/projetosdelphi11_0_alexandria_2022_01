// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria
unit PrdGrupTip;
interface

uses
{$IFDEF DELPHI12_UP}
 System.Generics.Collections,
{$ENDIF}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGridZTO,
  Menus, (*&&ABSMain,*) ComCtrls, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkCheckBox, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnMyLinguas, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmPrdGrupTip = class(TForm)
    PainelDados: TPanel;
    DsPrdGrupTip: TDataSource;
    QrPrdGrupTip: TmySQLQuery;
    PainelEdita: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipMadeBy: TSmallintField;
    QrPrdGrupTipFracio: TSmallintField;
    QrPrdGrupTipTipPrd: TSmallintField;
    QrPrdGrupTipNivCad: TSmallintField;
    QrPrdGrupTipNOME_MADEBY: TWideStringField;
    QrPrdGrupTipNOME_FRACIO: TWideStringField;
    QrPrdGrupTipNOME_TIPPRD: TWideStringField;
    QrPrdGrupTipNome: TWideStringField;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup3: TDBRadioGroup;
    DBRadioGroup4: TDBRadioGroup;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    QrPrdGrupTipFaixaIni: TIntegerField;
    QrPrdGrupTipFaixaFim: TIntegerField;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBRadioGroup5: TDBRadioGroup;
    QrPrdGrupTipGradeado: TSmallintField;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    GroupBox4: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DsPrdGrupAtr: TDataSource;
    QrPrdGrupJan: TmySQLQuery;
    DsPrdGrupJan: TDataSource;
    QrPrdGrupAtr: TmySQLQuery;
    QrPrdGrupAtrControle: TIntegerField;
    QrPrdGrupAtrGraAtrCad: TIntegerField;
    QrPrdGrupAtrCodUsu: TIntegerField;
    QrPrdGrupAtrNome: TWideStringField;
    PMTipo: TPopupMenu;
    Incluinovotipodegrupodeprodutos1: TMenuItem;
    Alteratipodegrupodeprodutosatual1: TMenuItem;
    Excluitipodegrupodeprodutosatual1: TMenuItem;
    QrPrdGrupJanOrdem: TIntegerField;
    QrPrdGrupJanControle: TIntegerField;
    QrPrdGrupJanJanela: TWideStringField;
    QrPrdGrupJanDescricao: TWideStringField;
    QrGraAtrCad: TmySQLQuery;
    QrGraAtrCadCodigo: TIntegerField;
    QrGraAtrCadCodUsu: TIntegerField;
    QrGraAtrCadNome: TWideStringField;
    QrPrdGrupCen: TmySQLQuery;
    DsPrdGrupCen: TDataSource;
    QrEstqCen: TmySQLQuery;
    QrPrdGrupCenCodUsu: TIntegerField;
    QrPrdGrupCenEstqCen: TIntegerField;
    QrPrdGrupCenNOMEESTQCEN: TWideStringField;
    QrEstqCenCodigo: TIntegerField;
    QrEstqCenCodUsu: TIntegerField;
    QrEstqCenNome: TWideStringField;
    QrEstqCenAtivo: TSmallintField;
    DBRadioGroup6: TDBRadioGroup;
    QrPrdGrupTipCustomizav: TSmallintField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnGrids: TPanel;
    Splitter1: TSplitter;
    Panel4: TPanel;
    DBGAtributos: TdmkDBGridZTO;
    DBGCentros: TdmkDBGridZTO;
    Panel6: TPanel;
    DBGJanelas: TdmkDBGridZTO;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    Panel8: TPanel;
    DBGGraGru9: TdmkDBGridZTO;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGGraGru1: TdmkDBGridZTO;
    Panel11: TPanel;
    QrGraGru9: TmySQLQuery;
    DsGraGru9: TDataSource;
    Panel12: TPanel;
    BtMove9: TBitBtn;
    QrGraGru9Nivel1: TIntegerField;
    QrGraGru9Codusu: TIntegerField;
    QrGraGru9Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    QrTipo9: TmySQLQuery;
    DsTipo9: TDataSource;
    QrTipo9Codigo: TIntegerField;
    QrTipo9CodUsu: TIntegerField;
    QrTipo9Nome: TWideStringField;
    Panel13: TPanel;
    BtMove1: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    QrPrdGrupTipImpedeCad: TSmallintField;
    PMJanelas: TPopupMenu;
    Incluijanelas1: TMenuItem;
    Excluijanelas1: TMenuItem;
    PMAtributos: TPopupMenu;
    PMCentros: TPopupMenu;
    IncluiAtributos1: TMenuItem;
    Excluiatributos1: TMenuItem;
    Incluicentro1: TMenuItem;
    Excluicentro1: TMenuItem;
    QrEstqCenPrdgrupTip: TIntegerField;
    QrPrdGrupCenPrdGrupTip: TIntegerField;
    QrPrdGrupTipLstPrcFisc: TSmallintField;
    DBRadioGroup7: TDBRadioGroup;
    QrPrdGrupTipPerComissF: TFloatField;
    QrPrdGrupTipPerComissR: TFloatField;
    Label24: TLabel;
    Label25: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    TabSheet3: TTabSheet;
    Panel14: TPanel;
    Label26: TLabel;
    Panel15: TPanel;
    Label27: TLabel;
    QrGraComiss3: TmySQLQuery;
    DsGraComiss3: TDataSource;
    DBGrid1: TDBGrid;
    QrGraComiss2: TmySQLQuery;
    DsGraComiss2: TDataSource;
    QrGraComiss3Nivel3: TIntegerField;
    QrGraComiss3CodUsu: TIntegerField;
    QrGraComiss3Nome: TWideStringField;
    QrGraComiss3PerComissF: TFloatField;
    QrGraComiss3PerComissR: TFloatField;
    DBGrid2: TDBGrid;
    PMComissao: TPopupMenu;
    ComissoNivel31: TMenuItem;
    ComissoNivel21: TMenuItem;
    QrGraComiss2Nivel2: TIntegerField;
    QrGraComiss2CodUsu: TIntegerField;
    QrGraComiss2Nome: TWideStringField;
    QrGraComiss2PerComissF: TFloatField;
    QrGraComiss2PerComissR: TFloatField;
    QrGraComiss2Nivel3: TIntegerField;
    QrPrdGrupTipTipo_Item: TSmallintField;
    QrPrdGrupTipTipo_Item_TXT: TWideStringField;
    Label28: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    TabSheet4: TTabSheet;
    Panel16: TPanel;
    Label29: TLabel;
    DBGrid3: TDBGrid;
    Panel17: TPanel;
    Label30: TLabel;
    QrGraPlaCta3: TmySQLQuery;
    QrGraPlaCta2: TmySQLQuery;
    DsGraPlaCta2: TDataSource;
    DsGraPlaCta3: TDataSource;
    QrGraPlaCta2PrdGrupTip: TIntegerField;
    QrGraPlaCta2Nivel2: TIntegerField;
    QrGraPlaCta2Nivel3: TIntegerField;
    QrGraPlaCta2CodUsu: TIntegerField;
    QrGraPlaCta2Nome: TWideStringField;
    QrGraPlaCta2Genero: TIntegerField;
    QrGraPlaCta3Nivel3: TIntegerField;
    QrGraPlaCta3CodUsu: TIntegerField;
    QrGraPlaCta3Nome: TWideStringField;
    QrGraPlaCta3Genero: TIntegerField;
    DBGrid4: TDBGrid;
    PMCTB: TPopupMenu;
    CTBNivel21: TMenuItem;
    CTBNivel31: TMenuItem;
    QrPrdGrupTipGenero: TIntegerField;
    Label31: TLabel;
    DBEdit11: TDBEdit;
    Label35: TLabel;
    DBEdit12: TDBEdit;
    Label36: TLabel;
    DBEdit13: TDBEdit;
    QrPrdGrupTipTitNiv4: TWideStringField;
    QrPrdGrupTipTitNiv5: TWideStringField;
    QrTemNiv: TmySQLQuery;
    QrTemNivITENS: TLargeintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    PainelEdit: TPanel;
    GroupBox1: TGroupBox;
    Panel18: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label6: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label208: TLabel;
    Label32: TLabel;
    RGTipPrd: TdmkRadioGroup;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdCodUsu: TdmkEdit;
    RGMadeBy: TdmkRadioGroup;
    RGFracio: TdmkRadioGroup;
    RGNivCad: TdmkRadioGroup;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    RGGradeado: TdmkRadioGroup;
    GroupBox5: TGroupBox;
    LaTitNiv5: TLabel;
    LaTitNiv4: TLabel;
    LaTitNiv3: TLabel;
    LaTitNiv2: TLabel;
    LaTitNiv1: TLabel;
    EdTitNiv3: TdmkEdit;
    EdTitNiv4: TdmkEdit;
    EdTitNiv5: TdmkEdit;
    EdTitNiv2: TdmkEdit;
    EdTitNiv1: TdmkEdit;
    RGCustomizav: TdmkRadioGroup;
    CkImpedeCad: TdmkCheckBox;
    RGLstPrcFisc: TdmkRadioGroup;
    EdPerComissF: TdmkEdit;
    EdPerComissR: TdmkEdit;
    EdTipo_Item: TdmkEdit;
    EdTipo_Item_TXT: TdmkEdit;
    EdGenero: TdmkEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtTipo: TBitBtn;
    BtJanelas: TBitBtn;
    BtAtributos: TBitBtn;
    BtCentros: TBitBtn;
    BtComissao: TBitBtn;
    BtCTB: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel19: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaGraTabApp: TLabel;
    EdGraTabApp: TdmkEditCB;
    CBGraTabApp: TdmkDBLookupComboBox;
    QrPrdGrupTipGraTabApp: TIntegerField;
    QrGraTabApp: TmySQLQuery;
    QrGraTabAppCodigo: TIntegerField;
    QrGraTabAppNome: TWideStringField;
    QrGraTabAppTabela: TWideStringField;
    DsGraTabApp: TDataSource;
    QrPrdGrupTipGraTabApp_TXT: TWideStringField;
    LaDBGraTabApp: TLabel;
    EdDBGraTabApp: TDBEdit;
    N1: TMenuItem;
    Desativarativartipodegrupodeprodutoatual1: TMenuItem;
    N2: TMenuItem;
    DBCheckBox2: TDBCheckBox;
    QrPrdGrupTipAtivo: TSmallintField;
    EdTipo9: TdmkEdit;
    DBEdit14: TDBEdit;
    SpeedButton5: TSpeedButton;
    DBRadioGroup8: TDBRadioGroup;
    RGUsaCores: TdmkRadioGroup;
    QrPrdGrupTipUsaCores: TSmallintField;
    QrCtbCadGru: TMySQLQuery;
    QrCtbCadGruCodigo: TIntegerField;
    QrCtbCadGruNome: TWideStringField;
    DsCtbCadGru: TDataSource;
    Label12: TLabel;
    EdCtbCadGru: TdmkEditCB;
    CBCtbCadGru: TdmkDBLookupComboBox;
    SbCtbCadGru: TSpeedButton;
    Label13: TLabel;
    EdCtbPlaCta: TdmkEditCB;
    CBCtbPlaCta: TdmkDBLookupComboBox;
    SbCtbPlaCta: TSpeedButton;
    Label14: TLabel;
    Label33: TLabel;
    QrPrdGrupTipNO_CtbPlaCta: TWideStringField;
    QrPrdGrupTipNO_CtbCadGru: TWideStringField;
    QrPrdGrupTipCtbCadGru: TIntegerField;
    QrPrdGrupTipCtbPlaCta: TIntegerField;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    QrPrdGrupTipIND_MOV: TSmallintField;
    DBRadioGroup9: TDBRadioGroup;
    RGIND_MOV: TdmkRadioGroup;
    QrPlaAllCad: TMySQLQuery;
    QrPlaAllCadCodigo: TIntegerField;
    QrPlaAllCadNome: TWideStringField;
    QrPlaAllCadNivel: TSmallintField;
    QrPlaAllCadAnaliSinte: TIntegerField;
    QrPlaAllCadCredito: TSmallintField;
    QrPlaAllCadDebito: TSmallintField;
    QrPlaAllCadOrdens: TWideStringField;
    DsPlaAllCad: TDataSource;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrdGrupTipAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrdGrupTipBeforeOpen(DataSet: TDataSet);
    procedure BtTipoClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrPrdGrupTipBeforeClose(DataSet: TDataSet);
    procedure QrPrdGrupTipAfterScroll(DataSet: TDataSet);
    procedure Incluinovotipodegrupodeprodutos1Click(Sender: TObject);
    procedure Alteratipodegrupodeprodutosatual1Click(Sender: TObject);
    procedure BtJanelasClick(Sender: TObject);
    procedure DBGGraGru9DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtMove9Click(Sender: TObject);
    procedure DBGGraGru1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtMove1Click(Sender: TObject);
    procedure PMTipoPopup(Sender: TObject);
    procedure Incluijanelas1Click(Sender: TObject);
    procedure Excluijanelas1Click(Sender: TObject);
    procedure IncluiAtributos1Click(Sender: TObject);
    procedure Excluiatributos1Click(Sender: TObject);
    procedure Incluicentro1Click(Sender: TObject);
    procedure Excluicentro1Click(Sender: TObject);
    procedure BtAtributosClick(Sender: TObject);
    procedure BtCentrosClick(Sender: TObject);
    procedure PMJanelasPopup(Sender: TObject);
    procedure PMAtributosPopup(Sender: TObject);
    procedure PMCentrosPopup(Sender: TObject);
    procedure Excluitipodegrupodeprodutosatual1Click(Sender: TObject);
    procedure QrGraComiss3AfterScroll(DataSet: TDataSet);
    procedure QrGraComiss3BeforeClose(DataSet: TDataSet);
    procedure PMComissaoPopup(Sender: TObject);
    procedure BtComissaoClick(Sender: TObject);
    procedure ComissoNivel31Click(Sender: TObject);
    procedure ComissoNivel21Click(Sender: TObject);
    procedure EdTipo_ItemChange(Sender: TObject);
    procedure EdTipo_ItemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTipo_Item_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPrdGrupTipCalcFields(DataSet: TDataSet);
    procedure PMCTBPopup(Sender: TObject);
    procedure CTBNivel21Click(Sender: TObject);
    procedure CTBNivel31Click(Sender: TObject);
    procedure BtCTBClick(Sender: TObject);
    procedure QrGraPlaCta3AfterScroll(DataSet: TDataSet);
    procedure QrGraPlaCta3BeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure RGNivCadClick(Sender: TObject);
    procedure Desativarativartipodegrupodeprodutoatual1Click(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure EdTipo9Redefinido(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SbCtbCadGruClick(Sender: TObject);
    procedure SbCtbPlaCtaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenPrdGrupAtr(Controle: Integer);
    procedure ReopenPrdGrupJan(Controle: Integer);
    procedure ReopenPrdGrupCen(EstqCen: Integer);
    procedure ReopenGraGru1(Nivel1, Nivel9: Integer(*; ReopenTipo9: Boolean = True*));
    procedure HabilitaBtMove1();
    procedure ConfiguraNiveis(Nivel: Integer; SQLType: TSQLType);
    procedure ReopenGraTabApp();
    procedure MostraEdicaoEspecifico();
    procedure ReopenTipo9();
    //
    function  ImpedePeloNivel(): Boolean;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenGraComiss2(CodUsu: Integer);
    procedure ReopenGraComiss3(CodUsu: Integer);
    procedure ReopenGraPlaCta2(CodUsu: Integer);
    procedure ReopenGraPlaCta3(CodUsu: Integer);
    //
    procedure SelecionaTipo9();
  end;

var
  FmPrdGrupTip: TFmPrdGrupTip;
const
  FFormatFloat = '00000';

implementation

uses
{$IfNDef NO_FINANCEIRO} UnFinanceiro, UnFinanceiroJan, UnFinance_Tabs, {$EndIf}
UnMyObjects, Module, (*ModProd,*) MyDBCheck, PrdGrupJan, MyListas,
  (*CashTabs,*) ModuleGeral, (*&&UnDmkABS_PF,*)
  PrdGrupAtr, PrdGrupCen, GraComiss, GraPlaCta, ModuleFin;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrdGrupTip.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrdGrupTip.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrdGrupTipCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrdGrupTip.DefParams;
begin
  VAR_GOTOTABELA := 'PrdGrupTip';
  VAR_GOTOMYSQLTABLE := QrPrdGrupTip;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, ');
  VAR_SQLx.Add('pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim, ');
  VAR_SQLx.Add('pgt.TitNiv1, pgt.TitNiv2, pgt.TitNiv3, ');
  VAR_SQLx.Add('pgt.TitNiv4, pgt.TitNiv5, Customizav, ');
  VAR_SQLx.Add('ELT(pgt.MadeBy, "Pr�prio","Terceiros") NOME_MADEBY,');
  VAR_SQLx.Add('ELT(pgt.Fracio+1, "N�o", "Sim") NOME_FRACIO, pgt.Gradeado, pgt.UsaCores,');
  VAR_SQLx.Add('ELT(pgt.TipPrd, "Produto", "Mat�ria-prima", "Outros") ');
  VAR_SQLx.Add('NOME_TIPPRD, pgt.ImpedeCad, pgt.LstPrcFisc, ');
  VAR_SQLx.Add('pgt.PerComissF, pgt.PerComissR, pgt.Tipo_Item, pgt.Genero, ');
  VAR_SQLx.Add('app.Nome GraTabApp_TXT, pgt.GraTabApp, pgt.Ativo,');
  VAR_SQLx.Add('pgt.CtbCadGru, pgt.CtbPlaCta, pgt.IND_MOV, ');
  VAR_SQLx.Add('cta.Nome NO_CtbPlaCta, ccg.Nome NO_CtbCadGru ');
  VAR_SQLx.Add('FROM prdgruptip pgt');
  VAR_SQLx.Add('LEFT JOIN gratabapp app ON pgt.GraTabApp = app.Codigo ');
  VAR_SQLx.Add('LEFT JOIN ctbcadgru ccg ON ccg.Codigo = pgt.CtbCadGru');
    {$IfDef UsaFinanceiro3}
    VAR_SQLx.Add('LEFT JOIN plaallcad cta ON cta.Codigo = pgt.CtbPlaCta');
    {$Else}
    VAR_SQLx.Add('LEFT JOIN contas    cta ON cta.Codigo = pgt.CtbPlaCta');
    {$EndIf}
  //VAR_SQLx.Add('WHERE pgt.Codigo <> 0');
  //
  VAR_SQL1.Add('WHERE pgt.Codigo=:P0');
  //
  VAR_SQL2.Add('WHERE pgt.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE pgt.Nome Like :P0');
  //
end;

procedure TFmPrdGrupTip.Desativarativartipodegrupodeprodutoatual1Click(
  Sender: TObject);
var
  Codigo, Ativo: Integer;
  Txt: String;
begin
  Codigo := QrPrdGrupTipCodigo.Value;
  Ativo  := QrPrdGrupTipAtivo.Value;
  //
  if Ativo = 1 then
  begin
    Txt   := 'desativar';
    Ativo := 0;
  end else
  begin
    Txt   := 'ativar';
    Ativo := 1;
  end;
  if Geral.MB_Pergunta('Deseja ' + Txt +
    ' o tipo de grupo de produto selecionado?') = ID_YES then
  begin
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE prdgruptip SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Codigo=' + Geral.FF0(Codigo),
      ''])
    then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPrdGrupTip.DBGGraGru1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  HabilitaBtMove1();
end;

procedure TFmPrdGrupTip.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'prdgruptip', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmPrdGrupTip.EdTipo9Redefinido(Sender: TObject);
begin
   ReopenTipo9();
  //HabilitaBtMove1();
end;

procedure TFmPrdGrupTip.EdTipo_ItemChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdTipo_Item_TXT.Text := UFinanceiro.SPED_Tipo_Item_Get(EdTipo_Item.ValueVariant);
{$EndIf}
end;

procedure TFmPrdGrupTip.EdTipo_ItemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdTipo_Item.Text := UFinanceiro.ListaDeSPED_Tipo_Item();
{$EndIf}
end;

procedure TFmPrdGrupTip.EdTipo_Item_TXTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdTipo_Item.Text := UFinanceiro.ListaDeSPED_Tipo_Item();
{$EndIf}
end;

procedure TFmPrdGrupTip.Excluiatributos1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPrdGrupAtr, TDBGrid(DBGAtributos),
  'PrdGrupAtr', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmPrdGrupTip.Excluicentro1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPrdGrupCen, TDBGrid(DBGCentros),
  'PrdGrupCen', ['PrdGrupTip', 'EstqCen'], ['PrdGrupTip', 'EstqCen'], istPergunta, '');
end;

procedure TFmPrdGrupTip.Excluijanelas1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPrdGrupJan, TDBGrid(DBGJanelas),
    'PrdGrupJan', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmPrdGrupTip.Excluitipodegrupodeprodutosatual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do tipo de grupos de produtos "' +
    QrPrdGrupTipNome.Value + '"?') = ID_YES then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT Nivel1 ',
      'FROM gragru1 ',
      'WHERE PrdGrupTip=' + Geral.FF0(QrPrdGrupTipCodigo.Value),
      '']);
    if DModG.QrAux.RecordCount > 0 then
    begin
      DModG.QrAux.Close;
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Motivo: Existem produtos cadastrados para este tipo de grupo de produtos!' +
        sLineBreak + 'Exclua-os e tente novamente.');
      Exit;
    end;
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrPrdGrupTip, 'PrdGrupTip', ['Codigo'], ['Codigo'], True);
  end;
end;

procedure TFmPrdGrupTip.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPrdGrupTip.MostraEdicaoEspecifico;
begin
  case CO_DMKID_APP of
    24: //Bugstrol
    begin
      LaGraTabApp.Visible := True;
      EdGraTabApp.Visible := True;
      CBGraTabApp.Visible := True;
      //
      LaDBGraTabApp.Visible := True;
      EdDBGraTabApp.Visible := True;
    end;
    else
    begin
      LaGraTabApp.Visible := False;
      EdGraTabApp.Visible := False;
      CBGraTabApp.Visible := False;
      //
      LaDBGraTabApp.Visible := False;
      EdDBGraTabApp.Visible := False;
    end;
  end;
end;

procedure TFmPrdGrupTip.PMAtributosPopup(Sender: TObject);
var
  Habilita1, Habilita2: Boolean;
begin
  Habilita1 := (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTip.RecordCount > 0);
  Habilita2 := (QrPrdGrupAtr.State <> dsInactive) and (QrPrdGrupAtr.RecordCount > 0);
  //
  IncluiAtributos1.Enabled := Habilita1;
  Excluiatributos1.Enabled := Habilita1 and Habilita2;
end;

procedure TFmPrdGrupTip.PMCentrosPopup(Sender: TObject);
var
  Habilita1, Habilita2: Boolean;
begin
  Habilita1 := (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTip.RecordCount > 0);
  Habilita2 := (QrPrdGrupCen.State <> dsInactive) and (QrPrdGrupCen.RecordCount > 0);
  //
  Incluicentro1.Enabled := Habilita1;
  Excluicentro1.Enabled := Habilita1 and Habilita2;
end;

procedure TFmPrdGrupTip.PMComissaoPopup(Sender: TObject);
var
  Habilita1, Habilita2, Habilita3: Boolean;
begin
  Habilita1 := (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTip.RecordCount > 0);
  Habilita2 := (QrGraComiss3.State <> dsInactive) and (QrGraComiss3.RecordCount > 0);
  Habilita3 := (QrGraComiss2.State <> dsInactive) and (QrGraComiss2.RecordCount > 0);
  //
  ComissoNivel31.Enabled := Habilita1 and Habilita2;
  ComissoNivel21.Enabled := Habilita1 and Habilita3;
end;

procedure TFmPrdGrupTip.PMCTBPopup(Sender: TObject);
var
  Habilita1, Habilita2, Habilita3: Boolean;
begin
  Habilita1 := (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTip.RecordCount > 0);
  Habilita2 := (QrGraComiss3.State <> dsInactive) and (QrGraComiss3.RecordCount > 0);
  Habilita3 := (QrGraComiss2.State <> dsInactive) and (QrGraComiss2.RecordCount > 0);
  //
  CTBNivel31.Enabled := Habilita1 and Habilita2;
  CTBNivel21.Enabled := Habilita1 and Habilita3;
end;

procedure TFmPrdGrupTip.PMJanelasPopup(Sender: TObject);
var
  Habilita1, Habilita2: Boolean;
begin
  Habilita1 := (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTip.RecordCount > 0);
  Habilita2 := (QrPrdGrupJan.State <> dsInactive) and (QrPrdGrupJan.RecordCount > 0);
  //
  Incluijanelas1.Enabled := Habilita1;
  Excluijanelas1.Enabled := Habilita1 and Habilita2;
end;

procedure TFmPrdGrupTip.PMTipoPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := QrPrdGrupTipCodigo.Value > 0;
  Alteratipodegrupodeprodutosatual1.Enabled := True(*Habilita*);
  Habilita := Habilita and (QrPrdGrupJan.State <> dsInactive) and (QrPrdGrupJan.RecordCount = 0);
  Habilita := Habilita and (QrPrdGrupAtr.State <> dsInactive) and (QrPrdGrupAtr.RecordCount = 0);
  Habilita := Habilita and (QrPrdGrupCen.State <> dsInactive) and (QrPrdGrupCen.RecordCount = 0);
  Habilita := Habilita and (QrGraGru1.State <> dsInactive) and (QrGraGru1.RecordCount = 0);
  Excluitipodegrupodeprodutosatual1.Enabled := Habilita;
  //
  if (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTip.RecordCount > 0) then
  begin
    Desativarativartipodegrupodeprodutoatual1.Visible := True;
    //
    if QrPrdGrupTipAtivo.Value = 1 then
      Desativarativartipodegrupodeprodutoatual1.Caption := 'Desativar tipo de grupo de &produto atual'
    else
      Desativarativartipodegrupodeprodutoatual1.Caption := 'Ativar tipo de grupo de &produto atual';
  end else
    Desativarativartipodegrupodeprodutoatual1.Visible := False;
end;

procedure TFmPrdGrupTip.ComissoNivel21Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraComiss, FmGraComiss, afmoNegarComAviso) then
  begin
    FmGraComiss.PnNivel2.Visible := True;
    FmGraComiss.ShowModal;
    FmGraComiss.Destroy;
  end;
end;

procedure TFmPrdGrupTip.ComissoNivel31Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraComiss, FmGraComiss, afmoNegarComAviso) then
  begin
    FmGraComiss.PnNivel3.Visible := True;
    FmGraComiss.ShowModal;
    FmGraComiss.Destroy;
  end;
end;

procedure TFmPrdGrupTip.ConfiguraNiveis(Nivel: Integer; SQLType: TSQLType);
begin
  LaTitNiv5.Visible := Nivel >= 5;
  EdTitNiv5.Visible := Nivel >= 5;
  LaTitNiv4.Visible := Nivel >= 4;
  EdTitNiv4.Visible := Nivel >= 4;
  LaTitNiv3.Visible := Nivel >= 3;
  EdTitNiv3.Visible := Nivel >= 3;
  LaTitNiv2.Visible := Nivel >= 2;
  EdTitNiv2.Visible := Nivel >= 2;
  EdTitNiv1.Visible := Nivel >= 1;
  LaTitNiv1.Visible := Nivel >= 1;
  //
  if SQLType = stIns then
  begin
    if EdTitNiv5.Visible then
      EdTitNiv5.ValueVariant := 'N�vel 5'
    else
      EdTitNiv5.ValueVariant := '';
    //
    if EdTitNiv4.Visible then
      EdTitNiv4.ValueVariant := 'N�vel 4'
    else
      EdTitNiv4.ValueVariant := '';
    //
    if EdTitNiv3.Visible then
      EdTitNiv3.ValueVariant := 'N�vel 3'
    else
      EdTitNiv3.ValueVariant := '';
    //
    if EdTitNiv2.Visible then
      EdTitNiv2.ValueVariant := 'N�vel 2'
    else
      EdTitNiv2.ValueVariant := '';
    //
    if EdTitNiv1.Visible then
      EdTitNiv1.ValueVariant := 'N�vel 1'
    else
      EdTitNiv1.ValueVariant := '';
  end;
end;

procedure TFmPrdGrupTip.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrdGrupTip.CTBNivel21Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraPlaCta, FmGraPlaCta, afmoNegarComAviso) then
  begin
    FmGraPlaCta.PnNivel2.Visible := True;
    FmGraPlaCta.ShowModal;
    FmGraPlaCta.Destroy;
  end;
end;

procedure TFmPrdGrupTip.CTBNivel31Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraPlaCta, FmGraPlaCta, afmoNegarComAviso) then
  begin
    FmGraPlaCta.PnNivel3.Visible := True;
    FmGraPlaCta.ShowModal;
    FmGraPlaCta.Destroy;
  end;
end;

{procedure TFmPrdGrupTip.AlteraRegistro;
var
  PrdGrupTip : Integer;
begin
  PrdGrupTip := QrPrdGrupTipCodigo.Value;
  if QrPrdGrupTipCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(PrdGrupTip, Dmod.MyDB, 'PrdGrupTip', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PrdGrupTip, Dmod.MyDB, 'PrdGrupTip', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPrdGrupTip.IncluiRegistro;
var
  Cursor : TCursor;
  PrdGrupTip : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PrdGrupTip := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PrdGrupTip', 'PrdGrupTip', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PrdGrupTip))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, PrdGrupTip);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmPrdGrupTip.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrdGrupTip.DBGGraGru9DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  BtMove9.Enabled := (DBGGraGru9.SelectedRows.Count > 0) and
  (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTipCodigo.Value > 0);
end;

procedure TFmPrdGrupTip.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPrdGrupTip.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrdGrupTip.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrdGrupTip.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrdGrupTip.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrdGrupTip.SpeedButton5Click(Sender: TObject);
begin
  SelecionaTipo9();
end;

procedure TFmPrdGrupTip.Alteratipodegrupodeprodutosatual1Click(Sender: TObject);
var
  Habilita: Boolean;
begin
  if QrPrdGrupTipCodigo.Value = 0 then
    Exit;
  //
  Habilita := (QrPrdGrupTipCodigo.Value > 0);
  //
  if not Habilita then
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPrdGrupTip, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'prdgruptip');
  //
  ConfiguraNiveis(QrPrdGrupTipNivCad.Value, stUpd);
  //
  MostraEdicaoEspecifico;
end;

procedure TFmPrdGrupTip.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrdGrupTipCodigo.Value;
  Close;
end;

procedure TFmPrdGrupTip.BtJanelasClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMJanelas, BtJanelas);
end;

procedure TFmPrdGrupTip.BtMove1Click(Sender: TObject);
var
  I, Nivel1, PrdGrupTip: Integer;
begin
  if Geral.MB_Pergunta('Confirma a retirada dos ' + IntToStr(
  DBGGraGru1.SelectedRows.Count) + ' itens selecionados para o Tipo ' +
  QrTipo9Nome.Value + '?') =
  ID_YES then
  begin
    PrdGrupTip := EdTipo9.ValueVariant;
    with DBGGraGru1.DataSource.DataSet do
    for I := 0 to DBGGraGru1.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGGraGru1.SelectedRows.Items[I]));
      GotoBookmark(DBGGraGru1.SelectedRows.Items[I]);
      //
      Nivel1 := QrGraGru1Nivel1.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
      'PrdGrupTip'], ['Nivel1'], [PrdGrupTip], [Nivel1], False);
      //
    end;
    QrGraGru1.Next;
    ReopenGraGru1(QrGraGru1Nivel1.Value, QrGraGru9Nivel1.Value);
  end;
end;

procedure TFmPrdGrupTip.BtMove9Click(Sender: TObject);
var
  I, Nivel1, PrdGrupTip: Integer;
begin
  if Geral.MB_Pergunta('Confirma a mudan�a dos ' + IntToStr(
  DBGGraGru9.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
  begin
    PrdGrupTip := QrPrdGrupTipCodigo.Value;
    with DBGGraGru9.DataSource.DataSet do
    for I := 0 to DBGGraGru9.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGGraGru9.SelectedRows.Items[I]));
      GotoBookmark(DBGGraGru9.SelectedRows.Items[I]);
      //
      Nivel1 := QrGraGru9Nivel1.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
      'PrdGrupTip'], ['Nivel1'], [PrdGrupTip], [Nivel1], False);
      //
    end;
    QrGraGru9.Next;
    ReopenGraGru1(QrGraGru1Nivel1.Value, QrGraGru9Nivel1.Value);
  end;
end;

procedure TFmPrdGrupTip.BtAtributosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMAtributos, BtAtributos);
end;

procedure TFmPrdGrupTip.BtCentrosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMCentros, BtCentros);
end;

procedure TFmPrdGrupTip.BtComissaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMComissao, BtComissao);
end;

procedure TFmPrdGrupTip.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  ErroServico: Boolean;
begin
  if (CO_GRADE_APP = False) and (RGGradeado.ItemIndex = 1) then
    if Geral.MB_Pergunta('Este grupo � realmente gradeado?') <> ID_YES then
      Exit;
  if MyObjects.FIC(RGIND_MOV.ItemIndex = -1,  EdNome, 'Defina o Movimento do Estqoe!') then Exit;
  //
  if MyObjects.FIC(EdNome.ValueVariant = '',  EdNome, 'Defina uma descri��o!') then Exit;
  if ImpedePeloNivel() then
    Exit;
  //
  ErroServico := (RGTipPrd.ItemIndex = 4) and (
    (RGGradeado.ItemIndex <> 0) or (RGUsaCores.ItemIndex <> 0)
  );
  if MyObjects.FIC(ErroServico,  RGTipPrd,
    'Servi�o n�o pode ter cor ou ser gradeado!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', ImgTipo.SQLType,
    QrPrdGrupTipCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmPrdGrupTip, PainelEdit,
    'prdgruptip', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPrdGrupTip.BtCTBClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCTB, BtCTB);
end;

procedure TFmPrdGrupTip.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PrdGrupTip', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrdGrupTip', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PrdGrupTip', 'Codigo');
end;

procedure TFmPrdGrupTip.BtTipoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMTipo, BtTipo);
end;

procedure TFmPrdGrupTip.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TabSheet3.TabVisible := False; //Comiss�es N�veis 2 e 3
  TabSheet4.TabVisible := False; // CTB Niveis 2 e 3
  //
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PainelEdit.Align   := alClient;
  PnGrids.Align      := alClient;
  CriaOForm;
  //
  ReopenGraTabApp;
  //
  UnDmkDAC_PF.AbreQuery(QrCtbCadGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlaAllCad, Dmod.MyDB);
  //
  MostraEdicaoEspecifico;
end;

procedure TFmPrdGrupTip.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrdGrupTipCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPrdGrupTip.SbCtbCadGruClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormCtbCadGru(EdCtbCadGru.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbCadGru, CBCtbCadGru, QrCtbCadGru, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmPrdGrupTip.SbCtbPlaCtaClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  {$IfDef UsaContabil}
    DModFin.MostraFormPlaAny(EdCtbPlaCta, CBCtbPlaCta, QrPlaAllCad, SbCtbPlaCta);
  {$Else}
    FinanceiroJan.MostraFormContas(EdCtbPlaCta.ValueVariant);
  {$EndIf}
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbPlaCta, CBCtbPlaCta, QrPlaAllCad, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmPrdGrupTip.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmPrdGrupTip.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrdGrupTip.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPrdGrupTipCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPrdGrupTip.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPrdGrupTip.QrGraComiss3AfterScroll(DataSet: TDataSet);
begin
  ReopenGraComiss2(0);
end;

procedure TFmPrdGrupTip.QrGraComiss3BeforeClose(DataSet: TDataSet);
begin
  QrGraComiss2.Close;
end;

procedure TFmPrdGrupTip.QrGraPlaCta3AfterScroll(DataSet: TDataSet);
begin
  ReopenGraPlaCta2(0);
end;

procedure TFmPrdGrupTip.QrGraPlaCta3BeforeClose(DataSet: TDataSet);
begin
  QrGraPlaCta2.Close;
end;

procedure TFmPrdGrupTip.QrPrdGrupTipAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrdGrupTip.QrPrdGrupTipAfterScroll(DataSet: TDataSet);
begin
  ReopenPrdGrupAtr(0);
  ReopenPrdGrupJan(0);
  ReopenPrdGrupCen(0);
  ReopenGraGru1(0,0);
  ReopenGraComiss3(0);
  ReopenGraPlaCta3(0);
end;

procedure TFmPrdGrupTip.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrdGrupTip.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrdGrupTipCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PrdGrupTip', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrdGrupTip.SelecionaTipo9();
const
  Aviso  = '...';
  Titulo = 'Sele��o de Tipo de Grupo de Produto';
  Prompt = 'Informe o Tipo de Grupo de Produto';
  Campo  = 'Descricao';
var
  iPrdGrupTip, Codigo: Variant;
  sPrdgrupTip, PesqSQL: String;
begin
  iPrdGrupTip := QrPrdGrupTipCodigo.Value;
  if iPrdGrupTip <> 0 then
  begin
    //if iPrdGrupTip < 0 then
      //if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
    //if not Continua
    sPrdgrupTip := Geral.FF0(iPrdGrupTip);
    PesqSQL := Geral.ATS([
    'SELECT Codigo, Nome ' + Campo,
    'FROM prdgruptip ',
    'WHERE NOT (Codigo IN (0, ' + sPrdGrupTip + '))',
    'ORDER BY ' + Campo,
    '']);
    iPrdGrupTip := 0;
    Codigo :=
      DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, iPrdGrupTip, [
      PesqSQL], Dmod.MyDB, True);
    if Codigo <> Null then
    begin
      EdTipo9.ValueVariant := Codigo;
    end;
  end;
end;

procedure TFmPrdGrupTip.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdGrupTip.HabilitaBtMove1();
begin
  BtMove1.Enabled :=
    (DBGGraGru1.SelectedRows.Count > 0) and (EdTipo9.ValueVariant <> 0) and
  (QrPrdGrupTip.State <> dsInactive) and (QrPrdGrupTipCodigo.Value > 0);
end;

procedure TFmPrdGrupTip.ImgTipoChange(Sender: TObject);
begin
  RGNivCad.Enabled := ImgTipo.SQLType = stIns;
end;

function TFmPrdGrupTip.ImpedePeloNivel(): Boolean;
var
  I, Nivel, PrdGrupTip: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando de n�veis');
  try
    Nivel := RGNivCad.ItemIndex;
    Result := MyObjects.FIC(Nivel = 0, RGNivCad, 'Informe o N�vel!');
    if Result or (ImgTipo.SQLType = stIns) then Exit;
    //
    PrdGrupTip := EdCodigo.ValueVariant;
    for I := 5 downto Nivel + 1 do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrTemNiv, Dmod.MyDB, [
      'SELECT COUNT(Nivel1) ITENS ',
      'FROM gragru1 ',
      'WHERE PrdGrupTip=' + Geral.FF0(PrdGrupTip),
      'AND Nivel' + Geral.FF0(I) + ' <> 0 ',
      '']);
      //
      if QrTemNivITENS.Value > 0 then
      begin
        Result := True;
        Geral.MB_Aviso('A quantidade de n�veis selecionada � insuficiente, ' +
        'pois j� existem produtos cadastrados no n�vel ' + Geral.FF0(I) +
        ' deste Tipo de Grupos!');
        if RGNivCad.ItemIndex < I then
          RGNivCad.ItemIndex := I;
        Exit;
      end;
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmPrdGrupTip.IncluiAtributos1Click(Sender: TObject);
var
  I, Controle, Codigo, GraAtrCad: Integer;
  SQL: String;
begin
  if DBCheck.CriaFm(TFmPrdGrupAtr, FmPrdGrupAtr, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    try
      with FmPrdGrupAtr do
      begin
(*
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('DROP TABLE atributos;   ');
        Query.SQL.Add('CREATE TABLE atributos (');
        Query.SQL.Add('  Ativo  smallint      ,');
        Query.SQL.Add('  CodUsu integer       ,');
        Query.SQL.Add('  Nome   varchar(30)   ,');
        Query.SQL.Add('  Codigo integer        ');
        Query.SQL.Add(');');
        //
*)
        SQL := Geral.ATS([
        'DROP TABLE IF EXISTS _abs_atributos;   ',
        'CREATE TABLE _abs_atributos (',
        '  Ativo  smallint      ,',
        '  CodUsu integer       ,',
        '  Nome   varchar(30)   ,',
        '  Codigo integer        ',
        ');',
        sLineBreak]);
        QrGraAtrCad.Close;
        UnDmkDAC_PF.AbreQuery(QrGraAtrCad, Dmod.MyDB);
        while not QrGraAtrCad.Eof do
        begin
(*
          Query.SQL.Add('INSERT INTO atributos (' +
          'Codigo, CodUsu, Nome, Ativo) VALUES (' +
            FormatFloat('0', QrGraAtrCadCodigo.Value) + ',' +
            FormatFloat('0', QrGraAtrCadCodUsu.Value) + ',"' +
            QrGraAtrCadNome.Value + '", 0);');
*)
          SQL := SQL +
            'INSERT INTO _abs_atributos (' +
            'Codigo, CodUsu, Nome, Ativo) VALUES (' +
            FormatFloat('0', QrGraAtrCadCodigo.Value) + ',' +
            FormatFloat('0', QrGraAtrCadCodUsu.Value) + ',"' +
            QrGraAtrCadNome.Value + '", 0);' + sLineBreak;
          //
          QrGraAtrCad.Next;
        end;
        //
(*
        Query.SQL.Add('SELECT * FROM atributos;');
        DmkABS_PF.AbreQuery(Query);
*)
        SQL := SQL + Geral.ATS([
          'SELECT * FROM _abs_atributos;',
          'DROP TABLE IF EXISTS _abs_atributos;',
          EmptyStr]);
        //Geral.MB_Teste(SQL);
        UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, SQL);
        //
        ShowModal;
        Controle := 0;
        if FmPrdGrupAtr.FCadastrar then
        begin
          QrPrdGrupAtr.First;
          while not QrPrdGrupAtr.Eof do
          begin
            UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'prdgrupatr',
              QrPrdGrupAtrControle.Value);
            //
            QrPrdGrupAtr.Next;
          end;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM prdgrupatr WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
          Dmod.QrUpd.ExecSQL;

          //

          Query.First;
          //j := 0;
          Controle := 0;
          Codigo := QrPrdGrupTipCodigo.Value;
(*
          while not Query.Eof do
          begin
            if QueryAtivo.Value = 1 then
            begin
              GraAtrCad := QueryCodigo.Value;
              Controle := UMyMod.BuscaEmLivreY_Def('prdgrupatr', 'Controle', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgrupatr', False, [
              'Codigo', 'GraAtrCad'], ['Controle'], [
              Codigo, GraAtrCad], [Controle], True);
            end;
            Query.Next;
          end;
*)
          with DBGItsToSel.DataSource.DataSet do
          for I := 0 to DBGItsToSel.SelectedRows.Count-1 do
          begin
            //GotoBookmark(pointer(DBGItsToSel.SelectedRows.Items[I]));
            GotoBookmark(DBGItsToSel.SelectedRows.Items[I]);
            //
            GraAtrCad := QueryCodigo.Value;
            Controle := UMyMod.BuscaEmLivreY_Def('prdgrupatr', 'Controle', stIns, 0);
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgrupatr', False, [
            'Codigo', 'GraAtrCad'], ['Controle'], [
            Codigo, GraAtrCad], [Controle], True);
          end;
        end;
        if Controle > 0 then
          ReopenPrdGrupAtr(Controle);
        Destroy;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPrdGrupTip.Incluicentro1Click(Sender: TObject);
var
  SQL: String;
  I: Integer;
begin
  if DBCheck.CriaFm(TFmPrdGrupCen, FmPrdGrupCen, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    try
      with FmPrdGrupCen do
      begin
{
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('DROP TABLE CenEstqPrd;   ');
        Query.SQL.Add('CREATE TABLE CenEstqPrd (');
        Query.SQL.Add('  Ativo  smallint      ,');
        Query.SQL.Add('  CodUsu integer       ,');
        Query.SQL.Add('  Nome   varchar(30)   ,');
        Query.SQL.Add('  Codigo integer        ');
        Query.SQL.Add(');');
        //
}
        SQL := Geral.ATS([
        'DROP TABLE IF EXISTS _abs_CenEstqPrd;   ',
        'CREATE TABLE _abs_CenEstqPrd (',
        '  Ativo  smallint      ,',
        '  CodUsu integer       ,',
        '  Nome   varchar(30)   ,',
        '  Codigo integer        ',
        ');',
        sLineBreak]);
        //
        QrEstqCen.Close;
        QrEstqCen.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
        UnDmkDAC_PF.AbreQuery(QrEstqCen, Dmod.MyDB);
        while not QrEstqCen.Eof do
        begin
(*
          Query.SQL.Add('INSERT INTO cenestqprd (' +
          'Ativo, Codigo, CodUsu, Nome) VALUES (' +
            FormatFloat('0', QrEstqCenAtivo.Value) + ',' +
            FormatFloat('0', QrEstqCenCodigo.Value) + ',' +
            FormatFloat('0', QrEstqCenCodUsu.Value) + ',"' +
            QrEstqCenNome.Value + '");');
*)
          SQL := SQL +
          'INSERT INTO _abs_cenestqprd (' +
          'Ativo, Codigo, CodUsu, Nome) VALUES (' +
            FormatFloat('0', QrEstqCenAtivo.Value) + ',' +
            FormatFloat('0', QrEstqCenCodigo.Value) + ',' +
            FormatFloat('0', QrEstqCenCodUsu.Value) + ',"' +
            QrEstqCenNome.Value + '");' + sLineBreak;
          //
          QrEstqCen.Next;
        end;
        //
(*
        Query.SQL.Add('SELECT * FROM cenestqprd;');
        DmkABS_PF.AbreQuery(Query);
*)
        SQL := SQL + Geral.ATS([
        'SELECT * FROM _abs_cenestqprd;',
        'DROP TABLE IF EXISTS _abs_cenestqprd;',
        sLineBreak]);
        //Geral.MB_Teste(SQL);
        UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, SQL);
        ShowModal;
        //Controle := 0;
        if FmPrdGrupCen.FCadastrar then
        begin
          {QrPrdGrupCen.First;
          while not QrPrdGrupCen.Eof do
          begin
            UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'prdgrupcen',
              QrPrdGrupCenControle.Value);
            //
            QrPrdGrupCen.Next;
          end;
          }
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM prdgrupcen WHERE prdgruptip=:P0');
          Dmod.QrUpd.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
          Dmod.QrUpd.ExecSQL;

          //

(*
          Query.First;
          //j := 0;
          //Controle := 0;
          //Codigo := QrPrdGrupTipCodigo.Value;
          while not Query.Eof do
          begin
            if QueryAtivo.Value = 1 then
            begin
              //GracenCad := QueryCodigo.Value;
              //? := UMyMod.BuscaEmLivreY_Def('prdgrupcen', ''PrdGrupTip', 'EstqCen', ImgTipo.SQLType, CodAtual);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgrupcen', False, [], [
              'PrdGrupTip', 'EstqCen'], [], [
              QrPrdGrupTipCodigo.Value, QueryCodigo.Value], True);
            end;
            Query.Next;
          end;
*)
          with DBGItsToSel.DataSource.DataSet do
          for I := 0 to DBGItsToSel.SelectedRows.Count-1 do
          begin
            //GotoBookmark(pointer(DBGItsToSel.SelectedRows.Items[I]));
            GotoBookmark(DBGItsToSel.SelectedRows.Items[I]);
            //
              //GracenCad := QueryCodigo.Value;
              //? := UMyMod.BuscaEmLivreY_Def('prdgrupcen', ''PrdGrupTip', 'EstqCen', ImgTipo.SQLType, CodAtual);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgrupcen', False, [], [
              'PrdGrupTip', 'EstqCen'], [], [
              QrPrdGrupTipCodigo.Value, QueryCodigo.Value], True);
          end;
        end;
        ReopenPrdGrupCen(QueryCodigo.Value);
        Destroy;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPrdGrupTip.Incluijanelas1Click(Sender: TObject);
var
  SQL: String;
  //
  function CarregaRegistrosDeJanelas(Query: TMySQLQuery;
  Janelas: array of String): Boolean;
  var
    i, j, k: Integer;
    Janela, Ativo: String;
  begin
    FLJanelas := TList<TJanelas>.Create;
    try
      MyList.CriaListaJanelas(FLJanelas);
{$IfNDef NO_FINANCEIRO}
      Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
{$EndIf}
      k := 0;
      for j := Low(Janelas) to High(Janelas) do
      begin
        Janela := Copy(Janelas[j], 1, pos(',', Janelas[j]) -1);
        Ativo  := Copy(Janelas[j], pos(',', Janelas[j]) +1, 1);
        for i:= 0 to FLJanelas.Count - 1 do
        begin
          FRJanelas := FLJanelas[i];
          //
          if FRJanelas.ID = Janela then
          begin
            inc(k, 1);
            //DModG.MyPID_DB.Execute('INSERT INTO _abs_janelas (' +
            SQL := SQL + 'INSERT INTO _abs_janelas (' +
            'janela,texto,ordem,ativo,Added) VALUES (' +
              '"' + Janela + '",' +
              '"' + FRJanelas.Descricao + '",' +
              FormatFloat('0', 900 + k) + ',' + Ativo + ',' +
              FormatFloat('0', k) + ');' + sLineBreak;
            Break;
          end;
        end;
      end;
      Result := True;
    finally
      FLJanelas.Free;
    end;
  end;

var
  I, Controle, Codigo, Ordem: Integer;
  Janela: String;
begin
  if DBCheck.CriaFm(TFmPrdGrupJan, FmPrdGrupJan, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    try
      with FmPrdGrupJan do
      begin
        //DModG.MyPID_DB.Execute(Geral.ATS([
        SQL := (Geral.ATS([
        'DROP TABLE IF EXISTS _abs_janelas;  ',
        'CREATE TABLE _abs_janelas ( ',
        '  Ativo   smallint     , ',
        '  Janela  varchar(30)  , ',
        '  Texto   varchar(255) , ',
        '  Ordem integer        , ',
        '  Added integer          ',
        '); ',
        EmptyStr]));
        //
        if QrPrdGrupTipGradeado.Value = 1 then
        begin
          {  Usar conditional Defines!! se usa CO_DMKID_APP ignora o c�digo abaixo!
          if CO_DMKID_APP = 24 then //Bugstrol
          begin
            CarregaRegistrosDeJanelas(Query,[
              'PRD-GRUPO-004,0', // Seleciona grade de tamanhos para grupo de produtos
              'PRD-GRUPO-005,0', // Seleciona cor para grupos de produtos
              'PRD-CORES-004,0', // Sele��o m�ltipla de cores em grade
              'PRD-GRUPO-008,0', // Ativa��o de Produtos
              'PRD-GRUPO-006,0', // Edita dados gerais do grupo de produtos
              'PRD-GRUPO-007,0', // Inclui / edita atributo para o grupo de produtos
              'PRD-ATRIB-003,0', // Cadastro de v�rios itens de atributo
              //'PRD-GRUPO-010,0', // Custos / Pre�os de Produtos
              'PRD-GRUPO-014,0', // Configura��es Fiscais do Grupo de Produtos
              'PRD-GRUPO-025,0', // Produtos de ...
              'PRD-GRUPO-023,0', // Produtos de ...
              'PRD-GRUPO-033,0' // Ve�culos
              // Aten��o: ao adicionar mais janelas aqui,
              // � necess�rio implemetar no "FmGraGruN.ContinuaSequenciaCadastro"
              ]);
          end else}
          begin
            CarregaRegistrosDeJanelas(Query,[
              'PRD-GRUPO-004,1', // Seleciona grade de tamanhos para grupo de produtos
              'PRD-GRUPO-005,0', // Seleciona cor para grupos de produtos
              'PRD-CORES-004,1', // Sele��o m�ltipla de cores em grade
              'PRD-GRUPO-008,1', // Ativa��o de Produtos
              'PRD-GRUPO-006,1', // Edita dados gerais do grupo de produtos
              'PRD-GRUPO-007,0', // Inclui / edita atributo para o grupo de produtos
              'PRD-ATRIB-003,0', // Cadastro de v�rios itens de atributo
              //'PRD-GRUPO-010,1', // Custos / Pre�os de Produtos
              'PRD-GRUPO-014,1'  // Configura��es Fiscais do Grupo de Produtos
              // Aten��o: ao adicionar mais janelas aqui,
              // � necess�rio implemetar no "FmGraGruN.ContinuaSequenciaCadastro"
              ]);
          end;
        end else
        begin
          {
          if CO_DMKID_APP = 24 then //Bugstrol
          begin
            CarregaRegistrosDeJanelas(Query,[
              'PRD-GRUPO-004,0', // Seleciona grade de tamanhos para grupo de produtos
              'PRD-GRUPO-005,0', // Seleciona cor para grupos de produtos
              'PRD-CORES-004,0', // Sele��o m�ltipla de cores em grade
              'PRD-GRUPO-008,0', // Ativa��o de Produtos
              'PRD-GRUPO-006,1', // Edita dados gerais do grupo de produtos
              'PRD-GRUPO-007,0', // Inclui / edita atributo para o grupo de produtos
              'PRD-ATRIB-003,0', // Cadastro de v�rios itens de atributo
              //'PRD-GRUPO-010,1', // Custos / Pre�os de Produtos
              'PRD-GRUPO-014,1',  // Configura��es Fiscais do Grupo de Produtos
              'PRD-GRUPO-025,0', // Produtos de ...
              'PRD-GRUPO-023,0', // Produtos de ...
              'PRD-GRUPO-033,0' // Ve�culos
              // Aten��o: ao adicionar mais janelas aqui,
              // � necess�rio implemetar no "FmGraGruN.ContinuaSequenciaCadastro"
              ]);
          end else}
          begin
            CarregaRegistrosDeJanelas(Query,[
              'PRD-GRUPO-004,0', // Seleciona grade de tamanhos para grupo de produtos
              'PRD-GRUPO-005,0', // Seleciona cor para grupos de produtos
              'PRD-CORES-004,0', // Sele��o m�ltipla de cores em grade
              'PRD-GRUPO-008,0', // Ativa��o de Produtos
              'PRD-GRUPO-006,1', // Edita dados gerais do grupo de produtos
              'PRD-GRUPO-007,0', // Inclui / edita atributo para o grupo de produtos
              'PRD-ATRIB-003,0', // Cadastro de v�rios itens de atributo
              //'PRD-GRUPO-010,1', // Custos / Pre�os de Produtos
              'PRD-GRUPO-014,1'  // Configura��es Fiscais do Grupo de Produtos
              // Aten��o: ao adicionar mais janelas aqui,
              // � necess�rio implemetar no "FmGraGruN.ContinuaSequenciaCadastro"
              ]);
          end;
        end;
        //
        //UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
        SQL := SQL + Geral.ATS([
        'SELECT * FROM _abs_janelas ',
        'ORDER BY Ativo DESC, Ordem, Added; ',
        'DROP TABLE _abs_janelas; ',
        EmptyStr]);
        //
        //Geral.MB_Teste(SQL);
        UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, SQL);
        ShowModal;
        Controle := 0;
        if FmPrdGrupJan.FCadastrar then
        begin
          QrPrdGrupJan.First;
          while not QrPrdGrupJan.Eof do
          begin
            UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'prdgrupjan',
              QrPrdGrupJanControle.Value);
            //
            QrPrdGrupJan.Next;
          end;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM prdgrupjan WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
          Dmod.QrUpd.ExecSQL;
          Query.First;
          //j := 0;
          Codigo   := QrPrdGrupTipCodigo.Value;
(*
          while not Query.Eof do
          begin
            if QueryAtivo.Value = 1 then
            begin
              Janela   := QueryJanela.Value;
              Ordem    := QueryOrdem.Value;
              Controle := UMyMod.BuscaEmLivreY_Def('prdgrupjan', 'Controle', stIns, 0);
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgrupjan', False, [
                'Codigo', 'Janela', 'Ordem'], ['Controle'], [
                Codigo, Janela, Ordem], [Controle], True);
            end;
            Query.Next;
          end;
*)
          with DBGItsToSel.DataSource.DataSet do
          for I := 0 to DBGItsToSel.SelectedRows.Count-1 do
          begin
            //GotoBookmark(pointer(DBGItsToSel.SelectedRows.Items[I]));
            GotoBookmark(DBGItsToSel.SelectedRows.Items[I]);
            //
            Janela   := QueryJanela.Value;
            Ordem    := QueryOrdem.Value;
            Controle := UMyMod.BuscaEmLivreY_Def('prdgrupjan', 'Controle', stIns, 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgrupjan', False, [
              'Codigo', 'Janela', 'Ordem'], ['Controle'], [
              Codigo, Janela, Ordem], [Controle], True);
          end;
        end;
        if Controle > 0 then
          ReopenPrdGrupJan(Controle);
        Destroy;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

end;

procedure TFmPrdGrupTip.Incluinovotipodegrupodeprodutos1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPrdGrupTip, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'prdgruptip');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'prdgruptip', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
  ConfiguraNiveis(1, stIns);
  //
  MostraEdicaoEspecifico;
end;

procedure TFmPrdGrupTip.QrPrdGrupTipBeforeClose(DataSet: TDataSet);
begin
  QrPrdGrupAtr.Close;
  QrPrdGrupJan.Close;
  QrPrdGrupCen.Close;
  QrGraGru1.Close;
  QrGraGru9.Close;
  QrGraComiss3.Close;
end;

procedure TFmPrdGrupTip.QrPrdGrupTipBeforeOpen(DataSet: TDataSet);
begin
  QrPrdGrupTipCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPrdGrupTip.QrPrdGrupTipCalcFields(DataSet: TDataSet);
begin
{$IfNDef NO_FINANCEIRO}
  QrPrdGrupTipTipo_Item_TXT.Value :=
    UFinanceiro.SPED_Tipo_Item_Get(QrPrdGrupTipTipo_Item.Value);
{$EndIf}
end;

procedure TFmPrdGrupTip.ReopenGraComiss2(CodUsu: Integer);
begin
  QrGraComiss2.Close;
  QrGraComiss2.Params[00].AsInteger := QrPrdGrupTipCodigo.Value;
  QrGraComiss2.Params[01].AsInteger := QrGraComiss3Nivel3.Value;
  QrGraComiss2.Params[02].AsInteger := QrPrdGrupTipCodigo.Value;
  QrGraComiss2.Params[03].AsInteger := QrGraComiss3Nivel3.Value;
  UnDmkDAC_PF.AbreQuery(QrGraComiss2, Dmod.MyDB);
  //
  QrGraComiss2.Locate('CodUsu', CodUsu, []);
end;

procedure TFmPrdGrupTip.ReopenGraComiss3(CodUsu: Integer);
begin
  QrGraComiss3.Close;
  QrGraComiss3.Params[00].AsInteger := QrPrdGrupTipCodigo.Value;
  QrGraComiss3.Params[01].AsInteger := QrPrdGrupTipCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraComiss3, Dmod.MyDB);
  //
  QrGraComiss3.Locate('CodUsu', CodUsu, []);
end;

procedure TFmPrdGrupTip.ReopenGraGru1(Nivel1, Nivel9: Integer(*; ReopenTipo9: Boolean*));
var
  Tipo9: Integer;
begin
  QrGraGru1.Close;
  QrGraGru1.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  if Nivel1 <> 0 then
    QrGraGru1.Locate('Nivel1', Nivel1, []);
  //
  ReopenTipo9();
{
  if ReopenTipo9 or (QrTipo9.State = dsInactive) then
  begin
    QrTipo9.Close;
    QrTipo9.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrTipo9, Dmod.MyDB);
  end;
  if not QrTipo9.Locate('CodUsu', EdTipo9.ValueVariant, []) then
  begin
    EdTipo9.ValueVariant := 0;
    CBTipo9.KeyValue     := Null;
  end;
  if EdTipo9.ValueVariant <> 0 then
    Tipo9 := QrTipo9Codigo.Value
  else
    Tipo9 := 0;
  //
  QrGraGru9.Close;
  QrGraGru9.SQL.Clear;
  QrGraGru9.SQL.Add('SELECT gg1.Nivel1, gg1.Codusu, gg1.Nome');
  QrGraGru9.SQL.Add('FROM gragru1 gg1');
  QrGraGru9.SQL.Add('WHERE gg1.PrdGrupTip<>:P0');
  if Tipo9 <> 0 then
    QrGraGru9.SQL.Add('AND PrdGrupTip=' + FormatFloat('0', Tipo9));
  QrGraGru9.SQL.Add('ORDER BY Nome');
  QrGraGru9.SQL.Add('');
  QrGraGru9.SQL.Add('');
  QrGraGru9.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGru9, Dmod.MyDB);
  if Nivel9 <> 0 then
    QrGraGru9.Locate('Nivel1', Nivel9, []);
}
end;

procedure TFmPrdGrupTip.ReopenGraPlaCta2(CodUsu: Integer);
begin
  QrGraPlaCta2.Close;
  QrGraPlaCta2.Params[00].AsInteger := QrPrdGrupTipCodigo.Value;
  QrGraPlaCta2.Params[01].AsInteger := QrGraPlaCta3Nivel3.Value;
  QrGraPlaCta2.Params[02].AsInteger := QrPrdGrupTipCodigo.Value;
  QrGraPlaCta2.Params[03].AsInteger := QrGraPlaCta3Nivel3.Value;
  UnDmkDAC_PF.AbreQuery(QrGraPlaCta2, Dmod.MyDB);
  //
  QrGraPlaCta2.Locate('CodUsu', CodUsu, []);
end;

procedure TFmPrdGrupTip.ReopenGraPlaCta3(CodUsu: Integer);
begin
  QrGraPlaCta3.Close;
  QrGraPlaCta3.Params[00].AsInteger := QrPrdGrupTipCodigo.Value;
  QrGraPlaCta3.Params[01].AsInteger := QrPrdGrupTipCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraPlaCta3, Dmod.MyDB);
  //
  QrGraPlaCta3.Locate('CodUsu', CodUsu, []);
end;

procedure TFmPrdGrupTip.ReopenGraTabApp;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraTabApp, Dmod.MyDB, [
    'SELECT * ',
    'FROM gratabapp ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmPrdGrupTip.ReopenPrdGrupAtr(Controle: Integer);
begin
  QrPrdGrupAtr.Close;
  QrPrdGrupAtr.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPrdGrupAtr, Dmod.MyDB);
  //
  QrPrdGrupAtr.Locate('Controle', Controle, []);
end;

procedure TFmPrdGrupTip.ReopenPrdGrupJan(Controle: Integer);
begin
  QrPrdGrupJan.Close;
  QrPrdGrupJan.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPrdGrupJan, Dmod.MyDB);
  //
  QrPrdGrupJan.Locate('Controle', Controle, []);
end;

procedure TFmPrdGrupTip.ReopenTipo9();
var
  Tipo9, PrdGrupTip: Integer;
  sPrdGrupTip: String;
begin
  QrTipo9.Close;
  Tipo9      := EdTipo9.ValueVariant;
  PrdGrupTip := QrPrdGrupTipCodigo.Value;
  sPrdGrupTip := Geral.FF0(PrdGrupTip);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTipo9, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome  ',
  'FROM prdgruptip ',
  'WHERE Codigo=' + Geral.FF0(Tipo9),
  'AND not (Codigo IN (0,' + sPrdgrupTip + ')) ',
  'ORDER BY Nome ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru9, Dmod.MyDB, [
  'SELECT gg1.Nivel1, gg1.Codusu, gg1.Nome ',
  'FROM gragru1 gg1 ',
  'WHERE gg1.PrdGrupTip=' + Geral.FF0(Tipo9),
  'AND NOT (gg1.PrdGrupTip IN (0,' + sPrdGrupTip + '))',
  '']);
  HabilitaBtMove1();
end;

procedure TFmPrdGrupTip.RGNivCadClick(Sender: TObject);
begin
  ConfiguraNiveis(RGNivCad.ItemIndex, ImgTipo.SQLType);
end;

procedure TFmPrdGrupTip.ReopenPrdGrupCen(EstqCen: Integer);
begin
  QrPrdGrupCen.Close;
  QrPrdGrupCen.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPrdGrupCen, Dmod.MyDB);
  //
  QrPrdGrupCen.Locate('EstqCen', EstqCen, []);
end;

end.

