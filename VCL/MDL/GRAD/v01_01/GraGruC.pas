unit GraGruC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmGraGruC = class(TForm)
    Panel1: TPanel;
    EdCor: TdmkEditCB;
    CBCor: TdmkDBLookupComboBox;
    QrGraCorCad: TmySQLQuery;
    DsGraCorCad: TDataSource;
    dmkValUsu1: TdmkValUsu;
    Label1: TLabel;
    DBEdit1: TdmkDBEdit;
    Label2: TLabel;
    EdControle: TdmkEdit;
    QrGraCorCadCodigo: TIntegerField;
    QrGraCorCadCodUsu: TIntegerField;
    QrGraCorCadNome: TWideStringField;
    SpeedButton1: TSpeedButton;
    Label3: TLabel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruC: TFmGraGruC;

implementation

uses UnMyObjects, Module, UMySQLModule, GraGruN, UnGrade_Jan, MyDBCheck,
  UnInternalConsts, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruC.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('gragruc', 'controle', ImgTipo.SQLType,
    FmGraGruN.QrGraGruCControle.Value);
  if UMyMod.ExecSQLInsUpdFm(Fmgragruc, ImgTipo.SQLType, 'gragruc', Controle,
    Dmod.QrUpd) then
  begin
    FmGraGruN.Reopengragruc(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType     := stIns;
      EdControle.Text    := '';
      EdCor.ValueVariant := 0;
      CBCor.KeyValue     := 0;
    end else
      Close;
  end;
end;

procedure TFmGraGruC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
end;

procedure TFmGraGruC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruC.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := True;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end;
end;

procedure TFmGraGruC.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  Grade_Jan.MostraFormGraCorCad(dmkValUsu1.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrGraCorCad.Close;
    UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
    QrGraCorCad.Locate('Codigo', VAR_CADASTRO, []);
    //
    EdCor.ValueVariant := QrGraCorCadCodUsu.Value;
    CBCor.KeyValue     := QrGraCorCadCodUsu.Value;
    CBCor.SetFocus;
  end;
end;

end.
