object FmStqBalLeiIts: TFmStqBalLeiIts
  Left = 339
  Top = 185
  Caption = 'STQ-BALAN-003 :: Balan'#231'o - Sele'#231#227'o de Produtos'
  ClientHeight = 576
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 411
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnSeleciona: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 61
        Height = 13
        Caption = 'Produto: [F7]'
      end
      object EdGraGru1: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGru1Change
        OnExit = EdGraGru1Exit
        OnRedefinido = EdGraGru1Redefinido
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 549
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraGru1
        TabOrder = 1
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object PCModo: TPageControl
      Left = 0
      Top = 48
      Width = 1008
      Height = 363
      ActivePage = TabSheet6
      Align = alClient
      TabOrder = 1
      TabPosition = tpBottom
      object TabSheet5: TTabSheet
        Caption = ' Sele'#231#227'o '
        ImageIndex = 2
        object GradeQ: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 320
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDblClick = GradeQDblClick
          OnDrawCell = GradeQDrawCell
          OnKeyDown = GradeQKeyDown
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 320
          Width = 629
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
            'a incluir / alterar a quantidade de etiquetas.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' C'#243'digos '
        ImageIndex = 3
        object GradeC: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 158
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
          OnClick = GradeCClick
          OnDrawCell = GradeCDrawCell
          RowHeights = (
            18
            19)
        end
        object PageControl2: TPageControl
          Left = 0
          Top = 158
          Width = 1000
          Height = 153
          ActivePage = TabSheet1
          Align = alBottom
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = ' por &Leitura '
            object PnLeitura: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 97
              Align = alTop
              BevelOuter = bvNone
              Enabled = False
              ParentBackground = False
              TabOrder = 0
              object Panel4: TPanel
                Left = 0
                Top = 0
                Width = 461
                Height = 97
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object Panel3: TPanel
                  Left = 0
                  Top = 0
                  Width = 461
                  Height = 41
                  Align = alTop
                  BevelOuter = bvNone
                  Enabled = False
                  TabOrder = 0
                  object LaQtdeLei: TLabel
                    Left = 280
                    Top = 1
                    Width = 26
                    Height = 13
                    Caption = 'Qtde:'
                  end
                  object Label2: TLabel
                    Left = 196
                    Top = 20
                    Width = 77
                    Height = 13
                    Caption = 'Estoque atual ->'
                  end
                  object Label8: TLabel
                    Left = 372
                    Top = 1
                    Width = 27
                    Height = 13
                    Caption = 'Valor:'
                  end
                  object DBEdit5: TDBEdit
                    Left = 280
                    Top = 16
                    Width = 85
                    Height = 21
                    DataField = 'Qtde'
                    DataSource = DsSoma
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                    OnChange = DBEdit5Change
                  end
                  object DBEdit11: TDBEdit
                    Left = 372
                    Top = 16
                    Width = 85
                    Height = 21
                    DataField = 'ValorAll'
                    DataSource = DsSoma
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                    OnChange = DBEdit5Change
                  end
                end
                object Panel5: TPanel
                  Left = 0
                  Top = 41
                  Width = 461
                  Height = 27
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label3: TLabel
                    Left = 4
                    Top = 5
                    Width = 89
                    Height = 13
                    Caption = 'Leitura / digita'#231#227'o:'
                  end
                  object EdLeitura: TEdit
                    Left = 96
                    Top = 1
                    Width = 180
                    Height = 21
                    MaxLength = 20
                    TabOrder = 0
                    OnChange = EdLeituraChange
                    OnKeyDown = EdLeituraKeyDown
                  end
                  object EdQtdLei: TdmkEdit
                    Left = 280
                    Top = 1
                    Width = 85
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdQtdLeiChange
                    OnKeyDown = EdQtdLeiKeyDown
                  end
                  object EdQtdVal: TdmkEdit
                    Left = 372
                    Top = 1
                    Width = 85
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdQtdLeiChange
                    OnExit = EdQtdValExit
                    OnKeyDown = EdQtdLeiKeyDown
                  end
                end
                object Panel12: TPanel
                  Left = 0
                  Top = 68
                  Width = 461
                  Height = 29
                  Align = alClient
                  BevelOuter = bvNone
                  Enabled = False
                  TabOrder = 2
                  Visible = False
                  object Label7: TLabel
                    Left = 156
                    Top = 6
                    Width = 118
                    Height = 13
                    Caption = 'Estoque ap'#243's balan'#231'o ->'
                  end
                  object EdQtdFut: TdmkEdit
                    Left = 280
                    Top = 1
                    Width = 85
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlue
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
              end
              object PnMultiGrandeza: TPanel
                Left = 561
                Top = 0
                Width = 352
                Height = 97
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                Visible = False
                object Panel7: TPanel
                  Left = 0
                  Top = 0
                  Width = 352
                  Height = 41
                  Align = alTop
                  BevelOuter = bvNone
                  Enabled = False
                  TabOrder = 0
                  object LaPecas: TLabel
                    Left = 4
                    Top = 1
                    Width = 33
                    Height = 13
                    Caption = 'Pe'#231'as:'
                  end
                  object LaAreaM2: TLabel
                    Left = 92
                    Top = 1
                    Width = 39
                    Height = 13
                    Caption = #193'rea m'#178':'
                  end
                  object LaAreaP2: TLabel
                    Left = 180
                    Top = 1
                    Width = 37
                    Height = 13
                    Caption = #193'rea ft'#178':'
                  end
                  object LaPeso: TLabel
                    Left = 260
                    Top = 1
                    Width = 27
                    Height = 13
                    Caption = 'Peso:'
                  end
                  object DBEdit6: TDBEdit
                    Left = 4
                    Top = 16
                    Width = 85
                    Height = 21
                    DataField = 'Pecas'
                    DataSource = DsSoma
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                  end
                  object DBEdit8: TDBEdit
                    Left = 260
                    Top = 16
                    Width = 85
                    Height = 21
                    DataField = 'Peso'
                    DataSource = DsSoma
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                  object DBEdit9: TDBEdit
                    Left = 92
                    Top = 16
                    Width = 80
                    Height = 21
                    DataField = 'AreaM2'
                    DataSource = DsSoma
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 2
                  end
                  object DBEdit10: TDBEdit
                    Left = 176
                    Top = 16
                    Width = 80
                    Height = 21
                    DataField = 'AreaP2'
                    DataSource = DsSoma
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 3
                  end
                end
                object Panel8: TPanel
                  Left = 0
                  Top = 41
                  Width = 352
                  Height = 27
                  BevelOuter = bvNone
                  TabOrder = 1
                  object EdPecas: TdmkEdit
                    Left = 4
                    Top = 1
                    Width = 85
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdPecasChange
                    OnKeyDown = EdPecasKeyDown
                  end
                  object EdAreaM2: TdmkEditCalc
                    Left = 92
                    Top = 1
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdAreaM2Change
                    OnKeyDown = EdAreaM2KeyDown
                    dmkEditCalcA = EdAreaP2
                    CalcType = ctM2toP2
                    CalcFrac = cfQuarto
                  end
                  object EdAreaP2: TdmkEditCalc
                    Left = 176
                    Top = 1
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdAreaP2Change
                    OnKeyDown = EdAreaP2KeyDown
                    dmkEditCalcA = EdAreaM2
                    CalcType = ctP2toM2
                    CalcFrac = cfCento
                  end
                  object EdPeso: TdmkEdit
                    Left = 260
                    Top = 1
                    Width = 85
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdPesoChange
                    OnKeyDown = EdPesoKeyDown
                  end
                end
                object Panel14: TPanel
                  Left = 0
                  Top = 69
                  Width = 352
                  Height = 28
                  Align = alBottom
                  BevelOuter = bvNone
                  Enabled = False
                  TabOrder = 2
                  Visible = False
                  object EdPecasFut: TdmkEdit
                    Left = 4
                    Top = 2
                    Width = 85
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlue
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdAreaM2Fut: TdmkEditCalc
                    Left = 92
                    Top = 2
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlue
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    dmkEditCalcA = EdAreaP2Fut
                    CalcType = ctM2toP2
                    CalcFrac = cfQuarto
                  end
                  object EdAreaP2Fut: TdmkEditCalc
                    Left = 176
                    Top = 2
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlue
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '0'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    dmkEditCalcA = EdAreaM2Fut
                    CalcType = ctP2toM2
                    CalcFrac = cfCento
                  end
                  object EdPesoFut: TdmkEdit
                    Left = 260
                    Top = 2
                    Width = 85
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlue
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 3
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
              end
              object Panel6: TPanel
                Left = 461
                Top = 0
                Width = 100
                Height = 97
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 2
                object BtOK2: TBitBtn
                  Tag = 14
                  Left = 4
                  Top = 36
                  Width = 90
                  Height = 40
                  Caption = '&OK'
                  NumGlyphs = 2
                  TabOrder = 0
                  Visible = False
                  OnClick = BtOK2Click
                end
              end
            end
            object Panel9: TPanel
              Left = 0
              Top = 97
              Width = 992
              Height = 28
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label4: TLabel
                Left = 4
                Top = 8
                Width = 86
                Height = 13
                Caption = 'Grupo de produto:'
                FocusControl = DBEdit1
              end
              object Label5: TLabel
                Left = 376
                Top = 8
                Width = 19
                Height = 13
                Caption = 'Cor:'
                FocusControl = DBEdit2
              end
              object Label6: TLabel
                Left = 560
                Top = 8
                Width = 48
                Height = 13
                Caption = 'Tamanho:'
                FocusControl = DBEdit3
              end
              object Label12: TLabel
                Left = 664
                Top = 8
                Width = 31
                Height = 13
                Caption = 'Pre'#231'o:'
                FocusControl = DBEdit7
              end
              object Label11: TLabel
                Left = 768
                Top = 8
                Width = 27
                Height = 13
                Caption = '% IPI:'
                FocusControl = DBEdit4
              end
              object DBEdit1: TDBEdit
                Left = 92
                Top = 4
                Width = 281
                Height = 21
                TabStop = False
                DataField = 'NOMENIVEL1'
                DataSource = DsItem
                TabOrder = 0
              end
              object DBEdit2: TDBEdit
                Left = 396
                Top = 4
                Width = 157
                Height = 21
                TabStop = False
                DataField = 'NOMECOR'
                DataSource = DsItem
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 612
                Top = 4
                Width = 49
                Height = 21
                TabStop = False
                DataField = 'NOMETAM'
                DataSource = DsItem
                TabOrder = 2
              end
              object DBEdit7: TDBEdit
                Left = 700
                Top = 4
                Width = 64
                Height = 21
                TabStop = False
                DataField = 'PrecoF'
                DataSource = DsPreco
                TabOrder = 3
              end
              object DBEdit4: TDBEdit
                Left = 800
                Top = 4
                Width = 40
                Height = 21
                TabStop = False
                DataField = 'IPI_Alq'
                DataSource = DsItem
                TabOrder = 4
              end
            end
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 311
          Width = 1000
          Height = 26
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object LaInforme1: TLabel
            Left = 8
            Top = 4
            Width = 719
            Height = 16
            Caption = 
              'Para informar diretamente o estoque futuro, tecle F4 na caixa de' +
              ' edi'#231#227'o da unidade de mediida correspondente!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object LaInforme2: TLabel
            Left = 7
            Top = 3
            Width = 719
            Height = 16
            Caption = 
              'Para informar diretamente o estoque futuro, tecle F4 na caixa de' +
              ' edi'#231#227'o da unidade de mediida correspondente!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 3945215
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Ativos '
        object GradeA: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 320
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeADrawCell
          RowHeights = (
            18
            18)
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 320
          Width = 475
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
            'esativar o produto.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' X '
        ImageIndex = 6
        object GradeX: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 337
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          RowHeights = (
            18
            18)
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 7
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 375
        Height = 32
        Caption = 'Balan'#231'o - Sele'#231#227'o de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 375
        Height = 32
        Caption = 'Balan'#231'o - Sele'#231#227'o de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 375
        Height = 32
        Caption = 'Balan'#231'o - Sele'#231#227'o de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 459
    Width = 1008
    Height = 53
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 512
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel15: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 145
        Top = 16
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,'
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,'
      'gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragru1 gg1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.Codigo=:P0'
      'ORDER BY gg1.Nome')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 36
    Top = 8
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Sequencia) Ultimo'
      'FROM etqgeraits'
      'WHERE GraGruX = :P0'
      '')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUltimo: TIntegerField
      FieldName = 'Ultimo'
      Required = True
    end
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    AfterScroll = QrItemAfterScroll
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, '
      'pgt.MadeBy, pgt.Fracio, '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.prod_indTot'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 308
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrItemFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrItemHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrItemGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrItemprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 336
    Top = 256
  end
  object QrPreco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.PrecoF, InfAdCuztm,'
      '(QuantP-QuantC-QuantV) QuantF,'
      'PercCustom, MedidaC, MedidaL,'
      'MedidaA, MedidaE, MedOrdem'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 364
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrecoPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPrecoQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
    object QrPrecoInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrPrecoPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrPrecoMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrPrecoMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrPrecoMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrPrecoMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrPrecoMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 392
    Top = 256
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde, SUM(Pecas) Pecas,  SUM(Peso) Peso,'
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2'
      'FROM stqmovitsa'
      'WHERE Empresa=:P0'
      'AND StqCenCad=:P1'
      'AND GraGruX=:P2')
    Left = 492
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSomaQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSomaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSomaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSomaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSomaAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSomaValorAll: TFloatField
      FieldName = 'ValorAll'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 492
    Top = 304
  end
end
