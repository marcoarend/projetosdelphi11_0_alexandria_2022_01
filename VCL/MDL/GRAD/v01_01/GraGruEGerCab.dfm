object FmGraGruEGerCab: TFmGraGruEGerCab
  Left = 339
  Top = 185
  Caption = 
    'PRD-GRUPO-044 :: Gerenciamento de C'#243'digos de Produtos de Fornece' +
    'dores'
  ClientHeight = 452
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 699
        Height = 32
        Caption = 'Gerenciamento de C'#243'digos de Produtos de Fornecedores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 699
        Height = 32
        Caption = 'Gerenciamento de C'#243'digos de Produtos de Fornecedores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 699
        Height = 32
        Caption = 'Gerenciamento de C'#243'digos de Produtos de Fornecedores'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 92
    Width = 1008
    Height = 360
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 290
      Width = 1008
      Height = 70
      Align = alBottom
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 863
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 861
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtCad: TBitBtn
          Tag = 10
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Gen'#233'rico'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCadClick
        end
        object BtIts: TBitBtn
          Tag = 11
          Left = 136
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Espec'#237'fico'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtItsClick
        end
      end
    end
    object Panel2: TPanel
      Left = 501
      Top = 0
      Width = 507
      Height = 290
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 507
        Height = 290
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 507
          Height = 290
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 507
            Height = 77
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 200
              Height = 77
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 200
                Height = 85
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label1: TLabel
                  Left = 8
                  Top = 4
                  Width = 46
                  Height = 13
                  Caption = 'Pesquisa:'
                end
                object EdPesq: TEdit
                  Left = 8
                  Top = 20
                  Width = 185
                  Height = 24
                  TabOrder = 0
                  OnChange = EdPesqChange
                end
                object TBTam: TTrackBar
                  Left = 4
                  Top = 40
                  Width = 193
                  Height = 41
                  Min = 1
                  ParentShowHint = False
                  Position = 4
                  SelStart = 3
                  ShowHint = True
                  TabOrder = 1
                  OnChange = TBTamChange
                end
              end
            end
            object LbItensMD: TListBox
              Left = 283
              Top = 0
              Width = 224
              Height = 77
              Align = alRight
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Courier New'
              Font.Style = []
              ItemHeight = 17
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 0
            Top = 77
            Width = 507
            Height = 213
            Align = alClient
            DataSource = DsGraGruEIts
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'uCom'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Descri'#231#227'o do reduzido'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nivel1'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLAUNIDMED'
                Title.Caption = 'Un'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEUNIDMED'
                Title.Caption = 'Descr'#231#227'o unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFOP'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NCM'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFOP_Inn'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Embalagem'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Embalagem'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observacao'
                Visible = True
              end>
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 0
      Width = 501
      Height = 290
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 501
        Height = 45
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label6: TLabel
          Left = 8
          Top = 4
          Width = 57
          Height = 13
          Caption = 'Fornecedor:'
        end
        object EdFornece: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Embalagem'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdForneceRedefinido
          DBLookupComboBox = CBFornece
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBFornece: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 425
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsFornece
          TabOrder = 1
          dmkEditCB = EdFornece
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object DBGrid1: TdmkDBGridZTO
        Left = 0
        Top = 45
        Width = 501
        Height = 245
        Align = alClient
        DataSource = DsGraGruECab
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'cProd'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xProd'
            Width = 190
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fornece'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FRN'
            Width = 140
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1005
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 880
    Top = 212
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 880
    Top = 260
  end
  object QrGraGruECab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGruECabBeforeClose
    AfterScroll = QrGraGruECabAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gge.cProd, gge.xProd, gge.Fornece,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN'
      'FROM gragrue gge '
      'LEFT JOIN entidades  frn ON frn.Codigo=gge.Fornece')
    Left = 132
    Top = 108
    object QrGraGruECabcProd: TWideStringField
      FieldName = 'cProd'
      Size = 60
    end
    object QrGraGruECabxProd: TWideStringField
      FieldName = 'xProd'
      Size = 120
    end
    object QrGraGruECabFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrGraGruECabNO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
  end
  object DsGraGruECab: TDataSource
    DataSet = QrGraGruECab
    Left = 132
    Top = 156
  end
  object QrGraGruEIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gge.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'emb.Nome NO_Embalagem'
      ''
      'FROM gragrue gge '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=gge.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'LEFT JOIN entidades  frn ON frn.Codigo=gge.Fornece'
      'LEFT JOIN embalagens emb ON emb.Codigo=gge.Embalagem'
      ''
      '')
    Left = 128
    Top = 204
    object QrGraGruEItscProd: TWideStringField
      FieldName = 'cProd'
      Size = 60
    end
    object QrGraGruEItsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruEItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruEItsFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrGraGruEItsEmbalagem: TIntegerField
      FieldName = 'Embalagem'
    end
    object QrGraGruEItsObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrGraGruEItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruEItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruEItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruEItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruEItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruEItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruEItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGruEItsxProd: TWideStringField
      FieldName = 'xProd'
      Size = 120
    end
    object QrGraGruEItsNCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 8
    end
    object QrGraGruEItsCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrGraGruEItsuCom: TWideStringField
      FieldName = 'uCom'
      Required = True
      Size = 6
    end
    object QrGraGruEItsCFOP_Inn: TIntegerField
      FieldName = 'CFOP_Inn'
    end
    object QrGraGruEItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruEItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruEItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruEItsNO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
    object QrGraGruEItsNO_Embalagem: TWideStringField
      FieldName = 'NO_Embalagem'
    end
  end
  object DsGraGruEIts: TDataSource
    DataSet = QrGraGruEIts
    Left = 128
    Top = 252
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 128
    Top = 300
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 128
    Top = 348
  end
  object PMCad: TPopupMenu
    Left = 280
    Top = 316
    object IncluiGenerico1: TMenuItem
      Caption = '&Inclui Gen'#233'rico'
      OnClick = IncluiGenerico1Click
    end
    object AlteraGenerico1: TMenuItem
      Caption = '&Altera Gen'#233'rico'
      OnClick = AlteraGenerico1Click
    end
    object ExcluiGenerico1: TMenuItem
      Caption = '&Exclui Gen'#233'rico'
      OnClick = ExcluiGenerico1Click
    end
  end
  object PMIts: TPopupMenu
    Left = 340
    Top = 316
    object IncluiEspecfico1: TMenuItem
      Caption = '&Inclui Espec'#237'fico'
      OnClick = IncluiEspecfico1Click
    end
    object AlteraEspecfico1: TMenuItem
      Caption = '&Altera Espec'#237'fico'
      OnClick = AlteraEspecfico1Click
    end
    object ExcluiEspecfico1: TMenuItem
      Caption = '&Exclui Espec'#237'fico'
      OnClick = ExcluiEspecfico1Click
    end
  end
end
