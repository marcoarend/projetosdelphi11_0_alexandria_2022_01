unit GraComiss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Mask, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmGraComiss = class(TForm)
    Panel1: TPanel;
    PnNivel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DsGraComiss3: TDataSource;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    PnNivel2: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DsGraComiss2: TDataSource;
    Panel4: TPanel;
    EdPerComissF: TdmkEdit;
    Label7: TLabel;
    EdPerComissR: TdmkEdit;
    Label8: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraComiss: TFmGraComiss;

implementation

uses UnMyObjects, PrdGrupTip, Module;

{$R *.DFM}

procedure TFmGraComiss.BtOKClick(Sender: TObject);
begin
  if PnNivel3.Visible then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM gracomiss');
    Dmod.QrUpd.SQL.Add('WHERE NivelX=3');
    Dmod.QrUpd.SQL.Add('AND PrdGrupTip=:P0');
    Dmod.QrUpd.SQL.Add('AND Nivel3=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraComiss3Nivel3.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO gracomiss');
    Dmod.QrUpd.SQL.Add('SET NivelX=3, Nivel2=0,');
    Dmod.QrUpd.SQL.Add('PrdGrupTip=:P0, ');
    Dmod.QrUpd.SQL.Add('Nivel3=:P1,');
    Dmod.QrUpd.SQL.Add('PerComissF=:P2,');
    Dmod.QrUpd.SQL.Add('PerComissR=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraComiss3Nivel3.Value;
    Dmod.QrUpd.Params[02].AsFloat   := EdPerComissF.ValueVariant;
    Dmod.QrUpd.Params[03].AsFloat   := EdPerComissR.ValueVariant;
    Dmod.QrUpd.ExecSQL;
    //
    FmPrdGrupTip.ReopenGraComiss3(FmPrdGrupTip.QrGraComiss3CodUsu.Value);
    //
  end else
  if PnNivel2.Visible then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM gracomiss');
    Dmod.QrUpd.SQL.Add('WHERE NivelX=2');
    Dmod.QrUpd.SQL.Add('AND PrdGrupTip=:P0');
    Dmod.QrUpd.SQL.Add('AND Nivel3=:P1');
    Dmod.QrUpd.SQL.Add('AND Nivel2=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraComiss2Nivel3.Value;
    Dmod.QrUpd.Params[02].AsInteger := FmPrdGrupTip.QrGraComiss2Nivel2.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO gracomiss');
    Dmod.QrUpd.SQL.Add('SET NivelX=2,');
    Dmod.QrUpd.SQL.Add('PrdGrupTip=:P0, ');
    Dmod.QrUpd.SQL.Add('Nivel3=:P1,');
    Dmod.QrUpd.SQL.Add('Nivel2=:P2,');
    Dmod.QrUpd.SQL.Add('PerComissF=:P3,');
    Dmod.QrUpd.SQL.Add('PerComissR=:P4');
    Dmod.QrUpd.Params[00].AsInteger := FmPrdGrupTip.QrPrdGrupTipCodigo.Value;
    Dmod.QrUpd.Params[01].AsInteger := FmPrdGrupTip.QrGraComiss2Nivel3.Value;
    Dmod.QrUpd.Params[02].AsInteger := FmPrdGrupTip.QrGraComiss2Nivel2.Value;
    Dmod.QrUpd.Params[03].AsFloat   := EdPerComissF.ValueVariant;
    Dmod.QrUpd.Params[04].AsFloat   := EdPerComissR.ValueVariant;
    Dmod.QrUpd.ExecSQL;
    //
    FmPrdGrupTip.ReopenGraComiss2(FmPrdGrupTip.QrGraComiss2CodUsu.Value);
    //
  end;
  //
  Close;
end;

procedure TFmGraComiss.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraComiss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraComiss.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmGraComiss.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
