object DmProd: TDmProd
  OnCreate = DataModuleCreate
  Height = 630
  Width = 971
  PixelsPerInch = 96
  object QrLocta_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta'
      'FROM movim'
      'WHERE Controle=:P0'
      'AND Grade=:P1'
      'AND Cor=:P2'
      'AND Tam=:P3')
    Left = 392
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocta_Conta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrProdutos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruC Cor, ggx.GraTamI Tam, '
      'ggx.Controle, ggx.Ativo '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON ggx.GraGru1=gg1.Nivel1'
      'WHERE ggx.GraGru1=:P0'
      '')
    Left = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutosCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrProdutosTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrProdutosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrGradesCors: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gc.Controle Cor, cc.Nome NOMECOR, gc.GraCorCad'
      'FROM gragruc gc'
      'LEFT JOIN gracorcad cc ON cc.Codigo=gc.GraCorCad'
      'WHERE gc.Nivel1=:P0'
      'ORDER BY NOMECOR')
    Left = 32
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
    object QrGradesCorsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
  end
  object QrGradesTams: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle Tam, Nome NOMETAM '
      'FROM gratamits'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsTam: TAutoIncField
      FieldName = 'Tam'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Required = True
      Size = 5
    end
  end
  object QrEstq_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.EstqQ, pro.EstqV'
      'FROM produtos pro'
      'WHERE pro.Codigo= :P0'
      'AND pro.Cor= :P1'
      'AND pro.Tam= :P2')
    Left = 392
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstq_EstqQ: TFloatField
      FieldName = 'EstqQ'
      Required = True
    end
    object QrEstq_EstqV: TFloatField
      FieldName = 'EstqV'
      Required = True
    end
  end
  object QrPeriodoBal_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 392
    Top = 100
    object QrPeriodoBal_Periodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrCustoProd_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (EstqV / EstqQ) Custo'
      'FROM produtos'
      'WHERE Codigo=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2')
    Left = 392
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCustoProd_Custo: TFloatField
      FieldName = 'Custo'
    end
  end
  object QrComisPrd_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eci.ComisTip, eci.ComisQtd '
      'FROM equicomits eci'
      'LEFT JOIN produtos prd ON prd.Codigo=eci.Produto'
      'WHERE eci.Codigo=:P0'
      'AND prd.Codigo=:P1'
      'AND prd.Cor=:P2'
      'AND prd.Tam=:P3')
    Left = 392
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object QrLocProd_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PrecoV'
      'FROM produtos'
      'WHERE Codigo=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2')
    Left = 392
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocProd_PrecoV: TFloatField
      FieldName = 'PrecoV'
    end
  end
  object QrGraCorCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gcc.Codigo, gcc.CodUsu, gcc.Nome'
      'FROM gracorcad gcc'
      '')
    Left = 32
    Top = 144
    object QrGraCorCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCorCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraCorCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object QrAtribIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gac.Nome NOMEATRIBCAD, gai.Controle, '
      'gai.Codigo, gai.CodUsu, gai.Nome NOMEATRIBITS '
      'FROM graatrits gai'
      'LEFT JOIN graatrcad gac ON gac.Codigo=gai.Codigo'
      'ORDER BY NOMEATRIBCAD, NOMEATRIBITS, gai.Codigo, gai.Controle')
    Left = 32
    Top = 192
    object QrAtribItsNOMEATRIBCAD: TWideStringField
      FieldName = 'NOMEATRIBCAD'
      Size = 30
    end
    object QrAtribItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrAtribItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrAtribItsNOMEATRIBITS: TWideStringField
      FieldName = 'NOMEATRIBITS'
      Required = True
      Size = 50
    end
    object QrAtribItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrGraAtrIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gga.Controle '
      'FROM gragruatr gga'
      'WHERE Nivel1=:P0'
      'AND gga.GraAtrCad=:P1')
    Left = 32
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraAtrItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrGraGruVal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruC, ggx.GraTamI, ggv.CustoPreco'
      'FROM gragruval ggv'#13
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggv.GraGruX'
      'WHERE ggv.GraGru1=:P0'
      'AND ggv.GraCusPrc=:P1'
      'AND ggv.Entidade=:P2')
    Left = 32
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGruValGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraGruValGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      Required = True
    end
  end
  object QrLocGGV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggv.Controle'
      'FROM gragruval ggv'
      ''
      'WHERE ggv.GraGruX=:P0'
      'AND ggv.GraGru1=:P1'
      ''
      ''
      ''
      'AND ggv.GraCusPrc=:P2'
      'AND ggv.Entidade=:P3')
    Left = 32
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocGGVControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrPrdGrupJan: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Janela, Ordem '
      'FROM prdgrupjan'#13
      'WHERE Codigo=:P0'#10
      'ORDER BY Ordem')
    Left = 32
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrdGrupJanJanela: TWideStringField
      FieldName = 'Janela'
      Size = 30
    end
    object QrPrdGrupJanOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object QrLocTPGI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tpi.Controle'
      'FROM tabeprcgri tpi'#13
      'WHERE tpi.GraGruX=:P0'
      'AND tpi.GraGru1=:P1'
      'AND tpi.TabePrcCab=:P2')
    Left = 108
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocTPGIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrTabePrcGrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruC, ggx.GraTamI, tpi.Preco'
      'FROM tabeprcgri tpi'#13
      'LEFT JOIN gragrux ggx ON ggx.Controle=tpi.GraGruX'
      'WHERE tpi.GraGru1=:P0'
      'AND tpi.TabePrcCab=:P1')
    Left = 108
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTabePrcGrIGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrTabePrcGrIGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrTabePrcGrIPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
  end
  object QrMaxTams: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM gratamits'
      'GROUP BY Codigo'
      'ORDER BY Itens Desc')
    Left = 32
    Top = 480
    object QrMaxTamsItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrItsLotEtq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(egi.Sequencia) ITENS, '
      'ggx.GraGruC, ggx.GraTamI'
      'FROM etqgeraits egi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=egi.GraGruX'
      'WHERE egi.Codigo=:P0'
      'GROUP BY egi.GraGruX')
    Left = 188
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsLotEtqITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrItsLotEtqGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrItsLotEtqGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
  end
  object QrEtqPrinCmd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM etqprincmd'
      'WHERE Tipo in (0,1,2)'
      'AND Codigo=:P0'
      'ORDER BY Tipo, Ordem')
    Left = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqPrinCmdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEtqPrinCmdTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEtqPrinCmdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEtqPrinCmdControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEtqPrinCmdConfig: TWideStringField
      FieldName = 'Config'
      Size = 255
    end
  end
  object QrEtqGeraIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etq.GraGruX REDUZIDO, etq.Sequencia, '
      'ggx.GraGruC, gg1.CodUsu NIVEL1, '
      'gti.Controle COD_TAM, gti.Nome NOMETAM, '
      'gtc.CodUsu COD_GRADE, gtc.Nome NOMEGRADE, '
      'gcc.CodUsu COD_COR, gcc.Nome NOMECOR,'
      'gg1.Nome NOMENIVEL1'
      'FROM etqgeraits     etq'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=etq.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gti.Codigo'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE etq.Codigo=:P0'
      'ORDER BY etq.GraGruX, etq.Sequencia')
    Left = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEtqGeraItsREDUZIDO: TIntegerField
      FieldName = 'REDUZIDO'
      Required = True
    end
    object QrEtqGeraItsSequencia: TIntegerField
      FieldName = 'Sequencia'
      Required = True
    end
    object QrEtqGeraItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrEtqGeraItsNIVEL1: TIntegerField
      FieldName = 'NIVEL1'
    end
    object QrEtqGeraItsCOD_TAM: TIntegerField
      FieldName = 'COD_TAM'
    end
    object QrEtqGeraItsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrEtqGeraItsCOD_GRADE: TIntegerField
      FieldName = 'COD_GRADE'
    end
    object QrEtqGeraItsNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 50
    end
    object QrEtqGeraItsCOD_COR: TIntegerField
      FieldName = 'COD_COR'
    end
    object QrEtqGeraItsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
    object QrEtqGeraItsNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Size = 30
    end
  end
  object QrQtdItsPrz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens '
      'FROM tabeprcpzg'
      'WHERE Codigo=:P0')
    Left = 108
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrQtdItsPrzItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrLocPrz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tpp.Variacao'
      'FROM tabeprcpzg tpp'
      'WHERE tpp.Codigo=:P0'
      'AND :P1 BETWEEN DiasIni AND DiasFim'
      'ORDER BY Variacao DESC'
      '')
    Left = 108
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocPrzVariacao: TFloatField
      FieldName = 'Variacao'
      Required = True
    end
  end
  object QrPrcGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Preco '
      'FROM tabeprcgrg'
      'WHERE Codigo=:P0'
      'AND Nivel1=:P1')
    Left = 264
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrcGruPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object QrPediVdaIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraTamI, ggx.GraGruC, ggx.GraGru1, pvi.*'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Codigo=:P0'
      'AND gg1.Nivel1=:P1')
    Left = 108
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPediVdaItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrPediVdaItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrPediVdaItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrPediVdaItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPediVdaItsPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrPediVdaItsPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaItsValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrPediVdaItsDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrPediVdaItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaItsDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
  end
  object QrStqPrdGer: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT smia.GraGruX, GraTamI, GraGruC, SUM(smia.Qtde*smia.Baixa)' +
        ' QTDE '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX '
      'WHERE smia.Ativo=1'
      'AND ggx.GraGru1=:P0'
      'AND smia.Empresa=:P1'
      'GROUP BY smia.GraGruX'
      '')
    Left = 108
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqPrdGerGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrStqPrdGerQTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrStqPrdGerGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrStqPrdGerGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
  end
  object QrStqMovItsA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smi.Qtde) Qtde, ggx.GraGruC, ggx.GraTamI'
      'FROM stqmovitsa smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'WHERE smi.Tipo=:P0'
      'AND smi.OriCodi=:P1'
      'AND ggx.GraGru1=:P2'
      'GROUP BY ggx.GraGruC, ggx.GraTamI')
    Left = 108
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrStqMovItsAQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrStqMovItsAGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrStqMovItsAGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
  end
  object QrStqMovItsB: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smi.Qtde) Qtde, ggx.GraGruC, ggx.GraTamI'
      'FROM stqmovitsb smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'WHERE smi.Tipo=:P0'
      'AND smi.OriCodi=:P1'
      'AND ggx.GraGru1=:P2'
      'GROUP BY ggx.GraGruC, ggx.GraTamI')
    Left = 108
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrStqMovItsBQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrStqMovItsBGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrStqMovItsBGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
  end
  object QrSdosPed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM((pvi.QuantP-pvi.QuantC-pvi.QuantV)) QuantF,'
      'ggx.GraGruC, ggx.GraTamI'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX'
      'WHERE pvi.Codigo=:P0'
      'AND ggx.GraGru1=:P1'
      'GROUP BY ggx.GraGruC, ggx.GraTamI')
    Left = 108
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSdosPedQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
    object QrSdosPedGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSdosPedGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
  end
  object QrOpenX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu Codigo, Nome '
      'FROM stqbalcad'
      'WHERE Empresa=:P0'
      'AND PrdGrupTip=:P1'
      'AND StqCenCad=:P2'
      'AND Encerrou<2')
    Left = 476
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrOpenXCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOpenXNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsOpenX: TDataSource
    DataSet = QrOpenX
    Left = 548
    Top = 8
  end
  object QrTabePrcGrX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tpi.Preco'
      'FROM tabeprcgri tpi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=tpi.GraGruX'
      'WHERE ggx.Controle=:P0'
      'AND tpi.TabePrcCab=:P1')
    Left = 108
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTabePrcGrXPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
  end
  object QrPrcGrX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tpg.Preco'
      'FROM tabeprcgrg tpg'
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=tpg.Nivel1'
      'WHERE tpg.Codigo=:P0'
      'AND ggx.Controle=:P1')
    Left = 180
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrcGrXPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
  end
  object QrGraGrXVal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggv.CustoPreco'
      'FROM gragruval ggv '
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggv.GraGruX'
      'WHERE ggx.Controle=:P0'
      'AND ggv.GraCusPrc=:P1')
    Left = 32
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGrXValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      Required = True
    end
  end
  object QrTabePrcGrU: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tpg.*'
      'FROM tabeprcgrg tpg'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpg.Nivel1'
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1'
      'WHERE Codigo=:P0'
      'AND ggx.Controle=:P1')
    Left = 184
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTabePrcGrUCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTabePrcGrUNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrTabePrcGrUPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object QrGradesCor2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gc.Controle Cor, cc.Nome NOMECOR'
      'FROM gragruc gc'
      'LEFT JOIN gracorcad cc ON cc.Codigo=gc.GraCorCad'
      'WHERE gc.Nivel1=:P0'
      'AND gc.Controle IN '
      '('
      '  SELECT AbrangeGgc  '
      '  FROM gramatits'
      '  WHERE NivAbrange=3'
      '  AND AbrangeGg1=:P1'
      ')'
      'ORDER BY NOMECOR')
    Left = 108
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGradesCor2Cor: TIntegerField
      FieldName = 'Cor'
    end
    object QrGradesCor2NOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
  end
  object QrGradesTam2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle Tam, Nome NOMETAM '
      'FROM gratamits'
      'WHERE Codigo=:P0'
      'AND Controle IN '
      '('
      '  SELECT AbrangeGti  '
      '  FROM gramatits'
      '  WHERE NivAbrange=2'
      '  AND AbrangeGti=:P1'
      ')')
    Left = 188
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGradesTam2Tam: TAutoIncField
      FieldName = 'Tam'
    end
    object QrGradesTam2NOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
  end
  object QrNeed1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, ggx.GraGruC, '
      'ggx.GraTamI,  ggx.GraGru1, gg1.PrdGrupTip '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE Controle=:P0'
      '')
    Left = 396
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNeed1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNeed1GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrNeed1GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrNeed1GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrNeed1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrNeed2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gramatits'
      'WHERE '
      '  ( (NivAbrange=5) and (AbrangePgt=:P0))'
      '  OR'
      '  ((NivAbrange=4) AND (AbrangeGg1=:P1))')
    Left = 400
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrNeed3_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gramatits'
      'WHERE NivAbrange=3'
      'AND AbrangeGgc=0'
      'AND AbrangeGg1=:P0')
    Left = 400
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrGraCorIdx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gracoridx'
      'WHERE Grupo=:P0')
    Left = 264
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCorIdxCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gracoridx.Codigo'
    end
    object QrGraCorIdxGrupo: TWideStringField
      FieldName = 'Grupo'
      Origin = 'gracoridx.Grupo'
      Size = 30
    end
    object QrGraCorIdxGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gracoridx.GraCorCad'
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM paramsemp'
      'WHERE Codigo=:P0'
      '')
    Left = 184
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrParamsEmpMoeda: TIntegerField
      FieldName = 'Moeda'
    end
    object QrParamsEmpSituacao: TIntegerField
      FieldName = 'Situacao'
    end
    object QrParamsEmpRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrParamsEmpFretePor: TSmallintField
      FieldName = 'FretePor'
    end
    object QrParamsEmpTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
    end
    object QrParamsEmpFatSemPrcL: TSmallintField
      FieldName = 'FatSemPrcL'
    end
    object QrParamsEmpBalQtdItem: TFloatField
      FieldName = 'BalQtdItem'
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrParamsEmpFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
    end
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
    end
    object QrParamsEmpCtaProdVen: TIntegerField
      FieldName = 'CtaProdVen'
    end
    object QrParamsEmpfaturasep: TWideStringField
      FieldName = 'faturasep'
      Size = 1
    end
    object QrParamsEmpfaturaseq: TSmallintField
      FieldName = 'faturaseq'
    end
    object QrParamsEmpFaturaDta: TSmallintField
      FieldName = 'FaturaDta'
    end
    object QrParamsEmpTxtProdVen: TWideStringField
      FieldName = 'TxtProdVen'
      Size = 100
    end
    object QrParamsEmpLogo3x1: TWideStringField
      FieldName = 'Logo3x1'
      Size = 255
    end
    object QrParamsEmpTipCalcJuro: TSmallintField
      FieldName = 'TipCalcJuro'
    end
    object QrParamsEmpSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrParamsEmpSimplesEst: TSmallintField
      FieldName = 'SimplesEst'
    end
    object QrParamsEmpDirNFeGer: TWideStringField
      FieldName = 'DirNFeGer'
      Size = 255
    end
    object QrParamsEmpDirNFeAss: TWideStringField
      FieldName = 'DirNFeAss'
      Size = 255
    end
    object QrParamsEmpDirEnvLot: TWideStringField
      FieldName = 'DirEnvLot'
      Size = 255
    end
    object QrParamsEmpDirRec: TWideStringField
      FieldName = 'DirRec'
      Size = 255
    end
    object QrParamsEmpDirPedRec: TWideStringField
      FieldName = 'DirPedRec'
      Size = 255
    end
    object QrParamsEmpDirProRec: TWideStringField
      FieldName = 'DirProRec'
      Size = 255
    end
    object QrParamsEmpDirDen: TWideStringField
      FieldName = 'DirDen'
      Size = 255
    end
    object QrParamsEmpDirPedCan: TWideStringField
      FieldName = 'DirPedCan'
      Size = 255
    end
    object QrParamsEmpDirCan: TWideStringField
      FieldName = 'DirCan'
      Size = 255
    end
    object QrParamsEmpDirPedInu: TWideStringField
      FieldName = 'DirPedInu'
      Size = 255
    end
    object QrParamsEmpDirInu: TWideStringField
      FieldName = 'DirInu'
      Size = 255
    end
    object QrParamsEmpDirPedSit: TWideStringField
      FieldName = 'DirPedSit'
      Size = 255
    end
    object QrParamsEmpDirSit: TWideStringField
      FieldName = 'DirSit'
      Size = 255
    end
    object QrParamsEmpDirPedSta: TWideStringField
      FieldName = 'DirPedSta'
      Size = 255
    end
    object QrParamsEmpDirSta: TWideStringField
      FieldName = 'DirSta'
      Size = 255
    end
    object QrParamsEmpUF_WebServ: TWideStringField
      FieldName = 'UF_WebServ'
      Size = 2
    end
    object QrParamsEmpSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
    object QrParamsEmpInfoPerCuz: TSmallintField
      FieldName = 'InfoPerCuz'
    end
    object QrParamsEmpPathLogoNF: TWideStringField
      FieldName = 'PathLogoNF'
      Size = 255
    end
    object QrParamsEmpCtaServico: TIntegerField
      FieldName = 'CtaServico'
    end
    object QrParamsEmpTxtServico: TWideStringField
      FieldName = 'TxtServico'
      Size = 100
    end
    object QrParamsEmpDupServico: TWideStringField
      FieldName = 'DupServico'
      Size = 3
    end
    object QrParamsEmpDupProdVen: TWideStringField
      FieldName = 'DupProdVen'
      Size = 3
    end
    object QrParamsEmpPedVdaMudLista: TSmallintField
      FieldName = 'PedVdaMudLista'
    end
    object QrParamsEmpPedVdaMudPrazo: TSmallintField
      FieldName = 'PedVdaMudPrazo'
    end
  end
  object QrFatConIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraTamI, ggx.GraGruC, ggx.GraGru1, '
      'SUM(fci.QuantP) QuantP, Sum(fci.ValLiq) ValLiq, '
      'SUM(fci.ValBru) / SUM(fci.QuantP) PrecoR,'
      
        '(SUM(fci.ValBru) - SUM(fci.ValLiq)) / SUM(fci.ValBru) * 100 Desc' +
        'oP'
      'FROM fatconits fci'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=fci.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE fci.Codigo=:P0'
      'AND gg1.Nivel1=:P1'
      'GROUP BY ggx.GraTamI, ggx.GraGruC')
    Left = 184
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFatConItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrFatConItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrFatConItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrFatConItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrFatConItsValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrFatConItsDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrFatConItsPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
  end
  object QrVendidos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fci.QuantV) QuantV, '
      'ggx.GraGruC, ggx.GraTamI'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fcc.FatConRet = :P0'
      'AND gg1.Nivel1=:P1'
      'GROUP BY fci.GraGruX'
      'ORDER BY gg1.Nome'
      '')
    Left = 184
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVendidosQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrVendidosGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrVendidosGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
  end
  object QrFatRetIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fci.QuantV) QuantV, SUM(fci.ValFat) ValFat,'
      
        '(SUM(fci.ValBru) -SUM(fci.ValLiq)) / SUM(fci.ValBru) * 100 Desco' +
        'P,'
      'SUM(fci.PrecoR * fci.QuantV) / SUM(fci.QuantV) PrecoR,'
      'SUM(fci.PrecoF * fci.QuantV) / SUM(fci.QuantV) PrecoF,'
      'ggx.GraGruC, ggx.GraTamI'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fcc.FatConRet = :P0'
      'AND gg1.Nivel1= :P1'
      'GROUP BY fci.GraGruX'
      'ORDER BY gg1.Nome'
      '')
    Left = 264
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFatRetItsQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrFatRetItsValFat: TFloatField
      FieldName = 'ValFat'
    end
    object QrFatRetItsDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrFatRetItsPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrFatRetItsPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrFatRetItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrFatRetItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
  end
  object QrBxaA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde) Qtde, '
      'SUM(smia.Pecas / smia.FatorClas) Pecas,'
      'SUM(smia.Peso) Peso, '
      'SUM(smia.AreaM2) AreaM2, '
      'SUM(smia.AreaP2) AreaP2'
      'FROM stqmovitsa smia'
      'WHERE smia.Tipo=:P0'
      'AND smia.OriCodi=:P1'
      '')
    Left = 476
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBxaAQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrBxaAPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrBxaAPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrBxaAAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrBxaAAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object QrBxaB: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smib.Qtde) Qtde, '
      'SUM(smib.Pecas / smib.FatorClas) Pecas,'
      'SUM(smib.Peso) Peso, '
      'SUM(smib.AreaM2) AreaM2, '
      'SUM(smib.AreaP2) AreaP2'
      'FROM stqmovitsb smib'
      'WHERE smib.Tipo=:P0'
      'AND smib.OriCodi=:P1'
      ''
      '')
    Left = 476
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBxaBQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrBxaBPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrBxaBPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrBxaBAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrBxaBAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object QrFatVenIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraTamI, ggx.GraGruC, ggx.GraGru1, pvi.*'
      'FROM fatvenits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Codigo=:P0'
      'AND gg1.Nivel1=:P1')
    Left = 184
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFatVenItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrFatVenItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrFatVenItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrFatVenItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatVenItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFatVenItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFatVenItsPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrFatVenItsPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrFatVenItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrFatVenItsQuantC: TFloatField
      FieldName = 'QuantC'
    end
    object QrFatVenItsQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrFatVenItsValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrFatVenItsDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrFatVenItsDescoV: TFloatField
      FieldName = 'DescoV'
    end
    object QrFatVenItsValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrFatVenItsPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrFatVenItsMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrFatVenItsMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrFatVenItsMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrFatVenItsMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrFatVenItsPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrFatVenItsCustomizad: TSmallintField
      FieldName = 'Customizad'
    end
    object QrFatVenItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
  end
  object DsPGT_SCC: TDataSource
    DataSet = QrPGT_SCC
    Left = 820
    Top = 4
  end
  object QrPGT_SCC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT gg1.PrdGrupTip, smic.StqCenCad, pgt.LstPrcFisc, '
      
        'FLOOR(smic.Empresa+0.1) Empresa, FLOOR(scc.EntiSitio+0.1) EntiSi' +
        'tio'
      'FROM stqmovitsc smic'
      'LEFT JOIN BlueDerm.gragrux ggx ON ggx.Controle=smic.GraGruX'
      'LEFT JOIN BlueDerm.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN BlueDerm.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN BlueDerm.stqcencad scc ON scc.Codigo=smic.StqCenCad'
      'WHERE (smic.Empresa=-11 OR scc.EntiSitio=-11)'
      'AND GraGruX <> 0'
      'ORDER BY gg1.PrdGrupTip, smic.StqCenCad')
    Left = 792
    Top = 4
    object QrPGT_SCCPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrPGT_SCCStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrPGT_SCCLstPrcFisc: TSmallintField
      FieldName = 'LstPrcFisc'
    end
    object QrPGT_SCCEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPGT_SCCEntiSitio: TLargeintField
      FieldName = 'EntiSitio'
    end
  end
  object QrAbertura2a: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Abertura, GrupoBal'
      'FROM stqbalcad'
      'WHERE Empresa=-11'
      'AND PrdGrupTip=-2'
      'AND StqCenCad=1'
      'AND Abertura < "2009-08-01"'
      'ORDER BY Abertura DESC'
      'LIMIT 1')
    Left = 708
    Top = 52
    object QrAbertura2aAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrAbertura2aGrupoBal: TIntegerField
      FieldName = 'GrupoBal'
    end
  end
  object QrAbertura2b: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Abertura'
      'FROM stqbalcad'
      'WHERE Empresa=-11'
      'AND PrdGrupTip=-2'
      'AND StqCenCad=1'
      'AND Abertura >= "2009-08-01"'
      'ORDER BY Abertura '
      'LIMIT 1')
    Left = 736
    Top = 52
    object QrAbertura2bAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
  end
  object QrSMIC2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sm2.*, '
      'ens.CNPJ ENS_CNPJ, ens.IE ENS_IE, uf1.Nome ENS_NO_UF,'
      'emp.CNPJ EMP_CNPJ, emp.IE EMP_IE, uf2.Nome EMP_NO_UF'
      'FROM _smic2_ sm2'
      'LEFT JOIN bluederm.entidades ens ON ens.Codigo=sm2.EntiSitio'
      'LEFT JOIN bluederm.entidades emp ON emp.Codigo=sm2.Empresa'
      'LEFT JOIN bluederm.ufs uf1 ON uf1.Codigo=ens.EUF'
      'LEFT JOIN bluederm.ufs uf2 ON uf2.Codigo=emp.EUF'
      'ORDER BY NO_PGT, NO_PRD')
    Left = 764
    Top = 52
    object QrSMIC2Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = '_smic2_.Nivel1'
    end
    object QrSMIC2NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = '_smic2_.NO_PRD'
      Size = 50
    end
    object QrSMIC2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = '_smic2_.PrdGrupTip'
    end
    object QrSMIC2UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = '_smic2_.UnidMed'
    end
    object QrSMIC2NCM: TWideStringField
      FieldName = 'NCM'
      Origin = '_smic2_.NCM'
      Size = 10
    end
    object QrSMIC2NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = '_smic2_.NO_PGT'
      Size = 30
    end
    object QrSMIC2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = '_smic2_.SIGLA'
      Size = 6
    end
    object QrSMIC2NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = '_smic2_.NO_TAM'
      Size = 5
    end
    object QrSMIC2NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = '_smic2_.NO_COR'
      Size = 30
    end
    object QrSMIC2GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = '_smic2_.GraCorCad'
    end
    object QrSMIC2GraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = '_smic2_.GraGruC'
    end
    object QrSMIC2GraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = '_smic2_.GraGru1'
    end
    object QrSMIC2GraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = '_smic2_.GraTamI'
    end
    object QrSMIC2Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_smic2_.Empresa'
    end
    object QrSMIC2StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = '_smic2_.StqCenCad'
    end
    object QrSMIC2Qtde: TFloatField
      FieldName = 'Qtde'
      Origin = '_smic2_.QTDE'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrSMIC2SitProd: TSmallintField
      FieldName = 'SitProd'
      Origin = 'stqcencad.SitProd'
    end
    object QrSMIC2ENS_CNPJ: TWideStringField
      FieldName = 'ENS_CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrSMIC2ENS_IE: TWideStringField
      FieldName = 'ENS_IE'
      Origin = 'entidades.IE'
    end
    object QrSMIC2ENS_NO_UF: TWideStringField
      FieldName = 'ENS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrSMIC2EMP_CNPJ: TWideStringField
      FieldName = 'EMP_CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrSMIC2EMP_IE: TWideStringField
      FieldName = 'EMP_IE'
      Origin = 'entidades.IE'
    end
    object QrSMIC2EMP_NO_UF: TWideStringField
      FieldName = 'EMP_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrSMIC2PECAS: TFloatField
      FieldName = 'PECAS'
      Origin = '_smic2_.PECAS'
      DisplayFormat = '#,###,###,##0.0'
    end
    object QrSMIC2PESO: TFloatField
      FieldName = 'PESO'
      Origin = '_smic2_.PESO'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrSMIC2AREAM2: TFloatField
      FieldName = 'AREAM2'
      Origin = '_smic2_.AREAM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC2AREAP2: TFloatField
      FieldName = 'AREAP2'
      Origin = '_smic2_.AREAP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC2PrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
      Origin = '_smic2_.PrcCusUni'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSMIC2GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = '_smic2_.GraGruX'
    end
    object QrSMIC2ValorTot: TFloatField
      FieldName = 'ValorTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSMIC2My_Idx: TIntegerField
      FieldName = 'My_Idx'
    end
    object QrSMIC2PrintTam: TSmallintField
      FieldName = 'PrintTam'
    end
    object QrSMIC2PrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
    object QrSMIC2EntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrSMIC2GraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrSMIC2Exportado: TLargeintField
      FieldName = 'Exportado'
    end
  end
  object DsSMIC2: TDataSource
    DataSet = QrSMIC2
    Left = 792
    Top = 52
  end
  object frxDs: TfrxDBDataset
    UserName = 'frxDsSMIC2'
    CloseDataSource = False
    DataSet = QrSMIC2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 820
    Top = 52
  end
  object QrExport2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrExport2BeforeClose
    AfterScroll = QrExport2AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM _smic2_'
      'WHERE Exportado=0'
      'ORDER BY NO_PGT, NO_PRD')
    Left = 712
    Top = 100
    object QrExport2My_Idx: TIntegerField
      FieldName = 'My_Idx'
    end
    object QrExport2Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrExport2NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 50
    end
    object QrExport2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrExport2UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrExport2NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrExport2NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrExport2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrExport2PrintTam: TSmallintField
      FieldName = 'PrintTam'
    end
    object QrExport2NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrExport2PrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
    object QrExport2NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrExport2GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrExport2GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrExport2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrExport2GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrExport2SitProd: TSmallintField
      FieldName = 'SitProd'
    end
    object QrExport2EntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
    object QrExport2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrExport2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrExport2StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrExport2QTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrExport2PECAS: TFloatField
      FieldName = 'PECAS'
    end
    object QrExport2PESO: TFloatField
      FieldName = 'PESO'
    end
    object QrExport2AREAM2: TFloatField
      FieldName = 'AREAM2'
    end
    object QrExport2AREAP2: TFloatField
      FieldName = 'AREAP2'
    end
    object QrExport2GraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrExport2PrcCusUni: TFloatField
      FieldName = 'PrcCusUni'
    end
    object QrExport2ValorTot: TFloatField
      FieldName = 'ValorTot'
    end
    object QrExport2Exportado: TLargeintField
      FieldName = 'Exportado'
    end
  end
  object DsExport2: TDataSource
    DataSet = QrExport2
    Left = 740
    Top = 100
  end
  object frxDsExporta2: TfrxDBDataset
    UserName = 'frxDsExporta2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'My_Idx=My_Idx'
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NCM=NCM'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'PrintTam=PrintTam'
      'NO_TAM=NO_TAM'
      'PrintCor=PrintCor'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'SitProd=SitProd'
      'EntiSitio=EntiSitio'
      'GraGruX=GraGruX'
      'Empresa=Empresa'
      'StqCenCad=StqCenCad'
      'QTDE=QTDE'
      'PECAS=PECAS'
      'PESO=PESO'
      'AREAM2=AREAM2'
      'AREAP2=AREAP2'
      'GraCusPrc=GraCusPrc'
      'PrcCusUni=PrcCusUni'
      'ValorTot=ValorTot'
      'Exportado=Exportado')
    DataSet = QrExport2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 768
    Top = 100
  end
  object QrListErr1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM listerr1'
      'WHERE My_Idx=:P0')
    Left = 712
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrListErr1My_Idx: TIntegerField
      FieldName = 'My_Idx'
    end
    object QrListErr1Codigo: TWideStringField
      FieldName = 'Codigo'
    end
    object QrListErr1Texto1: TWideStringField
      FieldName = 'Texto1'
      Size = 255
    end
  end
  object DsListErr1: TDataSource
    DataSet = QrListErr1
    Left = 740
    Top = 148
  end
  object frxDsListErr1: TfrxDBDataset
    UserName = 'frxDsListErr1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'My_Idx=My_Idx'
      'Codigo=Codigo'
      'Texto1=Texto1')
    DataSet = QrListErr1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 768
    Top = 148
  end
  object QrMvt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT TipoCalc'
      'FROM fisregmvt frm '
      'WHERE frm.Codigo=:P0'
      'AND StqCenCad=:P1'
      'AND frm.Empresa=:P2')
    Left = 392
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMvtTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
  end
  object QrStqPrdEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde*smia.Baixa) QTDE'
      'FROM stqmovitsa smia'
      'WHERE smia.Baixa=1'
      'AND smia.GraGrux=:P0'
      'AND smia.Empresa=:P1'
      '')
    Left = 184
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqPrdEmpQTDE: TFloatField
      FieldName = 'QTDE'
    end
  end
  object QrGraOptEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT BUCTipoPreco, BUCGraCusPrc '
      'FROM graoptent'
      'WHERE Codigo=:P0')
    Left = 184
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraOptEntBUCTipoPreco: TSmallintField
      FieldName = 'BUCTipoPreco'
    end
    object QrGraOptEntBUCGraCusPrc: TIntegerField
      FieldName = 'BUCGraCusPrc'
    end
  end
  object QrGraGruDel: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggd.Receptor, ggx.Controle'
      'FROM gragrudel ggd'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggd.Receptor'
      'WHERE ggd.Excluido=:P0')
    Left = 632
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruDelReceptor: TIntegerField
      FieldName = 'Receptor'
    end
    object QrGraGruDelControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrCTB: TMySQLQuery
    Database = Dmod.MyDB
    Left = 632
    Top = 308
    object QrCTBGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrGG1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.UnidMed, unm.SIGLA'
      'FROM gragru1 gg1'
      'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed'
      'WHERE gg1.Nivel1=:P0')
    Left = 728
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGG1SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrGG1UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object QrGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.Nivel1=:P0'
      'AND ggx.Controle <> 0'
      'ORDER BY Controle')
    Left = 728
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSINTEGR_50: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT s50.*'
      'FROM sintegr_50 s50'
      'WHERE s50.ImporExpor=:P0'
      'AND s50.AnoMes=:P1'
      'AND s50.Empresa=:P2'
      'AND s50.LinArq=:P3'
      '')
    Left = 800
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSINTEGR_50ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSINTEGR_50AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSINTEGR_50Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSINTEGR_50LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrSINTEGR_50TIPO: TSmallintField
      FieldName = 'TIPO'
    end
    object QrSINTEGR_50CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSINTEGR_50IE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrSINTEGR_50EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSINTEGR_50UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSINTEGR_50MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSINTEGR_50SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSINTEGR_50NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSINTEGR_50CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrSINTEGR_50EMITENTE: TWideStringField
      FieldName = 'EMITENTE'
      Size = 1
    end
    object QrSINTEGR_50ValorNF: TFloatField
      FieldName = 'ValorNF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50BCICMS: TFloatField
      FieldName = 'BCICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50ValICMS: TFloatField
      FieldName = 'ValICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50ValIsento: TFloatField
      FieldName = 'ValIsento'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50ValNCICMS: TFloatField
      FieldName = 'ValNCICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50AliqICMS: TFloatField
      FieldName = 'AliqICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrSINTEGR_50Tabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrSINTEGR_50FatID: TIntegerField
      FieldName = 'FatID'
      DisplayFormat = '0;-0; '
    end
    object QrSINTEGR_50FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrSINTEGR_50ACHOU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ACHOU_TXT'
      Size = 3
      Calculated = True
    end
    object QrSINTEGR_50ConfVal: TSmallintField
      FieldName = 'ConfVal'
    end
    object QrSINTEGR_50Situacao: TWideStringField
      FieldName = 'Situacao'
      Size = 1
    end
  end
  object QrGraGruVinc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggv.GraGruX, ggx.GraGru1'
      'FROM gragruvinc ggv'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggv.GraGruX'
      'WHERE ggv.Nome=:P0'
      '')
    Left = 564
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruVincGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruVincGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM unidmed'
      'WHERE Sigla=:P0')
    Left = 728
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLocRef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, gg1.PrdgrupTip,'
      'ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PerComissF, gg1.PerComissR,'
      'gg1.GraTamcad, gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle > -900000'
      'AND gg1.Referencia=:P0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 728
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocRefNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrLocRefGraTamcad: TIntegerField
      FieldName = 'GraTamcad'
    end
    object QrLocRefReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrLocRefPerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrLocRefPerComissR: TFloatField
      FieldName = 'PerComissR'
    end
    object QrLocRefNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrLocRefNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrLocRefNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrLocRefPrdgrupTip: TIntegerField
      FieldName = 'PrdgrupTip'
    end
  end
  object DsLocRef: TDataSource
    DataSet = QrLocRef
    Left = 756
    Top = 408
  end
  object QrPrcNiv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tpn.Nivel, tpn.Preco'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      
        'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=1 AND tpn.CodNiv=gg1.Nivel' +
        '1'
      'WHERE tpn.Nivel IS NOT NULL'
      'AND tpn.Codigo=1'
      'AND ggx.Controle=1315'
      ''
      'UNION'
      ''
      'SELECT tpn.Nivel, tpn.Preco'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      
        'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=2 AND tpn.CodNiv=gg1.Nivel' +
        '2'
      'WHERE tpn.Nivel IS NOT NULL'
      'AND tpn.Codigo=1'
      'AND ggx.Controle=1315'
      ''
      ''
      'UNION'
      ''
      'SELECT tpn.Nivel, tpn.Preco'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      
        'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=3 AND tpn.CodNiv=gg1.Nivel' +
        '3'
      'WHERE tpn.Nivel IS NOT NULL'
      'AND tpn.Codigo=1'
      'AND ggx.Controle=1315'
      ''
      ''
      'UNION'
      ''
      'SELECT tpn.Nivel, tpn.Preco'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      
        'LEFT JOIN tabeprcniv tpn ON tpn.Nivel=4 AND tpn.CodNiv=gg1.PrdGr' +
        'upTip'
      'WHERE tpn.Nivel IS NOT NULL'
      'AND tpn.Codigo=1'
      'AND ggx.Controle=1315'
      ''
      'ORDER BY Nivel')
    Left = 260
    Top = 152
    object QrPrcNivNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrPrcNivPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object QrEstqCen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde * Baixa) Qtde '
      'FROM stqmovitsa'
      'WHERE Ativo = 1'
      'AND GraGruX=:P0'
      'AND StqCenCad=:P1')
    Left = 629
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEstqCenQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrOpcoesGrad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrOpcoesGradBeforeOpen
    AfterOpen = QrOpcoesGradAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM opcoesgrad')
    Left = 260
    Top = 480
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla'
      'FROM unidmed'
      'WHERE CodUsu = :P0')
    Left = 264
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu'
      'FROM unidmed'
      'WHERE Sigla = :P0')
    Left = 264
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT FatID, FatNum, Empresa'
      'FROM nfecaby'
      'WHERE Lancto=0'
      'AND FatParcela=0'
      'ORDER BY FatNum')
    Left = 772
    Top = 504
    object QrYFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrYFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrYEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrYIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM nfecaby'
      'WHERE FatID=1'
      'AND FatNum=4'
      'AND Empresa=-11'
      'ORDER BY Controle')
    Left = 808
    Top = 504
    object QrYItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrYLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Sub, FatParcela'
      'FROM lanctos'
      'WHERE FatID=1'
      'AND FatNum=4'
      'AND CliInt=-11'
      'ORDER BY FatParcela')
    Left = 736
    Top = 504
    object QrYLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrYLctSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrYLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object QrNfeX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT FatID, FatNum, Empresa'
      'FROM nfeitsi'
      'WHERE FatID=103'
      'ORDER BY FatNum, nItem')
    Left = 700
    Top = 504
    object QrNfeXFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNfeXFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNfeXEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrEAN13: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT EAN13, GraTamI, GraGruC'
      'FROM gragrux'
      'WHERE Gragru1=:P0')
    Left = 508
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEAN13EAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrEAN13GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrEAN13GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
  end
  object QrNext: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CodUsu) Numero'
      'FROM gragru3'
      'WHERE CodUsu >= 1000'
      'AND CodUsu <=2000')
    Left = 560
    Top = 144
    object QrNextNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
  end
  object QrRange: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FaixaIni, FaixaFim'
      'FROM prdgruptip'
      'WHERE CodUsu=:P0'
      '')
    Left = 596
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRangeFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
    end
    object QrRangeFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
    end
  end
  object QrGG1Fisc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NCM, EX_TIPI, UnidMed  '
      'FROM GraGru1 ')
    Left = 336
    Top = 4
    object QrGG1FiscNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGG1FiscEX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrGG1FiscUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object QrProd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX, ggx.GraGru1, '
      'pgt.Tipo_Item, pgt.Genero, unm.Sigla, gg1.*'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle=:P0')
    Left = 908
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrProdGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrProdNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrProdNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrProdNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrProdCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrProdNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrProdPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrProdGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrProdUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrProdCST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrProdCST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrProdNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProdPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrProdIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrProdIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrProdIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrProdIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrProdIPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object QrProdTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
    object QrProdPerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
    end
    object QrProdPerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
    end
    object QrProdMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
    object QrProdPartePrinc: TIntegerField
      FieldName = 'PartePrinc'
    end
    object QrProdInfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Size = 255
    end
    object QrProdSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
    object QrProdHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrProdGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrProdPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrProdPIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
    end
    object QrProdPIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
    end
    object QrProdPISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
    end
    object QrProdPISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
    end
    object QrProdCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrProdCOFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
    end
    object QrProdCOFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
    end
    object QrProdCOFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
    end
    object QrProdCOFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
    end
    object QrProdICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrProdICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrProdICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrProdICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrProdICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrProdICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrProdICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
    end
    object QrProdICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
    end
    object QrProdIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrProdLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProdAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrProdcGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrProdEX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrProdPIS_pRedBC: TFloatField
      FieldName = 'PIS_pRedBC'
    end
    object QrProdPISST_pRedBCST: TFloatField
      FieldName = 'PISST_pRedBCST'
    end
    object QrProdCOFINS_pRedBC: TFloatField
      FieldName = 'COFINS_pRedBC'
    end
    object QrProdCOFINSST_pRedBCST: TFloatField
      FieldName = 'COFINSST_pRedBCST'
    end
    object QrProdICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrProdICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object QrProdFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrProdTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrProdGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrProdSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrProdUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
    end
  end
  object QrSRC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 584
    Top = 576
  end
  object QrPsq1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 500
    Top = 364
  end
  object QrX999: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efd_h010'
      'WHERE ImporExpor=3'
      'AND AnoMes=201401'
      'AND Empresa=-11'
      'AND H005=1'
      'AND BalID=4'
      'AND BalNum=0'
      'AND BalItm=0')
    Left = 744
    Top = 4
  end
  object QrGraPckQtd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruC, ggx.GraTamI, gpq.Qtde'
      'FROM grapckqtd gpq '
      'LEFT JOIN gragrux ggx ON ggx.Controle=gpq.GraGruX '
      'WHERE gpq.Codigo=1')
    Left = 32
    Top = 532
    object QrGraPckQtdGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraPckQtdGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraPckQtdQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
  end
  object MySQLDatabase1: TMySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 428
    Top = 552
  end
  object QrFiConsSpc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 108
    Top = 532
    object QrFiConsSpcGraTamI_Dest: TIntegerField
      FieldName = 'GraTamI_Dest'
    end
    object QrFiConsSpcGraGruC_Dest: TIntegerField
      FieldName = 'GraGruC_Dest'
    end
    object QrFiConsSpcQtdUso: TFloatField
      FieldName = 'QtdUso'
    end
    object QrFiConsSpcNO_TAM_Sorc: TWideStringField
      FieldName = 'NO_TAM_Sorc'
      Size = 5
    end
    object QrFiConsSpcNO_COR_Sorc: TWideStringField
      FieldName = 'NO_COR_Sorc'
      Size = 30
    end
  end
  object QrGradesTam3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle Tam, Nome NOMETAM '
      'FROM gratamits'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 580
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTam3Tam: TAutoIncField
      FieldName = 'Tam'
    end
    object QrGradesTam3NOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Required = True
      Size = 5
    end
  end
  object QrGradesCor3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gc.Controle Cor, cc.Nome NOMECOR'
      'FROM gragruc gc'
      'LEFT JOIN gracorcad cc ON cc.Codigo=gc.GraCorCad'
      'WHERE gc.Nivel1=:P0'
      'ORDER BY NOMECOR')
    Left = 108
    Top = 580
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCor3Cor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrGradesCor3NOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
  end
  object QrSoliComprIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraTamI, ggx.GraGruC, ggx.GraGru1, pvi.*'
      'FROM solicomprits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Codigo=:P0'
      'AND gg1.Nivel1=:P1')
    Left = 180
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSoliComprItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSoliComprItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSoliComprItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSoliComprItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSoliComprItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSoliComprItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrSoliComprItsPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrSoliComprItsPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrSoliComprItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrSoliComprItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrSoliComprItsValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrSoliComprItsDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrSoliComprItsDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
    object QrSoliComprItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrSoliComprItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrSoliComprItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrSoliComprItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrSoliComprItsQuantS: TFloatField
      FieldName = 'QuantS'
      Required = True
    end
  end
  object QrMovOpn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, 0.0 Mov_ID, "Balan'#231'o" MovTXT'
      'FROM stqbalcad'
      'WHERE Encerrou < 2'
      ''
      'UNION'
      ''
      'SELECT Codigo, 5.0 Mov_ID, "Compra" MovTXT'
      'FROM stqinncad'
      'WHERE Encerrou < 2'
      ''
      'UNION'
      ''
      'SELECT Codigo, 4201.0 Mov_ID, "Remessa de consignado" MovTXT'
      'FROM stqcnsggocab'
      'WHERE Encerrou < 2'
      ''
      'UNION'
      ''
      'SELECT Codigo, 4203.0 Mov_ID, "Retorno de consignado" MovTXT'
      'FROM stqcnsgbkcab'
      'WHERE Encerrou < 2'
      ''
      'UNION'
      ''
      'SELECT Codigo, 4025.0 Mov_ID, "Venda de consignado" MovTXT'
      'FROM stqcnsgvecab'
      'WHERE Encerrou < 2'
      ''
      'UNION'
      ''
      'SELECT Codigo, 99.0 Mov_ID, "Movimento manual" MovTXT'
      'FROM stqmancad'
      'WHERE Encerrou < 2')
    Left = 600
    Top = 448
    object QrMovOpnCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMovOpnMov_ID: TFloatField
      FieldName = 'Mov_ID'
      Required = True
    end
    object QrMovOpnMovTXT: TWideStringField
      FieldName = 'MovTXT'
      Required = True
      Size = 21
    end
  end
  object DsMovOpn: TDataSource
    DataSet = QrMovOpn
    Left = 600
    Top = 496
  end
end
