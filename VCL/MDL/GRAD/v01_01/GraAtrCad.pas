unit GraAtrCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids,
  dmkDBGridZTO, Menus, dmkImage, UnDmkEnums;

type
  TFmGraAtrCad = class(TForm)
    PainelDados: TPanel;
    DsGraAtrCad: TDataSource;
    QrGraAtrCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrGraAtrCadCodigo: TIntegerField;
    QrGraAtrCadCodUsu: TIntegerField;
    QrGraAtrCadNome: TWideStringField;
    DBGItens: TdmkDBGridZTO;
    PMAtributos: TPopupMenu;
    Incluinovoatributo1: TMenuItem;
    Alteraatributoatual1: TMenuItem;
    ExcluiAtributoatual1: TMenuItem;
    PMItens: TPopupMenu;
    Incluinovoitemdeatributo1: TMenuItem;
    Alteraitemdeatributoatual1: TMenuItem;
    Excluiitemdeatributoatual1: TMenuItem;
    QrGraAtrIts: TmySQLQuery;
    DsGraAtrIts: TDataSource;
    QrGraAtrItsCodigo: TIntegerField;
    QrGraAtrItsControle: TIntegerField;
    QrGraAtrItsCodUsu: TIntegerField;
    QrGraAtrItsNome: TWideStringField;
    Panel4: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel7: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    LaRegistro: TStaticText;
    Panel8: TPanel;
    Panel9: TPanel;
    BtAtributos: TBitBtn;
    BtItens: TBitBtn;
    BtSaida: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraAtrCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraAtrCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure Incluinovoatributo1Click(Sender: TObject);
    procedure Alteraatributoatual1Click(Sender: TObject);
    procedure BtAtributosClick(Sender: TObject);
    procedure PMAtributosPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure QrGraAtrCadBeforeClose(DataSet: TDataSet);
    procedure QrGraAtrCadAfterScroll(DataSet: TDataSet);
    procedure Incluinovoitemdeatributo1Click(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Alteraitemdeatributoatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenGraAtrIts(Controle: Integer);
  end;

var
  FmGraAtrCad: TFmGraAtrCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, GraAtrIts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraAtrCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraAtrCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraAtrCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraAtrCad.DefParams;
begin
  VAR_GOTOTABELA := 'GraAtrCad';
  VAR_GOTOMYSQLTABLE := QrGraAtrCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome');
  VAR_SQLx.Add('FROM graatrcad');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraAtrCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraAtrCad', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmGraAtrCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraAtrCad.PMAtributosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab:= (QrGraAtrCad.State <> dsInactive) and (QrGraAtrCad.RecordCount > 0) and
           (QrGraAtrCadCodigo.Value <> 0);
  //
  Alteraatributoatual1.Enabled := Enab;
end;

procedure TFmGraAtrCad.PMItensPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrGraAtrCad.State <> dsInactive) and (QrGraAtrCad.RecordCount > 0)
             and (QrGraAtrCadCodigo.Value <> 0);
  Enab2 := (QrGraAtrIts.State <> dsInactive) and (QrGraAtrIts.RecordCount > 0);
  //
  Incluinovoitemdeatributo1.Enabled  := Enab1;
  Alteraitemdeatributoatual1.Enabled := Enab1 and Enab2;
end;

procedure TFmGraAtrCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmGraAtrCad.AlteraRegistro;
var
  GraAtrCad : Integer;
begin
  GraAtrCad := QrGraAtrCadCodigo.Value;
  if QrGraAtrCadCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(GraAtrCad, Dmod.MyDB, 'GraAtrCad', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GraAtrCad, Dmod.MyDB, 'GraAtrCad', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGraAtrCad.IncluiRegistro;
var
  Cursor : TCursor;
  GraAtrCad : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    GraAtrCad := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GraAtrCad', 'GraAtrCad', 'Codigo');
    if Length(FormatFloat(FFormatFloat, GraAtrCad))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, GraAtrCad);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmGraAtrCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraAtrCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraAtrCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraAtrCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraAtrCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraAtrCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraAtrCad.Alteraatributoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraAtrCad, [PainelDados],
    [PainelEdita], EdCodUsu, ImgTipo, 'graatrcad');
end;

procedure TFmGraAtrCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraAtrCadCodigo.Value;
  VAR_CAD_ITEM := QrGraAtrItsControle.Value;
  Close;
end;

procedure TFmGraAtrCad.Alteraitemdeatributoatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmGraAtrIts, FmGraAtrIts, afmoNegarComAviso,
    QrGraAtrIts, stUpd);
end;

procedure TFmGraAtrCad.BtAtributosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtributos, BtAtributos);
end;

procedure TFmGraAtrCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('GraAtrCad', 'Codigo', ImgTipo.SQLType,
    QrGraAtrCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGraAtrCad, PainelEdit,
    'GraAtrCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraAtrCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GraAtrCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraAtrCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GraAtrCad', 'Codigo');
end;

procedure TFmGraAtrCad.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmGraAtrCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  DBGItens.Align    := alClient;
  CriaOForm;
end;

procedure TFmGraAtrCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraAtrCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraAtrCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmGraAtrCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraAtrCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraAtrCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmGraAtrCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraAtrCad.QrGraAtrCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraAtrCad.QrGraAtrCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraAtrIts(0);
end;

procedure TFmGraAtrCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraAtrCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraAtrCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GraAtrCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraAtrCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraAtrCad.Incluinovoatributo1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrGraAtrCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'graatrcad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'GraAtrCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
end;

procedure TFmGraAtrCad.Incluinovoitemdeatributo1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmGraAtrIts, FmGraAtrIts, afmoNegarComAviso,
    QrGraAtrIts, stIns);
end;

procedure TFmGraAtrCad.QrGraAtrCadBeforeClose(DataSet: TDataSet);
begin
  QrGraAtrIts.Close;
end;

procedure TFmGraAtrCad.QrGraAtrCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraAtrCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGraAtrCad.ReopenGraAtrIts(Controle: Integer);
begin
  QrGraAtrIts.Close;
  QrGraAtrIts.Params[0].AsInteger :=   QrGraAtrCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraAtrIts, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrGraAtrIts.Locate('Controle', Controle, []);
end;

end.

