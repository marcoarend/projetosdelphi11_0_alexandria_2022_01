unit GraGru1Trf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables;

type
  TFmGraGru1Trf = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraGru1Src: TmySQLQuery;
    QrGraGru1SrcNivel1: TIntegerField;
    QrGraGru1SrcCodUsu: TIntegerField;
    QrGraGru1SrcNome: TWideStringField;
    DsGraGru1Src: TDataSource;
    QrGraGru1Dst: TmySQLQuery;
    DsGraGru1Dst: TDataSource;
    Panel5: TPanel;
    LaNivel2: TLabel;
    Label1: TLabel;
    EdNivel1Src: TdmkEditCB;
    CBNivel1Src: TdmkDBLookupComboBox;
    EdNivel1Dst: TdmkEditCB;
    CBNivel1Dst: TdmkDBLookupComboBox;
    DBGrid1: TDBGrid;
    QrGraGruC: TmySQLQuery;
    QrGraGruCNivel1: TIntegerField;
    QrGraGruCControle: TIntegerField;
    QrGraGruCGraCorCad: TIntegerField;
    QrGraGruCCodUsuGTC: TIntegerField;
    QrGraGruCNomeGTC: TWideStringField;
    DsGraGruC: TDataSource;
    QrGraGru1DstNivel1: TIntegerField;
    QrGraGru1DstCodUsu: TIntegerField;
    QrGraGru1DstNome: TWideStringField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    DsGraGruX: TDataSource;
    Panel37: TPanel;
    DBGrid2: TDBGrid;
    Panel38: TPanel;
    StaticText6: TStaticText;
    BtReduzidoL: TBitBtn;
    BtCodificacao: TBitBtn;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrGraGru1SrcAfterScroll(DataSet: TDataSet);
    procedure QrGraGruCAfterScroll(DataSet: TDataSet);
    procedure QrGraGruCBeforeClose(DataSet: TDataSet);
    procedure QrGraGru1SrcBeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruC(Controle: Integer);
    procedure ReopenGraGruX(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmGraGru1Trf: TFmGraGru1Trf;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmGraGru1Trf.BtOKClick(Sender: TObject);
const
  Nome = 'Livre (cadastro livre para uso)';
var
  CodUsuSrc, CodUsuDst: Integer;
  Nivel1Src, Nivel1Dst: Integer;
begin
  CodUsuSrc := EdNivel1Src.ValueVariant;
  CodUsuDst := EdNivel1Dst.ValueVariant;
  //
  if MyObjects.FIC(CodUsuSrc = 0, EdNivel1Src,
  'Informe um Nivel 1 de origem v�lido!') then
    Exit;
  if MyObjects.FIC(CodUsuDst = 0, EdNivel1Dst,
  'Informe um Nivel 1 de destino v�lido!') then
    Exit;
  //
  Nivel1Src := QrGraGru1SrcNivel1.Value;
  Nivel1Dst := QrGraGru1DstNivel1.Value;
  //
  if MyObjects.FIC(Nivel1Src < 1, EdNivel1Src,
  'Informe um Nivel 1 de origem v�lido!') then
    Exit;
  if MyObjects.FIC(Nivel1Dst < 1, EdNivel1Dst,
  'Informe um Nivel 1 de destino v�lido!') then
    Exit;
  //
  try
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragruc', False, [
    'Nivel1'], ['Nivel1'], [
    Nivel1Dst], [Nivel1Src], True) then
  except
    Geral.MB_Erro('N�o foi poss�vel alterar o "GraGruC"!');
  end;
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
    'GraGru1'], ['GraGru1'], [
    Nivel1Dst], [Nivel1Src], True) then
    begin
      if Geral.MB_Pergunta('Alerar o nome do Nivel 1 de origem para ' +
      sLineBreak + ' "' + Nome + '" ?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
        'Nome'], ['Nivel1'], [
        Nome], [Nivel1Src], True) then
          ;
      end;
      //
      UnDmkDAC_PF.AbreQuery(QrGraGru1Src, Dmod.MyDB);
    end;
  end;
end;

procedure TFmGraGru1Trf.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGru1Trf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGru1Trf.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGru1Src, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGru1Dst, Dmod.MyDB);
end;

procedure TFmGraGru1Trf.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGru1Trf.QrGraGru1SrcAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGruC(0);
end;

procedure TFmGraGru1Trf.QrGraGru1SrcBeforeClose(DataSet: TDataSet);
begin
  QrGraGruC.Close;
end;

procedure TFmGraGru1Trf.QrGraGruCAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGruX(0);
end;

procedure TFmGraGru1Trf.QrGraGruCBeforeClose(DataSet: TDataSet);
begin
  QrGraGruX.Close;
end;

procedure TFmGraGru1Trf.ReopenGraGruC(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruC, Dmod.MyDB, [
  'SELECT ggc.Nivel1, ggc.Controle, ggc.GraCorCad, ',
  'gtc.CodUsu CodUsuGTC, gtc.Nome NomeGTC ',
  'FROM gragruc ggc ',
  'LEFT JOIN gracorcad gtc ON gtc.Codigo=ggc.GraCorCad ',
  'WHERE ggc.Nivel1=' + Geral.FF0(QrGraGru1SrcNivel1.Value),
  '']);
  if Controle <> 0 then
  QrGraGruC.Locate('Controle', Controle, []);
end;

procedure TFmGraGru1Trf.ReopenGraGruX(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE ggx.GraGruC=' + Geral.FF0(QrGraGruCControle.Value),
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
  if Controle <> 0 then
  QrGraGruX.Locate('Controle', Controle, []);
end;

end.
