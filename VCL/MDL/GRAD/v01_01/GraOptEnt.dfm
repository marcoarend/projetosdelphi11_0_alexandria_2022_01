object FmGraOptEnt: TFmGraOptEnt
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
  ClientHeight = 544
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEditar: TPanel
    Left = 0
    Top = 92
    Width = 784
    Height = 452
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object PCEditar: TPageControl
      Left = 0
      Top = 48
      Width = 784
      Height = 193
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = ' Baixa de Uso e Consumo '
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 776
          Height = 76
          Align = alTop
          Caption = ' Custos em baixa de uso e consumo: '
          TabOrder = 0
          object RGBUCTipoPreco: TdmkRadioGroup
            Left = 2
            Top = 15
            Width = 363
            Height = 59
            Align = alLeft
            Caption = ' Origem pre'#231'os: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Estoque'
              'Cadastro em listas'
              'A definir')
            TabOrder = 0
            OnClick = RGBUCTipoPrecoClick
            QryCampo = 'BUCTipoPreco'
            UpdCampo = 'BUCTipoPreco'
            UpdType = utYes
            OldValor = 0
          end
          object GroupBox4: TGroupBox
            Left = 365
            Top = 15
            Width = 409
            Height = 59
            Align = alClient
            Caption = ' Pre'#231'os de origem de cadastro: '
            TabOrder = 1
            object Label4: TLabel
              Left = 8
              Top = 15
              Width = 76
              Height = 13
              Caption = 'Lista de Pre'#231'os:'
            end
            object EdBUCGraCusPrc: TdmkEditCB
              Left = 8
              Top = 31
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBBUCGraCusPrc
              IgnoraDBLookupComboBox = False
            end
            object CBBUCGraCusPrc: TdmkDBLookupComboBox
              Left = 64
              Top = 31
              Width = 337
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsGraCusPrc
              TabOrder = 1
              dmkEditCB = EdBUCGraCusPrc
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object DBEdit3: TDBEdit
        Left = 64
        Top = 20
        Width = 709
        Height = 21
        DataField = 'NO_ENT'
        DataSource = DsEntidades
        TabOrder = 1
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 388
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 636
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 2
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtOK: TBitBtn
          Tag = 14
          Left = 24
          Top = 4
          Width = 120
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtOKClick
        end
      end
    end
  end
  object PnMostra: TPanel
    Left = 0
    Top = 92
    Width = 784
    Height = 452
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 713
        Height = 21
        BevelOuter = bvNone
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntidades
        TabOrder = 0
        dmkEditCB = EdEntidade
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEntidade: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEntidadeChange
        DBLookupComboBox = CBEntidade
        IgnoraDBLookupComboBox = False
      end
    end
    object PCMostra: TPageControl
      Left = 0
      Top = 48
      Width = 784
      Height = 193
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Baixa de Uso e Consumo '
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 776
          Height = 76
          Align = alTop
          Caption = ' Custos em baixa de uso e consumo: '
          TabOrder = 0
          object DBRadioGroup1: TDBRadioGroup
            Left = 2
            Top = 15
            Width = 363
            Height = 59
            Align = alLeft
            Caption = ' Origem pre'#231'os: '
            Columns = 3
            DataField = 'BUCTipoPreco'
            DataSource = DsGraOptEnt
            Items.Strings = (
              'Estoque'
              'Cadastro em listas'
              'A definir')
            ParentBackground = True
            TabOrder = 0
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9')
          end
          object GroupBox2: TGroupBox
            Left = 365
            Top = 15
            Width = 409
            Height = 59
            Align = alClient
            Caption = ' Pre'#231'os de origem de cadastro: '
            TabOrder = 1
            object Label2: TLabel
              Left = 8
              Top = 15
              Width = 76
              Height = 13
              Caption = 'Lista de Pre'#231'os:'
            end
            object DBEdit1: TDBEdit
              Left = 8
              Top = 31
              Width = 53
              Height = 21
              DataField = 'CU_GCP'
              DataSource = DsGraOptEnt
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 64
              Top = 31
              Width = 337
              Height = 21
              DataField = 'NO_GCP'
              DataSource = DsGraOptEnt
              TabOrder = 1
            end
          end
        end
      end
    end
    object GroupBox5: TGroupBox
      Left = 0
      Top = 388
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel8: TPanel
          Left = 636
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BitBtn1: TBitBtn
            Tag = 13
            Left = 10
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn1Click
          end
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 8
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 210
        Height = 32
        Caption = 'Grade x Entidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 210
        Height = 32
        Caption = 'Grade x Entidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 210
        Height = 32
        Caption = 'Grade x Entidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT '
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 100
    Top = 272
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 100
    Top = 320
  end
  object QrGraOptEnt: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraOptEntAfterOpen
    BeforeClose = QrGraOptEntBeforeClose
    SQL.Strings = (
      'SELECT gcp.Nome NO_GCP, gcp.CodUsu CU_GCP, goe.* '
      'FROM graoptent goe'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=goe.BUCGraCusPrc'
      'WHERE goe.Codigo=:P0'
      '')
    Left = 176
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraOptEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraOptEntBUCTipoPreco: TSmallintField
      FieldName = 'BUCTipoPreco'
    end
    object QrGraOptEntBUCGraCusPrc: TIntegerField
      FieldName = 'BUCGraCusPrc'
    end
    object QrGraOptEntCU_GCP: TIntegerField
      FieldName = 'CU_GCP'
      Required = True
    end
    object QrGraOptEntNO_GCP: TWideStringField
      FieldName = 'NO_GCP'
      Size = 30
    end
  end
  object DsGraOptEnt: TDataSource
    DataSet = QrGraOptEnt
    Left = 176
    Top = 320
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT gcp.Codigo, gcp.CodUsu, gcp.Nome,'
      'ELT(CustoPreco+1, "Custo", "Pre'#231'o") CusPrc,'
      
        'ELT(TipoCalc+1, "Manual", "Pre'#231'o m'#233'dio", "'#218'ltima compra") NomeTC' +
        ','
      'mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA'
      'FROM gracusprc gcp '
      'LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda')
    Left = 720
    Top = 8
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraCusPrcCusPrc: TWideStringField
      FieldName = 'CusPrc'
      Size = 5
    end
    object QrGraCusPrcNomeTC: TWideStringField
      FieldName = 'NomeTC'
      Size = 13
    end
    object QrGraCusPrcNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrGraCusPrcSIGLAMOEDA: TWideStringField
      FieldName = 'SIGLAMOEDA'
      Size = 5
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 748
    Top = 8
  end
  object VUBUCGraCusPrc: TdmkValUsu
    dmkEditCB = EdBUCGraCusPrc
    Panel = PnEditar
    QryCampo = 'BUCGraCusPrc'
    UpdCampo = 'BUCGraCusPrc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 256
    Top = 272
  end
end
