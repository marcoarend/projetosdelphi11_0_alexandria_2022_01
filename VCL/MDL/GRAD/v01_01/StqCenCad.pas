unit StqCenCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmStqCenCad = class(TForm)
    PainelDados: TPanel;
    DsStqCenCad: TDataSource;
    QrStqCenCad: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMCondicoes: TPopupMenu;
    QrStqCenLoc: TmySQLQuery;
    DsStqCenLoc: TDataSource;
    Panel4: TPanel;
    Incluinovocentrodeestoque1: TMenuItem;
    Alteracentrodeestoqueatual1: TMenuItem;
    Excluicentrodeestoqueatual1: TMenuItem;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    PMLocais: TPopupMenu;
    Incluilocaldeestoque1: TMenuItem;
    Alteralocaldeestoqueselecionado1: TMenuItem;
    Excluilocaldeestoqueselecionado1: TMenuItem;
    QrStqCenLocCodigo: TIntegerField;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocCodUsu: TIntegerField;
    QrStqCenLocNome: TWideStringField;
    DBGrid1: TDBGrid;
    StaticText1: TStaticText;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    Label33: TLabel;
    EdEntiSitio: TdmkEditCB;
    CBEntiSitio: TdmkDBLookupComboBox;
    QrStqCenCadEntiSitio: TIntegerField;
    QrStqCenCadNO_ENTSITIO: TWideStringField;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    RGSitProd: TdmkRadioGroup;
    QrStqCenCadSitProd: TIntegerField;
    DBRadioGroup1: TDBRadioGroup;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCondicoes: TBitBtn;
    BtLocais: TBitBtn;
    BtFiliais: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrStqCenLocCarteira: TIntegerField;
    QrStqCenLocNO_CART: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqCenCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqCenCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtCondicoesClick(Sender: TObject);
    procedure PMCondicoesPopup(Sender: TObject);
    procedure QrStqCenCadBeforeClose(DataSet: TDataSet);
    procedure QrStqCenCadAfterScroll(DataSet: TDataSet);
    procedure Incluinovocentrodeestoque1Click(Sender: TObject);
    procedure Alteracentrodeestoqueatual1Click(Sender: TObject);
    procedure Incluilocaldeestoque1Click(Sender: TObject);
    procedure Alteralocaldeestoqueselecionado1Click(Sender: TObject);
    procedure BtLocaisClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMLocaisPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenStqCenLoc(Controle: Integer);
  end;

var
  FmStqCenCad: TFmStqCenCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, StqCenLoc, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqCenCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqCenCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqCenCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqCenCad.DefParams;
begin
  VAR_GOTOTABELA := 'StqCenCad';
  VAR_GOTOMYSQLTABLE := QrStqCenCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)');
  VAR_SQLx.Add('NO_ENTSITIO, scc.*');
  VAR_SQLx.Add('FROM stqcencad scc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=scc.EntiSitio');
  VAR_SQLx.Add('WHERE scc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND scc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND scc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND scc.Nome Like :P0');
  //
end;

procedure TFmStqCenCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCenCad', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqCenCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqCenCad.PMCondicoesPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrStqCenCad.State <> dsInactive) and (QrStqCenCad.RecordCount > 0);
  //
  Alteracentrodeestoqueatual1.Enabled := Enab;
end;

procedure TFmStqCenCad.PMLocaisPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrStqCenCad.State <> dsInactive) and (QrStqCenCad.RecordCount > 0);
  Enab2 := (QrStqCenLoc.State <> dsInactive) and (QrStqCenLoc.RecordCount > 0);
  //
  Incluilocaldeestoque1.Enabled            := Enab1;
  Alteralocaldeestoqueselecionado1.Enabled := Enab1 and Enab2;
end;

procedure TFmStqCenCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqCenCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqCenCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqCenCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqCenCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqCenCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqCenCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqCenCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqCenCadCodigo.Value;
  VAR_CADASTRO2 := QrStqCenLocControle.Value;
  Close;
end;

procedure TFmStqCenCad.Alteracentrodeestoqueatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrStqCenCad, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'StqCenCad');
end;

procedure TFmStqCenCad.Alteralocaldeestoqueselecionado1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmStqCenLoc, FmStqCenLoc, afmoNegarComAviso,
    QrStqCenLoc, stUpd);
end;

procedure TFmStqCenCad.BtCondicoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondicoes, BtCondicoes);
end;

procedure TFmStqCenCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(EdEntiSitio.ValueVariant = 0, EdEntiSitio, '') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('StqCenCad', 'Codigo', ImgTipo.SQLType,
    QrStqCenCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmStqCenCad, PainelEdit,
    'StqCenCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmStqCenCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'StqCenCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCenCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCenCad', 'Codigo');
end;

procedure TFmStqCenCad.BtLocaisClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLocais, BtLocais);
end;

procedure TFmStqCenCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  Panel4.Align      := alClient;
  BtFiliais.Visible := False;
  //
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmStqCenCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqCenCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCenCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmStqCenCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqCenCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqCenCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmStqCenCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);

end;

procedure TFmStqCenCad.QrStqCenCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmStqCenCad.QrStqCenCadAfterScroll(DataSet: TDataSet);
begin
  ReopenStqCenLoc(0);
end;

procedure TFmStqCenCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqCenCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqCenCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'StqCenCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqCenCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCenCad.Incluilocaldeestoque1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmStqCenLoc, FmStqCenLoc, afmoNegarComAviso,
    QrStqCenLoc, stIns);
end;

procedure TFmStqCenCad.Incluinovocentrodeestoque1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrStqCenCad, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'StqCenCad');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCenCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  EdNome.ValueVariant := '';
end;

procedure TFmStqCenCad.QrStqCenCadBeforeClose(DataSet: TDataSet);
begin
  QrStqCenLoc.Close;
end;

procedure TFmStqCenCad.QrStqCenCadBeforeOpen(DataSet: TDataSet);
begin
  QrStqCenCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqCenCad.ReopenStqCenLoc(Controle: Integer);
begin
  QrStqCenLoc.Close;
  QrStqCenLoc.Params[0].AsInteger :=   QrStqCenCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrStqCenLoc.Locate('Controle', Controle, []);
end;

end.

