object FmGraImpConflitos: TFmGraImpConflitos
  Left = 339
  Top = 185
  Caption = 'PRD-PRINT-002 :: Impress'#227'o de Conflitos De Medidas'
  ClientHeight = 519
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 363
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object RGRelatorio: TRadioGroup
      Left = 0
      Top = 0
      Width = 784
      Height = 72
      Align = alTop
      Caption = ' Relat'#243'rios: '
      Items.Strings = (
        
          '"Cadastros de Medidas"  X  "Unidade de Medida"  X  "Unidade cont' +
          'roladora do estoque".'
        
          'Lan'#231'amentos de mat'#233'ria-prima no estoque sem correspond'#234'ncia na e' +
          'ntrade de couros')
      TabOrder = 0
      OnClick = RGRelatorioClick
    end
    object DBGConfli_0: TDBGrid
      Left = 0
      Top = 72
      Width = 784
      Height = 291
      Align = alClient
      DataSource = DsConfli_0
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 431
        Height = 32
        Caption = 'Impress'#227'o de Conflitos De Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 431
        Height = 32
        Caption = 'Impress'#227'o de Conflitos De Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 431
        Height = 32
        Caption = 'Impress'#227'o de Conflitos De Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 411
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 455
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtImprime: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object QrConfli_0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT DISTINCT smia.GraGruX, gg1.CodUsu CU_Nivel1, LEFT(gg1.Nom' +
        'e, 40)  NO_PRD,'
      'unm.Sigla, gg1.GerBxaEstq, '
      
        'ELT(gg1.GerBxaEstq+1,  '#39'? ? ?'#39', '#39'Pe'#231'a'#39', '#39'm'#178#39', '#39'kg'#39', '#39't'#39', '#39'ft'#178#39') ' +
        'NO_GBE, '
      ''
      'unm.Grandeza, ELT(unm.Grandeza+1,'
      #39'Pe'#231'a'#39', '#39#193'rea (m'#178', ft'#178')'#39', '#39'Peso (kg, t)'#39', '#39'Volume (L, m'#179')'#39', '
      #39'Linear (m)'#39', '#39'? ? ?'#39') NO_GRAND,'
      ''
      '/*((unm.Grandeza * 10) + gg1.GerBxaEstq)  RELACAO,*/'
      ''
      
        'CASE WHEN ((unm.Grandeza * 10) + gg1.GerBxaEstq) =  0 THEN "UCE ' +
        'n'#227'o definida!"'
      'WHEN ((unm.Grandeza * 10) + gg1.GerBxaEstq) IN  (1,12,15,23,24) '
      
        'THEN IF(UPPER(unm.Sigla) = UPPER(ELT(gg1.GerBxaEstq+1,  '#39'? ? ?'#39',' +
        ' '#39'Pe'#231'a'#39', '#39'm'#178#39', '
      
        #39'kg'#39', '#39't'#39', '#39'ft'#178#39')), "OK", CONCAT("Ver compatibilidade entre ",un' +
        'm.Sigla,'
      
        '" e ", ELT(gg1.GerBxaEstq+1,  '#39'? ? ?'#39', '#39'Pe'#231'a'#39', '#39'm'#178#39', '#39'kg'#39', '#39't'#39', ' +
        #39'ft'#178#39')))'
      'ELSE "Poss'#237'vel conflito" END NO_RELACAO'
      ''
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed'
      'WHERE Tipo IN (101,104,107,108)'
      'AND smia.GraGruX <> 0')
    Left = 720
    Top = 60
    object QrConfli_0GraGruX: TIntegerField
      DisplayLabel = 'Reduzido'
      FieldName = 'GraGruX'
      Origin = 'stqmovitsa.GraGruX'
    end
    object QrConfli_0CU_Nivel1: TIntegerField
      DisplayLabel = 'C'#243'd. Nivel1'
      FieldName = 'CU_Nivel1'
      Origin = 'gragru1.CodUsu'
    end
    object QrConfli_0NO_PRD: TWideStringField
      DisplayLabel = 'Nome do Produto'
      FieldName = 'NO_PRD'
      Origin = 'NO_PRD'
      Size = 40
    end
    object QrConfli_0Sigla: TWideStringField
      FieldName = 'Sigla'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrConfli_0GerBxaEstq: TSmallintField
      DisplayLabel = 'C'#243'd. GBE'
      FieldName = 'GerBxaEstq'
      Origin = 'gragru1.GerBxaEstq'
    end
    object QrConfli_0NO_GBE: TWideStringField
      DisplayLabel = 'Descr. GBE'
      FieldName = 'NO_GBE'
      Origin = 'NO_GBE'
      Size = 5
    end
    object QrConfli_0Grandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'unidmed.Grandeza'
    end
    object QrConfli_0NO_GRAND: TWideStringField
      DisplayLabel = 'Descri'#231#227'o Grandeza'
      FieldName = 'NO_GRAND'
      Origin = 'NO_GRAND'
      Size = 18
    end
    object QrConfli_0NO_RELACAO: TWideStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'NO_RELACAO'
      Size = 40
    end
  end
  object DsConfli_0: TDataSource
    DataSet = QrConfli_0
    Left = 748
    Top = 60
  end
  object frxConfli_0: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 664
    Top = 60
    Datasets = <
      item
        DataSet = frxDsConfli_0
        DataSetName = 'frxDsConfli_0'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 92.598476460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'An'#225'lise de Conflitos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CLASSIFICA'#199#195'O E RECLASSIFICA'#199#195'O')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Relacionamentos entre "Cadastro de Medidas" X "Unidade de Medida' +
              ' do Produto" X "Unidade Controladora de Estoque"')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Top = 79.370130000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 37.795275590000000000
          Top = 79.370130000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd. Nivel1')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 83.149606300000000000
          Top = 79.370130000000000000
          Width = 151.181102362204700000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Produto')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 234.330620790000000000
          Top = 79.370130000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 272.125896380000000000
          Top = 79.370130000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd. UCE')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 309.921169530000000000
          Top = 79.370130000000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descr. UCE')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 370.393700787401600000
          Top = 79.370130000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Grandeza')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 408.188976380000000000
          Top = 79.370130000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o Grandeza')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 483.779840000000000000
          Top = 79.370130000000000000
          Width = 196.535433070866100000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o Grandeza')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 32.125996460000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          Top = 18.897650000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 362.834880000000000000
          Top = 18.897650000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'UCE: Unidade controladora do estoque de produtos classificados e' +
              ' reclassificados. ')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 13.228346460000000000
        ParentFont = False
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        DataSet = frxDsConfli_0
        DataSetName = 'frxDsConfli_0'
        RowCount = 0
        object Memo10: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'GraGruX'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfli_0."GraGruX"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Left = 37.795275590000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CU_Nivel1'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfli_0."CU_Nivel1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 83.149606300000000000
          Width = 151.181102362204700000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NO_PRD'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConfli_0."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 234.330620790000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Sigla'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConfli_0."Sigla"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 272.125896380000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'GerBxaEstq'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfli_0."GerBxaEstq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 309.921169530000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NO_GBE'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConfli_0."NO_GBE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 370.393700787401600000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Grandeza'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConfli_0."Grandeza"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 408.188976380000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NO_GRAND'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConfli_0."NO_GRAND"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 483.779840000000000000
          Width = 196.535433070866100000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NO_RELACAO'
          DataSet = frxDsConfli_0
          DataSetName = 'frxDsConfli_0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.LeftLine.Width = 0.100000000000000000
          Frame.TopLine.Width = 0.100000000000000000
          Frame.RightLine.Width = 0.100000000000000000
          Frame.BottomLine.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConfli_0."NO_RELACAO"]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsConfli_0: TfrxDBDataset
    UserName = 'frxDsConfli_0'
    CloseDataSource = False
    DataSet = QrConfli_0
    BCDToCurrency = False
    Left = 692
    Top = 60
  end
end
