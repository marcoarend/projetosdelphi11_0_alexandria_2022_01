unit StqBalLeiIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, ComCtrls,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, Variants, Mask,
  dmkEditCalc, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmStqBalLeiIts = class(TForm)
    Panel1: TPanel;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    PnSeleciona: TPanel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    PCModo: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    PnLeitura: TPanel;
    Panel4: TPanel;
    PnMultiGrandeza: TPanel;
    Panel6: TPanel;
    QrItem: TmySQLQuery;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    QrItemMadeBy: TSmallintField;
    QrItemFracio: TSmallintField;
    QrItemHowBxaEstq: TSmallintField;
    QrItemGerBxaEstq: TSmallintField;
    QrItemprod_indTot: TSmallintField;
    DsItem: TDataSource;
    QrPreco: TmySQLQuery;
    QrPrecoPrecoF: TFloatField;
    QrPrecoQuantF: TFloatField;
    QrPrecoInfAdCuztm: TIntegerField;
    QrPrecoPercCustom: TFloatField;
    QrPrecoMedidaC: TFloatField;
    QrPrecoMedidaL: TFloatField;
    QrPrecoMedidaA: TFloatField;
    QrPrecoMedidaE: TFloatField;
    QrPrecoMedOrdem: TIntegerField;
    DsPreco: TDataSource;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit4: TDBEdit;
    Panel3: TPanel;
    Panel5: TPanel;
    Label3: TLabel;
    EdLeitura: TEdit;
    EdQtdLei: TdmkEdit;
    Panel7: TPanel;
    Panel8: TPanel;
    EdPecas: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    EdPeso: TdmkEdit;
    Panel12: TPanel;
    Panel14: TPanel;
    QrSoma: TmySQLQuery;
    QrSomaQtde: TFloatField;
    QrSomaPecas: TFloatField;
    QrSomaPeso: TFloatField;
    QrSomaAreaM2: TFloatField;
    QrSomaAreaP2: TFloatField;
    DsSoma: TDataSource;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    EdQtdFut: TdmkEdit;
    EdPecasFut: TdmkEdit;
    EdAreaM2Fut: TdmkEditCalc;
    EdAreaP2Fut: TdmkEditCalc;
    EdPesoFut: TdmkEdit;
    LaQtdeLei: TLabel;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    LaPeso: TLabel;
    BtOK2: TBitBtn;
    Label2: TLabel;
    Label7: TLabel;
    Panel10: TPanel;
    LaInforme1: TLabel;
    LaInforme2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel11: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel15: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    CkContinuar: TCheckBox;
    Label8: TLabel;
    DBEdit11: TDBEdit;
    EdQtdVal: TdmkEdit;   // 2023-08-11 e 12
    QrSomaValorAll: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeCClick(Sender: TObject);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure EdQtdLeiChange(Sender: TObject);
    procedure BtOK2Click(Sender: TObject);
    procedure EdLeituraChange(Sender: TObject);
    procedure EdLeituraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrItemAfterScroll(DataSet: TDataSet);
    procedure EdPecasChange(Sender: TObject);
    procedure EdAreaM2Change(Sender: TObject);
    procedure EdAreaP2Change(Sender: TObject);
    procedure EdPesoChange(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPecasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaP2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBEdit5Change(Sender: TObject);
    procedure EdQtdValExit(Sender: TObject);
    procedure EdGraGru1Redefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReconfiguraGradeQ();
    // Qtde de Itens
    function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    function ObtemQtde(var Qtde: Integer): Boolean;
    function ReopenItem(GraGruX: Integer): Boolean;
    procedure CalculaSaldoFuturo();
    procedure BtOKClick_Selecao();
    procedure BtOKClick_Leitura();


  public
    { Public declarations }
  end;

  var
  FmStqBalLeiIts: TFmStqBalLeiIts;

implementation

uses UnMyObjects, Module, ModProd, MyVCLSkin, GetValor, UMySQLModule,
  UnInternalConsts, ModuleGeral, StqBalCad, StqBalLei, StqCenLoc,
  UnitListasGrade, MyListas;

{$R *.DFM}

const
  FCasas = 3;
  
procedure TFmStqBalLeiIts.BtOK2Click(Sender: TObject);
var
  //Col, Row, Tot, StqCenCad, 
  Codigo, Controle, Tipo, StqCenLoc, GraGruX: Integer;
  QtdLei, QtdAnt: Double;
  //Texto,
  DataHora: String;
  //
  NeedQI, NeedQA, NeedQK: Boolean;
  PecasLei, PesoLei, AreaM2Lei, AreaP2Lei,
  PecasAnt, PesoAnt, AreaM2Ant, AreaP2Ant: Double;
  Erro: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Tipo   := QrGraGru1PrdGrupTip.Value;
    //
    if FmStqBalCad.QrStqBalCadForcada.Value = 1 then
      DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', FmStqBalCad.QrStqBalCadAbertura.Value)
    else
      DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    //StqCenCad := FmStqBalCad.QrStqBalCadStqCenCad.Value;
    StqCenLoc := FmStqBalLei.QrStqCenLocControle.Value;
    Codigo    := FmStqBalCad.QrStqBalCadCodigo.Value;
    //
    if Length(Trim(EdLeitura.Text)) < 10 then
    begin
      GraGruX := Geral.IMV(EdLeitura.Text);
      if MyObjects.FIC(GraGruX = 0, EdLeitura, 'Informe a Leitura / digita��o!') then
          Exit;
      if MyObjects.FIC((QrItem.State = dsInactive) or (QrItem.RecordCount = 0),
        EdLeitura, 'Reduzido n�o localizado!') then
          Exit;
    end else
    begin
      Geral.MensagemBox('Reduzido inv�lido! (Texto muito grande)',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
{
    Tot    := 0;
    Texto := '';
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
            Tot := Tot + Geral.IMV(GradeQ.Cells[Col, Row]);
          if Trim(GradeX.Cells[0,Row]) <> '' then
            Texto := Texto + Trim(GradeX.Cells[0,Row]);
        end;
      end;
    end;
    if Trim(Texto) = '' then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'N�o h� sele��o de itens!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;

    PB1.Position := 0;
    PB1.Max := Tot;
}
{
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) <> 0 then
          begin
            QtdLei := Geral.DMV(GradeQ.Cells[Col, Row]);
            if QtdLei >= 0 then
            begin
              GraGruX := Geral.IMV(GradeC.Cells[Col,Row]);
              if GraGruX <> 0 then
              begin
                // Saldo Anterior
                QrSoma.Close;
                QrSoma.Params[00].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
                QrSoma.Params[01].AsInteger := StqCenCad;
                QrSoma.Params[02].AsInteger := GraGruX;
                UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
                QtdAnt := QrSomaQtde.Value;
                //
                Controle := DModG.BuscaProximoInteiro('stqbalits', 'Controle', '', 0);
                //
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalits', False, [
                'Tipo', 'DataHora', 'StqCenLoc', 'GraGruX', 'QtdLei', 'QtdAnt'], [
                'Codigo', 'Controle'], [
                Tipo, DataHora, StqCenLoc, GraGruX, QtdLei, QtdAnt], [
                Codigo, Controle], False) then
                  FmStqBalLei.ReopenLidos(Controle);
              end;
            end;
          end;
        end;
      end;
    end;
}
    QtdLei := EdQtdLei.ValueVariant;
    //
    if FmStqBalCad.FMultiGrandeza then
    begin
      NeedQI := Geral.IntInConjunto(1, QrItemHowBxaEstq.Value);
      NeedQA := Geral.IntInConjunto(2, QrItemHowBxaEstq.Value);
      NeedQK := Geral.IntInConjunto(4, QrItemHowBxaEstq.Value);
      //
      PecasLei  := EdPecas.ValueVariant;
      PesoLei   := EdPeso.ValueVariant;
      AreaM2Lei := EdAreaM2.ValueVariant;
      AreaP2Lei := EdAreaP2.ValueVariant;
      //
      Erro   := '';
      if (PecasLei  = 0) and NeedQI then Erro := 'Pe�as';
      if (AreaM2Lei = 0) and NeedQA then Erro := '�rea';
      if (PesoLei   = 0) and NeedQK then Erro := 'Peso';
      if Erro <> '' then
      begin
        Geral.MensagemBox('Grandeza obrigat�ria: "' + Erro + '" para o item '
        + IntToStr(QrItem.RecNo) + '.', 'Aviso', MB_OK+MB_ICONWARNING);
        Screen.Cursor := crHourGlass;
        QrItem.EnableControls;
        Exit;
      end;
      // Saldo Anterior
{
      QrSoma.Close;
      QrSoma.Params[00].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
      QrSoma.Params[01].AsInteger := StqCenCad;
      QrSoma.Params[02].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
}
      QtdAnt := QrSomaQtde.Value;
      PecasAnt := QrSomaPecas.Value;
      PesoAnt := QrSomaPeso.Value;
      AreaM2Ant := QrSomaPecas.Value;
      AreaP2Ant := QrSomaPecas.Value;
      //
      Controle := DModG.BuscaProximoInteiro('stqbalits', 'Controle', '', 0);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalits', False, [
      'Tipo', 'DataHora', 'StqCenLoc', 'GraGruX', 'QtdLei', 'QtdAnt',
      'PecasLei', 'PecasAnt', 'PesoLei', 'PesoAnt', 'AreaM2Lei',
      'AreaM2Ant', 'AreaP2Lei', 'AreaP2Ant'], [
      'Codigo', 'Controle'], [
      Tipo, DataHora, StqCenLoc, GraGruX, QtdLei, QtdAnt,
      PecasLei, PecasAnt, PesoLei, PesoAnt, AreaM2Lei,
      AreaM2Ant, AreaP2Lei, AreaP2Ant], [
      Codigo, Controle], False) then
      begin
        FmStqBalLei.ReopenLidos(Controle);
{
        QrItem.Close;
        EdLeitura.Text := '';
        EdQtdLei.ValueVariant := 0;
}
      end;
    end;
    //
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmStqBalLeiIts.BtOKClick(Sender: TObject);
begin
  if PCModo.ActivePageIndex = 1 then
    BtOKClick_Leitura()
  else
    BtOKClick_Selecao();
end;

procedure TFmStqBalLeiIts.BtOKClick_Leitura(); // 2023-08-11 e 12
var
  Codigo, Controle, GraGruX, Tipo, StqCenLoc: Integer;
  QtdLei, QtdAnt, CustoAll: Double;
  DataHora: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Tipo := QrGraGru1PrdGrupTip.Value;
    //
    if FmStqBalCad.QrStqBalCadForcada.Value = 1 then
      DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', FmStqBalCad.QrStqBalCadAbertura.Value)
    else
      DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    //StqCenCad := FmStqBalCad.QrStqBalCadStqCenCad.Value;
    StqCenLoc := FmStqBalLei.QrStqCenLocControle.Value;
    Codigo    := FmStqBalCad.QrStqBalCadCodigo.Value;
    QtdLei    := EdQtdLei.ValueVariant;
    CustoAll  := EdQtdVal.ValueVariant;  // 2023-08-11 e 12
    if QtdLei >= 0 then
    begin
      if Length(Trim(EdLeitura.Text)) < 10 then
      begin
        GraGruX := Geral.IMV(EdLeitura.Text);
        if MyObjects.FIC(GraGruX = 0, EdLeitura, 'Informe a Leitura / digita��o!') then
            Exit;
        if MyObjects.FIC((QrItem.State = dsInactive) or (QrItem.RecordCount = 0),
          EdLeitura, 'Reduzido n�o localizado!') then
            Exit;
      end else
      begin
        Geral.MensagemBox('Reduzido inv�lido! (Texto muito grande)',
        'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
      if GraGruX <> QrItemGraGruX.Value then
      begin
        Geral.MB_Aviso(
        'N�o foi poss�vel definir o reduzido corretamente selecionado!' +
        sLineBreak + 'Selecione novamente!');
        Exit;
      end;
      if GraGruX <> 0 then
      begin
        // Saldo Anterior
        QtdAnt := QrSomaQtde.Value;
        //
        Controle := DModG.BuscaProximoInteiro('stqbalits', 'Controle', '', 0);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalits', False, [
        'Tipo', 'DataHora', 'StqCenLoc',
        'GraGruX', 'QtdLei', 'QtdAnt',
        'CustoAll'], [
        'Codigo', 'Controle'], [
        Tipo, DataHora, StqCenLoc,
        GraGruX, QtdLei, QtdAnt,
        CustoAll], [
        Codigo, Controle], False)
        then
          FmStqBalLei.ReopenLidos(Controle);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  if CkContinuar.Checked = True then
  begin
    EdGraGru1.ValueVariant := 0;
    CBGraGru1.KeyValue     := Null;
    EdGraGru1.SetFocus;
    //
    EdQtdLei.ValueVariant := 0;
    EdQtdVal.ValueVariant := 0;  // 2023-08-11 e 12
  end else
    Close;
end;

procedure TFmStqBalLeiIts.BtOKClick_Selecao();
var
  //StqCenCad,
  Col, Row, Codigo, Tot, Controle, Tipo, StqCenLoc, GraGruX: Integer;
  QtdLei, QtdAnt: Double;
  DataHora, Texto: String;
{
  i, Sequencia: Integer;
  Txt1, Txt2: String;
}
begin
  Screen.Cursor := crHourGlass;
  try
    Tipo := QrGraGru1PrdGrupTip.Value;
    //
    if FmStqBalCad.QrStqBalCadForcada.Value = 1 then
      DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', FmStqBalCad.QrStqBalCadAbertura.Value)
    else
      DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    //StqCenCad := FmStqBalCad.QrStqBalCadStqCenCad.Value;
    StqCenLoc := FmStqBalLei.QrStqCenLocControle.Value;
    Codigo    := FmStqBalCad.QrStqBalCadCodigo.Value;
    //
    Tot    := 0;
    Texto := '';
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
            Tot := Tot + Geral.IMV(GradeQ.Cells[Col, Row]);
          if Trim(GradeX.Cells[0,Row]) <> '' then
            Texto := Texto + Trim(GradeX.Cells[0,Row]);
        end;
      end;
    end;
    if Trim(Texto) = '' then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'N�o h� sele��o de itens!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    PB1.Position := 0;
    PB1.Max := Tot;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) <> 0 then
          begin
            QtdLei := Geral.DMV(GradeQ.Cells[Col, Row]);
            if QtdLei >= 0 then
            begin
              GraGruX := Geral.IMV(GradeC.Cells[Col,Row]);
              if GraGruX <> 0 then
              begin
                // Saldo Anterior
{
                QrSoma.Close;
                QrSoma.Params[00].AsInteger := FmStqBalCad.QrStqBalCadEmpresa.Value;
                QrSoma.Params[01].AsInteger := StqCenCad;
                QrSoma.Params[02].AsInteger := GraGruX;
                UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
}
                QtdAnt := QrSomaQtde.Value;
                //
                Controle := DModG.BuscaProximoInteiro('stqbalits', 'Controle', '', 0);
                //
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalits', False,
                  ['Tipo', 'DataHora', 'StqCenLoc', 'GraGruX', 'QtdLei', 'QtdAnt'],
                  ['Codigo', 'Controle'],
                  [Tipo, DataHora, StqCenLoc, GraGruX, QtdLei, QtdAnt],
                  [Codigo, Controle], False)
                then
                  FmStqBalLei.ReopenLidos(Controle);
              end;
              {
              if Codigo = 0 then
              begin
                Codigo := UMyMod.BuscaEmLivreY_Def('etqgeralot', 'Codigo', stIns, 0);
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'etqgeralot', False, [
                'Impresso', 'Nivel1'], ['Codigo'], [0, QrGraGru1Nivel1.Value],
                [Codigo], True) then
                begin
                  Txt1 := 'INSERT INTO etqgeraits SET Codigo=' +
                    dmkPF.FFP(Codigo, 0) + ', GraGruX=';
                end;
              end;
              GraGruX := Geral.IMV(GradeC.Cells[Col,Row]);
              if GraGruX > 0 then
              begin
                Txt2 := dmkPF.FFP(GraGruX, 0) + ', Sequencia=';
                QrLoc.Close;
                QrLoc.Params[0].AsInteger := GraGruX;
                UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
                Sequencia := QrLocUltimo.Value;
                for i := 1 to Qtd do
                begin
                  PB1.Position := PB1.Position + 1;
                  Update;
                  Application.ProcessMessages;
                  Sequencia := Sequencia + 1;
                  Dmod.MyDB.Execute(Txt1 + Txt2 + dmkPF.FFP(Sequencia, 0));
                end;
              end;
              }
            end;
          end;
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  if CkContinuar.Checked = True then
  begin
    EdGraGru1.ValueVariant := 0;
    CBGraGru1.KeyValue     := Null;
    EdGraGru1.SetFocus;
  end else
    Close;
end;

procedure TFmStqBalLeiIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqBalLeiIts.CalculaSaldoFuturo();
begin
  if QrSoma.State = dsInactive then
  begin
    EdQtdFut.ValueVariant := 0;
    EdPecasFut.ValueVariant := 0;
    EdAreaM2Fut.ValueVariant := 0;
    EdAreaP2Fut.ValueVariant := 0;
    EdPesoFut.ValueVariant := 0;
  end else
  begin
    EdQtdFut.ValueVariant := QrSomaQtde.Value + EdQtdLei.ValueVariant;
    EdPecasFut.ValueVariant := QrSomaPecas.Value + EdPecas.ValueVariant;
    EdAreaM2Fut.ValueVariant := QrSomaAreaM2.Value + EdAreaM2.ValueVariant;
    EdAreaP2Fut.ValueVariant := QrSomaAreaP2.Value + EdAreaP2.ValueVariant;
    EdPesoFut.ValueVariant := QrSomaPeso.Value + EdPeso.ValueVariant;
  end;
end;

procedure TFmStqBalLeiIts.DBEdit5Change(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmStqBalLeiIts.EdAreaM2Change(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmStqBalLeiIts.EdAreaM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: Double;
begin
  if Key=VK_F4 then
  begin
    if dmkPF.ObtemValorDouble(Valor, FCasas) then
      EdAreaM2.ValueVariant := Valor - QrSomaAreaM2.Value;
  end;
end;

procedure TFmStqBalLeiIts.EdAreaP2Change(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmStqBalLeiIts.EdAreaP2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: Double;
begin
  if Key=VK_F4 then
  begin
    if dmkPF.ObtemValorDouble(Valor, FCasas) then
      EdAreaP2.ValueVariant := Valor - QrSomaAreaP2.Value;
  end;
end;

procedure TFmStqBalLeiIts.EdGraGru1Change(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdGraGru1.ValueVariant <> Null)
  and
    (EdGraGru1.ValueVariant <> 0);
  //
  PnLeitura.Enabled := Habilita;
  //BtFatura.Enabled  := BtFatura.Visible and Habilita;
  //
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmStqBalLeiIts.EdGraGru1Exit(Sender: TObject);
begin
  ReconfiguraGradeQ();
end;

procedure TFmStqBalLeiIts.EdGraGru1Redefinido(Sender: TObject);
begin
  // 2023-08-11 e 12
  EdLeitura.Text := '';
  EdQtdLei.ValueVariant := 0;
  EdQtdVal.ValueVariant := 0;
end;

procedure TFmStqBalLeiIts.EdLeituraChange(Sender: TObject);
var
  Tam, StqCenCad, GraGruX: Integer;
  //Qtde: Double;
begin
  Tam := Length(EdLeitura.Text);
  QrSoma.Close;
  if Tam < 10 then
  begin
    StqCenCad := FmStqBalCad.QrStqBalCadStqCenCad.Value;
    GraGruX := Geral.IMV(EdLeitura.Text);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
    'SELECT SUM(Qtde * Baixa) Qtde, SUM(Pecas) Pecas,  SUM(Peso) Peso, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, SUM(ValorAll) ValorAll ',
    'FROM stqmovitsa ',
    'WHERE Empresa=' + Geral.FF0(FmStqBalCad.QrStqBalCadEmpresa.Value),
    'AND StqCenCad=' + Geral.FF0(StqCencad),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND Ativo=1',        // 2020-11-22
    'AND ValiStq <> 101',  // 2020-11-22
    '']);
  end;
  CalculaSaldoFuturo();
  if Tam = 20 then
  begin
    if ReopenItem(Geral.IMV(Copy(EdLeitura.Text, 7, 6))) then
(*
      if CkFixo.Checked then
      begin
        FTam20 := True;
        Qtde := Geral.DMV(EdQtdLei.Text);
        InsereItem2(Qtde);
        FmFatPedCab.ReopenFatPedIts(FOriCtrl);
        ReopenPediGru(QrPediGruNivel1.Value);
        if PnLeitura.Enabled then
          EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end;
*)
    ;
  end else
  (*if Tam = 0 then*)
    QrItem.Close;
end;

procedure TFmStqBalLeiIts.EdLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    if Length(EdLeitura.Text) <= 6 then
      ReopenItem(Geral.IMV(EdLeitura.Text))
    else Geral.MensagemBox('Quantidade de caracteres inv�lida para ' +
    'localiza��o pelo reduzido!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmStqBalLeiIts.EdPecasChange(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmStqBalLeiIts.EdPecasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: Double;
begin
  if Key=VK_F4 then
  begin
    if dmkPF.ObtemValorDouble(Valor, FCasas) then
      EdPecas.ValueVariant := Valor - QrSomaPecas.Value;
  end;
end;

procedure TFmStqBalLeiIts.EdPesoChange(Sender: TObject);
begin
  CalculaSaldoFuturo();
end;

procedure TFmStqBalLeiIts.EdPesoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: Double;
begin
  if Key=VK_F4 then
  begin
    if dmkPF.ObtemValorDouble(Valor, FCasas) then
      EdPeso.ValueVariant := Valor - QrSomaPeso.Value;
  end;
end;

procedure TFmStqBalLeiIts.EdQtdLeiChange(Sender: TObject);
begin
  if (EdQtdLei.ValueVariant >= 0.001) or (EdQtdLei.ValueVariant <= -0.001) then
  begin
    if FmStqBalCad.FMultiGrandeza then
    begin
      PnMultiGrandeza.Visible := True;
      case QrItemGerBxaEstq.Value of
        1: EdPecas.ValueVariant  := EdQtdLei.ValueVariant;
        2: EdAreaM2.ValueVariant := EdQtdLei.ValueVariant;
        3: EdPeso.ValueVariant   := EdQtdLei.ValueVariant;
        4: EdPeso.ValueVariant   := EdQtdLei.ValueVariant;
        5: EdAreaP2.ValueVariant := EdQtdLei.ValueVariant;
        else ;
      end;
    end;
  end else
  begin
    PnMultiGrandeza.Visible := False;
    EdPecas.ValueVariant  := 0;
    EdAreaM2.ValueVariant := 0;
    EdPeso.ValueVariant   := 0;
  end;
  CalculaSaldoFuturo();
end;

procedure TFmStqBalLeiIts.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Valor: Double;
begin
  if Key=VK_F4 then
  begin
    if dmkPF.ObtemValorDouble(Valor, FCasas) then
      EdQtdLei.ValueVariant := Valor - QrSomaQtde.Value;
      //
      case QrItemGerBxaEstq.Value of
        1: EdPecas.ValueVariant  := Valor - QrSomaPecas.Value;
        2: EdAreaM2.ValueVariant := Valor - QrSomaAreaM2.Value;
        3: EdPeso.ValueVariant   := Valor - QrSomaPeso.Value;
        4: EdPeso.ValueVariant   := Valor - QrSomaPeso.Value;
        5: EdAreaP2.ValueVariant := Valor - QrSomaAreaP2.Value;
        else ;
      end;
  end;
end;

procedure TFmStqBalLeiIts.EdQtdValExit(Sender: TObject);
begin
  BtOK.SetFocus;   // 2023-08-11 e 12
end;

procedure TFmStqBalLeiIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqBalLeiIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //PnMultiGrandeza.Visible := FmStqBalCad.FMultiGrandeza;
  PCModo.ActivePageIndex := 1;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  QrGraGru1.Close;
  QrGraGru1.Params[0].AsInteger := FmStqBalCad.QrStqBalCadPrdGrupTip.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  //
  MyObjects.ConfiguraF7AppCBGraGru1(CBGraGru1);
end;

procedure TFmStqBalLeiIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqBalLeiIts.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnSeleciona.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnSeleciona.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmStqBalLeiIts.GradeCClick(Sender: TObject);
begin
  EdLeitura.Text :=
    dmkPF.FFP(Geral.IMV(GradeC.Cells[GradeC.Col, GradeC.Row]), 0);
  if Geral.SoNumero1a9_TT(EdLeitura.Text) <> '' then
  begin
    ReopenItem(Geral.IMV(EdLeitura.Text));
    EdQtdLei.ValueVariant := 0;
    EdQtdVal.ValueVariant := 0;  // 2023-08-11 e 12
  end;
end;

procedure TFmStqBalLeiIts.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnSeleciona.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmStqBalLeiIts.GradeQDblClick(Sender: TObject);
begin
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
end;

procedure TFmStqBalLeiIts.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
  Texto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnSeleciona.Color
  else if GradeA.Cells[ACol, ARow] <> '' then
  begin
    Texto := GradeQ.Cells[ACol, ARow];
    Cor := clWindow;
  end else begin
    if (GradeC.Cells[ACol,ARow] = '')
    and (GradeQ.Cells[ACol,ARow] <> '')
    then  GradeQ.Cells[ACol,ARow] := '';
    Texto := '';
    Cor := clMenu;
  end;
  //


  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor, taLeftJustify,
      GradeQ.Cells[Acol, ARow], 1, 1, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeQ, Rect, clBlack,
      Cor, taRightJustify,
      (*GradeQ.Cells[AcolARow]*)Texto, 1, 1, False);
end;

procedure TFmStqBalLeiIts.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeQ, GradeC, Key, Shift);
end;

procedure TFmStqBalLeiIts.ReconfiguraGradeQ;
begin
  DmProd.ConfigGrades3(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC, GradeQ);
end;

function TFmStqBalLeiIts.ReopenItem(GraGruX: Integer): Boolean;
begin
  Result := False;
  QrItem.Close;
  if GraGruX <> 0 then
  begin
    QrItem.Params[0].AsInteger := GraGrux;
    UMyMod.AbreQuery(QrItem, Dmod.MyDB, 'TFmFatPedIts.ReopenItem()');
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      if PnLeitura.Enabled then
        EdQtdLei.SetFocus;
      { ###
      ReopenPrecoSel(FmFatPedCab.QrFatPedCabPedido.Value, GraGruX);
      }
    end else
      Application.MessageBox('Reduzido n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

function TFmStqBalLeiIts.ObtemQtde(var Qtde: Integer): Boolean;
var
  ResVar: Variant;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Balan�o', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;

procedure TFmStqBalLeiIts.QrItemAfterScroll(DataSet: TDataSet);
begin
  LaQtdeLei.Caption := 'Qtde (' + sListaGerBxaEstq[QrItemGerBxaEstq.Value] + '):';
end;

procedure TFmStqBalLeiIts.QrItemBeforeClose(DataSet: TDataSet);
begin
  QrPreco.Close;
  LaQtdeLei.Caption := 'Qtde: [?]';
end;

function TFmStqBalLeiIts.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde, c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Application.MessageBox('O item nunca foi ativado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde  := Geral.IMV(GridP.Cells[Coluna, Linha]);
  if ObtemQtde(Qtde) then
    GridP.Cells[Coluna, Linha] := FormatFloat('0', Qtde);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmStqBalLeiIts.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX, Qtde: Integer;
  QtdeTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemQtde(Qtde) then
  begin
    QtdeTxt := Geral.FFT(Qtde, 0, siPositivo);
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
        GradeQ.Cells[c,l] := QtdeTxt;
    end;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

end.
