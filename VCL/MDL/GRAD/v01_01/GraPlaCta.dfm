object FmGraPlaCta: TFmGraPlaCta
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-018 :: G'#234'nero de N'#237'vel'
  ClientHeight = 335
  ClientWidth = 559
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 559
    Height = 179
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 158
    object PnNivel3: TPanel
      Left = 0
      Top = 0
      Width = 559
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      Visible = False
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 557
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 559
        Height = 19
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'N'#205'VEL 3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 61
      end
      object Label2: TLabel
        Left = 8
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label3: TLabel
        Left = 144
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdit2
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 36
        Width = 134
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsGraPlaCta3
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 144
        Top = 36
        Width = 400
        Height = 21
        DataField = 'Nome'
        DataSource = DsGraPlaCta3
        TabOrder = 1
      end
    end
    object PnNivel2: TPanel
      Left = 0
      Top = 64
      Width = 559
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      Visible = False
      ExplicitLeft = 1
      ExplicitTop = 65
      ExplicitWidth = 557
      object Label4: TLabel
        Left = 0
        Top = 0
        Width = 559
        Height = 19
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'N'#205'VEL 2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 61
      end
      object Label5: TLabel
        Left = 8
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 144
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdit4
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 36
        Width = 134
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsGraPlaCta2
        TabOrder = 0
      end
      object DBEdit4: TDBEdit
        Left = 144
        Top = 36
        Width = 400
        Height = 21
        DataField = 'Nome'
        DataSource = DsGraPlaCta2
        TabOrder = 1
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 128
      Width = 559
      Height = 51
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitLeft = 1
      ExplicitTop = 129
      ExplicitWidth = 557
      ExplicitHeight = 28
      object Label8: TLabel
        Left = 8
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object EdGenero: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Genero'
        UpdCampo = 'Genero'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 559
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -76
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 511
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 463
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 197
        Height = 32
        Caption = 'G'#234'nero de N'#237'vel'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 197
        Height = 32
        Caption = 'G'#234'nero de N'#237'vel'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 197
        Height = 32
        Caption = 'G'#234'nero de N'#237'vel'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 227
    Width = 559
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -76
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 555
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 271
    Width = 559
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -76
    ExplicitTop = 236
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 555
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 411
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsGraPlaCta3: TDataSource
    DataSet = FmPrdGrupTip.QrGraPlaCta3
    Left = 36
    Top = 4
  end
  object DsGraPlaCta2: TDataSource
    DataSet = FmPrdGrupTip.QrGraPlaCta2
    Left = 8
    Top = 4
  end
end
