unit StqCnsgGo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy,UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, dmkDBGrid, frxClass, frxDBSet,
  UnInternalConsts3, Variants, UnDmkProcFunc, dmkImage, UnDmkEnums, UnGrl_Geral,
  dmkCheckBox, Vcl.ComCtrls, dmkLabelRotate;

type
  TFmStqCnsgGo = class(TForm)
    PnDados: TPanel;
    DsStqCnsgGo: TDataSource;
    QrStqCnsgGo: TMySQLQuery;
    PnEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrStqMovIts: TmySQLQuery;
    DsStqMovIts: TDataSource;
    PMCab: TPopupMenu;
    PMItens: TPopupMenu;
    DBGStqMovIts: TDBGrid;
    N1: TMenuItem;
    QrStqCnsgGoCodigo: TIntegerField;
    QrStqCnsgGoCodUsu: TIntegerField;
    QrStqCnsgGoNome: TWideStringField;
    QrStqCnsgGoEmpresa: TIntegerField;
    QrStqCnsgGoAbertura: TDateTimeField;
    QrStqCnsgGoEncerrou: TDateTimeField;
    QrStqCnsgGoStatus: TSmallintField;
    QrStqCnsgGoLk: TIntegerField;
    QrStqCnsgGoDataCad: TDateField;
    QrStqCnsgGoDataAlt: TDateField;
    QrStqCnsgGoUserCad: TIntegerField;
    QrStqCnsgGoUserAlt: TIntegerField;
    QrStqCnsgGoAlterWeb: TSmallintField;
    QrStqCnsgGoAtivo: TSmallintField;
    QrStqMovItsNivel1: TIntegerField;
    QrStqMovItsNO_PRD: TWideStringField;
    QrStqMovItsPrdGrupTip: TIntegerField;
    QrStqMovItsUnidMed: TIntegerField;
    QrStqMovItsNO_PGT: TWideStringField;
    QrStqMovItsSIGLA: TWideStringField;
    QrStqMovItsNO_TAM: TWideStringField;
    QrStqMovItsNO_COR: TWideStringField;
    QrStqMovItsGraCorCad: TIntegerField;
    QrStqMovItsGraGruC: TIntegerField;
    QrStqMovItsGraGru1: TIntegerField;
    QrStqMovItsGraTamI: TIntegerField;
    QrStqMovItsDataHora: TDateTimeField;
    QrStqMovItsIDCtrl: TIntegerField;
    QrStqMovItsTipo: TIntegerField;
    QrStqMovItsOriCodi: TIntegerField;
    QrStqMovItsOriCtrl: TIntegerField;
    QrStqMovItsOriCnta: TIntegerField;
    QrStqMovItsEmpresa: TIntegerField;
    QrStqMovItsStqCenCad: TIntegerField;
    QrStqMovItsGraGruX: TIntegerField;
    QrStqMovItsQtde: TFloatField;
    QrStqMovItsAlterWeb: TSmallintField;
    QrStqMovItsAtivo: TSmallintField;
    QrStqMovItsOriPart: TIntegerField;
    QrStqMovItsPecas: TFloatField;
    QrStqMovItsPeso: TFloatField;
    QrStqMovItsAreaM2: TFloatField;
    QrStqMovItsAreaP2: TFloatField;
    QrStqMovItsFatorClas: TFloatField;
    QrStqMovItsQuemUsou: TIntegerField;
    QrStqMovItsRetorno: TSmallintField;
    QrStqMovItsParTipo: TIntegerField;
    QrStqMovItsParCodi: TIntegerField;
    QrStqMovItsDebCtrl: TIntegerField;
    QrStqMovItsSMIMultIns: TIntegerField;
    Incluiitem1: TMenuItem;
    Excluiitem1: TMenuItem;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    QrStqCnsgGoNOMEFILIAL: TWideStringField;
    QrStqCnsgGoNO_FORNECE: TWideStringField;
    QrStqCnsgGoBalQtdItem: TFloatField;
    QrStqCnsgGoFatSemEstq: TSmallintField;
    QrStqCnsgGoFornece: TIntegerField;
    QrStqCnsgGoENCERROU_TXT: TWideStringField;
    GroupBox1: TGroupBox;
    QrFornece: TMySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    GroupBox5: TGroupBox;
    dmkLabel4: TdmkLabel;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    GroupBox2: TGroupBox;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    Label13: TLabel;
    EdNome: TdmkEdit;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    dmkLabel5: TdmkLabel;
    SpeedButton6: TSpeedButton;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    VUEmpresa: TdmkValUsu;
    QrStqMovItsCustoAll: TFloatField;
    QrStqMovItsValorAll: TFloatField;
    N2: TMenuItem;
    frxSTQ_ENTRA_001_1: TfrxReport;
    frxDsStqMovIts: TfrxDBDataset;
    QrForne: TmySQLQuery;
    QrForneE_ALL: TWideStringField;
    QrForneCNPJ_TXT: TWideStringField;
    QrForneNOME_TIPO_DOC: TWideStringField;
    QrForneTE1_TXT: TWideStringField;
    QrForneFAX_TXT: TWideStringField;
    QrForneNUMERO_TXT: TWideStringField;
    QrForneCEP_TXT: TWideStringField;
    QrForneCodigo: TIntegerField;
    QrForneTipo: TSmallintField;
    QrForneCodUsu: TIntegerField;
    QrForneNOME_ENT: TWideStringField;
    QrForneCNPJ_CPF: TWideStringField;
    QrForneIE_RG: TWideStringField;
    QrForneRUA: TWideStringField;
    QrForneCOMPL: TWideStringField;
    QrForneBAIRRO: TWideStringField;
    QrForneCIDADE: TWideStringField;
    QrForneNOMELOGRAD: TWideStringField;
    QrForneNOMEUF: TWideStringField;
    QrFornePais: TWideStringField;
    QrForneENDEREF: TWideStringField;
    QrForneTE1: TWideStringField;
    QrForneFAX: TWideStringField;
    QrForneIE: TWideStringField;
    QrForneCAD_FEDERAL: TWideStringField;
    QrForneCAD_ESTADUAL: TWideStringField;
    QrForneIE_TXT: TWideStringField;
    QrForneNO2_ENT: TWideStringField;
    frxDsForne: TfrxDBDataset;
    frxDsStqCnsgGo: TfrxDBDataset;
    PMPagto: TPopupMenu;
    Panel4: TPanel;
    Splitter1: TSplitter;
    IncluiBuy1: TMenuItem;
    ExcluiBuy1: TMenuItem;
    QrPgtBuy: TMySQLQuery;
    QrPgtBuyData: TDateField;
    QrPgtBuyVencimento: TDateField;
    QrPgtBuyBanco: TIntegerField;
    QrPgtBuySEQ: TIntegerField;
    QrPgtBuyFatID: TIntegerField;
    QrPgtBuyContaCorrente: TWideStringField;
    QrPgtBuyDocumento: TFloatField;
    QrPgtBuyDescricao: TWideStringField;
    QrPgtBuyFatParcela: TIntegerField;
    QrPgtBuyFatNum: TFloatField;
    QrPgtBuyNOMECARTEIRA: TWideStringField;
    QrPgtBuyNOMECARTEIRA2: TWideStringField;
    QrPgtBuyBanco1: TIntegerField;
    QrPgtBuyAgencia1: TIntegerField;
    QrPgtBuyConta1: TWideStringField;
    QrPgtBuyTipoDoc: TSmallintField;
    QrPgtBuyControle: TIntegerField;
    QrPgtBuyNOMEFORNECEI: TWideStringField;
    QrPgtBuyCARTEIRATIPO: TIntegerField;
    DsPgtBuy: TDataSource;
    QrPgtBuyDebito: TFloatField;
    SpeedButton5: TSpeedButton;
    QrPgtBuyAgencia: TIntegerField;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCab: TBitBtn;
    BtItens: TBitBtn;
    BtPagto: TBitBtn;
    QrForneNUMERO: TFloatField;
    QrForneUF: TFloatField;
    QrForneLOGRAD: TFloatField;
    QrForneCEP: TFloatField;
    QrStqCnsgGoCodCliInt: TIntegerField;
    Panel8: TPanel;
    Label5: TLabel;
    Ednfe_serie: TdmkEdit;
    Ednfe_nNF: TdmkEdit;
    Label6: TLabel;
    EdNFe_Id: TdmkEdit;
    Label192: TLabel;
    GroupBox4: TGroupBox;
    DBEdit3: TDBEdit;
    QrStqCnsgGonfe_serie: TIntegerField;
    QrStqCnsgGonfe_nNF: TIntegerField;
    QrStqCnsgGonfe_Id: TWideStringField;
    Label9: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit4: TDBEdit;
    DBEdNFe_Id: TDBEdit;
    PMNovo: TPopupMenu;
    ID1: TMenuItem;
    Pesquisadetalhada1: TMenuItem;
    CkAtzPrcMed: TdmkCheckBox;
    DBCkAtzPrcMed: TDBCheckBox;
    QrStqCnsgGoAtzPrcMed: TSmallintField;
    MeAvisos: TMemo;
    QrPgtFrt: TMySQLQuery;
    QrPgtFrtData: TDateField;
    QrPgtFrtVencimento: TDateField;
    QrPgtFrtBanco: TIntegerField;
    QrPgtFrtSEQ: TIntegerField;
    QrPgtFrtFatID: TIntegerField;
    QrPgtFrtContaCorrente: TWideStringField;
    QrPgtFrtDocumento: TFloatField;
    QrPgtFrtDescricao: TWideStringField;
    QrPgtFrtFatParcela: TIntegerField;
    QrPgtFrtFatNum: TFloatField;
    QrPgtFrtNOMECARTEIRA: TWideStringField;
    QrPgtFrtNOMECARTEIRA2: TWideStringField;
    QrPgtFrtBanco1: TIntegerField;
    QrPgtFrtAgencia1: TIntegerField;
    QrPgtFrtConta1: TWideStringField;
    QrPgtFrtTipoDoc: TSmallintField;
    QrPgtFrtControle: TIntegerField;
    QrPgtFrtNOMEFORNECEI: TWideStringField;
    QrPgtFrtCARTEIRATIPO: TIntegerField;
    QrPgtFrtDebito: TFloatField;
    QrPgtFrtAgencia: TIntegerField;
    DsPgtFrt: TDataSource;
    Produtos1: TMenuItem;
    Frete1: TMenuItem;
    IncluiFrt1: TMenuItem;
    ExcluiFrt1: TMenuItem;
    QrStqCnsgGoTransporta: TIntegerField;
    QrStqCnsgGoNO_TRANSPORTA: TWideStringField;
    dmkLabel2: TdmkLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrTransporta: TMySQLQuery;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNO_ENT: TWideStringField;
    DsTransporta: TDataSource;
    dmkLabel3: TdmkLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    SbTransporta: TSpeedButton;
    QrSumFrt: TMySQLQuery;
    QrStqCnsgGoValTotFrete: TFloatField;
    QrSumFrtDebito: TFloatField;
    QrSumIts: TMySQLQuery;
    QrSumItsQtde: TFloatField;
    QrStqMovItsCustoBuy: TFloatField;
    QrStqMovItsCustoFrt: TFloatField;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    DBGBuy: TDBGrid;
    dmkLabelRotate1: TdmkLabelRotate;
    dmkLabelRotate2: TdmkLabelRotate;
    DBGFrt: TDBGrid;
    Fracionamento1: TMenuItem;
    N4: TMenuItem;
    QrStqMovItsPackGGX: TIntegerField;
    QrStqMovItsPackUnMed: TIntegerField;
    QrStqMovItsPackQtde: TFloatField;
    Incluinovaremessa1: TMenuItem;
    Alteraremessaatual1: TMenuItem;
    Excluiremessaatual1: TMenuItem;
    Encerraremessa1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqCnsgGoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqCnsgGoBeforeOpen(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrStqCnsgGoBeforeClose(DataSet: TDataSet);
    procedure QrStqCnsgGoAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluiitem1Click(Sender: TObject);
    procedure Excluiitem1Click(Sender: TObject);
    procedure QrStqCnsgGoCalcFields(DataSet: TDataSet);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrForneCalcFields(DataSet: TDataSet);
    procedure BtPagtoClick(Sender: TObject);
    procedure IncluiBuy1Click(Sender: TObject);
    procedure ExcluiBuy1Click(Sender: TObject);
    procedure QrStqCnsgGoAfterClose(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure PMPagtoPopup(Sender: TObject);
    procedure ID1Click(Sender: TObject);
    procedure Pesquisadetalhada1Click(Sender: TObject);
    procedure QrPgtBuyCalcFields(DataSet: TDataSet);
    procedure IncluiFrt1Click(Sender: TObject);
    procedure ExcluiFrt1Click(Sender: TObject);
    procedure QrPgtFrtCalcFields(DataSet: TDataSet);
    procedure Fracionamento1Click(Sender: TObject);
    procedure Incluinovaremessa1Click(Sender: TObject);
    procedure Alteraremessaatual1Click(Sender: TObject);
    procedure Excluiremessaatual1Click(Sender: TObject);
    procedure Encerraremessa1Click(Sender: TObject);
  private
    FTabLctA: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure HabilitaBotoes();
    procedure AtivaItensNoEstoque(IDCtrl, Status: Integer);
  public
    { Public declarations }
    FThisFatID_Rem_Bxa,
    FThisFatID_Rem_Inn,
    FThisFatID_Frt: Integer;
    FStqInnNFe, FStqInnOpt: String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenStqMovIts(IDCtrl: Integer);
    procedure ReopenPagtosBuy();
    procedure ReopenPagtosFrt();
    procedure RateioCustoDoFrete();

  end;

var
  FmStqCnsgGo: TFmStqCnsgGo;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF,  UnDmkWeb, StqInnPesq,
  {$IfNDef semNFe_v0000}ModuleNFe_0000, NFeImporta_0400,{$EndIf}
  {$IFDEF FmPlacasAdd}PlacasAdd,{$ENDIF}
  {$IfNDef NO_FINANCEIRO}ModuleFin, UnPagtos,{$EndIf}
  UnGrade_Tabs, ModuleGeral, UCreate, StqInnNFe, Entidade2, StqInnIts,
  StqInnPack, UMySQLDB;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqCnsgGo.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqCnsgGo.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqCnsgGoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqCnsgGo.DefParams;
begin
  VAR_GOTOTABELA := 'StqCnsgGo';
  VAR_GOTOMYSQLTABLE := QrStqCnsgGo;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('pem.BalQtdItem, pem.FatSemEstq, sic.*, ei.CodCliInt');
  VAR_SQLx.Add('FROM stqcnsggo sic');
  VAR_SQLx.Add('LEFT JOIN enticliint ei ON ei.CodEnti=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades fil  ON fil.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem  ON pem.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece');
  VAR_SQLx.Add('LEFT JOIN entidades tsp ON tsp.Codigo=sic.Transporta');
  VAR_SQLx.Add('WHERE sic.Codigo > -1000');
  VAR_SQLx.Add('AND sic.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND sic.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sic.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sic.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Codigo IN (SELECT Codigo FROM stqcnsggo WHERE Empresa IN (' + VAR_LIB_EMPRESAS + '))';
end;

procedure TFmStqCnsgGo.RateioCustoDoFrete();
{
var
  Codigo, IDCtrl, GraGruX, Empresa: Integer;
  Fator, SumQtde, ValTotFrete, CustoFrt, CustoAll: Double;
  SQLType: TSQLType;
}
begin
{
  SQLType := stUpd;
  Codigo  := QrStqCnsgGoCodigo.Value;
  Empresa := QrStqCnsgGoEmpresa.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumFrt, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito',
  'FROM ' + FTabLctA + ' la',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgGoCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  '']);
  //
  ValTotFrete := QrSumFrtDebito.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(CustoBuy) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Tipo=' + Geral.FF0(FThisFatID_Buy),
  'AND OriCodi=' + Geral.FF0(QrStqCnsgGoCodigo.Value),
  '']);
  //
  SumQtde := QrSumItsQtde.Value;
  //
  if SumQtde > 0 then
    Fator := ValTotFrete / SumQtde
  else
    Fator := 0;
  //
  ReopenStqMovIts(0);
  QrStqMovIts.DisableControls;
  try
    QrStqMovIts.First;
    while not QrStqMovIts.Eof do
    begin
      CustoFrt := QrStqMovItsCustoBuy.Value * Fator;
      CustoAll := QrStqMovItsCustoBuy.Value + CustoFrt;
      //
      GraGruX  := QrStqMovItsGraGruX.Value;
      IDCtrl   := QrStqMovItsIDCtrl.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
      'CustoFrt', 'CustoAll'], [
      'IDCtrl'], [
      CustoFrt, CustoAll], [
      IDCtrl], False);
      //
      DmodG.AtualizaPrecosGraGruVal2(GraGruX, Empresa);
      //
      QrStqMovIts.Next;
    end;
  finally
    QrStqMovIts.EnableControls;
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqcnsggo', False, [
  'ValTotFrete'], [
  'Codigo'], [
  ValTotFrete], [
  Codigo], False);
  //
  LocCod(Codigo, Codigo);
}
end;

procedure TFmStqCnsgGo.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCnsgGo', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmStqCnsgGo.Encerraremessa1Click(Sender: TObject);
var
  LastCod, Codigo, Empresa, Status: Integer;
  ListaGraGruX: TStringList;
  //
  Encerrou: String;
begin
  Codigo := QrStqCnsgGoCodigo.Value;
  //
  if QrStqCnsgGoEncerrou.Value = 0 then
  begin
    Encerrou := Geral.FDT(DModG.ObtemAgora(), 109);
    Status := 1;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrUpdM, Dmod.MyDB, [
    'SELECT MAX(Codigo) Codigo',
    'FROM stqcnsggo',
    'WHERE Fornece=' + Geral.FF0(QrStqCnsgGoFornece.Value),
    '']);
    LastCod := USQLDB.v_i(Dmod.QrUpdM, 'Codigo');
    if LastCod <> QrStqCnsgGoCodigo.Value then
    begin
      Geral.MB_Aviso('Somente a �ltima remessa do vendedor/representante ' +
      QrStqCnsgGoNO_FORNECE.Value + ' pode ser desfeita!');
      Exit;
    end else
    begin
      Encerrou := '0';
      Status := 1;
    end;
    //
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqcnsggo', False, [
  'Encerrou', 'Status'], ['Codigo'], [
  Encerrou, Status], [Codigo], True);
  //
  AtivaItensNoEstoque(QrStqCnsgGoCodigo.Value, Status);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmStqCnsgGo.ExcluiBuy1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados: Integer;
begin
{
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID_Buy, QrStqCnsgGoCodigo.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPgtBuy, DBGBuy,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  ReopenPagtosBuy();
}
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmStqCnsgGo.ExcluiFrt1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados: Integer;
begin
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID_Frt, QrStqCnsgGoCodigo.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPgtFrt, DBGFrt,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  RateioCustoDoFrete();
  ReopenPagtosFrt();
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmStqCnsgGo.Excluiitem1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrStqMovIts, DBGStqMovIts,
  'StqMovItsA', ['IDCtrl'], ['IDCtrl'], istPergunta, '');
end;

procedure TFmStqCnsgGo.Excluiremessaatual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrStqMovIts.RecordCount > 0 then
    Geral.MB_Aviso(
    'Esta remessa n�o pode ser exclu�da, pois existem itens lan�ados nela.')
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o da remessa ID n�mero ' +
      Geral.FF0(QrStqCnsgGoCodigo.Value) + '?') <> ID_YES then Exit;
    //
    Codigo := QrStqCnsgGoCodigo.Value;
    //
    if DBCheck.ExcluiRegistro(Dmod.QrUpd, QrStqCnsgGo, 'StqCnsgGo', ['Codigo'],
      ['Codigo'], True, 'Confirma a exclus�o desta remessa?') then
    begin
      LocCod(Codigo, Codigo);
      Va(vpLast);
    end;
  end;
end;

procedure TFmStqCnsgGo.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqCnsgGo.Pesquisadetalhada1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmStqInnPesq, FmStqInnPesq, afmoNegarComAviso) then
  begin
    FmStqInnPesq.EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    FmStqInnPesq.FThisFatID             := FThisFatID_Rem_Inn;
    FmStqInnPesq.ShowModal;
    FmStqInnPesq.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
    LocCod(VAR_CADASTRO, VAR_CADASTRO);
end;

procedure TFmStqCnsgGo.PMCabPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrStqCnsgGo.State <> dsInactive) and (QrStqCnsgGo.RecordCount > 0);
  //
  if Enab then
    Enab2 := QrStqCnsgGoEncerrou.Value = 0
  else
    Enab2 := False;
  //
  AlteraRemessaatual1.Enabled := Enab and Enab2;
  ExcluiRemessaatual1.Enabled := Enab and Enab2;
  EncerraRemessa1.Enabled     := Enab;
end;

procedure TFmStqCnsgGo.PMItensPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrStqCnsgGo.State <> dsInactive) and (QrStqCnsgGo.RecordCount > 0);
  Enab2 := (QrStqMovIts.State <> dsInactive) and (QrStqMovIts.RecordCount > 0);
  //
  Incluiitem1.Enabled := Enab;
  Fracionamento1.Enabled := Enab and Enab2;
  Excluiitem1.Enabled := Enab and Enab2;
end;

procedure TFmStqCnsgGo.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrStqCnsgGo.State <> dsInactive) and (QrStqCnsgGo.RecordCount > 0);
  Enab2 := (QrPgtBuy.State <> dsInactive) and (QrPgtBuy.RecordCount > 0);
  Enab3 := (QrPgtFrt.State <> dsInactive) and (QrPgtFrt.RecordCount > 0);
  //
  IncluiBuy1.Enabled := Enab;
  ExcluiBuy1.Enabled := Enab and Enab2;
  //
  IncluiFrt1.Enabled := Enab;
  ExcluiFrt1.Enabled := Enab and Enab3;
  //
end;

procedure TFmStqCnsgGo.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqCnsgGo.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqCnsgGo.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqCnsgGo.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqCnsgGo.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqCnsgGo.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqCnsgGo.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqCnsgGo.SpeedButton5Click(Sender: TObject);
begin
  {$IFDEF FmPlacasAdd}
  if DBCheck.CriaFm(TFmPlacasAdd, FmPlacasAdd, afmoNegarComAviso) then
  begin
    FmPlacasAdd.ShowModal;
    EdNome.ValueVariant := FmPlacasAdd.FPlacas;
    FmPlacasAdd.Destroy;
  end;
  {$ENDIF}
end;

procedure TFmStqCnsgGo.SpeedButton6Click(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdFornece.ValueVariant;

  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFornece, CBFornece, QrFornece, VAR_CADASTRO);
    EdFornece.SetFocus;
  end;
end;

procedure TFmStqCnsgGo.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqCnsgGoCodigo.Value;
  Close;
end;

procedure TFmStqCnsgGo.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmStqCnsgGo.BtPagtoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagto);
end;

procedure TFmStqCnsgGo.Alteraremessaatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrStqCnsgGo, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'StqCnsgGo');
  //
  GroupBox2.Enabled := QrStqMovIts.RecordCount = 0;
end;

procedure TFmStqCnsgGo.AtivaItensNoEstoque(IDCtrl, Status: Integer);
var
  Ativo: Integer;
begin
  if Status > 0 then // Existe status maior que 1?
    Ativo := 1
  else
    Ativo := 0;
  //
(*
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE stqmovitsa ',
  'SET Ativo=' + Geral.FF0(Status),
  'WHERE Tipo="' + Geral.FF0(FThisFatID_Buy) + '"',
  'AND OriCodi="' + Geral.FF0(IDCtrl) + '"',
  '']);
*)
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE stqmovitsa ',
  'SET Ativo=' + Geral.FF0(Status),
  'WHERE Tipo IN (' + Geral.FF0(FThisFatID_Rem_Bxa) + ',' +
    Geral.FF0(FThisFatID_Rem_Inn) + ')',
  'AND OriCodi="' + Geral.FF0(IDCtrl) + '"',
  '']);
end;

procedure TFmStqCnsgGo.BtConfirmaClick(Sender: TObject);
var
  Nome, Abertura, (*Encerrou,*) nfe_Id: String;
  Codigo, CodUsu, Empresa, (*Status,*) Fornece, Cliente, nfe_serie,
  nfe_nNF, AtzPrcMed, Transporta: Integer;
  //ValTotFrete: Double;
  SQLType: TSQLType;
begin
  //
  if MyObjects.FIC(EdFilial.ValueVariant = 0, EdFilial, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdFornece.ValueVariant = 0, EdFornece, 'Informe o vendedor/representante!') then Exit;
  //if MyObjects.FIC(EdCodigo.ValueVariant = 0, nil, 'Informe o c�digo!') then Exit;
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  CodUsu         := EdCodUsu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  Abertura       := Geral.FDT(EdAbertura.ValueVariant, 109);
  //Encerrou       := ;
  //Status         := ;
  Fornece        := EdFornece.ValueVariant;
  Cliente        := 0;
  nfe_serie      := Ednfe_serie.ValueVariant;
  nfe_nNF        := Ednfe_nNF.ValueVariant;;
  nfe_Id         := Ednfe_Id.ValueVariant;;
  AtzPrcMed      := 0;
  Transporta     := EdTransporta.ValueVariant;
  //ValTotFrete    := ;
  //
  Codigo := UMyMod.BPGS1I32('stqcnsggo', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if SQLType = stIns then
    CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqcnsggo', False, [
  'CodUsu', 'Nome', 'Empresa',
  'Abertura', (*'Encerrou', 'Status',*)
  'Fornece', 'Cliente', 'nfe_serie',
  'nfe_nNF', 'nfe_Id', 'AtzPrcMed',
  'Transporta'(*, 'ValTotFrete'*)], [
  'Codigo'], [
  CodUsu, Nome, Empresa,
  Abertura, (*Encerrou, Status,*)
  Fornece, Cliente, nfe_serie,
  nfe_nNF, nfe_Id, AtzPrcMed,
  Transporta(*, ValTotFrete*)], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmStqCnsgGo.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'StqCnsgGo', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCnsgGo', 'Codigo');
  MostraEdicao(0, stlok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'StqCnsgGo', 'Codigo');
end;

procedure TFmStqCnsgGo.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmStqCnsgGo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FThisFatID_Rem_Bxa := VAR_FATID_4201;
  FThisFatID_Rem_Inn := VAR_FATID_4202;
  FThisFatID_Frt := VAR_FATID_0006;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBFilial.ListSource := DModG.DsEmpresas;
  DBGStqMovIts.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  //
  if UpperCase(Application.Title) = 'SAFECAR' then
  begin
    EdNome.Width         := 817;
    SpeedButton5.Visible := True;
  end else
  begin
    EdNome.Width         := 845;
    SpeedButton5.Visible := False;    
  end;
end;

procedure TFmStqCnsgGo.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqCnsgGoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCnsgGo.SbImprimeClick(Sender: TObject);
begin
  DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  DModG.ReopenEndereco(Geral.IMV(VAR_LIB_EMPRESAS));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrForne, Dmod.MyDB, [
    'SELECT en.Codigo, Tipo, CodUsu, IE, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF,',
    'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO2_ENT,',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG,',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA,',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0.000 NUMERO,',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL,',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO,',
    'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CIDADE,',
    'CASE WHEN en.Tipo=0 THEN en.EUF         ELSE en.PUF      END + 0.000 UF,',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD,',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF,',
    'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais,',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0.000 Lograd,',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0.000 CEP,',
    'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF,',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1,',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX,',
    'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL',
    'FROM entidades en',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd',
    'WHERE en.Codigo=' + Geral.FF0(QrStqCnsgGoFornece.Value),
    '']);
  //
  MyObjects.frxDefineDataSets(frxSTQ_ENTRA_001_1, [
    DModG.frxDsMaster,
    DModG.frxDsEndereco,
    frxDsForne,
    frxDsStqCnsgGo,
    frxDsStqMovIts
    ]);
  //
  MyObjects.frxMostra(frxSTQ_ENTRA_001_1, 'Informe da Compra n� ' +
    FormatFloat('000000', QrStqCnsgGoCodUsu.Value));
end;

procedure TFmStqCnsgGo.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqCnsgGo.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, SbNovo);
end;

procedure TFmStqCnsgGo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqCnsgGo.QrForneCalcFields(DataSet: TDataSet);
begin
  QrForneTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneTe1.Value);
  QrForneFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForneFax.Value);
  QrForneCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrForneCNPJ_CPF.Value);
  QrForneIE_TXT.Value :=
    Geral.Formata_IE(QrForneIE_RG.Value, QrForneUF.Value, '??', QrForneTipo.Value);
  QrForneNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrForneRua.Value, Trunc(QrForneNumero.Value), False);
  //
  QrForneE_ALL.Value := UpperCase(QrForneNOMELOGRAD.Value);
  if Trim(QrForneE_ALL.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ';
  QrForneE_ALL.Value := QrForneE_ALL.Value + Uppercase(QrForneRua.Value);
  if Trim(QrForneRua.Value) <> '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNUMERO_TXT.Value;
  if Trim(QrForneCompl.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' ' + Uppercase(QrForneCompl.Value);
  if Trim(QrForneBairro.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneBairro.Value);
  if QrForneCEP.Value > 0 then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrForneCEP.Value);
  if Trim(QrForneCidade.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + Uppercase(QrForneCidade.Value);
  if Trim(QrForneNOMEUF.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ', ' + QrForneNOMEUF.Value;
  if Trim(QrFornePais.Value) <>  '' then QrForneE_ALL.Value :=
    QrForneE_ALL.Value + ' - ' + QrFornePais.Value;
end;

procedure TFmStqCnsgGo.QrPgtBuyCalcFields(DataSet: TDataSet);
begin
  QrPgtBuySEQ.Value := QrPgtBuy.RecNo;
end;

procedure TFmStqCnsgGo.QrPgtFrtCalcFields(DataSet: TDataSet);
begin
  QrPgtFrtSEQ.Value := QrPgtFrt.RecNo;
end;

procedure TFmStqCnsgGo.QrStqCnsgGoAfterClose(DataSet: TDataSet);
begin
  HabilitaBotoes;
end;

procedure TFmStqCnsgGo.QrStqCnsgGoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  HabilitaBotoes;
end;

procedure TFmStqCnsgGo.QrStqCnsgGoAfterScroll(DataSet: TDataSet);
begin
  ReopenStqMovIts(0);
  //
  BtItens.Enabled := QrStqCnsgGoEncerrou.Value = 0;
  if QrStqCnsgGoCodCliInt.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrStqCnsgGoCodCliInt.Value)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenPagtosBuy();
  ReopenPagtosFrt();
  BtPagto.Enabled := QrStqCnsgGoEncerrou.Value = 0;
end;

procedure TFmStqCnsgGo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdNFe_Id.CharCase   := ecNormal;
  DBEdNFe_Id.CharCase := ecNormal;
end;

procedure TFmStqCnsgGo.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqCnsgGoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'StqCnsgGo', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqCnsgGo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCnsgGo.Fracionamento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqInnPack, FmStqInnPack, afmoNegarComAviso) then
  begin
    FmStqInnPack.ImgTipo.SQLType := stUpd;
    FmStqInnPack.FIDCtrl := QrStqMovItsIDCtrl.Value;
    if (QrStqMovItsPackGGX.Value > 0) or (QrStqMovItsPackQtde.Value > 0) then
    begin
      FmStqInnPack.EdGraGruX.ValueVariant := QrStqMovItsGraGruX.Value;
      FmStqInnPack.EdQtde.ValueVariant := QrStqMovItsQtde.Value;
      FmStqInnPack.EdPackGGX.ValueVariant := QrStqMovItsPackGGX.Value;
      FmStqInnPack.EdPackQtde.ValueVariant := QrStqMovItsPackQtde.Value;
    end else
    begin
      FmStqInnPack.EdGraGruX.ValueVariant := QrStqMovItsGraGruX.Value;;
      FmStqInnPack.EdQtde.ValueVariant := QrStqMovItsQtde.Value;
      FmStqInnPack.EdPackGGX.ValueVariant := QrStqMovItsGraGruX.Value;
      FmStqInnPack.EdPackQtde.ValueVariant := QrStqMovItsQtde.Value;
    end;
    FmStqInnPack.ShowModal;
    FmStqInnPack.Destroy;
    //
    ReopenStqMovIts(0);
  end;

end;

procedure TFmStqCnsgGo.HabilitaBotoes;
var
  Enab: Boolean;
  Texto: string;
begin
  if (QrStqCnsgGoStatus.Value > 0) and (QrStqCnsgGoEncerrou.Value <> 0) and
    (QrStqCnsgGo.RecordCount > 0) then
  begin
    Enab  := False;
    Texto := 'Desfaz encerramento da remessa';
  end else
  begin
    Enab  := True;
    Texto := 'Encerra a remessa';
  end;
  //
  BtItens.Enabled             := Enab;
  BtPagto.Enabled             := Enab;
  Alteraremessaatual1.Enabled := Enab;
  Excluiremessaatual1.Enabled := Enab;
  Encerraremessa1.Caption     := Texto;
end;

procedure TFmStqCnsgGo.ID1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqCnsgGoCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmStqCnsgGo.IncluiBuy1Click(Sender: TObject);
var
  Terceiro, Codigo, Empresa, NF: Integer;
  Valor: Double;
  Descricao: String;
begin
 {$IfNDef NO_FINANCEIRO}
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('SELECT SUM(CustoAll) CustoAll');
  Dmod.QrUpd.SQL.Add('FROM stqmovitsa');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0');
  Dmod.QrUpd.SQL.Add('AND OriCodi=:P1');
  Dmod.QrUpd.Params[0].AsInteger := FThisFatID_Rem_Inn;
  Dmod.QrUpd.Params[1].AsInteger := QrStqCnsgGoCodigo.Value;
  UnDmkDAC_PF.AbreQuery(Dmod.QrUpd, Dmod.MyDB);
  if Dmod.QrUpd.FieldByName('CustoAll').Value <> Null then
    Valor := Dmod.QrUpd.FieldByName('CustoAll').Value
  else
    Valor := 0;
  Dmod.QrUpd.Close;
  //
  Codigo        := QrStqCnsgGoCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Terceiro      := QrStqCnsgGoFornece.Value;
  Descricao     := 'Compra n� ' + Geral.FF0(QrStqCnsgGoCodUsu.Value);
  Empresa       := QrStqCnsgGoEmpresa.Value;
  NF            := QrStqCnsgGonfe_nNF.Value;
  //
  UPagtos.Pagto(QrPgtBuy, tpDeb, Codigo, Terceiro, FThisFatID_Rem_Inn, 0, 0, stIns,
    'Compra', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0, True, True, 0, 0,
    0, 0, NF, FTabLctA, Descricao);
  //
  ReopenPagtosBuy();
 {$Else}
   Geral.MB_Info('ERP sem pagamento de compra');
 {$EndIf}
end;

procedure TFmStqCnsgGo.IncluiFrt1Click(Sender: TObject);
var
  Terceiro, Codigo, Empresa, NF: Integer;
  Valor: Double;
  Descricao: String;
begin
 {$IfNDef NO_FINANCEIRO}
  Codigo        := QrStqCnsgGoCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Terceiro      := QrStqCnsgGoTransporta.Value;
  Descricao     := 'FC n� ' + Geral.FF0(QrStqCnsgGoCodUsu.Value);
  Empresa       := QrStqCnsgGoEmpresa.Value;
  NF            := QrStqCnsgGonfe_nNF.Value;
  //
  UPagtos.Pagto(QrPgtFrt, tpDeb, Codigo, Terceiro, FThisFatID_Frt, 0, 0, stIns,
    'Frete', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0, True, True, 0, 0,
    0, 0, NF, FTabLctA, Descricao);
  //
  RateioCustoDoFrete();
  ReopenPagtosBuy();
 {$Else}
   Geral.MB_Info('ERP sem pagamento de compra');
 {$EndIf}
end;

procedure TFmStqCnsgGo.Incluiitem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmStqInnIts, FmStqInnIts, afmoNegarComAviso) then
  begin
    FmStqInnIts.ImgTipo.SQLType := stIns;
    FmStqInnIts.ShowModal;
    FmStqInnIts.Destroy;
    //
    ReopenStqMovIts(0);
  end;
end;

procedure TFmStqCnsgGo.Incluinovaremessa1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrStqCnsgGo, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'StqCnsgGo');
  // Deve ser depois do UMyMod.ConfigPanelInsUpd
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'StqCnsgGo', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdNome);
  //
  EdAbertura.ValueVariant := DModG.ObtemAgora();
  GroupBox2.Enabled       := True;
  EdFilial.ValueVariant   := DModG.QrFiliLogFilial.Value;
  CBFilial.KeyValue       := DModG.QrFiliLogFilial.Value;
end;

procedure TFmStqCnsgGo.QrStqCnsgGoBeforeClose(DataSet: TDataSet);
begin
  QrStqMovIts.Close;
  QrPgtBuy.Close;
  //
  BtItens.Enabled := False;
  BtPagto.Enabled := False;
end;

procedure TFmStqCnsgGo.QrStqCnsgGoBeforeOpen(DataSet: TDataSet);
begin
  QrStqCnsgGoCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStqCnsgGo.QrStqCnsgGoCalcFields(DataSet: TDataSet);
begin
  if QrStqCnsgGoEncerrou.Value = 0 then
    QrStqCnsgGoENCERROU_TXT.Value := CO_MovimentoAberto
  else
    QrStqCnsgGoENCERROU_TXT.Value := Geral.FDT(QrStqCnsgGoEncerrou.Value, 0);
  //
end;

procedure TFmStqCnsgGo.ReopenPagtosBuy();
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgtBuy, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
  'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, ',
  'la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI,',
  'ca.Tipo CARTEIRATIPO',
  'FROM ' + FTabLctA + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgGoCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Rem_Inn),
  'ORDER BY la.FatParcela, la.Vencimento',
  '']);
{$EndIf}
end;

procedure TFmStqCnsgGo.ReopenPagtosFrt();
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgtFrt, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
  'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, ',
  'la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI,',
  'ca.Tipo CARTEIRATIPO',
  'FROM ' + FTabLctA + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
  'WHERE FatNum=' + Geral.FF0(QrStqCnsgGoCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Frt),
  'ORDER BY la.FatParcela, la.Vencimento',
  '']);
{$EndIf}
end;

procedure TFmStqCnsgGo.ReopenStqMovIts(IDCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqMovIts, Dmod.MyDB, [
    'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip, ',
    'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA, ',
    'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,  ',
    'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.*  ',
    'FROM stqmovitsa smia ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'WHERE Tipo=' + Geral.FF0(FThisFatID_Rem_Inn),
    'AND OriCodi=' + Geral.FF0(QrStqCnsgGoCodigo.Value),
    'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora ',
    '']);
  //
  QrStqMovIts.Locate('IDCtrl', IDCtrl, []);
end;

end.

