object FmGraGruDados: TFmGraGruDados
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-006 :: Dados Gerais do Grupo de Produtos'
  ClientHeight = 580
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 587
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 539
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 442
        Height = 32
        Caption = 'Dados Gerais do Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 442
        Height = 32
        Caption = 'Dados Gerais do Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 442
        Height = 32
        Caption = 'Dados Gerais do Grupo de Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 472
    Width = 635
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 631
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 516
    Width = 635
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel13: TPanel
      Left = 2
      Top = 15
      Width = 631
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 487
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 635
    Height = 351
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = 'Dados gen'#233'ricos'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 627
        Height = 323
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 627
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 12
            Top = 8
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label6: TLabel
            Left = 80
            Top = 8
            Width = 117
            Height = 13
            Caption = 'Unidade de Medida [F3]:'
          end
          object SBUnidMed: TSpeedButton
            Left = 532
            Top = 23
            Width = 21
            Height = 22
            Caption = '...'
            OnClick = SBUnidMedClick
          end
          object Label10: TLabel
            Left = 556
            Top = 8
            Width = 59
            Height = 13
            Caption = 'Refer'#234'ncia'#170':'
          end
          object dmkDBEdit1: TdmkDBEdit
            Left = 12
            Top = 24
            Width = 65
            Height = 21
            TabStop = False
            DataField = 'Nivel1'
            DataSource = FmGraGruN.DsGraGru1
            TabOrder = 0
            UpdCampo = 'Nivel1'
            UpdType = utInc
            Alignment = taLeftJustify
          end
          object EdUnidMed: TdmkEditCB
            Left = 80
            Top = 24
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdUnidMedChange
            OnKeyDown = EdUnidMedKeyDown
            DBLookupComboBox = CBUnidMed
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBUnidMed: TdmkDBLookupComboBox
            Left = 176
            Top = 24
            Width = 353
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsUnidMed
            TabOrder = 3
            OnKeyDown = EdUnidMedKeyDown
            dmkEditCB = EdUnidMed
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdSigla: TdmkEdit
            Left = 136
            Top = 24
            Width = 40
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdSiglaChange
            OnExit = EdSiglaExit
            OnKeyDown = EdSiglaKeyDown
          end
          object EdReferencia: TdmkEdit
            Left = 556
            Top = 24
            Width = 60
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Referencia'
            UpdCampo = 'Referencia'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 48
          Width = 627
          Height = 275
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 627
            Height = 275
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 627
              Height = 128
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 627
                Height = 73
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label5: TLabel
                  Left = 12
                  Top = 0
                  Width = 86
                  Height = 13
                  Caption = 'Peso espec'#237'fico '#178':'
                end
                object Label1: TLabel
                  Left = 308
                  Top = 0
                  Width = 85
                  Height = 13
                  Caption = '% comiss'#227'o fatur.:'
                end
                object Label4: TLabel
                  Left = 404
                  Top = 0
                  Width = 91
                  Height = 13
                  Caption = '% comiss'#227'o receb.:'
                end
                object EdPeso: TdmkEdit
                  Left = 12
                  Top = 16
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  QryCampo = 'Peso'
                  UpdCampo = 'Peso'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPerComissF: TdmkEdit
                  Left = 308
                  Top = 16
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 6
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000000'
                  QryCampo = 'PerComissF'
                  UpdCampo = 'PerComissF'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPerComissR: TdmkEdit
                  Left = 404
                  Top = 16
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 6
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '100'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000000'
                  QryCampo = 'PerComissR'
                  UpdCampo = 'PerComissR'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object CkPerComissZ: TdmkCheckBox
                  Left = 12
                  Top = 44
                  Width = 489
                  Height = 17
                  Caption = 
                    'N'#227'o usar percentuais de comiss'#227'o de outros n'#237'veis de cadastro de' +
                    ' produto.'
                  TabOrder = 3
                  QryCampo = 'PerComissZ'
                  UpdCampo = 'PerComissZ'
                  UpdType = utYes
                  ValCheck = '1'
                  ValUncheck = '0'
                  OldValor = #0
                end
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Dados espec'#237'ficos'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 627
        Height = 323
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object CGHowBxaEstq: TdmkCheckGroup
          Left = 0
          Top = 0
          Width = 627
          Height = 41
          Align = alTop
          Caption = 
            ' Grandezas extras na gera'#231#227'o, entrada e baixa (al'#233'm da unidade d' +
            'e medida informada nos dados gen'#233'ricos): '
          Columns = 3
          Items.Strings = (
            'Pe'#231'as'
            #193'rea (m'#178', in'#178')'
            'Peso (kg)')
          TabOrder = 0
          QryCampo = 'HowBxaEstq'
          UpdCampo = 'HowBxaEstq'
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object RGTipDimens: TdmkRadioGroup
          Left = 0
          Top = 41
          Width = 627
          Height = 64
          Align = alTop
          Caption = ' Mensuramento: (usado em personaliza'#231#227'o)'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Unimensur'#225'vel  -  pe'#231'a, kg, metros, litros, etc.'
            'Bimensur'#225'vell  -  '#193'rea (m'#178')'
            'Trimensur'#225'vel - Volume (m'#179' > Compr. x largura x altura)'
            'Quadrimensur'#225'vel - Volume x peso espec'#237'fico')
          TabOrder = 1
          QryCampo = 'TipDimens'
          UpdCampo = 'TipDimens'
          UpdType = utYes
          OldValor = 0
        end
        object Panel6: TPanel
          Left = 0
          Top = 105
          Width = 627
          Height = 89
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Label13: TLabel
            Left = 12
            Top = 4
            Width = 186
            Height = 13
            Caption = 'Ordem das medidas no mensuramento: '
          end
          object Label14: TLabel
            Left = 12
            Top = 44
            Width = 206
            Height = 13
            Caption = 'Parte principal em produtos personaliz'#225'veis:'
          end
          object SpeedButton2: TSpeedButton
            Left = 596
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object SpeedButton3: TSpeedButton
            Left = 596
            Top = 60
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton3Click
          end
          object EdMedOrdem: TdmkEditCB
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBMedOrdem
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBMedOrdem: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 521
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsMedOrdem
            TabOrder = 1
            dmkEditCB = EdMedOrdem
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdPartePrinc: TdmkEditCB
            Left = 12
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPartePrinc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPartePrinc: TdmkDBLookupComboBox
            Left = 72
            Top = 60
            Width = 521
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsMatPartCad
            TabOrder = 3
            dmkEditCB = EdPartePrinc
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 194
          Width = 627
          Height = 43
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object Label12: TLabel
            Left = 12
            Top = 0
            Width = 178
            Height = 13
            Caption = '% m'#237'nimo e m'#225'ximo de customiza'#231#227'o:'
          end
          object Label9: TLabel
            Left = 404
            Top = 0
            Width = 94
            Height = 13
            Caption = 'C'#243'digo do servi'#231'o '#179':'
          end
          object EdPerCuztMin: TdmkEdit
            Left = 12
            Top = 16
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'PerCuztMin'
            UpdCampo = 'PerCuztMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPerCuztMax: TdmkEdit
            Left = 112
            Top = 16
            Width = 96
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'PerCuztMax'
            UpdCampo = 'PerCuztMax'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCOD_LST: TdmkEdit
            Left = 404
            Top = 16
            Width = 117
            Height = 21
            MaxLength = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'COD_LST'
            UpdCampo = 'COD_LST'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdCOD_LSTChange
          end
        end
        object RGGerBxaEstq: TdmkRadioGroup
          Left = 0
          Top = 237
          Width = 627
          Height = 44
          Align = alTop
          Caption = 
            ' Unidade controladora do estoque (gera'#231#227'o de produtos e sub-prod' +
            'utos, NF-e, baixa de estoque, etc.): '
          Columns = 2
          Items.Strings = (
            'UnitListasGrade.sListaGerBxaEstq'
            '0(? ? ?) 1(Pe'#231'a) 2(m'#178') 3(kg) 4(t (ton)) 5(ft'#178')')
          TabOrder = 4
          QryCampo = 'GerBxaEstq'
          UpdCampo = 'GerBxaEstq'
          UpdType = utYes
          OldValor = 0
        end
        object RGFatorClas: TdmkRadioGroup
          Left = 0
          Top = 281
          Width = 627
          Height = 40
          Align = alTop
          Caption = 
            ' Qtde de pe'#231'as geradas deste produto ao clasificar o produto de ' +
            'origem: '
          Columns = 9
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8')
          TabOrder = 5
          QryCampo = 'FatorClas'
          UpdCampo = 'FatorClas'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Cont'#225'bil'
      ImageIndex = 2
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 627
        Height = 323
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 4
          Width = 72
          Height = 13
          Caption = 'Grupo cont'#225'bil:'
        end
        object Label15: TLabel
          Left = 12
          Top = 44
          Width = 142
          Height = 13
          Caption = 'Reduzido do plano de contas:'
        end
        object SbCtbCadGru: TSpeedButton
          Left = 584
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbCtbCadGruClick
        end
        object SbCtbPlaCta: TSpeedButton
          Left = 584
          Top = 60
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbCtbPlaCtaClick
        end
        object EdCtbCadGru: TdmkEditCB
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CtbCadGru'
          UpdCampo = 'CtbCadGru'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCtbCadGru
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCtbCadGru: TdmkDBLookupComboBox
          Left = 68
          Top = 20
          Width = 513
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCtbCadGru
          TabOrder = 1
          dmkEditCB = EdCtbCadGru
          QryCampo = 'CtbCadGru'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCtbPlaCta: TdmkEditCB
          Left = 12
          Top = 60
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CtbPlaCta'
          UpdCampo = 'CtbPlaCta'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCtbPlaCta
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCtbPlaCta: TdmkDBLookupComboBox
          Left = 68
          Top = 60
          Width = 513
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsPlaAllCad
          TabOrder = 3
          dmkEditCB = EdCtbPlaCta
          QryCampo = 'CtbPlaCta'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 399
    Width = 635
    Height = 73
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Label8: TLabel
      Left = 12
      Top = 20
      Width = 459
      Height = 13
      Caption = 
        #178': medidas para peso espec'#237'fico: kg ou kg/m'#178' ou kg/m'#179', conforme ' +
        'dimensionamento selecionado.'
    end
    object Label7: TLabel
      Left = 12
      Top = 36
      Width = 410
      Height = 13
      Caption = 
        #179': C'#243'digo do servi'#231'o conforme lista do Anexo I da Lei Complement' +
        'ar Federal n'#186' 116/03.'
    end
    object Label11: TLabel
      Left = 12
      Top = 52
      Width = 304
      Height = 13
      Caption = 
        #170': Informar somente se realizar venda pela refer'#234'ncia do produto' +
        '.'
    end
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 308
    Top = 216
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 308
    Top = 264
  end
  object QrMedOrdem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM medordem'
      'ORDER BY Nome')
    Left = 376
    Top = 216
    object QrMedOrdemCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMedOrdemCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMedOrdemNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMedOrdem: TDataSource
    DataSet = QrMedOrdem
    Left = 372
    Top = 264
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdPartePrinc
    Panel = TabSheet1
    QryCampo = 'MedOrdem'
    UpdCampo = 'MedOrdem'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 156
    Top = 8
  end
  object QrMatPartCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM matpartcad'
      'WHERE Parte=2'
      'OR Codigo=0'
      'ORDER BY Nome')
    Left = 456
    Top = 216
    object QrMatPartCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'matpartcad.Codigo'
    end
    object QrMatPartCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'matpartcad.CodUsu'
    end
    object QrMatPartCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'matpartcad.Nome'
      Size = 30
    end
  end
  object DsMatPartCad: TDataSource
    DataSet = QrMatPartCad
    Left = 456
    Top = 264
  end
  object dmkValUsu3: TdmkValUsu
    dmkEditCB = EdPartePrinc
    Panel = TabSheet1
    QryCampo = 'PartePrinc'
    UpdCampo = 'PartePrinc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 216
    Top = 8
  end
  object QrCtbCadGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ctbcadgru'
      'ORDER BY Nome')
    Left = 364
    Top = 66
    object QrCtbCadGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCtbCadGru: TDataSource
    DataSet = QrCtbCadGru
    Left = 364
    Top = 118
  end
  object QrPlaAllCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM plaallcad'
      'WHERE AnaliSinte=2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 471
    Top = 68
    object QrPlaAllCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCadNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCadAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCadCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCadDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCadOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object DsPlaAllCad: TDataSource
    DataSet = QrPlaAllCad
    Left = 471
    Top = 120
  end
end
