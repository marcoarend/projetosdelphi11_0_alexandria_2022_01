unit StqCnsgBkIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DB,
  mySQLDbTables, dmkRadioGroup, Mask, DBCtrls, dmkEdit, dmkCheckGroup,
  dmkGeral, dmkEditCalc, dmkImage, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox,
  dmkValUsu, UnProjGroup_Consts, UnStqPF, dmkDBGridZTO;

type
  TFmStqCnsgBkIts = class(TForm)
    Panel1: TPanel;
    PnData: TPanel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    DsGraGruX: TDataSource;
    QrGraGruXHowBxaEstq: TSmallintField;
    QrGraGruXGerBxaEstq: TSmallintField;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrGraGruXNo_SIGLA: TWideStringField;
    QrGraGruXSigla: TWideStringField;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    QrStqCenLocOri: TMySQLQuery;
    DsStqCenLocOri: TDataSource;
    QrStqCenLocOriControle: TIntegerField;
    QrStqCenLocOriNome: TWideStringField;
    QrGraGruXGrandeza: TIntegerField;
    QrStqCenCadOri: TMySQLQuery;
    QrStqCenCadOriCodigo: TIntegerField;
    QrStqCenCadOriCodUsu: TIntegerField;
    QrStqCenCadOriNome: TWideStringField;
    DsStqCenCadOri: TDataSource;
    QrGraGruXFracio: TIntegerField;
    EdCustoBuy: TdmkEdit;
    Label8: TLabel;
    EdCustoFrt: TdmkEdit;
    Label9: TLabel;
    EdValorAll: TdmkEdit;
    Label20: TLabel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    dmkLabel3: TdmkLabel;
    Label7: TLabel;
    EdStqCenCadOri: TdmkEditCB;
    CBStqCenCadOri: TdmkDBLookupComboBox;
    EdStqCenLocOri: TdmkEditCB;
    CBStqCenLocOri: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    dmkLabel1: TdmkLabel;
    Label2: TLabel;
    EdStqCenCadDst: TdmkEditCB;
    CBStqCenCadDst: TdmkDBLookupComboBox;
    EdStqCenLocDst: TdmkEditCB;
    CBStqCenLocDst: TdmkDBLookupComboBox;
    QrStqCenLocDst: TMySQLQuery;
    QrStqCenLocDstControle: TIntegerField;
    QrStqCenLocDstNome: TWideStringField;
    DsStqCenLocDst: TDataSource;
    DsStqCenCadDst: TDataSource;
    QrStqCenCadDst: TMySQLQuery;
    QrStqCenCadDstCodigo: TIntegerField;
    QrStqCenCadDstCodUsu: TIntegerField;
    QrStqCenCadDstNome: TWideStringField;
    QrEstoque: TMySQLQuery;
    QrEstoqueQtde: TFloatField;
    DsEstoque: TDataSource;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    DBEdit6: TDBEdit;
    Label4: TLabel;
    DBEdit7: TDBEdit;
    QrEstoqueCustoAll: TFloatField;
    SbPreco: TSpeedButton;
    Panel5: TPanel;
    PnGGX: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    Panel2: TPanel;
    Label11: TLabel;
    SpeedButton1: TSpeedButton;
    EdGraGruX: TdmkEdit;
    QrPreco: TMySQLQuery;
    QrPrecoPreco: TFloatField;
    Label5: TLabel;
    DBEdit8: TDBEdit;
    DsPreco: TDataSource;
    QrEstoqueCusMed: TFloatField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    SbImprime: TBitBtn;
    QrPrdEstq: TMySQLQuery;
    QrPrdEstqNivel1: TIntegerField;
    QrPrdEstqNO_PRD: TWideStringField;
    QrPrdEstqPrdGrupTip: TIntegerField;
    QrPrdEstqUnidMed: TIntegerField;
    QrPrdEstqNO_PGT: TWideStringField;
    QrPrdEstqSIGLA: TWideStringField;
    QrPrdEstqNO_TAM: TWideStringField;
    QrPrdEstqNO_COR: TWideStringField;
    QrPrdEstqGraCorCad: TIntegerField;
    QrPrdEstqGraGruC: TIntegerField;
    QrPrdEstqGraGru1: TIntegerField;
    QrPrdEstqGraTamI: TIntegerField;
    QrPrdEstqQTDE: TFloatField;
    QrPrdEstqPECAS: TFloatField;
    QrPrdEstqPESO: TFloatField;
    QrPrdEstqAREAM2: TFloatField;
    QrPrdEstqAREAP2: TFloatField;
    QrPrdEstqGraGruX: TIntegerField;
    QrPrdEstqNCM: TWideStringField;
    QrPrdEstqPrcCusUni: TFloatField;
    QrPrdEstqValorTot: TFloatField;
    QrPrdEstqNO_SCC: TWideStringField;
    QrPrdEstqCodUsu: TIntegerField;
    DsPrdEstq: TDataSource;
    QrPrdEstqNO_PRD_TAM_COR: TWideStringField;
    Panel3: TPanel;
    DBGPrdEstq: TdmkDBGridZTO;
    Label10: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdStqCenCadOriChange(Sender: TObject);
    procedure EdCustoBuyRedefinido(Sender: TObject);
    procedure EdCustoFrtRedefinido(Sender: TObject);
    procedure EdStqCenCadDstChange(Sender: TObject);
    procedure EdStqCenLocOriRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure SbPrecoClick(Sender: TObject);
    procedure EdGraGruXExit(Sender: TObject);
    procedure DBEdit5Change(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DBGPrdEstqDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Tipo: TSQLType);
    procedure ReabreEstoque();

  public
    { Public declarations }
    FTabePrcCab: Integer;
  end;

  var
  FmStqCnsgBkIts: TFmStqCnsgBkIts;

implementation

uses UnMyObjects, StqCnsgBkCab, UMySQLModule, Module, GraGruPesq1, MyDBCheck,
  ModuleGeral, DmkDAC_PF, ModProd, MyListas, GetValor, UnGrade_Jan, UnGrade_PF;

{$R *.DFM}

procedure TFmStqCnsgBkIts.BtOKClick(Sender: TObject);
const
  OriCnta = 0;
  FatorClas = 1;
  OriPart = 0;
var
  DataHora, Texto: String;
  GerBxaEstq,
  t, OriCodi, Empresa, TwnCtrl, GraGruX: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoBuy, CustoFrt, CustoAll, ValorAll,
  CusUni: Double;
  //
  function InsereItem(Tipo, OriCtrl, TwnBrth, Baixa, StqCenCad, StqCenLoc:
  Integer): Boolean;
  begin
    Result := False;
    //
    //OBS.: Ativo 0 para atualizar o estoque somente no encerramento
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovitsa', False, [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'Empresa',
      'StqCenCad', 'StqCenLoc',
      'GraGruX', 'Qtde',
      'OriPart', 'Pecas', 'Peso',
      'AreaM2', 'AreaP2', 'FatorClas',
      'CustoBuy', 'CustoFrt',
      'CustoAll', 'ValorAll', 'Ativo',
      'TwnCtrl', 'TwnBrth', 'Baixa'
      {, 'QuemUsou', 'Retorno', 'ParTipo',
      'ParCodi', 'DebCtrl', 'SMIMultIns'}], [
      'IDCtrl'], [
      DataHora, Tipo, OriCodi,
      OriCtrl, OriCnta, Empresa,
      StqCenCad, StqCenLoc,
      GraGruX, Qtde,
      OriPart, Pecas, Peso,
      AreaM2, AreaP2, FatorClas,
      CustoBuy, CustoFrt,
      CustoAll, ValorAll, 0,
      TwnCtrl, TwnBrth, Baixa
      {, QuemUsou, Retorno, ParTipo,
      ParCodi, DebCtrl, SMIMultIns}], [
      OriCtrl], False) then
    begin
      Result := True;
      ReabreEstoque();
  { N�o precisa?
  ? := UMyMod.BuscaEmLivreY_Def('stqmovvala', ''ID', ImgTipo.SQLType, CodAtual);
  if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd, ImgTipo.SQLType, 'stqmovvala', auto_increment?[
  'IDCtrl', 'Tipo', 'OriCodi',
  'OriCtrl', 'OriCnta', 'SeqInReduz',
  'Empresa', 'StqCenCad', 'GraGruX',
  'Qtde', 'Preco', 'Total',
  'CFOP', 'InfAdCuztm', 'PercCustom',
  'MedidaC', 'MedidaL', 'MedidaA',
  'MedidaE', 'MedOrdem', 'TipoNF',
  'refNFe', 'modNF', 'Serie',
  'nNF', 'SitDevolu', 'Servico',
  'RefProd', 'CFOP_Contrib', 'CFOP_MesmaUF',
  'CFOP_Proprio'], [
  'ID'], [
  IDCtrl, Tipo, OriCodi,
  OriCtrl, OriCnta, SeqInReduz,
  Empresa, StqCenCad, GraGruX,
  Qtde, Preco, Total,
  CFOP, InfAdCuztm, PercCustom,
  MedidaC, MedidaL, MedidaA,
  MedidaE, MedOrdem, TipoNF,
  refNFe, modNF, Serie,
  nNF, SitDevolu, Servico,
  RefProd, CFOP_Contrib, CFOP_MesmaUF,
  CFOP_Proprio], [
  ID], UserDataAlterweb?, IGNORE?
  }
    //
    end;
  end;
var
  IDCtrl1, IDCtrl2, StqCenCadOri, StqCenLocOri, StqCenCadDst, StqCenLocDst: Integer;
begin
  //
  DataHora  := DModG.ObtemAgoraTxt();
  OriCodi   := FmStqCnsgBkCab.QrStqCnsgBkCabCodigo.Value;
  Empresa   := FmStqCnsgBkCab.QrStqCnsgBkCabEmpresa.Value;
  StqCenCadOri := EdStqCenCadOri.ValueVariant;
  StqCenLocOri := EdStqCenLocOri.ValueVariant;
  StqCenCadDst := EdStqCenCadDst.ValueVariant;
  StqCenLocDst := EdStqCenLocDst.ValueVariant;
  //
  if MyObjects.FIC(StqCenCadOri = 0, EdStqCenCadOri, 'Defina o centro de estoque de origem!') then Exit;
  if MyObjects.FIC(StqCenCadDst = 0, EdStqCenCadDst, 'Defina o centro de estoque de destino!') then Exit;
  //
  if MyObjects.FIC(StqCenLocOri = 0, EdStqCenLocOri, 'Defina o local do centro de estoque de origem!') then Exit;
  if MyObjects.FIC(StqCenLocDst = 0, EdStqCenLocDst, 'Defina o local do centro de estoque de destino!') then Exit;
  //
  GraGruX   := EdGraGruX.ValueVariant;
  Pecas     := 0;// EdPecas.ValueVariant;
  Peso      := 0;// EdPLE.ValueVariant;
  AreaM2    := 0;// EdAreaM2.ValueVariant;
  AreaP2    := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  Qtde := EdQtde.ValueVariant;

  if QrEstoqueQtde.Value > 0 then
    CusUni := QrEstoqueCustoAll.Value / QrEstoqueQtde.Value
  else
    CusUni := 0.00;
  CustoAll := CusUni * Qtde;
  CustoBuy  := 0.00;
  CustoFrt  := 0.00;
  ValorAll  := 0.00;
  //
  if MyObjects.FIC(Qtde <= 0, nil, 'Quantidade n�o definida!') then Exit;
  if MyObjects.FIC(Qtde > QrEstoqueQtde.Value, nil, 'Estoque insuficiente!') then Exit;
  if MyObjects.FIC(CustoAll <= 0, nil, 'Custo total n�o definido!') then Exit;
  //
  Texto := '';
  t := QrGraGruXHowBxaEstq.Value;
  if GraGruX = 0 then
    Texto := Texto + sLineBreak + '-> Falta informar o reduzido.';
  if (Geral.IntInConjunto(1, t) and (Pecas = 0)) then
    Texto := Texto + sLineBreak + '-> Falta informar a quantidade de pe�as.';
  if (Geral.IntInConjunto(2, t) and (AreaM2 = 0)) then
    Texto := Texto + sLineBreak + '-> Falta informar a �rea.';
  if (Geral.IntInConjunto(4, t) and (Peso = 0)) then
    Texto := Texto + sLineBreak + '-> Falta informar o peso.';
  //
  if Texto <> '' then
  begin
    Texto := 'Neste item falta:' + Texto;
    Geral.MensagemBox(Texto, 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  IDCtrl1 :=  UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  IDCtrl2 :=  UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  TwnCtrl :=  UMyMod.BPGS1I32('stqmovitsa', 'TwnCtrl', '', '', tsPos, ImgTipo.SQLType, 0);
  if InsereItem(FmStqCnsgBkCab.FThisFatID_Ret_Bxa, IDCtrl1, IDCtrl2, (*Baixa*) -1, StqCenCadOri, StqCenLocOri) then
  begin
    CustoBuy  := EdCustoBuy.ValueVariant;
    CustoFrt  := EdCustoFrt.ValueVariant;
    ValorAll  := EdValorAll.ValueVariant;
    if InsereItem(FmStqCnsgBkCab.FThisFatID_Ret_Inn, IDCtrl2, IDCtrl1, (*Baixa*) 1, StqCenCadDst, StqCenLocDst) then
    begin
      if not CkContinuar.Checked then
      begin
        FmStqCnsgBkCab.LocCod(
          FmStqCnsgBkCab.QrStqCnsgBkCabCodigo.Value, FmStqCnsgBkCab.QrStqCnsgBkCabCodigo.Value);
        FmStqCnsgBkCab.ReopenStqMovIts(IDCtrl2);
        Close;
      end else
      begin
        FmStqCnsgBkCab.ReopenStqMovIts(IDCtrl2);
        MostraEdicao(stIns);
      end;
    end;
  end;
end;

procedure TFmStqCnsgBkIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqCnsgBkIts.DBEdit5Change(Sender: TObject);
begin
  LaQtde.Caption := DBEdit5.Text + ':';
end;

procedure TFmStqCnsgBkIts.DBGPrdEstqDblClick(Sender: TObject);
begin
  if (QrPrdEstq.State <> dsInactive) and (QrPrdEstq.RecordCount > 0) then
  begin
    EdGraGruX.ValueVariant := QrPrdEstqGraGruX.Value;
    EdQtde.ValueVariant := QrPrdEstqQtde.Value;
    EdQtde.SetFocus;
  end;
end;

procedure TFmStqCnsgBkIts.EdCustoBuyRedefinido(Sender: TObject);
begin
  StqPF.CalculaValorAll(EdCustoBuy, EdCustoFrt, EdValorAll);
end;

procedure TFmStqCnsgBkIts.EdCustoFrtRedefinido(Sender: TObject);
begin
  StqPF.CalculaValorAll(EdCustoBuy, EdCustoFrt, EdValorAll);
end;

procedure TFmStqCnsgBkIts.EdGraGruXChange(Sender: TObject);
begin
  StqPF.VerificaPainelGGX(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;

procedure TFmStqCnsgBkIts.EdGraGruXExit(Sender: TObject);
begin
  if PnData.Visible then
    EdStqCenCadOri.SetFocus
end;

procedure TFmStqCnsgBkIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    StqPF.AbrePesquisa(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;

procedure TFmStqCnsgBkIts.EdGraGruXRedefinido(Sender: TObject);
begin
  StqPF.ReopenEstoque(QrEstoque, EdGraGruX, EdStqCenLocOri);
  StqPF.DefinePreco(QrPreco, EdGraGruX.ValueVariant, FTabePrcCab);
end;

procedure TFmStqCnsgBkIts.EdQtdeRedefinido(Sender: TObject);
begin
  StqPF.DefineCustoRem(QrPreco, EdQtde, EdCustoBuy);
end;

procedure TFmStqCnsgBkIts.SbImprimeClick(Sender: TObject);
const
  PrdGrupTip = 0;
  GraGru1 = 0;
  Aba = 0;
var
  StqCenCadOri: Integer;
begin
  StqCenCadOri := EdStqCenCadOri.ValueVariant;
  //
  Grade_Jan.MostraFormGraImpLista(PrdGrupTip, GraGru1, Aba, StqCenCadOri);
end;

procedure TFmStqCnsgBkIts.SbPrecoClick(Sender: TObject);
  function ObtemPrecoManual(var Preco: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Preco        := 0;
    Result        := False;
    CasasDecimais := 2;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'Pre�o', 'Informe o pre�o: ',
    0, ResVar) then
    begin
      Preco := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  Preco, Qtde, CustoBuy: Double;
begin
  if QrPreco.State <> dsInactive then
    Preco := QrPrecoPreco.Value
  else
    Preco := 0.00;
  if ObtemPrecoManual(Preco) then
  begin
    Qtde := EdQtde.ValueVariant;
    CustoBuy := Qtde * Preco;
    EdCustoBuy.ValueVariant := CustoBuy;
  end;
end;

procedure TFmStqCnsgBkIts.SpeedButton1Click(Sender: TObject);
begin
  StqPF.AbrePesquisa(QrGraGruX, EdGraGruX, PnGGX, PnData);
end;

procedure TFmStqCnsgBkIts.EdStqCenCadDstChange(Sender: TObject);
var
  StqCenCad: Integer;
begin
  StqCenCad := EdStqCenCadDst.ValueVariant;
  //
  if StqCenCad <> 0 then
    StqPF.ReopenStqCenLoc(StqCenCad, QrStqCenLocDst, EdStqCenLocDst, CBStqCenLocDst);
end;

procedure TFmStqCnsgBkIts.EdStqCenCadOriChange(Sender: TObject);
var
  StqCenCad: Integer;
begin
  StqCenCad := EdStqCenCadOri.ValueVariant;
  //
  if StqCenCad <> 0 then
  begin
    ReabreEstoque();
    StqPF.ReopenStqCenLoc(StqCenCad, QrStqCenLocOri, EdStqCenLocOri, CBStqCenLocOri);
  end;
end;

procedure TFmStqCnsgBkIts.EdStqCenLocOriRedefinido(Sender: TObject);
begin
  StqPF.ReopenEstoque(QrEstoque, EdGraGruX, EdStqCenLocOri);
end;

procedure TFmStqCnsgBkIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqCnsgBkIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  StqPF.ReopenStqCenCad(QrStqCenCadOri,
    FmStqCnsgBkCab.QrStqCnsgBkCabFornece.Value, EdStqCenCadOri, CBStqCenCadOri, cmprIgual);
  StqPF.ReopenStqCenCad(QrStqCenCadDst,
    FmStqCnsgBkCab.QrStqCnsgBkCabEmpresa.Value, EdStqCenCadDst, CBStqCenCadDst, cmprIgual);
  //
  StqPF.ReopenGraGruX(QrGraGruX);
  //
  CkContinuar.Checked := ImgTipo.SQLType = stIns;
end;

procedure TFmStqCnsgBkIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCnsgBkIts.MostraEdicao(Tipo: TSQLType);
begin
  case Tipo of
    stIns:
    begin
      EdGraGruX.ValueVariant  := '';
      EdValorAll.ValueVariant := 0;
      //
      EdQtde.ValueVariant     := 0;
      EdCustoBuy.ValueVariant := 0;
      EdCustoFrt.ValueVariant := 0;
      //
      StqPF.VerificaPainelGGX(QrGraGruX, EdGraGruX, PnGGX, PnData);
    end;
  end;
  EdGraGruX.SetFocus;
end;

procedure TFmStqCnsgBkIts.ReabreEstoque();
const
  CGSitEstq_Value = 5;
  CkStqCenCad_Checked = True;
  CkSelecao0_Enabled = False;
  CkSelecao0_Checked = False;
  CkPrdGrupo_Checked = False;
var
  Filial, Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip, GraCusPrc, StqCenCad,
  InatIncTipo, InatIncOriCodi: Integer;
  SQL_Sum, SQL_Its: String;
begin
  DBGPrdEstq.Visible := False;
  //
  Empresa    := FmStqCnsgBkCab.QrStqCnsgBkCabEmpresa.Value;
  StqCenCad  := EdStqCenCadOri.ValueVariant;
  GraGru1    := 0; //EdGraGru1.ValueVariant;
  GraGru2    := 0; //EdGraGru2.ValueVariant;
  GraGru3    := 0; //EdGraGru3.ValueVariant;
  PrdGrupTip := 0; //EdPrdGrupTip.ValueVariant;
  GraCusPrc  := 0; //EdGraCusPrc.ValueVariant;
  //
  InatIncTipo    := FmStqCnsgBkCab.FThisFatID_Ret_Bxa;
  InatIncOriCodi := FmStqCnsgBkCab.QrStqCnsgBkCabCodigo.Value;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando');
  //
  if Grade_PF.SQL_Estoque(Empresa, GraGru1, GraGru2, GraGru3, PrdGrupTip,
  StqCenCad, GraCusPrc, CGSitEstq_Value, CkStqCenCad_Checked,
  CkSelecao0_Enabled, CkSelecao0_Checked, CkPrdGrupo_Checked,
  InatIncTipo, InatIncOriCodi, LaAviso1, LaAviso2, SQL_Sum, SQL_Its) then
  begin
    UnDmkDAC_PF.ExecutaDB(DMod.MyDB, SQL_Sum);
    //Geral.MB_Teste(QrSumA2.SQl.Text);

    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrdEstq, DModG.MyPID_DB, [SQL_Its]);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    DBGPrdEstq.Visible := True;
    QrPrdEstq.EnableControls;
    DBGPrdEstq.DataSource := DsPrdEstq;
  end;
end;

end.
