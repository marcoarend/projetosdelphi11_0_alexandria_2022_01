object FmGraGruCST: TFmGraGruCST
  Left = 339
  Top = 185
  Caption = 'FIS-REGRA-004 :: Impostos entre UFs por CFOP'
  ClientHeight = 593
  ClientWidth = 518
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 518
    Height = 437
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 65
      Width = 518
      Height = 372
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 100
        Height = 372
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 12
          Top = 8
          Width = 14
          Height = 13
          Caption = 'ID:'
          Enabled = False
        end
        object Label24: TLabel
          Left = 12
          Top = 48
          Width = 51
          Height = 13
          Caption = 'UF origem:'
        end
        object Label1: TLabel
          Left = 12
          Top = 88
          Width = 54
          Height = 13
          Caption = 'UF destino:'
        end
        object dmkEdit5: TdmkEdit
          Left = 12
          Top = 24
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdUF_Orig: TdmkEdit
          Left = 13
          Top = 64
          Width = 44
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtUF
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'EUF'
          UpdCampo = 'EUF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdUF_Dest: TdmkEdit
          Left = 13
          Top = 104
          Width = 44
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtUF
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'EUF'
          UpdCampo = 'EUF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 100
        Top = 0
        Width = 418
        Height = 372
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'UF'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'Adiciona'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsUFsAtoB
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = dmkDBGrid1CellClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'UF'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'Adiciona'
            Visible = True
          end>
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 518
      Height = 65
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label11: TLabel
        Left = 8
        Top = 4
        Width = 231
        Height = 13
        Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST ICMS): [F3]'
      end
      object Label6: TLabel
        Left = 8
        Top = 44
        Width = 482
        Height = 13
        Caption = 
          'Aten'#231#227'o! Ao informar o CST aqui, ele sobrepor'#225' o informado no ca' +
          'dastro do produto!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdCST_B: TdmkEdit
        Left = 8
        Top = 20
        Width = 29
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        QryCampo = 'CST_B'
        UpdCampo = 'CST_B'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCST_BChange
        OnKeyDown = EdCST_BKeyDown
      end
      object EdTextoB: TdmkEdit
        Left = 39
        Top = 20
        Width = 470
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 518
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 470
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 422
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 366
        Height = 32
        Caption = 'Impostos entre UFs por CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 366
        Height = 32
        Caption = 'Impostos entre UFs por CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 366
        Height = 32
        Caption = 'Impostos entre UFs por CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 485
    Width = 518
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 514
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 529
    Width = 518
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 514
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 370
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 139
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Marca todos itens'
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 231
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Desmarca todos itens'
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object DsUFsAtoB: TDataSource
    DataSet = QrUFsAtoB
    Left = 228
    Top = 52
  end
  object QrUFsAtoB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ufsatob')
    Left = 200
    Top = 52
    object QrUFsAtoBNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrUFsAtoBAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
end
