object FmTXTrflocIMEI: TFmTXTrflocIMEI
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-132 :: Transfer'#234'ncia de Local de Estoque TX - por IMEI'
  ClientHeight = 637
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 498
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Label4: TLabel
      Left = 328
      Top = 8
      Width = 29
      Height = 13
      Caption = 'Pallet:'
      Visible = False
    end
    object SBPallet: TSpeedButton
      Left = 452
      Top = 4
      Width = 21
      Height = 21
      Caption = '...'
      Visible = False
      OnClick = SBPalletClick
    end
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object EdPallet: TdmkEditCB
      Left = 360
      Top = 4
      Width = 25
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Pallet'
      UpdCampo = 'Pallet'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPallet
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPallet: TdmkDBLookupComboBox
      Left = 384
      Top = 4
      Width = 69
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTXPallet
      TabOrder = 2
      Visible = False
      OnKeyDown = CBGraGruXKeyDown
      dmkEditCB = EdPallet
      QryCampo = 'Pallet'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 591
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque TX - por IMEI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 591
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque TX - por IMEI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 591
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque TX - por IMEI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 523
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 756
        Height = 17
        Caption = 
          'Somente itens rastre'#225'veis ser'#227'o mostrados.     F4 na caixa de pe' +
          #231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de origem!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 756
        Height = 17
        Caption = 
          'Somente itens rastre'#225'veis ser'#227'o mostrados.     F4 na caixa de pe' +
          #231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de origem!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 567
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 136
        Top = 5
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReabreClick
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 225
    Width = 1008
    Height = 152
    Align = alTop
    DataSource = DsTXMovIts
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'NFeSer'
        Title.Caption = 'S'#233'rie '
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NFeNum'
        Title.Caption = 'N'#176' NFe'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Title.Caption = 'IME-I'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Ficha'
        Title.Caption = 'Ficha RMP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_FORNECE'
        Title.Caption = 'Fornecedor'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pallet'
        Title.Caption = 'ID Pallet'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_Pallet'
        Title.Caption = 'Pallet'
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_PRD_TAM_COR'
        Title.Caption = 'Artigo'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoVrtQtd'
        Title.Caption = 'Saldo Qtde'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MovimCod'
        Title.Caption = 'ID Estoque'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Qtde'
        Title.Caption = 'Quantidade'
        Visible = True
      end>
  end
  object Panel3: TPanel
    Left = 0
    Top = 410
    Width = 1008
    Height = 44
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 245
      Height = 44
      Align = alLeft
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label14: TLabel
        Left = 8
        Top = 0
        Width = 53
        Height = 13
        Caption = 'ID Reclas.:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 68
        Top = 0
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 128
        Top = 0
        Width = 43
        Height = 13
        Caption = 'ID Pallet:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 188
        Top = 0
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdSrcMovID: TdmkEdit
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSrcMovIDChange
      end
      object EdSrcNivel1: TdmkEdit
        Left = 68
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSrcNivel1Change
      end
      object EdSrcNivel2: TdmkEdit
        Left = 128
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSrcNivel2Change
      end
      object EdSrcGGX: TdmkEdit
        Left = 188
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSrcNivel2Change
      end
    end
    object Panel6: TPanel
      Left = 245
      Top = 0
      Width = 763
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object LaQtde: TLabel
        Left = 8
        Top = 0
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object LaValorT: TLabel
        Left = 89
        Top = 0
        Width = 53
        Height = 13
        Caption = 'Custo total:'
        Enabled = False
      end
      object SbValorT: TSpeedButton
        Left = 164
        Top = 16
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbValorTClick
      end
      object Label19: TLabel
        Left = 188
        Top = 0
        Width = 149
        Height = 13
        Caption = 'Material usado para emitir NF-e:'
        Enabled = False
      end
      object EdQtde: TdmkEdit
        Left = 8
        Top = 16
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdQtdeKeyDown
        OnRedefinido = EdQtdeRedefinido
      end
      object EdValorT: TdmkEdit
        Left = 91
        Top = 16
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ValorT'
        UpdCampo = 'ValorT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdValorTKeyDown
      end
      object EdGGXRcl: TdmkEditCB
        Left = 188
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGXRcl'
        UpdCampo = 'GGXRcl'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGGXRcl
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGGXRcl: TdmkDBLookupComboBox
        Left = 246
        Top = 16
        Width = 511
        Height = 21
        Enabled = False
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXRcl
        TabOrder = 3
        dmkEditCB = EdGGXRcl
        QryCampo = 'GGXRcl'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 80
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 412
      Height = 80
      Align = alClient
      Caption = ' Dados do cabe'#231'alho:'
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 20
        Width = 53
        Height = 13
        Caption = 'ID entrada:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 96
        Top = 20
        Width = 55
        Height = 13
        Caption = 'ID estoque:'
        FocusControl = DBEdMovimCod
      end
      object Label3: TLabel
        Left = 180
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdEmpresa
      end
      object Label7: TLabel
        Left = 228
        Top = 20
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        FocusControl = DBEdDtEntrada
      end
      object Label8: TLabel
        Left = 344
        Top = 20
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdCliVenda
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdMovimCod: TdmkDBEdit
        Left = 96
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdEmpresa: TdmkDBEdit
        Left = 180
        Top = 36
        Width = 45
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdDtEntrada: TdmkDBEdit
        Left = 228
        Top = 36
        Width = 112
        Height = 21
        TabStop = False
        DataField = 'DtVenda'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 3
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdCliVenda: TdmkDBEdit
        Left = 344
        Top = 36
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
    end
    object RGTipoCouro: TRadioGroup
      Left = 412
      Top = 0
      Width = 596
      Height = 80
      Align = alRight
      Caption = ' Tipo de Couro: '
      Columns = 2
      ItemIndex = 2
      Items.Strings = (
        'Mat'#233'ria-prima In Natura'
        'Artigo de Ribeira'
        'Artigo de Ribeira Classificado'
        'Artigo em Opera'#231#227'o'
        'Artigo Semi Acabado'
        'Artigo Acabado'
        'Sub Produto')
      TabOrder = 1
      OnClick = RGTipoCouroClick
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 128
    Width = 1008
    Height = 97
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 8
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 553
      Height = 97
      Align = alLeft
      Caption = ' Dados do item / filtros: '
      TabOrder = 0
      object Label6: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label1: TLabel
        Left = 96
        Top = 16
        Width = 155
        Height = 13
        Caption = 'Mat'#233'ria-prima (F4 - '#250'ltima usada):'
      end
      object Label39: TLabel
        Left = 280
        Top = 56
        Width = 82
        Height = 13
        Caption = 'Parte do material:'
      end
      object Label38: TLabel
        Left = 12
        Top = 56
        Width = 78
        Height = 13
        Caption = 'Tipo de material:'
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 152
        Top = 32
        Width = 393
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 2
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraGruX: TdmkEditCB
        Left = 96
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdGraGruXKeyDown
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdCouNiv2: TdmkEditCB
        Left = 12
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv2'
        UpdCampo = 'CouNiv2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCouNiv2
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCouNiv1: TdmkDBLookupComboBox
        Left = 336
        Top = 72
        Width = 208
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCouNiv1
        TabOrder = 4
        dmkEditCB = EdCouNiv1
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv1'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCouNiv1: TdmkEditCB
        Left = 280
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv1'
        UpdCampo = 'CouNiv1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCouNiv1
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCouNiv2: TdmkDBLookupComboBox
        Left = 68
        Top = 72
        Width = 208
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCouNiv2
        TabOrder = 6
        dmkEditCB = EdCouNiv2
        QryName = 'QrGrGruXCou'
        QryCampo = 'CouNiv2'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GroupBox3: TGroupBox
      Left = 553
      Top = 0
      Width = 455
      Height = 97
      Align = alClient
      Caption = ' Filtros: '
      TabOrder = 1
      object Label10: TLabel
        Left = 12
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Pallet:'
      end
      object Label11: TLabel
        Left = 96
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Ficha:'
      end
      object Label13: TLabel
        Left = 180
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label15: TLabel
        Left = 12
        Top = 55
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object EdPsqPallet: TdmkEdit
        Left = 12
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPsqPalletChange
      end
      object EdPsqFicha: TdmkEdit
        Left = 96
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPsqFichaChange
      end
      object EdPsqIMEI: TdmkEdit
        Left = 180
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPsqFichaChange
      end
      object CBStqCenLocSrc: TdmkDBLookupComboBox
        Left = 68
        Top = 71
        Width = 377
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 3
        dmkEditCB = EdStqCenLocSrc
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStqCenLocSrc: TdmkEditCB
        Left = 12
        Top = 71
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'StqCenLoc'
        UpdCampo = 'StqCenLoc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLocSrc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 454
    Width = 1008
    Height = 44
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Label49: TLabel
      Left = 12
      Top = 3
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
    end
    object Label50: TLabel
      Left = 510
      Top = 3
      Width = 74
      Height = 13
      Caption = 'Req.Mov.Estq.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label16: TLabel
      Left = 608
      Top = 3
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object EdStqCenLoc: TdmkEditCB
      Left = 12
      Top = 19
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStqCenLoc: TdmkDBLookupComboBox
      Left = 67
      Top = 19
      Width = 438
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 1
      dmkEditCB = EdStqCenLoc
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdReqMovEstq: TdmkEdit
      Left = 511
      Top = 19
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdObserv: TdmkEdit
      Left = 608
      Top = 19
      Width = 393
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Observ'
      UpdCampo = 'Observ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 108
    Top = 196
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 108
    Top = 248
  end
  object QrTXPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txpallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 36
    Top = 196
    object QrTXPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXPallet: TDataSource
    DataSet = QrTXPallet
    Left = 36
    Top = 248
  end
  object QrTXMovIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pal.Nome NO_Pallet, tmi.*  '
      'FROM txmovits tmi '
      'LEFT JOIN txpalleta pal ON pal.Codigo=tmi.Pallet '
      'WHERE tmi.Empresa=-11 '
      'AND tmi.GraGruX=3328 '
      'AND ( '
      '  tmi.MovimID=1 '
      '  OR  '
      '  tmi.SrcMovID<>0 '
      ') '
      'AND tmi.SdoVrtQtd > 0 '
      'ORDER BY DataHora, Pallet ')
    Left = 488
    Top = 240
    object QrTXMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrTXMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTXMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrTXMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrTXMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrTXMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXMovItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrTXMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrTXMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrTXMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMovItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTXMovItsTalao: TIntegerField
      FieldName = 'Talao'
    end
    object QrTXMovItsSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrTXMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrTXMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrTXMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrTXMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrTXMovItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrTXMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrTXMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrTXMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrTXMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrTXMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrTXMovItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrTXMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrTXMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrTXMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXMovItsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrTXMovItsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrTXMovItsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrTXMovItsTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrTXMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
    object QrTXMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrTXMovItsNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrTXMovItsNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
  end
  object DsTXMovIts: TDataSource
    DataSet = QrTXMovIts
    Left = 492
    Top = 292
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 260
    Top = 212
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 260
    Top = 260
  end
  object QrGGXRcl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 36
    Top = 304
    object QrGGXRclGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXRclSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXRclCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXRclNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 36
    Top = 356
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 340
    Top = 244
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
    object QrStqCenLocEntiSitio: TIntegerField
      FieldName = 'EntiSitio'
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 340
    Top = 292
  end
  object QrCouNiv1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 592
    Top = 248
    object QrCouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv1: TDataSource
    DataSet = QrCouNiv1
    Left = 592
    Top = 296
  end
  object QrCouNiv2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 668
    Top = 248
    object QrCouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv2: TDataSource
    DataSet = QrCouNiv2
    Left = 668
    Top = 296
  end
end
