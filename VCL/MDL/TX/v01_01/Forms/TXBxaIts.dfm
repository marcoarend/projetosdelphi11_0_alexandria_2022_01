object FmTXBxaIts: TFmTXBxaIts
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-028 :: Item de Baixa For'#231'ada de Couro na Ribeira'
  ClientHeight = 491
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 289
    Width = 875
    Height = 26
    BevelOuter = bvNone
    TabOrder = 0
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 273
        Height = 32
        Caption = 'Item de Baixa For'#231'ada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 273
        Height = 32
        Caption = 'Item de Baixa For'#231'ada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 273
        Height = 32
        Caption = 'Item de Baixa For'#231'ada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 377
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 447
        Height = 17
        Caption = 
          'F4 na caixa de pe'#231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de orige' +
          'm!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 447
        Height = 17
        Caption = 
          'F4 na caixa de pe'#231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de orige' +
          'm!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 421
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 256
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtExcluiClick
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 329
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object DBGrid1: TDBGrid
      Left = 405
      Top = 0
      Width = 603
      Height = 329
      Align = alClient
      DataSource = DsTXMovIts
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtQtd'
          Title.Caption = 'Sdo Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Qtde original'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MovimCod'
          Title.Caption = 'ID Estoque'
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 405
      Height = 329
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 64
        Width = 405
        Height = 265
        Align = alClient
        Caption = ' Dados do item: '
        TabOrder = 0
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label1: TLabel
          Left = 12
          Top = 56
          Width = 66
          Height = 13
          Caption = 'Mat'#233'ria-prima:'
        end
        object LaQtde: TLabel
          Left = 12
          Top = 177
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label4: TLabel
          Left = 12
          Top = 96
          Width = 29
          Height = 13
          Caption = 'Pallet:'
        end
        object SBPallet: TSpeedButton
          Left = 381
          Top = 113
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBPalletClick
        end
        object Label9: TLabel
          Left = 12
          Top = 217
          Width = 184
          Height = 13
          Caption = 'Observa'#231#227'o (Motivo da baixa for'#231'ada):'
        end
        object Label5: TLabel
          Left = 128
          Top = 177
          Width = 71
          Height = 13
          Caption = 'Valor total [F5]:'
        end
        object Label14: TLabel
          Left = 12
          Top = 137
          Width = 53
          Height = 13
          Caption = 'ID Reclas.:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object Label10: TLabel
          Left = 108
          Top = 137
          Width = 56
          Height = 13
          Caption = 'ID Estoque:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object Label17: TLabel
          Left = 204
          Top = 137
          Width = 43
          Height = 13
          Caption = 'ID Pallet:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object Label13: TLabel
          Left = 300
          Top = 137
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object EdControle: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 68
          Top = 72
          Width = 333
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsGraGruX
          TabOrder = 2
          OnKeyDown = CBGraGruXKeyDown
          dmkEditCB = EdGraGruX
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdGraGruX: TdmkEditCB
          Left = 12
          Top = 72
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdGraGruXKeyDown
          OnRedefinido = EdGraGruXRedefinido
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdQtde: TdmkEdit
          Left = 12
          Top = 193
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Qtde'
          UpdCampo = 'Qtde'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdQtdeKeyDown
        end
        object EdPsqPallet: TdmkEditCB
          Left = 12
          Top = 112
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pallet'
          UpdCampo = 'Pallet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdPsqPalletRedefinido
          DBLookupComboBox = CBPsqPallet
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPsqPallet: TdmkDBLookupComboBox
          Left = 68
          Top = 112
          Width = 309
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTXPallet
          TabOrder = 4
          OnKeyDown = CBGraGruXKeyDown
          dmkEditCB = EdPsqPallet
          QryCampo = 'Pallet'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdObserv: TdmkEdit
          Left = 12
          Top = 233
          Width = 389
          Height = 21
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ'
          UpdCampo = 'Observ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdValorT: TdmkEdit
          Left = 128
          Top = 193
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdValorTKeyDown
        end
        object EdSrcMovID: TdmkEdit
          Left = 12
          Top = 153
          Width = 92
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcMovIDChange
        end
        object EdSrcNivel1: TdmkEdit
          Left = 108
          Top = 153
          Width = 92
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'MovimCod'
          UpdCampo = 'MovimCod'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcNivel1Change
        end
        object EdSrcNivel2: TdmkEdit
          Left = 204
          Top = 153
          Width = 92
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcNivel2Change
        end
        object EdSrcGGX: TdmkEdit
          Left = 300
          Top = 153
          Width = 92
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdSrcNivel2Change
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 405
        Height = 64
        Align = alTop
        Caption = ' Dados do cabe'#231'alho:'
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 12
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label7: TLabel
          Left = 96
          Top = 20
          Width = 32
          Height = 13
          Caption = 'IME-C:'
          FocusControl = DBEdMovimCod
        end
        object Label8: TLabel
          Left = 180
          Top = 20
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdEmpresa
        end
        object Label12: TLabel
          Left = 228
          Top = 20
          Width = 58
          Height = 13
          Caption = 'Data / hora:'
          FocusControl = DBEdDtEntrada
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 12
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdMovimCod: TdmkDBEdit
          Left = 96
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'MovimCod'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdEmpresa: TdmkDBEdit
          Left = 180
          Top = 36
          Width = 45
          Height = 21
          TabStop = False
          DataField = 'Empresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdDtEntrada: TdmkDBEdit
          Left = 228
          Top = 36
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'DtBaixa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 652
    Top = 172
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 652
    Top = 216
  end
  object QrTXPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wbpallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 724
    Top = 68
    object QrTXPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXPallet: TDataSource
    DataSet = QrTXPallet
    Left = 724
    Top = 116
  end
  object QrTXMovIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 652
    Top = 68
    object QrTXMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrTXMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTXMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrTXMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrTXMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrTXMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXMovItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrTXMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrTXMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrTXMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTXMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrTXMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrTXMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrTXMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrTXMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrTXMovItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrTXMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrTXMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrTXMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrTXMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrTXMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrTXMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrTXMovItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrTXMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrTXMovItsTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrTXMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrTXMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXMovItsSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrTXMovItsTalao: TIntegerField
      FieldName = 'Talao'
    end
  end
  object DsTXMovIts: TDataSource
    DataSet = QrTXMovIts
    Left = 652
    Top = 116
  end
end
