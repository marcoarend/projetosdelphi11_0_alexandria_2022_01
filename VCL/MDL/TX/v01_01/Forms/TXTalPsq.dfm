object FmTXTalPsq: TFmTXTalPsq
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-037 :: Pesquisa de Movimento de Materiais'
  ClientHeight = 676
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 446
        Height = 32
        Caption = 'Pesquisa de Movimento de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 446
        Height = 32
        Caption = 'Pesquisa de Movimento de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 446
        Height = 32
        Caption = 'Pesquisa de Movimento de Materiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 514
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 514
        Align = alClient
        TabOrder = 0
        object DGDados: TDBGrid
          Left = 2
          Top = 257
          Width = 1004
          Height = 255
          Align = alClient
          DataSource = DsPesq
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DGDadosDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieTal'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Talao'
              Title.Caption = 'Ficha RMP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Marca'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'Descri'#231#227'o do n'#237'vel'
              Width = 221
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigo / tamanho / cor'
              Width = 274
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliVenda'
              Title.Caption = 'Cli.Venda'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOUni'
              Title.Caption = 'Custo MO Kg'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 242
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 808
            Height = 242
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label11: TLabel
              Left = 99
              Top = 16
              Width = 68
              Height = 13
              Caption = 'S'#233'rie do tal'#227'o:'
            end
            object Label4: TLabel
              Left = 316
              Top = 16
              Width = 30
              Height = 13
              Caption = 'Tal'#227'o:'
            end
            object Label12: TLabel
              Left = 416
              Top = 16
              Width = 33
              Height = 13
              Caption = 'Marca:'
            end
            object Label6: TLabel
              Left = 12
              Top = 16
              Width = 28
              Height = 13
              Caption = 'IME-I:'
            end
            object Label1: TLabel
              Left = 516
              Top = 16
              Width = 29
              Height = 13
              Caption = 'Pallet:'
            end
            object Label2: TLabel
              Left = 12
              Top = 56
              Width = 57
              Height = 13
              Caption = 'Fornecedor:'
            end
            object Label22: TLabel
              Left = 480
              Top = 56
              Width = 135
              Height = 13
              Caption = 'Motorista: (entrada in natura)'
            end
            object Label3: TLabel
              Left = 12
              Top = 96
              Width = 77
              Height = 13
              Caption = 'IME-I de origem:'
            end
            object Label5: TLabel
              Left = 96
              Top = 96
              Width = 80
              Height = 13
              Caption = 'IME-I de destino:'
            end
            object Label7: TLabel
              Left = 180
              Top = 96
              Width = 444
              Height = 13
              Caption = 
                'Observa'#231#245'es (Pode ser parcial. Use % para ignorar partes interme' +
                'di'#225'rias no meio da senten'#231'a:'
            end
            object Label8: TLabel
              Left = 12
              Top = 136
              Width = 134
              Height = 13
              Caption = 'Fornecedor de m'#227'o de obra:'
            end
            object Label9: TLabel
              Left = 408
              Top = 136
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object Label10: TLabel
              Left = 488
              Top = 196
              Width = 63
              Height = 13
              Caption = 'N'#250'mero NFe:'
            end
            object CBSerieTal: TdmkDBLookupComboBox
              Left = 139
              Top = 32
              Width = 169
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTXSerTal
              TabOrder = 2
              dmkEditCB = EdSerieTal
              QryCampo = 'SerieTal'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdSerieTal: TdmkEditCB
              Left = 99
              Top = 32
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'SerieTal'
              UpdCampo = 'SerieTal'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSerieTal
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdTalao: TdmkEdit
              Left = 316
              Top = 32
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Talao'
              UpdCampo = 'Talao'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdMarca: TdmkEdit
              Left = 416
              Top = 32
              Width = 93
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Marca'
              UpdCampo = 'Marca'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdControle: TdmkEdit
              Left = 12
              Top = 32
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Controle'
              UpdCampo = 'Controle'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object BtReabre: TBitBtn
              Tag = 18
              Left = 683
              Top = 21
              Width = 120
              Height = 40
              Caption = '&Reabre'
              NumGlyphs = 2
              TabOrder = 19
              OnClick = BtReabreClick
            end
            object EdPallet: TdmkEdit
              Left = 516
              Top = 32
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Pallet'
              UpdCampo = 'Pallet'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdFornecedor: TdmkEditCB
              Left = 12
              Top = 72
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFornecedor
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFornecedor: TdmkDBLookupComboBox
              Left = 68
              Top = 72
              Width = 408
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsFornecedor
              TabOrder = 8
              dmkEditCB = EdFornecedor
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdMotorista: TdmkEditCB
              Left = 480
              Top = 73
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Motorista'
              UpdCampo = 'Motorista'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMotorista
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBMotorista: TdmkDBLookupComboBox
              Left = 536
              Top = 73
              Width = 265
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsMotorista
              TabOrder = 10
              dmkEditCB = EdMotorista
              QryCampo = 'Motorista'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdSrcNivel2: TdmkEdit
              Left = 12
              Top = 112
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdDstNivel2: TdmkEdit
              Left = 96
              Top = 112
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdObserv: TdmkEdit
              Left = 180
              Top = 112
              Width = 621
              Height = 21
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Marca'
              UpdCampo = 'Marca'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdFornecMO: TdmkEditCB
              Left = 12
              Top = 152
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 14
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFornecMO
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFornecMO: TdmkDBLookupComboBox
              Left = 68
              Top = 152
              Width = 336
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsPrestador
              TabOrder = 15
              dmkEditCB = EdFornecMO
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCliente: TdmkEditCB
              Left = 408
              Top = 152
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 16
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCliente
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliente: TdmkDBLookupComboBox
              Left = 464
              Top = 152
              Width = 336
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsCliente
              TabOrder = 17
              dmkEditCB = EdCliente
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CkSdoPositivo: TdmkCheckBox
              Left = 12
              Top = 176
              Width = 165
              Height = 17
              Caption = 'Somente com saldo positivo.'
              TabOrder = 18
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object RGTabela: TRadioGroup
              Left = 612
              Top = 0
              Width = 65
              Height = 53
              Caption = 'Tabela:'
              ItemIndex = 0
              Items.Strings = (
                'Ativa'
                'Morto')
              TabOrder = 6
            end
            object GroupBox2: TGroupBox
              Left = 176
              Top = 176
              Width = 249
              Height = 61
              Caption = ' Pesquisa avan'#231'ada: '
              TabOrder = 20
              object Panel7: TPanel
                Left = 2
                Top = 15
                Width = 245
                Height = 44
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object EdQtde: TdmkEdit
                  Left = 9
                  Top = 20
                  Width = 92
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  QryCampo = 'Qtde'
                  UpdCampo = 'Qtde'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object CkQtde: TCheckBox
                  Left = 8
                  Top = 0
                  Width = 89
                  Height = 17
                  Caption = 'Quantidade:'
                  TabOrder = 1
                end
              end
            end
            object EdNFeNum: TdmkEdit
              Left = 488
              Top = 212
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 23
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'NFeNum'
              UpdCampo = 'NFeNum'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkNFeSer: TCheckBox
              Left = 436
              Top = 192
              Width = 89
              Height = 17
              Caption = 'S'#233'rie:'
              TabOrder = 21
            end
            object EdNFeSer: TdmkEdit
              Left = 437
              Top = 212
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 22
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'NFeSer'
              UpdCampo = 'NFeSer'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object DBGMovimNiv: TdmkDBGridZTO
            Left = 808
            Top = 0
            Width = 196
            Height = 242
            Align = alClient
            DataSource = DsMovimNiv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'MovimNiv'
                Title.Caption = 'Cod'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_MovimNiv'
                Title.Caption = 'Descri'#231#227'o do n'#237'vel'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 562
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 606
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Localiza'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtDefIMEI: TBitBtn
        Tag = 104
        Left = 132
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Copiar IMEI'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtDefIMEIClick
      end
    end
  end
  object DsTXSerTal: TDataSource
    DataSet = QrTXSerTal
    Left = 24
    Top = 460
  end
  object QrTXSerTal: TmySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 412
    object QrTXSerTalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXSerTalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    Left = 80
    Top = 412
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrPesqTalao: TIntegerField
      FieldName = 'Talao'
    end
    object QrPesqMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrPesqSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrPesqNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrPesqNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPesqNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrPesqMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrPesqCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrPesqCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrPesqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 80
    Top = 460
  end
  object QrFornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 156
    Top = 412
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 156
    Top = 460
  end
  object QrMotorista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 232
    Top = 412
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 232
    Top = 460
  end
  object QrPrestador: TmySQLQuery
    Database = Dmod.MyDB
    Left = 356
    Top = 412
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 356
    Top = 460
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 296
    Top = 412
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 296
    Top = 460
  end
  object QrMovimNiv: TmySQLQuery
    Database = Dmod.MyDB
    Left = 428
    Top = 412
    object QrMovimNivNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 60
    end
    object QrMovimNivMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
    end
  end
  object DsMovimNiv: TDataSource
    DataSet = QrMovimNiv
    Left = 428
    Top = 460
  end
end
