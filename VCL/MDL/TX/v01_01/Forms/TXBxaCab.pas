unit TXBxaCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker, AppListas,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, UnProjGroup_Consts, frxClass,
  frxDBSet, UnGrl_Consts, UnAppEnums;

type
  TFmTXBxaCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrTXBxaCab: TmySQLQuery;
    DsTXBxaCab: TDataSource;
    QrTXBxaIts: TmySQLQuery;
    DsTXBxaIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtBaixa: TdmkEditDateTimePicker;
    EdDtBaixa: TdmkEdit;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrTXBxaCabCodigo: TIntegerField;
    QrTXBxaCabMovimCod: TIntegerField;
    QrTXBxaCabEmpresa: TIntegerField;
    QrTXBxaCabDtBaixa: TDateTimeField;
    QrTXBxaCabQtde: TFloatField;
    QrTXBxaCabLk: TIntegerField;
    QrTXBxaCabDataCad: TDateField;
    QrTXBxaCabDataAlt: TDateField;
    QrTXBxaCabUserCad: TIntegerField;
    QrTXBxaCabUserAlt: TIntegerField;
    QrTXBxaCabAlterWeb: TSmallintField;
    QrTXBxaCabAtivo: TSmallintField;
    QrTXBxaCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    EdNome: TdmkEdit;
    Label3: TLabel;
    QrTXBxaCabNome: TWideStringField;
    DBEdit4: TDBEdit;
    N1: TMenuItem;
    AdicionaIMEIscompletos1: TMenuItem;
    QrTXBxaCabTemIMEIMrt: TIntegerField;
    QrTXBxaItsCodigo: TLargeintField;
    QrTXBxaItsControle: TLargeintField;
    QrTXBxaItsMovimCod: TLargeintField;
    QrTXBxaItsMovimNiv: TLargeintField;
    QrTXBxaItsMovimTwn: TLargeintField;
    QrTXBxaItsEmpresa: TLargeintField;
    QrTXBxaItsTerceiro: TLargeintField;
    QrTXBxaItsCliVenda: TLargeintField;
    QrTXBxaItsMovimID: TLargeintField;
    QrTXBxaItsDataHora: TDateTimeField;
    QrTXBxaItsPallet: TLargeintField;
    QrTXBxaItsGraGruX: TLargeintField;
    QrTXBxaItsQtde: TFloatField;
    QrTXBxaItsValorT: TFloatField;
    QrTXBxaItsSrcMovID: TLargeintField;
    QrTXBxaItsSrcNivel1: TLargeintField;
    QrTXBxaItsSrcNivel2: TLargeintField;
    QrTXBxaItsSrcGGX: TLargeintField;
    QrTXBxaItsSdoVrtQtd: TFloatField;
    QrTXBxaItsObserv: TWideStringField;
    QrTXBxaItsFornecMO: TLargeintField;
    QrTXBxaItsCustoMOKg: TFloatField;
    QrTXBxaItsCustoMOTot: TFloatField;
    QrTXBxaItsValorMP: TFloatField;
    QrTXBxaItsDstMovID: TLargeintField;
    QrTXBxaItsDstNivel1: TLargeintField;
    QrTXBxaItsDstNivel2: TLargeintField;
    QrTXBxaItsDstGGX: TLargeintField;
    QrTXBxaItsQtdGer: TFloatField;
    QrTXBxaItsQtdAnt: TFloatField;
    QrTXBxaItsNO_PALLET: TWideStringField;
    QrTXBxaItsNO_PRD_TAM_COR: TWideStringField;
    QrTXBxaItsNO_TTW: TWideStringField;
    QrTXBxaItsID_TTW: TLargeintField;
    QrOrigem: TmySQLQuery;
    QrOrigemNO_MovimID: TWideStringField;
    QrOrigemNO_CEN_LOC: TWideStringField;
    QrOrigemNO_FMO: TWideStringField;
    QrOrigemNO_CLI: TWideStringField;
    QrOrigemNO_PRD_TAM_COR: TWideStringField;
    QrOrigemGraGruX: TIntegerField;
    QrOrigemQtde: TFloatField;
    QrOrigemValorT: TFloatField;
    frxDsOrigem: TfrxDBDataset;
    frxWET_CURTI_027_1: TfrxReport;
    frxDsTXBxaCab: TfrxDBDataset;
    QrOrigemStqCenLoc: TIntegerField;
    QrOrigemDataHora: TDateTimeField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXBxaCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXBxaCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXBxaCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTXBxaCabBeforeClose(DataSet: TDataSet);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure AdicionaIMEIscompletos1Click(Sender: TObject);
    procedure frxWET_CURTI_027_1GetValue(const VarName: string;
      var Value: Variant);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormTXBxaIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTXBxaIts(Controle: Integer);

  end;

var
  FmTXBxaCab: TFmTXBxaCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
TXBxaIts, UnTX_PF, TXBxaItsIMEIs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXBxaCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXBxaCab.MostraFormTXBxaIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTXBxaIts, FmTXBxaIts, afmoNegarComAviso) then
  begin
    FmTXBxaIts.ImgTipo.SQLType := SQLType;
    FmTXBxaIts.FQrCab    := QrTXBxaCab;
    FmTXBxaIts.FDsCab    := DsTXBxaCab;
    FmTXBxaIts.FQrIts    := QrTXBxaIts;
    FmTXBxaIts.FDataHora := QrTXBxaCabDtBaixa.Value;
    FmTXBxaIts.FEmpresa  := QrTXBxaCabEmpresa.Value;
    if SQLType = stIns then
    begin
      //FmTXBxaIts.EdCPF1.ReadOnly := False
{
    end else
    begin
      FmTXBxaIts.FDataHora := QrTXBxaCabDtVenda.Value;
      FmTXBxaIts.EdControle.ValueVariant := QrTXBxaItsControle.Value;
      //
      FmTXBxaIts.EdGragruX.ValueVariant  := QrTXBxaItsGraGruX.Value;
      FmTXBxaIts.CBGragruX.KeyValue      := QrTXBxaItsGraGruX.Value;
      FmTXBxaIts.EdQtde.ValueVariant     := -QrTXBxaItsQtde.Value;
      //FmTXBxaIts.EdValorT.ValueVariant   := -QrTXBxaItsValorT.Value;
      FmTXBxaIts.EdPallet.ValueVariant   := QrTXBxaItsPallet.Value;
      //
    end;
}
    FmTXBxaIts.ShowModal;
    FmTXBxaIts.Destroy;
    end;
  end;
end;

procedure TFmTXBxaCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXBxaCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTXBxaCab, QrTXBxaIts);
end;

procedure TFmTXBxaCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTXBxaCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTXBxaIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTXBxaIts);
  //
  TX_PF.HabilitaComposTXAtivo(QrTXBxaItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmTXBxaCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXBxaCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXBxaCab.DefParams;
begin
  VAR_GOTOTABELA := 'txbxacab';
  VAR_GOTOMYSQLTABLE := QrTXBxaCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM txbxacab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTXBxaCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTXBxaCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXBxaCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXBxaCab.ItsAltera1Click(Sender: TObject);
begin
// Nao fazer!
end;

procedure TFmTXBxaCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrTXBxaItsCodigo.Value;
  MovimCod := QrTXBxaItsMovimCod.Value;
  //
  if TX_PF.ExcluiControleTXMovIts(QrTXBxaIts, TIntegerField(QrTXBxaItsControle),
  QrTXBxaItsControle.Value, CtrlBaix, QrTXBxaItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti027)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txbxacab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXBxaCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormTXBxaIts(stIns);
end;

procedure TFmTXBxaCab.ReopenTXBxaIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXBxaCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXBxaCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXBxaIts, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrTXBxaIts.Locate('Controle', Controle, []);
end;


procedure TFmTXBxaCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXBxaCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXBxaCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXBxaCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXBxaCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXBxaCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXBxaCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXBxaCabCodigo.Value;
  Close;
end;

procedure TFmTXBxaCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXBxaCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txbxacab');
  Empresa := DModG.ObtemFilialDeEntidade(QrTXBxaCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmTXBxaCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtBaixa, DataHora: String;
  Codigo, MovimCod, Empresa: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtBaixa        := Geral.FDT_TP_Ed(TPDtBaixa.Date, EdDtBaixa.Text);
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Nome = '', EdNome, 'Defina o motivo da baixa!') then Exit;
  if MyObjects.FIC(TPDtBaixa.DateTime < 2, TPDtBaixa,
    'Defina uma data de baixa!') then Exit;

  Codigo := UMyMod.BPGS1I32('txbxacab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'txbxacab', False, [
  'Nome', 'MovimCod', 'Empresa',
  'DtBaixa'], [
  'Codigo'], [
  Nome, MovimCod, Empresa,
  DtBaixa], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      TX_PF.InsereTXMovCab(MovimCod, emidForcado, Codigo)
    else
    begin
      DataHora := DtBaixa;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Empresa', CO_DATA_HORA_TMI], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTXBxaCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'txbxacab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txbxacab', 'Codigo');
end;

procedure TFmTXBxaCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTXBxaCab.AdicionaIMEIscompletos1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmTXBxaItsIMEIs, FmTXBxaItsIMEIs, afmoNegarComAviso) then
  begin
    FmTXBxaItsIMEIs.ImgTipo.SQLType := stIns;
    //
    FmTXBxaItsIMEIs.FCodigo   := QrTXBxaCabCodigo.Value;
    FmTXBxaItsIMEIs.FMovimCod := QrTXBxaCabMovimCod.Value;
    FmTXBxaItsIMEIs.FEmpresa  := QrTXBxaCabEmpresa.Value;
    //FmTXBxaItsIMEIs.FClientMO := QrTXBxaCabClientMO.Value;
    //
    FmTXBxaItsIMEIs.ShowModal;
    Controle := FmTXBxaItsIMEIs.FControle;
    FmTXBxaItsIMEIs.Destroy;
    ReopenTXBxaIts(Controle);
  end;
end;

procedure TFmTXBxaCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXBxaCab, QrTXBxaCabDtBaixa.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXBxaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmTXBxaCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXBxaCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXBxaCab.SbImprimeClick(Sender: TObject);
var
  SQL_Periodo, ATT_MovimID, ATT_MovimNiv: String;
begin
  //
  ATT_MovimID := dmkPF.ArrayToTexto('vmd.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmd.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  UnDMkDAC_PF.AbreMySQLQuery0(QrOrigem, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _BXA_ITENS_ORIG_O_;',
  'CREATE TABLE _BXA_ITENS_ORIG_O_',
  'SELECT * FROM ' + TMeuDB + '.' + CO_SEL_TAB_TMI + '',
  'WHERE MovimCod=' + Geral.FF0(QrTXBxaCabMovimCod.Value),
  ';',
  'DROP TABLE IF EXISTS _BXA_ITENS_ORIG_X_;',
  'CREATE TABLE _BXA_ITENS_ORIG_X_',
  'SELECT SrcNivel2 ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_TMI + '',
  'WHERE MovimCod=' + Geral.FF0(QrTXBxaCabMovimCod.Value),
  ';',
  'DROP TABLE IF EXISTS _BXA_ITENS_ORIG_D_;',
  'CREATE TABLE _BXA_ITENS_ORIG_D_',
  'SELECT tmi.* ',
  'FROM _BXA_ITENS_ORIG_X_  vmx ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_TMI + ' tmi ON tmi.Controle=vmx.SrcNivel2',
  ';',
  'SELECT ' + ATT_MovimID,
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_CEN_LOC, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FMO, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
  'CONCAT(gg1.Nome,   ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  '  NO_PRD_TAM_COR, vmd.GraGruX, vmd.StqCenLoc, vmd.DataHora, ',
  '-vmo.Qtde Qtde, -vmo.ValorT ValorT',
  '',
  'FROM _BXA_ITENS_ORIG_D_ vmd',
  'LEFT JOIN _BXA_ITENS_ORIG_O_ vmo ON vmo.SrcNivel2=vmd.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmd.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.entidades  cli ON cli.Codigo=vmd.ClientMO ',
  'LEFT JOIN ' + TMeuDB + '.entidades  fmo ON fmo.Codigo=vmd.FornecMO ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmd.StqCenLoc  ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo  ',
  ' ',
  'ORDER BY vmd.StqcenLoc, vmd.DataHora',
  '']);
  //Geral.MB_SQL(Self, QrOrigem);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_027_1, [
  DModG.frxDsDono,
  frxDsOrigem,
  frxDsTXBxaCab
  ]);
  MyObjects.frxMostra(frxWET_CURTI_027_1, 'Itens de Baixa for�ada');
end;

procedure TFmTXBxaCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXBxaCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrTXBxaCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXBxaCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXBxaCab.QrTXBxaCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //
end;

procedure TFmTXBxaCab.QrTXBxaCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTXBxaIts(0);
end;

procedure TFmTXBxaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXBxaCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXBxaCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrTXBxaCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'txbxacab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXBxaCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXBxaCab.frxWET_CURTI_027_1GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
end;

procedure TFmTXBxaCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXBxaCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txbxacab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDtBaixa.Date := DModG.ObtemAgora();
  EdDtBaixa.ValueVariant := DModG.ObtemAgora();
  if EdEmpresa.ValueVariant > 0 then
    TPDtBaixa.SetFocus;
end;

procedure TFmTXBxaCab.QrTXBxaCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXBxaIts.Close;
end;

procedure TFmTXBxaCab.QrTXBxaCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXBxaCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

