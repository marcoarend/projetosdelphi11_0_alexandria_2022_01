unit TXInnCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  dmkDBGridZTO, UnProjGroup_Consts, AppListas, UnGrl_Consts, UnComps_Vars,
  UnGrl_Geral, UnAppEnums, UnTX_Jan;

type
  TFmTXInnCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrTXInnCab: TmySQLQuery;
    DsTXInnCab: TDataSource;
    QrTXInnIts: TmySQLQuery;
    DsTXInnIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtCompra: TdmkEditDateTimePicker;
    EdDtCompra: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    QrTXItsBxa: TmySQLQuery;
    DsTXItsBxa: TDataSource;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    SbFornecedor: TSpeedButton;
    SbTransportador: TSpeedButton;
    Atualizaestoque1: TMenuItem;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrProcednc: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProcednc: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    SbClienteMO: TSpeedButton;
    Label21: TLabel;
    EdProcednc: TdmkEditCB;
    CBProcednc: TdmkDBLookupComboBox;
    SbProcedenc: TSpeedButton;
    Label22: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    PMVInnIts: TPopupMenu;
    PB1: TProgressBar;
    Panel7: TPanel;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    QrTXItsBxaNO_PALLET: TWideStringField;
    QrTXItsBxaNO_PRD_TAM_COR: TWideStringField;
    QrTXItsBxaNO_TTW: TWideStringField;
    QrTXItsBxaID_TTW: TLargeintField;
    Label28: TLabel;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Label29: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Edemi_serie: TdmkEdit;
    Label30: TLabel;
    Label31: TLabel;
    Edemi_nNF: TdmkEdit;
    Label34: TLabel;
    Label35: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    N3: TMenuItem;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label36: TLabel;
    Label37: TLabel;
    ProgressBar1: TProgressBar;
    DBEdit27: TDBEdit;
    Label38: TLabel;
    QrTXInnCabCodigo: TIntegerField;
    QrTXInnCabMovimCod: TIntegerField;
    QrTXInnCabEmpresa: TIntegerField;
    QrTXInnCabDtCompra: TDateTimeField;
    QrTXInnCabDtViagem: TDateTimeField;
    QrTXInnCabDtEntrada: TDateTimeField;
    QrTXInnCabFornecedor: TIntegerField;
    QrTXInnCabTransporta: TIntegerField;
    QrTXInnCabQtde: TFloatField;
    QrTXInnCabValorT: TFloatField;
    QrTXInnCabClienteMO: TIntegerField;
    QrTXInnCabProcednc: TIntegerField;
    QrTXInnCabMotorista: TIntegerField;
    QrTXInnCabPlaca: TWideStringField;
    QrTXInnCabTemIMEIMrt: TSmallintField;
    QrTXInnCabide_serie: TIntegerField;
    QrTXInnCabide_nNF: TIntegerField;
    QrTXInnCabemi_serie: TIntegerField;
    QrTXInnCabemi_nNF: TIntegerField;
    QrTXInnCabNFeStatus: TIntegerField;
    QrTXInnCabTXVmcWrn: TSmallintField;
    QrTXInnCabTXVmcObs: TWideStringField;
    QrTXInnCabTXVmcSeq: TWideStringField;
    QrTXInnCabTXVmcSta: TSmallintField;
    QrTXInnCabDtEnceRend: TDateTimeField;
    QrTXInnCabTpEnceRend: TSmallintField;
    QrTXInnCabLk: TIntegerField;
    QrTXInnCabDataCad: TDateField;
    QrTXInnCabDataAlt: TDateField;
    QrTXInnCabUserCad: TIntegerField;
    QrTXInnCabUserAlt: TIntegerField;
    QrTXInnCabAlterWeb: TSmallintField;
    QrTXInnCabAWServerID: TIntegerField;
    QrTXInnCabAWStatSinc: TSmallintField;
    QrTXInnCabAtivo: TSmallintField;
    QrTXInnCabDtEnceRend_TXT: TWideStringField;
    QrTXInnCabNO_EMPRESA: TWideStringField;
    QrTXInnCabNO_FORNECE: TWideStringField;
    QrTXInnCabNO_TRANSPORTA: TWideStringField;
    QrTXInnCabNO_CLIENTEMO: TWideStringField;
    QrTXInnCabNO_PROCEDNC: TWideStringField;
    QrTXInnCabNO_MOTORISTA: TWideStringField;
    EdValorT: TdmkEdit;
    Label17: TLabel;
    QrTXInnItsNO_TTW: TWideStringField;
    QrTXInnItsID_TTW: TLargeintField;
    QrTXInnItsCodigo: TLargeintField;
    QrTXInnItsControle: TLargeintField;
    QrTXInnItsMovimCod: TLargeintField;
    QrTXInnItsMovimNiv: TLargeintField;
    QrTXInnItsMovimTwn: TLargeintField;
    QrTXInnItsEmpresa: TLargeintField;
    QrTXInnItsClientMO: TLargeintField;
    QrTXInnItsTerceiro: TLargeintField;
    QrTXInnItsCliVenda: TLargeintField;
    QrTXInnItsMovimID: TLargeintField;
    QrTXInnItsDataHora: TDateTimeField;
    QrTXInnItsPallet: TLargeintField;
    QrTXInnItsGraGruX: TLargeintField;
    QrTXInnItsQtde: TFloatField;
    QrTXInnItsValorT: TFloatField;
    QrTXInnItsSrcMovID: TLargeintField;
    QrTXInnItsSrcNivel1: TLargeintField;
    QrTXInnItsSrcNivel2: TLargeintField;
    QrTXInnItsSrcGGX: TLargeintField;
    QrTXInnItsSdoVrtQtd: TFloatField;
    QrTXInnItsObserv: TWideStringField;
    QrTXInnItsFornecMO: TLargeintField;
    QrTXInnItsCustoMOTot: TFloatField;
    QrTXInnItsValorMP: TFloatField;
    QrTXInnItsCustoPQ: TFloatField;
    QrTXInnItsDstMovID: TLargeintField;
    QrTXInnItsDstNivel1: TLargeintField;
    QrTXInnItsDstNivel2: TLargeintField;
    QrTXInnItsDstGGX: TLargeintField;
    QrTXInnItsQtdGer: TFloatField;
    QrTXInnItsQtdAnt: TFloatField;
    QrTXInnItsMarca: TWideStringField;
    QrTXInnItsPedItsLib: TLargeintField;
    QrTXInnItsPedItsFin: TLargeintField;
    QrTXInnItsPedItsVda: TLargeintField;
    QrTXInnItsReqMovEstq: TLargeintField;
    QrTXInnItsStqCenLoc: TLargeintField;
    QrTXInnItsItemNFe: TLargeintField;
    QrTXInnItsTXMulFrnCab: TLargeintField;
    QrTXInnItsTXMulNFeCab: TLargeintField;
    QrTXInnItsDtCorrApo: TDateTimeField;
    QrTXInnItsIxxMovIX: TLargeintField;
    QrTXInnItsIxxFolha: TLargeintField;
    QrTXInnItsIxxLinha: TLargeintField;
    QrTXInnItsNO_PRD_TAM_COR: TWideStringField;
    QrTXInnItsNO_Pallet: TWideStringField;
    QrTXInnItsSIGLAUNIDMED: TWideStringField;
    QrTXInnItsNOMEUNIDMED: TWideStringField;
    QrTXItsBxaCodigo: TLargeintField;
    QrTXItsBxaControle: TLargeintField;
    QrTXItsBxaMovimCod: TLargeintField;
    QrTXItsBxaMovimNiv: TLargeintField;
    QrTXItsBxaMovimTwn: TLargeintField;
    QrTXItsBxaEmpresa: TLargeintField;
    QrTXItsBxaClientMO: TLargeintField;
    QrTXItsBxaTerceiro: TLargeintField;
    QrTXItsBxaCliVenda: TLargeintField;
    QrTXItsBxaMovimID: TLargeintField;
    QrTXItsBxaDataHora: TDateTimeField;
    QrTXItsBxaPallet: TLargeintField;
    QrTXItsBxaGraGruX: TLargeintField;
    QrTXItsBxaQtde: TFloatField;
    QrTXItsBxaValorT: TFloatField;
    QrTXItsBxaSrcMovID: TLargeintField;
    QrTXItsBxaSrcNivel1: TLargeintField;
    QrTXItsBxaSrcNivel2: TLargeintField;
    QrTXItsBxaSrcGGX: TLargeintField;
    QrTXItsBxaSdoVrtQtd: TFloatField;
    QrTXItsBxaObserv: TWideStringField;
    QrTXItsBxaFornecMO: TLargeintField;
    QrTXItsBxaCustoMOTot: TFloatField;
    QrTXItsBxaValorMP: TFloatField;
    QrTXItsBxaCustoPQ: TFloatField;
    QrTXItsBxaDstMovID: TLargeintField;
    QrTXItsBxaDstNivel1: TLargeintField;
    QrTXItsBxaDstNivel2: TLargeintField;
    QrTXItsBxaDstGGX: TLargeintField;
    QrTXItsBxaQtdGer: TFloatField;
    QrTXItsBxaQtdAnt: TFloatField;
    QrTXItsBxaMarca: TWideStringField;
    QrTXItsBxaPedItsLib: TLargeintField;
    QrTXItsBxaPedItsFin: TLargeintField;
    QrTXItsBxaPedItsVda: TLargeintField;
    QrTXItsBxaReqMovEstq: TLargeintField;
    QrTXItsBxaStqCenLoc: TLargeintField;
    QrTXItsBxaItemNFe: TLargeintField;
    QrTXItsBxaTXMulFrnCab: TLargeintField;
    QrTXItsBxaTXMulNFeCab: TLargeintField;
    QrTXItsBxaDtCorrApo: TDateTimeField;
    QrTXItsBxaIxxMovIX: TLargeintField;
    QrTXItsBxaIxxFolha: TLargeintField;
    QrTXItsBxaIxxLinha: TLargeintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXInnCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXInnCabBeforeOpen(DataSet: TDataSet);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXInnCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTXInnCabBeforeClose(DataSet: TDataSet);
    procedure QrTXInnItsBeforeClose(DataSet: TDataSet);
    procedure QrTXInnItsAfterScroll(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
    procedure SbTransportadorClick(Sender: TObject);
    procedure Atualizaestoque1Click(Sender: TObject);
    procedure SbClienteMOClick(Sender: TObject);
    procedure SbProcedencClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure Edide_nNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edemi_nNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Encerrarendimento1Click(Sender: TObject);
  private
    { Private declarations }
    FMPAG_Artigos: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormTXInnIts(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenTribIncIts(Controle: Integer);
    //Em desenvolvimento procedure ImportaDadosNFeCab(EdnNF: TDmkEdit);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaSaldoIts(Controle: Integer);
    procedure AtualizaNFeItens();
    procedure ReopenTXInnIts(Controle: Integer);
    procedure ReopenTXItsBxa(Controle: Integer);

  end;

var
  FmTXInnCab: TFmTXInnCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnTX_PF,
  Principal, TXInnIts, ModTX_CRC;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXInnCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXInnCab.MostraFormTXInnIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTXInnIts, FmTXInnIts, afmoNegarComAviso) then
  begin
    FmTXInnIts.ImgTipo.SQLType := SQLType;
    FmTXInnIts.FQrCab          := QrTXInnCab;
    FmTXInnIts.FDsCab          := DsTXInnCab;
    FmTXInnIts.FQrIts          := QrTXInnIts;
    FmTXInnIts.FEmpresa        := QrTXInnCabEmpresa.Value;
    FmTXInnIts.FFornece        := QrTXInnCabFornecedor.Value;
    if SQLType = stIns then
    begin
      FmTXInnIts.FDataHora       := QrTXInnCabDtEntrada.Value;
      //
      if QrTXInnCabClienteMO.Value <> 0 then
        FmTXInnIts.FClientMO := QrTXInnCabClienteMO.Value
      else
        FmTXInnIts.FClientMO := QrTXInnCabEmpresa.Value;
    end else
    begin
      FmTXInnIts.EdControle.ValueVariant := QrTXInnItsControle.Value;
      //
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXInnItsDataHora.Value,
        QrTXInnItsDtCorrApo.Value,  FmTXInnIts.FDataHora);
      //
      FmTXInnIts.FClientMO               := QrTXInnItsClientMO.Value;
      FmTXInnIts.EdGragruX.ValueVariant  := QrTXInnItsGraGruX.Value;
      FmTXInnIts.CBGragruX.KeyValue      := QrTXInnItsGraGruX.Value;
      FmTXInnIts.EdQtde.ValueVariant     := QrTXInnItsQtde.Value;
      FmTXInnIts.EdValorMP.ValueVariant  := QrTXInnItsValorMP.Value;
      FmTXInnIts.EdObserv.ValueVariant   := QrTXInnItsObserv.Value;
      FmTXInnIts.EdMarca.ValueVariant    := QrTXInnItsMarca.Value;
      //
      if QrTXInnItsSdoVrtQtd.Value < QrTXInnItsQtde.Value then
      begin
        FmTXInnIts.EdGraGruX.Enabled  := False;
        FmTXInnIts.CBGraGruX.Enabled  := False;
      end;
      //
      FmTXInnIts.EdStqCenLoc.ValueVariant   := QrTXInnItsStqCenLoc.Value;
      FmTXInnIts.EdReqMovEstq.ValueVariant  := QrTXInnItsReqMovEstq.Value;
    end;
    FmTXInnIts.ShowModal;
    FmTXInnIts.Destroy;
  end;
  LocCod(QrTXInnCabCodigo.Value, QrTXInnCabCodigo.Value);
end;

procedure TFmTXInnCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXInnCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTXInnCab, QrTXInnIts);
end;

procedure TFmTXInnCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTXInnCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTXInnIts);
  MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrTXInnIts, QrTXItsBxa);
  //
  TX_PF.HabilitaComposTXAtivo(QrTXInnItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
  //
end;

procedure TFmTXInnCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXInnCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXInnCab.DefParams;
begin
  VAR_GOTOTABELA := 'TXInnCab';
  VAR_GOTOMYSQLTABLE := QrTXInnCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(wic.DtEnceRend < "1900-01-01", "", DATE_FORMAT(wic.DtEnceRend, "%d/%m/%y %h:%i")) DtEnceRend_TXT,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,');
  VAR_SQLx.Add('IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,');
  VAR_SQLx.Add('IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA');
  VAR_SQLx.Add('FROM txinncab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc');
  VAR_SQLx.Add('LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTXInnCab.Edemi_nNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    //ImportaDadosNFeCab(Edemi_nNF);
end;

procedure TFmTXInnCab.Edide_nNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    //ImportaDadosNFeCab(Edide_nNF);
end;

procedure TFmTXInnCab.Encerrarendimento1Click(Sender: TObject);
begin
end;

(*Em desenvolvimento
procedure TFmTXInnCab.ImportaDadosNFeCab(EdnNF: TDmkEdit);
var
  Continua: Boolean;
  Empresa, NF: Integer;
begin
  Empresa  := EdEmpresa.ValueVariant;
  NF       := EdnNF.ValueVariant;
  Continua := True;
  //
  if NF <> 0 then
  begin
    if EdnNF = Edide_nNF then
      Continua := Geral.MB_Pergunta('A NF-e de entrada j� foi definida!' +
                    sLineBreak + 'Realmente deseja alter�-la?') = ID_YES
    else if EdnNF = Edemi_nNF then
      Continua := Geral.MB_Pergunta('A NF-e fornecedor j� foi definida!' +
                    sLineBreak + 'Realmente deseja alter�-la?') = ID_YES
    else
      Continua := False;
  end;
  //
  if Continua then
  begin
    if EdnNF = Edide_nNF then
      TX_PF.ImportaDadosNFeCab(0, nil, nil, nil, nil, nil,
        nil, TPDtEntrada, EdDtEntrada, Edide_serie, Edide_nNF)
    else if EdnNF = Edemi_nNF then
      TX_PF.ImportaDadosNFeCab(1, EdFornecedor, CBFornecedor, EdClienteMO,
        CBClienteMO, TPDtCompra, EdDtCompra, TPDtViagem, EdDtViagem,
        Edemi_serie, Edemi_nNF);
    //
    EdEmpresa.ValueVariant := Empresa;
    CBEmpresa.KeyValue     := Empresa;
  end;
end;
*)

procedure TFmTXInnCab.Estoque1Click(Sender: TObject);
begin
{POIU
  TX_PF.MostraFormTXMovImp(False, nil, nil, -1);
}
end;

procedure TFmTXInnCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormTXInnIts(stUpd);
end;

procedure TFmTXInnCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTXInnCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXInnCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXInnCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod, Controle: Integer;
begin
  Codigo   := QrTXInnItsCodigo.Value;
  MovimCod := QrTXInnItsMovimCod.Value;
  Controle := QrTXInnItsControle.Value;
  //
  if TX_PF.ExcluiControleTXMovIts(QrTXInnIts, TIntegerField(QrTXInnItsControle),
  Controle, CtrlBaix, QrTXInnItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti006)) then
  begin
    // Atualiza!
    TX_PF.AtualizaTotaisTXXxxCab('TXInnCab', MovimCod);
    //
    FmTXInnCab.LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXInnCab.ReopenTribIncIts(Controle: Integer);
begin
end;

procedure TFmTXInnCab.ReopenTXInnIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXInnCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'txp.Nome NO_Pallet, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXInnCabMovimCod.Value),
  '']);
  SQL_Group := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXInnIts, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrTXInnIts);
  //
  QrTXInnIts.Locate('Controle', Controle, []);
end;


procedure TFmTXInnCab.ReopenTXItsBxa(Controle: Integer);
const
  SrcMovID = TEstqMovimID.emidCompra;
var
  SrcNivel1, SrcNivel2, LocCtrl, TemIMEIMrt: Integer;
begin
  SrcNivel1  := QrTXInnItsCodigo.Value;
  SrcNivel2  := QrTXInnItsControle.Value;
  TemIMEIMrt := QrTXInnCabTemIMEIMrt.Value;
  //
  TX_PF.ReopenTXItsBxa(
    QrTXItsBxa, SrcMovID, SrcNivel1, SrcNivel2, TemIMEIMrt, [], Controle);
end;

procedure TFmTXInnCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXInnCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXInnCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXInnCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXInnCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXInnCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXInnCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXInnCabCodigo.Value;
  Close;
end;

procedure TFmTXInnCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormTXInnIts(stIns);
end;

procedure TFmTXInnCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXInnCab, [PnDados],
  [PnEdita], TPDtCompra, ImgTipo, 'TXInnCab');
  Empresa := DModG.ObtemFilialDeEntidade(QrTXInnCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmTXInnCab.BtConfirmaClick(Sender: TObject);
var
  DtCompra, DtViagem, DtEntrada, DataHora, Placa: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro, ClienteMO,
  Procednc, Motorista, ide_Serie, ide_nNF, emi_Serie, emi_nNF: Integer;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtCompra       := Geral.FDT_TP_Ed(TPDtCompra.Date, EdDtCompra.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrada      := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
  Fornecedor     := EdFornecedor.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ClienteMO      := EdClienteMO.ValueVariant;
  Procednc       := EdProcednc.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  ide_Serie      := Edide_Serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_Serie      := Edemi_Serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
  //
  if not TX_PF.ValidaCampoNF(5, Edide_nNF, True) then Exit;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Fornecedor = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(TPDtCompra.DateTime < 2, TPDtCompra, 'Defina uma data de compra!') then Exit;
  DataChegadaInvalida := (TPDtEntrada.Date < TPDtViagem.Date);
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txinncab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'txinncab', False, [
  'MovimCod', 'Empresa', 'DtCompra',
  'DtViagem', 'DtEntrada', 'Fornecedor',
  'Transporta',
  'ClienteMO', 'Procednc', 'Motorista',
  'Placa', 'ide_Serie', 'ide_nNF',
  'emi_Serie', 'emi_nNF'
  ], [
  'Codigo'], [
  MovimCod, Empresa, DtCompra,
  DtViagem, DtEntrada, Fornecedor,
  Transporta,
  ClienteMO, Procednc, Motorista,
  Placa, ide_Serie, ide_nNF,
  emi_Serie, emi_nNF], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      TX_PF.InsereTXMovCab(MovimCod, emidCompra, Codigo)
    else
    begin
      DataHora := DtEntrada;
      Terceiro := Fornecedor;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Empresa', 'ClientMO', 'Terceiro',
      CO_DATA_HORA_TMI], ['MovimCod'], [
      Empresa, ClienteMO, Terceiro,
      DataHora], [MovimCod], True);
      //
      if Edide_nNF.ValueVariant <> 0 then
        AtualizaNFeItens();
    end;
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if SQLType = stIns then
      MostraFormTXInnIts(stIns)
    else
    begin
      QrTXInnIts.First;
      while not QrTXInnIts.Eof do
      begin
        TX_PF.AtualizaDescendentes(QrTXInnItsControle.Value, ['ClientMO'], [ClienteMO]);
        //
        QrTXInnIts.Next;
      end;
    end;
{POIU N�o vai usar!!! ???
    if QrTXInnCabClienteMO.Value <> 0 then
    begin
      QrTXInnIts.First;
      while not QrTXInnIts.Eof do
      begin
        DmModTX_CRC.AtualizaTXMovIts_CusEmit(QrTXInnItsControle.Value);
        QrTXInnIts.Next;
      end;
    end;
}
  end;
end;

procedure TFmTXInnCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'TXInnCab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TXInnCab', 'Codigo');
end;

procedure TFmTXInnCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTXInnCab.Atualizaestoque1Click(Sender: TObject);
begin
  TX_PF.AtualizaSaldoIMEI(QrTXInnItsControle.Value, True);
end;

procedure TFmTXInnCab.AtualizaNFeItens();
begin
  DmModTX_CRC.AtualizaTXMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXInnCabMovimCod.Value, TEstqMovimID.emidCompra, [(**)], [eminSemNiv]);
end;

procedure TFmTXInnCab.AtualizaSaldoIts(Controle: Integer);
begin
  TX_PF.AtualizaTotaisTXXxxCab('txinncab', QrTXInnCabMovimCod.Value);
  TX_PF.AtualizaSaldoIMEI(Controle, True);
end;

procedure TFmTXInnCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXInnCab, QrTXInnCabDtEntrada.Value,
  BtCab, PMCab, [CabInclui1]);
end;

procedure TFmTXInnCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBIts.Align := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
end;

procedure TFmTXInnCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXInnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXInnCab.SbProcedencClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcednc.Text := IntToStr(VAR_ENTIDADE);
    CBProcednc.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXInnCab.SbClienteMOClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClienteMO.Text := IntToStr(VAR_ENTIDADE);
    CBClienteMO.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXInnCab.SbFornecedorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornecedor.Text := IntToStr(VAR_ENTIDADE);
    CBFornecedor.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXInnCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXInnCab.SbMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXInnCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXInnCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXInnCab.QrTXInnCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXInnCab.QrTXInnCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTXInnIts(0);
end;

procedure TFmTXInnCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXInnCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXInnCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := TX_Jan.MostraFormTXTalPsq(emidCompra, [], stPsq, Codigo, Controle,
  False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenTXInnIts(Controle);
  end;
end;

procedure TFmTXInnCab.SbTransportadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdTransporta.Text := IntToStr(VAR_ENTIDADE);
    CBTransporta.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXInnCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXInnCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmTXInnCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXInnCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'TXInnCab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
  Agora := DModG.ObtemAgora();
  TPDtEntrada.Date := Agora;
  EdDtEntrada.ValueVariant := Agora;
  //EdDtCompra.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
end;

procedure TFmTXInnCab.QrTXInnCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXInnIts.Close;
end;

procedure TFmTXInnCab.QrTXInnCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXInnCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTXInnCab.QrTXInnItsAfterScroll(DataSet: TDataSet);
begin
  ReopenTXItsBxa(0);
end;

procedure TFmTXInnCab.QrTXInnItsBeforeClose(DataSet: TDataSet);
begin
  QrTXItsBxa.Close;
end;

end.

