object FmTXRRMOriPall: TFmTXRRMOriPall
  Left = 339
  Top = 185
  Caption = 
    'TEX-FAXAO-179 :: Origem de Artigo de Reprocesso / Reparo (Pallet' +
    ')'
  ClientHeight = 666
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 527
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 1
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label14: TLabel
      Left = 608
      Top = 20
      Width = 51
      Height = 13
      Caption = 'ID Movim.:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label1: TLabel
      Left = 676
      Top = 20
      Width = 58
      Height = 13
      Caption = 'ID Gera'#231#227'o:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label17: TLabel
      Left = 744
      Top = 20
      Width = 61
      Height = 13
      Caption = 'ID It.Gerado:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label2: TLabel
      Left = 540
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Red. It.Ger.:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdSrcNivel2: TdmkEdit
      Left = 744
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcNivel1: TdmkEdit
      Left = 676
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcMovID: TdmkEdit
      Left = 608
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimCod: TdmkEdit
      Left = 80
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSrcGGX: TdmkEdit
      Left = 540
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 602
        Height = 32
        Caption = 'Origem de Artigo de Reprocesso / Reparo (Pallet)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 602
        Height = 32
        Caption = 'Origem de Artigo de Reprocesso / Reparo (Pallet)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 602
        Height = 32
        Caption = 'Origem de Artigo de Reprocesso / Reparo (Pallet)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 552
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 261
        Height = 16
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 261
        Height = 16
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 596
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAptos: TGroupBox
    Left = 0
    Top = 112
    Width = 1008
    Height = 314
    Align = alClient
    Caption = ' Filtros: '
    TabOrder = 0
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 86
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object LaGraGruX: TLabel
        Left = 224
        Top = 0
        Width = 66
        Height = 13
        Caption = 'Mat'#233'ria-prima:'
        Enabled = False
      end
      object LaTerceiro: TLabel
        Left = 516
        Top = 0
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        Enabled = False
      end
      object LaFicha: TLabel
        Left = 152
        Top = 0
        Width = 56
        Height = 13
        Caption = 'Ficha RMP:'
        Enabled = False
      end
      object Label11: TLabel
        Left = 8
        Top = 0
        Width = 83
        Height = 13
        Caption = 'S'#233'rie Ficha RMP:'
        Enabled = False
      end
      object EdGraGruX: TdmkEditCB
        Left = 224
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 280
        Top = 16
        Width = 233
        Height = 21
        Enabled = False
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 4
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTerceiro: TdmkEditCB
        Left = 516
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 572
        Top = 16
        Width = 153
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 6
        dmkEditCB = EdTerceiro
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFicha: TdmkEdit
        Left = 152
        Top = 16
        Width = 68
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFichaChange
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 880
        Top = 2
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtReabreClick
      end
      object EdSerieFch: TdmkEditCB
        Left = 8
        Top = 16
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieFch'
        UpdCampo = 'SerieFch'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSerieFchChange
        DBLookupComboBox = CBSerieFch
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSerieFch: TdmkDBLookupComboBox
        Left = 48
        Top = 16
        Width = 101
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTXSerTal
        TabOrder = 1
        dmkEditCB = EdSerieFch
        QryCampo = 'SerieFch'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkSemi: TCheckBox
        Left = 728
        Top = 20
        Width = 149
        Height = 17
        Caption = 'Permitir semi acabado.'
        Enabled = False
        TabOrder = 8
        OnClick = CkSemiClick
      end
      object CkPalEspecificos: TCheckBox
        Left = 8
        Top = 40
        Width = 273
        Height = 17
        Caption = 'Pallets Espec'#237'ficos. Ex.: (1-10,13)'
        TabOrder = 9
      end
      object EdPalEspecificos: TdmkEdit
        Left = 8
        Top = 60
        Width = 993
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object DBG04Estq: TdmkDBGridZTO
      Left = 2
      Top = 101
      Width = 1004
      Height = 181
      Align = alClient
      DataSource = DsEstqR4
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnAfterMultiselect = DBG04EstqAfterMultiselect
      OnDblClick = DBG04EstqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'NFeNum'
          Title.Caption = 'NFe'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Title.Caption = 'ID Pallet'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PALLET'
          Title.Caption = 'Pallet'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Title.Caption = 'Ficha RMP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdGGX'
          Title.Caption = 'Ordem'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeca'
          Title.Caption = 'Pe'#231'as'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtArM2'
          Title.Caption = #193'rea m'#178
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtPeso'
          Title.Caption = 'Peso kg'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Nome do terceiro'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MediaM2'
          Title.Caption = 'm'#178' / pe'#231'a'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / hora'
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 2
      Top = 282
      Width = 1004
      Height = 30
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      TabStop = True
      object Label50: TLabel
        Left = 10
        Top = 8
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 160
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Pe'#231'as: '
      end
      object EdReqMovEstq: TdmkEdit
        Left = 55
        Top = 4
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSelQtde: TdmkEdit
        Left = 196
        Top = 4
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdQtdeChange
        OnKeyDown = EdQtdeKeyDown
      end
    end
  end
  object PnGerar: TPanel
    Left = 0
    Top = 426
    Width = 1008
    Height = 60
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    object GBGerar: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 60
      Align = alClient
      Caption = ' Dados do item: '
      TabOrder = 0
      Visible = False
      object Label6: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object LaQtde: TLabel
        Left = 96
        Top = 16
        Width = 79
        Height = 13
        Caption = 'Quantidade: [F4]'
      end
      object Label9: TLabel
        Left = 232
        Top = 16
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 96
        Top = 32
        Width = 133
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdQtdeChange
        OnKeyDown = EdQtdeKeyDown
      end
      object EdObserv: TdmkEdit
        Left = 232
        Top = 32
        Width = 444
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 486
    Width = 1008
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 7
    object Label19: TLabel
      Left = 12
      Top = 0
      Width = 149
      Height = 13
      Caption = 'Material usado para emitir NF-e:'
    end
    object EdGGXRcl: TdmkEditCB
      Left = 12
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GGXRcl'
      UpdCampo = 'GGXRcl'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXRcl
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXRcl: TdmkDBLookupComboBox
      Left = 68
      Top = 16
      Width = 457
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXRcl
      TabOrder = 1
      dmkEditCB = EdGGXRcl
      QryCampo = 'GGXRcl'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 112
    Top = 192
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 112
    Top = 240
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 188
    Top = 192
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 188
    Top = 240
  end
  object QrTXSerTal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 260
    Top = 192
    object QrTXSerTalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXSerTalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXSerTal: TDataSource
    DataSet = QrTXSerTal
    Left = 260
    Top = 240
  end
  object QrEstqR4: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'DELETE FROM _vsmovimp2_ '
      '; '
      'INSERT INTO _vsmovimp2_ '
      'SELECT 1 OrdGrupSeq, '
      'wmi.Codigo, wmi.Controle, wmi.MovimCod, '
      'wmi.MovimNiv, wmi.Empresa, wmi.Terceiro, '
      'wmi.MovimID, wmi.DataHora, wmi.Pallet, '
      'wmi.GraGruX, wmi.Pecas, wmi.PesoKg, '
      'wmi.AreaM2, wmi.AreaP2, wmi.SrcMovID, '
      'wmi.SrcNivel1, wmi.SrcNivel2, '
      'wmi.SdoVrtPeca, wmi.SdoVrtArM2, '
      'wmi.Observ, 0 ValorT, ggx.GraGru1, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_PALLET, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      '0 AcumPecas, 0 AcumPesoKg, 0 AcumAreaM2, '
      '0 AcumAreaP2, 0 AcumValorT, 1 Ativo '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro '
      'WHERE wmi.Empresa=-11'
      'AND wmi.GraGruX=3328'
      'AND wmi.DataHora  BETWEEN "2013-09-07" AND "2013-12-06 23:59:59"'
      '; '
      'SELECT * FROM _vsmovimp2_'
      'ORDER BY OrdGrupSeq, DataHora, Pecas DESC; '
      ' ')
    Left = 44
    Top = 192
    object QrEstqR4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR4Qtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,###,###;-#,###,###,###,###; '
    end
    object QrEstqR4GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR4NO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR4Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR4NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR4Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR4NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR4ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR4Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEstqR4OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrEstqR4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrEstqR4Talao: TIntegerField
      FieldName = 'Talao'
    end
    object QrEstqR4SdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrEstqR4SerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrEstqR4DataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrEstqR4NFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
  end
  object DsEstqR4: TDataSource
    DataSet = QrEstqR4
    Left = 44
    Top = 240
  end
  object QrTXMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pal.Nome NO_Pallet, wmi.*  '
      'FROM vsmovits wmi '
      'LEFT JOIN vspallet pal ON pal.Codigo=wmi.Pallet '
      'WHERE wmi.Empresa=-11 '
      'AND wmi.GraGruX=3328 '
      'AND ( '
      '  wmi.MovimID=1 '
      '  OR  '
      '  wmi.SrcMovID<>0 '
      ') '
      'AND wmi.SdoVrtPeca > 0 '
      'ORDER BY DataHora, Pallet ')
    Left = 332
    Top = 192
    object QrTXMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrTXMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTXMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrTXMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrTXMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrTXMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXMovItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrTXMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrTXMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrTXMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMovItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTXMovItsTalao: TIntegerField
      FieldName = 'Talao'
    end
    object QrTXMovItsSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrTXMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrTXMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrTXMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrTXMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrTXMovItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrTXMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrTXMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrTXMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrTXMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrTXMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrTXMovItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrTXMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrTXMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrTXMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrTXMovItsTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrTXMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object QrGGXRcl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 412
    Top = 192
    object QrGGXRclGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXRclSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXRclCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXRclNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 412
    Top = 240
  end
end
