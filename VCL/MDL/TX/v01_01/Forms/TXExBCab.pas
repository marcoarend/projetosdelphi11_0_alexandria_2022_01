unit TXExBCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, UnProjGroup_Consts, UnAppEnums,
  UnGrl_Consts;

type
  TFmTXExBCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrTXExBCab: TmySQLQuery;
    DsTXExBCab: TDataSource;
    QrTXExBIts: TmySQLQuery;
    DsTXExBIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtBaixa: TdmkEditDateTimePicker;
    EdDtBaixa: TdmkEdit;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TDBGrid;
    EdNome: TdmkEdit;
    Label3: TLabel;
    DBEdit4: TDBEdit;
    N1: TMenuItem;
    AdicionaIMEIscompletos1: TMenuItem;
    Label4: TLabel;
    QrTXMotivBxa: TmySQLQuery;
    DsTXMotivBxa: TDataSource;
    EdMotivBxa: TdmkEditCB;
    CBMotivBxa: TdmkDBLookupComboBox;
    QrTXMotivBxaNome: TWideStringField;
    QrTXMotivBxaCodigo: TIntegerField;
    QrTXExBCabNO_MOTIVBXA: TWideStringField;
    QrTXExBCabCodigo: TIntegerField;
    QrTXExBCabNome: TWideStringField;
    QrTXExBCabMovimCod: TIntegerField;
    QrTXExBCabEmpresa: TIntegerField;
    QrTXExBCabDtBaixa: TDateTimeField;
    QrTXExBCabQtde: TFloatField;
    QrTXExBCabValorT: TFloatField;
    QrTXExBCabMotivBxa: TIntegerField;
    QrTXExBCabLk: TIntegerField;
    QrTXExBCabDataCad: TDateField;
    QrTXExBCabDataAlt: TDateField;
    QrTXExBCabUserCad: TIntegerField;
    QrTXExBCabUserAlt: TIntegerField;
    QrTXExBCabAlterWeb: TSmallintField;
    QrTXExBCabAtivo: TSmallintField;
    QrTXExBCabNO_EMPRESA: TWideStringField;
    Label5: TLabel;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    QrTXExBItsCodigo: TLargeintField;
    QrTXExBItsControle: TLargeintField;
    QrTXExBItsMovimCod: TLargeintField;
    QrTXExBItsMovimNiv: TLargeintField;
    QrTXExBItsMovimTwn: TLargeintField;
    QrTXExBItsEmpresa: TLargeintField;
    QrTXExBItsTerceiro: TLargeintField;
    QrTXExBItsCliVenda: TLargeintField;
    QrTXExBItsMovimID: TLargeintField;
    QrTXExBItsDataHora: TDateTimeField;
    QrTXExBItsPallet: TLargeintField;
    QrTXExBItsGraGruX: TLargeintField;
    QrTXExBItsQtde: TFloatField;
    QrTXExBItsValorT: TFloatField;
    QrTXExBItsSrcMovID: TLargeintField;
    QrTXExBItsSrcNivel1: TLargeintField;
    QrTXExBItsSrcNivel2: TLargeintField;
    QrTXExBItsSrcGGX: TLargeintField;
    QrTXExBItsSdoVrtQtd: TFloatField;
    QrTXExBItsObserv: TWideStringField;
    QrTXExBItsFornecMO: TLargeintField;
    QrTXExBItsCustoMOUni: TFloatField;
    QrTXExBItsCustoMOTot: TFloatField;
    QrTXExBItsValorMP: TFloatField;
    QrTXExBItsDstMovID: TLargeintField;
    QrTXExBItsDstNivel1: TLargeintField;
    QrTXExBItsDstNivel2: TLargeintField;
    QrTXExBItsDstGGX: TLargeintField;
    QrTXExBItsQtdGer: TFloatField;
    QrTXExBItsQtdAntPeca: TFloatField;
    QrTXExBItsNO_PALLET: TWideStringField;
    QrTXExBItsNO_PRD_TAM_COR: TWideStringField;
    QrTXExBItsNO_TTW: TWideStringField;
    QrTXExBItsID_TTW: TLargeintField;
    QrTXExBCabTemIMEIMrt: TSmallintField;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXExBCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXExBCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXExBCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTXExBCabBeforeClose(DataSet: TDataSet);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure AdicionaIMEIscompletos1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormTXExBIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTXExBIts(Controle: Integer);

  end;

var
  FmTXExBCab: TFmTXExBCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
  TXExBIts, UnTX_PF, TXExBItsIMEIs, UnTX_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXExBCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXExBCab.MostraFormTXExBIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTXExBIts, FmTXExBIts, afmoNegarComAviso) then
  begin
    FmTXExBIts.ImgTipo.SQLType := SQLType;
    FmTXExBIts.FQrCab    := QrTXExBCab;
    FmTXExBIts.FDsCab    := DsTXExBCab;
    FmTXExBIts.FQrIts    := QrTXExBIts;
    FmTXExBIts.FDataHora := QrTXExBCabDtBaixa.Value;
    FmTXExBIts.FEmpresa  := QrTXExBCabEmpresa.Value;
    if SQLType = stIns then
    begin
      //FmTXExBIts.EdCPF1.ReadOnly := False
{
    end else
    begin
      FmTXExBIts.FDataHora := QrTXExBCabDtVenda.Value;
      FmTXExBIts.EdControle.ValueVariant := QrTXExBItsControle.Value;
      //
      FmTXExBIts.EdGragruX.ValueVariant  := QrTXExBItsGraGruX.Value;
      FmTXExBIts.CBGragruX.KeyValue      := QrTXExBItsGraGruX.Value;
      FmTXExBIts.EdQtde.ValueVariant    := -QrTXExBItsQtde.Value;
      //FmTXExBIts.EdValorT.ValueVariant   := -QrTXExBItsValorT.Value;
      FmTXExBIts.EdPallet.ValueVariant   := QrTXExBItsPallet.Value;
      //
    end;
}
    FmTXExBIts.ShowModal;
    FmTXExBIts.Destroy;
    end;
  end;
end;

procedure TFmTXExBCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXExBCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTXExBCab, QrTXExBIts);
end;

procedure TFmTXExBCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTXExBCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTXExBIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTXExBIts);
  //
  TX_PF.HabilitaComposTXAtivo(QrTXExBItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmTXExBCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXExBCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXExBCab.DefParams;
begin
  VAR_GOTOTABELA := 'txexbcab';
  VAR_GOTOMYSQLTABLE := QrTXExBCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vmb.Nome NO_MOTIVBXA, wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM txexbcab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN txmotivbxa vmb ON vmb.Codigo=wic.MotivBxa');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTXExBCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTXExBCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXExBCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXExBCab.ItsAltera1Click(Sender: TObject);
begin
// Nao fazer!
end;

procedure TFmTXExBCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrTXExBItsCodigo.Value;
  MovimCod := QrTXExBItsMovimCod.Value;
  //
  if TX_PF.ExcluiControleTXMovIts(QrTXExBIts, TIntegerField(QrTXExBItsControle),
  QrTXExBItsControle.Value, CtrlBaix, QrTXExBItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti078)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txexbcab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXExBCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormTXExBIts(stIns);
end;

procedure TFmTXExBCab.ReopenTXExBIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXExBIts, Dmod.MyDB, [
  'SELECT tmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, txp.Nome NO_Pallet ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=tmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXExBCabMovimCod.Value),
  'ORDER BY tmi.Controle ',
  '']);
  //
  QrTXExBIts.Locate('Controle', Controle, []);
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXExBCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXExBCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXExBIts, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrTXExBIts.Locate('Controle', Controle, []);
end;


procedure TFmTXExBCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXExBCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXExBCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXExBCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXExBCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXExBCab.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXMotivBxa();
  UMyMod.SetaCodigoPesquisado(EdMotivBxa, CBMotivBxa, QrTXMotivBxa, VAR_CADASTRO);
end;

procedure TFmTXExBCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXExBCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXExBCabCodigo.Value;
  Close;
end;

procedure TFmTXExBCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXExBCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txexbcab');
  Empresa := DModG.ObtemFilialDeEntidade(QrTXExBCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmTXExBCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtBaixa, DataHora: String;
  Codigo, MovimCod, Empresa, MotivBxa: Integer;
  Qtde, ValorT: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtBaixa        := Geral.FDT_TP_Ed(TPDtBaixa.Date, EdDtBaixa.Text);
(*
  Qtde           := EdQtde.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  MotivBxa       := EdMotivBxa.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(MotivBxa = 0, EdMotivBxa, 'Defina o motivo da baixa extra!') then Exit;
  if MyObjects.FIC(TPDtBaixa.DateTime < 2, TPDtBaixa,
    'Defina uma data de baixa!') then Exit;

  Codigo := UMyMod.BPGS1I32('txexbcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'txexbcab', False, [
  'Nome', 'MovimCod', 'Empresa',
  'DtBaixa', 'MotivBxa'], [
  'Codigo'], [
  Nome, MovimCod, Empresa,
  DtBaixa, MotivBxa], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      if FSeq = 0 then
        TX_PF.InsereTXMovCab(MovimCod, emidExtraBxa, Codigo);
    end
    else
    begin
(*
      N�o fazer! Data / hora � individual do IME=I!
      DataHora := DtBaixa;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Empresa', CO_DATA_HORA_], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
*)
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if Codigo = QrTXExBCabCodigo.Value then
    begin
      if FSeq = 1 then
      begin
        FSeq := 2; // Inseriu = True
        Close;
      end;
    end;
  end;
end;

procedure TFmTXExBCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'txexbcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txexbcab', 'Codigo');
end;

procedure TFmTXExBCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTXExBCab.AdicionaIMEIscompletos1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmTXExBItsIMEIs, FmTXExBItsIMEIs, afmoNegarComAviso) then
  begin
    FmTXExBItsIMEIs.ImgTipo.SQLType := stIns;
    //
    FmTXExBItsIMEIs.FCodigo   := QrTXExBCabCodigo.Value;
    FmTXExBItsIMEIs.FMovimCod := QrTXExBCabMovimCod.Value;
    FmTXExBItsIMEIs.FEmpresa  := QrTXExBCabEmpresa.Value;
    //
    FmTXExBItsIMEIs.FDataHora := QrTXExBCabDtBaixa.Value;
    FmTXExBItsIMEIs.TPDataSenha.Date := Int(QrTXExBCabDtBaixa.Value);
    FmTXExBItsIMEIs.RGDataHora.Items[1] := 'Do cabe�alho: ' +
      Geral.FDT(QrTXExBCabDtBaixa.Value, 109);
    //
    FmTXExBItsIMEIs.ShowModal;
    Controle := FmTXExBItsIMEIs.FControle;
    FmTXExBItsIMEIs.Destroy;
    //
    ReopenTXExBIts(Controle);
  end;
end;

procedure TFmTXExBCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXExBCab, QrTXExBCabDtBaixa.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXExBCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDMkDAC_PF.AbreQuery(QrTXMotivBxa, Dmod.MyDB);
end;

procedure TFmTXExBCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXExBCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXExBCab.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmTXExBCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXExBCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrTXExBCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXExBCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXExBCab.QrTXExBCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXExBCab.QrTXExBCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTXExBIts(0);
end;

procedure TFmTXExBCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXExBCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXExBCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrTXExBCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'txexbcab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXExBCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXExBCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXExBCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txexbcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  TPDtBaixa.Date := DModG.ObtemAgora();
  EdDtBaixa.ValueVariant := DModG.ObtemAgora();
  if EdEmpresa.ValueVariant > 0 then
    TPDtBaixa.SetFocus;
end;

procedure TFmTXExBCab.QrTXExBCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXExBIts.Close;
end;

procedure TFmTXExBCab.QrTXExBCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXExBCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

