unit TXIndDst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup, Vcl.Menus,
  UnProjGroup_Consts, dmkCheckBox, Vcl.ComCtrls, dmkEditDateTimePicker,
  UnAppEnums, mySQLDirectQuery, UnAppPF, UMySQLDB;

type
  TFmTXIndDst = class(TForm)
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    QrTXPallet: TmySQLQuery;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletNome: TWideStringField;
    QrTXPalletLk: TIntegerField;
    QrTXPalletDataCad: TDateField;
    QrTXPalletDataAlt: TDateField;
    QrTXPalletUserCad: TIntegerField;
    QrTXPalletUserAlt: TIntegerField;
    QrTXPalletAlterWeb: TSmallintField;
    QrTXPalletAtivo: TSmallintField;
    QrTXPalletEmpresa: TIntegerField;
    QrTXPalletNO_EMPRESA: TWideStringField;
    QrTXPalletStatus: TIntegerField;
    QrTXPalletCliStat: TIntegerField;
    QrTXPalletGraGruX: TIntegerField;
    QrTXPalletNO_CLISTAT: TWideStringField;
    QrTXPalletNO_PRD_TAM_COR: TWideStringField;
    QrTXPalletNO_STATUS: TWideStringField;
    DsTXPallet: TDataSource;
    QrTXPedIts: TmySQLQuery;
    QrTXPedItsNO_PRD_TAM_COR: TWideStringField;
    QrTXPedItsControle: TIntegerField;
    DsTXPedIts: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    PMPallet: TPopupMenu;
    Criar1: TMenuItem;
    Gerenciamento1: TMenuItem;
    CkCpermitirCrust: TCheckBox;
    QrTXIndInn: TmySQLQuery;
    QrTXIndInnCusFrtMORet: TFloatField;
    QrTXIndInnCusFrtMOEnv: TFloatField;
    QrTXIndInnCodigo: TLargeintField;
    QrTXIndInnControle: TLargeintField;
    QrTXIndInnMovimCod: TLargeintField;
    QrTXIndInnMovimNiv: TLargeintField;
    QrTXIndInnMovimTwn: TLargeintField;
    QrTXIndInnEmpresa: TLargeintField;
    QrTXIndInnTerceiro: TLargeintField;
    QrTXIndInnCliVenda: TLargeintField;
    QrTXIndInnMovimID: TLargeintField;
    QrTXIndInnDataHora: TDateTimeField;
    QrTXIndInnPallet: TLargeintField;
    QrTXIndInnGraGruX: TLargeintField;
    QrTXIndInnQtde: TFloatField;
    QrTXIndInnValorT: TFloatField;
    QrTXIndInnSrcMovID: TLargeintField;
    QrTXIndInnSrcNivel1: TLargeintField;
    QrTXIndInnSrcNivel2: TLargeintField;
    QrTXIndInnSrcGGX: TLargeintField;
    QrTXIndInnSdoVrtQtd: TFloatField;
    QrTXIndInnObserv: TWideStringField;
    QrTXIndInnFornecMO: TLargeintField;
    QrTXIndInnCustoMOUni: TFloatField;
    QrTXIndInnCustoMOTot: TFloatField;
    QrTXIndInnValorMP: TFloatField;
    QrTXIndInnDstMovID: TLargeintField;
    QrTXIndInnDstNivel1: TLargeintField;
    QrTXIndInnDstNivel2: TLargeintField;
    QrTXIndInnDstGGX: TLargeintField;
    QrTXIndInnQtdGer: TFloatField;
    QrTXIndInnQtdAnt: TFloatField;
    QrTXIndInnNO_PALLET: TWideStringField;
    QrTXIndInnNO_PRD_TAM_COR: TWideStringField;
    QrTXIndInnNO_TTW: TWideStringField;
    QrTXIndInnID_TTW: TLargeintField;
    QrTXIndInnNO_FORNECE: TWideStringField;
    QrTXIndInnReqMovEstq: TLargeintField;
    QrTXIndInnCUSTO_M2: TFloatField;
    QrTXIndInnNO_LOC_CEN: TWideStringField;
    QrTXIndInnMarca: TWideStringField;
    QrTXIndInnPedItsLib: TLargeintField;
    QrTXIndInnStqCenLoc: TLargeintField;
    QrTXIndInnClientMO: TLargeintField;
    QrTXIndInnNO_FORNEC_MO: TWideStringField;
    QrTXIndInnCustoPQ: TFloatField;
    QrTXIndInnDtCorrApo: TDateTimeField;
    QrTXIndInnPedItsFin: TLargeintField;
    DsTXIndInn: TDataSource;
    MeAviso: TMemo;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    PnBaixa: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label4: TLabel;
    Label11: TLabel;
    EdCtrl2: TdmkEdit;
    EdBxaObserv: TdmkEdit;
    EdIMEISrc: TdmkEditCB;
    CBIMEISrc: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    EdBxaQtde: TdmkEdit;
    Label8: TLabel;
    DqPsq: TmySQLDirectQuery;
    EdEstqQtdeFut: TdmkEdit;
    Label14: TLabel;
    EdEstqQtdeAtu: TdmkEdit;
    Label15: TLabel;
    Panel5: TPanel;
    CkContinuar: TCheckBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RGBaixaEOuGera: TRadioGroup;
    PnGera: TPanel;
    Label6: TLabel;
    EdCtrl1: TdmkEdit;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    Label9: TLabel;
    EdObserv: TdmkEdit;
    Label20: TLabel;
    LaTXRibCla: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet1: TSpeedButton;
    SBNewPallet: TSpeedButton;
    CkEncerraPallet: TdmkCheckBox;
    Label48: TLabel;
    EdPedItsFin: TdmkEditCB;
    CBPedItsFin: TdmkDBLookupComboBox;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label10: TLabel;
    EdCustoMOUni: TdmkEdit;
    Label13: TLabel;
    EdCustoMOTot: TdmkEdit;
    EdValorMP: TdmkEdit;
    Label16: TLabel;
    Label21: TLabel;
    EdValorT: TdmkEdit;
    EdCusFrtMORet: TdmkEdit;
    Label23: TLabel;
    EdBxaValorT: TdmkEdit;
    Label22: TLabel;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    Label24: TLabel;
    EdSerieTal: TdmkEditCB;
    CBSerieTal: TdmkDBLookupComboBox;
    Label25: TLabel;
    EdTalao: TdmkEdit;
    QrTXIndInnSerieTal: TLargeintField;
    QrTXIndInnTalao: TLargeintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure Criar1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdIMEISrcRedefinido(Sender: TObject);
    procedure EdQtdeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RGBaixaEOuGeraClick(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure EdCustoMOUniRedefinido(Sender: TObject);
    procedure EdValorMPRedefinido(Sender: TObject);
    procedure EdCusFrtMORetRedefinido(Sender: TObject);
    procedure EdBxaQtdeRedefinido(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdTalaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FUltGGX, FGraGruX: Integer;
    FCriando, FBaixa, FGera: Boolean;
    FValUni: Double;
    //
    procedure CalculaValorGer();
    procedure MostraOcultaBaixaEOuGera();
    procedure ReopenTXInnIts(Controle: Integer);
    procedure RecalculaSaldoInn();

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FFornecMO: Integer;
    FPallOnEdit: array of Integer;
    FQtdEditing, FValorMPUniOri, FValorMPUniInn: Double;
    //
    procedure RedefineCustos();
  end;

  var
  FmTXIndDst: TFmTXIndDst;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXIndCab, Principal, UnTX_PF, AppListas, ModuleGeral, UnTX_EFD_ICMS_IPI,
  UnTX_Jan, ModProd;

{$R *.DFM}

procedure TFmTXIndDst.BtOKClick(Sender: TObject);
const
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CliVenda   = 0;
  //MovimTwn   = 0;
  //Pallet     = 0;
  QtdGer     = 0;
  AptoUso    = 1;
  //
  Fornecedor = 0;
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  //PedItsFin = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  TXMulFrnCab = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, ClienteMO, GraGruX,
  Ctrl1, Ctrl2, MovimTwn, Pallet, PedItsFin, StqCenLoc, ReqMovEstq, ClientMO,
  FornecMO, PalToClose: Integer;
  Qtde, ValorT, CustoMOUni, CustoMOTot, ValorMP: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
  //
  IMEISrc, SerieTal, Talao: Integer;
  SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, SrcGGX: Integer;
begin
  if FBaixa then
  begin
    IMEISrc := EdIMEISrc.ValueVariant;
    if MyObjects.FIC(IMEISrc = 0, EdIMEISrc,
    'Baixa do estoque selecionado, mas sem IME-I de origem!') then Exit;
  end;
  //
  if MyObjects.FIC(TPData.Date<2, TPData, 'Data/hora n�o definida!') then Exit;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := 0;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Ctrl1          := EdCtrl1.ValueVariant;
  Ctrl2          := EdCtrl2.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClienteMO      := FClientMO;
  ClientMO       := FClientMO;
  FornecMO       := FFornecMO;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  MovimID        := emidEmIndstrlzc;
  MovimNiv       := eminIndzcDst;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Qtde           := EdQtde.ValueVariant;
  CustoMOUni     := EdCustoMOUni.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Marca          := EdMarca.Text;
  Pallet         := EdPallet.ValueVariant;
  PedItsFin      := EdPedItsFin.ValueVariant;
  //
  SerieTal       := EdSerieTal.ValueVariant;
  Talao          := EdTalao.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  // Pode ser raspa por peso sem custo!
  if TX_PF.TXFic(GraGruX, Empresa, Fornecedor, Pallet, Qtde, ValorT, EdGraGruX,
    EdPallet, EdQtde, (*EdValorT*)nil, ExigeFornecedor, CO_GraGruY_6144_TXCadFcc,
    EdStqCenLoc, SerieTal, Talao, EdSerieTal, EdTalao)
  then
    Exit;
  if MyObjects.FIC(Pallet = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
  if MyObjects.FIC(SerieTal = 0, EdSerieTal, 'Informe a S�rie de Tal�o!') then
    Exit;
  if MyObjects.FIC(Talao = 0, EdTalao, 'Informe o n�mero do Tal�o!') then
    Exit;
  //
  PalToClose := Pallet;
  if FBaixa then
  begin
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  end else
    MovimTwn := 0;
  //
  SrcMovID   := TEstqMovimID(0);
  SrcNivel1  := 0;
  SrcNivel2  := 0;
  SrcGGX     := 0;
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Ctrl1);
  //
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
  MovimID, MovimNiv, Pallet, GraGruX, Qtde, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
  CliVenda, Controle, CustoMOUni, CustoMOTot, ValorMP, DstMovID,
  DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei004(*Gera��o de produto resultante de industrializa��o*)) then
  begin
    Ctrl1  := Controle;
    Pallet := 0;
    TX_PF.AtualizaSaldoIMEI(Ctrl1, True);
    TX_PF.AtualizaTXPedIts_Fin(PedItsFin);
    //
    if FBaixa then
    begin
      SrcMovID       := TEstqMovimID(QrTXIndInnMovimID.Value);
      SrcNivel1      := QrTXIndInnCodigo.Value;
      SrcNivel2      := QrTXIndInnControle.Value;
      SrcGGX         := QrTXIndInnGraGruX.Value;
      CustoMOUni     := 0;
      CustoMOTot     := 0;
      ValorMP        := 0;
      GraGruX        := FmTXIndCab.QrTXIndInnGraGruX.Value;
      Marca          := FmTXIndCab.QrTXIndInnMarca.Value;
      MovimID        := emidEmIndstrlzc;
      MovimNiv       := eminIndzcBxa;
      Qtde           := -EdBxaQtde.ValueVariant;
      ValorT         := -EdBxaValorT.ValueVariant;
      Observ         := EdBxaObserv.Text;
      SerieTal       := QrTXIndInnSerieTal.Value;
      Talao          := QrTXIndInnTalao.Value;
      //
      //PedItsLib      := 0;
      PedItsFin      := 0;
      //PedItsVda      := 0;
      //GSPSrcMovID     := 0;
      //GSPSrcNiv2     := 0;
      ReqMovEstq     := 0;
      if Ctrl2 = 0 then
        SQLType := stIns
      else
        SQLType := ImgTipo.SQLType;
      //
      Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, SQLType, Ctrl2);

      if TX_PF.InsUpdTXMovIts(SQLType, Codigo, MovimCod, MovimTwn, Empresa, FOrnecedor,
      MovimID, MovimNiv, Pallet, GraGruX, Qtde, ValorT,
      DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
      CliVenda, Controle, CustoMOUni, CustoMOTot, ValorMP, DstMovID,
      DstNivel1, DstNivel2, QtdGer,
      AptoUso, FornecMO, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda,
      ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
      QtdAnt, SerieTal, Talao,
      CO_0_GGXRcl,
      CO_0_MovCodPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iutpei005 (*Baixa de artigo em confec��o*)) then
      begin
        FmTXIndCab.DistribuicaoCusto();
        Ctrl2 := Controle;
        TX_PF.AtualizaSaldoIMEI(Ctrl2, True);
        TX_PF.AtualizaSaldoIMEI(SrcNivel2, True);
      end;
    end;
    // Nao se aplica. Calcula com function propria a seguir.
    //TX_PF.AtualizaTotaisTXXxxCab('vsopecab', MovimCod);
    TX_PF.AtualizaTotaisTXIndCab(FmTXIndCab.QrTXIndCabMovimCod.Value);
    FmTXIndCab.AtualizaNFeItens();
    //
    if CkEncerraPallet.Checked then
      TX_PF.EncerraPalletSimples(PalToClose, FEmpresa, FClientMO, QrTXPallet, FPallOnEdit);
    TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmIndstrlzc, MovimCod);
    FmTXIndCab.LocCod(Codigo, Codigo);
    ReopenTXInnIts(Ctrl1);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      //
      EdIMEISrc.ValueVariant     := 0;
      CBIMEISrc.KeyValue         := Null;
      RGBaixaEOuGera.Enabled     := True;
      EdBxaQtde.ValueVariant     := 0;
      EdBxaValorT.ValueVariant   := 0;
      EdBxaObserv.ValueVariant   := '';

      EdCtrl1.ValueVariant       := 0;
      EdCtrl2.ValueVariant       := 0;

      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdQtde.ValueVariant        := 0;
      EdCustoMOUni.ValueVariant  := 0;
      EdCusFrtMORet.ValueVariant := 0;
      EdValorT.ValueVariant      := 0;
      EdMarca.ValueVariant       := '';
      EdObserv.Text              := '';
      //
      EdGraGruX.Enabled          := True;
      CBGraGruX.Enabled          := True;
      //
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      //EdGraGruX.SetFocus;
      MostraOcultaBaixaEOuGera();
    end else
      Close;
  end;
end;

procedure TFmTXIndDst.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXIndDst.CalculaValorGer();
var
  Qtde, CustoMOUni, CustoMOTot, ValorMP, CusFrtMORet, ValorT: Double;
begin
  Qtde        := EdQtde.ValueVariant;
  CustoMOUni  := EdCustoMOUni.ValueVariant;
  ValorMP     := Qtde * (FValorMPUniInn + FValorMPUniOri); // vai ser um ou outro !!! ???
  CusFrtMORet := EdCusFrtMORet.ValueVariant;
  //
  CustoMOTot  := Qtde * CustoMOUni;
  ValorT      := ValorMP + CustoMOTot + CusFrtMORet;
  //
  EdValorMP.ValueVariant    := ValorMP;
  EdCustoMOTot.ValueVariant := CustoMOTot;
  EdValorT.ValueVariant     := ValorT;
end;

procedure TFmTXIndDst.Criar1Click(Sender: TObject);
(*
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  TX_PF.CadastraPalletWetEnd(FEmpresa, EdPallet, CBPallet, QrTXPallet,
    emidEmProcWE, GraGruX);
*)
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  TX_PF.CadastraPalletRibCla(FEmpresa, FClientMO, EdPallet, CBPallet,
    QrTXPallet, emidEmIndstrlzc, GraGruX);
end;

procedure TFmTXIndDst.EdBxaQtdeRedefinido(Sender: TObject);
begin
  RecalculaSaldoInn();
end;

procedure TFmTXIndDst.EdCusFrtMORetRedefinido(Sender: TObject);
begin
  CalculaValorGer();
end;

procedure TFmTXIndDst.EdCustoMOUniRedefinido(Sender: TObject);
begin
  CalculaValorGer();
end;

procedure TFmTXIndDst.EdGraGruXChange(Sender: TObject);
begin
  FGraGruX := EdGraGruX.ValueVariant;
  EdPallet.ValueVariant := 0;
  CBPallet.KeyValue     := Null;
  if FGraGruX = 0 then
    QrTXPallet.Close
  else
    TX_PF.ReopenTXPallet(QrTXPallet, FEmpresa, FClientMO, FGraGruX, '', FPallOnEdit);
end;

procedure TFmTXIndDst.EdIMEISrcRedefinido(Sender: TObject);
var
  IMEISrc, GraGruX, GraGruY, GGXTwn: Integer;
  Tabela: String;
begin
  Screen.Cursor := crHourGlass;
  try
    IMEISrc := EdIMEISrc.ValueVariant;
    if IMEISrc = 0 then
      Exit;
    //
    GraGruX := QrTXIndInnGraGruX.Value;
    GraGruY := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    Tabela  := AppPF.ObtemNomeTabelaGraGruY(GraGruY);
    //
    UnDmkDAC_PF.AbreMySQLDirectQuery0(DqPsq, DModG.MyCompressDB, [
    'SELECT GGXTwn  ',
    'FROM ' + Tabela,
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //
    GGXTwn := USQLDB.Int(DqPsq, 'GGXTwn');
    if GGXTwn = 0 then
      Exit;
    //
    if QrGraGruX.Locate('Controle', GGXTwn, []) then
    begin
      EdGraGruX.ValueVariant := QrGraGruXControle.Value;
      CBGraGruX.KeyValue     := QrGraGruXControle.Value;
    end;
    //
    RedefineCustos();
    //
    RecalculaSaldoInn();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTXIndDst.EdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdQtde.ValueVariant := EdBxaQtde.ValueVariant;
end;

procedure TFmTXIndDst.EdQtdeRedefinido(Sender: TObject);
begin
  CalculaValorGer();
end;

procedure TFmTXIndDst.EdTalaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Talao: Integer;
begin
  if Key = VK_F4 then
    if TX_PF.ObtemProximoTalao(FEmpresa, EdSerieTal, Talao) then
      EdTalao.ValueVariant := Talao;
end;

procedure TFmTXIndDst.EdValorMPRedefinido(Sender: TObject);
begin
  CalculaValorGer();
end;

procedure TFmTXIndDst.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXIndDst.FormCreate(Sender: TObject);
const
  Controle = 0;
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType     := stLok;
  FCriando            := True;
  FBaixa              := False;
  FGera               := False;
  FQtdEditing         := 0;
  Agora               := DModG.ObtemAgora();
  TPData.Date         := Agora;
  EdHora.ValueVariant := Agora;
  SetLength(FPallOnEdit, 0);
  FUltGGX             := 0;
  FGraGruX            := 0;
  FValUni             := 0;
  FValorMPUniOri      := 0;
  FValorMPUniInn      := 0;
  //
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_4096_TXCadInt) + ',' +
    Geral.FF0(CO_GraGruY_6144_TXCadFcc) + ')');
  TX_PF.AbreTXSerTal(QrTXSerTal);
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  TX_EFD_ICMS_IPI.ReopenTXIndPrcAtu(QrTXIndInn, FmTXIndCab.QrTXIndCabMovimCod.Value,
    Controle, FmTXIndCab.QrTXIndCabTemIMEIMrt.Value, TEstqMovimNiv.eminIndzcInn);
end;

procedure TFmTXIndDst.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXIndDst.FormShow(Sender: TObject);
begin
  FCriando := False;
end;

procedure TFmTXIndDst.Gerenciamento1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXPallet(EdPallet.ValueVariant);
end;

procedure TFmTXIndDst.MostraOcultaBaixaEOuGera();
begin
  FBaixa := RGBaixaEOuGera.ItemIndex in ([1]);
  FGera  := RGBaixaEOuGera.ItemIndex in ([1,2]);
  //
  PnBaixa.Visible := FBaixa;
  PnGera.Visible  := FGera;
  //
  BtOK.Enabled := RGBaixaEOuGera.ItemIndex > 0;
  try
    if not FCriando then
    begin
      if FBaixa then
        EdIMEISrc.SetFocus
      else
      if FGera then
        EdGraGruX.SetFocus;
    end;
  except
    // nada
  end;
end;

procedure TFmTXIndDst.RecalculaSaldoInn();
var
  EstqQtdeAtu, EstqQtdeFut, ValorMP, BxaQtde: Double;
begin
  BxaQtde     := EdBxaQtde.ValueVariant;
  EstqQtdeAtu := QrTXIndInnSdoVrtQtd.Value + FQtdEditing;
  EstqQtdeFut := EstqQtdeAtu - BxaQtde;
  //
  EdEstqQtdeAtu.ValueVariant := EstqQtdeAtu;
  EdEstqQtdeFut.ValueVariant := EstqQtdeFut;
  //
  if QrTXIndInnQtde.Value > 0 then
    FValUni   := QrTXIndInnValorT.Value / QrTXIndInnQtde.Value
  else
    FValUni   := 0;
  EdBxaValorT.ValueVariant := FValUni * EdBxaQtde.ValueVariant;
end;

procedure TFmTXIndDst.RedefineCustos();
begin
  if ImgTipo.SQLType = stIns then
    EdCustoMOUni.ValueVariant := QrTXIndInnCustoMOUni.Value;
  //
  if QrTXIndInnQtde.Value <> 0 then
    FValorMPUniInn := QrTXIndInnValorMP.Value / QrTXIndInnQtde.Value
  else
    FValorMPUniInn := 0;
  //
  CalculaValorGer();
end;

procedure TFmTXIndDst.ReopenTXInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXIndDst.RGBaixaEOuGeraClick(Sender: TObject);
begin
  MostraOcultaBaixaEOuGera();
end;

procedure TFmTXIndDst.SBNewPalletClick(Sender: TObject);
begin
  TX_PF.GeraNovoPallet(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  EdGraGruX, EdPallet, CBPallet, SBNewPallet, QrTXPallet, FPallOnEdit);
end;

procedure TFmTXIndDst.SbPallet1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, SbPallet1);
end;

end.
