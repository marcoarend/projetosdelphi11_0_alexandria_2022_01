unit ModTX_CRC;

interface

uses
  Winapi.Windows, System.SysUtils, System.Classes, mySQLDirectQuery, dmkGeral,
  UnAppEnums, UnDmkEnums, Data.DB, mySQLDbTables,
  UnProjGroup_Consts, UnTX_Tabs, UnGrl_Consts, UnProjGroup_Vars;

type
  TDmModTX_CRC = class(TDataModule)
    QrLocMFC: TmySQLQuery;
    QrLocMFCCodigo: TIntegerField;
    QrLocMFI: TmySQLQuery;
    QrLocMFIControle: TIntegerField;
    DqAux: TmySQLDirectQuery;
    DqAu2: TmySQLDirectQuery;
    QrSrcNiv2: TmySQLQuery;
    QrUniFrn: TmySQLQuery;
    QrUniFrnTerceiro: TIntegerField;
    QrUniFrnTXMulFrnCab: TIntegerField;
    QrUniFrnQtde: TFloatField;
    QrMulFrn: TmySQLQuery;
    QrMulFrnFornece: TIntegerField;
    QrMulFrnQtde: TFloatField;
    QrMulFrnSiglaTX: TWideStringField;
    QrTotFrn: TmySQLQuery;
    QrTotFrnQtde: TFloatField;
    QrMFSrc: TmySQLQuery;
    QrMFSrcControle: TIntegerField;
    QrMFSrcMovimID: TIntegerField;
    QrMFSrcMovimNiv: TIntegerField;
    QrMFSrcMovimCod: TIntegerField;
    QrBoxesPal: TmySQLQuery;
    QrBoxesPalCodigo: TIntegerField;
    QrBoxesPalCacCod: TIntegerField;
    QrBoxesPalTXPaXXXIts: TIntegerField;
    QrBoxesPalTXPallet: TFloatField;
    QrBoxesPalTMI_Dest: TIntegerField;
    QrBoxesPalCacID: TFloatField;
    QrBoxesPalTMI_Sorc: TIntegerField;
    QrBoxesPalTMI_Baix: TIntegerField;
    QrBoxesPalTecla: TIntegerField;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrTMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    QrIMEI: TmySQLQuery;
    QrIMEICodigo: TIntegerField;
    QrIMEIControle: TIntegerField;
    QrIMEIMovimCod: TIntegerField;
    QrIMEIMovimNiv: TIntegerField;
    QrIMEIMovimTwn: TIntegerField;
    QrIMEIEmpresa: TIntegerField;
    QrIMEITerceiro: TIntegerField;
    QrIMEICliVenda: TIntegerField;
    QrIMEIMovimID: TIntegerField;
    QrIMEIDataHora: TDateTimeField;
    QrIMEIPallet: TIntegerField;
    QrIMEIGraGruX: TIntegerField;
    QrIMEIQtde: TFloatField;
    QrIMEIValorT: TFloatField;
    QrIMEISrcMovID: TIntegerField;
    QrIMEISrcNivel1: TIntegerField;
    QrIMEISrcNivel2: TIntegerField;
    QrIMEISrcGGX: TIntegerField;
    QrIMEISdoVrtQtd: TFloatField;
    QrIMEIObserv: TWideStringField;
    QrIMEISerieTal: TIntegerField;
    QrIMEITalao: TIntegerField;
    QrIMEIFornecMO: TIntegerField;
    QrIMEICustoMOUni: TFloatField;
    QrIMEICustoMOTot: TFloatField;
    QrIMEIValorMP: TFloatField;
    QrIMEIDstMovID: TIntegerField;
    QrIMEIDstNivel1: TIntegerField;
    QrIMEIDstNivel2: TIntegerField;
    QrIMEIDstGGX: TIntegerField;
    QrIMEIQtdGer: TFloatField;
    QrIMEIQtdAnt: TFloatField;
    QrIMEIAptoUso: TSmallintField;
    QrIMEIMarca: TWideStringField;
    QrIMEITpCalcAuto: TIntegerField;
    QrIMEIZerado: TSmallintField;
    QrIMEIPedItsLib: TIntegerField;
    QrIMEIPedItsFin: TIntegerField;
    QrIMEIPedItsVda: TIntegerField;
    QrIMEILk: TIntegerField;
    QrIMEIDataCad: TDateField;
    QrIMEIDataAlt: TDateField;
    QrIMEIUserCad: TIntegerField;
    QrIMEIUserAlt: TIntegerField;
    QrIMEIAlterWeb: TSmallintField;
    QrIMEIAtivo: TSmallintField;
    QrIMEIReqMovEstq: TIntegerField;
    QrIMEIStqCenLoc: TIntegerField;
    QrIMEIItemNFe: TIntegerField;
    QrIMEITXMorCab: TIntegerField;
    QrIMEITXMulFrnCab: TIntegerField;
    DqSum: TmySQLDirectQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    // A
    procedure AtualizaImeiValorT(Controle: Integer);
    function  AtualizaTXMovIts_CusEmit(Controle: Integer): Boolean;
    procedure AtualizaTXMulFrnCabNew(SinalOrig: TSinal; DefMulFrn: TEstqDefMulFldEMxx;
              MovTypeCod: Integer; MovimIDs: array of TEstqMovimID;
              MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
    procedure AtualizaTXMulNFeCab(SinalOrig: TSinal;
              DefMulNFe: TEstqDefMulFldEMxx; MovTypeCod: Integer;
              MovimID: TEstqMovimID; MovimNivsOri,
              MovimNivsDst: array of TEstqMovimNiv);
    // D
    procedure DistribuicaoCusto(EstqMovimID: TEstqMovimID; eminSrc, eminInn,
              eminDst: TEstqMovimNiv);
    // E
    function  EncerraPallet(Pallet: Integer; Pergunta: Boolean): Boolean;
    // I
    procedure InformaSemFornecedor();
    procedure InformaSemNFe();
    // O
    function  ObtemSrcNivel2DeIMEI(IMEI: Integer): Integer;

  end;

var
  DmModTX_CRC: TDmModTX_CRC;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TDmModTX_CRC }

uses
  UnTX_PF, DmkDAC_PF, ModuleGeral, Module, UMySQLDB, UMySQLModule, UnMyObjects;

procedure TDmModTX_CRC.AtualizaImeiValorT(Controle: Integer);
const
  sProcName = 'TDmModTX_CRC.AtualizaImeiValorT()';
var
  CusFrtAvuls, CusFrtMOEnv, CusFrtMORet, (*CusFrtTrnsf,*) ValorMP, CustoPQ, ValorT,
  CustoMOTot: Double;
  MovimID, MovimNiv: Integer;
  Continua: Boolean;
begin
  TX_PF.AdicionarNovosTX_emid();
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT CusFrtAvuls, CusFrtMOEnv, CusFrtMORet, ',
  //'CusFrtTrnsf, ',
  'ValorMP, CustoPQ, CustoMOTot, ',
  'MovimID, MovimNiv ',
  'FROM txmovits ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  CusFrtAvuls := USQLDB.Flu(DqAux, 'CusFrtAvuls');
  CusFrtMOEnv := USQLDB.Flu(DqAux, 'CusFrtMOEnv');
  CusFrtMORet := USQLDB.Flu(DqAux, 'CusFrtMORet');
  //CusFrtTrnsf := USQLDB.Flu(DqAux, 'CusFrtTrnsf');
  ValorMP     := USQLDB.Flu(DqAux, 'ValorMP');
  CustoPQ     := USQLDB.Flu(DqAux, 'CustoPQ');
  CustoMOTot  := USQLDB.Flu(DqAux, 'CustoMOTot');
  //
  MovimID     := USQLDB.Int(DqAux, 'MovimID');
  MovimNiv    := USQLDB.Int(DqAux, 'MovimNiv');
  //
  ValorT      := 0;
  Continua    := False;
  case TEstqMovimID(MovimID) of
    (*01*)emidCompra,
    (*16*)emidEntradaPlC:
    begin
      ValorT := CusFrtAvuls + CusFrtMOEnv + ValorMP; // + CusFrtTrnsf
      Continua := True;
    end;
    (*02*)emidVenda:
    begin
      ValorT := ValorMP - CusFrtAvuls; // + CusFrtTrnsf
      Continua := True;
    end;
    (*05*)emidEmIndstrlzc:
    begin
      ValorT := CusFrtMORet + ValorMP + CustoPQ + CustoMOTot; // + CusFrtTrnsf
      Continua := True;
    end;
    (*25*)emidTransfLoc:
    begin
      ValorT := CusFrtAvuls + CusFrtMOEnv + CusFrtMORet + ValorMP; // + CusFrtTrnsf
      Continua := True;
    end;
    else
    begin
      Geral.MB_Erro('"MovimID" ' + Geral.FF0(MovimID) +
      ' n�o implementado em ' + sProcName + sLineBreak +
      'IME-I: ' + Geral.FF0(Controle));
      Continua := False;
    end;
  end;
  if Continua then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txmovits', False, [
    'ValorT'], ['Controle'], [
    ValorT], [Controle], True);
  end;
end;

function TDmModTX_CRC.AtualizaTXMovIts_CusEmit(Controle: Integer): Boolean;
begin
  Result := True;
end;

procedure TDmModTX_CRC.AtualizaTXMulFrnCabNew(SinalOrig: TSinal;
  DefMulFrn: TEstqDefMulFldEMxx; MovTypeCod: Integer;
  MovimIDs: array of TEstqMovimID; MovimNivsOri,
  MovimNivsDst: array of TEstqMovimNiv);
  //
const
 sProcName = 'TDmModTX_CRC.AtualizaTXMulFrnCabNew()';
  function ReInsereNovoTXMulFrnCab(): Integer;
  const
    Qtde    = 0.000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulfrncab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulFrn)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrnits', False, [
      'Qtde'], ['Codigo'], [Qtde], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulfrncab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulfrncab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulFrn, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoTXFrnMulIts(Codigo, Remistura, CodRemix, Fornece:
  Integer; OrigQtde: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Qtde: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigQtde < 0 then
      Qtde := - OrigQtde
    else
      Qtde := OrigQtde;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulfrnits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND Fornece=' + Geral.FF0(Fornece),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulfrnits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulfrnits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'Fornece', 'Qtde'(*, 'PerQtde'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    Fornece, Qtde(*, PerQtde*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  Terceiro, TXMulFrnCab, I: Integer;
  Codigo, Remistura, CodRemix, Fornece: Integer;
  Qtde, PerQtde, TotMul: Double;
  //
  SiglaTX, Nome, Percentual, FldUpd: String;
  sSrcNivel2: String;
begin
  SQLMovimID  := '';
  FldUpd      := '???';
  for I := Low(MovimIDs) to High(MovimIDs) do
  begin
    if SQLMovimID <> '' then
      SQLMovimID :=  SQLMovimID + ',';
    SQLMovimID :=  Geral.FF0(Integer(MovimIDs[I]));
  end;
  if SQLMovimID <> '' then
    SQLMovimID :=  'AND MovimID IN (' + SQLMovimID + ')';
  //
  SQLMovimNiv  := '';
  for I := Low(MovimNivsOri) to High(MovimNivsOri) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  // IMEIs de origem
  case DefMulFrn of
    TEstqDefMulFldEMxx.edmfSrcNiv2:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_SEL_TAB_TMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      '']);
      //
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, TXMulFrnCab, Qtde   ',
      'FROM ' + CO_SEL_TAB_TMI,
      'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
      '']);
      FldUpd := 'Controle';
    end;
    TEstqDefMulFldEMxx.edmfMovCod:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, TXMulFrnCab, SUM(Qtde) Qtde  ',
      'FROM ' + CO_SEL_TAB_TMI,
      'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
      SQLMovimID,
      SQLMovimNiv,
      'GROUP BY Terceiro, TXMulFrnCab ',
      'ORDER BY Terceiro,  TXMulFrnCab ',
      ' ',
      '']);
      FldUpd := 'MovimCod';
    end;
    else
    begin
      Geral.MB_Erro('"Defini��o de fornecedor abortada!' + sLineBreak +
      '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulFrn)));
      Exit;
    end;
  end;
  //
  //Geral.MB_SQL(nil, QrUniFrn);
  // Se tiver Item sem fornecedor, desiste!
  if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnTXMulFrnCab.Value = 0) then
  begin
(*20200920
    if VAR_MEMO_DEF_TX_MUL_FRN <> nil then
       VAR_MEMO_DEF_TX_MUL_FRN.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
       sLineBreak + VAR_MEMO_DEF_TX_MUL_FRN.Text
    else
      InformaSemFornecedor();
*)
    Exit;
  end;
  Terceiro    := 0;
  TXMulFrnCab := 0;
  //
  // Se tiver s� um registro...
  if QrUniFrn.RecordCount = 1 then
  begin
    if QrUniFrnTerceiro.Value <> 0 then
      Terceiro := QrUniFrnTerceiro.Value
    else
      TXMulFrnCab := QrUniFrnTXMulFrnCab.Value;
  end else
  // Se tiver varios registros...
  begin
    Terceiro := 0;
    Codigo   := ReInsereNovoTXMulFrnCab();
    while not QrUniFrn.Eof do
    begin
      if (QrUniFrnTerceiro.Value <> 0) and (QrUniFrnTXMulFrnCab.Value = 0) then
      begin
        Remistura := 0; // False
        CodRemix  := 0;
        Fornece   := QrUniFrnTerceiro.Value;
        Qtde     := QrUniFrnQtde.Value;
        ReInsereNovoTXFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Qtde, True);
      end else
      if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnTXMulFrnCab.Value <> 0) then
      begin
        Remistura := 1; // True
        CodRemix  := QrUniFrnTXMulFrnCab.Value;
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
        'SELECT Fornece, SUM(Qtde) Qtde, "" SiglaTX',
        'FROM vsmulfrnits',
        'WHERE Codigo=' + Geral.FF0(CodRemix),
        'GROUP BY Fornece ',
        '']);
        TotMul := 0;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          TotMul := TotMul + QrMulFrnQtde.Value;
          //
          QrMulFrn.Next;
        end;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          //Qtde     := QrMulFrnQtde.Value;
          if TotMul <> 0 then
            Qtde := (QrMulFrnQtde.Value / TotMul) * QrUniFrnQtde.Value
          else
            Qtde := 0;
          //
          Fornece   := QrMulFrnFornece.Value;
          ReInsereNovoTXFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Qtde, False);
          //
          QrMulFrn.Next;
        end;
      end;
      //
      QrUniFrn.Next;
    end;
    // Atualiza TXMulFrnCab!
    UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
    'SELECT mfi.Fornece, SUM(mfi.Qtde) Qtde, vem.SiglaTX',
    'FROM vsmulfrnits mfi',
    'LEFT JOIN vsentimp vem ON vem.Codigo=mfi.Fornece',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY mfi.Fornece',
    'ORDER BY Qtde DESC',
    '']);
    UnDMkDAC_PF.AbreMySQLQuery0(QrTotFrn, Dmod.MyDB, [
    'SELECT SUM(mfi.Qtde) Qtde ',
    'FROM vsmulfrnits mfi',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    '']);
    Nome := '';
    while not QrMulFrn.Eof do
    begin
      if QrMulFrnSiglaTX.Value = '' then
        SiglaTX := TX_PF.DefineSiglaTX_Frn(QrMulFrnFornece.Value)
      else
        SiglaTX := QrMulFrnSiglaTX.Value;
      //
      if QrTotFrnQtde.Value <> 0 then
      begin
        PerQtde   := QrMulFrnQtde.Value / QrTotFrnQtde.Value * 100;
        Percentual := Geral.FI64(Round(PerQtde));
      end else
      begin
        PerQtde   := 0;
        Percentual := '??';
      end;
      //
      Nome := Nome + SiglaTX + ' ' + Percentual + '% ';
      //
      QrMulFrn.Next;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrncab', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
      TXMulFrnCab := Codigo;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_UPD_TAB_TMI,
  'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
  'TXMulFrnCab=' + Geral.FF0(TXMulFrnCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_SEL_TAB_TMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle', EmptyStr);
  if Trim(sSrcNivel2) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
    'SELECT MovimID, MovimNiv, MovimCod, Controle ',
    'FROM ' + CO_SEL_TAB_TMI,
    'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
    '']);
    QrMFSrc.First;
    while not QrMFSrc.Eof do
    begin
      UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + CO_UPD_TAB_TMI,
      'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
      'TXMulFrnCab=' + Geral.FF0(TXMulFrnCab),
      'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
      '']);
      //
      QrMFSrc.Next;
    end;
  end;
end;

procedure TDmModTX_CRC.AtualizaTXMulNFeCab(SinalOrig: TSinal;
  DefMulNFe: TEstqDefMulFldEMxx; MovTypeCod: Integer; MovimID: TEstqMovimID;
  MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
{ Ver no futuro se precisa!
 //
  function ReInsereNovoTXMulNFeCab(): Integer;
  const
    Qtde    = 0.000;
    //PerQtde = 0.0000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulnfecab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulNFe)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulnfeits', False, [
      'Qtde'(*, 'PerQtde'*)], ['Codigo'], [
      Qtde(*, PerQtde*)], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulnfecab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulnfecab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulNFe, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoTXNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum:
  Integer; OrigQtde: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Qtde: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigQtde < 0 then
      Qtde := - OrigQtde
    else
      Qtde := OrigQtde;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulnfeits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND NFeSer=' + Geral.FF0(NFeSer),
    'AND NFeNum=' + Geral.FF0(NFeNum),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulnfeits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulnfeits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'NFeSer', 'NFeNum',
    'Qtde'(*, 'PerQtde'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    NFeSer, NFeNum,
    Qtde(*, PerQtde*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  TXMulNFeCab, I: Integer;
  //
  Codigo, Remistura, CodRemix, NFeSer, NFeNum: Integer;
  Qtde, PerQtde, TotMul: Double;
  //
  Nome, Percentual, FldUpd: String;
  sSrcNivel2, SiglaNFe: String;
  //
  TabCab, FldCabSer, FldCabdNum: String;
}
begin
{
  if MyObjects.FIC(DefMulNFe <> edmfMovCod, nil,
  '"ID" deve ser do tipo "edmfMovCod"!') then
    Exit;
  TabCab := TX_PF.ObtemNomeTabelaTXXxxCab(MovimID);
  TX_PF.ObtemNomeCamposNFeTXXxxCab(MovimID, FldCabSer, FldCabdNum);
  //
  FldUpd := 'MovimCod';
  NFeSer := 0;
  if MovimID = TEstqMovimID.emidIndsV S then
  begin
    NFeSer      := 0;
    NFeNum      := 0;
    TXMulNFeCab := 0;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrTXCabNFe, Dmod.MyDB, [
    'SELECT ' + FldCabSer + ' Serie, ' + FldCabdNum + ' nNF ',
    'FROM ' + TabCab,
    'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrTXCabNFenNF.Value <> 0 then
    begin
      NFeSer      := QrTXCabNFeSerie.Value;
      NFeNum      := QrTXCabNFenNF.Value;
      TXMulNFeCab := 0;
    end;
  end;
  if NFeNum = 0 then
  begin
    SQLMovimID :=  'AND MovimID=' + Geral.FF0(Integer(MovimID));
    //
    SQLMovimNiv  := '';
    for I := Low(MovimNivsOri) to High(MovimNivsOri) do
    begin
      if SQLMovimNiv <> '' then
        SQLMovimNiv :=  SQLMovimNiv + ',';
      SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
    end;
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
    //
    // IMEIs de origem
    case DefMulNFe of
      TEstqDefMulFldEMxx.edmfSrcNiv2:
      begin
        UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
        '  SELECT SrcNivel2 ',
        '  FROM ' + CO_SEL_TAB_TMI,
        '  WHERE Controle=' + Geral.FF0(MovTypeCod),
        '']);
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrUniNFe, Dmod.MyDB, [
        'SELECT NFeSer, NFeNum, TXMulNFeCab, Qtde   ',
        'FROM ' + CO_SEL_TAB_TMI,
        'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
        '']);
        FldUpd := 'Controle';
      end;
      TEstqDefMulFldEMxx.edmfMovCod:
      begin
        UnDMkDAC_PF.AbreMySQLQuery0(QrUniNFe, Dmod.MyDB, [
        'SELECT NFeSer, NFeNum, TXMulNFeCab, SUM(Qtde) Qtde  ',
        'FROM ' + CO_SEL_TAB_TMI,
        'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
        SQLMovimID,
        SQLMovimNiv,
        'GROUP BY NFeNum, TXMulNFeCab ',
        'ORDER BY NFeNum, TXMulNFeCab ',
        ' ',
        '']);
        FldUpd := 'MovimCod';
      end;
      else
      begin
        Geral.MB_Erro('"Defini��o de NFe abortada!' + sLineBreak +
        '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulNFe)));
        Exit;
      end;
    end;
    //
    //Geral.MB_SQL(nil, QrUniNFe);
    // Se tiver Item sem NFe, desiste!
    if (QrUniNFeNFeNum.Value = 0) and (QrUniNFeTXMulNFeCab.Value = 0) then
    begin
      if VAR_MEMO_DEF_V S_MUL_NFE <> nil then
         VAR_MEMO_DEF_V S_MUL_NFE.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
         sLineBreak + VAR_MEMO_DEF_V S_MUL_NFE.Text
      else
        InformaSemNFe();
      Exit;
    end;
    NFeSer      := 0;
    NFeNum      := 0;
    TXMulNFeCab := 0;
    //
    // Se tiver s� um registro...
    if QrUniNFe.RecordCount = 1 then
    begin
      if QrUniNFeNFeNum.Value <> 0 then
      begin
        NFeSer := QrUniNFeNFeSer.Value;
        NFeNum := QrUniNFeNFeNum.Value;
      end else
        TXMulNFeCab := QrUniNFeTXMulNFeCab.Value;
    end else
    // Se tiver varios registros...
    begin
      NFeSer   := 0;
      NFeNum   := 0;
      Codigo   := ReInsereNovoTXMulNFeCab();
      while not QrUniNFe.Eof do
      begin
        if (QrUniNFeNFeNum.Value <> 0) and (QrUniNFeTXMulNFeCab.Value = 0) then
        begin
          Remistura := 0; // False
          CodRemix  := 0;
          NFeSer    := QrUniNFeNFeSer.Value;
          NFeNum    := QrUniNFeNFeNum.Value;
          Qtde     := QrUniNFeQtde.Value;
          ReInsereNovoTXNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum, Qtde, True);
        end else
        if (QrUniNFeNFeNum.Value = 0) and (QrUniNFeTXMulNFeCab.Value <> 0) then
        begin
          Remistura := 1; // True
          CodRemix  := QrUniNFeTXMulNFeCab.Value;
          //
          UnDMkDAC_PF.AbreMySQLQuery0(QrMulNFe, Dmod.MyDB, [
          'SELECT MAX(NFeSer) NFeSer, NFeNum, SUM(Qtde) Qtde',
          'FROM vsmulnfeits',
          'WHERE Codigo=' + Geral.FF0(CodRemix),
          'GROUP BY NFeNum ',
          '']);
          TotMul := 0;
          QrMulNFe.First;
          while not QrMulNFe.Eof do
          begin
            TotMul := TotMul + QrMulNFeQtde.Value;
            //
            QrMulNFe.Next;
          end;
          QrMulNFe.First;
          while not QrMulNFe.Eof do
          begin
            //Qtde     := QrMulNFeQtde.Value;
            if TotMul <> 0 then
              Qtde := (QrMulNFeQtde.Value / TotMul) * QrUniNFeQtde.Value
            else
              Qtde := 0;
            //
            NFeSer   := QrMulNFeNFeSer.Value;
            NFeNum   := QrMulNFeNFeNum.Value;
            ReInsereNovoTXNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum, Qtde, False);
            //
            QrMulNFe.Next;
          end;
        end;
        //
        QrUniNFe.Next;
      end;
      // Atualiza TXMulNFeCab!
      UnDMkDAC_PF.AbreMySQLQuery0(QrMulNFe, Dmod.MyDB, [
      'SELECT mfi.NFeSer, mfi.NFeNum, ',
      'SUM(mfi.Qtde) Qtde ',
      'FROM vsmulnfeits mfi',
      'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
      'GROUP BY mfi.NFeNum',
      'ORDER BY Qtde DESC',
      '']);
      UnDMkDAC_PF.AbreMySQLQuery0(QrTotNFe, Dmod.MyDB, [
      'SELECT SUM(mfi.Qtde) Qtde ',
      'FROM vsmulnfeits mfi',
      'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
      '']);
      Nome := '';
      while not QrMulNFe.Eof do
      begin
        SiglaNFe := TX_PF.DefineSiglaTX_NFe(QrMulNFeNFeSer.Value, QrMulNFeNFeNum.Value);
        //
        if QrTotNFeQtde.Value <> 0 then
        begin
          PerQtde   := QrMulNFeQtde.Value / QrTotNFeQtde.Value * 100;
          Percentual := Geral.FI64(Round(PerQtde));
        end else
        begin
          PerQtde   := 0;
          Percentual := '??';
        end;
        //
        Nome := Nome + SiglaNFe + ' ' + Percentual + '% ';
        //
        QrMulNFe.Next;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulnfecab', False, [
      'Nome'], ['Codigo'], [
      Nome], [Codigo], True) then
        TXMulNFeCab := Codigo;
    end;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_UPD_TAB_TMI,
  'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
  'NFeNum=' + Geral.FF0(NFeNum) + ', ',
  'TXMulNFeCab=' + Geral.FF0(TXMulNFeCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_SEL_TAB_TMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
//     SQLMovimID,
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle');
  //
  if Trim(sSrcNivel2) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
    'SELECT MovimID, MovimNiv, MovimCod, Controle ',
    'FROM ' + CO_SEL_TAB_TMI,
    'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
    '']);
    QrMFSrc.First;
    while not QrMFSrc.Eof do
    begin
      case TEstqMovimID(QrMFSrcMovimID.Value) of
        (*6*)emidInds V S:
        begin
          UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_UPD_TAB_TMI,
          'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
          'NFeNum=' + Geral.FF0(NFeNum) + ', ',
          'TXMulNFeCab=' + Geral.FF0(TXMulNFeCab),
          'WHERE MovimCod=' + Geral.FF0(QrMFSrcMovimCod.Value),
          '']);
        end;
        else
        begin
          UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_UPD_TAB_TMI,
          'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
          'NFeNum=' + Geral.FF0(NFeNum) + ', ',
          'TXMulNFeCab=' + Geral.FF0(TXMulNFeCab),
          'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
          '']);
        end;
      end;
      //
      QrMFSrc.Next;
    end;
  end;
}
end;

procedure TDmModTX_CRC.DataModuleCreate(Sender: TObject);
begin
  DqAux.Database := DModG.MyCompressDB;
end;

procedure TDmModTX_CRC.DistribuicaoCusto(EstqMovimID: TEstqMovimID;
  eminSrc, eminInn, eminDst: TEstqMovimNiv);
  function ObtemQtde(MovimNiv: TEstqMovimNiv): Double;
  begin
    UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqSum, DModG.MyCompressDB, [
    'SELECT SUM(Qtde) Qtde  ',
    'FROM txmovits ',
    'WHERE MovimID=' + Geral.FF0(Integer(EstqMovimID)), //emidEmIndstrlzc = 38
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)), //62 ou 63 ',
    '']);
    //
    Result := USQLDB.Flu(DqSum, 'Qtde');
  end;
  //
  procedure AtualizaCusto(ValMPUni: Double; MovimNiv: TEstqMovimNiv);
  var
    ValTXT: String;
  begin
    ValTXT := Geral.FFT_Dot(ValMPUni, 6, siNegativo);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE txmovits ',
    'SET CustoMOTot=Qtde*CustoMOUni, ',
    'ValorMP=Qtde*' + ValTXT + ', ',
    'ValorT=Qtde*(CustoMOUni + ' + ValTXT + ') ',
    'WHERE MovimID=' + Geral.FF0(Integer(EstqMovimID)), //emidEmIndstrlzc = 38
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)), //62 ou 63 ',
    '']);
  end;
var
  ValorMP, Qtde, ValMPUni: Double;
begin
  UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqSum, DModG.MyCompressDB, [
  'SELECT SUM(ValorT) ValorT  ',
  'FROM txmovits ',
  'WHERE MovimID=' + Geral.FF0(Integer(EstqMovimID)), //emidEmIndstrlzc = 38
  'AND MovimNiv=' + Geral.FF0(Integer(eminSrc)), //61 ',
  '']);
  ValorMP := -USQLDB.Flu(DqSum, 'ValorT');
  //
  Qtde := ObtemQtde(eminInn);
  //
  if Qtde > 0 then
  begin
    ValMPUni := ValorMP / Qtde;
    //
    AtualizaCusto(ValMPUni, eminInn);
  end else
  begin
    Qtde := ObtemQtde(eminDst);
    //
    if Qtde > 0 then
    begin
      ValMPUni := ValorMP / Qtde;
      //
      AtualizaCusto(ValMPUni, eminDst);
    end;
  end;
end;

function TDmModTX_CRC.EncerraPallet(Pallet: Integer;
  Pergunta: Boolean): Boolean;
var
  TMI_Dest, TMI_Baix, TMI_Sorc: Integer;
  TXPaXXXIts, Codigo: Integer;
  DtHrFim, TabCab, TabIts, Campo: String;
begin
  Result := False;
  //
  if (not Pergunta)
  or (Geral.MB_Pergunta('Deseja realmente encerrar o pallet ' +
  Geral.FF0(Pallet) +  ' ?') = ID_YES) then
  begin
    DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
    UnDmkDAC_PF.AbreMySQLQuery0(QrBoxesPal, Dmod.MyDB, [
    'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle TXPaXXXIts, ',
    '0.000 TXPallet, pri.TMI_Dest, pri.TMI_Sorc, pri.TMI_Baix, ',
    'pri.Tecla, 7.000 CacID ',
    'FROM txpaclaitsa pri  ',
    'LEFT JOIN txpaclacaba prc ON prc.Codigo=pri.Codigo ',
    'LEFT JOIN txcacitsa cia ON pri.Controle=cia.TXPaRclIts ',
    'WHERE pri.TXPallet=' + Geral.FF0(Pallet),
    ' ',
    'UNION ',
    ' ',
    'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle TXPaXXXIts, ',
    'prc.TXPallet + 0.000 TXPallet, ',
    'pri.TMI_Dest, pri.TMI_Sorc, pri.TMI_Baix, ',
    'pri.Tecla, 8.000 CacID ',
    'FROM txparclitsa pri  ',
    'LEFT JOIN txparclcaba prc ON prc.Codigo=pri.Codigo ',
    'LEFT JOIN txcacitsa cia ON pri.Controle=cia.TXPaRclIts ',
    'WHERE pri.TXPallet=' + Geral.FF0(Pallet),
    '']);
    //
    QrBoxesPal.First;
    while not QrBoxesPal.Eof do
    begin
      TMI_Dest := QrBoxesPalTMI_Dest.Value;
      TMI_Baix := QrBoxesPalTMI_Baix.Value;
      TMI_Sorc := QrBoxesPalTMI_Sorc.Value;
      TX_PF.AtualizaTMIsDeBox(Pallet, TMI_Dest, TMI_Baix, TMI_Sorc,
        QrSumDest1, QrSumSorc1, QrTMISorc1, QrPalSorc1);
      //
      TXPaXXXIts := QrBoxesPalTXPaXXXIts.Value;
      case TEstqMovimID(Trunc(QrBoxesPalCacID.Value)) of
        (*7*)emidClassArtXXUni:
        begin
          TabCab := 'txpaclacaba';
          TabIts := 'txpaclaitsa';
        end;
        (*8*)emidReclasXXUni:
        begin
          TabCab := 'txparclcaba';
          TabIts := 'txparclitsa';
        end;
        //(*14*)emidClassArtXXMul: ;// ????? Precisa???

         else
         begin
           TabCab := '';
           TabIts := '';
           Geral.MB_Erro('"MovimID" n�o implementado em "EncerraItensDePallet"');
         end;
      end;
      if TabIts <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabIts, False, [
        'DtHrFim'], ['Controle'], [DtHrFim], [TXPaXXXIts], True);
        //
        Campo    := 'LstPal' + FormatFloat('00', QrBoxesPalTecla.Value);
        Codigo   := QrBoxesPalCodigo.Value;
        //
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabCab, False, [
        Campo], ['Codigo', Campo], [
        0], [Codigo, Pallet], True);
      end;
      //
      QrBoxesPal.Next;
    end;

    //////
    Codigo := Pallet;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txpalleta', False, [
    'DtHrEndAdd'], ['Codigo'], [DtHrFim], [Codigo], True) then
    begin
      TX_PF.AtualizaStatPall(Pallet);
      Result := True;
    end;
  end;
end;

procedure TDmModTX_CRC.InformaSemFornecedor;
begin
  if VAR_TXWarNoFrn = 1 then
    Geral.MB_Info('Defini��o de fornecedor abortada!' + sLineBreak +
    'Existe pelo menos um item de origem sem fornecedor definido!');
end;

procedure TDmModTX_CRC.InformaSemNFe;
begin
  if VAR_TXWarSemNF = 1 then
    Geral.MB_Info('Defini��o de NFe abortada!' + sLineBreak +
    'Existe pelo menos um item de origem sem NFe definida!');
end;

function TDmModTX_CRC.ObtemSrcNivel2DeIMEI(IMEI: Integer): Integer;
begin
  Result := 0;
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAu2, DModG.MyCompressDB, [
  'SELECT SrcNivel2 ',
  'FROM ' + CO_SEL_TAB_TMI,
  'WHERE Controle=' + Geral.FF0(Integer(IMEI)),
  '']);
  if DqAu2.RecordCount = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAu2, DModG.MyCompressDB, [
    'SELECT SrcNivel2 ',
    'FROM ' + CO_TAB_TMB,
    'WHERE Controle=' + Geral.FF0(Integer(IMEI)),
    '']);
  end;
  //
  Result := USQLDB.Int(DqAu2, 'SrcNivel2');
  if Result = 0 then
    Geral.MB_Aviso('SrcNivel2 n�o localizado para IMEI ' + Geral.FF0(IMEI));
end;

end.
