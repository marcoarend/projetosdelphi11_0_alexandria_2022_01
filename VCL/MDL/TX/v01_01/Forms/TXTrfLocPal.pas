unit TXTrfLocPal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  Vcl.Mask, dmkDBEdit, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnProjGroup_Consts, UnAppEnums, UnGrl_Vars;

type
  TFmTXTrfLocPal = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBG04Estq: TdmkDBGridZTO;
    QrEstqR4: TmySQLQuery;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Qtde: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Terceiro: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    DsEstqR4: TDataSource;
    Panel7: TPanel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    QrTXMovIts: TmySQLQuery;
    QrTXMovItsNO_Pallet: TWideStringField;
    QrTXMovItsCodigo: TIntegerField;
    QrTXMovItsControle: TIntegerField;
    QrTXMovItsMovimCod: TIntegerField;
    QrTXMovItsMovimNiv: TIntegerField;
    QrTXMovItsEmpresa: TIntegerField;
    QrTXMovItsTerceiro: TIntegerField;
    QrTXMovItsMovimID: TIntegerField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TIntegerField;
    QrTXMovItsGraGruX: TIntegerField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsSrcMovID: TIntegerField;
    QrTXMovItsSrcNivel1: TIntegerField;
    QrTXMovItsSrcNivel2: TIntegerField;
    QrTXMovItsLk: TIntegerField;
    QrTXMovItsDataCad: TDateField;
    QrTXMovItsDataAlt: TDateField;
    QrTXMovItsUserCad: TIntegerField;
    QrTXMovItsUserAlt: TIntegerField;
    QrTXMovItsAlterWeb: TSmallintField;
    QrTXMovItsAtivo: TSmallintField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsNO_FORNECE: TWideStringField;
    QrTXMovItsValorT: TFloatField;
    QrTXMovItsTalao: TIntegerField;
    QrTXMovItsSerieTal: TIntegerField;
    QrTXMovItsMovimTwn: TIntegerField;
    QrTXMovItsCliVenda: TIntegerField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsFornecMO: TIntegerField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsCustoMOTot: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TIntegerField;
    QrTXMovItsDstNivel1: TIntegerField;
    QrTXMovItsDstNivel2: TIntegerField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsAptoUso: TSmallintField;
    QrTXMovItsSrcGGX: TIntegerField;
    QrTXMovItsDstGGX: TIntegerField;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsGraGruY: TIntegerField;
    QrEstqR4SdoVrtQtd: TFloatField;
    QrTXMovItsPedItsLib: TIntegerField;
    QrTXMovItsPedItsFin: TIntegerField;
    QrTXMovItsPedItsVda: TIntegerField;
    QrEstqR4DataHora: TDateTimeField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    LaFicha: TLabel;
    Label11: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    QrTXMovItsTXMulFrnCab: TIntegerField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel10: TPanel;
    Label49: TLabel;
    Label50: TLabel;
    Label16: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    EdObserv: TdmkEdit;
    QrTXMovItsStqCenLoc: TIntegerField;
    QrTXMovItsClientMO: TIntegerField;
    EdItemNFe: TdmkEdit;
    LaItemNFe: TLabel;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdStqCenLocSrc: TdmkEditCB;
    CBStqCenLocSrc: TdmkDBLookupComboBox;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    QrEstqR4NFeSer: TSmallintField;
    QrEstqR4NFeNum: TIntegerField;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    Label38: TLabel;
    CBCouNiv2: TdmkDBLookupComboBox;
    Label39: TLabel;
    CBCouNiv1: TdmkDBLookupComboBox;
    EdCouNiv2: TdmkEditCB;
    EdCouNiv1: TdmkEditCB;
    EdTerceiro: TdmkEditCB;
    LaTerceiro: TLabel;
    CBTerceiro: TdmkDBLookupComboBox;
    QrEstqR4NO_LOC_CEN: TWideStringField;
    QrStqCenLocEntiSitio: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
  private
    { Private declarations }
    FTXMovImp4(*, FTXMovImp5*): String;
    procedure AdicionaPallet();
    procedure InsereIMEI_Atual();
    procedure ReopenTXMovIts();

  public
    { Public declarations }
    FEmpresa, FClientMO: Integer;
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure PesquisaPallets();
  end;

  var
  FmTXTrfLocPal: TFmTXTrfLocPal;

implementation

uses UnMyObjects, Module, ModuleGeral, TXTrfLocCab, UMySQLModule,
  DmkDAC_PF, UnTX_PF;

{$R *.DFM}

procedure TFmTXTrfLocPal.AdicionaPallet();
begin
  ReopenTXMovIts();
  QrTXMovIts.First;
  while not QrTXMovIts.Eof do
  begin
    InsereIMEI_Atual();
    QrTXMovIts.Next;
  end;
end;

procedure TFmTXTrfLocPal.BtOKClick(Sender: TObject);
var
  N, I: Integer;
begin
  if MyObjects.FIC(EdStqCenLoc.ValueVariant = 0, EdStqCenLoc,
  'Informe o local/centro de estoque') then
    Exit;
  //
  if MyObjects.FIC(EdItemNFe.ValueVariant = 0, EdItemNFe,
  'Informe o item da NFe') then
    Exit;
  //
  QrEstqR4.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      AdicionaPallet();
    end;
    if N > 0 then
      Close;
  finally
    QrEstqR4.EnableControls;
  end;
  FmTXTrfLocCab.AtualizaNFeItens();
end;

procedure TFmTXTrfLocPal.BtReabreClick(Sender: TObject);
begin
  PesquisaPallets();
end;

procedure TFmTXTrfLocPal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXTrfLocPal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
end;

procedure TFmTXTrfLocPal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TX_PF.AbreTXSerTal(QrTXSerTal);
  TX_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GraGruY <> 0');
  TX_PF.AbreGraGruXY(QrGGXRcl, ''); //'AND ggx.GraGruY <> 0');
  //
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
end;

procedure TFmTXTrfLocPal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXTrfLocPal.InsereIMEI_Atual();
const
  ExigeFornecedor = False;
  EdFicha         = nil;
  CustoMOUni      = 0;
  CustoMOM2       = 0;
  CustoMOTot      = 0;
  QtdGer          = 0;
  AptoUso         = 0; // Eh venda!
  //
  TpCalcAuto      = -1;
  //
  PedItsLib       = 0;
  PedItsFin       = 0;
  PedItsVda       = 0;
  //
  GSPSrcMovID     = TEstqMovimID(0);
  GSPSrcNiv2      = 0;
  //
  //
  QtdAnt          = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2, Pallet, Codigo, CtrlSorc, CtrlDest, MovimCod,
  Empresa, GraGruX, Terceiro, CliVenda, SerieTal, Talao,
  DstNivel1, DstNivel2, GraGruY, SrcGGX, DstGGX, ItemNFe, TXMulFrnCab,
  ClientMO: Integer;
  Qtde, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
  //
  FornecMO, ReqMovEstq, StqCenLoc, MovimTwn, GGXRcl: Integer;
  Observ: String;
begin
  FornecMO       := QrStqCenLocEntiSitio.Value;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  Observ         := EdObserv.Text;
  //
  SrcMovID       := 0;
  SrcNivel1      := 0;
  SrcNivel2      := 0;
  SrcGGX         := 0;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  CtrlSorc       := 0; //EdControle.ValueVariant;
  CtrlDest       := 0; //EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrTXMovItsClientMO.Value;
  Terceiro       := QrTXMovItsTerceiro.Value;
  TXMulFrnCab    := QrTXMovItsTXMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidTransfLoc;
  MovimNiv       := eminDestLocal;
  Pallet         := QrTXMovItsPallet.Value; //EdPallet.ValueVariant;
  GraGruX        := QrTXMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not TX_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  Qtde           := QrTXMovItsSdoVrtQtd.Value;
  Valor          := 0;
  if QrTXMovItsQtde.Value > 0 then
    Valor := QrTXMovItsSdoVrtQtd.Value *
    (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value);
  ValorMP        := Valor;
  ValorT         := ValorMP;
  //
  SerieTal       := 0; //QrTXMovItsSerieTal.Value;
  Talao          := 0;  //QrTXMovItsTalao.Value;
  Marca          := ''; //QrTXMovItsMarca.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrTXMovItsGraGruY.Value;
  //
  ItemNFe        := EdItemNFe.ValueVariant;
  //
(*
  if TX_PF.TXFic(...) then
    Exit;
*)
  //
  CtrlSorc := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlSorc);
  CtrlDest := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlDest);
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  //
  // Geração!!!
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, (*Controle*) CtrlDest,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGer, AptoUso, FornecMO,
  SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei079(*Geração de material no destino em transferência de estoque por pallet*)) then
  begin
    // Baixa!!!
    ReqMovEstq     := 0;
    StqCenLoc      := QrTXMovItsStqCenLoc.Value;
    Observ         := '';
    SrcMovID       := QrTXMovItsMovimID.Value;
    SrcNivel1      := QrTXMovItsCodigo.Value;
    SrcNivel2      := QrTXMovItsControle.Value;
    SrcGGX         := QrTXMovItsGraGruX.Value;
    //
    MovimNiv       := eminSorcLocal;
    Qtde           := -Qtde;
    ValorT         := -ValorT;
    //
    DstMovID       := TEstqMovimID(0);
    DstNivel1      := 0;
    DstNivel2      := 0;
    DstGGX         := 0;
    //
    if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
    Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
    ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
    Observ, CliVenda, (*Controle*) CtrlSorc,
    CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
    QtdGer, AptoUso, FornecMO,
    SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
    ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
    QtdAnt, SerieTal, Talao,
    CO_0_GGXRcl,
    CO_0_MovCodPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iutpei080(*Baixa de material da origem em transferência de estoque por pallet*)) then
    begin
      TX_PF.AtualizaSaldoIMEI(SrcNivel2, False);
      TX_PF.AtualizaSaldoIMEI(CtrlDest, False);
      TX_PF.AtualizaTotaisTXXxxCab('txtrfloccab', MovimCod);
      FmTXTrfLocCab.LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmTXTrfLocPal.PesquisaPallets();
const
  SQL_Especificos = '';
  Pallet    = 0;
  Terceiro  = 0;
  StqCenLoc = 0;
  GraGruYs  = '';
  MovimID   = 0;
  MovimCod  = 0;
var
  GraGruX: TPallArr;
  StqCenCad, CouNiv1, CouNiv2: Integer;
begin
  StqCenCad := EdStqCenLocSrc.ValueVariant;
  TX_PF.SetaGGXUnicoEmLista(EdGraGruX.ValueVariant, GraGruX);
  CouNiv2 := EdCouNiv2.ValueVariant;
  CouNiv1 := EdCouNiv1.ValueVariant;
  //
  if TX_PF.PesquisaPallets(FEmpresa, FClientMO, CouNiv2, CouNiv1, StqCenCad,
  StqCenLoc, Self.Name, GraGruYs, GraGruX, [],   SQL_Especificos, TEstqMovimID(
  MovimID), MovimCod, FTXMovImp4)
  then
    TX_PF.ReopenTXListaPallets(QrEstqR4, FTXMovImp4,
    FEmpresa, EdGraGruX.ValueVariant, Pallet, Terceiro, '');
end;

procedure TFmTXTrfLocPal.ReopenTXMovIts();
begin
  TX_PF.ReopenTXMovIts_Pallet2(QrTXMovIts, QrEstqR4);
end;

end.
