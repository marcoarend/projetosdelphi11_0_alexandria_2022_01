unit TXPallet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet,
  Vcl.ComCtrls, dmkEditDateTimePicker, System.Variants, dmkDBGridZTO,
  UnProjGroup_Consts, UnTX_Tabs, UnGrl_Consts, UnProjGroup_Vars, UnAppEnums;

type
  TFmTXPallet = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrTXPallet: TmySQLQuery;
    DsTXPallet: TDataSource;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIMEI: TBitBtn;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletNome: TWideStringField;
    QrTXPalletLk: TIntegerField;
    QrTXPalletDataCad: TDateField;
    QrTXPalletDataAlt: TDateField;
    QrTXPalletUserCad: TIntegerField;
    QrTXPalletUserAlt: TIntegerField;
    QrTXPalletAlterWeb: TSmallintField;
    QrTXPalletAtivo: TSmallintField;
    QrTXPalletEmpresa: TIntegerField;
    QrTXPalletNO_EMPRESA: TWideStringField;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    EdCliStat: TdmkEditCB;
    CBCliStat: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    SBCliente: TSpeedButton;
    LaTXRibCla: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrTXPalletStatus: TIntegerField;
    QrTXPalletCliStat: TIntegerField;
    QrTXPalletGraGruX: TIntegerField;
    QrTXPalletNO_CLISTAT: TWideStringField;
    QrTXPalletNO_PRD_TAM_COR: TWideStringField;
    QrTXPalletNO_STATUS: TWideStringField;
    Label4: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label12: TLabel;
    TPDtHrEndAdd: TdmkEditDateTimePicker;
    EdDtHrEndAdd: TdmkEdit;
    QrTXPalletDtHrEndAdd: TDateTimeField;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    QrTXPalletDtHrEndAdd_TXT: TWideStringField;
    QrSumPall: TmySQLQuery;
    QrIMEIs: TmySQLQuery;
    DsSumPall: TDataSource;
    DsIMEIs: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrSumPallGraGruX: TIntegerField;
    QrSumPallQtde: TFloatField;
    QrSumPallValorT: TFloatField;
    QrSumPallSdoVrtQtd: TFloatField;
    QrSumPallGraGru1: TIntegerField;
    QrSumPallNO_PRD_TAM_COR: TWideStringField;
    QrSumPallDataHora: TDateTimeField;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label15: TLabel;
    DBEdit12: TDBEdit;
    Label19: TLabel;
    DBEdit16: TDBEdit;
    Label20: TLabel;
    DBEdit17: TDBEdit;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    QrTXCacItsAOpn: TmySQLQuery;
    QrTXCacItsAOpnCacCod: TIntegerField;
    QrTXCacItsAOpnCacID: TIntegerField;
    QrTXCacItsAOpnCodigo: TIntegerField;
    QrTXCacItsAOpnControle: TLargeintField;
    QrTXCacItsAOpnClaAPalOri: TIntegerField;
    QrTXCacItsAOpnRclAPalOri: TIntegerField;
    QrTXCacItsAOpnTXPaClaIts: TIntegerField;
    QrTXCacItsAOpnTXPaRclIts: TIntegerField;
    QrTXCacItsAOpnTXPallet: TIntegerField;
    QrTXCacItsAOpnTMI_Sorc: TIntegerField;
    QrTXCacItsAOpnTMI_Dest: TIntegerField;
    QrTXCacItsAOpnBox: TIntegerField;
    QrTXCacItsAOpnQtde: TFloatField;
    QrTXCacItsAOpnRevisor: TIntegerField;
    QrTXCacItsAOpnDigitador: TIntegerField;
    QrTXCacItsAOpnDataHora: TDateTimeField;
    QrTXCacItsAOpnAlterWeb: TSmallintField;
    QrTXCacItsAOpnAtivo: TSmallintField;
    QrTXCacItsAOpnSumido: TSmallintField;
    QrTXCacItsAOpnRclAPalDst: TIntegerField;
    DsTXCacItsAOpn: TDataSource;
    DBGrid2: TDBGrid;
    N1: TMenuItem;
    Encerraclassificao1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    Label23: TLabel;
    EdQtdPrevPc: TdmkEdit;
    Label24: TLabel;
    QrTXPalletQtdPrevPc: TIntegerField;
    QrTXPalletGerRclCab: TIntegerField;
    QrTXPalletDtHrFimRcl: TDateTimeField;
    QrTXPalletMovimIDGer: TIntegerField;
    N2: TMenuItem;
    Atualizasaldo1: TMenuItem;
    DBGTXPaIts: TdmkDBGridZTO;
    QrTXPaIts: TmySQLQuery;
    DsTXPaIts: TDataSource;
    QrTXPaItsTXPallet: TIntegerField;
    QrTXPaItsTXPaClaIts: TIntegerField;
    QrTXPaItsTXPaRclIts: TIntegerField;
    QrTXPaItsTMI_Sorc: TIntegerField;
    QrTXPaItsTMI_Dest: TIntegerField;
    QrTXPaItsQtde: TFloatField;
    QrTXPaItens: TmySQLQuery;
    DsTXPaItens: TDataSource;
    QrTXPaItensCodigo: TIntegerField;
    QrTXPaItensControle: TIntegerField;
    QrTXPaItensTXPallet: TIntegerField;
    QrTXPaItensTMI_Sorc: TIntegerField;
    QrTXPaItensTMI_Baix: TIntegerField;
    QrTXPaItensTecla: TIntegerField;
    QrTXPaItensDtHrIni: TDateTimeField;
    QrTXPaItensDtHrFim: TDateTimeField;
    QrTXPaItensQIt_Sorc: TIntegerField;
    QrTXPaItensQIt_Baix: TIntegerField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrTXPaItensTMI_Dest: TIntegerField;
    PMIMEI: TPopupMenu;
    Irparajaneladomovimento1: TMenuItem;
    IrparajaneladedadosdoIMEI1: TMenuItem;
    N3: TMenuItem;
    ExcluiIMEIselecionado1: TMenuItem;
    TabSheet5: TTabSheet;
    GroupBox1: TGroupBox;
    Splitter1: TSplitter;
    GroupBox2: TGroupBox;
    dmkDBGridZTO2: TdmkDBGridZTO;
    QrOriRcl: TmySQLQuery;
    GroupBox3: TGroupBox;
    dmkDBGridZTO3: TdmkDBGridZTO;
    Splitter2: TSplitter;
    GroupBox4: TGroupBox;
    dmkDBGridZTO4: TdmkDBGridZTO;
    DsOriRcl: TDataSource;
    QrOriCla: TmySQLQuery;
    DsOriCla: TDataSource;
    QrDestino: TmySQLQuery;
    DsDestino: TDataSource;
    Panel6: TPanel;
    Label25: TLabel;
    EdDestino: TdmkEdit;
    Panel7: TPanel;
    Label26: TLabel;
    EdOriCla: TdmkEdit;
    Panel8: TPanel;
    Label27: TLabel;
    EdOriRcl: TdmkEdit;
    QrPallDst: TmySQLQuery;
    QrPallDstTXPallet: TIntegerField;
    QrPallDstQtde: TFloatField;
    QrPallDstTMI_Dest: TIntegerField;
    QrPallDstDtHrEndAdd: TDateTimeField;
    QrPallDstDtHrEndAdd_TXT: TWideStringField;
    DsPallDst: TDataSource;
    QrPallOri: TmySQLQuery;
    DsPallOri: TDataSource;
    QrPallOriClaRcl: TFloatField;
    QrPallOriNO_ClaRcl: TWideStringField;
    QrPallOriCacCod: TIntegerField;
    QrPallOriCodigo: TIntegerField;
    QrPallOriQtde: TFloatField;
    QrPallOriTXMovIts: TIntegerField;
    QrPallOriTXPallet: TFloatField;
    QrPallOriDtHrFimCla: TDateTimeField;
    QrPallOriDtHrFimCla_TXT: TWideStringField;
    GroupBox5: TGroupBox;
    dmkDBGridZTO5: TdmkDBGridZTO;
    GroupBox6: TGroupBox;
    dmkDBGridZTO6: TdmkDBGridZTO;
    AlteraIMEIorigemdoIMEISelecionado1: TMenuItem;
    EncerraPallet1: TMenuItem;
    Atualizasaldo2: TMenuItem;
    AtualizaStatus1: TMenuItem;
    QrTXCacItsAOpnTMI_Baix: TIntegerField;
    QrTXCacItsAOpnMartelo: TIntegerField;
    QrTXCacItsAOpnFrmaIns: TSmallintField;
    QrTXCacItsAOpnSubClass: TWideStringField;
    RecalculaIMEIsdeclassereclasse1: TMenuItem;
    IMEIsdegerao1: TMenuItem;
    PMCacIts: TPopupMenu;
    Alteradadosdiitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    QrSumTMI: TmySQLQuery;
    QrSumTMIQtde: TFloatField;
    PB1: TProgressBar;
    PMImprime: TPopupMenu;
    FichaCOMonomedoPallet1: TMenuItem;
    FichaSEMonomedoPallet1: TMenuItem;
    AlteraFornecedor1: TMenuItem;
    QrIMEIsCodigo: TLargeintField;
    QrIMEIsControle: TLargeintField;
    QrIMEIsMovimCod: TLargeintField;
    QrIMEIsMovimNiv: TLargeintField;
    QrIMEIsMovimTwn: TLargeintField;
    QrIMEIsEmpresa: TLargeintField;
    QrIMEIsTerceiro: TLargeintField;
    QrIMEIsCliVenda: TLargeintField;
    QrIMEIsMovimID: TLargeintField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsPallet: TLargeintField;
    QrIMEIsGraGruX: TLargeintField;
    QrIMEIsQtde: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSrcMovID: TLargeintField;
    QrIMEIsSrcNivel1: TLargeintField;
    QrIMEIsSrcNivel2: TLargeintField;
    QrIMEIsSrcGGX: TLargeintField;
    QrIMEIsSdoVrtQtd: TFloatField;
    QrIMEIsObserv: TWideStringField;
    QrIMEIsFornecMO: TLargeintField;
    QrIMEIsCustoMOUni: TFloatField;
    QrIMEIsCustoMOTot: TFloatField;
    QrIMEIsValorMP: TFloatField;
    QrIMEIsDstMovID: TLargeintField;
    QrIMEIsDstNivel1: TLargeintField;
    QrIMEIsDstNivel2: TLargeintField;
    QrIMEIsDstGGX: TLargeintField;
    QrIMEIsQtdGer: TFloatField;
    QrIMEIsQtdAnt: TFloatField;
    QrIMEIsStqCenLoc: TLargeintField;
    QrIMEIsReqMovEstq: TLargeintField;
    QrIMEIsGraGru1: TLargeintField;
    QrIMEIsNO_EstqMovimID: TWideStringField;
    QrIMEIsNO_MovimID: TWideStringField;
    QrIMEIsNO_MovimNiv: TWideStringField;
    QrIMEIsNO_PALLET: TWideStringField;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsNO_FORNECE: TWideStringField;
    QrIMEIsNO_TTW: TWideStringField;
    QrIMEIsID_TTW: TLargeintField;
    QrIMEIsNO_LOC_CEN: TWideStringField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrTXPalletClientMO: TIntegerField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    udo1: TMenuItem;
    ApenasaDescrio1: TMenuItem;
    frxDsTXCacItsAOpnImp: TfrxDBDataset;
    frxWET_CURTI_013_A: TfrxReport;
    N4: TMenuItem;
    Itensnopallet1: TMenuItem;
    frxDsTXPallet: TfrxDBDataset;
    QrTXCacItsAOpnImp: TmySQLQuery;
    QrTXCacItsAOpnImpCacCod: TIntegerField;
    QrTXCacItsAOpnImpCacID: TIntegerField;
    QrTXCacItsAOpnImpCodigo: TIntegerField;
    QrTXCacItsAOpnImpControle: TLargeintField;
    QrTXCacItsAOpnImpClaAPalOri: TIntegerField;
    QrTXCacItsAOpnImpRclAPalOri: TIntegerField;
    QrTXCacItsAOpnImpTXPaClaIts: TIntegerField;
    QrTXCacItsAOpnImpTXPaRclIts: TIntegerField;
    QrTXCacItsAOpnImpTXPallet: TIntegerField;
    QrTXCacItsAOpnImpTMI_Sorc: TIntegerField;
    QrTXCacItsAOpnImpTMI_Dest: TIntegerField;
    QrTXCacItsAOpnImpBox: TIntegerField;
    QrTXCacItsAOpnImpQtde: TFloatField;
    QrTXCacItsAOpnImpRevisor: TIntegerField;
    QrTXCacItsAOpnImpDigitador: TIntegerField;
    QrTXCacItsAOpnImpDataHora: TDateTimeField;
    QrTXCacItsAOpnImpAlterWeb: TSmallintField;
    QrTXCacItsAOpnImpAtivo: TSmallintField;
    QrTXCacItsAOpnImpSumido: TSmallintField;
    QrTXCacItsAOpnImpRclAPalDst: TIntegerField;
    QrTXCacItsAOpnImpTMI_Baix: TIntegerField;
    QrTXCacItsAOpnImpMartelo: TIntegerField;
    QrTXCacItsAOpnImpFrmaIns: TSmallintField;
    QrTXCacItsAOpnImpSubClass: TWideStringField;
    QrTXCacItsAOpnImpNoRev: TWideStringField;
    QrTXCacItsAOpnImpNoDig: TWideStringField;
    IncluiManualmente1: TMenuItem;
    Qr1: TmySQLQuery;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXPalletAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXPalletBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXPalletAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtIMEIClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrTXPalletBeforeClose(DataSet: TDataSet);
    procedure SBClienteClick(Sender: TObject);
    procedure QrTXPalletCalcFields(DataSet: TDataSet);
    procedure Encerraclassificao1Click(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure Atualizasaldo1Click(Sender: TObject);
    procedure QrTXPaItsAfterScroll(DataSet: TDataSet);
    procedure QrTXPaItsBeforeClose(DataSet: TDataSet);
    procedure Irparajaneladomovimento1Click(Sender: TObject);
    procedure PMIMEIPopup(Sender: TObject);
    procedure IrparajaneladedadosdoIMEI1Click(Sender: TObject);
    procedure ExcluiIMEIselecionado1Click(Sender: TObject);
    procedure AlteraIMEIorigemdoIMEISelecionado1Click(Sender: TObject);
    procedure EncerraPallet1Click(Sender: TObject);
    procedure Atualizasaldo2Click(Sender: TObject);
    procedure AtualizaStatus1Click(Sender: TObject);
    procedure IMEIsdegerao1Click(Sender: TObject);
    procedure PMCacItsPopup(Sender: TObject);
    procedure Alteradadosdiitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure FichaCOMonomedoPallet1Click(Sender: TObject);
    procedure FichaSEMonomedoPallet1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure AlteraFornecedor1Click(Sender: TObject);
    procedure ApenasaDescrio1Click(Sender: TObject);
    procedure udo1Click(Sender: TObject);
    procedure Itensnopallet1Click(Sender: TObject);
    procedure frxWET_CURTI_013_AGetValue(const VarName: string;
      var Value: Variant);
    procedure PageControl1Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure IncluiManualmente1Click(Sender: TObject);
  private
    FAtualizando: Boolean;
    procedure AtualizaStatPallDeTodosPallets();
    procedure AtualizaIMEIsDeGeracaoDeTodosPallets();
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    //procedure EncerraPalletCla();
    //procedure EncerraPalletRcl();
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenCourosEmClassificacao(Controle: Int64);
    procedure ReopenClassificacoes();
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType);
    //procedure AtualizaSaldoOrigemTMI_Dest(TMI_Dest, TMI_Sorc: Integer);
    procedure ImprimePallet(InfoNO_Pallet: Boolean);
    procedure ReabreQuerys(Tab: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    //procedure ReopenGraGruX();
    procedure ReopenSumPall();
    procedure ReopenIMEIs(Controle: Integer);
    procedure ReopenTXPaIts();
    procedure ReopenTXPaItens();

  end;

var
  FmTXPallet: TFmTXPallet;
  FmTXPallet1: TFmTXPallet;

const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnTX_Jan, ModuleGeral, MyListas,
  UnTX_PF, AppListas, GetValor;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXPallet.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXPallet.ReabreQuerys(Tab: Integer);
begin
  case Tab of
   //-1: ReopenSumPall();
    0: ReopenIMEIs(0);
    1: ReopenTXPaIts();
    2: ReopenCourosEmClassificacao(0);
    3: ReopenClassificacoes();
    4: ReopenClassificacoes();
    else
    begin
      ReopenIMEIs(0);
      ReopenTXPaIts();
      ReopenCourosEmClassificacao(0);
      ReopenClassificacoes();
      ReopenClassificacoes();
      //ReopenTXPalArt(0);
      //ReopenSumPall();
    end;
  end;
end;

procedure TFmTXPallet.PageControl1Change(Sender: TObject);
begin
  ReabreQuerys(PageControl1.ActivePageIndex);
end;

procedure TFmTXPallet.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXPallet);
  //MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTXPallet, Qr???);
  Encerraclassificao1.Enabled :=
    (QrTXPallet.State <> dsInactive) and (QrTXPalletDtHrEndAdd.Value < 2);
  IncluiManualmente1.Enabled := VAR_TXInsPalManu (*Dmod.QrControleTXInsPalManu.Value*) = 1;
end;

procedure TFmTXPallet.PMCacItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(Alteradadosdiitematual1, QrTXCacItsAOpn);
end;

procedure TFmTXPallet.PMIMEIPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(Irparajaneladomovimento1, QrIMEIs);
  MyObjects.HabilitaMenuItemItsUpd(IrparajaneladedadosdoIMEI1, QrIMEIs);
  MyObjects.HabilitaMenuItemItsUpd(AlteraIMEIorigemdoIMEISelecionado1, QrIMEIs);
end;

procedure TFmTXPallet.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXPalletCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXPallet.DefParams;
begin
  VAR_GOTOTABELA := 'TXPalleta';
  VAR_GOTOMYSQLTABLE := QrTXPallet;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT let.*,  ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,');
  VAR_SQLx.Add(' CONCAT(gg1.Nome, ');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ');
  VAR_SQLx.Add('NO_PRD_TAM_COR, vps.Nome NO_STATUS   ');
  VAR_SQLx.Add('FROM txpalleta let  ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  ');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  ');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ');
  VAR_SQLx.Add('LEFT JOIN txpalsta   vps ON vps.Codigo=let.Status');

  VAR_SQLx.Add('WHERE let.Codigo > 0');
  //
  VAR_SQL1.Add('AND let.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND let.Nome Like :P0');
  //
end;

procedure TFmTXPallet.Desfazencerramento1Click(Sender: TObject);
var
  Codigo, GraGruX: Integer;
begin
{POIU
  Codigo := QrTXPalletCodigo.Value;
  GraGruX := QrTXPalletGraGruX.Value;
  if TX_PF.DesfazEncerramentoPallet(Codigo, GraGruX) then
    LocCod(Codigo, Codigo);
}
end;

procedure TFmTXPallet.Encerraclassificao1Click(Sender: TObject);
const
  Pergunta = True;
begin
{POIU
  TX_PF.EncerraPalletNew(QrTXPalletCodigo.Value, Pergunta);
}
end;

procedure TFmTXPallet.EncerraPallet1Click(Sender: TObject);
const
  Pergunta = True;
begin
{POIU
  TX_PF.EncerraPalletNew(QrTXPalletCodigo.Value, Pergunta);
}
end;

procedure TFmTXPallet.ExcluiIMEIselecionado1Click(Sender: TObject);
begin
{POIU
  if Geral.MB_Pergunta('Confirme a exclus�o do IME-I' +
  Geral.FF0(QrIMEIsControle.Value) + '?') = ID_YES then
  begin
    if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(QrIMEIsControle.Value,
      Integer(TEstqMotivDel.emtdWetCurti013), Dmod.QrUpd, Dmod.MyDB, '') then
    begin
      LocCod(QrTXPalletCodigo.Value, QrTXPalletCodigo.Value);
    end;
  end;
}
end;

procedure TFmTXPallet.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel);
end;

procedure TFmTXPallet.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTXPallet.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
  //SbNumeroClick(Self); Erro no win server 2003
end;

procedure TFmTXPallet.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXPallet.ReopenClassificacoes();
var
  SQL_IMEI: String;
begin
  SQL_IMEI := Geral.ATS([
  '0 Codigo, 0 IMEC, 0 IMEI, ',
  '0 MovimID, 0 MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDestino, Dmod.MyDB, [
  'SELECT ',
  'cia.Controle, cia.TXPallet, ',
  'txp.Empresa, txp.GraGruX, 0 Qtde, 0 ValorT, 0 SdoVrtQtd, ',
  '-cia.Qtde LmbVrtQtd, ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, rca.TXPallet Pallet, txp.Nome NO_Pallet,',
  '0 Terceiro,',
  'IF(txp.CliStat IS NULL, 0, txp.CliStat),',
  'IF(txp.Status IS NULL, 0, txp.Status),',
  '"" NO_FORNECE,',
  'IF((txp.CliStat=0) OR (txp.CliStat IS NULL), "",',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
  'vps.Nome NO_STATUS,',
  'txp.DtHrEndAdd DataHora, 0 OrdGGX,',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  //
  '2 PalStat, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
  '0 SdoInteiros, ',
  '-cia.Qtde * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) LmbInteiros, ',
  SQL_IMEI,
  '1 Ativo  ',
  'FROM ' + TMeuDB + '.txcacitsa cia',
  'LEFT JOIN ' + TMeuDB + '.txparclcaba rca ON rca.Codigo=cia.Codigo',
  'LEFT JOIN ' + TMeuDB + '.txpalleta  txp ON txp.Codigo=rca.TXPallet',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=txp.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  '', // Sem terceiro???
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=txp.CliStat',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=txp.Empresa',
  'LEFT JOIN ' + TMeuDB + '.txpalsta  vps ON vps.Codigo=txp.Status',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
////////////  2015-03-05 ///////////////////////////////////////////////////////
  'WHERE cia.TXPaRclIts IN (',
  '     SELECT Controle ',
  '     FROM ' + TMeuDB + '.txparclitsa pri ',
  '     WHERE DtHrFim < "1900-01-01"',
  ')',
  //
  'AND rca.TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  '']);

////////////////////////////////////////////////////////////////////////////////
///
  UnDmkDAC_PF.AbreMySQLQuery0(QrOriRcl, Dmod.MyDB, [
  'SELECT ',
  'cia.Controle, cia.TXPallet, ',
  'txp.Empresa, txp.GraGruX, 0 Qtde, 0 ValorT, 0 SdoVrtQtd, ',
  'cia.Qtde LmbVrtQtd, ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, cia.TXPallet Pallet, txp.Nome NO_Pallet,',
  '0 Terceiro,',
  'IF(txp.CliStat IS NULL, 0, txp.CliStat),',
  'IF(txp.Status IS NULL, 0, txp.Status),',
  '"" NO_FORNECE,',
  'IF((txp.CliStat=0) OR (txp.CliStat IS NULL), "",',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
  'vps.Nome NO_STATUS,',
  'txp.DtHrEndAdd DataHora, 0 OrdGGX,',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  //
  '1 PalStat, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
  '0 SdoInteiros, ',
  'cia.Qtde * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) LmbInteiros, ',
  SQL_IMEI,
  '1 Ativo  ',
  'FROM ' + TMeuDB + '.txcacitsa cia',
  'LEFT JOIN ' + TMeuDB + '.txparclcaba rca ON rca.Codigo=cia.Codigo',
  'LEFT JOIN ' + TMeuDB + '.txpalleta  txp ON txp.Codigo=cia.TXPallet',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=txp.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  '', // Sem terceiro???
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=txp.CliStat',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=txp.Empresa',
  'LEFT JOIN ' + TMeuDB + '.txpalsta   vps ON vps.Codigo=txp.Status',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
////////////  2015-03-05 ///////////////////////////////////////////////////////
  'WHERE cia.TXPaRclIts IN (',
  '     SELECT Controle ',
  '     FROM ' + TMeuDB + '.txparclitsa pri ',
  '     WHERE DtHrFim < "1900-01-01"',
  ')',
  'AND cia.TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  '']);

////////////////////////////////////////////////////////////////////////////////
///
  UnDmkDAC_PF.AbreMySQLQuery0(QrOriCla, Dmod.MyDB, [
  'SELECT ',
  'cia.Controle, cia.TXPallet, ',
  'txp.Empresa, txp.GraGruX, 0 Qtde, 0 ValorT, 0 SdoVrtQtd, ',
  'cia.Qtde LmbVrtQtd, ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, cia.TXPallet Pallet, txp.Nome NO_Pallet, ',
  '0 Terceiro, ',
  'IF(txp.CliStat IS NULL, 0, txp.CliStat), ',
  'IF(txp.Status IS NULL, 0, txp.Status), ',
  '"" NO_FORNECE, ',
  'IF((txp.CliStat=0) OR (txp.CliStat IS NULL), "", ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vps.Nome NO_STATUS, ',
  'txp.DtHrEndAdd DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  '1 PalStat, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
  '0 SdoInteiros, ',
  'cia.Qtde * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) LmbInteiros, ',
  SQL_IMEI,
  '1 Ativo ',
  'FROM ' + TMeuDB + '.txcacitsa cia ',
  'LEFT JOIN ' + TMeuDB + '.txpaclacaba rca ON rca.Codigo=cia.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.txpalleta  txp ON txp.Codigo=cia.TXPallet ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=txp.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=txp.CliStat ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=txp.Empresa ',
  'LEFT JOIN ' + TMeuDB + '.txpalsta   vps ON vps.Codigo=txp.Status ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2 ',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE ( ',
  '(cia.TXPaClaIts <> 0) AND ',
  '(cia.TXPaClaIts IN ( ',
  '     SELECT DISTINCT Controle ',
  '     FROM ' + TMeuDB + '.txpaclaitsa ',
  '     WHERE DtHrFim < "1900-01-01") ',
  ')) ',
  'AND cia.TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  '']);
  //
  EdDestino.ValueVariant := Geral.FF0(QrDestino.RecordCount);
  EdOriCla.ValueVariant := Geral.FF0(QrOriCla.RecordCount);
  EdOriRcl.ValueVariant := Geral.FF0(QrOriRcl.RecordCount);



//  Fazer historia de pallets destino em aberto!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallDst, Dmod.MyDB, [
  'SELECT cia.TXPallet, SUM(Qtde) Qtde, TMI_Dest, pal.DtHrEndAdd, ',
  'IF(pal.DtHrEndAdd < "1900-01-01", "",  ',
  'DATE_FORMAT(pal.DtHrEndAdd, "%d/%m/%Y %H:%i:%s")) DtHrEndAdd_TXT  ',
  'FROM txcacitsa cia ',
  'LEFT JOIN txparclcaba rcl ON cia.CacCod=rcl.CacCod ',
  'LEFT JOIN txpalleta   pal ON pal.Codigo=cia.TXPallet ',
  'WHERE rcl.TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  'GROUP BY TMI_Dest ',
  '']);
  //Um dos itens da sql acima do pallet 489 tem 64 couros e eh para o Pallet 474
  //que estah removido!
  //Fazer tambem a origem no pallet 474 pois ele tem 248 couros no total!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallOri, Dmod.MyDB, [
  'SELECT 2.000 ClaRcl, "Reclassifica��o" NO_ClaRcl, ',
  'cia.CacCod, cia.Codigo, SUM(cia.Qtde) Qtde, ',
  'pcc.TXMovIts, pcc.TXPallet + 0.000 TXPallet, pcc.DtHrFimCla, ',
  'IF(pcc.DtHrFimCla < "1900-01-01", "",   ',
  'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT  ',
  'FROM txcacitsa cia ',
  'LEFT JOIN txparclcaba pcc ON pcc.CacCod=cia.CacCod ',
  //'LEFT JOIN v s m o v i t s tmi ON tmi.Controle=pcc.TXMovIts ',
  'LEFT JOIN ' + CO_SEL_TAB_TMI + ' tmi ON tmi.Controle=pcc.TXMovIts ',
  'WHERE cia.TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  'AND cia.CacID=8 ',
  'GROUP BY cia.CacCod, cia.Codigo ',
  ' ',
  'UNION ',
  ' ',
  'SELECT 1.000 ClaRcl, "Classifica��o" NO_ClaRcl,  ',
  'cia.CacCod, cia.Codigo, SUM(cia.Qtde) Qtde, ',
  'pcc.TXMovIts, 0 TXPallet, pcc.DtHrFimCla, ',
  'IF(pcc.DtHrFimCla < "1900-01-01", "",   ',
  'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %H:%i:%s")) DtHrFimCla_TXT  ',
  'FROM txcacitsa cia ',
  'LEFT JOIN txpaclacaba pcc ON pcc.CacCod=cia.CacCod ',
  //'LEFT JOIN v s m o v i t s tmi ON tmi.Controle=pcc.TXMovIts ',
  'LEFT JOIN ' + CO_SEL_TAB_TMI + ' tmi ON tmi.Controle=pcc.TXMovIts ',
  'WHERE cia.TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  'AND cia.CacID=7 ',
  'GROUP BY cia.CacCod, cia.Codigo ',
  ' ']);
end;

procedure TFmTXPallet.ReopenCourosEmClassificacao(Controle: Int64);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXCacItsAOpn, Dmod.MyDB, [
  'SELECT * ',
  'FROM txcacitsa',
  'WHERE TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  '']);
  if Controle > 0 then
    QrTXCacItsAOpn.Locate('Controle', Controle, []);
end;

(*
procedure TFmTXPallet.ReopenGraGruX();
begin
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1536_V SCouCal) + ',' +
    Geral.FF0(CO_GraGruY_2048_V SRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_V SRibCla) + ',' +
    Geral.FF0(CO_GraGruY_6144_V SFinCla) +
    ')');
end;
*)

procedure TFmTXPallet.ReopenIMEIs(Controle: Integer);
const
  TemIMEIMrt = 1;
var
  ATT_MovimID, ATT_MovimNiv: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('tmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('tmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  ATT_MovimID,
  ATT_MovimNiv,
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'txp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=tmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.Pallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrIMEIS);
  //
  if Controle <> 0 then
    QrIMEIs.Locate('Controle', Controle, []);
end;

procedure TFmTXPallet.ReopenTXPaItens();
var
  Tabela, Codigo: String;
begin
  if QrTXPaItsTXPaRclIts.Value > 0 then
  begin
    Tabela := 'txparclitsa';
    Codigo := Geral.FF0(QrTXPaItsTXPaRclIts.Value);
  end else
  begin
    Tabela := 'txpaclaitsa';
    Codigo := Geral.FF0(QrTXPaItsTXPaClaIts.Value);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPaItens, Dmod.MyDB, [
  'SELECT Codigo, Controle, TXPallet, TMI_Sorc, TMI_Baix, TMI_Dest, ',
  'Tecla, DtHrIni, DtHrFim, QIt_Sorc, QIt_Baix  ',
  'FROM ' + Tabela,
  'WHERE Controle=' + Codigo,
  '']);
end;

procedure TFmTXPallet.ReopenTXPaIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPaIts, Dmod.MyDB, [
  'SELECT TXPallet, TXPaClaIts, TXPaRclIts, TMI_Sorc, TMI_Dest,  ',
  'SUM(Qtde) Qtde ',
  'FROM txcacitsa ',
  'WHERE TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  'GROUP BY TXPallet, TXPaClaIts, TXPaRclIts,  ',
  'TMI_Sorc, TMI_Dest ',
  '']);
end;

procedure TFmTXPallet.ReopenSumPall();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPall, Dmod.MyDB, [
  'SELECT tmi.GraGruX, SUM(tmi.Qtde) Qtde, SUM(ValorT) ValorT, ',
  'SUM(tmi.SdoVrtQtd) SdoVrtQtd, ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, MAX(tmi.DataHora) DataHora ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=tmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE tmi.Pallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  //2015-05-05
  'AND tmi.Qtde>0 ',
  // FIM 2015-05-05
  'GROUP BY tmi.GraGruX ',
  '']);
end;

procedure TFmTXPallet.DBGrid1DblClick(Sender: TObject);
begin
  VAR_IMEI_SEL := QrIMEIsControle.Value;
  Close;
end;

procedure TFmTXPallet.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXPallet.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXPallet.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXPallet.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXPallet.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXPallet.udo1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXPallet, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'TXPalleta');
  Empresa := DmodG.ObtemFilialDeEntidade(QrTXPalletEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  TPDtHrEndAdd.Enabled := True;
  EdDtHrEndAdd.Enabled := True;
  //
  EdEmpresa.Enabled := False;
  CBEmpresa.Enabled := False;
  EdClientMO.Enabled := False;
  CBClientMO.Enabled := False;
end;

procedure TFmTXPallet.UpdDelCouroSelecionado(SQLTipo: TSQLType);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'txcacitsa';
  Campo = 'Controle';
var
  Controle: Int64;
  Qtde: Double;
  Txt: String;
  Continua: Boolean;
  TMI_Dest, TMI_Sorc, TMI_Baix, Codigo: Integer;
begin
(*
  Codigo         := QrTXPalletCodigo.Value;
  Controle       := QrTXCacItsAOpnControle.Value;
  TMI_Dest       := QrTXCacItsAOpnTMI_Dest.Value;
  TMI_Sorc       := QrTXCacItsAOpnTMI_Sorc.Value;
  TMI_Baix       := QrTXCacItsAOpnTMI_Baix.Value;
  case SQLTipo of
    stUpd:
    begin
      Qtde  := 1;
      Txt := FloatToStr(QrTXCacItsAOpn A r e a M2.Value * 100);
      if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
      begin
        A r e a M 2 := Geral.IMV(Txt);
        A r e a M 2 := A r e a M 2 / 100;
        A r e a P 2 := Geral.ConverteA r e a(A r e a M 2, ctM2toP2, cfQuarto);
        Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txcacitsa', False, [
                      'A r e a M 2', 'A r e a P 2'], ['Controle'], [A r e a M 2, A r e a P 2],
                      [Controle], True);
      end;
    end;
    stDel:
    begin
      Continua := TX_PF.ExcluiTXNaoTMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
    end;
    else
*)
      Geral.MB_Erro('N�o implementado!');
(*
  end;
  if Continua then
  begin
    TX_PF.AtualizaSaldoOrigemTMI_Dest(TMI_Dest, TMI_Sorc, TMI_Baix);
    //
    LocCod(Codigo, Codigo);
    ReopenCourosEmClassificacao(Controle);
  end;
*)
end;

procedure TFmTXPallet.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXPallet.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXPalletCodigo.Value;
  Close;
end;

procedure TFmTXPallet.Alteradadosdiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd);
end;

procedure TFmTXPallet.AlteraFornecedor1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Reclassifica��o';
  Prompt = 'Informe a reclassifica��o';
  Campo  = 'Descricao';
var
  Terceiro, Controle, ThisCtrl: Integer;
  Resp: Variant;
begin
  ThisCtrl := QrIMEIsControle.Value;
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT ent.Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Descricao ',
    'FROM entidades ent ',
    'WHERE ent.Fornece1="V" ',
    'ORDER BY Descricao ',
    ''], Dmod.MyDB, True);
  if Resp <> Null then
  begin
    Terceiro := Resp;
    QrIMEIs.First;
    while not QrIMEIs.Eof do
    begin
      Controle := QrIMEIsControle.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Terceiro'], [
      'Controle'], [
      Terceiro], [
      Controle], True);
      //
      QrIMEIs.Next;
    end;
    //
    ReopenIMEIs(ThisCtrl);
  end;
end;

procedure TFmTXPallet.AlteraIMEIorigemdoIMEISelecionado1Click(Sender: TObject);
var
  ResVar: Variant;
  Controle, SrcNivel2: Integer;
begin
  if VAR_USUARIO <> -1 then
  begin
    Geral.MB_Aviso('Usu�rio deve ser Master!');
    Exit;
  end;
  SrcNivel2 := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Troca IME-I Origem', 'Informe o novo IME-I de origem: ',
  0, ResVar) then
  begin
    SrcNivel2 := Geral.IMV(ResVar);
    Controle  := QrIMEIsControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
    'SrcNivel2'], ['Controle'], [
    SrcNivel2], [Controle], True) then
    begin
      ReopenIMEIs(Controle);
    end;
  end;
end;

procedure TFmTXPallet.ApenasaDescrio1Click(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
begin
  Nome := QrTXPalletNome.Value;
  if InputQuery('Descri��o', 'Informe a nova descri��o:', Nome) then
  begin
    Codigo := QrTXPalletCodigo.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'TXPalleta', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
    begin
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmTXPallet.AtualizaIMEIsDeGeracaoDeTodosPallets();
var
  Qry: TmySQLQuery;
  Pallet: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM txpalleta ',
    'ORDER BY Codigo DESC',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Pallet := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Pallet));
        //
        TX_PF.AtualizaPalletPelosIMEIsDeGeracao(Pallet, QrSumTMI);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmTXPallet.Atualizasaldo1Click(Sender: TObject);
begin
  QrIMEIS.DisableControls;
  try
    QrIMEIs.First;
    while not QrIMEIs.Eof do
    begin
      TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(QrIMEIsControle.Value,
        QrIMEIsMovimID.Value, QrIMEIsMovimNiv.Value);
      QrIMEIs.Next;
    end;
    //
    LocCod(QrTXPalletCodigo.Value, QrTXPalletCodigo.Value);
  finally
    QrIMEIS.EnableControls;
  end;
end;

procedure TFmTXPallet.Atualizasaldo2Click(Sender: TObject);
const
  Gera = False;
begin
  TX_PF.AtualizaSaldoIMEI(QrIMEIsControle.Value, Gera);
end;

procedure TFmTXPallet.AtualizaStatPallDeTodosPallets();
var
  Qry: TmySQLQuery;
  Pallet: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM txpalleta ',
    'ORDER BY Codigo DESC',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Pallet := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Pallet));
        //
        TX_PF.AtualizaStatPall(Pallet);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmTXPallet.AtualizaStatus1Click(Sender: TObject);
begin
  TX_PF.AtualizaStatPall(QrTXPalletCodigo.Value);
end;

procedure TFmTXPallet.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrEndAdd: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc,
  Controle, Terceiro, SrcGGX, DstGGX: Integer;
  TipoTrocaGgxVmiPall: TTipoTrocaGgxVmiPall;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Status         := EdStatus.ValueVariant;
  CliStat        := EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT_TP_Ed(TPDtHrEndAdd.Date, EdDtHrEndAdd.Text);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := EdQtdPrevPc.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina dono do couro!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('TXPalleta', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'TXPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    //Parei Aqui!! Alterar artigo de IMEIs !!!
    if Geral.MB_Pergunta('Deseja alterar o artigo dos IME-Is?') = ID_YES then
    begin
      //Parei aqui! somente dos destinos!
      QrIMEIs.First;
      while not QrIMEIs.Eof do
      begin
        Controle := QrIMEIsControle.Value;
        //
{
////////////////////////////////////////////////////////////////////////////////
   N�o pode!! Pallet n�o � pelo reduzido, mas pelo Nivel1!
////////////////////////////////////////////////////////////////////////////////
        TipoTrocaGgxVmiPall := TX_PF.RedefineReduzidoOnPallet(
          TEstqMovimID(QrIMEIsMovimID.Value),
          TEstqMovimNiv(QrIMEIsMovimNiv.Value));
        case TipoTrocaGgxVmiPall of
          TTipoTrocaGgxVmiPall.ttvpNenhum: ; // Nada!
          TTipoTrocaGgxVmiPall.ttvpGraGrux:
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
            'GraGruX'], [
            'Controle'], [
            GraGruX], [
            Controle], True);
          end;
          TTipoTrocaGgxVmiPall.ttvpClaRcl:
          begin
            SrcGGX := GraGruX;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
            'GraGruX', 'SrcGGX'], [
            'Controle'], [
            GraGruX, SrcGGX], [
            Controle], True);
            //
            UnDmkDAC_PF.AbreMySQLQuery0(Qr1, Dmod.MyDB, [
            'SELECT Controle ',
            'FROM txmovits ',
            'WHERE MovimTwn<>0 ',
            'AND MovimTwn=' + Geral.FF0(QrIMEIsMovimTwn.Value),
            'AND MovimNiv=1',
            '']);
            Controle := Qr1.FieldByName('Controle').AsInteger;
            if Controle <> 0 then
            begin
              DstGGX := GraGruX;
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
              'DstGGX'], [
              'Controle'], [
              DstGGX], [
              Controle], True);
            end else
              Geral.MB_Erro('Controle do MovimNiv 1 do MovimTwn ' +
              Geral.FF0(QrIMEIsMovimTwn.Value) +
              ' n�o localizado! Avise a Dermatek!');
          end;
        end;
}
        //
        QrIMEIs.Next;
      end;
    end;
    (*
    Descri��o: Foi criado um bot�o na janela principal em: Ferramentas => Corre��es => Atualiza Artigos De Pallets
    Motivo: Melhoria do desempenho desta janela
    Data: 25/04/2017
    //
    TX_PF.AtualizaArtigosDePallets(PB1, LaAviso1, LaAviso2);
    *)
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTXPallet.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'TXPalleta', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TXPalleta', 'Codigo');
end;

procedure TFmTXPallet.BtIMEIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIMEI, BtIMEI);
end;

procedure TFmTXPallet.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXPallet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  GBEdita.Align   := alClient;
  //DBGFrn.Align    := alClient;
  PageControl1.Align   := alClient;
  PageControl1.ActivePageIndex := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  CriaOForm;
  FSeq := 0;
  //
  //UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrTXRibCla, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrTXPalSta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  //ReopenGraGruX();
  TX_PF.ReopenGraGruX_Pallet(QrGraGruX);
end;

procedure TFmTXPallet.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXPalletCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXPallet.SBClienteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdCliStat.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCliStat.Text := IntToStr(VAR_ENTIDADE);
    CBCliStat.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXPallet.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXPallet.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTXPallet.SbNovoClick(Sender: TObject);
begin
(*
  FAtualizando := not FAtualizando;
  if FAtualizando then
    AtualizaStatPallDeTodosPallets();
*)
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
    AtualizaIMEIsDeGeracaoDeTodosPallets();
end;

procedure TFmTXPallet.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXPallet.QrTXPaItsAfterScroll(DataSet: TDataSet);
begin
  ReopenTXPaItens();
end;

procedure TFmTXPallet.QrTXPaItsBeforeClose(DataSet: TDataSet);
begin
  QrTXPaItens.Close;
end;

procedure TFmTXPallet.QrTXPalletAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXPallet.QrTXPalletAfterScroll(DataSet: TDataSet);
begin
  ReopenSumPall();
  ReabreQuerys(PageControl1.ActivePageIndex);
end;

procedure TFmTXPallet.FichaCOMonomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(True);
end;

procedure TFmTXPallet.FichaSEMonomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(False);
end;

procedure TFmTXPallet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXPalletCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXPallet.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTXPalletCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'TXPalleta', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTXPallet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXPallet.frxWET_CURTI_013_AGetValue(const VarName: string;
  var Value: Variant);
(*
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
*)
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
(*
  else
  if VarName ='VARF_DATA_MEU' then
    Value := TPDataRelativa.Date
*)
  else
end;

procedure TFmTXPallet.IMEIsdegerao1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  TMI_Dest, TMI_Sorc, TMI_Baix, Pallet, Controle: Integer;
  Qtde, SdoVrtQtd: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    Pallet := QrTXPalletCodigo.Value;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT cia.TMI_Baix, cia.TMI_Dest, cia.TMI_Sorc, ',
      'SUM(cia.Qtde) Qtde',
      'FROM txcacitsa cia',
      //'LEFT JOIN v s m o v i t s tmi ON tmi.Controle=cia.TMI_Baix',
      //'LEFT JOIN v s m o v i t s vmd ON vmd.Controle=cia.TMI_Dest',
      'WHERE cia.TXPallet=' + Geral.FF0(Pallet),
      'GROUP BY cia.TMI_Baix, cia.TMI_Dest, cia.TMI_Sorc',
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando item ' +
        Geral.FF0(Qry.RecNo) + ' de ' + Geral.FF0(Qry.RecordCount));
        //
        TMI_Dest := Qry.FieldByName('TMI_Dest').AsInteger;
        TMI_Sorc := Qry.FieldByName('TMI_Sorc').AsInteger;
        TMI_Baix := Qry.FieldByName('TMI_Baix').AsInteger;
        if TMI_Dest <> 0 then
        begin
          TX_PF.AtualizaSaldoOrigemTMI_Dest(TMI_Dest, TMI_Sorc, TMI_Baix);
        end;
        //
        Qry.Next;
      end;
      LocCod(Pallet, Pallet);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTXPallet.ImprimePallet(InfoNO_Pallet: Boolean);
var
  Empresa, ClientMO, Pallet: Integer;
  TempTab: String;
begin
  ReabreQuerys(-1);
  //
  if QrSumPallSdoVrtQtd.Value <= 0 then
    Geral.MB_Aviso('O Pallet s� ser� impresso se tiver saldo positivo!');
  Empresa  := QrTXPalletEmpresa.Value;
  ClientMO := QrTXPalletClientMO.Value;
  Pallet   := QrTXPalletCodigo.Value;
  TempTab  := Self.Name;
  //
  TX_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, TempTab, InfoNO_Pallet);
end;

procedure TFmTXPallet.IncluiManualmente1Click(Sender: TObject);
var
  Pallet: Integer;
begin
  Pallet := TX_PF.CadastraPalletRibCla((*Empresa*)VAR_LIB_EMPRESA_SEL,
  (*ClientMO*)0, (*EdPallet*)nil, (*CBPallet*)nil, (*QrTXPallet*)nil,
  (*MovimIDGer*) TEstqMovimID.emidAjuste, (*GraGruX*)0);
  if Pallet <> 0 then
    LocCod(Pallet, Pallet);
end;

procedure TFmTXPallet.IrparajaneladedadosdoIMEI1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXMovIts(QrIMEIsControle.Value);
end;

procedure TFmTXPallet.Irparajaneladomovimento1Click(Sender: TObject);
begin
  if QrIMEIs.RecordCount > 0 then
    TX_Jan.MostraFormTX_Do_IMEI(QrIMEISControle.Value);
end;

procedure TFmTXPallet.Itensnopallet1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXCacItsAOpnImp, Dmod.MyDB, [
  'SELECT its.*, rev.Nome NoRev, dig.Nome NoDig ',
  'FROM txcacitsa its ',
  'LEFT JOIN entidades rev ON rev.Codigo=its.Revisor ',
  'LEFT JOIN entidades dig ON dig.Codigo=its.Digitador ',
  'WHERE its.TXPallet=' + Geral.FF0(QrTXPalletCodigo.Value),
  'ORDER BY its.DataHora, its.Controle ',
  '']);
  MyObjects.frxDefineDataSets(frxWET_CURTI_013_A, [
  DmodG.frxDsDono,
  frxDsTXPallet,
  frxDsTXCacItsAOpnImp
  ]);
  MyObjects.frxMostra(frxWET_CURTI_013_A, 'Itens em Pallet');
end;

procedure TFmTXPallet.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXPallet, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'TXPalleta');
  EdStatus.ValueVariant := 0; //CO_STAT_TXPALLET_0100_DISPONIVEL;
  CBStatus.KeyValue     := 0; //CO_STAT_TXPALLET_0100_DISPONIVEL;
  TPDtHrEndAdd.Enabled := False;
  EdDtHrEndAdd.Enabled := False;
  TPDtHrEndAdd.Date := 0;
  EdDtHrEndAdd.ValueVariant := 0;
  EdNome.ValueVariant := '';
  //
  EdEmpresa.Enabled := True;
  CBEmpresa.Enabled := True;
  EdClientMO.Enabled := True;
  CBClientMO.Enabled := True;
end;

procedure TFmTXPallet.QrTXPalletBeforeClose(
  DataSet: TDataSet);
begin
  //QrTXPalArt.Close;
  QrSumPall.Close;
  QrIMEIs.Close;
  QrTXPaIts.Close;
end;

procedure TFmTXPallet.QrTXPalletBeforeOpen(DataSet: TDataSet);
begin
  QrTXPalletCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTXPallet.QrTXPalletCalcFields(DataSet: TDataSet);
begin
  QrTXPalletDtHrEndAdd_TXT.Value := Geral.FDT(QrTXPalletDtHrEndAdd.Value, 106, True);
end;

end.


