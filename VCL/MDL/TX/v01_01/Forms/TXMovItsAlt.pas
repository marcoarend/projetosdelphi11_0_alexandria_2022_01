unit TXMovItsAlt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnDmkProcFunc, AppListas,
  dmkCheckBox, dmkCheckGroup, UnProjGroup_Consts, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmTXMovItsAlt = class(TForm)
    GBQtdOriginal: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label1: TLabel;
    Label6: TLabel;
    EdObserv: TdmkEdit;
    EdValorT: TdmkEdit;
    QrTXMovSrc: TmySQLQuery;
    DsTXMovSrc: TDataSource;
    QrTXMovDst: TmySQLQuery;
    DsTXMovDst: TDataSource;
    QrTXMovIts: TmySQLQuery;
    QrTXMovItsCodigo: TIntegerField;
    QrTXMovItsControle: TIntegerField;
    QrTXMovItsMovimCod: TIntegerField;
    QrTXMovItsMovimNiv: TIntegerField;
    QrTXMovItsMovimTwn: TIntegerField;
    QrTXMovItsEmpresa: TIntegerField;
    QrTXMovItsTerceiro: TIntegerField;
    QrTXMovItsCliVenda: TIntegerField;
    QrTXMovItsMovimID: TIntegerField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TIntegerField;
    QrTXMovItsGraGruX: TIntegerField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsValorT: TFloatField;
    QrTXMovItsSrcMovID: TIntegerField;
    QrTXMovItsSrcNivel1: TIntegerField;
    QrTXMovItsSrcNivel2: TIntegerField;
    QrTXMovItsSrcGGX: TIntegerField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsSerieTal: TIntegerField;
    QrTXMovItsTalao: TIntegerField;
    QrTXMovItsFornecMO: TIntegerField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TIntegerField;
    QrTXMovItsDstNivel1: TIntegerField;
    QrTXMovItsDstNivel2: TIntegerField;
    QrTXMovItsDstGGX: TIntegerField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsAptoUso: TSmallintField;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsLk: TIntegerField;
    QrTXMovItsDataCad: TDateField;
    QrTXMovItsDataAlt: TDateField;
    QrTXMovItsUserCad: TIntegerField;
    QrTXMovItsUserAlt: TIntegerField;
    QrTXMovItsAlterWeb: TSmallintField;
    QrTXMovItsAtivo: TSmallintField;
    QrTXMovItsGraGru1: TIntegerField;
    QrTXMovItsNO_PRD_TAM_COR: TWideStringField;
    QrTXMovItsNO_EstqMovimID: TWideStringField;
    QrTXMovItsNO_DstMovID: TWideStringField;
    QrTXMovItsNO_SrcMovID: TWideStringField;
    QrTXMovItsTpCalcAuto: TIntegerField;
    DsTXMovIts: TDataSource;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    Panel9: TPanel;
    Label15: TLabel;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox5: TGroupBox;
    Panel14: TPanel;
    Label28: TLabel;
    DBEdit28: TDBEdit;
    GroupBox6: TGroupBox;
    Panel16: TPanel;
    Label42: TLabel;
    DBEdit42: TDBEdit;
    GroupBox7: TGroupBox;
    Panel17: TPanel;
    Label47: TLabel;
    DBEdit47: TDBEdit;
    Panel7: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
    Panel11: TPanel;
    Label2: TLabel;
    Label13: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label14: TLabel;
    Label20: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit12: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit11: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    GroupBox4: TGroupBox;
    Panel12: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    GroupBox8: TGroupBox;
    Panel13: TPanel;
    Label26: TLabel;
    Label27: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    Panel15: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label33: TLabel;
    Label35: TLabel;
    Label46: TLabel;
    Label51: TLabel;
    DBEdit40: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit31: TDBEdit;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit46: TDBEdit;
    Label52: TLabel;
    GBDadosArtigo: TGroupBox;
    QrTXSertal: TmySQLQuery;
    QrTXSertalCodigo: TIntegerField;
    QrTXSertalNome: TWideStringField;
    DsTXSertal: TDataSource;
    Panel5: TPanel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    CBSerieTal: TdmkDBLookupComboBox;
    EdSerieTal: TdmkEditCB;
    EdTalao: TdmkEdit;
    EdMarca: TdmkEdit;
    CkZerado: TdmkCheckBox;
    QrTXMovItsZerado: TSmallintField;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    EdStqCenLoc: TdmkEditCB;
    EdReqMovEstq: TdmkEdit;
    Label56: TLabel;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label57: TLabel;
    DBEdit38: TDBEdit;
    Label38: TLabel;
    DBEdit36: TDBEdit;
    Label37: TLabel;
    DBEdit37: TDBEdit;
    Label36: TLabel;
    QrTXMovItsReqMovEstq: TIntegerField;
    QrTXMovItsStqCenLoc: TIntegerField;
    QrTXMovSrcCodigo: TLargeintField;
    QrTXMovSrcControle: TLargeintField;
    QrTXMovSrcMovimCod: TLargeintField;
    QrTXMovSrcMovimNiv: TLargeintField;
    QrTXMovSrcMovimTwn: TLargeintField;
    QrTXMovSrcEmpresa: TLargeintField;
    QrTXMovSrcTerceiro: TLargeintField;
    QrTXMovSrcCliVenda: TLargeintField;
    QrTXMovSrcMovimID: TLargeintField;
    QrTXMovSrcDataHora: TDateTimeField;
    QrTXMovSrcPallet: TLargeintField;
    QrTXMovSrcGraGruX: TLargeintField;
    QrTXMovSrcQtde: TFloatField;
    QrTXMovSrcValorT: TFloatField;
    QrTXMovSrcSrcMovID: TLargeintField;
    QrTXMovSrcSrcNivel1: TLargeintField;
    QrTXMovSrcSrcNivel2: TLargeintField;
    QrTXMovSrcSrcGGX: TLargeintField;
    QrTXMovSrcSdoVrtQtd: TFloatField;
    QrTXMovSrcObserv: TWideStringField;
    QrTXMovSrcSerieTal: TLargeintField;
    QrTXMovSrcTalao: TLargeintField;
    QrTXMovSrcFornecMO: TLargeintField;
    QrTXMovSrcCustoMOUni: TFloatField;
    QrTXMovSrcCustoMOTot: TFloatField;
    QrTXMovSrcValorMP: TFloatField;
    QrTXMovSrcDstMovID: TLargeintField;
    QrTXMovSrcDstNivel1: TLargeintField;
    QrTXMovSrcDstNivel2: TLargeintField;
    QrTXMovSrcDstGGX: TLargeintField;
    QrTXMovSrcQtdGer: TFloatField;
    QrTXMovSrcQtdAnt: TFloatField;
    QrTXMovSrcStqCenLoc: TLargeintField;
    QrTXMovSrcGraGru1: TLargeintField;
    QrTXMovSrcNO_EstqMovimID: TWideStringField;
    QrTXMovSrcNO_MovimID: TWideStringField;
    QrTXMovSrcNO_MovimNiv: TWideStringField;
    QrTXMovSrcNO_PALLET: TWideStringField;
    QrTXMovSrcNO_PRD_TAM_COR: TWideStringField;
    QrTXMovSrcNO_FORNECE: TWideStringField;
    QrTXMovSrcNO_SerieTal: TWideStringField;
    QrTXMovSrcNO_TTW: TWideStringField;
    QrTXMovSrcID_TTW: TLargeintField;
    QrTXMovSrcNO_LOC_CEN: TWideStringField;
    QrTXMovDstCodigo: TLargeintField;
    QrTXMovDstControle: TLargeintField;
    QrTXMovDstMovimCod: TLargeintField;
    QrTXMovDstMovimNiv: TLargeintField;
    QrTXMovDstMovimTwn: TLargeintField;
    QrTXMovDstEmpresa: TLargeintField;
    QrTXMovDstTerceiro: TLargeintField;
    QrTXMovDstCliVenda: TLargeintField;
    QrTXMovDstMovimID: TLargeintField;
    QrTXMovDstDataHora: TDateTimeField;
    QrTXMovDstPallet: TLargeintField;
    QrTXMovDstGraGruX: TLargeintField;
    QrTXMovDstQtde: TFloatField;
    QrTXMovDstValorT: TFloatField;
    QrTXMovDstSrcMovID: TLargeintField;
    QrTXMovDstSrcNivel1: TLargeintField;
    QrTXMovDstSrcNivel2: TLargeintField;
    QrTXMovDstSrcGGX: TLargeintField;
    QrTXMovDstSdoVrtQtd: TFloatField;
    QrTXMovDstObserv: TWideStringField;
    QrTXMovDstSerieTal: TLargeintField;
    QrTXMovDstTalao: TLargeintField;
    QrTXMovDstFornecMO: TLargeintField;
    QrTXMovDstCustoMOUni: TFloatField;
    QrTXMovDstCustoMOTot: TFloatField;
    QrTXMovDstValorMP: TFloatField;
    QrTXMovDstDstMovID: TLargeintField;
    QrTXMovDstDstNivel1: TLargeintField;
    QrTXMovDstDstNivel2: TLargeintField;
    QrTXMovDstDstGGX: TLargeintField;
    QrTXMovDstQtdGer: TFloatField;
    QrTXMovDstQtdAnt: TFloatField;
    QrTXMovDstStqCenLoc: TLargeintField;
    QrTXMovDstReqMovEstq: TLargeintField;
    QrTXMovDstGraGru1: TLargeintField;
    QrTXMovDstNO_EstqMovimID: TWideStringField;
    QrTXMovDstNO_MovimID: TWideStringField;
    QrTXMovDstNO_MovimNiv: TWideStringField;
    QrTXMovDstNO_PALLET: TWideStringField;
    QrTXMovDstNO_PRD_TAM_COR: TWideStringField;
    QrTXMovDstNO_FORNECE: TWideStringField;
    QrTXMovDstNO_SerieTal: TWideStringField;
    QrTXMovDstNO_TTW: TWideStringField;
    QrTXMovDstID_TTW: TLargeintField;
    QrTXMovDstNO_LOC_CEN: TWideStringField;
    TPDataHora: TdmkEditDateTimePicker;
    Label58: TLabel;
    EdDataHora: TdmkEdit;
    QrTXMovSrcReqMovEstq: TLargeintField;
    QrTXMovItsCustoMOTot: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrTXMovItsAfterOpen(DataSet: TDataSet);
    procedure QrTXMovItsBeforeClose(DataSet: TDataSet);
    procedure QrTXMovItsCalcFields(DataSet: TDataSet);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FatorQtde, FatorValT: Integer;
    procedure ReopenTXMovIts(Controle: Integer);
  public
    { Public declarations }
    FAtualizaSaldoModoGenerico: Boolean;
    FTXMovIts: Integer;
    (*
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    *)
    //
    procedure DefineImeiAEditar(Controle: Integer);
  end;

  var
  FmTXMovItsAlt: TFmTXMovItsAlt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, UnTX_PF,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmTXMovItsAlt.BtOKClick(Sender: TObject);
var
  //Observ,
  DataHora, Marca, Observ: String;
  //Codigo,
  Controle,(*MovimCod,*) MovimNiv, (*MovimTwn, Empresa, Terceiro, CliVenda,*)
  MovimID(*, Pallet, GraGruX, SrcMovID, SrcNivel1,
  SrcNivel2, SrcGGX*), SerieTal, Talao (*FornecMO, DstMovID, DstNivel1,
  DstNivel2, DstGGX, AptoUso, TpCalcAuto*): Integer;
  Qtde, ValorT(*,
  SdoVrtQtd, CustoMOUni, CustoMOTot, ValorMP,
  QtdGer, QtdAnt*): Double;
  Zerado, StqCenLoc, ReqMovEstq: Integer;
begin
  //Codigo         := ;
  Controle       := QrTXMovItsControle.Value; // >> FTXMovIts
(*  MovimCod       := ;
*)
  MovimNiv       := QrTXMovItsMovimNiv.Value;;
(*  MovimTwn       := ;
  Empresa        := ;
  Terceiro       := ;
  CliVenda       := ;
*)
  MovimID        := QrTXMovItsMovimID.Value;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' + EdDataHora.Text;
(*
  Pallet         := ;
  GraGruX        := ;
*)
  Qtde           := EdQtde.ValueVariant  * FatorQtde;
  ValorT         := EdValorT.ValueVariant * FatorValT;
(*  SrcMovID       := ;
  SrcNivel1      := ;
  SrcNivel2      := ;
  SrcGGX         := ;
  SdoVrtQtd      := ;
*)
  Observ         := EdObserv.Text;
  SerieTal       := EdSerieTal.ValueVariant;
  Talao          := EdTalao.ValueVariant;;
(*
  FornecMO       := ;
  CustoMOUni     := ;
  CustoMOTot     := ;
  ValorMP        := ;
  DstMovID       := ;
  DstNivel1      := ;
  DstNivel2      := ;
  DstGGX         := ;
  QtdGer         := ;
  QtdAnt         := ;
  AptoUso        := ;
*)
  Marca          := EdMarca.Text;
(*  TpCalcAuto     := ;
*)
  Zerado   := Geral.BoolToInt(CkZerado.Checked);
  //
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
  (*'Codigo', 'MovimCod', 'MovimNiv',
  'MovimTwn', 'Empresa', 'Terceiro',
  'CliVenda', 'MovimID', *) CO_DATA_HORA_TMI, (*'Pallet',
  'GraGruX',*) 'Qtde', 'ValorT'(*,
  'SrcMovID', 'SrcNivel1', 'SrcNivel2',
  'SrcGGX', 'SdoVrtPeca', 'SdoVrtPeso',
  'SdoVrtArM2'*), 'Observ', 'SerieTal',
  'Talao'(*, 'FornecMO',
  'CustoMOUni', 'CustoMOTot', 'ValorMP',
  'DstMovID', 'DstNivel1', 'DstNivel2',
  'DstGGX', 'QtdGer', 'QtdAnt',
  'AptoUso'*), 'Marca',
  (*'TpCalcAuto'*)
  'Zerado',
  'StqCenLoc', 'ReqMovEstq'], [
  'Controle'], [
  (*Codigo, MovimCod, MovimNiv,
  MovimTwn, Empresa, Terceiro,
  CliVenda, MovimID,*)
  DataHora, (*Pallet,
  GraGruX,*) Qtde, ValorT(*,
  SrcMovID, SrcNivel1, SrcNivel2,
  SrcGGX, SdoVrtPeca, SdoVrtPeso,
  SdoVrtArM2*), Observ, SerieTal,
  Talao (*, FornecMO,
  CustoMOUni, CustoMOTot, ValorMP,
  DstMovID, DstNivel1, DstNivel2,
  DstGGX, QtdGer, QtdAnt,
  AptoUso*), Marca,
  (*TpCalcAuto*)
  Zerado,
  StqCenLoc, ReqMovEstq], [
  Controle], True) then
  begin
    if FAtualizaSaldoModoGenerico
    or (Zerado <> QrTXMovItsZerado.Value) then
      TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(Controle, MovimID, MovimNiv);
    Close;
  end;
end;

procedure TFmTXMovItsAlt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXMovItsAlt.FormActivate(Sender: TObject);
begin
(*
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
*)
  MyObjects.CorIniComponente();
end;

procedure TFmTXMovItsAlt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FAtualizaSaldoModoGenerico := False;
  //
  UnDmkDAC_PF.AbreQuery(QrTXSertal, Dmod.MyDB);
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmTXMovItsAlt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXMovItsAlt.QrTXMovItsAfterOpen(DataSet: TDataSet);
const
  CtrlSrc = 0;
  CtrlDst = 0;
  TemIMEIMrt = 1; // ver o que fazer !!
begin
  TX_PF.ReopenTXMovXXX(QrTXMovSrc, 'SrcNivel2', QrTXMovItsControle.Value, TemIMEIMrt, CtrlSrc);
  TX_PF.ReopenTXMovXXX(QrTXMovDst, 'DstNivel2', QrTXMovItsControle.Value, TemIMEIMrt, CtrlDst);
end;

procedure TFmTXMovItsAlt.QrTXMovItsBeforeClose(DataSet: TDataSet);
begin
  QrTXMovSrc.Close;
  QrTXMovDst.Close;
end;

procedure TFmTXMovItsAlt.QrTXMovItsCalcFields(DataSet: TDataSet);
begin
  QrTXMovItsNO_SrcMovID.Value    := sEstqMovimID[QrTXMovItsSrcMovID.Value];
  QrTXMovItsNO_DstMovID.Value    := sEstqMovimID[QrTXMovItsDstMovID.Value];
end;

procedure TFmTXMovItsAlt.DefineImeiAEditar(Controle: Integer);
begin
  FTXMovIts := Controle;
  ReopenTXMovIts(FTXMovIts);
  if QrTXMovItsQtde.Value < 0 then
    FatorQtde := -1
  else
    FatorQtde := 1;
  if QrTXMovItsValorT.Value < 0 then
    FatorValT := -1
  else
    FatorValT := 1;
  EdQtde.ValueVariant   := QrTXMovItsQtde.Value  * FatorQtde;
  EdValorT.ValueVariant := QrTXMovItsValorT.Value * FatorValT;
  EdObserv.ValueVariant := QrTXMovItsObserv.Value;
  //
  EdSerieTal.ValueVariant := QrTXMovItsSerieTal.Value;
  CBSerieTal.KeyValue     := QrTXMovItsSerieTal.Value;
  EdTalao.ValueVariant    := QrTXMovItsTalao.Value;
  EdMarca.Text            := QrTXMovItsMarca.Value;
  //
  CkZerado.Checked        := QrTXMovItsZerado.Value = 1;
  //
  EdStqCenLoc.ValueVariant  := QrTXMovItsStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrTXMovItsStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrTXMovItsReqMovEstq.Value;
  //
  EdObserv.Text             := QrTXMovItsObserv.Value;
  //
  TPDataHora.Date           := Trunc(QrTXMovItsDataHora.Value);
  EdDataHora.ValueVariant   := QrTXMovItsDataHora.Value;
  //
end;

procedure TFmTXMovItsAlt.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Preco, Qtde: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      Preco := Geral.DMV(Preco_TXT);
      Qtde  := EdQtde.ValueVariant;
      EdValorT.ValueVariant := Qtde * Preco
    end;
  end;
end;

procedure TFmTXMovItsAlt.ReopenTXMovIts(Controle: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('tmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('tmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXMovIts, Dmod.MyDB, [
  'SELECT tmi.*,',
  ATT_MovimID,
  ATT_MovimNiv,
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, ',
  'IF(tmi.Terceiro=0, "Desconhecido", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(tmi.Talao=0, "?????", CONCAT(IF(tst.Nome IS NULL, ',
  '"?????", tst.Nome), " ", tmi.Talao)) NO_TALAO ',
  //
  'FROM ' + CO_SEL_TAB_TMI + ' tmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=tmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN entidades  ent ON ent.Codigo=tmi.Terceiro',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'WHERE tmi.Controle=' + Geral.FF0(Controle),
  '']);
end;

end.
