unit TXPalletManual;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables;

type
  TFmTXPalletManual = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    EdPallet: TdmkEdit;
    Label1: TLabel;
    QrTXPallet: TmySQLQuery;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FPermiteIncluir: Boolean;
    FNumNewPallet: Integer;
  end;

  var
  FmTXPalletManual: TFmTXPalletManual;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnTX_PF, UnTX_Jan;

{$R *.DFM}

procedure TFmTXPalletManual.BtOKClick(Sender: TObject);
var
  Pallet: Integer;
  Pallet_Txt: String;
begin
  Pallet := EdPallet.ValueVariant;
  if MyObjects.FIC(Pallet = 0, EdPallet, 'Informe o n�mero do pallet!') then
    Exit;
  //
  Pallet_Txt := Geral.FF0(Pallet);
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPallet, Dmod.MyDB, [
  'SELECT * ',
  'FROM txpalleta ',
  'WHERE Codigo=' + Pallet_Txt,
  ' ']);
  if QrTXPallet.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('O pallet ' + Pallet_Txt + ' j� existe!' + sLineBreak
    + 'Desej� localiz�-lo?') = ID_Yes then
      TX_Jan.MostraFormTXPallet1(Pallet);
    //
    Close;
    Exit;
  end else
  begin
    FPermiteIncluir := True;
    FNumNewPallet   := EdPallet.ValueVariant;
    Close;
  end;
end;

procedure TFmTXPalletManual.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXPalletManual.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXPalletManual.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FPermiteIncluir := False;
  FNumNewPallet   := 0;
end;

procedure TFmTXPalletManual.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXPalletManual.SpeedButton1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Novo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Codigo) Codigo ',
    'FROM txpalleta ',
    '']);
    Novo := Qry.FieldByName('Codigo').AsInteger;
    // Evitar Nul !! ???
    Novo := Novo + 1;
    EdPallet.ValueVariant := Novo;
  finally
    Qry.Free;
  end;
end;

end.
