object FmTXExBItsIMEIs: TFmTXExBItsIMEIs
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-080 :: Baixas Extras de IMEIs Completos'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 419
        Height = 32
        Caption = 'Baixas Extras de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 419
        Height = 32
        Caption = 'Baixas Extras de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 419
        Height = 32
        Caption = 'Baixas Extras de IMEIs Completos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGIMEIs: TdmkDBGridZTO
          Left = 261
          Top = 15
          Width = 745
          Height = 450
          Align = alClient
          DataSource = DsIMEIs
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IMEI'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstGGX'
              Title.Caption = 'Reduzido'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigo'
              Width = 245
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtQtd'
              Title.Caption = 'Sdo Qtde'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 259
          Height = 450
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object DBGGraGruY: TdmkDBGridZTO
            Left = 0
            Top = 41
            Width = 259
            Height = 198
            Align = alClient
            DataSource = DsGraGruY
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnAfterMultiselect = DBGGraGruYAfterMultiselect
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 239
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 259
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object RGPosiNega: TRadioGroup
              Left = 0
              Top = 0
              Width = 259
              Height = 41
              Align = alClient
              Caption = ' Estoque: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Positivo'
                'Negativo')
              TabOrder = 0
              OnClick = RGPosiNegaClick
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 344
            Width = 259
            Height = 106
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 2
            object Label2: TLabel
              Left = 8
              Top = 4
              Width = 140
              Height = 13
              Caption = 'Data / hora "informada aqui":'
            end
            object Label1: TLabel
              Left = 8
              Top = 44
              Width = 123
              Height = 13
              Caption = 'Senha p/ uso IME-I vago:'
            end
            object CkContinuar: TCheckBox
              Left = 8
              Top = 84
              Width = 141
              Height = 17
              Caption = 'Continuar inserindo.'
              TabOrder = 0
            end
            object TPDataSenha: TdmkEditDateTimePicker
              Left = 7
              Top = 20
              Width = 112
              Height = 21
              Date = 42131.794453055550000000
              Time = 42131.794453055550000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdHoraSenha: TdmkEdit
              Left = 122
              Top = 20
              Width = 61
              Height = 21
              TabOrder = 2
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '20:00:00'
              QryCampo = 'DtCompra'
              UpdCampo = 'DtCompra'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.833333333333333400
              ValWarn = False
            end
            object EdSenha: TEdit
              Left = 6
              Top = 61
              Width = 125
              Height = 20
              Font.Charset = SYMBOL_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              PasswordChar = 'l'
              TabOrder = 3
            end
          end
          object RGDataHora: TRadioGroup
            Left = 0
            Top = 239
            Width = 259
            Height = 105
            Align = alBottom
            Caption = ' Fonte da data / hora de baixa:'
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o definido'
              'Do cabe'#231'alho'
              'Informada aqui (abaixo)'
              'Conforme '#250'ltima movimenta'#231#227'o de cada item')
            TabOrder = 3
            OnClick = RGDataHoraClick
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 736
        Top = 8
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtNenhumClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 616
        Top = 8
        Width = 120
        Height = 40
        Caption = '&Todos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
    end
  end
  object QrGraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'ORDER BY Ordem')
    Left = 92
    Top = 232
    object QrGraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object QrGraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrGraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsGraGruY: TDataSource
    DataSet = QrGraGruY
    Left = 92
    Top = 280
  end
  object QrIMEIs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 524
    Top = 376
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIMEIsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIMEIsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrIMEIsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrIMEIsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrIMEIsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrIMEIsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrIMEIsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrIMEIsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIMEIsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrIMEIsPallet: TIntegerField
      FieldName = 'Pallet'
      DisplayFormat = '0;-0; '
    end
    object QrIMEIsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIMEIsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrIMEIsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrIMEIsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrIMEIsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrIMEIsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrIMEIsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrIMEIsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrIMEIsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrIMEIsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrIMEIsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrIMEIsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrIMEIsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrIMEIsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrIMEIsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrIMEIsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrIMEIsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrIMEIsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrIMEIsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrIMEIsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrIMEIsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEIsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIMEIsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIMEIsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIMEIsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIMEIsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIMEIsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIMEIsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIMEIsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrIMEIsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrIMEIsTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrIMEIsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrIMEIsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIsSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrIMEIsTalao: TIntegerField
      FieldName = 'Talao'
    end
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 524
    Top = 424
  end
  object QrLastMov: TMySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 168
    object QrLastMovDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
end
