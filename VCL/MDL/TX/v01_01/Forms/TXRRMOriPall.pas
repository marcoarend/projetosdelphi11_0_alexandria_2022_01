unit TXRRMOriPall;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, UnAppEnums, UnGrl_Vars;

type
  TFmTXRRMOriPall = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    Label6: TLabel;
    LaQtde: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdQtde: TdmkEdit;
    EdObserv: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstqR4: TmySQLQuery;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Qtde: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Terceiro: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    DsEstqR4: TDataSource;
    QrTXMovIts: TmySQLQuery;
    QrTXMovItsNO_Pallet: TWideStringField;
    QrTXMovItsCodigo: TIntegerField;
    QrTXMovItsControle: TIntegerField;
    QrTXMovItsMovimCod: TIntegerField;
    QrTXMovItsMovimNiv: TIntegerField;
    QrTXMovItsEmpresa: TIntegerField;
    QrTXMovItsTerceiro: TIntegerField;
    QrTXMovItsMovimID: TIntegerField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TIntegerField;
    QrTXMovItsGraGruX: TIntegerField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsSrcMovID: TIntegerField;
    QrTXMovItsSrcNivel1: TIntegerField;
    QrTXMovItsSrcNivel2: TIntegerField;
    QrTXMovItsLk: TIntegerField;
    QrTXMovItsDataCad: TDateField;
    QrTXMovItsDataAlt: TDateField;
    QrTXMovItsUserCad: TIntegerField;
    QrTXMovItsUserAlt: TIntegerField;
    QrTXMovItsAlterWeb: TSmallintField;
    QrTXMovItsAtivo: TSmallintField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsNO_FORNECE: TWideStringField;
    QrTXMovItsValorT: TFloatField;
    QrTXMovItsTalao: TIntegerField;
    QrTXMovItsSerieTal: TIntegerField;
    QrTXMovItsMovimTwn: TIntegerField;
    QrTXMovItsCliVenda: TIntegerField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsFornecMO: TIntegerField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsCustoMOTot: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TIntegerField;
    QrTXMovItsDstNivel1: TIntegerField;
    QrTXMovItsDstNivel2: TIntegerField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsAptoUso: TSmallintField;
    QrTXMovItsSrcGGX: TIntegerField;
    QrTXMovItsDstGGX: TIntegerField;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsGraGruY: TIntegerField;
    QrEstqR4Talao: TIntegerField;
    QrEstqR4SerieTal: TIntegerField;
    QrEstqR4SdoVrtQtd: TFloatField;
    QrEstqR4DataHora: TDateTimeField;
    CkSemi: TCheckBox;
    Panel6: TPanel;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    QrTXMovItsTXMulFrnCab: TIntegerField;
    CkPalEspecificos: TCheckBox;
    EdPalEspecificos: TdmkEdit;
    EdSelQtde: TdmkEdit;
    Label3: TLabel;
    QrTXMovItsClientMO: TIntegerField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Panel3: TPanel;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    QrEstqR4NFeNum: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdQtdeChange(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure CkSemiClick(Sender: TObject);
    procedure DBG04EstqAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    //
    procedure AdicionaPallet();
    procedure InsereIMEI_Atual(InsereTudo: Boolean; ParcQtde: Double);
    procedure FechaPesquisa();
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    //procedure ReopenTXGerArtSrc(Controle: Integer);
    //function  ValorTParcial(): Double;
    procedure LiberaEdicao(Libera, Avisa: Boolean);
  public
    { Public declarations }
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FStqCenLoc, FFornecMO: Integer;
    FDataHora: TDateTime;
    FParcial: Boolean;
    //
    function  BaixaPalletTotal(): Integer;
    function  BaixaPalletParcial(): Integer;
    procedure ReopenItensAptos();
    procedure ReopenGraGruX();
  end;

  var
  FmTXRRMOriPall: TFmTXRRMOriPall;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXRRMCab, UnTX_PF, AppListas, UnGrade_PF;

{$R *.DFM}

procedure TFmTXRRMOriPall.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

procedure TFmTXRRMOriPall.AdicionaPallet();
begin
  TX_PF.ReopenTXMovIts_Pallet2(QrTXMovIts, QrEstqR4);
  QrTXMovIts.First;
  while not QrTXMovIts.Eof do
  begin
    InsereIMEI_Atual(True, 0);
    QrTXMovIts.Next;
  end;
end;

function TFmTXRRMOriPall.BaixaPalletParcial(): Integer;
var
  Qtde: Double;
  SobraQtde: Double;
begin
  Result := 0;
  Qtde   := EdQtde.ValueVariant;
  //
  SobraQtde   := Qtde;
  //
  if MyObjects.FIC(Qtde <= 0, EdQtde, 'Informe a quantidade!') then
    Exit;
  TX_PF.ReopenTXMovIts_Pallet2(QrTXMovIts, QrEstqR4);
  QrTXMovIts.Last;
  while not QrTXMovIts.Bof do
  begin
    if SobraQtde > 0 then
    begin
      if SobraQtde >= QrTXMovItsSdoVrtQtd.Value then
      begin
        InsereIMEI_Atual(True, 0);
        SobraQtde  := SobraQtde  - QrTXMovItsSdoVrtQtd.Value;
        Result := Result + 1;
      end else
      if SobraQtde < QrTXMovItsSdoVrtQtd.Value then
      begin
        InsereIMEI_Atual(False, SobraQtde);
        SobraQtde   := 0;
        Result := Result + 1;
        //
        QrTXMovIts.Prior;
        Exit;
      end;
    end;
    QrTXMovIts.Prior;
  end;
  // Inserir se ficar sobra mesmo que a origem fique negativa!
  if SobraQtde > 0 then
  begin
    InsereIMEI_Atual(False, SobraQtde);
    Result := Result + 1;
  end;
end;

function TFmTXRRMOriPall.BaixaPalletTotal(): Integer;
var
  N, I, Codigo, MovimCod: Integer;
begin
  QrEstqR4.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      AdicionaPallet();
    end;
(*
    // Nao se aplica. Calcula com function propria a seguir.
    //TX_PF.AtualizaTotaisVSXxxCab('VSRRMcab', MovimCod);
    MovimCod := FmTXRRMCab.QrTXRRMCabMovimCod.Value;
    TX_PF.AtualizaTotaisVSRRMCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmTXRRMCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinuar.Checked then
      begin
        ReopenItensAptos();
        MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      end else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
*)
  finally
    QrEstqR4.EnableControls;
  end;
  Result := N;
end;

procedure TFmTXRRMOriPall.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo: Integer;
begin
  if FParcial then
    N := BaixaPalletParcial()
  else
    N := BaixaPalletTotal();
  //
  // Nao se aplica. Calcula com function propria a seguir.
  //TX_PF.AtualizaTotaisVSXxxCab('VSRRMcab', MovimCod);
  MovimCod := FmTXRRMCab.QrTXRRMCabMovimCod.Value;
  TX_PF.AtualizaTotaisTXRRMCab(MovimCod);
  //
  Codigo := EdCodigo.ValueVariant;
  FmTXRRMCab.LocCod(Codigo, Codigo);
  if N > 0 then
  begin
    if CkContinuar.Checked then
    begin
      EdControle.ValueVariant := 0;
      EdQtde.ValueVariant     := 0;
      EdObserv.ValueVariant   := '';
      //
      ReopenItensAptos();
      MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      LiberaEdicao(False, False);
    end else
      Close;
  end else
    Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
end;

procedure TFmTXRRMOriPall.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXRRMOriPall.CkSemiClick(Sender: TObject);
begin
  ReopenGraGruX();
end;

procedure TFmTXRRMOriPall.DBG04EstqAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
var
  I: Integer;
  Qtde: Double;
begin
  Qtde  := 0;
  //
  if DBG04Estq.SelectedRows.Count > 0 then
  begin
    with DBG04Estq.DataSource.DataSet do
    for i:= 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[i]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[i]);
      Qtde   := Qtde  + QrEstqR4Qtde.Value;
      //
    end;
  end;
  EdSelQtde.ValueVariant   := Qtde;
end;

procedure TFmTXRRMOriPall.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmTXRRMOriPall.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriPall.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriPall.EdQtdeChange(Sender: TObject);
(*
var
  Pecas: Double;
*)
begin
(*
  // ver pelas pecas se eh o resto e pegar todo resto do peso
  Pecas := EdPecas.ValueVariant;
  if (QrAptos.State <> dsInactive) and (QrAptos.RecordCount > 0)
  and (Pecas >= QrAptosSdoVrtPeca.Value) then
  begin
    EdPesoKg.ValueVariant  := QrAptosSdoVrtPeso.Value;
    LaPesoKg.Enabled  := False;
    EdPesoKg.Enabled  := False;
  end else
  begin
    LaPesoKg.Enabled  := GBGerar.Visible;
    EdPesoKg.Enabled  := GBGerar.Visible;
  end;
*)
end;

procedure TFmTXRRMOriPall.EdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdQtde.ValueVariant  := QrEstqR4SdoVrtQtd.Value;
end;

procedure TFmTXRRMOriPall.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriPall.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriPall.FechaPesquisa();
begin
  QrEstqR4.Close;
end;

procedure TFmTXRRMOriPall.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    EdReqMovEstq.SetFocus;
  except
    //
  end;
end;

procedure TFmTXRRMOriPall.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FParcial := False;
  TX_PF.AbreTXSerTal(QrTXSerTal);
  //ReopenGraGruX();
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
end;

procedure TFmTXRRMOriPall.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXRRMOriPall.InsereIMEI_Atual(InsereTudo: Boolean; ParcQtde: Double);
const
  Observ = '';
  MovimTwn   = 0;
  ExigeFornecedor = False;
(*
  EdTalao     = nil;
  EdPallet    = nil;
  EdValorT    = nil;
  EdStqCenLoc = nil;
  EdSerieTal  = nil;
*)
  CustoMOUni  = 0;
  CustoMOTot  = 0;
  QtdGer      = 0;
  AptoUso     = 0; // Eh venda!
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  ItemNFe    = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, TXMulFrnCab,
  SerieTal, Talao, DstNivel1, DstNivel2, GraGruY, SrcGGX, DstGGX, ReqMovEstq,
  ClientMO, GGXRcl, StqCenLoc, FornecMO: Integer;
  Qtde, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
begin
  SrcMovID       := QrTXMovItsMovimID.Value;
  SrcNivel1      := QrTXMovItsCodigo.Value;
  SrcNivel2      := QrTXMovItsControle.Value;
  SrcGGX         := QrTXMovItsGraGruX.Value;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  Terceiro       := QrTXMovItsTerceiro.Value;
  TXMulFrnCab    := QrTXMovItsTXMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEmReprRM;
  MovimNiv       := eminSorcRRM;
  Pallet         := QrTXMovItsPallet.Value;
  GraGruX        := QrTXMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not TX_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  if not InsereTudo then
  begin
    Qtde         := -ParcQtde;
  end else
  begin
    Qtde           := -QrTXMovItsSdoVrtQtd.Value;
  end;
  if QrTXMovItsQtde.Value > 0 then
    Valor := (-Qtde) *
    (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value)
  else
    Valor := 0;
  //
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  SerieTal       := 0; //QrTXMovItsSerieFch.Value;
  Talao          := 0;  //QrTXMovItsFicha.Value;
  Marca          := ''; //QrTXMovItsMarca.Value;
  //Misturou       := QrTXMovItsMisturou.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrTXMovItsGraGruY.Value;
  //
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  ClientMO       := QrTXMovItsClientMO.Value;
  StqCenLoc      := FStqCenLoc;
  FornecMo       := FFornecMO;
  //
  Controle := UMyMod.BPGS1I32(CO_SEL_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //

(*
  if TX_PF.TXFic(GraGruX, Empresa, Terceiro, Pallet, Qtde, ValorT,
  EdGraGruX, EdPallet, EdQtde, EdValorT, ExigeFornecedor, GraGruY, EdStqCenLoc,
  SerieTal, Talao, EdSerieTal, EdTalao) then
    Exit;
*)

  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, (*Fornecedor*)Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGer, AptoUso, FornecMO,
  SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei089(*Baixa de material em reprocesso/reparo por pallet*)) then
  begin
    TX_PF.AtualizaSaldoIMEI(SrcNivel2, True);
  end;
end;

procedure TFmTXRRMOriPall.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status: Boolean;
begin
  if FParcial then
  begin
    if Libera then
      Status := QrEstqR4.RecordCount > 0
    else
      Status := False;
    //
    GBAptos.Enabled := not Status;
    GBGerar.Visible := Status;
    //
    LaFicha.Enabled := not Status;
    EdFicha.Enabled := not Status;
    LaGraGruX.Enabled := not Status;
    EdGraGruX.Enabled := not Status;
    CBGraGruX.Enabled := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled := not Status;
    //
    if Libera and (EdQtde.Enabled) and (EdQtde.Visible) then
      EdQtde.SetFocus;
  end else
    if Avisa then
      Geral.MB_Aviso(
      'Para inclu�o parcial feche a janela e selecione a op��o apropriada!');
end;

procedure TFmTXRRMOriPall.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmTXRRMOriPall.ReopenGraGruX();
(*
var
  SQL_Semi: String;
*)
begin
(*
  if CkSemi.Checked then
    SQL_Semi := ',' + Geral.FF0(CO_GraGruY_6144_VSFinCla)
  else
    SQL_Semi := '';
*)
  TX_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GragruY<>0');
(*    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_VSRibCla) +  SQL_Semi +
    ')');
*)
  TX_PF.AbreGraGruXY(QrGGXRcl, 'AND ggx.GragruY<>0');
(*
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_VSRibCla) +  SQL_Semi +
    ')');
*)
end;

procedure TFmTXRRMOriPall.ReopenItensAptos();
const
  //GraGruX   = 0;
  Pallet    = 0;
  Terceiro  = 0;
  CouNiv2   = 0;
  CouNiv1   = 0;
  GraGruYs  = '';
  StqCenCad = 0;
  StqCenLoc = 0;
  MovimID   = 0;
  MovimCod  = 0;
var
  GraGruX: TPallArr;
  SQL_Especificos: String;
begin
  TX_PF.SetaGGXUnicoEmLista(EdGraGruX.ValueVariant, GraGruX);
  //
  if CkPalEspecificos.Checked and (Trim(EdPalEspecificos.Text) <> '') then
    SQL_Especificos := ' vmi.Pallet IN (' + Trim(EdPalEspecificos.Text) + ') '
  else
    SQL_Especificos := '';
  //
  if TX_PF.PesquisaPallets(FEmpresa, FClientMO, CouNiv2, CouNiv1, StqCenCad,
  StqCenLoc, Self.Name, GraGruYs, GraGruX, [], SQL_Especificos, TEstqMovimID(
  MovimID), MovimCod, FVSMovImp4)
  then
    TX_PF.ReopenTXListaPallets(QrEstqR4, FVSMovImp4,
    FEmpresa, EdGraGruX.ValueVariant, Pallet, Terceiro, '');
end;

{
procedure TFmTXRRMOri.ReopenTXGerArtSrc(Controle: Integer);
begin
  if FVSRRMOriIMEI <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FVSRRMOriIMEI, Dmod.MyDB);
    if Controle <> 0 then
      FVSRRMOriIMEI.Locate('Controle', Controle, []);
  end;
  if FVSRRMOriPallet <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FVSRRMOriPallet, Dmod.MyDB);
    if Controle <> 0 then
      FVSRRMOriPallet.Locate('Pallet', Pallet, []);
  end;
end;
}

{
function TFmTXRRMOri.ValorTParcial(): Double;
begin
  if QrAptosPesoKg.Value > 0 then
    Result := EdPesoKg.ValueVariant / QrAptosPesoKg.Value * QrAptosValorT.Value
  else
    Result := 0;
end;
}

end.
