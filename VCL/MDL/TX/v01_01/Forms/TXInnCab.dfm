object FmTXInnCab: TFmTXInnCab
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-006 :: Entrada de Mat'#233'ria Prima'
  ClientHeight = 707
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 601
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 181
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 468
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 584
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 700
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 828
        Top = 136
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label9: TLabel
        Left = 916
        Top = 136
        Width = 50
        Height = 13
        Caption = 'Valor total:'
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object Label15: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object Label24: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object Label25: TLabel
        Left = 508
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label26: TLabel
        Left = 508
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label27: TLabel
        Left = 744
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label32: TLabel
        Left = 496
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label33: TLabel
        Left = 532
        Top = 136
        Width = 62
        Height = 13
        Caption = 'NFe entrada:'
      end
      object Label34: TLabel
        Left = 620
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label35: TLabel
        Left = 656
        Top = 136
        Width = 59
        Height = 13
        Caption = 'NFe fornec.:'
      end
      object Label38: TLabel
        Left = 816
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data encer. rendim.:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXInnCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXInnCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 333
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXInnCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 468
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtCompra'
        DataSource = DsTXInnCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 584
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsTXInnCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 700
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtEntrada'
        DataSource = DsTXInnCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsTXInnCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsTXInnCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 564
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsTXInnCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsTXInnCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsTXInnCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 828
        Top = 152
        Width = 84
        Height = 21
        DataField = 'Qtde'
        DataSource = DsTXInnCab
        TabOrder = 11
      end
      object DBEdit15: TDBEdit
        Left = 916
        Top = 152
        Width = 84
        Height = 21
        DataField = 'ValorT'
        DataSource = DsTXInnCab
        TabOrder = 12
      end
      object DBEdit16: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsTXInnCab
        TabOrder = 13
      end
      object DBEdit17: TDBEdit
        Left = 72
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsTXInnCab
        TabOrder = 14
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'Motorista'
        DataSource = DsTXInnCab
        TabOrder = 15
      end
      object DBEdit19: TDBEdit
        Left = 72
        Top = 152
        Width = 421
        Height = 21
        DataField = 'NO_MOTORISTA'
        DataSource = DsTXInnCab
        TabOrder = 16
      end
      object DBEdit20: TDBEdit
        Left = 744
        Top = 152
        Width = 80
        Height = 21
        DataField = 'Placa'
        DataSource = DsTXInnCab
        TabOrder = 17
      end
      object DBEdit21: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Procednc'
        DataSource = DsTXInnCab
        TabOrder = 18
      end
      object DBEdit22: TDBEdit
        Left = 72
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_PROCEDNC'
        DataSource = DsTXInnCab
        TabOrder = 19
      end
      object DBEdit23: TDBEdit
        Left = 496
        Top = 152
        Width = 33
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsTXInnCab
        TabOrder = 20
      end
      object DBEdit24: TDBEdit
        Left = 532
        Top = 152
        Width = 85
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsTXInnCab
        TabOrder = 21
      end
      object DBEdit25: TDBEdit
        Left = 620
        Top = 152
        Width = 33
        Height = 21
        DataField = 'emi_serie'
        DataSource = DsTXInnCab
        TabOrder = 22
      end
      object DBEdit26: TDBEdit
        Left = 656
        Top = 152
        Width = 85
        Height = 21
        DataField = 'emi_nNF'
        DataSource = DsTXInnCab
        TabOrder = 23
      end
      object DBEdit27: TDBEdit
        Left = 816
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtEnceRend_TXT'
        DataSource = DsTXInnCab
        TabOrder = 24
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 537
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 74
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 248
        Top = 15
        Width = 758
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 625
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 291
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 435
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 4
      Top = 216
      Width = 1008
      Height = 285
      ActivePage = TabSheet1
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Dados da entrada'
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GBIts: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 257
            Align = alClient
            Caption = ' Itens da entrada: '
            TabOrder = 0
            object Splitter3: TSplitter
              Left = 2
              Top = 96
              Width = 996
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitTop = 99
              ExplicitWidth = 998
            end
            object DGDados: TDBGrid
              Left = 2
              Top = 15
              Width = 996
              Height = 81
              Align = alClient
              DataSource = DsTXInnIts
              PopupMenu = PMVInnIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TTW'
                  Title.Caption = 'Tabela'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'IME-I'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'StqCenLoc'
                  Title.Caption = 'Local'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ReqMovEstq'
                  Title.Caption = 'N'#176' RME'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Marca'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Mat'#233'ria-prima'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorT'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SdoVrtQtd'
                  Title.Caption = 'Sdo virtual p'#231
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdGer'
                  Title.Caption = 'm'#178' gerados'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 300
                  Visible = True
                end>
            end
            object Panel7: TPanel
              Left = 2
              Top = 101
              Width = 996
              Height = 154
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object Splitter4: TSplitter
                Left = 993
                Top = 0
                Height = 154
                Align = alRight
                ExplicitLeft = 622
              end
              object GroupBox2: TGroupBox
                Left = 0
                Top = 0
                Width = 993
                Height = 154
                Align = alClient
                Caption = 'Itens baixados do item de entrada selecionado acima: '
                TabOrder = 0
                object DBGrid1: TDBGrid
                  Left = 2
                  Top = 15
                  Width = 989
                  Height = 137
                  Align = alClient
                  DataSource = DsTXItsBxa
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_TTW'
                      Title.Caption = 'Tabela'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'IME-I'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Qtde'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorT'
                      Title.Caption = 'Valor total'
                      Width = 72
                      Visible = True
                    end>
                end
              end
            end
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 601
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 504
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label5: TLabel
        Left = 504
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object LaQtde: TLabel
        Left = 816
        Top = 136
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object SbFornecedor: TSpeedButton
        Left = 968
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbFornecedorClick
      end
      object SbTransportador: TSpeedButton
        Left = 968
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTransportadorClick
      end
      object Label20: TLabel
        Left = 16
        Top = 56
        Width = 201
        Height = 13
        Caption = 'Dono do material (Cliente de m'#227'o-de-obra):'
      end
      object SbClienteMO: TSpeedButton
        Left = 480
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteMOClick
      end
      object Label21: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object SbProcedenc: TSpeedButton
        Left = 480
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbProcedencClick
      end
      object Label22: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object SbMotorista: TSpeedButton
        Left = 492
        Top = 152
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMotoristaClick
      end
      object Label23: TLabel
        Left = 516
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label28: TLabel
        Left = 600
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label29: TLabel
        Left = 636
        Top = 136
        Width = 62
        Height = 13
        Caption = 'NFe entrada:'
      end
      object Label30: TLabel
        Left = 708
        Top = 136
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label31: TLabel
        Left = 744
        Top = 136
        Width = 59
        Height = 13
        Caption = 'NFe fornec.:'
      end
      object Label17: TLabel
        Left = 904
        Top = 136
        Width = 50
        Height = 13
        Caption = 'Valor total:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtCompra: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtCompra: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrada: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdDtEntrada: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFornecedor: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornecedor'
        UpdCampo = 'Fornecedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 13
        dmkEditCB = EdFornecedor
        QryCampo = 'Fornecedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 17
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdQtde: TdmkEdit
        Left = 816
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 25
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClienteMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteMO'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClienteMO
        TabOrder = 11
        dmkEditCB = EdClienteMO
        QryCampo = 'ClienteMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProcednc: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Procednc'
        UpdCampo = 'Procednc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProcednc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProcednc: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcednc
        TabOrder = 15
        dmkEditCB = EdProcednc
        QryCampo = 'Procednc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdMotorista: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Motorista'
        UpdCampo = 'Motorista'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotorista
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotorista: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 417
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsMotorista
        TabOrder = 19
        dmkEditCB = EdMotorista
        QryCampo = 'Motorista'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPlaca: TdmkEdit
        Left = 516
        Top = 152
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 20
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Placa'
        UpdCampo = 'Placa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edide_serie: TdmkEdit
        Left = 600
        Top = 152
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 636
        Top = 152
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = Edide_nNFKeyDown
      end
      object Edemi_serie: TdmkEdit
        Left = 708
        Top = 152
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_serie'
        UpdCampo = 'emi_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_nNF: TdmkEdit
        Left = 744
        Top = 152
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_nNF'
        UpdCampo = 'emi_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = Edemi_nNFKeyDown
      end
      object EdValorT: TdmkEdit
        Left = 904
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 26
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'ValorT'
        UpdCampo = 'ValorT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 538
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 245
      Width = 1008
      Height = 54
      Align = alTop
      Caption = ' Avisos: '
      TabOrder = 2
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 37
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label36: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label37: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object ProgressBar1: TProgressBar
          Left = 0
          Top = 20
          Width = 1004
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 309
        Height = 32
        Caption = 'Entrada de Mat'#233'ria Prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 309
        Height = 32
        Caption = 'Entrada de Mat'#233'ria Prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 309
        Height = 32
        Caption = 'Entrada de Mat'#233'ria Prima'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 54
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 20
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 420
    Top = 44
  end
  object QrTXInnCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXInnCabBeforeOpen
    AfterOpen = QrTXInnCabAfterOpen
    BeforeClose = QrTXInnCabBeforeClose
    AfterScroll = QrTXInnCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      
        'IF(wic.DtEnceRend < "1900-01-01", "", DATE_FORMAT(wic.DtEnceRend' +
        ', "%d/%m/%y %h:%i")) DtEnceRend_TXT,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,'
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,'
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA'
      'FROM txinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta'
      'LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO'
      'LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc'
      'LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista'
      ''
      'WHERE wic.Codigo > 0'
      'AND wic.Codigo=:P0')
    Left = 208
    Top = 53
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXInnCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXInnCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXInnCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXInnCabDtCompra: TDateTimeField
      FieldName = 'DtCompra'
    end
    object QrTXInnCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrTXInnCabDtEntrada: TDateTimeField
      FieldName = 'DtEntrada'
    end
    object QrTXInnCabFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrTXInnCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrTXInnCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXInnCabValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXInnCabClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrTXInnCabProcednc: TIntegerField
      FieldName = 'Procednc'
    end
    object QrTXInnCabMotorista: TIntegerField
      FieldName = 'Motorista'
    end
    object QrTXInnCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrTXInnCabTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
    object QrTXInnCabide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrTXInnCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrTXInnCabemi_serie: TIntegerField
      FieldName = 'emi_serie'
    end
    object QrTXInnCabemi_nNF: TIntegerField
      FieldName = 'emi_nNF'
    end
    object QrTXInnCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrTXInnCabTXVmcWrn: TSmallintField
      FieldName = 'TXVmcWrn'
    end
    object QrTXInnCabTXVmcObs: TWideStringField
      FieldName = 'TXVmcObs'
      Size = 60
    end
    object QrTXInnCabTXVmcSeq: TWideStringField
      FieldName = 'TXVmcSeq'
      Size = 25
    end
    object QrTXInnCabTXVmcSta: TSmallintField
      FieldName = 'TXVmcSta'
    end
    object QrTXInnCabDtEnceRend: TDateTimeField
      FieldName = 'DtEnceRend'
    end
    object QrTXInnCabTpEnceRend: TSmallintField
      FieldName = 'TpEnceRend'
    end
    object QrTXInnCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXInnCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXInnCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXInnCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXInnCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXInnCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXInnCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXInnCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXInnCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXInnCabDtEnceRend_TXT: TWideStringField
      FieldName = 'DtEnceRend_TXT'
      Size = 14
    end
    object QrTXInnCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXInnCabNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXInnCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrTXInnCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrTXInnCabNO_PROCEDNC: TWideStringField
      FieldName = 'NO_PROCEDNC'
      Size = 100
    end
    object QrTXInnCabNO_MOTORISTA: TWideStringField
      FieldName = 'NO_MOTORISTA'
      Size = 100
    end
  end
  object DsTXInnCab: TDataSource
    DataSet = QrTXInnCab
    Left = 212
    Top = 97
  end
  object QrTXInnIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXInnItsBeforeClose
    AfterScroll = QrTXInnItsAfterScroll
    SQL.Strings = (
      'SELECT  "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, '
      'CAST(vmi.Codigo AS SIGNED) Codigo, '
      'CAST(vmi.Controle AS SIGNED) Controle, '
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, '
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, '
      'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, '
      'CAST(vmi.Empresa AS SIGNED) Empresa, '
      'CAST(vmi.ClientMO AS SIGNED) ClientMO, '
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, '
      'CAST(vmi.CliVenda AS SIGNED) CliVenda, '
      'CAST(vmi.MovimID AS SIGNED) MovimID, '
      'CAST(vmi.DataHora AS DATETIME) DataHora, '
      'CAST(vmi.Pallet AS SIGNED) Pallet, '
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, '
      'CAST(vmi.Qtde  AS DECIMAL (15,3)) Qtde, '
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, '
      'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, '
      'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, '
      'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, '
      'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, '
      'CAST(vmi.SdoVrtQtd AS DECIMAL (15,3)) SdoVrtQtd, '
      'CAST(vmi.Observ AS CHAR) Observ, '
      'CAST(vmi.FornecMO AS SIGNED) FornecMO, '
      'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, '
      'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, '
      'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, '
      'CAST(vmi.DstMovID AS SIGNED) DstMovID, '
      'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, '
      'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, '
      'CAST(vmi.DstGGX AS SIGNED) DstGGX, '
      'CAST(vmi.QtdGer AS DECIMAL (15,3)) QtdGer, '
      'CAST(vmi.QtdAnt AS DECIMAL (15,3)) QtdAnt, '
      'CAST(vmi.Marca AS CHAR) Marca, '
      'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, '
      'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, '
      'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, '
      'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, '
      'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, '
      'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, '
      'CAST(vmi.TXMulFrnCab AS SIGNED) TXMulFrnCab, '
      'CAST(vmi.NFeSer + 0.000 AS SIGNED) NFeSer, '
      'CAST(vmi.NFeNum AS SIGNED) NFeNum, '
      'CAST(vmi.TXMulNFeCab AS SIGNED) TXMulNFeCab, '
      'CAST(vmi.JmpMovID AS SIGNED) JmpMovID, '
      'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1, '
      'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2, '
      'CAST(vmi.RmsMovID AS SIGNED) RmsMovID, '
      'CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1, '
      'CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2, '
      'CAST(vmi.GGXRcl AS SIGNED) GGXRcl, '
      'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, '
      'CAST(vmi.JmpGGX AS SIGNED) JmpGGX, '
      'CAST(vmi.DtCorrApo AS DATETIME) DtCorrApo, '
      'CAST(vmi.IxxMovIX AS UNSIGNED) IxxMovIX, '
      'CAST(vmi.IxxFolha AS SIGNED) IxxFolha, '
      'CAST(vmi.IxxLinha AS SIGNED) IxxLinha, '
      'CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome' +
        '))) '
      'NO_PRD_TAM_COR, '
      ''
      'txp.Nome NO_Pallet, '
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED '
      ''
      ''
      'FROM txmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      ''
      'LEFT JOIN txpalleta  txp ON txp.Codigo=vmi.Pallet '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      ''
      ''
      'WHERE vmi.MovimCod=1')
    Left = 292
    Top = 53
    object QrTXInnItsNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrTXInnItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrTXInnItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXInnItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXInnItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXInnItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXInnItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXInnItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXInnItsClientMO: TLargeintField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrTXInnItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXInnItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXInnItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXInnItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXInnItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXInnItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXInnItsQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
      DisplayFormat = '#,###.##0.000;-#,###.##0.000; '
    end
    object QrTXInnItsValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXInnItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXInnItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXInnItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXInnItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXInnItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      Required = True
    end
    object QrTXInnItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXInnItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
      Required = True
    end
    object QrTXInnItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXInnItsValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXInnItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXInnItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXInnItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXInnItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXInnItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXInnItsQtdGer: TFloatField
      FieldName = 'QtdGer'
      Required = True
    end
    object QrTXInnItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      Required = True
    end
    object QrTXInnItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXInnItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrTXInnItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrTXInnItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrTXInnItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
      Required = True
    end
    object QrTXInnItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrTXInnItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrTXInnItsTXMulFrnCab: TLargeintField
      FieldName = 'TXMulFrnCab'
      Required = True
    end
    object QrTXInnItsTXMulNFeCab: TLargeintField
      FieldName = 'TXMulNFeCab'
      Required = True
    end
    object QrTXInnItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXInnItsIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
      Required = True
    end
    object QrTXInnItsIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
      Required = True
    end
    object QrTXInnItsIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
      Required = True
    end
    object QrTXInnItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXInnItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrTXInnItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrTXInnItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsTXInnIts: TDataSource
    DataSet = QrTXInnIts
    Left = 292
    Top = 101
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 96
    Top = 508
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Atualizaestoque1: TMenuItem
      Caption = 'Atuali&za estoque'
      OnClick = Atualizaestoque1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 44
    Top = 508
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 692
    Top = 4
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 692
    Top = 48
  end
  object QrTransporta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 768
    Top = 4
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 768
    Top = 48
  end
  object QrTXItsBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 840
    Top = 1
    object QrTXItsBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXItsBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXItsBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXItsBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXItsBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXItsBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXItsBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXItsBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXItsBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXItsBxaClientMO: TLargeintField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrTXItsBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXItsBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXItsBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXItsBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXItsBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXItsBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXItsBxaQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
      DisplayFormat = '#,###.##0.000;-#,###.##0.000; '
    end
    object QrTXItsBxaValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXItsBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXItsBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXItsBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXItsBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXItsBxaSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      Required = True
    end
    object QrTXItsBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXItsBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
      Required = True
    end
    object QrTXItsBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXItsBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXItsBxaCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
      DisplayFormat = '#,###.##0.00;-#,###.##0.00; '
    end
    object QrTXItsBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXItsBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXItsBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXItsBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXItsBxaQtdGer: TFloatField
      FieldName = 'QtdGer'
      Required = True
    end
    object QrTXItsBxaQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      Required = True
    end
    object QrTXItsBxaMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXItsBxaPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrTXItsBxaPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrTXItsBxaPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrTXItsBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
      Required = True
    end
    object QrTXItsBxaStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrTXItsBxaItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrTXItsBxaTXMulFrnCab: TLargeintField
      FieldName = 'TXMulFrnCab'
      Required = True
    end
    object QrTXItsBxaTXMulNFeCab: TLargeintField
      FieldName = 'TXMulNFeCab'
      Required = True
    end
    object QrTXItsBxaDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXItsBxaIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
      Required = True
    end
    object QrTXItsBxaIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
      Required = True
    end
    object QrTXItsBxaIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
      Required = True
    end
    object QrTXItsBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsTXItsBxa: TDataSource
    DataSet = QrTXItsBxa
    Left = 840
    Top = 49
  end
  object PMImprime: TPopupMenu
    Left = 28
    Top = 36
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
  end
  object QrClienteMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 548
    Top = 496
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteMO: TDataSource
    DataSet = QrClienteMO
    Left = 548
    Top = 544
  end
  object QrProcednc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 628
    Top = 496
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsProcednc: TDataSource
    DataSet = QrProcednc
    Left = 628
    Top = 544
  end
  object QrMotorista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 480
    Top = 496
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 480
    Top = 544
  end
  object PMVInnIts: TPopupMenu
    Left = 148
    Top = 288
  end
end
