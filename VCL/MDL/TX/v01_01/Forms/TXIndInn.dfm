object FmTXIndInn: TFmTXIndInn
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-039 :: Item de Artigo em Industrializa'#231#227'o'
  ClientHeight = 487
  ClientWidth = 760
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 112
    Width = 760
    Height = 261
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 760
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 96
      Top = 20
      Width = 32
      Height = 13
      Caption = 'IME-C:'
      FocusControl = DBEdMovimCod
    end
    object Label3: TLabel
      Left = 180
      Top = 20
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object Label7: TLabel
      Left = 228
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Data / hora:'
      FocusControl = DBEdDtEntrada
    end
    object Label19: TLabel
      Left = 344
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Movim Twn:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdMovimCod: TdmkDBEdit
      Left = 96
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'MovimCod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 180
      Top = 36
      Width = 45
      Height = 21
      TabStop = False
      DataField = 'Empresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdDtEntrada: TdmkDBEdit
      Left = 228
      Top = 36
      Width = 112
      Height = 21
      TabStop = False
      DataField = 'DtHrAberto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object EdMovimTwn: TdmkEdit
      Left = 344
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 760
    Height = 261
    Align = alClient
    Caption = ' Dados do item a ser gerado: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 28
      Height = 13
      Caption = 'IME-I:'
    end
    object Label1: TLabel
      Left = 96
      Top = 16
      Width = 122
      Height = 13
      Caption = 'Artigo em industrializa'#231#227'o:'
    end
    object LaQtde: TLabel
      Left = 356
      Top = 56
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label9: TLabel
      Left = 260
      Top = 96
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object Label20: TLabel
      Left = 12
      Top = 136
      Width = 29
      Height = 13
      Caption = 'Pallet:'
    end
    object LaVSRibCla: TLabel
      Left = 68
      Top = 136
      Width = 76
      Height = 13
      Caption = 'Nome do Artigo:'
    end
    object SbPallet1: TSpeedButton
      Left = 464
      Top = 152
      Width = 22
      Height = 22
      Caption = '...'
      OnClick = SbPallet1Click
    end
    object Label48: TLabel
      Left = 12
      Top = 176
      Width = 114
      Height = 13
      Caption = 'Item de pedido atrelado:'
    end
    object Label49: TLabel
      Left = 12
      Top = 216
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
      Enabled = False
    end
    object Label50: TLabel
      Left = 510
      Top = 216
      Width = 74
      Height = 13
      Caption = 'Req.Mov.Estq.:'
      Color = clBtnFace
      ParentColor = False
    end
    object SBNewPallet: TSpeedButton
      Left = 488
      Top = 152
      Width = 22
      Height = 22
      Caption = '+'
      OnClick = SBNewPalletClick
    end
    object Label53: TLabel
      Left = 13
      Top = 97
      Width = 58
      Height = 13
      Caption = 'Data / hora:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label4: TLabel
      Left = 440
      Top = 56
      Width = 76
      Height = 13
      Caption = '$ M.O. unidade:'
    end
    object Label12: TLabel
      Left = 164
      Top = 96
      Width = 81
      Height = 13
      Caption = 'Marca / carimbo:'
    end
    object Label8: TLabel
      Left = 524
      Top = 56
      Width = 62
      Height = 13
      Caption = '$ M.O. Total:'
      Enabled = False
    end
    object Label11: TLabel
      Left = 440
      Top = 96
      Width = 75
      Height = 13
      Caption = '$ Mat'#233'ria prima:'
      Enabled = False
    end
    object Label10: TLabel
      Left = 524
      Top = 96
      Width = 57
      Height = 13
      Caption = 'Custo Total:'
      Enabled = False
    end
    object Label13: TLabel
      Left = 12
      Top = 56
      Width = 72
      Height = 13
      Caption = 'S'#233'rie do Tal'#227'o:'
    end
    object Label14: TLabel
      Left = 260
      Top = 56
      Width = 51
      Height = 13
      Caption = 'Tal'#227'o [F4]:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 453
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGraGruX
      TabOrder = 2
      dmkEditCB = EdGraGruX
      QryCampo = 'GraGruX'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGraGruX: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GraGruX'
      UpdCampo = 'GraGruX'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdQtde: TdmkEdit
      Left = 356
      Top = 72
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Qtde'
      UpdCampo = 'Qtde'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdQtdeRedefinido
    end
    object EdObserv: TdmkEdit
      Left = 260
      Top = 112
      Width = 177
      Height = 21
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Observ'
      UpdCampo = 'Observ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPallet: TdmkEditCB
      Left = 12
      Top = 152
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPallet
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPallet: TdmkDBLookupComboBox
      Left = 68
      Top = 152
      Width = 393
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsTXPallet
      TabOrder = 16
      dmkEditCB = EdPallet
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPedItsFin: TdmkEditCB
      Left = 12
      Top = 192
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 18
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPedItsFin
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPedItsFin: TdmkDBLookupComboBox
      Left = 68
      Top = 192
      Width = 537
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsTXPedIts
      TabOrder = 19
      dmkEditCB = EdPedItsFin
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdStqCenLoc: TdmkEditCB
      Left = 12
      Top = 232
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 20
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStqCenLoc: TdmkDBLookupComboBox
      Left = 68
      Top = 232
      Width = 438
      Height = 21
      Enabled = False
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 21
      dmkEditCB = EdStqCenLoc
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdReqMovEstq: TdmkEdit
      Left = 511
      Top = 232
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 22
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CkEncerraPallet: TdmkCheckBox
      Left = 511
      Top = 156
      Width = 97
      Height = 17
      Caption = 'Encerra o Pallet.'
      TabOrder = 17
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object MeAviso: TMemo
      Left = 608
      Top = 4
      Width = 144
      Height = 249
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 23
    end
    object TPData: TdmkEditDateTimePicker
      Left = 13
      Top = 112
      Width = 108
      Height = 21
      Date = 0.639644131944805900
      Time = 0.639644131944805900
      TabOrder = 9
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DtHrAberto'
      UpdCampo = 'DtHrAberto'
      UpdType = utYes
      DatePurpose = dmkdpSPED_EFD_MIN
    end
    object EdHora: TdmkEdit
      Left = 122
      Top = 112
      Width = 40
      Height = 21
      TabOrder = 10
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00:00'
      QryName = 'QrVSGerArt'
      QryCampo = 'DtHrAberto'
      UpdCampo = 'DtHrAberto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCustoMOUni: TdmkEdit
      Left = 440
      Top = 72
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'CustoMOUni'
      UpdCampo = 'CustoMOUni'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdCustoMOUniRedefinido
    end
    object EdMarca: TdmkEdit
      Left = 164
      Top = 112
      Width = 93
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 11
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Marca'
      UpdCampo = 'Marca'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCustoMOTot: TdmkEdit
      Left = 524
      Top = 72
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'CustoMOTot'
      UpdCampo = 'CustoMOTot'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdValorMP: TdmkEdit
      Left = 440
      Top = 112
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorMP'
      UpdCampo = 'ValorMP'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdValorMPRedefinido
    end
    object EdValorT: TdmkEdit
      Left = 524
      Top = 112
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdSerieTal: TdmkEditCB
      Left = 12
      Top = 72
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'SerieTal'
      UpdCampo = 'SerieTal'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSerieTal
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBSerieTal: TdmkDBLookupComboBox
      Left = 52
      Top = 72
      Width = 205
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTXSerTal
      TabOrder = 4
      dmkEditCB = EdSerieTal
      QryCampo = 'SerieTal'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdTalao: TdmkEdit
      Left = 260
      Top = 72
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Talao'
      UpdCampo = 'Talao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdTalaoKeyDown
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 760
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 712
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 664
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 411
        Height = 32
        Caption = 'Item de Artigo em Industrializa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 411
        Height = 32
        Caption = 'Item de Artigo em Industrializa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 411
        Height = 32
        Caption = 'Item de Artigo em Industrializa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 373
    Width = 760
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 756
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 417
    Width = 760
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 614
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 612
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 504
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 504
    Top = 104
  end
  object QrTXPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 428
    Top = 57
    object QrTXPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrTXPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrTXPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrTXPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrTXPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsTXPallet: TDataSource
    DataSet = QrTXPallet
    Left = 428
    Top = 101
  end
  object QrTXPedIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 664
    Top = 152
    object QrTXPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrTXPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsTXPedIts: TDataSource
    DataSet = QrTXPedIts
    Left = 664
    Top = 196
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 344
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 344
    Top = 104
  end
  object PMPallet: TPopupMenu
    Left = 664
    Top = 244
    object Criar1: TMenuItem
      Caption = '&Criar Novo'
      OnClick = Criar1Click
    end
    object Gerenciamento1: TMenuItem
      Caption = '&Gerenciamento'
      OnClick = Gerenciamento1Click
    end
  end
  object QrTXSerTal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM txsertal'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 576
    Top = 56
    object QrTXSerTalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXSerTalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXSerTal: TDataSource
    DataSet = QrTXSerTal
    Left = 576
    Top = 104
  end
end
