unit TXTalPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, mySQLDbTables, AppListas, UnDmkProcFunc, dmkDBGridZTO,
  dmkCheckBox, UnProjGroup_Consts, UnProjGroup_Vars, UnAppEnums;

type
  TFmTXTalPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DsTXSerTal: TDataSource;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqControle: TIntegerField;
    QrPesqNO_SerieTal: TWideStringField;
    QrPesqTalao: TIntegerField;
    QrPesqMarca: TWideStringField;
    QrPesqCodigo: TIntegerField;
    DGDados: TDBGrid;
    QrPesqSerieTal: TIntegerField;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    Panel6: TPanel;
    Panel5: TPanel;
    Label11: TLabel;
    Label4: TLabel;
    Label12: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label22: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    CBSerieTal: TdmkDBLookupComboBox;
    EdSerieTal: TdmkEditCB;
    EdTalao: TdmkEdit;
    EdMarca: TdmkEdit;
    EdControle: TdmkEdit;
    BtReabre: TBitBtn;
    EdPallet: TdmkEdit;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    EdSrcNivel2: TdmkEdit;
    EdDstNivel2: TdmkEdit;
    EdObserv: TdmkEdit;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    DBGMovimNiv: TdmkDBGridZTO;
    QrMovimNiv: TmySQLQuery;
    DsMovimNiv: TDataSource;
    QrMovimNivNO_MovimNiv: TWideStringField;
    QrPesqNO_MovimNiv: TWideStringField;
    CkSdoPositivo: TdmkCheckBox;
    QrPesqNO_FORNECE: TWideStringField;
    RGTabela: TRadioGroup;
    QrMovimNivMovimNiv: TLargeintField;
    QrPesqMovimCod: TIntegerField;
    QrPesqCliVenda: TIntegerField;
    QrPesqCustoMOUni: TFloatField;
    QrPesqDataHora: TDateTimeField;
    QrPesqNO_PRD_TAM_COR: TWideStringField;
    BtDefIMEI: TBitBtn;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    EdQtde: TdmkEdit;
    CkQtde: TCheckBox;
    EdNFeNum: TdmkEdit;
    Label10: TLabel;
    CkNFeSer: TCheckBox;
    EdNFeSer: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtDefIMEIClick(Sender: TObject);
  private
    { Private declarations }
    procedure LocalizaRegistro;
    procedure ReabrePesquisa();
  public
    { Public declarations }
    FOnlySelIMEI: Boolean;
    FMovimID: TEstqMovimID;
    FCodigo, FControle, FSerieTal, FTalao: Integer;
    //
    procedure ReopenMovimNiv(MovimID: TEstqMovimID);
  end;

  var
  FmTXTalPsq: TFmTXTalPsq;

implementation

uses UnMyObjects, Module, UnTX_PF, DmkDAC_PF, ModTX_CRC;

{$R *.DFM}

procedure TFmTXTalPsq.BtDefIMEIClick(Sender: TObject);
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FCodigo   := QrPesqCodigo.Value;
    FControle := QrPesqControle.Value;
    FSerieTal := QrPesqSerieTal.Value;
    FTalao    := QrPesqTalao.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(DmModTX_CRC.QrIMEI, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_XMI,
    'WHERE Controle=' + Geral.FF0(QrPesqControle.Value),
    '']);
    //
    Close;
  end;
end;

procedure TFmTXTalPsq.BtOKClick(Sender: TObject);
begin
  LocalizaRegistro;
end;

procedure TFmTXTalPsq.BtReabreClick(Sender: TObject);
begin
  ReabrePesquisa();
  if (QrPesq.RecordCount = 0) and (RGTabela.Itemindex = 0) then
  begin
    RGTabela.Itemindex := 1;
    ReabrePesquisa();
    RGTabela.Itemindex := 0;
  end;
end;

procedure TFmTXTalPsq.BtSaidaClick(Sender: TObject);
begin
  FCodigo   := 0;
  FControle := 0;
  FSerieTal := 0;
  FTalao    := 0;
  DmModTX_CRC.QrIMEI.Close;
  Close;
end;

procedure TFmTXTalPsq.DGDadosDblClick(Sender: TObject);
begin
  case ImgTipo.SQLType of
    stPsq:
    begin
      if FOnlySelIMEI then
      begin
        VAR_IMEI_SEL := QrPesqControle.Value;
        Close;
      end else
        LocalizaRegistro;
    end;
    stCpy: BtDefIMEIClick(Self);
  end;
end;

procedure TFmTXTalPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXTalPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FOnlySelIMEI := False;
  //
  FCodigo   := 0;
  FControle := 0;
  FSerieTal := 0;
  FTalao    := 0;
  DmModTX_CRC.QrIMEI.Close;
  //
  TX_PF.AbreTXSerTal(QrTXSerTal);
  TX_PF.ReopenTXPrestador(QrPrestador);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
end;

procedure TFmTXTalPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXTalPsq.FormShow(Sender: TObject);
begin
  EdControle.SetFocus;
end;

procedure TFmTXTalPsq.LocalizaRegistro;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FCodigo   := QrPesqCodigo.Value;
    FControle := QrPesqControle.Value;
    FSerieTal := QrPesqSerieTal.Value;
    FTalao    := QrPesqTalao.Value;
    Close;
  end;
end;

procedure TFmTXTalPsq.QrPesqAfterOpen(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Registros encontrados: ' +
  Geral.FF0(QrPesq.RecordCount));
end;

procedure TFmTXTalPsq.QrPesqBeforeClose(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
end;

procedure TFmTXTalPsq.ReabrePesquisa();
var
  Observ, Marca, SQL1, SQL2, SQL3, SQL4, SQL5, SQL6, SQL7, SQL8, SQL9, SQL0,
  SQLA, SQLB, SQLC, SQLD, SQLE, (*SQLF, SQLG, SQLH,*) SQLI, SQLJ, SQL_MovimID,
  MovimNivs: String;
  Controle, SerieTal, Talao, Pallet, Terceiro, Motorista, Fornecedor, SrcNivel2,
  DstNivel2, CliVenda, FornecMO, NFeNum: Integer;
  ATT_MovimNiv, Tabela: String;
begin
  case RGTabela.ItemIndex of
    0: Tabela := CO_SEL_TAB_TMI;
    1: Tabela := CO_TAB_TMB;
    else Tabela := 'txmovit?';
  end;
  ATT_MovimNiv := dmkPF.ArrayToTexto('MovimNiv', 'NO_MovimNiv', pvNo, True,
    sEstqMovimNiv);
  Controle := EdControle.ValueVariant;
  SerieTal := EdSerieTal.ValueVariant;
  Talao    := EdTalao.ValueVariant;
  Marca    := EdMarca.Text;
  Pallet   := EdPallet.ValueVariant;
  Fornecedor  := EdFornecedor.ValueVariant;
  CliVenda    := EdCliente.ValueVariant;
  FornecMO    := EdFornecMO.ValueVariant;
  Motorista   := EdMotorista.ValueVariant;
  SrcNivel2   := EdSrcNivel2.ValueVariant;
  DstNivel2   := EdDstNivel2.ValueVariant;
  Observ      := EdObserv.Text;
  MovimNivs   := TX_PF.DefineIDs_Str(DBGMovimNiv, QrMovimNiv, 'MovimNiv');
  NFeNum      := EdNFeNum.ValueVariant;
  //
  if FMovimID <> emidAjuste then
    SQL_MovimID := 'WHERE tmi.MovimID=' + Geral.FF0(Integer(FMovimID))
  else
    SQL_MovimID := 'WHERE tmi.MovimID >= 0';
  //
  if Controle <> 0 then
    SQL1 := 'AND tmi.Controle=' + Geral.FF0(Controle)
  else
    SQL1 := '';
  //
  if SerieTal <> 0 then
    SQL2 := 'AND tmi.SerieTal=' + Geral.FF0(SerieTal)
  else
    SQL2 := '';
  //
  if Talao <> 0 then
    SQL3 := 'AND tmi.Talao=' + Geral.FF0(Talao)
  else
    SQL3 := '';
  //
  if Marca <> '' then
  begin
    if Pos('%', Marca) > 0 then
      SQL4 := 'AND tmi.Marca LIKE "' + Marca + '"'
    else
      SQL4 := 'AND tmi.Marca="' + Marca + '"'
  end else
    SQL4 := '';
  //
  if Pallet <> 0 then
    SQL5 := 'AND tmi.Pallet=' + Geral.FF0(Pallet)
  else
    SQL5 := '';
  //
  if Fornecedor <> 0 then
    SQL6 := 'AND tmi.Terceiro=' + Geral.FF0(Fornecedor)
  else
    SQL6 := '';
  //
  if Motorista <> 0 then
    SQL7 := Geral.ATS([
    'AND tmi.MovimCod IN (',
    '  SELECT MovimCod',
    '  FROM txinncab',
    '  WHERE Motorista=' + Geral.FF0(Motorista),
    '  )'])
  else
    SQL7 := '';
  if SrcNivel2 <> 0 then
    SQL8 := 'AND tmi.SrcNivel2=' + Geral.FF0(SrcNivel2)
  else
    SQL8 := '';
  //
  if DstNivel2 <> 0 then
    SQL9 := 'AND tmi.DstNivel2=' + Geral.FF0(DstNivel2)
  else
    SQL9 := '';
  //
  if Observ <> '' then
    SQL0 := 'AND tmi.Observ LIKE ' + '"%' + Observ + '%"'
  else
    SQL0 := '';
  //
  if CliVenda <> 0 then
    SQLA := 'AND tmi.CliVenda=' + Geral.FF0(CliVenda)
  else
    SQLA := '';
  //
  if FornecMO <> 0 then
    SQLB := 'AND tmi.FornecMO=' + Geral.FF0(FornecMO)
  else
    SQLB := '';
  //
  if MovimNivs <> '' then
    SQLC := 'AND tmi.MovimNiv IN (' + MovimNivs + ')'
  else
    SQLC := '';
  if CkSdoPositivo.Checked then
    SQLD := 'AND tmi.SdoVrtQtd > 0'
  else
    SQLD := '';
  //
  SQLE := Geral.FFT_Dot(EdQtde.ValueVariant, 3, siPositivo);
  if CkQtde.Checked then
    SQLE := 'AND (tmi.Qtde = ' + SQLE + ' OR tmi.Qtde = -' + SQLE +
    ' OR tmi.SdoVrtQtd=' + SQLE + ') '
  else
    SQLE := '';
  //
  if EdNFeNum.ValueVariant <> 0 then
    SQLI := 'AND tmi.NFeNum = ' + Geral.FF0(EdNFeNum.ValueVariant)
  else
    SQLI := '';
  //
  if CkNFeSer.Checked then
    SQLJ := 'AND tmi.NFeSer = ' + Geral.FF0(EdNFeSer.ValueVariant)
  else
    SQLJ := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT DISTINCT ',
    TX_PF.SQL_NO_GGX(),
    TX_PF.SQL_NO_FRN(),
    'tmi.Codigo, tmi.MovimCod, tmi.Controle, tst.Nome NO_SerieTal, ',
    'tmi.Talao, tmi.Marca, tmi.SerieTal, ',
    'tmi.DataHora, tmi.CliVenda, tmi.CustoMOUni, ',
    ATT_MovimNiv,
    'FROM ' + Tabela + ' tmi ',
    'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
    TX_PF.SQL_LJ_GGX(),
    TX_PF.SQL_LJ_FRN(),
    //'WHERE tmi.Ativo = 1 ',
    SQL_MovimID,
    SQL1,
    SQL2,
    SQL3,
    SQL4,
    SQL5,
    SQL6,
    SQL7,
    SQL8,
    SQL9,
    SQL0,
    SQLA,
    SQLB,
    SQLC,
    SQLD,
    SQLE,
    //SQLF,
    //SQLG,
    //SQLH,
    SQLI,
    SQLJ,
    //
    'ORDER BY tmi.Controle DESC ',
    '']);
  //Geral.MB_SQL(Self, QrPesq);
end;

procedure TFmTXTalPsq.ReopenMovimNiv(MovimID: TEstqMovimID);
var
  ATT_MovimNiv: String;
begin
  ATT_MovimNiv := dmkPF.ArrayToTexto('MovimNiv', 'NO_MovimNiv', pvNo, True,
    sEstqMovimNiv);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovimNiv, Dmod.MyDB, [
  'SELECT DISTINCT CAST(MovimNiv AS SIGNED) MovimNiv, ',
  ATT_MovimNiv,
  'FROM ' + CO_TAB_TMB,
  'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
  '',
  'UNION ',
  '',
  'SELECT DISTINCT CAST(MovimNiv AS SIGNED) MovimNiv, ',
  ATT_MovimNiv,
  'FROM ' + CO_SEL_TAB_TMI,
  'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
  '']);
end;

end.
