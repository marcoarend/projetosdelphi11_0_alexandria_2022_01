unit TXTrfLocCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet,
  UnProjGroup_Consts, UnGrl_Consts, UnGrl_Geral, UnAppEnums, dmkDBGridZTO,
  UnProjGroup_Vars, UnTX_Jan;

type
  TFmTXTrfLocCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrTXTrfLocCab: TmySQLQuery;
    DsTXTrfLocCab: TDataSource;
    QrIMEIDest: TmySQLQuery;
    DsIMEIDest: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExcluiIMEI1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtVenda: TdmkEditDateTimePicker;
    EdDtVenda: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrega: TdmkEditDateTimePicker;
    EdDtEntrega: TdmkEdit;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrTXTrfLocCabCodigo: TIntegerField;
    QrTXTrfLocCabMovimCod: TIntegerField;
    QrTXTrfLocCabEmpresa: TIntegerField;
    QrTXTrfLocCabDtVenda: TDateTimeField;
    QrTXTrfLocCabDtViagem: TDateTimeField;
    QrTXTrfLocCabDtEntrega: TDateTimeField;
    QrTXTrfLocCabCliente: TIntegerField;
    QrTXTrfLocCabTransporta: TIntegerField;
    QrTXTrfLocCabQtde: TFloatField;
    QrTXTrfLocCabLk: TIntegerField;
    QrTXTrfLocCabDataCad: TDateField;
    QrTXTrfLocCabDataAlt: TDateField;
    QrTXTrfLocCabUserCad: TIntegerField;
    QrTXTrfLocCabUserAlt: TIntegerField;
    QrTXTrfLocCabAlterWeb: TSmallintField;
    QrTXTrfLocCabAtivo: TSmallintField;
    QrTXTrfLocCabNO_EMPRESA: TWideStringField;
    QrTXTrfLocCabNO_CLIENTE: TWideStringField;
    QrTXTrfLocCabNO_TRANSPORTA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    GBIts: TGroupBox;
    PorIMEI1: TMenuItem;
    PorPallet1: TMenuItem;
    PMImprime: TPopupMenu;
    PackingList1: TMenuItem;
    QrPallets: TmySQLQuery;
    DsPallets: TDataSource;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDados: TDBGrid;
    DBGrid1: TDBGrid;
    QrPalletsArtigoImp: TWideStringField;
    QrPalletsClasseImp: TWideStringField;
    QrPalletsSEQ: TIntegerField;
    frxDsPallets: TfrxDBDataset;
    frxTEX_FAXAO_019_00_A: TfrxReport;
    Porkgsubproduto1: TMenuItem;
    InformaNmerodaRME1: TMenuItem;
    TabSheet3: TTabSheet;
    QrTXOutNFeIts: TmySQLQuery;
    DsTXOutNFeIts: TDataSource;
    DBGrid2: TDBGrid;
    QrTXOutNFeItsNO_PRD_TAM_COR: TWideStringField;
    QrTXOutNFeItsCodigo: TIntegerField;
    QrTXOutNFeItsControle: TIntegerField;
    QrTXOutNFeItsItemNFe: TIntegerField;
    QrTXOutNFeItsGraGruX: TIntegerField;
    QrTXOutNFeItsValorT: TFloatField;
    QrTXOutNFeItsTpCalcVal: TSmallintField;
    QrTXOutNFeItsValorU: TFloatField;
    QrTXOutNFeItsTribDefSel: TIntegerField;
    QrTXOutNFeItsLk: TIntegerField;
    QrTXOutNFeItsDataCad: TDateField;
    QrTXOutNFeItsDataAlt: TDateField;
    QrTXOutNFeItsUserCad: TIntegerField;
    QrTXOutNFeItsUserAlt: TIntegerField;
    QrTXOutNFeItsAlterWeb: TSmallintField;
    QrTXOutNFeItsAtivo: TSmallintField;
    BtTXOutNFeCab: TBitBtn;
    PMTXOutNFeIts: TPopupMenu;
    IncluiitemdeNF1: TMenuItem;
    AlteraitemdeNF1: TMenuItem;
    ExcluiitemdeNF1: TMenuItem;
    QrTribIncIts: TmySQLQuery;
    QrTribIncItsFatID: TIntegerField;
    QrTribIncItsFatNum: TIntegerField;
    QrTribIncItsFatParcela: TIntegerField;
    QrTribIncItsEmpresa: TIntegerField;
    QrTribIncItsControle: TIntegerField;
    QrTribIncItsData: TDateField;
    QrTribIncItsHora: TTimeField;
    QrTribIncItsValorFat: TFloatField;
    QrTribIncItsBaseCalc: TFloatField;
    QrTribIncItsValrTrib: TFloatField;
    QrTribIncItsPercent: TFloatField;
    QrTribIncItsLk: TIntegerField;
    QrTribIncItsDataCad: TDateField;
    QrTribIncItsDataAlt: TDateField;
    QrTribIncItsUserCad: TIntegerField;
    QrTribIncItsUserAlt: TIntegerField;
    QrTribIncItsAlterWeb: TSmallintField;
    QrTribIncItsAtivo: TSmallintField;
    QrTribIncItsTributo: TIntegerField;
    QrTribIncItsVUsoCalc: TFloatField;
    QrTribIncItsOperacao: TSmallintField;
    QrTribIncItsNO_Tributo: TWideStringField;
    DsTribIncIts: TDataSource;
    GroupBox3: TGroupBox;
    DBGrid4: TDBGrid;
    BtTribIncIts: TBitBtn;
    PMTribIncIts: TPopupMenu;
    IncluiTributo1: TMenuItem;
    AlteraTributoAtual1: TMenuItem;
    ExcluiTributoAtual1: TMenuItem;
    QrTribIncItsFatorDC: TSmallintField;
    QrPalletsTXT_ARTIGO: TWideStringField;
    QrIMEIDestCodigo: TLargeintField;
    QrIMEIDestControle: TLargeintField;
    QrIMEIDestMovimCod: TLargeintField;
    QrIMEIDestMovimNiv: TLargeintField;
    QrIMEIDestMovimTwn: TLargeintField;
    QrIMEIDestEmpresa: TLargeintField;
    QrIMEIDestTerceiro: TLargeintField;
    QrIMEIDestCliVenda: TLargeintField;
    QrIMEIDestMovimID: TLargeintField;
    QrIMEIDestDataHora: TDateTimeField;
    QrIMEIDestPallet: TLargeintField;
    QrIMEIDestGraGruX: TLargeintField;
    QrIMEIDestQtde: TFloatField;
    QrIMEIDestValorT: TFloatField;
    QrIMEIDestSrcMovID: TLargeintField;
    QrIMEIDestSrcNivel1: TLargeintField;
    QrIMEIDestSrcNivel2: TLargeintField;
    QrIMEIDestSrcGGX: TLargeintField;
    QrIMEIDestSdoVrtQtd: TFloatField;
    QrIMEIDestObserv: TWideStringField;
    QrIMEIDestSerieTal: TLargeintField;
    QrIMEIDestTalao: TLargeintField;
    QrIMEIDestFornecMO: TLargeintField;
    QrIMEIDestCustoMOUni: TFloatField;
    QrIMEIDestCustoMOTot: TFloatField;
    QrIMEIDestValorMP: TFloatField;
    QrIMEIDestDstMovID: TLargeintField;
    QrIMEIDestDstNivel1: TLargeintField;
    QrIMEIDestDstNivel2: TLargeintField;
    QrIMEIDestDstGGX: TLargeintField;
    QrIMEIDestQtdGer: TFloatField;
    QrIMEIDestQtdAnt: TFloatField;
    QrIMEIDestPedItsFin: TLargeintField;
    QrIMEIDestMarca: TWideStringField;
    QrIMEIDestStqCenLoc: TLargeintField;
    QrIMEIDestNO_PALLET: TWideStringField;
    QrIMEIDestNO_PRD_TAM_COR: TWideStringField;
    QrIMEIDestNO_TTW: TWideStringField;
    QrIMEIDestID_TTW: TLargeintField;
    QrIMEIDestReqMovEstq: TLargeintField;
    QrIMEIDestNO_FORNECE: TWideStringField;
    QrTXTrfLocCabTemIMEIMrt: TIntegerField;
    QrPalletsPallet: TLargeintField;
    QrPalletsGraGruX: TLargeintField;
    QrPalletsQtde: TFloatField;
    QrPalletsNO_PRD_TAM_COR: TWideStringField;
    QrPalletsNO_Pallet: TWideStringField;
    QrPalletsSerieTal: TLargeintField;
    QrPalletsTalao: TLargeintField;
    QrPalletsNO_TTW: TWideStringField;
    QrPalletsID_TTW: TLargeintField;
    N1: TMenuItem;
    CorrigeFornecedores1: TMenuItem;
    QrIMEISorc: TmySQLQuery;
    DsIMEISrc: TDataSource;
    QrIMEISorcCodigo: TLargeintField;
    QrIMEISorcControle: TLargeintField;
    QrIMEISorcMovimCod: TLargeintField;
    QrIMEISorcMovimNiv: TLargeintField;
    QrIMEISorcMovimTwn: TLargeintField;
    QrIMEISorcEmpresa: TLargeintField;
    QrIMEISorcTerceiro: TLargeintField;
    QrIMEISorcCliVenda: TLargeintField;
    QrIMEISorcMovimID: TLargeintField;
    QrIMEISorcDataHora: TDateTimeField;
    QrIMEISorcPallet: TLargeintField;
    QrIMEISorcGraGruX: TLargeintField;
    QrIMEISorcQtde: TFloatField;
    QrIMEISorcValorT: TFloatField;
    QrIMEISorcSrcMovID: TLargeintField;
    QrIMEISorcSrcNivel1: TLargeintField;
    QrIMEISorcSrcNivel2: TLargeintField;
    QrIMEISorcSrcGGX: TLargeintField;
    QrIMEISorcSdoVrtQtd: TFloatField;
    QrIMEISorcObserv: TWideStringField;
    QrIMEISorcSerieTal: TLargeintField;
    QrIMEISorcTalao: TLargeintField;
    QrIMEISorcFornecMO: TLargeintField;
    QrIMEISorcCustoMOUni: TFloatField;
    QrIMEISorcCustoMOTot: TFloatField;
    QrIMEISorcValorMP: TFloatField;
    QrIMEISorcDstMovID: TLargeintField;
    QrIMEISorcDstNivel1: TLargeintField;
    QrIMEISorcDstNivel2: TLargeintField;
    QrIMEISorcDstGGX: TLargeintField;
    QrIMEISorcQtdGer: TFloatField;
    QrIMEISorcQtdAnt: TFloatField;
    QrIMEISorcPedItsFin: TLargeintField;
    QrIMEISorcMarca: TWideStringField;
    QrIMEISorcStqCenLoc: TLargeintField;
    QrIMEISorcNO_FORNECE: TWideStringField;
    QrIMEISorcNO_PALLET: TWideStringField;
    QrIMEISorcNO_PRD_TAM_COR: TWideStringField;
    QrIMEISorcNO_TTW: TWideStringField;
    QrIMEISorcID_TTW: TLargeintField;
    QrIMEISorcReqMovEstq: TLargeintField;
    DBGrid3: TDBGrid;
    Label9: TLabel;
    QrTXOutNFeCab: TmySQLQuery;
    QrTXOutNFeCabCodigo: TIntegerField;
    QrTXOutNFeCabMovimCod: TIntegerField;
    QrTXOutNFeCabide_serie: TIntegerField;
    QrTXOutNFeCabide_nNF: TIntegerField;
    QrTXOutNFeCabOriCod: TIntegerField;
    QrTXOutNFeCabLk: TIntegerField;
    QrTXOutNFeCabDataCad: TDateField;
    QrTXOutNFeCabDataAlt: TDateField;
    QrTXOutNFeCabUserCad: TIntegerField;
    QrTXOutNFeCabUserAlt: TIntegerField;
    QrTXOutNFeCabAlterWeb: TSmallintField;
    QrTXOutNFeCabAtivo: TSmallintField;
    DsTXOutNFeCab: TDataSource;
    DBGrid5: TDBGrid;
    BtTXOutNFeIts: TBitBtn;
    PMTXOutNFeCab: TPopupMenu;
    IncluiSerieNumeroDeNFe1: TMenuItem;
    AlteraSerieNumeroDeNFe1: TMenuItem;
    ExcluiSerieNumeroDeNFe1: TMenuItem;
    frxDsTXOutNFeCab: TfrxDBDataset;
    frxDsTXOutNFeIts: TfrxDBDataset;
    Edide_serie: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    Edide_nNF: TdmkEdit;
    QrTXTrfLocCabide_serie: TSmallintField;
    QrTXTrfLocCabide_nNF: TIntegerField;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    LaCliente: TLabel;
    QrClienteCNPJ_CPF: TWideStringField;
    AlteraLocaldeestoque1: TMenuItem;
    QrIMEIDestIxxMovIX: TLargeintField;
    QrIMEIDestIxxFolha: TLargeintField;
    QrIMEIDestIxxLinha: TLargeintField;
    InformaIEC1: TMenuItem;
    TsFrCompr: TTabSheet;
    QrTXMOEnvAvu: TmySQLQuery;
    QrTXMOEnvAvuCodigo: TIntegerField;
    QrTXMOEnvAvuCFTMA_FatID: TIntegerField;
    QrTXMOEnvAvuCFTMA_FatNum: TIntegerField;
    QrTXMOEnvAvuCFTMA_Empresa: TIntegerField;
    QrTXMOEnvAvuCFTMA_Terceiro: TIntegerField;
    QrTXMOEnvAvuCFTMA_nItem: TIntegerField;
    QrTXMOEnvAvuCFTMA_SerCT: TIntegerField;
    QrTXMOEnvAvuCFTMA_nCT: TIntegerField;
    QrTXMOEnvAvuCFTMA_Qtde: TFloatField;
    QrTXMOEnvAvuCFTMA_PesTrKg: TFloatField;
    QrTXMOEnvAvuCFTMA_CusTrKg: TFloatField;
    QrTXMOEnvAvuCFTMA_ValorT: TFloatField;
    QrTXMOEnvAvuTXTMI_MovimCod: TIntegerField;
    QrTXMOEnvAvuLk: TIntegerField;
    QrTXMOEnvAvuDataCad: TDateField;
    QrTXMOEnvAvuDataAlt: TDateField;
    QrTXMOEnvAvuUserCad: TIntegerField;
    QrTXMOEnvAvuUserAlt: TIntegerField;
    QrTXMOEnvAvuAlterWeb: TSmallintField;
    QrTXMOEnvAvuAWServerID: TIntegerField;
    QrTXMOEnvAvuAWStatSinc: TSmallintField;
    QrTXMOEnvAvuAtivo: TSmallintField;
    DsTXMOEnvAvu: TDataSource;
    QrTXMOEnvATMI: TmySQLQuery;
    QrTXMOEnvATMICodigo: TIntegerField;
    QrTXMOEnvATMITXMOEnvAvu: TIntegerField;
    QrTXMOEnvATMITXMovIts: TIntegerField;
    QrTXMOEnvATMILk: TIntegerField;
    QrTXMOEnvATMIDataCad: TDateField;
    QrTXMOEnvATMIDataAlt: TDateField;
    QrTXMOEnvATMIUserCad: TIntegerField;
    QrTXMOEnvATMIUserAlt: TIntegerField;
    QrTXMOEnvATMIAlterWeb: TSmallintField;
    QrTXMOEnvATMIAWServerID: TIntegerField;
    QrTXMOEnvATMIAWStatSinc: TSmallintField;
    QrTXMOEnvATMIAtivo: TSmallintField;
    QrTXMOEnvATMIValorFrete: TFloatField;
    QrTXMOEnvATMIQtde: TFloatField;
    DsTXMOEnvATMI: TDataSource;
    PnFrCompr: TPanel;
    DBGTXMOEnvAvu: TdmkDBGridZTO;
    DBGTXMOEnvATMI: TdmkDBGridZTO;
    AtrelamentoFreteCompra1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    IncluiAtrelamento3: TMenuItem;
    AlteraAtrelamento3: TMenuItem;
    ExcluiAtrelamento3: TMenuItem;
    QrIMEIDestCusFrtAvuls: TFloatField;
    TsEnvioMO: TTabSheet;
    TsRetornoMO: TTabSheet;
    QrTXMOEnvEnv: TmySQLQuery;
    QrTXMOEnvEnvCodigo: TIntegerField;
    QrTXMOEnvEnvNFEMP_FatID: TIntegerField;
    QrTXMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrTXMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrTXMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrTXMOEnvEnvNFEMP_nItem: TIntegerField;
    QrTXMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrTXMOEnvEnvNFEMP_nNF: TIntegerField;
    QrTXMOEnvEnvNFEMP_Qtde: TFloatField;
    QrTXMOEnvEnvNFEMP_ValorT: TFloatField;
    QrTXMOEnvEnvCFTMP_FatID: TIntegerField;
    QrTXMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrTXMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrTXMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrTXMOEnvEnvCFTMP_nItem: TIntegerField;
    QrTXMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrTXMOEnvEnvCFTMP_nCT: TIntegerField;
    QrTXMOEnvEnvCFTMP_Qtde: TFloatField;
    QrTXMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrTXMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrTXMOEnvEnvCFTMP_ValorT: TFloatField;
    QrTXMOEnvEnvTXTMI_MovimCod: TIntegerField;
    QrTXMOEnvEnvLk: TIntegerField;
    QrTXMOEnvEnvDataCad: TDateField;
    QrTXMOEnvEnvDataAlt: TDateField;
    QrTXMOEnvEnvUserCad: TIntegerField;
    QrTXMOEnvEnvUserAlt: TIntegerField;
    QrTXMOEnvEnvAlterWeb: TSmallintField;
    QrTXMOEnvEnvAWServerID: TIntegerField;
    QrTXMOEnvEnvAWStatSinc: TSmallintField;
    QrTXMOEnvEnvAtivo: TSmallintField;
    DsTXMOEnvEnv: TDataSource;
    QrTXMOEnvETMI: TmySQLQuery;
    QrTXMOEnvETMICodigo: TIntegerField;
    QrTXMOEnvETMITXMOEnvEnv: TIntegerField;
    QrTXMOEnvETMITXMovIts: TIntegerField;
    QrTXMOEnvETMILk: TIntegerField;
    QrTXMOEnvETMIDataCad: TDateField;
    QrTXMOEnvETMIDataAlt: TDateField;
    QrTXMOEnvETMIUserCad: TIntegerField;
    QrTXMOEnvETMIUserAlt: TIntegerField;
    QrTXMOEnvETMIAlterWeb: TSmallintField;
    QrTXMOEnvETMIAWServerID: TIntegerField;
    QrTXMOEnvETMIAWStatSinc: TSmallintField;
    QrTXMOEnvETMIAtivo: TSmallintField;
    QrTXMOEnvETMIValorFrete: TFloatField;
    QrTXMOEnvETMIQtde: TFloatField;
    DsTXMOEnvETMI: TDataSource;
    QrTXMOEnvRet: TmySQLQuery;
    QrTXMOEnvRetCodigo: TIntegerField;
    QrTXMOEnvRetNFCMO_FatID: TIntegerField;
    QrTXMOEnvRetNFCMO_FatNum: TIntegerField;
    QrTXMOEnvRetNFCMO_Empresa: TIntegerField;
    QrTXMOEnvRetNFCMO_nItem: TIntegerField;
    QrTXMOEnvRetNFCMO_SerNF: TIntegerField;
    QrTXMOEnvRetNFCMO_nNF: TIntegerField;
    QrTXMOEnvRetNFCMO_Qtde: TFloatField;
    QrTXMOEnvRetNFCMO_CusMOUni: TFloatField;
    QrTXMOEnvRetNFCMO_ValorT: TFloatField;
    QrTXMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrTXMOEnvRetNFRMP_FatID: TIntegerField;
    QrTXMOEnvRetNFRMP_FatNum: TIntegerField;
    QrTXMOEnvRetNFRMP_Empresa: TIntegerField;
    QrTXMOEnvRetNFRMP_nItem: TIntegerField;
    QrTXMOEnvRetNFRMP_SerNF: TIntegerField;
    QrTXMOEnvRetNFRMP_nNF: TIntegerField;
    QrTXMOEnvRetNFRMP_Qtde: TFloatField;
    QrTXMOEnvRetNFRMP_ValorT: TFloatField;
    QrTXMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrTXMOEnvRetCFTPA_FatID: TIntegerField;
    QrTXMOEnvRetCFTPA_FatNum: TIntegerField;
    QrTXMOEnvRetCFTPA_Empresa: TIntegerField;
    QrTXMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrTXMOEnvRetCFTPA_nItem: TIntegerField;
    QrTXMOEnvRetCFTPA_SerCT: TIntegerField;
    QrTXMOEnvRetCFTPA_nCT: TIntegerField;
    QrTXMOEnvRetCFTPA_Qtde: TFloatField;
    QrTXMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrTXMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrTXMOEnvRetCFTPA_ValorT: TFloatField;
    DsTXMOEnvRet: TDataSource;
    QrTXMOEnvRTmi: TmySQLQuery;
    QrTXMOEnvRTmiCodigo: TIntegerField;
    QrTXMOEnvRTmiTXMOEnvRet: TIntegerField;
    QrTXMOEnvRTmiTXMOEnvEnv: TIntegerField;
    QrTXMOEnvRTmiQtde: TFloatField;
    QrTXMOEnvRTmiValorFrete: TFloatField;
    QrTXMOEnvRTmiLk: TIntegerField;
    QrTXMOEnvRTmiDataCad: TDateField;
    QrTXMOEnvRTmiDataAlt: TDateField;
    QrTXMOEnvRTmiUserCad: TIntegerField;
    QrTXMOEnvRTmiUserAlt: TIntegerField;
    QrTXMOEnvRTmiAlterWeb: TSmallintField;
    QrTXMOEnvRTmiAWServerID: TIntegerField;
    QrTXMOEnvRTmiAWStatSinc: TSmallintField;
    QrTXMOEnvRTmiAtivo: TSmallintField;
    QrTXMOEnvRTmiNFEMP_SerNF: TIntegerField;
    QrTXMOEnvRTmiNFEMP_nNF: TIntegerField;
    QrTXMOEnvRTmiValorT: TFloatField;
    DsTXMOEnvRTmi: TDataSource;
    QrTXMOEnvGTmi: TmySQLQuery;
    QrTXMOEnvGTmiCodigo: TIntegerField;
    QrTXMOEnvGTmiTXMOEnvRet: TIntegerField;
    QrTXMOEnvGTmiTXMovIts: TIntegerField;
    QrTXMOEnvGTmiQtde: TFloatField;
    QrTXMOEnvGTmiValorFrete: TFloatField;
    QrTXMOEnvGTmiLk: TIntegerField;
    QrTXMOEnvGTmiDataCad: TDateField;
    QrTXMOEnvGTmiDataAlt: TDateField;
    QrTXMOEnvGTmiUserCad: TIntegerField;
    QrTXMOEnvGTmiUserAlt: TIntegerField;
    QrTXMOEnvGTmiAlterWeb: TSmallintField;
    QrTXMOEnvGTmiAWServerID: TIntegerField;
    QrTXMOEnvGTmiAWStatSinc: TSmallintField;
    QrTXMOEnvGTmiAtivo: TSmallintField;
    QrTXMOEnvGTmiTXMovimCod: TIntegerField;
    DsTXMOEnvGTmi: TDataSource;
    PnEnvioMO: TPanel;
    DBGTXMOEnvEnv: TdmkDBGridZTO;
    DBGTXMOEnvETmi: TdmkDBGridZTO;
    PnRetornoMO: TPanel;
    DBGTXMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter2: TSplitter;
    GroupBox8: TGroupBox;
    DBGTXMOEnvRTmi: TdmkDBGridZTO;
    GroupBox9: TGroupBox;
    DBGTXMOEnvGTmi: TdmkDBGridZTO;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    QrIMEIDestCusFrtMOEnv: TFloatField;
    QrIMEIDestCusFrtMORet: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXTrfLocCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXTrfLocCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXTrfLocCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExcluiIMEI1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTXTrfLocCabBeforeClose(DataSet: TDataSet);
    procedure PorIMEI1Click(Sender: TObject);
    procedure PorPallet1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PackingList1Click(Sender: TObject);
    procedure QrPalletsCalcFields(DataSet: TDataSet);
    procedure frxTEX_FAXAO_019_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure Porkgsubproduto1Click(Sender: TObject);
    procedure QrIMEIDestAfterOpen(DataSet: TDataSet);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure BtTXOutNFeCabClick(Sender: TObject);
    procedure IncluiitemdeNF1Click(Sender: TObject);
    procedure AlteraitemdeNF1Click(Sender: TObject);
    procedure PMTXOutNFeItsPopup(Sender: TObject);
    procedure QrTXOutNFeItsBeforeClose(DataSet: TDataSet);
    procedure QrTXOutNFeItsAfterScroll(DataSet: TDataSet);
    procedure BtTribIncItsClick(Sender: TObject);
    procedure AlteraTributoAtual1Click(Sender: TObject);
    procedure ExcluiTributoAtual1Click(Sender: TObject);
    procedure CorrigeFornecedores1Click(Sender: TObject);
    procedure QrIMEIDestBeforeClose(DataSet: TDataSet);
    procedure QrIMEIDestAfterScroll(DataSet: TDataSet);
    procedure QrTXOutNFeCabBeforeClose(DataSet: TDataSet);
    procedure QrTXOutNFeCabAfterScroll(DataSet: TDataSet);
    procedure IncluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure BtTXOutNFeItsClick(Sender: TObject);
    procedure PMTXOutNFeCabPopup(Sender: TObject);
    procedure AlteraSerieNumeroDeNFe1Click(Sender: TObject);
    procedure ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure ExcluiitemdeNF1Click(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure AlteraLocaldeestoque1Click(Sender: TObject);
    procedure InformaIEC1Click(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure QrTXMOEnvAvuAfterScroll(DataSet: TDataSet);
    procedure QrTXMOEnvAvuBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento3Click(Sender: TObject);
    procedure AlteraAtrelamento3Click(Sender: TObject);
    procedure ExcluiAtrelamento3Click(Sender: TObject);
    procedure QrTXMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrTXMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrTXMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure QrTXMOEnvRetBeforeClose(DataSet: TDataSet);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormTXTrfLocIMEI(SQLType: TSQLType);
    procedure MostraFormTXOutPeso(SQLType: TSQLType);
    procedure MostraFormTXTrfLocPall(SQLType: TSQLType);
    procedure ReopenIMEISorc();
    //
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaNFeItens();
    procedure ReopenTXTrfLocIts(Controle: Integer);
    procedure ReopenTXOutNFeCab(Codigo: Integer);
    procedure ReopenTXOutNFeIts(Controle: Integer);
    procedure ReopenTribIncIts(Controle: Integer);
    procedure CorrigeFornecedores();

  end;

var
  FmTXTrfLocCab: TFmTXTrfLocCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
  TXTrfLocIMEI, TXTrfLocPal, ModTX_CRC,
  UnTX_PF;

{$R *.DFM}

const
  FSubTitImp: array [0..1] of String = (
    'Lotes enviados para opra��es e/ou processos',
    'Lotes enviados para demonstra��o e/ou venda'
  );
var
  FSubTitSel: Integer;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXTrfLocCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXTrfLocCab.MostraFormTXTrfLocIMEI(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrTXTrfLocCabCodigo.Value;
  if DBCheck.CriaFm(TFmTXTrfLocIMEI, FmTXTrfLocIMEI, afmoNegarComAviso) then
  begin
    FmTXTrfLocIMEI.ImgTipo.SQLType := SQLType;
    FmTXTrfLocIMEI.FQrCab := QrTXTrfLocCab;
    FmTXTrfLocIMEI.FDsCab := DsTXTrfLocCab;
    FmTXTrfLocIMEI.FQrIts := QrIMEIDest;
    FmTXTrfLocIMEI.FDataHora := QrTXTrfLocCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmTXTrfLocIMEI.EdCPF1.ReadOnly := False
(*
    end else
    begin
      FmTXTrfLocIMEI.FDataHora := QrTXTrfLocCabDtVenda.Value;
      FmTXTrfLocIMEI.EdControle.ValueVariant := QrIMEIDestControle.Value;
      //
      FmTXTrfLocIMEI.EdGragruX.ValueVariant  := QrIMEIDestGraGruX.Value;
      FmTXTrfLocIMEI.CBGragruX.KeyValue      := QrIMEIDestGraGruX.Value;
      FmTXTrfLocIMEI.EdQtde.ValueVariant     := -QrIMEIDestQtde.Value;
      //FmTXTrfLocIMEI.EdValorT.ValueVariant   := -QrIMEIDestValorT.Value;
      FmTXTrfLocIMEI.EdPallet.ValueVariant   := QrIMEIDestPallet.Value;
      //
    end;
*)
    end;
    FmTXTrfLocIMEI.ShowModal;
    FmTXTrfLocIMEI.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXTrfLocCab.MostraFormTXTrfLocPall(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrTXTrfLocCabCodigo.Value;
  if DBCheck.CriaFm(TFmTXTrfLocPal, FmTXTrfLocPal, afmoNegarComAviso) then
  begin
    FmTXTrfLocPal.ImgTipo.SQLType := SQLType;
    FmTXTrfLocPal.FQrCab := QrTXTrfLocCab;
    FmTXTrfLocPal.FDsCab := DsTXTrfLocCab;
    FmTXTrfLocPal.FQrIts := QrIMEIDest;
    FmTXTrfLocPal.FDataHora := QrTXTrfLocCabDtVenda.Value;
    FmTXTrfLocPal.FEmpresa  := QrTXTrfLocCabEmpresa.Value;
    FmTXTrfLocPal.FClientMO := 0; //QrTXTrfLocCabClientMO.Value;
    FmTXTrfLocPal.PesquisaPallets();
    //
    FmTXTrfLocPal.ShowModal;
    FmTXTrfLocPal.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXTrfLocCab.IncluiSerieNumeroDeNFe1Click(Sender: TObject);
const
  Codigo    = 0;
  ide_serie = 0;
  ide_nNF   = 0;
begin
{POIU
  if TX_PF.MostraFormTXNFeOutCab(stIns, QrTXOutNfeCab, QrTXTrfLocCabMovimCod.Value,
  QrTXTrfLocCabCodigo.Value, Codigo, ide_serie, ide_nNF) then
    TX_PF.MostraFormTXNFeOutIts(stIns, QrTXOutNfeIts, QrTribIncIts,
      QrTXOutNfeCabCodigo.Value, QrTXOutNfeCabMovimCod.Value,
      QrTXTrfLocCabEmpresa.Value, QrTXTrfLocCabDtVenda.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
}
end;

procedure TFmTXTrfLocCab.MostraFormTXOutPeso(SQLType: TSQLType);
var
  Codigo: Integer;
begin
{
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrTXTrfLocCabCodigo.Value;
  if DBCheck.CriaFm(TFmTXOutPeso, FmTXOutPeso, afmoNegarComAviso) then
  begin
    FmTXOutPeso.ImgTipo.SQLType := SQLType;
    FmTXOutPeso.FQrCab := QrTXTrfLocCab;
    FmTXOutPeso.FDsCab := DsTXTrfLocCab;
    FmTXOutPeso.FQrIts := QrIMEIDest;
    FmTXOutPeso.FDataHora := QrTXTrfLocCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmTXOutPeso.EdCPF1.ReadOnly := False
(*
    end else
    begin
      FmTXOutPeso.FDataHora := QrTXTrfLocCabDtVenda.Value;
      FmTXOutPeso.EdControle.ValueVariant := QrIMEIDestControle.Value;
      //
      FmTXOutPeso.EdGragruX.ValueVariant  := QrIMEIDestGraGruX.Value;
      FmTXOutPeso.CBGragruX.KeyValue      := QrIMEIDestGraGruX.Value;
      FmTXOutPeso.EdQtde.ValueVariant     := -QrIMEIDestQtde.Value;
      //FmTXOutPeso.EdValorT.ValueVariant   := -QrIMEIDestValorT.Value;
      FmTXOutPeso.EdPallet.ValueVariant   := QrIMEIDestPallet.Value;
      //
    end;
*)
    end;
    FmTXOutPeso.ShowModal;
    FmTXOutPeso.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
}
end;

procedure TFmTXTrfLocCab.PackingList1Click(Sender: TObject);
var
  Linhas: String;
  QtdLin: Integer;
  MD003: TfrxMasterData;
begin
  FSubTitSel := MyObjects.SelRadioGroup('Tranfer�ncia de Estoque',
  'Selecione a finalidade', FSubTitImp, 1);
  if not (FSubTitSel in ([0,1])) then
    Exit;
  Linhas := '27';
  if InputQuery('Linhas de Baixa de Estoque', 'Informe a quantidade de linhas',
  Linhas) then
    QtdLin := Geral.IMV(Linhas)
  else
    QtdLin := 0;
  //
  MD003 := frxTEX_FAXAO_019_00_A.FindObject('MD003') as TfrxMasterData;
  MD003.RowCount := QtdLin;
  //
  MyObjects.frxDefineDataSets(frxTEX_FAXAO_019_00_A, [
  DModG.frxDsDono,
  frxDsPallets,
  frxDsTXOutNFeCab,
  frxDsTXOutNFeIts
  ]);
  MyObjects.frxMostra(frxTEX_FAXAO_019_00_A, 'Packing List');
end;

procedure TFmTXTrfLocCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXTrfLocCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTXTrfLocCab, QrIMEIDest);
end;

procedure TFmTXTrfLocCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTXTrfLocCab);
  //MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrIMEIDest);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiIMEI1, QrIMEIDest);
  ItsExcluiIMEI1.Enabled :=
    ItsExcluiIMEI1.Enabled and (PCItens.ActivePageIndex = 1);
  InformaNmerodaRME1.Enabled := ItsExcluiIMEI1.Enabled;
  //InformaItemdeNFe1.Enabled := ItsExcluiIMEI1.Enabled;
  AlteraLocaldeestoque1.Enabled := ItsExcluiIMEI1.Enabled;
  //
  TX_PF.HabilitaComposTXAtivo(QrIMEIDestID_TTW.Value, [(*ItsAltera1,*) ItsExcluiIMEI1]);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento3, QrIMEIDest);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento3, QrTXMOEnvAvu);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento3, QrTXMOEnvAvu);
  AtrelamentoFreteCompra1.Enabled := PCItens.ActivePage = TsFrCompr;
  //
  AtrelamentoNFsdeMO1.Enabled := PCItens.ActivePage = TsEnvioMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrIMEIDest);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrTXMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrTXMOEnvEnv);
  //
  MyObjects.HabilitaMenuItemItsIns(InformaIEC1, QrIMEIDest);
end;

procedure TFmTXTrfLocCab.PMTXOutNFeCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSerieNumeroDeNFe1, QrTXTrfLocCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSerieNumeroDeNFe1, QrTXOutNFeCab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiSerieNumeroDeNFe1, QrTXOutNFeCab, QrTXOutNFeIts);
end;

procedure TFmTXTrfLocCab.PMTXOutNFeItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiitemdeNF1, QrTXOutNFeCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraitemdeNF1, QrTXOutNFeIts);
  MyObjects.HabilitaMenuItemCabDel(ExcluiitemdeNF1, QrTXOutNFeIts, QrTribIncIts);
end;

procedure TFmTXTrfLocCab.PorIMEI1Click(Sender: TObject);
begin
  MostraFormTXTrfLocIMEI(stIns);
end;

procedure TFmTXTrfLocCab.Porkgsubproduto1Click(Sender: TObject);
begin
{
  MostraFormTXOutPeso(stIns);
}
end;

procedure TFmTXTrfLocCab.PorPallet1Click(Sender: TObject);
begin
  MostraFormTXTrfLocPall(stIns);
end;

procedure TFmTXTrfLocCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXTrfLocCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXTrfLocCab.DefParams;
begin
  VAR_GOTOTABELA := 'txtrfloccab';
  VAR_GOTOMYSQLTABLE := QrTXTrfLocCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA');
  VAR_SQLx.Add('FROM txtrfloccab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.transporta');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTXTrfLocCab.DGDadosDblClick(Sender: TObject);
begin
  if TX_PF.EditaIxx(TDBGrid(DGDados), QrIMEIDest) then
    PCItens.ActivePageIndex := 1;
end;

procedure TFmTXTrfLocCab.EdClienteChange(Sender: TObject);
begin
  if EdCliente.VAlueVariant = 0 then
    LaCliente.Caption := '...'
  else
    LaCliente.Caption := Geral.FormataCNPJ_TT(QrClienteCNPJ_CPF.Value);
end;

procedure TFmTXTrfLocCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  PCItens.ActivePage := TsEnvioMO;
  //
  DmModTX_CRC.ExcluiAtrelamentoMOEnvEnv(QrTXMOEnvEnv, QrTXMOEnvETMI);
  //
  TX_PF.AtualizaTotaisTXOpeCab(QrTXTrfLocCabMovimCod.Value);
  //
  LocCod(QrTXTrfLocCabCodigo.Value, QrTXTrfLocCabCodigo.Value);
}
{$Else}
{
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
}
{$EndIf}
end;

procedure TFmTXTrfLocCab.ExcluiAtrelamento3Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  PCItens.ActivePage := TsFrCompr;
  //
  DmModTX_CRC.ExcluiAtrelamentoMOEnvAvu(QrTXMOEnvAvu, QrTXMOEnvATMI);
  //
  LocCod(QrTXTrfLocCabCodigo.Value,QrTXTrfLocCabCodigo.Value);
}
{$Else}
{
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
}
{$EndIf}
end;

procedure TFmTXTrfLocCab.ExcluiitemdeNF1Click(Sender: TObject);
var
  Controle: Integer;
begin
{$IfDef sAllTX}
  if TX_PF.ExcluiTXNaoTMI('Confirma a exclus�o do item selecionado?',
  'txoutnfeIts', 'Controle', QrTXOutNFeItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTXOutNFeIts,
      QrTXOutNFeItsControle, QrTXOutNFeItsControle.Value);
    ReopenTXOutNFeIts(Controle);
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmTXTrfLocCab.ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{$IfDef sAllTX}
{POIU
  if TX_PF.ExcluiTXNaoTMI('Confirma a exclus�o do item selecionado?',
  'txoutnfecab', 'Codigo', QrTXOutNFeCabCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrTXOutNFeCab,
      QrTXOutNFeCabCodigo, QrTXOutNFeCabCodigo.Value);
    ReopenTXOutNFeCab(Codigo);
  end;
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmTXTrfLocCab.ExcluiTributoAtual1Click(Sender: TObject);
var
  Controle: Integer;
begin
{POIU
  Controle := QrTribIncItsControle.Value;
  if TX_PF.ExcluiTXNaoTMI('Confirma a exclus�o deste item de tributo?',
  'tribincits', 'Controle', Controle, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTribIncIts,
      QrTribIncItsControle, QrTribIncItsControle.Value);
    ReopenTribIncIts(Controle);
  end;
}
end;

procedure TFmTXTrfLocCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCItens.ActivePageIndex := 2;
  PCItens.ActivePage := TsEnvioMO;
  //
  TX_PF.MostraFormTXMOEnvEnv(stIns, QrIMEIDest, QrTXMOEnvEnv, QrTXMOEnvETMI,
  siNegativo, QrTXTrfLocCabide_serie.Value, QrTXTrfLocCabide_nNF.Value);
  //
  LocCod(QrTXTrfLocCabCodigo.Value,QrTXTrfLocCabCodigo.Value);
  //
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmTXTrfLocCab.IncluiAtrelamento3Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  PCItens.ActivePage := TsFrCompr;
  //
  TX_PF.MostraFormTXMOEnvAvu(stIns, QrIMEIDest, QrTXMOEnvAvu, QrTXMOEnvATMI,
  siPositivo,
  QrTXTrfLocCabCliente.Value, QrTXTrfLocCabide_serie.Value, QrTXTrfLocCabide_nNF.Value);

  //
  TX_PF.ReopenTXMOEnvAvu(QrTXMOEnvAvu, QrTXTrfLocCabMovimCod.Value, 0);
  //
  LocCod(QrTXTrfLocCabCodigo.Value,QrTXTrfLocCabCodigo.Value);
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmTXTrfLocCab.IncluiitemdeNF1Click(Sender: TObject);
begin
{POIU
  TX_PF.MostraFormTXNFeOutIts(stIns, QrTXOutNfeIts, QrTribIncIts,
    QrTXOutNfeCabCodigo.Value, QrTXOutNfeCabMovimCod.Value,
      QrTXTrfLocCabEmpresa.Value, QrTXTrfLocCabDtVenda.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
}
end;

procedure TFmTXTrfLocCab.InformaIEC1Click(Sender: TObject);
begin
  if TX_PF.InformaIxx(DGDados, QrIMEIDest, 'Controle') then
    PCItens.ActivePageIndex := 1;
end;

procedure TFmTXTrfLocCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  TX_PF.InfoReqMovEstq(QrIMEIDestControle.Value,
    QrIMEIDestReqMovEstq.Value, QrIMEIDest);
  ReopenTXTrfLocIts(QrIMEIDestControle.Value);
  PCItens.ActivePageIndex := 1;
end;

procedure TFmTXTrfLocCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if MyObjects.FIC((QrPallets.RecordCount > 0) or (QrIMEIDest.RecordCount > 0)
  {POIU or (QrTXOutNFeCab.RecordCount > 0)}, nil,
  'Existem lan�anmentos em tabelas filhas que impedem a exclus�o do cabe�alho!')
  then Exit;
  //
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do cabe�alho selecionado?',
  'txtrfloccab', 'Codigo', QrTXTrfLocCabCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrTXTrfLocCab,
      QrTXTrfLocCabCodigo, QrTXTrfLocCabCodigo.Value);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXTrfLocCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast); Desabilita botao quando fecha mes!
end;

procedure TFmTXTrfLocCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXTrfLocCab.ItsExcluiIMEI1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrIMEIDestCodigo.Value;
  MovimCod := QrIMEIDestMovimCod.Value;
  //
  if TX_PF.ExcluiControleTXMovIts(QrIMEIDest, TIntegerField(QrIMEIDestControle),
  QrIMEIDestControle.Value, QrIMEISorcControle.Value, QrIMEISorcSrcNivel2.Value,
  False, Integer(TEstqMotivDel.emtdWetCurti131)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txtrfloccab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXTrfLocCab.ReopenIMEISorc();
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXTrfLocCabTemIMEIMrt.Value;
  //
  // Por IME-I
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  TX_PF.SQL_NO_FRN(),
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  TX_PF.SQL_LJ_FRN(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXTrfLocCabMovimCod.Value),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcLocal)),
  'AND tmi.MovimTwn=' + Geral.FF0(QrIMEIDestMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEISorc, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrIMEISorc);
end;

procedure TFmTXTrfLocCab.ReopenTribIncIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribIncIts, Dmod.MyDB, [
  'SELECT tii.*, tdd.Nome NO_Tributo  ',
  'FROM tribincits tii ',
  'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo',
  'WHERE tii.FatID=' + Geral.FF0(VAR_FATID_1009),
  'AND tii.FatNum=' + Geral.FF0(QrTXOutNFeCabCodigo.Value),
  'AND tii.FatParcela=' + Geral.FF0(QrTXOutNFeItsItemNFe.Value),
  'ORDER BY tii.Controle DESC ',
  '']);
end;

procedure TFmTXTrfLocCab.ReopenTXTrfLocIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXTrfLocCabTemIMEIMrt.Value;
  //
  // Por IME-I
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  TX_PF.SQL_NO_FRN(),
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  TX_PF.SQL_LJ_FRN(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXTrfLocCabMovimCod.Value),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestLocal)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIDest, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrIMEIDest);
  //
  QrIMEIDest.Locate('Controle', Controle, []);
  // Por Pallet
  //
  SQL_Flds := Geral.ATS([
  ', IF(cou.ArtigoImp = "",  ',
  '  CONCAT(gg1.Nome,  ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))), cou.ArtigoImp) TXT_ARTIGO, ',
  'cou.ArtigoImp, cou.ClasseImp ',
  '']);
  SQL_Left := Geral.ATS([
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle  ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXTrfLocCabMovimCod.Value),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestLocal)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
end;

procedure TFmTXTrfLocCab.ReopenTXOutNFeCab(Codigo: Integer);
begin
{$IfDef sAllTX}
{POIU
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXOutNFeCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM txoutnfecab ',
  'WHERE MovimCod=' + Geral.FF0(QrTXTrfLocCabMovimCod.Value),
  '']);
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmTXTrfLocCab.ReopenTXOutNFeIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXOutNFeIts, Dmod.MyDB, [
  'SELECT CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, voi.* ',
  'FROM txoutnfeits voi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE voi.Codigo=' + Geral.FF0(QrTXOutNFeCabCodigo.Value),
  '']);
end;

procedure TFmTXTrfLocCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXTrfLocCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXTrfLocCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXTrfLocCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXTrfLocCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXTrfLocCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXTrfLocCab.BtTribIncItsClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMTribIncIts, BtTribIncIts);
end;

procedure TFmTXTrfLocCab.BtTXOutNFeCabClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMTXOutNFeCab, BtTXOutNFeCab);
end;

procedure TFmTXTrfLocCab.BtTXOutNFeItsClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMTXOutNFeIts, BtTXOutNFeIts);
end;

procedure TFmTXTrfLocCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXTrfLocCabCodigo.Value;
  Close;
end;

procedure TFmTXTrfLocCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXTrfLocCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txtrfloccab');
  Empresa := DModG.ObtemFilialDeEntidade(QrTXTrfLocCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmTXTrfLocCab.BtConfirmaClick(Sender: TObject);
var
  DtVenda, DtViagem, DtEntrega, DataHora: String;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
  Codigo, MovimCod, Empresa, Cliente, Transporta, CliVenda, ide_serie, ide_nNF,
  Forma: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtVenda        := Geral.FDT_TP_Ed(TPDtVenda.Date, EdDtVenda.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrega      := Geral.FDT_TP_Ed(TPDtEntrega.Date, EdDtEntrega.Text);
  Cliente        := EdCliente.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  //
  if not TX_PF.ValidaCampoNF(1, Edide_nNF, True) then Exit;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtVenda.DateTime < 2, TPDtVenda, 'Defina uma data de Venda!') then Exit;
  //DataChegadaInvalida := TPDtEntrega.Date < TPDtViagem.Date;
  //if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de chegada v�lida!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txtrfloccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'txtrfloccab', False, [
  'MovimCod', 'Empresa', 'DtVenda',
  'DtViagem', 'DtEntrega', 'Cliente',
  'Transporta'(*, 'Qtde', 'ValorT'*),
  'ide_serie', 'ide_nNF'], [
  'Codigo'], [
  MovimCod, Empresa, DtVenda,
  DtViagem, DtEntrega, Cliente,
  Transporta (*Qtde, ValorT*),
  ide_serie, ide_nNF], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      TX_PF.InsereTXMovCab(MovimCod, emidTransfLoc, Codigo)
    else
    begin
      DataHora := DtVenda;
      CliVenda := Cliente;
      //
      if Edide_nNF.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_TMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrIMEIDest.RecordCount = 0) then
    begin
      //MostraTXGArtOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', ['Pallet', 'IME-I', 'Peso kg (sub produto)'], -1);
      case Forma of
        0: MostraFormTXTrfLocPall(stIns);
        1: MostraFormTXTrfLocIMEI(stIns);
        2: MostraFormTXOutPeso(stIns);
      end;
    end;
  end;
end;

procedure TFmTXTrfLocCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'txtrfloccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txtrfloccab', 'Codigo');
end;

procedure TFmTXTrfLocCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTXTrfLocCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{
  PCItens.ActivePage := TsEnvioMO;
  //
  TX_PF.MostraFormTXMOEnvEnv(stUpd, QrIMEIDest, QrTXMOEnvEnv, QrTXMOEnvETMI,
  siNegativo, 0, 0);
  //
  LocCod(QrTXTrfLocCabCodigo.Value,QrTXTrfLocCabCodigo.Value);
  //
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmTXTrfLocCab.AlteraAtrelamento3Click(Sender: TObject);
begin
{$IfDef sAllTX}
{
  PCItens.ActivePage := TsFrCompr;
  //
  TX_PF.MostraFormTXMOEnvAvu(stUpd, QrIMEIDest, QrTXMOEnvAvu, QrTXMOEnvATMI,
  siPositivo,
  QrTXTrfLocCabCliente.Value, QrTXTrfLocCabide_serie.Value, QrTXTrfLocCabide_nNF.Value);
  //
  TX_PF.ReopenTXMOEnvAvu(QrTXMOEnvAvu, QrTXTrfLocCabMovimCod.Value, 0);
  //
  LocCod(QrTXTrfLocCabCodigo.Value,QrTXTrfLocCabCodigo.Value);
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmTXTrfLocCab.AlteraitemdeNF1Click(Sender: TObject);
begin
{POIU
  TX_PF.MostraFormTXNFeOutIts(stUpd, QrTXOutNfeIts, QrTribIncIts,
    QrTXOutNfeCabCodigo.Value, QrTXOutNfeCabMovimCod.Value,
      QrTXTrfLocCabEmpresa.Value, QrTXTrfLocCabDtVenda.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
}
end;

procedure TFmTXTrfLocCab.AlteraLocaldeestoque1Click(Sender: TObject);
begin
  TX_PF.InfoStqCenLoc(QrIMEIDestControle.Value,
    QrIMEIDestStqCenLoc.Value, QrIMEIDest);
end;

procedure TFmTXTrfLocCab.AlteraSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo, ide_serie, ide_nNF: Integer;
begin
{POIU{
  Codigo    := QrTXOutNFeCabCodigo.Value;
  ide_serie := QrTXOutNFeCabide_serie.Value;
  ide_nNF   := QrTXOutNFeCabide_nNF.Value;
  //
  TX_PF.MostraFormTXNFeOutCab(stUpd, QrTXOutNfeCab, QrTXTrfLocCabMovimCod.Value,
  QrTXTrfLocCabCodigo.Value, Codigo, ide_serie, ide_nNF);
}
end;

procedure TFmTXTrfLocCab.AlteraTributoAtual1Click(Sender: TObject);
{POIU
var
  Operacao, Tributo, FatID, FatNum, FatParcela, Empresa, Controle: Integer;
  Data, Hora: TDateTime;
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib: Double;
}
begin
{$IfDef sAllTX}
{POIU
  FatNum     := QrTribIncItsFatNum.Value;
  FatParcela := QrTribIncItsFatParcela.Value;
  Empresa    := QrTribIncItsEmpresa.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Controle   := QrTribIncItsControle.Value;
  Operacao   := QrTribIncItsOperacao.Value;
  Tributo    := QrTribIncItsTributo.Value;
  ValorFat   := QrTribIncItsValorFat.Value;
  BaseCalc   := QrTribIncItsBaseCalc.Value;
  Percent    := QrTribIncItsPercent.Value;
  VUsoCalc   := QrTribIncItsVUsoCalc.Value;
  ValrTrib   := QrTribIncItsValrTrib.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stUpd, QrTribIncIts, VAR_FATID_1009,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siNegativo);
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}end;

procedure TFmTXTrfLocCab.AtualizaNFeItens();
begin
  DmModTX_CRC.AtualizaTXMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
end;

procedure TFmTXTrfLocCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXTrfLocCab, QrTXTrfLocCabDtVenda.Value,
  BtCab, PMCab, [CabInclui1, CorrigeFornecedores1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXTrfLocCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCItens, VAR_PC_FRETE_TX_NAME, 0);
end;

procedure TFmTXTrfLocCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXTrfLocCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXTrfLocCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXTrfLocCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXTrfLocCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrTXTrfLocCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXTrfLocCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXTrfLocCab.QrPalletsCalcFields(DataSet: TDataSet);
begin
  QrPalletsSEQ.Value := QrPallets.RecNo;
end;

procedure TFmTXTrfLocCab.QrTXTrfLocCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXTrfLocCab.QrTXTrfLocCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTXTrfLocIts(0);
  ReopenTXOutNFeCab(0);
  TX_PF.ReopenTXMOEnvAvu(QrTXMOEnvAvu, QrTXTrfLocCabMovimCod.Value, 0);
  TX_PF.ReopenTXMOEnvEnv(QrTXMOEnvEnv, QrTXTrfLocCabMovimCod.Value, 0);
  TX_PF.ReopenTXMOEnvRet(QrTXMOEnvRet, QrTXTrfLocCabMovimCod.Value, 0);
end;

procedure TFmTXTrfLocCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXTrfLocCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXTrfLocCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := TX_Jan.MostraFormTXTalPsq(emidTransfLoc, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenTXTrfLocIts(Controle);
  end;
end;

procedure TFmTXTrfLocCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXTrfLocCab.frxTEX_FAXAO_019_00_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := QrTXTrfLocCabNO_EMPRESA.Value
  else
  if VarName = 'VARF_CLIENTE' then
    Value := QrTXTrfLocCabNO_CLIENTE.Value
  else
  if VarName = 'VARF_CODIGO' then
    Value := QrTXTrfLocCabCodigo.Value
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_SUB_TIT' then
    Value := FSubTitImp[FSubTitSel]
  else
end;

procedure TFmTXTrfLocCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXTrfLocCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txtrfloccab');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDtVenda.Date := Agora;
  EdDtVenda.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtVenda.SetFocus;
end;

procedure TFmTXTrfLocCab.CorrigeFornecedores();
begin
{
  QrIMEIDest.First;
  while not QrIMEIDest.Eof do
  begin
    TX_PF.AtualizaFornecedorOut(QrIMEIDestControle.Value);
    //
    QrIMEIDest.Next;
  end;
  LocCod(QrTXTrfLocCabCodigo.Value, QrTXTrfLocCabCodigo.Value);
}
end;

procedure TFmTXTrfLocCab.CorrigeFornecedores1Click(Sender: TObject);
begin
  CorrigeFornecedores();
end;

procedure TFmTXTrfLocCab.QrTXTrfLocCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXMOEnvEnv.Close;
  QrTXMOEnvRet.Close;
  QrTXMOEnvAvu.Close;
  QrIMEIDest.Close;
  QrTXOutNFeCab.Close;
end;

procedure TFmTXTrfLocCab.QrTXTrfLocCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXTrfLocCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTXTrfLocCab.QrIMEIDestAfterOpen(DataSet: TDataSet);
begin
  if (QrIMEIDest.RecordCount > 0) and (PCItens.ActivePageIndex < 2) then
  begin
    if QrIMEIDestPallet.Value > 0 then
      PCItens.ActivePageIndex := 0
    else
      PCItens.ActivePageIndex := 1;
  end;
end;

procedure TFmTXTrfLocCab.QrIMEIDestAfterScroll(DataSet: TDataSet);
begin
  ReopenIMEISorc();
end;

procedure TFmTXTrfLocCab.QrIMEIDestBeforeClose(DataSet: TDataSet);
begin
  QrIMEISorc.Close;
end;

procedure TFmTXTrfLocCab.QrTXMOEnvAvuAfterScroll(DataSet: TDataSet);
begin
  TX_PF.ReopenTXMOEnvATMI(QrTXMOEnvATmi, QrTXMOEnvAvuCodigo.VAlue, 0);
end;

procedure TFmTXTrfLocCab.QrTXMOEnvAvuBeforeClose(DataSet: TDataSet);
begin
  QrTXMOEnvATMI.Close;
end;

procedure TFmTXTrfLocCab.QrTXMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  TX_PF.ReopenTXMOEnvETmi(QrTXMOEnvETmi, QrTXMOEnvEnvCodigo.Value, 0);
end;

procedure TFmTXTrfLocCab.QrTXMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrTXMOEnvETMI.Close;
end;

procedure TFmTXTrfLocCab.QrTXMOEnvRetAfterScroll(DataSet: TDataSet);
begin
  TX_PF.ReopenTXMOEnvRTmi(QrTXMOEnvRTmi, QrTXMOEnvRetCodigo.Value, 0);
  TX_PF.ReopenTXMOEnvGTmi(QrTXMOEnvGTmi, QrTXMOEnvRetCodigo.Value, 0);
end;

procedure TFmTXTrfLocCab.QrTXMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrTXMOEnvGTmi.Close;
  QrTXMOEnvRTmi.Close;
end;

procedure TFmTXTrfLocCab.QrTXOutNFeCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTXOutNFeIts(0);
end;

procedure TFmTXTrfLocCab.QrTXOutNFeCabBeforeClose(DataSet: TDataSet);
begin
  QrTXOutNFeIts.Close;
end;

procedure TFmTXTrfLocCab.QrTXOutNFeItsAfterScroll(DataSet: TDataSet);
begin
  ReopenTribIncIts(0);
end;

procedure TFmTXTrfLocCab.QrTXOutNFeItsBeforeClose(DataSet: TDataSet);
begin
  QrTribIncIts.Close;
end;

{
object IncluiAtrelamento1: TMenuItem
  Caption = '&Inclui Atrelamento'
  OnClick = IncluiAtrelamento1Click
end
object AlteraAtrelamento1: TMenuItem
  Caption = '&Altera Atrelamento'
  OnClick = AlteraAtrelamento1Click
end
object ExcluiAtrelamento1: TMenuItem
  Caption = '&Exclui Atrelamento'
  OnClick = ExcluiAtrelamento1Click
end
}
end.

