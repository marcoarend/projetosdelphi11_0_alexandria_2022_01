unit TXIndCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnProjGroup_Consts,
  UnMyObjects, UnGrl_Consts, UnTX_EFD_ICMS_IPI, UnAppEnums, dmkDBGridZTO,
  UnProjGroup_Vars, mySQLDirectQuery, UMySQLDB;

type
  TFmTXIndCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrTXIndCab: TmySQLQuery;
    DsTXIndCab: TDataSource;
    QrTXIndInn: TmySQLQuery;
    DsTXIndInn: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrTXIndOri: TmySQLQuery;
    DsTXIndOri: TDataSource;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label3: TLabel;
    EdFluxoCab: TdmkEditCB;
    CBFluxoCab: TdmkDBLookupComboBox;
    N1: TMenuItem;
    CabLibera1: TMenuItem;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    CabReeditar1: TMenuItem;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    DsTXIndDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    N3: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    QrTXIndBxa: TmySQLQuery;
    DsTXIndBxa: TDataSource;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    DBEdit31: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    DBEdit35: TDBEdit;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    DBEdit30: TDBEdit;
    Definiodequantidadesmanualmente1: TMenuItem;
    PCIndOri: TPageControl;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    QrTXPedIts: TmySQLQuery;
    QrTXPedItsNO_PRD_TAM_COR: TWideStringField;
    QrTXPedItsControle: TIntegerField;
    DsTXPedIts: TDataSource;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Corrigirfornecedor1: TMenuItem;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    DBEdit44: TDBEdit;
    Label58: TLabel;
    Label59: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label56: TLabel;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label57: TLabel;
    Ordensdeoperaoemaberto1: TMenuItem;
    porPalletparcial1: TMenuItem;
    N4: TMenuItem;
    porIMEIParcial1: TMenuItem;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosQtde: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtQtd: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOUni: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGer: TFloatField;
    QrForcadosQtdAnt: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    QrTXIndOriCodigo: TLargeintField;
    QrTXIndOriControle: TLargeintField;
    QrTXIndOriMovimCod: TLargeintField;
    QrTXIndOriMovimNiv: TLargeintField;
    QrTXIndOriMovimTwn: TLargeintField;
    QrTXIndOriEmpresa: TLargeintField;
    QrTXIndOriTerceiro: TLargeintField;
    QrTXIndOriCliVenda: TLargeintField;
    QrTXIndOriMovimID: TLargeintField;
    QrTXIndOriDataHora: TDateTimeField;
    QrTXIndOriPallet: TLargeintField;
    QrTXIndOriGraGruX: TLargeintField;
    QrTXIndOriQtde: TFloatField;
    QrTXIndOriValorT: TFloatField;
    QrTXIndOriSrcMovID: TLargeintField;
    QrTXIndOriSrcNivel1: TLargeintField;
    QrTXIndOriSrcNivel2: TLargeintField;
    QrTXIndOriSrcGGX: TLargeintField;
    QrTXIndOriSdoVrtQtd: TFloatField;
    QrTXIndOriObserv: TWideStringField;
    QrTXIndOriFornecMO: TLargeintField;
    QrTXIndOriCustoMOUni: TFloatField;
    QrTXIndOriCustoMOTot: TFloatField;
    QrTXIndOriValorMP: TFloatField;
    QrTXIndOriDstMovID: TLargeintField;
    QrTXIndOriDstNivel1: TLargeintField;
    QrTXIndOriDstNivel2: TLargeintField;
    QrTXIndOriDstGGX: TLargeintField;
    QrTXIndOriQtdGer: TFloatField;
    QrTXIndOriQtdAnt: TFloatField;
    QrTXIndOriNO_PALLET: TWideStringField;
    QrTXIndOriNO_PRD_TAM_COR: TWideStringField;
    QrTXIndOriNO_TTW: TWideStringField;
    QrTXIndOriID_TTW: TLargeintField;
    QrTXIndOriNO_FORNECE: TWideStringField;
    QrTXIndOriReqMovEstq: TLargeintField;
    QrTXIndDst: TmySQLQuery;
    QrTXIndDstCodigo: TLargeintField;
    QrTXIndDstControle: TLargeintField;
    QrTXIndDstMovimCod: TLargeintField;
    QrTXIndDstMovimNiv: TLargeintField;
    QrTXIndDstMovimTwn: TLargeintField;
    QrTXIndDstEmpresa: TLargeintField;
    QrTXIndDstTerceiro: TLargeintField;
    QrTXIndDstCliVenda: TLargeintField;
    QrTXIndDstMovimID: TLargeintField;
    QrTXIndDstDataHora: TDateTimeField;
    QrTXIndDstPallet: TLargeintField;
    QrTXIndDstGraGruX: TLargeintField;
    QrTXIndDstQtde: TFloatField;
    QrTXIndDstValorT: TFloatField;
    QrTXIndDstSrcMovID: TLargeintField;
    QrTXIndDstSrcNivel1: TLargeintField;
    QrTXIndDstSrcNivel2: TLargeintField;
    QrTXIndDstSrcGGX: TLargeintField;
    QrTXIndDstSdoVrtQtd: TFloatField;
    QrTXIndDstObserv: TWideStringField;
    QrTXIndDstFornecMO: TLargeintField;
    QrTXIndDstCustoMOUni: TFloatField;
    QrTXIndDstCustoMOTot: TFloatField;
    QrTXIndDstValorMP: TFloatField;
    QrTXIndDstDstMovID: TLargeintField;
    QrTXIndDstDstNivel1: TLargeintField;
    QrTXIndDstDstNivel2: TLargeintField;
    QrTXIndDstDstGGX: TLargeintField;
    QrTXIndDstQtdGer: TFloatField;
    QrTXIndDstQtdAnt: TFloatField;
    QrTXIndDstNO_PALLET: TWideStringField;
    QrTXIndDstNO_PRD_TAM_COR: TWideStringField;
    QrTXIndDstNO_TTW: TWideStringField;
    QrTXIndDstID_TTW: TLargeintField;
    QrTXIndDstNO_FORNECE: TWideStringField;
    QrTXIndDstReqMovEstq: TLargeintField;
    QrTXIndDstMarca: TWideStringField;
    QrTXIndDstPedItsFin: TLargeintField;
    QrTXIndBxaCodigo: TLargeintField;
    QrTXIndBxaControle: TLargeintField;
    QrTXIndBxaMovimCod: TLargeintField;
    QrTXIndBxaMovimNiv: TLargeintField;
    QrTXIndBxaMovimTwn: TLargeintField;
    QrTXIndBxaEmpresa: TLargeintField;
    QrTXIndBxaTerceiro: TLargeintField;
    QrTXIndBxaCliVenda: TLargeintField;
    QrTXIndBxaMovimID: TLargeintField;
    QrTXIndBxaDataHora: TDateTimeField;
    QrTXIndBxaPallet: TLargeintField;
    QrTXIndBxaGraGruX: TLargeintField;
    QrTXIndBxaQtde: TFloatField;
    QrTXIndBxaValorT: TFloatField;
    QrTXIndBxaSrcMovID: TLargeintField;
    QrTXIndBxaSrcNivel1: TLargeintField;
    QrTXIndBxaSrcNivel2: TLargeintField;
    QrTXIndBxaSrcGGX: TLargeintField;
    QrTXIndBxaSdoVrtQtd: TFloatField;
    QrTXIndBxaObserv: TWideStringField;
    QrTXIndBxaFornecMO: TLargeintField;
    QrTXIndBxaCustoMOUni: TFloatField;
    QrTXIndBxaCustoMOTot: TFloatField;
    QrTXIndBxaValorMP: TFloatField;
    QrTXIndBxaDstMovID: TLargeintField;
    QrTXIndBxaDstNivel1: TLargeintField;
    QrTXIndBxaDstNivel2: TLargeintField;
    QrTXIndBxaDstGGX: TLargeintField;
    QrTXIndBxaQtdGer: TFloatField;
    QrTXIndBxaQtdAnt: TFloatField;
    QrTXIndBxaNO_PALLET: TWideStringField;
    QrTXIndBxaNO_PRD_TAM_COR: TWideStringField;
    QrTXIndBxaNO_TTW: TWideStringField;
    QrTXIndBxaID_TTW: TLargeintField;
    QrTXIndBxaNO_FORNECE: TWideStringField;
    QrTXIndBxaReqMovEstq: TLargeintField;
    QrTXIndInnCodigo: TLargeintField;
    QrTXIndInnControle: TLargeintField;
    QrTXIndInnMovimCod: TLargeintField;
    QrTXIndInnMovimNiv: TLargeintField;
    QrTXIndInnMovimTwn: TLargeintField;
    QrTXIndInnEmpresa: TLargeintField;
    QrTXIndInnTerceiro: TLargeintField;
    QrTXIndInnCliVenda: TLargeintField;
    QrTXIndInnMovimID: TLargeintField;
    QrTXIndInnDataHora: TDateTimeField;
    QrTXIndInnPallet: TLargeintField;
    QrTXIndInnGraGruX: TLargeintField;
    QrTXIndInnQtde: TFloatField;
    QrTXIndInnValorT: TFloatField;
    QrTXIndInnSrcMovID: TLargeintField;
    QrTXIndInnSrcNivel1: TLargeintField;
    QrTXIndInnSrcNivel2: TLargeintField;
    QrTXIndInnSrcGGX: TLargeintField;
    QrTXIndInnSdoVrtQtd: TFloatField;
    QrTXIndInnObserv: TWideStringField;
    QrTXIndInnFornecMO: TLargeintField;
    QrTXIndInnCustoMOUni: TFloatField;
    QrTXIndInnCustoMOTot: TFloatField;
    QrTXIndInnValorMP: TFloatField;
    QrTXIndInnDstMovID: TLargeintField;
    QrTXIndInnDstNivel1: TLargeintField;
    QrTXIndInnDstNivel2: TLargeintField;
    QrTXIndInnDstGGX: TLargeintField;
    QrTXIndInnQtdGer: TFloatField;
    QrTXIndInnQtdAnt: TFloatField;
    QrTXIndInnNO_PALLET: TWideStringField;
    QrTXIndInnNO_PRD_TAM_COR: TWideStringField;
    QrTXIndInnNO_TTW: TWideStringField;
    QrTXIndInnID_TTW: TLargeintField;
    QrTXIndInnNO_FORNECE: TWideStringField;
    QrTXIndInnReqMovEstq: TLargeintField;
    QrTXIndInnCUSTO_M2: TFloatField;
    QrTXIndInnNO_LOC_CEN: TWideStringField;
    QrTXIndInnMarca: TWideStringField;
    QrTXIndInnPedItsLib: TLargeintField;
    QrTXIndInnStqCenLoc: TLargeintField;
    N5: TMenuItem;
    AlteraPallet1: TMenuItem;
    IrparajaneladoPallet1: TMenuItem;
    QrTXIndOriTXMulFrnCab: TLargeintField;
    AlteraLocaldoestoque1: TMenuItem;
    N6: TMenuItem;
    QrTXIndDstStqCenLoc: TLargeintField;
    QrTXIndDstNO_LOC_CEN: TWideStringField;
    Alteraartigodedestino1: TMenuItem;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    QrTXIndInnClientMO: TLargeintField;
    Somenteinfo1: TMenuItem;
    Apreencher1: TMenuItem;
    Incluiraspa1: TMenuItem;
    N7: TMenuItem;
    Corrigeartigodedestino1: TMenuItem;
    QrOperacoes: TmySQLQuery;
    DsOperacoes: TDataSource;
    QrOperacoesCodigo: TIntegerField;
    QrOperacoesNome: TWideStringField;
    Label60: TLabel;
    DBEdit42: TDBEdit;
    Oitemselecionado1: TMenuItem;
    odosapartirdoselecionado1: TMenuItem;
    EdSerieRem: TdmkEdit;
    dmkDBEdit3: TdmkDBEdit;
    GBConfig: TGroupBox;
    EdTXCOPCab: TdmkEditCB;
    Label61: TLabel;
    CBTXCOPCab: TdmkDBLookupComboBox;
    SbTXCOPCabCad: TSpeedButton;
    SbTXCOPCabCpy: TSpeedButton;
    QrTXCOPCab: TmySQLQuery;
    DsTXCOPCab: TDataSource;
    QrTXCOPCabCodigo: TIntegerField;
    QrTXCOPCabNome: TWideStringField;
    Desfazencerramento1: TMenuItem;
    N8: TMenuItem;
    TransfereTodaOPparaOutra1: TMenuItem;
    Comorigemedestino1: TMenuItem;
    QrTXIndOriDtCorrApo: TDateTimeField;
    QrTXIndDstDtCorrApo: TDateTimeField;
    QrForcadosDtCorrApo: TDateTimeField;
    QrTXIndInnNO_FORNEC_MO: TWideStringField;
    este1: TMenuItem;
    QrTXIndOriIxxMovIX: TLargeintField;
    QrTXIndOriIxxFolha: TLargeintField;
    QrTXIndOriIxxLinha: TLargeintField;
    InformaPginaelinhadeIEC1: TMenuItem;
    ReceberDadosDaOPdeOrigem1: TMenuItem;
    ransfenciadecourosorigementreOPs1: TMenuItem;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    TsEnvioMO: TTabSheet;
    QrTXMOEnvEnv: TmySQLQuery;
    QrTXMOEnvEnvCodigo: TIntegerField;
    QrTXMOEnvEnvNFEMP_FatID: TIntegerField;
    QrTXMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrTXMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrTXMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrTXMOEnvEnvNFEMP_nItem: TIntegerField;
    QrTXMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrTXMOEnvEnvNFEMP_nNF: TIntegerField;
    QrTXMOEnvEnvNFEMP_Qtde: TFloatField;
    QrTXMOEnvEnvNFEMP_ValorT: TFloatField;
    QrTXMOEnvEnvCFTMP_FatID: TIntegerField;
    QrTXMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrTXMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrTXMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrTXMOEnvEnvCFTMP_nItem: TIntegerField;
    QrTXMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrTXMOEnvEnvCFTMP_nCT: TIntegerField;
    QrTXMOEnvEnvCFTMP_Qtde: TFloatField;
    QrTXMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrTXMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrTXMOEnvEnvCFTMP_ValorT: TFloatField;
    QrTXMOEnvEnvTXTMI_MovimCod: TIntegerField;
    QrTXMOEnvEnvLk: TIntegerField;
    QrTXMOEnvEnvDataCad: TDateField;
    QrTXMOEnvEnvDataAlt: TDateField;
    QrTXMOEnvEnvUserCad: TIntegerField;
    QrTXMOEnvEnvUserAlt: TIntegerField;
    QrTXMOEnvEnvAlterWeb: TSmallintField;
    QrTXMOEnvEnvAWServerID: TIntegerField;
    QrTXMOEnvEnvAWStatSinc: TSmallintField;
    QrTXMOEnvEnvAtivo: TSmallintField;
    DsTXMOEnvEnv: TDataSource;
    QrTXMOEnvETMI: TmySQLQuery;
    QrTXMOEnvETMICodigo: TIntegerField;
    QrTXMOEnvETMITXMOEnvEnv: TIntegerField;
    QrTXMOEnvETMITXMovIts: TIntegerField;
    QrTXMOEnvETMILk: TIntegerField;
    QrTXMOEnvETMIDataCad: TDateField;
    QrTXMOEnvETMIDataAlt: TDateField;
    QrTXMOEnvETMIUserCad: TIntegerField;
    QrTXMOEnvETMIUserAlt: TIntegerField;
    QrTXMOEnvETMIAlterWeb: TSmallintField;
    QrTXMOEnvETMIAWServerID: TIntegerField;
    QrTXMOEnvETMIAWStatSinc: TSmallintField;
    QrTXMOEnvETMIAtivo: TSmallintField;
    QrTXMOEnvETMIValorFrete: TFloatField;
    QrTXMOEnvETMIQtde: TFloatField;
    DsTXMOEnvETMI: TDataSource;
    TsRetornoMO: TTabSheet;
    AtrelamentoNFsdeMO2: TMenuItem;
    QrTXMOEnvRet: TmySQLQuery;
    QrTXMOEnvRetCodigo: TIntegerField;
    QrTXMOEnvRetNFCMO_FatID: TIntegerField;
    QrTXMOEnvRetNFCMO_FatNum: TIntegerField;
    QrTXMOEnvRetNFCMO_Empresa: TIntegerField;
    QrTXMOEnvRetNFCMO_nItem: TIntegerField;
    QrTXMOEnvRetNFCMO_SerNF: TIntegerField;
    QrTXMOEnvRetNFCMO_nNF: TIntegerField;
    QrTXMOEnvRetNFCMO_Qtde: TFloatField;
    QrTXMOEnvRetNFCMO_ValorT: TFloatField;
    QrTXMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrTXMOEnvRetNFRMP_FatID: TIntegerField;
    QrTXMOEnvRetNFRMP_FatNum: TIntegerField;
    QrTXMOEnvRetNFRMP_Empresa: TIntegerField;
    QrTXMOEnvRetNFRMP_nItem: TIntegerField;
    QrTXMOEnvRetNFRMP_SerNF: TIntegerField;
    QrTXMOEnvRetNFRMP_nNF: TIntegerField;
    QrTXMOEnvRetNFRMP_Qtde: TFloatField;
    QrTXMOEnvRetNFRMP_ValorT: TFloatField;
    QrTXMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrTXMOEnvRetCFTPA_FatID: TIntegerField;
    QrTXMOEnvRetCFTPA_FatNum: TIntegerField;
    QrTXMOEnvRetCFTPA_Empresa: TIntegerField;
    QrTXMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrTXMOEnvRetCFTPA_nItem: TIntegerField;
    QrTXMOEnvRetCFTPA_SerCT: TIntegerField;
    QrTXMOEnvRetCFTPA_nCT: TIntegerField;
    QrTXMOEnvRetCFTPA_Qtde: TFloatField;
    QrTXMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrTXMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrTXMOEnvRetCFTPA_ValorT: TFloatField;
    DsTXMOEnvRet: TDataSource;
    QrTXMOEnvRTmi: TmySQLQuery;
    QrTXMOEnvRTmiCodigo: TIntegerField;
    QrTXMOEnvRTmiTXMOEnvRet: TIntegerField;
    QrTXMOEnvRTmiTXMOEnvEnv: TIntegerField;
    QrTXMOEnvRTmiQtde: TFloatField;
    QrTXMOEnvRTmiValorFrete: TFloatField;
    QrTXMOEnvRTmiLk: TIntegerField;
    QrTXMOEnvRTmiDataCad: TDateField;
    QrTXMOEnvRTmiDataAlt: TDateField;
    QrTXMOEnvRTmiUserCad: TIntegerField;
    QrTXMOEnvRTmiUserAlt: TIntegerField;
    QrTXMOEnvRTmiAlterWeb: TSmallintField;
    QrTXMOEnvRTmiAWServerID: TIntegerField;
    QrTXMOEnvRTmiAWStatSinc: TSmallintField;
    QrTXMOEnvRTmiAtivo: TSmallintField;
    QrTXMOEnvRTmiNFEMP_SerNF: TIntegerField;
    QrTXMOEnvRTmiNFEMP_nNF: TIntegerField;
    QrTXMOEnvRTmiValorT: TFloatField;
    DsTXMOEnvRTmi: TDataSource;
    QrTXMOEnvGTmi: TmySQLQuery;
    QrTXMOEnvGTmiCodigo: TIntegerField;
    QrTXMOEnvGTmiTXMOEnvRet: TIntegerField;
    QrTXMOEnvGTmiTXMovIts: TIntegerField;
    QrTXMOEnvGTmiQtde: TFloatField;
    QrTXMOEnvGTmiValorFrete: TFloatField;
    QrTXMOEnvGTmiLk: TIntegerField;
    QrTXMOEnvGTmiDataCad: TDateField;
    QrTXMOEnvGTmiDataAlt: TDateField;
    QrTXMOEnvGTmiUserCad: TIntegerField;
    QrTXMOEnvGTmiUserAlt: TIntegerField;
    QrTXMOEnvGTmiAlterWeb: TSmallintField;
    QrTXMOEnvGTmiAWServerID: TIntegerField;
    QrTXMOEnvGTmiAWStatSinc: TSmallintField;
    QrTXMOEnvGTmiAtivo: TSmallintField;
    QrTXMOEnvGTmiTXMovimCod: TIntegerField;
    DsTXMOEnvGTmi: TDataSource;
    PnRetornoMO: TPanel;
    DBGTXMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter2: TSplitter;
    GroupBox8: TGroupBox;
    DBGTXMOEnvRTmi: TdmkDBGridZTO;
    GroupBox9: TGroupBox;
    DBGTXMOEnvGTmi: TdmkDBGridZTO;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    PnEnvioMO: TPanel;
    DBGTXMOEnvEnv: TdmkDBGridZTO;
    DBGTXMOEnvETmi: TdmkDBGridZTO;
    QrTXIndInnCusFrtMOEnv: TFloatField;
    QrTXIndInnCusFrtMORet: TFloatField;
    QrTXIndDstCusFrtMORet: TFloatField;
    QrTXIndInnCustoPQ: TFloatField;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrFluxoCab: TmySQLQuery;
    QrFluxoCabCodigo: TSmallintField;
    QrFluxoCabNome: TWideStringField;
    DsFluxoCab: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    Label54: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    DBEdit14: TDBEdit;
    QrTXIndCabNO_TXCOPCab: TWideStringField;
    QrTXIndCabQtdeINI: TFloatField;
    QrTXIndCabNO_FLUXOCAB: TWideStringField;
    QrTXIndCabCodigo: TIntegerField;
    QrTXIndCabMovimCod: TIntegerField;
    QrTXIndCabCacCod: TIntegerField;
    QrTXIndCabFluxoCab: TIntegerField;
    QrTXIndCabNome: TWideStringField;
    QrTXIndCabEmpresa: TIntegerField;
    QrTXIndCabCliente: TIntegerField;
    QrTXIndCabClientMO: TIntegerField;
    QrTXIndCabFornecMO: TIntegerField;
    QrTXIndCabStqCenLoc: TIntegerField;
    QrTXIndCabPedItsLib: TIntegerField;
    QrTXIndCabDtHrAberto: TDateTimeField;
    QrTXIndCabDtHrLibOpe: TDateTimeField;
    QrTXIndCabDtHrCfgOpe: TDateTimeField;
    QrTXIndCabDtHrFimOpe: TDateTimeField;
    QrTXIndCabQtdeMan: TFloatField;
    QrTXIndCabValorTMan: TFloatField;
    QrTXIndCabQtdeSrc: TFloatField;
    QrTXIndCabValorTSrc: TFloatField;
    QrTXIndCabQtdeDst: TFloatField;
    QrTXIndCabValorTDst: TFloatField;
    QrTXIndCabQtdeBxa: TFloatField;
    QrTXIndCabValorTBxa: TFloatField;
    QrTXIndCabQtdeSdo: TFloatField;
    QrTXIndCabValorTSdo: TFloatField;
    QrTXIndCabCustoManMOUni: TFloatField;
    QrTXIndCabCustoManMOTot: TFloatField;
    QrTXIndCabValorManMP: TFloatField;
    QrTXIndCabValorManT: TFloatField;
    QrTXIndCabSerieRem: TIntegerField;
    QrTXIndCabNFeRem: TIntegerField;
    QrTXIndCabLPFMO: TWideStringField;
    QrTXIndCabTemIMEIMrt: TSmallintField;
    QrTXIndCabNFeStatus: TIntegerField;
    QrTXIndCabTXCOPCab: TIntegerField;
    QrTXIndCabTXVmcWrn: TSmallintField;
    QrTXIndCabTXVmcObs: TWideStringField;
    QrTXIndCabTXVmcSeq: TWideStringField;
    QrTXIndCabTXVmcSta: TSmallintField;
    QrTXIndCabLk: TIntegerField;
    QrTXIndCabDataCad: TDateField;
    QrTXIndCabDataAlt: TDateField;
    QrTXIndCabUserCad: TIntegerField;
    QrTXIndCabUserAlt: TIntegerField;
    QrTXIndCabAlterWeb: TSmallintField;
    QrTXIndCabAWServerID: TIntegerField;
    QrTXIndCabAWStatSinc: TSmallintField;
    QrTXIndCabAtivo: TSmallintField;
    QrTXIndCabNO_EMPRESA: TWideStringField;
    QrTXIndCabNO_DtHrLibOpe: TWideStringField;
    QrTXIndCabNO_DtHrFimOpe: TWideStringField;
    QrTXIndCabNO_DtHrCfgOpe: TWideStringField;
    QrTXIndCabQtdeInn: TFloatField;
    QrTXIndCabValorTInn: TFloatField;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit7: TDBEdit;
    Label14: TLabel;
    DBEdit8: TDBEdit;
    Label15: TLabel;
    DBEdit9: TDBEdit;
    Label16: TLabel;
    DBEdit10: TDBEdit;
    QrTXIndCabCustoMOTot: TFloatField;
    Label17: TLabel;
    DBEdit11: TDBEdit;
    BtInn: TBitBtn;
    PMInn: TPopupMenu;
    ItsIncluiInn: TMenuItem;
    ItsAlteraInn: TMenuItem;
    ItsExcluiInn: TMenuItem;
    QrTXIndInnDtCorrApo: TDateTimeField;
    Adicionaartigodeorigem1: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    PCInnDst: TPageControl;
    TsInn: TTabSheet;
    TsDst: TTabSheet;
    PnDst: TPanel;
    DGDadosDst: TDBGrid;
    DBGrid1: TDBGrid;
    DBGrid4: TDBGrid;
    QrTXIndInnPedItsFin: TLargeintField;
    TabSheet5: TTabSheet;
    DBGrid3: TDBGrid;
    AlteraLocaldoestoque2: TMenuItem;
    N11: TMenuItem;
    AtualizaestoqueEmindustrializao1: TMenuItem;
    QrTXIndInnSerieTal: TLargeintField;
    QrTXIndInnTalao: TLargeintField;
    QrTXIndDstSerieTal: TLargeintField;
    QrTXIndDstTalao: TLargeintField;
    DqSum: TmySQLDirectQuery;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXIndCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXIndCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXIndCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrTXIndCabBeforeClose(DataSet: TDataSet);
    procedure QrTXIndCabCalcFields(DataSet: TDataSet);
    procedure CabReeditar1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure QrTXIndDstBeforeClose(DataSet: TDataSet);
    procedure QrTXIndDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure porPallet1Click(Sender: TObject);
    procedure porIMEI1Click(Sender: TObject);
    procedure InformaNmerodaRME11Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure Corrigirfornecedor1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure Ordensdeoperaoemaberto1Click(Sender: TObject);
    procedure porPalletparcial1Click(Sender: TObject);
    procedure porIMEIParcial1Click(Sender: TObject);
    procedure AlteraPallet1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure AlteraLocaldoestoque1Click(Sender: TObject);
    procedure AlteraLocaldoestoque2Click(Sender: TObject);
    procedure Alteraartigodedestino1Click(Sender: TObject);
    procedure Somenteinfo1Click(Sender: TObject);
    procedure Apreencher1Click(Sender: TObject);
    procedure Incluiraspa1Click(Sender: TObject);
    procedure Oitemselecionado1Click(Sender: TObject);
    procedure odosapartirdoselecionado1Click(Sender: TObject);
    procedure SbTXCOPCabCpyClick(Sender: TObject);
    procedure SbTXCOPCabCadClick(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure Comorigemedestino1Click(Sender: TObject);
    procedure este1Click(Sender: TObject);
    procedure InformaPginaelinhadeIEC1Click(Sender: TObject);
    procedure TransfereTodaOPparaOutra1Click(Sender: TObject);
    procedure ReceberDadosDaOPdeOrigem1Click(Sender: TObject);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure QrTXMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrTXMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
    procedure QrTXMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure QrTXMOEnvRetBeforeClose(DataSet: TDataSet);
    procedure BtInnClick(Sender: TObject);
    procedure PMInnPopup(Sender: TObject);
    procedure ItsIncluiInnClick(Sender: TObject);
    procedure Adicionaartigodeorigem1Click(Sender: TObject);
    procedure ItsAlteraInnClick(Sender: TObject);
    procedure ItsExcluiInnClick(Sender: TObject);
    procedure AtualizaestoqueEmindustrializao1Click(Sender: TObject);
  private
    FItsExcluiOriIMEI_Enabled, FItsExcluiOriPallet_Enabled,
    FItsIncluiOri_Enabled, FItsIncluiInn_Enabled, FItsIncluiDst_Enabled,
    FAtualizando: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraTXIndInn(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraTXIndOri(SQLType: TSQLType; Quanto: TPartOuTodo);

    procedure MostraTXOpeOriIMEI2(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraTXOpeOriPall2(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraTXIndDst(SQLType: TSQLType; DesabilitaBaixa: Boolean);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenTXIndOris(Controle, Pallet: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure ImprimeIMEI(Formato: TXXImpImeiKind);
    procedure CorrigeArtigos(Todos: Boolean);
    function  CalculoCustoOrigemUni(): Double;
  public
    { Public declarations }
    FSeq, FCabIni, FOOOrigemCodigo, FOOOrigemMovimCod, FOOOrigemAtuControle: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure AtualizaNFeItens();
    procedure DistribuicaoCusto();

  end;

var
  FmTXIndCab: TFmTXIndCab;
const
  FFormatFloat = '00000';

implementation

uses
  Module, MyDBCheck, DmkDAC_PF, ModuleGeral, AppListas, UnTX_Jan,
{$IfDef sAllTX} UnTX_PF, {$EndIf}
  {POIU TXOpeDst, TXOpeOriPall2, TXOpeOriIMEI2,}
  ModTX_CRC, GraGruX, TXIndInn, TXIndOri, TXIndDst;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

const
  FGerArX2Env = True;
  FGerArX2Ret = False;

procedure TFmTXIndCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXIndCab.MostraTXIndDst(SQLType: TSQLType;
  DesabilitaBaixa: Boolean);
begin
  if (not DesabilitaBaixa) then
    if (not FItsIncluiDst_Enabled) then
      if not DBCheck.LiberaPelaSenhaAdmin then
        Exit;
  if DBCheck.CriaFm(TFmTXIndDst, FmTXIndDst, afmoNegarComAviso) then
  begin
    FmTXIndDst.ImgTipo.SQLType := SQLType;
    FmTXIndDst.FQrCab := QrTXIndCab;
    FmTXIndDst.FDsCab := DsTXIndCab;
    FmTXIndDst.FQrIts := QrTXIndDst;
    FmTXIndDst.FEmpresa  := QrTXIndCabEmpresa.Value;
    FmTXIndDst.FClientMO := QrTXIndInnClientMO.Value;
    FmTXIndDst.FFornecMO := QrTXIndInnFornecMO.Value;
    FmTXIndDst.RGBaixaEOuGera.ItemIndex := 0;
    //FmTXIndDst.FValorMPUniOri := 0; // Define depois???
    //FmTXIndDst.FValorMPUniInn := 0; Define na sele��o
    if SQLType = stIns then
    begin
      TX_PF.ReopenPedItsXXX(FmTXIndDst.QrTXPedIts, QrTXIndInnControle.Value,
      QrTXIndInnControle.Value);
      FmTXIndDst.RGBaixaEOuGera.Enabled     := True;
      // Sugerir o Lib do Novo gerado!
      FmTXIndDst.EdPedItsFin.ValueVariant   := QrTXIndInnPedItsLib.Value;
      FmTXIndDst.CBPedItsFin.KeyValue       := QrTXIndInnPedItsLib.Value;
{
      // GGX Dest do GGX Em processo!
      FmTXIndDst.EdGragruX.ValueVariant     := QrTXIndCabGGXDst.Value;
      FmTXIndDst.CBGragruX.KeyValue         := QrTXIndCabGGXDst.Value;
}
      //
{
      FmTXIndDst.TPData.Date                := QrTXIndCabDtHrAberto.Value;
      FmTXIndDst.EdHora.ValueVariant        := QrTXIndCabDtHrAberto.Value;
}
    end else
    begin
      TX_PF.ReopenPedItsXXX(FmTXIndDst.QrTXPedIts, QrTXIndInnControle.Value,
       QrTXIndDstControle.Value);
      FmTXIndDst.RGBaixaEOuGera.Enabled     := False;
      FmTXIndDst.FQtdEditing                := QrTXIndDstQtde.Value;
      FmTXIndDst.EdCtrl1.ValueVariant       := QrTXIndDstControle.Value;
      FmTXIndDst.EdMovimTwn.ValueVariant    := QrTXIndDstMovimTwn.Value;
      FmTXIndDst.EdGragruX.ValueVariant     := QrTXIndDstGraGruX.Value;
      FmTXIndDst.CBGragruX.KeyValue         := QrTXIndDstGraGruX.Value;
      FmTXIndDst.EdQtde.ValueVariant        := QrTXIndDstQtde.Value;
      // Cuidado!!!
      FmTXIndDst.EdValorMP.ValueVariant     := QrTXIndDstValorMP.Value;
      FmTXIndDst.EdValorT.ValueVariant      := QrTXIndDstValorT.Value;
      FmTXIndDst.EdCusFrtMORet.ValueVariant := QrTXIndDstCusFrtMORet.Value;
      //
      FmTXIndDst.EdObserv.ValueVariant      := QrTXIndDstObserv.Value;
      FmTXIndDst.EdMarca.ValueVariant       := QrTXIndDstMarca.Value;
      FmTXIndDst.EdPedItsFin.ValueVariant   := QrTXIndDstPedItsFin.Value;
      FmTXIndDst.CBPedItsFin.KeyValue       := QrTXIndDstPedItsFin.Value;
      FmTXIndDst.EdPallet.ValueVariant      := QrTXIndDstPallet.Value;
      FmTXIndDst.CBPallet.KeyValue          := QrTXIndDstPallet.Value;
      FmTXIndDst.EdStqCenLoc.ValueVariant   := QrTXIndDstStqCenLoc.Value;
      FmTXIndDst.CBStqCenLoc.KeyValue       := QrTXIndDstStqCenLoc.Value;
      FmTXIndDst.EdReqMovEstq.ValueVariant  := QrTXIndDstReqMovEstq.Value;
      FmTXIndInn.EdSerieTal.ValueVariant   := QrTXIndInnSerieTal.Value;
      FmTXIndInn.CBSerieTal.KeyValue       := QrTXIndInnSerieTal.Value;
      FmTXIndInn.EdTalao.ValueVariant      := QrTXIndInnTalao.Value;
      //
      FmTXIndDst.EdCustoMOUni.ValueVariant  := QrTXIndDstCustoMOUni.Value;
      FmTXIndDst.EdCustoMOTot.ValueVariant  := QrTXIndDstCustoMOTot.Value;
      FmTXIndDst.EdCusFrtMORet.ValueVariant := QrTXIndDstCusFrtMORet.Value;
      // Cuidado!!!
      FmTXIndDst.EdValorT.ValueVariant      := QrTXIndDstValorT.Value;
      //
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXIndDstDataHora.Value,
        QrTXIndDstDtCorrApo.Value,  FmTXIndDst.TPData, FmTXIndDst.EdHora);
      //
      if QrTXIndDstSdoVrtQtd.Value < QrTXIndDstQtde.Value then
      begin
        FmTXIndDst.EdGraGruX.Enabled        := False;
        FmTXIndDst.CBGraGruX.Enabled        := False;
      end;
      if QrTXIndBxa.RecordCount > 0 then
      begin
        FmTXIndDst.RGBaixaEOuGera.ItemIndex := 1;

        FmTXIndDst.EdIMEISrc.ValueVariant   := QrTXIndBxaSrcNivel2.Value;
        FmTXIndDst.CBIMEISrc.KeyValue       := QrTXIndBxaSrcNivel2.Value;
        FmTXIndDst.EdCtrl2.ValueVariant     := QrTXIndBxaControle.Value;
        FmTXIndDst.EdBxaQtde.ValueVariant   := -QrTXIndBxaQtde.Value;
        //FmTXIndDst.EdEstqQtdeAtu Calculado!!!
        //FmTXIndDst.EdEstqQtdeFut Calculado!!!
        FmTXIndDst.EdBxaValorT.ValueVariant := -QrTXIndBxaValorT.Value;
        FmTXIndDst.EdBxaObserv.ValueVariant := QrTXIndBxaObserv.Value;
      end else
        FmTXIndDst.RGBaixaEOuGera.ItemIndex := 2;
    end;
    //
    if DesabilitaBaixa then
    begin
      FmTXIndDst.RGBaixaEOuGera.ItemIndex   := 2;
      FmTXIndDst.RGBaixaEOuGera.Enabled     := False;
    end;
    // Cuidado!!
    FmTXIndDst.RedefineCustos();
    //
    FmTXIndDst.ShowModal;
    FmTXIndDst.Destroy;
    TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmTXIndCab.MostraTXIndInn(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  //if (not DesabilitaBaixa) then
    if (not FItsIncluiInn_Enabled) then
      if not DBCheck.LiberaPelaSenhaAdmin then
        Exit;
  if DBCheck.CriaFm(TFmTXIndInn, FmTXIndInn, afmoNegarComAviso) then
  begin
    FmTXIndInn.ImgTipo.SQLType := SQLType;
    FmTXIndInn.FQrCab    := QrTXIndCab;
    FmTXIndInn.FDsCab    := DsTXIndCab;
    FmTXIndInn.FQrIts    := QrTXIndInn;
    FmTXIndInn.FEmpresa  := QrTXIndCabEmpresa.Value;
    FmTXIndInn.FClientMO := QrTXIndCabClientMO.Value;
    FmTXIndInn.FFornecMO := QrTXIndCabFornecMO.Value;
    FmTXIndInn.FValM2    := CalculoCustoOrigemUni();
    FmTXIndInn.EdStqCenLoc.ValueVariant  := QrTXIndCabStqCenLoc.Value;
    FmTXIndInn.CBStqCenLoc.KeyValue      := QrTXIndCabStqCenLoc.Value;
    if SQLType = stIns then
    begin
(*
      TX_PF.ReopenPedItsXXX(FmTXIndInn.QrTXPedIts, QrTXInd???Controle.Value,
      QrTXInd???Controle.Value);
      // Sugerir o Lib do Novo gerado!
      FmTXIndInn.EdPedItsFin.ValueVariant  := QrTXInd???PedItsLib.Value;
      FmTXIndInn.CBPedItsFin.KeyValue      := QrTXInd???PedItsLib.Value;
*)
      //
      FmTXIndInn.TPData.Date               := QrTXIndCabDtHrAberto.Value;
      FmTXIndInn.EdHora.ValueVariant       := QrTXIndCabDtHrAberto.Value;
    end else
    begin
      TX_PF.ReopenPedItsXXX(FmTXIndInn.QrTXPedIts, QrTXIndInnControle.Value,
       QrTXIndInnControle.Value);
      FmTXIndInn.EdControle.ValueVariant   := QrTXIndInnControle.Value;
      FmTXIndInn.EdMovimTwn.ValueVariant   := QrTXIndInnMovimTwn.Value;
      FmTXIndInn.EdGragruX.ValueVariant    := QrTXIndInnGraGruX.Value;
      FmTXIndInn.CBGragruX.KeyValue        := QrTXIndInnGraGruX.Value;
      FmTXIndInn.EdQtde.ValueVariant       := QrTXIndInnQtde.Value;
      // Cuidado!!! Apenas informativo. Calcula depois na distribui��o de custo!!
      FmTXIndInn.EdValorMP.ValueVariant     := QrTXIndInnValorMP.Value;
      FmTXIndInn.EdCustoMOUni.ValueVariant  := QrTXIndInnCustoMOUni.Value;
      FmTXIndInn.EdCustoMOTot.ValueVariant  := QrTXIndInnCustoMOTot.Value;
      FmTXIndInn.EdValorT.ValueVariant      := QrTXIndInnValorT.Value;
      //
      FmTXIndInn.EdObserv.ValueVariant     := QrTXIndInnObserv.Value;
      FmTXIndInn.EdMarca.ValueVariant      := QrTXIndInnMarca.Value;
      FmTXIndInn.EdSerieTal.ValueVariant   := QrTXIndInnSerieTal.Value;
      FmTXIndInn.CBSerieTal.KeyValue       := QrTXIndInnSerieTal.Value;
      FmTXIndInn.EdTalao.ValueVariant      := QrTXIndInnTalao.Value;
      (*
      FmTXIndInn.EdPedItsFin.ValueVariant  := QrTXIndInnPedItsFin.Value;
      FmTXIndInn.CBPedItsFin.KeyValue      := QrTXIndInnPedItsFin.Value;
      *)
      FmTXIndInn.EdPallet.ValueVariant     := QrTXIndInnPallet.Value;
      FmTXIndInn.CBPallet.KeyValue         := QrTXIndInnPallet.Value;
      FmTXIndInn.EdReqMovEstq.ValueVariant := QrTXIndInnReqMovEstq.Value;
      //
      FmTXIndInn.EdCustoMOUni.ValueVariant := QrTXIndInnCustoMOUni.Value;
      FmTXIndInn.EdCustoMOTot.ValueVariant := QrTXIndInnCustoMOTot.Value;
      //FmTXIndInn.EdCusFrtMORet.ValueVariant := QrTXIndInnCusFrtMORet.Value;
      // Cuidado!!!
      FmTXIndInn.EdValorT.ValueVariant    := QrTXIndInnValorT.Value;
      //
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXIndInnDataHora.Value,
        QrTXIndInnDtCorrApo.Value,  FmTXIndInn.TPData, FmTXIndInn.EdHora);
      //
      if QrTXIndInnSdoVrtQtd.Value < QrTXIndInnQtde.Value then
      begin
        FmTXIndInn.EdGraGruX.Enabled        := False;
        FmTXIndInn.CBGraGruX.Enabled        := False;
      end;
    end;
    //
    //
    FmTXIndInn.ShowModal;
    FmTXIndInn.Destroy;
    TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
    //CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmTXIndCab.MostraTXIndOri(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmTXIndOri, FmTXIndOri, afmoNegarComAviso) then
  begin
    FmTXIndOri.ImgTipo.SQLType := SQLType;
    FmTXIndOri.FQrCab          := QrTXIndCab;
    FmTXIndOri.FDsCab          := DsTXIndCab;
    FmTXIndOri.FEmpresa        := QrTXIndCabEmpresa.Value;
    FmTXIndOri.FClientMO       := QrTXIndCabClientMO.Value;
    FmTXIndOri.FFornecMO       := QrTXIndCabFornecMO.Value;
    FmTXIndOri.FStqCenLoc      := QrTXIndCabStqCenLoc.Value;
    //
    FmTXIndOri.EdCodigo.ValueVariant    := QrTXIndCabCodigo.Value;
    FmTXIndOri.EdMovimCod.ValueVariant  := QrTXIndCabMovimCod.Value;
    //
(*
    FmTXIndOri.EdSrcMovID.ValueVariant  := QrTXIndInnMovimID.Value;
    FmTXIndOri.EdSrcNivel1.ValueVariant := QrTXIndInnCodigo.Value;
    FmTXIndOri.EdSrcNivel2.ValueVariant := QrTXIndInnControle.Value;
    FmTXIndOri.EdSrcGGX.ValueVariant    := QrTXIndInnGraGruX.Value;
*)
    //
    FmTXIndOri.ReopenItensAptos();
    //
    if SQLType = stIns then
    begin
      FmTXIndOri.FDataHora                := QrTXIndCabDtHrAberto.Value;
    end else
    begin
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXIndOriDataHora.Value,
        QrTXIndOriDtCorrApo.Value,  FmTXIndOri.FDataHora);
    end;
    case Quanto of
      ptParcial:
      begin
        FmTXIndOri.FParcial := True;
        FmTXIndOri.DBG04Estq.Options := FmTXIndOri.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmTXIndOri.FParcial := False;
        FmTXIndOri.DBG04Estq.Options := FmTXIndOri.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmTXIndOri.FParcial := False;
        FmTXIndOri.DBG04Estq.Options := FmTXIndOri.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmTXIndOri.ShowModal;
    FmTXIndOri.Destroy;
    TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
  DistribuicaoCusto();
end;

procedure TFmTXIndCab.MostraTXOpeOriPall2(SQLType: TSQLType;
  Quanto: TPartOuTodo);
begin
{POIU
  if not FItsIncluiOri_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmTXOpeOriPall2, FmTXOpeOriPall2, afmoNegarComAviso) then
  begin
    FmTXOpeOriPall2.ImgTipo.SQLType := SQLType;
    FmTXOpeOriPall2.FQrCab          := QrTXIndCab;
    FmTXOpeOriPall2.FDsCab          := DsTXIndCab;
    FmTXOpeOriPall2.FEmpresa        := QrTXIndCabEmpresa.Value;
    FmTXOpeOriPall2.FOrigMovimNiv   := QrTXIndInnMovimNiv.Value;
    FmTXOpeOriPall2.FOrigMovimCod   := QrTXIndInnMovimCod.Value;
    FmTXOpeOriPall2.FOrigCodigo     := QrTXIndInnCodigo.Value;
    FmTXOpeOriPall2.FNewGraGruX     := QrTXIndInnGraGruX.Value;
    FmTXOpeOriPall2.FClientMO       := QrTXIndInnClientMO.Value;
    FmTXOpeOriPall2.FFornecMO       := QrTXIndInnFornecMO.Value;
    FmTXOpeOriPall2.FStqCenLoc      := QrTXIndInnStqCenLoc.Value;
    //
    FmTXOpeOriPall2.EdCodigo.ValueVariant    := QrTXIndCabCodigo.Value;
    FmTXOpeOriPall2.EdMovimCod.ValueVariant  := QrTXIndCabMovimCod.Value;
    //
    FmTXOpeOriPall2.EdSrcMovID.ValueVariant  := QrTXIndInnMovimID.Value;
    FmTXOpeOriPall2.EdSrcNivel1.ValueVariant := QrTXIndInnCodigo.Value;
    FmTXOpeOriPall2.EdSrcNivel2.ValueVariant := QrTXIndInnControle.Value;
    FmTXOpeOriPall2.EdSrcGGX.ValueVariant    := QrTXIndInnGraGruX.Value;
    //
    FmTXOpeOriPall2.ReopenItensAptos();
    //
    if SQLType = stIns then
    begin
      FmTXOpeOriPall2.FDataHora                := QrTXIndCabDtHrAberto.Value;
    end else
    begin
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXIndOriDataHora.Value,
        QrTXIndOriDtCorrApo.Value,  FmTXOpeOriPall2.FDataHora);
    end;
    case Quanto of
      ptParcial:
      begin
        FmTXOpeOriPall2.FParcial := True;
        FmTXOpeOriPall2.DBG04Estq.Options := FmTXOpeOriPall2.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmTXOpeOriPall2.FParcial := False;
        FmTXOpeOriPall2.DBG04Estq.Options := FmTXOpeOriPall2.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmTXOpeOriPall2.FParcial := False;
        FmTXOpeOriPall2.DBG04Estq.Options := FmTXOpeOriPall2.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmTXOpeOriPall2.ShowModal;
    FmTXOpeOriPall2.Destroy;
    TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
}
end;

procedure TFmTXIndCab.odosapartirdoselecionado1Click(Sender: TObject);
begin
  CorrigeArtigos(True);
end;

procedure TFmTXIndCab.MostraTXOpeOriIMEI2(SQLType: TSQLType;
  Quanto: TPartOuTodo);
begin
{POIU
  if DBCheck.CriaFm(TFmTXOpeOriIMEI2, FmTXOpeOriIMEI2, afmoNegarComAviso) then
  begin
    FmTXOpeOriIMEI2.ImgTipo.SQLType := SQLType;
    FmTXOpeOriIMEI2.FQrCab          := QrTXIndCab;
    FmTXOpeOriIMEI2.FDsCab          := DsTXIndCab;
    FmTXOpeOriIMEI2.FEmpresa        := QrTXIndCabEmpresa.Value;
    FmTXOpeOriIMEI2.FClientMO       := QrTXIndInnClientMO.Value;
    FmTXOpeOriIMEI2.FFornecMO       := QrTXIndInnFornecMO.Value;
    FmTXOpeOriIMEI2.FStqCenLoc      := QrTXIndInnStqCenLoc.Value;
    FmTXOpeOriIMEI2.FOrigMovimNiv   := QrTXIndInnMovimNiv.Value;
    FmTXOpeOriIMEI2.FOrigMovimCod   := QrTXIndInnMovimCod.Value;
    FmTXOpeOriIMEI2.FOrigCodigo     := QrTXIndInnCodigo.Value;
    FmTXOpeOriIMEI2.FNewGraGruX     := QrTXIndInnGraGruX.Value;
    //
    FmTXOpeOriIMEI2.EdCodigo.ValueVariant    := QrTXIndCabCodigo.Value;
    FmTXOpeOriIMEI2.EdMovimCod.ValueVariant  := QrTXIndCabMovimCod.Value;
    //
    FmTXOpeOriIMEI2.EdSrcMovID.ValueVariant  := QrTXIndInnMovimID.Value;
    FmTXOpeOriIMEI2.EdSrcNivel1.ValueVariant := QrTXIndInnCodigo.Value;
    FmTXOpeOriIMEI2.EdSrcNivel2.ValueVariant := QrTXIndInnControle.Value;
    FmTXOpeOriIMEI2.EdSrcGGX.ValueVariant    := QrTXIndInnGraGruX.Value;
    //
    FmTXOpeOriIMEI2.ReopenItensAptos();
    //
    if SQLType = stIns then
    begin
      FmTXOpeOriIMEI2.FDataHora                := QrTXIndCabDtHrAberto.Value;
    end else
    begin
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXIndOriDataHora.Value,
        QrTXIndOriDtCorrApo.Value,  FmTXOpeOriIMEI2.FDataHora);
    end;
    case Quanto of
      ptParcial:
      begin
        FmTXOpeOriIMEI2.FParcial := True;
        FmTXOpeOriIMEI2.DBG04Estq.Options := FmTXOpeOriIMEI2.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmTXOpeOriIMEI2.FParcial := False;
        FmTXOpeOriIMEI2.DBG04Estq.Options := FmTXOpeOriIMEI2.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmTXOpeOriIMEI2.FParcial := False;
        FmTXOpeOriIMEI2.DBG04Estq.Options := FmTXOpeOriIMEI2.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmTXOpeOriIMEI2.ShowModal;
    FmTXOpeOriIMEI2.Destroy;
    TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
}
end;

procedure TFmTXIndCab.Oitemselecionado1Click(Sender: TObject);
begin
  CorrigeArtigos(False);
end;

procedure TFmTXIndCab.Ordensdeoperaoemaberto1Click(Sender: TObject);
begin
{POIU
  TX_PF.ImprimeOXsAbertas(1, 2, 0, 1, [TEstqMovimID.emidEmIndstrlzc], True, False, '', 0);
}
end;

procedure TFmTXIndCab.Outrasimpresses1Click(Sender: TObject);
begin
{POIU
  TX_PF.MostraFormTXMovImp(False, nil, nil, -1);
}
end;

procedure TFmTXIndCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXIndCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXIndCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{POIU
  Codigo := TX_PF.LocalizaPeloIMEC(emidEmIndstrlzc);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
}
end;

procedure TFmTXIndCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{POIU
  Codigo := TX_PF.LocalizaPeloIMEI(emidEmIndstrlzc);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
}
end;

procedure TFmTXIndCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXIndCab);
  MyObjects.HabilitaMenuItemCabDelC1I2(*3*)(CabExclui1, QrTXIndCab,
  QrTXIndOri, (*QrTXIndOriPallet,*) QrTXIndDst);
  MyObjects.HabilitaMenuItemCabUpd(CabLibera1, QrTXIndCab);
  MyObjects.HabilitaMenuItemCabUpd(CabReeditar1, QrTXIndCab);
  if QrTXIndCabDtHrFimOpe.Value > 2 then
  begin
    CabLibera1.Enabled := False;
    CabReeditar1.Enabled := False;
  end else
  if QrTXIndCabDtHrLibOpe.Value < 2 then
  begin
    CabReeditar1.Enabled := False;
  end;
  //
  TransfereTodaOPparaOutra1.Enabled := FOOOrigemMovimCod = 0;
  ReceberDadosDaOPdeOrigem1.Enabled := FOOOrigemMovimCod <> 0;
end;

procedure TFmTXIndCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrTXIndCab);
  FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrTXIndDst);
  if (QrTXIndCabDtHrLibOpe.Value > 2) or (QrTXIndCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    ItsExcluiDst.Enabled := False;
  end;
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrTXIndDst);
  //
  TX_PF.HabilitaComposTXAtivo(QrTXIndDstID_TTW.Value, [ItsExcluiDst]);
  if FItsIncluiDst_Enabled then
  begin
    FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
    ItsIncluiDst.Enabled  := True;
  end;
  //
  AtrelamentoNFsdeMO2.Enabled := PCIndOri.ActivePage = TsRetornoMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento2, QrTXIndCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento2, QrTXMOEnvRet);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento2, QrTXMOEnvRet);
  //
end;

procedure TFmTXIndCab.PMInnPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiInn, QrTXIndCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraInn, QrTXIndInn);
  FItsIncluiInn_Enabled := ItsIncluiInn.Enabled;
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiInn, QrTXIndInn);
  if (QrTXIndCabDtHrLibOpe.Value > 2) or (QrTXIndCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiInn.Enabled := False;
    ItsExcluiInn.Enabled := False;
  end;
{POIU
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME3, QrTXIndInn);
}
  //
  TX_PF.HabilitaComposTXAtivo(QrTXIndInnID_TTW.Value, [ItsExcluiInn]);
  if FItsIncluiInn_Enabled then
  begin
    FItsIncluiInn_Enabled := ItsIncluiInn.Enabled;
    ItsIncluiInn.Enabled  := True;
  end;
  //
  AlteraLocaldoestoque2.Enabled := ItsAlteraInn.Enabled;
end;

procedure TFmTXIndCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrTXIndCab);
  FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrTXIndOri);
  MyObjects.HabilitaMenuItemItsUpd(InformaPginaelinhadeIEC1, QrTXIndOri);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrTXIndOri);
  //MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrTXIndOriPallet);
  if (QrTXIndCabDtHrLibOpe.Value > 2) or (QrTXIndCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  //
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCIndOri.ActivePageIndex = 1);
  InformaPginaelinhadeIEC1.Enabled := InformaNmerodaRME1.Enabled;
  //
  TX_PF.HabilitaComposTXAtivo(QrTXIndOriID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  //TX_PF.HabilitaComposTXAtivo(QrTXIndOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  //
  FItsExcluiOriIMEI_Enabled   := ItsExcluiOriIMEI.Enabled;
  ItsExcluiOriIMEI.Enabled    := True;
  FItsExcluiOriPallet_Enabled := ItsExcluiOriPallet.Enabled;
  ItsExcluiOriPallet.Enabled  := True;
(*
  case PCIndOri.ActivePageIndex of
    0: ItsExcluiOriIMEI.Enabled := False;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
*)
  if ItsExcluiOriIMEI.Enabled then
    ItsExcluiOriIMEI.Enabled := PCIndOri.ActivePageIndex = 0;
  //
  if FItsIncluiOri_Enabled then
  begin
    FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
    ItsIncluiOri.Enabled  := True;
  end;
  //
  AtrelamentoNFsdeMO1.Enabled := PCIndOri.ActivePage = TsEnvioMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrTXIndOri);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrTXMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrTXMOEnvEnv);
  //
end;

procedure TFmTXIndCab.porIMEI1Click(Sender: TObject);
begin
  MostraTXOpeOriIMEI2(stIns, ptTotal);
end;

procedure TFmTXIndCab.porIMEIParcial1Click(Sender: TObject);
begin
  MostraTXOpeOriIMEI2(stIns, ptParcial);
end;

procedure TFmTXIndCab.porPallet1Click(Sender: TObject);
begin
  MostraTXOpeOriPall2(stIns, ptTotal);
end;

procedure TFmTXIndCab.porPalletparcial1Click(Sender: TObject);
begin
  MostraTXOpeOriPall2(stIns, ptParcial);
end;

procedure TFmTXIndCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXIndCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXIndCab.DefParams;
begin
  VAR_GOTOTABELA := 'txindcab';
  VAR_GOTOMYSQLTABLE := QrTXIndCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  //VAR_SQLx.Add('SELECT vcc.Nome NO_TXCOPCab, ');
  VAR_SQLx.Add('SELECT "" NO_TXCOPCab,  ');
  VAR_SQLx.Add('IF(QtdeMan<>0, QtdeMan, -QtdeSrc) QtdeINI, ');
  VAR_SQLx.Add('flu.Nome NO_FLUXOCAB, voc.*, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM txindcab voc ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa ');
  VAR_SQLx.Add('LEFT JOIN fluxocab flu ON flu.Codigo=voc.FluxoCab ');
    //VAR_SQLx.Add('LEFT JOIN txcopcab  vcc ON vcc.Codigo=voc.TXCOPCab');
  VAR_SQLx.Add('WHERE voc.Codigo > 0 ');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmTXIndCab.Desfazencerramento1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrTXIndCabCodigo.Value;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE txindcab ',
  'SET DtHrFimOpe="1899-12-30" ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  LocCod(Codigo, Codigo);
end;

procedure TFmTXIndCab.DistribuicaoCusto();
begin
    DmModTX_CRC.DistribuicaoCusto(emidEmIndstrlzc, eminIndzcSrc, eminIndzcInn, eminIndzcDst);
{
    //Movido para ModTX_CRC
  function ObtemQtde(MovimNiv: TEstqMovimNiv): Double;
  begin
    UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqSum, DModG.MyCompressDB, [
    'SELECT SUM(Qtde) Qtde  ',
    'FROM txmovits ',
    'WHERE MovimID=' + Geral.FF0(Integer(emidEmIndstrlzc)), //38 ',
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)), //62 ou 63 ',
    '']);
    //
    Result := USQLDB.Flu(DqSum, 'Qtde');
  end;
  //
  procedure AtualizaCusto(ValMPUni: Double; MovimNiv: TEstqMovimNiv);
  var
    ValTXT: String;
  begin
    ValTXT := Geral.FFT_Dot(ValMPUni, 6, siNegativo);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE txmovits ',
    'SET CustoMOTot=Qtde*CustoMOUni, ',
    'ValorMP=Qtde*' + ValTXT + ', ',
    'ValorT=Qtde*(CustoMOUni + ' + ValTXT + ') ',
    'WHERE MovimID=' + Geral.FF0(Integer(emidEmIndstrlzc)), //38 ',
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)), //62 ou 63 ',
    '']);
  end;
var
  ValorMP, Qtde, ValMPUni: Double;
begin
  UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqSum, DModG.MyCompressDB, [
  'SELECT SUM(ValorT) ValorT  ',
  'FROM txmovits ',
  'WHERE MovimID=' + Geral.FF0(Integer(emidEmIndstrlzc)), //38 ',
  'AND MovimNiv=' + Geral.FF0(Integer(eminIndzcSrc)), //61 ',
  '']);
  ValorMP := -USQLDB.Flu(DqSum, 'ValorT');
  //
  Qtde := ObtemQtde(eminIndzcInn);
  //
  if Qtde > 0 then
  begin
    ValMPUni := ValorMP / Qtde;
    //
    AtualizaCusto(ValMPUni, eminIndzcInn);
  end else
  begin
    Qtde := ObtemQtde(eminIndzcDst);
    //
    if Qtde > 0 then
    begin
      ValMPUni := ValorMP / Qtde;
      //
      AtualizaCusto(ValMPUni, eminIndzcDst);
    end;
  end;
}
end;


procedure TFmTXIndCab.este1Click(Sender: TObject);
begin
  TX_EFD_ICMS_IPI.ReopenTXRibItss(QrTXIndInn, QrTXIndOri,
  (*QrTXIndOriPallet*)nil,
  QrTXIndDst, (*QrTXOpeInd*)nil, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  (*QrEmit*)nil, (*QrPQO*)nil, QrTXIndCabCodigo.Value,
  QrTXIndCabMovimCod.Value, QrTXIndCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmIndstrlzc, emidEmIndstrlzc, emidEmIndstrlzc,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminIndzcInn, eminIndzcSrc, eminIndzcDst);
end;

procedure TFmTXIndCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
  //PCIndOri.ActivePageIndex := 2;
{POIU
  PCIndOri.ActivePage := TsEnvioMO;
  //
  DmModTX_CRC.ExcluiAtrelamentoMOEnvEnv(QrTXMOEnvEnv, QrTXMOEnvETMI);
  //
  TX_PF.AtualizaTotaisTXIndCab(QrTXIndCabMovimCod.Value);
  //
  LocCod(QrTXIndCabCodigo.Value, QrTXIndCabCodigo.Value);
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXIndCab.ExcluiAtrelamento2Click(Sender: TObject);
begin
{POIU
  DmModTX_CRC.ExcluiAtrelamentoMOEnvRet(QrTXMOEnvRet, QrTXMOEnvGTMI, QrTXMOEnvRTMI);
  //
  TX_PF.AtualizaTotaisTXIndCab(QrTXIndCabMovimCod.Value);
  //
  LocCod(QrTXIndCabCodigo.Value,QrTXIndCabCodigo.Value);
}
end;

procedure TFmTXIndCab.CabReeditar1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrTXIndCabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txindcab', False, [
  'DtHrLibOpe'], ['Codigo'], [
  '0000-00-00'], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

function TFmTXIndCab.CalculoCustoOrigemUni(): Double;
begin
  Result := TX_PF.CalculoValorOrigemUni(QrTXIndInnQtde.Value,
    QrTXIndInnCustoPQ.Value, QrTXIndInnValorMP.Value,
    QrTXIndInnCusFrtMOEnv.Value);
end;

procedure TFmTXIndCab.Comorigemedestino1Click(Sender: TObject);
begin
{POIU
  TX_PF.ImprimeOrdem(emidEmIndstrlzc, QrTXIndCabEmpresa.Value,
  QrTXIndCabCodigo.Value, QrTXIndCabMovimCod.Value);
}
end;

procedure TFmTXIndCab.CorrigeArtigos(Todos: Boolean);
begin
  Geral.MB_Info('N�o implementado! Solicite � DERMATEK!');
{POIU
  if TX_PF.VerificaOpeSemOperacao() then
    Exit;
  TX_PF.CorrigeArtigoOpeEmDiante(QrTXIndDstControle.Value,
    QrTXIndDstGraGruX.Value);
  if Todos then
  begin
    QrTXIndDst.Next;
    while not QrTXIndDst.Eof do
    begin
      TX_PF.CorrigeArtigoOpeEmDiante(QrTXIndDstControle.Value,
        QrTXIndDstGraGruX.Value);
      //
      QrTXIndDst.Next;
    end;
  end;
  //
  TX_PF.ReopenTXIndPrcDst(QrTXIndDst, QrTXIndCabMovimCod.Value, 0,
    QrTXIndCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestIndzc);
}
end;

procedure TFmTXIndCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
{POIU
  IMEIAtu     := QrTXIndInnControle.Value;
  MovimCod    := QrTXIndCabMovimCod.Value;
  Codigo      := QrTXIndCabCodigo.Value;
  MovimNivSrc := eminSorcIndzc;
  MovimNivDst := eminDestIndzc;
  TX_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
}
end;

procedure TFmTXIndCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmTXIndCab.Corrigirfornecedor1Click(Sender: TObject);
begin
{POIU
  TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  LocCod(QrTXIndCabCodigo.Value, QrTXIndCabCodigo.Value);
}
end;

procedure TFmTXIndCab.ImprimeIMEI(Formato: TXXImpImeiKind);
var
  NFeRem: String;
begin
{POIU
  if QrTXIndCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrTXIndCabNFeRem.Value)
  else
    NFeRem := '';
  TX_PF.ImprimeIMEI([QrTXIndInnControle.Value], Formato, QrTXIndCabLPFMO.Value,
    NFeRem, QrTXIndCab);
}
end;

procedure TFmTXIndCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCIndOri.ActivePageIndex := 2;
  PCIndOri.ActivePage := TsEnvioMO;
  //
  TX_PF.MostraFormTXMOEnvEnv(stIns, QrTXIndOri, QrTXMOEnvEnv, QrTXMOEnvETMI,
  siNegativo, QrTXIndCabSerieRem.Value, QrTXIndCabNFeRem.Value);
  //
  LocCod(QrTXIndCabCodigo.Value,QrTXIndCabCodigo.Value);
  //
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXIndCab.IncluiAtrelamento2Click(Sender: TObject);
var
  MovimCod: Integer;
begin
{$IfDef sAllTX}
{POIU
  PCIndOri.ActivePage := TsRetornoMO;
  //
  MovimCod := QrTXIndCabMovimCod.Value;
  //
  TX_PF.MostraFormTXMOEnvRet(stIns, QrTXIndInn, QrTXIndDst, QrTXIndBxa,
  QrTXMOEnvRet, QrTXMOEnvGTMI, QrTXMOEnvRTMI, MovimCod, 0, 0, siPositivo,
  FGerArX2Ret);
  //
  LocCod(QrTXIndCabCodigo.Value,QrTXIndCabCodigo.Value);
  //
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXIndCab.Incluiraspa1Click(Sender: TObject);
const
  DesabilitaBaixa = True;
begin
  MostraTXIndDst(stIns, DesabilitaBaixa);
end;

procedure TFmTXIndCab.InformaNmerodaRME11Click(Sender: TObject);
begin
  TX_PF.InfoReqMovEstq(QrTXIndOriControle.Value,
    QrTXIndOriReqMovEstq.Value, QrTXIndOri);
end;

procedure TFmTXIndCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  TX_PF.InfoReqMovEstq(QrTXIndOriControle.Value,
    QrTXIndOriReqMovEstq.Value, QrTXIndOri);
end;

procedure TFmTXIndCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  TX_PF.InfoReqMovEstq(QrTXIndDstControle.Value,
    QrTXIndDstReqMovEstq.Value, QrTXIndDst);
end;

procedure TFmTXIndCab.InformaPginaelinhadeIEC1Click(Sender: TObject);
begin
{POIU
  if TX_PF.InformaIxx(DGDadosOri, QrTXIndOri, 'Controle') then
    PCIndOri.ActivePageIndex := 1;
}
end;

procedure TFmTXIndCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXPallet(QrTXIndDstPallet.Value);
end;

procedure TFmTXIndCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{POIU
  Codigo := QrTXIndCabCodigo.Value;
  ///Permitir excluir IMEC 337
  ///  Excluir tambem QrTXIndInn!!!
  ///  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  ///  Caption + sLineBreak + TMenuItem(Sender).Name);
  TX_PF.ExcluiCabEIMEI_OpeCab(QrTXIndCabMovimCod.Value,
    QrTXIndInnControle.Value, emidEmIndstrlzc, TEstqMotivDel.emtdWetCurti038);
  //
  LocCod(Codigo, Codigo);
}
end;

procedure TFmTXIndCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmTXIndCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXIndCab.TransfereTodaOPparaOutra1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
{POIU
  FOOOrigemCodigo := 0;
  FOOOrigemMovimCod := 0;
  FOOOrigemAtuControle := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM ' + CO_TAB_TAB_TMI,
    'WHERE MovimID=' + Geral.FF0(Integer(emidEmIndstrlzc)),
    'AND NOT (MovimNiv IN (' + Geral.FF0(Integer(eminIndzcSrc)) +
    ',' + Geral.FF0(Integer(eminIndzcInn)) + '))',
    'AND MovimCod=' + Geral.FF0(QrTXIndCabMovimCod.Value),
    '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Esta OO n�o pode ser movida pois j� posui descendentes!');
      Exit;
    end;
    //
    FOOOrigemMovimCod := QrTXIndCabMovimCod.Value;
    FOOOrigemCodigo   := QrTXIndCabCodigo.Value;
    FOOOrigemAtuControle := QrTXIndInnControle.Value;
    if FOOOrigemMovimCod <> 0 then
    begin
      Geral.MB_Info('O n�mero da OP foi salvo em mem�ria!' + sLineBreak +
      'V� para a OP de destino e clique no item de menu "Transfer�ncia de couros origem entre OPs" > "Receber dados da OP de origem" do bot�o "Em Opera��o"!');
    end;
  except
    Qry.Free;
  end;
}
end;

procedure TFmTXIndCab.ReceberDadosDaOPdeOrigem1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
{POIU
  Codigo   := QrTXIndCabCodigo.Value;
  MovimCod := QrTXIndCabMovimCod.Value;
  if (Codigo = 0) or (MovimCod = 0) then
  begin
    Geral.MB_Aviso('Esta OP n�o pode receber dados de outra OP! Selecione uma OP v�lida!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirme a tranfer�ncia dos couros de origem da OP ' +
  Geral.FF0(FOOOrigemCodigo) + ' (IME-C ' + Geral.FF0(FOOOrigemMovimCod) +
  ') para esta OP?') = ID_Yes then
  begin
    UnDmkDAC_PF.ExecutaMySQLQUery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_TMI +
    ' SET Codigo=' + Geral.FF0(Codigo),
    ', MovimCod=' + Geral.FF0(MovimCod),
    'WHERE MovimID=' + Geral.FF0(Integer(emidEmIndstrlzc)),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcIndzc)),
    'AND Codigo=' + Geral.FF0(FOOOrigemCodigo),
    'AND MovimCod=' + Geral.FF0(FOOOrigemMovimCod),
    '']);
    //
    TX_PF.AtualizaTotaisTXIndCab(MovimCod);
    //
    LocCod(Codigo, Codigo);
    //
  end;
}
end;

procedure TFmTXIndCab.ReopenTXIndOris(Controle, Pallet: Integer);
const
  SQL_Limit = '';
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXIndCabTemIMEIMrt.Value;
  TX_EFD_ICMS_IPI.ReopenTXIndPrcOriIMEI(QrTXIndOri, QrTXIndCabMovimCod.Value,
  Controle, TemIMEIMrt, eminIndzcSrc, SQL_Limit);
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXIndCabMovimCod.Value),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(eminIndzcSrc)),
  '']);
(*
  SQL_Group :=   'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXIndOriPallet, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  //Geral.MB_SQL(self, QrTXIndOriPallet);
  QrTXIndOriPallet.Locate('Pallet', Pallet, []);
*)
end;

procedure TFmTXIndCab.ItsAlteraInnClick(Sender: TObject);
begin
  MostraTXIndInn(stUpd, TPartOuTodo.ptParcial);
end;

procedure TFmTXIndCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod, TXPedIts, SrcNivel2: Integer;
begin
  if Geral.MB_Pergunta('Confirme a exclus�o do item de destino?') = ID_YES then
  //
  begin
    Codigo    := QrTXIndCabCodigo.Value;
    CtrlDel   := QrTXIndDstControle.Value;
    CtrlDst   := QrTXIndDstSrcNivel2.Value;
    MovimNiv  := QrTXIndInnMovimNiv.Value;
    MovimCod  := QrTXIndInnMovimCod.Value;
    TXPedIts  := QrTXIndDstPedItsFin.Value;
    //
    //
    if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    //CO_:::_TAB_TMI, 'Controle', CtrlDel, Dmod.MyDB) = ID_YES then
    begin
      DistribuicaoCusto();
      // Nao se aplica!
      //Dmod.AtualizaTotaisTXXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      TX_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := TX_PF.ObtemControleMovimTwin(
        QrTXIndDstMovimCod.Value, QrTXIndDstMovimTwn.Value, emidEmIndstrlzc,
        eminIndzcBxa);
      SrcNivel2 := DmModTX_CRC.ObtemSrcNivel2DeIMEI(CtrlDel);
      //
      TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      TX_PF.AtualizaSaldoIMEI(SrcNivel2, True);
      TX_PF.AtualizaTXPedIts_Fin(TXPedIts);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXIndDst,
        TIntegerField(QrTXIndDstControle), QrTXIndDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //TX_PF.AtualizaTotaisTXXxxCab('txindcab', MovimCod);
      TX_PF.AtualizaTotaisTXIndCab(QrTXIndCabMovimCod.Value);
      //
      TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
      TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmIndstrlzc, MovimCod);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
(*
      TX_PF.ReopenTXIndPrcDst(QrTXIndDst, QrTXIndCabMovimCod.Value, CtrlNxt,
        QrTXIndCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestIndzc);
*)
      TX_EFD_ICMS_IPI.ReopenTXPrcPrcDst(QrTXIndDst, QrTXIndCabCodigo.Value,
      QrTXIndCabMovimCod.Value, CtrlNxt, QrTXIndCabTemIMEIMrt.Value,
      TEstqMovimID.emidEmIndstrlzc, TEstqMovimNiv.eminIndzcDst);
    end;
  end;
end;

procedure TFmTXIndCab.ItsExcluiInnClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod, TXPedIts: Integer;
begin
  if QrTXIndInnSdoVrtQtd.Value < QrTXIndInnQtde.Value then
  begin
    Geral.MB_Aviso(
    'Este item em processo n�o pode ser exclu�do pois j� possui baixa!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirme a exclus�o do item em processo?') = ID_YES then
  //
  begin
    Codigo   := QrTXIndCabCodigo.Value;
    CtrlDel  := QrTXIndInnControle.Value;
    CtrlDst  := QrTXIndInnSrcNivel2.Value;
    MovimNiv := QrTXIndInnMovimNiv.Value;
    MovimCod := QrTXIndInnMovimCod.Value;
    TXPedIts := QrTXIndInnPedItsFin.Value;
    //
    if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    //CO_:::_TAB_TMI, 'Controle', CtrlDel, Dmod.MyDB) = ID_YES then
    begin
      DistribuicaoCusto();
      // Nao se aplica!
      //Dmod.AtualizaTotaisTXXxxCab('vsinncab', MovimCod);

      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      // Nao se aplica!
      //TX_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);

{
      // Nao se aplica!
      // Excluir Baixa tambem
      CtrlDel := TX_PF.ObtemControleMovimTwin(
        QrTXIndInnMovimCod.Value, QrTXIndInnMovimTwn.Value, emidEmIndstrlzc,
        eminEmOperBxa);
      TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
}
      //
      TX_PF.AtualizaTXPedIts_Fin(TXPedIts);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXIndInn,
        TIntegerField(QrTXIndInnControle), QrTXIndInnControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //TX_PF.AtualizaTotaisTXXxxCab('txindcab', MovimCod);
      TX_PF.AtualizaTotaisTXIndCab(QrTXIndCabMovimCod.Value);
      //
      TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
      TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmIndstrlzc, MovimCod);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      //
      TX_EFD_ICMS_IPI.ReopenTXIndPrcAtu(QrTXIndInn, QrTXIndCabMovimCod.Value,
      CtrlNxt, QrTXIndCabTemIMEIMrt.Value, TEstqMovimNiv.eminIndzcInn);
    end;
  end;
end;

procedure TFmTXIndCab.ItsExcluiOriIMEIClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if not FItsExcluiOriIMEI_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  begin
    Codigo    := QrTXIndCabCodigo.Value;
    CtrlDel   := QrTXIndOriControle.Value;
    CtrlOri   := QrTXIndOriSrcNivel2.Value;
    MovimNiv  := QrTXIndInnMovimNiv.Value;
    MovimCod  := QrTXIndInnMovimCod.Value;
    //
    if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
    begin
      if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti038), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
      begin
        TX_PF.AtualizaSaldoIMEI(CtrlOri, True);
        //
        // Nao se aplica. Calcula com function propria a seguir.
        //TX_PF.AtualizaTotaisTXXxxCab('txindcab', MovimCod);
        TX_PF.AtualizaTotaisTXIndCab(QrTXIndCabMovimCod.Value);
        //
        CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXIndOri,
          TIntegerField(QrTXIndOriControle), QrTXIndOriControle.Value);
        //
        TX_PF.AtualizaFornecedorInd(MovimCod);
        CorrigeFornecedorePalletsDestino(False);
        DistribuicaoCusto();
        LocCod(Codigo, Codigo);
        ReopenTXIndOris(CtrlNxt, 0);
      end;
    end;
  end;
end;

procedure TFmTXIndCab.ItsExcluiOriPalletClick(Sender: TObject);
(*
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
*)
begin
(*
  if not FItsExcluiOriPallet_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  //
  Pallet    := QrTXIndOriPalletPallet.Value;
  Itens     := 0;
  //
  QrTXIndOri.First;
  while not QrTXIndOri.Eof do
  begin
    if QrTXIndOriPallet.Value = Pallet then
      Itens := Itens + 1;
    //
    QrTXIndOri.Next;
  end;
  if Itens > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
    Geral.FF0(Pallet) + '?') = ID_YES then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Exclu�ndo IMEIS do Pallet selecionado!');
      //
      Itens := 0;
      QrTXIndOri.First;
      while not QrTXIndOri.Eof do
      begin
        if QrTXIndOriPallet.Value = Pallet then
        begin
          Itens := Itens + 1;
          //
          Codigo    := QrTXIndCabCodigo.Value;
          CtrlDel   := QrTXIndOriControle.Value;
          CtrlOri   := QrTXIndOriSrcNivel2.Value;
          MovimNiv  := QrTXIndInnMovimNiv.Value;
          MovimCod  := QrTXIndInnMovimCod.Value;
          //
          if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
          Integer(TEstqMotivDel.emtdWetCurti038),
          Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
          //Dmod.MyDB) = ID_YES then
          begin
            TX_PF.AtualizaSaldoIMEI(CtrlOri, True);
            //
            // Nao se aplica. Calcula com function propria a seguir.
            //TX_PF.AtualizaTotaisTXXxxCab('txindcab', MovimCod);
            TX_PF.AtualizaTotaisTXIndCab(QrTXIndCabMovimCod.Value);
          end;
          //
        end;
        QrTXIndOri.Next;
      end;
    end;
    CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXIndOri,
    TIntegerField(QrTXIndOriControle), QrTXIndOriControle.Value);
    //
    TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(False);
    LocCod(Codigo, Codigo);
    ReopenTXIndOris(CtrlNxt, 0);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end else Geral.MB_Erro(
    'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  //
  DistribuicaoCusto();
*)
end;

procedure TFmTXIndCab.DefineONomeDoForm;
begin
end;

procedure TFmTXIndCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Procedimento n�o implementado! Solicite � DERMATEK!');
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXIndCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXIndCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXIndCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXIndCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXIndCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXIndCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXIndCabCodigo.Value;
  Close;
end;

procedure TFmTXIndCab.ItsIncluiDstClick(Sender: TObject);
const
  DesabilitaBaixa = False;
begin
  MostraTXIndDst(stIns, DesabilitaBaixa);
end;

procedure TFmTXIndCab.ItsIncluiInnClick(Sender: TObject);
begin
  MostraTXIndInn(stIns, TPartOuTodo.ptParcial);
end;

procedure TFmTXIndCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXIndCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'txindcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrTXIndCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
{
  EdSerieRem.ValueVariant   := QrTXIndCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrTXIndCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrTXIndCabLPFMO.Value;
  //
  EdOperacoes.ValueVariant   := QrTXIndCabOperacoes.Value;
  CBOperacoes.KeyValue       := QrTXIndCabOperacoes.Value;
  //
  //
  EdControle.ValueVariant   := QrTXIndCabControle.Value;
  EdGraGruX.ValueVariant    := QrTXIndCabGraGruX.Value;
  CBGraGruX.KeyValue        := QrTXIndCabGraGruX.Value;
  EdCustoMOTot.ValueVariant := QrTXIndCabCustoMOTot.Value;
  EdQtde.ValueVariant      := QrTXIndCabQtde.Value;
  EdMovimTwn.ValueVariant   := QrTXIndCabMovimTwn.Value;
  EdObserv.ValueVariant     := QrTXIndCabObserv.Value;
  //
  TX_PF.ReopenPedItsXXX(QrTXPedIts, QrTXIndCabControle.Value, QrTXIndCabControle.Value);
  Habilita := QrTXIndCabPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrTXIndCabPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrTXIndCabPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrTXIndCabStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrTXIndCabStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrTXIndCabReqMovEstq.Value;
  //
  EdCliente.ValueVariant    := QrTXIndCabCliente.Value;
  CBCliente.KeyValue        := QrTXIndCabCliente.Value;
  //
  EdClientMO.ValueVariant   := QrTXIndCabClientMO.Value;
  CBClientMO.KeyValue       := QrTXIndCabClientMO.Value;
  //
  EdFornecMO.ValueVariant   := QrTXIndCabFornecMO.Value;
  CBFornecMO.KeyValue       := QrTXIndCabFornecMO.Value;
  //
  EdTXCOPCab.ValueVariant   := QrTXIndCabTXCOPCab.Value;
  CBTXCOPCab.KeyValue       := QrTXIndCabTXCOPCab.Value;
  //
}
end;

procedure TFmTXIndCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrAberto, LPFMO: String;
  Codigo, MovimCod, Empresa, TXCOPCab, Cliente, SerieRem, NFeRem, FluxoCab,
  ClientMO, FornecMO, StqCenLoc, PedItsLib: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  MovimCod       := EdMovimCod.ValueVariant;
  Nome           := EdNome.Text;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  TXCOPCab       := EdTXCOPCab.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  FluxoCab       := EdFluxoCab.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  //CustoMOTot     := EdCustoMOTot.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdCLientMO, 'Defina o dono do material (a pr�pria empresa ou outra contratante de M.O.!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque!') then
    Exit;
  if MyObjects.FIC(FluxoCab = 0, EdFluxoCab, 'Informe o fluxo de produ��o!') then
    Exit;
  //
  if not TX_PF.ValidaCampoNF(2, EdNFeRem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txindcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txindcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'txindcab', False, [
  'MovimCod', 'Nome', 'Empresa',
  'Cliente', 'DtHrAberto', 'SerieRem',
  'NFeRem', 'LPFMO', 'TXCOPCab',
  'FluxoCab', 'ClientMO', 'FornecMO',
  'StqCenLoc'], [
  'Codigo'], [
  MovimCod, Nome, Empresa,
  Cliente, DtHrAberto, SerieRem,
  NFeRem, LPFMO, TXCOPCab,
  FluxoCab, ClientMO, FornecMO,
  StqCenLoc], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      TX_PF.InsereTXMovCab(MovimCod, emidEmIndstrlzc, Codigo);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrTXIndOri.RecordCount = 0) then
    begin
{POIU
      InsereArtigoEmOperacao(Codigo, MovimCod, Empresa, ClientMO, GraGruX, DtHrAberto);
      //
      ItsIncluiDst.Enabled  := True;
      //
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      'Pallet Total', 'Pallet Parcial',
      'IME-I Total',  'IME-I Parcial'], -1);
      case Forma of
        0: MostraTXOpeOriPall2(stIns, ptTotal);
        1: MostraTXOpeOriPall2(stIns, ptParcial);
        2: MostraTXOpeOriIMEI2(stIns, ptTotal);
        3: MostraTXOpeOriIMEI2(stIns, ptParcial);
      end;
}
      ItsIncluiDst.Enabled  := False;
    end else begin
{POIU
      TX_PF.AtualizaFornecedorInd(MovimCod);
      //
      if EdNFeRem.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
}
      TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmIndstrlzc, QrTXIndCabMovimCod.Value);
    end;
  end;
end;

procedure TFmTXIndCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'txindcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txindcab', 'Codigo');
end;

procedure TFmTXIndCab.BtDstClick(Sender: TObject);
begin
  PCInnDst.ActivePage := TsDst;
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmTXIndCab.BtInnClick(Sender: TObject);
begin
  PCInnDst.ActivePage := TsInn;
  MyObjects.MostraPopUpDeBotao(PMInn, BtInn);
end;

procedure TFmTXIndCab.BtOriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmTXIndCab.Adicionaartigodeorigem1Click(Sender: TObject);
begin
  //MostraTXOpeOriIMEI2(stIns, ptParcial);
  MostraTXIndOri(stIns, ptParcial);
end;

procedure TFmTXIndCab.Alteraartigodedestino1Click(Sender: TObject);
var
  DesabilitaBaixa: Boolean;
begin
  DesabilitaBaixa := QrTXIndBxa.RecordCount = 0;
  MostraTXIndDst(stUpd, DesabilitaBaixa);
end;

procedure TFmTXIndCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCIndOri.ActivePageIndex := 2;
  PCIndOri.ActivePage := TsEnvioMO;
  //
  TX_PF.MostraFormTXMOEnvEnv(stUpd, QrTXIndOri, QrTXMOEnvEnv, QrTXMOEnvETMI,
  siNegativo, 0, 0);
  //
  LocCod(QrTXIndCabCodigo.Value,QrTXIndCabCodigo.Value);
  //
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXIndCab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCIndOri.ActivePageIndex := 2;
  PCIndOri.ActivePage := TsRetornoMO;
  //
  TX_PF.MostraFormTXMOEnvRet(stUpd, QrTXIndInn, QrTXIndDst, QrTXIndBxa,
  QrTXMOEnvRet, QrTXMOEnvGTMI, QrTXMOEnvRTMI, 0, 0, 0, siPositivo, FGerArX2Env);
  //
  LocCod(QrTXIndCabCodigo.Value,QrTXIndCabCodigo.Value);
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXIndCab.AlteraLocaldoestoque1Click(Sender: TObject);
begin
  TX_PF.InfoStqCenLoc(QrTXIndDstControle.Value,
    QrTXIndDstStqCenLoc.Value, QrTXIndDst);
end;

procedure TFmTXIndCab.AlteraLocaldoestoque2Click(Sender: TObject);
begin
  TX_PF.InfoStqCenLoc(QrTXIndInnControle.Value,
    QrTXIndInnStqCenLoc.Value, QrTXIndInn);
end;

procedure TFmTXIndCab.AlteraPallet1Click(Sender: TObject);
begin
  if QrTXIndDstQtde.Value <> QrTXIndDstSdoVrtQtd.Value   then
  begin
    Geral.MB_Info('Altera��o abortada! Saldo difere do esperado!');
    Exit;
  end;
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  TX_PF.InfoPalletTMI(QrTXIndDstControle.Value,
    QrTXIndDstPallet.Value, QrTXIndDst);
end;

procedure TFmTXIndCab.Apreencher1Click(Sender: TObject);
begin
  ImprimeIMEI(viikOPaPreencher);
end;

procedure TFmTXIndCab.AtualizaestoqueEmindustrializao1Click(Sender: TObject);
begin
  TX_PF.AtualizaSaldoIMEI(QrTXIndInnControle.Value, True);
  TX_PF.AtualizaTotaisTXIndCab(QrTXIndCabMovimCod.Value);
  LocCod(QrTXIndCabCodigo.Value, QrTXIndCabCodigo.Value);
end;

procedure TFmTXIndCab.AtualizaFornecedoresDeDestino();
var
  Qry: TmySQLQuery;
  Codigo, MovimCod: Integer;
begin
{POIU
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, MovimCod',
    'FROM txindcab ',
    'WHERE Codigo>=' + Geral.FF0(QrTXIndCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Codigo   := Qry.FieldByName('Codigo').AsInteger;
        MovimCod := Qry.FieldByName('MovimCod').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando a OO ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        TX_PF.AtualizaFornecedorInd(QrTXIndCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
}
end;

procedure TFmTXIndCab.AtualizaNFeItens();
begin
{POIU
  DmModTX_CRC.AtualizaTXMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXIndCabMovimCod.Value, TEstqMovimID.emidEmIndstrlzc, [eminSorcIndzc],
  [eminEmIndzcInn, eminDestIndzc, eminEmOperBxa]);
}
end;

procedure TFmTXIndCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXIndCab, QrTXIndCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, CabAltera1, Corrigirfornecedor1,
  Desfazencerramento1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXIndCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  FOOOrigemCodigo := 0;
  FOOOrigemMovimCod := 0;
  FOOOrigemAtuControle := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  //GBEdita.Align := alClient;
  PnDst.Align := alClient;
  PCInnDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
{POIU
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_4096_TXRibOpe));
  TX_PF.AbreGraGruXY(QrGGXDst,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_3072_TXRibCla));
}
  TX_PF.ReopenTXPrestador(QrPrestador);
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrFluxoCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOperacoes, Dmod.MyDB);
{POIU
  TX_PF.ReopenTXCOPCab(QrTXCOPCab, emidEmIndstrlzc);
}
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCIndOri, VAR_PC_FRETE_TX_NAME, 0);
  //MyObjects.DefinePageControlActivePageByName(PCOpeDst, VAR_PC_FRETE_TX_NAME, 0);
end;

procedure TFmTXIndCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXIndCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTXIndCab.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmTXIndCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmTXIndCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXIndCab.QrTXMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
{POIU
  TX_PF.ReopenTXMOEnvETMI(QrTXMOEnvETmi, QrTXMOEnvEnvCodigo.Value, 0);
}
end;

procedure TFmTXIndCab.QrTXMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrTXMOEnvETMI.Close;
end;

procedure TFmTXIndCab.QrTXMOEnvRetAfterScroll(DataSet: TDataSet);
begin
{POIU
  TX_PF.ReopenTXMOEnvRTmi(QrTXMOEnvRTmi, QrTXMOEnvRetCodigo.Value, 0);
  TX_PF.ReopenTXMOEnvGTmi(QrTXMOEnvGTmi, QrTXMOEnvRetCodigo.Value, 0);
}
end;

procedure TFmTXIndCab.QrTXMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrTXMOEnvGTmi.Close;
  QrTXMOEnvRTmi.Close;
end;

procedure TFmTXIndCab.QrTXIndCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXIndCab.QrTXIndCabAfterScroll(DataSet: TDataSet);
begin
  TX_EFD_ICMS_IPI.ReopenTXRibItss(QrTXIndInn, QrTXIndOri,
  (*QrTXIndOriPallet*) nil,
  QrTXIndDst, (*QrTXOpeInd*)nil, (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  (*QrEmit*)nil, (*QrPQO*)nil, QrTXIndCabCodigo.Value,
  QrTXIndCabMovimCod.Value, QrTXIndCabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmIndstrlzc, emidEmIndstrlzc, emidEmIndstrlzc,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminIndzcInn, eminIndzcSrc, eminIndzcDst);
  //
{$IfDef sAllTX}
  TX_PF.ReopenTXMOEnvEnv(QrTXMOEnvEnv, QrTXIndCabMovimCod.Value, 0);
  TX_PF.ReopenTXMOEnvRet(QrTXMOEnvRet, QrTXIndCabMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXIndCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXIndCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXIndCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTXIndCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'txindcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTXIndCab.SbTXCOPCabCadClick(Sender: TObject);
begin
{POIU
  VAR_CADASTRO := 0;
  TX_PF.MostraFormTXCOPCab(0);
  UMyMod.SetaCodigoPesquisado(EdTXCOPCab, CBTXCOPCab, QrTXCOPCab, VAR_CADASTRO);
}
end;

procedure TFmTXIndCab.SbTXCOPCabCpyClick(Sender: TObject);
const
  EdCliente = nil;
  CBCliente = nil;
  EdCustoMOKg = nil;
begin
{POIU
  TX_PF.PreencheDadosDeTXCOPCab(EdTXCOPCab.ValueVariant, EdEmpresa, CBEmpresa, RGTipoA r e a,
  EdGraGruX, CBGraGruX, EdGGXDst, CBGGXDst, EdFornecMO, CBFornecMO, EdStqCenLoc,
  CBStqCenLoc, EdOperacoes, CBOperacoes, EdCliente, CBCliente, EdPedItsLib,
  CBPedItsLib, EdClientMO, CBClientMO, EdCustoMOKg);
}
end;

procedure TFmTXIndCab.Somenteinfo1Click(Sender: TObject);
begin
  ImprimeIMEI(viikArtigoGerado);
end;

procedure TFmTXIndCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXIndCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmTXIndCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdCliente.ValueVariant    := 0;
  CBCliente.KeyValue        := 0;
  EdClientMO.ValueVariant   := 0;
  CBClientMO.KeyValue       := 0;
  EdFornecMO.ValueVariant   := 0;
  CBFornecMO.KeyValue       := 0;
  EdFluxoCab.ValueVariant   := 0;
  CBFluxoCab.KeyValue       := 0;
  EdCustoMOTot.ValueVariant := 0;
  //EdQtde.ValueVariant       := 0;
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXIndCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txindcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  TX_PF.ReopenPedItsXXX(QrTXPedIts, 0, 0);
  LaPedItsLib.Enabled       := True;
  EdPedItsLib.Enabled       := True;
  CBPedItsLib.Enabled       := True;
  if EdEmpresa.ValueVariant > 0 then
    TPData.SetFocus;
end;

procedure TFmTXIndCab.QrTXIndCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXIndInn.Close;
  QrTXIndOri.Close;
  //QrTXIndOriPallet.Close;
  QrTXIndDst.Close;
  QrForcados.Close;
  QrTXMOEnvEnv.Close;
  QrTXMOEnvRet.Close;
end;

procedure TFmTXIndCab.QrTXIndCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXIndCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTXIndCab.QrTXIndCabCalcFields(DataSet: TDataSet);
begin
  QrTXIndCabNO_DtHrLibOpe.Value := Geral.FDT(QrTXIndCabDtHrLibOpe.Value, 106, True);
  QrTXIndCabNO_DtHrFimOpe.Value := Geral.FDT(QrTXIndCabDtHrFimOpe.Value, 106, True);
  QrTXIndCabNO_DtHrCfgOpe.Value := Geral.FDT(QrTXIndCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmTXIndCab.QrTXIndDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXIndCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  vsp ON vsp.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXIndCabMovimCod.Value),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(eminIndzcBxa)),
  'AND tmi.MovimTwn=' + Geral.FF0(QrTXIndDstMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXIndBxa, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
end;

procedure TFmTXIndCab.QrTXIndDstBeforeClose(DataSet: TDataSet);
begin
  QrTXIndBxa.Close;
end;

{
object QrGraGruX: TmySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
    'FROM vsribcad wmp'
    'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
    '')
  Left = 792
  Top = 452
  object QrGraGruXGraGru1: TIntegerField
    FieldName = 'GraGru1'
  end
  object QrGraGruXControle: TIntegerField
    FieldName = 'Controle'
  end
  object QrGraGruXNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrGraGruXSIGLAUNIDMED: TWideStringField
    FieldName = 'SIGLAUNIDMED'
    Size = 6
  end
  object QrGraGruXCODUSUUNIDMED: TIntegerField
    FieldName = 'CODUSUUNIDMED'
  end
  object QrGraGruXNOMEUNIDMED: TWideStringField
    FieldName = 'NOMEUNIDMED'
    Size = 30
  end
end
object DsGraGruX: TDataSource
  DataSet = QrGraGruX
  Left = 792
  Top = 500
end
}

{
object CkCpermitirCrust: TCheckBox
  Left = 668
  Top = 14
  Width = 141
  Height = 17
  Caption = 'Permitir semi e acabado.'
  Enabled = False
  TabOrder = 2
  OnClick = CkCpermitirCrustClick
end
}
{POIU
  if CkCpermitirCrust.Checked then
    TX_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_TXFinCla))
  else
    TX_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_3072_TXRibCla));
}

{
object QrTXIndOriPallet: TmySQLQuery
  Database = Dmod.MyDB
  Left = 260
  Top = 365
  object QrTXIndOriPalletPallet: TLargeintField
    FieldName = 'Pallet'
  end
  object QrTXIndOriPalletGraGruX: TLargeintField
    FieldName = 'GraGruX'
  end
  object QrTXIndOriPalletQtde: TFloatField
    FieldName = 'Qtde'
  end
  object QrTXIndOriPalletNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrTXIndOriPalletNO_Pallet: TWideStringField
    FieldName = 'NO_Pallet'
    Size = 60
  end
  object QrTXIndOriPalletNO_TTW: TWideStringField
    FieldName = 'NO_TTW'
    Required = True
    Size = 5
  end
  object QrTXIndOriPalletID_TTW: TLargeintField
    FieldName = 'ID_TTW'
    Required = True
  end
  object QrTXIndOriPalletTerceiro: TLargeintField
    FieldName = 'Terceiro'
  end
  object QrTXIndOriPalletMarca: TWideStringField
    FieldName = 'Marca'
  end
  object QrTXIndOriPalletNO_FORNECE: TWideStringField
    FieldName = 'NO_FORNECE'
    Size = 100
  end
  object QrTXIndOriPalletValorT: TFloatField
    FieldName = 'ValorT'
  end
  object QrTXIndOriPalletSdoVrtQtd: TFloatField
    FieldName = 'SdoVrtQtd'
    DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
  end
end
object DsTXOpeOriPallet: TDataSource
  DataSet = QrTXIndOriPallet
  Left = 260
  Top = 413
end
}

end.

