unit TXPlCIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnProjGroup_Consts, UnAppEnums;

type
  TFmTXPlCIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label4: TLabel;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    DsDefPecas: TDataSource;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    QrTXPallet: TmySQLQuery;
    DsTXPallet: TDataSource;
    QrTXPalletCodigo: TIntegerField;
    DsFornecedor: TDataSource;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    Panel3: TPanel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdMovimCod: TdmkDBEdit;
    Label3: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Panel5: TPanel;
    EdValorT: TdmkEdit;
    Label7: TLabel;
    Label10: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGruY: TIntegerField;
    Label8: TLabel;
    EdValorMP: TdmkEdit;
    Label12: TLabel;
    EdCustoMOUni: TdmkEdit;
    EdCustoMOTot: TdmkEdit;
    Label13: TLabel;
    EdFornecMO: TdmkEditCB;
    Label14: TLabel;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    Label11: TLabel;
    EdSerieTal: TdmkEditCB;
    CBSerieTal: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdTalao: TdmkEdit;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    Label9: TLabel;
    EdMarca: TdmkEdit;
    EdObserv: TdmkEdit;
    Label16: TLabel;
    SBNewPallet: TSpeedButton;
    QrTXPalletGraGruX: TIntegerField;
    QrTXPalletNO_PRD_TAM_COR: TWideStringField;
    Label17: TLabel;
    EdCustoMOM2: TdmkEdit;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure EdValorMPChange(Sender: TObject);
    procedure EdTalaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCustoMOUniChange(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure ReopenTXPallet();
    procedure ReopenTXPlcIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure CalculaCustos();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO: Integer;
  end;

  var
  FmTXPlCIts: TFmTXPlCIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXPlCCab, Principal, UnTX_PF, ModuleGeral, AppListas, ModTX_CRC, UnTX_Jan;

{$R *.DFM}

procedure TFmTXPlCIts.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = True;
  ExigeAreaouPeca = True;
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGer     = 0;
  AptoUso    = 0;
  SrcGGX     = 0;
  DstGGX     = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletMP = nil;
  EdAreaM2MP = nil;
  EdFichaRMP = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  TXMulFrnCab = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, ClientMO, GraGruX, Terceiro,
  Talao, GraGruY, FornecMO, SerieTal, StqCenLoc, ReqMovEstq: Integer;
  Qtde, CustoMOUni, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  EdPalletX, EdAreaM2X: TdmkEdit;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := FClientMO;
  Terceiro       := EdFornecedor.ValueVariant;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEntradaPlC;
  MovimNiv       := eminSemNiv;
  Pallet         := EdPallet.ValueVariant;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Qtde           := EdQtde.ValueVariant;
  CustoMOUni     := EdCustoMOUni.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Talao          := EdTalao.ValueVariant;
  GraGruY        := QrGraGruXGraGruY.Value;
  FornecMO       := EdFornecMO.ValueVariant;
  SerieTal       := EdSerieTal.ValueVariant;
  Marca          := EdMarca.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  case GraGruY of
{
    CO_GraGruY_0512_TXSubPrd,
    CO_GraGruY_0683_TXPSPPro,
    CO_GraGruY_0853_TXPSPEnd,
}
    CO_GraGruY_1024_TXCadNat:
    begin
      EdPalletX := nil;
    end;
    CO_GraGruY_2048_TXCadInd:
    begin
      EdPalletX := EdPallet;
    end;
(*
    CO_GraGruY_3072_TXRibCla:
    begin
      EdPalletX := EdPallet;
    end;
*)
    CO_GraGruY_4096_TXCadInt,
    //CO_GraGruY_5120_TXWetEnd,
    CO_GraGruY_6144_TXCadFcc:
    begin
      EdPalletX := nil;
    end;
    else
    begin
      Geral.MB_Erro('"GraGruY" n�o implementado em "FmTXPlCIts.BtOKClick()"');
    end;
  end;
  //
  if MyObjects.FIC(GraGruX <> QrTXPalletGraGruX.Value, EdPallet,
  'Artigo do pallet selecionado difere do artigo selecionado!') then
    Exit;
  //
  if TX_PF.TalaoErro(EdSerieTal, Empresa, Controle, Talao) then
    Exit;
  //
  if TX_PF.TXFic(GraGruX, Empresa, (*Fornecedor*)ClientMO, Pallet, Qtde, ValorT,
    EdGraGruX, EdPalletX, EdQtde, EdValorT, ExigeFornecedor, GraGruY,
    EdStqCenLoc, SerieTal, Talao, EdSerieTal, EdTalao) then
      Exit;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos,
    SQLType, Controle);
  if TX_PF.InsUpdTXMovIts(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
  CliVenda, Controle, CustoMOUni, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao, CO_0_GGXRcl,

  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei035(*Item de Entrada de Artigo Semi Pronto por Compra*)) then
  begin
    DmModTX_CRC.AtualizaImeiValorT(Controle);
    TX_PF.AtualizaTotaisTXXxxCab('txplccab', MovimCod);
    TX_PF.AtualizaSaldoIMEI(Controle, True);
    FmTXPlcCab.AtualizaNFeItens();
    FmTXPlCCab.LocCod(Codigo, Codigo);
    FmTXPlCCab.ReopenTXPlcIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      //EdFornecedor.ValueVariant  := 0;
      //CBFornecedor.KeyValue      := Null;
      EdSerieTal.ValueVariant    := 0;
      CBSerieTal.KeyValue        := Null;
      EdTalao.ValueVariant       := 0;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      EdValorT.ValueVariant      := 0;
      EdCustoMOTot.ValueVariant  := 0;
      EdQtde.ValueVariant        := 0;
      EdCustoMOUni.ValueVariant  := 0;
      EdFornecMO.ValueVariant    := 0;
      CBFornecMO.KeyValue        := Null;
      EdObserv.Text              := '';
      //
      EdValorT.ValueVariant      := 0;
      EdValorMP.ValueVariant     := 0;
      EdCustoMOTot.ValueVariant  := 0;
      //
      SBNewPallet.Enabled := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTXPlCIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXPlCIts.CalculaCustos();
var
  ValorMP, CustoMOUni, CustoMOTot, ValorT, Qtde: Double;
begin
  ValorMP      := EdValorMP.ValueVariant;
  CustoMOUni   := EdCustoMOUni.ValueVariant;
  Qtde         := EdQtde.ValueVariant;
  //
  CustoMOTot := (CustoMOUni * Qtde);
  //
  EdCustoMOTot.ValueVariant := CustoMOTot;
  ValorT := ValorMP + CustoMOTot;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmTXPlCIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXPlCIts.EdCustoMOUniChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXPlCIts.EdTalaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Talao: Integer;
begin
  if Key = VK_F4 then
    if TX_PF.ObtemProximoTalao(FEmpresa, EdSerieTal, Talao) then
      EdTalao.ValueVariant := Talao;
end;

procedure TFmTXPlCIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXPlCIts.EdQtdeRedefinido(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXPlCIts.EdValorMPChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmTXPlCIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXPlCIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_PlC_TX + ') ');
  ReopenTXPallet();
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  TX_PF.AbreTXSerTal(QrTXSerTal);
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmTXPlCIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXPlCIts.ReopenTXPlcIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXPlCIts.ReopenTXPallet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPallet, Dmod.MyDB, [
  'SELECT pla.Codigo, pla.GraGruX, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  '" ", pla.Nome)  ',
  'NO_PRD_TAM_COR  ',
  'FROM txpalleta pla  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE pla.Ativo=1  ',
  'ORDER BY NO_PRD_TAM_COR, pla.Codigo DESC ',
  '']);
end;

procedure TFmTXPlCIts.SBNewPalletClick(Sender: TObject);
var
  Nome, DtHrEndAdd: String;
  Codigo, Empresa, CLientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(ClientMO = 0, nil, 'Defina o dono do couro!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('TXPalleta', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'TXPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    TX_PF.AtualizaStatPall(Codigo);
    ReopenTXPallet();
    EdPallet.ValueVariant := Codigo;
    CBPallet.KeyValue := Codigo;
    //
    SBNewPallet.Enabled := False;
  end;
end;

procedure TFmTXPlCIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrTXPallet, VAR_CADASTRO);
end;

procedure TFmTXPlCIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
