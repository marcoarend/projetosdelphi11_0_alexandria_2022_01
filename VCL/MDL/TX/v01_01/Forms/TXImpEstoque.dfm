object FmTXImpEstoque: TFmTXImpEstoque
  Left = 0
  Top = 0
  Caption = 'TEX-FAXAO-123 :: Impress'#227'o de Estoque TX'
  ClientHeight = 611
  ClientWidth = 853
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dmkDBGridZTO2: TdmkDBGridZTO
    Left = 0
    Top = 0
    Width = 853
    Height = 611
    Align = alClient
    DataSource = DsEstqR1
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    RowColors = <>
  end
  object QrEstqR1: TMySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrEstqR1CalcFields
    SQL.Strings = (
      'SELECT * FROM _txmovimp1_')
    Left = 64
    Top = 52
    object QrEstqR1Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_teste_.Empresa'
    end
    object QrEstqR1GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = '_teste_.GraGruX'
    end
    object QrEstqR1Qtde: TFloatField
      FieldName = 'Qtde'
      Origin = '_teste_.Qtde'
    end
    object QrEstqR1ValorT: TFloatField
      FieldName = 'ValorT'
      Origin = '_teste_.ValorT'
    end
    object QrEstqR1SdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      Origin = '_teste_.SdoVrtQtd'
    end
    object QrEstqR1PalVrtQtd: TFloatField
      FieldName = 'PalVrtQtd'
      Origin = '_teste_.PalVrtQtd'
    end
    object QrEstqR1LmbVrtQtd: TFloatField
      FieldName = 'LmbVrtQtd'
      Origin = '_teste_.LmbVrtQtd'
    end
    object QrEstqR1GraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = '_teste_.GraGru1'
    end
    object QrEstqR1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Origin = '_teste_.NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR1Pallet: TIntegerField
      FieldName = 'Pallet'
      Origin = '_teste_.Pallet'
    end
    object QrEstqR1NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = '_teste_.NO_PALLET'
      Size = 60
    end
    object QrEstqR1Terceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = '_teste_.Terceiro'
    end
    object QrEstqR1CliStat: TIntegerField
      FieldName = 'CliStat'
      Origin = '_teste_.CliStat'
    end
    object QrEstqR1Status: TIntegerField
      FieldName = 'Status'
      Origin = '_teste_.Status'
    end
    object QrEstqR1NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Origin = '_teste_.NO_FORNECE'
      Size = 100
    end
    object QrEstqR1NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Origin = '_teste_.NO_CLISTAT'
      Size = 100
    end
    object QrEstqR1NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Origin = '_teste_.NO_EMPRESA'
      Size = 100
    end
    object QrEstqR1NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Origin = '_teste_.NO_STATUS'
    end
    object QrEstqR1DataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = '_teste_.DataHora'
    end
    object QrEstqR1OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
      Origin = '_teste_.OrdGGX'
    end
    object QrEstqR1OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
      Origin = '_teste_.OrdGGY'
    end
    object QrEstqR1GraGruY: TIntegerField
      FieldName = 'GraGruY'
      Origin = '_teste_.GraGruY'
    end
    object QrEstqR1NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Origin = '_teste_.NO_GGY'
      Size = 255
    end
    object QrEstqR1NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Origin = '_teste_.NO_PalStat'
      Size = 11
    end
    object QrEstqR1Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = '_teste_.Ativo'
    end
    object QrEstqR1NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Origin = '_teste_.NO_MovimNiv'
      Size = 40
    end
    object QrEstqR1NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Origin = '_teste_.NO_MovimID'
      Size = 30
    end
    object QrEstqR1MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Origin = '_teste_.MovimNiv'
    end
    object QrEstqR1MovimID: TIntegerField
      FieldName = 'MovimID'
      Origin = '_teste_.MovimID'
    end
    object QrEstqR1IMEC: TIntegerField
      FieldName = 'IMEC'
      Origin = '_teste_.IMEC'
    end
    object QrEstqR1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = '_teste_.Codigo'
    end
    object QrEstqR1IMEI: TIntegerField
      FieldName = 'IMEI'
      Origin = '_teste_.IMEI'
    end
    object QrEstqR1Inteiros: TFloatField
      FieldName = 'Inteiros'
      Origin = '_teste_.Inteiros'
    end
    object QrEstqR1PalStat: TIntegerField
      FieldName = 'PalStat'
      Origin = '_teste_.PalStat'
    end
    object QrEstqR1CUS_UNIT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUS_UNIT'
      Calculated = True
    end
    object QrEstqR1ReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
      Origin = '_teste_.ReqMovEstq'
    end
    object QrEstqR1StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = '_teste_.StqCenCad'
    end
    object QrEstqR1NO_StqCenCad: TWideStringField
      FieldName = 'NO_StqCenCad'
      Origin = '_teste_.NO_StqCenCad'
      Size = 50
    end
    object QrEstqR1StqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Origin = '_teste_.StqCenLoc'
    end
    object QrEstqR1NO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Origin = '_teste_.NO_LOC_CEN'
      Size = 120
    end
    object QrEstqR1Historico: TWideStringField
      FieldName = 'Historico'
      Size = 255
    end
    object QrEstqR1TXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrEstqR1MulFornece: TIntegerField
      FieldName = 'MulFornece'
    end
    object QrEstqR1NO_MulFornece: TWideStringField
      FieldName = 'NO_MulFornece'
      Size = 30
    end
    object QrEstqR1NO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object QrEstqR1NO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object QrEstqR1CouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrEstqR1CouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrEstqR1ID_UNQ: TWideStringField
      FieldName = 'ID_UNQ'
      Size = 60
    end
    object QrEstqR1Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEstqR1GraGruValU: TFloatField
      FieldName = 'GraGruValU'
    end
    object QrEstqR1GraGruValT: TFloatField
      FieldName = 'GraGruValT'
    end
    object QrEstqR1NO_GRANDEZA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_GRANDEZA'
      Size = 5
      Calculated = True
    end
    object QrEstqR1GGVU_qt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GGVU_qt'
      Calculated = True
    end
    object QrEstqR1ClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrEstqR1NFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrEstqR1NFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrEstqR1TXMulNFeCab: TIntegerField
      FieldName = 'TXMulNFeCab'
    end
    object QrEstqR1NFeAgrup: TWideStringField
      FieldName = 'NFeAgrup'
      Size = 60
    end
    object QrEstqR1Marca: TWideStringField
      FieldName = 'Marca'
    end
    object QrEstqR1MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEstqR1FornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrEstqR1NO_ClientMO: TWideStringField
      FieldName = 'NO_ClientMO'
      Size = 100
    end
    object QrEstqR1NO_FornecMO: TWideStringField
      FieldName = 'NO_FornecMO'
      Size = 100
    end
    object QrEstqR1ValorTx: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ValorTx'
      Calculated = True
    end
    object QrEstqR1SerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrEstqR1Talao: TIntegerField
      FieldName = 'Talao'
    end
    object QrEstqR1NO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
  end
  object frxDsEstqR1: TfrxDBDataset
    UserName = 'frxDsEstqR1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'ValorT=ValorT'
      'SdoVrtQtd=SdoVrtQtd'
      'PalVrtQtd=PalVrtQtd'
      'LmbVrtQtd=LmbVrtQtd'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Terceiro=Terceiro'
      'CliStat=CliStat'
      'Status=Status'
      'NO_FORNECE=NO_FORNECE'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'Ativo=Ativo'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_MovimID=NO_MovimID'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'IMEC=IMEC'
      'Codigo=Codigo'
      'IMEI=IMEI'
      'Inteiros=Inteiros'
      'PalStat=PalStat'
      'CUS_UNIT=CUS_UNIT'
      'ReqMovEstq=ReqMovEstq'
      'StqCenCad=StqCenCad'
      'NO_StqCenCad=NO_StqCenCad'
      'StqCenLoc=StqCenLoc'
      'NO_LOC_CEN=NO_LOC_CEN'
      'Historico=Historico'
      'TXMulFrnCab=TXMulFrnCab'
      'MulFornece=MulFornece'
      'NO_MulFornece=NO_MulFornece'
      'NO_CouNiv1=NO_CouNiv1'
      'NO_CouNiv2=NO_CouNiv2'
      'CouNiv1=CouNiv1'
      'CouNiv2=CouNiv2'
      'ID_UNQ=ID_UNQ'
      'Grandeza=Grandeza'
      'GraGruValU=GraGruValU'
      'GraGruValT=GraGruValT'
      'NO_GRANDEZA=NO_GRANDEZA'
      'GGVU_qt=GGVU_qt'
      'ClientMO=ClientMO'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'TXMulNFeCab=TXMulNFeCab'
      'NFeAgrup=NFeAgrup'
      'Marca=Marca'
      'MovimCod=MovimCod'
      'FornecMO=FornecMO'
      'NO_ClientMO=NO_ClientMO'
      'NO_FornecMO=NO_FornecMO'
      'ValorTx=ValorTx'
      'SerieTal=SerieTal'
      'Talao=Talao'
      'NO_SerieTal=NO_SerieTal')
    DataSet = QrEstqR1
    BCDToCurrency = False
    Left = 64
    Top = 96
  end
  object DsEstqR1: TDataSource
    DataSet = QrEstqR1
    Left = 64
    Top = 140
  end
  object frxTEX_FAXAO_123_00_C2: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxTEX_FAXAO_123_00_AGetValue
    Left = 64
    Top = 236
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR1
        DataSetName = 'frxDsEstqR1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_MEU]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 86.929190000000000000
          Top = 64.252010000000000000
          Width = 294.803242360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 30.236240000000000000
          Top = 64.252010000000000000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NO_STQCENCAD]')
          ParentFont = False
        end
        object MeTitQtde: TfrxMemoView
          Left = 582.047620000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 514.016080000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Custo total')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 476.220780000000000000
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/Uni')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 279.685220000000000000
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_NO_FORNECE]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 381.732530000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo69: TfrxMemoView
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo70: TfrxMemoView
          Left = 650.079160000000000000
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEstqR1
        DataSetName = 'frxDsEstqR1'
        RowCount = 0
        object Memo64: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 30.236240000000000000
          Width = 619.842920000000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
        end
        object MeValNome: TfrxMemoView
          Left = 86.929190000000000000
          Width = 294.803242360000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 30.236240000000000000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR1."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValQtde: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."PalVrtQtd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD2_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."GraGruValT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD2_UM2: TfrxMemoView
          Left = 476.220780000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."GGVU_Qt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 381.732530000000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          DataField = 'Historico'
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR1."Historico"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 566.929499999999900000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118119999999980000
          Width = 476.220584720000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeRS1_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Top = 15.118119999999980000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."GraGruValT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 476.220780000000000000
          Top = 15.118119999999980000
          Width = 37.795280470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."GraGruValT">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1' +
              '), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 582.047620000000000000
          Top = 15.118119999999980000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruY"'
        object Memo33: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Me_GH0: TfrxMemoView
          Width = 680.314985040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        object Memo51: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Me_FT0: TfrxMemoView
          Left = 45.354360000000000000
          Width = 430.866249130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeFT0_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."GraGruValT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 476.220780000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."GraGruValT">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1' +
              '), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Memo42: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_GH2: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.519685040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Memo35: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Me_GH1: TfrxMemoView
          Left = 11.338590000000000000
          Width = 657.637805040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        object Memo49: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Me_FT1: TfrxMemoView
          Left = 45.354360000000000000
          Width = 430.866249130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeFT1_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."GraGruValT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 476.220780000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."GraGruValT">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1' +
              '), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo45: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_FT2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 430.866249130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeFT2_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."GraGruValT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 476.220780000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."GraGruValT">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1' +
              '), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_03: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Memo43: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Me_GH3: TfrxMemoView
          Left = 26.456710000000000000
          Width = 623.622035040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_03: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo44: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 430.866249130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeFT3_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."GraGruValT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeFT3_UM2: TfrxMemoView
          Left = 476.220780000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."GraGruValT">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1' +
              '), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxTEX_FAXAO_123_00_C1: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxTEX_FAXAO_123_00_AGetValue
    Left = 64
    Top = 188
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR1
        DataSetName = 'frxDsEstqR1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_MEU]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 86.929190000000000000
          Top = 64.252010000000000000
          Width = 291.023712360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 30.236240000000000000
          Top = 64.252010000000000000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_NO_STQCENCAD]')
          ParentFont = False
        end
        object MeTitQtde: TfrxMemoView
          Left = 582.047620000000000000
          Top = 64.252010000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 514.016080000000000000
          Top = 64.252010000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Custo total')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 483.779840000000000000
          Top = 64.252010000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/Uni')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 279.685220000000000000
          Top = 41.574830000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_NO_FORNECE]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 377.953000000000000000
          Top = 64.252010000000000000
          Width = 105.826771650000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo69: TfrxMemoView
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo70: TfrxMemoView
          Left = 650.079160000000000000
          Top = 64.252010000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEstqR1
        DataSetName = 'frxDsEstqR1'
        RowCount = 0
        object Memo64: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 30.236240000000000000
          Width = 619.842920000000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
        end
        object MeValNome: TfrxMemoView
          Left = 86.929190000000000000
          Width = 291.023712360000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 30.236240000000000000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR1."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValQtde: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."PalVrtQtd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD2_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."ValorTx"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD2_UM2: TfrxMemoView
          Left = 483.779840000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."CUS_UNIT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 377.953000000000000000
          Width = 105.826771650000000000
          Height = 13.228346460000000000
          DataField = 'Historico'
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR1."Historico"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 566.929499999999900000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118119999999980000
          Width = 483.779644720000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeRS1_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Top = 15.118119999999980000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorTx">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 483.779840000000000000
          Top = 15.118119999999980000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."ValorTx">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1), ' +
              '0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 582.047620000000000000
          Top = 15.118119999999980000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruY"'
        object Memo33: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Me_GH0: TfrxMemoView
          Width = 680.314985040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        object Memo51: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Me_FT0: TfrxMemoView
          Left = 45.354360000000000000
          Width = 438.425309130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeFT0_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorTx">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 483.779840000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."ValorTx">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1), ' +
              '0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Memo42: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_GH2: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.519685040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Memo35: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Me_GH1: TfrxMemoView
          Left = 11.338590000000000000
          Width = 657.637805040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        object Memo49: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Me_FT1: TfrxMemoView
          Left = 45.354360000000000000
          Width = 438.425309130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeFT1_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorTx">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 483.779840000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."ValorTx">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1), ' +
              '0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo45: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Me_FT2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 438.425309130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"] ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeFT2_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorTx">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 483.779840000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."ValorTx">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1), ' +
              '0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_03: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEstqR1."GraGruX"'
        object Memo43: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Me_GH3: TfrxMemoView
          Left = 26.456710000000000000
          Width = 623.622035040000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_03: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo44: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11053224
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.196865040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 15.118120000000000000
          Width = 650.078745040000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14211288
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 22.677180000000000000
          Width = 634.960625040000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15527148
          ParentFont = False
        end
        object Me_FT3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 438.425309130000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR1."NO_GGY"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeFT3_TVL: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqR1."ValorTx">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeFT3_UM2: TfrxMemoView
          Left = 483.779840000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataSet = frxDsEstqR1
          DataSetName = 'frxDsEstqR1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1) > 0, SUM(<frxDsEstqR' +
              '1."ValorTx">,MD002,1) / SUM(<frxDsEstqR1."PalVrtQtd">,MD002,1), ' +
              '0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
