unit TXMovIts;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, AppListas,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, UnProjGroup_Consts, UnGrl_Consts,
  UnAppEnums;

type
  TFmTXMovIts = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrTXMovIts: TmySQLQuery;
    DsTXMovIts: TDataSource;
    QrTXMovSrc: TmySQLQuery;
    DsTXMovSrc: TDataSource;
    QrTXMovDst: TmySQLQuery;
    DsTXMovDst: TDataSource;
    Panel6: TPanel;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    Panel9: TPanel;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    Panel10: TPanel;
    GroupBox2: TGroupBox;
    Panel11: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label13: TLabel;
    DBEdit12: TDBEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label14: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GroupBox3: TGroupBox;
    Panel12: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    GroupBox4: TGroupBox;
    Panel13: TPanel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    GroupBox5: TGroupBox;
    Panel14: TPanel;
    DBEdit28: TDBEdit;
    Panel15: TPanel;
    DBEdit40: TDBEdit;
    Label40: TLabel;
    DBEdit33: TDBEdit;
    DBEdit41: TDBEdit;
    Label41: TLabel;
    Label33: TLabel;
    DBEdit32: TDBEdit;
    Label32: TLabel;
    Label31: TLabel;
    DBEdit31: TDBEdit;
    GroupBox6: TGroupBox;
    Panel16: TPanel;
    DBEdit42: TDBEdit;
    GroupBox7: TGroupBox;
    Panel17: TPanel;
    DBEdit47: TDBEdit;
    Panel7: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    BtIMEI: TBitBtn;
    PMIMEI: TPopupMenu;
    ExcluiIMEI1: TMenuItem;
    GBMovSrc: TGroupBox;
    DGMovSrc: TDBGrid;
    GBMovDst: TGroupBox;
    Splitter1: TSplitter;
    DGMovDst: TDBGrid;
    AlteradadosdoartigodoIMEI1: TMenuItem;
    N1: TMenuItem;
    QrInacab: TmySQLQuery;
    QrInacabTMI_Baix: TIntegerField;
    QrInacabTXPaClaIts: TIntegerField;
    QrInacabBxaQtde: TFloatField;
    QrInacabTMI_Qtde: TFloatField;
    QrInacabSALDO: TFloatField;
    QrInacabTMI_Sorc: TIntegerField;
    PB1: TProgressBar;
    Recalcula1: TMenuItem;
    Label46: TLabel;
    EdSenha: TEdit;
    Label51: TLabel;
    DBEdit46: TDBEdit;
    IrparagerenciamentodoMovimento1: TMenuItem;
    PMIMIEOrig: TPopupMenu;
    IrparaajaneladoMovimento1: TMenuItem;
    PMIMEIDest: TPopupMenu;
    MenuItem1: TMenuItem;
    Irparajaneladopallet1: TMenuItem;
    IrparajaneladaFichaRMP1: TMenuItem;
    Label52: TLabel;
    DBEdit51: TDBEdit;
    AlteradadosdoitemdeIMEI1: TMenuItem;
    Label53: TLabel;
    DBEdit52: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    Label38: TLabel;
    Label37: TLabel;
    DBEdit36: TDBEdit;
    Label36: TLabel;
    DBEdit35: TDBEdit;
    Label35: TLabel;
    Label28: TLabel;
    Label42: TLabel;
    Label47: TLabel;
    QrTXMovSrcCodigo: TLargeintField;
    QrTXMovSrcControle: TLargeintField;
    QrTXMovSrcMovimCod: TLargeintField;
    QrTXMovSrcMovimNiv: TLargeintField;
    QrTXMovSrcMovimTwn: TLargeintField;
    QrTXMovSrcEmpresa: TLargeintField;
    QrTXMovSrcTerceiro: TLargeintField;
    QrTXMovSrcCliVenda: TLargeintField;
    QrTXMovSrcMovimID: TLargeintField;
    QrTXMovSrcDataHora: TDateTimeField;
    QrTXMovSrcPallet: TLargeintField;
    QrTXMovSrcGraGruX: TLargeintField;
    QrTXMovSrcQtde: TFloatField;
    QrTXMovSrcValorT: TFloatField;
    QrTXMovSrcSrcMovID: TLargeintField;
    QrTXMovSrcSrcNivel1: TLargeintField;
    QrTXMovSrcSrcNivel2: TLargeintField;
    QrTXMovSrcSrcGGX: TLargeintField;
    QrTXMovSrcSdoVrtQtd: TFloatField;
    QrTXMovSrcObserv: TWideStringField;
    QrTXMovSrcSerieTal: TLargeintField;
    QrTXMovSrcTalao: TLargeintField;
    QrTXMovSrcFornecMO: TLargeintField;
    QrTXMovSrcCustoMOUni: TFloatField;
    QrTXMovSrcCustoMOTot: TFloatField;
    QrTXMovSrcValorMP: TFloatField;
    QrTXMovSrcDstMovID: TLargeintField;
    QrTXMovSrcDstNivel1: TLargeintField;
    QrTXMovSrcDstNivel2: TLargeintField;
    QrTXMovSrcDstGGX: TLargeintField;
    QrTXMovSrcQtdGer: TFloatField;
    QrTXMovSrcQtdAnt: TFloatField;
    QrTXMovSrcStqCenLoc: TLargeintField;
    QrTXMovSrcReqMovEstq: TLargeintField;
    QrTXMovSrcGraGru1: TLargeintField;
    QrTXMovSrcNO_EstqMovimID: TWideStringField;
    QrTXMovSrcNO_MovimID: TWideStringField;
    QrTXMovSrcNO_MovimNiv: TWideStringField;
    QrTXMovSrcNO_PALLET: TWideStringField;
    QrTXMovSrcNO_PRD_TAM_COR: TWideStringField;
    QrTXMovSrcNO_FORNECE: TWideStringField;
    QrTXMovSrcNO_SerieTal: TWideStringField;
    QrTXMovSrcNO_TTW: TWideStringField;
    QrTXMovSrcID_TTW: TLargeintField;
    QrTXMovSrcNO_LOC_CEN: TWideStringField;
    QrTXMovDstCodigo: TLargeintField;
    QrTXMovDstControle: TLargeintField;
    QrTXMovDstMovimCod: TLargeintField;
    QrTXMovDstMovimNiv: TLargeintField;
    QrTXMovDstMovimTwn: TLargeintField;
    QrTXMovDstEmpresa: TLargeintField;
    QrTXMovDstTerceiro: TLargeintField;
    QrTXMovDstCliVenda: TLargeintField;
    QrTXMovDstMovimID: TLargeintField;
    QrTXMovDstDataHora: TDateTimeField;
    QrTXMovDstPallet: TLargeintField;
    QrTXMovDstGraGruX: TLargeintField;
    QrTXMovDstQtde: TFloatField;
    QrTXMovDstValorT: TFloatField;
    QrTXMovDstSrcMovID: TLargeintField;
    QrTXMovDstSrcNivel1: TLargeintField;
    QrTXMovDstSrcNivel2: TLargeintField;
    QrTXMovDstSrcGGX: TLargeintField;
    QrTXMovDstSdoVrtQtd: TFloatField;
    QrTXMovDstObserv: TWideStringField;
    QrTXMovDstSerieTal: TLargeintField;
    QrTXMovDstTalao: TLargeintField;
    QrTXMovDstFornecMO: TLargeintField;
    QrTXMovDstCustoMOUni: TFloatField;
    QrTXMovDstCustoMOTot: TFloatField;
    QrTXMovDstValorMP: TFloatField;
    QrTXMovDstDstMovID: TLargeintField;
    QrTXMovDstDstNivel1: TLargeintField;
    QrTXMovDstDstNivel2: TLargeintField;
    QrTXMovDstDstGGX: TLargeintField;
    QrTXMovDstQtdGer: TFloatField;
    QrTXMovDstQtdAnt: TFloatField;
    QrTXMovDstStqCenLoc: TLargeintField;
    QrTXMovDstReqMovEstq: TLargeintField;
    QrTXMovDstGraGru1: TLargeintField;
    QrTXMovDstNO_EstqMovimID: TWideStringField;
    QrTXMovDstNO_MovimID: TWideStringField;
    QrTXMovDstNO_MovimNiv: TWideStringField;
    QrTXMovDstNO_PALLET: TWideStringField;
    QrTXMovDstNO_PRD_TAM_COR: TWideStringField;
    QrTXMovDstNO_FORNECE: TWideStringField;
    QrTXMovDstNO_SerieTal: TWideStringField;
    QrTXMovDstNO_TTW: TWideStringField;
    QrTXMovDstID_TTW: TLargeintField;
    QrTXMovDstNO_LOC_CEN: TWideStringField;
    QrTXMovItsCodigo: TLargeintField;
    QrTXMovItsControle: TLargeintField;
    QrTXMovItsMovimCod: TLargeintField;
    QrTXMovItsMovimNiv: TLargeintField;
    QrTXMovItsMovimTwn: TLargeintField;
    QrTXMovItsEmpresa: TLargeintField;
    QrTXMovItsTerceiro: TLargeintField;
    QrTXMovItsCliVenda: TLargeintField;
    QrTXMovItsMovimID: TLargeintField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TLargeintField;
    QrTXMovItsGraGruX: TLargeintField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsValorT: TFloatField;
    QrTXMovItsSrcMovID: TLargeintField;
    QrTXMovItsSrcNivel1: TLargeintField;
    QrTXMovItsSrcNivel2: TLargeintField;
    QrTXMovItsSrcGGX: TLargeintField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsSerieTal: TLargeintField;
    QrTXMovItsTalao: TLargeintField;
    QrTXMovItsFornecMO: TLargeintField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsCustoMOTot: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TLargeintField;
    QrTXMovItsDstNivel1: TLargeintField;
    QrTXMovItsDstNivel2: TLargeintField;
    QrTXMovItsDstGGX: TLargeintField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsNO_PALLET: TWideStringField;
    QrTXMovItsNO_PRD_TAM_COR: TWideStringField;
    QrTXMovItsNO_TTW: TWideStringField;
    QrTXMovItsID_TTW: TLargeintField;
    QrTXMovItsNO_FORNECE: TWideStringField;
    QrTXMovItsNO_SerieTal: TWideStringField;
    QrTXMovItsReqMovEstq: TLargeintField;
    QrTXMovItsNO_EstqMovimID: TWideStringField;
    QrTXMovItsNO_DstMovID: TWideStringField;
    QrTXMovItsNO_SrcMovID: TWideStringField;
    QrTXMovItsNO_LOC_CEN: TWideStringField;
    QrTXMovItsGraGru1: TLargeintField;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsPedItsLib: TLargeintField;
    QrTXMovItsPedItsFin: TLargeintField;
    QrTXMovItsPedItsVda: TLargeintField;
    QrTXMovItsStqCenLoc: TLargeintField;
    QrTXMovItsItemNFe: TLargeintField;
    Label54: TLabel;
    DBEdit53: TDBEdit;
    QrTXMovItsClientMO: TLargeintField;
    QrTXMovItsCustoPQ: TFloatField;
    QrTXMovItsTXMulFrnCab: TLargeintField;
    QrTXMovItsNFeSer: TLargeintField;
    QrTXMovItsNFeNum: TLargeintField;
    QrTXMovItsTXMulNFeCab: TLargeintField;
    QrTXMovItsDtCorrApo: TDateTimeField;
    DBEdit54: TDBEdit;
    Label55: TLabel;
    Label56: TLabel;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    Label57: TLabel;
    DBEdit57: TDBEdit;
    Label58: TLabel;
    DBEdit58: TDBEdit;
    Panel18: TPanel;
    Label59: TLabel;
    DBEdit59: TDBEdit;
    Label60: TLabel;
    DBEdit60: TDBEdit;
    Recriarbaixa1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXMovItsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXMovItsBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXMovItsAfterScroll(DataSet: TDataSet);
    procedure QrTXMovItsBeforeClose(DataSet: TDataSet);
    procedure QrTXMovItsCalcFields(DataSet: TDataSet);
    procedure BtIMEIClick(Sender: TObject);
    procedure ExcluiIMEI1Click(Sender: TObject);
    procedure AlteradadosdoartigodoIMEI1Click(Sender: TObject);
    procedure IrparagerenciamentodoMovimento1Click(Sender: TObject);
    procedure IrparaajaneladoMovimento1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure IrparajaneladaFichaRMP1Click(Sender: TObject);
    procedure Irparajaneladopallet1Click(Sender: TObject);
    procedure AlteradadosdoitemdeIMEI1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMIMEIPopup(Sender: TObject);
    procedure Recriarbaixa1Click(Sender: TObject);
    procedure Recalcula1Click(Sender: TObject);
  private
    FAtualizando: Boolean;
    FTXTMIClaSdo: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormPorIMEI(IMEI: Integer);
    procedure MostraFormPorPallet(Pallet: Integer);
    function  IndiceIDNiv(): Integer;
    procedure CriaBaixaDeCaleado();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenTXMovSrc(Controle: Integer);
    procedure ReopenTXMovDst(Controle: Integer);
    //procedure ReopenTXMovXXX(Qry: TmySQLQuery; Campo: String; IMEI, CtrlLoc:
    //          Integer);
  end;

var
  FmTXMovIts: TFmTXMovIts;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnTX_PF, CreateTX,
  ModuleGeral, UnProjGroup_Vars, UnGrl_Geral, GetValor, UnTX_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXMovIts.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXMovIts.MenuItem1Click(Sender: TObject);
begin
  MostraFormPorIMEI(QrTXMovItsDstNivel2.Value);
end;

procedure TFmTXMovIts.MenuItem2Click(Sender: TObject);
begin
  MostraFormPorPallet(QrTXMovItsControle.Value);
end;

procedure TFmTXMovIts.MostraFormPorIMEI(IMEI: Integer);
begin
  if (QrTXMovIts.State <> dsInactive) and (QrTXMovIts.RecordCount > 0) then
    TX_Jan.MostraFormTX_Do_IMEI(IMEI);
end;

procedure TFmTXMovIts.MostraFormPorPallet(Pallet: Integer);
begin
  if (QrTXMovIts.State <> dsInactive) and (QrTXMovIts.RecordCount > 0) then
    TX_Jan.MostraFormTXPallet(Pallet);
end;

procedure TFmTXMovIts.PMIMEIPopup(Sender: TObject);
begin
  Recriarbaixa1.Enabled := IndiceIDNiv() = 29031;
end;

procedure TFmTXMovIts.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXMovItsControle.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXMovIts.DefParams;
const
  TemIMEIMrt = 0;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  VAR_GOTOTABELA := CO_TAB_TAB_TMI;
  VAR_GOTOMYSQLTABLE := QrTXMovIts;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := 'Controle';
  VAR_GOTONOME := 'Observ';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CAST(tst.Nome AS CHAR) NO_SerieTal, ',
  'CAST(txt.Nome AS CHAR) NO_Pallet, ',
  'CAST(CONCAT(scl.Nome, " (", scc.Nome, ")") AS CHAR) NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txt ON txt.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=tmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.Controle > 0' ,
  '']);
  SQL_Group := '';
  VAR_SQLx.Add(Geral.ATS([
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']));
  //Geral.MB_SQL(Self, QrTXMovIts);
  //
  //
  VAR_SQL1.Add('AND tmi.Controle=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND tmi.Observ Like :P0');
  //
end;

procedure TFmTXMovIts.ExcluiIMEI1Click(Sender: TObject);
var
  Controle, SrcNivel2, DstNivel2: Integer;
begin
  Controle := QrTXMovItsControle.Value;
  SrcNivel2 := QrTXMovItsSrcNivel2.Value;
  DstNivel2 := QrTXMovItsDstNivel2.Value;
  //
  if (QrTXMovSrc.RecordCount > 1 )
  or (QrTXMovDst.RecordCount > 1 ) then
  begin
    Geral.MB_Aviso('O IME-I ' + Geral.FF0(Controle) +
    ' n�o pode ser exclu�do pois tem atrelamentos com um ou mais outros IME-Is!');
    Exit;
  end;
  if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(Controle,
  Integer(TEstqMotivDel.emtdWetCurti045),
  //SrcNivel2, DstNivel2,
  Dmod.QrUpd, Dmod.MyDB, EdSenha.Text) then
  begin
    LocCod(Controle, Controle);
  end;
end;

procedure TFmTXMovIts.CriaBaixaDeCaleado();
var
  ResVar: Variant;
  Controle, MovimID, MovimNiv, SrcNivel1, SrcNivel2, CtrlExiste: Integer;
  Qtde: Double;
  CamposZ: String;
  IsOk: Boolean;
  QtdFlds: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  ResVar := QrTXMovItsControle.Value + 1;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  ResVar, 0, 0, '', '', True, 'Troca IME-I Origem', 'Informe o novo IME-I de origem: ',
  0, ResVar) then
  begin
    Controle := Geral.IMV(ResVar);
    CtrlExiste := Dmod.MyDB.SelectInteger(
    'SELECT Controle FROM txmovits WHERE Controle=' + Geral.FF0(Controle), IsOk,
    'Controle');
    if IsOk and (CtrlExiste = Controle) then
      Geral.MB_Info('O IME-I ' + Geral.FF0(Controle) +
      ' j� existe e n�o pode ser criado!')
    else
    begin
      MovimID   := QrTXMovItsMovimID.Value;
      MovimNiv  := QrTXMovItsMovimNiv.Value;
      SrcNivel1 := QrTXMovItsSrcNivel1.Value;
      SrcNivel2 := QrTXMovItsSrcNivel2.Value;
      Qtde      := QrTXMovItsQtde.Value;
      //
      CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'txmovits', '', QtdFlds);
      CamposZ := StringReplace(CamposZ, ' Controle',
        ' ' + Geral.FF0(Controle) + ' Controle', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' MovimNiv',
        ' ' + Geral.FF0(Integer(eminEmCalBxa)) + ' MovimNiv', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' SrcNivel1',
        ' ' + Geral.FF0(SrcNivel1) + ' SrcNivel1', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' SrcNivel2',
        ' ' + Geral.FF0(SrcNivel2) + ' SrcNivel2', [rfReplaceAll]);
      CamposZ := StringReplace(CamposZ, ' Qtde',
        ' ' + Geral.FFT_Dot(-Qtde, 3, sinegativo) + ' Qtde', [rfReplaceAll]);
      //
      CamposZ := 'INSERT INTO txmovits SELECT ' + sLineBreak +
      CamposZ + sLineBreak +
      ' FROM txmovits '+ sLineBreak +
      'WHERE Controle=' + Geral.FF0(QrTXMovItsControle.Value);
      //
      //Geral.MB_Info(CamposZ);
      DModG.MyCompressDB.Execute(CamposZ);
      LocCod(QrTXMovItsControle.Value, Controle);
    end;
  end;
end;

procedure TFmTXMovIts.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXMovIts.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXMovIts.Recalcula1Click(Sender: TObject);
const
  Gera = False;
var
  Qtde, ValorT: Double;
  Controle: Integer;
begin
  // ver se tem classificados sem dar baixa!
// 2016-01-11
  //TX_PF.AtualizaSaldoIMEI(QrTXMovItsControle.Value, Gera);
  TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(QrTXMovItsControle.Value,
  QrTXMovItsMovimID.Value, QrTXMovItsMovimNiv.Value);
// FIM 2016-01-11
  LocCod(QrTXMovItsControle.Value, QrTXMovItsControle.Value);
end;

procedure TFmTXMovIts.Recriarbaixa1Click(Sender: TObject);
const
  sProcName = 'TFmTXMovIts.Recriarbaixa1Click()';
begin
  case IndiceIDNiv() of
    29031: CriaBaixaDeCaleado();
    else Geral.MB_ERRO('"ID+Niv" n�o implementado em ' + sProcName);
  end;
end;

procedure TFmTXMovIts.ReopenTXMovDst(Controle: Integer);
const
  TemIMEIMrt = 1; // ver o que fazer !!
begin
  TX_PF.ReopenTXMovXXX(QrTXMovDst, 'DstNivel2', QrTXMovItsControle.Value, TemIMEIMrt, Controle);
end;

procedure TFmTXMovIts.ReopenTXMovSrc(Controle: Integer);
const
  TemIMEIMrt = 1; // ver o que fazer !!
begin
  TX_PF.ReopenTXMovXXX(QrTXMovSrc, 'SrcNivel2', QrTXMovItsControle.Value, TemIMEIMrt, Controle);
end;

procedure TFmTXMovIts.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXMovIts.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXMovIts.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXMovIts.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXMovIts.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXMovIts.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXMovIts.AlteradadosdoartigodoIMEI1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
begin
  Controle := QrTXMovItsControle.Value;
  TX_Jan.MostraFormTXMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbDadosArtigo]);
  LocCod(Controle, Controle);
end;

procedure TFmTXMovIts.AlteradadosdoitemdeIMEI1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
begin
  Controle := QrTXMovItsControle.Value;
  TX_Jan.MostraFormTXMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  LocCod(Controle, Controle);
end;

procedure TFmTXMovIts.BtIMEIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIMEI, BtIMEI);
end;

procedure TFmTXMovIts.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXMovItsControle.Value;
  Close;
end;

procedure TFmTXMovIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBMovDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmTXMovIts.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXMovItsControle.Value, LaRegistro.Caption);
end;

procedure TFmTXMovIts.SbImprimeClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXMovIts.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTXMovIts.SbNovoClick(Sender: TObject);
const
  Codigo = 0;
  Ativo  = 1;
var
  QryAll, QryUni: TmySQLQuery;
  Controle, MovimID, MovimNiv, Status, IMEI: Integer;
  DataEmis: String;
  QtdeAnt, SdoVrtQtdAnt, QtdeDep, SdoVrtQtdDep: Double;
  Agora: TDateTime;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MB_Pergunta(
  'Confirma a atualiza��o dos saldos dos IME-Is ativos?') <> ID_YES then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    Agora := DModG.ObtemAgora;
    DataEmis := Geral.FDT(Agora, 109);
    VAR_NOME_TAB_VERIF_IMEI :=
      UnCreateTX.RecriaTempTableNovo(ntrttTXVerifIMEI, DModG.QrUpdPID1,
      False, 0, '_TXVerifIMEI_' + FormatDateTime('yymmddhhmmss', Agora));
    QryAll := TmySQLQuery.Create(Dmod);
    QryUni := TmySQLQuery.Create(Dmod);
    try
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QryAll, Dmod.MyDB, [
      'SELECT *  ',
      'FROM ' + CO_SEL_TAB_TMI,
      'ORDER BY Controle ',
      '']);
      PB1.Position := 0;
      PB1.Max := QryAll.RecordCount;
      QryAll.First;
      while not QryAll.Eof do
      begin
        Controle := QryAll.FieldByName('Controle').AsInteger;
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Verificando IMEI ' + Geral.FF0(Controle));
        Application.ProcessMessages;
        MovimID        := QryAll.FieldByName('MovimID').AsInteger;
        MovimNiv       := QryAll.FieldByName('MovimNiv').AsInteger;
        QtdeAnt        := QryAll.FieldByName('Qtde').AsFloat;
        SdoVrtQtdAnt   := QryAll.FieldByName('SdoVrtQtd').AsFloat;
        //
        TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(Controle, MovimID, MovimNiv);
        UnDmkDAC_PF.AbreMySQLQuery0(QryUni, Dmod.MyDB, [
        'SELECT *  ',
        'FROM ' + CO_SEL_TAB_TMI,
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        QtdeDep        := QryUni.FieldByName('Qtde').AsFloat;
        SdoVrtQtdDep   := QryUni.FieldByName('SdoVrtQtd').AsFloat;
        Status := 0;
        if QtdeAnt        <> QtdeDep       then Status := Status + 1;
        if SdoVrtQtdAnt   <> SdoVrtQtdDep  then Status := Status + 8;
        //
        if Status > 0 then
        begin
          IMEI := Controle;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, VAR_NOME_TAB_VERIF_IMEI,False, [
          'Codigo', 'DataEmis', 'IMEI',
          'Status', 'Ativo'], [
          ], [
          Codigo, DataEmis, IMEI,
          Status, Ativo], [
          ], False);
        end;
        //
        QryAll.Next;
      end;
      Geral.MB_Info('Verifica��o finalizada!');
      PB1.Position := 0;
      PB1.Max := 0;
    finally
      QryUni.Free;
    end;
    finally
      QryAll.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTXMovIts.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXMovIts.QrTXMovItsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXMovIts.QrTXMovItsAfterScroll(DataSet: TDataSet);
begin
  ReopenTXMovSrc(0);
  ReopenTXMovDst(0);
end;

procedure TFmTXMovIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
(*
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXMovItsControle??.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
*)
end;

procedure TFmTXMovIts.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTXMovItsControle.Value,
  CuringaLoc.CriaForm('Controle', CO_NOME, CO_SEL_TAB_TMI, Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTXMovIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmTXMovIts.IndiceIDNiv(): Integer;
begin
  Result := TX_PF.GetIDNiv(QrTXMovItsMovimID.Value, QrTXMovItsMovimNiv.Value);
end;

procedure TFmTXMovIts.IrparaajaneladoMovimento1Click(Sender: TObject);
begin
  MostraFormPorIMEI(QrTXMovItsSrcNivel2.Value);
end;

procedure TFmTXMovIts.IrparagerenciamentodoMovimento1Click(Sender: TObject);
begin
  MostraFormPorIMEI(QrTXMovItsControle.Value);
end;

procedure TFmTXMovIts.IrparajaneladaFichaRMP1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek!');
{POIU
  if (QrTXMovIts.State <> dsInactive) and (QrTXMovIts.RecordCount > 0) then
    TX_PF.MostraFormTXFchGerCab(QrTXMovItsSerieTal.Value, QrTXMovItsTalao.Value);
}
end;

procedure TFmTXMovIts.Irparajaneladopallet1Click(Sender: TObject);
begin
  if QrTXMovItsPallet.Value <> 0 then
    TX_Jan.MostraFormTXPallet(QrTXMovItsPallet.Value)
  else
    Geral.MB_Aviso('Este IME-I n�o pertence a nenhum pallet!');
end;

procedure TFmTXMovIts.QrTXMovItsBeforeClose(
  DataSet: TDataSet);
begin
  QrTXMovSrc.Close;
  QrTXMovDst.Close;
end;

procedure TFmTXMovIts.QrTXMovItsBeforeOpen(DataSet: TDataSet);
begin
  QrTXMovItsControle.DisplayFormat := FFormatFloat;
end;

procedure TFmTXMovIts.QrTXMovItsCalcFields(DataSet: TDataSet);
begin
  QrTXMovItsNO_EstqMovimID.Value := sEstqMovimID[QrTXMovItsMovimID.Value];
  QrTXMovItsNO_SrcMovID.Value    := sEstqMovimID[QrTXMovItsSrcMovID.Value];
  QrTXMovItsNO_DstMovID.Value    := sEstqMovimID[QrTXMovItsDstMovID.Value];
end;

end.

