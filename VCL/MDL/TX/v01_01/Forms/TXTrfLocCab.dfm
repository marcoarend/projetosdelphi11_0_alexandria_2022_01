object FmTXTrfLocCab: TFmTXTrfLocCab
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-131 :: Transfer'#234'ncia de Local de Estoque TX'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 91
        Height = 13
        Caption = 'Data / hora venda:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 97
        Height = 13
        Caption = 'Data / hora entrega:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 42
        Height = 13
        Caption = 'Terceiro:'
      end
      object Label5: TLabel
        Left = 504
        Top = 56
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object LaQtde: TLabel
        Left = 16
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label28: TLabel
        Left = 104
        Top = 96
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label29: TLabel
        Left = 136
        Top = 96
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object LaCliente: TLabel
        Left = 72
        Top = 56
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtVenda: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtVenda'
        UpdCampo = 'DtVenda'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtVenda: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtVenda'
        UpdCampo = 'DtVenda'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'FimVisPrv'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrega: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrega'
        UpdCampo = 'DtEntrega'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtEntrega: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrega'
        UpdCampo = 'DtEntrega'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 428
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 11
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 428
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 13
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdQtde: TdmkEdit
        Left = 16
        Top = 112
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Edide_serie: TdmkEdit
        Left = 104
        Top = 112
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 136
        Top = 112
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 476
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora fatura:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 42
        Height = 13
        Caption = 'Terceiro:'
      end
      object Label15: TLabel
        Left = 504
        Top = 56
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label16: TLabel
        Left = 16
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label20: TLabel
        Left = 368
        Top = 96
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label21: TLabel
        Left = 400
        Top = 96
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXTrfLocCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXTrfLocCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 341
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXTrfLocCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 476
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtVenda'
        DataSource = DsTXTrfLocCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 628
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsTXTrfLocCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 780
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtEntrega'
        DataSource = DsTXTrfLocCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsTXTrfLocCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsTXTrfLocCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 72
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_CLIENTE'
        DataSource = DsTXTrfLocCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsTXTrfLocCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsTXTrfLocCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 16
        Top = 112
        Width = 84
        Height = 21
        DataField = 'Qtde'
        DataSource = DsTXTrfLocCab
        TabOrder = 11
      end
      object DBEdit15: TDBEdit
        Left = 368
        Top = 112
        Width = 28
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsTXTrfLocCab
        TabOrder = 12
      end
      object DBEdit16: TDBEdit
        Left = 400
        Top = 112
        Width = 64
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsTXTrfLocCab
        TabOrder = 13
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 86
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 260
        Top = 15
        Width = 746
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 613
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtTXOutNFeCab: TBitBtn
          Tag = 456
          Left = 248
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFe Cab'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtTXOutNFeCabClick
        end
        object BtTribIncIts: TBitBtn
          Tag = 502
          Left = 492
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtTribIncItsClick
        end
        object BtTXOutNFeIts: TBitBtn
          Tag = 575
          Left = 370
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'N&Fe Itm'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtTXOutNFeItsClick
        end
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 137
      Width = 1008
      Height = 228
      Align = alTop
      Caption = ' Itens da sa'#237'da: '
      TabOrder = 2
      object PCItens: TPageControl
        Left = 2
        Top = 15
        Width = 1004
        Height = 211
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Por Pallet'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            DataSource = DsPallets
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEQ'
                Title.Caption = 'Item'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXT_ARTIGO'
                Title.Caption = 'Artigo'
                Width = 280
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ClasseImp'
                Title.Caption = 'Classe'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Pe'#231'as'
                Width = 100
                Visible = True
              end>
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Por IME-I'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label9: TLabel
            Left = 0
            Top = 111
            Width = 140
            Height = 13
            Align = alBottom
            Caption = 'Baixas do IME-I selecionado: '
          end
          object DGDados: TDBGrid
            Left = 0
            Top = 0
            Width = 996
            Height = 111
            Align = alClient
            DataSource = DsIMEIDest
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DGDadosDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxMovIX'
                Title.Caption = 'IXX'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxFolha'
                Title.Caption = 'Folha'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IxxLinha'
                Title.Caption = 'Lin'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ItemNFe'
                Title.Caption = 'NFi'
                Width = 22
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Talao'
                Title.Caption = 'Ficha RMP'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Title.Caption = 'ID Pallet'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Pallet'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMP'
                Title.Caption = 'Valor MP'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtAvuls'
                Title.Caption = 'Frete simples '
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtMOEnv'
                Title.Caption = 'Frete ida'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtMORet'
                Title.Caption = 'Frete volta'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor total'
                Width = 68
                Visible = True
              end>
          end
          object DBGrid3: TDBGrid
            Left = 0
            Top = 124
            Width = 996
            Height = 59
            Align = alBottom
            DataSource = DsIMEISrc
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Width = 72
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Itens NF'
          ImageIndex = 2
          object DBGrid2: TDBGrid
            Left = 205
            Top = 0
            Width = 569
            Height = 183
            Align = alClient
            DataSource = DsTXOutNFeIts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ItemNFe'
                Title.Caption = 'NFi'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigo / tamanho / cor'
                Width = 254
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorU'
                Title.Caption = 'Valor Unit.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor Total'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
          object GroupBox3: TGroupBox
            Left = 774
            Top = 0
            Width = 222
            Height = 183
            Align = alRight
            Caption = ' Impostos do Item: '
            TabOrder = 1
            object DBGrid4: TDBGrid
              Left = 2
              Top = 15
              Width = 218
              Height = 166
              Align = alClient
              DataSource = DsTribIncIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_Tributo'
                  Title.Caption = 'Tributo'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Percent'
                  Title.Caption = '%'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrTrib'
                  Title.Caption = 'Valor '
                  Width = 72
                  Visible = True
                end>
            end
          end
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 205
            Height = 183
            Align = alLeft
            DataSource = DsTXOutNFeCab
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_serie'
                Title.Caption = 'S'#233'rie'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_nNF'
                Title.Caption = 'N'#186' NFe'
                Visible = True
              end>
          end
        end
        object TsFrCompr: TTabSheet
          Caption = 'Dados frete simples envio'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PnFrCompr: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGTXMOEnvAvu: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 529
              Height = 183
              Align = alLeft
              DataSource = DsTXMOEnvAvu
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_nCT'
                  Title.Caption = 'N'#186' CT'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_Qtde'
                  Title.Caption = 'Qtde'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMA_ValorT'
                  Title.Caption = 'Valor frete'
                  Width = 72
                  Visible = True
                end>
            end
            object DBGTXMOEnvATMI: TdmkDBGridZTO
              Left = 529
              Top = 0
              Width = 467
              Height = 183
              Align = alClient
              DataSource = DsTXMOEnvATMI
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TXMovIts'
                  Title.Caption = 'IME-I'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorFrete'
                  Title.Caption = '$ Frete'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Width = 72
                  Visible = True
                end>
            end
          end
        end
        object TsEnvioMO: TTabSheet
          Caption = ' Dados do envio para retorno'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PnEnvioMO: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGTXMOEnvEnv: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 529
              Height = 183
              Align = alLeft
              DataSource = DsTXMOEnvEnv
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFEMP_nNF'
                  Title.Caption = 'N'#186' NF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_nCT'
                  Title.Caption = 'N'#186' CT'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_Qtde'
                  Title.Caption = 'Qtde'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTMP_ValorT'
                  Title.Caption = 'Valor frete'
                  Width = 72
                  Visible = True
                end>
            end
            object DBGTXMOEnvETmi: TdmkDBGridZTO
              Left = 529
              Top = 0
              Width = 467
              Height = 183
              Align = alClient
              DataSource = DsTXMOEnvETMI
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TXMovIts'
                  Title.Caption = 'IME-I'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorFrete'
                  Title.Caption = '$ Frete'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Width = 72
                  Visible = True
                end>
            end
          end
        end
        object TsRetornoMO: TTabSheet
          Caption = ' Retorno do envio'
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PnRetornoMO: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 183
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGTXMOEnvRet: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 661
              Height = 183
              Align = alLeft
              DataSource = DsTXMOEnvRet
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_nCT'
                  Title.Caption = 'N'#186' CT'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_ValorT'
                  Title.Caption = 'Valor frete'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CFTPA_Qtde'
                  Title.Caption = 'Qtde'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFRMP_SerNF'
                  Title.Caption = 'S'#233'r. Ret'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFRMP_nNF'
                  Title.Caption = 'NF Retorno'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFCMO_SerNF'
                  Title.Caption = 'S'#233'r. M.O.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NFCMO_nNF'
                  Title.Caption = 'NF M.O.'
                  Visible = True
                end>
            end
            object PnItensRetMO: TPanel
              Left = 661
              Top = 0
              Width = 335
              Height = 183
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Splitter2: TSplitter
                Left = 0
                Top = 68
                Width = 335
                Height = 5
                Cursor = crVSplit
                Align = alBottom
                ExplicitLeft = 585
                ExplicitTop = 1
                ExplicitWidth = 208
              end
              object GroupBox8: TGroupBox
                Left = 0
                Top = 0
                Width = 335
                Height = 68
                Align = alClient
                Caption = ' Itens de NFes enviados para M.O.: '
                TabOrder = 0
                object DBGTXMOEnvRTmi: TdmkDBGridZTO
                  Left = 2
                  Top = 15
                  Width = 331
                  Height = 51
                  TabStop = False
                  Align = alClient
                  DataSource = DsTXMOEnvRTmi
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NFEMP_SerNF'
                      Title.Caption = 'S'#233'r. NF'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFEMP_nNF'
                      Title.Caption = 'NF'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Qtde'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorT'
                      Title.Caption = 'Valor Total'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorFrete'
                      Title.Caption = 'Valor Frete'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'ID Item'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TXMOEnvEnv'
                      Title.Caption = 'ID Envio'
                      Visible = True
                    end>
                end
              end
              object GroupBox9: TGroupBox
                Left = 0
                Top = 73
                Width = 335
                Height = 110
                Align = alBottom
                Caption = ' IME-Is de couros prontos retornados: '
                TabOrder = 1
                object DBGTXMOEnvGTmi: TdmkDBGridZTO
                  Left = 2
                  Top = 15
                  Width = 331
                  Height = 93
                  TabStop = False
                  Align = alClient
                  DataSource = DsTXMOEnvGTmi
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'ID Item'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TXMovIts'
                      Title.Caption = 'IME-I'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Qtde'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorFrete'
                      Visible = True
                    end>
                end
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 462
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 462
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 462
        Height = 32
        Caption = 'Transfer'#234'ncia de Local de Estoque TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 64
    Top = 56
  end
  object QrTXTrfLocCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXTrfLocCabBeforeOpen
    AfterOpen = QrTXTrfLocCabAfterOpen
    BeforeClose = QrTXTrfLocCabBeforeClose
    AfterScroll = QrTXTrfLocCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA'
      'FROM txinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.transporta')
    Left = 312
    Top = 293
    object QrTXTrfLocCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXTrfLocCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXTrfLocCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXTrfLocCabDtVenda: TDateTimeField
      FieldName = 'DtVenda'
    end
    object QrTXTrfLocCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrTXTrfLocCabDtEntrega: TDateTimeField
      FieldName = 'DtEntrega'
    end
    object QrTXTrfLocCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrTXTrfLocCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrTXTrfLocCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrTXTrfLocCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXTrfLocCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXTrfLocCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXTrfLocCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXTrfLocCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXTrfLocCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXTrfLocCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXTrfLocCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXTrfLocCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrTXTrfLocCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrTXTrfLocCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrTXTrfLocCabide_serie: TSmallintField
      FieldName = 'ide_serie'
    end
    object QrTXTrfLocCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
  end
  object DsTXTrfLocCab: TDataSource
    DataSet = QrTXTrfLocCab
    Left = 312
    Top = 337
  end
  object QrIMEIDest: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrIMEIDestAfterOpen
    BeforeClose = QrIMEIDestBeforeClose
    AfterScroll = QrIMEIDestAfterScroll
    SQL.Strings = (
      'SELECT tmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM txmovits tmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=tmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY tmi.Controle')
    Left = 396
    Top = 293
    object QrIMEIDestCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEIDestControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEIDestMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEIDestMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEIDestMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEIDestEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEIDestTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEIDestCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEIDestMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEIDestDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEIDestPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEIDestGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEIDestQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIDestValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEIDestSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEIDestSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEIDestSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEIDestSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrIMEIDestObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEIDestSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrIMEIDestTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrIMEIDestFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEIDestCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIDestDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEIDestDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEIDestDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEIDestDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEIDestQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrIMEIDestQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrIMEIDestPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrIMEIDestMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEIDestStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIDestNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrIMEIDestNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEIDestNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIDestNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEIDestID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEIDestReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIDestIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrIMEIDestIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrIMEIDestIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
    object QrIMEIDestCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrIMEIDestCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrIMEIDestCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsIMEIDest: TDataSource
    DataSet = QrIMEIDest
    Left = 396
    Top = 337
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 416
    Top = 568
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      object PorPallet1: TMenuItem
        Caption = 'Por &Pallet'
        OnClick = PorPallet1Click
      end
      object PorIMEI1: TMenuItem
        Caption = 'Por &IME-I'
        OnClick = PorIMEI1Click
      end
      object Porkgsubproduto1: TMenuItem
        Caption = 'Por kg (sub produto)'
        Visible = False
        OnClick = Porkgsubproduto1Click
      end
    end
    object ItsExcluiIMEI1: TMenuItem
      Caption = '&Remove o IME-I'
      Enabled = False
      OnClick = ItsExcluiIMEI1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object AtrelamentoFreteCompra1: TMenuItem
      Caption = 'Atrelamento Frete simples envio'
      object IncluiAtrelamento3: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento3Click
      end
      object AlteraAtrelamento3: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento3Click
      end
      object ExcluiAtrelamento3: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento3Click
      end
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de envio (para posterior retorno)'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      OnClick = InformaNmerodaRME1Click
    end
    object InformaIEC1: TMenuItem
      Caption = 'Informa P'#225'gina e linha de IEC'
      OnClick = InformaIEC1Click
    end
    object AlteraLocaldeestoque1: TMenuItem
      Caption = 'Altera &Local de estoque'
      OnClick = AlteraLocaldeestoque1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 288
    Top = 576
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
      Visible = False
    end
    object CorrigeFornecedores1: TMenuItem
      Caption = 'Corrige Fornecedores'
      Visible = False
      OnClick = CorrigeFornecedores1Click
    end
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF'
      'FROM entidades ent'
      'ORDER BY NOMEENTIDADE')
    Left = 112
    Top = 20
    object QrClienteCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 112
    Top = 64
  end
  object QrTransporta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 180
    Top = 20
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 180
    Top = 64
  end
  object PMImprime: TPopupMenu
    Left = 12
    object PackingList1: TMenuItem
      Caption = 'Packing List'
      OnClick = PackingList1Click
    end
  end
  object QrPallets: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPalletsCalcFields
    Left = 60
    Top = 292
    object QrPalletsPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrPalletsGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrPalletsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPalletsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletsSerieTal: TLargeintField
      FieldName = 'SerieTal'
    end
    object QrPalletsTalao: TLargeintField
      FieldName = 'Talao'
    end
    object QrPalletsNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrPalletsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrPalletsArtigoImp: TWideStringField
      FieldName = 'ArtigoImp'
      Size = 50
    end
    object QrPalletsClasseImp: TWideStringField
      FieldName = 'ClasseImp'
      Size = 30
    end
    object QrPalletsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPalletsTXT_ARTIGO: TWideStringField
      FieldName = 'TXT_ARTIGO'
      Size = 512
    end
  end
  object DsPallets: TDataSource
    DataSet = QrPallets
    Left = 60
    Top = 340
  end
  object frxDsPallets: TfrxDBDataset
    UserName = 'frxDsPallets'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ArtigoImp=ArtigoImp'
      'ClasseImp=ClasseImp'
      'Pallet=Pallet'
      'Qtde=Qtde'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'SEQ=SEQ'
      'MediaM2=MediaM2'
      'FaixaMediaM2=FaixaMediaM2'
      'TXT_ARTIGO=TXT_ARTIGO')
    DataSet = QrPallets
    BCDToCurrency = False
    Left = 60
    Top = 388
  end
  object frxTEX_FAXAO_019_00_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxTEX_FAXAO_019_00_AGetValue
    Left = 60
    Top = 440
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end
      item
        DataSet = frxDsTXOutNFeCab
        DataSetName = 'frxDsTXOutNFeCab'
      end
      item
        DataSet = frxDsTXOutNFeIts
        DataSetName = 'frxDsTXOutNFeIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 34.015770000000010000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 34.015770000000010000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List de Transfer'#234'ncia N'#186' [VARF_CODIGO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 132.283550000000000000
          Width = 343.937034720000000000
          Height = 22.677165350000000000
          DataField = 'TXT_ARTIGO'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."TXT_ARTIGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 45.354360000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          DataField = 'Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValQtde: TfrxMemoView
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsPallets."Qtde">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 476.220780000000000000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          DataField = 'ClasseImp'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."ClasseImp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'SEQ'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."SEQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 653.858690000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        object MeTitNome: TfrxMemoView
          Left = 132.283550000000000000
          Width = 343.937034720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 45.354360000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitQtde: TfrxMemoView
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 476.220780000000000000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Width = 589.606484720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  [COUNT(MD001)] Pallets  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 589.606680000000000000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsPallets."Qtde">,MD001,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 45.354345350000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTXOutNFeCab
        DataSetName = 'frxDsTXOutNFeCab'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 45.354360000000000000
          Width = 143.622096060000000000
          Height = 22.677165350000000000
          DataField = 'ide_nNF'
          DataSet = frxDsTXOutNFeCab
          DataSetName = 'frxDsTXOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXOutNFeCab."ide_nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'ide_serie'
          DataSet = frxDsTXOutNFeCab
          DataSetName = 'frxDsTXOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXOutNFeCab."ide_serie"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 120.944960000000000000
          Top = 22.677180000000020000
          Width = 445.984344720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataSet = frxDsTXOutNFeCab
          DataSetName = 'frxDsTXOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 45.354360000000000000
          Top = 22.677180000000020000
          Width = 75.590570710000000000
          Height = 22.677165350000000000
          DataSet = frxDsTXOutNFeCab
          DataSetName = 'frxDsTXOutNFeCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 566.929499999999900000
          Top = 22.677180000000020000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DD002A: TfrxDetailData
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTXOutNFeIts
        DataSetName = 'frxDsTXOutNFeIts'
        RowCount = 0
        object Memo12: TfrxMemoView
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'ItemNFe'
          DataSet = frxDsTXOutNFeIts
          DataSetName = 'frxDsTXOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXOutNFeIts."ItemNFe"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 120.944960000000000000
          Width = 445.984344720000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsTXOutNFeIts
          DataSetName = 'frxDsTXOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXOutNFeIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 45.354360000000000000
          Width = 75.590570710000000000
          Height = 22.677165350000000000
          DataField = 'GraGruX'
          DataSet = frxDsTXOutNFeIts
          DataSetName = 'frxDsTXOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXOutNFeIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 566.929499999999900000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          DataSet = frxDsTXOutNFeIts
          DataSetName = 'frxDsTXOutNFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXOutNFeIts."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Top = 3.779530000000022000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-es')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 45.354360000000000000
          Top = 26.456709999999990000
          Width = 143.622096060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero NF-e')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Top = 26.456709999999990000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 476.220780000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          Top = 3.779530000000022000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_SUB_TIT]')
          ParentFont = False
        end
        object MeOldC3L0: TfrxMemoView
          Left = 94.488250000000000000
          Top = 22.574830000000020000
          Width = 192.755907950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
        end
        object MeOldC4L0: TfrxMemoView
          Left = 287.244280000000000000
          Top = 22.574830000000020000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object MeOldC5L0: TfrxMemoView
          Left = 370.393940000000000000
          Top = 22.574830000000020000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object MeOldC6L0: TfrxMemoView
          Left = 578.268090000000000000
          Top = 22.574830000000020000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde Pronto')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Top = 22.574830000000020000
          Width = 94.488127950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 472.441250000000000000
          Top = 22.677180000000020000
          Width = 105.826774090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        Height = 26.456692910000000000
        Top = 540.472790000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo51: TfrxMemoView
          Left = 472.441250000000000000
          Width = 105.826774090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC3L1: TfrxMemoView
          Left = 94.488250000000000000
          Width = 192.755907950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC4L1: TfrxMemoView
          Left = 287.244280000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC5L1: TfrxMemoView
          Left = 370.393940000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC6L1: TfrxMemoView
          Left = 578.268090000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 589.606680000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrTXOutNFeIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXOutNFeItsBeforeClose
    AfterScroll = QrTXOutNFeItsAfterScroll
    SQL.Strings = (
      'SELECT CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, voi.* '
      'FROM txoutnfi voi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE voi.Codigo=0')
    Left = 384
    Top = 432
    object QrTXOutNFeItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXOutNFeItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXOutNFeItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTXOutNFeItsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrTXOutNFeItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXOutNFeItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTXOutNFeItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTXOutNFeItsTpCalcVal: TSmallintField
      FieldName = 'TpCalcVal'
    end
    object QrTXOutNFeItsValorU: TFloatField
      FieldName = 'ValorU'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrTXOutNFeItsTribDefSel: TIntegerField
      FieldName = 'TribDefSel'
    end
    object QrTXOutNFeItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXOutNFeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXOutNFeItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXOutNFeItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXOutNFeItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXOutNFeItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXOutNFeItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTXOutNFeIts: TDataSource
    DataSet = QrTXOutNFeIts
    Left = 384
    Top = 480
  end
  object PMTXOutNFeIts: TPopupMenu
    OnPopup = PMTXOutNFeItsPopup
    Left = 672
    Top = 564
    object IncluiitemdeNF1: TMenuItem
      Caption = '&Inclui item de NF'
      OnClick = IncluiitemdeNF1Click
    end
    object AlteraitemdeNF1: TMenuItem
      Caption = '&Altera item de NF'
      OnClick = AlteraitemdeNF1Click
    end
    object ExcluiitemdeNF1: TMenuItem
      Caption = '&Exclui item de NF'
      OnClick = ExcluiitemdeNF1Click
    end
  end
  object QrTribIncIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tii.*, tdd.Nome NO_Tributo  '
      'FROM tribincits tii '
      'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo'
      'WHERE tii.FatID=1003 '
      'AND tii.FatNum=14 '
      'AND tii.FatParcela=851 '
      'ORDER BY tii.Controle ')
    Left = 556
    Top = 296
    object QrTribIncItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrTribIncItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrTribIncItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrTribIncItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTribIncItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTribIncItsData: TDateField
      FieldName = 'Data'
    end
    object QrTribIncItsHora: TTimeField
      FieldName = 'Hora'
    end
    object QrTribIncItsValorFat: TFloatField
      FieldName = 'ValorFat'
    end
    object QrTribIncItsBaseCalc: TFloatField
      FieldName = 'BaseCalc'
    end
    object QrTribIncItsValrTrib: TFloatField
      FieldName = 'ValrTrib'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrTribIncItsPercent: TFloatField
      FieldName = 'Percent'
      DisplayFormat = '0.00'
    end
    object QrTribIncItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTribIncItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTribIncItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTribIncItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTribIncItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTribIncItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTribIncItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTribIncItsTributo: TIntegerField
      FieldName = 'Tributo'
    end
    object QrTribIncItsVUsoCalc: TFloatField
      FieldName = 'VUsoCalc'
    end
    object QrTribIncItsOperacao: TSmallintField
      FieldName = 'Operacao'
    end
    object QrTribIncItsNO_Tributo: TWideStringField
      FieldName = 'NO_Tributo'
      Size = 60
    end
    object QrTribIncItsFatorDC: TSmallintField
      FieldName = 'FatorDC'
    end
  end
  object DsTribIncIts: TDataSource
    DataSet = QrTribIncIts
    Left = 556
    Top = 344
  end
  object PMTribIncIts: TPopupMenu
    Left = 796
    Top = 564
    object IncluiTributo1: TMenuItem
      Caption = '&Inclui Tributo'
    end
    object AlteraTributoAtual1: TMenuItem
      Caption = '&Altera Tributo Atual'
      OnClick = AlteraTributoAtual1Click
    end
    object ExcluiTributoAtual1: TMenuItem
      Caption = '&Exclui Tributo Atual'
      OnClick = ExcluiTributoAtual1Click
    end
  end
  object QrIMEISorc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM txmovits tmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=tmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY tmi.Controle')
    Left = 476
    Top = 293
    object QrIMEISorcCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEISorcControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEISorcMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEISorcMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEISorcMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEISorcEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEISorcTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEISorcCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEISorcMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEISorcDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEISorcPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEISorcGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEISorcQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEISorcValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEISorcSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEISorcSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEISorcSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEISorcSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrIMEISorcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEISorcSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrIMEISorcTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrIMEISorcFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEISorcCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEISorcDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEISorcDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEISorcDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEISorcDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEISorcQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrIMEISorcQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrIMEISorcPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrIMEISorcMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEISorcStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEISorcNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrIMEISorcNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEISorcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEISorcNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEISorcID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEISorcReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsIMEISrc: TDataSource
    DataSet = QrIMEISorc
    Left = 476
    Top = 337
  end
  object QrTXOutNFeCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXOutNFeCabBeforeClose
    AfterScroll = QrTXOutNFeCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM txoutnfecab'
      'WHERE MovimCod=:P0')
    Left = 276
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXOutNFeCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXOutNFeCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXOutNFeCabide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrTXOutNFeCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrTXOutNFeCabOriCod: TIntegerField
      FieldName = 'OriCod'
    end
    object QrTXOutNFeCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXOutNFeCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXOutNFeCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXOutNFeCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXOutNFeCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXOutNFeCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXOutNFeCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTXOutNFeCab: TDataSource
    DataSet = QrTXOutNFeCab
    Left = 276
    Top = 480
  end
  object PMTXOutNFeCab: TPopupMenu
    OnPopup = PMTXOutNFeCabPopup
    Left = 544
    Top = 556
    object IncluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Inclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = IncluiSerieNumeroDeNFe1Click
    end
    object AlteraSerieNumeroDeNFe1: TMenuItem
      Caption = 'Aterai s'#233'rie/ n'#250'mero de NFe'
      OnClick = AlteraSerieNumeroDeNFe1Click
    end
    object ExcluiSerieNumeroDeNFe1: TMenuItem
      Caption = 'Exclui s'#233'rie/ n'#250'mero de NFe'
      OnClick = ExcluiSerieNumeroDeNFe1Click
    end
  end
  object frxDsTXOutNFeCab: TfrxDBDataset
    UserName = 'frxDsTXOutNFeCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'OriCod=OriCod'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrTXOutNFeCab
    BCDToCurrency = False
    Left = 276
    Top = 528
  end
  object frxDsTXOutNFeIts: TfrxDBDataset
    UserName = 'frxDsTXOutNFeIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Codigo=Codigo'
      'Controle=Controle'
      'ItemNFe=ItemNFe'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'ValorT=ValorT'
      'TpCalcVal=TpCalcVal'
      'ValorU=ValorU'
      'TribDefSel=TribDefSel'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrTXOutNFeIts
    BCDToCurrency = False
    Left = 384
    Top = 528
  end
  object QrTXMOEnvAvu: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXMOEnvAvuBeforeClose
    AfterScroll = QrTXMOEnvAvuAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM txmoenvenv cmo'
      'WHERE cmo.TXTMI_Controle=:P0')
    Left = 660
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvAvuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvAvuCFTMA_FatID: TIntegerField
      FieldName = 'CFTMA_FatID'
    end
    object QrTXMOEnvAvuCFTMA_FatNum: TIntegerField
      FieldName = 'CFTMA_FatNum'
    end
    object QrTXMOEnvAvuCFTMA_Empresa: TIntegerField
      FieldName = 'CFTMA_Empresa'
    end
    object QrTXMOEnvAvuCFTMA_Terceiro: TIntegerField
      FieldName = 'CFTMA_Terceiro'
    end
    object QrTXMOEnvAvuCFTMA_nItem: TIntegerField
      FieldName = 'CFTMA_nItem'
    end
    object QrTXMOEnvAvuCFTMA_SerCT: TIntegerField
      FieldName = 'CFTMA_SerCT'
    end
    object QrTXMOEnvAvuCFTMA_nCT: TIntegerField
      FieldName = 'CFTMA_nCT'
    end
    object QrTXMOEnvAvuCFTMA_Qtde: TFloatField
      FieldName = 'CFTMA_Qtde'
    end
    object QrTXMOEnvAvuCFTMA_PesTrKg: TFloatField
      FieldName = 'CFTMA_PesTrKg'
    end
    object QrTXMOEnvAvuCFTMA_CusTrKg: TFloatField
      FieldName = 'CFTMA_CusTrKg'
    end
    object QrTXMOEnvAvuCFTMA_ValorT: TFloatField
      FieldName = 'CFTMA_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvAvuTXTMI_MovimCod: TIntegerField
      FieldName = 'TXTMI_MovimCod'
    end
    object QrTXMOEnvAvuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvAvuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvAvuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvAvuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvAvuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvAvuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvAvuAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvAvuAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvAvuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTXMOEnvAvu: TDataSource
    DataSet = QrTXMOEnvAvu
    Left = 660
    Top = 384
  end
  object QrTXMOEnvATMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM txmoenvevmi'
      'WHERE TXMOEnvEnv=:p0')
    Left = 660
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvATMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvATMITXMOEnvAvu: TIntegerField
      FieldName = 'TXMOEnvAvu'
    end
    object QrTXMOEnvATMITXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrTXMOEnvATMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvATMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvATMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvATMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvATMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvATMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvATMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvATMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvATMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvATMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvATMIQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0'
    end
  end
  object DsTXMOEnvATMI: TDataSource
    DataSet = QrTXMOEnvATMI
    Left = 660
    Top = 484
  end
  object QrTXMOEnvEnv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXMOEnvEnvBeforeClose
    AfterScroll = QrTXMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM txmoenvenv cmo'
      'WHERE cmo.TXTMI_Controle=:P0')
    Left = 600
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrTXMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrTXMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrTXMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrTXMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrTXMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrTXMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrTXMOEnvEnvNFEMP_Qtde: TFloatField
      FieldName = 'NFEMP_Qtde'
    end
    object QrTXMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrTXMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrTXMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrTXMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrTXMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrTXMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrTXMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrTXMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrTXMOEnvEnvCFTMP_Qtde: TFloatField
      FieldName = 'CFTMP_Qtde'
    end
    object QrTXMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrTXMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrTXMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvEnvTXTMI_MovimCod: TIntegerField
      FieldName = 'TXTMI_MovimCod'
    end
    object QrTXMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTXMOEnvEnv: TDataSource
    DataSet = QrTXMOEnvEnv
    Left = 600
    Top = 56
  end
  object QrTXMOEnvETMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM txmoenvevmi'
      'WHERE TXMOEnvEnv=:p0')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvETMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvETMITXMOEnvEnv: TIntegerField
      FieldName = 'TXMOEnvEnv'
    end
    object QrTXMOEnvETMITXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrTXMOEnvETMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvETMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvETMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvETMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvETMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvETMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvETMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvETMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvETMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvETMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvETMIQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0'
    end
  end
  object DsTXMOEnvETMI: TDataSource
    DataSet = QrTXMOEnvETMI
    Left = 684
    Top = 56
  end
  object QrTXMOEnvRet: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXMOEnvRetBeforeClose
    AfterScroll = QrTXMOEnvRetAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM txmoenvret cmo'
      'WHERE cmo.TXTMI_Controle=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrTXMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrTXMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrTXMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrTXMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrTXMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrTXMOEnvRetNFCMO_Qtde: TFloatField
      FieldName = 'NFCMO_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetNFCMO_CusMOUni: TFloatField
      FieldName = 'NFCMO_CusMOUni'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object QrTXMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrTXMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrTXMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrTXMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrTXMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrTXMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrTXMOEnvRetNFRMP_Qtde: TFloatField
      FieldName = 'NFRMP_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object QrTXMOEnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object QrTXMOEnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object QrTXMOEnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object QrTXMOEnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object QrTXMOEnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object QrTXMOEnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object QrTXMOEnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object QrTXMOEnvRetCFTPA_Qtde: TFloatField
      FieldName = 'CFTPA_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTXMOEnvRet: TDataSource
    DataSet = QrTXMOEnvRet
    Left = 768
    Top = 56
  end
  object QrTXMOEnvRTmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, mev.* '
      'FROM txmoenvrvmi mev'
      'LEFT JOIN txmoenvenv vee ON vee.Codigo=mev.TXMOEnvEnv')
    Left = 852
    Top = 8
    object QrTXMOEnvRTmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvRTmiTXMOEnvRet: TIntegerField
      FieldName = 'TXMOEnvRet'
    end
    object QrTXMOEnvRTmiTXMOEnvEnv: TIntegerField
      FieldName = 'TXMOEnvEnv'
    end
    object QrTXMOEnvRTmiQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRTmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRTmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvRTmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvRTmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvRTmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvRTmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvRTmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvRTmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvRTmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvRTmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvRTmiNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrTXMOEnvRTmiNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrTXMOEnvRTmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTXMOEnvRTmi: TDataSource
    DataSet = QrTXMOEnvRTmi
    Left = 852
    Top = 56
  end
  object QrTXMOEnvGTmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM txmoenvgvmi')
    Left = 940
    Top = 8
    object QrTXMOEnvGTmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvGTmiTXMOEnvRet: TIntegerField
      FieldName = 'TXMOEnvRet'
    end
    object QrTXMOEnvGTmiTXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrTXMOEnvGTmiQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvGTmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvGTmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvGTmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvGTmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvGTmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvGTmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvGTmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvGTmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvGTmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvGTmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvGTmiTXMovimCod: TIntegerField
      FieldName = 'TXMovimCod'
    end
  end
  object DsTXMOEnvGTmi: TDataSource
    DataSet = QrTXMOEnvGTmi
    Left = 940
    Top = 56
  end
end
