object DmModTX_CRC: TDmModTX_CRC
  OnCreate = DataModuleCreate
  Height = 855
  Width = 1020
  PixelsPerInch = 96
  object QrLocMFC: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 304
    Top = 4
    object QrLocMFCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLocMFI: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 304
    Top = 52
    object QrLocMFIControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = DModG.MyCompressDB
    Left = 360
    Top = 444
  end
  object DqAu2: TMySQLDirectQuery
    Database = DModG.MyCompressDB
    Left = 360
    Top = 492
  end
  object QrSrcNiv2: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 116
    Top = 376
  end
  object QrUniFrn: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 564
    Top = 4
    object QrUniFrnTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrUniFrnTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrUniFrnQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrMulFrn: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 564
    Top = 52
    object QrMulFrnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrMulFrnQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrMulFrnSiglaTX: TWideStringField
      FieldName = 'SiglaTX'
      Size = 10
    end
  end
  object QrTotFrn: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 564
    Top = 100
    object QrTotFrnQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrMFSrc: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 568
    Top = 196
    object QrMFSrcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMFSrcMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrMFSrcMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrMFSrcMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrBoxesPal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle TXPaRclIts,'
      '0.000 TXPallet, pri.TMI_Dest, 7.000 CacID'
      'FROM txpaclaitsa pri'
      'LEFT JOIN txpaclacaba prc ON prc.Codigo=pri.Codigo'
      'LEFT JOIN txcacitsa cia ON pri.Controle=cia.TXPaRclIts'
      'WHERE pri.TXPallet=?'
      ''
      'UNION'
      ''
      'SELECT DISTINCT prc.Codigo, prc.CacCod, pri.Controle TXPaRclIts,'
      'prc.TXPallet + 0.000 TXPallet, pri.TMI_Dest, 8.000 CacID'
      'FROM txparclitsa pri'
      'LEFT JOIN txparclcaba prc ON prc.Codigo=pri.Codigo'
      'LEFT JOIN txcacitsa cia ON pri.Controle=cia.TXPaRclIts'
      'WHERE pri.TXPallet=?')
    Left = 24
    Top = 552
    object QrBoxesPalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBoxesPalCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrBoxesPalTXPaXXXIts: TIntegerField
      FieldName = 'TXPaXXXIts'
      Required = True
    end
    object QrBoxesPalTXPallet: TFloatField
      FieldName = 'TXPallet'
    end
    object QrBoxesPalTMI_Dest: TIntegerField
      FieldName = 'TMI_Dest'
      Required = True
    end
    object QrBoxesPalCacID: TFloatField
      FieldName = 'CacID'
      Required = True
    end
    object QrBoxesPalTMI_Sorc: TIntegerField
      FieldName = 'TMI_Sorc'
    end
    object QrBoxesPalTMI_Baix: TIntegerField
      FieldName = 'TMI_Baix'
    end
    object QrBoxesPalTecla: TIntegerField
      FieldName = 'Tecla'
    end
  end
  object QrSumDest1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 600
  end
  object QrSumSorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 648
  end
  object QrTMISorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 696
  end
  object QrPalSorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 744
  end
  object QrIMEI: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 388
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIMEICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIMEIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIMEIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrIMEIMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrIMEIMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrIMEIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrIMEITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrIMEICliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrIMEIMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrIMEIPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrIMEIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIMEIQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrIMEIValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrIMEISrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrIMEISrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrIMEISrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrIMEISrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrIMEISdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrIMEISerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrIMEITalao: TIntegerField
      FieldName = 'Talao'
    end
    object QrIMEIFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrIMEICustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrIMEIDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrIMEIDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrIMEIDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrIMEIDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrIMEIQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrIMEIQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrIMEIAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrIMEIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEITpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrIMEIZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrIMEIPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrIMEIPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrIMEIPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrIMEILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIMEIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIMEIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIMEIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIMEIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIMEIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIMEIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIMEIReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrIMEITXMorCab: TIntegerField
      FieldName = 'TXMorCab'
    end
    object QrIMEITXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
  end
  object DqSum: TMySQLDirectQuery
    Database = Dmod.ZZDB
    Left = 360
    Top = 540
  end
end
