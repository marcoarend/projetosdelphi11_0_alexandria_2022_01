object FmTXImpEstqEm: TFmTXImpEstqEm
  Left = 0
  Top = 0
  Caption = 'TEX-FAXAO-129 :: Estoque TX Em...'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QrInd: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      '')
    Left = 56
    Top = 52
    object QrIndQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrIndMID: TIntegerField
      FieldName = 'MID'
    end
    object QrIndCod: TIntegerField
      FieldName = 'Cod'
    end
  end
end
