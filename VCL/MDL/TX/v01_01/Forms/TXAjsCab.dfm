object FmTXAjsCab: TFmTXAjsCab
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-023 :: Ajuste de Estoque'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 684
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Data / hora ajuste:'
        Color = clBtnFace
        ParentColor = False
      end
      object LaQtde: TLabel
        Left = 852
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 940
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 68
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label20: TLabel
        Left = 8
        Top = 56
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDataHora: TdmkEditDateTimePicker
        Left = 684
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataHora'
        UpdCampo = 'DataHora'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDataHora: TdmkEdit
        Left = 792
        Top = 32
        Width = 54
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DataHora'
        UpdCampo = 'DataHora'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 852
        Top = 32
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 940
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 68
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 124
        Top = 32
        Width = 553
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClienteMO: TdmkEditCB
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteMO'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteMO: TdmkDBLookupComboBox
        Left = 64
        Top = 72
        Width = 933
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClienteMO
        TabOrder = 8
        dmkEditCB = EdClienteMO
        QryCampo = 'ClienteMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 81
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 68
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 664
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Data / hora ajuste:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 816
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label3: TLabel
        Left = 8
        Top = 60
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXAjsCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXAjsCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 124
        Top = 32
        Width = 537
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXAjsCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 664
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DataHora'
        DataSource = DsTXAjsCab
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsTXAjsCab
        TabOrder = 4
      end
      object DBEdit11: TDBEdit
        Left = 816
        Top = 32
        Width = 112
        Height = 21
        DataField = 'Qtde'
        DataSource = DsTXAjsCab
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 84
        Top = 56
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsTXAjsCab
        TabOrder = 6
      end
      object DBEdit5: TDBEdit
        Left = 140
        Top = 56
        Width = 857
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsTXAjsCab
        TabOrder = 7
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 81
      Width = 1008
      Height = 105
      Align = alTop
      Caption = ' Itens do ajuste: '
      TabOrder = 2
      object DGDados: TdmkDBGridZTO
        Left = 2
        Top = 15
        Width = 1004
        Height = 88
        Align = alClient
        DataSource = DsTXAjsIts
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IxxMovIX'
            Title.Caption = 'IXX'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IxxFolha'
            Title.Caption = 'Folha'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IxxLinha'
            Title.Caption = 'Lin'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Arquivo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FORNECE'
            Title.Caption = 'Fornecedor / Cliente'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SerieTal'
            Title.Caption = 'ID Serie'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Talao'
            Title.Caption = 'Tal'#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Title.Caption = 'ID Pallet'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Pallet'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = 'Custo total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CUS_Qtde'
            Title.Caption = '$ Uni'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SrcNivel2'
            Title.Caption = 'IMEI Ori Bxa '
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 222
        Height = 32
        Caption = 'Ajuste de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 222
        Height = 32
        Caption = 'Ajuste de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 222
        Height = 32
        Caption = 'Ajuste de Estoque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 84
    Top = 8
  end
  object QrTXAjsCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXAjsCabBeforeOpen
    AfterOpen = QrTXAjsCabAfterOpen
    BeforeClose = QrTXAjsCabBeforeClose
    AfterScroll = QrTXAjsCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.transporta')
    Left = 144
    Top = 37
    object QrTXAjsCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXAjsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXAjsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXAjsCabDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXAjsCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrTXAjsCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXAjsCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXAjsCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXAjsCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXAjsCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXAjsCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXAjsCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXAjsCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXAjsCabTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
    object QrTXAjsCabClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrTXAjsCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
  end
  object DsTXAjsCab: TDataSource
    DataSet = QrTXAjsCab
    Left = 148
    Top = 81
  end
  object QrTXAjsIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 228
    Top = 37
    object QrTXAjsItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXAjsItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXAjsItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXAjsItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXAjsItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXAjsItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXAjsItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXAjsItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXAjsItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXAjsItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXAjsItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXAjsItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXAjsItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXAjsItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTXAjsItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXAjsItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXAjsItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXAjsItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXAjsItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXAjsItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXAjsItsSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXAjsItsTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXAjsItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXAjsItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXAjsItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrTXAjsItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrTXAjsItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrTXAjsItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXAjsItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXAjsItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXAjsItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXAjsItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXAjsItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXAjsItsCUS_Qtde: TFloatField
      FieldName = 'CUS_Qtde'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXAjsItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXAjsItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXAjsItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXAjsItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXAjsItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXAjsItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXAjsItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXAjsItsIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrTXAjsItsIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrTXAjsItsIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
  end
  object DsTXAjsIts: TDataSource
    DataSet = QrTXAjsIts
    Left = 232
    Top = 81
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 656
    Top = 580
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 8
    object Classificao1: TMenuItem
      Caption = '&Resumo do ajuste'
      OnClick = Classificao1Click
    end
    object Fichas1: TMenuItem
      Caption = '&Fichas de Pallets afetados'
      object FichasCOMnomedoPallet1: TMenuItem
        Caption = 'Fichas &COM nome do Pallet'
        OnClick = FichasCOMnomedoPallet1Click
      end
      object FichasSEMnomedoPallet1: TMenuItem
        Caption = 'Fichas &SEM nome do Pallet'
        OnClick = FichasSEMnomedoPallet1Click
      end
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
  end
  object frxTEX_FAXAO_010_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412040000000
    ReportOptions.LastChange = 41608.425381412040000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxTEX_FAXAO_010_01GetValue
    Left = 324
    Top = 36
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsTXAjsCab
        DataSetName = 'frxDsTXAjsCab'
      end
      item
        DataSet = frxDsTXAjsIts
        DataSetName = 'frxDsTXAjsIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 102.047300240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Ajuste de Estoque de Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 816.378480000000000000
          Top = 18.897650000000000000
          Width = 147.401606540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 113.385900000000000000
          Top = 86.929190000000000000
          Width = 226.771653540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor / Cliente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 86.929190000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSAjsCab."NO_EMPRESA"]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Top = 64.252010000000000000
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Top = 64.252010000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo Ajuste:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 86.929190000000000000
          Top = 64.252010000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSAjsCab."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 188.976500000000000000
          Top = 64.252010000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora ajuste:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 283.464750000000000000
          Top = 64.252010000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSAjsCab."DataHora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 578.268090000000000000
          Top = 86.929190000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Top = 86.929190000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 634.961040000000000000
          Top = 86.929190000000000000
          Width = 336.378133390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 340.157700000000000000
          Top = 86.929190000000000000
          Width = 238.110243540000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 204.094620000000000000
        Width = 971.339210000000000000
        DataSet = frxDsTXAjsIts
        DataSetName = 'frxDsTXAjsIts'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 340.157700000000000000
          Width = 238.110243540000000000
          Height = 15.118110240000000000
          DataSet = frxDsTXAjsIts
          DataSetName = 'frxDsTXAjsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSAjsIts."GraGruX"] - [frxDsVSAjsIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Width = 37.795275590551180000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsTXAjsIts
          DataSetName = 'frxDsTXAjsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXAjsIts."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 578.268090000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Qtde'
          DataSet = frxDsTXAjsIts
          DataSetName = 'frxDsTXAjsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXAjsIts."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 37.795300000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'NO_PALLET'
          DataSet = frxDsTXAjsIts
          DataSetName = 'frxDsTXAjsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXAjsIts."NO_PALLET"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 634.961040000000000000
          Width = 336.378133390000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsTXAjsIts
          DataSetName = 'frxDsTXAjsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXAjsIts."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 113.385900000000000000
          Width = 226.771653540000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsTXAjsIts
          DataSetName = 'frxDsTXAjsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXAjsIts."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 684.094930000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 650.079160000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 241.889920000000000000
        Width = 971.339210000000000000
        object Memo17: TfrxMemoView
          Width = 578.267894720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 578.268090000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSAjsIts."Qtde">,MD002,1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Top = 181.417440000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsVSAjsIts."Codigo"'
      end
    end
  end
  object frxDsTXAjsIts: TfrxDBDataset
    UserName = 'frxDsTXAjsIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Observ=Observ'
      'NO_FORNECE=NO_FORNECE')
    DataSet = QrTXAjsIts
    BCDToCurrency = False
    Left = 232
    Top = 124
  end
  object frxDsTXAjsCab: TfrxDBDataset
    UserName = 'frxDsTXAjsCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DataHora=DataHora'
      'Qtde=Qtde'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA')
    DataSet = QrTXAjsCab
    BCDToCurrency = False
    Left = 148
    Top = 128
  end
  object QrPalletCla: TmySQLQuery
    Database = Dmod.MyDB
    Left = 564
    Top = 356
    object QrPalletClaPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrPalletClaDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPalletClaQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPalletClaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletClaNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletClaNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrPalletClaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object frxTEX_FAXAO_010_03: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxTEX_FAXAO_010_01GetValue
    Left = 564
    Top = 312
    Datasets = <
      item
        DataSet = frxDsPalletCla
        DataSetName = 'frxDsPalletCla'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370078740000100000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPalletCla
        DataSetName = 'frxDsPalletCla'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 483.779840000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 200.315090000000000000
          Top = 90.811070000000000000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-Prima para Semi / Acabado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 90.811070000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 483.779840000000000000
          Top = 90.811070000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 128.795300000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'PALLET [frxDsPalletCla."Pallet"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 176.574830000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 1.118120000000000000
          Top = 173.015770000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 268.448828660000000000
          Width = 188.976377950000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 268.448828660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Proced'#234'ncia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 226.574830000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 351.598444720000000000
          Width = 680.315290160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 306.244106690000000000
          Width = 188.976377950000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletCla."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 306.244106690000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPalletCla."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 120.944881890000000000
          Top = 226.574830000000000000
          Width = 559.370078740000100000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPalletCla."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 389.393722760000000000
          Width = 680.315290160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsPalletCla."' +
              'Qtde">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 120.944881890000000000
          Top = 180.795300000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPalletCla."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 180.795300000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 56.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 226.771800000000000000
          Top = 128.606370000000000000
          Width = 434.645510630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPalletCla."NO_Pallet"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Top = 457.323130000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line4: TfrxLineView
          Left = 7.559060000000000000
          Top = 491.338900000000000000
          Width = 665.197280000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxDsPalletCla: TfrxDBDataset
    UserName = 'frxDsPalletCla'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Pallet=Pallet'
      'DataHora=DataHora'
      'Qtde=Qtde'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE')
    DataSet = QrPalletCla
    BCDToCurrency = False
    Left = 564
    Top = 404
  end
  object QrClienteMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 40
    Top = 224
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteMO: TDataSource
    DataSet = QrClienteMO
    Left = 40
    Top = 272
  end
end
