unit TXImpIMEI;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet, Data.DB,
  mySQLDbTables, dmkGeral, UnInternalConsts, UnDmkEnums, UnProjGroup_Consts,
  UnDmkProcFunc, UnTX_PF, UnProjGroup_PF, UnAppEnums;

type
  TFmTXImpIMEI = class(TForm)
    QrOld01: TmySQLQuery;
    QrOld01Pecas: TFloatField;
    QrOld01PesoKg: TFloatField;
    QrOld01NO_PRD_TAM_COR: TWideStringField;
    QrOld01Observ: TWideStringField;
    QrOld01NO_FORNECE: TWideStringField;
    QrOld01NO_SerieFch: TWideStringField;
    frxDsOld01: TfrxDBDataset;
    QrOld02: TmySQLQuery;
    frxDsOld02: TfrxDBDataset;
    QrOld03: TmySQLQuery;
    frxDsOld03: TfrxDBDataset;
    QrOld04: TmySQLQuery;
    frxDsOld04: TfrxDBDataset;
    QrOld05: TmySQLQuery;
    frxDsOld05: TfrxDBDataset;
    frxDsEstqR4: TfrxDBDataset;
    frxTEX_FAXAO_025_01: TfrxReport;
    QrTXGerArtOld: TmySQLQuery;
    QrEstqR4: TmySQLQuery;
    QrEstqR4Codigo: TIntegerField;
    QrEstqR4Controle: TIntegerField;
    QrEstqR4MovimCod: TIntegerField;
    QrEstqR4MovimNiv: TIntegerField;
    QrEstqR4MovimID: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Pecas: TFloatField;
    QrEstqR4PesoKg: TFloatField;
    QrEstqR4AreaM2: TFloatField;
    QrEstqR4AreaP2: TFloatField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4SdoVrtPeca: TFloatField;
    QrEstqR4SdoVrtPeso: TFloatField;
    QrEstqR4SdoVrtArM2: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4NO_EMPRESA: TWideStringField;
    QrEstqR4Cliente: TIntegerField;
    QrEstqR4NO_CLIENTE: TWideStringField;
    QrEstqR4Fornece: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Status: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    QrEstqR4DataHora: TDateTimeField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4OrdGGY: TIntegerField;
    QrEstqR4GraGruY: TIntegerField;
    QrEstqR4NO_GGY: TWideStringField;
    QrEstqR4NO_FICHA: TWideStringField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4TERCEIRO: TIntegerField;
    QrEstqR4TIPO_TERCEIRO: TWideStringField;
    QrEstqR4NO_TIPO_COURO: TWideStringField;
    QrEstqR4NO_ACAO_COURO: TWideStringField;
    frxTEX_FAXAO_025_02_A: TfrxReport;
    QrEstqR4ReqMovEstq: TIntegerField;
    QrEstqR4Observ: TWideStringField;
    QrEstqR4PedItsLib: TIntegerField;
    frxTEX_FAXAO_025_03: TfrxReport;
    QrOPs: TmySQLQuery;
    frxDsOPs: TfrxDBDataset;
    QrEstqR4NO_FRNCALC: TWideStringField;
    QrTXGerArtOldCodigo: TLargeintField;
    QrTXGerArtOldControle: TLargeintField;
    QrTXGerArtOldMovimCod: TLargeintField;
    QrTXGerArtOldMovimNiv: TLargeintField;
    QrTXGerArtOldMovimTwn: TLargeintField;
    QrTXGerArtOldEmpresa: TLargeintField;
    QrTXGerArtOldTerceiro: TLargeintField;
    QrTXGerArtOldCliVenda: TLargeintField;
    QrTXGerArtOldMovimID: TLargeintField;
    QrTXGerArtOldDataHora: TDateTimeField;
    QrTXGerArtOldPallet: TLargeintField;
    QrTXGerArtOldGraGruX: TLargeintField;
    QrTXGerArtOldPecas: TFloatField;
    QrTXGerArtOldPesoKg: TFloatField;
    QrTXGerArtOldAreaM2: TFloatField;
    QrTXGerArtOldAreaP2: TFloatField;
    QrTXGerArtOldValorT: TFloatField;
    QrTXGerArtOldSrcMovID: TLargeintField;
    QrTXGerArtOldSrcNivel1: TLargeintField;
    QrTXGerArtOldSrcNivel2: TLargeintField;
    QrTXGerArtOldSrcGGX: TLargeintField;
    QrTXGerArtOldSdoVrtPeca: TFloatField;
    QrTXGerArtOldSdoVrtPeso: TFloatField;
    QrTXGerArtOldSdoVrtArM2: TFloatField;
    QrTXGerArtOldObserv: TWideStringField;
    QrTXGerArtOldSerieFch: TLargeintField;
    QrTXGerArtOldFicha: TLargeintField;
    QrTXGerArtOldMisturou: TLargeintField;
    QrTXGerArtOldFornecMO: TLargeintField;
    QrTXGerArtOldCustoMOKg: TFloatField;
    QrTXGerArtOldCustoMOM2: TFloatField;
    QrTXGerArtOldCustoMOTot: TFloatField;
    QrTXGerArtOldValorMP: TFloatField;
    QrTXGerArtOldDstMovID: TLargeintField;
    QrTXGerArtOldDstNivel1: TLargeintField;
    QrTXGerArtOldDstNivel2: TLargeintField;
    QrTXGerArtOldDstGGX: TLargeintField;
    QrTXGerArtOldQtdGerPeca: TFloatField;
    QrTXGerArtOldQtdGerPeso: TFloatField;
    QrTXGerArtOldQtdGerArM2: TFloatField;
    QrTXGerArtOldQtdGerArP2: TFloatField;
    QrTXGerArtOldQtdAntPeca: TFloatField;
    QrTXGerArtOldQtdAntPeso: TFloatField;
    QrTXGerArtOldQtdAntArM2: TFloatField;
    QrTXGerArtOldQtdAntArP2: TFloatField;
    QrTXGerArtOldNotaMPAG: TFloatField;
    QrTXGerArtOldNO_PALLET: TWideStringField;
    QrTXGerArtOldNO_PRD_TAM_COR: TWideStringField;
    QrTXGerArtOldNO_TTW: TWideStringField;
    QrTXGerArtOldID_TTW: TLargeintField;
    QrTXGerArtOldNO_FORNECE: TWideStringField;
    QrTXGerArtOldNO_SerieFch: TWideStringField;
    QrTXGerArtOldReqMovEstq: TLargeintField;
    QrOld01Controle: TLargeintField;
    QrOld01GraGruX: TLargeintField;
    QrOld01Ficha: TLargeintField;
    QrOld02Controle: TLargeintField;
    QrOld02GraGruX: TLargeintField;
    QrOld02Pecas: TFloatField;
    QrOld02PesoKg: TFloatField;
    QrOld02NO_PRD_TAM_COR: TWideStringField;
    QrOld02Observ: TWideStringField;
    QrOld02Ficha: TLargeintField;
    QrOld02NO_FORNECE: TWideStringField;
    QrOld02NO_SerieFch: TWideStringField;
    QrOld03Controle: TLargeintField;
    QrOld03GraGruX: TLargeintField;
    QrOld03Pecas: TFloatField;
    QrOld03PesoKg: TFloatField;
    QrOld03NO_PRD_TAM_COR: TWideStringField;
    QrOld03Observ: TWideStringField;
    QrOld03Ficha: TLargeintField;
    QrOld03NO_FORNECE: TWideStringField;
    QrOld03NO_SerieFch: TWideStringField;
    QrOld04Controle: TLargeintField;
    QrOld04GraGruX: TLargeintField;
    QrOld04Pecas: TFloatField;
    QrOld04PesoKg: TFloatField;
    QrOld04NO_PRD_TAM_COR: TWideStringField;
    QrOld04Observ: TWideStringField;
    QrOld04Ficha: TLargeintField;
    QrOld04NO_FORNECE: TWideStringField;
    QrOld04NO_SerieFch: TWideStringField;
    QrOld05Controle: TLargeintField;
    QrOld05GraGruX: TLargeintField;
    QrOld05Pecas: TFloatField;
    QrOld05PesoKg: TFloatField;
    QrOld05NO_PRD_TAM_COR: TWideStringField;
    QrOld05Observ: TWideStringField;
    QrOld05Ficha: TLargeintField;
    QrOld05NO_FORNECE: TWideStringField;
    QrOld05NO_SerieFch: TWideStringField;
    frxTEX_FAXAO_025_04: TfrxReport;
    frxReport1: TfrxReport;
    QrEstqR4SdoVrtArP2: TFloatField;
    frxTEX_FAXAO_025_02_B: TfrxReport;
    QrTXPWEDst: TmySQLQuery;
    QrTXPWEDstCodigo: TLargeintField;
    QrTXPWEDstControle: TLargeintField;
    QrTXPWEDstMovimCod: TLargeintField;
    QrTXPWEDstMovimNiv: TLargeintField;
    QrTXPWEDstMovimTwn: TLargeintField;
    QrTXPWEDstEmpresa: TLargeintField;
    QrTXPWEDstTerceiro: TLargeintField;
    QrTXPWEDstCliVenda: TLargeintField;
    QrTXPWEDstMovimID: TLargeintField;
    QrTXPWEDstDataHora: TDateTimeField;
    QrTXPWEDstPallet: TLargeintField;
    QrTXPWEDstGraGruX: TLargeintField;
    QrTXPWEDstPecas: TFloatField;
    QrTXPWEDstPesoKg: TFloatField;
    QrTXPWEDstAreaM2: TFloatField;
    QrTXPWEDstAreaP2: TFloatField;
    QrTXPWEDstValorT: TFloatField;
    QrTXPWEDstSrcMovID: TLargeintField;
    QrTXPWEDstSrcNivel1: TLargeintField;
    QrTXPWEDstSrcNivel2: TLargeintField;
    QrTXPWEDstSrcGGX: TLargeintField;
    QrTXPWEDstSdoVrtPeca: TFloatField;
    QrTXPWEDstSdoVrtPeso: TFloatField;
    QrTXPWEDstSdoVrtArM2: TFloatField;
    QrTXPWEDstObserv: TWideStringField;
    QrTXPWEDstSerieFch: TLargeintField;
    QrTXPWEDstFicha: TLargeintField;
    QrTXPWEDstMisturou: TLargeintField;
    QrTXPWEDstFornecMO: TLargeintField;
    QrTXPWEDstCustoMOKg: TFloatField;
    QrTXPWEDstCustoMOM2: TFloatField;
    QrTXPWEDstCustoMOTot: TFloatField;
    QrTXPWEDstValorMP: TFloatField;
    QrTXPWEDstDstMovID: TLargeintField;
    QrTXPWEDstDstNivel1: TLargeintField;
    QrTXPWEDstDstNivel2: TLargeintField;
    QrTXPWEDstDstGGX: TLargeintField;
    QrTXPWEDstQtdGerPeca: TFloatField;
    QrTXPWEDstQtdGerPeso: TFloatField;
    QrTXPWEDstQtdGerArM2: TFloatField;
    QrTXPWEDstQtdGerArP2: TFloatField;
    QrTXPWEDstQtdAntPeca: TFloatField;
    QrTXPWEDstQtdAntPeso: TFloatField;
    QrTXPWEDstQtdAntArM2: TFloatField;
    QrTXPWEDstQtdAntArP2: TFloatField;
    QrTXPWEDstNotaMPAG: TFloatField;
    QrTXPWEDstNO_PALLET: TWideStringField;
    QrTXPWEDstNO_PRD_TAM_COR: TWideStringField;
    QrTXPWEDstNO_TTW: TWideStringField;
    QrTXPWEDstID_TTW: TLargeintField;
    QrTXPWEDstNO_FORNECE: TWideStringField;
    QrTXPWEDstNO_SerieFch: TWideStringField;
    QrTXPWEDstReqMovEstq: TLargeintField;
    QrTXPWEDstPedItsFin: TLargeintField;
    QrTXPWEDstMarca: TWideStringField;
    QrTXPWEDstStqCenLoc: TLargeintField;
    frxDsTXPWEDst: TfrxDBDataset;
    QrTXPWEBxa: TmySQLQuery;
    QrTXPWEBxaCodigo: TLargeintField;
    QrTXPWEBxaControle: TLargeintField;
    QrTXPWEBxaMovimCod: TLargeintField;
    QrTXPWEBxaMovimNiv: TLargeintField;
    QrTXPWEBxaMovimTwn: TLargeintField;
    QrTXPWEBxaEmpresa: TLargeintField;
    QrTXPWEBxaTerceiro: TLargeintField;
    QrTXPWEBxaCliVenda: TLargeintField;
    QrTXPWEBxaMovimID: TLargeintField;
    QrTXPWEBxaDataHora: TDateTimeField;
    QrTXPWEBxaPallet: TLargeintField;
    QrTXPWEBxaGraGruX: TLargeintField;
    QrTXPWEBxaPecas: TFloatField;
    QrTXPWEBxaPesoKg: TFloatField;
    QrTXPWEBxaAreaM2: TFloatField;
    QrTXPWEBxaAreaP2: TFloatField;
    QrTXPWEBxaValorT: TFloatField;
    QrTXPWEBxaSrcMovID: TLargeintField;
    QrTXPWEBxaSrcNivel1: TLargeintField;
    QrTXPWEBxaSrcNivel2: TLargeintField;
    QrTXPWEBxaSrcGGX: TLargeintField;
    QrTXPWEBxaSdoVrtPeca: TFloatField;
    QrTXPWEBxaSdoVrtPeso: TFloatField;
    QrTXPWEBxaSdoVrtArM2: TFloatField;
    QrTXPWEBxaObserv: TWideStringField;
    QrTXPWEBxaSerieFch: TLargeintField;
    QrTXPWEBxaFicha: TLargeintField;
    QrTXPWEBxaMisturou: TLargeintField;
    QrTXPWEBxaFornecMO: TLargeintField;
    QrTXPWEBxaCustoMOKg: TFloatField;
    QrTXPWEBxaCustoMOM2: TFloatField;
    QrTXPWEBxaCustoMOTot: TFloatField;
    QrTXPWEBxaValorMP: TFloatField;
    QrTXPWEBxaDstMovID: TLargeintField;
    QrTXPWEBxaDstNivel1: TLargeintField;
    QrTXPWEBxaDstNivel2: TLargeintField;
    QrTXPWEBxaDstGGX: TLargeintField;
    QrTXPWEBxaQtdGerPeca: TFloatField;
    QrTXPWEBxaQtdGerPeso: TFloatField;
    QrTXPWEBxaQtdGerArM2: TFloatField;
    QrTXPWEBxaQtdGerArP2: TFloatField;
    QrTXPWEBxaQtdAntPeca: TFloatField;
    QrTXPWEBxaQtdAntPeso: TFloatField;
    QrTXPWEBxaQtdAntArM2: TFloatField;
    QrTXPWEBxaQtdAntArP2: TFloatField;
    QrTXPWEBxaNotaMPAG: TFloatField;
    QrTXPWEBxaNO_PALLET: TWideStringField;
    QrTXPWEBxaNO_PRD_TAM_COR: TWideStringField;
    QrTXPWEBxaNO_TTW: TWideStringField;
    QrTXPWEBxaID_TTW: TLargeintField;
    QrTXPWEBxaNO_FORNECE: TWideStringField;
    QrTXPWEBxaNO_SerieFch: TWideStringField;
    QrTXPWEBxaReqMovEstq: TLargeintField;
    frxDsTXPWEBxa: TfrxDBDataset;
    QrTXPWEDstAreaM2WB: TFloatField;
    QrTXPWEDstPercRendim: TFloatField;
    frxTEX_FAXAO_025_05: TfrxReport;
    frxTEX_FAXAO_025_02_C: TfrxReport;
    QrTXCurOriIMEI: TmySQLQuery;
    QrTXCurOriIMEICodigo: TLargeintField;
    QrTXCurOriIMEIControle: TLargeintField;
    QrTXCurOriIMEIMovimCod: TLargeintField;
    QrTXCurOriIMEIMovimNiv: TLargeintField;
    QrTXCurOriIMEIMovimTwn: TLargeintField;
    QrTXCurOriIMEIEmpresa: TLargeintField;
    QrTXCurOriIMEITerceiro: TLargeintField;
    QrTXCurOriIMEICliVenda: TLargeintField;
    QrTXCurOriIMEIMovimID: TLargeintField;
    QrTXCurOriIMEIDataHora: TDateTimeField;
    QrTXCurOriIMEIPallet: TLargeintField;
    QrTXCurOriIMEIGraGruX: TLargeintField;
    QrTXCurOriIMEIPecas: TFloatField;
    QrTXCurOriIMEIPesoKg: TFloatField;
    QrTXCurOriIMEIAreaM2: TFloatField;
    QrTXCurOriIMEIAreaP2: TFloatField;
    QrTXCurOriIMEIValorT: TFloatField;
    QrTXCurOriIMEISrcMovID: TLargeintField;
    QrTXCurOriIMEISrcNivel1: TLargeintField;
    QrTXCurOriIMEISrcNivel2: TLargeintField;
    QrTXCurOriIMEISrcGGX: TLargeintField;
    QrTXCurOriIMEISdoVrtPeca: TFloatField;
    QrTXCurOriIMEISdoVrtPeso: TFloatField;
    QrTXCurOriIMEISdoVrtArM2: TFloatField;
    QrTXCurOriIMEIObserv: TWideStringField;
    QrTXCurOriIMEISerieFch: TLargeintField;
    QrTXCurOriIMEIFicha: TLargeintField;
    QrTXCurOriIMEIMisturou: TLargeintField;
    QrTXCurOriIMEIFornecMO: TLargeintField;
    QrTXCurOriIMEICustoMOKg: TFloatField;
    QrTXCurOriIMEICustoMOM2: TFloatField;
    QrTXCurOriIMEICustoMOTot: TFloatField;
    QrTXCurOriIMEIValorMP: TFloatField;
    QrTXCurOriIMEIDstMovID: TLargeintField;
    QrTXCurOriIMEIDstNivel1: TLargeintField;
    QrTXCurOriIMEIDstNivel2: TLargeintField;
    QrTXCurOriIMEIDstGGX: TLargeintField;
    QrTXCurOriIMEIQtdGerPeca: TFloatField;
    QrTXCurOriIMEIQtdGerPeso: TFloatField;
    QrTXCurOriIMEIQtdGerArM2: TFloatField;
    QrTXCurOriIMEIQtdGerArP2: TFloatField;
    QrTXCurOriIMEIQtdAntPeca: TFloatField;
    QrTXCurOriIMEIQtdAntPeso: TFloatField;
    QrTXCurOriIMEIQtdAntArM2: TFloatField;
    QrTXCurOriIMEIQtdAntArP2: TFloatField;
    QrTXCurOriIMEINotaMPAG: TFloatField;
    QrTXCurOriIMEINO_PALLET: TWideStringField;
    QrTXCurOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrTXCurOriIMEINO_TTW: TWideStringField;
    QrTXCurOriIMEIID_TTW: TLargeintField;
    QrTXCurOriIMEINO_FORNECE: TWideStringField;
    QrTXCurOriIMEINO_SerieFch: TWideStringField;
    QrTXCurOriIMEIReqMovEstq: TLargeintField;
    QrTXCurOriIMEICustoPQ: TFloatField;
    frxDsTXCurOriIMEI: TfrxDBDataset;
    QrTXDesclDst: TmySQLQuery;
    QrTXDesclDstCodigo: TLargeintField;
    QrTXDesclDstControle: TLargeintField;
    QrTXDesclDstMovimCod: TLargeintField;
    QrTXDesclDstMovimNiv: TLargeintField;
    QrTXDesclDstMovimTwn: TLargeintField;
    QrTXDesclDstEmpresa: TLargeintField;
    QrTXDesclDstTerceiro: TLargeintField;
    QrTXDesclDstCliVenda: TLargeintField;
    QrTXDesclDstMovimID: TLargeintField;
    QrTXDesclDstDataHora: TDateTimeField;
    QrTXDesclDstPallet: TLargeintField;
    QrTXDesclDstGraGruX: TLargeintField;
    QrTXDesclDstPecas: TFloatField;
    QrTXDesclDstPesoKg: TFloatField;
    QrTXDesclDstAreaM2: TFloatField;
    QrTXDesclDstAreaP2: TFloatField;
    QrTXDesclDstValorT: TFloatField;
    QrTXDesclDstSrcMovID: TLargeintField;
    QrTXDesclDstSrcNivel1: TLargeintField;
    QrTXDesclDstSrcNivel2: TLargeintField;
    QrTXDesclDstSrcGGX: TLargeintField;
    QrTXDesclDstSdoVrtPeca: TFloatField;
    QrTXDesclDstSdoVrtPeso: TFloatField;
    QrTXDesclDstSdoVrtArM2: TFloatField;
    QrTXDesclDstObserv: TWideStringField;
    QrTXDesclDstSerieFch: TLargeintField;
    QrTXDesclDstFicha: TLargeintField;
    QrTXDesclDstMisturou: TLargeintField;
    QrTXDesclDstFornecMO: TLargeintField;
    QrTXDesclDstCustoMOKg: TFloatField;
    QrTXDesclDstCustoMOM2: TFloatField;
    QrTXDesclDstCustoMOTot: TFloatField;
    QrTXDesclDstValorMP: TFloatField;
    QrTXDesclDstDstMovID: TLargeintField;
    QrTXDesclDstDstNivel1: TLargeintField;
    QrTXDesclDstDstNivel2: TLargeintField;
    QrTXDesclDstDstGGX: TLargeintField;
    QrTXDesclDstQtdGerPeca: TFloatField;
    QrTXDesclDstQtdGerPeso: TFloatField;
    QrTXDesclDstQtdGerArM2: TFloatField;
    QrTXDesclDstQtdGerArP2: TFloatField;
    QrTXDesclDstQtdAntPeca: TFloatField;
    QrTXDesclDstQtdAntPeso: TFloatField;
    QrTXDesclDstQtdAntArM2: TFloatField;
    QrTXDesclDstQtdAntArP2: TFloatField;
    QrTXDesclDstNotaMPAG: TFloatField;
    QrTXDesclDstNO_PALLET: TWideStringField;
    QrTXDesclDstNO_PRD_TAM_COR: TWideStringField;
    QrTXDesclDstNO_TTW: TWideStringField;
    QrTXDesclDstID_TTW: TLargeintField;
    QrTXDesclDstNO_FORNECE: TWideStringField;
    QrTXDesclDstNO_SerieFch: TWideStringField;
    QrTXDesclDstReqMovEstq: TLargeintField;
    QrTXDesclDstPedItsFin: TLargeintField;
    QrTXDesclDstMarca: TWideStringField;
    QrTXDesclDstStqCenLoc: TLargeintField;
    frxDsTXDesclDst: TfrxDBDataset;
    QrOPsCodigo: TIntegerField;
    QrOPsControle: TIntegerField;
    QrOPsMovimCod: TIntegerField;
    QrOPsMovimNiv: TIntegerField;
    QrOPsMovimTwn: TIntegerField;
    QrOPsEmpresa: TIntegerField;
    QrOPsTerceiro: TIntegerField;
    QrOPsCliVenda: TIntegerField;
    QrOPsMovimID: TIntegerField;
    QrOPsLnkIDXtr: TIntegerField;
    QrOPsLnkNivXtr1: TIntegerField;
    QrOPsLnkNivXtr2: TIntegerField;
    QrOPsDataHora: TDateTimeField;
    QrOPsPallet: TIntegerField;
    QrOPsGraGruX: TIntegerField;
    QrOPsPecas: TFloatField;
    QrOPsPesoKg: TFloatField;
    QrOPsAreaM2: TFloatField;
    QrOPsAreaP2: TFloatField;
    QrOPsValorT: TFloatField;
    QrOPsSrcMovID: TIntegerField;
    QrOPsSrcNivel1: TIntegerField;
    QrOPsSrcNivel2: TIntegerField;
    QrOPsSrcGGX: TIntegerField;
    QrOPsSdoVrtPeca: TFloatField;
    QrOPsSdoVrtPeso: TFloatField;
    QrOPsSdoVrtArM2: TFloatField;
    QrOPsObserv: TWideStringField;
    QrOPsSerieFch: TIntegerField;
    QrOPsFicha: TIntegerField;
    QrOPsMisturou: TSmallintField;
    QrOPsFornecMO: TIntegerField;
    QrOPsCustoMOKg: TFloatField;
    QrOPsCustoMOTot: TFloatField;
    QrOPsValorMP: TFloatField;
    QrOPsDstMovID: TIntegerField;
    QrOPsDstNivel1: TIntegerField;
    QrOPsDstNivel2: TIntegerField;
    QrOPsDstGGX: TIntegerField;
    QrOPsQtdGerPeca: TFloatField;
    QrOPsQtdGerPeso: TFloatField;
    QrOPsQtdGerArM2: TFloatField;
    QrOPsQtdGerArP2: TFloatField;
    QrOPsQtdAntPeca: TFloatField;
    QrOPsQtdAntPeso: TFloatField;
    QrOPsQtdAntArM2: TFloatField;
    QrOPsQtdAntArP2: TFloatField;
    QrOPsAptoUso: TSmallintField;
    QrOPsNotaMPAG: TFloatField;
    QrOPsMarca: TWideStringField;
    QrOPsTpCalcAuto: TIntegerField;
    QrOPsZerado: TSmallintField;
    QrOPsEmFluxo: TSmallintField;
    QrOPsNotFluxo: TIntegerField;
    QrOPsFatNotaVNC: TFloatField;
    QrOPsFatNotaVRC: TFloatField;
    QrOPsPedItsLib: TIntegerField;
    QrOPsPedItsFin: TIntegerField;
    QrOPsPedItsVda: TIntegerField;
    QrOPsLk: TIntegerField;
    QrOPsDataCad: TDateField;
    QrOPsDataAlt: TDateField;
    QrOPsUserCad: TIntegerField;
    QrOPsUserAlt: TIntegerField;
    QrOPsAlterWeb: TSmallintField;
    QrOPsAtivo: TSmallintField;
    QrOPsGSPInnNiv2: TIntegerField;
    QrOPsGSPArtNiv2: TIntegerField;
    QrOPsCustoMOM2: TFloatField;
    QrOPsReqMovEstq: TIntegerField;
    QrOPsStqCenLoc: TIntegerField;
    QrOPsItemNFe: TIntegerField;
    QrOPsTXMorCab: TIntegerField;
    QrOPsTXMulFrnCab: TIntegerField;
    QrOPsClientMO: TIntegerField;
    QrOPsCustoPQ: TFloatField;
    QrOPsKgCouPQ: TFloatField;
    QrOPsNFeSer: TSmallintField;
    QrOPsNFeNum: TIntegerField;
    QrOPsTXMulNFeCab: TIntegerField;
    QrOPsGGXRcl: TIntegerField;
    QrOPsJmpMovID: TIntegerField;
    QrOPsJmpNivel1: TIntegerField;
    QrOPsJmpNivel2: TIntegerField;
    QrOPsJmpGGX: TIntegerField;
    QrOPsRmsMovID: TIntegerField;
    QrOPsRmsNivel1: TIntegerField;
    QrOPsRmsNivel2: TIntegerField;
    QrOPsRmsGGX: TIntegerField;
    QrOPsGSPSrcMovID: TIntegerField;
    QrOPsGSPSrcNiv2: TIntegerField;
    QrOPsGSPJmpMovID: TIntegerField;
    QrOPsGSPJmpNiv2: TIntegerField;
    QrOPsDtCorrApo: TDateTimeField;
    QrOPsNO_MovimID: TWideStringField;
    QrOPsNO_MovimNiv: TWideStringField;
    QrOPsNO_CliVenda: TWideStringField;
    QrOPsNO_FornecMO: TWideStringField;
    QrOPsInteiros: TFloatField;
    QrOPsSdoIntei: TFloatField;
    QrOPsNO_FORNECE: TWideStringField;
    QrOPsNO_PRD_TAM_COR: TWideStringField;
    procedure frxTEX_FAXAO_025_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEstqR4AfterScroll(DataSet: TDataSet);
    procedure QrEstqR4CalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure frxTEX_FAXAO_025_04GetValue(const VarName: string;
      var Value: Variant);
    procedure QrTXPWEDstCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FTXImpIMEI: String;
    //
    function  SQL_ListaControles(): String;
  public
    { Public declarations }
    FTXImpImeiKind: TXXImpImeiKind;
    FControles: array of Integer;
    FLPFMO, FNFeRem: String;
    FQryCab: TmySQLQuery;
    //
    procedure ImprimeIMEI();
    procedure ImprimeOPsAbertasUni(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
              RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
              MovimID: TEstqMovimID);
    procedure ImprimeOPsAbertasMul(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
              RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
              MovimIDs: array of TEstqMovimID; Ck18Cor_Checked,
              Ck18Especificos_Checked: Boolean; Ed18Especificos_Text: String;
              Ed18Fornecedor_ValueVariant: Integer);
  end;

var
  FmTXImpIMEI: TFmTXImpIMEI;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, CreateTX,
  AppListas;

{$R *.dfm}

{ TForm1 }

procedure TFmTXImpIMEI.FormCreate(Sender: TObject);
begin
  FTXImpImeiKind := viikArtigoGerado;
  FLPFMO  := '';
  FNFeRem := '';
end;

procedure TFmTXImpIMEI.frxTEX_FAXAO_025_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_LPFMO' then
  begin
    Value := '';
    if Trim(FLPFMO) <> '' then
      Value := 'OP ' + FLPFMO + '  ';
    if Trim(FNFeRem) <> '' then
       Value := Value + 'NF Rem.: ' + FNFeRem;
  end else
  if VarName = 'VARF_SoCincoMostrados' then
    Value := QrTXGerArtOld.RecordCount
  else
  if VarName = 'VARF_ITENS_OLD' then
    Value := QrTXGerArtOld.RecordCount
  else
  if VarName = 'VARF_DV_DO_IMEI' then
    Value := Geral.FFN(
      DmkPF.DigitoVerificardorDmk3CasasInteger(QrEstqR4Controle.Value), 3)
  else
  if VarName = 'VARF_CODIGO_BARRA' then
    Value := Geral.FFN(
      DmkPF.DigitoVerificardorDmk3CasasInteger(QrEstqR4Controle.Value), 3) +
      Geral.FFN(QrEstqR4Controle.Value, 9)
  else
  if VarName = 'VARF_OBS_OPE' then
  begin
    Value := '';
    if FQryCab <> nil then
      Value := FQryCab.FieldByName('Nome').AsString;
  end else
end;

procedure TFmTXImpIMEI.frxTEX_FAXAO_025_04GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_EMPRESA' then
    Value := VAR_LIB_EMPRESAS_NOME
  else
  if VarName ='VARF_LETRA_OX' then
  begin
    case TEStqMovimID(QrOPsMovimID.Value) of
      emidEmProcWE: Value := 'P';
      emidEmOperacao: Value := 'O';
      else Value := '?';
    end;
  end
  else
end;

procedure TFmTXImpIMEI.ImprimeIMEI();
const
  CtrlNxt = 0;
  TemIMEIMrt = 1;
  SQL_Limit = '';
var
  //SQL_Limit, Cab_DtHr: String;
  //Empresa: Integer;
  //SQL_Empresa: String;
  Relatorio: TfrxReport;
begin
  if (Length(FControles) = 0) or ((Length(FControles) = 1) and (FControles[0] = 0)) then
    Geral.MB_Info('Nenhum IME-I foi informado!');
  FTXImpIMEI :=
    UnCreateTX.RecriaTempTableNovo(ntrttTXImpIMEI, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR4, DModG.MyPID_DB, [
  DELETE_FROM + ' ' + FTXImpIMEI + '; ',
  'INSERT INTO  ' + FTXImpIMEI,
  'SELECT vsi.Codigo, vsi.Controle, vsi.MovimCod, vsi.MovimNiv, vsi.MovimID, ',
  'vsi.GraGruX, vsi.Qtde, vsi.ValorT, vsi.SdoVrtQtd ',
  'ggx.GraGru1, CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, vsi.Pallet, vsp.Nome NO_Pallet,   ',
  'vsi.Empresa, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) Cliente,  ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE, ',
  'vsi.Terceiro Fornece, IF(vsi.Terceiro = 0, "V�rios", ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_FORNECE, ',
  'IF(vsp.Status IS NULL, -1, vsp.Status), vps.Nome NO_STATUS, DataHora, 0 OrdGGX,   ',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  'IF(vsi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vsi.Ficha)) NO_FICHA, ',
  'vsi.ReqMovEstq, vsi.Observ, vsi.PedItsLib, ',
  '1 Ativo   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_TMI + ' vsi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsi.GraGruX   ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY   ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vsi.Pallet   ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat   ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsi.Empresa   ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status   ',
  'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vsi.SerieFch ',
  'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vsi.Terceiro',
  'LEFT JOIN ' + TMeuDB + '.entidades cli ON cli.Codigo=vsp.CliStat  ',
  'WHERE vsi.Controle IN (' + SQL_ListaControles() + ') ',
  //SQL_Empresa,
  '; ',
  'SELECT er4.* ',
  'FROM ' + FTXImpIMEI + ' er4 ',
  'ORDER BY er4.Controle; ',
  '']);
  if QrEstqR4.RecordCount = 0 then
    Geral.MB_Info('Nenhum IME-I foi encontrado na tabela ativa!');
  //
  case FTXImpImeiKind of
    viikOrdemOperacao: Relatorio := frxTEX_FAXAO_025_02_A;
    viikOPPreenchida:
    begin
      TX_PF.ReopenTXIndPrcDst(QrTXPWEDst, QrEstqR4MovimCod.Value, CtrlNxt,
      TemIMEIMrt, TEstqMovimNiv.eminDestWEnd);
      //
      TX_PF.ReopenTXIndDesclDst(QrTXDesclDst, QrEstqR4Codigo.Value,
      QrEstqR4Controle.Value, (*Pallet*)0, TemIMEIMrt);

      Relatorio := frxTEX_FAXAO_025_02_B;
    end;
    viikOPaPreencher: Relatorio := frxTEX_FAXAO_025_05;
    viikProcCurt:
    begin
      //VS_EFD_ICMS_IPI.
      ProjGroup_PF.ReopenTXIndPrcOriIMEI(QrTXCurOriIMEI, QrEstqR4MovimCod.Value,
      CtrlNxt, TemIMEIMrt, eminSorcCur, SQL_Limit);
      //
      Relatorio := frxTEX_FAXAO_025_02_C;
    end;
    else // viik1
       Relatorio := frxTEX_FAXAO_025_01;
  end;
  case FTXImpImeiKind of
    viikOPPreenchida:
    begin
      MyObjects.frxDefineDataSets(Relatorio, [
      DModG.frxDsDono,
      frxDsEstqR4,
      frxDsOld01,
      frxDsOld02,
      frxDsOld03,
      frxDsOld04,
      frxDsOld05,
      frxDsTXPWEDst,
      frxDsTXDesclDst
      ]);
    end;
    viikProcCurt:
    begin
      MyObjects.frxDefineDataSets(Relatorio, [
      DModG.frxDsDono,
      frxDsEstqR4,
      frxDsOld01,
      frxDsOld02,
      frxDsOld03,
      frxDsOld04,
      frxDsOld05,
      frxDsTXCurOriIMEI
      ]);
    end;
    else
    begin
      MyObjects.frxDefineDataSets(Relatorio, [
      DModG.frxDsDono,
      frxDsEstqR4,
      frxDsOld01,
      frxDsOld02,
      frxDsOld03,
      frxDsOld04,
      frxDsOld05
      ]);
    end;
  end;
  MyObjects.frxMostra(Relatorio, 'Romaneio de Artigo de Ribeira',
    'IMEI_' + Geral.FF0(FControles[0]));
end;

procedure TFmTXImpIMEI.ImprimeOPsAbertasMul(RG18Ordem1_ItemIndex,
  RG18Ordem2_ItemIndex, RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
  MovimIDs: array of TEstqMovimID; Ck18Cor_Checked, Ck18Especificos_Checked:
  Boolean; Ed18Especificos_Text: String; Ed18Fornecedor_ValueVariant: Integer);
const
  Ordens: array[0..3] of String = ('NO_CliVenda', 'NO_FornecMO',
    'NO_PRD_TAM_COR', 'Codigo');
  Tituls: array[0..3] of String = ('Cliente', 'Fornecedor MO',
    'Artigo', 'Ordem');
var
  I: Integer;
  //Titulo,
  Campo, TitGru, LetraOX, SQL_IDs, SQLNivs, ATT_MovimID, ATT_MovimNiv,
  SQL_IMEIS, SQL_FornecMO: String;
  //MovimNiv: TEstqMovimNiv;
  MovimID: TEstqMovimID;
  Cor1, Cor2: TColor;
begin
  SQL_IDs := '';
  SQLNivs := '';
  SQL_FornecMO := '';
  if Ck18Especificos_Checked and (Trim(Ed18Especificos_Text) <> '') then
    SQL_IMEIS := 'AND vmi.MovimCod IN (' + Ed18Especificos_Text + ')'
  else
    SQL_IMEIS := 'AND vmi.SdoVrtPeca>0  ';
  if Ed18Fornecedor_ValueVariant <> 0 then
    SQL_FornecMO := 'AND vmi.FornecMO=' + Geral.FF0(Ed18Fornecedor_ValueVariant)
  else
    SQL_FornecMO := '';
  for I := Low(MovimIDs) to High(MovimIDs) do
  begin
    MovimID := MovimIDs[I];
    if SQL_IDs <> '' then
    begin
      SQL_IDs := SQL_IDs + ',';
      SQLNivs := SQLNivs + ',';
    end;
    SQL_IDs := SQL_IDs + Geral.FF0(Integer(MovimID));
    //
    case MovimID of
      (*
      emidAjuste: ;
      emidCompra: ;
      emidVenda: ;
      emidReclasWE: ;
      emidBaixa: ;
      emidIndsWE: ;
      emidIndsXX: ;
      emidClassArtXXUni: ;
      emidReclasXXUni: ;
      emidForcado: ;
      emidSemOrigem: ;
      *)
      emidEmOperacao:
      begin
        LetraOX := 'O';
        //Titulo  := 'Ordens de Opera��o Abertas';
        //MovimNiv := TEstqMovimNiv.eminEmOperInn; // 8
        SQLNivs := SQLNivs + Geral.FF0(Integer(TEstqMovimNiv.eminEmOperInn));
      end;
      (*
      emidResiduoReclas: ;
      emidInventario: ;
      emidClassArtXXMul: ;
      emidPreReclasse: ;
      emidEntradaPlC: ;
      emidExtraBxa: ;
      emidSaldoAnterior: ;
      *)
      emidEmProcWE:
      begin
        LetraOX := 'P';
        //Titulo  := 'Ordens de Produ��o Abertas';
        //MovimNiv := TEstqMovimNiv.eminEmWEndInn; // 21
        SQLNivs := SQLNivs + Geral.FF0(Integer(TEstqMovimNiv.eminEmWEndInn));
      end;
      (*
      emidFinished: ;
      emidDevolucao: ;
      emidRetrabalho: ;
      emidGeraSubProd: ;
      emidReclasXXMul: ;
      *)
      else
      begin
        LetraOX := '?';
        //Titulo  := 'Ordens de ??????? Abertas';
      end;
    end;
  end;

  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOPs, Dmod.MyDB, [
  'SELECT CASE vmi.MovimID  ',
  '  WHEN 11 THEN ope.NFeRem ',
  '  WHEN 19 THEN pwe.NFeRem ',
  '  ELSE -1.000 END NFeRem,  ',
  'vmi.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliVenda, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
  'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) SdoIntei, ',
  'IF(vmi.Terceiro <> 0, ',
  '  IF(fmp.Tipo=0, fmp.RazaoSocial, fmp.Nome), ',
  '  mfc.Nome) NO_FORNECE, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM ' + CO_SEL_TAB_TMI + ' vmi  ',
  'LEFT JOIN gragrux     ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc     ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits   gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades   cli ON cli.Codigo=vmi.CliVenda ',
  'LEFT JOIN entidades   fmo ON fmo.Codigo=vmi.FornecMO ',
  'LEFT JOIN entidades   fmp ON fmp.Codigo=vmi.Terceiro',
  'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.TXMulFrnCab ',
  'LEFT JOIN gragruxcou  xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1     cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'LEFT JOIN vsopecab    ope ON ope.MovimCod=vmi.MovimCod ',
  'LEFT JOIN vspwecab    pwe ON pwe.MovimCod=vmi.MovimCod ',
  'WHERE vmi.MovimID IN (' + SQL_IDs + ') ', //11 ou 19
  'AND vmi.MovimNiv IN (' + SQLNivs + ') ', //8 ou 21
  'AND vmi.Empresa IN (' + VAR_LIB_EMPRESAS + ') ',
  SQL_IMEIS,
  SQL_FornecMO,
  'ORDER BY vmi.MovimID, ' +
  Ordens[RG18Ordem1_ItemIndex] + ', ' +
  Ordens[RG18Ordem2_ItemIndex] + ', ' +
  Ordens[RG18Ordem3_ItemIndex],
  '']);
  //Geral.MB_SQL(Self, QrOPs);
  //
  MyObjects.frxDefineDataSets(frxTEX_FAXAO_025_04, [
    DModG.frxDsDono,
    frxDsOPs
  ]);
(*
  if VarName = 'VARF_AGRUP1' then
    Value := RG04Agrupa.ItemIndex >= 1
  else
  if VarName = 'VARF_AGRUP2' then
    Value := RG04Agrupa.ItemIndex >= 2
  else
  GF001.Visible := <VARF_AGRUP1>;
  GH001.Condition := <VARF_GRUPO1>;
  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;
  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;

  GH002.Visible := <VARF_AGRUP2>;
  GF002.Visible := <VARF_AGRUP2>;
  GH002.Condition := <VARF_GRUPO2>;
  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;
  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;
*)
  frxTEX_FAXAO_025_04.Variables['VARF_AGRUP1']   := RG18Agrupa_ItemIndex >= 1;
  frxTEX_FAXAO_025_04.Variables['VARF_AGRUP2']   := RG18Agrupa_ItemIndex >= 2;

  Campo  := Ordens[RG18Ordem1_ItemIndex];
  TitGru := Tituls[RG18Ordem1_ItemIndex];
  frxTEX_FAXAO_025_04.Variables['VARF_GRUPO1']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxTEX_FAXAO_025_04.Variables['VARF_HEADR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxTEX_FAXAO_025_04.Variables['VARF_FOOTR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  Campo  := Ordens[RG18Ordem2_ItemIndex];
  TitGru := Tituls[RG18Ordem2_ItemIndex];
  frxTEX_FAXAO_025_04.Variables['VARF_GRUPO2']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxTEX_FAXAO_025_04.Variables['VARF_HEADR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxTEX_FAXAO_025_04.Variables['VARF_FOOTR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  frxTEX_FAXAO_025_04.Variables['VARF_LETRA_OX'] := QuotedStr(LetraOX);
  //frxTEX_FAXAO_025_04.Variables['VARF_TITULO']   := QuotedStr(Titulo);

  //
  if Ck18Cor_Checked then
  begin
    Cor1 := $00E6C29B;
    Cor2 := $00B4E0C6;
  end else
  begin
    Cor1 := clWhite;
    Cor2 := clWhite;
  end;
  (frxTEX_FAXAO_025_04.FindObject('Me_GH1') as TfrxMemoView).Color      := Cor1;
  (frxTEX_FAXAO_025_04.FindObject('Me_GH2') as TfrxMemoView).Color      := Cor2;
  (frxTEX_FAXAO_025_04.FindObject('MeGH_02_F01') as TfrxMemoView).Color := Cor1;
  (frxTEX_FAXAO_025_04.FindObject('MeMD_F01') as TfrxMemoView).Color    := Cor1;
  (frxTEX_FAXAO_025_04.FindObject('MeMD_F02') as TfrxMemoView).Color    := Cor2;
  (frxTEX_FAXAO_025_04.FindObject('MeFT_02_F01') as TfrxMemoView).Color := Cor1;
  (frxTEX_FAXAO_025_04.FindObject('MeFT_02_F02') as TfrxMemoView).Color := Cor2;
  (frxTEX_FAXAO_025_04.FindObject('MeFT_01') as TfrxMemoView).Color     := Cor1;
  //
  (frxTEX_FAXAO_025_04.FindObject('MeMD_F01') as TfrxMemoView).Visible  := RG18Agrupa_ItemIndex > 0;
  (frxTEX_FAXAO_025_04.FindObject('MeMD_F02') as TfrxMemoView).Visible  := RG18Agrupa_ItemIndex > 1;
  //
  MyObjects.frxMostra(frxTEX_FAXAO_025_04, 'Ordens em Aberto', 'OPsEmAberto');
end;

procedure TFmTXImpIMEI.ImprimeOPsAbertasUni(RG18Ordem1_ItemIndex,
  RG18Ordem2_ItemIndex, RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
  MovimID: TEstqMovimID);
const
  Ordens: array[0..3] of String = ('NO_CliVenda', 'NO_FornecMO',
    'NO_PRD_TAM_COR', 'Codigo');
  Tituls: array[0..3] of String = ('Cliente', 'Fornecedor MO',
    'Artigo', 'Ordem');
var
  Campo, TitGru, LetraOX, Titulo, ATT_MovimID, ATT_MovimNiv: String;
  MovimNiv: TEstqMovimNiv;
begin
  case MovimID of
    (*
    emidAjuste: ;
    emidCompra: ;
    emidVenda: ;
    emidReclasWE: ;
    emidBaixa: ;
    emidIndsWE: ;
    emidIndsXX: ;
    emidClassArtXXUni: ;
    emidReclasXXUni: ;
    emidForcado: ;
    emidSemOrigem: ;
    *)
    emidEmOperacao:
    begin
      LetraOX := 'O';
      Titulo  := 'Ordens de Opera��o Abertas';
      MovimNiv := TEstqMovimNiv.eminEmOperInn; // 8
    end;
    (*
    emidResiduoReclas: ;
    emidInventario: ;
    emidClassArtXXMul: ;
    emidPreReclasse: ;
    emidEntradaPlC: ;
    emidExtraBxa: ;
    emidSaldoAnterior: ;
    *)
    emidEmProcWE:
    begin
      LetraOX := 'P';
      Titulo  := 'Ordens de Produ��o Abertas';
      MovimNiv := TEstqMovimNiv.eminEmWEndInn; // 21
    end;
    (*
    emidFinished: ;
    emidDevolucao: ;
    emidRetrabalho: ;
    emidGeraSubProd: ;
    emidReclasXXMul: ;
    *)
    else
    begin
      LetraOX := '?';
      Titulo  := 'Ordens de ??????? Abertas';
    end;
  end;
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
(*
  'SELECT vmi.*,   ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliVenda,  ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FornecMO,  ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros,  ',
  'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) SdoIntei,  ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    ',
  'NO_PRD_TAM_COR  ',
  '  ',
  'FROM ' + CO_SEL_TAB_TMI + ' vmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'LEFT JOIN entidades  cli ON cli.Codigo=vmi.CliVenda  ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.FornecMO  ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrOPs, Dmod.MyDB, [
  'SELECT vmi.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliVenda, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
  'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) SdoIntei, ',
  'IF(vmi.Terceiro <> 0, ',
  '  IF(fmp.Tipo=0, fmp.RazaoSocial, fmp.Nome), ',
  '  mfc.Nome) NO_FORNECE, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM ' + CO_SEL_TAB_TMI + ' vmi  ',
  'LEFT JOIN gragrux     ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc     ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits   gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades   cli ON cli.Codigo=vmi.CliVenda ',
  'LEFT JOIN entidades   fmo ON fmo.Codigo=vmi.FornecMO ',
  'LEFT JOIN entidades   fmp ON fmp.Codigo=vmi.Terceiro',
  'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.TXMulFrnCab ',
  'LEFT JOIN gragruxcou  xco ON xco.GraGruX=ggx.Controle ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(MovimID)), //11 ou 19
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)), //8 ou 21
  'AND vmi.Empresa IN (' + VAR_LIB_EMPRESAS + ')',
  'AND vmi.SdoVrtPeca>0  ',
  'ORDER BY ' +
  Ordens[RG18Ordem1_ItemIndex] + ', ' +
  Ordens[RG18Ordem2_ItemIndex] + ', ' +
  Ordens[RG18Ordem3_ItemIndex],
  '']);
  //
  MyObjects.frxDefineDataSets(frxTEX_FAXAO_025_03, [
    DModG.frxDsDono,
    frxDsOPs
  ]);
(*
  if VarName = 'VARF_AGRUP1' then
    Value := RG04Agrupa.ItemIndex >= 1
  else
  if VarName = 'VARF_AGRUP2' then
    Value := RG04Agrupa.ItemIndex >= 2
  else
  GF001.Visible := <VARF_AGRUP1>;
  GH001.Condition := <VARF_GRUPO1>;
  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;
  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;

  GH002.Visible := <VARF_AGRUP2>;
  GF002.Visible := <VARF_AGRUP2>;
  GH002.Condition := <VARF_GRUPO2>;
  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;
  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;
*)
  frxTEX_FAXAO_025_03.Variables['VARF_AGRUP1']   := RG18Agrupa_ItemIndex >= 1;
  frxTEX_FAXAO_025_03.Variables['VARF_AGRUP2']   := RG18Agrupa_ItemIndex >= 2;

  Campo  := Ordens[RG18Ordem1_ItemIndex];
  TitGru := Tituls[RG18Ordem1_ItemIndex];
  frxTEX_FAXAO_025_03.Variables['VARF_GRUPO1']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxTEX_FAXAO_025_03.Variables['VARF_HEADR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxTEX_FAXAO_025_03.Variables['VARF_FOOTR1']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  Campo  := Ordens[RG18Ordem2_ItemIndex];
  TitGru := Tituls[RG18Ordem2_ItemIndex];
  frxTEX_FAXAO_025_03.Variables['VARF_GRUPO2']   := ''''+'frxDsOPs."'+Campo+'"''';
  frxTEX_FAXAO_025_03.Variables['VARF_HEADR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  frxTEX_FAXAO_025_03.Variables['VARF_FOOTR2']   := ''''+TitGru+': [frxDsOPs."'+Campo+'"]''';
  //
  frxTEX_FAXAO_025_03.Variables['VARF_LETRA_OX'] := QuotedStr(LetraOX);
  frxTEX_FAXAO_025_03.Variables['VARF_TITULO']   := QuotedStr(Titulo);

  MyObjects.frxMostra(frxTEX_FAXAO_025_03, Titulo);
end;

procedure TFmTXImpIMEI.QrEstqR4AfterScroll(DataSet: TDataSet);
const
  TemIMEIMrt = 0;  // Ver o que fazer !
begin
  TX_PF.ReopenTXGerArtSrc(QrTXGerArtOld, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, '', eminSorcCurtiXX);
  //
  TX_PF.ReopenTXGerArtSrc(QrOld01, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 0,1 ', eminSorcCurtiXX);
  TX_PF.ReopenTXGerArtSrc(QrOld02, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 1,1 ', eminSorcCurtiXX);
  TX_PF.ReopenTXGerArtSrc(QrOld03, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 2,1 ', eminSorcCurtiXX);
  TX_PF.ReopenTXGerArtSrc(QrOld04, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 3,1 ', eminSorcCurtiXX);
  TX_PF.ReopenTXGerArtSrc(QrOld05, QrEstqR4MovimCod.Value, 0, TemIMEIMrt, 'LIMIT 4,1 ', eminSorcCurtiXX);
end;

procedure TFmTXImpIMEI.QrEstqR4CalcFields(DataSet: TDataSet);
var
  Tipo: Integer;
  Texto: String;
begin
  Tipo := -1;
  Texto := '?????';
  QrEstqR4NO_ACAO_COURO.Value := sEstqMovimID[QrEstqR4MovimID.Value];
  QrEstqR4NO_TIPO_COURO.Value := sEstqMovimIDLong[QrEstqR4MovimID.Value];
{20200920
  Tipo := iEstqMovimID[QrEstqR4MovimID.Value];
}
  case Tipo of
    1:
    begin
      QrEstqR4TERCEIRO.Value := QrEstqR4Fornece.Value;
      QrEstqR4NO_FRNCALC.Value := QrEstqR4NO_FORNECE.Value;
      QrEstqR4TIPO_TERCEIRO.Value := 'Proced�ncia';
    end;
    2:
    begin
      QrEstqR4TERCEIRO.Value := QrEstqR4Cliente.Value;
      QrEstqR4NO_FRNCALC.Value := QrEstqR4NO_CLIENTE.Value;
      QrEstqR4TIPO_TERCEIRO.Value := 'Cliente';
    end;
    else
    begin
      QrEstqR4TERCEIRO.Value := 0;
      QrEstqR4NO_FRNCALC.Value := '';
      QrEstqR4TIPO_TERCEIRO.Value := '';
    end;
  end;
  QrEstqR4SdoVrtArP2.Value := Geral.ConverteArea(QrEstqR4SdoVrtArM2.Value, ctM2toP2, cfQuarto);
end;

procedure TFmTXImpIMEI.QrTXPWEDstCalcFields(DataSet: TDataSet);
const
  Controle = 0;
  SQL_Limit = '';
  TemIMEIMrt = 1;
begin
  TX_PF.ReopenTXIndPrcBxa(QrTXPWEBxa, QrEstqR4MovimCod.Value,
  QrTXPWEDstMovimTwn.Value, Controle, TemIMEIMrt, eminEmWEndBxa, SQL_Limit);
  // Area WB
  QrTXPWEDstAreaM2WB.Value := QrTXPWEBxaAreaM2.Value;
  // Rendimento FI sobre WB
  if QrTXPWEDstAreaM2WB.Value = 0 then
    QrTXPWEDstPercRendim.Value := 0
  else
    QrTXPWEDstPercRendim.Value := (QrTXPWEDstAreaM2.Value +
    QrTXPWEDstAreaM2WB.Value) / -QrTXPWEDstAreaM2WB.Value * 100;
end;

function TFmTXImpIMEI.SQL_ListaControles(): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(FControles);
  for I := Low(FControles) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(FControles[I]);
    if I < N then
      Result := Result + ',';
  end;
end;

end.
