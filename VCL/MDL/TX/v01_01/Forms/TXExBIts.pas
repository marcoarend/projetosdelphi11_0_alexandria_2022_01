unit TXExBIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Grids, Vcl.DBGrids, UnProjGroup_Consts, UnAppEnums,
  UnTX_Jan;

type
  TFmTXExBIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrTXPallet: TmySQLQuery;
    DsTXPallet: TDataSource;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletNome: TWideStringField;
    BtExclui: TBitBtn;
    QrGraGruXGraGruY: TIntegerField;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    LaQtde: TLabel;
    Label4: TLabel;
    SBPallet: TSpeedButton;
    Label9: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdQtde: TdmkEdit;
    EdPsqPallet: TdmkEditCB;
    CBPsqPallet: TdmkDBLookupComboBox;
    EdObserv: TdmkEdit;
    EdValorT: TdmkEdit;
    Label14: TLabel;
    EdSrcMovID: TdmkEdit;
    Label10: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    QrTXMovIts: TmySQLQuery;
    QrTXMovItsNO_Pallet: TWideStringField;
    QrTXMovItsCodigo: TIntegerField;
    QrTXMovItsControle: TIntegerField;
    QrTXMovItsMovimCod: TIntegerField;
    QrTXMovItsMovimNiv: TIntegerField;
    QrTXMovItsEmpresa: TIntegerField;
    QrTXMovItsTerceiro: TIntegerField;
    QrTXMovItsMovimID: TIntegerField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TIntegerField;
    QrTXMovItsGraGruX: TIntegerField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsSrcMovID: TIntegerField;
    QrTXMovItsSrcNivel1: TIntegerField;
    QrTXMovItsSrcNivel2: TIntegerField;
    QrTXMovItsLk: TIntegerField;
    QrTXMovItsDataCad: TDateField;
    QrTXMovItsDataAlt: TDateField;
    QrTXMovItsUserCad: TIntegerField;
    QrTXMovItsUserAlt: TIntegerField;
    QrTXMovItsAlterWeb: TSmallintField;
    QrTXMovItsAtivo: TSmallintField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsNO_FORNECE: TWideStringField;
    QrTXMovItsValorT: TFloatField;
    DsTXMovIts: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    EdSrcGGX: TdmkEdit;
    Label13: TLabel;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsMovimTwn: TIntegerField;
    QrTXMovItsCliVenda: TIntegerField;
    QrTXMovItsSrcGGX: TIntegerField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsFornecMO: TIntegerField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsCustoMOTot: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TIntegerField;
    QrTXMovItsDstNivel1: TIntegerField;
    QrTXMovItsDstNivel2: TIntegerField;
    QrTXMovItsDstGGX: TIntegerField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsAptoUso: TSmallintField;
    QrTXMovItsTpCalcAuto: TIntegerField;
    RGPosiNega: TRadioGroup;
    EdIMEI: TdmkEdit;
    Label15: TLabel;
    QrTXMovItsTXMulFrnCab: TIntegerField;
    QrTXMovItsClientMO: TIntegerField;
    QrTXMovItsStqCenLoc: TIntegerField;
    QrTXMovItsSerieTal: TIntegerField;
    QrTXMovItsTalao: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdPsqPalletRedefinido(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGPosiNegaClick(Sender: TObject);
    procedure EdIMEIRedefinido(Sender: TObject);
    procedure EdQtdeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FUltGGX: Integer;

    //
    procedure CopiaTotaisIMEI();
    procedure ReopenTXExbIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure HabilitaInclusao();
    procedure ReopenTXMovIts();
    procedure ReopenTXPallet();

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FDataHora: TDateTime;
    FEmpresa, FAntSrcNivel2: Integer;
    //FDsCab: TDataSource;
  end;

  var
  FmTXExBIts: TFmTXExBIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal, ModuleGeral, UnTX_PF, TXExbCab, AppListas;

{$R *.DFM}

procedure TFmTXExBIts.BtExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := EdControle.ValueVariant;
  if Geral.MB_Pergunta('Confirme a exclus�o do IME-I' +
  Geral.FF0(Controle) + '?') = ID_YES then
  begin
    TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(Controle,
    Integer(TEstqMotivDel.emtdWetCurti079), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
    TX_PF.AtualizaSaldoIMEI(Controle, False);
    //
    Close;
  end;
end;

procedure TFmTXExBIts.BtOKClick(Sender: TObject);
const
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOUni = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGer     = 0;
  AptoUso    = 0;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  ItemNFe    = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, StqCenCad, GraGruX, Terceiro,
  SrcNivel1, SrcNivel2, GraGruY, SrcGGX, TXMulFrnCab, ClientMO, FornecMO,
  StqCenLoc, SerieTal, Talao: Integer;
  Qtde, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  SrcMovID       := EdSrcMovID.ValueVariant;
  SrcNivel1      := EdSrcNivel1.ValueVariant;
  SrcNivel2      := EdSrcNivel2.ValueVariant;
  SrcGGX         := EdSrcGGX.ValueVariant;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClientMO       := QrTXMovItsClientMO.Value;
  FornecMO       := QrTXMovItsFornecMO.Value;
  if FornecMO = 0 then
    FornecMO := QrTXMovItsEmpresa.Value;
  StqCenLoc      := QrTXMovItsStqCenLoc.Value;
  Terceiro       := QrTXMovItsTerceiro.Value;
  TXMulFrnCab    := QrTXMovItsTXMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidExtraBxa;
  MovimNiv       := eminSemNiv;
  Pallet         := QrTXMovItsPallet.Value;
  GraGruX        := QrTXMovItsGraGruX.Value;
  FUltGGX        := GraGruX;
  Qtde           := -EdQtde.ValueVariant;
  ValorT         := -EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Marca          := QrTXMovItsMarca.Value;
  GraGruY        := QrGraGruXGraGruY.Value;
  //
  SerieTal       := QrTXMovItsSerieTal.Value;
  Talao          := QrTXMovItsTalao.Value;
  //
  if TX_PF.TXFic(GraGruX, Empresa, Terceiro, Pallet, Qtde, ValorT, EdGraGruX,
  EdPallet, EdQtde, EdValorT, ExigeFornecedor, GraGruY, nil, 0, 0, nil, nil) then
    Exit;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, Controle, CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei059(*Baixa extra 1/2*)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txexbcab', MovimCod);
    TX_PF.AtualizaSaldoIMEI(FAntSrcNivel2, False);
    TX_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FmTXExbCab.LocCod(Codigo, Codigo);
    ReopenTXExbIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdQtde.ValueVariant        := 0;
      EdValorT.ValueVariant      := 0;
      EdPsqPallet.ValueVariant   := 0;
      CBPsqPallet.KeyValue       := 0;
      EdObserv.Text              := '';
      EdSrcMovID.ValueVariant    := 0;
      EdSrcNivel1.ValueVariant   := 0;
      EdSrcNivel2.ValueVariant   := 0;
      EdSrcGGX.ValueVariant      := 0;
      //
      ReopenTXPallet();
      ReopenTXMovIts();
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTXExBIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXExBIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXExBIts.CopiaTotaisIMEI;
var
  Valor: Double;
begin
  EdQtde.ValueVariant  := QrTXMovItsSdoVrtQtd.Value;
  if QrTXMovItsQtde.Value > 0 then
    Valor := QrTXMovItsSdoVrtQtd.Value *
    (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value);
  EdValorT.ValueVariant := Valor;
end;

procedure TFmTXExBIts.DBGrid1DblClick(Sender: TObject);
var
  Valor: Double;
begin
  EdSrcMovID.ValueVariant  := QrTXMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrTXMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrTXMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrTXMovItsGraGruX.Value;
  //
  try
    EdQtde.SetFocus;
  except
    //
  end;
end;

procedure TFmTXExBIts.EdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
end;

procedure TFmTXExBIts.EdEmpresaRedefinido(Sender: TObject);
begin
  ReopenTXPallet();
end;

procedure TFmTXExBIts.EdFornecedorChange(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXExBIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXExBIts.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenTXPallet();
  ReopenTXMovIts();
end;

procedure TFmTXExBIts.EdIMEIRedefinido(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXExBIts.EdPsqPalletRedefinido(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXExBIts.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXExBIts.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXExBIts.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXExBIts.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Qtde, Valor: Double;
begin
  if Key = VK_F5 then
  begin
    Qtde  := EdQtde.ValueVariant;
    Valor := 0;
    if QrTXMovItsQtde.Value > 0 then
      Valor := Qtde * (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value);
    //
    EdValorT.ValueVariant := Valor;
  end;
end;

procedure TFmTXExBIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXExBIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_ALL_TX + ') ');
end;

procedure TFmTXExBIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXExBIts.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    //(EdSrcMovID.ValueVariant <> 0) and  MovimID = 13 >> Inventario
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmTXExBIts.ImgTipoChange(Sender: TObject);
begin
  BtExclui.Visible := ImgTipo.SQLType = stUpd;
end;

procedure TFmTXExBIts.ReopenTXMovIts();
var
  Controle, GraGruX, Empresa, Pallet: Integer;
  SQL_Controle, SQL_Pallet, SQL_GraGruX, Sinal: String;
begin
  Empresa    := FEmpresa;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then
  begin
    Close;
    Exit;
  end;
  SQL_Pallet := '';
  Pallet     := EdPsqPallet.ValueVariant;
  GraGruX    := EdGraGruX.ValueVariant;
  Controle   := EdIMEI.ValueVariant;
  if Pallet <> 0 then
    SQL_Pallet := 'AND tmi.Pallet=' + Geral.FF0(Pallet);
  if GraGruX <> 0 then
    SQL_GraGruX  := 'AND tmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := '';
  if Controle <> 0 then
    SQL_Controle  := 'AND tmi.Controle=' + Geral.FF0(Controle)
  else
    SQL_Controle := '';
  if RGPosiNega.ItemIndex = 0 then
    Sinal := '>'
  else
    Sinal := '<';
  //
  //DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa := FEmpresa;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, tmi.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'LEFT JOIN txpalleta pal ON pal.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=tmi.Terceiro',
  'WHERE tmi.Empresa=' + Geral.FF0(Empresa),
  SQL_GraGruX,
  'AND (tmi.SdoVrtQtd ' + Sinal + ' 0 ',
  ') ',
  SQL_Controle,
  SQL_Pallet,
  'ORDER BY DataHora, Pallet',
  '']);
  //
end;

procedure TFmTXExBIts.ReopenTXExbIts(Controle: Integer);
begin
  if (FQrIts <> nil)
  and (FQrCab <> nil) then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXExBIts.ReopenTXPallet();
var
  Empresa, GraGruX: Integer;
  SQL_GraGruX, Sinal: String;
begin
  Empresa    := FEmpresa;
  GraGruX := EdGraGruX.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then
  begin
    Close;
    Exit;
  end;
  if GraGruX <> 0 then
    SQL_GraGruX  := '  AND tmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := '';
  if RGPosiNega.ItemIndex = 0 then
    Sinal := '>'
  else
    Sinal := '<';
  //
  //DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Empresa := FEmpresa;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPallet, Dmod.MyDB, [
  'SELECT pal.Codigo, pal.Nome ',
  'FROM txpalleta pal, ' + CO_SEL_TAB_TMI + ' tmi ',
  'WHERE pal.Codigo=tmi.Pallet ',
  'AND (',
  '  (tmi.Qtde>=0  AND tmi.SdoVrtQtd>0 ) ',
  ')',
  'AND tmi.Empresa=' + Geral.FF0(Empresa),
  SQL_GraGruX,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmTXExBIts.RGPosiNegaClick(Sender: TObject);
begin
  ReopenTXPallet();
  ReopenTXMovIts();
end;

procedure TFmTXExBIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPsqPallet, CBPsqPallet, QrTXPallet, VAR_CADASTRO);
end;

procedure TFmTXExBIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
