unit TXIndInn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup, Vcl.Menus,
  UnProjGroup_Consts, dmkCheckBox, Vcl.ComCtrls, dmkEditDateTimePicker,
  UnAppEnums;

type
  TFmTXIndInn = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    Label9: TLabel;
    EdObserv: TdmkEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    QrTXPallet: TmySQLQuery;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletNome: TWideStringField;
    QrTXPalletLk: TIntegerField;
    QrTXPalletDataCad: TDateField;
    QrTXPalletDataAlt: TDateField;
    QrTXPalletUserCad: TIntegerField;
    QrTXPalletUserAlt: TIntegerField;
    QrTXPalletAlterWeb: TSmallintField;
    QrTXPalletAtivo: TSmallintField;
    QrTXPalletEmpresa: TIntegerField;
    QrTXPalletNO_EMPRESA: TWideStringField;
    QrTXPalletStatus: TIntegerField;
    QrTXPalletCliStat: TIntegerField;
    QrTXPalletGraGruX: TIntegerField;
    QrTXPalletNO_CLISTAT: TWideStringField;
    QrTXPalletNO_PRD_TAM_COR: TWideStringField;
    QrTXPalletNO_STATUS: TWideStringField;
    DsTXPallet: TDataSource;
    Label20: TLabel;
    LaVSRibCla: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet1: TSpeedButton;
    QrTXPedIts: TmySQLQuery;
    QrTXPedItsNO_PRD_TAM_COR: TWideStringField;
    QrTXPedItsControle: TIntegerField;
    DsTXPedIts: TDataSource;
    EdPedItsFin: TdmkEditCB;
    Label48: TLabel;
    CBPedItsFin: TdmkDBLookupComboBox;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    PMPallet: TPopupMenu;
    Criar1: TMenuItem;
    Gerenciamento1: TMenuItem;
    CkEncerraPallet: TdmkCheckBox;
    SBNewPallet: TSpeedButton;
    MeAviso: TMemo;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    EdCustoMOUni: TdmkEdit;
    Label4: TLabel;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    EdCustoMOTot: TdmkEdit;
    Label8: TLabel;
    Label11: TLabel;
    EdValorMP: TdmkEdit;
    EdValorT: TdmkEdit;
    Label10: TLabel;
    Label13: TLabel;
    EdSerieTal: TdmkEditCB;
    CBSerieTal: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdTalao: TdmkEdit;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure Criar1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure EdCustoMOUniRedefinido(Sender: TObject);
    procedure EdValorMPRedefinido(Sender: TObject);
    procedure EdTalaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FUltGGX, FGraGruX: Integer;
    //
    procedure CalculaValorT();
    procedure ReopenVSInnIts(Controle: Integer);

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FFornecMO, FStqCenLoc: Integer;
    FValM2: Double;
    FPallOnEdit: array of Integer;
  end;

  var
  FmTXIndInn: TFmTXIndInn;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXIndCab, Principal, UnTX_PF, AppListas, ModuleGeral, UnTX_Jan;

{$R *.DFM}

procedure TFmTXIndInn.BtOKClick(Sender: TObject);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CliVenda   = 0;
  //MovimTwn   = 0;
  //Pallet     = 0;
  QtdGer     = 0;
  AptoUso    = 1;
  //
  Fornecedor = 0;
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  //PedItsFin = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  TXMulFrnCab = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, GraGruX, MovimTwn, Pallet,
  PedItsFin, StqCenLoc, ReqMovEstq, ClientMO, FornecMO, PalToClose,
  SerieTal, Talao: Integer;
  Qtde, ValorT, CustoMOUni, CustoMOTot, ValorMP: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
  //
begin
  if MyObjects.FIC(TPData.Date<2, TPData, 'Data/hora n�o definida!') then Exit;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  FornecMO       := FFornecMO;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  MovimID        := emidEmIndstrlzc;
  MovimNiv       := eminIndzcInn;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Qtde           := EdQtde.ValueVariant;

  CustoMOUni     := EdCustoMOUni.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;

  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Marca          := EdMarca.Text;
  Pallet         := EdPallet.ValueVariant;
  PedItsFin      := EdPedItsFin.ValueVariant;
  //
  SerieTal       := EdSerieTal.ValueVariant;
  Talao          := EdTalao.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  // Pode ser raspa por peso sem custo!
  if TX_PF.TXFic(GraGruX, Empresa, Fornecedor, Pallet, Qtde, ValorT,
    EdGraGruX, EdPallet, EdQtde, EdValorT, ExigeFornecedor,
    CO_GraGruY_2048_TXCadInd, EdStqCenLoc, SerieTal, Talao, EdSerieTal, EdTalao)
  then
    Exit;
  if MyObjects.FIC(Pallet = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
  if MyObjects.FIC(SerieTal = 0, EdSerieTal, 'Informe a S�rie de Tal�o!') then
    Exit;
  if MyObjects.FIC(Talao = 0, EdTalao, 'Informe o n�mero do Tal�o!') then
    Exit;
  //
  PalToClose := Pallet;
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Qtde, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin,
  PedItsVda, ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO, QtdAnt,
  SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei003) then
  begin
    FmTXIndCab.DistribuicaoCusto();
    TX_PF.AtualizaSaldoIMEI(Controle, True);
    TX_PF.AtualizaTXPedIts_Fin(PedItsFin);
    //
    // Nao se aplica. Calcula com function propria a seguir.
    //TX_PF.AtualizaTotaisVSXxxCab('txindcab', MovimCod);
    TX_PF.AtualizaTotaisTXIndCab(FmTXIndCab.QrTXIndCabMovimCod.Value);
    FmTXIndCab.AtualizaNFeItens();
    //
    if CkEncerraPallet.Checked then
      TX_PF.EncerraPalletSimples(PalToClose, FEmpresa, FClientMO, QrTXPallet, FPallOnEdit);
    TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmIndstrlzc, MovimCod);
    FmTXIndCab.LocCod(Codigo, Codigo);
    ReopenVSInnIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdQtde.ValueVariant        := 0;
      EdValorMP.ValueVariant     := 0;
      EdCustoMOUni.ValueVariant  := 0;
      EdCustoMOTot.ValueVariant  := 0;
      EdValorT.ValueVariant      := 0;
      EdObserv.Text              := '';
      EdMarca.ValueVariant       := '';
      //
      EdGraGruX.Enabled          := True;
      CBGraGruX.Enabled          := True;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTXIndInn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXIndInn.CalculaValorT();
var
  Qtde, CustoMOUni, CustoMOTot, ValorMP, ValorT: Double;
begin
  Qtde       := EdQtde.ValueVariant;
  CustoMOUni := EdCustoMOUni.ValueVariant;
  ValorMP    := EdValorMP.ValueVariant;
  //
  CustoMOTot := Qtde * CustoMOUni;
  ValorT     := ValorMP + CustoMOTot;
  //
  EdCustoMOTot.ValueVariant := CustoMOTot;
  EdValorT.ValueVariant     := ValorT;
end;

procedure TFmTXIndInn.Criar1Click(Sender: TObject);
var
  GraGruX: Integer;
begin
{POIU
  GraGruX := EdGraGruX.ValueVariant;
  TX_PF.CadastraPalletRibCla(FEmpresa, FClientMO, EdPallet, CBPallet,
    QrTXPallet, emidEmOperacao, GraGruX);
}
end;

procedure TFmTXIndInn.EdCustoMOUniRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmTXIndInn.EdTalaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Talao: Integer;
begin
  if Key = VK_F4 then
    if TX_PF.ObtemProximoTalao(FEmpresa, EdSerieTal, Talao) then
      EdTalao.ValueVariant := Talao;
end;

procedure TFmTXIndInn.EdGraGruXChange(Sender: TObject);
begin
  FGraGruX := EdGraGruX.ValueVariant;
  EdPallet.ValueVariant := 0;
  CBPallet.KeyValue     := Null;
  if FGraGruX = 0 then
    QrTXPallet.Close
  else
    TX_PF.ReopenTXPallet(QrTXPallet, FEmpresa, FClientMO, FGraGruX, '', FPallOnEdit);
end;

procedure TFmTXIndInn.EdQtdeRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmTXIndInn.EdValorMPRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmTXIndInn.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXIndInn.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  SetLength(FPallOnEdit, 0);
  FUltGGX  := 0;
  FGraGruX := 0;
  //
  TX_PF.AbreTXSerTal(QrTXSerTal);
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_2048_TXCadInd));
  //
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmTXIndInn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXIndInn.Gerenciamento1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXPallet(EdPallet.ValueVariant);
end;

procedure TFmTXIndInn.ReopenVSInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXIndInn.SBNewPalletClick(Sender: TObject);
begin
  TX_PF.GeraNovoPallet(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  EdGraGruX, EdPallet, CBPallet, SBNewPallet, QrTXPallet, FPallOnEdit);
end;

procedure TFmTXIndInn.SbPallet1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, SbPallet1);
end;

{
object EdCustoMOTot: TdmkEdit
  Left = 260
  Top = 72
  Width = 112
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 5
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  QryCampo = 'CustoMOTot'
  UpdCampo = 'CustoMOTot'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
object Label8: TLabel
  Left = 260
  Top = 56
  Width = 62
  Height = 13
  Caption = '$ M.O. Total:'
  Enabled = False
end
object Label11: TLabel
  Left = 376
  Top = 56
  Width = 75
  Height = 13
  Caption = '$ Mat'#233'ria prima:'
  Enabled = False
end
object EdValorMP: TdmkEdit
  Left = 376
  Top = 72
  Width = 112
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 6
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  QryCampo = 'ValorMP'
  UpdCampo = 'ValorMP'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
  OnRedefinido = EdValorMPRedefinido
end
object EdValorT: TdmkEdit
  Left = 492
  Top = 72
  Width = 112
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 7
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  QryCampo = 'ValorT'
  UpdCampo = 'ValorT'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
object Label10: TLabel
  Left = 492
  Top = 56
  Width = 57
  Height = 13
  Caption = 'Custo Total:'
  Enabled = False
end
}
end.
