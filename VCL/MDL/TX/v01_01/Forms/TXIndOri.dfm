object FmTXIndOri: TFmTXIndOri
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-040 :: Origem de Artigo em Industraliza'#231#227'o'
  ClientHeight = 678
  ClientWidth = 860
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 539
    Width = 860
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 860
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 1
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimCod: TdmkEdit
      Left = 80
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MovimCod'
      UpdCampo = 'MovimCod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 860
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 812
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 764
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 440
        Height = 32
        Caption = 'Origem de Artigo em Industraliza'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 440
        Height = 32
        Caption = 'Origem de Artigo em Industraliza'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 440
        Height = 32
        Caption = 'Origem de Artigo em Industraliza'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 564
    Width = 860
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 856
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 261
        Height = 16
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 261
        Height = 16
        Caption = 'D'#234' um duplo clique no item a ser adicionado.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 608
    Width = 860
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 714
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 712
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAptos: TGroupBox
    Left = 0
    Top = 112
    Width = 860
    Height = 344
    Align = alClient
    Caption = ' Filtros: '
    TabOrder = 0
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 856
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaGraGruX: TLabel
        Left = 8
        Top = 0
        Width = 66
        Height = 13
        Caption = 'Mat'#233'ria-prima:'
      end
      object LaTerceiro: TLabel
        Left = 516
        Top = 0
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        Enabled = False
      end
      object EdGraGruX: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 64
        Top = 16
        Width = 445
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 1
        dmkEditCB = EdGraGruX
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTerceiro: TdmkEditCB
        Left = 516
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiroChange
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 572
        Top = 16
        Width = 153
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 3
        dmkEditCB = EdTerceiro
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 732
        Top = 2
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtReabreClick
      end
    end
    object DBG04Estq: TdmkDBGridZTO
      Left = 2
      Top = 57
      Width = 856
      Height = 285
      Align = alClient
      DataSource = DsEstque
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBG04EstqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'IMEI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pallet'
          Title.Caption = 'ID Pallet'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Pallet'
          Title.Caption = 'Pallet'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdGGX'
          Title.Caption = 'Ordem'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtQtd'
          Title.Caption = 'Quantidade'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SdoVrtVal'
          Title.Caption = 'Valor total'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Nome do terceiro'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieTal'
          Title.Caption = 'S'#233'rie'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Talao'
          Title.Caption = 'Tal'#227'o'
          Visible = True
        end>
    end
  end
  object PnGerar: TPanel
    Left = 0
    Top = 456
    Width = 860
    Height = 83
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    object GBGerar: TGroupBox
      Left = 0
      Top = 0
      Width = 860
      Height = 83
      Align = alClient
      Caption = ' Dados do item: '
      TabOrder = 0
      Visible = False
      object Label6: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object LaQtde: TLabel
        Left = 88
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label9: TLabel
        Left = 12
        Top = 60
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
      end
      object Label19: TLabel
        Left = 176
        Top = 16
        Width = 170
        Height = 13
        Caption = 'Material usado para emitir NF-e [F4]:'
      end
      object Label50: TLabel
        Left = 778
        Top = 16
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 32
        Width = 72
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 88
        Top = 32
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 88
        Top = 56
        Width = 765
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdGGXRcl: TdmkEditCB
        Left = 176
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGXRcl'
        UpdCampo = 'GGXRcl'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdGGXRclKeyDown
        DBLookupComboBox = CBGGXRcl
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGGXRcl: TdmkDBLookupComboBox
        Left = 236
        Top = 32
        Width = 539
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXRcl
        TabOrder = 3
        dmkEditCB = EdGGXRcl
        QryCampo = 'GGXRcl'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdReqMovEstq: TdmkEdit
        Left = 779
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 112
    Top = 192
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 112
    Top = 240
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 188
    Top = 192
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 188
    Top = 240
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    Left = 200
    Top = 340
    object QrEstoqueControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstoqueEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstoqueGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEstoqueGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstoqueNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEstoquePallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstoqueNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrEstoqueTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstoqueNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstoqueNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEstoqueNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Required = True
      Size = 0
    end
    object QrEstoqueDataHora: TWideStringField
      FieldName = 'DataHora'
      Required = True
      Size = 19
    end
    object QrEstoqueOrdGGX: TLargeintField
      FieldName = 'OrdGGX'
      Required = True
    end
    object QrEstoqueOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEstoqueCodiGGY: TIntegerField
      FieldName = 'CodiGGY'
      Required = True
    end
    object QrEstoqueNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEstoqueCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstoqueIMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object QrEstoqueIMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object QrEstoqueMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstoqueMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstoqueNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Required = True
      Size = 0
    end
    object QrEstoqueNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Required = True
      Size = 0
    end
    object QrEstoqueAtivo: TLargeintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEstoqueGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrEstoqueSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrEstoqueTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrEstoqueSdoVrtVal: TFloatField
      FieldName = 'SdoVrtVal'
    end
    object QrEstoqueSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrEstoqueTalao: TIntegerField
      FieldName = 'Talao'
    end
  end
  object DsEstque: TDataSource
    DataSet = QrEstoque
    Left = 200
    Top = 384
  end
  object QrGGXRcl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 36
    Top = 304
    object QrGGXRclGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXRclSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXRclCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXRclNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 36
    Top = 356
  end
end
