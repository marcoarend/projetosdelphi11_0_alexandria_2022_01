object FmTXImpPallet: TFmTXImpPallet
  Left = 0
  Top = 0
  Caption = 'TEX-FAXAO-026 :: Impress'#227'o de Pallet TX'
  ClientHeight = 592
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 281
        Height = 32
        Caption = 'Impress'#227'o de Pallet TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 281
        Height = 32
        Caption = 'Impress'#227'o de Pallet TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 281
        Height = 32
        Caption = 'Impress'#227'o de Pallet TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 522
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 478
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 430
    Align = alClient
    TabOrder = 3
    object DBGrid1: TDBGrid
      Left = 2
      Top = 15
      Width = 1004
      Height = 413
      Align = alClient
      DataSource = DsTXMovImp4
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Pallet'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ARTIGO'
          Title.Caption = 'Artigo'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Pe'#231'as'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieTal'
          Title.Caption = 'S'#233'rie Ficha'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Talao'
          Title.Caption = 'N'#250'mero Talao'
          Width = 62
          Visible = True
        end>
    end
  end
  object frxTEX_FAXAO_026_01_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxTEX_FAXAO_026_01_AGetValue
    Left = 52
    Top = 204
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370078740000100000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 438.425480000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 192.756030000000000000
          Top = 491.441250000000000000
          Width = 487.559370000000000000
          Height = 45.354360000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_FORNECEDORES]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 498.897960000000000000
          Top = 536.795610000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Top = 536.795610000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 64.543290000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'PALLET [frxDsEstqR5."Pallet"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 112.322820000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 1.118120000000000000
          Top = 108.763760000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 491.441098660000000000
          Width = 188.976377950000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 204.196818660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 162.322820000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 351.598444720000000000
          Width = 680.315290160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 514.118256689999900000
          Width = 188.976377950000000000
          Height = 22.677165354330710000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 241.992096690000000000
          Width = 491.338582680000000000
          Height = 109.606345590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -48
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 120.944881890000000000
          Top = 162.322820000000000000
          Width = 559.370078740000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 389.393722760000000000
          Width = 680.315290160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR5."Pal' +
              'VrtPeca">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 120.944881890000000000
          Top = 116.543290000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_EMPNOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 116.543290000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPDESC]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 177.637910000000000000
          Top = 536.795610000000000000
          Width = 321.260050000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 26.456710000000000000
          Top = 427.189240000000000000
          Width = 631.181070630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_PALLET]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Top = 487.559370000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Memo10: TfrxMemoView
          Left = 257.008040000000000000
          Top = 83.149660000000000000
          Width = 385.511620630000000000
          Height = 38.000000000000000000
          DataField = 'NO_PalStat'
          DataSet = frxDsEstqR5
          DataSetName = 'frxDsEstqR5'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR5."NO_PalStat"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 3.779530000000000000
          Top = 200.315090000000000000
          Width = 177.637910000000000000
          Height = 143.622140000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_REVISORES]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 434.645950000000000000
          Top = 56.692949999999990000
          Width = 207.874150000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'DV : [VARF_DV_PALLET]')
          ParentFont = False
        end
      end
    end
  end
  object QrEstqR5: TmySQLQuery
    Database = Dmod.ZZDB
    Filter = '(PalVrtPeca > 0) OR (Ativo<>1)'
    Filtered = True
    OnCalcFields = QrEstqR5CalcFields
    Left = 56
    Top = 56
    object QrEstqR5Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR5GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR5Qtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR5GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR5NO_PRD_TAM_COR: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR5Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEstqR5NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR5Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR5NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR5ValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEstqR5Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEstqR5OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrEstqR5NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrEstqR5NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEstqR5DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEstqR5NO_CliStat: TWideStringField
      FieldName = 'NO_CliStat'
      Size = 100
    end
    object QrEstqR5CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrEstqR5SdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrEstqR5PalVrtQtd: TFloatField
      FieldName = 'PalVrtQtd'
    end
    object QrEstqR5LmbVrtQtd: TFloatField
      FieldName = 'LmbVrtQtd'
    end
    object QrEstqR5Status: TIntegerField
      FieldName = 'Status'
    end
    object QrEstqR5OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object QrEstqR5GraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrEstqR5NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrEstqR5NO_PalStat: TWideStringField
      FieldName = 'NO_PalStat'
      Size = 11
    end
    object QrEstqR5PalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object QrEstqR5TEXTO_QRCODE: TWideStringField
      DisplayWidth = 500
      FieldKind = fkCalculated
      FieldName = 'TEXTO_QRCODE'
      Size = 500
      Calculated = True
    end
    object QrEstqR5PAL_TXT: TWideStringField
      FieldName = 'PAL_TXT'
      Size = 50
    end
    object QrEstqR5SerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrEstqR5Talao: TIntegerField
      FieldName = 'Talao'
    end
    object QrEstqR5IMEI: TIntegerField
      FieldName = 'IMEI'
    end
  end
  object frxDsEstqR5: TfrxDBDataset
    UserName = 'frxDsEstqR5'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Terceiro=Terceiro'
      'NO_FORNECE=NO_FORNECE'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'ValorT=ValorT'
      'Ativo=Ativo'
      'OrdGGX=OrdGGX'
      'NO_STATUS=NO_STATUS'
      'NO_EMPRESA=NO_EMPRESA'
      'DataHora=DataHora'
      'NO_CliStat=NO_CliStat'
      'CliStat=CliStat'
      'SdoVrtQtd=SdoVrtQtd'
      'PalVrtQtd=PalVrtQtd'
      'LmbVrtQtd=LmbVrtQtd'
      'Status=Status'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_PalStat=NO_PalStat'
      'PalStat=PalStat'
      'TEXTO_QRCODE=TEXTO_QRCODE'
      'PAL_TXT=PAL_TXT'
      'SerieTal=SerieTal'
      'Talao=Talao'
      'IMEI=IMEI')
    DataSet = QrEstqR5
    BCDToCurrency = False
    Left = 56
    Top = 104
  end
  object QrTXCacIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 56
    Top = 152
    object QrTXCacItsNO_REVISOR: TWideStringField
      FieldName = 'NO_REVISOR'
      Size = 100
    end
    object QrTXCacItsQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object frxTEX_FAXAO_026_02: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '                                        '
      'end.')
    OnGetValue = frxTEX_FAXAO_026_02GetValue
    Left = 316
    Top = 4
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-Is de Pallets X Fornecedores ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 393.071120000000000000
          Top = 64.252010000000000000
          Width = 385.511972130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 343.937230000000000000
          Top = 64.252010000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitQtde: TfrxMemoView
          Left = 778.583180000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 56.692950000000000000
          Top = 64.252010000000000000
          Width = 113.385826771653500000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Top = 64.252010000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 211.653680000000000000
          Top = 64.252010000000000000
          Width = 132.283464566929100000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 170.078850000000000000
          Top = 64.252010000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 846.614720000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 990.236860000000000000
        DataSet = frxDsGGXPalTer
        DataSetName = 'frxDsGGXPalTer'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 393.071120000000000000
          Width = 385.511972130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 343.937230000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValQtde: TfrxMemoView
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'SdoVrtPeca'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 56.692950000000000000
          Width = 113.385826771653500000
          Height = 15.118110240000000000
          DataField = 'NO_Pallet'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 211.653680000000000000
          Width = 132.283464566929100000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 170.078850000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Terceiro'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDsGGXPalTer
          DataSetName = 'frxDsGGXPalTer'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGGXPalTer."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 453.543600000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 650.079160000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 408.189240000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779530000000022000
          Width = 778.582984720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 778.583180000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 846.614720000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsGGXPalTer."NO_PRD_TAM_COR"'
        object MeGrupo1Head: TfrxMemoView
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 325.039580000000000000
        Width = 990.236860000000000000
        object MeGrupo1Foot: TfrxMemoView
          Width = 778.582984720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsGGXPalTer."NO_PRD_TAM_COR"'
        object MeGrupo2Head: TfrxMemoView
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsGGXPalTer."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 279.685220000000000000
        Width = 990.236860000000000000
        object MeGrupo2Foot: TfrxMemoView
          Width = 778.582984720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 778.583180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."SdoVrtPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 846.614720000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGGXPalTer."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrGGXPalTer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(vmi.SdoVrtPeca) SdoVrtPeca,  '
      
        'SUM(vmi.SdoVrtPeso) SdoVrtPeso, SUM(vmi.SdoVrtArM2) SdoVrtArM2, ' +
        ' '
      'vmi.Terceiro, vmi.GraGruX, vmi.Pallet,  '
      'vmi.SerieTal, vmi.Talao, '
      'vsf.Nome NO_SerieTal, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet, '
      
        'SUM(vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) ' +
        'Inteiros,  '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  '
      'FROM txmovits vmi  '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN txpalleta  wbp ON wbp.Codigo=vmi.Pallet  '
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro '
      'LEFT JOIN txsertal   vsf ON vsf.Codigo=vmi.SerieTal  '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle   '
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 '
      'WHERE vmi.SdoVrtQtd > 0 '
      'AND vmi.Pallet IN ( '
      '1126, '
      '1125, '
      '1124, '
      '1123, '
      '1122, '
      '1121, '
      '1120, '
      '1119, '
      '1118, '
      '1117, '
      '1116, '
      '1115, '
      '1114, '
      '1113, '
      '1112, '
      '1111, '
      '1109, '
      '1108, '
      '1106, '
      '1105, '
      '1104, '
      '1103, '
      '1102, '
      '1101, '
      '1100, '
      '1099, '
      '1098, '
      '1097, '
      '1096, '
      '1095, '
      '1094, '
      '1093, '
      '1092, '
      '1091, '
      '1090, '
      '1089, '
      '1088, '
      '1087, '
      '1086, '
      '1085, '
      '1084, '
      '1083, '
      '1082, '
      '1081, '
      '1080, '
      '1079, '
      '1078, '
      '1077, '
      '1076, '
      '1075, '
      '1074, '
      '1073, '
      '1072, '
      '1071, '
      '1069, '
      '1068, '
      '1067, '
      '1066, '
      '1065, '
      '1064, '
      '1063, '
      '1062, '
      '1061, '
      '1060, '
      '1058, '
      '1057, '
      '1056, '
      '1055, '
      '1054, '
      '1053, '
      '1052, '
      '1051, '
      '1050, '
      '1049, '
      '1047, '
      '1046, '
      '1045, '
      '1044, '
      '1042, '
      '1041, '
      '1039, '
      '1038, '
      '1037, '
      '1036, '
      '1029, '
      '1028, '
      '1027, '
      '1026, '
      '1025, '
      '1024, '
      '1023, '
      '1022, '
      '1020, '
      '1019, '
      '1018, '
      '1017, '
      '1016, '
      '1015, '
      '1013, '
      '1012, '
      '1011, '
      '1010, '
      '1004, '
      '1003, '
      '1002, '
      '991, '
      '988, '
      '980, '
      '978, '
      '976, '
      '975, '
      '970, '
      '969, '
      '968, '
      '967, '
      '965, '
      '963, '
      '960, '
      '958, '
      '957, '
      '955, '
      '953, '
      '949, '
      '946, '
      '945, '
      '944, '
      '943, '
      '939, '
      '932, '
      '893, '
      '891, '
      '887, '
      '884, '
      '882, '
      '876, '
      '875, '
      '857, '
      '848, '
      '827, '
      '823, '
      '813, '
      '810, '
      '804, '
      '802, '
      '797, '
      '792, '
      '791, '
      '766, '
      '754, '
      '739, '
      '683, '
      '595, '
      '594, '
      '529, '
      '519, '
      '505, '
      '458, '
      '394, '
      '369, '
      '364, '
      '363, '
      '362) '
      'GROUP BY Terceiro, Pallet '
      'ORDER BY Terceiro, Pallet, NO_PRD_TAM_COR '
      '')
    Left = 192
    Top = 100
    object QrGGXPalTerSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrGGXPalTerTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrGGXPalTerGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGGXPalTerPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrGGXPalTerSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrGGXPalTerTalao: TIntegerField
      FieldName = 'Talao'
    end
    object QrGGXPalTerNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrGGXPalTerNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXPalTerNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrGGXPalTerInteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object QrGGXPalTerNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object frxDsGGXPalTer: TfrxDBDataset
    UserName = 'frxDsGGXPalTer'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Terceiro=Terceiro'
      'GraGruX=GraGruX'
      'Pallet=Pallet'
      'SerieTal=SerieTal'
      'Talao=Talao'
      'NO_SerieTal=NO_SerieTal'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'Inteiros=Inteiros'
      'NO_FORNECE=NO_FORNECE')
    DataSet = QrGGXPalTer
    BCDToCurrency = False
    Left = 192
    Top = 144
  end
  object QrMulFrn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 196
    Top = 4
    object QrMulFrnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrMulFrnQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrMulFrnSiglaTX: TWideStringField
      FieldName = 'SiglaTX'
      Size = 10
    end
  end
  object QrTotFrn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 196
    Top = 52
    object QrTotFrnQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object frxTEX_FAXAO_026_03: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42471.522633287000000000
    ReportOptions.LastChange = 42471.522633287000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Barcode2D1.Text := <VARF_QRCODE_1>;                           ' +
        '                          '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxTEX_FAXAO_026_01_AGetValue
    Left = 316
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 100.000000000000000000
      PaperHeight = 150.000000000000000000
      PaperSize = 256
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 548.031508270000000000
        Top = 18.897650000000000000
        Width = 377.953000000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 343.937230000000000000
          Top = 385.512035590000000000
          Width = 22.677057950000000000
          Height = 37.795285350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          Rotation = 270
        end
        object Memo8: TfrxMemoView
          Left = 343.937230000000000000
          Top = 423.307335590000000000
          Width = 22.677057950000000000
          Height = 124.724475350000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsEstqR5."DataHora"]')
          ParentFont = False
          Rotation = 270
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 343.937151890000000000
          Top = 18.897637800000000000
          Width = 22.676818740000000000
          Height = 366.614397800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_EMPNOME]')
          ParentFont = False
          Rotation = 270
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 366.614410000000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 56.692950000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Fabricado por:')
          ParentFont = False
          Rotation = 270
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Left = 275.905690000000000000
          Top = 18.897650000000000000
          Width = 68.031540000000000000
          Height = 529.134200000000000000
          Curve = 1
          Shape = skRoundRectangle
        end
        object Memo22: TfrxMemoView
          Left = 275.905690000000000000
          Top = 75.393630000000000000
          Width = 68.031178740000000000
          Height = 438.425148030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"]')
          ParentFont = False
          Rotation = 270
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo14: TfrxMemoView
          Left = 275.905768110000000000
          Top = 22.677180000000000000
          Width = 68.031540000000010000
          Height = 52.913420000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo  espessura  cor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape2: TfrxShapeView
          Left = 173.858380000000000000
          Top = 18.897650000000000000
          Width = 102.047310000000000000
          Height = 529.134200000000000000
          Curve = 1
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 173.858380000000000000
          Top = 41.866110000000000000
          Width = 102.046870630000000000
          Height = 506.456688030000000000
          DataField = 'Pallet'
          DataSet = frxDsEstqR5
          DataSetName = 'frxDsEstqR5'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -96
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."Pallet"]')
          ParentFont = False
          Rotation = 270
        end
        object Memo12: TfrxMemoView
          Left = 173.858536220000000000
          Top = 18.897650000000000000
          Width = 102.047310000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          Left = 117.165430000000000000
          Top = 173.858380000000000000
          Width = 56.692913390000000000
          Height = 374.173470000000000000
          Curve = 1
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 151.181419690000000000
          Top = 177.637910000000000000
          Width = 22.677131180000000000
          Height = 366.614348980000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          Rotation = 270
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 117.165649690000000000
          Top = 177.637910000000000000
          Width = 34.015721180000000000
          Height = 366.614348980000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 1.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR5."Pal' +
              'VrtPeca">)]')
          ParentFont = False
          Rotation = 270
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape4: TfrxShapeView
          Left = 79.370130000000000000
          Top = 173.858380000000000000
          Width = 37.795300000000000000
          Height = 374.173470000000000000
          Curve = 1
          Shape = skRoundRectangle
        end
        object Shape5: TfrxShapeView
          Left = 18.897650000000000000
          Top = 173.858380000000000000
          Width = 60.472480000000000000
          Height = 374.173470000000000000
          Curve = 1
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 18.897650000000000000
          Top = 177.638083310000000000
          Width = 60.472480000000000000
          Height = 188.976500000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            '[VARF_FORNECEDORES]')
          ParentFont = False
          Rotation = 270
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 79.370007950000000000
          Top = 177.637910000000000000
          Width = 37.795275590000000000
          Height = 366.614385590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"]')
          ParentFont = False
          Rotation = 270
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 18.897650000000000000
          Top = 366.512233310000000000
          Width = 60.472480000000000000
          Height = 177.637910000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_REVISORES]')
          ParentFont = False
          Rotation = 270
        end
        object Memo3: TfrxMemoView
          Left = 102.047310000000000000
          Top = 177.637910000000000000
          Width = 15.118120000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          Rotation = 270
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 173.858380000000000000
          Top = 18.897650000000000000
          Width = 11.338526540000000000
          Height = 124.724490000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          Rotation = 270
          VAlign = vaCenter
        end
        object Barcode2D1: TfrxBarcode2DView
          Left = 41.574830000000000000
          Top = 34.015770000000010000
          Width = 42.000000000000000000
          Height = 42.000000000000000000
          BarType = bcCodeQR
          BarProperties.Encoding = qrAuto
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecL
          BarProperties.PixelSize = 4
          BarProperties.CodePage = 0
          Rotation = 0
          ShowText = False
          Text = '12345678'
          Zoom = 0.500000000000000000
          FontScaled = True
          QuietZone = 0
        end
      end
    end
  end
  object frxTEX_FAXAO_026_04: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Visivel: Boolean;'
      '    '
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Visivel := (<frxDsEstqR5."Ativo"> = 1) OR (<frxDsEstqR5."Ativo' +
        '"> = 3);'
      '  //'
      
        '  Memo1.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo2.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo3.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo4.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo5.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo6.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo7.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo8.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo9.Visible := Visivel;                                     ' +
        '             '
      
        '  Memo10.Visible := Visivel;                                    ' +
        '              '
      
        '  Memo11.Visible := Visivel;                                    ' +
        '              '
      
        '  Memo12.Visible := Visivel;                                    ' +
        '              '
      
        '  Memo13.Visible := Visivel;                                    ' +
        '              '
      
        '  Memo14.Visible := Visivel;                                    ' +
        '              '
      
        '  Memo15.Visible := Visivel;                                    ' +
        '              '
      '  Memo16.Visible := Visivel;'
      '  Memo17.Visible := Visivel;'
      '  Memo18.Visible := Visivel;'
      '  //        '
      
        '  Shape1.Visible := Visivel;                                    ' +
        '              '
      'end;'
      ''
      'begin'
      '  Visivel := True;                                    '
      'end.')
    OnGetValue = frxTEX_FAXAO_026_01_AGetValue
    Left = 316
    Top = 108
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 4.000000000000000000
      RightMargin = 4.000000000000000000
      Columns = 2
      ColumnWidth = 99.500000000000000000
      ColumnPositions.Strings = (
        '0'
        '102')
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 253.228346460000000000
        Top = 132.283550000000000000
        Width = 376.063235000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        ColumnGap = 9.448818897637795000
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Shape1: TfrxShapeView
          Left = 3.779527560000000000
          Top = 3.779527559999991000
          Width = 360.944881890000000000
          Height = 245.669291340000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 1.118120000000000000
          Top = 19.188930000000000000
          Width = 362.834643230000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PALLET [frxDsEstqR5."Pallet"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 3.779530000000000000
          Top = 79.173160000000000000
          Width = 15.118041890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 3.779310310000000000
          Top = 166.401474720000000000
          Width = 360.944999060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie - Tal'#227'o:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 3.779530000000000000
          Top = 113.488054720000000000
          Width = 360.944999060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 18.897650000000000000
          Top = 219.314916690000000000
          Width = 321.259927950000000000
          Height = 18.897635350000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 18.897571890000000000
          Top = 79.173160000000000000
          Width = 345.826766770000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 3.779310310000000000
          Top = 181.519572760000000000
          Width = 360.944999060000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."SerieTal"] - [frxDsEstqR5."Talao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo11: TfrxMemoView
          Left = 3.779530000000000000
          Top = 128.606152760000000000
          Width = 360.944999060000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR5."Pal' +
              'VrtPeca">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 18.897650000000000000
          Top = 235.842517240000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Top = 56.795300000000000000
          Width = 366.613970630000000000
          Height = 22.881880000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_PALLET]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 86.929111890000000000
          Top = 3.779527559055112000
          Width = 264.566738740000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_EMPNOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779527559055112000
          Width = 75.590521890000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPDESC]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 54.047244090000000000
        Top = 18.897650000000000000
        Width = 763.465060000000000000
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > 0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 504
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 504
    Top = 104
  end
  object TbTXMovImp4: TmySQLTable
    Database = DModG.MyPID_DB
    Filter = 'Ativo IN (2,3)'
    Filtered = True
    BeforePost = TbTXMovImp4BeforePost
    TableName = '_vsmovimp4_fmvsmovimp'
    Left = 604
    Top = 208
    object TbTXMovImp4Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object TbTXMovImp4GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object TbTXMovImp4Qtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object TbTXMovImp4ValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object TbTXMovImp4SdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      Required = True
    end
    object TbTXMovImp4LmbVrtQtd: TFloatField
      FieldName = 'LmbVrtQtd'
      Required = True
    end
    object TbTXMovImp4GraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object TbTXMovImp4NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object TbTXMovImp4Pallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object TbTXMovImp4NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object TbTXMovImp4Terceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object TbTXMovImp4CliStat: TIntegerField
      FieldName = 'CliStat'
      Required = True
    end
    object TbTXMovImp4Status: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object TbTXMovImp4NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object TbTXMovImp4NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object TbTXMovImp4NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object TbTXMovImp4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object TbTXMovImp4DataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object TbTXMovImp4OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
      Required = True
    end
    object TbTXMovImp4OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
      Required = True
    end
    object TbTXMovImp4GraGruY: TIntegerField
      FieldName = 'GraGruY'
      Required = True
    end
    object TbTXMovImp4NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object TbTXMovImp4PalStat: TIntegerField
      FieldName = 'PalStat'
      Required = True
    end
    object TbTXMovImp4CouNiv2: TIntegerField
      FieldName = 'CouNiv2'
      Required = True
    end
    object TbTXMovImp4CouNiv1: TIntegerField
      FieldName = 'CouNiv1'
      Required = True
    end
    object TbTXMovImp4NO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object TbTXMovImp4NO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object TbTXMovImp4FatorInt: TFloatField
      FieldName = 'FatorInt'
      Required = True
    end
    object TbTXMovImp4SdoInteiros: TFloatField
      FieldName = 'SdoInteiros'
      Required = True
    end
    object TbTXMovImp4LmbInteiros: TFloatField
      FieldName = 'LmbInteiros'
      Required = True
    end
    object TbTXMovImp4Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object TbTXMovImp4IMEC: TIntegerField
      FieldName = 'IMEC'
      Required = True
    end
    object TbTXMovImp4IMEI: TIntegerField
      FieldName = 'IMEI'
      Required = True
    end
    object TbTXMovImp4MovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object TbTXMovImp4MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object TbTXMovImp4NO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 30
    end
    object TbTXMovImp4NO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 40
    end
    object TbTXMovImp4SerieTal: TIntegerField
      FieldName = 'SerieTal'
      Required = True
    end
    object TbTXMovImp4NO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object TbTXMovImp4Talao: TIntegerField
      FieldName = 'Talao'
      Required = True
    end
    object TbTXMovImp4StatPall: TIntegerField
      FieldName = 'StatPall'
      Required = True
    end
    object TbTXMovImp4NO_StatPall: TWideStringField
      FieldName = 'NO_StatPall'
      Size = 60
    end
    object TbTXMovImp4NFeSer: TSmallintField
      FieldName = 'NFeSer'
      Required = True
    end
    object TbTXMovImp4NFeNum: TIntegerField
      FieldName = 'NFeNum'
      Required = True
    end
    object TbTXMovImp4TXMulNFeCab: TIntegerField
      FieldName = 'TXMulNFeCab'
      Required = True
    end
    object TbTXMovImp4StqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Required = True
    end
    object TbTXMovImp4NO_StqCenCad: TWideStringField
      FieldName = 'NO_StqCenCad'
      Size = 50
    end
    object TbTXMovImp4StqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object TbTXMovImp4NO_StqCenLoc: TWideStringField
      FieldName = 'NO_StqCenLoc'
      Size = 50
    end
    object TbTXMovImp4Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbTXMovImp4NO_ARTIGO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_ARTIGO'
      LookupDataSet = QrGraGruX
      LookupKeyFields = 'Controle'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'GraGruX'
      Size = 255
      Lookup = True
    end
  end
  object DsTXMovImp4: TDataSource
    DataSet = TbTXMovImp4
    Left = 604
    Top = 256
  end
  object frxTEX_FAXAO_026_01_B: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41608.425381412000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'begin'
      '  Imprime := True;                                  '
      'end.')
    OnGetValue = frxTEX_FAXAO_026_01_AGetValue
    Left = 52
    Top = 252
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370078740000100000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEstqR5
        DataSetName = 'frxDsEstqR5'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 438.425480000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 192.756030000000000000
          Top = 491.441250000000000000
          Width = 487.559370000000000000
          Height = 45.354360000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_FORNECEDORES]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 498.897960000000000000
          Top = 536.795610000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Top = 536.795610000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 64.543290000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'PALLET [frxDsEstqR5."Pallet"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 112.322820000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 1.118120000000000000
          Top = 108.763760000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 491.441098660000000000
          Width = 188.976377950000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 204.196818660000000000
          Width = 491.338582680000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 162.322820000000000000
          Width = 120.944881890000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Mat'#233'ria-prima:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 351.598444720000000000
          Width = 680.315290160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 514.118256689999900000
          Width = 188.976377950000000000
          Height = 22.677165354330710000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 241.992096690000000000
          Width = 491.338582680000000000
          Height = 109.606345590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -48
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR5."CliStat"] - [frxDsEstqR5."NO_CliStat"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 120.944881890000000000
          Top = 162.322820000000000000
          Width = 559.370078740000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR5."GraGruX"] - [frxDsEstqR5."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 389.393722760000000000
          Width = 680.315290160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR5."Pal' +
              'VrtPeca">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 120.944881890000000000
          Top = 116.543290000000000000
          Width = 559.370078740000100000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_EMPNOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 116.543290000000000000
          Width = 120.944881890000000000
          Height = 40.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPDESC]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 177.637910000000000000
          Top = 536.795610000000000000
          Width = 321.260050000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 26.456710000000000000
          Top = 427.189240000000000000
          Width = 631.181070630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_PALLET]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Top = 487.559370000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Memo10: TfrxMemoView
          Left = 257.008040000000000000
          Top = 83.149660000000000000
          Width = 385.511620630000000000
          Height = 38.000000000000000000
          DataField = 'NO_PalStat'
          DataSet = frxDsEstqR5
          DataSetName = 'frxDsEstqR5'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR5."NO_PalStat"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 3.779530000000000000
          Top = 200.315090000000000000
          Width = 177.637910000000000000
          Height = 143.622140000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_REVISORES]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 434.645950000000000000
          Top = 56.692949999999990000
          Width = 207.874150000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'DV : [VARF_DV_PALLET]')
          ParentFont = False
        end
      end
    end
  end
end
