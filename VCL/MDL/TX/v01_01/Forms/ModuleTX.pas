unit ModuleTX;

interface

uses
  System.SysUtils, System.Classes, mySQLDirectQuery, dmkGeral,
  UnAppEnums, UnDmkEnums;

type
  TDmModTX_CRC = class(TDataModule)
    DqAux: TmySQLDirectQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AtualizaImeiValorT(Controle: Integer);
  end;

var
  DmModTX_CRC: TDmModTX_CRC;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TDmModTX_CRC }

uses
  UnTX_PF, DmkDAC_PF, ModuleGeral, Module, UMySQLDB, UMySQLModule;

procedure TDmModTX_CRC.AtualizaImeiValorT(Controle: Integer);
const
  sProcName = 'TDmModTX_CRC.AtualizaImeiValorT()';
var
  CusFrtAvuls, CusFrtMOEnv, CusFrtMORet, (*CusFrtTrnsf,*) ValorMP, CustoPQ, ValorT,
  CustoMOTot: Double;
  MovimID, MovimNiv: Integer;
  Continua: Boolean;
begin
  TX_PF.AdicionarNovosTX_emid();
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT CusFrtAvuls, CusFrtMOEnv, CusFrtMORet, ',
  //'CusFrtTrnsf, ',
  'ValorMP, CustoPQ, CustoMOTot, ',
  'MovimID, MovimNiv ',
  'FROM txmovits ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  CusFrtAvuls := USQLDB.Flu(DqAux, 'CusFrtAvuls');
  CusFrtMOEnv := USQLDB.Flu(DqAux, 'CusFrtMOEnv');
  CusFrtMORet := USQLDB.Flu(DqAux, 'CusFrtMORet');
  //CusFrtTrnsf := USQLDB.Flu(DqAux, 'CusFrtTrnsf');
  ValorMP     := USQLDB.Flu(DqAux, 'ValorMP');
  CustoPQ     := USQLDB.Flu(DqAux, 'CustoPQ');
  CustoMOTot  := USQLDB.Flu(DqAux, 'CustoMOTot');
  //
  MovimID     := USQLDB.Int(DqAux, 'MovimID');
  MovimNiv    := USQLDB.Int(DqAux, 'MovimNiv');
  //
  ValorT      := 0;
  Continua    := False;
  case TEstqMovimID(MovimID) of
    (*01*)emidCompra:
    begin
      ValorT := CusFrtAvuls + CusFrtMOEnv + ValorMP; // + CusFrtTrnsf
      Continua := True;
    end;
    (*02*)emidVenda:
    begin
      ValorT := ValorMP - CusFrtAvuls; // + CusFrtTrnsf
      Continua := True;
    end;
    (*05*)emidIndstrlzc:
    begin
      ValorT := CusFrtMORet + ValorMP + CustoPQ + CustoMOTot; // + CusFrtTrnsf
      Continua := True;
    end;
{
    (*25*)emidTransfLoc:
    begin
      ValorT := CusFrtAvuls + CusFrtMOEnv + CusFrtMORet + ValorMP; // + CusFrtTrnsf
      Continua := True;
    end;
}
    else
    begin
      Geral.MB_Erro('"MovimID" ' + Geral.FF0(MovimID) +
      ' n�o implementado em ' + sProcName + sLineBreak +
      'IME-I: ' + Geral.FF0(Controle));
      Continua := False;
    end;
  end;
  if Continua then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txmovits', False, [
    'ValorT'], ['Controle'], [
    ValorT], [Controle], True);
  end;
end;

procedure TDmModTX_CRC.DataModuleCreate(Sender: TObject);
begin
  DqAux.Database := DModG.MyCompressDB;
end;

end.
