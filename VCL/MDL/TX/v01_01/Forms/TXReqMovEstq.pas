unit TXReqMovEstq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  UnProjGroup_Consts;

type
  TFmTXReqMovEstq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    Label5: TLabel;
    EdInteiro: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCampo: String;
    FQry: TmySQLQuery;
  end;

  var
  FmTXReqMovEstq: TFmTXReqMovEstq;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmTXReqMovEstq.BtOKClick(Sender: TObject);
var
  Controle, Inteiro: Integer;
begin
  Controle       := EdControle.ValueVariant;
  Inteiro        := EdInteiro.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_UPD_TAB_TMI, False, [
  FCampo], [
  'Controle'], [
  Inteiro], [
  Controle], True) then
  begin
    if FQry <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
      try
        FQry.Locate('Controle', Controle, []);
      except
        ; // Nada
      end;
    end;
    Close;
  end;
end;

procedure TFmTXReqMovEstq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXReqMovEstq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXReqMovEstq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmTXReqMovEstq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
