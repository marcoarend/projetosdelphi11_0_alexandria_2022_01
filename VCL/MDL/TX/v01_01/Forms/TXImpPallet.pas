unit TXImpPallet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  dmkGeral, UnInternalConsts, Data.DB, mySQLDbTables, frxDBSet,
  UnProjGroup_Consts, UnGrl_Consts, UnDmkEnums, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, dmkImage, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Vars;

type
  TFmTXImpPallet = class(TForm)
    frxTEX_FAXAO_026_01_A: TfrxReport;
    QrEstqR5: TmySQLQuery;
    QrEstqR5Empresa: TIntegerField;
    QrEstqR5GraGruX: TIntegerField;
    QrEstqR5Qtde: TFloatField;
    QrEstqR5GraGru1: TIntegerField;
    QrEstqR5NO_PRD_TAM_COR: TWideStringField;
    QrEstqR5Terceiro: TIntegerField;
    QrEstqR5NO_FORNECE: TWideStringField;
    QrEstqR5Pallet: TIntegerField;
    QrEstqR5NO_PALLET: TWideStringField;
    QrEstqR5ValorT: TFloatField;
    QrEstqR5Ativo: TSmallintField;
    QrEstqR5OrdGGX: TIntegerField;
    QrEstqR5NO_STATUS: TWideStringField;
    frxDsEstqR5: TfrxDBDataset;
    QrEstqR5NO_EMPRESA: TWideStringField;
    QrEstqR5DataHora: TDateTimeField;
    QrEstqR5NO_CliStat: TWideStringField;
    QrEstqR5CliStat: TIntegerField;
    QrEstqR5SdoVrtQtd: TFloatField;
    QrEstqR5PalVrtQtd: TFloatField;
    QrEstqR5LmbVrtQtd: TFloatField;
    QrEstqR5Status: TIntegerField;
    QrEstqR5OrdGGY: TIntegerField;
    QrEstqR5GraGruY: TIntegerField;
    QrEstqR5NO_GGY: TWideStringField;
    QrEstqR5NO_PalStat: TWideStringField;
    QrEstqR5PalStat: TIntegerField;
    QrTXCacIts: TmySQLQuery;
    QrTXCacItsNO_REVISOR: TWideStringField;
    QrTXCacItsQtde: TFloatField;
    frxTEX_FAXAO_026_02: TfrxReport;
    QrGGXPalTer: TmySQLQuery;
    frxDsGGXPalTer: TfrxDBDataset;
    QrGGXPalTerSdoVrtQtd: TFloatField;
    QrGGXPalTerTerceiro: TIntegerField;
    QrGGXPalTerGraGruX: TIntegerField;
    QrGGXPalTerPallet: TIntegerField;
    QrGGXPalTerSerieTal: TIntegerField;
    QrGGXPalTerTalao: TIntegerField;
    QrGGXPalTerNO_SerieTal: TWideStringField;
    QrGGXPalTerNO_PRD_TAM_COR: TWideStringField;
    QrGGXPalTerNO_Pallet: TWideStringField;
    QrGGXPalTerInteiros: TFloatField;
    QrGGXPalTerNO_FORNECE: TWideStringField;
    QrMulFrn: TmySQLQuery;
    QrMulFrnFornece: TIntegerField;
    QrMulFrnQtde: TFloatField;
    QrMulFrnSiglaTX: TWideStringField;
    QrTotFrn: TmySQLQuery;
    QrTotFrnQtde: TFloatField;
    frxTEX_FAXAO_026_03: TfrxReport;
    QrEstqR5TEXTO_QRCODE: TWideStringField;
    frxTEX_FAXAO_026_04: TfrxReport;
    QrEstqR5PAL_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    TbTXMovImp4: TmySQLTable;
    DsTXMovImp4: TDataSource;
    DBGrid1: TDBGrid;
    TbTXMovImp4Empresa: TIntegerField;
    TbTXMovImp4GraGruX: TIntegerField;
    TbTXMovImp4Qtde: TFloatField;
    TbTXMovImp4ValorT: TFloatField;
    TbTXMovImp4SdoVrtQtd: TFloatField;
    TbTXMovImp4LmbVrtQtd: TFloatField;
    TbTXMovImp4GraGru1: TIntegerField;
    TbTXMovImp4NO_PRD_TAM_COR: TWideStringField;
    TbTXMovImp4Pallet: TIntegerField;
    TbTXMovImp4NO_PALLET: TWideStringField;
    TbTXMovImp4Terceiro: TIntegerField;
    TbTXMovImp4CliStat: TIntegerField;
    TbTXMovImp4Status: TIntegerField;
    TbTXMovImp4NO_FORNECE: TWideStringField;
    TbTXMovImp4NO_CLISTAT: TWideStringField;
    TbTXMovImp4NO_EMPRESA: TWideStringField;
    TbTXMovImp4NO_STATUS: TWideStringField;
    TbTXMovImp4DataHora: TDateTimeField;
    TbTXMovImp4OrdGGX: TIntegerField;
    TbTXMovImp4OrdGGY: TIntegerField;
    TbTXMovImp4GraGruY: TIntegerField;
    TbTXMovImp4NO_GGY: TWideStringField;
    TbTXMovImp4PalStat: TIntegerField;
    TbTXMovImp4CouNiv2: TIntegerField;
    TbTXMovImp4CouNiv1: TIntegerField;
    TbTXMovImp4NO_CouNiv2: TWideStringField;
    TbTXMovImp4NO_CouNiv1: TWideStringField;
    TbTXMovImp4FatorInt: TFloatField;
    TbTXMovImp4SdoInteiros: TFloatField;
    TbTXMovImp4LmbInteiros: TFloatField;
    TbTXMovImp4Codigo: TIntegerField;
    TbTXMovImp4IMEC: TIntegerField;
    TbTXMovImp4IMEI: TIntegerField;
    TbTXMovImp4MovimID: TIntegerField;
    TbTXMovImp4MovimNiv: TIntegerField;
    TbTXMovImp4NO_MovimID: TWideStringField;
    TbTXMovImp4NO_MovimNiv: TWideStringField;
    TbTXMovImp4SerieTal: TIntegerField;
    TbTXMovImp4NO_SerieTal: TWideStringField;
    TbTXMovImp4Talao: TIntegerField;
    TbTXMovImp4StatPall: TIntegerField;
    TbTXMovImp4NO_StatPall: TWideStringField;
    TbTXMovImp4NFeSer: TSmallintField;
    TbTXMovImp4NFeNum: TIntegerField;
    TbTXMovImp4TXMulNFeCab: TIntegerField;
    TbTXMovImp4StqCenCad: TIntegerField;
    TbTXMovImp4NO_StqCenCad: TWideStringField;
    TbTXMovImp4StqCenLoc: TIntegerField;
    TbTXMovImp4NO_StqCenLoc: TWideStringField;
    TbTXMovImp4Ativo: TSmallintField;
    TbTXMovImp4NO_ARTIGO: TWideStringField;
    QrEstqR5SerieTal: TIntegerField;
    QrEstqR5Talao: TIntegerField;
    QrEstqR5IMEI: TIntegerField;
    frxTEX_FAXAO_026_01_B: TfrxReport;
    procedure frxTEX_FAXAO_026_01_AGetValue(const VarName: string;
      var Value: Variant);
    procedure frxTEX_FAXAO_026_02GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEstqR5CalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TbTXMovImp4BeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FTXImpRandStr: String;
    FJaDescript_TXImpRandStr: Boolean;
    //FTXMovImp5: String;
    //
    function  SQL_ListaPallets(): String;
    //
    function  EncodeDadosFicha(): String;
    function  Obtem_TXImpRandStr(): String;
  public
    { Public declarations }
    FTXMovImp4: String;
    FEmpresa_Cod: Integer;
    FEmpresa_Txt: String;
    FPallets: array of Integer;
    FInfoNO_PALLET, (*FTermica,*) FImpEmpresa: Boolean;
    FDestImprFichaPallet: TDestImprFichaPallet;
    //
    procedure PreparaPallets(TXMovImp4, TXLstPalBox: String;
              EmptyLabels: Integer = 0);
    procedure ImprimePallets(TXMovImp4: String; Ordem: TORD_SQL);
    procedure ImprimeIMEIsPallets(Ordenacao: Integer);
    procedure ImprimeTermica();
    procedure ReopenGraGruX();
  end;

var
  FmTXImpPallet: TFmTXImpPallet;

implementation


uses Module, ModuleGeral, DmkDAC_PF, UnMyObjects, ModTX_CRC, UnDmkProcFunc,
  UMySQLModule;

{$R *.dfm}

(*
function A_Set_Sensor_Mode(type_Renamed: char; continuous: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Speed(speed: char): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Gap(gap: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Backfeed(back: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Darkness(darkness: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Set_Syssetting(transfer, cut_peel, length, zero, pause: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_CreatePrn(selection: integer; FileName: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_CreateUSBPort(nPort: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Print_Out(width, height, copies, amount: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Text(x, y, ori, font, typee, hor_factor, ver_factor: integer; mode: char; numeric: integer; data: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Barcode(x, y, ori: integer; typee: char; narrow, width, height: integer; mode: char; numeric: integer; data: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Text_TrueType(x, y, FSize: integer; FType: pchar; Fspin, FWeight, FItalic, FUnline, FStrikeOut: integer; id_name, data: pchar; mem_mode: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Prn_Text_TrueType_W(x, y, FHeight, FWidth: integer; FType: pchar; Fspin, FWeight, FItalic, FUnline, FStrikeOut: integer; id_name, data: pchar; mem_mode: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Get_Graphic(x, y, mem_mode: integer; format: char; filename: pchar): integer; stdcall; external 'WINPPLA.DLL';
function A_Draw_Box(mode, x, y, width, height, top, side: integer): integer; stdcall; external 'WINPPLA.DLL';
function A_Draw_Line(mode, x, y, width, height: integer): integer; stdcall; external 'WINPPLA.DLL';
procedure A_ClosePrn(); stdcall; external 'WINPPLA.DLL';
function A_GetUSBBufferLen(): integer; stdcall; external 'WINPPLA.DLL';
procedure A_Feed_Label(); stdcall; external 'WINPPLA.DLL';
*)
procedure TFmTXImpPallet.frxTEX_FAXAO_026_02GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt: String;
  Invalido: Boolean;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmTXImpPallet.BtOKClick(Sender: TObject);
begin
  ImprimePallets(FTXMovImp4, sqlordASC);
end;

procedure TFmTXImpPallet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmTXImpPallet.EncodeDadosFicha(): String;
var
  I: Integer;
  Texto, ContraSenha: String;
begin
  Texto :=
    Geral.FF0(QrEstqR5Pallet.Value) + sLineBreak +
    Geral.FF0(QrEstqR5GraGruX.Value) + '-' + QrEstqR5NO_PRD_TAM_COR.Value + sLineBreak +
    QrEstqR5NO_PALLET.Value + sLineBreak +
    Geral.FFT(QrEstqR5Qtde.Value, 1, siNegativo) + ' Qtd' + sLineBreak;
  ContraSenha := Obtem_TXImpRandStr();
  if ContraSenha <> '' then
    Result :=  DmkPF.HexCripto(Texto, ContraSenha)
  else
    Result := Texto;
end;

procedure TFmTXImpPallet.FormCreate(Sender: TObject);
begin
  FTXImpRandStr := '';
  FImpEmpresa   := True;
  FJaDescript_TXImpRandStr := False;
end;

procedure TFmTXImpPallet.frxTEX_FAXAO_026_01_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NO_PALLET' then
  begin
    if FInfoNO_PALLET then
      Value := QrEstqR5NO_PALLET.Value
    else
    begin
      if Trim(QrEstqR5NO_PALLET.Value) <> '' then
        Value := '...'
      else
        Value := '';
    end;
  end;
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_TXNIVGER' then
    Value := '00000' //Dmod.QrControleTXNivGer.Value    <=== Nao usa! Tirar!
  else
  if VarName ='VARF_REVISORES' then
  begin
    Value := '';
    UnDmkDAC_PF.AbreMySQLQuery0(QrTXCacIts, Dmod.MyDB, [
    'SELECT cia.Revisor, SUM(cia.Qtde) Qtde, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_REVISOR ',
    'FROM txcacitsa cia ',
    'LEFT JOIN entidades ent ON ent.Codigo=cia.Revisor ',
    'WHERE cia.TXPallet=' + Geral.FF0(QrEstqR5Pallet.Value),
    'GROUP BY cia.Revisor',
    '']);
    QrTXCacIts.First;
    while not QrTXCacIts.Eof do
    begin
      Value := Value + QrTXCacItsNO_REVISOR.Value + ' (' + FloatToStr(
        QrTXCacItsQtde.Value) + ')' + sLineBreak;
      //
      QrTXCacIts.Next;
    end;
  end else
  if VarName = 'VARF_FORNECEDORES' then
  begin
{POIU
    if VAR_InfoMulFrnImpTX (*Dmod.QrControleInfoMulFrnImpTX.Value*) = 0 then
      Value := ''
    else
      Value := DmModTX_CRC.ObtemFornecedores_Pallet(QrEstqR5Pallet.Value);
}
  end else
  if VarName = 'VARF_QRCODE_1' then
  begin
    Value := EncodeDadosFicha();
  end else
  if VarName = 'VARF_EMPDESC' then
  begin
    if FImpEmpresa then
      Value := 'Fabricado por: '
    else
      Value := '';
  end else
  if VarName = 'VARF_EMPNOME' then
  begin
    if FImpEmpresa then
      Value := QrEstqR5NO_EMPRESA.Value
    else
      Value := '';
  end else
  if VarName = 'VARF_DV_PALLET' then
  begin
    Value := DmkPF.DigitoVerificardorDmk3CasasInteger(QrEstqR5IMEI.Value)
  end else
end;

procedure TFmTXImpPallet.ImprimePallets(TXMovImp4: String; Ordem: TORD_SQL);
const
  sProcName = 'FmTXImpPallet.ImprimePallets()';
var
  SQL_Pallets: String;
  SQL_ORD: String;
  FrxToImp: TfrxReport;
begin
  SQL_Pallets := SQL_ListaPallets();
  if Trim(SQL_Pallets) = '' then
    SQL_Pallets := Geral.FF0(- High(Integer));
  SQL_Pallets :=  'AND mi4.Pallet IN (' + SQL_Pallets + ') ';
  //
  if Ordem = TORD_SQL.sqlordDESC then
    SQL_ORD :=   'ORDER BY Pallet DESC, PalStat '
  else
    SQL_ORD := 'ORDER BY OrdGGX ';
  //
  //fazer ficha e testar
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR5, DModG.MyPID_DB, [
  'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Qtde) Qtde, SUM(mi4.ValorT) ValorT, ',
  'SUM(mi4.SdoVrtQtd) SdoVrtQtd, ',
  'SUM(mi4.SdoVrtQtd+mi4.LmbVrtQtd) PalVrtQtd, ',
  'SUM(mi4.LmbVrtQtd) LmbVrtQtd, ',
  'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.Terceiro, ',
  'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO_EMPRESA, ',
  'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY, ',
  'mi4.NO_GGY, StatPall PalStat, NO_StatPall NO_PalStat, mi4.Ativo, ',
  'IF(mi4.Pallet < 1, "           ",',
  ' CONCAT("PALLET ", FORMAT(mi4.Pallet, 0))) PAL_TXT, ',
  'mi4.SerieTal, mi4.Talao, mi4.IMEI ',
  'FROM  ' + TXMovImp4 + ' mi4',
  'WHERE ((mi4.SdoVrtQtd > 0 OR mi4.LmbVrtQtd <> 0)  ',
  SQL_Pallets,
  ') OR (Ativo  <> 1) ',
  'GROUP BY mi4.Pallet ',
  SQL_ORD,
  //
  '']);
  //Geral.MB_SQL(Self, QrEstqR5);
    //
  case FDestImprFichaPallet of
    difpPapelA4:
    begin
{POIU
      if Dmod.QrControleTXImpMediaPall.Value = 1 then
        frxToImp := frxTEX_FAXAO_026_01_B
      else
}
        frxToImp := frxTEX_FAXAO_026_01_A;
      //
      MyObjects.frxDefineDataSets(frxToImp, [
      DModG.frxDsDono,
      frxDsEstqR5
      ]);
      MyObjects.frxMostra(frxToImp, 'Pallet de Artigo de Ribeira');
   end;
   difpEtiq2x4:
   begin
      MyObjects.frxDefineDataSets(frxTEX_FAXAO_026_04, [
      DModG.frxDsDono,
      frxDsEstqR5
      ]);
      MyObjects.frxMostra(frxTEX_FAXAO_026_04, 'Pallet de Artigo de Ribeira');
   end;

   difpTermica:
   begin
   //if FTermica then
     ImprimeTermica();
   end;
   else Geral.MB_Erro('"FDestImprFichaPallet" indefinido em ' + sProcName)
 end;
end;

procedure TFmTXImpPallet.ImprimeIMEISPallets(Ordenacao: Integer);
var
  SQL, Campo1A, Campo1B, Campo2A, Campo2B, TitGru1, TitGru2: String;
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2: TfrxMemoView;
begin
  SQL := SQL_ListaPallets();
  //
  if Ordenacao = 1 then
  begin
    Campo1A := 'Pallet';
    Campo1B := 'Pallet';
    TitGru1 := 'Pallet';
    //
    Campo2A := 'Terceiro';
    Campo2B := 'NO_FORNECE';
    TitGru2 := 'Fornecedor';
  end else
  begin
    Campo1A := 'Terceiro';
    Campo1B := 'NO_FORNECE';
    TitGru1 := 'Fornecedor';
    //
    Campo2A := 'Pallet';
    Campo2B := 'Pallet';
    TitGru2 := 'Pallet';
  end;
  if SQL <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGGXPalTer, Dmod.MyDB, [
    'SELECT SUM(tmi.SdoVrtQtd) SdoVrtQtd,  ',
    'tmi.Terceiro, tmi.GraGruX, tmi.Pallet,  ',
    'tmi.SerieTal, tmi.Talao, ',
    'tst.Nome NO_SerieTal, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, wbp.Nome NO_Pallet, ',
    'SUM(tmi.SdoVrtQtd * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) Inteiros, ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
    'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=tmi.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN txpalleta  wbp ON wbp.Codigo=tmi.Pallet ',
    'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
    'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    'WHERE tmi.SdoVrtQtd > 0',
    'AND tmi.Pallet IN (' + SQL + ')',
    'GROUP BY ' + Campo1A + ', ' + Campo2A,
    'ORDER BY ' + Campo1A + ', ' + Campo2A + ', NO_PRD_TAM_COR',
    '']);
    //Geral.MB_SQL(Self, QrGGXPalTer);
     //
    MyObjects.frxDefineDataSets(frxTEX_FAXAO_026_02, [
      DModG.frxDsDono,
      frxDsGGXPalTer
    ]);
    //
    Grupo1 := frxTEX_FAXAO_026_02.FindObject('GH001') as TfrxGroupHeader;
    Grupo1.Visible := True;
    Futer1 := frxTEX_FAXAO_026_02.FindObject('GF001') as TfrxGroupFooter;
    Futer1.Visible := Grupo1.Visible;
    Me_GH1 := frxTEX_FAXAO_026_02.FindObject('MeGrupo1Head') as TfrxMemoView;
    Me_FT1 := frxTEX_FAXAO_026_02.FindObject('MeGrupo1Foot') as TfrxMemoView;
    Grupo1.Condition := 'frxDsGGXPalTer."'+Campo1A+'"';
    Me_GH1.Memo.Text := TitGru1+': [frxDsGGXPalTer."'+Campo1B+'"]';
    Me_FT1.Memo.Text := TitGru1+': [frxDsGGXPalTer."'+Campo1B+'"]';
    //
    Grupo2 := frxTEX_FAXAO_026_02.FindObject('GH002') as TfrxGroupHeader;
    Grupo2.Visible := False;
    Futer2 := frxTEX_FAXAO_026_02.FindObject('GF002') as TfrxGroupFooter;
    Futer2.Visible := Grupo2.Visible;
(*
    Grupo2.Visible := True;
    Futer2.Visible := Grupo2.Visible;
    Me_GH2 := frxTEX_FAXAO_026_02.FindObject('MeGrupo2Head') as TfrxMemoView;
    Me_FT2 := frxTEX_FAXAO_026_02.FindObject('MeGrupo2Foot') as TfrxMemoView;
    Grupo2.Condition := 'frxDsGGXPalTer."'+Campo2A+'"';
    Me_GH2.Memo.Text := TitGru2+': [frxDsGGXPalTer."'+Campo2B+'"]';
    Me_FT2.Memo.Text := TitGru2+': [frxDsGGXPalTer."'+Campo2B+'"]';
*)
    //
    MyObjects.frxMostra(frxTEX_FAXAO_026_02, 'Lista Pallets x Fornecedor');
  end;
end;

procedure TFmTXImpPallet.PreparaPallets(TXMovImp4, TXLstPalBox: String;
  EmptyLabels: Integer);
const
  sProcName = 'FmTXImpPallet.ImprimePallets()';
var
  SQL_Empresa: String;
  I, Inteiro: Integer;
begin
  if FEmpresa_Cod <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(FEmpresa_Cod)
  else
    SQL_Empresa := '';
  //
  for I := 0 to EmptyLabels - 1 do
  begin
    Inteiro := High(Integer) - I;
    //Inteiro := - (I + 1);
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, TXMovImp4, False,
    ['Ativo', 'OrdGGX'], ['Pallet'], [2, I + 1], [Inteiro], False);
  end;
  //
end;

procedure TFmTXImpPallet.ImprimeTermica();
var
  Page: TfrxReportPage;
  //Page: TfrxPage;
  // Topo em branco
(*
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  BANLEF, BANTOP, BANWID, BANHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  L, T, W, H, i: Integer;
  Fator: Double;
*)
//begin
(*

  // Configura p�gina
  Page := frxTEX_FAXAO_026_03.FindObject('Page1') as TfrxReportPage;
  //Page := frxTEX_FAXAO_026_03.Pages[0];
  Page.LeftMargin   := 0;//QrPagePagLef.Value / VAR_DOTIMP;
  Page.RightMargin  := 0;
  Page.TopMargin    := 0;//QrPagePagTop.Value / VAR_DOTIMP;
  Page.BottomMargin := 0;
  //
  Page.PaperSize    := DMPAPER_USER;
  Page.PaperHeight  := 50;
  Page.PaperWidth   := 50;
  //
//begin
*)


{
var pa_porta: string;
  pa_nLen, pa_cortador, pa_tamanho_etiqueta: integer;
  pa_tem_cortador, pa_ribon, pa_papelcontinuo: boolean;
begin
  //setar variaveis da impressora
  pa_porta := 'USB';

  if pa_porta = 'USB' then // se a impressora � conectada na USB
  begin
    pa_nLen := A_GetUSBBufferLen();
    A_CreateUSBPort(1);
  end
  else
  begin
    A_CreatePrn(1, 'c:\lpt1.prn'); //se a impressora � LPT1
  end;

  A_Prn_Text_TrueType(20, 160, 60, 'Arial', 1, 0, 0, 0, 0, 'A1', pwidechar('XXXXX'), 1);
  A_Prn_Text_TrueType(250, 160, 60, 'Arial', 1, 0, 0, 0, 0, 'A11', pchar('10 UN'), 1);
  A_Prn_Text_TrueType(20, 135, 50, 'Impact', 1, 0, 0, 0, 0, 'A2', pchar('DESCRICAO DO MATERIAL 1'), 1);
  A_Prn_Text_TrueType(20, 115, 50, 'Impact', 1, 0, 0, 0, 0, 'A21', pchar('DESCRICAO DO MATERIAL 2'), 1);
  A_Prn_Text_TrueType(20, 90, 50, 'Arial', 1, 0, 0, 0, 0, 'A3', pchar('Endere�o: R01.01.01.01'), 1);
  A_Prn_Barcode(150, 15, 1, 'E', 2, 2, 30, 'N', 0, pchar('XXXXX'));
  //pa_tamanho_etiqueta := 170;
  pa_tamanho_etiqueta := 300;

  A_Set_Darkness(round(8));
  A_Set_Speed('G');
  //A_Set_Backfeed(round(170));

  pa_tem_cortador := false;
  pa_ribon := true;
  pa_papelcontinuo := False; //etiqueta ou papel continuo


  if pa_tem_cortador then
    pa_cortador := 1 //com_cortador de papel
  else
    pa_cortador := 9; //sem_cortador cortador de papel

  if pa_ribon then
  begin
    A_Set_Syssetting(2, pa_cortador, 0, 0, 0); //tem ribbon
  end
  else
  begin
    A_Set_Syssetting(1, pa_cortador, 0, 0, 0); //n�o tem ribbon
  end;

  if pa_papelcontinuo then
  begin
    A_Set_Sensor_Mode('c', round(pa_tamanho_etiqueta)); //papel continuo
  end
  else
  begin
    A_Set_Sensor_Mode('r', 0); //etiqueta
  end;

  if A_Print_Out(1, 1, 1, 1) <> 0 then
    MessageDlg('N�o conseguiu imprimir!', mtError, [mbOk], 0);

  A_ClosePrn();
end;
}

begin
  MyObjects.frxDefineDataSets(frxTEX_FAXAO_026_03, [
  DModG.frxDsDono,
  frxDsEstqR5
  ]);
  MyObjects.frxMostra(frxTEX_FAXAO_026_03, 'Pallet de Artigo de Ribeira');
end;

function TFmTXImpPallet.Obtem_TXImpRandStr(): String;
begin
  if FJaDescript_TXImpRandStr = False then
  begin
    FTXImpRandStr := DmkPF.HexDecripto(VAR_TXImpRandStr(*Dmod.QrControleTXImpRandStr.Value*), CO_RandStrWeb01);
    FJaDescript_TXImpRandStr := True;
  end;
  Result := FTXImpRandStr;
end;

procedure TFmTXImpPallet.QrEstqR5CalcFields(DataSet: TDataSet);
begin
  QrEstqR5TEXTO_QRCODE.Value := QrEstqR5NO_PRD_TAM_COR.Value +
    sLineBreak + QrEstqR5NO_PALLET.Value;
end;

procedure TFmTXImpPallet.ReopenGraGruX;
begin
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

function TFmTXImpPallet.SQL_ListaPallets(): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(FPallets);
  for I := Low(FPallets) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(FPallets[I]);
    if I < N then
      Result := Result + ',';
  end;
end;

procedure TFmTXImpPallet.TbTXMovImp4BeforePost(DataSet: TDataSet);
begin
  if TbTXMovImp4Qtde.Value = 0 then
  begin
    TbTXMovImp4Ativo.Value := 2;
  end else
  begin
    TbTXMovImp4Ativo.Value := 3;
    //
    TbTXMovImp4NO_PRD_TAM_COR.Value := TbTXMovImp4NO_ARTIGO.Value;
    TbTXMovImp4Empresa.Value := FEmpresa_Cod;
    TbTXMovImp4NO_Empresa.Value := FEmpresa_Txt;
    TbTXMovImp4SdoVrtQtd.Value  := TbTXMovImp4Qtde.Value;
    TbTXMovImp4DataHora.Value   := Now();
  end;
end;

end.
