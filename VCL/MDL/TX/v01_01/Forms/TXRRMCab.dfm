object FmTXRRMCab: TFmTXRRMCab
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-178 :: Reproceso / Reparo de Material'
  ClientHeight = 773
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 668
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 329
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 461
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 940
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit0
      end
      object Label2: TLabel
        Left = 80
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 592
        Top = 16
        Width = 107
        Height = 13
        Caption = 'Liberado p/ classificar:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 344
        Top = 56
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label23: TLabel
        Left = 476
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Data/hora gera'#231#227'o:'
        FocusControl = DBEdit15
      end
      object Label32: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label33: TLabel
        Left = 708
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Configur. p/ classif.:'
        FocusControl = DBEdit25
      end
      object Label34: TLabel
        Left = 824
        Top = 16
        Width = 83
        Height = 13
        Caption = 'Fim classifica'#231#227'o:'
        FocusControl = DBEdit26
      end
      object Label55: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit42
      end
      object Label58: TLabel
        Left = 680
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label59: TLabel
        Left = 888
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label67: TLabel
        Left = 20
        Top = 164
        Width = 91
        Height = 13
        Caption = 'Fluxo de produ'#231#227'o:'
      end
      object DBEdit0: TdmkDBEdit
        Left = 940
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        DataSource = DsTXRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 6
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXRRMCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 136
        Top = 32
        Width = 337
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXRRMCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 592
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrLibOpe'
        DataSource = DsTXRRMCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 344
        Top = 72
        Width = 333
        Height = 21
        DataField = 'Nome'
        DataSource = DsTXRRMCab
        TabOrder = 4
      end
      object DBEdit15: TDBEdit
        Left = 476
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtHrAberto'
        DataSource = DsTXRRMCab
        TabOrder = 5
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit25: TDBEdit
        Left = 708
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrCfgOpe'
        DataSource = DsTXRRMCab
        TabOrder = 7
      end
      object DBEdit26: TDBEdit
        Left = 824
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrFimOpe'
        DataSource = DsTXRRMCab
        TabOrder = 8
      end
      object DBEdit42: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsTXRRMCab
        TabOrder = 9
      end
      object DBEdit43: TDBEdit
        Left = 72
        Top = 72
        Width = 269
        Height = 21
        DataField = 'NO_Cliente'
        DataSource = DsTXRRMCab
        TabOrder = 10
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 920
        Top = 72
        Width = 81
        Height = 21
        TabStop = False
        DataField = 'NFeRem'
        DataSource = DsTXRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 11
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit44: TDBEdit
        Left = 680
        Top = 72
        Width = 205
        Height = 21
        DataField = 'LPFMO'
        DataSource = DsTXRRMCab
        TabOrder = 12
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 888
        Top = 72
        Width = 29
        Height = 21
        TabStop = False
        DataField = 'SerieRem'
        DataSource = DsTXRRMCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 13
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit49: TDBEdit
        Left = 112
        Top = 160
        Width = 56
        Height = 21
        DataField = 'FluxoCab'
        DataSource = DsTXRRMCab
        TabOrder = 14
      end
      object DBEdit50: TDBEdit
        Left = 168
        Top = 160
        Width = 829
        Height = 21
        DataField = 'NO_FLUXOCAB'
        DataSource = DsTXRRMCab
        TabOrder = 15
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 96
        Width = 195
        Height = 61
        Caption = ' Origem:'
        TabOrder = 16
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label36: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label12: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit27: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeSrc'
            TabOrder = 0
          end
          object DBEdit6: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTSrc'
            TabOrder = 1
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 410
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Destino:'
        TabOrder = 17
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label14: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit31: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeDst'
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTDst'
            TabOrder = 1
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 804
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Saldo em industrializa'#231#227'o:'
        TabOrder = 18
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label44: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label16: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit35: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeSdo'
            TabOrder = 0
          end
          object DBEdit10: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTSdo'
            TabOrder = 1
          end
        end
      end
      object GroupBox1: TGroupBox
        Left = 213
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Em industrializa'#231#227'o:'
        TabOrder = 19
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label13: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label15: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit5: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeInn'
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTInn'
            TabOrder = 1
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 607
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Baixas normais:'
        TabOrder = 20
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label39: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label17: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit30: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeBxa'
            TabOrder = 0
          end
          object DBEdit9: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTBxa'
            TabOrder = 1
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 604
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 202
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 376
        Top = 15
        Width = 630
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 497
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 296
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em Reprocesso'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtOri: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Origem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtOriClick
        end
        object BtDst: TBitBtn
          Tag = 447
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Destino'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDstClick
        end
        object BtInn: TBitBtn
          Tag = 435
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em processo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtInnClick
        end
      end
    end
    object PCRRMOri: TPageControl
      Left = 0
      Top = 185
      Width = 1008
      Height = 144
      ActivePage = TsRetornoMO
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Palltets'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 728
          Height = 92
          Align = alClient
          DataSource = DsTXRRMOriPallet
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end>
        end
        object GroupBox7: TGroupBox
          Left = 728
          Top = 0
          Width = 272
          Height = 92
          Align = alRight
          Caption = ' Baixas for'#231'adas: '
          TabOrder = 1
          object DBGrid3: TDBGrid
            Left = 2
            Top = 15
            Width = 268
            Height = 75
            Align = alClient
            DataSource = DsForcados
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso Kg'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'IME-Is'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DGDadosOri: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          DataSource = DsTXRRMOriIMEI
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = 'm'#178' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Pesagens de recurtimento'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid6: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Pesagem'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataEmis'
              Title.Caption = 'Emiss'#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Pe'#231'as'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Caption = 'Peso KG'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fulao'
              Title.Caption = 'Ful'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Receita'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Nome da Receita'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECI'
              Title.Caption = 'Dono dos produtos'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Custo'
              Title.Caption = 'Custo Total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Obs'
              Width = 600
              Visible = True
            end>
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Outras baixas de insumos'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid7: TDBGrid
          Left = 0
          Top = 0
          Width = 517
          Height = 92
          Align = alLeft
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataB'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoTotal'
              Title.Caption = 'Custo'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmitGru'
              Title.Caption = 'Grupo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMITGRU'
              Title.Caption = 'Nome Grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Width = 90
              Visible = True
            end>
        end
        object DBGIts: TDBGrid
          Left = 517
          Top = 0
          Width = 483
          Height = 92
          Align = alClient
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Insumo'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'digo'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliDest'
              Title.Caption = 'Empresa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Mercadoria'
              Width = 366
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Alignment = taRightJustify
              Title.Caption = 'Quantidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor total'
              Width = 92
              Visible = True
            end>
        end
      end
      object TsEnvioMO: TTabSheet
        Caption = ' Dados do envio para M.O. '
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnEnvioMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 92
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 108
          object DBGTXMOEnvEnv: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 92
            Align = alLeft
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFEMP_nNF'
                Title.Caption = 'N'#186' NF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGTXMOEnvETmi: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 471
            Height = 92
            Align = alClient
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TsRetornoMO: TTabSheet
        Caption = ' Retorno da M.O. '
        ImageIndex = 5
        object PnRetornoMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 116
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGTXMOEnvRet: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 661
            Height = 116
            Align = alLeft
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_nCT'
                Title.Caption = 'N'#186' CT'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_AreaM2'
                Title.Caption = 'Area m'#178
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_SerNF'
                Title.Caption = 'S'#233'r. Ret'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_nNF'
                Title.Caption = 'NF Retorno'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_SerNF'
                Title.Caption = 'S'#233'r. M.O.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_nNF'
                Title.Caption = 'NF M.O.'
                Visible = True
              end>
          end
          object PnItensRetMO: TPanel
            Left = 661
            Top = 0
            Width = 339
            Height = 116
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Splitter2: TSplitter
              Left = 0
              Top = 31
              Width = 339
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitLeft = 585
              ExplicitTop = 1
              ExplicitWidth = 208
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 0
              Width = 339
              Height = 31
              Align = alClient
              Caption = ' Itens de NFes enviados para M.O.: '
              TabOrder = 0
              object DBGTXMOEnvRTmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 14
                TabStop = False
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NFEMP_SerNF'
                    Title.Caption = 'S'#233'r. NF'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFEMP_nNF'
                    Title.Caption = 'NF'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor Total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Title.Caption = 'Valor Frete'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TXMOEnvEnv'
                    Title.Caption = 'ID Envio'
                    Visible = True
                  end>
              end
            end
            object GroupBox9: TGroupBox
              Left = 0
              Top = 36
              Width = 339
              Height = 80
              Align = alBottom
              Caption = ' IME-Is de couros prontos retornados: '
              TabOrder = 1
              object DBGTXMOEnvGTmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 63
                TabStop = False
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TXMovIts'
                    Title.Caption = 'IME-I'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
    object PCInnDst: TPageControl
      Left = 0
      Top = 444
      Width = 1008
      Height = 160
      ActivePage = TsInn
      Align = alBottom
      TabOrder = 3
      object TsInn: TTabSheet
        Caption = ' Produtos em Processo'
        object DBGrid9: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 132
          Align = alClient
          DataSource = DsTXRRMInn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Tabela'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_LOC_CEN'
              Title.Caption = 'Local e centro de estoque '
              Width = 137
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PALLET'
              Title.Caption = 'Hist'#243'rico'
              Width = 145
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos destino da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtQtd'
              Title.Caption = 'Saldo qtde.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMP'
              Title.Caption = 'Valor MP'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOUni'
              Title.Caption = 'M.O. Uni'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOTot'
              Title.Caption = 'Custo MO Tot'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CusFrtMORet'
              Title.Caption = 'Frete retorno'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = 'Valor total'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / Hora'
              Visible = True
            end>
        end
      end
      object TsDst: TTabSheet
        Caption = ' Produtos Prontos (Destino) '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 142
        object PCRRMDst: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 132
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 0
          ExplicitTop = 504
          ExplicitWidth = 1008
          ExplicitHeight = 100
          object TabSheet3: TTabSheet
            Caption = ' Destino Semi Acabado'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 104
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1000
              ExplicitHeight = 72
              object DGDadosDst: TDBGrid
                Left = 0
                Top = 0
                Width = 585
                Height = 104
                Align = alClient
                DataSource = DsTXRRMDst
                PopupMenu = PMDst
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_TTW'
                    Title.Caption = 'Arquivo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ReqMovEstq'
                    Title.Caption = 'N'#176' RME'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / hora'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'IME-I'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PALLET'
                    Title.Caption = 'Hist'#243'rico'
                    Width = 145
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_SerieFch'
                    Title.Caption = 'S'#233'rie RMP'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ficha'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Artigos destino da opera'#231#227'o'
                    Title.Font.Charset = DEFAULT_CHARSET
                    Title.Font.Color = clWindowText
                    Title.Font.Height = -11
                    Title.Font.Name = 'MS Sans Serif'
                    Title.Font.Style = [fsBold]
                    Width = 240
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NotaMPAG'
                    Title.Caption = 'Nota MPAG'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = 'm'#178' gerado'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORNECE'
                    Title.Caption = 'Fornecedor'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observ'
                    Title.Caption = 'Observa'#231#245'es'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Visible = True
                  end>
              end
              object Panel14: TPanel
                Left = 585
                Top = 0
                Width = 407
                Height = 104
                Align = alRight
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                ExplicitLeft = 593
                ExplicitHeight = 72
                object PCRRMDestSub: TPageControl
                  Left = 0
                  Top = 0
                  Width = 407
                  Height = 104
                  ActivePage = TabSheet9
                  Align = alClient
                  TabOrder = 0
                  ExplicitHeight = 72
                  object TabSheet8: TTabSheet
                    Caption = ' Baixa destino'
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 44
                    object DBGrid1: TDBGrid
                      Left = 0
                      Top = 0
                      Width = 399
                      Height = 120
                      Align = alClient
                      DataSource = DsTXRRMBxa
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Controle'
                          Title.Caption = 'IME-I'
                          Width = 60
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Pallet'
                          Width = 60
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'GraGruX'
                          Title.Caption = 'Reduzido'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Pecas'
                          Title.Caption = 'Pe'#231'as'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'AreaM2'
                          Title.Caption = 'm'#178' gerado'
                          Width = 60
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'PesoKg'
                          Title.Caption = 'Peso kg'
                          Width = 60
                          Visible = True
                        end>
                    end
                  end
                  object TabSheet9: TTabSheet
                    Caption = ' Cobran'#231'a MO'
                    ImageIndex = 1
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 44
                    object dmkDBGridZTO1: TdmkDBGridZTO
                      Left = 0
                      Top = 0
                      Width = 399
                      Height = 120
                      Align = alClient
                      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                    end
                  end
                end
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Destino Desclassificado'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 72
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object DBGrid4: TDBGrid
                Left = 1
                Top = 1
                Width = 642
                Height = 70
                Align = alClient
                DataSource = DsTXRRMDesclDst
                PopupMenu = PMDst
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_TTW'
                    Title.Caption = 'Arquivo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ReqMovEstq'
                    Title.Caption = 'N'#176' RME'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / hora'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'IME-I'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PALLET'
                    Title.Caption = 'Hist'#243'rico'
                    Width = 145
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_SerieFch'
                    Title.Caption = 'S'#233'rie RMP'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ficha'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD_TAM_COR'
                    Title.Caption = 'Artigos destino da opera'#231#227'o'
                    Title.Font.Charset = DEFAULT_CHARSET
                    Title.Font.Color = clWindowText
                    Title.Font.Height = -11
                    Title.Font.Name = 'MS Sans Serif'
                    Title.Font.Style = [fsBold]
                    Width = 240
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NotaMPAG'
                    Title.Caption = 'Nota MPAG'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = 'm'#178' gerado'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_FORNECE'
                    Title.Caption = 'Fornecedor'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observ'
                    Title.Caption = 'Observa'#231#245'es'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Visible = True
                  end>
              end
              object DBGrid5: TDBGrid
                Left = 643
                Top = 1
                Width = 356
                Height = 70
                Align = alRight
                DataSource = DsTXRRMDesclBxa
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'IME-I'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pallet'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = 'm'#178' gerado'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 60
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'Destino Sub-produto WB'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid8: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 72
              Align = alClient
              PopupMenu = PMDst
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TTW'
                  Title.Caption = 'Arquivo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ReqMovEstq'
                  Title.Caption = 'N'#176' RME'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataHora'
                  Title.Caption = 'Data / hora'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'IME-I'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pallet'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PALLET'
                  Title.Caption = 'Hist'#243'rico'
                  Width = 145
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_SerieFch'
                  Title.Caption = 'S'#233'rie RMP'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ficha'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Artigos destino da opera'#231#227'o'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = [fsBold]
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NotaMPAG'
                  Title.Caption = 'Nota MPAG'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = 'm'#178' gerado'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_FORNECE'
                  Title.Caption = 'Fornecedor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 668
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita1: TGroupBox
      Left = 0
      Top = 56
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 560
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 712
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 182
        Height = 13
        Caption = 'Observa'#231#227'o sobre a opera'#231#227'o (IMEC):'
      end
      object Label56: TLabel
        Left = 448
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 644
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label66: TLabel
        Left = 16
        Top = 96
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 560
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdHora: TdmkEdit
        Left = 668
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 712
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 429
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrVSGerArt'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdLPFMO: TdmkEdit
        Left = 448
        Top = 72
        Width = 193
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNFeRem: TdmkEdit
        Left = 676
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSerieRem: TdmkEdit
        Left = 644
        Top = 72
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmitGru: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmitGru
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmitGru: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 761
        Height = 21
        Color = clWhite
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 11
        dmkEditCB = EdEmitGru
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 605
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBConfig: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 56
      Align = alTop
      TabOrder = 0
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
        Enabled = False
      end
      object Label64: TLabel
        Left = 100
        Top = 16
        Width = 123
        Height = 13
        Caption = 'Configura'#231#227'o de inclus'#227'o:'
      end
      object SbTXCOPCabCad: TSpeedButton
        Left = 956
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTXCOPCabCadClick
      end
      object SbTXCOPCabCpy: TSpeedButton
        Left = 976
        Top = 32
        Width = 21
        Height = 21
        Caption = '>'
        OnClick = SbTXCOPCabCpyClick
      end
      object EdControle_: TdmkEdit
        Left = 16
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTXCOPCab: TdmkEditCB
        Left = 100
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTXCOPCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTXCOPCab: TdmkDBLookupComboBox
        Left = 156
        Top = 32
        Width = 797
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTXCOPCab
        TabOrder = 2
        dmkEditCB = EdTXCOPCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBEdita2: TGroupBox
      Left = 0
      Top = 193
      Width = 1008
      Height = 140
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label3: TLabel
        Left = 460
        Top = 16
        Width = 91
        Height = 13
        Caption = 'Fluxo de produ'#231#227'o:'
      end
      object Label35: TLabel
        Left = 16
        Top = 56
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label49: TLabel
        Left = 444
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label62: TLabel
        Left = 16
        Top = 16
        Width = 412
        Height = 13
        Caption = 
          'Dono do material: (a pr'#243'pria empresa ou terceiro que contratou s' +
          'ervi'#231'o de m'#227'o de obra)'
      end
      object LaPedItsLib: TLabel
        Left = 16
        Top = 96
        Width = 162
        Height = 13
        Caption = 'Item de pedido de venda atrelado:'
        Enabled = False
      end
      object Label54: TLabel
        Left = 404
        Top = 96
        Width = 226
        Height = 13
        Caption = 'Cliente (somente quando houver pedido pr'#233'vio):'
      end
      object Label10: TLabel
        Left = 908
        Top = 96
        Width = 56
        Height = 13
        Caption = '$ Total MO:'
        Enabled = False
      end
      object EdFluxoCab: TdmkEditCB
        Left = 460
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FluxoCab'
        UpdCampo = 'FluxoCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFluxoCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFluxoCab: TdmkDBLookupComboBox
        Left = 520
        Top = 32
        Width = 477
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFluxoCab
        TabOrder = 3
        dmkEditCB = EdFluxoCab
        QryCampo = 'FluxoCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFornecMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FornecMO'
        UpdCampo = 'FornecMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 369
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 5
        dmkEditCB = EdFornecMO
        QryCampo = 'FornecMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 444
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'StqCenLoc'
        UpdCampo = 'StqCenLoc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 500
        Top = 72
        Width = 497
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 7
        dmkEditCB = EdStqCenLoc
        QryCampo = 'StqCenLoc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClientMO: TdmkEditCB
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClientMO'
        UpdCampo = 'ClientMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 72
        Top = 32
        Width = 385
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 1
        dmkEditCB = EdClientMO
        QryCampo = 'ClientMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPedItsLib: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PedItsLib'
        UpdCampo = 'PedItsLib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPedItsLib
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPedItsLib: TdmkDBLookupComboBox
        Left = 76
        Top = 112
        Width = 325
        Height = 21
        Enabled = False
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsTXPedIts
        TabOrder = 9
        dmkEditCB = EdPedItsLib
        QryCampo = 'PedItsLib'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 404
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 461
        Top = 112
        Width = 444
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 11
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCustoMOTot: TdmkEdit
        Left = 908
        Top = 112
        Width = 94
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'CustoMOTot'
        UpdCampo = 'CustoMOTot'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 388
        Height = 32
        Caption = 'Reproceso / Reparo de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 388
        Height = 32
        Caption = 'Reproceso / Reparo de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 9
        Top = 10
        Width = 388
        Height = 32
        Caption = 'Reproceso / Reparo de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrTXRRMCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXRRMCabBeforeOpen
    AfterOpen = QrTXRRMCabAfterOpen
    BeforeClose = QrTXRRMCabBeforeClose
    AfterScroll = QrTXRRMCabAfterScroll
    OnCalcFields = QrTXRRMCabCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(PecasMan<>0, PecasMan, PecasSrc) PecasINI,'
      'IF(AreaManM2<>0, AreaManM2, AreaSrcM2) AreaINIM2,'
      'IF(AreaManP2<>0, AreaManP2, AreaSrcM2) AreaINIP2,'
      'IF(PesoKgMan<>0, PesoKgMan, PesoKgSrc) PesoKgINI,'
      'voc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsopecab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa'
      'WHERE voc.Codigo > 0')
    Left = 100
    Top = 441
    object QrTXRRMCabNO_TXCOPCab: TWideStringField
      FieldName = 'NO_TXCOPCab'
      Size = 60
    end
    object QrTXRRMCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXRRMCabNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrTXRRMCabNO_FLUXOCAB: TWideStringField
      FieldName = 'NO_FLUXOCAB'
      Size = 100
    end
    object QrTXRRMCabQtdeINI: TFloatField
      FieldName = 'QtdeINI'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXRRMCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'vsopecab.Codigo'
    end
    object QrTXRRMCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'vsopecab.MovimCod'
    end
    object QrTXRRMCabCacCod: TIntegerField
      FieldName = 'CacCod'
      Origin = 'vsopecab.CacCod'
    end
    object QrTXRRMCabFluxoCab: TIntegerField
      FieldName = 'FluxoCab'
    end
    object QrTXRRMCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'vsopecab.Nome'
      Size = 100
    end
    object QrTXRRMCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'vsopecab.Empresa'
    end
    object QrTXRRMCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrTXRRMCabClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrTXRRMCabFornecMO: TIntegerField
      FieldName = 'FornecMO'
      LookupDataSet = Dmod.QrAgora
    end
    object QrTXRRMCabStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrTXRRMCabPeditsLib: TIntegerField
      FieldName = 'PeditsLib'
    end
    object QrTXRRMCabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
      Origin = 'vsopecab.DtHrAberto'
    end
    object QrTXRRMCabDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
      Origin = 'vsopecab.DtHrLibOpe'
    end
    object QrTXRRMCabDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
      Origin = 'vsopecab.DtHrCfgOpe'
    end
    object QrTXRRMCabDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
      Origin = 'vsopecab.DtHrFimOpe'
    end
    object QrTXRRMCabQtdeMan: TFloatField
      FieldName = 'QtdeMan'
      Origin = 'vsopecab.QtdeMan'
    end
    object QrTXRRMCabValorTMan: TFloatField
      FieldName = 'ValorTMan'
      Origin = 'vsopecab.ValorTMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXRRMCabQtdeSrc: TFloatField
      FieldName = 'QtdeSrc'
      Origin = 'vsopecab.QtdeSrc'
    end
    object QrTXRRMCabValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
      Origin = 'vsopecab.ValorTSrc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXRRMCabQtdeInn: TFloatField
      FieldName = 'QtdeInn'
    end
    object QrTXRRMCabValorTInn: TFloatField
      FieldName = 'ValorTInn'
    end
    object QrTXRRMCabQtdeDst: TFloatField
      FieldName = 'QtdeDst'
      Origin = 'vsopecab.QtdeDst'
    end
    object QrTXRRMCabValorTDst: TFloatField
      FieldName = 'ValorTDst'
    end
    object QrTXRRMCabQtdeBxa: TFloatField
      FieldName = 'QtdeBxa'
      Origin = 'vsopecab.PecasBxa'
    end
    object QrTXRRMCabValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
      Origin = 'vsopecab.ValorTBxa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXRRMCabQtdeSdo: TFloatField
      FieldName = 'QtdeSdo'
      Origin = 'vsopecab.QtdeSdo'
    end
    object QrTXRRMCabValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
      Origin = 'vsopecab.ValorTSdo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXRRMCabCustoManMOUni: TFloatField
      FieldName = 'CustoManMOUni'
      Origin = 'vsopecab.CustoManMOUni'
    end
    object QrTXRRMCabCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
      Origin = 'vsopecab.CustoManMOTot'
    end
    object QrTXRRMCabValorManMP: TFloatField
      FieldName = 'ValorManMP'
      Origin = 'vsopecab.ValorManMP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXRRMCabValorManT: TFloatField
      FieldName = 'ValorManT'
      Origin = 'vsopecab.ValorManT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXRRMCabCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrTXRRMCabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrTXRRMCabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrTXRRMCabLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrTXRRMCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrTXRRMCabTXCOPCab: TIntegerField
      FieldName = 'TXCOPCab'
    end
    object QrTXRRMCabNO_DtHrFimOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimOpe'
      Calculated = True
    end
    object QrTXRRMCabNO_DtHrLibOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibOpe'
      Calculated = True
    end
    object QrTXRRMCabNO_DtHrCfgOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgOpe'
      Calculated = True
    end
    object QrTXRRMCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vsopecab.Ativo'
    end
    object QrTXRRMCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vsopecab.AlterWeb'
    end
    object QrTXRRMCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vsopecab.UserAlt'
    end
    object QrTXRRMCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vsopecab.UserCad'
    end
    object QrTXRRMCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vsopecab.DataAlt'
    end
    object QrTXRRMCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vsopecab.DataCad'
    end
    object QrTXRRMCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vsopecab.Lk'
    end
  end
  object DsTXRRMCab: TDataSource
    DataSet = QrTXRRMCab
    Left = 100
    Top = 489
  end
  object QrTXRRMInn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 184
    Top = 441
    object QrTXRRMInnCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXRRMInnControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXRRMInnMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXRRMInnMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXRRMInnMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXRRMInnEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXRRMInnTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXRRMInnCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXRRMInnMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXRRMInnDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXRRMInnPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXRRMInnGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXRRMInnQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMInnValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMInnSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXRRMInnSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXRRMInnSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXRRMInnSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXRRMInnSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXRRMInnObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXRRMInnSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXRRMInnTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXRRMInnFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXRRMInnCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrTXRRMInnCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMInnValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMInnDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXRRMInnDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXRRMInnDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXRRMInnDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXRRMInnQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXRRMInnQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXRRMInnNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXRRMInnNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXRRMInnNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXRRMInnID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXRRMInnNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXRRMInnReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXRRMInnCUSTO_UNI: TFloatField
      FieldName = 'CUSTO_UNI'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrTXRRMInnNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrTXRRMInnMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXRRMInnPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrTXRRMInnStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXRRMInnNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrTXRRMInnClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrTXRRMInnCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMInnCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
    end
    object QrTXRRMInnDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsTXRRMInn: TDataSource
    DataSet = QrTXRRMInn
    Left = 180
    Top = 489
  end
  object PMOri: TPopupMenu
    OnPopup = PMOriPopup
    Left = 656
    Top = 492
    object ItsIncluiOri: TMenuItem
      Caption = '&Adiciona artigo de origem'
      Enabled = False
      object porPallet1: TMenuItem
        Caption = 'por &Pallet'
        object Parcial1: TMenuItem
          Caption = '&Parcial'
          OnClick = Parcial1Click
        end
        object otal1: TMenuItem
          Caption = '&Total'
          OnClick = otal1Click
        end
      end
      object porIMEI1: TMenuItem
        Caption = 'por &IME-I'
        object Parcial2: TMenuItem
          Caption = '&Parcial'
          OnClick = Parcial2Click
        end
        object otal2: TMenuItem
          Caption = '&Total'
          OnClick = otal2Click
        end
      end
    end
    object ItsAlteraOri: TMenuItem
      Caption = '&Edita artigo de origem'
      Enabled = False
      Visible = False
      OnClick = ItsAlteraOriClick
    end
    object ItsExcluiOriIMEI: TMenuItem
      Caption = '&Remove IMEI de origem'
      Enabled = False
      OnClick = ItsExcluiOriIMEIClick
    end
    object ItsExcluiOriPallet: TMenuItem
      Caption = '&Remove Pallet de origem'
      Enabled = False
      OnClick = ItsExcluiOriPalletClick
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME1Click
    end
    object IrparacadastrodoPallet1: TMenuItem
      Caption = 'Ir para cadastro do &Pallet'
      OnClick = IrparacadastrodoPallet1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 656
    Top = 444
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Definiodequantidadesmanualmente1: TMenuItem
      Caption = '&Defini'#231#227'o de quantidades manualmente'
      OnClick = Definiodequantidadesmanualmente1Click
    end
    object CorrigirFornecedor1: TMenuItem
      Caption = 'Corrigir Fornecedor'
      OnClick = CorrigirFornecedor1Click
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 396
    Top = 36
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 396
    Top = 88
  end
  object QrTXRRMOriIMEI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 268
    Top = 441
    object QrTXRRMOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXRRMOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXRRMOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXRRMOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXRRMOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXRRMOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXRRMOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXRRMOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXRRMOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXRRMOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXRRMOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXRRMOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXRRMOriIMEIQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXRRMOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXRRMOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXRRMOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXRRMOriIMEISdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXRRMOriIMEISerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXRRMOriIMEITalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXRRMOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXRRMOriIMEICustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXRRMOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXRRMOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXRRMOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXRRMOriIMEIQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMOriIMEIQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXRRMOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXRRMOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXRRMOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXRRMOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXRRMOriIMEINO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXRRMOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXRRMOriIMEIDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsTXRRMOriIMEI: TDataSource
    DataSet = QrTXRRMOriIMEI
    Left = 268
    Top = 489
  end
  object QrPrestador: TmySQLQuery
    Database = Dmod.MyDB
    Left = 312
    Top = 36
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 312
    Top = 80
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &OP (Ordem de Produ'#231#227'o)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object IMEIArtigodeRibeiragerado1: TMenuItem
      Caption = '&IME-I  Ordem de Opera'#231#227'o'
      OnClick = IMEIArtigodeRibeiragerado1Click
    end
    object OPpreenchida1: TMenuItem
      Caption = 'OP preenchida'
      OnClick = OPpreenchida1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object OrdensdeProduoemAberto1: TMenuItem
      Caption = 'Ordens de produ'#231#227'o em &aberto'
      OnClick = OrdensdeProduoemAberto1Click
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
    object CobranadeMO1: TMenuItem
      Caption = 'Cobran'#231'a de MO'
      OnClick = CobranadeMO1Click
    end
  end
  object QrTXRRMDst: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXRRMDstBeforeClose
    AfterScroll = QrTXRRMDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 448
    Top = 445
    object QrTXRRMDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXRRMDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXRRMDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXRRMDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXRRMDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXRRMDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXRRMDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXRRMDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXRRMDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXRRMDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXRRMDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXRRMDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXRRMDstQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXRRMDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXRRMDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXRRMDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXRRMDstSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXRRMDstSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXRRMDstTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXRRMDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXRRMDstCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXRRMDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXRRMDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXRRMDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXRRMDstQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDstQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXRRMDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXRRMDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXRRMDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXRRMDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXRRMDstNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXRRMDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXRRMDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXRRMDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXRRMDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXRRMDstDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXRRMDstCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
    end
  end
  object DsTXRRMDst: TDataSource
    DataSet = QrTXRRMDst
    Left = 448
    Top = 493
  end
  object PMDst: TPopupMenu
    OnPopup = PMDstPopup
    Left = 656
    Top = 540
    object ItsIncluiDst: TMenuItem
      Caption = '&Adiciona artigo de destino'
      Enabled = False
      OnClick = ItsIncluiDstClick
    end
    object ItsAlteraDst: TMenuItem
      Caption = '&Edita artigo de destino'
      Enabled = False
      OnClick = ItsAlteraDstClick
    end
    object Editaquantidadedebaixa1: TMenuItem
      Caption = 'Edita quantidade de baixa'
      OnClick = Editaquantidadedebaixa1Click
    end
    object Corrigetodasbaixas1: TMenuItem
      Caption = 'Corrige todas baixas'
      OnClick = Corrigetodasbaixas1Click
    end
    object ItsExcluiDst: TMenuItem
      Caption = '&Remove artigo de destino'
      Enabled = False
      OnClick = ItsExcluiDstClick
    end
    object InformaNmerodaRME2: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME2Click
    end
    object AtrelamentoNFsdeMO2: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento2: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento2Click
      end
      object AlteraAtrelamento2: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento2Click
      end
      object ExcluiAtrelamento2: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento2Click
      end
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object Adicionadesclassificado1: TMenuItem
      Caption = 'Adiciona desclassificado'
      OnClick = Adicionadesclassificado1Click
    end
    object Removedesclassificado1: TMenuItem
      Caption = '&Remove desclassificado'
      OnClick = Removedesclassificado1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object IrparajaneladoPallet1: TMenuItem
      Caption = 'Ir para janela do &Pallet'
      OnClick = IrparajaneladoPallet1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object MudarodestinoparaoutraOP1: TMenuItem
      Caption = 'Mudar o destino para outra OP'
      OnClick = MudarodestinoparaoutraOP1Click
    end
  end
  object QrTwn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 840
    Top = 384
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrTXRRMBxa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 528
    Top = 445
    object QrTXRRMBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXRRMBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXRRMBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXRRMBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXRRMBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXRRMBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXRRMBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXRRMBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXRRMBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXRRMBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXRRMBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXRRMBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXRRMBxaQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXRRMBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXRRMBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXRRMBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXRRMBxaSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXRRMBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXRRMBxaSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXRRMBxaTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXRRMBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXRRMBxaCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXRRMBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXRRMBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXRRMBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXRRMBxaQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXRRMBxaQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXRRMBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXRRMBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXRRMBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXRRMBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXRRMBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXRRMBxaNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXRRMBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsTXRRMBxa: TDataSource
    DataSet = QrTXRRMBxa
    Left = 524
    Top = 493
  end
  object QrTXRRMOriPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Pallet, wmi.GraGruX, wmi.Ficha, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,'
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet'
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch '
      'WHERE wmi.MovimCod=662'
      'AND wmi.MovimNiv=7'
      'GROUP BY Pallet'
      'ORDER BY NO_Pallet')
    Left = 356
    Top = 441
    object QrTXRRMOriPalletPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrTXRRMOriPalletGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrTXRRMOriPalletQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTXRRMOriPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXRRMOriPalletNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrTXRRMOriPalletSerieTal: TLargeintField
      FieldName = 'SerieTal'
    end
    object QrTXRRMOriPalletTalao: TLargeintField
      FieldName = 'Talao'
    end
    object QrTXRRMOriPalletNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrTXRRMOriPalletID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrTXRRMOriPalletTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrTXRRMOriPalletMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXRRMOriPalletNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXRRMOriPalletValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTXRRMOriPalletSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsTXRRMOriPallet: TDataSource
    DataSet = QrTXRRMOriPallet
    Left = 356
    Top = 489
  end
  object QrForcados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimID=9 '
      'AND SrcMovID=11'
      'AND SrcNivel1=5'
      '')
    Left = 908
    Top = 420
    object QrForcadosCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForcadosControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrForcadosMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrForcadosMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrForcadosMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrForcadosEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrForcadosTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrForcadosCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrForcadosMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrForcadosDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrForcadosPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrForcadosGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrForcadosQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrForcadosSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrForcadosSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrForcadosSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrForcadosSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrForcadosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrForcadosSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrForcadosTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrForcadosFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrForcadosCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrForcadosDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrForcadosDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrForcadosDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrForcadosQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrForcadosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrForcadosNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrForcadosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsForcados: TDataSource
    DataSet = QrForcados
    Left = 908
    Top = 468
  end
  object QrTXPedIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 708
    Top = 468
    object QrTXPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrTXPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsTXPedIts: TDataSource
    DataSet = QrTXPedIts
    Left = 708
    Top = 512
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 236
    Top = 84
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 236
    Top = 36
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 172
    Top = 36
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 172
    Top = 84
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 56
    object CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem
      Caption = 'Corrige &Fornecedores destino a partir deste processo'
      OnClick = CorrigeFornecedoresdestinoapartirdestaoperao1Click
    end
    object CorrigeMO1: TMenuItem
      Caption = 'Corrige &MO'
      OnClick = CorrigeMO1Click
    end
  end
  object QrClientMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 24
    Top = 444
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 20
    Top = 492
  end
  object QrGGXDst: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 960
    Top = 96
    object QrGGXDstGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXDstControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXDstSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXDstCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXDstNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXDst: TDataSource
    DataSet = QrGGXDst
    Left = 960
    Top = 148
  end
  object QrTXCOPCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 400
    Top = 188
    object QrTXCOPCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXCOPCabNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXCOPCab: TDataSource
    DataSet = QrTXCOPCab
    Left = 400
    Top = 240
  end
  object QrTXRRMDesclDst: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrTXRRMDesclDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 812
    Top = 461
    object QrTXRRMDesclDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXRRMDesclDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXRRMDesclDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXRRMDesclDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXRRMDesclDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXRRMDesclDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXRRMDesclDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXRRMDesclDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXRRMDesclDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXRRMDesclDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXRRMDesclDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXRRMDesclDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXRRMDesclDstQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXRRMDesclDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXRRMDesclDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXRRMDesclDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXRRMDesclDstSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXRRMDesclDstSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXRRMDesclDstTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXRRMDesclDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXRRMDesclDstCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXRRMDesclDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXRRMDesclDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXRRMDesclDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXRRMDesclDstQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclDstQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXRRMDesclDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXRRMDesclDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXRRMDesclDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXRRMDesclDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXRRMDesclDstNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXRRMDesclDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXRRMDesclDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXRRMDesclDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXRRMDesclDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
  end
  object DsTXRRMDesclDst: TDataSource
    DataSet = QrTXRRMDesclDst
    Left = 812
    Top = 509
  end
  object QrTXRRMDesclBxa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 912
    Top = 517
    object QrTXRRMDesclBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXRRMDesclBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXRRMDesclBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXRRMDesclBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXRRMDesclBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXRRMDesclBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXRRMDesclBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXRRMDesclBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXRRMDesclBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXRRMDesclBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXRRMDesclBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXRRMDesclBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXRRMDesclBxaQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXRRMDesclBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXRRMDesclBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXRRMDesclBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXRRMDesclBxaSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXRRMDesclBxaSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXRRMDesclBxaTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXRRMDesclBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXRRMDesclBxaCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXRRMDesclBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXRRMDesclBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXRRMDesclBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXRRMDesclBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXRRMDesclBxaQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclBxaQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXRRMDesclBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXRRMDesclBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXRRMDesclBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXRRMDesclBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXRRMDesclBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXRRMDesclBxaNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXRRMDesclBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsTXRRMDesclBxa: TDataSource
    DataSet = QrTXRRMDesclBxa
    Left = 908
    Top = 565
  end
  object PMInn: TPopupMenu
    OnPopup = PMInnPopup
    Left = 656
    Top = 588
    object ItsIncluiInn: TMenuItem
      Caption = '&Adiciona artigo em processo'
      Enabled = False
    end
    object ItsAlteraInn: TMenuItem
      Caption = '&Edita artigo em processo'
      Enabled = False
    end
    object ItsExcluiInn: TMenuItem
      Caption = '&Remove artigo em processo'
      Enabled = False
    end
    object MenuItem1: TMenuItem
      Caption = '-'
    end
    object AtualizaestoqueEmindustrializao1: TMenuItem
      Caption = 'Atualiza estoque "Em reprocesso"'
      OnClick = AtualizaestoqueEmindustrializao1Click
    end
    object AlteraoParcial1: TMenuItem
      Caption = 'Altera'#231#227'o Parcial'
      object AlteraFornecedorMO1: TMenuItem
        Caption = 'Fornecedor M&O'
        OnClick = AlteraFornecedorMO1Click
      end
      object AlteraLocaldoestoque1: TMenuItem
        Caption = 'Local do esto&que'
        OnClick = AlteraLocaldoestoque1Click
      end
      object AlteraCliente1: TMenuItem
        Caption = 'Cliente'
        OnClick = AlteraCliente1Click
      end
    end
  end
  object QrTXMOEnvEnv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXMOEnvEnvBeforeClose
    AfterScroll = QrTXMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.TXTMI_Controle=:P0')
    Left = 600
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrTXMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrTXMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrTXMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrTXMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrTXMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrTXMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrTXMOEnvEnvNFEMP_Qtde: TFloatField
      FieldName = 'NFEMP_Qtde'
    end
    object QrTXMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrTXMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrTXMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrTXMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrTXMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrTXMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrTXMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrTXMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrTXMOEnvEnvCFTMP_Qtde: TFloatField
      FieldName = 'CFTMP_Qtde'
    end
    object QrTXMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrTXMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrTXMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvEnvTXTMI_MovimCod: TIntegerField
      FieldName = 'TXTMI_MovimCod'
    end
    object QrTXMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTXMOEnvEnv: TDataSource
    DataSet = QrTXMOEnvEnv
    Left = 600
    Top = 56
  end
  object QrTXMOEnvETMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvetmi'
      'WHERE TXMOEnvEnv=:p0')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvETMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvETMITXMOEnvEnv: TIntegerField
      FieldName = 'TXMOEnvEnv'
    end
    object QrTXMOEnvETMITXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrTXMOEnvETMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvETMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvETMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvETMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvETMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvETMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvETMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvETMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvETMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvETMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvETMIQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0'
    end
  end
  object DsTXMOEnvETMI: TDataSource
    DataSet = QrTXMOEnvETMI
    Left = 684
    Top = 56
  end
  object QrTXMOEnvRet: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXMOEnvRetBeforeClose
    AfterScroll = QrTXMOEnvRetAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvret cmo'
      'WHERE cmo.TXTMI_Controle=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrTXMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrTXMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrTXMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrTXMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrTXMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrTXMOEnvRetNFCMO_Qtde: TFloatField
      FieldName = 'NFCMO_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object QrTXMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrTXMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrTXMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrTXMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrTXMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrTXMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrTXMOEnvRetNFRMP_Qtde: TFloatField
      FieldName = 'NFRMP_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object QrTXMOEnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object QrTXMOEnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object QrTXMOEnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object QrTXMOEnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object QrTXMOEnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object QrTXMOEnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object QrTXMOEnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object QrTXMOEnvRetCFTPA_Qtde: TFloatField
      FieldName = 'CFTPA_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTXMOEnvRet: TDataSource
    DataSet = QrTXMOEnvRet
    Left = 768
    Top = 56
  end
  object QrTXMOEnvRTmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, mev.* '
      'FROM vsmoenvrtmi mev'
      'LEFT JOIN vsmoenvenv vee ON vee.Codigo=mev.TXMOEnvEnv')
    Left = 852
    Top = 8
    object QrTXMOEnvRTmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvRTmiTXMOEnvRet: TIntegerField
      FieldName = 'TXMOEnvRet'
    end
    object QrTXMOEnvRTmiTXMOEnvEnv: TIntegerField
      FieldName = 'TXMOEnvEnv'
    end
    object QrTXMOEnvRTmiQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRTmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRTmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvRTmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvRTmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvRTmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvRTmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvRTmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvRTmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvRTmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvRTmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvRTmiNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrTXMOEnvRTmiNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrTXMOEnvRTmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTXMOEnvRTmi: TDataSource
    DataSet = QrTXMOEnvRTmi
    Left = 852
    Top = 56
  end
  object QrTXMOEnvGTmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmoenvgtmi')
    Left = 940
    Top = 8
    object QrTXMOEnvGTmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvGTmiTXMOEnvRet: TIntegerField
      FieldName = 'TXMOEnvRet'
    end
    object QrTXMOEnvGTmiTXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrTXMOEnvGTmiQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvGTmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvGTmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvGTmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvGTmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvGTmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvGTmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvGTmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvGTmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvGTmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvGTmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvGTmiTXMovimCod: TIntegerField
      FieldName = 'TXMovimCod'
    end
  end
  object DsTXMOEnvGTmi: TDataSource
    DataSet = QrTXMOEnvGTmi
    Left = 940
    Top = 56
  end
  object QrFluxoCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM fluxocab'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 600
    Top = 444
    object QrFluxoCabCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrFluxoCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsFluxoCab: TDataSource
    DataSet = QrFluxoCab
    Left = 600
    Top = 488
  end
end
