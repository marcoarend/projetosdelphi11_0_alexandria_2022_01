unit TXPalletAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, AppListas, UnAppEnums;

type
  TFmTXPalletAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    DsClientes: TDataSource;
    QrTXPalSta: TmySQLQuery;
    QrTXPalStaCodigo: TIntegerField;
    QrTXPalStaNome: TWideStringField;
    DsTXPalSta: TDataSource;
    QrVSRibCla: TmySQLQuery;
    QrVSRibClaGraGruX: TIntegerField;
    QrVSRibClaNO_PRD_TAM_COR: TWideStringField;
    DsVSRibCla: TDataSource;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    SBCliente: TSpeedButton;
    LaVSRibCla: TLabel;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCliStat: TdmkEditCB;
    CBCliStat: TdmkDBLookupComboBox;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdQtdPrevPc: TdmkEdit;
    QrVSRibClaPrevPcPal: TIntegerField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    EdClientMO: TdmkEditCB;
    Label20: TLabel;
    CBClientMO: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenRibCla();
  public
    { Public declarations }
    FMovimIDGer: TEstqMovimID;
    FPallet, FNewPallet: Integer;
  end;

  var
  FmTXPalletAdd: TFmTXPalletAdd;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmTXPalletAdd.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Empresa, ClientMO, Status, CliStat, GraGruX, MovimIDGer, QtdPrevPc: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  Status         := EdStatus.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  CliStat        := EdCliStat.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  MovimIDGer     := Integer(FMovimIDGer);
  QtdPrevPc      := EdQtdPrevPc.ValueVariant;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then Exit;
  //
  if (SQLType = stIns) and (FNewPallet <> 0) then
    Codigo := FNewPallet
  else
    Codigo := UMyMod.BPGS1I32('TXPalleta', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'TXPalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'MovimIDGer',
  'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, MovimIDGer,
  QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    FNewPallet := 0;
    FPallet    := Codigo;
(*
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
*)
      Close;
  end;
end;

procedure TFmTXPalletAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXPalletAdd.EdEmpresaRedefinido(Sender: TObject);
var
  Empresa: Integer;
begin
  if (EdEmpresa.ValueVariant <> 0) and (ImgTipo.SQLType = stIns) then
  begin
    Empresa := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
    EdClientMO.ValueVariant := Empresa;
    CBClientMO.KeyValue     := Empresa;
  end;
end;

procedure TFmTXPalletAdd.EdGraGruXRedefinido(Sender: TObject);
begin
  if (QrVSRibClaPrevPcPal.Value <> 0) and (ImgTipo.SQLType = stIns) then
    EdQtdPrevPc.ValueVariant := QrVSRibClaPrevPcPal.Value;
end;

procedure TFmTXPalletAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXPalletAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FPallet := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTXPalSta, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrVSRibCla, Dmod.MyDB);
  ReopenRibCla();
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
end;

procedure TFmTXPalletAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXPalletAdd.ReopenRibCla();
begin
{POIU
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSRibCla, Dmod.MyDB, [
  'SELECT ggx.Controle GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, cou.PrevPcPal ',
  'FROM gragrux ggx ',
  'LEFT JOIN vsribcla wmp ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'WHERE ggx.GragruY IN (' +
  Geral.FF0(CO_GraGruY_1536_VSCouCal) + ',' +
  //Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
  Geral.FF0(CO_GraGruY_3072_VSRibCla)// + ',' +
  //Geral.FF0(CO_GraGruY_6144_VSFinCla) +
  + ')',
  'ORDER BY ggx.GragruY DESC, NO_PRD_TAM_COR ',
  '']);
}
end;

procedure TFmTXPalletAdd.SBClienteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdCliStat.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCliStat.Text := IntToStr(VAR_ENTIDADE);
    CBCliStat.KeyValue := VAR_ENTIDADE;
  end;
end;

end.
