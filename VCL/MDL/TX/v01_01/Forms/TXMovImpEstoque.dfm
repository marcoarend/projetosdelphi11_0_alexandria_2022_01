object FmTXMovImpEstoque: TFmTXMovImpEstoque
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-123 :: Impress'#227'o de Estoque TX'
  ClientHeight = 731
  ClientWidth = 1011
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1011
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 963
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 915
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 314
        Height = 32
        Caption = 'Impress'#227'o de Estoque TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 314
        Height = 32
        Caption = 'Impress'#227'o de Estoque TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 314
        Height = 32
        Caption = 'Impress'#227'o de Estoque TX'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1011
    Height = 569
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 42
      Width = 1011
      Height = 527
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel43: TPanel
        Left = 723
        Top = 0
        Width = 288
        Height = 527
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 201
          Width = 288
          Height = 3
          Cursor = crVSplit
          Align = alTop
          ExplicitWidth = 275
        end
        object GroupBox16: TGroupBox
          Left = 0
          Top = 0
          Width = 288
          Height = 201
          Align = alTop
          Caption = ' Grupos de Estoque: '
          TabOrder = 0
          object DBG00GraGruY: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 284
            Height = 184
            Align = alClient
            DataSource = Ds00GraGruY
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Width = 39
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 238
                Visible = True
              end>
          end
        end
        object GroupBox18: TGroupBox
          Left = 0
          Top = 204
          Width = 288
          Height = 323
          Align = alClient
          Caption = ' Artigos: '
          TabOrder = 1
          object DBG00GraGruX: TdmkDBGridZTO
            Left = 2
            Top = 45
            Width = 284
            Height = 276
            Align = alClient
            DataSource = Ds00GraGruX
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 43
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Nome'
                Visible = True
              end>
          end
          object Panel52: TPanel
            Left = 2
            Top = 15
            Width = 284
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Ed00NomeGG1: TEdit
              Left = 8
              Top = 4
              Width = 257
              Height = 21
              TabOrder = 0
              OnChange = Ed00NomeGG1Change
            end
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 723
        Height = 527
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 723
          Height = 527
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          HorzScrollBar.Margin = 15
          HorzScrollBar.Visible = False
          VertScrollBar.ButtonSize = 15
          Align = alClient
          TabOrder = 0
          object Panel24: TPanel
            Left = 0
            Top = 0
            Width = 704
            Height = 522
            BevelOuter = bvNone
            TabOrder = 0
            object Panel25: TPanel
              Left = 0
              Top = 0
              Width = 704
              Height = 173
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Panel47: TPanel
                Left = 0
                Top = 0
                Width = 389
                Height = 173
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object Panel46: TPanel
                  Left = 0
                  Top = 0
                  Width = 389
                  Height = 173
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label53: TLabel
                    Left = 8
                    Top = 4
                    Width = 90
                    Height = 13
                    Caption = 'Centro de estoque:'
                  end
                  object Label55: TLabel
                    Left = 8
                    Top = 44
                    Width = 57
                    Height = 13
                    Caption = 'Fornecedor:'
                  end
                  object Ed00StqCenCad: TdmkEditCB
                    Left = 8
                    Top = 20
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CB00StqCenCad
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CB00StqCenCad: TdmkDBLookupComboBox
                    Left = 64
                    Top = 20
                    Width = 313
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = Ds00StqCenCad
                    TabOrder = 1
                    dmkEditCB = Ed00StqCenCad
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object Ck00DataCompra: TCheckBox
                    Left = 8
                    Top = 84
                    Width = 381
                    Height = 17
                    Caption = 
                      'N'#227'o considerar na soma as compras que n'#227'o chegaram na empresa ai' +
                      'nda.'
                    TabOrder = 2
                  end
                  object RG00ZeroNegat: TRadioGroup
                    Left = 0
                    Top = 129
                    Width = 389
                    Height = 44
                    Align = alBottom
                    Caption = ' Estoques a serem considerados: '
                    Columns = 3
                    ItemIndex = 0
                    Items.Strings = (
                      'Somente positivos'
                      'Positivos e negativos'
                      'Todos inclusive zerados')
                    TabOrder = 4
                  end
                  object Ed00Terceiro: TdmkEditCB
                    Left = 8
                    Top = 60
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 5
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CB00Terceiro
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CB00Terceiro: TdmkDBLookupComboBox
                    Left = 64
                    Top = 60
                    Width = 313
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NOMEENTIDADE'
                    ListSource = Ds00Fornecedor
                    TabOrder = 6
                    dmkEditCB = Ed00Terceiro
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object Ck00EmProcessoBH: TCheckBox
                    Left = 8
                    Top = 104
                    Width = 265
                    Height = 17
                    Caption = 'Mostrar estoque em processo jumped.'
                    Enabled = False
                    TabOrder = 3
                  end
                end
              end
              object GroupBox19: TGroupBox
                Left = 389
                Top = 0
                Width = 315
                Height = 173
                Align = alClient
                Caption = ' Tipo de material: '
                TabOrder = 1
                object DBG00CouNiv2: TdmkDBGridZTO
                  Left = 2
                  Top = 15
                  Width = 311
                  Height = 116
                  Align = alClient
                  DataSource = Ds00CouNiv2
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Width = 39
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Width = 148
                      Visible = True
                    end>
                end
                object Panel69: TPanel
                  Left = 2
                  Top = 131
                  Width = 311
                  Height = 40
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label1: TLabel
                    Left = 4
                    Top = 16
                    Width = 32
                    Height = 13
                    Caption = 'IME-C:'
                  end
                  object Ed00MovimCod: TdmkEdit
                    Left = 44
                    Top = 12
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
              end
            end
            object Panel26: TPanel
              Left = 0
              Top = 173
              Width = 704
              Height = 264
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object RG00_Ordem1: TRadioGroup
                Left = 0
                Top = 0
                Width = 117
                Height = 264
                Align = alLeft
                Caption = ' Ordem 1:'
                ItemIndex = 5
                Items.Strings = (
                  'MP / Artigo'
                  'Fornecedor'
                  'Pallet'
                  'IME-I'
                  'Tal'#227'o'
                  'Marca'
                  'Centro'
                  'Local'
                  'Tipo Material'
                  'Est'#225'gio Artigo'
                  'NFe'
                  'IME-C'
                  'Movimento'
                  'Cliente M.O.'
                  'Fornecedor M.O.')
                TabOrder = 0
              end
              object RG00_Ordem2: TRadioGroup
                Left = 117
                Top = 0
                Width = 117
                Height = 264
                Align = alLeft
                Caption = ' Ordem 2:'
                ItemIndex = 8
                Items.Strings = (
                  'MP / Artigo'
                  'Fornecedor'
                  'Pallet'
                  'IME-I'
                  'Tal'#227'o'
                  'Marca'
                  'Centro'
                  'Local'
                  'Tipo Material'
                  'Est'#225'gio Artigo'
                  'NFe'
                  'IME-C'
                  'Movimento'
                  'Cliente M.O.'
                  'Fornecedor M.O.')
                TabOrder = 1
              end
              object RG00_Ordem3: TRadioGroup
                Left = 234
                Top = 0
                Width = 117
                Height = 264
                Align = alLeft
                Caption = ' Ordem 3:'
                ItemIndex = 0
                Items.Strings = (
                  'MP / Artigo'
                  'Fornecedor'
                  'Pallet'
                  'IME-I'
                  'Tal'#227'o'
                  'Marca'
                  'Centro'
                  'Local'
                  'Tipo Material'
                  'Est'#225'gio Artigo'
                  'NFe'
                  'IME-C'
                  'Movimento'
                  'Cliente M.O.'
                  'Fornecedor M.O.')
                TabOrder = 2
              end
              object Panel49: TPanel
                Left = 585
                Top = 0
                Width = 119
                Height = 264
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 4
                object RG00_Agrupa: TRadioGroup
                  Left = 0
                  Top = 0
                  Width = 119
                  Height = 61
                  Align = alTop
                  Caption = ' Agrupamentos:'
                  Columns = 3
                  ItemIndex = 2
                  Items.Strings = (
                    '0'
                    '1'
                    '2'
                    '3'
                    '4')
                  TabOrder = 0
                end
                object Panel50: TPanel
                  Left = 0
                  Top = 61
                  Width = 119
                  Height = 203
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Panel58: TPanel
                    Left = 0
                    Top = 0
                    Width = 119
                    Height = 45
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Ck00EstoqueEm: TCheckBox
                      Left = 4
                      Top = 0
                      Width = 97
                      Height = 17
                      Caption = 'Estoque em...:'
                      TabOrder = 0
                    end
                    object TP00EstoqueEm: TdmkEditDateTimePicker
                      Left = 4
                      Top = 20
                      Width = 112
                      Height = 21
                      CalColors.TextColor = clMenuText
                      Date = 37636.777157974500000000
                      Time = 37636.777157974500000000
                      TabOrder = 1
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                  end
                  object RG00DescrAgruNoItm: TRadioGroup
                    Left = 0
                    Top = 45
                    Width = 119
                    Height = 158
                    Align = alClient
                    Caption = ' Descri'#231#227'o do item:'
                    ItemIndex = 0
                    Items.Strings = (
                      'N'#227'o definido'
                      'Artigo'
                      #218'ltimo agrupam.')
                    TabOrder = 1
                  end
                end
              end
              object RG00_Ordem4: TRadioGroup
                Left = 351
                Top = 0
                Width = 117
                Height = 264
                Align = alLeft
                Caption = ' Ordem 4:'
                ItemIndex = 2
                Items.Strings = (
                  'MP / Artigo'
                  'Fornecedor'
                  'Pallet'
                  'IME-I'
                  'Tal'#227'o'
                  'Marca'
                  'Centro'
                  'Local'
                  'Tipo Material'
                  'Est'#225'gio Artigo'
                  'NFe'
                  'IME-C'
                  'Movimento'
                  'Cliente M.O.'
                  'Fornecedor M.O.')
                TabOrder = 3
              end
              object RG00_Ordem5: TRadioGroup
                Left = 468
                Top = 0
                Width = 117
                Height = 264
                Align = alClient
                Caption = ' Ordem 5:'
                ItemIndex = 1
                Items.Strings = (
                  'MP / Artigo'
                  'Fornecedor'
                  'Pallet'
                  'IME-I'
                  'Tal'#227'o'
                  'Marca'
                  'Centro'
                  'Local'
                  'Tipo Material'
                  'Est'#225'gio Artigo'
                  'NFe'
                  'IME-C'
                  'Movimento'
                  'Cliente M.O.'
                  'Fornecedor M.O.')
                TabOrder = 5
              end
            end
            object Panel63: TPanel
              Left = 0
              Top = 437
              Width = 704
              Height = 85
              Align = alClient
              TabOrder = 2
              object Label61: TLabel
                Left = 8
                Top = 4
                Width = 69
                Height = 13
                Caption = 'Lista de custo:'
              end
              object Ed00GraCusPrc: TdmkEditCB
                Left = 8
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CB00GraCusPrc
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CB00GraCusPrc: TdmkDBLookupComboBox
                Left = 64
                Top = 20
                Width = 313
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = Ds00GraCusPrc
                TabOrder = 1
                dmkEditCB = Ed00GraCusPrc
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object GroupBox28: TGroupBox
                Left = 388
                Top = 8
                Width = 237
                Height = 73
                Caption = ' Intervalo de NF-es emitidas: '
                TabOrder = 2
                object Panel65: TPanel
                  Left = 2
                  Top = 15
                  Width = 233
                  Height = 56
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label64: TLabel
                    Left = 12
                    Top = 4
                    Width = 30
                    Height = 13
                    Caption = 'Inicial:'
                  end
                  object Label65: TLabel
                    Left = 96
                    Top = 4
                    Width = 25
                    Height = 13
                    Caption = 'Final:'
                  end
                  object Ed00NFeIni: TdmkEdit
                    Left = 8
                    Top = 20
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object Ed00NFeFim: TdmkEdit
                    Left = 96
                    Top = 20
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object Ck00Serie: TCheckBox
                    Left = 180
                    Top = 0
                    Width = 53
                    Height = 17
                    Caption = 'S'#233'rie:'
                    TabOrder = 2
                  end
                  object Ed00Serie: TdmkEdit
                    Left = 180
                    Top = 20
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
              end
            end
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 1011
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label7: TLabel
        Left = 8
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label54: TLabel
        Left = 600
        Top = 0
        Width = 63
        Height = 13
        Caption = 'Data relativa:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 16
        Width = 533
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDataRelativa: TdmkEditDateTimePicker
        Left = 600
        Top = 16
        Width = 112
        Height = 21
        Date = 42199.636796111110000000
        Time = 42199.636796111110000000
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 617
    Width = 1011
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1007
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 661
    Width = 1011
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 865
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 863
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object Qr00GraGruY: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gragruy'
      'ORDER BY Ordem')
    Left = 736
    Top = 104
    object Qr00GraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00GraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object Qr00GraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object Qr00GraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object Ds00GraGruY: TDataSource
    DataSet = Qr00GraGruY
    Left = 736
    Top = 148
  end
  object Qr00CouNiv2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 804
    Top = 104
    object Qr00CouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00CouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object Ds00CouNiv2: TDataSource
    DataSet = Qr00CouNiv2
    Left = 804
    Top = 148
  end
  object Qr00StqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 884
    Top = 104
    object Qr00StqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr00StqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object Ds00StqCenCad: TDataSource
    DataSet = Qr00StqCenCad
    Left = 884
    Top = 148
  end
  object Qr00Fornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 432
    Top = 84
    object Qr00FornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00FornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object Ds00Fornecedor: TDataSource
    DataSet = Qr00Fornecedor
    Left = 436
    Top = 128
  end
  object Qr00GraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > 0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 512
    Top = 84
    object Qr00GraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object Qr00GraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr00GraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr00GraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object Qr00GraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object Qr00GraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object Ds00GraGruX: TDataSource
    DataSet = Qr00GraGruX
    Left = 512
    Top = 128
  end
  object Qr00GraCusPrc: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM gracusprc'
      'ORDER BY Nome')
    Left = 596
    Top = 84
    object Qr00GraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr00GraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object Ds00GraCusPrc: TDataSource
    DataSet = Qr00GraCusPrc
    Left = 596
    Top = 132
  end
end
