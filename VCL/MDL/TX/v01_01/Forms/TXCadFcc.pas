unit TXCadFcc;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO, UnGrade_PF;

type
  TFmTXCadFcc = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtArtigo: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrTXCadFcc: TmySQLQuery;
    DsTXCadFcc: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrTXCadFccGraGruX: TIntegerField;
    QrTXCadFccLk: TIntegerField;
    QrTXCadFccDataCad: TDateField;
    QrTXCadFccDataAlt: TDateField;
    QrTXCadFccUserCad: TIntegerField;
    QrTXCadFccUserAlt: TIntegerField;
    QrTXCadFccAlterWeb: TSmallintField;
    QrTXCadFccAtivo: TSmallintField;
    QrTXCadFccGraGru1: TIntegerField;
    QrTXCadFccNO_PRD_TAM_COR: TWideStringField;
    frxWET_RECUR_013: TfrxReport;
    frxDsCad: TfrxDBDataset;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    PMArtigo: TPopupMenu;
    DBGrid1: TdmkDBGridZTO;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    QrTXCadFccFatorNota: TFloatField;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrGraGruXCou: TmySQLQuery;
    DsGraGruXCou: TDataSource;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    QrTXCadFccArtigoImp: TWideStringField;
    QrTXCadFccClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    QrGraGruXCouPrevPcPal: TIntegerField;
    QrTXCadFccMediaMinM2: TFloatField;
    QrTXCadFccMediaMaxM2: TFloatField;
    DBEdit2: TDBEdit;
    Label15: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label16: TLabel;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    QrGraGruXCouGrandeza: TSmallintField;
    QrTXCadFccGGXTwn: TIntegerField;
    IncluinovoProdutoAcabado1: TMenuItem;
    AlteraProdutoAcabadoselecionado1: TMenuItem;
    RemoveProdutoAcabadoselecionado1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXCadFccAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXCadFccBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
    procedure BtArtigoClick(Sender: TObject);
    procedure PMArtigoPopup(Sender: TObject);
    procedure QrTXCadFccBeforeClose(DataSet: TDataSet);
    procedure QrTXCadFccAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Excluilinkdeprodutos1Click(Sender: TObject);
    procedure IncluinovoProdutoAcabado1Click(Sender: TObject);
    procedure AlteraProdutoAcabadoselecionado1Click(Sender: TObject);
    procedure RemoveProdutoAcabadoselecionado1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    //procedure MostraVSNatArt(SQLType: TSQLType);
    //procedure ReopenVSNatArt(Codigo: Integer);

  public
    { Public declarations }
    FSeq: Integer;
    FQryMul: TmySQLQuery;
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmTXCadFcc: TFmTXCadFcc;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck, UnTX_PF,
  //VSNatArt,
  AppListas, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXCadFcc.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

{POIU
procedure TFmTXCadFcc.MostraVSNatArt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSNatArt, FmVSNatArt, afmoNegarComAviso) then
  begin
    FmVSNatArt.ImgTipo.SQLType := SQLType;
    FmVSNatArt.FQrCab := QrTXCadFcc;
    FmVSNatArt.FDsCab := DsVSNatCad;
    FmVSNatArt.FQrIts := QrVSNatArt;
    //
    FmVSNatArt.LaVSNatCad.Enabled := False;
    FmVSNatArt.EdVSNatCad.Enabled := False;
    FmVSNatArt.CBVSNatCad.Enabled := False;
    //
    FmVSNatArt.LaVSRibCad.Enabled := True;
    FmVSNatArt.EdVSRibCad.Enabled := True;
    FmVSNatArt.CBVSRibCad.Enabled := True;
    //

    FmVSNatArt.EdVSNatCad.ValueVariant := QrTXCadFccGraGruX.Value;
    FmVSNatArt.CBVSNatCad.KeyValue     := QrTXCadFccGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSNatArt.FCodigo := 0;
    end else
    begin
      FmVSNatArt.FCodigo := QrVSNatArtCodigo.Value;
      //
      FmVSNatArt.EdVSRibCad.ValueVariant := QrVSNatArtVSRibCad.Value;
      FmVSNatArt.CBVSRibCad.KeyValue     := QrVSNatArtVSRibCad.Value;
      //
    end;
    FmVSNatArt.ShowModal;
    FmVSNatArt.Destroy;
  end;
end;
}

procedure TFmTXCadFcc.PMArtigoPopup(Sender: TObject);
begin
{POIU
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeprodutos1, QrTXCadFcc);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeprodutos1, QrVSNatArt);
  MyObjects.HabilitaMenuItemItsDel(Alteralinkdeprodutos1, QrVSNatArt);
}
end;

procedure TFmTXCadFcc.PMMPPopup(Sender: TObject);
begin
//  MyObjects.HabilitaMenuItemCabUpd(AlteraMP1, QrTXCadFcc);
  MyObjects.HabilitaMenuItemCabUpd(RemoveProdutoAcabadoselecionado1, QrTXCadFcc);
end;

procedure TFmTXCadFcc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXCadFccGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXCadFcc.DefParams;
begin
  VAR_GOTOTABELA := 'txcadfcc';
  VAR_GOTOMYSQLTABLE := QrTXCadFcc;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wcn.*, cou.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM txcadfcc wcn');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wcn.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wcn.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wcn.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmTXCadFcc.Excluilinkdeprodutos1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{ POIU
  if VS_PF.ExcluiVSNaoVMI(
  'Confirma a retirada do link de artigo selecionado?',
  'VSNatArt', 'Codigo', QrVSNatArtCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSNatArt,
      QrVSNatArtCodigo, QrVSNatArtCodigo.Value);
    ReopenVSNatArt(Codigo);
  end;
}
end;

procedure TFmTXCadFcc.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXCadFcc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXCadFcc.RemoveProdutoAcabadoselecionado1Click(Sender: TObject);
const
  GraGruY = 0;
var
  Controle: Integer;
begin
  Controle := QrTXCadFccGraGruX.Value;
  if TX_PF.ExcluiTXNaoTMI('Confirma a remo��o do reduzido selecionado?',
  'txcadfcc', 'GraGruX', QrTXCadFccGraGruX.Value, Dmod.MyDB) = ID_YES then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
    'GraGruY'], ['Controle'], [
    GraGruY], [Controle], True);
    //
    LocCod(Controle, Controle);
  end;
end;

{POIU
procedure TFmTXCadFcc.ReopenVSNatArt(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSNatArt, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsnatart vna ',
  'LEFT JOIN vsribcad wmp ON wmp.GraGruX=vna.VSRibCad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSNatCad=' + Geral.FF0(QrTXCadFccGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;
}

procedure TFmTXCadFcc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXCadFcc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXCadFcc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXCadFcc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXCadFcc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXCadFcc.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXCadFcc.Alteralinkdeprodutos1Click(Sender: TObject);
begin
{POIU
  MostraVSNatArt(stUpd);
}
end;

procedure TFmTXCadFcc.AlteraProdutoAcabadoselecionado1Click(Sender: TObject);
begin
{ N�o usa !!!
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrTXCadFccGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdFatorNota, ImgTipo, 'gragruxcou');
}
end;

procedure TFmTXCadFcc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXCadFccGraGruX.Value;
  Close;
end;

procedure TFmTXCadFcc.BtConfirmaClick(Sender: TObject);
const
  MediaMinM2 = 0;
  MediaMaxM2 = 0;
  Grandeza   = -1;
var
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, GraGru1, GGXTwn: Integer;
  FatorNota, PrevAMPal, PrevKgPal: Double;
  ArtigoImp, ClasseImp, Nome: String;
begin
{POIU
  GraGruX        := QrTXCadFccGraGruX.Value;
  GraGru1        := QrTXCadFccGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  FatorNota      := EdFatorNota.ValueVariant;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.Text;
  ClasseImp      := EdClasseImp.Text;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  Nome           := EdNome.Text;
  GGXTwn      := EdRmsGGX.ValueVariant;
  //
  if MyObjects.FIC(FatorNota <= 0, EdFatorNota, 'Informe o "Fator Nota MPAG"!') then
    Exit;
  if MyObjects.FIC(CouNiv2 = 0, EdCouNiv2, 'Informe o "Tipo do material"!') then
    Exit;
  if MyObjects.FIC(CouNiv1 = 0, EdCouNiv1, 'Informe a "Parte do material"!') then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txcadfcc', False, [
  'FatorNota', 'GGXTwn'], [
  'GraGruX'], [
  FatorNota, GGXTwn], [
  GraGruX], True) then
  begin
    VS_PF.ReIncluiCouNivs(GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1,
    1, PrevAMPal, PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMinM2);
    //
    if FQryMul <> nil then
    begin
      FQryMul.First;
      while not FQryMul.Eof do
      begin
        GraGruX := FQryMul.FieldByName('Controle').AsInteger;
        VS_PF.ReIncluiCouNivs(
          GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1, (*Bastidao*)-1,
          PrevAMPal, PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2);
        //
        FQryMul.Next;
      end;
    end;
    //
    Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    if FSeq = 0 then
    begin
      LocCod(GraGruX, GraGruX);
    end else
      Close;
  end;
}
end;

procedure TFmTXCadFcc.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTXCadFcc.BtArtigoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArtigo, BtArtigo);
end;

procedure TFmTXCadFcc.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmTXCadFcc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  //VS_PF.ConfiguraRGVSBastidao(RGBastidao, False(*Habilita*), (*Default*)1);
{POIU
  Grade_PF.AbreGraGruXY(QrRmsGGX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_???));
}
  CriaOForm;
end;

procedure TFmTXCadFcc.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXCadFccGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmTXCadFcc.SbImprimeClick(Sender: TObject);
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM txcadfcc wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
end;

procedure TFmTXCadFcc.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXCadFcc.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrTXCadFccGraGruX.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXCadFcc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXCadFcc.QrTXCadFccAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXCadFcc.QrTXCadFccAfterScroll(DataSet: TDataSet);
begin
{POIU
  ReopenVSNatArt(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrTXCadFccGraGruX.Value);
}
end;

procedure TFmTXCadFcc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXCadFcc.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrTXCadFccGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'txcadfcc', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXCadFcc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXCadFcc.Incluilinkdeprodutos1Click(Sender: TObject);
begin
{POIU
  MostraVSNatArt(stIns);
}
end;

procedure TFmTXCadFcc.IncluinovoProdutoAcabado1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_6144_TXCadFcc);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM txcadfcc ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'txcadfcc', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
      AlteraProdutoAcabadoselecionado1Click(Self);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmTXCadFcc.QrTXCadFccBeforeClose(DataSet: TDataSet);
begin
{POIU
  QrVSNatArt.Close;
  QrGraGruXCou.Close;
}
end;

procedure TFmTXCadFcc.QrTXCadFccBeforeOpen(DataSet: TDataSet);
begin
  QrTXCadFccGraGruX.DisplayFormat := FFormatFloat;
end;

end.

