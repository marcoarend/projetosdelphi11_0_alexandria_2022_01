object FmTXIndCab: TFmTXIndCab
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-038 :: Artigo em Industrializa'#231#227'o'
  ClientHeight = 785
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 689
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 560
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 712
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
      end
      object Label56: TLabel
        Left = 448
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 644
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrTXGerArt'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrTXGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrTXGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 560
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdHora: TdmkEdit
        Left = 668
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrTXGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 712
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrTXGerArt'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 429
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrTXGerArt'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdLPFMO: TdmkEdit
        Left = 448
        Top = 72
        Width = 193
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'LPFMO'
        UpdCampo = 'LPFMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNFeRem: TdmkEdit
        Left = 676
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NFeRem'
        UpdCampo = 'NFeRem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSerieRem: TdmkEdit
        Left = 644
        Top = 72
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieRem'
        UpdCampo = 'SerieRem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 626
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita2: TGroupBox
      Left = 0
      Top = 153
      Width = 1008
      Height = 140
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label3: TLabel
        Left = 460
        Top = 16
        Width = 91
        Height = 13
        Caption = 'Fluxo de produ'#231#227'o:'
      end
      object Label35: TLabel
        Left = 16
        Top = 56
        Width = 240
        Height = 13
        Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
      end
      object Label49: TLabel
        Left = 444
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label62: TLabel
        Left = 16
        Top = 16
        Width = 412
        Height = 13
        Caption = 
          'Dono do material: (a pr'#243'pria empresa ou terceiro que contratou s' +
          'ervi'#231'o de m'#227'o de obra)'
      end
      object LaPedItsLib: TLabel
        Left = 16
        Top = 96
        Width = 162
        Height = 13
        Caption = 'Item de pedido de venda atrelado:'
        Enabled = False
      end
      object Label54: TLabel
        Left = 404
        Top = 96
        Width = 226
        Height = 13
        Caption = 'Cliente (somente quando houver pedido pr'#233'vio):'
      end
      object Label10: TLabel
        Left = 908
        Top = 96
        Width = 56
        Height = 13
        Caption = '$ Total MO:'
        Enabled = False
      end
      object EdFluxoCab: TdmkEditCB
        Left = 460
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FluxoCab'
        UpdCampo = 'FluxoCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFluxoCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFluxoCab: TdmkDBLookupComboBox
        Left = 520
        Top = 32
        Width = 477
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFluxoCab
        TabOrder = 3
        dmkEditCB = EdFluxoCab
        QryCampo = 'FluxoCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFornecMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FornecMO'
        UpdCampo = 'FornecMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 369
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 5
        dmkEditCB = EdFornecMO
        QryCampo = 'FornecMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 444
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'StqCenLoc'
        UpdCampo = 'StqCenLoc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 500
        Top = 72
        Width = 497
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 7
        dmkEditCB = EdStqCenLoc
        QryCampo = 'StqCenLoc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClientMO: TdmkEditCB
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClientMO'
        UpdCampo = 'ClientMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 72
        Top = 32
        Width = 385
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 1
        dmkEditCB = EdClientMO
        QryCampo = 'ClientMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPedItsLib: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PedItsLib'
        UpdCampo = 'PedItsLib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPedItsLib
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPedItsLib: TdmkDBLookupComboBox
        Left = 76
        Top = 112
        Width = 325
        Height = 21
        Enabled = False
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsTXPedIts
        TabOrder = 9
        dmkEditCB = EdPedItsLib
        QryCampo = 'PedItsLib'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 404
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 461
        Top = 112
        Width = 444
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 11
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCustoMOTot: TdmkEdit
        Left = 908
        Top = 112
        Width = 94
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'CustoMOTot'
        UpdCampo = 'CustoMOTot'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfig: TGroupBox
      Left = 0
      Top = 97
      Width = 1008
      Height = 56
      Align = alTop
      TabOrder = 0
      object Label61: TLabel
        Left = 16
        Top = 16
        Width = 123
        Height = 13
        Caption = 'Configura'#231#227'o de inclus'#227'o:'
        Enabled = False
      end
      object SbTXCOPCabCad: TSpeedButton
        Left = 956
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
        OnClick = SbTXCOPCabCadClick
      end
      object SbTXCOPCabCpy: TSpeedButton
        Left = 976
        Top = 32
        Width = 21
        Height = 21
        Caption = '>'
        OnClick = SbTXCOPCabCpyClick
      end
      object EdTXCOPCab: TdmkEditCB
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TXCOPCab'
        UpdCampo = 'TXCOPCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTXCOPCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTXCOPCab: TdmkDBLookupComboBox
        Left = 76
        Top = 32
        Width = 877
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTXCOPCab
        TabOrder = 1
        dmkEditCB = EdTXCOPCab
        QryCampo = 'TXCOPCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 689
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 385
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 437
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 197
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 940
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit0
      end
      object Label2: TLabel
        Left = 80
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 592
        Top = 16
        Width = 107
        Height = 13
        Caption = 'Liberado p/ classificar:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label23: TLabel
        Left = 476
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Data/hora abertura:'
        FocusControl = DBEdit15
      end
      object Label32: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label33: TLabel
        Left = 708
        Top = 16
        Width = 94
        Height = 13
        Caption = 'Configur. p/ classif.:'
        FocusControl = DBEdit25
      end
      object Label34: TLabel
        Left = 824
        Top = 16
        Width = 67
        Height = 13
        Caption = 'Fim opera'#231#227'o:'
        FocusControl = DBEdit26
      end
      object Label58: TLabel
        Left = 680
        Top = 56
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label59: TLabel
        Left = 888
        Top = 56
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label60: TLabel
        Left = 428
        Top = 56
        Width = 91
        Height = 13
        Caption = 'Fluxo de produ'#231#227'o:'
        FocusControl = DBEdit42
      end
      object Label17: TLabel
        Left = 20
        Top = 156
        Width = 60
        Height = 13
        Caption = 'CustoMOTot'
        FocusControl = DBEdit11
      end
      object DBEdit0: TdmkDBEdit
        Left = 940
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        DataSource = DsTXIndCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 6
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXIndCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 136
        Top = 32
        Width = 337
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXIndCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 592
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrLibOpe'
        DataSource = DsTXIndCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 72
        Width = 409
        Height = 21
        DataField = 'Nome'
        DataSource = DsTXIndCab
        TabOrder = 4
      end
      object DBEdit15: TDBEdit
        Left = 476
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtHrAberto'
        DataSource = DsTXIndCab
        TabOrder = 5
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXIndCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit25: TDBEdit
        Left = 708
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrCfgOpe'
        DataSource = DsTXIndCab
        TabOrder = 7
      end
      object DBEdit26: TDBEdit
        Left = 824
        Top = 32
        Width = 112
        Height = 21
        DataField = 'NO_DtHrFimOpe'
        DataSource = DsTXIndCab
        TabOrder = 8
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 96
        Width = 195
        Height = 61
        Caption = ' Origem:'
        TabOrder = 9
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label36: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label12: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit27: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeSrc'
            DataSource = DsTXIndCab
            TabOrder = 0
          end
          object DBEdit6: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTSrc'
            DataSource = DsTXIndCab
            TabOrder = 1
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 410
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Destino:'
        TabOrder = 10
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label14: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit31: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeDst'
            DataSource = DsTXIndCab
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTDst'
            DataSource = DsTXIndCab
            TabOrder = 1
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 804
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Saldo em industrializa'#231#227'o:'
        TabOrder = 11
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label44: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label16: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit35: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeSdo'
            DataSource = DsTXIndCab
            TabOrder = 0
          end
          object DBEdit10: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTSdo'
            DataSource = DsTXIndCab
            TabOrder = 1
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 607
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Baixas normais:'
        TabOrder = 12
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label39: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label15: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit30: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeBxa'
            DataSource = DsTXIndCab
            TabOrder = 0
          end
          object DBEdit9: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTBxa'
            DataSource = DsTXIndCab
            TabOrder = 1
          end
        end
      end
      object DBEdit44: TDBEdit
        Left = 680
        Top = 72
        Width = 205
        Height = 21
        DataField = 'LPFMO'
        DataSource = DsTXIndCab
        TabOrder = 13
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 920
        Top = 72
        Width = 81
        Height = 21
        TabStop = False
        DataField = 'NFeRem'
        DataSource = DsTXIndCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 14
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit42: TDBEdit
        Left = 476
        Top = 72
        Width = 202
        Height = 21
        DataField = 'NO_FLUXOCAB'
        DataSource = DsTXIndCab
        TabOrder = 15
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 888
        Top = 72
        Width = 29
        Height = 21
        TabStop = False
        DataField = 'SerieRem'
        DataSource = DsTXIndCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 16
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit14: TDBEdit
        Left = 428
        Top = 72
        Width = 48
        Height = 21
        DataField = 'FluxoCab'
        DataSource = DsTXIndCab
        TabOrder = 17
      end
      object GroupBox1: TGroupBox
        Left = 213
        Top = 96
        Width = 195
        Height = 61
        Caption = 'Em industrializa'#231#227'o:'
        TabOrder = 18
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 191
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label6: TLabel
            Left = 4
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label13: TLabel
            Left = 84
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object DBEdit5: TDBEdit
            Left = 4
            Top = 20
            Width = 76
            Height = 21
            DataField = 'QtdeInn'
            DataSource = DsTXIndCab
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 84
            Top = 20
            Width = 102
            Height = 21
            DataField = 'ValorTInn'
            DataSource = DsTXIndCab
            TabOrder = 1
          end
        end
      end
      object DBEdit11: TDBEdit
        Left = 20
        Top = 172
        Width = 100
        Height = 21
        DataField = 'CustoMOTot'
        DataSource = DsTXIndCab
        TabOrder = 19
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 625
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 202
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 376
        Top = 15
        Width = 630
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 497
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 408
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em Opera'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtOri: TBitBtn
          Tag = 30
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Origem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtOriClick
        end
        object BtDst: TBitBtn
          Tag = 290
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Destino'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtDstClick
        end
        object BtInn: TBitBtn
          Tag = 435
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em processo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtInnClick
        end
      end
    end
    object PCIndOri: TPageControl
      Left = 0
      Top = 197
      Width = 1008
      Height = 188
      ActivePage = TabSheet5
      Align = alTop
      TabOrder = 2
      object TabSheet2: TTabSheet
        Caption = 'IME-Is de mat'#233'rias primas de origem (baixados)'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DGDadosOri: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 160
          Align = alClient
          DataSource = DsTXIndOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IxxMovIX'
              Title.Caption = 'IXX'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IxxFolha'
              Title.Caption = 'Folha'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IxxLinha'
              Title.Caption = 'Lin'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOUni'
              Title.Caption = 'M.O. Uni]'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOTot'
              Title.Caption = 'M.O. tot'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMP'
              Title.Caption = '$ MP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = '$ Total'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdGer'
              Title.Caption = 'Gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXMulFrnCab'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Baixas for'#231'adas'
        ImageIndex = 1
        object DBGrid3: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 160
          Align = alClient
          DataSource = DsForcados
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end>
        end
      end
      object TsEnvioMO: TTabSheet
        Caption = ' Dados do envio para M.O. '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnEnvioMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 160
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGTXMOEnvEnv: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 160
            Align = alLeft
            DataSource = DsTXMOEnvEnv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFEMP_nNF'
                Title.Caption = 'N'#186' NF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_Qtde'
                Title.Caption = 'Quantidade'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGTXMOEnvETmi: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 471
            Height = 160
            Align = alClient
            DataSource = DsTXMOEnvETMI
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TsRetornoMO: TTabSheet
        Caption = ' Retorno da M.O. '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnRetornoMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 160
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGTXMOEnvRet: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 661
            Height = 160
            Align = alLeft
            DataSource = DsTXMOEnvRet
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_nCT'
                Title.Caption = 'N'#186' CT'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_Qtde'
                Title.Caption = 'Quantidade'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_SerNF'
                Title.Caption = 'S'#233'r. Ret'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_nNF'
                Title.Caption = 'NF Retorno'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_SerNF'
                Title.Caption = 'S'#233'r. M.O.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_nNF'
                Title.Caption = 'NF M.O.'
                Visible = True
              end>
          end
          object PnItensRetMO: TPanel
            Left = 661
            Top = 0
            Width = 339
            Height = 160
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Splitter2: TSplitter
              Left = 0
              Top = 79
              Width = 339
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitLeft = 585
              ExplicitTop = 1
              ExplicitWidth = 208
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 0
              Width = 339
              Height = 79
              Align = alClient
              Caption = ' Itens de NFes enviados para M.O.: '
              TabOrder = 0
              object DBGTXMOEnvRTmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 62
                TabStop = False
                Align = alClient
                DataSource = DsTXMOEnvRTmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NFEMP_SerNF'
                    Title.Caption = 'S'#233'r. NF'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFEMP_nNF'
                    Title.Caption = 'NF'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Title.Caption = 'Quantidade'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor Total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Title.Caption = 'Valor Frete'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TXMOEnvEnv'
                    Title.Caption = 'ID Envio'
                    Visible = True
                  end>
              end
            end
            object GroupBox9: TGroupBox
              Left = 0
              Top = 84
              Width = 339
              Height = 76
              Align = alBottom
              Caption = ' IME-Is de couros prontos retornados: '
              TabOrder = 1
              object DBGTXMOEnvGTmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 59
                TabStop = False
                Align = alClient
                DataSource = DsTXMOEnvGTmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TXMovIts'
                    Title.Caption = 'IME-I'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Title.Caption = 'Quantidade'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
    object PCInnDst: TPageControl
      Left = 0
      Top = 390
      Width = 1008
      Height = 207
      ActivePage = TsInn
      Align = alTop
      TabOrder = 3
      object TsInn: TTabSheet
        Caption = 'Produtos em processo '
        object DBGrid4: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 179
          Align = alClient
          DataSource = DsTXIndInn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Tabela'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_LOC_CEN'
              Title.Caption = 'Local e centro de estoque '
              Width = 137
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PALLET'
              Title.Caption = 'Hist'#243'rico'
              Width = 145
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos destino da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtQtd'
              Title.Caption = 'Saldo qtde.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMP'
              Title.Caption = 'Valor MP'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOUni'
              Title.Caption = 'M.O. Uni'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoMOTot'
              Title.Caption = 'Custo MO Tot'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CusFrtMORet'
              Title.Caption = 'Frete retorno'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = 'Valor total'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / Hora'
              Visible = True
            end>
        end
      end
      object TsDst: TTabSheet
        Caption = 'Produtos Prontos (Destino)'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnDst: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 179
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DGDadosDst: TDBGrid
            Left = 0
            Top = 0
            Width = 644
            Height = 179
            Align = alClient
            DataSource = DsTXIndDst
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Tabela'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_LOC_CEN'
                Title.Caption = 'Local e centro de estoque '
                Width = 137
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Hist'#243'rico'
                Width = 145
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigos destino da opera'#231#227'o'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observ'
                Title.Caption = 'Observa'#231#245'es'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMP'
                Title.Caption = 'Valor MP'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CustoMOTot'
                Title.Caption = 'Custo MO Tot'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtMORet'
                Title.Caption = 'Frete retorno'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor total'
                Width = 68
                Visible = True
              end>
          end
          object DBGrid1: TDBGrid
            Left = 644
            Top = 0
            Width = 356
            Height = 179
            Align = alRight
            DataSource = DsTXIndBxa
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 313
        Height = 32
        Caption = 'Artigo em Industrializa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 313
        Height = 32
        Caption = 'Artigo em Industrializa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 313
        Height = 32
        Caption = 'Artigo em Industrializa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrTXIndCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXIndCabBeforeOpen
    AfterOpen = QrTXIndCabAfterOpen
    BeforeClose = QrTXIndCabBeforeClose
    AfterScroll = QrTXIndCabAfterScroll
    OnCalcFields = QrTXIndCabCalcFields
    SQL.Strings = (
      'SELECT "" NO_TXCOPCab, '
      'IF(QtdeMan<>0, QtdeMan, -QtdeSrc) QtdeINI,'
      'flu.Nome NO_FLUXOCAB, voc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM txindcab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa'
      'LEFT JOIN fluxocab flu ON flu.Codigo=voc.FluxoCab'
      'WHERE voc.Codigo > 0'
      'AND voc.Codigo=:P0')
    Left = 20
    Top = 365
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXIndCabNO_TXCOPCab: TWideStringField
      FieldName = 'NO_TXCOPCab'
      Required = True
      Size = 0
    end
    object QrTXIndCabQtdeINI: TFloatField
      FieldName = 'QtdeINI'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXIndCabNO_FLUXOCAB: TWideStringField
      FieldName = 'NO_FLUXOCAB'
      Size = 100
    end
    object QrTXIndCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXIndCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXIndCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrTXIndCabFluxoCab: TIntegerField
      FieldName = 'FluxoCab'
    end
    object QrTXIndCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTXIndCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXIndCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrTXIndCabClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrTXIndCabFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrTXIndCabStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrTXIndCabPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrTXIndCabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
    end
    object QrTXIndCabDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
    end
    object QrTXIndCabDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
    end
    object QrTXIndCabDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
    end
    object QrTXIndCabQtdeMan: TFloatField
      FieldName = 'QtdeMan'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXIndCabValorTMan: TFloatField
      FieldName = 'ValorTMan'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabQtdeSrc: TFloatField
      FieldName = 'QtdeSrc'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXIndCabValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabQtdeDst: TFloatField
      FieldName = 'QtdeDst'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXIndCabValorTDst: TFloatField
      FieldName = 'ValorTDst'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabQtdeInn: TFloatField
      FieldName = 'QtdeInn'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXIndCabValorTInn: TFloatField
      FieldName = 'ValorTInn'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabQtdeBxa: TFloatField
      FieldName = 'QtdeBxa'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXIndCabValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabQtdeSdo: TFloatField
      FieldName = 'QtdeSdo'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrTXIndCabValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabCustoManMOUni: TFloatField
      FieldName = 'CustoManMOUni'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabValorManMP: TFloatField
      FieldName = 'ValorManMP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabValorManT: TFloatField
      FieldName = 'ValorManT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXIndCabSerieRem: TIntegerField
      FieldName = 'SerieRem'
    end
    object QrTXIndCabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrTXIndCabLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrTXIndCabTemIMEIMrt: TSmallintField
      FieldName = 'TemIMEIMrt'
    end
    object QrTXIndCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrTXIndCabTXCOPCab: TIntegerField
      FieldName = 'TXCOPCab'
    end
    object QrTXIndCabTXVmcWrn: TSmallintField
      FieldName = 'TXVmcWrn'
    end
    object QrTXIndCabTXVmcObs: TWideStringField
      FieldName = 'TXVmcObs'
      Size = 60
    end
    object QrTXIndCabTXVmcSeq: TWideStringField
      FieldName = 'TXVmcSeq'
      Size = 25
    end
    object QrTXIndCabTXVmcSta: TSmallintField
      FieldName = 'TXVmcSta'
    end
    object QrTXIndCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXIndCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXIndCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXIndCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXIndCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXIndCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXIndCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXIndCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXIndCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXIndCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXIndCabNO_DtHrLibOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibOpe'
      Size = 19
      Calculated = True
    end
    object QrTXIndCabNO_DtHrFimOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimOpe'
      Size = 19
      Calculated = True
    end
    object QrTXIndCabNO_DtHrCfgOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgOpe'
      Size = 19
      Calculated = True
    end
    object QrTXIndCabCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTXIndCab: TDataSource
    DataSet = QrTXIndCab
    Left = 20
    Top = 413
  end
  object QrTXIndInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 88
    Top = 366
    object QrTXIndInnCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXIndInnControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXIndInnMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXIndInnMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXIndInnMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXIndInnEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXIndInnTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXIndInnCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXIndInnMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXIndInnDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXIndInnPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXIndInnGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXIndInnQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndInnValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXIndInnSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXIndInnSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXIndInnSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXIndInnSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndInnObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXIndInnSerieTal: TLargeintField
      FieldName = 'SerieTal'
    end
    object QrTXIndInnTalao: TLargeintField
      FieldName = 'Talao'
    end
    object QrTXIndInnFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXIndInnCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXIndInnDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXIndInnDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXIndInnDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXIndInnQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXIndInnQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXIndInnNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXIndInnNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXIndInnNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXIndInnID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXIndInnNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXIndInnReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXIndInnCUSTO_M2: TFloatField
      FieldName = 'CUSTO_UNI'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrTXIndInnNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrTXIndInnMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXIndInnPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrTXIndInnStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXIndInnClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrTXIndInnNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrTXIndInnCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrTXIndInnDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXIndInnPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXIndInnCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXIndInnCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsTXIndInn: TDataSource
    DataSet = QrTXIndInn
    Left = 88
    Top = 413
  end
  object PMOri: TPopupMenu
    OnPopup = PMOriPopup
    Left = 632
    Top = 516
    object Adicionaartigodeorigem1: TMenuItem
      Caption = '&Adiciona artigo de origem'
      OnClick = Adicionaartigodeorigem1Click
    end
    object ItsExcluiOriIMEI: TMenuItem
      Caption = '&Remove IMEI de origem'
      Enabled = False
      OnClick = ItsExcluiOriIMEIClick
    end
    object ItsExcluiOriPallet: TMenuItem
      Caption = '&Remove Pallet de origem'
      Enabled = False
      Visible = False
      OnClick = ItsExcluiOriPalletClick
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object ItsIncluiOri: TMenuItem
      Caption = '&Adiciona artigo de origem'
      Enabled = False
      object porPallet1: TMenuItem
        Caption = 'por Pallet &Total'
        OnClick = porPallet1Click
      end
      object porPalletparcial1: TMenuItem
        Caption = 'por Pallet &Parcial'
        OnClick = porPalletparcial1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object porIMEI1: TMenuItem
        Caption = 'por &IME-I Total'
        OnClick = porIMEI1Click
      end
      object porIMEIParcial1: TMenuItem
        Caption = 'por IM&E-I Parcial'
        OnClick = porIMEIParcial1Click
      end
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME1Click
    end
    object InformaPginaelinhadeIEC1: TMenuItem
      Caption = 'Informa P'#225'gina e linha de IEC'
      OnClick = InformaPginaelinhadeIEC1Click
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 632
    Top = 468
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CabLibera1: TMenuItem
      Caption = '&Libera classifica'#231#227'o'
    end
    object CabReeditar1: TMenuItem
      Caption = '&Volta a editar'
      Enabled = False
      OnClick = CabReeditar1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Definiodequantidadesmanualmente1: TMenuItem
      Caption = '&Defini'#231#227'o de quantidades manualmente'
      OnClick = Definiodequantidadesmanualmente1Click
    end
    object Corrigirfornecedor1: TMenuItem
      Caption = 'Corrigir &Fornecedor'
      OnClick = Corrigirfornecedor1Click
    end
    object Desfazencerramento1: TMenuItem
      Caption = 'Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object este1: TMenuItem
      Caption = 'Teste'
      OnClick = este1Click
    end
    object ransfenciadecourosorigementreOPs1: TMenuItem
      Caption = 'Transfer'#234'ncia de couros origem entre OPs'
      object TransfereTodaOPparaOutra1: TMenuItem
        Caption = 'Transfere toda OP para outra'
        OnClick = TransfereTodaOPparaOutra1Click
      end
      object ReceberDadosDaOPdeOrigem1: TMenuItem
        Caption = 'Receber dados da OP de origem'
        OnClick = ReceberDadosDaOPdeOrigem1Click
      end
    end
  end
  object QrTXIndOri: TmySQLQuery
    Database = Dmod.MyDB
    Left = 168
    Top = 365
    object QrTXIndOriCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXIndOriControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXIndOriMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXIndOriMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXIndOriMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXIndOriEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXIndOriTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXIndOriCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXIndOriMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXIndOriDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXIndOriPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXIndOriGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXIndOriQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndOriValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndOriSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXIndOriSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXIndOriSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXIndOriSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXIndOriSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndOriObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXIndOriFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXIndOriCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndOriCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndOriValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndOriDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXIndOriDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXIndOriDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXIndOriDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXIndOriQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndOriQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndOriNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXIndOriNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXIndOriNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXIndOriID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXIndOriNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrTXIndOriReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXIndOriTXMulFrnCab: TLargeintField
      FieldName = 'TXMulFrnCab'
    end
    object QrTXIndOriDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXIndOriIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrTXIndOriIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrTXIndOriIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
  end
  object DsTXIndOri: TDataSource
    DataSet = QrTXIndOri
    Left = 168
    Top = 413
  end
  object QrPrestador: TmySQLQuery
    Database = Dmod.MyDB
    Left = 84
    Top = 556
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 84
    Top = 600
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &OO (Ordem de Opera'#231#227'o)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object IMEIArtigodeRibeiragerado1: TMenuItem
      Caption = '&IME-I  Ordem de Opera'#231#227'o'
      object Somenteinfo1: TMenuItem
        Caption = '&Somente info'
        OnClick = Somenteinfo1Click
      end
      object Apreencher1: TMenuItem
        Caption = '&A preencher'
        OnClick = Apreencher1Click
      end
      object Comorigemedestino1: TMenuItem
        Caption = '&Com origem e destino'
        OnClick = Comorigemedestino1Click
      end
    end
    object Ordensdeoperaoemaberto1: TMenuItem
      Caption = 'Ordens de opera'#231#227'o em &aberto'
      OnClick = Ordensdeoperaoemaberto1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
  end
  object DsTXIndDst: TDataSource
    DataSet = QrTXIndDst
    Left = 344
    Top = 413
  end
  object PMDst: TPopupMenu
    OnPopup = PMDstPopup
    Left = 632
    Top = 564
    object ItsIncluiDst: TMenuItem
      Caption = '&Inclui artigo de destino'
      OnClick = ItsIncluiDstClick
    end
    object ItsExcluiDst: TMenuItem
      Caption = '&Remove artigo de destino'
      OnClick = ItsExcluiDstClick
    end
    object Incluiraspa1: TMenuItem
      Caption = 'Inclui artigo de destino (Ra&spa)'
      Visible = False
      OnClick = Incluiraspa1Click
    end
    object Alteraartigodedestino1: TMenuItem
      Caption = '&Altera artigo de destino'
      OnClick = Alteraartigodedestino1Click
    end
    object AtrelamentoNFsdeMO2: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento2: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento2Click
      end
      object AlteraAtrelamento2: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento2Click
      end
      object ExcluiAtrelamento2: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento2Click
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object IrparajaneladoPallet1: TMenuItem
      Caption = 'Ir para &Janela do Pallet'
      OnClick = IrparajaneladoPallet1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object InformaNmerodaRME2: TMenuItem
      Caption = 'Informa &N'#250'mero da RME'
      OnClick = InformaNmerodaRME2Click
    end
    object AlteraPallet1: TMenuItem
      Caption = 'Altera &Pallet'
      OnClick = AlteraPallet1Click
    end
    object AlteraLocaldoestoque1: TMenuItem
      Caption = 'Altera &Local do estoque'
      OnClick = AlteraLocaldoestoque1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Corrigeartigodedestino1: TMenuItem
      Caption = 'Corrige artigo de destino'
      object Oitemselecionado1: TMenuItem
        Caption = 'O item selecionado'
        OnClick = Oitemselecionado1Click
      end
      object odosapartirdoselecionado1: TMenuItem
        Caption = 'Todos a partir do selecionado'
        OnClick = odosapartirdoselecionado1Click
      end
    end
  end
  object QrTwn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 708
    Top = 424
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrTXIndBxa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 416
    Top = 365
    object QrTXIndBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXIndBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXIndBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXIndBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXIndBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXIndBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXIndBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXIndBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXIndBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXIndBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXIndBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXIndBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXIndBxaQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXIndBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXIndBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXIndBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXIndBxaSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXIndBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXIndBxaCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXIndBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXIndBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXIndBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXIndBxaQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndBxaQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXIndBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXIndBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXIndBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXIndBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXIndBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsTXIndBxa: TDataSource
    DataSet = QrTXIndBxa
    Left = 416
    Top = 413
  end
  object QrForcados: TmySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 56
    object QrForcadosCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForcadosControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrForcadosMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrForcadosMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrForcadosMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrForcadosEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrForcadosTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrForcadosCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrForcadosMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrForcadosDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrForcadosPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrForcadosGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrForcadosQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrForcadosSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrForcadosSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrForcadosSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrForcadosSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrForcadosFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrForcadosCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrForcadosDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrForcadosDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrForcadosDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrForcadosQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrForcadosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrForcadosNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrForcadosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrForcadosDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsForcados: TDataSource
    DataSet = QrForcados
    Left = 272
    Top = 104
  end
  object QrTXPedIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.TXWetEnd=0')
    Left = 708
    Top = 468
    object QrTXPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrTXPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsTXPedIts: TDataSource
    DataSet = QrTXPedIts
    Left = 708
    Top = 512
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 156
    Top = 556
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 156
    Top = 600
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 56
    object CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem
      Caption = '&Corrige Fornecedores destino a partir desta opera'#231#227'o'
      OnClick = CorrigeFornecedoresdestinoapartirdestaoperao1Click
    end
  end
  object QrTXIndDst: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXIndDstBeforeClose
    AfterScroll = QrTXIndDstAfterScroll
    Left = 344
    Top = 365
    object QrTXIndDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXIndDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXIndDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXIndDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXIndDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXIndDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXIndDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXIndDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXIndDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXIndDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXIndDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXIndDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXIndDstQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXIndDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXIndDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXIndDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXIndDstSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXIndDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXIndDstCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXIndDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXIndDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXIndDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXIndDstQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndDstQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXIndDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXIndDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXIndDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXIndDstNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrTXIndDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXIndDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXIndDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXIndDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXIndDstNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrTXIndDstDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXIndDstCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndDstSerieTal: TLargeintField
      FieldName = 'SerieTal'
    end
    object QrTXIndDstTalao: TLargeintField
      FieldName = 'Talao'
    end
  end
  object QrClientMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 488
    Top = 56
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 488
    Top = 104
  end
  object QrOperacoes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 324
    Top = 188
    object QrOperacoesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOperacoesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsOperacoes: TDataSource
    DataSet = QrOperacoes
    Left = 324
    Top = 240
  end
  object QrTXCOPCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 292
    Top = 556
    object QrTXCOPCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXCOPCabNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXCOPCab: TDataSource
    DataSet = QrTXCOPCab
    Left = 292
    Top = 600
  end
  object QrTXMOEnvEnv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXMOEnvEnvBeforeClose
    AfterScroll = QrTXMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.TXTMI_Controle=:P0')
    Left = 600
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrTXMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrTXMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrTXMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrTXMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrTXMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrTXMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrTXMOEnvEnvNFEMP_Qtde: TFloatField
      FieldName = 'NFEMP_Qtde'
    end
    object QrTXMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrTXMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrTXMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrTXMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrTXMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrTXMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrTXMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrTXMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrTXMOEnvEnvCFTMP_Qtde: TFloatField
      FieldName = 'CFTMP_Qtde'
    end
    object QrTXMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrTXMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrTXMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvEnvTXTMI_MovimCod: TIntegerField
      FieldName = 'TXTMI_MovimCod'
    end
    object QrTXMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTXMOEnvEnv: TDataSource
    DataSet = QrTXMOEnvEnv
    Left = 600
    Top = 56
  end
  object QrTXMOEnvETMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvetmi'
      'WHERE TXMOEnvEnv=:p0')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvETMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvETMITXMOEnvEnv: TIntegerField
      FieldName = 'TXMOEnvEnv'
    end
    object QrTXMOEnvETMITXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrTXMOEnvETMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvETMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvETMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvETMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvETMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvETMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvETMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvETMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvETMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvETMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXMOEnvETMIQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0'
    end
  end
  object DsTXMOEnvETMI: TDataSource
    DataSet = QrTXMOEnvETMI
    Left = 684
    Top = 56
  end
  object QrTXMOEnvRet: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXMOEnvRetBeforeClose
    AfterScroll = QrTXMOEnvRetAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvret cmo'
      'WHERE cmo.TXTMI_Controle=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrTXMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrTXMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrTXMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrTXMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrTXMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrTXMOEnvRetNFCMO_Qtde: TFloatField
      FieldName = 'NFCMO_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object QrTXMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrTXMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrTXMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrTXMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrTXMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrTXMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrTXMOEnvRetNFRMP_Qtde: TFloatField
      FieldName = 'NFRMP_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object QrTXMOEnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object QrTXMOEnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object QrTXMOEnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object QrTXMOEnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object QrTXMOEnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object QrTXMOEnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object QrTXMOEnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object QrTXMOEnvRetCFTPA_Qtde: TFloatField
      FieldName = 'CFTPA_Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTXMOEnvRet: TDataSource
    DataSet = QrTXMOEnvRet
    Left = 768
    Top = 56
  end
  object QrTXMOEnvRTmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, mev.* '
      'FROM vsmoenvrtmi mev'
      'LEFT JOIN vsmoenvenv vee ON vee.Codigo=mev.TXMOEnvEnv')
    Left = 852
    Top = 8
    object QrTXMOEnvRTmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvRTmiTXMOEnvRet: TIntegerField
      FieldName = 'TXMOEnvRet'
    end
    object QrTXMOEnvRTmiTXMOEnvEnv: TIntegerField
      FieldName = 'TXMOEnvEnv'
    end
    object QrTXMOEnvRTmiQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvRTmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvRTmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvRTmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvRTmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvRTmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvRTmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvRTmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvRTmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvRTmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvRTmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvRTmiNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrTXMOEnvRTmiNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrTXMOEnvRTmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTXMOEnvRTmi: TDataSource
    DataSet = QrTXMOEnvRTmi
    Left = 852
    Top = 56
  end
  object QrTXMOEnvGTmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmoenvgtmi')
    Left = 940
    Top = 8
    object QrTXMOEnvGTmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMOEnvGTmiTXMOEnvRet: TIntegerField
      FieldName = 'TXMOEnvRet'
    end
    object QrTXMOEnvGTmiTXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrTXMOEnvGTmiQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrTXMOEnvGTmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTXMOEnvGTmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMOEnvGTmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMOEnvGTmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMOEnvGTmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMOEnvGTmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMOEnvGTmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMOEnvGTmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrTXMOEnvGTmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrTXMOEnvGTmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMOEnvGTmiTXMovimCod: TIntegerField
      FieldName = 'TXMovimCod'
    end
  end
  object DsTXMOEnvGTmi: TDataSource
    DataSet = QrTXMOEnvGTmi
    Left = 940
    Top = 56
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 24
    Top = 556
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 24
    Top = 600
  end
  object QrFluxoCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM fluxocab'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 224
    Top = 556
    object QrFluxoCabCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrFluxoCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsFluxoCab: TDataSource
    DataSet = QrFluxoCab
    Left = 224
    Top = 600
  end
  object PMInn: TPopupMenu
    OnPopup = PMInnPopup
    Left = 632
    Top = 612
    object ItsIncluiInn: TMenuItem
      Caption = '&Adiciona artigo em processo'
      Enabled = False
      OnClick = ItsIncluiInnClick
    end
    object ItsAlteraInn: TMenuItem
      Caption = '&Edita artigo em processo'
      Enabled = False
      OnClick = ItsAlteraInnClick
    end
    object ItsExcluiInn: TMenuItem
      Caption = '&Remove artigo em processo'
      Enabled = False
      OnClick = ItsExcluiInnClick
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object AtualizaestoqueEmindustrializao1: TMenuItem
      Caption = 'Atualiza estoque "Em industrializa'#231#227'o"'
      OnClick = AtualizaestoqueEmindustrializao1Click
    end
    object AlteraLocaldoestoque2: TMenuItem
      Caption = 'Altera Local do esto&que'
      OnClick = AlteraLocaldoestoque2Click
    end
  end
  object DqSum: TmySQLDirectQuery
    Database = Dmod.ZZDB
    Left = 360
    Top = 540
  end
end
