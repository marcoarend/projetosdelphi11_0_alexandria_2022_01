unit TXBxaIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Grids, Vcl.DBGrids, UnAppEnums, UnProjGroup_Consts;

type
  TFmTXBxaIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrTXPallet: TmySQLQuery;
    DsTXPallet: TDataSource;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletNome: TWideStringField;
    BtExclui: TBitBtn;
    QrGraGruXGraGruY: TIntegerField;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    LaQtde: TLabel;
    Label4: TLabel;
    SBPallet: TSpeedButton;
    Label9: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdQtde: TdmkEdit;
    EdPsqPallet: TdmkEditCB;
    CBPsqPallet: TdmkDBLookupComboBox;
    EdObserv: TdmkEdit;
    EdValorT: TdmkEdit;
    Label14: TLabel;
    EdSrcMovID: TdmkEdit;
    Label10: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    QrTXMovIts: TmySQLQuery;
    QrTXMovItsNO_Pallet: TWideStringField;
    QrTXMovItsCodigo: TIntegerField;
    QrTXMovItsControle: TIntegerField;
    QrTXMovItsMovimCod: TIntegerField;
    QrTXMovItsMovimNiv: TIntegerField;
    QrTXMovItsEmpresa: TIntegerField;
    QrTXMovItsTerceiro: TIntegerField;
    QrTXMovItsMovimID: TIntegerField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TIntegerField;
    QrTXMovItsGraGruX: TIntegerField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsSrcMovID: TIntegerField;
    QrTXMovItsSrcNivel1: TIntegerField;
    QrTXMovItsSrcNivel2: TIntegerField;
    QrTXMovItsLk: TIntegerField;
    QrTXMovItsDataCad: TDateField;
    QrTXMovItsDataAlt: TDateField;
    QrTXMovItsUserCad: TIntegerField;
    QrTXMovItsUserAlt: TIntegerField;
    QrTXMovItsAlterWeb: TSmallintField;
    QrTXMovItsAtivo: TSmallintField;
    QrTXMovItsNO_FORNECE: TWideStringField;
    QrTXMovItsValorT: TFloatField;
    DsTXMovIts: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    EdSrcGGX: TdmkEdit;
    Label13: TLabel;
    QrTXMovItsMovimTwn: TIntegerField;
    QrTXMovItsCliVenda: TIntegerField;
    QrTXMovItsSrcGGX: TIntegerField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsFornecMO: TIntegerField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsCustoMOTot: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TIntegerField;
    QrTXMovItsDstNivel1: TIntegerField;
    QrTXMovItsDstNivel2: TIntegerField;
    QrTXMovItsDstGGX: TIntegerField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsAptoUso: TSmallintField;
    QrTXMovItsTpCalcAuto: TIntegerField;
    QrTXMovItsTXMulFrnCab: TIntegerField;
    QrTXMovItsClientMO: TIntegerField;
    QrTXMovItsStqCenLoc: TIntegerField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsSerieTal: TIntegerField;
    QrTXMovItsTalao: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBPalletClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdPsqPalletRedefinido(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdQtdeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FUltGGX: Integer;

    //
    procedure CopiaTotaisIMEI();
    procedure ReopenTXBxaIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure HabilitaInclusao();
    procedure ReopenTXMovIts();
    procedure ReopenTXPallet();

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FDataHora: TDateTime;
    FEmpresa, FAntSrcNivel2: Integer;
    //FDsCab: TDataSource;
  end;

  var
  FmTXBxaIts: TFmTXBxaIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal, ModuleGeral, UnTX_PF, TXBxaCab, AppListas, UnTX_Jan;

{$R *.DFM}

procedure TFmTXBxaIts.BtExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := EdControle.ValueVariant;
  if Geral.MB_Pergunta('Confirme a exclus�o do IME-I' +
  Geral.FF0(Controle) + '?') = ID_YES then
  begin
    TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(Controle,
    Integer(TEstqMotivDel.emtdWetCurti028), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
    TX_PF.AtualizaSaldoIMEI(Controle, False);
    //
    Close;
  end;
end;

procedure TFmTXBxaIts.BtOKClick(Sender: TObject);
const
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOUni = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGer     = 0;
  AptoUso    = 0;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  ItemNFe    = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, SrcNivel1,
  SrcNivel2, GraGruY, SrcGGX, TXMulFrnCab, ClientMO, FornecMO, StqCenLoc,
  SerieTal, Talao: Integer;
  Qtde, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  SrcMovID       := EdSrcMovID.ValueVariant;
  SrcNivel1      := EdSrcNivel1.ValueVariant;
  SrcNivel2      := EdSrcNivel2.ValueVariant;
  SrcGGX         := EdSrcGGX.ValueVariant;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClientMO       := QrTXMovItsClientMO.Value;
  FornecMO       := QrTXMovItsFornecMO.Value;
  if FornecMO = 0 then
    FornecMO := QrTXMovItsEmpresa.Value;
  StqCenLoc      := QrTXMovItsStqCenLoc.Value;
  Terceiro       := QrTXMovItsTerceiro.Value;
  TXMulFrnCab    := QrTXMovItsTXMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidForcado;
  MovimNiv       := eminSemNiv;
  Pallet         := QrTXMovItsPallet.Value;
  GraGruX        := QrTXMovItsGraGruX.Value;
  FUltGGX        := GraGruX;
  Qtde           := -EdQtde.ValueVariant;
  ValorT         := -EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  Marca          := QrTXMovItsMarca.Value;
  GraGruY        := QrGraGruXGraGruY.Value;
  //
  SerieTal       := QrTXMovItsSerieTal.Value;
  Talao          := QrTXMovItsTalao.Value;
  //
  //
  if TX_PF.TXFic(GraGruX, Empresa, Terceiro, Pallet, Qtde, ValorT, EdGraGruX,
  EdPallet, EdQtde, EdValorT, ExigeFornecedor, GraGruY, nil, 0, 0, nil, nil) then
    Exit;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGer, AptoUso, FornecMO,
  SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei057(*Baixa for�ada 1/2*)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txbxacab', MovimCod);
    TX_PF.AtualizaSaldoIMEI(FAntSrcNivel2, False);
    TX_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FmTXBxaCab.LocCod(Codigo, Codigo);
    ReopenTXBxaIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdQtde.ValueVariant       := 0;
      EdValorT.ValueVariant      := 0;
      EdPsqPallet.ValueVariant   := 0;
      CBPsqPallet.KeyValue       := 0;
      EdObserv.Text              := '';
      EdSrcMovID.ValueVariant    := 0;
      EdSrcNivel1.ValueVariant   := 0;
      EdSrcNivel2.ValueVariant   := 0;
      EdSrcGGX.ValueVariant      := 0;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTXBxaIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXBxaIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXBxaIts.CopiaTotaisIMEI;
var
  Valor: Double;
begin
  EdQtde.ValueVariant  := QrTXMovItsSdoVrtQtd.Value;
  //
  if QrTXMovItsQtde.Value > 0 then
    Valor := QrTXMovItsSdoVrtQtd.Value *
    (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value)
  else
    Valor := 0;
  //
  EdValorT.ValueVariant := Valor;
end;

procedure TFmTXBxaIts.DBGrid1DblClick(Sender: TObject);
var
  Valor: Double;
begin
  EdSrcMovID.ValueVariant  := QrTXMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrTXMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrTXMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrTXMovItsGraGruX.Value;
  //
  try
    EdQtde.SetFocus;
  except
    //
  end;
end;

procedure TFmTXBxaIts.EdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
end;

procedure TFmTXBxaIts.EdEmpresaRedefinido(Sender: TObject);
begin
  ReopenTXPallet();
end;

procedure TFmTXBxaIts.EdFornecedorChange(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXBxaIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXBxaIts.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenTXPallet();
  ReopenTXMovIts();
end;

procedure TFmTXBxaIts.EdPsqPalletRedefinido(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXBxaIts.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXBxaIts.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXBxaIts.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXBxaIts.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Qtde, Valor: Double;
begin
  if Key = VK_F5 then
  begin
    Qtde  := EdQtde.ValueVariant;
    Valor := 0;
    if QrTXMovItsQtde.Value > 0 then
          Valor := Qtde * (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value);
    EdValorT.ValueVariant := Valor;
  end;
end;

procedure TFmTXBxaIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXBxaIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' + CO_GraGruY_ALL_TX + ') ');
end;

procedure TFmTXBxaIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXBxaIts.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    //(EdSrcMovID.ValueVariant <> 0) and  MovimID = 13 >> Inventario
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmTXBxaIts.ImgTipoChange(Sender: TObject);
begin
  BtExclui.Visible := ImgTipo.SQLType = stUpd;
end;

procedure TFmTXBxaIts.ReopenTXMovIts();
var
  GraGruX, Empresa, Pallet: Integer;
  SQL_Pallet: String;
begin
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := FEmpresa;
  if (GragruX = 0) or (Empresa = 0) then
  begin
    QrTXMovIts.Close;
    Exit;
  end;
  SQL_Pallet := '';
  Pallet     := EdPsqPallet.ValueVariant;
  if Pallet <> 0 then
    SQL_Pallet := 'AND tmi.Pallet=' + Geral.FF0(Pallet);
  //
  Empresa := FEmpresa;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, tmi.*,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'LEFT JOIN txpalleta pal ON pal.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=tmi.Terceiro',
  'WHERE tmi.Empresa=' + Geral.FF0(Empresa),
  'AND tmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND (tmi.SdoVrtQtd > 0 ',
  // Precisa? Nao!!!! tem saldos negativos!!!!
  //'OR tmi.SdoVrtArM2 > 0 ',
  ') ',
  SQL_Pallet,
  'ORDER BY DataHora, Pallet',
  '']);
  //
end;

procedure TFmTXBxaIts.ReopenTXBxaIts(Controle: Integer);
begin
  if (FQrIts <> nil)
  and (FQrCab <> nil) then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXBxaIts.ReopenTXPallet();
var
  Empresa, GraGruX: Integer;
begin
  Empresa    := FEmpresa;
  GraGruX := EdGraGruX.ValueVariant;
  //
  if (Empresa = 0) or (GraGruX = 0) then
  begin
    QrTXPallet.Close;
    Exit;
  end;
  //
  Empresa := FEmpresa;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPallet, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _028_PALTS_; ',
  'CREATE TABLE _028_PALTS_ ',
  ' ',
  '  SELECT DISTINCT tmi.Pallet   ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_TMI + ' tmi  ',
  '  WHERE tmi.Qtde >=0  ',
  '  AND tmi.SdoVrtQtd > 0  ',
  '  AND tmi.Empresa=' + Geral.FF0(Empresa),
  '  AND tmi.GraGruX=' + Geral.FF0(GraGruX),
  '; ',
  ' ',
  'SELECT pal.Codigo, pal.Nome   ',
  'FROM _028_PALTS_ _28 ',
  'LEFT JOIN ' + TMeuDB + '.txpalleta pal ON _28.Pallet=pal.Codigo ',
  'ORDER BY Nome  ',
  //
  ' ']);
end;

procedure TFmTXBxaIts.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPsqPallet, CBPsqPallet, QrTXPallet, VAR_CADASTRO);
end;

procedure TFmTXBxaIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
