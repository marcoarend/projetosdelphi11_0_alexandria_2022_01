unit TXRRMOriIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, UnAppEnums;

type
  TFmTXRRMOriIMEI = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    LaFicha: TLabel;
    EdFicha: TdmkEdit;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    Label6: TLabel;
    LaQtde: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdQtde: TdmkEdit;
    EdObserv: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    Label11: TLabel;
    EdSerieFch: TdmkEditCB;
    CBSerieFch: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstoque: TmySQLQuery;
    QrEstoqueControle: TIntegerField;
    QrEstoqueEmpresa: TIntegerField;
    QrEstoqueGraGruX: TIntegerField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorT: TFloatField;
    QrEstoqueGraGru1: TIntegerField;
    QrEstoqueNO_PRD_TAM_COR: TWideStringField;
    QrEstoquePallet: TIntegerField;
    QrEstoqueNO_Pallet: TWideStringField;
    QrEstoqueTerceiro: TIntegerField;
    QrEstoqueNO_FORNECE: TWideStringField;
    QrEstoqueNO_EMPRESA: TWideStringField;
    QrEstoqueNO_STATUS: TWideStringField;
    QrEstoqueDataHora: TWideStringField;
    QrEstoqueOrdGGX: TLargeintField;
    QrEstoqueOrdem: TIntegerField;
    QrEstoqueCodiGGY: TIntegerField;
    QrEstoqueNome: TWideStringField;
    QrEstoqueCodigo: TIntegerField;
    QrEstoqueIMEC: TIntegerField;
    QrEstoqueIMEI: TIntegerField;
    QrEstoqueMovimID: TIntegerField;
    QrEstoqueMovimNiv: TIntegerField;
    QrEstoqueNO_MovimID: TWideStringField;
    QrEstoqueNO_MovimNiv: TWideStringField;
    QrEstoqueAtivo: TLargeintField;
    DsEstque: TDataSource;
    QrEstoqueGraGruY: TIntegerField;
    Panel6: TPanel;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    QrEstoqueTXMulFrnCab: TIntegerField;
    QrEstoqueClientMO: TIntegerField;
    QrEstoqueSdoVrtQtd: TFloatField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Panel3: TPanel;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    QrEstoqueNFeSer: TSmallintField;
    QrEstoqueNFeNum: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure EdFichaChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdSerieFchChange(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure EdQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGGXRclKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FTXMovImp4(*, FTXMovImp5*): String;
    //
    procedure InsereIMEI_Atual(InsereTudo: Boolean; ParcQtde: Double);
    procedure FechaPesquisa();
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure LiberaEdicao(Libera, Avisa: Boolean);
    function  BaixaIMEIParcial(): Integer;
    function  BaixaIMEITotal(): Integer;
  public
    { Public declarations }
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FStqCenLoc, FFornecMO: Integer;
    FDataHora: TDateTime;
    FParcial: Boolean;
    //
    procedure ReopenItensAptos();
    procedure ReopenGraGruX();
  end;

  var
  FmTXRRMOriIMEI: TFmTXRRMOriIMEI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
TXRRMCab, UnTX_PF, AppListas, UnGrade_PF;

{$R *.DFM}

procedure TFmTXRRMOriIMEI.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

function TFmTXRRMOriIMEI.BaixaIMEIParcial(): Integer;
var
  Qtde, SobraQtde: Double;
begin
  Result := 0;
  Qtde       := EdQtde.ValueVariant;
  SobraQtde  := Qtde;
  //
  if MyObjects.FIC(Qtde <= 0, EdQtde, 'Informe a quantidade') then
    Exit;
  InsereIMEI_Atual(False, SobraQtde);
  Result := 1;
end;

function TFmTXRRMOriIMEI.BaixaIMEITotal(): Integer;
var
  N, I: Integer;
begin
  QrEstoque.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      //AdicionaPallet();
      InsereIMEI_Atual(True, 0);
    end;
  finally
    QrEstoque.EnableControls;
  end;
  Result := N;
end;

procedure TFmTXRRMOriIMEI.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo: Integer;
begin
  if FParcial then
    N := BaixaIMEIParcial()
  else
    N := BaixaIMEITotal();
  //
    // Nao se aplica. Calcula com function propria a seguir.
    //TX_PF.AtualizaTotaisTXXxxCab('TXRRMcab', MovimCod);
    MovimCod := FmTXRRMCab.QrTXRRMCabMovimCod.Value;
    TX_PF.AtualizaTotaisTXRRMCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmTXRRMCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinuar.Checked then
      begin
        EdControle.ValueVariant := 0;
        EdQtde.ValueVariant     := 0;
        EdObserv.ValueVariant   := '';
        ReopenItensAptos();
        MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
        LiberaEdicao(False, False);
      end else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
(*
  // Nao se aplica. Calcula com function propria a seguir.
  //TX_PF.AtualizaTotaisTXXxxCab('TXRRMcab', MovimCod);
  MovimCod := FmTXRRMCab.QrTXRRMCabMovimCod.Value;
  TX_PF.AtualizaTotaisTXRRMCab(MovimCod);
  //
  Codigo := EdCodigo.ValueVariant;
  FmTXRRMCab.LocCod(Codigo, Codigo);
  if N > 0 then
  begin
    if CkContinuar.Checked then
    begin
      ReopenItensAptos();
      MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
      LiberaEdicao(False, False);
    end else
      Close;
  end else
    Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
*)
end;

procedure TFmTXRRMOriIMEI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXRRMOriIMEI.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmTXRRMOriIMEI.EdFichaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriIMEI.EdGGXRclKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdGGXRcl.ValueVariant := QrEstoqueGraGruX.Value;
    CBGGXRcl.KeyValue     := QrEstoqueGraGruX.Value;
  end;
end;

procedure TFmTXRRMOriIMEI.EdGraGruXChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriIMEI.EdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdQtde.ValueVariant  := QrEstoqueSdoVrtQtd.Value;
end;

procedure TFmTXRRMOriIMEI.EdSerieFchChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriIMEI.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXRRMOriIMEI.FechaPesquisa();
begin
  QrEstoque.Close;
end;

procedure TFmTXRRMOriIMEI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    EdReqMovEstq.SetFocus;
  except
    //
  end;
end;

procedure TFmTXRRMOriIMEI.FormCreate(Sender: TObject);
const
  Default = -1;
begin
  ImgTipo.SQLType := stLok;
  //
  FParcial := False;
  TX_PF.AbreTXSerTal(QrTXSerTal);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  //
  //TX_PF.SetIDTipoCouro(RGTipoCouro, CO_RGTIPOCOURO_COLUNAS, Default);
end;

procedure TFmTXRRMOriIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXRRMOriIMEI.InsereIMEI_Atual(InsereTudo: Boolean; ParcQtde:
  Double);
const
  Observ = '';
  MovimTwn   = 0;
  ExigeFornecedor = False;
(*
  EdTalao     = nil;
  EdPallet    = nil;
  EdValorT    = nil;
  EdStqCenLoc = nil;
  EdSerieTal  = nil;
*)
  CustoMOUni  = 0;
  CustoMOTot  = 0;
  QtdGer      = 0;
  QtdAnt      = 0;
  AptoUso     = 0; // Eh venda!
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  ItemNFe    = 0;
  //
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, SerieTal,
  Talao, DstNivel1, DstNivel2, GraGruY, SrcGGX, DstGGX, ReqMovEstq, TXMulFrnCab,
  ClientMO, GGXRcl, StqCenLoc, FornecMO: Integer;
  Qtde, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
begin
  SrcMovID       := QrEstoqueMovimID.Value;
  SrcNivel1      := QrEstoqueCodigo.Value;
  SrcNivel2      := QrEstoqueControle.Value;
  SrcGGX         := QrEstoqueGraGruX.Value;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  Terceiro       := QrEstoqueTerceiro.Value;
  TXMulFrnCab    := QrEstoqueTXMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEmReprRM;
  MovimNiv       := eminSorcRRM;
  Pallet         := QrEstoquePallet.Value;
  GraGruX        := QrEstoqueGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not TX_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  if not InsereTudo then
  begin
    Qtde         := -ParcQtde;
  end else
  begin
    Qtde           := -QrEstoqueQtde.Value;
  end;
  if QrEstoqueQtde.Value > 0 then
    Valor := -Qtde *
    (QrEstoqueValorT.Value / QrEstoqueQtde.Value)
  else
    Valor := 0;
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  SerieTal       := 0; //QrEstoqueSerieTal.Value;
  Talao          := 0;  //QrEstoqueTalao.Value;
  Marca          := ''; //QrEstoqueMarca.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrEstoqueGraGruY.Value;
  //
  StqCenLoc      := FStqCenLoc;
  FornecMO       := FFornecMO;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  ClientMO       := QrEstoqueClientMO.Value;
  //
(*
  if TX_PF.TXFic(GraGruX, Empresa, Terceiro, Pallet, Qtde, ValorT,
  EdGraGruX, EdPallet, EdQtde, EdValorT, ExigeFornecedor, GraGruY, EdStqCenLoc,
  SerieTal, Talao, EdSerieTal, EdTalao) then
    Exit;
*)
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, (*Fornecedor*)Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGer, AptoUso, FornecMO,
  SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei088(*Baixa de material em reprocesso/reparo por IME-I*)) then
  begin
    TX_PF.AtualizaSaldoIMEI(SrcNivel2, True);
  end;
end;

procedure TFmTXRRMOriIMEI.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status: Boolean;
begin
  if FParcial then
  begin
    if Libera then
      Status := QrEstoque.RecordCount > 0
    else
      Status := False;
    //
    GBAptos.Enabled := not Status;
    GBGerar.Visible := Status;
    //
    LaFicha.Enabled := not Status;
    EdFicha.Enabled := not Status;
    LaGraGruX.Enabled := not Status;
    EdGraGruX.Enabled := not Status;
    CBGraGruX.Enabled := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled := not Status;
    //
    if Libera and (EdQtde.Enabled) and (EdQtde.Visible) then
      EdQtde.SetFocus;
  end;
end;

procedure TFmTXRRMOriIMEI.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmTXRRMOriIMEI.ReopenGraGruX;
begin
  TX_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GragruY<>0');
(*
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_TX...) + ',' +
    Geral.FF0(CO_GraGruY_3072_TX...) +
    ')');
*)
  TX_PF.AbreGraGruXY(QrGGXRcl, 'AND ggx.GragruY<>0');
(*
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_2048_TX...) + ',' +
    Geral.FF0(CO_GraGruY_3072_TX...) +
    ')');
*)
end;

procedure TFmTXRRMOriIMEI.ReopenItensAptos();
var
  GraGruX, GraGruY: Integer;
  SQL_GraGruX, SQL_GraGruY: String;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND tmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := 'AND tmi.GraGruX<>0 ';
  //
(*
  if RGTipoCouro.ItemIndex > -1 then
  begin
    GraGruY := TX_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
    if GraGruY <> 0 then
      SQL_GraGruY := 'AND ggx.GraGruY=' + Geral.FF0(GraGruY)
    else
      SQL_GraGruY := '';
  end else
    SQL_GraGruY := 'AND tmi.Pallet = 0 ';
  //
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT tmi.Controle, tmi.Empresa, tmi.ClientMO, ',
  'tmi.GraGruX, tmi.Qtde, tmi.SdoVrtQtd, ValorT, ',
  'ggx.GraGru1, ggx.GraGruY, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, tmi.Pallet, txp.Nome NO_Pallet, ',
  'tmi.Terceiro, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  '"" NO_STATUS, ',
  '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, ',
  'tmi.Codigo, tmi.MovimCod IMEC, tmi.Controle IMEI, ',
  'tmi.MovimID, tmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ',
  'tmi.TXMulFrnCab, tmi.SerieTal, tmi.Talao, tmi.Marca, ',
  'tmi.NFeSer, tmi.NFeNum, ',
  '1 Ativo ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(tmi.Pallet <> 0, txp.GraGruX, tmi.GraGruX) ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  ent ON ent.Codigo=tmi.Terceiro ',
  'LEFT JOIN entidades  emp ON emp.Codigo=tmi.Empresa ',
  'WHERE tmi.Controle <> 0 ',
  SQL_GraGruX,
  'AND tmi.SdoVrtQtd > 0 ',
  SQL_GraGruY,
  'AND tmi.Empresa=' + Geral.FF0(FEmpresa),
  ' ']);
  //Geral.MB_SQL(self, QrEstoque);
end;

{
StqCenLoc indefinido!
MovimID=33 emidEmReprRM
MovimNiv=54 eminSorcRRM
iutpei = 88 : Baixa de material em reprocesso/reparo por IME-I
}

end.
