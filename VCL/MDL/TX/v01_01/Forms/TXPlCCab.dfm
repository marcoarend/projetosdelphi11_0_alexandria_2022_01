object FmTXPlCCab: TFmTXPlCCab
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-076 :: Entrada de Artigo Semi Pronto por Compra'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 177
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object Label15: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object Label24: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object Label25: TLabel
        Left = 508
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label26: TLabel
        Left = 508
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label27: TLabel
        Left = 508
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label9: TLabel
        Left = 908
        Top = 136
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label16: TLabel
        Left = 440
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object Label17: TLabel
        Left = 408
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label33: TLabel
        Left = 900
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label34: TLabel
        Left = 932
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object Label35: TLabel
        Left = 432
        Top = 136
        Width = 34
        Height = 13
        Caption = 'Valor T'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXPlCCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXPlCCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 341
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXPlCCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 476
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtCompra'
        DataSource = DsTXPlCCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 628
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsTXPlCCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 780
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtEntrada'
        DataSource = DsTXPlCCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsTXPlCCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsTXPlCCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 564
        Top = 72
        Width = 333
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsTXPlCCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsTXPlCCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsTXPlCCab
        TabOrder = 10
      end
      object DBEdit13: TDBEdit
        Left = 908
        Top = 152
        Width = 88
        Height = 21
        DataField = 'Qtde'
        DataSource = DsTXPlCCab
        TabOrder = 11
      end
      object DBEdit15: TDBEdit
        Left = 432
        Top = 152
        Width = 72
        Height = 21
        DataField = 'ValorT'
        DataSource = DsTXPlCCab
        TabOrder = 12
      end
      object DBEdit16: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsTXPlCCab
        TabOrder = 13
      end
      object DBEdit17: TDBEdit
        Left = 72
        Top = 72
        Width = 333
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsTXPlCCab
        TabOrder = 14
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'Motorista'
        DataSource = DsTXPlCCab
        TabOrder = 15
      end
      object DBEdit19: TDBEdit
        Left = 72
        Top = 152
        Width = 357
        Height = 21
        DataField = 'NO_MOTORISTA'
        DataSource = DsTXPlCCab
        TabOrder = 16
      end
      object DBEdit20: TDBEdit
        Left = 508
        Top = 152
        Width = 77
        Height = 21
        DataField = 'Placa'
        DataSource = DsTXPlCCab
        TabOrder = 17
      end
      object DBEdit21: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Procednc'
        DataSource = DsTXPlCCab
        TabOrder = 18
      end
      object DBEdit22: TDBEdit
        Left = 72
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_PROCEDNC'
        DataSource = DsTXPlCCab
        TabOrder = 19
      end
      object DBEdit23: TDBEdit
        Left = 408
        Top = 72
        Width = 28
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsTXPlCCab
        TabOrder = 20
      end
      object DBEdit24: TDBEdit
        Left = 440
        Top = 72
        Width = 64
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsTXPlCCab
        TabOrder = 21
      end
      object DBEdit25: TDBEdit
        Left = 900
        Top = 72
        Width = 28
        Height = 21
        DataField = 'emi_serie'
        DataSource = DsTXPlCCab
        TabOrder = 22
      end
      object DBEdit26: TDBEdit
        Left = 932
        Top = 72
        Width = 64
        Height = 21
        DataField = 'emi_nNF'
        DataSource = DsTXPlCCab
        TabOrder = 23
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 4
      Top = 216
      Width = 1008
      Height = 285
      ActivePage = TabSheet1
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Dados da entrada'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GBIts: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 257
            Align = alClient
            Caption = ' Itens da entrada: '
            TabOrder = 0
            object DGDados: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 996
              Height = 240
              Align = alClient
              DataSource = DsTXPlCIts
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              PopupMenu = PMTXPlcIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TTW'
                  Title.Caption = 'Tabela'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SerieTal'
                  Title.Caption = 'ID Serie'
                  Width = 43
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Talao'
                  Title.Caption = 'Tal'#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pallet'
                  Title.Caption = 'ID Pallet'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PALLET'
                  Title.Caption = 'Pallet'
                  Width = 76
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Title.Caption = 'Quantidade'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'IME-I'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 300
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 504
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label5: TLabel
        Left = 504
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object LaQtde: TLabel
        Left = 900
        Top = 136
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object SbFornecedor: TSpeedButton
        Left = 868
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbFornecedorClick
      end
      object SbTransportador: TSpeedButton
        Left = 968
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTransportadorClick
      end
      object Label20: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object SbClienteMO: TSpeedButton
        Left = 380
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteMOClick
      end
      object Label21: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object SbProcedenc: TSpeedButton
        Left = 480
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbProcedencClick
      end
      object Label22: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object SbMotorista: TSpeedButton
        Left = 480
        Top = 152
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMotoristaClick
      end
      object Label23: TLabel
        Left = 504
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label28: TLabel
        Left = 404
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label29: TLabel
        Left = 436
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object Label31: TLabel
        Left = 892
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label32: TLabel
        Left = 924
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtCompra: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtCompra: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrada: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtEntrada: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFornecedor: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornecedor'
        UpdCampo = 'Fornecedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 15
        dmkEditCB = EdFornecedor
        QryCampo = 'Fornecedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 21
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdQtde: TdmkEdit
        Left = 900
        Top = 152
        Width = 88
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 25
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClienteMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteMO'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClienteMO
        TabOrder = 11
        dmkEditCB = EdClienteMO
        QryCampo = 'ClienteMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProcednc: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Procednc'
        UpdCampo = 'Procednc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProcednc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProcednc: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcednc
        TabOrder = 19
        dmkEditCB = EdProcednc
        QryCampo = 'Procednc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdMotorista: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Motorista'
        UpdCampo = 'Motorista'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotorista
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotorista: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsMotorista
        TabOrder = 23
        dmkEditCB = EdMotorista
        QryCampo = 'Motorista'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPlaca: TdmkEdit
        Left = 504
        Top = 152
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 24
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Placa'
        UpdCampo = 'Placa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edide_serie: TdmkEdit
        Left = 404
        Top = 72
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 436
        Top = 72
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_serie: TdmkEdit
        Left = 892
        Top = 72
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_serie'
        UpdCampo = 'emi_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_nNF: TdmkEdit
        Left = 924
        Top = 72
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_nNF'
        UpdCampo = 'emi_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 518
        Height = 32
        Caption = 'Entrada de Artigo Semi Pronto por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 518
        Height = 32
        Caption = 'Entrada de Artigo Semi Pronto por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 518
        Height = 32
        Caption = 'Entrada de Artigo Semi Pronto por Compra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 600
    Top = 44
  end
  object QrTXPlCCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXPlCCabBeforeOpen
    AfterOpen = QrTXPlCCabAfterOpen
    BeforeClose = QrTXPlCCabBeforeClose
    AfterScroll = QrTXPlCCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,'
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,'
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta'
      'LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO'
      'LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc'
      'LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista'
      ''
      'WHERE wic.Codigo > 0')
    Left = 684
    Top = 1
    object QrTXPlCCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPlCCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXPlCCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXPlCCabDtCompra: TDateTimeField
      FieldName = 'DtCompra'
    end
    object QrTXPlCCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrTXPlCCabDtEntrada: TDateTimeField
      FieldName = 'DtEntrada'
    end
    object QrTXPlCCabFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrTXPlCCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrTXPlCCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPlCCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXPlCCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXPlCCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXPlCCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXPlCCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXPlCCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXPlCCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXPlCCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXPlCCabNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXPlCCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrTXPlCCabValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPlCCabClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrTXPlCCabProcednc: TIntegerField
      FieldName = 'Procednc'
    end
    object QrTXPlCCabMotorista: TIntegerField
      FieldName = 'Motorista'
    end
    object QrTXPlCCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrTXPlCCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrTXPlCCabNO_PROCEDNC: TWideStringField
      FieldName = 'NO_PROCEDNC'
      Size = 100
    end
    object QrTXPlCCabNO_MOTORISTA: TWideStringField
      FieldName = 'NO_MOTORISTA'
      Size = 100
    end
    object QrTXPlCCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrTXPlCCabide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrTXPlCCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrTXPlCCabemi_serie: TIntegerField
      FieldName = 'emi_serie'
    end
    object QrTXPlCCabemi_nNF: TIntegerField
      FieldName = 'emi_nNF'
    end
    object QrTXPlCCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
  end
  object DsTXPlCCab: TDataSource
    DataSet = QrTXPlCCab
    Left = 688
    Top = 45
  end
  object QrTXPlCIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,'
      'IF(wmi.SdoVrtPeca > 0, 0,'
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),'
      '  ",", ""), ".", ",")) RendKgm2_TXT,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE('
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,'
      'IF(wmi.Misturou = 1, "SIM", "N'#195'O") Misturou_TXT,'
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(wmi.Pecas > 0, wmi.QtdGerArM2 / wmi.Pecas, 0), 3),'
      '  ",", ""), ".", ",")) m2_CouroTXT,'
      'IF(wmi.Pecas <> 0, wmi.PesoKg / wmi.Pecas, 0) KgMedioCouro'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE wmi.MovimCod=0'
      'ORDER BY NO_Pallet, wmi.Controle')
    Left = 768
    Top = 1
    object QrTXPlCItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXPlCItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXPlCItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXPlCItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXPlCItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXPlCItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXPlCItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXPlCItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXPlCItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXPlCItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXPlCItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXPlCItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXPlCItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPlCItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPlCItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXPlCItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXPlCItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXPlCItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXPlCItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXPlCItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXPlCItsSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXPlCItsTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXPlCItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXPlCItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPlCItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPlCItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPlCItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXPlCItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXPlCItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXPlCItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXPlCItsQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPlCItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPlCItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXPlCItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXPlCItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXPlCItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXPlCItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXPlCItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXPlCItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXPlCItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXPlCItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrTXPlCItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrTXPlCItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrTXPlCItsClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
  end
  object DsTXPlCIts: TDataSource
    DataSet = QrTXPlCIts
    Left = 772
    Top = 45
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 660
    Top = 580
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Atualizaestoque1: TMenuItem
      Caption = 'Atuali&za estoque'
      OnClick = Atualizaestoque1Click
    end
    object Histrico1: TMenuItem
      Caption = '&Hist'#243'rico'
      object Inclui1: TMenuItem
        Caption = '&Inclui'
      end
      object Altera1: TMenuItem
        Caption = '&Altera'
      end
      object Exclui1: TMenuItem
        Caption = '&Exclui'
      end
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'ORDER BY NOMEENTIDADE')
    Left = 844
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 844
    Top = 44
  end
  object QrTransporta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 924
    Top = 4
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 924
    Top = 48
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 4
    object FichadePallets1: TMenuItem
      Caption = 'Ficha do pallet selecionado'
      object FichaCOMnomedoPallet1: TMenuItem
        Caption = 'Ficha &COM nome do Pallet'
        OnClick = FichaCOMnomedoPallet1Click
      end
      object FichaSEMnomedoPallet1: TMenuItem
        Caption = 'Ficha &SEM nome do Pallet'
        OnClick = FichaSEMnomedoPallet1Click
      end
    end
    object Fichadetodospalletsdestacompra1: TMenuItem
      Caption = 'Ficha de todos pallets desta compra'
      object FichasCOMnomedoPallet1: TMenuItem
        Caption = 'Fichas &COM nome do Pallet'
        OnClick = FichasCOMnomedoPallet1Click
      end
      object FichasSEMnomedoPallet1: TMenuItem
        Caption = 'Fichas &SEM nome do Pallet'
        OnClick = FichasSEMnomedoPallet1Click
      end
    end
    object Rendimentodesemiacabado1: TMenuItem
      Caption = 'Rendimento de semi acabado'
      OnClick = Rendimentodesemiacabado1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
  end
  object frxDsTXPlCCab: TfrxDBDataset
    UserName = 'frxDsTXPlCCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DtCompra=DtCompra'
      'DtViagem=DtViagem'
      'DtEntrada=DtEntrada'
      'Fornecedor=Fornecedor'
      'Transporta=Transporta'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE'
      'NO_TRANSPORTA=NO_TRANSPORTA'
      'ValorT=ValorT')
    DataSet = QrTXPlCCab
    BCDToCurrency = False
    Left = 688
    Top = 92
  end
  object frxDsTXPlCIts: TfrxDBDataset
    UserName = 'frxDsTXPlCIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'ValorT=ValorT'
      'MovimTwn=MovimTwn'
      'Misturou=Misturou'
      'Ficha=Ficha'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'NotaMPAG=NotaMPAG'
      'RendKgm2=RendKgm2'
      'NotaMPAG_TXT=NotaMPAG_TXT'
      'RendKgm2_TXT=RendKgm2_TXT'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'SdoVrtPeso=SdoVrtPeso'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'SrcGGX=SrcGGX'
      'DstGGX=DstGGX'
      'Misturou_TXT=Misturou_TXT'
      'NOMEUNIDMED=NOMEUNIDMED'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'm2_CouroTXT=m2_CouroTXT'
      'KgMedioCouro=KgMedioCouro')
    DataSet = QrTXPlCIts
    BCDToCurrency = False
    Left = 772
    Top = 92
  end
  object QrClienteMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 864
    Top = 340
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteMO: TDataSource
    DataSet = QrClienteMO
    Left = 864
    Top = 388
  end
  object QrProcednc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 944
    Top = 340
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsProcednc: TDataSource
    DataSet = QrProcednc
    Left = 944
    Top = 388
  end
  object QrMotorista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 796
    Top = 340
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 796
    Top = 388
  end
  object PMTXPlcIts: TPopupMenu
    Left = 136
    Top = 368
    object IrparajaneladegerenciamentodeFichaRMP1: TMenuItem
      Caption = '&Ir para janela de gerenciamento de Ficha RMP'
      OnClick = IrparajaneladegerenciamentodeFichaRMP1Click
    end
  end
end
