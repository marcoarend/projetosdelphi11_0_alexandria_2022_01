unit TXTrflocIMEI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, Vcl.Grids, Vcl.DBGrids,
  UnProjGroup_Consts, UnAppEnums, UnTX_Jan;

type
  TFmTXTrflocIMEI = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBGrid1: TDBGrid;
    QrTXPallet: TmySQLQuery;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletNome: TWideStringField;
    DsTXPallet: TDataSource;
    Panel3: TPanel;
    QrTXMovIts: TmySQLQuery;
    DsTXMovIts: TDataSource;
    QrTXMovItsNO_Pallet: TWideStringField;
    QrTXMovItsCodigo: TIntegerField;
    QrTXMovItsControle: TIntegerField;
    QrTXMovItsMovimCod: TIntegerField;
    QrTXMovItsMovimNiv: TIntegerField;
    QrTXMovItsEmpresa: TIntegerField;
    QrTXMovItsTerceiro: TIntegerField;
    QrTXMovItsMovimID: TIntegerField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TIntegerField;
    QrTXMovItsGraGruX: TIntegerField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsSrcMovID: TIntegerField;
    QrTXMovItsSrcNivel1: TIntegerField;
    QrTXMovItsSrcNivel2: TIntegerField;
    QrTXMovItsLk: TIntegerField;
    QrTXMovItsDataCad: TDateField;
    QrTXMovItsDataAlt: TDateField;
    QrTXMovItsUserCad: TIntegerField;
    QrTXMovItsUserAlt: TIntegerField;
    QrTXMovItsAlterWeb: TSmallintField;
    QrTXMovItsAtivo: TSmallintField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsNO_FORNECE: TWideStringField;
    Panel5: TPanel;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label9: TLabel;
    EdSrcNivel1: TdmkEdit;
    EdSrcNivel2: TdmkEdit;
    Label17: TLabel;
    Panel6: TPanel;
    EdQtde: TdmkEdit;
    LaQtde: TLabel;
    Label4: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SBPallet: TSpeedButton;
    EdValorT: TdmkEdit;
    LaValorT: TLabel;
    QrTXMovItsValorT: TFloatField;
    QrTXMovItsTalao: TIntegerField;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdMovimCod: TdmkDBEdit;
    DBEdEmpresa: TdmkDBEdit;
    DBEdDtEntrada: TdmkDBEdit;
    DBEdCliVenda: TdmkDBEdit;
    RGTipoCouro: TRadioGroup;
    Panel8: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    GroupBox3: TGroupBox;
    EdPsqPallet: TdmkEdit;
    Label10: TLabel;
    EdPsqFicha: TdmkEdit;
    Label11: TLabel;
    QrTXMovItsSerieTal: TIntegerField;
    Label12: TLabel;
    EdSrcGGX: TdmkEdit;
    QrTXMovItsMovimTwn: TIntegerField;
    QrTXMovItsCliVenda: TIntegerField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsFornecMO: TIntegerField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsCustoMOTot: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TIntegerField;
    QrTXMovItsDstNivel1: TIntegerField;
    QrTXMovItsDstNivel2: TIntegerField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsAptoUso: TSmallintField;
    QrTXMovItsSrcGGX: TIntegerField;
    QrTXMovItsDstGGX: TIntegerField;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsPedItsLib: TIntegerField;
    QrTXMovItsPedItsFin: TIntegerField;
    QrTXMovItsPedItsVda: TIntegerField;
    SbValorT: TSpeedButton;
    Label13: TLabel;
    EdPsqIMEI: TdmkEdit;
    BtReabre: TBitBtn;
    QrTXMovItsTXMulFrnCab: TIntegerField;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    Panel10: TPanel;
    Label49: TLabel;
    Label50: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label16: TLabel;
    EdObserv: TdmkEdit;
    QrTXMovItsStqCenLoc: TIntegerField;
    QrTXMovItsNO_PRD_TAM_COR: TWideStringField;
    QrTXMovItsClientMO: TIntegerField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    QrTXMovItsNFeSer: TSmallintField;
    QrTXMovItsNFeNum: TIntegerField;
    CBStqCenLocSrc: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdStqCenLocSrc: TdmkEditCB;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    Label39: TLabel;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    Label38: TLabel;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrStqCenLocEntiSitio: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSrcMovIDChange(Sender: TObject);
    procedure EdSrcNivel1Change(Sender: TObject);
    procedure EdSrcNivel2Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure SBPalletClick(Sender: TObject);
    procedure RGTipoCouroClick(Sender: TObject);
    procedure EdPsqPalletChange(Sender: TObject);
    procedure EdPsqFichaChange(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure EdValorTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbValorTClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure CopiaTotaisIMEI();
    procedure ReopenTXInnIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure ReopenTXMovIts((*GraGruX: Integer*));
    procedure HabilitaInclusao();
    procedure ReopenGGXY();
    procedure CalculaValorT();

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmTXTrflocIMEI: TFmTXTrflocIMEI;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXTrfLocCab, UnTX_PF, StqCenLoc;

{$R *.DFM}

procedure TFmTXTrflocIMEI.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = False;
  EdTalao     = nil;
  CustoMOUni  = 0;
  CustoMOM2   = 0;
  CustoMOTot  = 0;
  QtdGer      = 0;
  AptoUso     = 0; // Eh venda!
  //
  TpCalcAuto = -1;
  //
  PedItsLib   = 0;
  PedItsFin   = 0;
  PedItsVda  = 0;
  //
  QtdAnt      = 0;
  //
var
  DataHora, Marca: String;
  SrcMovID, SrcNivel1, SrcNivel2,
  Pallet, Codigo, CtrlSorc, CtrlDest, MovimCod, Empresa, GraGruX, Terceiro,
  CliVenda, SerieTal, Talao, DstNivel1, DstNivel2, GraGruY, SrcGGX,
  MovimTwn, DstGGX, ItemNFe, TXMulFrnCab, ClientMO: Integer;
  Qtde, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  FornecMO, ReqMovEstq, StqCenLoc, GGXRcl: Integer;
  Observ: String;
begin
  FornecMO       := QrStqCenLocEntiSitio.Value;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  Observ         := EdObserv.Text;
  SrcMovID       := 0;
  SrcNivel1      := 0;
  SrcNivel2      := 0;
  SrcGGX         := 0;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  CtrlSorc       := 0; //Ed????.ValueVariant;
  CtrlDest       := 0; //Ed????.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := Geral.IMV(DBEdEmpresa.Text);
  ClientMO       := QrTXMovItsClientMO.Value;
  Terceiro       := QrTXMovItsTerceiro.Value;
  TXMulFrnCab    := QrTXMovItsTXMulFrnCab.Value;
  CliVenda       := Geral.IMV(DBEdCliVenda.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidTransfLoc;
  MovimNiv       := eminDestLocal;
  Pallet         := QrTXMovItsPallet.Value; //EdPallet.ValueVariant;
  GraGruX        := QrTXMovItsGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not TX_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  FUltGGX        := GraGruX;
  Qtde           := EdQtde.ValueVariant;
  ValorMP        := EdValorT.ValueVariant;
  ValorT         := ValorMP;
  //
  SerieTal       := QrTXMovItsSerieTal.Value;
  Talao          := QrTXMovItsTalao.Value;
  Marca          := QrTXMovItsMarca.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := TX_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
  //
  if TX_PF.TXFic(GraGruX, Empresa, Terceiro, Pallet, Qtde,
  ValorT, nil, nil, EdQtde, EdValorT, ExigeFornecedor, GraGruY,
  nil, SerieTal, Talao, (*EdSerieTal*)nil, (*EdTalao*)nil) then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc,
  'Informe o local/centro de estoque') then
    Exit;
  //
  ItemNFe     := 0;
  //
  CtrlSorc := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlSorc);
  CtrlDest := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, CtrlDest);
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  //
  //Inclusao!!!
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, (*Controle*) CtrlDest,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGer, AptoUso, FornecMO,
  SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei077(*Geração de material no destino em transferência de estoque por IME-I*)) then
  begin
    //Baixa!!!
    ReqMovEstq     := 0;
    StqCenLoc      := QrTXMovItsStqCenLoc.Value;;
    Observ         := '';
    SrcMovID       := EdSrcMovID.ValueVariant;
    SrcNivel1      := EdSrcNivel1.ValueVariant;
    SrcNivel2      := EdSrcNivel2.ValueVariant;
    SrcGGX         := EdSrcGGX.ValueVariant;
    //
    MovimNiv       := eminSorcLocal;
    Qtde           := -Qtde;
    ValorT         := -ValorT;
    //
    DstMovID       := TEstqMovimID(0);
    DstNivel1      := 0;
    DstNivel2      := 0;
    DstGGX         := 0;
    //
    if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
    Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
    ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
    Observ, CliVenda, (*Controle*) CtrlSorc,
    CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
    QtdGer, AptoUso, FornecMO,
    SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
    ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
    QtdAnt, SerieTal, Talao,
    CO_0_GGXRcl,
    CO_0_MovCodPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iutpei078(*Baixa de material da origem em transferência de estoque por IME-I*)) then
    begin
      TX_PF.AtualizaSaldoIMEI(SrcNivel2, False);
      TX_PF.AtualizaSaldoIMEI(CtrlDest, False);
      TX_PF.AtualizaTotaisTXXxxCab('txtrfloccab', MovimCod);
      FmTXTrfLocCab.LocCod(Codigo, Codigo);
      ReopenTXInnIts(CtrlDest);
      FmTXTrfLocCab.AtualizaNFeItens();
      if CkContinuar.Checked then
      begin
        ImgTipo.SQLType            := stIns;
        //
        EdValorT.Enabled           := False;
        LaValorT.Enabled           := False;
        //
        EdSrcMovID.ValueVariant    := 0;
        EdSrcNivel1.ValueVariant   := 0;
        EdSrcNivel2.ValueVariant   := 0;
        EdSrcGGX.ValueVariant      := 0;
        //
        EdControle.ValueVariant    := 0;
        EdGraGruX.ValueVariant     := 0;
        CBGraGruX.KeyValue         := Null;
        EdQtde.ValueVariant        := 0;
        EdValorT.ValueVariant      := 0;
        EdPallet.ValueVariant      := 0;//DefineProximoPallet();
        //
        EdGraGruX.SetFocus;
        //
        ReopenTXMovIts();
      end else
        Close;
    end;
  end;
end;

procedure TFmTXTrflocIMEI.BtReabreClick(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXTrflocIMEI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXTrflocIMEI.CalculaValorT();
var
  Qtde, Valor: Double;
begin
  begin
    Qtde   := EdQtde.ValueVariant;
    Valor := 0;
    if QrTXMovItsQtde.Value > 0 then
      Valor := Qtde * (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value);
    //
    EdValorT.ValueVariant := Valor;
  end;
end;

procedure TFmTXTrflocIMEI.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXTrflocIMEI.CopiaTotaisIMEI();
var
  Valor: Double;
begin
  // Deve ser depois dos zerados!
  EdQtde.ValueVariant  := QrTXMovItsSdoVrtQtd.Value;
  //
  if QrTXMovItsQtde.Value > 0 then
    Valor := QrTXMovItsSdoVrtQtd.Value *
    (QrTXMovItsValorT.Value / QrTXMovItsQtde.Value);
  //
  EdValorT.ValueVariant := Valor;
end;

procedure TFmTXTrflocIMEI.DBGrid1DblClick(Sender: TObject);
begin
  EdSrcMovID.ValueVariant  := QrTXMovItsMovimID.Value;
  EdSrcNivel1.ValueVariant := QrTXMovItsCodigo.Value;
  EdSrcNivel2.ValueVariant := QrTXMovItsControle.Value;
  EdSrcGGX.ValueVariant    := QrTXMovItsGraGruX.Value;
  //
  try
    EdQtde.SetFocus;
  except
    //
  end;
end;

{
function TFmTXOutKno.DefineProximoPallet(): String;
var
  Txt: String;
begin
  Result := '';
  Txt := EdPallet.Text;
  if Geral.SoNumero_TT(Txt) = Txt then
  begin
    try
      Result := Geral.FI64(Geral.I64(EdPallet.Text) + 1);
    except
      Result := '';
    end;
  end else
    Result := '';
end;
}

procedure TFmTXTrflocIMEI.EdSrcNivel2Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXTrflocIMEI.EdValorTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    CalculaValorT();
end;

procedure TFmTXTrflocIMEI.EdSrcMovIDChange(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXTrflocIMEI.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmTXTrflocIMEI.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXTrflocIMEI.EdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaTotaisIMEI();
end;

procedure TFmTXTrflocIMEI.EdQtdeRedefinido(Sender: TObject);
begin
  CalculaValorT();
end;

procedure TFmTXTrflocIMEI.EdPsqFichaChange(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXTrflocIMEI.EdPsqPalletChange(Sender: TObject);
begin
  ReopenTXMovIts();
end;

procedure TFmTXTrflocIMEI.EdSrcNivel1Change(Sender: TObject);
begin
  HabilitaInclusao();
end;

procedure TFmTXTrflocIMEI.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdCliVenda.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXTrflocIMEI.FormCreate(Sender: TObject);
const
  Default = 2;
begin
  ImgTipo.SQLType := stLok;
  DBGrid1.Align := alClient;
  FUltGGX := 0;
  //
  ReopenGGXY();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPallet, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM txpalleta ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  //
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  TX_PF.SetIDTipoCouro(RGTipoCouro, CO_RGTIPOCOURO_COLUNAS, Default);
end;

procedure TFmTXTrflocIMEI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXTrflocIMEI.HabilitaInclusao();
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdSrcMovID.ValueVariant <> 0) and
    (EdSrcNivel1.ValueVariant <> 0) and
    (EdSrcNivel2.ValueVariant <> 0);
  //
  BtOK.Enabled := Habilita;
end;

procedure TFmTXTrflocIMEI.ReopenGGXY();
begin
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(TX_PF.ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex)));
  //
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue     := 0;
  TX_PF.AbreGraGruXY(QrGGXRcl, '');
end;

procedure TFmTXTrflocIMEI.ReopenTXInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXTrflocIMEI.ReopenTXMovIts();
const
  ImeiSrc   = 0;
  StqCenLoc = 0;
var
  StqCenCad, GraGruX, Empresa, Pallet, Ficha, IMEI, CouNiv1, CouNiv2: Integer;
begin
  StqCenCad  := EdStqCenLocSrc.ValueVariant;
  GraGruX    := EdGraGruX.ValueVariant;
  Empresa    := Geral.IMV(DBEdEmpresa.Text);
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  IMEI       := EdPsqIMEI.ValueVariant;
  CouNiv2    := EdCouNiv2.ValueVariant;
  CouNiv1    := EdCouNiv1.ValueVariant;
  TX_PF.ReopenIMEIsPositivos(QrTXMovIts, Empresa, GraGruX, Pallet, Ficha, IMEI,
    RGTipoCouro.ItemIndex, StqCenCad, StqCenLoc, ImeiSrc, CouNiv1, CouNiv2,
    FmTXTrflocCab.QrTXTrfLocCabDtVenda.Value);
end;

procedure TFmTXTrflocIMEI.RGTipoCouroClick(Sender: TObject);
begin
  ReopenGGXY();
end;

procedure TFmTXTrflocIMEI.SBPalletClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXPallet(0);
  UMyMod.SetaCodigoPesquisado(EdPallet, CBPallet, QrTXPallet, VAR_CADASTRO);
end;

procedure TFmTXTrflocIMEI.SbValorTClick(Sender: TObject);
begin
  EdValorT.Enabled := True;
  LaValorT.Enabled := True;
  EdValorT.SetFocus;
end;

procedure TFmTXTrflocIMEI.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
