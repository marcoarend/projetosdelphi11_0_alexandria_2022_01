unit TXCadInt;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO, UnGrade_PF;

type
  TFmTXCadInt = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtArtigo: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrTXCadInt: TmySQLQuery;
    DsTXCadInt: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrTXCadIntGraGruX: TIntegerField;
    QrTXCadIntLk: TIntegerField;
    QrTXCadIntDataCad: TDateField;
    QrTXCadIntDataAlt: TDateField;
    QrTXCadIntUserCad: TIntegerField;
    QrTXCadIntUserAlt: TIntegerField;
    QrTXCadIntAlterWeb: TSmallintField;
    QrTXCadIntAtivo: TSmallintField;
    QrTXCadIntGraGru1: TIntegerField;
    QrTXCadIntNO_PRD_TAM_COR: TWideStringField;
    frxWET_RECUR_013: TfrxReport;
    frxDsCad: TfrxDBDataset;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    PMArtigo: TPopupMenu;
    DBGrid1: TdmkDBGridZTO;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    QrTXCadIntFatorNota: TFloatField;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrGraGruXCou: TmySQLQuery;
    DsGraGruXCou: TDataSource;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    QrTXCadIntArtigoImp: TWideStringField;
    QrTXCadIntClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    QrGraGruXCouPrevPcPal: TIntegerField;
    QrTXCadIntMediaMinM2: TFloatField;
    QrTXCadIntMediaMaxM2: TFloatField;
    DBEdit2: TDBEdit;
    Label15: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label16: TLabel;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    QrGraGruXCouGrandeza: TSmallintField;
    QrTXCadIntGGXTwn: TIntegerField;
    Incluinovoprodutoemprocesso1: TMenuItem;
    Alteraprodutoemprocessoselecionado1: TMenuItem;
    Removeprodutoemprocessoselecionado1: TMenuItem;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    Panel6: TPanel;
    Label3: TLabel;
    EdGGXTwn: TdmkEditCB;
    CBGGXTwn: TdmkDBLookupComboBox;
    QrTXCadIntNO_GGXTwn: TWideStringField;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXCadIntAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXCadIntBeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
    procedure BtArtigoClick(Sender: TObject);
    procedure PMArtigoPopup(Sender: TObject);
    procedure QrTXCadIntBeforeClose(DataSet: TDataSet);
    procedure QrTXCadIntAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Excluilinkdeprodutos1Click(Sender: TObject);
    procedure Incluinovoprodutoemprocesso1Click(Sender: TObject);
    procedure Alteraprodutoemprocessoselecionado1Click(Sender: TObject);
    procedure Removeprodutoemprocessoselecionado1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    //procedure MostraVSNatArt(SQLType: TSQLType);
    //procedure ReopenVSNatArt(Codigo: Integer);

  public
    { Public declarations }
    FSeq: Integer;
    FQryMul: TmySQLQuery;
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmTXCadInt: TFmTXCadInt;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck, UnTX_PF,
  //VSNatArt,
  AppListas, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXCadInt.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

{POIU
procedure TFmTXCadInt.MostraVSNatArt(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmVSNatArt, FmVSNatArt, afmoNegarComAviso) then
  begin
    FmVSNatArt.ImgTipo.SQLType := SQLType;
    FmVSNatArt.FQrCab := QrTXCadInt;
    FmVSNatArt.FDsCab := DsVSNatCad;
    FmVSNatArt.FQrIts := QrVSNatArt;
    //
    FmVSNatArt.LaVSNatCad.Enabled := False;
    FmVSNatArt.EdVSNatCad.Enabled := False;
    FmVSNatArt.CBVSNatCad.Enabled := False;
    //
    FmVSNatArt.LaVSRibCad.Enabled := True;
    FmVSNatArt.EdVSRibCad.Enabled := True;
    FmVSNatArt.CBVSRibCad.Enabled := True;
    //

    FmVSNatArt.EdVSNatCad.ValueVariant := QrTXCadIntGraGruX.Value;
    FmVSNatArt.CBVSNatCad.KeyValue     := QrTXCadIntGraGruX.Value;
    //
    if SQLType = stIns then
    begin
      FmVSNatArt.FCodigo := 0;
    end else
    begin
      FmVSNatArt.FCodigo := QrVSNatArtCodigo.Value;
      //
      FmVSNatArt.EdVSRibCad.ValueVariant := QrVSNatArtVSRibCad.Value;
      FmVSNatArt.CBVSRibCad.KeyValue     := QrVSNatArtVSRibCad.Value;
      //
    end;
    FmVSNatArt.ShowModal;
    FmVSNatArt.Destroy;
  end;
end;
}

procedure TFmTXCadInt.PMArtigoPopup(Sender: TObject);
begin
{POIU
  MyObjects.HabilitaMenuItemItsIns(Incluilinkdeprodutos1, QrTXCadInt);
  MyObjects.HabilitaMenuItemItsUpd(Alteralinkdeprodutos1, QrVSNatArt);
  MyObjects.HabilitaMenuItemItsDel(Alteralinkdeprodutos1, QrVSNatArt);
}
end;

procedure TFmTXCadInt.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(Alteraprodutoemprocessoselecionado1, QrTXCadInt);
  MyObjects.HabilitaMenuItemCabUpd(Removeprodutoemprocessoselecionado1, QrTXCadInt);
end;

procedure TFmTXCadInt.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXCadIntGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXCadInt.DefParams;
begin
  VAR_GOTOTABELA := 'txcadint';
  VAR_GOTOMYSQLTABLE := QrTXCadInt;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  //
  VAR_SQLx.Add('SELECT wcn.*, cou.*, ');
  VAR_SQLx.Add('ggx1.GraGru1, CONCAT(gg11.Nome,  ');
  VAR_SQLx.Add('IF(gti1.PrintTam=0 OR gti1.Codigo IS NULL, "", CONCAT(" ", gti1.Nome)),  ');
  VAR_SQLx.Add('IF(gcc1.PrintCor=0,"", CONCAT(" ", gcc1.Nome)))  ');
  VAR_SQLx.Add('NO_PRD_TAM_COR,  ');
  VAR_SQLx.Add('CONCAT(gg12.Nome,  ');
  VAR_SQLx.Add('IF(gti2.PrintTam=0 OR gti2.Codigo IS NULL, "", CONCAT(" ", gti2.Nome)),  ');
  VAR_SQLx.Add('IF(gcc2.PrintCor=0,"", CONCAT(" ", gcc2.Nome)))  ');
  VAR_SQLx.Add('NO_GGXTwn  ');
  VAR_SQLx.Add(' ');
  VAR_SQLx.Add('FROM txcadint wcn  ');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggx1 ON ggx1.Controle=wcn.GraGruX  ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc1 ON ggc1.Controle=ggx1.GraGruC  ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc1 ON gcc1.Codigo=ggc1.GraCorCad  ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti1 ON gti1.Controle=ggx1.GraTamI  ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  ');
  VAR_SQLx.Add(' ');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou  ON cou.GraGruX=ggx1.Controle');
  VAR_SQLx.Add(' ');
  VAR_SQLx.Add('LEFT JOIN gragrux    ggx2 ON ggx2.Controle=wcn.GGXTwn   ');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc2 ON ggc2.Controle=ggx2.GraGruC  ');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc2 ON gcc2.Codigo=ggc2.GraCorCad  ');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti2 ON gti2.Controle=ggx2.GraTamI  ');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg12 ON gg12.Nivel1=ggx2.GraGru1  ');
  //
  VAR_SQLx.Add('WHERE wcn.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wcn.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmTXCadInt.Excluilinkdeprodutos1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{ POIU
  if VS_PF.ExcluiVSNaoVMI(
  'Confirma a retirada do link de artigo selecionado?',
  'VSNatArt', 'Codigo', QrVSNatArtCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSNatArt,
      QrVSNatArtCodigo, QrVSNatArtCodigo.Value);
    ReopenVSNatArt(Codigo);
  end;
}
end;

procedure TFmTXCadInt.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXCadInt.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXCadInt.Removeprodutoemprocessoselecionado1Click(Sender: TObject);
const
  GraGruY = 0;
var
  Controle: Integer;
begin
  Controle := QrTXCadIntGraGruX.Value;
  if TX_PF.ExcluiTXNaoTMI('Confirma a remo��o do reduzido selecionado?',
  'txcadint', 'GraGruX', QrTXCadIntGraGruX.Value, Dmod.MyDB) = ID_YES then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
    'GraGruY'], ['Controle'], [
    GraGruY], [Controle], True);
    //
    LocCod(Controle, Controle);
  end;
end;

{POIU
procedure TFmTXCadInt.ReopenVSNatArt(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSNatArt, Dmod.MyDB, [
  'SELECT wmp.GraGruX, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vna.* ',
  'FROM vsnatart vna ',
  'LEFT JOIN vsribcad wmp ON wmp.GraGruX=vna.VSRibCad ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vna.VSNatCad=' + Geral.FF0(QrTXCadIntGraGruX.Value),
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;
}

procedure TFmTXCadInt.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXCadInt.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXCadInt.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXCadInt.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXCadInt.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXCadInt.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXCadInt.Alteralinkdeprodutos1Click(Sender: TObject);
begin
{POIU
  MostraVSNatArt(stUpd);
}
end;

procedure TFmTXCadInt.Alteraprodutoemprocessoselecionado1Click(Sender: TObject);
begin
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrTXCadIntGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdGGXTwn, ImgTipo, 'gragruxcou');
end;

procedure TFmTXCadInt.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXCadIntGraGruX.Value;
  Close;
end;

procedure TFmTXCadInt.BtConfirmaClick(Sender: TObject);
const
  MediaMinM2 = 0;
  MediaMaxM2 = 0;
  Grandeza   = -1;
var
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, GraGru1, GGXTwn: Integer;
  FatorNota, PrevAMPal, PrevKgPal: Double;
  ArtigoImp, ClasseImp, Nome: String;
begin
  GraGruX        := QrTXCadIntGraGruX.Value;
  GraGru1        := QrTXCadIntGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  Nome           := EdNome.Text;
  GGXTwn         := EdGGXTwn.ValueVariant;
  //
  if MyObjects.FIC(GGXTwn = 0, EdGGXTwn, 'Informe o "Artigo resultante"!') then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txcadint', False, [
  'GGXTwn'], [
  'GraGruX'], [
  GGXTwn], [
  GraGruX], True) then
  begin
{
    VS_PF.ReIncluiCouNivs(GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1,
    1, PrevAMPal, PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMinM2);
}
    //
{
    if FQryMul <> nil then
    begin
      FQryMul.First;
      while not FQryMul.Eof do
      begin
        GraGruX := FQryMul.FieldByName('Controle').AsInteger;
        VS_PF.ReIncluiCouNivs(
          GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1, (*Bastidao*)-1,
          PrevAMPal, PrevKgPal, ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2);
        //
        FQryMul.Next;
      end;
    end;
}
    //
    Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    if FSeq = 0 then
    begin
      LocCod(GraGruX, GraGruX);
    end else
      Close;
  end;
end;

procedure TFmTXCadInt.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTXCadInt.BtArtigoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArtigo, BtArtigo);
end;

procedure TFmTXCadInt.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmTXCadInt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  //VS_PF.ConfiguraRGVSBastidao(RGBastidao, False(*Habilita*), (*Default*)1);
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_TXCadFcc));
  CriaOForm;
end;

procedure TFmTXCadInt.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXCadIntGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmTXCadInt.SbImprimeClick(Sender: TObject);
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  Ordem := MyObjects.SelRadioGroup('Ordem de Impress�o',
  'Selecione a ordem de Impress�o', [
  'Ordem',
  'Nome',
  'Pre�o'], 2);
  case Ordem of
    0: Ord_Txt := 'Ordem';
    1: Ord_Txt := 'NO_PRD_TAM_COR';
    2: Ord_Txt := 'BRLMedM2';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCad, Dmod.MyDB, [
  'SELECT wmp.*, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM txcadint wmp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY ' + Ord_Txt,
  '']);
  MyObjects.frxDefineDataSets(frxWET_RECUR_013, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxWET_RECUR_013,
    'Configura��o de Mat�ria-prima para Semi / Acabado');
end;

procedure TFmTXCadInt.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXCadInt.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrTXCadIntGraGruX.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXCadInt.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXCadInt.QrTXCadIntAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXCadInt.QrTXCadIntAfterScroll(DataSet: TDataSet);
begin
{POIU
  ReopenVSNatArt(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrTXCadIntGraGruX.Value);
}
end;

procedure TFmTXCadInt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXCadInt.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrTXCadIntGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'txcadint', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXCadInt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXCadInt.Incluilinkdeprodutos1Click(Sender: TObject);
begin
{POIU
  MostraVSNatArt(stIns);
}
end;

procedure TFmTXCadInt.Incluinovoprodutoemprocesso1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_4096_TXCadInt);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM txcadint ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'txcadint', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
      Alteraprodutoemprocessoselecionado1Click(Self);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmTXCadInt.QrTXCadIntBeforeClose(DataSet: TDataSet);
begin
{POIU
  QrVSNatArt.Close;
  QrGraGruXCou.Close;
}
end;

procedure TFmTXCadInt.QrTXCadIntBeforeOpen(DataSet: TDataSet);
begin
  QrTXCadIntGraGruX.DisplayFormat := FFormatFloat;
end;

end.

