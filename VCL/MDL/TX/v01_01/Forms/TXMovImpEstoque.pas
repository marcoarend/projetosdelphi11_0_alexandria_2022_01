unit TXMovImpEstoque;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, dmkDBGridZTO,
  mySQLDbTables, UnProjGroup_Consts, AppListas;

type
  TFmTXMovImpEstoque = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel43: TPanel;
    Splitter1: TSplitter;
    GroupBox16: TGroupBox;
    DBG00GraGruY: TdmkDBGridZTO;
    GroupBox18: TGroupBox;
    DBG00GraGruX: TdmkDBGridZTO;
    Panel52: TPanel;
    Ed00NomeGG1: TEdit;
    Panel6: TPanel;
    ScrollBox1: TScrollBox;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel47: TPanel;
    Panel46: TPanel;
    Label53: TLabel;
    Label55: TLabel;
    Ed00StqCenCad: TdmkEditCB;
    CB00StqCenCad: TdmkDBLookupComboBox;
    Ck00DataCompra: TCheckBox;
    RG00ZeroNegat: TRadioGroup;
    Ed00Terceiro: TdmkEditCB;
    CB00Terceiro: TdmkDBLookupComboBox;
    Ck00EmProcessoBH: TCheckBox;
    GroupBox19: TGroupBox;
    DBG00CouNiv2: TdmkDBGridZTO;
    Panel69: TPanel;
    Label1: TLabel;
    Ed00MovimCod: TdmkEdit;
    Panel26: TPanel;
    RG00_Ordem1: TRadioGroup;
    RG00_Ordem2: TRadioGroup;
    RG00_Ordem3: TRadioGroup;
    Panel49: TPanel;
    RG00_Agrupa: TRadioGroup;
    Panel50: TPanel;
    Panel58: TPanel;
    Ck00EstoqueEm: TCheckBox;
    TP00EstoqueEm: TdmkEditDateTimePicker;
    RG00DescrAgruNoItm: TRadioGroup;
    RG00_Ordem4: TRadioGroup;
    RG00_Ordem5: TRadioGroup;
    Panel63: TPanel;
    Label61: TLabel;
    Ed00GraCusPrc: TdmkEditCB;
    CB00GraCusPrc: TdmkDBLookupComboBox;
    GroupBox28: TGroupBox;
    Panel65: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Ed00NFeIni: TdmkEdit;
    Ed00NFeFim: TdmkEdit;
    Ck00Serie: TCheckBox;
    Ed00Serie: TdmkEdit;
    Qr00GraGruY: TmySQLQuery;
    Qr00GraGruYCodigo: TIntegerField;
    Qr00GraGruYTabela: TWideStringField;
    Qr00GraGruYNome: TWideStringField;
    Qr00GraGruYOrdem: TIntegerField;
    Ds00GraGruY: TDataSource;
    Qr00CouNiv2: TmySQLQuery;
    Qr00CouNiv2Codigo: TIntegerField;
    Qr00CouNiv2Nome: TWideStringField;
    Ds00CouNiv2: TDataSource;
    Qr00StqCenCad: TmySQLQuery;
    Qr00StqCenCadCodigo: TIntegerField;
    Qr00StqCenCadNome: TWideStringField;
    Ds00StqCenCad: TDataSource;
    Qr00Fornecedor: TmySQLQuery;
    Qr00FornecedorCodigo: TIntegerField;
    Qr00FornecedorNOMEENTIDADE: TWideStringField;
    Ds00Fornecedor: TDataSource;
    Qr00GraGruX: TmySQLQuery;
    Qr00GraGruXGraGru1: TIntegerField;
    Qr00GraGruXControle: TIntegerField;
    Qr00GraGruXNO_PRD_TAM_COR: TWideStringField;
    Qr00GraGruXSIGLAUNIDMED: TWideStringField;
    Qr00GraGruXCODUSUUNIDMED: TIntegerField;
    Qr00GraGruXNOMEUNIDMED: TWideStringField;
    Ds00GraGruX: TDataSource;
    Qr00GraCusPrc: TmySQLQuery;
    Qr00GraCusPrcCodigo: TIntegerField;
    Qr00GraCusPrcNome: TWideStringField;
    Ds00GraCusPrc: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    Label54: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataRelativa: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Ed00NomeGG1Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Reopen00GraGruX();
    procedure ImprimeEstoque();

  public
    { Public declarations }
  end;

  var
  FmTXMovImpEstoque: TFmTXMovImpEstoque;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UnTX_PF;

{$R *.DFM}

procedure TFmTXMovImpEstoque.BtOKClick(Sender: TObject);
begin
  ImprimeEstoque();
end;

procedure TFmTXMovImpEstoque.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXMovImpEstoque.Ed00NomeGG1Change(Sender: TObject);
begin
  Reopen00GraGruX();
end;

procedure TFmTXMovImpEstoque.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXMovImpEstoque.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  TP00EstoqueEm.Date := Geral.PrimeiroDiaDoMes(date()) - 1;
  //
  MyObjects.PreencheComponente(RG00_Ordem1, sMax_TX_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem2, sMax_TX_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem3, sMax_TX_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem4, sMax_TX_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem5, sMax_TX_IMP_ESTQ_ORD, 1);
  RG00_Ordem1.ItemIndex := 5;
  RG00_Ordem2.ItemIndex := 8;
  RG00_Ordem3.ItemIndex := 0;
  RG00_Ordem4.ItemIndex := 2;
  RG00_Ordem5.ItemIndex := 1;
  //
  UnDmkDAC_PF.AbreQuery(Qr00GraGruY, Dmod.MyDB);
  Reopen00GraGruX();
  UnDmkDAC_PF.AbreQuery(Qr00StqCenCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(Qr00CouNiv2, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(Qr00Fornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(Qr00GraCusPrc, Dmod.MyDB);
end;

procedure TFmTXMovImpEstoque.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXMovImpEstoque.ImprimeEstoque();
const
  DataRetroativa = '';
  MostraFrx = True;
var
  Entidade, Filial, ItemAgruNoItm: Integer;
  TableSrc: String;
  DescrAgruNoItm: Boolean;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Entidade);
  if MyObjects.FIC(Entidade = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  Filial := EdEmpresa.ValueVariant;
  DescrAgruNoItm := RG00DescrAgruNoItm.ItemIndex = 2;
  if not DescrAgruNoItm then
  begin
    ItemAgruNoItm := -1;
    ItemAgruNoItm := MyObjects.SelRadioGroup('Descri��o do item',
      'Selecione a forma de "Descri��o do item"',
      Geral.TSTA(RG00DescrAgruNoItm.Items), 1, -1);
    RG00DescrAgruNoItm.ItemIndex := ItemAgruNoItm;
    DescrAgruNoItm := RG00DescrAgruNoItm.ItemIndex = 2;
  end;
  if ItemAgruNoItm < 0 then
    Exit;
  if MyObjects.FIC(RG00DescrAgruNoItm.ItemIndex = 0, RG00DescrAgruNoItm,
  'Informe a forma de "Descri��o do item"') then
    Exit;
  if Ck00EstoqueEm.Checked then
  begin
    TX_PF.ImprimeEstoqueEm(Entidade, Filial, Ed00Terceiro.ValueVariant,
    RG00_Ordem1.ItemIndex, RG00_Ordem2.ItemIndex, RG00_Ordem3.ItemIndex,
    RG00_Ordem4.ItemIndex, RG00_Ordem5.ItemIndex, RG00_Agrupa.ItemIndex,
    DescrAgruNoItm, Ed00StqCenCad.ValueVariant,
    RG00ZeroNegat.ItemIndex, CBEmpresa.Text, CB00StqCenCad.Text,
    CB00Terceiro.Text, TPDataRelativa.Date, Ck00DataCompra.Checked,
    Ck00EmProcessoBH.Checked, DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2,
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2, TP00EstoqueEm.Date,
    Ed00GraCusPrc.ValueVariant, Ed00NFeIni.ValueVariant,
    Ed00NFeFim.ValueVariant, Ck00Serie.Checked, Ed00Serie.ValueVariant,
    Ed00MovimCod.ValueVariant, LaAviso1, LaAviso2, MostraFrx)
  end else
  begin
    TableSrc := TMeuDB + '.' + CO_SEL_TAB_TMI;
    //
    TX_PF.ImprimeEstoqueReal(Entidade, Filial, Ed00Terceiro.ValueVariant,
    RG00_Ordem1.ItemIndex, RG00_Ordem2.ItemIndex, RG00_Ordem3.ItemIndex,
    RG00_Ordem4.ItemIndex, RG00_Ordem5.ItemIndex, RG00_Agrupa.ItemIndex,
    DescrAgruNoItm, Ed00StqCenCad.ValueVariant,
    RG00ZeroNegat.ItemIndex, CBEmpresa.Text, CB00StqCenCad.Text,
    CB00Terceiro.Text, TPDataRelativa.Date, Ck00DataCompra.Checked,
    Ck00EmProcessoBH.Checked, DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2,
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2, TableSrc, DataRetroativa,
    Ed00GraCusPrc.ValueVariant, DmodG.ObtemAgora(), Ed00NFeIni.ValueVariant,
    Ed00NFeFim.ValueVariant, Ck00Serie.Checked, Ed00Serie.ValueVariant,
    Ed00MovimCod.ValueVariant, MostraFrx);
  end;
end;

procedure TFmTXMovImpEstoque.Reopen00GraGruX();
var
  Texto: String;
begin
  Texto := Ed00NomeGG1.Text;
  if Length(Texto) < 3 then
    Texto := ''
  else
    Texto := Geral.ATS(['AND CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'LIKE "' + Texto + '"']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr00GraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,   ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED  ',
  'FROM gragrux ggx   ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE ggx.Controle > 0  ',
  Texto,
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle   ',
  '']);
end;

end.
