unit TXRRMCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnMyObjects,
  UnProjGroup_Consts, dmkDBGridZTO, UnGrl_Consts, UnTX_EFD_ICMS_IPI, UnAppEnums,
  UnProjGroup_Vars, UnTX_Jan;

type
  TFmTXRRMCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrTXRRMCab: TmySQLQuery;
    DsTXRRMCab: TDataSource;
    QrTXRRMInn: TmySQLQuery;
    DsTXRRMInn: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    ItsAlteraOri: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrTXRRMCabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrTXRRMCabEmpresa: TIntegerField;
    QrTXRRMCabDtHrAberto: TDateTimeField;
    QrTXRRMCabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrTXRRMCabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrTXRRMOriIMEI: TmySQLQuery;
    DsTXRRMOriIMEI: TDataSource;
    QrTXRRMCabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrTXRRMCabLk: TIntegerField;
    QrTXRRMCabDataCad: TDateField;
    QrTXRRMCabDataAlt: TDateField;
    QrTXRRMCabUserCad: TIntegerField;
    QrTXRRMCabUserAlt: TIntegerField;
    QrTXRRMCabAlterWeb: TSmallintField;
    QrTXRRMCabAtivo: TSmallintField;
    QrTXRRMCabQtdeMan: TFloatField;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrTXRRMCabNO_DtHrFimOpe: TWideStringField;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrTXRRMCabNO_DtHrLibOpe: TWideStringField;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrTXRRMCabNO_DtHrCfgOpe: TWideStringField;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrTXRRMCabCacCod: TIntegerField;
    QrTXRRMCabCustoManMOUni: TFloatField;
    QrTXRRMCabCustoManMOTot: TFloatField;
    QrTXRRMCabValorManMP: TFloatField;
    QrTXRRMCabValorManT: TFloatField;
    QrTXRRMCabDtHrLibOpe: TDateTimeField;
    QrTXRRMCabDtHrCfgOpe: TDateTimeField;
    QrTXRRMCabDtHrFimOpe: TDateTimeField;
    QrTXRRMCabQtdeSrc: TFloatField;
    QrTXRRMCabQtdeDst: TFloatField;
    QrTXRRMCabQtdeSdo: TFloatField;
    QrTXRRMDst: TmySQLQuery;
    DsTXRRMDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsAlteraDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    N3: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    QrTXRRMBxa: TmySQLQuery;
    DsTXRRMBxa: TDataSource;
    QrTXRRMCabValorTMan: TFloatField;
    QrTXRRMCabValorTSrc: TFloatField;
    QrTXRRMCabValorTSdo: TFloatField;
    QrTXRRMCabQtdeBxa: TFloatField;
    QrTXRRMCabValorTBxa: TFloatField;
    Definiodequantidadesmanualmente1: TMenuItem;
    PCRRMOri: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    QrTXRRMOriPallet: TmySQLQuery;
    DsTXRRMOriPallet: TDataSource;
    DBGrid2: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    Parcial1: TMenuItem;
    otal1: TMenuItem;
    Parcial2: TMenuItem;
    otal2: TMenuItem;
    QrTXPedIts: TmySQLQuery;
    QrTXPedItsNO_PRD_TAM_COR: TWideStringField;
    QrTXPedItsControle: TIntegerField;
    DsTXPedIts: TDataSource;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    DsStqCenLoc: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrTXRRMCabCliente: TIntegerField;
    QrTXRRMCabNO_Cliente: TWideStringField;
    Label55: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    CorrigirFornecedor1: TMenuItem;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label56: TLabel;
    QrTXRRMCabNFeRem: TIntegerField;
    QrTXRRMCabLPFMO: TWideStringField;
    Label58: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit44: TDBEdit;
    OrdensdeProduoemAberto1: TMenuItem;
    QrTXRRMCabTemIMEIMrt: TIntegerField;
    QrTXRRMInnCodigo: TLargeintField;
    QrTXRRMInnControle: TLargeintField;
    QrTXRRMInnMovimCod: TLargeintField;
    QrTXRRMInnMovimNiv: TLargeintField;
    QrTXRRMInnMovimTwn: TLargeintField;
    QrTXRRMInnEmpresa: TLargeintField;
    QrTXRRMInnTerceiro: TLargeintField;
    QrTXRRMInnCliVenda: TLargeintField;
    QrTXRRMInnMovimID: TLargeintField;
    QrTXRRMInnDataHora: TDateTimeField;
    QrTXRRMInnPallet: TLargeintField;
    QrTXRRMInnGraGruX: TLargeintField;
    QrTXRRMInnQtde: TFloatField;
    QrTXRRMInnValorT: TFloatField;
    QrTXRRMInnSrcMovID: TLargeintField;
    QrTXRRMInnSrcNivel1: TLargeintField;
    QrTXRRMInnSrcNivel2: TLargeintField;
    QrTXRRMInnSrcGGX: TLargeintField;
    QrTXRRMInnSdoVrtQtd: TFloatField;
    QrTXRRMInnObserv: TWideStringField;
    QrTXRRMInnSerieTal: TLargeintField;
    QrTXRRMInnTalao: TLargeintField;
    QrTXRRMInnFornecMO: TLargeintField;
    QrTXRRMInnCustoMOUni: TFloatField;
    QrTXRRMInnCustoMOTot: TFloatField;
    QrTXRRMInnValorMP: TFloatField;
    QrTXRRMInnDstMovID: TLargeintField;
    QrTXRRMInnDstNivel1: TLargeintField;
    QrTXRRMInnDstNivel2: TLargeintField;
    QrTXRRMInnDstGGX: TLargeintField;
    QrTXRRMInnQtdGer: TFloatField;
    QrTXRRMInnQtdAnt: TFloatField;
    QrTXRRMInnNO_PALLET: TWideStringField;
    QrTXRRMInnNO_PRD_TAM_COR: TWideStringField;
    QrTXRRMInnNO_TTW: TWideStringField;
    QrTXRRMInnID_TTW: TLargeintField;
    QrTXRRMInnNO_FORNECE: TWideStringField;
    QrTXRRMInnReqMovEstq: TLargeintField;
    QrTXRRMInnCUSTO_UNI: TFloatField;
    QrTXRRMInnNO_LOC_CEN: TWideStringField;
    QrTXRRMInnMarca: TWideStringField;
    QrTXRRMInnPedItsLib: TLargeintField;
    QrTXRRMInnStqCenLoc: TLargeintField;
    QrTXRRMOriIMEICodigo: TLargeintField;
    QrTXRRMOriIMEIControle: TLargeintField;
    QrTXRRMOriIMEIMovimCod: TLargeintField;
    QrTXRRMOriIMEIMovimNiv: TLargeintField;
    QrTXRRMOriIMEIMovimTwn: TLargeintField;
    QrTXRRMOriIMEIEmpresa: TLargeintField;
    QrTXRRMOriIMEITerceiro: TLargeintField;
    QrTXRRMOriIMEICliVenda: TLargeintField;
    QrTXRRMOriIMEIMovimID: TLargeintField;
    QrTXRRMOriIMEIDataHora: TDateTimeField;
    QrTXRRMOriIMEIPallet: TLargeintField;
    QrTXRRMOriIMEIGraGruX: TLargeintField;
    QrTXRRMOriIMEIQtde: TFloatField;
    QrTXRRMOriIMEIValorT: TFloatField;
    QrTXRRMOriIMEISrcMovID: TLargeintField;
    QrTXRRMOriIMEISrcNivel1: TLargeintField;
    QrTXRRMOriIMEISrcNivel2: TLargeintField;
    QrTXRRMOriIMEISrcGGX: TLargeintField;
    QrTXRRMOriIMEISdoVrtQtd: TFloatField;
    QrTXRRMOriIMEIObserv: TWideStringField;
    QrTXRRMOriIMEISerieTal: TLargeintField;
    QrTXRRMOriIMEITalao: TLargeintField;
    QrTXRRMOriIMEIFornecMO: TLargeintField;
    QrTXRRMOriIMEICustoMOUni: TFloatField;
    QrTXRRMOriIMEICustoMOTot: TFloatField;
    QrTXRRMOriIMEIValorMP: TFloatField;
    QrTXRRMOriIMEIDstMovID: TLargeintField;
    QrTXRRMOriIMEIDstNivel1: TLargeintField;
    QrTXRRMOriIMEIDstNivel2: TLargeintField;
    QrTXRRMOriIMEIDstGGX: TLargeintField;
    QrTXRRMOriIMEIQtdGer: TFloatField;
    QrTXRRMOriIMEIQtdAnt: TFloatField;
    QrTXRRMOriIMEINO_PALLET: TWideStringField;
    QrTXRRMOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrTXRRMOriIMEINO_TTW: TWideStringField;
    QrTXRRMOriIMEIID_TTW: TLargeintField;
    QrTXRRMOriIMEINO_FORNECE: TWideStringField;
    QrTXRRMOriIMEINO_SerieTal: TWideStringField;
    QrTXRRMOriIMEIReqMovEstq: TLargeintField;
    QrTXRRMOriPalletPallet: TLargeintField;
    QrTXRRMOriPalletGraGruX: TLargeintField;
    QrTXRRMOriPalletQtde: TFloatField;
    QrTXRRMOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrTXRRMOriPalletNO_Pallet: TWideStringField;
    QrTXRRMOriPalletSerieTal: TLargeintField;
    QrTXRRMOriPalletTalao: TLargeintField;
    QrTXRRMOriPalletNO_TTW: TWideStringField;
    QrTXRRMOriPalletID_TTW: TLargeintField;
    QrTXRRMOriPalletTerceiro: TLargeintField;
    QrTXRRMOriPalletMarca: TWideStringField;
    QrTXRRMOriPalletNO_FORNECE: TWideStringField;
    QrTXRRMOriPalletValorT: TFloatField;
    QrTXRRMOriPalletSdoVrtQtd: TFloatField;
    QrTXRRMDstCodigo: TLargeintField;
    QrTXRRMDstControle: TLargeintField;
    QrTXRRMDstMovimCod: TLargeintField;
    QrTXRRMDstMovimNiv: TLargeintField;
    QrTXRRMDstMovimTwn: TLargeintField;
    QrTXRRMDstEmpresa: TLargeintField;
    QrTXRRMDstTerceiro: TLargeintField;
    QrTXRRMDstCliVenda: TLargeintField;
    QrTXRRMDstMovimID: TLargeintField;
    QrTXRRMDstDataHora: TDateTimeField;
    QrTXRRMDstPallet: TLargeintField;
    QrTXRRMDstGraGruX: TLargeintField;
    QrTXRRMDstQtde: TFloatField;
    QrTXRRMDstValorT: TFloatField;
    QrTXRRMDstSrcMovID: TLargeintField;
    QrTXRRMDstSrcNivel1: TLargeintField;
    QrTXRRMDstSrcNivel2: TLargeintField;
    QrTXRRMDstSrcGGX: TLargeintField;
    QrTXRRMDstSdoVrtQtd: TFloatField;
    QrTXRRMDstObserv: TWideStringField;
    QrTXRRMDstSerieTal: TLargeintField;
    QrTXRRMDstTalao: TLargeintField;
    QrTXRRMDstFornecMO: TLargeintField;
    QrTXRRMDstCustoMOUni: TFloatField;
    QrTXRRMDstCustoMOTot: TFloatField;
    QrTXRRMDstValorMP: TFloatField;
    QrTXRRMDstDstMovID: TLargeintField;
    QrTXRRMDstDstNivel1: TLargeintField;
    QrTXRRMDstDstNivel2: TLargeintField;
    QrTXRRMDstDstGGX: TLargeintField;
    QrTXRRMDstQtdGer: TFloatField;
    QrTXRRMDstQtdAnt: TFloatField;
    QrTXRRMDstNO_PALLET: TWideStringField;
    QrTXRRMDstNO_PRD_TAM_COR: TWideStringField;
    QrTXRRMDstNO_TTW: TWideStringField;
    QrTXRRMDstID_TTW: TLargeintField;
    QrTXRRMDstNO_FORNECE: TWideStringField;
    QrTXRRMDstNO_SerieTal: TWideStringField;
    QrTXRRMDstReqMovEstq: TLargeintField;
    QrTXRRMDstPedItsFin: TLargeintField;
    QrTXRRMDstMarca: TWideStringField;
    QrTXRRMBxaCodigo: TLargeintField;
    QrTXRRMBxaControle: TLargeintField;
    QrTXRRMBxaMovimCod: TLargeintField;
    QrTXRRMBxaMovimNiv: TLargeintField;
    QrTXRRMBxaMovimTwn: TLargeintField;
    QrTXRRMBxaEmpresa: TLargeintField;
    QrTXRRMBxaTerceiro: TLargeintField;
    QrTXRRMBxaCliVenda: TLargeintField;
    QrTXRRMBxaMovimID: TLargeintField;
    QrTXRRMBxaDataHora: TDateTimeField;
    QrTXRRMBxaPallet: TLargeintField;
    QrTXRRMBxaGraGruX: TLargeintField;
    QrTXRRMBxaQtde: TFloatField;
    QrTXRRMBxaValorT: TFloatField;
    QrTXRRMBxaSrcMovID: TLargeintField;
    QrTXRRMBxaSrcNivel1: TLargeintField;
    QrTXRRMBxaSrcNivel2: TLargeintField;
    QrTXRRMBxaSrcGGX: TLargeintField;
    QrTXRRMBxaSdoVrtQtd: TFloatField;
    QrTXRRMBxaObserv: TWideStringField;
    QrTXRRMBxaSerieTal: TLargeintField;
    QrTXRRMBxaTalao: TLargeintField;
    QrTXRRMBxaFornecMO: TLargeintField;
    QrTXRRMBxaCustoMOUni: TFloatField;
    QrTXRRMBxaCustoMOTot: TFloatField;
    QrTXRRMBxaValorMP: TFloatField;
    QrTXRRMBxaDstMovID: TLargeintField;
    QrTXRRMBxaDstNivel1: TLargeintField;
    QrTXRRMBxaDstNivel2: TLargeintField;
    QrTXRRMBxaDstGGX: TLargeintField;
    QrTXRRMBxaQtdGer: TFloatField;
    QrTXRRMBxaQtdAnt: TFloatField;
    QrTXRRMBxaNO_PALLET: TWideStringField;
    QrTXRRMBxaNO_PRD_TAM_COR: TWideStringField;
    QrTXRRMBxaNO_TTW: TWideStringField;
    QrTXRRMBxaID_TTW: TLargeintField;
    QrTXRRMBxaNO_FORNECE: TWideStringField;
    QrTXRRMBxaNO_SerieTal: TWideStringField;
    QrTXRRMBxaReqMovEstq: TLargeintField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosQtde: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtQtd: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieTal: TLargeintField;
    QrForcadosTalao: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOUni: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGer: TFloatField;
    QrForcadosQtdAnt: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    IrparajaneladoPallet1: TMenuItem;
    QrTXRRMInnNO_FORNEC_MO: TWideStringField;
    QrTXRRMDstStqCenLoc: TLargeintField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    QrTXRRMInnClientMO: TLargeintField;
    OPpreenchida1: TMenuItem;
    QrGGXDst: TmySQLQuery;
    QrGGXDstGraGru1: TIntegerField;
    QrGGXDstControle: TIntegerField;
    QrGGXDstNO_PRD_TAM_COR: TWideStringField;
    QrGGXDstSIGLAUNIDMED: TWideStringField;
    QrGGXDstCODUSUUNIDMED: TIntegerField;
    QrGGXDstNOMEUNIDMED: TWideStringField;
    DsGGXDst: TDataSource;
    IrparacadastrodoPallet1: TMenuItem;
    dmkDBEdit3: TdmkDBEdit;
    Label59: TLabel;
    QrTXRRMCabSerieRem: TSmallintField;
    EdSerieRem: TdmkEdit;
    Label57: TLabel;
    N5: TMenuItem;
    MudarodestinoparaoutraOP1: TMenuItem;
    QrTXCOPCab: TmySQLQuery;
    QrTXCOPCabCodigo: TIntegerField;
    QrTXCOPCabNome: TWideStringField;
    DsTXCOPCab: TDataSource;
    GBConfig: TGroupBox;
    Label6: TLabel;
    Label64: TLabel;
    EdControle_: TdmkEdit;
    EdTXCOPCab: TdmkEditCB;
    CBTXCOPCab: TdmkDBLookupComboBox;
    QrTXRRMCabTXCOPCab: TIntegerField;
    QrTXRRMCabNO_TXCOPCab: TWideStringField;
    SbTXCOPCabCad: TSpeedButton;
    SbTXCOPCabCpy: TSpeedButton;
    Adicionadesclassificado1: TMenuItem;
    QrTXRRMDesclDst: TmySQLQuery;
    QrTXRRMDesclDstCodigo: TLargeintField;
    QrTXRRMDesclDstControle: TLargeintField;
    QrTXRRMDesclDstMovimCod: TLargeintField;
    QrTXRRMDesclDstMovimNiv: TLargeintField;
    QrTXRRMDesclDstMovimTwn: TLargeintField;
    QrTXRRMDesclDstEmpresa: TLargeintField;
    QrTXRRMDesclDstTerceiro: TLargeintField;
    QrTXRRMDesclDstCliVenda: TLargeintField;
    QrTXRRMDesclDstMovimID: TLargeintField;
    QrTXRRMDesclDstDataHora: TDateTimeField;
    QrTXRRMDesclDstPallet: TLargeintField;
    QrTXRRMDesclDstGraGruX: TLargeintField;
    QrTXRRMDesclDstQtde: TFloatField;
    QrTXRRMDesclDstValorT: TFloatField;
    QrTXRRMDesclDstSrcMovID: TLargeintField;
    QrTXRRMDesclDstSrcNivel1: TLargeintField;
    QrTXRRMDesclDstSrcNivel2: TLargeintField;
    QrTXRRMDesclDstSrcGGX: TLargeintField;
    QrTXRRMDesclDstSdoVrtQtd: TFloatField;
    QrTXRRMDesclDstObserv: TWideStringField;
    QrTXRRMDesclDstSerieTal: TLargeintField;
    QrTXRRMDesclDstTalao: TLargeintField;
    QrTXRRMDesclDstFornecMO: TLargeintField;
    QrTXRRMDesclDstCustoMOUni: TFloatField;
    QrTXRRMDesclDstCustoMOTot: TFloatField;
    QrTXRRMDesclDstValorMP: TFloatField;
    QrTXRRMDesclDstDstMovID: TLargeintField;
    QrTXRRMDesclDstDstNivel1: TLargeintField;
    QrTXRRMDesclDstDstNivel2: TLargeintField;
    QrTXRRMDesclDstDstGGX: TLargeintField;
    QrTXRRMDesclDstQtdGer: TFloatField;
    QrTXRRMDesclDstQtdAnt: TFloatField;
    QrTXRRMDesclDstNO_PALLET: TWideStringField;
    QrTXRRMDesclDstNO_PRD_TAM_COR: TWideStringField;
    QrTXRRMDesclDstNO_TTW: TWideStringField;
    QrTXRRMDesclDstID_TTW: TLargeintField;
    QrTXRRMDesclDstNO_FORNECE: TWideStringField;
    QrTXRRMDesclDstNO_SerieTal: TWideStringField;
    QrTXRRMDesclDstReqMovEstq: TLargeintField;
    QrTXRRMDesclDstPedItsFin: TLargeintField;
    QrTXRRMDesclDstMarca: TWideStringField;
    QrTXRRMDesclDstStqCenLoc: TLargeintField;
    DsTXRRMDesclDst: TDataSource;
    QrTXRRMDesclBxa: TmySQLQuery;
    QrTXRRMDesclBxaCodigo: TLargeintField;
    QrTXRRMDesclBxaControle: TLargeintField;
    QrTXRRMDesclBxaMovimCod: TLargeintField;
    QrTXRRMDesclBxaMovimNiv: TLargeintField;
    QrTXRRMDesclBxaMovimTwn: TLargeintField;
    QrTXRRMDesclBxaEmpresa: TLargeintField;
    QrTXRRMDesclBxaTerceiro: TLargeintField;
    QrTXRRMDesclBxaCliVenda: TLargeintField;
    QrTXRRMDesclBxaMovimID: TLargeintField;
    QrTXRRMDesclBxaDataHora: TDateTimeField;
    QrTXRRMDesclBxaPallet: TLargeintField;
    QrTXRRMDesclBxaGraGruX: TLargeintField;
    QrTXRRMDesclBxaQtde: TFloatField;
    QrTXRRMDesclBxaValorT: TFloatField;
    QrTXRRMDesclBxaSrcMovID: TLargeintField;
    QrTXRRMDesclBxaSrcNivel1: TLargeintField;
    QrTXRRMDesclBxaSrcNivel2: TLargeintField;
    QrTXRRMDesclBxaSrcGGX: TLargeintField;
    QrTXRRMDesclBxaSdoVrtQtd: TFloatField;
    QrTXRRMDesclBxaObserv: TWideStringField;
    QrTXRRMDesclBxaSerieTal: TLargeintField;
    QrTXRRMDesclBxaTalao: TLargeintField;
    QrTXRRMDesclBxaFornecMO: TLargeintField;
    QrTXRRMDesclBxaCustoMOUni: TFloatField;
    QrTXRRMDesclBxaCustoMOTot: TFloatField;
    QrTXRRMDesclBxaValorMP: TFloatField;
    QrTXRRMDesclBxaDstMovID: TLargeintField;
    QrTXRRMDesclBxaDstNivel1: TLargeintField;
    QrTXRRMDesclBxaDstNivel2: TLargeintField;
    QrTXRRMDesclBxaDstGGX: TLargeintField;
    QrTXRRMDesclBxaQtdGer: TFloatField;
    QrTXRRMDesclBxaQtdAnt: TFloatField;
    QrTXRRMDesclBxaNO_PALLET: TWideStringField;
    QrTXRRMDesclBxaNO_PRD_TAM_COR: TWideStringField;
    QrTXRRMDesclBxaNO_TTW: TWideStringField;
    QrTXRRMDesclBxaID_TTW: TLargeintField;
    QrTXRRMDesclBxaNO_FORNECE: TWideStringField;
    QrTXRRMDesclBxaNO_SerieTal: TWideStringField;
    QrTXRRMDesclBxaReqMovEstq: TLargeintField;
    DsTXRRMDesclBxa: TDataSource;
    N6: TMenuItem;
    Removedesclassificado1: TMenuItem;
    Corrigetodasbaixas1: TMenuItem;
    TabSheet5: TTabSheet;
    DBGrid6: TDBGrid;
    PB1: TProgressBar;
    QrTXRRMInnCustoPQ: TFloatField;
    Label66: TLabel;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    Label67: TLabel;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    TabSheet6: TTabSheet;
    DBGrid7: TDBGrid;
    DBGIts: TDBGrid;
    N9: TMenuItem;
    CorrigeMO1: TMenuItem;
    AtrelamentoNFsdeMO2: TMenuItem;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    CobranadeMO1: TMenuItem;
    QrTXRRMOriIMEIDtCorrApo: TDateTimeField;
    QrTXRRMDstDtCorrApo: TDateTimeField;
    QrTXRRMDstCusFrtMORet: TFloatField;
    TsEnvioMO: TTabSheet;
    TsRetornoMO: TTabSheet;
    PnEnvioMO: TPanel;
    DBGTXMOEnvEnv: TdmkDBGridZTO;
    DBGTXMOEnvETmi: TdmkDBGridZTO;
    PnRetornoMO: TPanel;
    DBGTXMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter2: TSplitter;
    GroupBox8: TGroupBox;
    DBGTXMOEnvRTmi: TdmkDBGridZTO;
    GroupBox9: TGroupBox;
    DBGTXMOEnvGTmi: TdmkDBGridZTO;
    N10: TMenuItem;
    N11: TMenuItem;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    QrTXRRMInnCusFrtMOEnv: TFloatField;
    Editaquantidadedebaixa1: TMenuItem;
    BtInn: TBitBtn;
    PMInn: TPopupMenu;
    ItsIncluiInn: TMenuItem;
    ItsAlteraInn: TMenuItem;
    ItsExcluiInn: TMenuItem;
    MenuItem1: TMenuItem;
    AtualizaestoqueEmindustrializao1: TMenuItem;
    AlteraLocaldoestoque1: TMenuItem;
    AlteraoParcial1: TMenuItem;
    AlteraFornecedorMO1: TMenuItem;
    AlteraCliente1: TMenuItem;
    QrTXMOEnvEnv: TmySQLQuery;
    QrTXMOEnvEnvCodigo: TIntegerField;
    QrTXMOEnvEnvNFEMP_FatID: TIntegerField;
    QrTXMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrTXMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrTXMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrTXMOEnvEnvNFEMP_nItem: TIntegerField;
    QrTXMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrTXMOEnvEnvNFEMP_nNF: TIntegerField;
    QrTXMOEnvEnvNFEMP_Qtde: TFloatField;
    QrTXMOEnvEnvNFEMP_ValorT: TFloatField;
    QrTXMOEnvEnvCFTMP_FatID: TIntegerField;
    QrTXMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrTXMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrTXMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrTXMOEnvEnvCFTMP_nItem: TIntegerField;
    QrTXMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrTXMOEnvEnvCFTMP_nCT: TIntegerField;
    QrTXMOEnvEnvCFTMP_Qtde: TFloatField;
    QrTXMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrTXMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrTXMOEnvEnvCFTMP_ValorT: TFloatField;
    QrTXMOEnvEnvTXTMI_MovimCod: TIntegerField;
    QrTXMOEnvEnvLk: TIntegerField;
    QrTXMOEnvEnvDataCad: TDateField;
    QrTXMOEnvEnvDataAlt: TDateField;
    QrTXMOEnvEnvUserCad: TIntegerField;
    QrTXMOEnvEnvUserAlt: TIntegerField;
    QrTXMOEnvEnvAlterWeb: TSmallintField;
    QrTXMOEnvEnvAWServerID: TIntegerField;
    QrTXMOEnvEnvAWStatSinc: TSmallintField;
    QrTXMOEnvEnvAtivo: TSmallintField;
    DsTXMOEnvEnv: TDataSource;
    QrTXMOEnvETMI: TmySQLQuery;
    QrTXMOEnvETMICodigo: TIntegerField;
    QrTXMOEnvETMITXMOEnvEnv: TIntegerField;
    QrTXMOEnvETMITXMovIts: TIntegerField;
    QrTXMOEnvETMILk: TIntegerField;
    QrTXMOEnvETMIDataCad: TDateField;
    QrTXMOEnvETMIDataAlt: TDateField;
    QrTXMOEnvETMIUserCad: TIntegerField;
    QrTXMOEnvETMIUserAlt: TIntegerField;
    QrTXMOEnvETMIAlterWeb: TSmallintField;
    QrTXMOEnvETMIAWServerID: TIntegerField;
    QrTXMOEnvETMIAWStatSinc: TSmallintField;
    QrTXMOEnvETMIAtivo: TSmallintField;
    QrTXMOEnvETMIValorFrete: TFloatField;
    QrTXMOEnvETMIQtde: TFloatField;
    DsTXMOEnvETMI: TDataSource;
    QrTXMOEnvRet: TmySQLQuery;
    QrTXMOEnvRetCodigo: TIntegerField;
    QrTXMOEnvRetNFCMO_FatID: TIntegerField;
    QrTXMOEnvRetNFCMO_FatNum: TIntegerField;
    QrTXMOEnvRetNFCMO_Empresa: TIntegerField;
    QrTXMOEnvRetNFCMO_nItem: TIntegerField;
    QrTXMOEnvRetNFCMO_SerNF: TIntegerField;
    QrTXMOEnvRetNFCMO_nNF: TIntegerField;
    QrTXMOEnvRetNFCMO_Qtde: TFloatField;
    QrTXMOEnvRetNFCMO_CusMOM2: TFloatField;
    QrTXMOEnvRetNFCMO_CusMOKG: TFloatField;
    QrTXMOEnvRetNFCMO_ValorT: TFloatField;
    QrTXMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrTXMOEnvRetNFRMP_FatID: TIntegerField;
    QrTXMOEnvRetNFRMP_FatNum: TIntegerField;
    QrTXMOEnvRetNFRMP_Empresa: TIntegerField;
    QrTXMOEnvRetNFRMP_nItem: TIntegerField;
    QrTXMOEnvRetNFRMP_SerNF: TIntegerField;
    QrTXMOEnvRetNFRMP_nNF: TIntegerField;
    QrTXMOEnvRetNFRMP_Qtde: TFloatField;
    QrTXMOEnvRetNFRMP_ValorT: TFloatField;
    QrTXMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrTXMOEnvRetCFTPA_FatID: TIntegerField;
    QrTXMOEnvRetCFTPA_FatNum: TIntegerField;
    QrTXMOEnvRetCFTPA_Empresa: TIntegerField;
    QrTXMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrTXMOEnvRetCFTPA_nItem: TIntegerField;
    QrTXMOEnvRetCFTPA_SerCT: TIntegerField;
    QrTXMOEnvRetCFTPA_nCT: TIntegerField;
    QrTXMOEnvRetCFTPA_Qtde: TFloatField;
    QrTXMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrTXMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrTXMOEnvRetCFTPA_ValorT: TFloatField;
    DsTXMOEnvRet: TDataSource;
    QrTXMOEnvRTmi: TmySQLQuery;
    QrTXMOEnvRTmiCodigo: TIntegerField;
    QrTXMOEnvRTmiTXMOEnvRet: TIntegerField;
    QrTXMOEnvRTmiTXMOEnvEnv: TIntegerField;
    QrTXMOEnvRTmiQtde: TFloatField;
    QrTXMOEnvRTmiValorFrete: TFloatField;
    QrTXMOEnvRTmiLk: TIntegerField;
    QrTXMOEnvRTmiDataCad: TDateField;
    QrTXMOEnvRTmiDataAlt: TDateField;
    QrTXMOEnvRTmiUserCad: TIntegerField;
    QrTXMOEnvRTmiUserAlt: TIntegerField;
    QrTXMOEnvRTmiAlterWeb: TSmallintField;
    QrTXMOEnvRTmiAWServerID: TIntegerField;
    QrTXMOEnvRTmiAWStatSinc: TSmallintField;
    QrTXMOEnvRTmiAtivo: TSmallintField;
    QrTXMOEnvRTmiNFEMP_SerNF: TIntegerField;
    QrTXMOEnvRTmiNFEMP_nNF: TIntegerField;
    QrTXMOEnvRTmiValorT: TFloatField;
    DsTXMOEnvRTmi: TDataSource;
    QrTXMOEnvGTmi: TmySQLQuery;
    QrTXMOEnvGTmiCodigo: TIntegerField;
    QrTXMOEnvGTmiTXMOEnvRet: TIntegerField;
    QrTXMOEnvGTmiTXMovIts: TIntegerField;
    QrTXMOEnvGTmiQtde: TFloatField;
    QrTXMOEnvGTmiValorFrete: TFloatField;
    QrTXMOEnvGTmiLk: TIntegerField;
    QrTXMOEnvGTmiDataCad: TDateField;
    QrTXMOEnvGTmiDataAlt: TDateField;
    QrTXMOEnvGTmiUserCad: TIntegerField;
    QrTXMOEnvGTmiUserAlt: TIntegerField;
    QrTXMOEnvGTmiAlterWeb: TSmallintField;
    QrTXMOEnvGTmiAWServerID: TIntegerField;
    QrTXMOEnvGTmiAWStatSinc: TSmallintField;
    QrTXMOEnvGTmiAtivo: TSmallintField;
    QrTXMOEnvGTmiTXMovimCod: TIntegerField;
    DsTXMOEnvGTmi: TDataSource;
    PCInnDst: TPageControl;
    TsInn: TTabSheet;
    TsDst: TTabSheet;
    PCRRMDst: TPageControl;
    TabSheet3: TTabSheet;
    Panel12: TPanel;
    DGDadosDst: TDBGrid;
    Panel14: TPanel;
    PCRRMDestSub: TPageControl;
    TabSheet8: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet9: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    TabSheet4: TTabSheet;
    Panel13: TPanel;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    TabSheet7: TTabSheet;
    DBGrid8: TDBGrid;
    DBGrid9: TDBGrid;
    QrFluxoCab: TmySQLQuery;
    QrFluxoCabCodigo: TSmallintField;
    QrFluxoCabNome: TWideStringField;
    DsFluxoCab: TDataSource;
    GBEdita2: TGroupBox;
    Label3: TLabel;
    Label35: TLabel;
    Label49: TLabel;
    Label62: TLabel;
    LaPedItsLib: TLabel;
    Label54: TLabel;
    Label10: TLabel;
    EdFluxoCab: TdmkEditCB;
    CBFluxoCab: TdmkDBLookupComboBox;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdCustoMOTot: TdmkEdit;
    QrTXRRMCabClientMO: TIntegerField;
    QrTXRRMCabFornecMO: TIntegerField;
    QrTXRRMCabStqCenLoc: TIntegerField;
    QrTXRRMInnDtCorrApo: TDateTimeField;
    QrTXRRMCabFluxoCab: TIntegerField;
    QrTXRRMCabPeditsLib: TIntegerField;
    QrTXRRMCabQtdeInn: TFloatField;
    QrTXRRMCabValorTInn: TFloatField;
    QrTXRRMCabValorTDst: TFloatField;
    QrTXRRMCabCustoMOTot: TFloatField;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    Label12: TLabel;
    DBEdit27: TDBEdit;
    DBEdit6: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label14: TLabel;
    DBEdit31: TDBEdit;
    DBEdit8: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label16: TLabel;
    DBEdit35: TDBEdit;
    DBEdit10: TDBEdit;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label13: TLabel;
    Label15: TLabel;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label17: TLabel;
    DBEdit30: TDBEdit;
    DBEdit9: TDBEdit;
    QrTXRRMCabNO_FLUXOCAB: TWideStringField;
    QrTXRRMCabQtdeINI: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXRRMCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXRRMCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXRRMCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure ItsAlteraOriClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrTXRRMCabBeforeClose(DataSet: TDataSet);
    procedure QrTXRRMCabCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure IMEIArtigodeRibeiragerado1Click(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsAlteraDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure QrTXRRMDstBeforeClose(DataSet: TDataSet);
    procedure QrTXRRMDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure Parcial1Click(Sender: TObject);
    procedure otal1Click(Sender: TObject);
    procedure Parcial2Click(Sender: TObject);
    procedure otal2Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure CorrigirFornecedor1Click(Sender: TObject);
    procedure OrdensdeProduoemAberto1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure OPpreenchida1Click(Sender: TObject);
    procedure IrparacadastrodoPallet1Click(Sender: TObject);
    procedure MudarodestinoparaoutraOP1Click(Sender: TObject);
    procedure SbTXCOPCabCadClick(Sender: TObject);
    procedure SbTXCOPCabCpyClick(Sender: TObject);
    procedure Adicionadesclassificado1Click(Sender: TObject);
    procedure QrTXRRMDesclDstAfterScroll(DataSet: TDataSet);
    procedure Removedesclassificado1Click(Sender: TObject);
    procedure Corrigetodasbaixas1Click(Sender: TObject);
    procedure PMPesagemPopup(Sender: TObject);
    procedure Emiteoutrasbaixas1Click(Sender: TObject);
    procedure EmitePesagem2Click(Sender: TObject);
    procedure CorrigeMO1Click(Sender: TObject);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure CobranadeMO1Click(Sender: TObject);
    procedure QrTXMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrTXMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrTXMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure QrTXMOEnvRetBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
    procedure Editaquantidadedebaixa1Click(Sender: TObject);
    procedure BtInnClick(Sender: TObject);
    procedure AlteraLocaldoestoque1Click(Sender: TObject);
    procedure AlteraFornecedorMO1Click(Sender: TObject);
    procedure AlteraCliente1Click(Sender: TObject);
    procedure AtualizaestoqueEmindustrializao1Click(Sender: TObject);
    procedure PMInnPopup(Sender: TObject);
  private
    FItsExcluiOriIMEI_Enabled, FItsExcluiOriPallet_Enabled,
    FItsIncluiInn_Enabled, FItsIncluiOri_Enabled, FItsIncluiDst_Enabled,
    FAtualizando: Boolean;
    //
    function  CalculoCustoOrigemUni(): Double;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraTXRRMInn(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraTXRRMOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraTXRRMOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraTXRRMDst(SQLType: TSQLType);
    procedure MostraTXRRMDescl(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenTXRRMDesclDst(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure ReabreGGX();
    procedure RecalculaCusto();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure AtualizaNFeItens();
    procedure DistribuicaoCusto();
  end;

var
  FmTXRRMCab: TFmTXRRMCab;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnTX_PF, AppListas, UnGrade_PF,
  TXRRMOriPall, TXRRMOriIMEI, TXRRMDst, UnEntities, ModTX_CRC, TXRRMInn;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

const
  FGerArX2Env = True;
  FGerArX2Ret = False;

procedure TFmTXRRMCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXRRMCab.MostraTXRRMDescl(SQLType: TSQLType);
begin
//
end;

procedure TFmTXRRMCab.MostraTXRRMDst(SQLType: TSQLType);
begin
{POIU
  if not FItsIncluiDst_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmTXRRMDst, FmTXRRMDst, afmoNegarComAviso) then
  begin
    FmTXRRMDst.ImgTipo.SQLType := SQLType;
    FmTXRRMDst.FQrCab := QrTXRRMCab;
    FmTXRRMDst.FDsCab := DsTXRRMCab;
    FmTXRRMDst.FQrIts := QrTXRRMDst;
    //
    //FmTXRRMDst.FDataHora    := QrTXRRMCabDtHrAberto.Value;
    FmTXRRMDst.FEmpresa     := QrTXRRMCabEmpresa.Value;
    FmTXRRMDst.FClientMO    := QrTXRRMInnClientMO.Value;
    FmTXRRMDst.FValM2       := CalculoCustoOrigemUni();
    //FmTXRRMDst.FFatorIntSrc := QrTXRRMInnFatorInt.Value;
    FmTXRRMDst.FGraGruXSrc  := QrTXRRMInnGraGruX.Value;
    FmTXRRMDst.FFornecMO    := QrTXRRMInnFornecMO.Value;
    //
    FmTXRRMDst.EdCustoMOM2.ValueVariant := QrTXRRMInnCustoMOM2.Value;
    //
    if SQLType = stIns then
    begin
      FmTXRRMDst.FSdoPecas  := QrTXRRMInnSdoVrtPeca.Value;
      FmTXRRMDst.FSdoPesoKg := QrTXRRMInnSdoVrtPeso.Value;
      FmTXRRMDst.FSdoReaM2  := QrTXRRMInnSdoVrtArM2.Value;
      //
      //FmTXRRMDst.EdCPF1.ReadOnly := False
      TX_PF.ReopenPedItsXXX(FmTXRRMDst.QrTXPedIts, QrTXRRMInnControle.Value,
      QrTXRRMInnControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmTXRRMDst.EdPedItsFin.ValueVariant := QrTXRRMInnPedItsLib.Value;
      FmTXRRMDst.CBPedItsFin.KeyValue     := QrTXRRMInnPedItsLib.Value;
      //
      //FmTXRRMDst.TPData.Date              := QrTXRRMCabDtHrAberto.Value;
      //FmTXRRMDst.EdHora.ValueVariant      := QrTXRRMCabDtHrAberto.Value;
      //
      FmTXRRMDst.EdGragruX.ValueVariant    := QrTXRRMCabGGXDst.Value;
      FmTXRRMDst.CBGragruX.KeyValue        := QrTXRRMCabGGXDst.Value;
    end else
    begin
      SetLength(FmTXRRMDst.FPallOnEdit, 1);
      FmTXRRMDst.FPallOnEdit[0] := QrTXRRMDstPallet.Value;
      FmTXRRMDst.FSdoPecas  := 0;
      FmTXRRMDst.FSdoPesoKg := 0;
      FmTXRRMDst.FSdoReaM2  := 0;
      //
      TX_PF.ReopenPedItsXXX(FmTXRRMDst.QrTXPedIts, QrTXRRMInnControle.Value,
       QrTXRRMDstControle.Value);
      FmTXRRMDst.EdCtrl1.ValueVariant      := QrTXRRMDstControle.Value;
      //FmTXRRMDst.EdCtrl2.ValueVariant     := TX_PF.ObtemControleMovimTwin(
        //QrTXRRMDstMovimCod.Value, QrTXRRMDstMovimTwn.Value, emidEmReprRM,
        //eminEmRRMBxa);
      FmTXRRMDst.EdMovimTwn.ValueVariant     := QrTXRRMDstMovimTwn.Value;
      FmTXRRMDst.EdGragruX.ValueVariant      := QrTXRRMDstGraGruX.Value;
      FmTXRRMDst.CBGragruX.KeyValue          := QrTXRRMDstGraGruX.Value;
      FmTXRRMDst.EdPecas.ValueVariant        := QrTXRRMDstPecas.Value;
      FmTXRRMDst.EdPesoKg.ValueVariant       := QrTXRRMDstPesoKg.Value;
      FmTXRRMDst.EdAreaM2.ValueVariant       := QrTXRRMDstAreaM2.Value;
      FmTXRRMDst.EdAreaP2.ValueVariant       := QrTXRRMDstAreaP2.Value;
      FmTXRRMDst.EdValorT.ValueVariant       := QrTXRRMDstValorT.Value;
      FmTXRRMDst.EdObserv.ValueVariant       := QrTXRRMDstObserv.Value;
      FmTXRRMDst.EdSerieFch.ValueVariant     := QrTXRRMDstSerieFch.Value;
      FmTXRRMDst.CBSerieFch.KeyValue         := QrTXRRMDstSerieFch.Value;
      FmTXRRMDst.EdFicha.ValueVariant        := QrTXRRMDstFicha.Value;
      FmTXRRMDst.EdMarca.ValueVariant        := QrTXRRMDstMarca.Value;
      //FmTXRRMDst.RGMisturou.ItemIndex      := QrTXRRMDstMisturou.Value;
      FmTXRRMDst.EdPedItsFin.ValueVariant    := QrTXRRMDstPedItsFin.Value;
      FmTXRRMDst.CBPedItsFin.KeyValue        := QrTXRRMDstPedItsFin.Value;
      //
      //FmTXRRMDst.TPData.Date               := QrTXRRMDstDataHora.Value;
      //FmTXRRMDst.EdHora.ValueVariant       := QrTXRRMDstDataHora.Value;
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXRRMDstDataHora.Value,
        QrTXRRMDstDtCorrApo.Value,  FmTXRRMDst.TPData, FmTXRRMDst.EdHora);
      //
      FmTXRRMDst.EdPedItsFin.ValueVariant    := QrTXRRMDstPedItsFin.Value;
      FmTXRRMDst.CBPedItsFin.KeyValue        := QrTXRRMDstPedItsFin.Value;
      FmTXRRMDst.EdPallet.ValueVariant       := QrTXRRMDstPallet.Value;
      FmTXRRMDst.CBPallet.KeyValue           := QrTXRRMDstPallet.Value;
      FmTXRRMDst.EdStqCenLoc.ValueVariant    := QrTXRRMDstStqCenLoc.Value;
      FmTXRRMDst.CBStqCenLoc.KeyValue        := QrTXRRMDstStqCenLoc.Value;
      FmTXRRMDst.EdReqMovEstq.ValueVariant   := QrTXRRMDstReqMovEstq.Value;
      FmTXRRMDst.EdCustoMOPc.ValueVariant    := QrTXRRMDstCustoMOPc.Value;
      FmTXRRMDst.EdCustoMOKg.ValueVariant    := QrTXRRMDstCustoMOKg.Value;
      FmTXRRMDst.EdCustoMOM2.ValueVariant    := QrTXRRMDstCustoMOM2.Value;
      FmTXRRMDst.EdCustoMOTot.ValueVariant   := QrTXRRMDstCustoMOTot.Value;
      FmTXRRMDst.EdCusFrtMORet.ValueVariant  := QrTXRRMDstCusFrtMORet.Value;
      FmTXRRMDst.EdValorT.ValueVariant       := QrTXRRMDstValorT.Value;
      //FmTXRRMDst.EdRendimento.ValueVariant := QrTXRRMDstRendimento.Value;
      //
      if QrTXRRMDstSdoVrtPeca.Value < QrTXRRMDstPecas.Value then
      begin
        FmTXRRMDst.EdGraGruX.Enabled        := False;
        FmTXRRMDst.CBGraGruX.Enabled        := False;
      end;
      if QrTXRRMBxa.RecordCount > 0 then
      begin
        FmTXRRMDst.CkBaixa.Checked          := True;
        FmTXRRMDst.EdBxaPecas.ValueVariant  := -QrTXRRMBxaPecas.Value;
        FmTXRRMDst.EdBxaPesoKg.ValueVariant := -QrTXRRMBxaPesoKg.Value;
        FmTXRRMDst.EdBxaAreaM2.ValueVariant := -QrTXRRMBxaAreaM2.Value;
        FmTXRRMDst.EdBxaAreaP2.ValueVariant := -QrTXRRMBxaAreaP2.Value;
        FmTXRRMDst.EdBxaValorT.ValueVariant := -QrTXRRMBxaValorT.Value;
        FmTXRRMDst.EdCtrl2.ValueVariant     := QrTXRRMBxaControle.Value;
        FmTXRRMDst.EdBxaObserv.ValueVariant := QrTXRRMBxaObserv.Value;
      end;
      if QrTXRRMDstSdoVrtPeca.Value < QrTXRRMDstPecas.Value then
      begin
        FmTXRRMDst.EdGraGruX.Enabled  := False;
        FmTXRRMDst.CBGraGruX.Enabled  := False;
        FmTXRRMDst.EdSerieFch.Enabled := False;
        FmTXRRMDst.CBSerieFch.Enabled := False;
        FmTXRRMDst.EdFicha.Enabled    := False;
      end;
      FmTXRRMDst.CalculaCusto();
    end;
    //
    FmTXRRMDst.LaBxaPesoKg.Enabled  := QrTXRRMCabPesoKgINI.Value <> 0;
    FmTXRRMDst.EdBxaPesoKg.Enabled  := QrTXRRMCabPesoKgINI.Value <> 0;
    FmTXRRMDst.LaBxaAreaM2.Enabled  := QrTXRRMCabAreaINIM2.Value <> 0;
    FmTXRRMDst.EdBxaAreaM2.Enabled  := QrTXRRMCabAreaINIM2.Value <> 0;
    FmTXRRMDst.LaBxaAreaP2.Enabled  := QrTXRRMCabAreaINIM2.Value <> 0;
    FmTXRRMDst.EdBxaAreaP2.Enabled  := QrTXRRMCabAreaINIM2.Value <> 0;
    //
    FmTXRRMDst.ShowModal;
    FmTXRRMDst.Destroy;
    //
    TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
}
end;

procedure TFmTXRRMCab.MostraTXRRMInn(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  //if (not DesabilitaBaixa) then
    if (not FItsIncluiInn_Enabled) then
      if not DBCheck.LiberaPelaSenhaAdmin then
        Exit;
  if DBCheck.CriaFm(TFmTXRRMInn, FmTXRRMInn, afmoNegarComAviso) then
  begin
    FmTXRRMInn.ImgTipo.SQLType := SQLType;
    FmTXRRMInn.FQrCab    := QrTXRRMCab;
    FmTXRRMInn.FDsCab    := DsTXRRMCab;
    FmTXRRMInn.FQrIts    := QrTXRRMInn;
    FmTXRRMInn.FEmpresa  := QrTXRRMCabEmpresa.Value;
    FmTXRRMInn.FClientMO := QrTXRRMCabClientMO.Value;
    FmTXRRMInn.FFornecMO := QrTXRRMCabFornecMO.Value;
    FmTXRRMInn.FValM2    := CalculoCustoOrigemUni();
    FmTXRRMInn.EdStqCenLoc.ValueVariant  := QrTXRRMCabStqCenLoc.Value;
    FmTXRRMInn.CBStqCenLoc.KeyValue      := QrTXRRMCabStqCenLoc.Value;
    if SQLType = stIns then
    begin
(*
      TX_PF.ReopenPedItsXXX(FmTXRRMInn.QrTXPedIts, QrTXRRM???Controle.Value,
      QrTXRRM???Controle.Value);
      // Sugerir o Lib do Novo gerado!
      FmTXRRMInn.EdPedItsFin.ValueVariant  := QrTXRRM???PedItsLib.Value;
      FmTXRRMInn.CBPedItsFin.KeyValue      := QrTXRRM???PedItsLib.Value;
*)
      //
      FmTXRRMInn.TPData.Date               := QrTXRRMCabDtHrAberto.Value;
      FmTXRRMInn.EdHora.ValueVariant       := QrTXRRMCabDtHrAberto.Value;
    end else
    begin
      TX_PF.ReopenPedItsXXX(FmTXRRMInn.QrTXPedIts, QrTXRRMInnControle.Value,
       QrTXRRMInnControle.Value);
      FmTXRRMInn.EdControle.ValueVariant   := QrTXRRMInnControle.Value;
      FmTXRRMInn.EdMovimTwn.ValueVariant   := QrTXRRMInnMovimTwn.Value;
      FmTXRRMInn.EdGragruX.ValueVariant    := QrTXRRMInnGraGruX.Value;
      FmTXRRMInn.CBGragruX.KeyValue        := QrTXRRMInnGraGruX.Value;
      FmTXRRMInn.EdQtde.ValueVariant       := QrTXRRMInnQtde.Value;
      // Cuidado!!! Apenas informativo. Calcula depois na distribui��o de custo!!
      FmTXRRMInn.EdValorMP.ValueVariant     := QrTXRRMInnValorMP.Value;
      FmTXRRMInn.EdCustoMOUni.ValueVariant  := QrTXRRMInnCustoMOUni.Value;
      FmTXRRMInn.EdCustoMOTot.ValueVariant  := QrTXRRMInnCustoMOTot.Value;
      FmTXRRMInn.EdValorT.ValueVariant      := QrTXRRMInnValorT.Value;
      //
      FmTXRRMInn.EdObserv.ValueVariant     := QrTXRRMInnObserv.Value;
      FmTXRRMInn.EdMarca.ValueVariant      := QrTXRRMInnMarca.Value;
      FmTXRRMInn.EdSerieTal.ValueVariant   := QrTXRRMInnSerieTal.Value;
      FmTXRRMInn.CBSerieTal.KeyValue       := QrTXRRMInnSerieTal.Value;
      FmTXRRMInn.EdTalao.ValueVariant      := QrTXRRMInnTalao.Value;
      (*
      FmTXRRMInn.EdPedItsFin.ValueVariant  := QrTXRRMInnPedItsFin.Value;
      FmTXRRMInn.CBPedItsFin.KeyValue      := QrTXRRMInnPedItsFin.Value;
      *)
      FmTXRRMInn.EdPallet.ValueVariant     := QrTXRRMInnPallet.Value;
      FmTXRRMInn.CBPallet.KeyValue         := QrTXRRMInnPallet.Value;
      FmTXRRMInn.EdReqMovEstq.ValueVariant := QrTXRRMInnReqMovEstq.Value;
      //
      FmTXRRMInn.EdCustoMOUni.ValueVariant := QrTXRRMInnCustoMOUni.Value;
      FmTXRRMInn.EdCustoMOTot.ValueVariant := QrTXRRMInnCustoMOTot.Value;
      //FmTXRRMInn.EdCusFrtMORet.ValueVariant := QrTXRRMInnCusFrtMORet.Value;
      // Cuidado!!!
      FmTXRRMInn.EdValorT.ValueVariant    := QrTXRRMInnValorT.Value;
      //
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXRRMInnDataHora.Value,
        QrTXRRMInnDtCorrApo.Value,  FmTXRRMInn.TPData, FmTXRRMInn.EdHora);
      //
      if QrTXRRMInnSdoVrtQtd.Value < QrTXRRMInnQtde.Value then
      begin
        FmTXRRMInn.EdGraGruX.Enabled        := False;
        FmTXRRMInn.CBGraGruX.Enabled        := False;
      end;
    end;
    //
    //
    FmTXRRMInn.ShowModal;
    FmTXRRMInn.Destroy;
    TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
    //CorrigeFornecedorePalletsDestino(True);
    DistribuicaoCusto();
  end;
end;

procedure TFmTXRRMCab.MostraTXRRMOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmTXRRMOriIMEI, FmTXRRMOriIMEI, afmoNegarComAviso) then
  begin
    FmTXRRMOriIMEI.ImgTipo.SQLType := SQLType;
    FmTXRRMOriIMEI.FQrCab          := QrTXRRMCab;
    FmTXRRMOriIMEI.FDsCab          := DsTXRRMCab;
    //FmTXRRMOriIMEI.FTXRRMOriIMEIIMEI   := QrTXRRMOriIMEIIMEI;
    //FmTXRRMOriIMEI.FTXRRMOriIMEIIMEIet := QrTXRRMOriIMEIIMEIet;
    FmTXRRMOriIMEI.FEmpresa        := QrTXRRMCabEmpresa.Value;
    //FmTXRRMOriIMEI.FStqCenLoc      := QrTXRRMInnStqCenLoc.Value;
    FmTXRRMOriIMEI.FStqCenLoc      := QrTXRRMCabStqCenLoc.Value;
    //FmTXRRMOriIMEI.FFornecMO       := QrTXRRMInnFornecMO.Value;
    FmTXRRMOriIMEI.FFornecMO       := QrTXRRMCabFornecMO.Value;
    //
    FmTXRRMOriIMEI.EdCodigo.ValueVariant    := QrTXRRMCabCodigo.Value;
    FmTXRRMOriIMEI.EdMovimCod.ValueVariant  := QrTXRRMCabMovimCod.Value;
    FmTXRRMOriIMEI.EdSrcMovID.ValueVariant  := QrTXRRMInnMovimID.Value;
    FmTXRRMOriIMEI.EdSrcNivel1.ValueVariant := QrTXRRMInnCodigo.Value;
    FmTXRRMOriIMEI.EdSrcNivel2.ValueVariant := QrTXRRMInnControle.Value;
    FmTXRRMOriIMEI.EdSrcGGX.ValueVariant    := QrTXRRMInnGraGruX.Value;
    //
    FmTXRRMOriIMEI.ReopenItensAptos();
    //
    if SQLType = stIns then
    begin
      FmTXRRMOriIMEI.FDataHora                := QrTXRRMCabDtHrAberto.Value;
    end else
    begin
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXRRMOriIMEIDataHora.Value,
        QrTXRRMOriIMEIDtCorrApo.Value,  FmTXRRMOriIMEI.FDataHora);
(*
      FmTXRRMOriIMEI.EdControle.ValueVariant := QrTXRRMCabOldControle.Value;
      //
      FmTXRRMOriIMEI.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrTXRRMCabOldCNPJ_CPF.Value);
      FmTXRRMOriIMEI.EdNomeEmiSac.Text := QrTXRRMCabOldNome.Value;
      FmTXRRMOriIMEI.EdCPF1.ReadOnly := True;
*)
    end;
    case Quanto of
      ptParcial:
      begin
        FmTXRRMOriIMEI.FParcial := True;
        FmTXRRMOriIMEI.DBG04Estq.Options := FmTXRRMOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmTXRRMOriIMEI.FParcial := False;
        FmTXRRMOriIMEI.DBG04Estq.Options := FmTXRRMOriIMEI.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmTXRRMOriIMEI.FParcial := False;
        FmTXRRMOriIMEI.DBG04Estq.Options := FmTXRRMOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    // Deve ser o mesmo
    FmTXRRMOriIMEI.EdGraGruX.ValueVariant := QrTXRRMInnGraGruX.Value;
    FmTXRRMOriIMEI.CBGraGruX.KeyValue     := QrTXRRMInnGraGruX.Value;
    FmTXRRMOriIMEI.ReopenGraGruX();
    FmTXRRMOriIMEI.ReopenItensAptos();
    //
    FmTXRRMOriIMEI.ShowModal;
    FmTXRRMOriIMEI.Destroy;
    RecalculaCusto();
    TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmTXRRMCab.MostraTXRRMOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
{POIU
  if not FItsIncluiOri_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmTXRRMOriPall, FmTXRRMOriPall, afmoNegarComAviso) then
  begin
    FmTXRRMOriPall.ImgTipo.SQLType := SQLType;
    FmTXRRMOriPall.FQrCab          := QrTXRRMCab;
    FmTXRRMOriPall.FDsCab          := DsTXRRMCab;
    //FmTXRRMOriPall.FTXRRMOriPallIMEI   := QrTXRRMOriPallIMEI;
    //FmTXRRMOriPall.FTXRRMOriPallPallet := QrTXRRMOriPallPallet;
    FmTXRRMOriPall.FEmpresa        := QrTXRRMCabEmpresa.Value;
    FmTXRRMOriPall.FClientMO       := QrTXRRMInnClientMO.Value;
    FmTXRRMOriPall.FTipoArea       := QrTXRRMCabTipoArea.Value;
    FmTXRRMOriPall.FOrigMovimNiv   := QrTXRRMInnMovimNiv.Value;
    FmTXRRMOriPall.FOrigMovimCod   := QrTXRRMInnMovimCod.Value;
    FmTXRRMOriPall.FOrigCodigo     := QrTXRRMInnCodigo.Value;
    FmTXRRMOriPall.FNewGraGruX     := QrTXRRMInnGraGruX.Value;
    FmTXRRMOriPall.FStqCenLoc      := QrTXRRMInnStqCenLoc.Value;
    FmTXRRMOriPall.FFornecMO       := QrTXRRMInnFornecMO.Value;
    //
    FmTXRRMOriPall.EdCodigo.ValueVariant    := QrTXRRMCabCodigo.Value;
    FmTXRRMOriPall.EdMovimCod.ValueVariant  := QrTXRRMCabMovimCod.Value;
    FmTXRRMOriPall.EdSrcMovID.ValueVariant  := QrTXRRMInnMovimID.Value;
    FmTXRRMOriPall.EdSrcNivel1.ValueVariant := QrTXRRMInnCodigo.Value;
    FmTXRRMOriPall.EdSrcNivel2.ValueVariant := QrTXRRMInnControle.Value;
    FmTXRRMOriPall.EdSrcGGX.ValueVariant    := QrTXRRMInnGraGruX.Value;
    //
    FmTXRRMOriPall.ReopenItensAptos();
    FmTXRRMOriPall.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmTXRRMOriPall.FDataHora                := QrTXRRMCabDtHrAberto.Value;
    end else
    begin
      TX_PF.DefineDataHoraOuDtCorrApoCompos(QrTXRRMOriIMEIDataHora.Value,
        QrTXRRMOriIMEIDtCorrApo.Value,  FmTXRRMOriPall.FDataHora);
(*
      FmTXRRMOriPall.EdControle.ValueVariant := QrTXRRMCabOldControle.Value;
      //
      FmTXRRMOriPall.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrTXRRMCabOldCNPJ_CPF.Value);
      FmTXRRMOriPall.EdNomeEmiSac.Text := QrTXRRMCabOldNome.Value;
      FmTXRRMOriPall.EdCPF1.ReadOnly := True;
*)
    end;
    case Quanto of
      ptParcial:
      begin
        FmTXRRMOriPall.FParcial := True;
        FmTXRRMOriPall.DBG04Estq.Options := FmTXRRMOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmTXRRMOriPall.FParcial := False;
        FmTXRRMOriPall.DBG04Estq.Options := FmTXRRMOriPall.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmTXRRMOriPall.FParcial := False;
        FmTXRRMOriPall.DBG04Estq.Options := FmTXRRMOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    // Deve ser o mesmo!
    FmTXRRMOriPall.EdGraGruX.ValueVariant := QrTXRRMInnGraGruX.Value;
    FmTXRRMOriPall.CBGraGruX.KeyValue     := QrTXRRMInnGraGruX.Value;
    FmTXRRMOriPall.ReopenGraGruX();
    FmTXRRMOriPall.ReopenItensAptos();
    //
    FmTXRRMOriPall.ShowModal;
    FmTXRRMOriPall.Destroy;
    RecalculaCusto();
    TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
}
end;

procedure TFmTXRRMCab.MudarodestinoparaoutraOP1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de OP';
  Prompt = 'Informe o n�mero da OP';
  Campo  = 'Descricao';
var
  //Terceiro, Controle, ThisCtrl: Integer;
  OP, Codigo, MovimCod, Controle, Ctrl1: Integer;
  Resp: Variant;
  Qry: TmySQLQuery;
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT cab.Codigo, cab.Nome Descricao ',
    'FROM txrrmcab cab ',
    'ORDER BY Descricao ',
    ''], Dmod.MyDB, True);
  if Resp <> Null then
  begin
    OP := Resp;
    if OP <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * FROM txrrmcab ',
        'WHERE Codigo=' + Geral.FF0(OP),
        '']);
        if Qry.recordCount > 0 then
        begin
          Codigo   := Qry.FieldByName('Codigo').AsInteger;
          MovimCod := Qry.FieldByName('MovimCod').AsInteger;
          //
          Controle := QrTXRRMBxaControle.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
          'Codigo', 'MovimCod'], [
          'Controle'], [
          Codigo, MovimCod], [
          Controle], True) then
          begin
            Ctrl1 := Controle;
            Controle := QrTXRRMDstControle.Value;
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
            'Codigo', 'MovimCod'], [
            'Controle'], [
            Codigo, MovimCod], [
            Controle], True) then
            begin
              TX_PF.AtualizaTotaisTXRRMCab(MovimCod);
              TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
              //TX_PF.AtualizaNF_IMEI(QrTXRRMCabMovimCod.Value, Ctrl1);
              //TX_PF.AtualizaNF_IMEI(QrTXRRMCabMovimCod.Value, Controle);
              //
              LocCod(QrTXRRMCabMovimCod.Value, QrTXRRMCabMovimCod.Value);
            end;
          end;
        end;
      finally
        Qry.Free;
      end;
    end;
  end;
end;

procedure TFmTXRRMCab.OPpreenchida1Click(Sender: TObject);
var
  NFeRem: String;
begin
{POIU
  if QrTXRRMCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrTXRRMCabNFeRem.Value)
  else
    NFeRem := '';
  TX_PF.ImprimeIMEI([QrTXRRMInnControle.Value], viikOPPreenchida, QrTXRRMCabLPFMO.Value,
    NFeRem, QrTXRRMCab);
}
end;

procedure TFmTXRRMCab.OrdensdeProduoemAberto1Click(Sender: TObject);
begin
{POIU
  TX_PF.ImprimeOXsAbertas(0, 1, 2, 2, [TEstqMovimID.emidEmReprRM], True, False, '', 0);
}
end;

procedure TFmTXRRMCab.otal1Click(Sender: TObject);
begin
  MostraTXRRMOriPall(stIns, ptTotal);
end;

procedure TFmTXRRMCab.otal2Click(Sender: TObject);
begin
  MostraTXRRMOriIMEI(stIns, ptTotal);
end;

procedure TFmTXRRMCab.Outrasimpresses1Click(Sender: TObject);
begin
{POIU
  TX_Jan.MostraFormTXMovImp(False, nil, nil, -1);
}
end;

procedure TFmTXRRMCab.Parcial1Click(Sender: TObject);
begin
  MostraTXRRMOriPall(stIns, ptParcial);
end;

procedure TFmTXRRMCab.Parcial2Click(Sender: TObject);
begin
  MostraTXRRMOriIMEI(stIns, ptParcial);
end;

procedure TFmTXRRMCab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXRRMCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXRRMCab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := TX_PF.LocalizaPeloIMEC(emidEmReprRM);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmTXRRMCab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := TX_PF.LocalizaPeloIMEI(emidEmReprRM);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmTXRRMCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXRRMCab);
  MyObjects.HabilitaMenuItemCabDelC1I3(CabExclui1, QrTXRRMCab, QrTXRRMOriIMEI,
    QrTXRRMOriPallet, QrTXRRMDst);
(*
  MyObjects.HabilitaMenuItemCabUpd(CabLibera1, QrTXRRMCab);
  MyObjects.HabilitaMenuItemCabUpd(CabReeditar1, QrTXRRMCab);
  if QrTXRRMCabDtHrFimOpe.Value > 2 then
  begin
    CabLibera1.Enabled := False;
    CabReeditar1.Enabled := False;
  end else
  if QrTXRRMCabDtHrLibOpe.Value < 2 then
  begin
    CabReeditar1.Enabled := False;
  end;
*)
  //
  //TX_PF.HabilitaMenuItensTXAberto(QrTXRRMCab, QrTXRRMCabDtHrAberto.Value, [CabAltera1, CabExclui1]);
end;

procedure TFmTXRRMCab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrTXRRMCab);
  FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrTXRRMDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrTXRRMDst);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrTXRRMDst);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiDst, QrTXRRMDst);

  MyObjects.HabilitaMenuItemItsIns(Adicionadesclassificado1, QrTXRRMCab);
  MyObjects.HabilitaMenuItemItsDel(Removedesclassificado1, QrTXRRMDesclDst);

(* Movidos para os click dos itens de menu!!!
  if (QrTXRRMCabDtHrLibOpe.Value > 2) or (QrTXRRMCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    ItsAlteraDst.Enabled := False;
    ItsExcluiDst.Enabled := False;
  end;
*)
  TX_PF.HabilitaComposTXAtivo(QrTXRRMDstID_TTW.Value, [ItsExcluiDst]);
  MyObjects.HabilitaMenuItemItsUpd(Editaquantidadedebaixa1, QrTXRRMBxa);
  Corrigetodasbaixas1.Enabled :=
    (QrTXRRMCab.State <> dsInactive) and (QrTXRRMCabQtdeSdo.Value < 0);
 //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrTXRRMDst);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrTXMOEnvRet);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrTXMOEnvRet);
  //
  if (QrTXRRMCabDtHrLibOpe.Value > 2) or (QrTXRRMCabDtHrFimOpe.Value > 2) then
    ItsIncluiDst.Enabled := False;
  if FItsIncluiDst_Enabled then
  begin
    FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
    ItsIncluiDst.Enabled  := True;
  end;
end;

procedure TFmTXRRMCab.PMInnPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiInn, QrTXRRMCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraInn, QrTXRRMInn);
  FItsIncluiInn_Enabled := ItsIncluiInn.Enabled;
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiInn, QrTXRRMInn);
  if (QrTXRRMCabDtHrLibOpe.Value > 2) or (QrTXRRMCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiInn.Enabled := False;
    ItsExcluiInn.Enabled := False;
  end;
{POIU
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME3, QrTXRRMInn);
}
  //
  TX_PF.HabilitaComposTXAtivo(QrTXRRMInnID_TTW.Value, [ItsExcluiInn]);
  if FItsIncluiInn_Enabled then
  begin
    FItsIncluiInn_Enabled := ItsIncluiInn.Enabled;
    ItsIncluiInn.Enabled  := True;
  end;
  //
  AlteraLocaldoestoque1.Enabled := ItsAlteraInn.Enabled;
end;

procedure TFmTXRRMCab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrTXRRMCab);
  FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrTXRRMOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrTXRRMOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrTXRRMOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrTXRRMOriPallet);
  if (QrTXRRMCabDtHrLibOpe.Value > 2) or (QrTXRRMCabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCRRMOri.ActivePageIndex = 1);
  //
  TX_PF.HabilitaComposTXAtivo(QrTXRRMOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  TX_PF.HabilitaComposTXAtivo(QrTXRRMOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  //
  FItsExcluiOriIMEI_Enabled   := ItsExcluiOriIMEI.Enabled;
  ItsExcluiOriIMEI.Enabled    := True;
  FItsExcluiOriPallet_Enabled := ItsExcluiOriPallet.Enabled;
  ItsExcluiOriPallet.Enabled  := True;
  case PCRRMOri.ActivePageIndex of
    0: ItsExcluiOriIMEI.Enabled := False;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
  if FItsIncluiOri_Enabled then
  begin
    FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
    ItsIncluiOri.Enabled  := True;
  end;
end;

procedure TFmTXRRMCab.PMPesagemPopup(Sender: TObject);
begin
{
  MyObjects.HabilitaMenuItemItsIns(EmitePesagem2, QrTXRRMCab);
  MyObjects.HabilitaMenuItemItsIns(Emiteoutrasbaixas1, QrTXRRMCab);
  MyObjects.HabilitaMenuItemItsIns(Novoitem1, QrPQO);
  MyObjects.HabilitaMenuItemItsUpd(Reimprimereceita1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiPesagem1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrPQOIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiGrupo1, QrPQO);
}
end;

procedure TFmTXRRMCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXRRMCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXRRMCab.DefParams;
begin
  VAR_GOTOTABELA := 'txrrmcab';
  VAR_GOTOMYSQLTABLE := QrTXRRMCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  //VAR_SQLx.Add('SELECT vcc.Nome NO_TXCOPCab, ');
  VAR_SQLx.Add('SELECT "" NO_TXCOPCab, ');
  VAR_SQLx.Add('IF(QtdeMan<>0, QtdeMan, -QtdeSrc) QtdeINI, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  VAR_SQLx.Add('flu.Nome NO_FLUXOCAB, voc.* ');
  VAR_SQLx.Add('FROM txrrmcab voc');
  VAR_SQLx.Add('LEFT JOIN fluxocab flu ON flu.Codigo=voc.FluxoCab ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente');
  //VAR_SQLx.Add('LEFT JOIN txcopcab  vcc ON vcc.Codigo=voc.TXCOPCab');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmTXRRMCab.DistribuicaoCusto();
begin
  DmModTX_CRC.DistribuicaoCusto(emidEmReprRM, eminSorcRRM, eminEmRRMInn, eminDestRRM);
end;

procedure TFmTXRRMCab.Editaquantidadedebaixa1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  PCInnDst.ActivePageIndex := 1;
  PCRRMDst.ActivePageIndex := 0;
  PCRRMDestSub.ActivePageIndex := 0;
  //
  if TX_PF.AlteraTMI_Qtde(QrTXRRMBxaMovimID.Value,
  QrTXRRMBxaMovimNiv.Value, QrTXRRMBxaControle.Value,
  QrTXRRMBxaQtde.Value, siNegativo) then
  begin
    TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(QrTXRRMInnControle.Value,
    QrTXRRMInnMovimID.Value, QrTXRRMInnMovimNiv.Value);
    LocCod(QrTXRRMCabCodigo.Value, QrTXRRMCabCodigo.Value);
  end;
end;

procedure TFmTXRRMCab.Emiteoutrasbaixas1Click(Sender: TObject);
begin
  PCRRMOri.ActivePageIndex := 3;
end;

procedure TFmTXRRMCab.EmitePesagem2Click(Sender: TObject);
begin
  PCRRMOri.ActivePageIndex := 2;
end;

procedure TFmTXRRMCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCOpeOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsEnvioMO;
  //
  DmModTX_CRC.ExcluiAtrelamentoMOEnvEnv(QrTXMOEnvEnv, QrTXMOEnvETMI);
  //
  TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
  //
  LocCod(QrTXRRMCabCodigo.Value,QrTXRRMCabCodigo.Value);
  //
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXRRMCab.ExcluiAtrelamento2Click(Sender: TObject);
begin
{POIU
  DmModTX_CRC.ExcluiAtrelamentoMOEnvRet(QrTXMOEnvRet, QrTXMOEnvGTMI, QrTXMOEnvRTMI);
  //
  TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
  //
  LocCod(QrTXRRMCabCodigo.Value, QrTXRRMCabCodigo.Value);
  //
}end;

function TFmTXRRMCab.CalculoCustoOrigemUni(): Double;
begin
  Result := TX_PF.CalculoValorOrigemUni(QrTXRRMInnQtde.Value,
    (*QrTXRRMInnCustoPQ.Value*)0, QrTXRRMInnValorMP.Value,
    QrTXRRMInnCusFrtMOEnv.Value);
end;

procedure TFmTXRRMCab.AlteraCliente1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrTXRRMCabCodigo.Value;
  if TX_PF.AlteraTMI_CliVenda(QrTXRRMInnControle.Value,
  QrTXRRMInnCliVenda.Value) then
  begin
    LocCod(Codigo, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txrrmcab', False, [
    'Cliente'], [
    'Codigo'], [
    QrTXRRMInnCliVenda.Value], [
    Codigo], True) then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXRRMCab.CobranadeMO1Click(Sender: TObject);
begin
{POIU
  TX_Jan.MostraFormTXImpMOEnvRet();
}
end;

procedure TFmTXRRMCab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
{POIU
  IMEIAtu     := QrTXRRMInnControle.Value;
  MovimCod    := QrTXRRMCabMovimCod.Value;
  Codigo      := QrTXRRMCabCodigo.Value;
  MovimNivSrc := eminSorcRRM;
  MovimNivDst := eminDestRRM;
  TX_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
}
end;

procedure TFmTXRRMCab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmTXRRMCab.CorrigeMO1Click(Sender: TObject);
begin
  //TX_Jan.MostraFormTXMOPWEGer(); Fazer????
end;

procedure TFmTXRRMCab.Corrigetodasbaixas1Click(Sender: TObject);
var
  Controle: Integer;
  Fator, Qtde, Soma: Double;
begin
  if QrTXRRMCabQtdeINI.Value > 0 then
  begin
    Soma  := 0;
    Fator := QrTXRRMCabQtdeDst.Value / QrTXRRMCabQtdeINI.Value;
    QrTXRRMDst.First;
    while not QrTXRRMDst.Eof do
    begin
      Controle := QrTXRRMBxaControle.Value;
      if QrTXRRMDst.RecNo = QrTXRRMDst.RecordCount then
        Qtde := - (QrTXRRMCabQtdeINI.Value + Soma)
      else
        Qtde := QrTXRRMBxaQtde.Value * Fator;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Qtde'], ['Controle'], [Qtde], [Controle], True) then ;
        //TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(Controle, MovimID, MovimNiv);
      //
      Soma := Soma + Qtde;
      //
      QrTXRRMDst.Next;
    end;
    TX_PF.AtualizaSaldoVirtualTXMovIts_Generico(QrTXRRMInnControle.Value,
    QrTXRRMInnMovimID.Value, QrTXRRMInnMovimNiv.Value);
    LocCod(QrTXRRMCabCodigo.Value, QrTXRRMCabCodigo.Value);
  end else
    Geral.MB_Aviso('Quantidade inicial inv�lida!');
end;

procedure TFmTXRRMCab.CorrigirFornecedor1Click(Sender: TObject);
begin
  TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  LocCod(QrTXRRMCabCodigo.Value, QrTXRRMCabCodigo.Value);
end;

procedure TFmTXRRMCab.IMEIArtigodeRibeiragerado1Click(Sender: TObject);
var
  NFeRem: String;
begin
  if QrTXRRMCabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrTXRRMCabNFeRem.Value)
  else
    NFeRem := '';
  TX_PF.ImprimeIMEI([QrTXRRMInnControle.Value], viikOrdemOperacao,
    QrTXRRMCabLPFMO.Value, NFeRem, QrTXRRMCab);
end;

procedure TFmTXRRMCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCOpeOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsEnvioMO;
  //
  TX_Jan.MostraFormTXMOEnvEnv(stIns, QrTXRRMOriIMEI, QrTXMOEnvEnv, QrTXMOEnvETMI,
  siNegativo, QrTXRRMCabSerieRem.Value, QrTXRRMCabNFeRem.Value);
  //
  LocCod(QrTXRRMCabCodigo.Value,QrTXRRMCabCodigo.Value);
  //
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXRRMCab.IncluiAtrelamento2Click(Sender: TObject);
var
  MovimCod: Integer;
begin
{$IfDef sAllTX}
  //PCRRMOri.ActivePageIndex := 2;
{POIU
  PCRRMOri.ActivePage := TsRetornoMO;
  //
  MovimCod := QrTXRRMCabMovimCod.Value;
  //
  TX_Jan.MostraFormTXMOEnvRet(stIns, QrTXRRMInn, QrTXRRMDst, QrTXRRMBxa,
  QrTXMOEnvRet, QrTXMOEnvGTMI, QrTXMOEnvRTMI, MovimCod, 0, 0, siPositivo,
  FGerArX2Ret);
  //
  LocCod(QrTXRRMCabCodigo.Value,QrTXRRMCabCodigo.Value);
  //
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXRRMCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  TX_PF.InfoReqMovEstq(QrTXRRMOriIMEIControle.Value,
    QrTXRRMOriIMEIReqMovEstq.Value, QrTXRRMOriIMEI);
end;

procedure TFmTXRRMCab.InformaNmerodaRME2Click(Sender: TObject);
begin
  TX_PF.InfoReqMovEstq(QrTXRRMDstControle.Value,
    QrTXRRMDstReqMovEstq.Value, QrTXRRMDst);
end;

{
procedure TFmTXRRMCab.InsereArtigoEmProcesso(Codigo, MovimCod, Empresa, ClientMO,
GraGruX, CliVenda: Integer; DataHora: String);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  //CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  //CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  TXMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle, FornecMO, PedItsLib, StqCenLoc, ReqMovEstq: Integer;
  CustoMOUni, CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmReprRM;
  MovimNiv       := eminEmRRMInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  CustoMOUni     := EdCustoMOUni.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if TX_PF.TXFic(GraGruX, Empresa, Fornecedor, Pallet, Qtde, ValorT, EdGraGruX,
    EdPallet, EdQtde, EdValorT, ExigeFornecedor, CO_GraGruY_???,
    EdStqCenLoc, SerieTal, Talao, EdSerieTal, EdTalao) then
    Exit;
*)
  //
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
  CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX,
  Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei087(*Gera��o de couro que est� em reprocesso/reparo*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //TX_PF.AtualizaTotaisTXXxxCab('txrrmcab', MovimCod);
    TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
    if GBTXPedIts.Visible then
      TX_PF.AtualizaTXPedIts_Lib(PedItsLib, Controle,
      Pecas, PesoKg, AreaM2, AreaP2);
  end;
end;
}

procedure TFmTXRRMCab.IrparacadastrodoPallet1Click(Sender: TObject);
begin
  case PCRRMOri.ActivePageIndex of
    0: TX_Jan.MostraFormTXPallet(QrTXRRMOriPalletPallet.Value);
    1: TX_Jan.MostraFormTXPallet(QrTXRRMOriIMEIPallet.Value);
    else Geral.MB_Erro(
      '"PCRRMOri.ActivePageIndex" n�o implementado em "FmTXRRMCab.IrparacadastrodoPallet"');
  end;
end;

procedure TFmTXRRMCab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXPallet(QrTXRRMDstPallet.Value);
end;

procedure TFmTXRRMCab.ItsAlteraDstClick(Sender: TObject);
begin
  if (QrTXRRMCabDtHrLibOpe.Value > 2) or (QrTXRRMCabDtHrFimOpe.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
  MostraTXRRMDst(stUpd);
end;

procedure TFmTXRRMCab.ItsAlteraOriClick(Sender: TObject);
begin
  //MostraTXGArtOri(stUpd);
end;

procedure TFmTXRRMCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{POIU
  Codigo := QrTXRRMCabCodigo.Value;
  TX_PF.ExcluiCabEIMEI_OpeCab(QrTXRRMCabMovimCod.Value,
    QrTXRRMInnControle.Value, emidEmReprRM, TEstqMotivDel.emtdWetCurti161);
  //
  LocCod(Codigo, Codigo);
}
end;

procedure TFmTXRRMCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmTXRRMCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXRRMCab.ReabreGGX();
begin
{POIU
(*
  if CkCpermitirCrust.Checked then
    TX_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_TXFinCla))
  else
    TX_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_3072_TXRibCla));
*)
  TX_PF.AbreGraGruXY(QrGraGruX, 'AND ggx.GragruY<>0');
  TX_PF.AbreGraGruXY(QrGGXDst, 'AND ggx.GragruY<>0');
}
end;

procedure TFmTXRRMCab.RecalculaCusto();
begin
{POIU
  TX_PF.AtualizaTotaisTXCurCab(QrTXRRMCabMovimCod.Value);
  LocCod(QrTXRRMCabCodigo.Value, QrTXRRMCabCodigo.Value);
}
end;

procedure TFmTXRRMCab.Removedesclassificado1Click(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  PCInnDst.ActivePageIndex := 1;
  PCRRMDst.ActivePageIndex := 1;
  if Geral.MB_Pergunta('Confirma a exclus�o do item desclassificado?') = ID_YES then
  begin
    Codigo   := QrTXRRMCabCodigo.Value;
    CtrlDel  := QrTXRRMDesclDstControle.Value;
    CtrlDst  := QrTXRRMDesclDstSrcNivel2.Value;
    MovimNiv := QrTXRRMInnMovimNiv.Value;
    MovimCod := QrTXRRMInnMovimCod.Value;
    //
    if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisTXXxxCab('txrrmcab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      TX_PF.AtualizaSaldoIMEI(QrTXRRMInnControle.Value, True);
      // Excluir Baixa tambem
      CtrlDel := TX_PF.ObtemControleMovimTwin(
        QrTXRRMDesclDstMovimCod.Value, QrTXRRMDesclDstMovimTwn.Value, emidDesclasse,
        eminSorcClass);
      //
      TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXRRMDesclDst,
        TIntegerField(QrTXRRMDesclDstControle), QrTXRRMDesclDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //TX_PF.AtualizaTotaisTXXxxCab('txrrmcab', MovimCod);
      TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
      //
      TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmReprRM, MovimCod);
      LocCod(Codigo, Codigo);
      ReopenTXRRMDesclDst(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmTXRRMCab.ReopenTXRRMDesclDst(Controle, Pallet: Integer);
begin
  TX_PF.ReopenTXXxxExtra(QrTXRRMDesclDst, QrTXRRMCabCodigo.Value,
    (*QrTXRRMInnControle.Value,*) QrTXRRMCabTemIMEIMrt.Value,
    emidDesclasse, emidEmReprRM, eminDestClass);
end;

procedure TFmTXRRMCab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if (QrTXRRMCabDtHrLibOpe.Value > 2) or (QrTXRRMCabDtHrFimOpe.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
(*
  if QrTXRRMInnSdoVrtPeca.Value < QrTXRRMInnPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de destino cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirma a exclus�o do item de destino?') = ID_YES then
  begin
    Codigo   := QrTXRRMCabCodigo.Value;
    CtrlDel  := QrTXRRMDstControle.Value;
    CtrlDst  := QrTXRRMDstSrcNivel2.Value;
    MovimNiv := QrTXRRMInnMovimNiv.Value;
    MovimCod := QrTXRRMInnMovimCod.Value;
    //
    if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisTXXxxCab('txrrmcab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      TX_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := TX_PF.ObtemControleMovimTwin(
        QrTXRRMDstMovimCod.Value, QrTXRRMDstMovimTwn.Value, emidEmReprRM,
        eminEmRRMBxa);
      //
      TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXRRMDst,
        TIntegerField(QrTXRRMDstControle), QrTXRRMDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //TX_PF.AtualizaTotaisTXXxxCab('txrrmcab', MovimCod);
      TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
      //
      TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      TX_PF.ReopenTXIndPrcDst(QrTXRRMDst, QrTXRRMCabMovimCod.Value, CtrlNxt,
      QrTXRRMCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestRRM);
      //
      DistribuicaoCusto();
    end;
  end;
end;

procedure TFmTXRRMCab.ItsExcluiOriIMEIClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if not FItsExcluiOriIMEI_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
(*
  if QrTXRRMInnSdoVrtPeca.Value < QrTXRRMInnPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de origem cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
  begin
    Codigo    := QrTXRRMCabCodigo.Value;
    CtrlDel   := QrTXRRMOriIMEIControle.Value;
    CtrlOri   := QrTXRRMOriIMEISrcNivel2.Value;
    MovimNiv  := QrTXRRMInnMovimNiv.Value;
    MovimCod  := QrTXRRMInnMovimCod.Value;
    //
    if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      TX_PF.AtualizaSaldoIMEI(CtrlOri, True);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //TX_PF.AtualizaTotaisTXXxxCab('txrrmcab', MovimCod);
      TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
     //
     CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXRRMOriIMEI,
        TIntegerField(QrTXRRMOriIMEIControle), QrTXRRMOriIMEIControle.Value);
      //
      RecalculaCusto();
      TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      TX_PF.ReopenTXXxxOris(QrTXRRMCab, QrTXRRMOriIMEI, QrTXRRMOriPallet,
      CtrlNxt, 0, eminSorcRRM);
      //
      DistribuicaoCusto();
    end;
  end;
end;

procedure TFmTXRRMCab.ItsExcluiOriPalletClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
begin
  if not FItsExcluiOriPallet_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if MyObjects.FIC(QrTXRRMOriPalletPallet.Value = 0, nil,
  'Item n�o � um pallet!') then
    Exit;
  //
  if (QrTXRRMInnSdoVrtQtd.Value < QrTXRRMInnQtde.Value) then
  begin
    (*
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
     *)
  end;
  //else
  begin
    Pallet    := QrTXRRMOriPalletPallet.Value;
    Itens     := 0;
    //
    QrTXRRMOriIMEI.First;
    while not QrTXRRMOriIMEI.Eof do
    begin
      if QrTXRRMOriIMEIPallet.Value = Pallet then
        Itens := Itens + 1;
      //
      QrTXRRMOriIMEI.Next;
    end;
    if Itens > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
      Geral.FF0(Pallet) + '?') = ID_YES then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Exclu�ndo IMEIS do Pallet selecionado!');
        //
        Itens := 0;
        QrTXRRMOriIMEI.First;
        while not QrTXRRMOriIMEI.Eof do
        begin
          if QrTXRRMOriIMEIPallet.Value = Pallet then
          begin
            Itens := Itens + 1;
            //
            Codigo    := QrTXRRMCabCodigo.Value;
            CtrlDel   := QrTXRRMOriIMEIControle.Value;
            CtrlOri   := QrTXRRMOriIMEISrcNivel2.Value;
            MovimNiv  := QrTXRRMInnMovimNiv.Value;
            MovimCod  := QrTXRRMInnMovimCod.Value;
            //
            if TX_PF.ExcluiTXMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti161),
            Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            //Dmod.MyDB) = ID_YES then
            begin
              TX_PF.AtualizaSaldoIMEI(CtrlOri, True);
              //
              // Nao se aplica. Calcula com function propria a seguir.
              //TX_PF.AtualizaTotaisTXXxxCab('txrrmcab', MovimCod);
              TX_PF.AtualizaTotaisTXRRMCab(QrTXRRMCabMovimCod.Value);
            end;
            //
          end;
          QrTXRRMOriIMEI.Next;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrTXRRMOriIMEI,
      TIntegerField(QrTXRRMOriIMEIControle), QrTXRRMOriIMEIControle.Value);
      //
      RecalculaCusto();
      DistribuicaoCusto();
      TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      TX_PF.ReopenTXXxxOris(QrTXRRMCab, QrTXRRMOriIMEI, QrTXRRMOriPallet,
      CtrlNxt, 0, eminSorcRRM);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Geral.MB_Erro(
      'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmTXRRMCab.DefineONomeDoForm;
begin
end;

procedure TFmTXRRMCab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Procedimento n�o implementado! Solicite � DERMATEK!');
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXRRMCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXRRMCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXRRMCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXRRMCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXRRMCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXRRMCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXRRMCabCodigo.Value;
  Close;
end;

procedure TFmTXRRMCab.ItsIncluiDstClick(Sender: TObject);
begin
  MostraTXRRMDst(stIns);
end;

procedure TFmTXRRMCab.CabAltera1Click(Sender: TObject);
var
  Empresa(*, TXPedIts*): Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXRRMCab, [PnDados],
  [PnEdita], TPData, ImgTipo, 'txrrmcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrTXRRMCabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdClientMO.ValueVariant    := QrTXRRMInnClientMO.Value;
  CBClientMO.KeyValue        := QrTXRRMInnClientMO.Value;
  //
  EdCliente.ValueVariant    := QrTXRRMCabCliente.Value;
  CBCliente.KeyValue        := QrTXRRMCabCliente.Value;
  //
  EdSerieRem.ValueVariant   := QrTXRRMCabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrTXRRMCabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrTXRRMCabLPFMO.Value;
  //
(*
  EdControle.ValueVariant   := QrTXRRMCabControle.Value;
  EdGraGruX.ValueVariant    := QrTXRRMCabGraGruX.Value;
  CBGraGruX.KeyValue        := QrTXRRMCabGraGruX.Value;
  //EdCustoMOTot.ValueVariant := QrTXRRMCabCustoMOTot.Value;
  EdCustoMOUni.ValueVariant := QrTXRRMCabCustoMOUni.Value;
  EdQtde.ValueVariant      := QrTXRRMCabPecas.Value;
  EdMovimTwn.ValueVariant   := QrTXRRMCabMovimTwn.Value;
  EdObserv.ValueVariant     := QrTXRRMCabObserv.Value;
  EdReqMovEstq.ValueVariant := QrTXRRMCabReqMovEstq.Value;
  //
  TX_PF.ReopenPedItsXXX(QrTXPedIts, QrTXRRMCabControle.Value, QrTXRRMCabControle.Value);
*)
  Habilita := QrTXRRMCabPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrTXRRMCabPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrTXRRMCabPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrTXRRMCabStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrTXRRMCabStqCenLoc.Value;
  //
  EdFornecMO.ValueVariant   := QrTXRRMCabFornecMO.Value;
  CBFornecMO.KeyValue       := QrTXRRMCabFornecMO.Value;
  //
  EdClientMO.ValueVariant   := QrTXRRMCabClientMO.Value;
  CBClientMO.KeyValue       := QrTXRRMCabClientMO.Value;
  //
  EdTXCOPCab.ValueVariant   := QrTXRRMCabTXCOPCab.Value;
  CBTXCOPCab.KeyValue       := QrTXRRMCabTXCOPCab.Value;
  //
(*
  EdEmitGru.ValueVariant    := QrTXRRMCabEmitGru.Value;
  CBEmitGru.KeyValue        := QrTXRRMCabEmitGru.Value;
*)
  //
  EdTXCOPCab.SetFocus;
end;

procedure TFmTXRRMCab.BtConfirmaClick(Sender: TObject);
var
  //GraGruX, GGXDst,
  ClientMO, FluxoCab, FornecMO, Cliente, StqCenLoc, PedItsLib, TXCOPCab,
  Codigo, Empresa, MovimCod, MovimNiv,
  Forma, Quant, CliVenda, NFeRem, SerieRem,
  EmitGru: Integer;
  Nome, DtHrAberto, DataHora, LPFMO: String;
  SQLType: TSQLType;
  //ExigeNFe: Boolean;
  CustoMOUni: Double;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
{
  GraGruX        := EdGragruX.ValueVariant;
  GGXDst         := EdGGXDst.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  TipoArea       := RGTipoArea.ItemIndex;
}
  DtHrAberto     := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Nome           := EdNome.Text;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  EmitGru        := EdEmitGru.ValueVariant;
  //
  ClientMO       := EdClientMO.ValueVariant;
  FluxoCab       := EdFluxoCab.ValueVariant;
  FornecMO       := EdFornecMO.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  //CustoMOTot     := EdCustoMOTot.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
{
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira!') then
    Exit;
  if MyObjects.FIC(GGXDst = 0, EdGGXDst, 'Informe o Artigo a ser produzido!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then
    Exit;
  ExigeNFe := (FornecMO <> 0) and (FornecMO <> ClientMO) and (NFeRem = 0);
  if MyObjects.FIC(ExigeNFe, EdNFeRem, 'Defina a NFe de remessa!') then
    Exit;
}
  if MyObjects.FIC(ClientMO = 0, EdCLientMO, 'Defina o dono do material (a pr�pria empresa ou outra contratante de M.O.!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque!') then
    Exit;
  if MyObjects.FIC(FluxoCab = 0, EdFluxoCab, 'Informe o fluxo de produ��o!') then
    Exit;
  //
  //
  if not TX_PF.ValidaCampoNF(9, EdNFeRem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txrrmcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'txrrmcab', False, [
(*
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'Cliente', 'LPFMO', 'NFeRem',
  'SerieRem', 'GGXDst', 'EmitGru'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  Cliente, LPFMO, NFeRem,
  SerieRem, GGXDst, EmitGru], [
  Codigo], True) then
*)
  'MovimCod', 'Nome', 'Empresa',
  'Cliente', 'DtHrAberto', 'SerieRem',
  'NFeRem', 'LPFMO', 'TXCOPCab',
  'FluxoCab', 'ClientMO', 'FornecMO',
  'StqCenLoc'], [
  'Codigo'], [
  MovimCod, Nome, Empresa,
  Cliente, DtHrAberto, SerieRem,
  NFeRem, LPFMO, TXCOPCab,
  FluxoCab, ClientMO, FornecMO,
  StqCenLoc], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      TX_PF.InsereTXMovCab(MovimCod, emidEmReprRM, Codigo)
    else
    begin
      if EdNFeRem.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
      CliVenda := Cliente;
      DataHora := DtHrAberto;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_TMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
      //
(*
      CustoMOUni := EdCustoMOUni.ValueVariant;
      MovimNiv   := Integer(eminEmRRMInn);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'CustoMOUni'], ['MovimCod', 'MovimNiv'], [
      CustoMOUni], [MovimCod, MovimNiv], True);
*)
    end;

    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrTXRRMOriIMEI.RecordCount = 0) then
    begin
      //MostraTXRRMOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o de mat�ria-prima',
      'Escolha a forma de sele��o da mat�ria-prima', [
      'Pallet Total', 'Pallet Parcial',
      'IME-I Total', 'IME-I Parcial'], -1);
      FItsIncluiOri_Enabled := True;
      case Forma of
        0: MostraTXRRMOriPall(stIns, ptTotal);
        1: MostraTXRRMOriPall(stIns, ptParcial);
        2: MostraTXRRMOriIMEI(stIns, ptTotal);
        3: MostraTXRRMOriIMEI(stIns, ptParcial);
      end;
      MostraTXRRMInn(stIns, TPartOuTodo.ptParcial);
    end;
    TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmReprRM, MovimCod);
    TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
  end;
  TX_PF.AtualizaTotaisTXXxxCab('txplccab', MovimCod);
  DistribuicaoCusto();
  LocCod(Codigo, Codigo);
end;

procedure TFmTXRRMCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'txrrmcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txrrmcab', 'Codigo');
end;

procedure TFmTXRRMCab.BtDstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmTXRRMCab.BtInnClick(Sender: TObject);
begin
  PCInnDst.ActivePageIndex := 0;
  PCRRMDst.ActivePage := TsInn;
  MyObjects.MostraPopUpDeBotao(PMInn, BtInn);
end;

procedure TFmTXRRMCab.BtOriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmTXRRMCab.Adicionadesclassificado1Click(Sender: TObject);
begin
  MostraTXRRMDescl(stIns);
end;

procedure TFmTXRRMCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCRRMOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsEnvioMO;
  //
  TX_Jan.MostraFormTXMOEnvEnv(stUpd, QrTXRRMOriIMEI, QrTXMOEnvEnv, QrTXMOEnvETMI,
  siNegativo, 0, 0);
  //
  LocCod(QrTXRRMCabCodigo.Value,QrTXRRMCabCodigo.Value);
  //
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXRRMCab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllTX}
{POIU
  //PCRRMOri.ActivePageIndex := 2;
  PCRRMOri.ActivePage := TsRetornoMO;
  //
  TX_Jan.MostraFormTXMOEnvRet(stUpd, QrTXRRMInn, QrTXRRMDst, QrTXRRMBxa,
  QrTXMOEnvRet, QrTXMOEnvGTMI, QrTXMOEnvRTMI, 0, 0, 0, siPositivo, FGerArX2Env);
  //
  LocCod(QrTXRRMCabCodigo.Value,QrTXRRMCabCodigo.Value);
  //
}
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXRRMCab.AlteraLocaldoestoque1Click(Sender: TObject);
begin
  TX_PF.InfoStqCenLoc(QrTXRRMInnControle.Value,
    QrTXRRMInnStqCenLoc.Value, QrTXRRMInn);
end;

procedure TFmTXRRMCab.AtualizaestoqueEmindustrializao1Click(Sender: TObject);
begin
  TX_PF.AtualizaSaldoIMEI(QrTXRRMInnControle.Value, True);
  TX_PF.AtualizaTotaisTXIndCab(QrTXRRMCabMovimCod.Value);
  LocCod(QrTXRRMCabCodigo.Value, QrTXRRMCabCodigo.Value);
end;

procedure TFmTXRRMCab.AtualizaFornecedoresDeDestino();
var
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM txrrmcab ',
    'WHERE Codigo>=' + Geral.FF0(QrTXRRMCabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        TX_PF.AtualizaFornecedorRRM(QrTXRRMCabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmTXRRMCab.AtualizaNFeItens();
begin
  DmModTX_CRC.AtualizaTXMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXRRMCabMovimCod.Value, TEstqMovimID.emidEmReprRM, [eminSorcRRM],
  [eminEmRRMInn, eminDestRRM, eminEmRRMBxa]);
  //
  if QrTXRRMDesclDst.RecordCount > 0 then
    DmModTX_CRC.AtualizaTXMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
    QrTXRRMDesclDstMovimCod.Value, TEstqMovimID.emidDesclasse, [eminSorcClass],
    [eminDestClass]);
end;

procedure TFmTXRRMCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXRRMCab, QrTXRRMCabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, AlteraFornecedorMO1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXRRMCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  //GBEdita.Align := alClient;
  //PnDst.Align := alClient;
  PCInnDst.Align := alClient;
  PCRRMDestSub.ActivePageIndex := 1;
  CriaOForm;
  FSeq := 0;
  //
  TX_PF.ReopenTXPrestador(QrPrestador);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrFluxoCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
{POIU
  TX_PF.ReopenTXCOPCab(QrTXCOPCab, emidEmReprRM);
}
  ReabreGGX();
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCRRMOri, VAR_PC_FRETE_TX_NAME, 0);
  MyObjects.DefinePageControlActivePageByName(PCRRMDst, VAR_PC_FRETE_TX_NAME, 0);
end;

procedure TFmTXRRMCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXRRMCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTXRRMCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrTXRRMCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmTXRRMCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmTXRRMCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXRRMCab.QrTXMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  TX_PF.ReopenTXMOEnvETmi(QrTXMOEnvETmi, QrTXMOEnvEnvCodigo.Value, 0);
end;

procedure TFmTXRRMCab.QrTXMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrTXMOEnvETmi.Close;
end;

procedure TFmTXRRMCab.QrTXMOEnvRetAfterScroll(DataSet: TDataSet);
begin
  TX_PF.ReopenTXMOEnvRTmi(QrTXMOEnvRTmi, QrTXMOEnvRetCodigo.Value, 0);
  TX_PF.ReopenTXMOEnvGTmi(QrTXMOEnvGTmi, QrTXMOEnvRetCodigo.Value, 0);
end;

procedure TFmTXRRMCab.QrTXMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrTXMOEnvGTmi.Close;
  QrTXMOEnvRTmi.Close;
end;

procedure TFmTXRRMCab.QrTXRRMCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXRRMCab.QrTXRRMCabAfterScroll(DataSet: TDataSet);
begin
  // Em processo
  TX_EFD_ICMS_IPI.ReopenTXIndPrcAtu(QrTXRRMInn, QrTXRRMCabMovimCod.Value, 0,
  QrTXRRMCabTemIMEIMrt.Value, eminEmRRMInn);
  // materia-prima
  TX_PF.ReopenTXXxxOris(QrTXRRMCab, QrTXRRMOriIMEI, QrTXRRMOriPallet, 0, 0,
  eminSorcRRM);
  ReopenTXRRMDesclDst(0, 0);
  // Semi acabado
  TX_PF.ReopenTXIndPrcDst(QrTXRRMDst, QrTXRRMCabMovimCod.Value, 0,
  QrTXRRMCabTemIMEIMrt.Value, TEstqMovimNiv.eminDestRRM);
  // Baixa forcada
  TX_EFD_ICMS_IPI.ReopenTXIndPrcForcados(QrForcados, 0, QrTXRRMCabCodigo.Value,
    QrTXRRMInnControle.Value, QrTXRRMCabTemIMEIMrt.Value, emidEmReprRM);
end;

procedure TFmTXRRMCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXRRMCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXRRMCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := TX_Jan.MostraFormTXTalPsq(emidEmReprRM, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmTXRRMCab.SbTXCOPCabCadClick(Sender: TObject);
begin
{POIU
  VAR_CADASTRO := 0;
  TX_Jan.MostraFormTXCOPCab(0);
  UMyMod.SetaCodigoPesquisado(EdTXCOPCab, CBTXCOPCab, QrTXCOPCab, VAR_CADASTRO);
}
end;

procedure TFmTXRRMCab.SbTXCOPCabCpyClick(Sender: TObject);
const
  EdOperacoes = nil;
  CBOperacoes = nil;
  EdCustoMOKg = nil;
begin
{POIU
  TX_PF.PreencheDadosDeTXCOPCab(EdTXCOPCab.ValueVariant, EdEmpresa, CBEmpresa, RGTipoA r e a,
  EdGraGruX, CBGraGruX, EdGGXDst, CBGGXDst, EdFornecMO, CBFornecMO, EdStqCenLoc,
  CBStqCenLoc, EdOperacoes, CBOperacoes, EdCliente, CBCliente, EdPedItsLib,
  CBPedItsLib, EdClientMO, CBClientMO, EdCustoMOM2);
}
end;

procedure TFmTXRRMCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXRRMCab.AlteraFornecedorMO1Click(Sender: TObject);
begin
  if TX_PF.AlteraTMI_FornecMO(QrTXRRMInnControle.Value,
  QrTXRRMInnFornecMO.Value) then
    LocCod(QrTXRRMCabCodigo.Value, QrTXRRMCabCodigo.Value);
end;

procedure TFmTXRRMCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmTXRRMCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  (*
  EdControle.ValueVariant   := 0;
  EdGraGruX.ValueVariant    := 0;
  CBGraGruX.KeyValue        := 0;
  EdQtde.ValueVariant       := 0;
  EdMovimTwn.ValueVariant   := 0;
  EdObserv.ValueVariant     := '';
  *)
  EdCustoMOTot.ValueVariant := 0;
  EdFornecMO.ValueVariant   := 0;
  CBFornecMO.KeyValue       := 0;
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXRRMCab, [PnDados],
  [PnEdita], EdTXCOPCab, ImgTipo, 'txrrmcab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
  TX_PF.ReopenPedItsXXX(QrTXPedIts, 0, 0);
end;

procedure TFmTXRRMCab.QrTXRRMCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXRRMInn.Close;
  QrTXRRMOriIMEI.Close;
  QrTXRRMOriPallet.Close;
  QrTXRRMDst.Close;
  QrForcados.Close;
  QrTXMOEnvEnv.Close;
  QrTXMOEnvRet.Close;
end;

procedure TFmTXRRMCab.QrTXRRMCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXRRMCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTXRRMCab.QrTXRRMCabCalcFields(DataSet: TDataSet);
begin
  QrTXRRMCabNO_DtHrLibOpe.Value := Geral.FDT(QrTXRRMCabDtHrLibOpe.Value, 106, True);
  QrTXRRMCabNO_DtHrFimOpe.Value := Geral.FDT(QrTXRRMCabDtHrFimOpe.Value, 106, True);
  QrTXRRMCabNO_DtHrCfgOpe.Value := Geral.FDT(QrTXRRMCabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmTXRRMCab.QrTXRRMDesclDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXRRMCabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'tst.Nome NO_SerieTal, ',
  'tsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  tsp ON tsp.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),
  'AND tmi.SrcMovID=' + Geral.FF0(Integer(emidEmReprRM)),
  'AND tmi.SrcNivel1=' + Geral.FF0(QrTXRRMCabCodigo.Value),
  'AND tmi.SrcNivel2=' + Geral.FF0(QrTXRRMInnControle.Value),
  'AND tmi.MovimTwn=' + Geral.FF0(QrTXRRMDesclDstMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXRRMDesclBxa, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
end;

procedure TFmTXRRMCab.QrTXRRMDstAfterScroll(DataSet: TDataSet);
const
  SQL_Limit = '';
  Controle = 0;
var
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXRRMCabTemIMEIMrt.Value;
  TX_PF.ReopenTXIndPrcBxa(QrTXRRMBxa, QrTXRRMCabMovimCod.Value,
  QrTXRRMDstMovimTwn.Value, Controle, TemIMEIMrt, eminEmRRMBxa, SQL_Limit);
end;

procedure TFmTXRRMCab.QrTXRRMDstBeforeClose(DataSet: TDataSet);
begin
  QrTXRRMBxa.Close;
  QrTXMOEnvRet.Close;
end;

end.

