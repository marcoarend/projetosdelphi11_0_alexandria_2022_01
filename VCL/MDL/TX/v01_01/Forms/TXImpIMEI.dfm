object FmTXImpIMEI: TFmTXImpIMEI
  Left = 0
  Top = 0
  Caption = 'TEX-FAXAO-025 :: Impress'#227'o de IME-I'
  ClientHeight = 687
  ClientWidth = 1022
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QrOld01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 344
    Top = 217
    object QrOld01Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrOld01GraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrOld01Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld01PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld01NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrOld01Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrOld01Ficha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrOld01NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrOld01NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object frxDsOld01: TfrxDBDataset
    UserName = 'frxDsOld01'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Observ=Observ'
      'Ficha=Ficha'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch')
    DataSet = QrOld01
    BCDToCurrency = False
    Left = 344
    Top = 264
  end
  object QrOld02: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 404
    Top = 221
    object QrOld02Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrOld02GraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrOld02Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld02PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld02NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrOld02Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrOld02Ficha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrOld02NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrOld02NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object frxDsOld02: TfrxDBDataset
    UserName = 'frxDsOld02'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Observ=Observ'
      'Ficha=Ficha'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch')
    DataSet = QrOld02
    BCDToCurrency = False
    Left = 404
    Top = 264
  end
  object QrOld03: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 468
    Top = 221
    object QrOld03Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrOld03GraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrOld03Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld03PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld03NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrOld03Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrOld03Ficha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrOld03NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrOld03NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object frxDsOld03: TfrxDBDataset
    UserName = 'frxDsOld03'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Observ=Observ'
      'Ficha=Ficha'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch')
    DataSet = QrOld03
    BCDToCurrency = False
    Left = 468
    Top = 268
  end
  object QrOld04: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 532
    Top = 217
    object QrOld04Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrOld04GraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrOld04Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld04PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld04NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrOld04Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrOld04Ficha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrOld04NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrOld04NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object frxDsOld04: TfrxDBDataset
    UserName = 'frxDsOld04'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Observ=Observ'
      'Ficha=Ficha'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch')
    DataSet = QrOld04
    BCDToCurrency = False
    Left = 532
    Top = 264
  end
  object QrOld05: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 596
    Top = 217
    object QrOld05Controle: TLargeintField
      FieldName = 'Controle'
    end
    object QrOld05GraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrOld05Pecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld05PesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrOld05NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrOld05Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrOld05Ficha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrOld05NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrOld05NO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object frxDsOld05: TfrxDBDataset
    UserName = 'frxDsOld05'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Observ=Observ'
      'Ficha=Ficha'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch')
    DataSet = QrOld05
    BCDToCurrency = False
    Left = 596
    Top = 268
  end
  object frxDsEstqR4: TfrxDBDataset
    UserName = 'frxDsEstqR4'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'GraGru1=GraGru1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'Empresa=Empresa'
      'NO_EMPRESA=NO_EMPRESA'
      'Cliente=Cliente'
      'NO_CLIENTE=NO_CLIENTE'
      'Fornece=Fornece'
      'NO_FORNECE=NO_FORNECE'
      'Status=Status'
      'NO_STATUS=NO_STATUS'
      'DataHora=DataHora'
      'OrdGGX=OrdGGX'
      'OrdGGY=OrdGGY'
      'GraGruY=GraGruY'
      'NO_GGY=NO_GGY'
      'NO_FICHA=NO_FICHA'
      'Ativo=Ativo'
      'TERCEIRO=TERCEIRO'
      'TIPO_TERCEIRO=TIPO_TERCEIRO'
      'NO_TIPO_COURO=NO_TIPO_COURO'
      'NO_ACAO_COURO=NO_ACAO_COURO'
      'ReqMovEstq=ReqMovEstq'
      'Observ=Observ'
      'PedItsLib=PedItsLib'
      'NO_FRNCALC=NO_FRNCALC'
      'SdoVrtArP2=SdoVrtArP2')
    DataSet = QrEstqR4
    BCDToCurrency = False
    Left = 104
    Top = 16
  end
  object frxTEX_FAXAO_025_01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41905.510812419000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '  FItens: Extended;                                   '
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  FItens := <VARF_SoCincoMostrados>;                            ' +
        '  '
      '  Me_SoCincoMostrados.Visible := FItens > 5;'
      '  //        '
      '  Imprime := True;'
      '  //'
      
        '  MeOldC1L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC1L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC2L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC3L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC4L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC5L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC6L0.Visible := FItens >= 1;                             ' +
        '                         '
      '  MeOldC7L0.Visible := FItens >= 1;'
      '  //'
      
        '  MeOldC1L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC1L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC2L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC3L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC4L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC5L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC6L1.Visible := FItens >= 1;                             ' +
        '                         '
      '  MeOldC7L1.Visible := FItens >= 1;'
      '  //        '
      
        '  MeOldC1L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC1L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC2L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC3L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC4L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC5L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC6L2.Visible := FItens >= 2;                             ' +
        '                         '
      '  MeOldC7L2.Visible := FItens >= 2;'
      '  //        '
      
        '  MeOldC1L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC2L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC3L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC4L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC5L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC6L3.Visible := FItens >= 3;                             ' +
        '                         '
      '  MeOldC7L3.Visible := FItens >= 3;'
      '  //        '
      
        '  MeOldC1L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC2L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC3L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC4L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC5L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC6L4.Visible := FItens >= 4;                             ' +
        '                         '
      '  MeOldC7L4.Visible := FItens >= 4;'
      '  //        '
      
        '  MeOldC1L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC2L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC3L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC4L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC5L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC6L5.Visible := FItens >= 5;                             ' +
        '                         '
      '  MeOldC7L5.Visible := FItens >= 5;'
      '  //        '
      'end;'
      ''
      'procedure BarCode1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  BarCode1.Text := <VARF_CODIGO_BARRA>;  '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxTEX_FAXAO_025_01GetValue
    Left = 104
    Top = 116
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
      end
      item
        DataSet = frxDsOld01
        DataSetName = 'frxDsOld01'
      end
      item
        DataSet = frxDsOld02
        DataSetName = 'frxDsOld02'
      end
      item
        DataSet = frxDsOld03
        DataSetName = 'frxDsOld03'
      end
      item
        DataSet = frxDsOld04
        DataSetName = 'frxDsOld04'
      end
      item
        DataSet = frxDsOld05
        DataSetName = 'frxDsOld05'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370103150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 510.236550000000000000
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 132.283550000000000000
          Top = 53.015770000000010000
          Width = 415.748300000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Romaneio de [frxDsEstqR4."NO_TIPO_COURO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 56.795300000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 483.779840000000000000
          Top = 56.795300000000000000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 83.440940000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'IME-I  [frxDsEstqR4."Controle"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 124.724409448818900000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 264.566943780000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 264.566943780000000000
          Width = 302.362204720000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."TIPO_TERCEIRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 162.322820000000000000
          Width = 26.456631890000000000
          Height = 79.370093390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 336.480324720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 336.480324720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 336.480324720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 336.480324720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 291.125986690000000000
          Width = 188.976377950000000000
          Height = 45.354330710000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 291.125986690000000000
          Width = 302.362204720000000000
          Height = 45.354330710000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Terceiro"] - [frxDsEstqR4."NO_FORNECE"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo22: TfrxMemoView
          Left = 26.456631890000000000
          Top = 162.322820000000000000
          Width = 442.204648740000000000
          Height = 79.370093390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -29
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."GraGruX"] - [frxDsEstqR4."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 366.716542760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."SdoVrtArP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 366.716542760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 366.716542760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR4."Sdo' +
              'VrtPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 366.716542760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."SdoVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 98.267701890000000000
          Top = 124.724409450000000000
          Width = 370.393578740000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 124.724409448818900000
          Width = 98.267701890000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 37.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 249.448980000000000000
          Top = 79.472480000000000000
          Width = 411.968330630000000000
          Height = 41.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              'IME-C: [frxDsEstqR4."MovimCod"] [frxDsEstqR4."NO_ACAO_COURO"]: [' +
              'frxDsEstqR4."Codigo"]    ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Line3: TfrxLineView
          Top = 559.370103150000000000
          Width = 680.315400000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Memo10: TfrxMemoView
          Left = 491.338900000000000000
          Top = 264.566943780000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 491.338900000000000000
          Top = 291.023788030000000000
          Width = 188.976377950000000000
          Height = 45.354330710000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_FICHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeOldC1L0: TfrxMemoView
          Left = 18.897650000000000000
          Top = 408.189240000000000000
          Width = 60.472406770000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
        end
        object MeOldC1L1: TfrxMemoView
          Left = 18.897650000000000000
          Top = 427.086890000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld01."Controle"]')
          ParentFont = False
        end
        object MeOldC1L2: TfrxMemoView
          Left = 18.897650000000000000
          Top = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld02."Controle"]')
          ParentFont = False
        end
        object MeOldC1L3: TfrxMemoView
          Left = 18.897650000000000000
          Top = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld03."Controle"]')
          ParentFont = False
        end
        object MeOldC1L4: TfrxMemoView
          Left = 18.897650000000000000
          Top = 483.779840000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld04."Controle"]')
          ParentFont = False
        end
        object MeOldC1L5: TfrxMemoView
          Left = 18.897650000000000000
          Top = 502.677490000000100000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld05."Controle"]')
          ParentFont = False
        end
        object MeOldC2L1: TfrxMemoView
          Left = 79.370130000000000000
          Top = 427.086890000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld01."NO_SerieFch"] [frxDsOld01."Ficha"]')
          ParentFont = False
        end
        object MeOldC2L2: TfrxMemoView
          Left = 79.370130000000000000
          Top = 445.984540000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld02."NO_SerieFch"] [frxDsOld02."Ficha"]')
          ParentFont = False
        end
        object MeOldC2L3: TfrxMemoView
          Left = 79.370130000000000000
          Top = 464.882190000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld03."NO_SerieFch"] [frxDsOld03."Ficha"]')
          ParentFont = False
        end
        object MeOldC2L4: TfrxMemoView
          Left = 79.370130000000000000
          Top = 483.779840000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld04."NO_SerieFch"] [frxDsOld04."Ficha"]')
          ParentFont = False
        end
        object MeOldC2L5: TfrxMemoView
          Left = 79.370130000000000000
          Top = 502.677490000000100000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld05."NO_SerieFch"] [frxDsOld05."Ficha"]')
          ParentFont = False
        end
        object MeOldC2L0: TfrxMemoView
          Left = 79.370130000000000000
          Top = 408.189240000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
        end
        object MeOldC3L1: TfrxMemoView
          Left = 173.858380000000000000
          Top = 427.086890000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld01."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object MeOldC3L2: TfrxMemoView
          Left = 173.858380000000000000
          Top = 445.984540000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld02."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object MeOldC3L3: TfrxMemoView
          Left = 173.858380000000000000
          Top = 464.882190000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld03."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object MeOldC3L4: TfrxMemoView
          Left = 173.858380000000000000
          Top = 483.779840000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld04."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object MeOldC3L5: TfrxMemoView
          Left = 173.858380000000000000
          Top = 502.677490000000100000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld05."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object MeOldC3L0: TfrxMemoView
          Left = 173.858380000000000000
          Top = 408.189240000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mat'#233'ria-prima In Natura')
          ParentFont = False
        end
        object MeOldC4L0: TfrxMemoView
          Left = 362.834880000000000000
          Top = 408.189240000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object MeOldC4L1: TfrxMemoView
          Left = 362.834880000000000000
          Top = 427.086890000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld01."Pecas">]')
          ParentFont = False
        end
        object MeOldC4L2: TfrxMemoView
          Left = 362.834880000000000000
          Top = 445.984540000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld02."Pecas">]')
          ParentFont = False
        end
        object MeOldC4L3: TfrxMemoView
          Left = 362.834880000000000000
          Top = 464.882190000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld03."Pecas">]')
          ParentFont = False
        end
        object MeOldC4L4: TfrxMemoView
          Left = 362.834880000000000000
          Top = 483.779840000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld04."Pecas">]')
          ParentFont = False
        end
        object MeOldC4L5: TfrxMemoView
          Left = 362.834880000000000000
          Top = 502.677490000000100000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld05."Pecas">]')
          ParentFont = False
        end
        object MeOldC5L0: TfrxMemoView
          Left = 408.189240000000000000
          Top = 408.189240000000000000
          Width = 60.472406770000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object MeOldC5L1: TfrxMemoView
          Left = 408.189240000000000000
          Top = 427.086890000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld01."PesoKg">]')
          ParentFont = False
        end
        object MeOldC5L2: TfrxMemoView
          Left = 408.189240000000000000
          Top = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld02."PesoKg">]')
          ParentFont = False
        end
        object MeOldC5L3: TfrxMemoView
          Left = 408.189240000000000000
          Top = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld03."PesoKg">]')
          ParentFont = False
        end
        object MeOldC5L4: TfrxMemoView
          Left = 408.189240000000000000
          Top = 483.779840000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld04."PesoKg">]')
          ParentFont = False
        end
        object MeOldC5L5: TfrxMemoView
          Left = 408.189240000000000000
          Top = 502.677490000000100000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsOld05."PesoKg">]')
          ParentFont = False
        end
        object MeOldC6L1: TfrxMemoView
          Left = 468.661720000000000000
          Top = 427.086890000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld01."NO_FORNECE"]')
          ParentFont = False
        end
        object MeOldC6L2: TfrxMemoView
          Left = 468.661720000000000000
          Top = 445.984540000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld02."NO_FORNECE"]')
          ParentFont = False
        end
        object MeOldC6L3: TfrxMemoView
          Left = 468.661720000000000000
          Top = 464.882190000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld03."NO_FORNECE"]')
          ParentFont = False
        end
        object MeOldC6L4: TfrxMemoView
          Left = 468.661720000000000000
          Top = 483.779840000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld04."NO_FORNECE"]')
          ParentFont = False
        end
        object MeOldC6L5: TfrxMemoView
          Left = 468.661720000000000000
          Top = 502.677490000000100000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld05."NO_FORNECE"]')
          ParentFont = False
        end
        object MeOldC6L0: TfrxMemoView
          Left = 468.661720000000000000
          Top = 408.189240000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Proced'#234'ncia')
          ParentFont = False
        end
        object MeOldC7L1: TfrxMemoView
          Left = 563.149970000000000000
          Top = 427.086890000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld01."Observ"]')
          ParentFont = False
        end
        object MeOldC7L2: TfrxMemoView
          Left = 563.149970000000000000
          Top = 445.984540000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld02."Observ"]')
          ParentFont = False
        end
        object MeOldC7L3: TfrxMemoView
          Left = 563.149970000000000000
          Top = 464.882190000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld03."Observ"]')
          ParentFont = False
        end
        object MeOldC7L4: TfrxMemoView
          Left = 563.149970000000000000
          Top = 483.779840000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld04."Observ"]')
          ParentFont = False
        end
        object MeOldC7L5: TfrxMemoView
          Left = 563.149970000000000000
          Top = 502.677490000000100000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOld05."Observ"]')
          ParentFont = False
        end
        object MeOldC7L0: TfrxMemoView
          Left = 563.149970000000100000
          Top = 408.189240000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Me_SoCincoMostrados: TfrxMemoView
          Left = 56.692950000000010000
          Top = 521.575140000000100000
          Width = 566.929377950000000000
          Height = 17.007874020000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Existem [VARF_ITENS_OLD] itens de mat'#233'ria-prima geradores deste ' +
              'artigo, mas s'#243' 5 s'#227'o mostrados!!!')
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 510.236550000000000000
          Top = 136.063080000000000000
          Width = 154.500000000000000000
          Height = 83.149660000000000000
          OnBeforePrint = 'BarCode1OnBeforePrint'
          BarType = bcCodeEAN13
          CalcCheckSum = True
          Rotation = 0
          TestLine = False
          Text = '0'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.500000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo12: TfrxMemoView
          Left = 468.661720000000000000
          Top = 124.724490000000000000
          Width = 22.677180000000000000
          Height = 117.165430000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DV IME-I: [VARF_DV_DO_IMEI]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Top = 241.889920000000000000
          Width = 680.315400000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_OBS_OPE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrTXGerArtOld: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 200
    Top = 41
    object QrTXGerArtOldCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXGerArtOldControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXGerArtOldMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXGerArtOldMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXGerArtOldMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXGerArtOldEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXGerArtOldTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXGerArtOldCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXGerArtOldMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXGerArtOldDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXGerArtOldPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXGerArtOldGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXGerArtOldPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXGerArtOldPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXGerArtOldAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXGerArtOldSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXGerArtOldSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXGerArtOldSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXGerArtOldSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrTXGerArtOldSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXGerArtOldSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXGerArtOldSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrTXGerArtOldFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrTXGerArtOldMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrTXGerArtOldFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXGerArtOldCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXGerArtOldDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXGerArtOldDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXGerArtOldDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXGerArtOldQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrTXGerArtOldQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXGerArtOldQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrTXGerArtOldQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXGerArtOldQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXGerArtOldNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrTXGerArtOldNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXGerArtOldNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXGerArtOldNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXGerArtOldID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXGerArtOldNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXGerArtOldNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrTXGerArtOldReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object QrEstqR4: TMySQLQuery
    Database = Dmod.ZZDB
    AfterScroll = QrEstqR4AfterScroll
    OnCalcFields = QrEstqR4CalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM _vsmovimp4_')
    Left = 104
    Top = 64
    object QrEstqR4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstqR4Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEstqR4MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEstqR4MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrEstqR4MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEstqR4GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqR4Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEstqR4PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEstqR4AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEstqR4AreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrEstqR4ValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrEstqR4SdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrEstqR4SdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrEstqR4SdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrEstqR4GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrEstqR4NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEstqR4Pallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrEstqR4NO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrEstqR4Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqR4NO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEstqR4Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEstqR4NO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrEstqR4Fornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrEstqR4NO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrEstqR4Status: TIntegerField
      FieldName = 'Status'
    end
    object QrEstqR4NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrEstqR4DataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrEstqR4OrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrEstqR4OrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object QrEstqR4GraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrEstqR4NO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrEstqR4NO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
      Size = 60
    end
    object QrEstqR4Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEstqR4TERCEIRO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Calculated = True
    end
    object QrEstqR4TIPO_TERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPO_TERCEIRO'
      Calculated = True
    end
    object QrEstqR4NO_TIPO_COURO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_COURO'
      Size = 255
      Calculated = True
    end
    object QrEstqR4NO_ACAO_COURO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ACAO_COURO'
      Size = 50
      Calculated = True
    end
    object QrEstqR4ReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrEstqR4Observ: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrEstqR4PedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrEstqR4NO_FRNCALC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_FRNCALC'
      Size = 100
      Calculated = True
    end
    object QrEstqR4SdoVrtArP2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SdoVrtArP2'
      Calculated = True
    end
  end
  object frxTEX_FAXAO_025_02_A: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41905.510812419000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxTEX_FAXAO_025_01GetValue
    Left = 104
    Top = 168
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
      end
      item
        DataSet = frxDsOld01
        DataSetName = 'frxDsOld01'
      end
      item
        DataSet = frxDsOld02
        DataSetName = 'frxDsOld02'
      end
      item
        DataSet = frxDsOld03
        DataSetName = 'frxDsOld03'
      end
      item
        DataSet = frxDsOld04
        DataSetName = 'frxDsOld04'
      end
      item
        DataSet = frxDsOld05
        DataSetName = 'frxDsOld05'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 1096.063363150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
        RowCount = 0
        object Memo47: TfrxMemoView
          Left = 132.283550000000000000
          Top = 37.897650000000000000
          Width = 415.748300000000000000
          Height = 41.574830000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Romaneio de [frxDsEstqR4."NO_TIPO_COURO"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 64.354360000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 200.315090000000000000
          Top = 64.354360000000000000
          Width = 468.661656540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item de pedido: [frxDsEstqR4."PedItsLib"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 83.440940000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'IME-I  [frxDsEstqR4."Controle"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 123.661410000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 249.448831100000000000
          Width = 132.283464570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 162.519675280000000000
          Width = 94.488188980000000000
          Height = 60.472440944881890000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -37
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 321.362204720000000000
          Width = 170.078740160000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Top = 321.362204720000000000
          Width = 170.078740160000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 321.362204720000000000
          Width = 170.078740160000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 321.362204720000000000
          Width = 170.078740160000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Top = 276.007866690000000000
          Width = 132.283464570000000000
          Height = 45.354330710000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 132.283427950000000000
          Top = 275.905541100000000000
          Width = 415.748104720000000000
          Height = 45.354330710000000000
          DataField = 'Observ'
          DataSet = frxDsEstqR4
          DataSetName = 'frxDsEstqR4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Observ"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo22: TfrxMemoView
          Left = 94.488188980000000000
          Top = 162.519675280000000000
          Width = 585.826788740000000000
          Height = 60.472440944881890000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."GraGruX"] - [frxDsEstqR4."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 347.818892760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 347.818892760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 347.818892760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR4."Pes' +
              'oKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 347.818892760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 94.488171890000000000
          Top = 124.102350000000000000
          Width = 585.826788740000000000
          Height = 37.795275590551200000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 124.102350000000000000
          Width = 94.488188980000000000
          Height = 37.795275590551200000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 26.559060000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 548.031850000000000000
          Top = 249.448831100000000000
          Width = 132.283464570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 548.031850000000000000
          Top = 275.905668030000000000
          Width = 132.283464570000000000
          Height = 45.354330710000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_FICHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeOldC3L1: TfrxMemoView
          Left = 200.315090000000000000
          Top = 400.630180000000000000
          Width = 98.267716535433060000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC3L0: TfrxMemoView
          Left = 200.315090000000000000
          Top = 381.732530000000000000
          Width = 98.267716535433060000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
        end
        object MeOldC4L0: TfrxMemoView
          Left = 298.582870000000000000
          Top = 381.732530000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as WB')
          ParentFont = False
        end
        object MeOldC4L1: TfrxMemoView
          Left = 298.582870000000000000
          Top = 400.630180000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC5L0: TfrxMemoView
          Left = 381.732530000000000000
          Top = 381.732530000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'm'#178' ou kg WB')
          ParentFont = False
        end
        object MeOldC5L1: TfrxMemoView
          Left = 381.732530000000000000
          Top = 400.630180000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC6L1: TfrxMemoView
          Left = 578.268090000000000000
          Top = 400.630180000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC6L0: TfrxMemoView
          Left = 578.268090000000000000
          Top = 381.732530000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'm'#178' ou kg Pronto')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 272.126160000000000000
          Top = 249.448980000000000000
          Width = 275.905604570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_LPFMO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 132.283550000000000000
          Top = 249.448980000000000000
          Width = 139.842524570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              'Lote de produ'#231#227'o do fornecedor da m'#227'o de obra e/ou NFe de remess' +
              'a:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 147.401670000000000000
          Top = 83.252010000000000000
          Width = 529.133760630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              'RME: [frxDsEstqR4."ReqMovEstq"]  IME-C: [frxDsEstqR4."MovimCod"]' +
              '  [frxDsEstqR4."NO_ACAO_COURO"]: [frxDsEstqR4."Codigo"]    ...')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo38: TfrxMemoView
          Top = 381.732530000000000000
          Width = 94.488127950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Top = 400.630180000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Top = 222.992270000000000000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_OBS_OPE]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 94.488250000000000000
          Top = 381.732530000000000000
          Width = 105.826771650000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local baixa')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 94.488250000000000000
          Top = 400.630180000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 483.779840000000000000
          Top = 381.732530000000000000
          Width = 94.488127950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pe'#231'as pronto')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 483.779840000000000000
          Top = 400.630180000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 200.315090000000000000
          Top = 427.086890000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 298.582870000000000000
          Top = 427.086890000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 381.732530000000000000
          Top = 427.086890000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 578.268090000000000000
          Top = 427.086890000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Top = 427.086890000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 94.488250000000000000
          Top = 427.086890000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 483.779840000000000000
          Top = 427.086890000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 200.315090000000000000
          Top = 453.543600000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 298.582870000000000000
          Top = 453.543600000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 381.732530000000000000
          Top = 453.543600000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 578.268090000000000000
          Top = 453.543600000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Top = 453.543600000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 94.488250000000000000
          Top = 453.543600000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 483.779840000000000000
          Top = 453.543600000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 200.315090000000000000
          Top = 480.000310000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 298.582870000000000000
          Top = 480.000310000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 381.732530000000000000
          Top = 480.000310000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 578.268090000000000000
          Top = 480.000310000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Top = 480.000310000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 94.488250000000000000
          Top = 480.000310000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 483.779840000000000000
          Top = 480.000310000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 200.315090000000000000
          Top = 506.457020000000100000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 298.582870000000000000
          Top = 506.457020000000100000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 381.732530000000000000
          Top = 506.457020000000100000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 578.268090000000000000
          Top = 506.457020000000100000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Top = 506.457020000000100000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 94.488250000000000000
          Top = 506.457020000000100000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 483.779840000000000000
          Top = 506.457020000000100000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 200.315090000000000000
          Top = 532.913730000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 298.582870000000000000
          Top = 532.913730000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 381.732530000000000000
          Top = 532.913730000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 578.268090000000000000
          Top = 532.913730000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Top = 532.913730000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 94.488250000000000000
          Top = 532.913730000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 483.779840000000000000
          Top = 532.913730000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 200.315090000000000000
          Top = 559.370440000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 298.582870000000000000
          Top = 559.370440000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 381.732530000000000000
          Top = 559.370440000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 578.268090000000000000
          Top = 559.370440000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Top = 559.370440000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 94.488250000000000000
          Top = 559.370440000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 483.779840000000000000
          Top = 559.370440000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 200.315090000000000000
          Top = 585.827150000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 298.582870000000000000
          Top = 585.827150000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 381.732530000000000000
          Top = 585.827150000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 578.268090000000000000
          Top = 585.827150000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Top = 585.827150000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 94.488250000000000000
          Top = 585.827150000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 483.779840000000000000
          Top = 585.827150000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 200.315090000000000000
          Top = 612.283860000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 298.582870000000000000
          Top = 612.283860000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 381.732530000000000000
          Top = 612.283860000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 578.268090000000000000
          Top = 612.283860000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Top = 612.283860000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 94.488250000000000000
          Top = 612.283860000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 483.779840000000000000
          Top = 612.283860000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 200.315090000000000000
          Top = 638.740570000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 298.582870000000000000
          Top = 638.740570000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 381.732530000000000000
          Top = 638.740570000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 578.268090000000000000
          Top = 638.740570000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Top = 638.740570000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 94.488250000000000000
          Top = 638.740570000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 483.779840000000000000
          Top = 638.740570000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 200.315090000000000000
          Top = 665.197280000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 298.582870000000000000
          Top = 665.197280000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 381.732530000000000000
          Top = 665.197280000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 578.268090000000000000
          Top = 665.197280000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Top = 665.197280000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 94.488250000000000000
          Top = 665.197280000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 483.779840000000000000
          Top = 665.197280000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 200.315090000000000000
          Top = 691.653990000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 298.582870000000000000
          Top = 691.653990000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 381.732530000000000000
          Top = 691.653990000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 578.268090000000000000
          Top = 691.653990000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Top = 691.653990000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 94.488250000000000000
          Top = 691.653990000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Left = 483.779840000000000000
          Top = 691.653990000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Left = 200.315090000000000000
          Top = 718.110700000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Left = 298.582870000000000000
          Top = 718.110700000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Left = 381.732530000000000000
          Top = 718.110700000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Left = 578.268090000000000000
          Top = 718.110700000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Top = 718.110700000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Left = 94.488250000000000000
          Top = 718.110700000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Left = 483.779840000000000000
          Top = 718.110700000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Left = 200.315090000000000000
          Top = 744.567410000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Left = 298.582870000000000000
          Top = 744.567410000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Left = 381.732530000000000000
          Top = 744.567410000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 578.268090000000000000
          Top = 744.567410000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Top = 744.567410000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Left = 94.488250000000000000
          Top = 744.567410000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 483.779840000000000000
          Top = 744.567410000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 200.315090000000000000
          Top = 771.024120000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Left = 298.582870000000000000
          Top = 771.024120000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 381.732530000000000000
          Top = 771.024120000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 578.268090000000000000
          Top = 771.024120000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Top = 771.024120000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 94.488250000000000000
          Top = 771.024120000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Left = 483.779840000000000000
          Top = 771.024120000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 200.315090000000000000
          Top = 797.480830000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Left = 298.582870000000000000
          Top = 797.480830000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Left = 381.732530000000000000
          Top = 797.480830000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Left = 578.268090000000000000
          Top = 797.480830000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Top = 797.480830000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Left = 94.488250000000000000
          Top = 797.480830000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Left = 483.779840000000000000
          Top = 797.480830000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Left = 200.315090000000000000
          Top = 823.937540000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Left = 298.582870000000000000
          Top = 823.937540000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Left = 381.732530000000000000
          Top = 823.937540000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Left = 578.268090000000000000
          Top = 823.937540000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Top = 823.937540000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Left = 94.488250000000000000
          Top = 823.937540000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Left = 483.779840000000000000
          Top = 823.937540000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Left = 200.315090000000000000
          Top = 850.394250000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Left = 298.582870000000000000
          Top = 850.394250000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 381.732530000000000000
          Top = 850.394250000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Left = 578.268090000000000000
          Top = 850.394250000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Top = 850.394250000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Left = 94.488250000000000000
          Top = 850.394250000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Left = 483.779840000000000000
          Top = 850.394250000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 200.315090000000000000
          Top = 876.850960000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Left = 298.582870000000000000
          Top = 876.850960000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Left = 381.732530000000000000
          Top = 876.850960000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Left = 578.268090000000000000
          Top = 876.850960000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Top = 876.850960000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Left = 94.488250000000000000
          Top = 876.850960000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Left = 483.779840000000000000
          Top = 876.850960000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 200.315090000000000000
          Top = 903.307670000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 298.582870000000000000
          Top = 903.307670000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 381.732530000000000000
          Top = 903.307670000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 578.268090000000000000
          Top = 903.307670000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Top = 903.307670000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 94.488250000000000000
          Top = 903.307670000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 483.779840000000000000
          Top = 903.307670000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 200.315090000000000000
          Top = 929.764380000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 298.582870000000000000
          Top = 929.764380000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Left = 381.732530000000000000
          Top = 929.764380000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Left = 578.268090000000000000
          Top = 929.764380000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Top = 929.764380000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Left = 94.488250000000000000
          Top = 929.764380000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Left = 483.779840000000000000
          Top = 929.764380000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Left = 200.315090000000000000
          Top = 956.221090000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Left = 298.582870000000000000
          Top = 956.221090000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Left = 381.732530000000000000
          Top = 956.221090000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Left = 578.268090000000000000
          Top = 956.221090000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Top = 956.221090000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Left = 94.488250000000000000
          Top = 956.221090000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Left = 483.779840000000000000
          Top = 956.221090000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Left = 200.315090000000000000
          Top = 982.677800000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Left = 298.582870000000000000
          Top = 982.677800000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Left = 381.732530000000000000
          Top = 982.677800000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Left = 578.268090000000000000
          Top = 982.677800000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Top = 982.677800000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Left = 94.488250000000000000
          Top = 982.677800000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Left = 483.779840000000000000
          Top = 982.677800000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Left = 200.315090000000000000
          Top = 1009.134510000000000000
          Width = 98.267716540000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Left = 298.582870000000000000
          Top = 1009.134510000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Left = 381.732530000000000000
          Top = 1009.134510000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Left = 578.268090000000000000
          Top = 1009.134510000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo190: TfrxMemoView
          Top = 1009.134510000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo191: TfrxMemoView
          Left = 94.488250000000000000
          Top = 1009.134510000000000000
          Width = 105.826771650000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo192: TfrxMemoView
          Left = 483.779840000000000000
          Top = 1009.134510000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
    end
  end
  object frxTEX_FAXAO_025_03: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '                                        '
      'end.')
    OnGetValue = frxTEX_FAXAO_025_04GetValue
    Left = 104
    Top = 324
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsOPs
        DataSetName = 'frxDsOPs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 287.244280000000000000
          Top = 64.252010000000000000
          Width = 230.551242130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 3.779530000000000000
          Top = 41.574830000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 638.740570000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 517.795610000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 56.692950000000000000
          Top = 64.252010000000000000
          Width = 75.590551181102360000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'O[VARF_LETRA_OX]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 132.283550000000000000
          Top = 64.252010000000000000
          Width = 154.960629920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 706.772110000000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 752.126470000000000000
          Top = 64.252010000000000000
          Width = 124.724389920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornece MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 876.850960000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sdo '#193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 944.882500000000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sdo Int.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 990.236860000000000000
        DataSet = frxDsOPs
        DataSetName = 'frxDsOPs'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 336.378170000000000000
          Width = 181.417352130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOPs."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 287.244280000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 585.827150000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 638.740570000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 517.795610000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 56.692950000000000000
          Width = 75.590551181102360000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOPs."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 173.858380000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'NO_CliVenda'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOPs."NO_CliVenda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 132.283550000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'CliVenda'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."CliVenda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 706.772110000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 793.701300000000000000
          Width = 83.149586770000000000
          Height = 15.118110240000000000
          DataField = 'NO_FornecMO'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOPs."NO_FornecMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 752.126470000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'FornecMO'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."FornecMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 876.850960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'SdoVrtArM2'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."SdoVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 944.882500000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'SdoIntei'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."SdoIntei"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 453.543600000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 668.976810000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 408.189240000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779530000000000000
          Width = 517.795414720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE O[VARF_LETRA_OX]s: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 585.827150000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 638.740570000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 517.795610000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 876.850960000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 944.882500000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoIntei">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsOPs."CliVenda"'
        object MeGrupo1Head: TfrxMemoView
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsOPs."NO_CliVenda"]')
          ParentFont = False
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 325.039580000000000000
        Width = 990.236860000000000000
        object MeGrupo1Foot: TfrxMemoView
          Width = 445.984344720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 445.984540000000000000
          Width = 71.810874720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'O[VARF_LETRA_OX]s: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 585.827150000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 638.740570000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 517.795610000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 706.772110000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 876.850960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 944.882500000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoIntei">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsOPs."FornecMO"'
        object MeGrupo2Head: TfrxMemoView
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsOPs."NO_FornecMO"]')
          ParentFont = False
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 279.685220000000000000
        Width = 990.236860000000000000
        object MeGrupo2Foot: TfrxMemoView
          Width = 445.984344720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 585.827150000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 638.740570000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 517.795610000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 445.984540000000000000
          Width = 71.810874720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'O[VARF_LETRA_OX]s: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo29: TfrxMemoView
          Left = 706.772110000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 876.850960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 944.882500000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoIntei">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrOPs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*FmVSImpIMEI.QrOPs*/'
      '/*********** SQL ***********/'
      'SELECT vmi.*, '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Entrada","Sa'#237'da","Reclasse","Baixa"' +
        ',"Recurtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado"' +
        ',"Sem Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","' +
        'Pr'#233' reclasse","Compra de Classificado","Baixa extra","Saldo ante' +
        'rior","Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub P' +
        'roduto","Reclasse Mul.","Transfer. Local","Em proc. caleiro","Em' +
        ' proc. curtimento","Desclassifica'#231#227'o","Caleado","Couro PDA","Cou' +
        'ro DTA","Sub-produto em proc.","Reprocesso / reparo") NO_MovimID' +
        ','
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo' +
        ' Gerado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Ar' +
        'tigo In Natura","Artigo In Natura","Artigo Gerado","Artigo Class' +
        'ificado","Artigo em Opera'#231#227'o","Origem semi acabado em processo",' +
        '"Semi acabado em processo","Destino semi acabado em processo","B' +
        'aixa de semi acabado em processo","Artigo Semi Acabado","Artigo ' +
        'Acabado","Sub produto","Origem de transf. de local","Destino de ' +
        'transf. de local","Origem caleiro em processo","Caleiro em proce' +
        'sso","Destino caleiro em processo","Baixa de caleiro em processo' +
        '","Artigo de caleiro","Origem curtimento em processo","Curtiment' +
        'o em processo","Destino curtimento em processo","Baixa de curtim' +
        'ento em processo","Artigo de curtimento","Origem PDA em opera'#231#227'o' +
        '","PDA em opera'#231#227'o","Destino PDA em opera'#231#227'o","Baixa de PDA em o' +
        'pera'#231#227'o","Artigo de PDA","Origem DTA em opera'#231#227'o","DTA opera'#231#227'o"' +
        ',"Destino DTA em opera'#231#227'o","Baixa de DTA em opera'#231#227'o","Artigo de' +
        ' DTA","Origem PSP em processo","PSP processo","Destino PSP em pr' +
        'ocesso","Baixa de PSP em processo","Artigo de PSP","Origem RRM e' +
        'm reprocesso","RRM reprocesso","Destino RRM em reprocesso","Baix' +
        'a de RRM em reprocesso","Artigo de Reprocesso / reparo") NO_Movi' +
        'mNiv,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CliVenda, '
      'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, '
      'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, '
      
        'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) SdoIn' +
        'tei, '
      'IF(vmi.Terceiro <> 0, '
      '  IF(fmp.Tipo=0, fmp.RazaoSocial, fmp.Nome), '
      '  mfc.Nome) NO_FORNECE, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vsmovits vmi  '
      'LEFT JOIN gragrux     ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc     ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits   gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades   cli ON cli.Codigo=vmi.CliVenda '
      'LEFT JOIN entidades   fmo ON fmo.Codigo=vmi.FornecMO '
      'LEFT JOIN entidades   fmp ON fmp.Codigo=vmi.Terceiro'
      'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.TXMulFrnCab '
      'LEFT JOIN gragruxcou  xco ON xco.GraGruX=ggx.Controle '
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 '
      'WHERE vmi.MovimID=19'
      'AND vmi.MovimNiv=21'
      'AND vmi.Empresa IN (-11)'
      'AND vmi.SdoVrtPeca>0  '
      'ORDER BY NO_CliVenda, NO_FornecMO, NO_PRD_TAM_COR'
      '/********* FIM SQL *********/'
      ''
      '/*****Query sem parametros*******/')
    Left = 344
    Top = 324
    object QrOPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOPsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOPsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrOPsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrOPsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrOPsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOPsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrOPsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrOPsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrOPsLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrOPsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrOPsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrOPsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrOPsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrOPsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOPsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrOPsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrOPsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrOPsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrOPsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrOPsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrOPsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrOPsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrOPsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrOPsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrOPsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrOPsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrOPsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrOPsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrOPsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrOPsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrOPsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrOPsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrOPsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrOPsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrOPsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrOPsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrOPsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrOPsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrOPsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrOPsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrOPsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrOPsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrOPsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrOPsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrOPsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrOPsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrOPsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrOPsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrOPsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrOPsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrOPsZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrOPsEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrOPsNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrOPsFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrOPsFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrOPsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrOPsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrOPsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrOPsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOPsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOPsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOPsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOPsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOPsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOPsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOPsGSPInnNiv2: TIntegerField
      FieldName = 'GSPInnNiv2'
    end
    object QrOPsGSPArtNiv2: TIntegerField
      FieldName = 'GSPArtNiv2'
    end
    object QrOPsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrOPsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrOPsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrOPsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrOPsTXMorCab: TIntegerField
      FieldName = 'TXMorCab'
    end
    object QrOPsTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrOPsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrOPsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrOPsKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrOPsNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrOPsNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrOPsTXMulNFeCab: TIntegerField
      FieldName = 'TXMulNFeCab'
    end
    object QrOPsGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
    end
    object QrOPsJmpMovID: TIntegerField
      FieldName = 'JmpMovID'
    end
    object QrOPsJmpNivel1: TIntegerField
      FieldName = 'JmpNivel1'
    end
    object QrOPsJmpNivel2: TIntegerField
      FieldName = 'JmpNivel2'
    end
    object QrOPsJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
    end
    object QrOPsRmsMovID: TIntegerField
      FieldName = 'RmsMovID'
    end
    object QrOPsRmsNivel1: TIntegerField
      FieldName = 'RmsNivel1'
    end
    object QrOPsRmsNivel2: TIntegerField
      FieldName = 'RmsNivel2'
    end
    object QrOPsRmsGGX: TIntegerField
      FieldName = 'RmsGGX'
    end
    object QrOPsGSPSrcMovID: TIntegerField
      FieldName = 'GSPSrcMovID'
    end
    object QrOPsGSPSrcNiv2: TIntegerField
      FieldName = 'GSPSrcNiv2'
    end
    object QrOPsGSPJmpMovID: TIntegerField
      FieldName = 'GSPJmpMovID'
    end
    object QrOPsGSPJmpNiv2: TIntegerField
      FieldName = 'GSPJmpNiv2'
    end
    object QrOPsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrOPsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrOPsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrOPsNO_CliVenda: TWideStringField
      FieldName = 'NO_CliVenda'
      Size = 100
    end
    object QrOPsNO_FornecMO: TWideStringField
      FieldName = 'NO_FornecMO'
      Size = 100
    end
    object QrOPsInteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object QrOPsSdoIntei: TFloatField
      FieldName = 'SdoIntei'
    end
    object QrOPsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrOPsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object frxDsOPs: TfrxDBDataset
    UserName = 'frxDsOPs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkIDXtr=LnkIDXtr'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Zerado=Zerado'
      'EmFluxo=EmFluxo'
      'NotFluxo=NotFluxo'
      'FatNotaVNC=FatNotaVNC'
      'FatNotaVRC=FatNotaVRC'
      'PedItsLib=PedItsLib'
      'PedItsFin=PedItsFin'
      'PedItsVda=PedItsVda'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'GSPInnNiv2=GSPInnNiv2'
      'GSPArtNiv2=GSPArtNiv2'
      'CustoMOM2=CustoMOM2'
      'ReqMovEstq=ReqMovEstq'
      'StqCenLoc=StqCenLoc'
      'ItemNFe=ItemNFe'
      'TXMorCab=TXMorCab'
      'TXMulFrnCab=TXMulFrnCab'
      'ClientMO=ClientMO'
      'CustoPQ=CustoPQ'
      'KgCouPQ=KgCouPQ'
      'NFeSer=NFeSer'
      'NFeNum=NFeNum'
      'TXMulNFeCab=TXMulNFeCab'
      'GGXRcl=GGXRcl'
      'JmpMovID=JmpMovID'
      'JmpNivel1=JmpNivel1'
      'JmpNivel2=JmpNivel2'
      'JmpGGX=JmpGGX'
      'RmsMovID=RmsMovID'
      'RmsNivel1=RmsNivel1'
      'RmsNivel2=RmsNivel2'
      'RmsGGX=RmsGGX'
      'GSPSrcMovID=GSPSrcMovID'
      'GSPSrcNiv2=GSPSrcNiv2'
      'GSPJmpMovID=GSPJmpMovID'
      'GSPJmpNiv2=GSPJmpNiv2'
      'DtCorrApo=DtCorrApo'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_CliVenda=NO_CliVenda'
      'NO_FornecMO=NO_FornecMO'
      'Inteiros=Inteiros'
      'SdoIntei=SdoIntei'
      'NO_FORNECE=NO_FORNECE'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR')
    DataSet = QrOPs
    BCDToCurrency = False
    Left = 344
    Top = 372
  end
  object frxTEX_FAXAO_025_04: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  Me_GH1.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  Me_GH2.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '                                        '
      'end.')
    OnGetValue = frxTEX_FAXAO_025_04GetValue
    Left = 104
    Top = 376
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsOPs
        DataSetName = 'frxDsOPs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Ordens de Opera'#231#227'o (OO) e de Produ'#231#227'o (OP) em Aberto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 279.685220000000000000
          Top = 64.252010000000000000
          Width = 173.858292130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 3.779530000000000000
          Top = 41.574830000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 544.251953860000000000
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 498.897569450000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 453.543265590000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Top = 64.252010000000000000
          Width = 37.795275590551180000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ordem')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 136.063080000000000000
          Top = 64.252010000000000000
          Width = 143.622039920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 582.047180630000000000
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 759.685530000000000000
          Top = 64.252010000000000000
          Width = 94.488149920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornece MO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 899.527563940000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sdo '#193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 944.882500000000000000
          Top = 64.252010000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sdo Int.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 98.267780000000000000
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 854.173780000000000000
          Top = 64.252010000000000000
          Width = 45.354325830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ MO/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 102.047236770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor(es) Mat'#233'ria Prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 721.890230000000000000
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NFe rem.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 529.134200000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 668.976810000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 483.779840000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 449.763874720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE Ordens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo13: TfrxMemoView
          Left = 544.251953860000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 498.897569450000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 453.543265590000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 582.047180630000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 899.527563940000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 944.882500000000000000
          Top = 3.779530000000000000
          Width = 37.795270710000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoIntei">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsOPs."CliVenda"'
        object Me_GH1: TfrxMemoView
          Width = 990.236860000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          Memo.UTF8W = (
            '[frxDsOPs."NO_CliVenda"]')
          ParentFont = False
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 238.110390000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsOPs."FornecMO"'
        object MeGH_02_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object Me_GH2: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677311810000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          Memo.UTF8W = (
            '[frxDsOPs."NO_FornecMO"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 12.472440940000000000
        Top = 275.905690000000000000
        Width = 990.236860000000000000
        DataSet = frxDsOPs
        DataSetName = 'frxDsOPs'
        RowCount = 0
        object MeMD_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 12.472440944881890000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeMD_F02: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677800000000000000
          Height = 12.472440944881890000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeValNome: TfrxMemoView
          Left = 302.362400000000000000
          Width = 151.181102360000000000
          Height = 12.472440940000000000
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsOPs."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 279.685220000000000000
          Width = 22.677165350000000000
          Height = 12.472440940000000000
          DataField = 'GraGruX'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 544.252320000000000000
          Width = 37.795265830000000000
          Height = 12.472440940000000000
          DataField = 'Pecas'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 498.897960000000000000
          Width = 45.354330710000000000
          Height = 12.472440940000000000
          DataField = 'AreaM2'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 453.543265590000000000
          Width = 45.354330710000000000
          Height = 12.472440940000000000
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Width = 60.472440940000000000
          Height = 12.472440940000000000
          DataField = 'DataHora'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsOPs."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 7.559060000000000000
          Width = 30.236215590000000000
          Height = 12.472440940000000000
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo7: TfrxMemoView
          Left = 173.858380000000000000
          Width = 105.826774090000000000
          Height = 12.472440940000000000
          DataField = 'NO_CliVenda'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsOPs."NO_CliVenda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 136.063080000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          DataField = 'CliVenda'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."CliVenda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 582.047180630000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 759.685530000000000000
          Width = 94.488176770000000000
          Height = 12.472440940000000000
          DataField = 'NO_FornecMO'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsOPs."NO_FornecMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 634.961040000000000000
          Width = 22.677165350000000000
          Height = 12.472440944881890000
          DataField = 'FornecMO'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."FornecMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 899.527563940000000000
          Width = 45.354330710000000000
          Height = 12.472440940000000000
          DataField = 'SdoVrtArM2'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."SdoVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 944.882500000000000000
          Width = 37.795270710000000000
          Height = 12.472440944881890000
          DataField = 'SdoIntei'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."SdoIntei"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 98.267780000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          DataField = 'Controle'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo51: TfrxMemoView
          Left = 854.173780000000000000
          Width = 45.354325830000000000
          Height = 12.472440940000000000
          DataField = 'CustoMOM2'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOPs."CustoMOM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 619.842920000000000000
          Width = 102.047261180000000000
          Height = 12.472440940000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsOPs."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 721.890230000000000000
          Width = 37.795275590000000000
          Height = 12.472440940000000000
          DataField = 'NFeRem'
          DataSet = frxDsOPs
          DataSetName = 'frxDsOPs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOPs."NFeRem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        Height = 24.566929130000000000
        Top = 309.921460000000000000
        Width = 990.236860000000000000
        OnBeforePrint = 'FT_02OnBeforePrint'
        object MeFT_02_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 24.566929133858270000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeFT_02_F02: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677800000000000000
          Height = 20.787401574803150000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeGrupo2Foot: TfrxMemoView
          Left = 7.559060000000000000
          Top = 3.779530000000000000
          Width = 381.732334720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 544.251953860000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 498.897569450000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 453.543265590000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 389.291590000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'O[VARF_LETRA_OX]s: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo29: TfrxMemoView
          Left = 582.047180630000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 899.527563940000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299212598430000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 944.882500000000000000
          Top = 3.779530000000000000
          Width = 37.795270710000000000
          Height = 13.606299212598430000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoIntei">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        Height = 20.787401570000000000
        Top = 359.055350000000000000
        Width = 990.236860000000000000
        OnBeforePrint = 'FT_01OnBeforePrint'
        object MeFT_01: TfrxMemoView
          Width = 990.236371810000000000
          Height = 20.787401574803150000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeGrupo1Foot: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 385.511864720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 389.291590000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'O[VARF_LETRA_OX]s: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 544.251953860000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 498.897569450000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 453.543265590000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 582.047180630000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 899.527563940000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299212598430000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 944.882500000000000000
          Top = 3.779530000000000000
          Width = 37.795270710000000000
          Height = 13.606299212598430000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoIntei">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsOPs."MovimID"'
        object Memo54: TfrxMemoView
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'O[VARF_LETRA_OX]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 37.795300000000000000
          Width = 952.441560000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOPs."NO_MovimID"] - [frxDsOPs."NO_MovimNiv"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 17.385829210000000000
        Top = 404.409710000000000000
        Width = 990.236860000000000000
        object Memo37: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 449.763874720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE O[VARF_LETRA_OX]s: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 544.251953860000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 498.897569450000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 453.543265590000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 582.047180630000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 899.527563940000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 13.606299212598430000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoVrtArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 944.882500000000000000
          Top = 3.779530000000000000
          Width = 37.795270710000000000
          Height = 13.606299212598430000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOPs."SdoIntei">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxReport1: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42285.580445347230000000
    ReportOptions.LastChange = 42285.580445347230000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 468
    Top = 500
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118207880000000000
        Top = 102.047310000000000000
        Width = 1046.929810000000000000
        DataSetName = 'frxDs14VSMovIts'
        RowCount = 0
        object MeMD_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeMD_F02: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677800000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeValNome: TfrxMemoView
          Left = 377.953000000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 343.937230000000000000
          Width = 34.015738270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs14VSMovIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 582.047620000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 929.764380000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 196.535560000000000000
          Width = 147.401584570000000000
          Height = 15.118110240000000000
          DataField = 'NO_FORNECE'
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 162.519790000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Terceiro'
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 702.992580000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."MediaKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 755.906000000000000000
          Top = 0.000097639999999996
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'QtdGerArM2'
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."QtdGerArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 823.937540000000000000
          Top = 0.000097639999999996
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."MediaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 49.133890000000000000
          Width = 113.385824330000000000
          Height = 15.118110240000000000
          DataField = 'NO_SERIE_FICHA'
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_SERIE_FICHA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 7.559060000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDs14VSMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 642.520100000000000000
          Top = 0.000097639999999996
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 876.850960000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DataField = 'NotaMPAG'
          DataSetName = 'frxDs14VSMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NotaMPAG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 275.905690000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Me_FTT: TfrxMemoView
          Top = 3.779530000000000000
          Width = 514.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE ITENS: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePesoKg_T: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeAreaM2_T: TfrxMemoView
          Left = 755.906000000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeInteiros_T: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779627640000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 929.764380000000000000
          Top = 3.779530000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."BaseMPAG">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiro' +
              's">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 823.937540000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."QtdGerArM2">,MD002,1) / SUM(<frxDs14VSMovIts."Intei' +
              'ros">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVNC_T: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.5n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."MediaKg">,MD002,1) > 0, SUM(<frxDs14V' +
              'SMovIts."SumVNC">,MD002,1) / SUM(<frxDs14VSMovIts."MediaKg">,MD0' +
              '02,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVRC_T: TfrxMemoView
          Left = 71.811070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.5n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."PesoKg">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiros"' +
              '>,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."MediaKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNotaMPAG_T: TfrxMemoView
          Left = 876.850960000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        Condition = 'frxDs14VSMovIts."GraGruX"'
        object Me_GH1: TfrxMemoView
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.456692910000000000
        Top = 188.976500000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'FT_01OnBeforePrint'
        object MeFT_01: TfrxMemoView
          Width = 990.236371810000000000
          Height = 22.677167800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object Me_FT1: TfrxMemoView
          Left = 7.559060000000000000
          Top = 3.779530000000000000
          Width = 423.307164720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 430.866420000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePesoKg_1: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeAreaM2_1: TfrxMemoView
          Left = 755.906000000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeInteiros_1: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779627640000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 929.764380000000000000
          Top = 3.779530000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."BaseMPAG">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiro' +
              's">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 823.937540000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."QtdGerArM2">,MD002,1) / SUM(<frxDs14VSMovIts."Intei' +
              'ros">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVNC_1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVNC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVRC_1: TfrxMemoView
          Left = 71.811070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVRC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."PesoKg">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiros"' +
              '>,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."MediaKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNotaMPAG_1: TfrxMemoView
          Left = 876.850960000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.007874020000000000
        Top = 60.472480000000000000
        Width = 1046.929810000000000000
        Condition = 'frxDs14VSMovIts."GraGruX"'
        object MeGH_02_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object Me_GH2: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677311810000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          Memo.UTF8W = (
            '[frxDs14VSMovIts."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 139.842610000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'FT_02OnBeforePrint'
        object MeFT_02_F01: TfrxMemoView
          Width = 990.236860000000000000
          Height = 26.456695350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeFT_02_F02: TfrxMemoView
          Left = 3.779530000000000000
          Width = 982.677800000000000000
          Height = 22.677167800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object Me_FT2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 3.779530000000000000
          Width = 423.307164720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePesoKg_2: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 430.866420000000000000
          Top = 3.779530000000000000
          Width = 83.149606300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."PesoKg">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiros"' +
              '>,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 929.764380000000000000
          Top = 3.779530000000000000
          Width = 52.913376060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."BaseMPAG">,MD002,1) / SUM(<frxDs14VSMovIts."Inteiro' +
              's">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeAreaM2_2: TfrxMemoView
          Left = 755.906000000000000000
          Top = 3.779627640000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 823.937540000000000000
          Top = 3.779627640000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."Inteiros">,MD002,1) = 0, 0, SUM(<frxD' +
              's14VSMovIts."QtdGerArM2">,MD002,1) / SUM(<frxDs14VSMovIts."Intei' +
              'ros">,MD002,1))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeInteiros_2: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779627640000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."Inteiros">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVNC_2: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVNC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSumVRC_2: TfrxMemoView
          Left = 71.811070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."SumVRC">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 181.417440000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."MediaKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeNotaMPAG_2: TfrxMemoView
          Left = 876.850960000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1) > 0, SUM(<frxDs' +
              '14VSMovIts."SumVRC">,MD002,1) / SUM(<frxDs14VSMovIts."QtdGerArM2' +
              '">,MD002,1), 0)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxTEX_FAXAO_025_02_B: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41905.510812419000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxTEX_FAXAO_025_01GetValue
    Left = 104
    Top = 220
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
      end
      item
        DataSet = frxDsOld01
        DataSetName = 'frxDsOld01'
      end
      item
        DataSet = frxDsOld02
        DataSetName = 'frxDsOld02'
      end
      item
        DataSet = frxDsOld03
        DataSetName = 'frxDsOld03'
      end
      item
        DataSet = frxDsOld04
        DataSetName = 'frxDsOld04'
      end
      item
        DataSet = frxDsOld05
        DataSetName = 'frxDsOld05'
      end
      item
        DataSet = frxDsTXDesclDst
        DataSetName = 'frxDsTXDesclDst'
      end
      item
        DataSet = frxDsTXPWEDst
        DataSetName = 'frxDsTXPWEDst'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 378.055350000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 22.779530000000000000
          Width = 680.315400000000000000
          Height = 355.275820000000000000
          Curve = 1
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 132.283550000000000000
          Top = 37.897650000000000000
          Width = 415.748300000000000000
          Height = 41.574830000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Romaneio de [frxDsEstqR4."NO_TIPO_COURO"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 64.354360000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 200.315090000000000000
          Top = 64.354360000000000000
          Width = 468.661656540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item de pedido: [frxDsEstqR4."PedItsLib"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 83.440940000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'IME-I  [frxDsEstqR4."Controle"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 123.661410000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 230.551181100000000000
          Width = 132.283464570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 162.519675280000000000
          Width = 94.488188980000000000
          Height = 68.031496060000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -37
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Top = 257.110216690000000000
          Width = 132.283464566929100000
          Height = 52.913395590000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 132.283427950000000000
          Top = 257.007891100000000000
          Width = 415.748104720000000000
          Height = 52.913395590000000000
          DataField = 'Observ'
          DataSet = frxDsEstqR4
          DataSetName = 'frxDsEstqR4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Observ"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo22: TfrxMemoView
          Left = 94.488188980000000000
          Top = 162.519675280000000000
          Width = 585.826788740000000000
          Height = 68.031503390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."GraGruX"] - [frxDsEstqR4."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR4."Pes' +
              'oKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 94.488171890000000000
          Top = 124.102350000000000000
          Width = 585.826788740000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 124.102350000000000000
          Width = 94.488188980000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 26.559060000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 548.031850000000000000
          Top = 230.551181100000000000
          Width = 132.283464570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 548.031850000000000000
          Top = 257.008018030000000000
          Width = 132.283464570000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_FICHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 241.889920000000000000
          Top = 230.551330000000000000
          Width = 306.141844570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_LPFMO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 132.283550000000000000
          Top = 230.551330000000000000
          Width = 109.606284570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Lote de produ'#231#227'o do fornecedor da m'#227'o de obra:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 147.401670000000000000
          Top = 83.252010000000000000
          Width = 529.133760630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              'RME: [frxDsEstqR4."ReqMovEstq"]  IME-C: [frxDsEstqR4."MovimCod"]' +
              '  [frxDsEstqR4."NO_ACAO_COURO"]: [frxDsEstqR4."Codigo"]    ...')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
      end
      object DD001: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 461.102660000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTXPWEDst
        DataSetName = 'frxDsTXPWEDst'
        RowCount = 0
        object Memo45: TfrxMemoView
          Left = 117.165430000000000000
          Width = 188.976377952755900000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXPWEDst."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 306.141930000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXPWEDst."Pecas"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 366.614410000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[-<frxDsTXPWEDst."AreaM2WB">]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 461.102660000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXPWEDst."AreaM2"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Width = 52.913385826771650000
          Height = 18.897650000000000000
          DataField = 'DataHora'
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXPWEDst."DataHora"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 52.913420000000000000
          Width = 64.251944090000000000
          Height = 18.897650000000000000
          DataField = 'Pallet'
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXPWEDst."Pallet"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 631.181510000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXPWEDst."PercRendim"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 555.590910000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsTXPWEDst."AreaM2"> + <frxDsTXPWEDst."AreaM2WB">]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 419.527830000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsTXPWEDst."Codigo"'
        object MeOldC3L0: TfrxMemoView
          Left = 117.165430000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
        end
        object MeOldC4L0: TfrxMemoView
          Left = 306.141930000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object MeOldC5L0: TfrxMemoView
          Left = 366.614410000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' WB')
          ParentFont = False
        end
        object MeOldC6L0: TfrxMemoView
          Left = 461.102660000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178' Pronto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 52.913420000000000000
          Width = 64.251944090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 631.181510000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Rend.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 555.590910000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Rend. m'#178)
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 623.622450000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          Left = 306.141930000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTXPWEDst."Pecas">,DD001)]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 366.614410000000000000
          Top = 3.779530000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(-<frxDsTXPWEDst."AreaM2WB">,DD001)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 461.102660000000000000
          Top = 3.779530000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTXPWEDst."AreaM2">,DD001)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 631.181510000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsTXPWEDst."AreaM2">,DD001) + SUM(<frxDsTXPWEDst."Area' +
              'M2WB">,DD001)) / SUM(-<frxDsTXPWEDst."AreaM2WB">,DD001) * 100]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 555.590910000000000000
          Top = 3.779530000000000000
          Width = 75.590568270000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsTXPWEDst."AreaM2">,DD001) + SUM(<frxDsTXPWEDst."Area' +
              'M2WB">,DD001))]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Top = 3.779530000000000000
          Width = 306.141807950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL  ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 306.141930000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR4."SdoVrtPeca"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 366.614410000000000000
          Top = 22.677180000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXPWEDst
          DataSetName = 'frxDsTXPWEDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqR4."SdoVrtArM2"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Top = 22.677180000000000000
          Width = 306.141807950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO EM PROCESSO  ')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 502.677490000000000000
        Width = 680.315400000000000000
        object Memo42: TfrxMemoView
          Left = 117.165430000000000000
          Top = 7.559060000000000000
          Width = 313.700867950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Couro Desclassificado')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 430.866420000000000000
          Top = 7.559060000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 491.338900000000000000
          Top = 7.559060000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 585.827150000000000000
          Top = 7.559060000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Top = 7.559060000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 52.913420000000000000
          Top = 7.559060000000000000
          Width = 64.251944090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 551.811380000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTXDesclDst
        DataSetName = 'frxDsTXDesclDst'
        RowCount = 0
        object Memo34: TfrxMemoView
          Left = 117.165430000000000000
          Width = 313.700867950000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsTXDesclDst
          DataSetName = 'frxDsTXDesclDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXDesclDst."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 430.866420000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsTXDesclDst
          DataSetName = 'frxDsTXDesclDst'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXDesclDst."Pecas"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 491.338900000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDsTXDesclDst
          DataSetName = 'frxDsTXDesclDst'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXDesclDst."PesoKg"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 585.827150000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataField = 'AreaM2'
          DataSet = frxDsTXDesclDst
          DataSetName = 'frxDsTXDesclDst'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXDesclDst."AreaM2"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataField = 'DataHora'
          DataSet = frxDsTXDesclDst
          DataSetName = 'frxDsTXDesclDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXDesclDst."DataHora"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 52.913420000000000000
          Width = 64.251944090000000000
          Height = 18.897650000000000000
          DataField = 'Pallet'
          DataSet = frxDsTXDesclDst
          DataSetName = 'frxDsTXDesclDst'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXDesclDst."Pallet"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrTXPWEDst: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTXPWEDstCalcFields
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 664
    Top = 217
    object QrTXPWEDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXPWEDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXPWEDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXPWEDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXPWEDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXPWEDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXPWEDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXPWEDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXPWEDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXPWEDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXPWEDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXPWEDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXPWEDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXPWEDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXPWEDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXPWEDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXPWEDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrTXPWEDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXPWEDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrTXPWEDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrTXPWEDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrTXPWEDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXPWEDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXPWEDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXPWEDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXPWEDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXPWEDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrTXPWEDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrTXPWEDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrTXPWEDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXPWEDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXPWEDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXPWEDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXPWEDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXPWEDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrTXPWEDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXPWEDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXPWEDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXPWEDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXPWEDstAreaM2WB: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AreaM2WB'
      Calculated = True
    end
    object QrTXPWEDstPercRendim: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercRendim'
      Calculated = True
    end
  end
  object frxDsTXPWEDst: TfrxDBDataset
    UserName = 'frxDsTXPWEDst'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq'
      'PedItsFin=PedItsFin'
      'Marca=Marca'
      'StqCenLoc=StqCenLoc'
      'AreaM2WB=AreaM2WB'
      'PercRendim=PercRendim')
    DataSet = QrTXPWEDst
    BCDToCurrency = False
    Left = 668
    Top = 268
  end
  object QrTXPWEBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 740
    Top = 217
    object QrTXPWEBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXPWEBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXPWEBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXPWEBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXPWEBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXPWEBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXPWEBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXPWEBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXPWEBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXPWEBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXPWEBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXPWEBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXPWEBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXPWEBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXPWEBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXPWEBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXPWEBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrTXPWEBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXPWEBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrTXPWEBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrTXPWEBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrTXPWEBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXPWEBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXPWEBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXPWEBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXPWEBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXPWEBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrTXPWEBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrTXPWEBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXPWEBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXPWEBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrTXPWEBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXPWEBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXPWEBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXPWEBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXPWEBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXPWEBxaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrTXPWEBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object frxDsTXPWEBxa: TfrxDBDataset
    UserName = 'frxDsTXPWEBxa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq')
    DataSet = QrTXPWEBxa
    BCDToCurrency = False
    Left = 744
    Top = 268
  end
  object frxTEX_FAXAO_025_05: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41905.510812419000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Imprime: Boolean;'
      '  FItens: Extended;                                   '
      '    '
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  //Line3.Visible := Imprime;                         '
      
        '  Imprime := not Imprime;                                       ' +
        '                                                                ' +
        '                  '
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  FItens := <VARF_SoCincoMostrados>;                            ' +
        '  '
      '  //Me_SoCincoMostrados.Visible := FItens > 5;'
      '  //        '
      '  Imprime := True;'
      '  //'
      '(*'
      '  MeOldC1L0.Visible := FItens >= 1;'
      
        '  MeOldC1L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC2L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC3L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC4L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC5L0.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC6L0.Visible := FItens >= 1;                             ' +
        '                         '
      '  MeOldC7L0.Visible := FItens >= 1;'
      '  //'
      
        '  MeOldC1L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC1L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC2L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC3L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC4L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC5L1.Visible := FItens >= 1;                             ' +
        '                         '
      
        '  MeOldC6L1.Visible := FItens >= 1;                             ' +
        '                         '
      '  MeOldC7L1.Visible := FItens >= 1;'
      '  //        '
      
        '  MeOldC1L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC1L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC2L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC3L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC4L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC5L2.Visible := FItens >= 2;                             ' +
        '                         '
      
        '  MeOldC6L2.Visible := FItens >= 2;                             ' +
        '                         '
      '  MeOldC7L2.Visible := FItens >= 2;'
      '  //        '
      
        '  MeOldC1L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC2L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC3L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC4L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC5L3.Visible := FItens >= 3;                             ' +
        '                         '
      
        '  MeOldC6L3.Visible := FItens >= 3;                             ' +
        '                         '
      '  MeOldC7L3.Visible := FItens >= 3;'
      '  //        '
      
        '  MeOldC1L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC2L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC3L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC4L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC5L4.Visible := FItens >= 4;                             ' +
        '                         '
      
        '  MeOldC6L4.Visible := FItens >= 4;                             ' +
        '                         '
      '  MeOldC7L4.Visible := FItens >= 4;'
      '  //        '
      
        '  MeOldC1L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC2L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC3L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC4L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC5L5.Visible := FItens >= 5;                             ' +
        '                         '
      
        '  MeOldC6L5.Visible := FItens >= 5;                             ' +
        '                         '
      '  MeOldC7L5.Visible := FItens >= 5;'
      '  //'
      '  *)        '
      'end;'
      ''
      'procedure BarCode1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  BarCode1.Text := <VARF_CODIGO_BARRA>;  '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxTEX_FAXAO_025_01GetValue
    Left = 104
    Top = 424
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
      end
      item
        DataSet = frxDsOld01
        DataSetName = 'frxDsOld01'
      end
      item
        DataSet = frxDsOld02
        DataSetName = 'frxDsOld02'
      end
      item
        DataSet = frxDsOld03
        DataSetName = 'frxDsOld03'
      end
      item
        DataSet = frxDsOld04
        DataSetName = 'frxDsOld04'
      end
      item
        DataSet = frxDsOld05
        DataSetName = 'frxDsOld05'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 1039.370413150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
        RowCount = 0
        object Memo47: TfrxMemoView
          Left = 132.283550000000000000
          Top = 26.559060000000000000
          Width = 415.748300000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Romaneio de [frxDsEstqR4."NO_TIPO_COURO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 30.338590000000010000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 483.779840000000000000
          Top = 30.338590000000010000
          Width = 177.637846540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item [Line#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 56.984229999999990000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'IME-I  [frxDsEstqR4."Controle"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 98.267699450000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 204.094463780000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 188.976377950000000000
          Top = 204.094463780000000000
          Width = 302.362204720000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados do Fornecedor de MO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 135.866110000000000000
          Width = 26.456631890000000000
          Height = 68.031496062992100000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 283.566904720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 283.566904720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 283.566904720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 283.566904720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 230.551181102362200000
          Width = 188.976377950000000000
          Height = 52.913395590000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 188.976377950000000000
          Top = 230.551181102362200000
          Width = 302.362204720000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_LPFMO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 26.456631890000000000
          Top = 135.866110000000000000
          Width = 442.204648740000000000
          Height = 68.031496062992100000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -29
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."GraGruX"] - [frxDsEstqR4."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 313.803122760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 313.803122760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 313.803122760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR4."Pes' +
              'oKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 313.803122760000000000
          Width = 170.078740160000000000
          Height = 37.795275590000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 98.267701890000000000
          Top = 98.267699450000000000
          Width = 370.393578740000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 98.267699450000000000
          Width = 98.267701890000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 19.000000000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 249.448980000000000000
          Top = 53.015770000000010000
          Width = 411.968330630000000000
          Height = 41.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              'IME-C: [frxDsEstqR4."MovimCod"] [frxDsEstqR4."NO_ACAO_COURO"]: [' +
              'frxDsEstqR4."Codigo"]    ')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo10: TfrxMemoView
          Left = 491.338900000000000000
          Top = 204.094463780000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 491.338900000000000000
          Top = 230.551181102362200000
          Width = 188.976377950000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_FICHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode1: TfrxBarCodeView
          Left = 510.236550000000000000
          Top = 109.606370000000000000
          Width = 154.500000000000000000
          Height = 83.149660000000000000
          OnBeforePrint = 'BarCode1OnBeforePrint'
          BarType = bcCodeEAN13
          CalcCheckSum = True
          Rotation = 0
          TestLine = False
          Text = '0'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.500000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo12: TfrxMemoView
          Left = 468.661720000000000000
          Top = 98.267780000000000000
          Width = 22.677180000000000000
          Height = 105.826840000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DV IME-I: [VARF_DV_DO_IMEI]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
          VAlign = vaCenter
        end
        object MeOldC3L1: TfrxMemoView
          Left = 200.315090000000000000
          Top = 374.173470000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC3L0: TfrxMemoView
          Left = 200.315090000000000000
          Top = 355.275820000000000000
          Width = 188.976312050000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
        end
        object MeOldC4L0: TfrxMemoView
          Left = 389.291590000000000000
          Top = 355.275820000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object MeOldC4L1: TfrxMemoView
          Left = 389.291590000000000000
          Top = 374.173470000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC5L0: TfrxMemoView
          Left = 472.441250000000000000
          Top = 355.275820000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178' WB')
          ParentFont = False
        end
        object MeOldC5L1: TfrxMemoView
          Left = 472.441250000000000000
          Top = 374.173470000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object MeOldC6L1: TfrxMemoView
          Left = 574.488560000000000000
          Top = 374.173470000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeOldC6L0: TfrxMemoView
          Left = 574.488560000000000000
          Top = 355.275820000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'kg Raspa')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 3.779530000000000000
          Top = 355.275820000000000000
          Width = 94.488127950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 3.779530000000000000
          Top = 374.173470000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 200.315090000000000000
          Top = 400.630180000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 389.291590000000000000
          Top = 400.630180000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 472.441250000000000000
          Top = 400.630180000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 574.488560000000000000
          Top = 400.630180000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 3.779530000000000000
          Top = 400.630180000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 200.315090000000000000
          Top = 427.086890000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 389.291590000000000000
          Top = 427.086890000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 472.441250000000000000
          Top = 427.086890000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 574.488560000000000000
          Top = 427.086890000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 3.779530000000000000
          Top = 427.086890000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 200.315090000000000000
          Top = 453.543600000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 389.291590000000000000
          Top = 453.543600000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 472.441250000000000000
          Top = 453.543600000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 574.488560000000000000
          Top = 453.543600000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 3.779530000000000000
          Top = 453.543600000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 200.315090000000000000
          Top = 480.000310000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 389.291590000000000000
          Top = 480.000310000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 472.441250000000000000
          Top = 480.000310000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 574.488560000000000000
          Top = 480.000310000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 3.779530000000000000
          Top = 480.000310000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 200.315090000000000000
          Top = 506.457019999999900000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 389.291590000000000000
          Top = 506.457019999999900000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 472.441250000000000000
          Top = 506.457019999999900000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 574.488560000000000000
          Top = 506.457019999999900000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 3.779530000000000000
          Top = 506.457019999999900000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 200.315090000000000000
          Top = 532.913730000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 389.291590000000000000
          Top = 532.913730000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 472.441250000000000000
          Top = 532.913730000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 574.488560000000000000
          Top = 532.913730000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 3.779530000000000000
          Top = 532.913730000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 200.315090000000000000
          Top = 559.370440000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 389.291590000000000000
          Top = 559.370440000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 472.441250000000000000
          Top = 559.370440000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 574.488560000000000000
          Top = 559.370440000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 3.779530000000000000
          Top = 559.370440000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 200.315090000000000000
          Top = 585.827150000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 389.291590000000000000
          Top = 585.827150000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 472.441250000000000000
          Top = 585.827150000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 574.488560000000000000
          Top = 585.827150000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 3.779530000000000000
          Top = 585.827150000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 200.315090000000000000
          Top = 612.283860000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 389.291590000000000000
          Top = 612.283860000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 472.441250000000000000
          Top = 612.283860000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 574.488560000000000000
          Top = 612.283860000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 3.779530000000000000
          Top = 612.283860000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 200.315090000000000000
          Top = 638.740570000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 389.291590000000000000
          Top = 638.740570000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 472.441250000000000000
          Top = 638.740570000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 574.488560000000000000
          Top = 638.740570000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 3.779530000000000000
          Top = 638.740570000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 200.315090000000000000
          Top = 665.197280000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 389.291590000000000000
          Top = 665.197280000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 472.441250000000000000
          Top = 665.197280000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 574.488560000000000000
          Top = 665.197280000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 3.779530000000000000
          Top = 665.197280000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 200.315090000000000000
          Top = 691.653990000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 389.291590000000000000
          Top = 691.653990000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 472.441250000000000000
          Top = 691.653990000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 574.488560000000000000
          Top = 691.653990000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 3.779530000000000000
          Top = 691.653990000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 200.315090000000000000
          Top = 718.110700000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 389.291590000000000000
          Top = 718.110700000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 472.441250000000000000
          Top = 718.110700000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 574.488560000000000000
          Top = 718.110700000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 3.779530000000000000
          Top = 718.110700000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 200.315090000000000000
          Top = 744.567410000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 389.291590000000000000
          Top = 744.567410000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 472.441250000000000000
          Top = 744.567410000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 574.488560000000000000
          Top = 744.567410000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 3.779530000000000000
          Top = 744.567410000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 200.315090000000000000
          Top = 771.024120000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 389.291590000000000000
          Top = 771.024120000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Left = 472.441250000000000000
          Top = 771.024120000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 574.488560000000000000
          Top = 771.024120000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 3.779530000000000000
          Top = 771.024120000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 200.315090000000000000
          Top = 797.480830000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 389.291590000000000000
          Top = 797.480830000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 472.441250000000000000
          Top = 797.480830000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 574.488560000000000000
          Top = 797.480830000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Left = 3.779530000000000000
          Top = 797.480830000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 200.315090000000000000
          Top = 823.937540000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Left = 389.291590000000000000
          Top = 823.937540000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Left = 472.441250000000000000
          Top = 823.937540000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Left = 574.488560000000000000
          Top = 823.937540000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Left = 3.779530000000000000
          Top = 823.937540000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Left = 200.315090000000000000
          Top = 850.394250000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 389.291590000000000000
          Top = 850.394250000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Left = 472.441250000000000000
          Top = 850.394250000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Left = 574.488560000000000000
          Top = 850.394250000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Left = 3.779530000000000000
          Top = 850.394250000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Left = 200.315090000000000000
          Top = 876.850960000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Left = 389.291590000000000000
          Top = 876.850960000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 472.441250000000000000
          Top = 876.850960000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 574.488560000000000000
          Top = 876.850960000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Left = 3.779530000000000000
          Top = 876.850960000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 200.315090000000000000
          Top = 903.307670000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 389.291590000000000000
          Top = 903.307670000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Left = 472.441250000000000000
          Top = 903.307670000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 574.488560000000000000
          Top = 903.307670000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 3.779530000000000000
          Top = 903.307670000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 200.315090000000000000
          Top = 929.764380000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 389.291590000000000000
          Top = 929.764380000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Left = 472.441250000000000000
          Top = 929.764380000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 574.488560000000000000
          Top = 929.764380000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Left = 3.779530000000000000
          Top = 929.764380000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Left = 200.315090000000000000
          Top = 956.221090000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Left = 389.291590000000000000
          Top = 956.221090000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Left = 472.441250000000000000
          Top = 956.221090000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Left = 574.488560000000000000
          Top = 956.221090000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Left = 3.779530000000000000
          Top = 956.221090000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Left = 200.315090000000000000
          Top = 982.677800000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Left = 389.291590000000000000
          Top = 982.677800000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Left = 472.441250000000000000
          Top = 982.677800000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Left = 574.488560000000000000
          Top = 982.677800000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Left = 3.779530000000000000
          Top = 982.677800000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Left = 200.315090000000000000
          Top = 1009.134510000000000000
          Width = 188.976377950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Left = 389.291590000000000000
          Top = 1009.134510000000000000
          Width = 83.149606300000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Left = 472.441250000000000000
          Top = 1009.134510000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Left = 574.488560000000000000
          Top = 1009.134510000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 3.779530000000000000
          Top = 1009.134510000000000000
          Width = 94.488127950000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Left = 98.267780000000000000
          Top = 374.173470000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Left = 98.267780000000000000
          Top = 400.630180000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Left = 98.267780000000000000
          Top = 427.086890000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Left = 98.267780000000000000
          Top = 453.543600000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 98.267780000000000000
          Top = 480.000310000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Left = 98.267780000000000000
          Top = 506.457020000000100000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Left = 98.267780000000000000
          Top = 532.913730000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Left = 98.267780000000000000
          Top = 559.370440000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Left = 98.267780000000000000
          Top = 585.827150000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Left = 98.267780000000000000
          Top = 612.283860000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Left = 98.267780000000000000
          Top = 638.740570000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 98.267780000000000000
          Top = 665.197280000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 98.267780000000000000
          Top = 691.653990000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 98.267780000000000000
          Top = 718.110700000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 98.267780000000000000
          Top = 744.567410000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 98.267780000000000000
          Top = 771.024120000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 98.267780000000000000
          Top = 797.480830000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 98.267780000000000000
          Top = 823.937540000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 98.267780000000000000
          Top = 850.394250000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 98.267780000000000000
          Top = 876.850960000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Left = 98.267780000000000000
          Top = 903.307670000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Left = 98.267780000000000000
          Top = 929.764380000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Left = 98.267780000000000000
          Top = 956.221090000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Left = 98.267780000000000000
          Top = 982.677800000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Left = 98.267780000000000000
          Top = 1009.134510000000000000
          Width = 102.047244090000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Left = 98.267780000000000000
          Top = 355.275820000000000000
          Width = 102.047122050000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
        end
      end
    end
  end
  object frxTEX_FAXAO_025_02_C: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41608.425381412000000000
    ReportOptions.LastChange = 41905.510812419000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Line3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxTEX_FAXAO_025_01GetValue
    Left = 104
    Top = 272
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
      end
      item
        DataSet = frxDsOld01
        DataSetName = 'frxDsOld01'
      end
      item
        DataSet = frxDsOld02
        DataSetName = 'frxDsOld02'
      end
      item
        DataSet = frxDsOld03
        DataSetName = 'frxDsOld03'
      end
      item
        DataSet = frxDsOld04
        DataSetName = 'frxDsOld04'
      end
      item
        DataSet = frxDsOld05
        DataSetName = 'frxDsOld05'
      end
      item
        DataSet = frxDsTXCurOriIMEI
        DataSetName = 'frxDsTXCurOriIMEI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 378.055350000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsEstqR4
        DataSetName = 'frxDsEstqR4'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 22.779530000000000000
          Width = 680.315400000000000000
          Height = 355.275820000000000000
          Curve = 1
          Shape = skRoundRectangle
        end
        object Memo47: TfrxMemoView
          Left = 132.283550000000000000
          Top = 37.897650000000000000
          Width = 415.748300000000000000
          Height = 41.574830000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Romaneio de [frxDsEstqR4."NO_TIPO_COURO"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 64.354360000000000000
          Width = 181.417376540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 200.315090000000000000
          Top = 64.354360000000000000
          Width = 468.661656540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Item de pedido: [frxDsEstqR4."PedItsLib"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 20.015770000000000000
          Top = 83.440940000000000000
          Width = 411.968330630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'IME-I  [frxDsEstqR4."Controle"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.118120000000000000
          Top = 123.661410000000000000
          Width = 679.086580000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo2: TfrxMemoView
          Top = 230.551181100000000000
          Width = 132.283464570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 162.519675280000000000
          Width = 94.488188980000000000
          Height = 68.031496060000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -37
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Artigo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 340.157480310000000000
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 510.236220470000000000
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 170.078740160000000000
          Top = 310.023614720000000000
          Width = 170.078740160000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Top = 257.110216690000000000
          Width = 132.283464566929100000
          Height = 52.913395590000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 132.283427950000000000
          Top = 257.007891100000000000
          Width = 415.748104720000000000
          Height = 52.913395590000000000
          DataField = 'Observ'
          DataSet = frxDsEstqR4
          DataSetName = 'frxDsEstqR4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Observ"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo22: TfrxMemoView
          Left = 94.488188980000000000
          Top = 162.519675280000000000
          Width = 585.826788740000000000
          Height = 68.031503390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."GraGruX"] - [frxDsEstqR4."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo24: TfrxMemoView
          Left = 340.157480310000000000
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236220470000000000
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###.###;-#,###,###.###; '#39', <frxDsEstqR4."Pes' +
              'oKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 170.078740160000000000
          Top = 340.259832760000000000
          Width = 170.078740160000000000
          Height = 34.015748030000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -27
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 94.488171890000000000
          Top = 124.102350000000000000
          Width = 585.826788740000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_EMPRESA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Top = 124.102350000000000000
          Width = 94.488188980000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente Interno: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Top = 26.559060000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 548.031850000000000000
          Top = 230.551181100000000000
          Width = 132.283464570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 548.031850000000000000
          Top = 257.008018030000000000
          Width = 132.283464570000000000
          Height = 52.913395590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqR4."NO_FICHA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 241.889920000000000000
          Top = 230.551330000000000000
          Width = 306.141844570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_LPFMO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 132.283550000000000000
          Top = 230.551330000000000000
          Width = 109.606284570000000000
          Height = 26.456685590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Lote de produ'#231#227'o do fornecedor da m'#227'o de obra:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 147.401670000000000000
          Top = 83.252010000000000000
          Width = 529.133760630000000000
          Height = 38.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              'RME: [frxDsEstqR4."ReqMovEstq"]  IME-C: [frxDsEstqR4."MovimCod"]' +
              '  [frxDsEstqR4."NO_ACAO_COURO"]: [frxDsEstqR4."Codigo"]    ...')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTXCurOriIMEI
        DataSetName = 'frxDsTXCurOriIMEI'
        RowCount = 0
        object Memo45: TfrxMemoView
          Left = 117.165430000000000000
          Width = 128.503917480000000000
          Height = 18.897650000000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 408.189240000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'Pecas'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."Pecas"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 468.661720000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'PesoKg'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."PesoKg"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Width = 52.913385826771650000
          Height = 18.897650000000000000
          DataField = 'DataHora'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."DataHora"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 52.913420000000000000
          Width = 64.251944090000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."Controle"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 544.252320000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'QtdAntPeca'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."QtdAntPeca"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'QtdAntPeso'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."QtdAntPeso"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 245.669450000000000000
          Width = 98.267694570000000000
          Height = 18.897650000000000000
          DataField = 'NO_SerieFch'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."NO_SerieFch"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 343.937230000000000000
          Width = 64.251924570000000000
          Height = 18.897650000000000000
          DataField = 'Ficha'
          DataSet = frxDsTXCurOriIMEI
          DataSetName = 'frxDsTXCurOriIMEI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXCurOriIMEI."Ficha"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 419.527830000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsTXCurOriIMEI."Codigo"'
        object MeOldC3L0: TfrxMemoView
          Left = 117.165430000000000000
          Width = 128.503917480000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeOldC4L0: TfrxMemoView
          Left = 408.189240000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeOldC5L0: TfrxMemoView
          Left = 468.661720000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Width = 52.913385830000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 52.913420000000000000
          Width = 64.251944090000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 544.252320000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 604.724800000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso Kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 245.669450000000000000
          Width = 162.519704570000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 408.189240000000000000
          Width = 136.063040940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Baixa')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 544.252320000000000000
          Width = 136.063040940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gera'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 521.575140000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrTXCurOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 420
    Top = 321
    object QrTXCurOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXCurOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXCurOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXCurOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXCurOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXCurOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXCurOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXCurOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXCurOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXCurOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXCurOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXCurOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXCurOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXCurOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXCurOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXCurOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXCurOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXCurOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXCurOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrTXCurOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXCurOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXCurOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrTXCurOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrTXCurOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrTXCurOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXCurOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXCurOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXCurOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXCurOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXCurOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrTXCurOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXCurOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrTXCurOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXCurOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXCurOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrTXCurOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXCurOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXCurOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXCurOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXCurOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXCurOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrTXCurOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXCurOriIMEICustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object frxDsTXCurOriIMEI: TfrxDBDataset
    UserName = 'frxDsTXCurOriIMEI'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq'
      'CustoPQ=CustoPQ')
    DataSet = QrTXCurOriIMEI
    BCDToCurrency = False
    Left = 420
    Top = 372
  end
  object QrTXDesclDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 824
    Top = 217
    object QrTXDesclDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXDesclDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXDesclDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXDesclDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXDesclDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXDesclDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXDesclDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXDesclDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXDesclDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXDesclDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXDesclDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXDesclDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXDesclDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXDesclDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXDesclDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXDesclDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXDesclDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXDesclDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXDesclDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrTXDesclDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXDesclDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXDesclDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrTXDesclDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrTXDesclDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrTXDesclDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXDesclDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXDesclDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXDesclDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXDesclDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXDesclDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrTXDesclDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXDesclDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrTXDesclDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXDesclDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXDesclDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrTXDesclDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXDesclDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXDesclDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXDesclDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXDesclDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXDesclDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrTXDesclDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXDesclDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXDesclDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXDesclDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
  end
  object frxDsTXDesclDst: TfrxDBDataset
    UserName = 'frxDsTXDesclDst'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq'
      'PedItsFin=PedItsFin'
      'Marca=Marca'
      'StqCenLoc=StqCenLoc')
    DataSet = QrTXDesclDst
    BCDToCurrency = False
    Left = 824
    Top = 268
  end
end
