unit TXIndOri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, dmkDBGridZTO, UnProjGroup_Consts, UnAppEnums;

type
  TFmTXIndOri = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    GBAptos: TGroupBox;
    Panel5: TPanel;
    LaGraGruX: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    LaTerceiro: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    BtReabre: TBitBtn;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    Label6: TLabel;
    LaQtde: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdQtde: TdmkEdit;
    EdObserv: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    DBG04Estq: TdmkDBGridZTO;
    QrEstoque: TmySQLQuery;
    QrEstoqueControle: TIntegerField;
    QrEstoqueEmpresa: TIntegerField;
    QrEstoqueGraGruX: TIntegerField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorT: TFloatField;
    QrEstoqueGraGru1: TIntegerField;
    QrEstoqueNO_PRD_TAM_COR: TWideStringField;
    QrEstoquePallet: TIntegerField;
    QrEstoqueNO_Pallet: TWideStringField;
    QrEstoqueTerceiro: TIntegerField;
    QrEstoqueNO_FORNECE: TWideStringField;
    QrEstoqueNO_EMPRESA: TWideStringField;
    QrEstoqueNO_STATUS: TWideStringField;
    QrEstoqueDataHora: TWideStringField;
    QrEstoqueOrdGGX: TLargeintField;
    QrEstoqueOrdem: TIntegerField;
    QrEstoqueCodiGGY: TIntegerField;
    QrEstoqueNome: TWideStringField;
    QrEstoqueCodigo: TIntegerField;
    QrEstoqueIMEC: TIntegerField;
    QrEstoqueIMEI: TIntegerField;
    QrEstoqueMovimID: TIntegerField;
    QrEstoqueMovimNiv: TIntegerField;
    QrEstoqueNO_MovimID: TWideStringField;
    QrEstoqueNO_MovimNiv: TWideStringField;
    QrEstoqueAtivo: TLargeintField;
    DsEstque: TDataSource;
    QrEstoqueGraGruY: TIntegerField;
    QrEstoqueSdoVrtQtd: TFloatField;
    QrGGXRcl: TmySQLQuery;
    QrGGXRclGraGru1: TIntegerField;
    QrGGXRclControle: TIntegerField;
    QrGGXRclNO_PRD_TAM_COR: TWideStringField;
    QrGGXRclSIGLAUNIDMED: TWideStringField;
    QrGGXRclCODUSUUNIDMED: TIntegerField;
    QrGGXRclNOMEUNIDMED: TWideStringField;
    DsGGXRcl: TDataSource;
    Label19: TLabel;
    EdGGXRcl: TdmkEditCB;
    CBGGXRcl: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    QrEstoqueTXMulFrnCab: TIntegerField;
    QrEstoqueSdoVrtVal: TFloatField;
    QrEstoqueSerieTal: TIntegerField;
    QrEstoqueTalao: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTerceiroChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure DBG04EstqDblClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdGGXRclKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FVSMovImp4(*, FVSMovImp5*): String;
    FGGXPsq: Integer;
    //
    procedure InsereIMEI_Atual(InsereTudo: Boolean; ParcQtde: Double);
    procedure FechaPesquisa();
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    //procedure ReopenVSGerArtSrc(Controle: Integer);
    //function  ValorTParcial(): Double;
    procedure LiberaEdicao(Libera, Avisa: Boolean);
    function  BaixaIMEIParcial(): Integer;
    function  BaixaIMEITotal(): Integer;
  public
    { Public declarations }
    //FVSOpeOriIMEI, FVSOpeOriPallet,
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FFornecMO, FStqCenLoc: Integer;
    FDataHora: TDateTime;
    FParcial: Boolean;
    //
    procedure ReopenItensAptos();
  end;

  var
  FmTXIndOri: TFmTXIndOri;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
TXIndCab, UnTX_PF, AppListas;

{$R *.DFM}

procedure TFmTXIndOri.BtReabreClick(Sender: TObject);
begin
  ReopenItensAptos();
end;

function TFmTXIndOri.BaixaIMEIParcial(): Integer;
var
  Qtde: Double;
  SobraQtde: Double;
begin
  Result := 0;
  Qtde  := EdQtde.ValueVariant;
  //
  SobraQtde  := Qtde;
  //
  if MyObjects.FIC(Qtde <= 0, EdQtde, 'Informe a quantidade!') then
    Exit;
  InsereIMEI_Atual(False, SobraQtde);
  Result := 1;
end;

function TFmTXIndOri.BaixaIMEITotal(): Integer;
var
  N, I: Integer;
begin
  QrEstoque.DisableControls;
  try
    N := 0;
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to DBG04Estq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      N := N + 1;
      //
      //AdicionaPallet();
      InsereIMEI_Atual(True, 0);
    end;
  finally
    QrEstoque.EnableControls;
  end;
  Result := N;
end;

procedure TFmTXIndOri.BtOKClick(Sender: TObject);
var
  N, MovimCod, Codigo: Integer;
begin
  if FParcial then
    N := BaixaIMEIParcial()
  else
    N := BaixaIMEITotal();
  //
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_PF.AtualizaTotaisVSXxxCab('V S O p e c a b ', MovimCod);
    MovimCod := FmTXIndCab.QrTXIndCabMovimCod.Value;
    TX_PF.AtualizaTotaisTXIndCab(MovimCod);
    //
    Codigo := EdCodigo.ValueVariant;
    FmTXIndCab.LocCod(Codigo, Codigo);
    if N > 0 then
    begin
      if CkContinuar.Checked then
      begin
        EdControle.ValueVariant := 0;
        EdQtde.ValueVariant     := 0;
        EdObserv.ValueVariant   := '';
        EdGGXRcl.ValueVariant   := 0;
        CBGGXRcl.KeyValue       := 0;
        ReopenItensAptos();
        MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG04Estq), False);
        LiberaEdicao(False, False);
      end else
        Close;
    end else
      Geral.MB_Aviso('Nenhum pallet foi selecionado / adicionado!');
end;

procedure TFmTXIndOri.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXIndOri.DBG04EstqDblClick(Sender: TObject);
begin
  LiberaEdicao(True, True);
end;

procedure TFmTXIndOri.EdGGXRclKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{
var
  Selecionados: TArrSelIdxInt1;
  ReduzidosStr: array of String;
  I, N, Item, GraGruX: Integer;
}
begin
  if Key = VK_F4 then
  begin
{
    GraGruX := 0;
    //
    MyObjects.GetSelecionadosBookmark_Int1(Self, TDBGrid(DBG04Estq), 'GraGruX',
      Selecionados);
    N := Length(Selecionados);
    if N > 0 then
    begin
      if N = 1 then
        GraGruX := Selecionados[0]
      else
      begin
        SetLength(ReduzidosStr, N);
        for I := 0 to N -1 do
          ReduzidosStr[I] := Geral.FF0(Selecionados[I]);
        //
        Item := MyObjects.SelRadioGroup('Reduzido na NFe?',
        'Selecione o Reduzido: ', ReduzidosStr, 2);
        //
        if Item >= 0 then
          GraGruX := Selecionados[Item];
      end;
    end;
    if GraGruX <> 0 then
    begin
      EdGGXRcl.ValueVariant := GraGruX;
      CBGGXRcl.KeyValue     := GraGruX;
    end;
}
    EdGGXRcl.ValueVariant := QrEstoqueGraGruX.Value;
    CBGGXRcl.KeyValue     := QrEstoqueGraGruX.Value;
  end;
end;

procedure TFmTXIndOri.EdGraGruXRedefinido(Sender: TObject);
begin
  if FGGXPsq <> EdGraGruX.ValueVariant then
    FechaPesquisa();
  FGGXPsq := EdGraGruX.ValueVariant;
end;

procedure TFmTXIndOri.EdTerceiroChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTXIndOri.FechaPesquisa();
begin
  QrEstoque.Close;
end;

procedure TFmTXIndOri.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    //EdReqMovEstq.SetFocus;
  except
    //
  end;
end;

procedure TFmTXIndOri.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FParcial := False;
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1024_TXCadNat) + //',' +
    //Geral.FF0(CO_GraGruY_3072_VSRibCla) +
    ')');
  TX_PF.AbreGraGruXY(QrGGXRcl,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1024_TXCadNat) + //',' +
    //Geral.FF0(CO_GraGruY_3072_VSRibCla) +
    ')');

  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
end;

procedure TFmTXIndOri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXIndOri.InsereIMEI_Atual(InsereTudo: Boolean; ParcQtde: Double);
const
  Observ = '';
  MovimTwn   = 0;
  ExigeFornecedor = False;
  QtdGer      = 0;
  AptoUso     = 0; // Eh venda!
  CliVenda    = 0;
  //
  TpCalcAuto = -1;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe    = 0;
  //
  QtdAnt     = 0;
  CustoMOUni = 0;
  CustoMOTot = 0;
var
  DataHora, Marca: String;
  SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2,
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  DstNivel1, DstNivel2, GraGruY, SrcGGX, DstGGX, SerieTal, Talao,
  ReqMovEstq, TXMulFrnCab, ClientMO, FornecMO, StqCenLoc, GGXRcl: Integer;
  Qtde, ValorMP, ValorT: Double;
  MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Valor: Double;
begin
  SrcMovID       := TEstqMovimID(QrEstoqueMovimID.Value);
  SrcNivel1      := QrEstoqueCodigo.Value;
  SrcNivel2      := QrEstoqueControle.Value;
  SrcGGX         := QrEstoqueGraGruX.Value;
  //
  Codigo         := EdCodigo.ValueVariant;
  Controle       := 0; //EdControle.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  FornecMO       := FFornecMO;
  Terceiro       := QrEstoqueTerceiro.Value;
  TXMulFrnCab    := QrEstoqueTXMulFrnCab.Value;
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidEmIndstrlzc;
  MovimNiv       := eminIndzcSrc;
  Pallet         := QrEstoquePallet.Value;
  GraGruX        := QrEstoqueGraGruX.Value;
  GGXRcl         := EdGGXRcl.ValueVariant;
  if GGXRcl <> 0 then
    if not TX_PF.ComparaUnidMed2GGX(GraGruX, GGXRcl) then
      Exit;
  if not InsereTudo then
    Qtde := -ParcQtde
  else
    Qtde := -QrEstoqueQtde.Value;

  if QrEstoqueQtde.Value > 0 then
    Valor := -Qtde *
    (QrEstoqueValorT.Value / QrEstoqueQtde.Value)
  else
    Valor := 0;
  ValorMP        := -Valor;
  ValorT         := ValorMP;
  //
  Marca          := ''; //QrEstoqueMarca.Value;
  DstMovID       := TEstqMovimID(0);
  DstNivel1      := 0;
  DstNivel2      := 0;
  DstGGX         := 0;
  //
  GraGruY        := QrEstoqueGraGruY.Value;
  //
  StqCenLoc      := FStqCenLoc;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  SerieTal       := QrEstoqueSerieTal.Value;
  Talao          := QrEstoqueTalao.Value;
  //
  if TX_PF.TXFic(GraGruX, Empresa, Terceiro, Pallet, Qtde, ValorT,
  EdGraGruX, nil(*EdPallet*), EdQtde, nil(*EdValorT*), ExigeFornecedor,
  GraGruY, nil(*EdStqCenLoc*), 0, 0, nil, nil) then
    Exit;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);

  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, (*Fornecedor*)Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin,
  PedItsVda, ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO, QtdAnt,
  SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei002(*Origem de mat�ria prima em opera��o (baixa)*)) then
  begin
    TX_PF.AtualizaSaldoIMEI(SrcNivel2, True);
  end;
end;

procedure TFmTXIndOri.LiberaEdicao(Libera, Avisa: Boolean);
var
  Status: Boolean;
begin
  if FParcial then
  begin
    if Libera then
      Status := QrEstoque.RecordCount > 0
    else
      Status := False;
    //
    GBAptos.Enabled := not Status;
    GBGerar.Visible := Status;
    //
    LaGraGruX.Enabled := not Status;
    EdGraGruX.Enabled := not Status;
    CBGraGruX.Enabled := not Status;
    LaTerceiro.Enabled := not Status;
    EdTerceiro.Enabled := not Status;
    CBTerceiro.Enabled := not Status;
    BtReabre.Enabled := not Status;
    //
    if Libera and (EdQtde.Enabled) and (EdQtde.Visible) then
      EdQtde.SetFocus;
  end;
end;

procedure TFmTXIndOri.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
{
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
}
end;

procedure TFmTXIndOri.ReopenItensAptos();
var
  GraGruX: Integer;
  SQL_GraGruX: String;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND tmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := 'AND tmi.GraGruX<>0 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT tmi.Controle, tmi.Empresa, tmi.ClientMO,  ',
  'tmi.GraGruX, tmi.Qtde, tmi.SdoVrtQtd, ValorT,  ',
  'IF(tmi.Qtde=0, 0, tmi.SdoVrtQtd / tmi.Qtde * tmi.ValorT) SdoVrtVal, ',
  'ggx.GraGru1, ggx.GraGruY, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, tmi.Pallet, vsp.Nome NO_Pallet,  ',
  'tmi.Terceiro, tmi.TXMulFrnCab, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,  ',
  '"" NO_STATUS,  ',
  '"0000-00-00 00:00:00" DataHora, 0 OrdGGX,  ',
  'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome,  ',
  'tmi.Codigo, tmi.MovimCod IMEC, tmi.Controle IMEI,  ',
  'tmi.MovimID, tmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv,  ',
  'tmi.Marca, tmi.SerieTal, tmi.Talao, ',
  '1 Ativo  ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'LEFT JOIN txpalleta  vsp ON vsp.Codigo=tmi.Pallet  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(tmi.Pallet <> 0, vsp.GraGruX, tmi.GraGruX)  ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN entidades  ent ON ent.Codigo=tmi.Terceiro  ',
  'LEFT JOIN entidades  emp ON emp.Codigo=tmi.Empresa  ',
  'WHERE tmi.Controle <> 0  ',
  SQL_GraGruX,
  'AND tmi.SdoVrtQtd > 0  ',
  'AND tmi.Pallet = 0  ',
  'AND tmi.Empresa=' + Geral.FF0(FEmpresa),
  ' ']);
  //
  //Geral.MB_SQL(self, QrEstoque);
end;

{
object Label2: TLabel
  Left = 540
  Top = 20
  Width = 58
  Height = 13
  Caption = 'Red. It.Ger.:'
  Color = clBtnFace
  ParentColor = False
end
object EdSrcGGX: TdmkEdit
  Left = 540
  Top = 36
  Width = 64
  Height = 21
  TabStop = False
  Alignment = taRightJustify
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  ReadOnly = True
  TabOrder = 5
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'Controle'
  UpdCampo = 'Controle'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
end
object Label14: TLabel
  Left = 608
  Top = 20
  Width = 51
  Height = 13
  Caption = 'ID Movim.:'
  Color = clBtnFace
  ParentColor = False
end
object EdSrcMovID: TdmkEdit
  Left = 608
  Top = 36
  Width = 64
  Height = 21
  TabStop = False
  Alignment = taRightJustify
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  ReadOnly = True
  TabOrder = 2
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'Codigo'
  UpdCampo = 'Codigo'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
end
object Label1: TLabel
  Left = 676
  Top = 20
  Width = 58
  Height = 13
  Caption = 'ID Gera'#231#227'o:'
  Color = clBtnFace
  ParentColor = False
end
object EdSrcNivel1: TdmkEdit
  Left = 676
  Top = 36
  Width = 64
  Height = 21
  TabStop = False
  Alignment = taRightJustify
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  ReadOnly = True
  TabOrder = 1
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'MovimCod'
  UpdCampo = 'MovimCod'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
end
object EdSrcNivel2: TdmkEdit
  Left = 744
  Top = 36
  Width = 64
  Height = 21
  TabStop = False
  Alignment = taRightJustify
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  ReadOnly = True
  TabOrder = 0
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'Controle'
  UpdCampo = 'Controle'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
end
object Label17: TLabel
  Left = 744
  Top = 20
  Width = 61
  Height = 13
  Caption = 'ID It.Gerado:'
  Color = clBtnFace
  ParentColor = False
end
}

end.
