object FmTXIxx: TFmTXIxx
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-200 :: IXX - Atrelamento de Informa'#231#227'o Manual'
  ClientHeight = 286
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 549
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 501
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 490
        Height = 32
        Caption = 'IXX - Atrelamento de Informa'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 490
        Height = 32
        Caption = 'IXX - Atrelamento de Informa'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 490
        Height = 32
        Caption = 'IXX - Atrelamento de Informa'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 597
    Height = 124
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 597
      Height = 124
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 597
        Height = 124
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 593
          Height = 58
          Align = alTop
          Caption = ' Dados do Item:'
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 589
            Height = 41
            Align = alClient
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 0
            object Label5: TLabel
              Left = 12
              Top = 0
              Width = 14
              Height = 13
              Caption = 'ID:'
            end
            object Label2: TLabel
              Left = 76
              Top = 0
              Width = 32
              Height = 13
              Caption = 'IME-C:'
            end
            object Label3: TLabel
              Left = 140
              Top = 0
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label1: TLabel
              Left = 192
              Top = 0
              Width = 32
              Height = 13
              Caption = 'IME-C:'
            end
            object Label4: TLabel
              Left = 256
              Top = 0
              Width = 41
              Height = 13
              Caption = 'GraGruX'
              FocusControl = DBEdit5
            end
            object Label6: TLabel
              Left = 316
              Top = 0
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
              FocusControl = DBEdit6
            end
            object DBEdit1: TDBEdit
              Left = 12
              Top = 16
              Width = 62
              Height = 21
              DataField = 'Codigo'
              DataSource = DsTXMovIts
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 76
              Top = 16
              Width = 62
              Height = 21
              DataField = 'MovimCod'
              DataSource = DsTXMovIts
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 140
              Top = 16
              Width = 49
              Height = 21
              DataField = 'Empresa'
              DataSource = DsTXMovIts
              TabOrder = 2
            end
            object DBEdit4: TDBEdit
              Left = 192
              Top = 16
              Width = 62
              Height = 21
              DataField = 'Controle'
              DataSource = DsTXMovIts
              TabOrder = 3
            end
            object DBEdit5: TDBEdit
              Left = 256
              Top = 16
              Width = 56
              Height = 21
              DataField = 'GraGruX'
              DataSource = DsTXMovIts
              TabOrder = 4
            end
            object DBEdit6: TDBEdit
              Left = 316
              Top = 16
              Width = 80
              Height = 21
              DataField = 'Qtde'
              DataSource = DsTXMovIts
              TabOrder = 5
            end
          end
        end
        object Panel9: TPanel
          Left = 2
          Top = 73
          Width = 593
          Height = 36
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label20: TLabel
            Left = 423
            Top = 16
            Width = 29
            Height = 13
            Caption = 'Folha:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label21: TLabel
            Left = 515
            Top = 16
            Width = 29
            Height = 13
            Caption = 'Linha:'
            Color = clBtnFace
            ParentColor = False
          end
          object RGIxxMovIX: TRadioGroup
            Left = 0
            Top = 0
            Width = 416
            Height = 36
            Align = alLeft
            Caption = ' Informa'#231#227'o manual de movimenta'#231#227'o:'
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'Entrada de couro'
              'Classe / reclasse'
              'Sa'#237'da de couro')
            TabOrder = 0
          end
          object EdIxxFolha: TdmkEdit
            Left = 456
            Top = 12
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ReqMovEstq'
            UpdCampo = 'ReqMovEstq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdIxxLinha: TdmkEdit
            Left = 548
            Top = 12
            Width = 33
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ReqMovEstq'
            UpdCampo = 'ReqMovEstq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 172
    Width = 597
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 593
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 216
    Width = 597
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 451
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 449
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 380
    Top = 199
  end
  object QrTXMovIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM txmovits'
      'WHERE Controle=1')
    Left = 216
    Top = 164
    object QrTXMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTXMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrTXMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrTXMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrTXMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrTXMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrTXMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrTXMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXMovItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTXMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTXMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrTXMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrTXMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrTXMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrTXMovItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrTXMovItsSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrTXMovItsTalao: TIntegerField
      FieldName = 'Talao'
    end
    object QrTXMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrTXMovItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrTXMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrTXMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrTXMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrTXMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrTXMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrTXMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrTXMovItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrTXMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrTXMovItsZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrTXMovItsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrTXMovItsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrTXMovItsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrTXMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrTXMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovItsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrTXMovItsTXMorCab: TIntegerField
      FieldName = 'TXMorCab'
    end
    object QrTXMovItsTXMulFrnCab: TIntegerField
      FieldName = 'TXMulFrnCab'
    end
    object QrTXMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrTXMovItsNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrTXMovItsNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrTXMovItsTXMulNFeCab: TIntegerField
      FieldName = 'TXMulNFeCab'
    end
    object QrTXMovItsGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
    end
    object QrTXMovItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXMovItsMovCodPai: TIntegerField
      FieldName = 'MovCodPai'
    end
    object QrTXMovItsIxxMovIX: TSmallintField
      FieldName = 'IxxMovIX'
    end
    object QrTXMovItsIxxFolha: TIntegerField
      FieldName = 'IxxFolha'
    end
    object QrTXMovItsIxxLinha: TIntegerField
      FieldName = 'IxxLinha'
    end
  end
  object DsTXMovIts: TDataSource
    DataSet = QrTXMovIts
    Left = 216
    Top = 212
  end
end
