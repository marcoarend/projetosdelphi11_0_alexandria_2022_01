unit TXIxx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.Mask, UnProjGroup_Consts;

type
  TFmTXIxx = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel9: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    RGIxxMovIX: TRadioGroup;
    EdIxxFolha: TdmkEdit;
    EdIxxLinha: TdmkEdit;
    Label1: TLabel;
    QrTXMovIts: TmySQLQuery;
    QrTXMovItsCodigo: TIntegerField;
    QrTXMovItsControle: TIntegerField;
    QrTXMovItsMovimCod: TIntegerField;
    QrTXMovItsMovimNiv: TIntegerField;
    QrTXMovItsMovimTwn: TIntegerField;
    QrTXMovItsEmpresa: TIntegerField;
    QrTXMovItsTerceiro: TIntegerField;
    QrTXMovItsCliVenda: TIntegerField;
    QrTXMovItsMovimID: TIntegerField;
    QrTXMovItsDataHora: TDateTimeField;
    QrTXMovItsPallet: TIntegerField;
    QrTXMovItsGraGruX: TIntegerField;
    QrTXMovItsQtde: TFloatField;
    QrTXMovItsValorT: TFloatField;
    QrTXMovItsSrcMovID: TIntegerField;
    QrTXMovItsSrcNivel1: TIntegerField;
    QrTXMovItsSrcNivel2: TIntegerField;
    QrTXMovItsSrcGGX: TIntegerField;
    QrTXMovItsSdoVrtQtd: TFloatField;
    QrTXMovItsObserv: TWideStringField;
    QrTXMovItsSerieTal: TIntegerField;
    QrTXMovItsTalao: TIntegerField;
    QrTXMovItsFornecMO: TIntegerField;
    QrTXMovItsCustoMOUni: TFloatField;
    QrTXMovItsCustoMOTot: TFloatField;
    QrTXMovItsValorMP: TFloatField;
    QrTXMovItsDstMovID: TIntegerField;
    QrTXMovItsDstNivel1: TIntegerField;
    QrTXMovItsDstNivel2: TIntegerField;
    QrTXMovItsDstGGX: TIntegerField;
    QrTXMovItsQtdGer: TFloatField;
    QrTXMovItsQtdAnt: TFloatField;
    QrTXMovItsAptoUso: TSmallintField;
    QrTXMovItsMarca: TWideStringField;
    QrTXMovItsTpCalcAuto: TIntegerField;
    QrTXMovItsZerado: TSmallintField;
    QrTXMovItsPedItsLib: TIntegerField;
    QrTXMovItsPedItsFin: TIntegerField;
    QrTXMovItsPedItsVda: TIntegerField;
    QrTXMovItsLk: TIntegerField;
    QrTXMovItsDataCad: TDateField;
    QrTXMovItsDataAlt: TDateField;
    QrTXMovItsUserCad: TIntegerField;
    QrTXMovItsUserAlt: TIntegerField;
    QrTXMovItsAlterWeb: TSmallintField;
    QrTXMovItsAtivo: TSmallintField;
    QrTXMovItsReqMovEstq: TIntegerField;
    QrTXMovItsStqCenLoc: TIntegerField;
    QrTXMovItsItemNFe: TIntegerField;
    QrTXMovItsTXMorCab: TIntegerField;
    QrTXMovItsTXMulFrnCab: TIntegerField;
    QrTXMovItsClientMO: TIntegerField;
    QrTXMovItsNFeSer: TSmallintField;
    QrTXMovItsNFeNum: TIntegerField;
    QrTXMovItsTXMulNFeCab: TIntegerField;
    QrTXMovItsGGXRcl: TIntegerField;
    QrTXMovItsDtCorrApo: TDateTimeField;
    QrTXMovItsMovCodPai: TIntegerField;
    QrTXMovItsIxxMovIX: TSmallintField;
    QrTXMovItsIxxFolha: TIntegerField;
    QrTXMovItsIxxLinha: TIntegerField;
    DBEdit1: TDBEdit;
    DsTXMovIts: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FQrIMEI: TmySQLQuery;
    FIMEI: Integer;
    FResult: Boolean;
    //
    procedure PreparaEdicao(IxxMovIX, IxxFolha, IxxLinha: Integer);
  end;

  var
  FmTXIxx: TFmTXIxx;

implementation

uses UnMyObjects, Module, UnTX_PF, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmTXIxx.BtOKClick(Sender: TObject);
var
  Controle: Integer;
  //
  IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
begin
  Controle := FIMEI;
  //
  IxxMovIX := TEstqMovInfo(RGIxxMovIX.ItemIndex);
  IxxFolha := EdIxxFolha.ValueVariant;
  IxxLinha := EdIxxLinha.ValueVariant;
  if TX_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha) then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_TMI, False, [
  'IxxMovIX', 'IxxFolha', 'IxxLinha'], ['Controle'], [
  IxxMovIX, IxxFolha, IxxLinha], [Controle], True) then
  begin
    FResult := True;
    //
    MyObjects.IncrementaFolhaLinha(EdIxxFolha, EdIxxLinha, CO_TX_MaxLin);
    Close;
  end
end;

procedure TFmTXIxx.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXIxx.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXIxx.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FResult := False;
end;

procedure TFmTXIxx.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXIxx.PreparaEdicao(IxxMovIX, IxxFolha, IxxLinha: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXMovIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM txmovits ',
  'WHERE Controle=' + Geral.FF0(FIMEI),
  '']);
  //
  if QrTXMovItsIxxMovIX.Value > 0 then
  begin
    RGIxxMovIX.ItemIndex    := QrTXMovItsIxxMovIX.Value;
    EdIxxFolha.ValueVariant := QrTXMovItsIxxFolha.Value;
    EdIxxLinha.ValueVariant := QrTXMovItsIxxLinha.Value;
  end else
  if IxxMovIX > 0 then
  begin
    RGIxxMovIX.ItemIndex    := IxxMovIX;
    EdIxxFolha.ValueVariant := IxxFolha;
    EdIxxLinha.ValueVariant := IxxLinha;
  end;
end;

end.
