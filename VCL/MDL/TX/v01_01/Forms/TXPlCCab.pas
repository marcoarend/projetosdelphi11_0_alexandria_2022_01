unit TXPlCCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  dmkDBGridZTO, UnProjGroup_Consts, UnGrl_Consts, UnGrl_Geral, UnAppEnums;

type
  TFmTXPlCCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrTXPlCCab: TmySQLQuery;
    DsTXPlCCab: TDataSource;
    QrTXPlCIts: TmySQLQuery;
    DsTXPlCIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtCompra: TdmkEditDateTimePicker;
    EdDtCompra: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrada: TdmkEditDateTimePicker;
    EdDtEntrada: TdmkEdit;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrTXPlCCabCodigo: TIntegerField;
    QrTXPlCCabMovimCod: TIntegerField;
    QrTXPlCCabEmpresa: TIntegerField;
    QrTXPlCCabDtCompra: TDateTimeField;
    QrTXPlCCabDtViagem: TDateTimeField;
    QrTXPlCCabDtEntrada: TDateTimeField;
    QrTXPlCCabFornecedor: TIntegerField;
    QrTXPlCCabTransporta: TIntegerField;
    QrTXPlCCabQtde: TFloatField;
    QrTXPlCCabLk: TIntegerField;
    QrTXPlCCabDataCad: TDateField;
    QrTXPlCCabDataAlt: TDateField;
    QrTXPlCCabUserCad: TIntegerField;
    QrTXPlCCabUserAlt: TIntegerField;
    QrTXPlCCabAlterWeb: TSmallintField;
    QrTXPlCCabAtivo: TSmallintField;
    QrTXPlCCabNO_EMPRESA: TWideStringField;
    QrTXPlCCabNO_FORNECE: TWideStringField;
    QrTXPlCCabNO_TRANSPORTA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    N2: TMenuItem;
    QrTXPlCCabValorT: TFloatField;
    DBEdit15: TDBEdit;
    SbFornecedor: TSpeedButton;
    SbTransportador: TSpeedButton;
    N1: TMenuItem;
    Atualizaestoque1: TMenuItem;
    frxDsTXPlCCab: TfrxDBDataset;
    frxDsTXPlCIts: TfrxDBDataset;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrProcednc: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProcednc: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    SbClienteMO: TSpeedButton;
    Label21: TLabel;
    EdProcednc: TdmkEditCB;
    CBProcednc: TdmkDBLookupComboBox;
    SbProcedenc: TSpeedButton;
    Label22: TLabel;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    SbMotorista: TSpeedButton;
    Label23: TLabel;
    EdPlaca: TdmkEdit;
    QrTXPlCCabClienteMO: TIntegerField;
    QrTXPlCCabProcednc: TIntegerField;
    QrTXPlCCabMotorista: TIntegerField;
    QrTXPlCCabPlaca: TWideStringField;
    QrTXPlCCabNO_CLIENTEMO: TWideStringField;
    QrTXPlCCabNO_PROCEDNC: TWideStringField;
    QrTXPlCCabNO_MOTORISTA: TWideStringField;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    Histrico1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    GBIts: TGroupBox;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMTXPlcIts: TPopupMenu;
    IrparajaneladegerenciamentodeFichaRMP1: TMenuItem;
    DGDados: TdmkDBGridZTO;
    FichadePallets1: TMenuItem;
    Fichadetodospalletsdestacompra1: TMenuItem;
    FichaCOMnomedoPallet1: TMenuItem;
    FichaSEMnomedoPallet1: TMenuItem;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrTXPlCItsCodigo: TLargeintField;
    QrTXPlCItsControle: TLargeintField;
    QrTXPlCItsMovimCod: TLargeintField;
    QrTXPlCItsMovimNiv: TLargeintField;
    QrTXPlCItsMovimTwn: TLargeintField;
    QrTXPlCItsEmpresa: TLargeintField;
    QrTXPlCItsTerceiro: TLargeintField;
    QrTXPlCItsCliVenda: TLargeintField;
    QrTXPlCItsMovimID: TLargeintField;
    QrTXPlCItsDataHora: TDateTimeField;
    QrTXPlCItsPallet: TLargeintField;
    QrTXPlCItsGraGruX: TLargeintField;
    QrTXPlCItsQtde: TFloatField;
    QrTXPlCItsValorT: TFloatField;
    QrTXPlCItsSrcMovID: TLargeintField;
    QrTXPlCItsSrcNivel1: TLargeintField;
    QrTXPlCItsSrcNivel2: TLargeintField;
    QrTXPlCItsSrcGGX: TLargeintField;
    QrTXPlCItsSdoVrtQtd: TFloatField;
    QrTXPlCItsObserv: TWideStringField;
    QrTXPlCItsSerieTal: TLargeintField;
    QrTXPlCItsTalao: TLargeintField;
    QrTXPlCItsFornecMO: TLargeintField;
    QrTXPlCItsCustoMOUni: TFloatField;
    QrTXPlCItsCustoMOTot: TFloatField;
    QrTXPlCItsValorMP: TFloatField;
    QrTXPlCItsDstMovID: TLargeintField;
    QrTXPlCItsDstNivel1: TLargeintField;
    QrTXPlCItsDstNivel2: TLargeintField;
    QrTXPlCItsDstGGX: TLargeintField;
    QrTXPlCItsQtdGer: TFloatField;
    QrTXPlCItsQtdAnt: TFloatField;
    QrTXPlCItsPedItsFin: TLargeintField;
    QrTXPlCItsMarca: TWideStringField;
    QrTXPlCItsStqCenLoc: TLargeintField;
    QrTXPlCItsNO_PALLET: TWideStringField;
    QrTXPlCItsNO_PRD_TAM_COR: TWideStringField;
    QrTXPlCItsNO_TTW: TWideStringField;
    QrTXPlCItsID_TTW: TLargeintField;
    QrTXPlCItsReqMovEstq: TLargeintField;
    QrTXPlCItsNOMEUNIDMED: TWideStringField;
    QrTXPlCItsSIGLAUNIDMED: TWideStringField;
    QrTXPlCItsSEQ: TIntegerField;
    QrTXPlCCabTemIMEIMrt: TIntegerField;
    QrTXPlCItsClientMO: TLargeintField;
    Edide_serie: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    Edide_nNF: TdmkEdit;
    QrTXPlCCabide_serie: TIntegerField;
    QrTXPlCCabide_nNF: TIntegerField;
    Label9: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Edemi_serie: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    Edemi_nNF: TdmkEdit;
    QrTXPlCCabemi_serie: TIntegerField;
    QrTXPlCCabemi_nNF: TIntegerField;
    Label33: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    Label35: TLabel;
    Rendimentodesemiacabado1: TMenuItem;
    QrTXPlCCabNFeStatus: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXPlCCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXPlCCabBeforeOpen(DataSet: TDataSet);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXPlCCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTXPlCCabBeforeClose(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
    procedure SbTransportadorClick(Sender: TObject);
    procedure Atualizaestoque1Click(Sender: TObject);
    procedure AnliseMPAG1Click(Sender: TObject);
    procedure frxWET_CURTI_006_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SbClienteMOClick(Sender: TObject);
    procedure SbProcedencClick(Sender: TObject);
    procedure SbMotoristaClick(Sender: TObject);
    procedure IrparajaneladegerenciamentodeFichaRMP1Click(Sender: TObject);
    procedure FichaCOMnomedoPallet1Click(Sender: TObject);
    procedure FichaSEMnomedoPallet1Click(Sender: TObject);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
    procedure Rendimentodesemiacabado1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormTXPlCIts(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ImprimePallet(InfoNO_Pallet: Boolean);
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaNFeItens();
    procedure ReopenTXPlCIts(Controle: Integer);

  end;

var
  FmTXPlCCab: TFmTXPlCCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, TXPlCIts, ModuleGeral, UnTX_PF,
  Principal, CreateTX, ModTX_CRC, UnTX_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXPlCCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXPlCCab.MostraFormTXPlCIts(SQLType: TSQLType);
begin
{
  if DBCheck.CriaFm(TFmTXPlCIts, FmTXPlCIts, afmoNegarComAviso) then
  begin
    FmTXPlCIts.ImgTipo.SQLType := SQLType;
    FmTXPlCIts.FQrCab := QrTXPlCCab;
    FmTXPlCIts.FDsCab := DsTXPlCCab;
    FmTXPlCIts.FQrIts := QrTXPlCIts;
    FmTXPlCIts.FDataHora := QrTXPlCCabDtEntrada.Value;
    FmTXPlCIts.FEmpresa := QrTXPlCCabEmpresa.Value;
    FmTXPlCIts.FFornece := QrTXPlCCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmTXPlCIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmTXPlCIts.EdControle.ValueVariant := QrTXPlCItsControle.Value;
      //
      FmTXPlCIts.EdGragruX.ValueVariant  := QrTXPlCItsGraGruX.Value;
      FmTXPlCIts.CBGragruX.KeyValue      := QrTXPlCItsGraGruX.Value;
      FmTXPlCIts.EdQtde.ValueVariant     := QrTXPlCItsQtde.Value;
      FmTXPlCIts.EdValorT.ValueVariant   := QrTXPlCItsValorT.Value;
      FmTXPlCIts.EdObserv.ValueVariant   := QrTXPlCItsObserv.Value;
      FmTXPlCIts.EdSerieTal.ValueVariant := QrTXPlCItsSerieTal.Value;
      FmTXPlCIts.CBSerieTal.KeyValue     := QrTXPlCItsSerieTal.Value;
      FmTXPlCIts.EdTalao.ValueVariant    := QrTXPlCItsTalao.Value;
      FmTXPlCIts.EdMarca.ValueVariant    := QrTXPlCItsMarca.Value;
      //
      if QrTXPlCItsSdoVrtQtd.Value < QrTXPlCItsQtde.Value then
      begin
        FmTXPlCIts.EdGraGruX.Enabled  := False;
        FmTXPlCIts.CBGraGruX.Enabled  := False;
        FmTXPlCIts.EdSerieTal.Enabled := False;
        FmTXPlCIts.CBSerieTal.Enabled := False;
        FmTXPlCIts.EdTalao.Enabled    := False;
      end;
      //
    end;
    FmTXPlCIts.ShowModal;
    FmTXPlCIts.Destroy;
  end;
}
  if DBCheck.CriaFm(TFmTXPlCIts, FmTXPlCIts, afmoNegarComAviso) then
  begin
    FmTXPlCIts.ImgTipo.SQLType := SQLType;
    FmTXPlCIts.FQrCab := QrTXPlCCab;
    FmTXPlCIts.FDsCab := DsTXPlCCab;
    FmTXPlCIts.FQrIts := QrTXPlCIts;
    FmTXPlCIts.FDataHora := QrTXPlCCabDtEntrada.Value;
    FmTXPlCIts.FEmpresa  := QrTXPlCCabEmpresa.Value;
    FmTXPlCIts.FClientMO  := QrTXPlCCabClienteMO.Value;
    //
    FmTXPlCIts.EdFornecedor.ValueVariant := QrTXPlCCabFornecedor.Value;
    FmTXPlCIts.CBFornecedor.KeyValue     := QrTXPlCCabFornecedor.Value;
    if SQLType = stIns then
    begin
      //FmTXPlCIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmTXPlCIts.EdControle.ValueVariant := QrTXPlCItsControle.Value;
      //
      FmTXPlCIts.EdGragruX.ValueVariant    := QrTXPlCItsGraGruX.Value;
      FmTXPlCIts.CBGragruX.KeyValue        := QrTXPlCItsGraGruX.Value;
      FmTXPlCIts.EdSerieTal.ValueVariant   := QrTXPlCItsSerieTal.Value;
      FmTXPlCIts.CBSerieTal.KeyValue       := QrTXPlCItsSerieTal.Value;
      FmTXPlCIts.EdTalao.ValueVariant      := QrTXPlCItsTalao.Value;
      FmTXPlCIts.EdPallet.ValueVariant     := QrTXPlCItsPallet.Value;
      FmTXPlCIts.CBPallet.KeyValue         := QrTXPlCItsPallet.Value;
      FmTXPlCIts.EdQtde.ValueVariant       := QrTXPlCItsQtde.Value;
      FmTXPlCIts.EdValorMP.ValueVariant    := QrTXPlCItsValorMP.Value;
      FmTXPlCIts.EdCustoMOUni.ValueVariant := QrTXPlCItsCustoMOUni.Value;
      FmTXPlCIts.EdFornecMO.ValueVariant   := QrTXPlCItsFornecMO.Value;
      FmTXPlCIts.CBFornecMO.KeyValue       := QrTXPlCItsFornecMO.Value;
      FmTXPlCIts.EdObserv.ValueVariant     := QrTXPlCItsObserv.Value;
      FmTXPlCIts.EdStqCenLoc.ValueVariant  := QrTXPlCItsStqCenLoc.Value;
      FmTXPlCIts.CBStqCenLoc.KeyValue      := QrTXPlCItsStqCenLoc.Value;
      FmTXPlCIts.EdReqMovEstq.ValueVariant := QrTXPlCItsReqMovEstq.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmTXPlCIts.EdCustoMOTot.ValueVariant := QrTXPlCItsCustoMOTot.Value;
      FmTXPlCIts.EdValorT.ValueVariant     := QrTXPlCItsValorT.Value;
    end;
    FmTXPlCIts.ShowModal;
    FmTXPlCIts.Destroy;
  end;

end;

procedure TFmTXPlCCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXPlCCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTXPlCCab, QrTXPlCIts);
end;

procedure TFmTXPlCCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTXPlCCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTXPlCIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTXPlCIts);
  //
  TX_PF.HabilitaComposTXAtivo(QrTXPlCItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmTXPlCCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXPlCCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXPlCCab.DefParams;
begin
  VAR_GOTOTABELA := 'txplccab';
  VAR_GOTOMYSQLTABLE := QrTXPlCCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,');
  VAR_SQLx.Add('IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,');
  VAR_SQLx.Add('IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA');
  VAR_SQLx.Add('FROM txplccab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc');
  VAR_SQLx.Add('LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTXPlCCab.Estoque1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXMovImpEstoque(False);
end;

procedure TFmTXPlCCab.ImprimePallet(InfoNO_Pallet: Boolean);
var
  Empresa, ClientMO, Pallet: Integer;
  TempTab: String;
begin
  Empresa  := QrTXPlCItsEmpresa.Value;
  ClientMO := QrTXPlCItsClientMO.Value;
  Pallet   := QrTXPlCItsPallet.Value;
  TempTab  := Self.Name;
  TX_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, TempTab, InfoNO_Pallet);
end;

procedure TFmTXPlCCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  TXLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateTX.RecriaTempTableNovo(ntrttTXMovImp4, DModG.QrUpdPID1,
      False, 1, '_txmovimp4_' + Self.Name);
  Empresa := QrTXPlCItsEmpresa.Value;
  N := 0;
  QrTXPlCIts.First;
  while not QrTXPlCIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrTXPlCItsPallet.Value;
    //
    QrTXPlCIts.Next;
  end;
  if N > 0 then
    //TX_PF.ImprimePallets(Empresa, MyArr, TempTab, TXLstPalBox)
    TX_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmTXPlCCab.IrparajaneladegerenciamentodeFichaRMP1Click(
  Sender: TObject);
var
  SerieFicha, Ficha: Integer;
begin

{$IfDef sAllTX}
{POIU
  SerieFicha := QrTXPlCItsSerieFch.Value;
  Ficha      := QrTXPlCItsFicha.Value;
  TX_PF.MostraFormTXFchGerCab(SerieFicha, Ficha);
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXPlCCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormTXPlCIts(stUpd);
end;

procedure TFmTXPlCCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTXPlCCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXPlCCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXPlCCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod, Controle: Integer;
begin
  Codigo   := QrTXPlCItsCodigo.Value;
  MovimCod := QrTXPlCItsMovimCod.Value;
  Controle := QrTXPlCItsControle.Value;
  //
  if TX_PF.ExcluiControleTXMovIts(QrTXPlCIts, TIntegerField(QrTXPlCItsControle),
  Controle, CtrlBaix, QrTXPlCItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti076)) then
  begin
    // Atualiza!
    TX_PF.AtualizaTotaisTXXxxCab('txplccab', MovimCod);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXPlCCab.Rendimentodesemiacabado1Click(Sender: TObject);
begin
{$IfDef sAllTX}
{
  TX_PF.ImprimeRendimentoIMEIS([QrTXPlCItsControle.Value], [False], [False],
  [], []);
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TFmTXPlCCab.ReopenTXPlCIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXPlCCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'txp.Nome NO_Pallet, ',
  'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXPlCCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPlCIts, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  QrTXPlCIts.Locate('Controle', Controle, []);
end;


procedure TFmTXPlCCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXPlCCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXPlCCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXPlCCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXPlCCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXPlCCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXPlCCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXPlCCabCodigo.Value;
  Close;
end;

procedure TFmTXPlCCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormTXPlCIts(stIns);
end;

procedure TFmTXPlCCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXPlCCab, [PnDados],
  [PnEdita], TPDtCompra, ImgTipo, 'txplccab');
  Empresa := DModG.ObtemFilialDeEntidade(QrTXPlCCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmTXPlCCab.BtConfirmaClick(Sender: TObject);
var
  DtCompra, DtViagem, DtEntrada, DataHora, Placa: String;
  Codigo, MovimCod, Empresa, Fornecedor, Transporta, Terceiro, ClienteMO,
  Procednc, Motorista, ide_serie, ide_nNF, emi_serie, emi_nNF: Integer;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtCompra       := Geral.FDT_TP_Ed(TPDtCompra.Date, EdDtCompra.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrada      := Geral.FDT_TP_Ed(TPDtEntrada.Date, EdDtEntrada.Text);
  Fornecedor     := EdFornecedor.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ClienteMO      := EdClienteMO.ValueVariant;
  Procednc       := EdProcednc.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  emi_serie      := Edemi_serie.ValueVariant;
  emi_nNF        := Edemi_nNF.ValueVariant;
(*
  Qtde           := EdQtde.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Fornecedor = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  if MyObjects.FIC(TPDtCompra.DateTime < 2, TPDtCompra, 'Defina uma data de compra!') then Exit;
  DataChegadaInvalida := (TPDtEntrada.Date < TPDtViagem.Date);
  if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de viagem v�lida!') then Exit;
  if MyObjects.FIC(ClienteMO = 0, EdClienteMO, 'Defina o dono do couro!') then Exit;
  //
  if not TX_PF.ValidaCampoNF(10, Edide_nNF, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txplccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'txplccab', False, [
  'MovimCod', 'Empresa', 'DtCompra',
  'DtViagem', 'DtEntrada', 'Fornecedor',
  'Transporta',(*, 'Qtde', 'ValorT'*)
  'ClienteMO', 'Procednc', 'Motorista',
  'Placa', 'ide_serie', 'ide_nNF',
  'emi_serie', 'emi_nNF'
  ], [
  'Codigo'], [
  MovimCod, Empresa, DtCompra,
  DtViagem, DtEntrada, Fornecedor,
  Transporta, (*Qtde, ValorT*)
  ClienteMO, Procednc, Motorista,
  Placa, ide_serie, ide_nNF,
  emi_serie, emi_nNF
  ], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      TX_PF.InsereTXMovCab(MovimCod, emidEntradaPlC, Codigo)
    else
    begin
      DataHora := DtEntrada;
      Terceiro := Fornecedor;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Empresa', 'Terceiro', CO_DATA_HORA_TMI,
      'ClientMO'], ['MovimCod'], [
      Empresa, Terceiro, DataHora,
      ClienteMO], [MovimCod], True);
      //
      if Edide_nNF.ValueVariant <> 0 then
        AtualizaNFeItens();
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if SQLType = stIns then
      MostraFormTXPlCIts(stIns);
  end;
end;

procedure TFmTXPlCCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'txplccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txplccab', 'Codigo');
end;

procedure TFmTXPlCCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTXPlCCab.AnliseMPAG1Click(Sender: TObject);
begin
{
  MyObjects.frxDefineDataSets(frxWET_CURTI_006_01, [
    DModG.frxDsDono,
    frxDsTXPlCCab,
    frxDsTXPlCIts,
    frxDsTXItsGer
  ]);
  MyObjects.frxMostra(frxWET_CURTI_006_01, 'An�lise MPAG');
}
end;

procedure TFmTXPlCCab.Atualizaestoque1Click(Sender: TObject);
begin
  TX_PF.AtualizaSaldoIMEI(QrTXPlCItsControle.Value, True);
end;

procedure TFmTXPlCCab.AtualizaNFeItens();
begin
  DmModTX_CRC.AtualizaTXMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrTXPlCCabMovimCod.Value, TEstqMovimID.emidEntradaPlC, [(**)], [eminSemNiv]);
end;

procedure TFmTXPlCCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXPlCCab, QrTXPlCCabDtEntrada.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXPlCCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBIts.Align := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
end;

procedure TFmTXPlCCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXPlCCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXPlCCab.SbProcedencClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrProcednc, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdProcednc.Text := IntToStr(VAR_ENTIDADE);
    CBProcednc.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXPlCCab.SbClienteMOClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdClienteMO.Text := IntToStr(VAR_ENTIDADE);
    CBClienteMO.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXPlCCab.SbFornecedorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdFornecedor.Text := IntToStr(VAR_ENTIDADE);
    CBFornecedor.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXPlCCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXPlCCab.SbMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXPlCCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXPlCCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXPlCCab.QrTXPlCCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXPlCCab.QrTXPlCCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTXPlCIts(0);
end;

procedure TFmTXPlCCab.FichaCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(True);
end;

procedure TFmTXPlCCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmTXPlCCab.FichaSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimePallet(False);
end;

procedure TFmTXPlCCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmTXPlCCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXPlCCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXPlCCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := TX_Jan.MostraFormTXTalPsq(emidEntradaPlC, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenTXPlCIts(Controle);
  end;
end;

procedure TFmTXPlCCab.SbTransportadorClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdTransporta.Text := IntToStr(VAR_ENTIDADE);
    CBTransporta.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmTXPlCCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXPlCCab.frxWET_CURTI_006_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmTXPlCCab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmTXPlCCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXPlCCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txplccab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
  Agora := DModG.ObtemAgora();
  TPDtEntrada.Date := Agora;
  //EdDtCompra.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtCompra.SetFocus;
end;

procedure TFmTXPlCCab.QrTXPlCCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXPlCIts.Close;
end;

procedure TFmTXPlCCab.QrTXPlCCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXPlCCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

