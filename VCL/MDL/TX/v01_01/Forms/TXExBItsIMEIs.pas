unit TXExBItsIMEIs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  Vcl.Menus, dmkEditDateTimePicker, dmkEdit, UnProjGroup_Consts, UnAppEnums;

type
  TFmTXExBItsIMEIs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraGruY: TmySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    DsGraGruY: TDataSource;
    QrIMEIs: TmySQLQuery;
    DsIMEIs: TDataSource;
    DBGIMEIs: TdmkDBGridZTO;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsCodigo: TIntegerField;
    QrIMEIsControle: TIntegerField;
    QrIMEIsMovimCod: TIntegerField;
    QrIMEIsMovimNiv: TIntegerField;
    QrIMEIsMovimTwn: TIntegerField;
    QrIMEIsEmpresa: TIntegerField;
    QrIMEIsTerceiro: TIntegerField;
    QrIMEIsCliVenda: TIntegerField;
    QrIMEIsMovimID: TIntegerField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsPallet: TIntegerField;
    QrIMEIsGraGruX: TIntegerField;
    QrIMEIsQtde: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSrcMovID: TIntegerField;
    QrIMEIsSrcNivel1: TIntegerField;
    QrIMEIsSrcNivel2: TIntegerField;
    QrIMEIsSrcGGX: TIntegerField;
    QrIMEIsSdoVrtQtd: TFloatField;
    QrIMEIsObserv: TWideStringField;
    QrIMEIsFornecMO: TIntegerField;
    QrIMEIsCustoMOUni: TFloatField;
    QrIMEIsCustoMOTot: TFloatField;
    QrIMEIsValorMP: TFloatField;
    QrIMEIsDstMovID: TIntegerField;
    QrIMEIsDstNivel1: TIntegerField;
    QrIMEIsDstNivel2: TIntegerField;
    QrIMEIsDstGGX: TIntegerField;
    QrIMEIsQtdGer: TFloatField;
    QrIMEIsQtdAnt: TFloatField;
    QrIMEIsAptoUso: TSmallintField;
    QrIMEIsMarca: TWideStringField;
    QrIMEIsLk: TIntegerField;
    QrIMEIsDataCad: TDateField;
    QrIMEIsDataAlt: TDateField;
    QrIMEIsUserCad: TIntegerField;
    QrIMEIsUserAlt: TIntegerField;
    QrIMEIsAlterWeb: TSmallintField;
    QrIMEIsAtivo: TSmallintField;
    QrIMEIsTpCalcAuto: TIntegerField;
    QrIMEIsGraGruY: TIntegerField;
    Panel5: TPanel;
    DBGGraGruY: TdmkDBGridZTO;
    Panel6: TPanel;
    RGPosiNega: TRadioGroup;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    Panel7: TPanel;
    CkContinuar: TCheckBox;
    RGDataHora: TRadioGroup;
    TPDataSenha: TdmkEditDateTimePicker;
    EdHoraSenha: TdmkEdit;
    Label2: TLabel;
    QrLastMov: TmySQLQuery;
    QrLastMovDataHora: TDateTimeField;
    Label1: TLabel;
    EdSenha: TEdit;
    QrIMEIsTXMulFrnCab: TIntegerField;
    QrIMEIsClientMO: TIntegerField;
    QrIMEIsStqCenLoc: TIntegerField;
    QrIMEIsSerieTal: TIntegerField;
    QrIMEIsTalao: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGGraGruYAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure BtOKClick(Sender: TObject);
    procedure RGPosiNegaClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure RGDataHoraClick(Sender: TObject);
  private
    { Private declarations }
    FBalDtHr: String;
    //
    procedure BaixaIMEIAtual();
    procedure ReopenIMEIs();
    procedure SetaTodos(Ativo: Boolean);
  public
    { Public declarations }
    FDataHora: TDateTime;
    FControle: Integer;
    FCodigo, FMovimCod, FEmpresa: Integer;
  end;

  var
  FmTXExBItsIMEIs: TFmTXExBItsIMEIs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnTX_PF, ModuleGeral, UMySQLModule,
  MyDBCheck, StqCenLoc;

{$R *.DFM}

procedure TFmTXExBItsIMEIs.BaixaIMEIAtual();
var
  BalDtHr: String;
  //
  function DefineLastDtHr(DataHora: TDateTime): String;
  begin
    if TX_PF.ImpedePeloBalanco(DataHora, False, False) then
      Result := FBalDtHr
    else
      Result := Geral.FDT(DataHora, 109);
  end;
const
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOUni = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGer     = 0;
  AptoUso    = 0;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //
  ItemNFe    = 0;
  //
  QtdAnt     = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  SrcNivel1, SrcNivel2, GraGruY, SrcGGX,
  TXMulFrnCab, ClientMO, FornecMO, StqCenLoc, SerieTal, Talao: Integer;
  Qtde, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  SrcMovID       := TEstqmovimID(QrIMEIsMovimID.Value);
  SrcNivel1      := QrIMEIsCodigo.Value;
  SrcNivel2      := QrIMEIsControle.Value;
  SrcGGX         := QrIMEIsGraGruX.Value;
  //
  Codigo         := FCodigo;
  Controle       := 0;
  MovimCod       := FMovimCod;
  Empresa        := FEmpresa;
  ClientMO       := QrIMEIsClientMO.Value;
  FornecMO       := QrIMEIsFornecMO.Value;
  if FornecMO = 0 then
    FornecMO := QrIMEIsEmpresa.Value;
  StqCenLoc      := QrIMEIsStqCenLoc.Value;
  Terceiro       := QrIMEIsTerceiro.Value;
  TXMulFrnCab    := QrIMEIsTXMulFrnCab.Value;
  //DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimID        := emidExtraBxa;
  MovimNiv       := eminSemNiv;
  Pallet         := QrIMEIsPallet.Value;
  GraGruX        := QrIMEIsGraGruX.Value;
  Qtde           := -QrIMEIsSdoVrtQtd.Value;
  ValorT         := 0;
  Observ         := '';
  Marca          := QrIMEIsMarca.Value;
  GraGruY        := QrIMEIsGraGruY.Value;
  //
  SerieTal       := QrIMEIsSerieTal.Value;
  Talao          := QrIMEISTalao.Value;
  //
  if not TX_PF.ObtemControleIMEI(ImgTipo.SQLType, Controle, EdSenha.Text) then
  begin
    Close;
    Exit;
  end;
  case RGDataHora.ItemIndex of
    1: DataHora := Geral.FDT(FDataHora, 109);
    2: DataHora := Geral.FDT(TPDataSenha.Date, 1) + ' ' + EdHoraSenha.Text;
    3:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLastMov, Dmod.MyDB, [
      'SELECT MAX(DataHora) DataHora ',
      'FROM ' + CO_SEL_TAB_TMI + ' ',
      'WHERE SrcNivel2=' + Geral.FF0(QrIMEIsControle.Value),
      '']);
      DataHora := DefineLastDtHr(QrLastMovDataHora.Value);
    end;
    else
    begin
      Geral.MB_Erro('"Fonte de data / hora" n�o implementada!');
      DataHora := Geral.FDT(DmodG.ObtemAgora(), 109);
    end;
  end;
  //
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Qtde, ValorT,
  DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGer, AptoUso, FornecMO,
  SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab,
  ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei060(*Baixa extra 2/2*)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txexbcab', MovimCod);
    TX_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FControle := Controle;
  end;
end;

procedure TFmTXExBItsIMEIs.BtNenhumClick(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmTXExBItsIMEIs.BtOKClick(Sender: TObject);
var
  I: Integer;
  Senha: String;
begin
  if MyObjects.FIC(RGDataHora.ItemIndex = 0, RGDataHora,
    'Informe a " Fonte da data / hora de baixa"!') then
      Exit;
  Senha := EdSenha.Text;
  if Trim(Senha) <> '' then
  begin
    if Senha <> CO_MASTER then
    begin
      Geral.MB_Aviso('Senha invalida!');
      Halt(0);
    end;
  end;
  if TX_PF.ImpedePeloBalanco(Now(), False, False) then
    FBalDtHr := Geral.FDT(TX_PF.ObtemDataBalanco(FEmpresa), 109)
  else
    FBalDtHr := Geral.FDT(Now(), 109);
  if (DBGIMEIs.SelectedRows.Count > 0) then
  begin
    try
      for I := 0 to DBGIMEIs.SelectedRows.Count - 1 do
      begin
        //QrIMEIs.GotoBookmark(pointer(DBGIMEIs.SelectedRows.Items[I]));
        QrIMEIs.GotoBookmark(DBGIMEIs.SelectedRows.Items[I]);
        //
        BaixaIMEIAtual();
      end;
    finally
      ReopenIMEIs();
    end;
    if not CkContinuar.Checked then
      Close;
  end else
    Geral.MB_Aviso('Nenhum IME-I foi selecionado!');
end;

procedure TFmTXExBItsIMEIs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXExBItsIMEIs.BtTodosClick(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmTXExBItsIMEIs.DBGGraGruYAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
begin
  ReopenIMEIs();
end;

procedure TFmTXExBItsIMEIs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTXExBItsIMEIs.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
  FControle := 0;
  Agora := DModG.ObtemAgora();
  TPDataSenha.Date := Agora;
  //EdHoraSenha.ValueVariant := Agora;
end;

procedure TFmTXExBItsIMEIs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXExBItsIMEIs.RGDataHoraClick(Sender: TObject);
begin
//
end;

procedure TFmTXExBItsIMEIs.ReopenIMEIs();
var
  I: Integer;
  GraGruYs, Sinal: String;
begin
  GraGruYs := '';
  if (DBGGraGruY.SelectedRows.Count > 0) and
  (DBGGraGruY.SelectedRows.Count < QrGraGruY.RecordCount) then
  begin
    for I := 0 to DBGGraGruY.SelectedRows.Count - 1 do
    begin
      //QrGraGruY.GotoBookmark(pointer(DBGGraGruY.SelectedRows.Items[I]));
      QrGraGruY.GotoBookmark(DBGGraGruY.SelectedRows.Items[I]);
      //
      if GraGruYs <> '' then
        GraGruYs := GraGruYs + ',';
      GraGruYs := GraGruYs + Geral.FF0(QrGraGruYCodigo.Value);
    end;
  end;
  if Trim(GraGruYs) <> '' then
  begin
    if RGPosiNega.ItemIndex = 0 then
      Sinal := '>'
    else
      Sinal := '<';
    UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
    'SELECT CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, ggx.GraGruY, tmi.* ',
    'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=tmi.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
    'WHERE tmi.SdoVrtQtd' + Sinal + '0 ',
    'AND ggx.GraGruY IN (' + GraGruYs + ') ',
    'AND tmi.Empresa=' + Geral.FF0(FEmpresa),
    'ORDER BY tmi.Controle ',
    '']);
  end else
    QrIMEIs.Close;
end;

procedure TFmTXExBItsIMEIs.RGPosiNegaClick(Sender: TObject);
begin
  ReopenIMEIs();
  BtTodos.Enabled := RGPosiNega.ItemIndex = 1;
  BtNenhum.Enabled := RGPosiNega.ItemIndex = 1;
end;

procedure TFmTXExBItsIMEIs.SetaTodos(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBGIMEIs))
  else
    DBGIMEIs.SelectedRows.Clear;
end;

end.
