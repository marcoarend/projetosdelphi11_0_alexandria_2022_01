unit TXInnIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup,
  UnProjGroup_Consts, UnGrl_Geral, UnAppEnums, UnGrade_PF;

type
  TFmTXInnIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    Label8: TLabel;
    DBEdFornecedor: TdmkDBEdit;
    Label9: TLabel;
    EdObserv: TdmkEdit;
    Label10: TLabel;
    EdValorMP: TdmkEdit;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Panel7: TPanel;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    QrGraGruXCouNiv2: TFloatField;
    QrGraGruXGrandeza: TFloatField;
    SbStqCenLoc: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValorMPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbStqCenLocClick(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    procedure ReopenTXInnIts(Controle: Integer);

  public
    { Public declarations }
    FDataHora: TDateTime;
    FQrCab, FQrIts, FQrTribIncIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FFornece, FClientMO: Integer;
  end;

  var
  FmTXInnIts: TFmTXInnIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXInnCab, Principal, AppListas, UnTX_PF, ModTX_CRC;

{$R *.DFM}

procedure TFmTXInnIts.BtOKClick(Sender: TObject);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  Pallet     = 0;
  CustoMOUni = 0;
  CustoMOTot = 0;
  QtdGer     = 0;
  QtdAnt     = 0;
  AptoUso    = 1;
  FornecMO   = 0;
  //
  ExigeFornecedor = True;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  PedItsFin = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  TXMulFrnCab = 0;
  //
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, ClienteMO, GraGruX, Fornecedor,
  StqCenLoc, ReqMovEstq, TribDefSel, ClientMO: Integer;
  Qtde, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  EdQtdeX: TdmkEdit;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClienteMO      := FmTXInnCab.QrTXInnCabClienteMO.Value;
  Fornecedor     := Geral.IMV(DBEdFornecedor.Text);
  DataHora       := Geral.FDT(FDataHora, 109);
  MovimID        := emidCompra;
  MovimNiv       := eminSemNiv;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Qtde           := EdQtde.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := ValorMP;
  Observ         := EdObserv.Text;
  Marca          := EdMarca.Text;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  ClientMO       := FClientMO;
  //
  if TX_PF.TXFic(GraGruX, Empresa, Fornecedor, Pallet, Qtde, ValorT,
  EdGraGruX, EdPallet, EdQtde, EdValorMP, ExigeFornecedor,
    CO_GraGruY_1024_TXCadNat, EdStqCenLoc, 0, 0, nil, nil)
  then
    Exit;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Qtde, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGer,
  AptoUso, FornecMO, SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin,
  PedItsVda, ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO, QtdAnt,
  CO_0_SerieTal, CO_0_Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei001(*Item de entrada de mat�ria prima*)) then
  begin
    DmModTX_CRC.AtualizaImeiValorT(Controle);
    TX_PF.AtualizaTotaisTXXxxCab('txinncab', MovimCod);
    TX_PF.AtualizaSaldoIMEI(Controle, True);
    //s
    FmTXInnCab.AtualizaNFeItens();
    //
    if FmTXInnCab.QrTXInnCabClienteMO.Value <> 0 then
    begin
      DmModTX_CRC.AtualizaTXMovIts_CusEmit(Controle);
    end;
    //
    ReopenTXInnIts(Controle);
    //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdQtde.ValueVariant        := 0;
      EdValorMP.ValueVariant     := 0;
      EdObserv.Text              := '';
      //
      EdGraGruX.Enabled  := True;
      CBGraGruX.Enabled  := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTXInnIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXInnIts.EdValorMPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Preco, Qtde: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      Preco := Geral.DMV(Preco_TXT);
      Qtde  := EdQtde.ValueVariant;
      //
      EdValorMP.ValueVariant := Qtde * Preco
    end;
  end;
end;

procedure TFmTXInnIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  DBEdFornecedor.DataSource := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXInnIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  //
  TX_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1024_TXCadNat));
  //
  TX_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0, TEstqMovimID.emidCompra);
end;

procedure TFmTXInnIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXInnIts.ReopenTXInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXInnIts.SbStqCenLocClick(Sender: TObject);
begin
  TX_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidCompra);
end;

end.
