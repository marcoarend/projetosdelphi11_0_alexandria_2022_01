object FmTXMovItsAlt: TFmTXMovItsAlt
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-056 :: Altera'#231#227'o Manual de IME-I'
  ClientHeight = 669
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GBQtdOriginal: TGroupBox
    Left = 0
    Top = 389
    Width = 1008
    Height = 96
    Align = alTop
    Caption = ' Dados do item: '
    Enabled = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 79
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaQtde: TLabel
        Left = 8
        Top = 1
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label1: TLabel
        Left = 100
        Top = 1
        Width = 71
        Height = 13
        Caption = 'Valor total [F4]:'
      end
      object Label6: TLabel
        Left = 8
        Top = 41
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
      end
      object Label56: TLabel
        Left = 900
        Top = 1
        Width = 74
        Height = 13
        Caption = 'Req.Mov.Estq.:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 192
        Top = 1
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label58: TLabel
        Left = 736
        Top = 1
        Width = 86
        Height = 13
        Caption = 'Data / hora baixa:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdQtde: TdmkEdit
        Left = 8
        Top = 17
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 8
        Top = 57
        Width = 985
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Observ'
        UpdCampo = 'Observ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdValorT: TdmkEdit
        Left = 100
        Top = 17
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ValorT'
        UpdCampo = 'ValorT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdValorTKeyDown
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 192
        Top = 17
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'StqCenLoc'
        UpdCampo = 'StqCenLoc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdReqMovEstq: TdmkEdit
        Left = 900
        Top = 17
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReqMovEstq'
        UpdCampo = 'ReqMovEstq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 248
        Top = 17
        Width = 485
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 3
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDataHora: TdmkEditDateTimePicker
        Left = 736
        Top = 17
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtCompra'
        UpdCampo = 'DtaVisPrv'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDataHora: TdmkEdit
        Left = 844
        Top = 17
        Width = 53
        Height = 21
        TabOrder = 5
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 324
        Height = 32
        Caption = 'Altera'#231#227'o Manual de IME-I'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 324
        Height = 32
        Caption = 'Altera'#231#227'o Manual de IME-I'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 324
        Height = 32
        Caption = 'Altera'#231#227'o Manual de IME-I'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 555
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 599
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBDados: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 341
    Align = alTop
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 324
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel8: TPanel
        Left = 597
        Top = 0
        Width = 407
        Height = 324
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Gera'#231#227'o: '
          TabOrder = 0
          object Panel9: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label15: TLabel
              Left = 4
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Qtde'
              FocusControl = DBEdit15
            end
            object Label19: TLabel
              Left = 316
              Top = 0
              Width = 31
              Height = 13
              Caption = 'ValorT'
              FocusControl = DBEdit19
            end
            object DBEdit15: TDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              DataField = 'Qtde'
              DataSource = DsTXMovIts
              TabOrder = 0
            end
            object DBEdit19: TDBEdit
              Left = 316
              Top = 16
              Width = 80
              Height = 21
              DataField = 'ValorT'
              DataSource = DsTXMovIts
              TabOrder = 1
            end
          end
        end
        object GroupBox5: TGroupBox
          Left = 0
          Top = 57
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Saldo atual: '
          TabOrder = 1
          object Panel14: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label28: TLabel
              Left = 4
              Top = 0
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
              FocusControl = DBEdit28
            end
            object DBEdit28: TDBEdit
              Left = 4
              Top = 16
              Width = 88
              Height = 21
              DataField = 'SdoVrtQtd'
              DataSource = DsTXMovIts
              TabOrder = 0
            end
          end
        end
        object GroupBox6: TGroupBox
          Left = 0
          Top = 114
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Gera'#231#227'o a partir deste IME-I: '
          TabOrder = 2
          object Panel16: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label42: TLabel
              Left = 4
              Top = 0
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
              FocusControl = DBEdit42
            end
            object DBEdit42: TDBEdit
              Left = 4
              Top = 16
              Width = 88
              Height = 21
              DataField = 'QtdGer'
              DataSource = DsTXMovIts
              TabOrder = 0
            end
          end
        end
        object GroupBox7: TGroupBox
          Left = 0
          Top = 171
          Width = 407
          Height = 57
          Align = alTop
          Caption = ' Estoque Anterior ???: '
          TabOrder = 3
          object Panel17: TPanel
            Left = 2
            Top = 15
            Width = 403
            Height = 40
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label47: TLabel
              Left = 4
              Top = 0
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
              FocusControl = DBEdit47
            end
            object DBEdit47: TDBEdit
              Left = 4
              Top = 16
              Width = 88
              Height = 21
              DataField = 'QtdAnt'
              DataSource = DsTXMovIts
              TabOrder = 0
            end
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 228
          Width = 407
          Height = 96
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 4
          object Label7: TLabel
            Left = 8
            Top = 4
            Width = 47
            Height = 13
            Caption = 'MovimNiv'
            FocusControl = DBEdit6
          end
          object Label8: TLabel
            Left = 68
            Top = 4
            Width = 52
            Height = 13
            Caption = 'MovimTwn'
            FocusControl = DBEdit7
          end
          object Label9: TLabel
            Left = 128
            Top = 4
            Width = 42
            Height = 13
            Caption = 'MovimID'
            FocusControl = DBEdit8
          end
          object Label38: TLabel
            Left = 8
            Top = 44
            Width = 40
            Height = 13
            Caption = 'ValorMP'
            FocusControl = DBEdit38
          end
          object Label37: TLabel
            Left = 176
            Top = 44
            Width = 60
            Height = 13
            Caption = 'CustoMOTot'
            FocusControl = DBEdit37
          end
          object Label36: TLabel
            Left = 92
            Top = 44
            Width = 64
            Height = 13
            Caption = 'Custo MO uni'
            FocusControl = DBEdit36
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            DataField = 'MovimNiv'
            DataSource = DsTXMovIts
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 68
            Top = 20
            Width = 56
            Height = 21
            DataField = 'MovimTwn'
            DataSource = DsTXMovIts
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 128
            Top = 20
            Width = 56
            Height = 21
            DataField = 'MovimID'
            DataSource = DsTXMovIts
            TabOrder = 2
          end
          object DBEdit38: TDBEdit
            Left = 8
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ValorMP'
            DataSource = DsTXMovIts
            TabOrder = 3
          end
          object DBEdit36: TDBEdit
            Left = 92
            Top = 60
            Width = 80
            Height = 21
            DataField = 'CustoMOUni'
            DataSource = DsTXMovIts
            TabOrder = 4
          end
          object DBEdit37: TDBEdit
            Left = 176
            Top = 60
            Width = 80
            Height = 21
            DataField = 'CustoMOTot'
            DataSource = DsTXMovIts
            TabOrder = 5
          end
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 597
        Height = 324
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 597
          Height = 121
          Align = alTop
          Caption = ' Dados Gerais: '
          TabOrder = 0
          object Panel11: TPanel
            Left = 2
            Top = 15
            Width = 593
            Height = 104
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label2: TLabel
              Left = 4
              Top = 0
              Width = 28
              Height = 13
              Caption = 'IME-I:'
              FocusControl = DBEdCodigo
            end
            object Label13: TLabel
              Left = 64
              Top = 0
              Width = 29
              Height = 13
              Caption = 'Pallet:'
              FocusControl = DBEdit12
            end
            object Label3: TLabel
              Left = 124
              Top = 0
              Width = 167
              Height = 13
              Caption = 'Processo ou opera'#231#227'o de gera'#231#227'o:'
            end
            object Label12: TLabel
              Left = 472
              Top = 0
              Width = 46
              Height = 13
              Caption = 'DataHora'
              FocusControl = DBEdit11
            end
            object Label4: TLabel
              Left = 4
              Top = 40
              Width = 32
              Height = 13
              Caption = 'IME-C:'
              FocusControl = dmkDBEdit1
            end
            object Label5: TLabel
              Left = 64
              Top = 40
              Width = 44
              Height = 13
              Caption = 'Empresa:'
              FocusControl = DBEdit3
            end
            object Label14: TLabel
              Left = 124
              Top = 40
              Width = 42
              Height = 13
              Caption = 'Terceiro:'
              FocusControl = DBEdit4
            end
            object Label20: TLabel
              Left = 184
              Top = 40
              Width = 41
              Height = 13
              Caption = 'Cli. pref.:'
              FocusControl = DBEdit5
            end
            object Label21: TLabel
              Left = 4
              Top = 80
              Width = 33
              Height = 13
              Caption = 'Artigo: '
              FocusControl = DBEdit13
            end
            object Label52: TLabel
              Left = 244
              Top = 40
              Width = 90
              Height = 13
              Caption = 'N'#237'vel do processo:'
              FocusControl = DBEdit46
            end
            object DBEdCodigo: TdmkDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'Controle'
              DataSource = DsTXMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit12: TDBEdit
              Left = 64
              Top = 16
              Width = 56
              Height = 21
              DataField = 'Pallet'
              DataSource = DsTXMovIts
              TabOrder = 1
            end
            object DBEdit1: TDBEdit
              Left = 124
              Top = 16
              Width = 56
              Height = 21
              DataField = 'MovimCod'
              DataSource = DsTXMovIts
              TabOrder = 2
            end
            object DBEdit2: TDBEdit
              Left = 180
              Top = 16
              Width = 289
              Height = 21
              DataField = 'NO_EstqMovimID'
              DataSource = DsTXMovIts
              TabOrder = 3
            end
            object DBEdit11: TDBEdit
              Left = 472
              Top = 16
              Width = 112
              Height = 21
              DataField = 'DataHora'
              DataSource = DsTXMovIts
              TabOrder = 4
            end
            object dmkDBEdit1: TdmkDBEdit
              Left = 4
              Top = 56
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'Codigo'
              DataSource = DsTXMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 5
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit3: TDBEdit
              Left = 64
              Top = 56
              Width = 56
              Height = 21
              DataField = 'Empresa'
              DataSource = DsTXMovIts
              TabOrder = 6
            end
            object DBEdit4: TDBEdit
              Left = 124
              Top = 56
              Width = 56
              Height = 21
              DataField = 'Terceiro'
              DataSource = DsTXMovIts
              TabOrder = 7
            end
            object DBEdit5: TDBEdit
              Left = 184
              Top = 56
              Width = 56
              Height = 21
              DataField = 'CliVenda'
              DataSource = DsTXMovIts
              TabOrder = 8
            end
            object DBEdit13: TDBEdit
              Left = 44
              Top = 80
              Width = 56
              Height = 21
              DataField = 'GraGruX'
              TabOrder = 9
            end
            object DBEdit14: TDBEdit
              Left = 104
              Top = 80
              Width = 481
              Height = 21
              DataField = 'NO_PRD_TAM_COR'
              TabOrder = 10
            end
            object DBEdit46: TDBEdit
              Left = 244
              Top = 56
              Width = 341
              Height = 21
              DataField = 'NO_MovimNiv'
              TabOrder = 11
            end
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 121
          Width = 597
          Height = 60
          Align = alTop
          Caption = ' Dados de Origem: '
          TabOrder = 1
          object Panel12: TPanel
            Left = 2
            Top = 15
            Width = 593
            Height = 43
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label22: TLabel
              Left = 4
              Top = 0
              Width = 51
              Height = 13
              Caption = 'IME-I orig.:'
              FocusControl = dmkDBEdit2
            end
            object Label23: TLabel
              Left = 64
              Top = 0
              Width = 29
              Height = 13
              Caption = 'Pallet:'
              Enabled = False
              FocusControl = DBEdit20
            end
            object Label24: TLabel
              Left = 124
              Top = 0
              Width = 159
              Height = 13
              Caption = 'Processo ou opera'#231#227'o de origem:'
            end
            object Label25: TLabel
              Left = 528
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Artigo:'
              FocusControl = DBEdit23
            end
            object dmkDBEdit2: TdmkDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'SrcNivel2'
              DataSource = DsTXMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit20: TDBEdit
              Left = 64
              Top = 16
              Width = 56
              Height = 21
              TabOrder = 1
              Visible = False
            end
            object DBEdit21: TDBEdit
              Left = 124
              Top = 16
              Width = 56
              Height = 21
              DataField = 'SrcNivel1'
              DataSource = DsTXMovIts
              TabOrder = 2
            end
            object DBEdit22: TDBEdit
              Left = 180
              Top = 16
              Width = 345
              Height = 21
              DataField = 'NO_SrcMovID'
              DataSource = DsTXMovIts
              TabOrder = 3
            end
            object DBEdit23: TDBEdit
              Left = 528
              Top = 16
              Width = 56
              Height = 21
              DataField = 'SrcGGX'
              DataSource = DsTXMovIts
              TabOrder = 4
            end
          end
        end
        object GroupBox8: TGroupBox
          Left = 0
          Top = 181
          Width = 597
          Height = 60
          Align = alTop
          Caption = ' Dados de Destino '
          TabOrder = 2
          object Panel13: TPanel
            Left = 2
            Top = 15
            Width = 593
            Height = 43
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label26: TLabel
              Left = 4
              Top = 0
              Width = 54
              Height = 13
              Caption = 'IME-I dest.:'
              FocusControl = dmkDBEdit3
            end
            object Label27: TLabel
              Left = 64
              Top = 0
              Width = 29
              Height = 13
              Caption = 'Pallet:'
              Enabled = False
              FocusControl = DBEdit24
            end
            object Label31: TLabel
              Left = 124
              Top = 0
              Width = 162
              Height = 13
              Caption = 'Processo ou opera'#231#227'o de destino:'
            end
            object Label32: TLabel
              Left = 528
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Artigo:'
              FocusControl = DBEdit27
            end
            object dmkDBEdit3: TdmkDBEdit
              Left = 4
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              DataField = 'DstNivel2'
              DataSource = DsTXMovIts
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
              UpdType = utYes
              Alignment = taRightJustify
            end
            object DBEdit24: TDBEdit
              Left = 64
              Top = 16
              Width = 56
              Height = 21
              TabOrder = 1
              Visible = False
            end
            object DBEdit25: TDBEdit
              Left = 124
              Top = 16
              Width = 56
              Height = 21
              DataField = 'DstNivel1'
              DataSource = DsTXMovIts
              TabOrder = 2
            end
            object DBEdit26: TDBEdit
              Left = 180
              Top = 16
              Width = 345
              Height = 21
              DataField = 'NO_DstMovID'
              DataSource = DsTXMovIts
              TabOrder = 3
            end
            object DBEdit27: TDBEdit
              Left = 528
              Top = 16
              Width = 56
              Height = 21
              DataField = 'DstGGX'
              DataSource = DsTXMovIts
              TabOrder = 4
            end
          end
        end
        object Panel15: TPanel
          Left = 0
          Top = 241
          Width = 597
          Height = 83
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 3
          object Label40: TLabel
            Left = 240
            Top = 44
            Width = 30
            Height = 13
            Caption = 'Marca'
            FocusControl = DBEdit40
          end
          object Label41: TLabel
            Left = 128
            Top = 44
            Width = 40
            Height = 13
            Caption = 'GraGru1'
            FocusControl = DBEdit41
          end
          object Label33: TLabel
            Left = 68
            Top = 44
            Width = 30
            Height = 13
            Caption = 'Tal'#227'o:'
            FocusControl = DBEdit33
          end
          object Label35: TLabel
            Left = 360
            Top = 44
            Width = 50
            Height = 13
            Caption = 'FornecMO'
            FocusControl = DBEdit35
          end
          object Label46: TLabel
            Left = 8
            Top = 44
            Width = 57
            Height = 13
            Caption = 'S'#233'rie Tal'#227'o:'
            FocusControl = DBEdit32
          end
          object Label51: TLabel
            Left = 8
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Observ'
            FocusControl = DBEdit31
          end
          object DBEdit40: TDBEdit
            Left = 239
            Top = 60
            Width = 118
            Height = 21
            DataField = 'Marca'
            DataSource = DsTXMovIts
            TabOrder = 0
          end
          object DBEdit33: TDBEdit
            Left = 68
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Talao'
            DataSource = DsTXMovIts
            TabOrder = 1
          end
          object DBEdit41: TDBEdit
            Left = 128
            Top = 60
            Width = 56
            Height = 21
            DataField = 'GraGru1'
            DataSource = DsTXMovIts
            TabOrder = 2
          end
          object DBEdit35: TDBEdit
            Left = 360
            Top = 60
            Width = 56
            Height = 21
            DataField = 'FornecMO'
            DataSource = DsTXMovIts
            TabOrder = 3
          end
          object DBEdit32: TDBEdit
            Left = 8
            Top = 60
            Width = 56
            Height = 21
            DataField = 'SerieTal'
            DataSource = DsTXMovIts
            TabOrder = 4
          end
          object DBEdit31: TDBEdit
            Left = 8
            Top = 20
            Width = 580
            Height = 21
            DataField = 'Observ'
            DataSource = DsTXMovIts
            TabOrder = 5
          end
        end
      end
    end
  end
  object GBDadosArtigo: TGroupBox
    Left = 0
    Top = 485
    Width = 1008
    Height = 70
    Align = alClient
    Caption = 'Dados do artigo: '
    Enabled = False
    TabOrder = 5
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label53: TLabel
        Left = 7
        Top = 0
        Width = 68
        Height = 13
        Caption = 'S'#233'rie do tal'#227'o:'
      end
      object Label54: TLabel
        Left = 224
        Top = 0
        Width = 30
        Height = 13
        Caption = 'Tal'#227'o:'
      end
      object Label55: TLabel
        Left = 324
        Top = 0
        Width = 33
        Height = 13
        Caption = 'Marca:'
      end
      object CBSerieTal: TdmkDBLookupComboBox
        Left = 47
        Top = 16
        Width = 169
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTXSerTal
        TabOrder = 1
        dmkEditCB = EdSerieTal
        QryCampo = 'SerieTal'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSerieTal: TdmkEditCB
        Left = 7
        Top = 16
        Width = 40
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SerieTal'
        UpdCampo = 'SerieTal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSerieTal
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdTalao: TdmkEdit
        Left = 224
        Top = 16
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Talao'
        UpdCampo = 'Talao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMarca: TdmkEdit
        Left = 324
        Top = 16
        Width = 93
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Marca'
        UpdCampo = 'Marca'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkZerado: TdmkCheckBox
        Left = 500
        Top = 16
        Width = 201
        Height = 17
        Caption = 'Considerar o estoque como zerado.'
        TabOrder = 4
        QryCampo = 'Zerado'
        UpdCampo = 'Zerado'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
    end
  end
  object QrTXMovSrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 492
    Top = 65525
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMovSrcCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXMovSrcControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXMovSrcMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXMovSrcMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXMovSrcMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXMovSrcEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXMovSrcTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXMovSrcCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXMovSrcMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXMovSrcDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXMovSrcPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovSrcGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXMovSrcQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovSrcValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXMovSrcSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXMovSrcSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXMovSrcSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXMovSrcSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovSrcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXMovSrcSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXMovSrcTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXMovSrcFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXMovSrcCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXMovSrcDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXMovSrcDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXMovSrcDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXMovSrcQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovSrcQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovSrcStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovSrcReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXMovSrcGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrTXMovSrcNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrTXMovSrcNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrTXMovSrcNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrTXMovSrcNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXMovSrcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXMovSrcNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovSrcNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXMovSrcNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXMovSrcID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXMovSrcNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsTXMovSrc: TDataSource
    DataSet = QrTXMovSrc
    Left = 492
    Top = 37
  end
  object QrTXMovDst: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 568
    Top = 65525
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMovDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXMovDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXMovDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXMovDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXMovDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXMovDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXMovDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXMovDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXMovDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXMovDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXMovDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXMovDstQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXMovDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXMovDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXMovDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXMovDstSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXMovDstSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXMovDstTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXMovDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXMovDstCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXMovDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXMovDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXMovDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXMovDstQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovDstQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXMovDstGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrTXMovDstNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrTXMovDstNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrTXMovDstNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrTXMovDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXMovDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXMovDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovDstNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXMovDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXMovDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXMovDstNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsTXMovDst: TDataSource
    DataSet = QrTXMovDst
    Left = 568
    Top = 37
  end
  object QrTXMovIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrTXMovItsAfterOpen
    BeforeClose = QrTXMovItsBeforeClose
    OnCalcFields = QrTXMovItsCalcFields
    SQL.Strings = (
      'SELECT wmi.*,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR'
      'FROM txmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Controle=1087')
    Left = 408
    Top = 65525
    object QrTXMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTXMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrTXMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrTXMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrTXMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrTXMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrTXMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrTXMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXMovItsQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTXMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrTXMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrTXMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrTXMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrTXMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrTXMovItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrTXMovItsSerieTal: TIntegerField
      FieldName = 'SerieTal'
    end
    object QrTXMovItsTalao: TIntegerField
      FieldName = 'Talao'
    end
    object QrTXMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrTXMovItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
    end
    object QrTXMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrTXMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrTXMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrTXMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrTXMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrTXMovItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrTXMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXMovItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrTXMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXMovItsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrTXMovItsNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrTXMovItsNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrTXMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrTXMovItsZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrTXMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrTXMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
  end
  object DsTXMovIts: TDataSource
    DataSet = QrTXMovIts
    Left = 408
    Top = 37
  end
  object QrTXSerTal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM txsertal'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 636
    Top = 65524
    object QrTXSerTalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXSerTalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXSerTal: TDataSource
    DataSet = QrTXSerTal
    Left = 633
    Top = 33
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 708
    Top = 65524
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 708
    Top = 36
  end
end
