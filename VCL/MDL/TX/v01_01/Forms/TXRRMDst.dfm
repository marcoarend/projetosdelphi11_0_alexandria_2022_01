object FmTXRRMDst: TFmTXRRMDst
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-181 :: Destino de Artigo de Reprocesso / Reparo'
  ClientHeight = 654
  ClientWidth = 913
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 112
    Width = 913
    Height = 428
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 913
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 96
      Top = 20
      Width = 32
      Height = 13
      Caption = 'IME-C:'
      FocusControl = DBEdMovimCod
    end
    object Label3: TLabel
      Left = 180
      Top = 20
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object Label7: TLabel
      Left = 228
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Data / hora:'
      FocusControl = DBEdDtEntrada
    end
    object Label19: TLabel
      Left = 344
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Movim Twn:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdMovimCod: TdmkDBEdit
      Left = 96
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'MovimCod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 180
      Top = 36
      Width = 45
      Height = 21
      TabStop = False
      DataField = 'Empresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdDtEntrada: TdmkDBEdit
      Left = 228
      Top = 36
      Width = 112
      Height = 21
      TabStop = False
      DataField = 'DtHrAberto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object EdMovimTwn: TdmkEdit
      Left = 344
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 913
    Height = 428
    Align = alClient
    Caption = ' Dados do item a ser gerado: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 28
      Height = 13
      Caption = 'IME-I:'
    end
    object Label1: TLabel
      Left = 96
      Top = 16
      Width = 108
      Height = 13
      Caption = 'Artigo novo que gerou:'
    end
    object Label4: TLabel
      Left = 224
      Top = 56
      Width = 77
      Height = 13
      Caption = 'Ficha RMP [F4]:'
      Enabled = False
    end
    object Label11: TLabel
      Left = 12
      Top = 56
      Width = 83
      Height = 13
      Caption = 'S'#233'rie Ficha RMP:'
      Enabled = False
    end
    object Label12: TLabel
      Left = 320
      Top = 56
      Width = 33
      Height = 13
      Caption = 'Marca:'
      Enabled = False
    end
    object Label20: TLabel
      Left = 12
      Top = 304
      Width = 29
      Height = 13
      Caption = 'Pallet:'
    end
    object Label0: TLabel
      Left = 68
      Top = 304
      Width = 186
      Height = 13
      Caption = 'Nome do Artigo de Ribeira Classificado:'
    end
    object SbPallet1: TSpeedButton
      Left = 548
      Top = 319
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SbPallet1Click
    end
    object Label48: TLabel
      Left = 12
      Top = 344
      Width = 114
      Height = 13
      Caption = 'Item de pedido atrelado:'
    end
    object Label53: TLabel
      Left = 420
      Top = 56
      Width = 58
      Height = 13
      Caption = 'Data / hora:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label49: TLabel
      Left = 12
      Top = 384
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
    end
    object Label50: TLabel
      Left = 602
      Top = 384
      Width = 74
      Height = 13
      Caption = 'Req.Mov.Estq.:'
      Color = clBtnFace
      ParentColor = False
    end
    object SBNewPallet: TSpeedButton
      Left = 572
      Top = 319
      Width = 21
      Height = 21
      Caption = '+'
      OnClick = SBNewPalletClick
    end
    object EdCtrl1: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 549
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGraGruX
      TabOrder = 2
      dmkEditCB = EdGraGruX
      QryCampo = 'GraGruX'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGraGruX: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GraGruX'
      UpdCampo = 'GraGruX'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      OnRedefinido = EdGraGruXRedefinido
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdTalao: TdmkEdit
      Left = 224
      Top = 72
      Width = 92
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdTalaoKeyDown
    end
    object EdSerieTal: TdmkEditCB
      Left = 12
      Top = 72
      Width = 40
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'SerieFch'
      UpdCampo = 'SerieFch'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSerieTal
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBSerieTal: TdmkDBLookupComboBox
      Left = 52
      Top = 72
      Width = 169
      Height = 21
      Enabled = False
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTXSerTal
      TabOrder = 4
      dmkEditCB = EdSerieTal
      QryCampo = 'SerieFch'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMarca: TdmkEdit
      Left = 320
      Top = 72
      Width = 93
      Height = 21
      CharCase = ecUpperCase
      Enabled = False
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Marca'
      UpdCampo = 'Marca'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object GroupBox3: TGroupBox
      Left = 12
      Top = 100
      Width = 685
      Height = 101
      Caption = 
        '                                                                ' +
        '     '
      TabOrder = 10
      object CkBaixa: TCheckBox
        Left = 16
        Top = -2
        Width = 189
        Height = 17
        Caption = 'Baixar do estoque de em opera'#231#227'o:'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 0
        OnClick = CkBaixaClick
      end
      object PnBaixa: TPanel
        Left = 2
        Top = 15
        Width = 681
        Height = 84
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label8: TLabel
          Left = 8
          Top = 3
          Width = 82
          Height = 13
          Caption = 'Quantidade: [F4]:'
        end
        object Label16: TLabel
          Left = 376
          Top = 4
          Width = 50
          Height = 13
          Caption = 'Valor total:'
          Enabled = False
        end
        object Label17: TLabel
          Left = 472
          Top = 4
          Width = 28
          Height = 13
          Caption = 'IME-I:'
          Enabled = False
        end
        object Label18: TLabel
          Left = 8
          Top = 43
          Width = 61
          Height = 13
          Caption = 'Observa'#231#227'o:'
        end
        object EdBxaQtde: TdmkEdit
          Left = 8
          Top = 19
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdBxaQtdeChange
          OnKeyDown = EdBxaQtdeKeyDown
        end
        object EdBxaValorT: TdmkEdit
          Left = 377
          Top = 19
          Width = 92
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdBxaValorTChange
        end
        object EdCtrl2: TdmkEdit
          Left = 472
          Top = 19
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdBxaObserv: TdmkEdit
          Left = 8
          Top = 59
          Width = 665
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object BtCalculo: TButton
          Left = 556
          Top = 16
          Width = 75
          Height = 25
          Caption = 'C'#225'lculos'
          TabOrder = 3
          TabStop = False
          OnClick = BtCalculoClick
        end
      end
    end
    object EdPallet: TdmkEditCB
      Left = 12
      Top = 320
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPallet
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPallet: TdmkDBLookupComboBox
      Left = 68
      Top = 320
      Width = 477
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsTXPallet
      TabOrder = 13
      dmkEditCB = EdPallet
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPedItsFin: TdmkEditCB
      Left = 12
      Top = 360
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPedItsFin
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPedItsFin: TdmkDBLookupComboBox
      Left = 68
      Top = 360
      Width = 629
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsTXPedIts
      TabOrder = 16
      dmkEditCB = EdPedItsFin
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object GroupBox4: TGroupBox
      Left = 12
      Top = 204
      Width = 685
      Height = 97
      Caption = ' Entrada no estoque de couro processado (pronto):'
      TabOrder = 11
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 681
        Height = 80
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaPecas: TLabel
          Left = 8
          Top = 0
          Width = 54
          Height = 13
          Caption = 'Pe'#231'as [F4]:'
        end
        object Label9: TLabel
          Left = 84
          Top = 0
          Width = 61
          Height = 13
          Caption = 'Observa'#231#227'o:'
        end
        object Label13: TLabel
          Left = 620
          Top = 0
          Width = 53
          Height = 13
          Caption = '% Rendim.:'
          Enabled = False
        end
        object Label14: TLabel
          Left = 488
          Top = 40
          Width = 66
          Height = 13
          Caption = '$ Custo Total:'
          Enabled = False
        end
        object Label15: TLabel
          Left = 276
          Top = 39
          Width = 59
          Height = 13
          Caption = '$ Custo MO:'
        end
        object Label22: TLabel
          Left = 8
          Top = 39
          Width = 74
          Height = 13
          Caption = '$ MO/Unidade:'
        end
        object Label21: TLabel
          Left = 392
          Top = 40
          Width = 72
          Height = 13
          Caption = '$ Frete retorno:'
          Enabled = False
        end
        object EdQtde: TdmkEdit
          Left = 8
          Top = 16
          Width = 72
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtdeChange
          OnKeyDown = EdQtdeKeyDown
        end
        object EdObserv: TdmkEdit
          Left = 84
          Top = 16
          Width = 269
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ'
          UpdCampo = 'Observ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdRendimento: TdmkEdit
          Left = 621
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValorT: TdmkEdit
          Left = 489
          Top = 56
          Width = 188
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCustoMOTot: TdmkEdit
          Left = 277
          Top = 55
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCustoMOUni: TdmkEdit
          Left = 8
          Top = 55
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdCustoMOUniChange
        end
        object EdCusFrtMORet: TdmkEdit
          Left = 393
          Top = 55
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'CusFrtMORet'
          UpdCampo = 'CusFrtMORet'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdCusFrtMORetChange
        end
      end
    end
    object TPData: TdmkEditDateTimePicker
      Left = 420
      Top = 72
      Width = 108
      Height = 21
      Date = 0.639644131944805900
      Time = 0.639644131944805900
      TabOrder = 7
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DtHrAberto'
      UpdCampo = 'DtHrAberto'
      UpdType = utYes
      DatePurpose = dmkdpSPED_EFD_MIN
    end
    object EdHora: TdmkEdit
      Left = 528
      Top = 72
      Width = 40
      Height = 21
      TabOrder = 8
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00:00'
      QryName = 'QrVSGerArt'
      QryCampo = 'DtHrAberto'
      UpdCampo = 'DtHrAberto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdStqCenLoc: TdmkEditCB
      Left = 12
      Top = 400
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 17
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStqCenLoc: TdmkDBLookupComboBox
      Left = 67
      Top = 400
      Width = 530
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 18
      dmkEditCB = EdStqCenLoc
      UpdType = utYes
      LocF7NameFldName = 'Nome'
      LocF7SQLMasc = '$#'
    end
    object EdReqMovEstq: TdmkEdit
      Left = 603
      Top = 400
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 19
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object MeAviso: TMemo
      Left = 708
      Top = 20
      Width = 197
      Height = 401
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 9
    end
    object CkEncerraPallet: TdmkCheckBox
      Left = 596
      Top = 323
      Width = 97
      Height = 17
      Caption = 'Encerra o Pallet.'
      TabOrder = 14
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 913
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 865
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 817
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 514
        Height = 32
        Caption = 'Destino de Artigo de Reprocesso / Reparo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 514
        Height = 32
        Caption = 'Destino de Artigo de Reprocesso / Reparo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 514
        Height = 32
        Caption = 'Destino de Artigo de Reprocesso / Reparo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 540
    Width = 913
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 909
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 584
    Width = 913
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 767
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 765
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 504
    Top = 56
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 504
    Top = 104
  end
  object QrTXSerTal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 576
    Top = 56
    object QrTXSerTalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXSerTalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXSerTal: TDataSource
    DataSet = QrTXSerTal
    Left = 576
    Top = 104
  end
  object QrTXPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 428
    Top = 57
    object QrTXPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrTXPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrTXPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrTXPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrTXPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsTXPallet: TDataSource
    DataSet = QrTXPallet
    Left = 428
    Top = 105
  end
  object QrTXPedIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 648
    Top = 56
    object QrTXPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrTXPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsTXPedIts: TDataSource
    DataSet = QrTXPedIts
    Left = 648
    Top = 100
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 344
    Top = 56
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 344
    Top = 104
  end
  object PMPallet: TPopupMenu
    Left = 600
    Top = 448
    object Criar1: TMenuItem
      Caption = '&Criar Novo'
      OnClick = Criar1Click
    end
    object Gerenciamento1: TMenuItem
      Caption = '&Gerenciamento'
      OnClick = Gerenciamento1Click
    end
  end
  object PMCalculo: TPopupMenu
    Left = 592
    Top = 348
    object Peasapartirdareadesemi1: TMenuItem
      Caption = 'Pe'#231'as e '#225'rea WB a partir da '#225'rea de semi'
    end
    object Pesoderaspa1: TMenuItem
      Caption = 'Pe'#231'as de raspa a partir da '#225'rea de semi'
      OnClick = Pesoderaspa1Click
    end
  end
end
