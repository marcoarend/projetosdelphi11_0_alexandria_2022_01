unit TXRRMDst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, dmkRadioGroup, Vcl.ComCtrls,
  dmkEditDateTimePicker, Vcl.Menus, UnProjGroup_Consts, dmkCheckBox,
  UnAppEnums, UnTX_Jan;

type
  TFmTXRRMDst = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdCtrl1: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DBEdMovimCod: TdmkDBEdit;
    Label2: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    Label3: TLabel;
    Label7: TLabel;
    DBEdDtEntrada: TdmkDBEdit;
    EdTalao: TdmkEdit;
    Label4: TLabel;
    QrTXSerTal: TmySQLQuery;
    QrTXSerTalCodigo: TIntegerField;
    QrTXSerTalNome: TWideStringField;
    DsTXSerTal: TDataSource;
    Label11: TLabel;
    EdSerieTal: TdmkEditCB;
    CBSerieTal: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdMarca: TdmkEdit;
    GroupBox3: TGroupBox;
    CkBaixa: TCheckBox;
    PnBaixa: TPanel;
    Label8: TLabel;
    EdBxaQtde: TdmkEdit;
    Label16: TLabel;
    EdBxaValorT: TdmkEdit;
    EdCtrl2: TdmkEdit;
    Label17: TLabel;
    EdBxaObserv: TdmkEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    QrTXPallet: TmySQLQuery;
    QrTXPalletCodigo: TIntegerField;
    QrTXPalletNome: TWideStringField;
    QrTXPalletLk: TIntegerField;
    QrTXPalletDataCad: TDateField;
    QrTXPalletDataAlt: TDateField;
    QrTXPalletUserCad: TIntegerField;
    QrTXPalletUserAlt: TIntegerField;
    QrTXPalletAlterWeb: TSmallintField;
    QrTXPalletAtivo: TSmallintField;
    QrTXPalletEmpresa: TIntegerField;
    QrTXPalletNO_EMPRESA: TWideStringField;
    QrTXPalletStatus: TIntegerField;
    QrTXPalletCliStat: TIntegerField;
    QrTXPalletGraGruX: TIntegerField;
    QrTXPalletNO_CLISTAT: TWideStringField;
    QrTXPalletNO_PRD_TAM_COR: TWideStringField;
    QrTXPalletNO_STATUS: TWideStringField;
    DsTXPallet: TDataSource;
    Label20: TLabel;
    Label0: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet1: TSpeedButton;
    QrTXPedIts: TmySQLQuery;
    QrTXPedItsNO_PRD_TAM_COR: TWideStringField;
    QrTXPedItsControle: TIntegerField;
    DsTXPedIts: TDataSource;
    EdPedItsFin: TdmkEditCB;
    Label48: TLabel;
    CBPedItsFin: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    Panel3: TPanel;
    EdQtde: TdmkEdit;
    LaPecas: TLabel;
    EdObserv: TdmkEdit;
    Label9: TLabel;
    EdRendimento: TdmkEdit;
    Label13: TLabel;
    EdValorT: TdmkEdit;
    Label14: TLabel;
    EdCustoMOTot: TdmkEdit;
    Label15: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    Label50: TLabel;
    EdReqMovEstq: TdmkEdit;
    PMPallet: TPopupMenu;
    Criar1: TMenuItem;
    Gerenciamento1: TMenuItem;
    MeAviso: TMemo;
    BtCalculo: TButton;
    PMCalculo: TPopupMenu;
    Peasapartirdareadesemi1: TMenuItem;
    Pesoderaspa1: TMenuItem;
    SBNewPallet: TSpeedButton;
    CkEncerraPallet: TdmkCheckBox;
    Label22: TLabel;
    EdCustoMOUni: TdmkEdit;
    Label21: TLabel;
    EdCusFrtMORet: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTalaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkBaixaClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure EdBxaQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBxaValorTChange(Sender: TObject);
    procedure EdQtdeChange(Sender: TObject);
    procedure EdBxaQtdeChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure Criar1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure Pesoderaspa1Click(Sender: TObject);
    procedure BtCalculoClick(Sender: TObject);
    procedure SBNewPalletClick(Sender: TObject);
    procedure EdQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCustoMOUniChange(Sender: TObject);
    procedure EdCusFrtMORetChange(Sender: TObject);
  private
    { Private declarations }
    FUltGGX, FGraGruXDst, FFatorIntDst, FFatorIntSrc: Integer;
    //
    procedure ReopenTXInnIts(Controle: Integer);
  public
    { Public declarations }
    //FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FClientMO, FGraGruXSrc, FFornecMO: Integer;
    FValM2, FSdoQtde: Double;
    FPallOnEdit: array of Integer;
    FAreaTotal: Double;
    //
    procedure CalculaCusto();
  end;

  var
  FmTXRRMDst: TFmTXRRMDst;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  TXRRMCab, Principal, UnTX_PF, AppListas, UnGrade_PF(*, CalcParc4Val*);

{$R *.DFM}

procedure TFmTXRRMDst.BtOKClick(Sender: TObject);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CliVenda   = 0;
  QtdGer     = 0;
  QtdAnt     = 0;
  AptoUso    = 1;
  //
  Fornecedor = 0;
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  PedItsLib = 0;
  PedItsVda = 0;
  //
  ItemNFe     = 0;
  TXMulFrnCab = 0;
  //
var
  DataHora, Observ, Marca: String;
  Codigo, Controle, MovimCod, Empresa, GraGruX, SerieTal, Talao, Ctrl1, Ctrl2,
  MovimTwn, Pallet, PedItsFin, StqCenLoc, ReqMovEstq, ClientMO, PalToClose,
  FornecMO: Integer;
  Qtde, CustoMOUni, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
begin
{
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := 0;
  MovimTwn       := EdMovimTwn.ValueVariant;
  Ctrl1          := EdCtrl1.ValueVariant;
  Ctrl2          := EdCtrl2.ValueVariant;
  MovimCod       := Geral.IMV(DBEdMovimCod.Text);
  Empresa        := FEmpresa;
  ClientMO       := FClientMO;
  //
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  MovimID        := emidEmReprRM;
  MovimNiv       := eminDestRRM;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Qtde           := EdQtde.ValueVariant;
  CustoMOUni     := EdCustoMOUni.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  ValorMP        := EdBxaValorT.ValueVariant;
  Observ         := EdObserv.Text;
  SerieTal       := EdSerieTal.ValueVariant;
  Talao          := EdTalao.ValueVariant;
  Marca          := EdMarca.Text;
  Pallet         := EdPallet.ValueVariant;
  PalToClose     := Pallet;
  PedItsFin      := EdPedItsFin.ValueVariant;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  FornecMO       := FFornecMO;
  //
  if TX_PF.TalaoErro(EdSerieTal, Empresa, Ctrl1, Talao) then
    Exit;
  // Pode ser raspa por peso sem custo!
  if TX_PF.TXFic(GraGruX, Empresa, Fornecedor, Pallet, Qtde, ValorT, EdGraGruX,
  EdPallet, EdQtde, EdValorT, ExigeFornecedor, CO_GraGruY_6144_TXCadFcc, EdStqCenLoc,
  SerieTal, Talao, EdSerieTal, EdTalao) then
    Exit;
  //
  MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Ctrl1);
  //
  if TX_PF.InsUpdTXMovIts(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Qtde,
  ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, CliVenda, Controle,
  CustoMOUni, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
  QtdGer, AptoUso, FornecMO,
  SrcGGX, DstGGX, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
  QtdAnt, SerieTal, Talao,
  CO_0_GGXRcl,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_FALSE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iutpei090(*Gera��o de material resultante de reprocesso/reparo*)) then
  begin
    Ctrl1  := Controle;
    Pallet := 0;
    TX_PF.AtualizaSaldoIMEI(Ctrl1, True);
    TX_PF.AtualizaTXPedIts_Fin(PedItsFin);
    //
    if CkBaixa.Checked then
    begin
      GraGruX        := FmTXRRMCab.QrTXRRMAtuGraGruX.Value;
      SerieTal       := FmTXRRMCab.QrTXRRMAtuSerieTal.Value;
      Talao          := FmTXRRMCab.QrTXRRMAtuTalao.Value;
      Marca          := FmTXRRMCab.QrTXRRMAtuMarca.Value;
      MovimID        := emidEmReprRM;
      MovimNiv       := eminEmRRMBxa;
      Qtde           := -EdBxaQtde.ValueVariant;
      ValorT         := -EdBxaValorT.ValueVariant;
      ValorMP        := 0;
      CustoMOUni     := 0;
      CustoMOTot     := 0;
      Observ         := EdBxaObserv.Text;
      //
      //PedItsLib      := 0;
      PedItsFin      := 0;
      //PedItsVda      := 0;
      //GSPSrcMovID     := 0;
      //GSPSrcNiv2     := 0;
      ReqMovEstq     := 0;

      Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Ctrl2);
      //
      if TX_PF.InsUpdVSMovIts2(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
      MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
      DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
      CliVenda, Controle, Ficha, (*Misturou,*) CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
      SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
      AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, TXMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl,
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei091(*Baixa de couro em reprocesso/reparo na gera��o de couro resultante de reprocesso/reparo*)) then
      begin
        Ctrl2 := Controle;
        //TX_PF.AtualizaSaldoIMEI(Ctrl2, True);
        //TX_PF.AtualizaSaldoIMEI(FmTXRRMCab.QrTXRRMAtuControle.Value, True);
      end;
    end;
    // Nao se aplica. Calcula com function propria a seguir.
    //TX_PF.AtualizaTotaisTXXxxCab('TXRRMcab', MovimCod);
    TX_PF.AtualizaTotaisTXRRMCab(FmTXRRMCab.QrTXRRMCabMovimCod.Value);
    TX_PF.AtualizaNF_IMEI(FmTXRRMCab.QrTXRRMCabMovimCod.Value, Ctrl1);
    FmTXRRMCab.AtualizaNFeItens();
    //
    if CkEncerraPallet.Checked then
      TX_PF.EncerraPalletSimples(PalToClose, FEmpresa, FClientMO, QrTXPallet, FPallOnEdit);
    TX_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmReprRM, MovimCod);
    FmTXRRMCab.LocCod(Codigo, Codigo);
    ReopenTXInnIts(Ctrl1);
    if FmTXRRMCab.QrTXRRMAtuFornecMO.Value <>
    FmTXRRMCab.QrTXRRMAtuEmpresa.Value then
    begin
      if ImgTipo.SQLType = stIns then
        FmTXRRMCab.IncluiAtrelamento1Click(FmTXRRMCab.IncluiAtrelamento1)
      else
        FmTXRRMCab.AlteraAtrelamento1Click(FmTXRRMCab.AlteraAtrelamento1);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdPecas.ReadOnly           := True;
      EdCtrl1.ValueVariant       := 0;
      EdCtrl2.ValueVariant       := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdValorT.ValueVariant      := 0;
      EdObserv.Text              := '';
      EdFicha.ValueVariant       := 0;
      EdMarca.ValueVariant       := '';
      //
      //CkBaixa.Checked            := False;
      EdBxaPecas.ValueVariant    := 0;
      EdBxaPesoKg.ValueVariant   := 0;
      EdBxaAreaM2.ValueVariant   := 0;
      EdBxaAreaP2.ValueVariant   := 0;
      EdBxaValorT.ValueVariant   := 0;
      EdBxaObserv.ValueVariant   := '';
      //
      EdGraGruX.Enabled          := True;
      CBGraGruX.Enabled          := True;
      //EdSerieFch.Enabled         := True;
      //CBSerieFch.Enabled         := True;
      //EdFicha.Enabled            := True;
      //EdMarca.Enabled            := True;
      //EdValorT.Enabled           := True;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      SBNewPallet.Enabled        := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
}
end;

procedure TFmTXRRMDst.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXRRMDst.BtCalculoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCalculo, BtCalculo);
end;

procedure TFmTXRRMDst.CalculaCusto();
var
  BxaValorT, ValorT, Qtde, MOUni, ValorMP, ValorT2, CustoMOTot,
  CusFrtMORet: Double;
begin
  BxaValorT   := EdBxaValorT.ValueVariant;
  //////////////////////////////////////////////////////////////////////////////
  Qtde        := EdQtde.ValueVariant;
  MOUni       := EdCustoMOUni.ValueVariant;
  //
  CusFrtMORet := EdCusFrtMORet.ValueVariant;
  CustoMOTot  := (Qtde * MOUni);
  //////////////////////////////////////////////////////////////////////////////
  ValorT      := BxaValorT + CustoMOTot + CusFrtMORet;
  //////////////////////////////////////////////////////////////////////////////
  EdCustoMOTot.ValueVariant := CustoMOTot;
  EdValorT.ValueVariant     := ValorT;
end;

procedure TFmTXRRMDst.CkBaixaClick(Sender: TObject);
begin
  PnBaixa.Visible := CkBaixa.Checked;
end;

procedure TFmTXRRMDst.Criar1Click(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  TX_PF.CadastraPalletRibCla(FEmpresa, FClientMO, EdPallet, CBPallet,
    QrTXPallet, emidEmReprRM, GraGruX);
  UnDmkDAC_PF.AbreQuery(QrTXPallet, Dmod.MyDB);
end;

procedure TFmTXRRMDst.EdBxaQtdeChange(Sender: TObject);
var
  Qtde, FatorQtde: Double;
begin
  // ver pelas pecas se eh o resto e pegar todo resto da �rea
  Qtde := EdBxaQtde.ValueVariant;
  if (FSdoQtde > 0) and (ImgTipo.SQLType = stIns) then
  begin
    if Qtde >= FSdoQtde then
      FatorQtde := 1
    else
      FatorQtde := Qtde / FSdoQtde;
  end;
  //
  if (FFatorIntSrc <> 0) and (FFatorIntDst <> 0) then
    EdQtde.ValueVariant := Qtde * FFatorIntSrc / FFatorIntDst;
  //
  /// Precisa?
  //EdBxaValorT.ValueVariant := FValM2 * EdBxaAreaM2.ValueVariant;
end;

procedure TFmTXRRMDst.EdBxaQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdBxaQtde.ValueVariant  := FmTXRRMCab.QrTXRRMCabQtdeSdo.Value;
end;

procedure TFmTXRRMDst.EdBxaValorTChange(Sender: TObject);
begin
  EdValorT.ValueVariant := EdBxaValorT.ValueVariant;
end;

procedure TFmTXRRMDst.EdCusFrtMORetChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmTXRRMDst.EdCustoMOUniChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmTXRRMDst.EdTalaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Talao: Integer;
begin
  if Key = VK_F4 then
    if TX_PF.ObtemProximoTalao(FEmpresa, EdSerieTal, Talao) then
      EdTalao.ValueVariant := Talao;
end;

procedure TFmTXRRMDst.EdGraGruXChange(Sender: TObject);
begin
  FGraGruXDst := EdGraGruX.ValueVariant;
  EdPallet.ValueVariant := 0;
  CBPallet.KeyValue     := Null;
  if FGraGruXDst = 0 then
    QrTXPallet.Close
  else
    TX_PF.ReopenTXPallet(QrTXPallet, FEmpresa, FClientMO, FGraGruXDst, '', FPallOnEdit);
end;

procedure TFmTXRRMDst.EdGraGruXRedefinido(Sender: TObject);
begin
  if (FGraGruXDst <> null) and (FGraGruXDst <> 0) then
  begin
    TX_PF.ObtemFatorInteiro(FGraGruXSrc, FFatorIntSrc, False, 1, MeAviso);
    TX_PF.ObtemFatorInteiro(FGraGruXDst, FFatorIntDst, True, FFatorIntSrc, MeAviso);
  end else
  begin
    FFatorIntSrc := 0;
    FFatorIntDst := 0;
  end;
end;

procedure TFmTXRRMDst.EdQtdeChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmTXRRMDst.EdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdQtde.ReadOnly := False;
    Geral.MB_Info('Informe a quantidade direto na caixa de edi��o!')
  end;
end;

procedure TFmTXRRMDst.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
  DBEdDtEntrada.DataSource  := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmTXRRMDst.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAreaTotal := 0;
  FUltGGX  := 0;
  FGraGruXDst := 0;
  TPData.Date := Now();
  EdHora.ValueVariant := Now();
  //
  TX_PF.AbreGraGruXY(QrGraGruX, '');
    //'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_TXFinCla));
  //
  TX_PF.AbreTXSerTal(QrTXSerTal);
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
end;

procedure TFmTXRRMDst.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXRRMDst.Gerenciamento1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXPallet(EdPallet.ValueVariant);
end;

procedure TFmTXRRMDst.Pesoderaspa1Click(Sender: TObject);
var
  KgM2, ArM2: String;
  Area, Peso, Pecas: Double;
begin
{POIU
  if FSdoPesoKg > 0 then
  begin
    KgM2 := '3,500';
    if InputQuery('Calculo de pe�as de raspa', 'Kg por m�:', KgM2) then
    begin
      Area := EdAreaM2.ValueVariant;
      if Area = 0 then
      begin
        ArM2 := '0,00';
        if InputQuery('Calculo de pe�as de raspa', '�rea m�:', ArM2) then
        begin
          Area := Geral.DMV(ArM2);
          EdAreaM2.ValueVariant := Area;
        end;
      end;
      Peso := Geral.DMV(KgM2);
      Peso := Round(Area * Peso);
      //
      Pecas := Round(Peso / FSdoPesoKg * FSdoPecas);
      //
      EdBxaPecas.ValueVariant := Pecas;
    end;
  end;
}
end;

procedure TFmTXRRMDst.ReopenTXInnIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTXRRMDst.SBNewPalletClick(Sender: TObject);
begin
  TX_PF.GeraNovoPallet(FEmpresa, FClientMO, EdGraGruX.ValueVariant,
  EdGraGruX, EdPallet, CBPallet, SBNewPallet, QrTXPallet, FPallOnEdit);
end;

procedure TFmTXRRMDst.SbPallet1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, SbPallet1);
end;

end.
