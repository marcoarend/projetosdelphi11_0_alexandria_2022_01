object FmTXIndDst: TFmTXIndDst
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-041 :: Destino de Artigo em Industraliza'#231#227'o'
  ClientHeight = 631
  ClientWidth = 760
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 760
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 96
      Top = 20
      Width = 32
      Height = 13
      Caption = 'IME-C:'
      FocusControl = DBEdMovimCod
    end
    object Label3: TLabel
      Left = 180
      Top = 20
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object Label7: TLabel
      Left = 228
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Data / hora:'
      FocusControl = DBEdDtEntrada
    end
    object Label19: TLabel
      Left = 344
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Movim Twn:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdMovimCod: TdmkDBEdit
      Left = 96
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'MovimCod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 180
      Top = 36
      Width = 45
      Height = 21
      TabStop = False
      DataField = 'Empresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdDtEntrada: TdmkDBEdit
      Left = 228
      Top = 36
      Width = 112
      Height = 21
      TabStop = False
      DataField = 'DtHrAberto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object EdMovimTwn: TdmkEdit
      Left = 344
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 760
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 712
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 664
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 445
        Height = 32
        Caption = 'Destino de Artigo em Industraliza'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 445
        Height = 32
        Caption = 'Destino de Artigo em Industraliza'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 445
        Height = 32
        Caption = 'Destino de Artigo em Industraliza'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 561
    Width = 760
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 614
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 612
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkCpermitirCrust: TCheckBox
        Left = 160
        Top = 16
        Width = 141
        Height = 17
        Caption = 'Permitir semi e acabado.'
        TabOrder = 1
      end
    end
  end
  object MeAviso: TMemo
    Left = 616
    Top = 112
    Width = 144
    Height = 449
    TabStop = False
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
  end
  object Panel3: TPanel
    Left = 0
    Top = 112
    Width = 616
    Height = 449
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object GroupBox3: TGroupBox
      Left = 0
      Top = 45
      Width = 616
      Height = 100
      Align = alTop
      Caption = ' Baixar do estoque de em industrializa'#231#227'o:  '
      TabOrder = 1
      object PnBaixa: TPanel
        Left = 2
        Top = 15
        Width = 612
        Height = 83
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label17: TLabel
          Left = 528
          Top = 4
          Width = 28
          Height = 13
          Caption = 'IME-I:'
          Enabled = False
        end
        object Label18: TLabel
          Left = 392
          Top = 43
          Width = 61
          Height = 13
          Caption = 'Observa'#231#227'o:'
        end
        object Label4: TLabel
          Left = 8
          Top = 3
          Width = 56
          Height = 13
          Caption = 'IME-I baixa:'
        end
        object Label11: TLabel
          Left = 92
          Top = 3
          Width = 168
          Height = 13
          Caption = 'Nome do Artigo em industrializa'#231#227'o:'
        end
        object Label8: TLabel
          Left = 8
          Top = 43
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label14: TLabel
          Left = 200
          Top = 43
          Width = 84
          Height = 13
          Caption = 'Qtde saldo futuro:'
          Enabled = False
        end
        object Label15: TLabel
          Left = 104
          Top = 43
          Width = 67
          Height = 13
          Caption = 'Qtde estoque:'
          Enabled = False
        end
        object Label22: TLabel
          Left = 296
          Top = 43
          Width = 55
          Height = 13
          Caption = 'Valor baixa:'
        end
        object EdCtrl2: TdmkEdit
          Left = 528
          Top = 19
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdBxaObserv: TdmkEdit
          Left = 392
          Top = 59
          Width = 217
          Height = 21
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdIMEISrc: TdmkEditCB
          Left = 8
          Top = 19
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdIMEISrcRedefinido
          DBLookupComboBox = CBIMEISrc
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBIMEISrc: TdmkDBLookupComboBox
          Left = 92
          Top = 19
          Width = 429
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsTXIndInn
          TabOrder = 1
          dmkEditCB = EdIMEISrc
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdBxaQtde: TdmkEdit
          Left = 8
          Top = 59
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdBxaQtdeRedefinido
        end
        object EdEstqQtdeFut: TdmkEdit
          Left = 200
          Top = 59
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdEstqQtdeAtu: TdmkEdit
          Left = 104
          Top = 59
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdBxaValorT: TdmkEdit
          Left = 296
          Top = 59
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 145
      Width = 616
      Height = 263
      Align = alClient
      Caption = ' Dados do item a ser gerado: '
      TabOrder = 2
      object PnGera: TPanel
        Left = 2
        Top = 15
        Width = 612
        Height = 246
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label6: TLabel
          Left = 8
          Top = 4
          Width = 28
          Height = 13
          Caption = 'IME-I:'
        end
        object Label1: TLabel
          Left = 92
          Top = 4
          Width = 108
          Height = 13
          Caption = 'Artigo novo que gerou:'
        end
        object Label12: TLabel
          Left = 8
          Top = 84
          Width = 33
          Height = 13
          Caption = 'Marca:'
          Enabled = False
        end
        object Label53: TLabel
          Left = 105
          Top = 85
          Width = 58
          Height = 13
          Caption = 'Data / hora:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label9: TLabel
          Left = 256
          Top = 84
          Width = 61
          Height = 13
          Caption = 'Observa'#231#227'o:'
        end
        object Label20: TLabel
          Left = 8
          Top = 124
          Width = 29
          Height = 13
          Caption = 'Pallet:'
        end
        object LaTXRibCla: TLabel
          Left = 64
          Top = 124
          Width = 113
          Height = 13
          Caption = 'Nome do Artigo destino:'
        end
        object SbPallet1: TSpeedButton
          Left = 460
          Top = 140
          Width = 22
          Height = 22
          Caption = '...'
          OnClick = SbPallet1Click
        end
        object SBNewPallet: TSpeedButton
          Left = 484
          Top = 140
          Width = 22
          Height = 22
          Caption = '+'
          OnClick = SBNewPalletClick
        end
        object Label48: TLabel
          Left = 8
          Top = 164
          Width = 114
          Height = 13
          Caption = 'Item de pedido atrelado:'
        end
        object Label49: TLabel
          Left = 8
          Top = 204
          Width = 60
          Height = 13
          Caption = 'Localiza'#231#227'o:'
        end
        object Label50: TLabel
          Left = 506
          Top = 204
          Width = 74
          Height = 13
          Caption = 'Req.Mov.Estq.:'
          Color = clBtnFace
          ParentColor = False
        end
        object LaQtde: TLabel
          Left = 356
          Top = 44
          Width = 76
          Height = 13
          Caption = 'Quantidade[F4]:'
        end
        object Label10: TLabel
          Left = 440
          Top = 44
          Width = 76
          Height = 13
          Caption = '$ M.O. unidade:'
        end
        object Label13: TLabel
          Left = 524
          Top = 44
          Width = 62
          Height = 13
          Caption = '$ M.O. Total:'
          Enabled = False
        end
        object Label16: TLabel
          Left = 356
          Top = 84
          Width = 75
          Height = 13
          Caption = '$ Mat'#233'ria prima:'
          Enabled = False
        end
        object Label21: TLabel
          Left = 524
          Top = 84
          Width = 57
          Height = 13
          Caption = 'Custo Total:'
          Enabled = False
        end
        object Label23: TLabel
          Left = 440
          Top = 84
          Width = 72
          Height = 13
          Caption = '$ Frete retorno:'
          Enabled = False
        end
        object Label24: TLabel
          Left = 8
          Top = 44
          Width = 72
          Height = 13
          Caption = 'S'#233'rie do Tal'#227'o:'
        end
        object Label25: TLabel
          Left = 260
          Top = 44
          Width = 51
          Height = 13
          Caption = 'Tal'#227'o [F4]:'
        end
        object EdCtrl1: TdmkEdit
          Left = 8
          Top = 20
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdGraGruX: TdmkEditCB
          Left = 92
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 148
          Top = 20
          Width = 457
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsGraGruX
          TabOrder = 2
          dmkEditCB = EdGraGruX
          QryCampo = 'GraGruX'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdMarca: TdmkEdit
          Left = 8
          Top = 100
          Width = 93
          Height = 21
          CharCase = ecUpperCase
          Enabled = False
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Marca'
          UpdCampo = 'Marca'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPData: TdmkEditDateTimePicker
          Left = 105
          Top = 100
          Width = 108
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 10
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN
        end
        object EdHora: TdmkEdit
          Left = 213
          Top = 100
          Width = 40
          Height = 21
          TabOrder = 11
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryName = 'QrVSGerArt'
          QryCampo = 'DtHrAberto'
          UpdCampo = 'DtHrAberto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdObserv: TdmkEdit
          Left = 256
          Top = 100
          Width = 97
          Height = 21
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ'
          UpdCampo = 'Observ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPallet: TdmkEditCB
          Left = 8
          Top = 140
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 16
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPallet
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPallet: TdmkDBLookupComboBox
          Left = 66
          Top = 140
          Width = 393
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsTXPallet
          TabOrder = 17
          dmkEditCB = EdPallet
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CkEncerraPallet: TdmkCheckBox
          Left = 507
          Top = 144
          Width = 97
          Height = 17
          Caption = 'Encerra o Pallet.'
          TabOrder = 18
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdPedItsFin: TdmkEditCB
          Left = 8
          Top = 180
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 19
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPedItsFin
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPedItsFin: TdmkDBLookupComboBox
          Left = 66
          Top = 180
          Width = 537
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsTXPedIts
          TabOrder = 20
          dmkEditCB = EdPedItsFin
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdStqCenLoc: TdmkEditCB
          Left = 8
          Top = 220
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 21
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'StqCenLoc'
          UpdCampo = 'StqCenLoc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBStqCenLoc
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenLoc: TdmkDBLookupComboBox
          Left = 66
          Top = 220
          Width = 438
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_LOC_CEN'
          ListSource = DsStqCenLoc
          TabOrder = 22
          dmkEditCB = EdStqCenLoc
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdReqMovEstq: TdmkEdit
          Left = 507
          Top = 220
          Width = 93
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 23
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ReqMovEstq'
          UpdCampo = 'ReqMovEstq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdQtde: TdmkEdit
          Left = 356
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Qtde'
          UpdCampo = 'Qtde'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdQtdeKeyDown
          OnRedefinido = EdQtdeRedefinido
        end
        object EdCustoMOUni: TdmkEdit
          Left = 440
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          QryCampo = 'CustoMOUni'
          UpdCampo = 'CustoMOUni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdCustoMOUniRedefinido
        end
        object EdCustoMOTot: TdmkEdit
          Left = 524
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'CustoMOTot'
          UpdCampo = 'CustoMOTot'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValorMP: TdmkEdit
          Left = 356
          Top = 100
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorMP'
          UpdCampo = 'ValorMP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdValorMPRedefinido
        end
        object EdValorT: TdmkEdit
          Left = 524
          Top = 100
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 15
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCusFrtMORet: TdmkEdit
          Left = 440
          Top = 100
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdCusFrtMORetRedefinido
        end
        object EdSerieTal: TdmkEditCB
          Left = 8
          Top = 60
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'SerieTal'
          UpdCampo = 'SerieTal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBSerieTal
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnFormActivate
        end
        object CBSerieTal: TdmkDBLookupComboBox
          Left = 48
          Top = 60
          Width = 209
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTXSerTal
          TabOrder = 4
          dmkEditCB = EdSerieTal
          QryCampo = 'SerieTal'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdTalao: TdmkEdit
          Left = 260
          Top = 60
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Talao'
          UpdCampo = 'Talao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdTalaoKeyDown
        end
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 408
      Width = 616
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 133
        Height = 41
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object CkContinuar: TCheckBox
          Left = 12
          Top = 12
          Width = 113
          Height = 17
          Caption = 'Continuar inserindo.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
      end
      object GBAvisos1: TGroupBox
        Left = 133
        Top = 0
        Width = 483
        Height = 41
        Align = alClient
        Caption = ' Avisos: '
        TabOrder = 1
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 479
          Height = 24
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 12
            Height = 17
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 12
            Height = 17
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
    object RGBaixaEOuGera: TRadioGroup
      Left = 0
      Top = 0
      Width = 616
      Height = 45
      Align = alTop
      Caption = ' A'#231#245'es: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'Baixa e gera'
        'Gera sem baixa')
      TabOrder = 0
      OnClick = RGBaixaEOuGeraClick
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 596
    Top = 24
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 596
    Top = 68
  end
  object QrTXPallet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 536
    Top = 24
    object QrTXPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrTXPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrTXPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrTXPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrTXPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
  end
  object DsTXPallet: TDataSource
    DataSet = QrTXPallet
    Left = 536
    Top = 68
  end
  object QrTXPedIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 720
    Top = 25
    object QrTXPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrTXPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsTXPedIts: TDataSource
    DataSet = QrTXPedIts
    Left = 720
    Top = 69
  end
  object QrStqCenLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 468
    Top = 19
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 468
    Top = 67
  end
  object PMPallet: TPopupMenu
    Left = 568
    Top = 440
    object Criar1: TMenuItem
      Caption = '&Criar Novo'
      OnClick = Criar1Click
    end
    object Gerenciamento1: TMenuItem
      Caption = '&Gerenciamento'
      OnClick = Gerenciamento1Click
    end
  end
  object QrTXIndInn: TmySQLQuery
    Database = Dmod.MyDB
    Left = 660
    Top = 25
    object QrTXIndInnCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXIndInnCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTXIndInnCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXIndInnControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXIndInnMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXIndInnMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXIndInnMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXIndInnEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXIndInnTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXIndInnCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXIndInnMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXIndInnDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXIndInnPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXIndInnGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXIndInnQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndInnValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXIndInnSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXIndInnSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXIndInnSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXIndInnSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXIndInnObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXIndInnFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXIndInnCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXIndInnDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXIndInnDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXIndInnDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXIndInnDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXIndInnQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXIndInnQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXIndInnNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXIndInnNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXIndInnNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXIndInnID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXIndInnNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXIndInnReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXIndInnCUSTO_M2: TFloatField
      FieldName = 'CUSTO_UNI'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrTXIndInnNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrTXIndInnMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXIndInnPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrTXIndInnStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXIndInnClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrTXIndInnNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrTXIndInnCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrTXIndInnDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrTXIndInnPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrTXIndInnSerieTal: TLargeintField
      FieldName = 'SerieTal'
    end
    object QrTXIndInnTalao: TLargeintField
      FieldName = 'Talao'
    end
  end
  object DsTXIndInn: TDataSource
    DataSet = QrTXIndInn
    Left = 660
    Top = 68
  end
  object DqPsq: TmySQLDirectQuery
    Database = DModG.MyCompressDB
    Left = 648
    Top = 200
  end
  object QrTXSerTal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM txsertal'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 716
    Top = 120
    object QrTXSerTalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXSerTalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXSerTal: TDataSource
    DataSet = QrTXSerTal
    Left = 716
    Top = 168
  end
end
