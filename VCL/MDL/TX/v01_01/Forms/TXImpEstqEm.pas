unit TXImpEstqEm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  frxClass, frxDBSet, Data.DB, mySQLDbTables,
  dmkGeral, DmkEditCB, dmkDBGridZTO, UnDmkProcFunc, AppListas, UnInternalConsts,
  UnProjGroup_Consts, Vcl.Grids, Vcl.DBGrids, UnDmkEnums, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.ExtCtrls, UnAppEnums;

type
  TFmTXImpEstqEm = class(TForm)
    QrInd: TmySQLQuery;
    QrIndQtde: TFloatField;
    QrIndMID: TIntegerField;
    QrIndCod: TIntegerField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTmiEstqEmTmi, FTmiEstqEmAtzA, FTmiEstqEmAtzB, FTmiEstqEmS01: String;
    //
    procedure Atualiza_Ope_e_PWE();
    procedure AtualizaZerados();
    procedure GeraItensEm();
    procedure InsereTMIsDeTabela(TabelaSrc: String);
  public
    { Public declarations }
    FEntidade, FFilial, Ed00Terceiro_ValueVariant, RG00_Ordem1_ItemIndex,
    RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex,
    RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex, Ed00StqCenCad_ValueVariant,
    RG00ZeroNegat_ItemIndex, FGraCusPrc, FEd00NFeIni_ValueVariant,
    FEd00NFeFim_ValueVariant, FEd00Serie_ValueVariant, FMovimCod: Integer;
    FNO_EMPRESA, CB00StqCenCad_Text, CB00Terceiro_Text: String;
    TPDataRelativa_Date: TDateTime;
    Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked, FMostraFrx: Boolean;
    //EdEmpresa: TDmkEditCB;
    DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
    Ck00DescrAgruNoItm_Checked, FCk00Serie_Checked: Boolean;
    //
    FLaAviso1, FLaAviso2: TLabel;
    //
    FDataEm: TDateTime;
    //
    function  ImprimeEstoqueEm(): Boolean;
  end;

var
  FmTXImpEstqEm: TFmTXImpEstqEm;

implementation

{$R *.dfm}

uses ModuleGeral, CreateTX, UnMyObjects, DmkDAC_PF, UMySQLModule, Module,
  MyDBCheck, UnTX_PF;

{ TFmTXImpEstqEm }

procedure TFmTXImpEstqEm.AtualizaZerados();
const
  Zerado = 1;
  SdoVrtQtd = 0;
begin
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True,
    'Gerando saldo zerado de zerados for�ados pelo usu�rio');
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FTmiEstqEmTmi, False, [
  'SdoVrtQtd'], ['Zerado'], [SdoVrtQtd], [
  Zerado], False);
end;

procedure TFmTXImpEstqEm.Atualiza_Ope_e_PWE();
const
  TxtPre = 'Gerando saldos de OOs e OPs. Item ';
var
  //MovimCod,
  Codigo, MovimID, MovimNiv: Integer;
  Qtde: Double;
  TxtNum, TxtPos: String;
begin
  FTmiEstqEmAtzA := '_vmi_estq_em_atz_a';
  FTmiEstqEmAtzB := '_vmi_estq_em_atz_b';
  UnDMkDAC_PF.AbreMySQLQuery0(QrInd, DModG.MyPID_DB, [
////////////////////////////////////////////////////////////////////////////////
  'DROP TABLE IF EXISTS ' + FTmiEstqEmAtzA + ';  ',
  'CREATE TABLE ' + FTmiEstqEmAtzA + ' ',
  'SELECT COUNT(IF(SdoVrtQtd>0, 1, 0)) Itens, ',
  'IF(MovimID IN (6,11,19,26,27,32,33,38), IF(MovimNiv IN (15,34,37), SrcMovID, MovimID),  ',
  '  IF(SrcMovID=20 AND MovimID=28, 19, IF(MovimID=29 AND SrcMovID=0, 26, SrcMovID))) MID, ',
  'IF(MovimID IN (6,11,19,26,27,29,32,33,38), IF(MovimNiv IN (15,34,37), SrcNivel1, Codigo), SrcNivel1) Cod,  ',
  //  Fim 2017-11-08
  'SUM(IF(MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_GET + '), -1, 1) * Qtde) Qtde,  ',
  'SUM(IF(MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_GET + '), -1, 1) * ValorT) ValorT  ',
  'FROM ' + FTmiEstqEmTmi,
  'WHERE ( ',
  '  MovimID IN (' + CO_CODS_OPER_PROC_ID_GET + ')  ', // 2017-11-08 adicionado 29
  '  AND MovimNiv IN (' + CO_CODS_OPER_PROC_NIV_FRO + ')  ',
  ') OR ( ',
  '    MovimID IN (' + CO_ALL_CODS_BXA_POSIT_TX + ')  ',
  '    AND SrcNivel2 IN ',
  '    ( ',
  '      SELECT Controle ',
  '      FROM ' + FTmiEstqEmTmi + ' ',
  '      WHERE MovimID IN (' + CO_CODS_OPER_PROC_ID_GET + ') ',
  '    ) ',
  ') OR (',
  '  MovimID=6 AND SrcMovID=27 ',
  ') ',
  'GROUP BY MID, Cod ',
  ';  ',
  'DROP TABLE IF EXISTS ' + FTmiEstqEmAtzB + ';  ',
  'CREATE TABLE ' + FTmiEstqEmAtzB + ' ',
  'SELECT Itens, MID, Cod, SUM(Qtde) Qtde  ',
  'FROM ' + FTmiEstqEmAtzA + ' ',
  'GROUP BY MID, Cod ',
  '; ',
  //
  'UPDATE ' + FTmiEstqEmTmi,
  'SET SdoVrtQtd=0 ', //, SdoVrtArP2=0 ',
  'WHERE MovimID IN (26,27) AND MovimNiv IN (30,35);',
  //
  'SELECT * FROM ' + FTmiEstqEmAtzB + ' ',
  'WHERE Qtde>0  ',
  '']);
  //Geral.MB_SQL(Self, QrInd);
  //
  TxtPos := Geral.FF0(QrInd.RecordCount);
  //
  QrInd.First;
  while not QrInd.Eof do
  begin
    TxtNum := Geral.FF0(QrInd.RecNo);
    MyObjects.Informa2(FLaAviso1, FLaAviso2, True, TxtPre + TxtNum + TxtPos);
    //
    Codigo  := QrIndCod.Value;
    MovimID := QrIndMID.Value;
    Qtde    := 0;
    //
    case TEstqMovimID(QrIndMID.Value) of
{
      emidCompra:     MovimNiv := Integer(TEstqMovimNiv.eminEmCalInn);  // 30
      emidEmOperacao: MovimNiv := Integer(TEstqMovimNiv.eminEmOperInn); //  8
      emidEmProcWE:   MovimNiv := Integer(TEstqMovimNiv.eminEmWEndInn); // 21
      emidEmProcCal:  MovimNiv := Integer(TEstqMovimNiv.eminEmCalInn);  // 30
      emidEmProcCur:  MovimNiv := Integer(TEstqMovimNiv.eminEmCurInn);  // 35
      emidEmProcSP:   MovimNiv := Integer(TEstqMovimNiv.eminEmPSPInn);  // 50
      emidEmReprRM:   MovimNiv := Integer(TEstqMovimNiv.eminEmRRMInn);  // 55
}
      emidEmIndstrlzc: MovimNiv := Integer(TEstqMovimNiv.eminIndzcInn);  // 62
      else
      begin
        MovimNiv := -1;
        Geral.MB_Erro(
        '"MovimID" n�o implementado em "FmTXImpEstqEm.Atualiza_Ope_e_PWE()"' +
        slineBreak + 'MovimID: ' + Geral.FF0(QrIndMID.Value));
      end;
    end;
    //
    if QrIndQtde.Value >= 0 then
    begin
      Qtde  := QrIndQtde.Value;
    end else
{
    if (TEstqMovimID(QrIndMID.Value) = emidEmProcSP(*32*))
    and (QrIndP e s o K g.Value >= 0) then
}
    begin
      //Qtde  := QrIndQtde.Value;
    end;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FTmiEstqEmTmi, False, [
    'SdoVrtQtd'], ['Codigo', 'MovimID', 'MovimNiv'], [
    Qtde], [Codigo, MovimID, MovimNiv], False);
    //
    QrInd.Next;
  end;
end;

procedure TFmTXImpEstqEm.FormCreate(Sender: TObject);
begin
  FMostraFrx := True;
end;

procedure TFmTXImpEstqEm.GeraItensEm();
var
  ID_e_Niv: String;
begin
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo');
  FTmiEstqEmTmi := UnCreateTX.RecriaTempTableNovo(ntrttTmiEstqEmTmi,
    DModG.QrUpdPID1, False);
  FTmiEstqEmS01 := '_tmi_estq_em_s01_';
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo - Morto');
  InsereTMIsDeTabela(CO_TAB_TMB);
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Copiando dados de IMEIS do per�odo - Ativo');
  InsereTMIsDeTabela(CO_SEL_TAB_TMI);
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de In Natura');
  // MovimID=1 (Entrada ID=1Niv=0 e baixa por ID6Niv14 + IDs9Niv0 e ID17Niv0
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + FTmiEstqEmTmi,
  'SET SdoVrtQtd=Qtde ',
  'WHERE MovimID=1;',
  '',
  'DROP TABLE IF EXISTS ' + FTmiEstqEmS01 + '; ',
  'CREATE TABLE ' + FTmiEstqEmS01 + ' ',
  'SELECT SrcNivel2,  ',
  'SUM(vmi.Qtde) Qtde  ',
  'FROM ' + FTmiEstqEmTmi + ' vmi  ',
  'WHERE vmi.SrcNivel2 IN ( ',
  '  SELECT Controle ',
  '  FROM ' + FTmiEstqEmTmi + ' ',
  '  WHERE MovimID=1 ',
  ') ',
  'AND (vmi.Qtde < 0  ',
  'OR (vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_TX + '))  ',
  ') ',
  'GROUP BY SrcNivel2 ',
  '; ',
  'UPDATE ' + FTmiEstqEmTmi + ' vmi  ',
  'LEFT JOIN ' + FTmiEstqEmS01 + ' s01 ON vmi.Controle = s01.SrcNivel2 ',
  'SET  ',
  '  vmi.SdoVrtQtd = vmi.Qtde  + s01.Qtde ',
  'WHERE vmi.MovimID=1 ',
  'AND NOT s01.Qtde IS NULL ',
  '; ',
  '']);
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de movimentos');
  ID_e_Niv := TX_PF.SQL_MovIDeNiv_Pos_All();
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + FTmiEstqEmTmi,
  'SET SdoVrtQtd=Qtde ',
  'WHERE (' + ID_e_Niv + ');',
  '',
  'DROP TABLE IF EXISTS ' + FTmiEstqEmS01 + '; ',
  'CREATE TABLE ' + FTmiEstqEmS01 + ' ',
  'SELECT vmi.SrcNivel2, SUM(vmi.Qtde) Qtde,  ',
  'SUM(vmi.QtdGer) QtdGer ',
  'FROM ' + FTmiEstqEmTmi + ' vmi  ',
  'WHERE vmi.SrcNivel2 IN (  ',
  '  SELECT Controle  ',
  '  FROM ' + FTmiEstqEmTmi + '  ',
  '  WHERE (' + ID_e_Niv + ') ',
  ')  ',
  'AND (vmi.Qtde < 0  ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_TX + ')  ',
  ')  ',
  'GROUP BY SrcNivel2  ',
  '; ',
  'UPDATE ' + FTmiEstqEmTmi + ' vmi  ',
  'LEFT JOIN ' + FTmiEstqEmS01 + ' s01 ON vmi.Controle = s01.SrcNivel2 ',
  'SET  ',
  '  vmi.SdoVrtQtd = vmi.Qtde  + s01.Qtde ',
  'WHERE ((' + ID_e_Niv + '))',
  'AND NOT s01.Qtde IS NULL ',
  '; ',
  '']);
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  //
end;

function TFmTXImpEstqEm.ImprimeEstoqueEm(): Boolean;
var
  DataRetroativa: String;
begin
  Result := False;
  GeraItensEm();  // _vmi_estq_em_s01_
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando saldos de OOs e OPs.');
  Atualiza_Ope_e_PWE();
  AtualizaZerados();
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Gerando relat�rio de "estoque em"');
  DataRetroativa := Geral.FDT(FDataEm, 2);
  Result := TX_PF.ImprimeEstoqueReal(FEntidade, FFilial, Ed00Terceiro_ValueVariant,
  RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
  RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex,
  Ck00DescrAgruNoItm_Checked, Ed00StqCenCad_ValueVariant,
  RG00ZeroNegat_ItemIndex, FNO_EMPRESA, CB00StqCenCad_Text, CB00Terceiro_Text,
  TPDataRelativa_Date, Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked,
  DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2, Qr00GraGruY, Qr00GraGruX,
  Qr00CouNiv2, FTmiEstqEmTmi, DataRetroativa, FGraCusPrc, FDataEm,
  FEd00NFeIni_ValueVariant, FEd00NFeFim_ValueVariant,
  FCk00Serie_Checked, FEd00Serie_ValueVariant, FMovimCod,
  FMostraFrx);
  //
  MyObjects.Informa2(FLaAviso1, FLaAviso2, False, '...');
end;

procedure TFmTXImpEstqEm.InsereTMIsDeTabela(TabelaSrc: String);
begin
  UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FTmiEstqEmTmi,
  'SELECT  ',
  'Codigo, Controle, ',
  'MovimCod, MovimNiv,  ',
  'MovimTwn, Empresa, Terceiro,  ',
  'CliVenda, MovimID, DataHora,  ',
  'Pallet, GraGruX, Qtde,  ',
  'ValorT, SrcMovID, SrcNivel1,  ',
  'SrcNivel2, SrcGGX, 0 SdoVrtQtd, Observ,  ',
  'FornecMO, CustoMOUni, CustoMOTot,  ',
  'ValorMP, DstMovID, DstNivel1,  ',
  'DstNivel2, DstGGX, QtdGer,  ',
  'QtdAnt, AptoUso, ',
  'Marca, TpCalcAuto, Zerado,  ',
  //'EmFluxo, NotFluxo, FatNotaVNC,  ',
  //'FatNotaVRC,
  'PedItsLib, PedItsFin, PedItsVda, ',
  //'GSPSrcMovID, GSPSrcNiv2,   ',
  'ReqMovEstq, StqCenLoc,  ',
  'ItemNFe, TXMorCab, TXMulFrnCab, ',
  'ClientMO, ',
  'NFeSer, NFeNum, TXMulNFeCab, ',
  'SerieTal, Talao, ',
  'Ativo ',
  'FROM ' + TMeuDB + '.' + TabelaSrc,
  ' ',
  'WHERE DataHora<"' + Geral.FDT(Int(FDataEm + 1), 1) + '" ',
  '; ',
  '']);
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
end;

end.
