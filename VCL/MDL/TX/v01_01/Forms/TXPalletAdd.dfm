object FmTXPalletAdd: TFmTXPalletAdd
  Left = 339
  Top = 185
  Caption = 'TEX-FAXAO-014 :: Cria'#231#227'o de Pallet de Artigo'
  ClientHeight = 337
  ClientWidth = 730
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 730
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 682
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 634
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 323
        Height = 32
        Caption = 'Cria'#231#227'o de Pallet de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 323
        Height = 32
        Caption = 'Cria'#231#227'o de Pallet de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 323
        Height = 32
        Caption = 'Cria'#231#227'o de Pallet de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 730
    Height = 175
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 730
      Height = 175
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBEdita: TGroupBox
        Left = 0
        Top = 0
        Width = 730
        Height = 175
        Align = alClient
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 16
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label9: TLabel
          Left = 76
          Top = 16
          Width = 120
          Height = 13
          Caption = 'Descri'#231#227'o / Observa'#231#227'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label8: TLabel
          Left = 16
          Top = 56
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label5: TLabel
          Left = 520
          Top = 96
          Width = 33
          Height = 13
          Caption = 'Status:'
        end
        object SBCliente: TSpeedButton
          Left = 496
          Top = 112
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBClienteClick
        end
        object LaVSRibCla: TLabel
          Left = 312
          Top = 56
          Width = 69
          Height = 13
          Caption = 'Artigo (Nivel1):'
        end
        object Label4: TLabel
          Left = 16
          Top = 96
          Width = 93
          Height = 13
          Caption = 'Cliente preferencial:'
        end
        object Label1: TLabel
          Left = 672
          Top = 56
          Width = 28
          Height = 13
          Caption = 'PPP*:'
        end
        object Label20: TLabel
          Left = 16
          Top = 136
          Width = 83
          Height = 13
          Caption = 'Dono do produto:'
        end
        object EdCodigo: TdmkEdit
          Left = 16
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 76
          Top = 32
          Width = 641
          Height = 21
          MaxLength = 60
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 16
          Top = 72
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Empresa'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdEmpresaRedefinido
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 76
          Top = 72
          Width = 233
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 3
          dmkEditCB = EdEmpresa
          QryCampo = 'Empresa'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCliStat: TdmkEditCB
          Left = 16
          Top = 112
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CliStat'
          UpdCampo = 'CliStat'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliStat
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliStat: TdmkDBLookupComboBox
          Left = 72
          Top = 112
          Width = 424
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsClientes
          TabOrder = 8
          dmkEditCB = EdCliStat
          QryCampo = 'CliStat'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdStatus: TdmkEditCB
          Left = 520
          Top = 112
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Status'
          UpdCampo = 'Status'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBStatus
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStatus: TdmkDBLookupComboBox
          Left = 576
          Top = 112
          Width = 141
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTXPalSta
          TabOrder = 10
          dmkEditCB = EdStatus
          QryCampo = 'Status'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdGraGruX: TdmkEditCB
          Left = 312
          Top = 72
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdGraGruXRedefinido
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 371
          Top = 72
          Width = 298
          Height = 21
          KeyField = 'GraGruX'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsVSRibCla
          TabOrder = 5
          dmkEditCB = EdGraGruX
          QryCampo = 'GraGruX'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdQtdPrevPc: TdmkEdit
          Left = 672
          Top = 72
          Width = 45
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '110'
          QryCampo = 'QtdPrevPc'
          UpdCampo = 'QtdPrevPc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 110
          ValWarn = False
        end
        object EdClientMO: TdmkEditCB
          Left = 16
          Top = 152
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ClienteMO'
          UpdCampo = 'ClienteMO'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBClientMO
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBClientMO: TdmkDBLookupComboBox
          Left = 73
          Top = 152
          Width = 644
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsClientMO
          TabOrder = 12
          dmkEditCB = EdClientMO
          QryCampo = 'ClienteMO'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 223
    Width = 730
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 726
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 203
        Height = 16
        Caption = 'PPP*: Previs'#227'o de pe'#231'as no pallet.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 203
        Height = 16
        Caption = 'PPP*: Previs'#227'o de pe'#231'as no pallet.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 267
    Width = 730
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 584
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 582
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NO_ENT')
    Left = 448
    Top = 73
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 448
    Top = 117
  end
  object QrTXPalSta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vspalsta'
      'ORDER BY Nome')
    Left = 516
    Top = 72
    object QrTXPalStaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPalStaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTXPalSta: TDataSource
    DataSet = QrTXPalSta
    Left = 516
    Top = 116
  end
  object QrVSRibCla: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmp.GraGruX, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, cou.PrevPcPal'
      'FROM vsribcla wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle'
      'WHERE ggx.GragruY IN (1536,2048)'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 576
    Top = 72
    object QrVSRibClaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSRibClaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSRibClaPrevPcPal: TIntegerField
      FieldName = 'PrevPcPal'
    end
  end
  object DsVSRibCla: TDataSource
    DataSet = QrVSRibCla
    Left = 576
    Top = 120
  end
  object QrClientMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 180
    Top = 76
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 180
    Top = 124
  end
end
