object FmTXBxaCab: TFmTXBxaCab
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-027 :: Baixa For'#231'ada'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 97
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 676
        Top = 16
        Width = 86
        Height = 13
        Caption = 'Data / hora baixa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 78
        Height = 13
        Caption = 'Motivo da baixa:'
      end
      object Label16: TLabel
        Left = 828
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXBxaCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXBxaCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 541
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXBxaCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 676
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtBaixa'
        DataSource = DsTXBxaCab
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsTXBxaCab
        TabOrder = 4
      end
      object DBEdit11: TDBEdit
        Left = 828
        Top = 36
        Width = 100
        Height = 21
        DataField = 'Qtde'
        DataSource = DsTXBxaCab
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 72
        Width = 980
        Height = 21
        DataField = 'Nome'
        DataSource = DsTXBxaCab
        TabOrder = 6
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 97
      Width = 1008
      Height = 172
      Align = alTop
      Caption = ' Itens da sa'#237'da: '
      TabOrder = 2
      object DGDados: TDBGrid
        Left = 2
        Top = 15
        Width = 1004
        Height = 155
        Align = alClient
        DataSource = DsTXBxaIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Arquivo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'EME-I'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Title.Caption = 'ID Pallet'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Pallet'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
            Width = 292
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Pe'#231'as'
            Width = 72
            Visible = True
          end>
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 676
        Top = 16
        Width = 86
        Height = 13
        Caption = 'Data / hora baixa:'
        Color = clBtnFace
        ParentColor = False
      end
      object LaQtde: TLabel
        Left = 828
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 117
        Height = 13
        Caption = 'Motivo da baixa for'#231'ada:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtBaixa: TdmkEditDateTimePicker
        Left = 676
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtBaixa'
        UpdCampo = 'DtBaixa'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtBaixa: TdmkEdit
        Left = 784
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtBaixa'
        UpdCampo = 'DtBaixa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 828
        Top = 32
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 537
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 973
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 174
        Height = 32
        Caption = 'Baixa For'#231'ada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 174
        Height = 32
        Caption = 'Baixa For'#231'ada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 174
        Height = 32
        Caption = 'Baixa For'#231'ada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 600
    Top = 44
  end
  object QrTXBxaCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXBxaCabBeforeOpen
    AfterOpen = QrTXBxaCabAfterOpen
    BeforeClose = QrTXBxaCabBeforeClose
    AfterScroll = QrTXBxaCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.transporta')
    Left = 684
    Top = 1
    object QrTXBxaCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXBxaCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrTXBxaCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrTXBxaCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXBxaCabDtBaixa: TDateTimeField
      FieldName = 'DtBaixa'
    end
    object QrTXBxaCabQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrTXBxaCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXBxaCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXBxaCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXBxaCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXBxaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXBxaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXBxaCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXBxaCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXBxaCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
  end
  object DsTXBxaCab: TDataSource
    DataSet = QrTXBxaCab
    Left = 688
    Top = 45
  end
  object QrTXBxaIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 768
    Top = 1
    object QrTXBxaItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXBxaItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXBxaItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXBxaItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXBxaItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXBxaItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXBxaItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXBxaItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXBxaItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXBxaItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXBxaItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXBxaItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXBxaItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXBxaItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXBxaItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXBxaItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXBxaItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXBxaItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXBxaItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXBxaItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXBxaItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXBxaItsCustoMOKg: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXBxaItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXBxaItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXBxaItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXBxaItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXBxaItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXBxaItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXBxaItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXBxaItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXBxaItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXBxaItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXBxaItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXBxaItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsTXBxaIts: TDataSource
    DataSet = QrTXBxaIts
    Left = 772
    Top = 45
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 656
    Top = 580
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AdicionaIMEIscompletos1: TMenuItem
      Caption = 'Adiciona IME-Is completos'
      OnClick = AdicionaIMEIscompletos1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrOrigem: TmySQLQuery
    Database = Dmod.MyDB
    Left = 300
    Top = 64
    object QrOrigemNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrOrigemNO_CEN_LOC: TWideStringField
      FieldName = 'NO_CEN_LOC'
      Size = 103
    end
    object QrOrigemNO_FMO: TWideStringField
      FieldName = 'NO_FMO'
      Size = 100
    end
    object QrOrigemNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrOrigemNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrOrigemGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrOrigemQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrOrigemValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrOrigemStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrOrigemDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object frxDsOrigem: TfrxDBDataset
    UserName = 'frxDsOrigem'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_MovimID=NO_MovimID'
      'NO_CEN_LOC=NO_CEN_LOC'
      'NO_FMO=NO_FMO'
      'NO_CLI=NO_CLI'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'ValorT=ValorT'
      'StqCenLoc=StqCenLoc'
      'DataHora=DataHora')
    DataSet = QrOrigem
    BCDToCurrency = False
    Left = 300
    Top = 112
  end
  object frxWET_CURTI_027_1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_027_1GetValue
    Left = 300
    Top = 160
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsOrigem
        DataSetName = 'frxDsOrigem'
      end
      item
        DataSet = frxDsTXBxaCab
        DataSetName = 'frxDsTXBxaCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929180240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 952.441560000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 740.787880000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'BAIXA FOR'#199'ADA N'#176' [frxDsVSBxaCab."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsVSBxaCab."DtBaixa"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 854.173780000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 480.000310000000000000
          Top = 71.811070000000000000
          Width = 355.275722360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 442.205010000000000000
          Top = 71.811070000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitQtde: TfrxMemoView
          Left = 903.307670000000000000
          Top = 71.811070000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 835.276130000000000000
          Top = 71.811070000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ custo total')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Top = 71.811070000000000000
          Width = 151.181102362204700000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor de m'#227'o-de-obra')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 151.181200000000000000
          Top = 71.811070000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente da m'#227'o-de-obra')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Top = 37.795300000000000000
          Width = 971.339210000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSBxaCab."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 400.630180000000000000
          Top = 71.811070000000000000
          Width = 41.574732360000000000
          Height = 15.118110240000000000
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 302.362400000000000000
          Top = 71.811070000000000000
          Width = 98.267682360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 211.653680000000000000
        Width = 971.339210000000000000
        DataSet = frxDsOrigem
        DataSetName = 'frxDsOrigem'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 480.000310000000000000
          Width = 355.275722360000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOrigem."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 442.205010000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOrigem."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValQtde: TfrxMemoView
          Left = 903.307670000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Qtde'
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          DisplayFormat.FormatStr = '#,###,##0.0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOrigem."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 835.276130000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOrigem."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Width = 151.181102362204700000
          Height = 15.118110240000000000
          DataField = 'NO_FMO'
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOrigem."NO_FMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 151.181200000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          DataField = 'NO_CLI'
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOrigem."NO_CLI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 400.630180000000000000
          Width = 41.574732360000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOrigem."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 302.362400000000000000
          Width = 98.267682360000000000
          Height = 15.118110240000000000
          DataField = 'NO_MovimID'
          DataSet = frxDsOrigem
          DataSetName = 'frxDsOrigem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOrigem."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 385.512060000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 646.299630000000000000
          Width = 325.039580000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 377.952936540000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 332.598640000000000000
        Width = 971.339210000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118119999999980000
          Width = 835.275934720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 903.307670000000000000
          Top = 15.118119999999980000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,##0.0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOrigem."Qtde">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 835.276130000000000000
          Top = 15.118119999999980000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOrigem."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 22.677170240000000000
        ParentFont = False
        Top = 166.299320000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsOrigem."StqCenLoc"'
        object Me_GH0: TfrxMemoView
          Top = 3.779529999999994000
          Width = 971.338795040000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsOrigem."NO_CEN_LOC"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 20.787406460000000000
        Top = 249.448980000000000000
        Width = 971.339210000000000000
        object Memo3: TfrxMemoView
          Left = 903.307670000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,##0.0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOrigem."Qtde">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT0: TfrxMemoView
          Width = 835.275910310000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOrigem."NO_CEN_LOC"]: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 835.276130000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOrigem."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsTXBxaCab: TfrxDBDataset
    UserName = 'frxDsTXBxaCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DtBaixa=DtBaixa'
      'Qtde=Qtde'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA'
      'TemIMEIMrt=TemIMEIMrt')
    DataSet = QrTXBxaCab
    BCDToCurrency = False
    Left = 688
    Top = 92
  end
end
