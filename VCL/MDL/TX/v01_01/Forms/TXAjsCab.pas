unit TXAjsCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, dmkDBGridZTO,
  UnProjGroup_Consts, UnGrl_Consts, UnAppEnums;

type
  TFmTXAjsCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrTXAjsCab: TmySQLQuery;
    DsTXAjsCab: TDataSource;
    QrTXAjsIts: TmySQLQuery;
    DsTXAjsIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    LaQtde: TLabel;
    EdQtde: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrTXAjsCabCodigo: TIntegerField;
    QrTXAjsCabMovimCod: TIntegerField;
    QrTXAjsCabEmpresa: TIntegerField;
    QrTXAjsCabDataHora: TDateTimeField;
    QrTXAjsCabQtde: TFloatField;
    QrTXAjsCabLk: TIntegerField;
    QrTXAjsCabDataCad: TDateField;
    QrTXAjsCabDataAlt: TDateField;
    QrTXAjsCabUserCad: TIntegerField;
    QrTXAjsCabUserAlt: TIntegerField;
    QrTXAjsCabAlterWeb: TSmallintField;
    QrTXAjsCabAtivo: TSmallintField;
    QrTXAjsCabNO_EMPRESA: TWideStringField;
    QrTXAjsItsQtde: TFloatField;
    QrTXAjsItsNO_PRD_TAM_COR: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TdmkDBGridZTO;
    QrTXAjsItsNO_PALLET: TWideStringField;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    Classificao1: TMenuItem;
    frxTEX_FAXAO_010_01: TfrxReport;
    frxDsTXAjsIts: TfrxDBDataset;
    frxDsTXAjsCab: TfrxDBDataset;
    QrTXAjsItsObserv: TWideStringField;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    QrPalletCla: TmySQLQuery;
    QrPalletClaPallet: TIntegerField;
    QrPalletClaDataHora: TDateTimeField;
    QrPalletClaQtde: TFloatField;
    QrPalletClaNO_PRD_TAM_COR: TWideStringField;
    QrPalletClaNO_Pallet: TWideStringField;
    QrPalletClaNO_EMPRESA: TWideStringField;
    QrPalletClaNO_FORNECE: TWideStringField;
    frxTEX_FAXAO_010_03: TfrxReport;
    frxDsPalletCla: TfrxDBDataset;
    QrTXAjsItsNO_FORNECE: TWideStringField;
    QrTXAjsItsDataHora: TDateTimeField;
    QrTXAjsItsValorT: TFloatField;
    QrTXAjsItsSdoVrtQtd: TFloatField;
    QrTXAjsItsCustoMOUni: TFloatField;
    QrTXAjsItsCustoMOTot: TFloatField;
    QrTXAjsItsValorMP: TFloatField;
    QrTXAjsItsQtdGer: TFloatField;
    QrTXAjsItsQtdAnt: TFloatField;
    QrTXAjsItsCUS_Qtde: TFloatField;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrTXAjsCabTemIMEIMrt: TSmallintField;
    QrTXAjsItsFornecMO: TLargeintField;
    QrTXAjsItsNO_TTW: TWideStringField;
    QrTXAjsItsCodigo: TLargeintField;
    QrTXAjsItsControle: TLargeintField;
    QrTXAjsItsMovimCod: TLargeintField;
    QrTXAjsItsMovimNiv: TLargeintField;
    QrTXAjsItsMovimTwn: TLargeintField;
    QrTXAjsItsEmpresa: TLargeintField;
    QrTXAjsItsTerceiro: TLargeintField;
    QrTXAjsItsCliVenda: TLargeintField;
    QrTXAjsItsMovimID: TLargeintField;
    QrTXAjsItsPallet: TLargeintField;
    QrTXAjsItsGraGruX: TLargeintField;
    QrTXAjsItsSrcMovID: TLargeintField;
    QrTXAjsItsSrcNivel1: TLargeintField;
    QrTXAjsItsSrcNivel2: TLargeintField;
    QrTXAjsItsSrcGGX: TLargeintField;
    QrTXAjsItsSerieTal: TLargeintField;
    QrTXAjsItsTalao: TLargeintField;
    QrTXAjsItsDstMovID: TLargeintField;
    QrTXAjsItsDstNivel1: TLargeintField;
    QrTXAjsItsDstNivel2: TLargeintField;
    QrTXAjsItsDstGGX: TLargeintField;
    QrTXAjsItsID_TTW: TLargeintField;
    QrTXAjsItsStqCenLoc: TLargeintField;
    QrTXAjsItsReqMovEstq: TLargeintField;
    QrTXAjsCabClienteMO: TIntegerField;
    Label20: TLabel;
    EdClienteMO: TdmkEditCB;
    CBClienteMO: TdmkDBLookupComboBox;
    DBEdit4: TDBEdit;
    Label3: TLabel;
    DBEdit5: TDBEdit;
    QrTXAjsCabNO_CLIENTEMO: TWideStringField;
    QrClienteMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    QrTXAjsItsIxxMovIX: TLargeintField;
    QrTXAjsItsIxxFolha: TLargeintField;
    QrTXAjsItsIxxLinha: TLargeintField;
    QrTXAjsItsMarca: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTXAjsCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTXAjsCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTXAjsCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTXAjsCabBeforeClose(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure Classificao1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxTEX_FAXAO_010_01GetValue(const VarName: string;
      var Value: Variant);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormTXAjsIts(SQLType: TSQLType);
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTXAjsIts(Controle: Integer);

  end;

var
  FmTXAjsCab: TFmTXAjsCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, TXAjsIts, ModuleGeral,
  Principal, UnTX_PF, CreateTX, UnTX_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTXAjsCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTXAjsCab.MostraFormTXAjsIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTXAjsIts, FmTXAjsIts, afmoNegarComAviso) then
  begin
    FmTXAjsIts.ImgTipo.SQLType := SQLType;
    FmTXAjsIts.FQrCab := QrTXAjsCab;
    FmTXAjsIts.FDsCab := DsTXAjsCab;
    FmTXAjsIts.FQrIts := QrTXAjsIts;
    FmTXAjsIts.FDataHora := QrTXAjsCabDataHora.Value;
    FmTXAjsIts.FEmpresa  := QrTXAjsCabEmpresa.Value;
    FmTXAjsIts.FClientMO := QrTXAjsCabClienteMO.Value;
    if SQLType = stIns then
    begin
      //FmTXAjsIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmTXAjsIts.EdControle.ValueVariant := QrTXAjsItsControle.Value;
      //
      FmTXAjsIts.EdGragruX.ValueVariant    := QrTXAjsItsGraGruX.Value;
      FmTXAjsIts.CBGragruX.KeyValue        := QrTXAjsItsGraGruX.Value;
      FmTXAjsIts.EdFornecedor.ValueVariant := QrTXAjsItsTerceiro.Value;
      FmTXAjsIts.CBFornecedor.KeyValue     := QrTXAjsItsTerceiro.Value;
      FmTXAjsIts.EdSerieTal.ValueVariant   := QrTXAjsItsSerieTal.Value;
      FmTXAjsIts.CBSerieTal.KeyValue       := QrTXAjsItsSerieTal.Value;
      FmTXAjsIts.EdTalao.ValueVariant      := QrTXAjsItsTalao.Value;
      FmTXAjsIts.EdPallet.ValueVariant     := QrTXAjsItsPallet.Value;
      FmTXAjsIts.CBPallet.KeyValue         := QrTXAjsItsPallet.Value;
      FmTXAjsIts.EdQtde.ValueVariant       := QrTXAjsItsQtde.Value;
      FmTXAjsIts.EdValorMP.ValueVariant    := QrTXAjsItsValorMP.Value;
      FmTXAjsIts.EdCustoMOUni.ValueVariant := QrTXAjsItsCustoMOUni.Value;
      FmTXAjsIts.EdFornecMO.ValueVariant   := QrTXAjsItsFornecMO.Value;
      FmTXAjsIts.CBFornecMO.KeyValue       := QrTXAjsItsFornecMO.Value;
      FmTXAjsIts.EdMarca.ValueVariant      := QrTXAjsItsMarca.Value;
      FmTXAjsIts.EdObserv.ValueVariant     := QrTXAjsItsObserv.Value;
      FmTXAjsIts.EdStqCenLoc.ValueVariant  := QrTXAjsItsStqCenLoc.Value;
      FmTXAjsIts.CBStqCenLoc.KeyValue      := QrTXAjsItsStqCenLoc.Value;
      FmTXAjsIts.EdReqMovEstq.ValueVariant := QrTXAjsItsReqMovEstq.Value;
      FmTXAjsIts.RGIxxMovIX.ItemIndex      := QrTXAjsItsIxxMovIX.Value;
      FmTXAjsIts.EdIxxFolha.ValueVariant   := QrTXAjsItsIxxFolha.Value;
      FmTXAjsIts.EdIxxLinha.ValueVariant   := QrTXAjsItsIxxLinha.Value;
      //
      FmTXAjsIts.EdNivel2.ValueVariant     := QrTXAjsItsDstNivel2.Value;
      FmTXAjsIts.CBNivel2.KeyValue         := QrTXAjsItsDstNivel2.Value;
      if QrTXAjsItsDstNivel2.Value <> 0 then
        FmTXAjsIts.RGMovimentacao.ItemIndex := 1
      else
        FmTXAjsIts.RGMovimentacao.ItemIndex := 2;
      // Devem ser os �ltimos? Na ordem abaixo?
      FmTXAjsIts.EdCustoMOTot.ValueVariant := QrTXAjsItsCustoMOTot.Value;
      FmTXAjsIts.EdValorT.ValueVariant     := QrTXAjsItsValorT.Value;
    end;
    FmTXAjsIts.ShowModal;
    FmTXAjsIts.Destroy;
  end;
end;

procedure TFmTXAjsCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTXAjsCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTXAjsCab, QrTXAjsIts);
end;

procedure TFmTXAjsCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTXAjsCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTXAjsIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTXAjsIts);
  //
  TX_PF.HabilitaComposTXAtivo(QrTXAjsItsID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmTXAjsCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTXAjsCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTXAjsCab.DefParams;
begin
  VAR_GOTOTABELA := 'txajscab';
  VAR_GOTOMYSQLTABLE := QrTXAjsCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome) NO_CLIENTEMO ');
  VAR_SQLx.Add('FROM txajscab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cmo ON cmo.Codigo=wic.ClienteMO');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  VAR_SQLx.Add('AND wic.Codigo=:P0');  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTXAjsCab.Estoque1Click(Sender: TObject);
begin
  TX_Jan.MostraFormTXMovImpEstoque(False);
end;

procedure TFmTXAjsCab.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  TXLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateTX.RecriaTempTableNovo(ntrttTXMovImp4, DModG.QrUpdPID1,
      False, 1, '_TXmovimp4_' + Self.Name);
  Empresa := QrTXAjsCabEmpresa.Value;
  N := 0;
  QrTXAjsIts.First;
  while not QrTXAjsIts.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrTXAjsItsPallet.Value;
    //
    QrTXAjsIts.Next;
  end;
  if N > 0 then
    TX_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmTXAjsCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormTXAjsIts(stUpd);
end;

procedure TFmTXAjsCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTXAjsCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTXAjsCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTXAjsCab.ItsExclui1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrTXAjsItsCodigo.Value;
  MovimCod := QrTXAjsItsMovimCod.Value;
  //
  if TX_PF.ExcluiControleTXMovIts(QrTXAjsIts, TIntegerField(QrTXAjsItsMovimCod),
  QrTXAjsItsControle.Value, CtrlBaix, QrTXAjsItsSrcNivel2.Value, False,
  Integer(TEstqMotivDel.emtdWetCurti023)) then
  begin
    TX_PF.AtualizaTotaisTXXxxCab('txajscab', MovimCod);
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTXAjsCab.ReopenTXAjsIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXAjsCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'txp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(tmi.Qtde=0, 0, tmi.ValorT / tmi.Qtde) CUS_Qtde ',
  '']);
  //'FROM ' + TX_PF.TabMovTX_Tab(tab) + ' tmi ',
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXAjsCabMovimCod.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXAjsIts, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrTXAjsIts);
  //
  QrTXAjsIts.Locate('Controle', Controle, []);
end;


procedure TFmTXAjsCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTXAjsCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTXAjsCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTXAjsCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTXAjsCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTXAjsCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTXAjsCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTXAjsCabCodigo.Value;
  Close;
end;

procedure TFmTXAjsCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormTXAjsIts(stIns);
end;

procedure TFmTXAjsCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTXAjsCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txajscab');
  Empresa := DModG.ObtemFilialDeEntidade(QrTXAjsCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmTXAjsCab.BtConfirmaClick(Sender: TObject);
var
  DataHora: String;
  Codigo, MovimCod, Empresa, ClienteMO, Terceiro: Integer;
  DataChegadaInvalida: Boolean;
begin
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  ClienteMO      := EdClienteMO.ValueVariant;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' +
                    Geral.FDT(EdDataHora.ValueVariant, 100);
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(ClienteMO = 0, EdClienteMO, 'Defina o dono do material!') then Exit;
  if MyObjects.FIC(TPDataHora.DateTime < 2, TPDataHora, 'Defina uma data de compra!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txajscab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('txmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'txajscab', False, [
  'MovimCod', 'Empresa', CO_DATA_HORA_GRL,
  'ClienteMO'], [
  'Codigo'], [
  MovimCod, Empresa, DataHora,
  ClienteMO], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
      TX_PF.InsereTXMovCab(MovimCod, emidInventario, Codigo)
    else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Empresa', CO_DATA_HORA_TMI, 'ClientMO'
      ], ['MovimCod'], [
      Empresa, DataHora, ClienteMO
      ], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTXAjsCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'txajscab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txajscab', 'Codigo');
end;

procedure TFmTXAjsCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTXAjsCab.BtCabClick(Sender: TObject);
begin
  TX_PF.HabilitaMenuInsOuAllTXAberto(QrTXAjsCab, QrTXAjsCabDataHora.Value,
  BtCab, PMCab, [CabInclui1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTXAjsCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrClienteMO, Dmod.MyDB);
end;

procedure TFmTXAjsCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTXAjsCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTXAjsCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmTXAjsCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXAjsCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrTXAjsCabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
  'UPDATE ' + CO_UPD_TAB_TMI,
  'SET MovimID=' + Geral.FF0(Integer(emidInventario)),
  'WHERE MovimID=' + Geral.FF0(Integer(emidAjuste)),
  'AND MovimCod IN ( ',
  '     SELECT MovimCod ',
  '     FROM txajscab ',
  ') ',
  '']));
  Geral.MB_Aviso('Atualiza��o 0 > 13 OK!');
end;

procedure TFmTXAjsCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTXAjsCab.QrTXAjsCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTXAjsCab.QrTXAjsCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTXAjsIts(0);
end;

procedure TFmTXAjsCab.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmTXAjsCab.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmTXAjsCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTXAjsCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTXAjsCab.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrTXAjsCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'txajscab', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTXAjsCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTXAjsCab.frxTEX_FAXAO_010_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmTXAjsCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTXAjsCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'txajscab');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDataHora.SetFocus;
end;

procedure TFmTXAjsCab.Classificao1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxTEX_FAXAO_010_01, 'Ajuste de Estoque');
end;

procedure TFmTXAjsCab.QrTXAjsCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTXAjsIts.Close;
end;

procedure TFmTXAjsCab.QrTXAjsCabBeforeOpen(DataSet: TDataSet);
begin
  QrTXAjsCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

