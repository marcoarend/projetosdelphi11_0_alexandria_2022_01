object FmTXMovIts: TFmTXMovIts
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-045 :: Gerenciamento de IME-Is'
  ClientHeight = 731
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 625
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 562
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 106
    Width = 1008
    Height = 625
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 474
      Width = 1008
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 478
      ExplicitWidth = 30
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 369
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 310
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel8: TPanel
          Left = 597
          Top = 0
          Width = 407
          Height = 310
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Gera'#231#227'o: '
            TabOrder = 0
            object Panel9: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label15: TLabel
                Left = 4
                Top = 0
                Width = 58
                Height = 13
                Caption = 'Quantidade:'
                FocusControl = DBEdit15
              end
              object Label19: TLabel
                Left = 96
                Top = 0
                Width = 50
                Height = 13
                Caption = 'Valor total:'
                FocusControl = DBEdit19
              end
              object DBEdit15: TDBEdit
                Left = 4
                Top = 16
                Width = 88
                Height = 21
                DataField = 'Qtde'
                DataSource = DsTXMovIts
                TabOrder = 0
              end
              object DBEdit19: TDBEdit
                Left = 96
                Top = 16
                Width = 80
                Height = 21
                DataField = 'ValorT'
                DataSource = DsTXMovIts
                TabOrder = 1
              end
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 57
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Saldo atual: '
            TabOrder = 1
            object Panel14: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label28: TLabel
                Left = 4
                Top = 0
                Width = 58
                Height = 13
                Caption = 'Quantidade:'
                FocusControl = DBEdit15
              end
              object DBEdit28: TDBEdit
                Left = 4
                Top = 16
                Width = 88
                Height = 21
                DataField = 'SdoVrtQtd'
                DataSource = DsTXMovIts
                TabOrder = 0
              end
            end
          end
          object GroupBox6: TGroupBox
            Left = 0
            Top = 114
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Gera'#231#227'o a partir deste IME-I: '
            TabOrder = 2
            object Panel16: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label42: TLabel
                Left = 4
                Top = 0
                Width = 58
                Height = 13
                Caption = 'Quantidade:'
                FocusControl = DBEdit15
              end
              object DBEdit42: TDBEdit
                Left = 4
                Top = 16
                Width = 88
                Height = 21
                DataField = 'QtdGer'
                DataSource = DsTXMovIts
                TabOrder = 0
              end
            end
          end
          object GroupBox7: TGroupBox
            Left = 0
            Top = 171
            Width = 407
            Height = 57
            Align = alTop
            Caption = ' Estoque Anterior ???: '
            TabOrder = 3
            object Panel17: TPanel
              Left = 2
              Top = 15
              Width = 403
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label47: TLabel
                Left = 4
                Top = 0
                Width = 58
                Height = 13
                Caption = 'Quantidade:'
                FocusControl = DBEdit15
              end
              object DBEdit47: TDBEdit
                Left = 4
                Top = 16
                Width = 88
                Height = 21
                DataField = 'QtdAnt'
                DataSource = DsTXMovIts
                TabOrder = 0
              end
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 228
            Width = 407
            Height = 82
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 4
            object Label7: TLabel
              Left = 8
              Top = 4
              Width = 47
              Height = 13
              Caption = 'MovimNiv'
              FocusControl = DBEdit6
            end
            object Label8: TLabel
              Left = 68
              Top = 4
              Width = 52
              Height = 13
              Caption = 'MovimTwn'
              FocusControl = DBEdit7
            end
            object Label9: TLabel
              Left = 128
              Top = 4
              Width = 42
              Height = 13
              Caption = 'MovimID'
              FocusControl = DBEdit8
            end
            object Label38: TLabel
              Left = 236
              Top = 44
              Width = 40
              Height = 13
              Caption = 'ValorMP'
              FocusControl = DBEdit38
            end
            object Label37: TLabel
              Left = 152
              Top = 44
              Width = 60
              Height = 13
              Caption = 'CustoMOTot'
              FocusControl = DBEdit37
            end
            object Label36: TLabel
              Left = 68
              Top = 44
              Width = 66
              Height = 13
              Caption = 'Custo MO Uni'
              FocusControl = DBEdit36
            end
            object Label35: TLabel
              Left = 8
              Top = 44
              Width = 50
              Height = 13
              Caption = 'FornecMO'
              FocusControl = DBEdit35
            end
            object Label58: TLabel
              Left = 320
              Top = 44
              Width = 42
              Height = 13
              Caption = 'CustoPQ'
              FocusControl = DBEdit58
            end
            object DBEdit6: TDBEdit
              Left = 8
              Top = 20
              Width = 56
              Height = 21
              DataField = 'MovimNiv'
              DataSource = DsTXMovIts
              TabOrder = 0
            end
            object DBEdit7: TDBEdit
              Left = 68
              Top = 20
              Width = 56
              Height = 21
              DataField = 'MovimTwn'
              DataSource = DsTXMovIts
              TabOrder = 1
            end
            object DBEdit8: TDBEdit
              Left = 128
              Top = 20
              Width = 56
              Height = 21
              DataField = 'MovimID'
              DataSource = DsTXMovIts
              TabOrder = 2
            end
            object DBEdit37: TDBEdit
              Left = 152
              Top = 60
              Width = 80
              Height = 21
              DataField = 'CustoMOTot'
              DataSource = DsTXMovIts
              TabOrder = 3
            end
            object DBEdit38: TDBEdit
              Left = 236
              Top = 60
              Width = 80
              Height = 21
              DataField = 'ValorMP'
              DataSource = DsTXMovIts
              TabOrder = 4
            end
            object DBEdit36: TDBEdit
              Left = 68
              Top = 60
              Width = 80
              Height = 21
              DataField = 'CustoMOUni'
              DataSource = DsTXMovIts
              TabOrder = 5
            end
            object DBEdit35: TDBEdit
              Left = 8
              Top = 60
              Width = 56
              Height = 21
              DataField = 'FornecMO'
              DataSource = DsTXMovIts
              TabOrder = 6
            end
            object DBEdit58: TDBEdit
              Left = 320
              Top = 60
              Width = 80
              Height = 21
              DataField = 'CustoPQ'
              DataSource = DsTXMovIts
              TabOrder = 7
            end
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 597
          Height = 310
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 597
            Height = 105
            Align = alTop
            Caption = ' Dados Gerais: '
            TabOrder = 0
            object Panel11: TPanel
              Left = 2
              Top = 15
              Width = 593
              Height = 88
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label1: TLabel
                Left = 4
                Top = 0
                Width = 28
                Height = 13
                Caption = 'IME-I:'
                FocusControl = DBEdCodigo
              end
              object Label13: TLabel
                Left = 64
                Top = 0
                Width = 29
                Height = 13
                Caption = 'Pallet:'
                FocusControl = DBEdit12
              end
              object Label2: TLabel
                Left = 180
                Top = 0
                Width = 167
                Height = 13
                Caption = 'Processo ou opera'#231#227'o de gera'#231#227'o:'
              end
              object Label12: TLabel
                Left = 428
                Top = 0
                Width = 58
                Height = 13
                Caption = 'Data / hora:'
                FocusControl = DBEdit11
              end
              object Label3: TLabel
                Left = 124
                Top = 0
                Width = 32
                Height = 13
                Caption = 'IME-C:'
                FocusControl = dmkDBEdit1
              end
              object Label4: TLabel
                Left = 64
                Top = 40
                Width = 44
                Height = 13
                Caption = 'Empresa:'
                FocusControl = DBEdit3
              end
              object Label5: TLabel
                Left = 124
                Top = 40
                Width = 42
                Height = 13
                Caption = 'Terceiro:'
                FocusControl = DBEdit4
              end
              object Label6: TLabel
                Left = 184
                Top = 40
                Width = 41
                Height = 13
                Caption = 'Cli. pref.:'
                FocusControl = DBEdit5
              end
              object Label14: TLabel
                Left = 304
                Top = 40
                Width = 33
                Height = 13
                Caption = 'Artigo: '
                FocusControl = DBEdit13
              end
              object Label51: TLabel
                Left = 4
                Top = 40
                Width = 36
                Height = 13
                Caption = 'Codigo:'
                FocusControl = DBEdit3
              end
              object Label54: TLabel
                Left = 544
                Top = 0
                Width = 39
                Height = 13
                Caption = 'Arquivo:'
                FocusControl = DBEdit53
              end
              object Label57: TLabel
                Left = 244
                Top = 40
                Width = 43
                Height = 13
                Caption = 'ClientMO'
                FocusControl = DBEdit57
              end
              object DBEdCodigo: TdmkDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'Controle'
                DataSource = DsTXMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit12: TDBEdit
                Left = 64
                Top = 16
                Width = 56
                Height = 21
                DataField = 'Pallet'
                DataSource = DsTXMovIts
                TabOrder = 1
              end
              object DBEdit1: TDBEdit
                Left = 124
                Top = 16
                Width = 56
                Height = 21
                DataField = 'MovimCod'
                DataSource = DsTXMovIts
                TabOrder = 2
              end
              object DBEdit2: TDBEdit
                Left = 204
                Top = 16
                Width = 221
                Height = 21
                DataField = 'NO_EstqMovimID'
                DataSource = DsTXMovIts
                TabOrder = 3
              end
              object DBEdit11: TDBEdit
                Left = 428
                Top = 16
                Width = 112
                Height = 21
                DataField = 'DataHora'
                DataSource = DsTXMovIts
                TabOrder = 4
              end
              object dmkDBEdit1: TdmkDBEdit
                Left = 4
                Top = 56
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'Codigo'
                DataSource = DsTXMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 5
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit3: TDBEdit
                Left = 64
                Top = 56
                Width = 56
                Height = 21
                DataField = 'Empresa'
                DataSource = DsTXMovIts
                TabOrder = 6
              end
              object DBEdit4: TDBEdit
                Left = 124
                Top = 56
                Width = 56
                Height = 21
                DataField = 'Terceiro'
                DataSource = DsTXMovIts
                TabOrder = 7
              end
              object DBEdit5: TDBEdit
                Left = 184
                Top = 56
                Width = 56
                Height = 21
                DataField = 'CliVenda'
                DataSource = DsTXMovIts
                TabOrder = 8
              end
              object DBEdit13: TDBEdit
                Left = 304
                Top = 56
                Width = 56
                Height = 21
                DataField = 'GraGruX'
                DataSource = DsTXMovIts
                TabOrder = 9
              end
              object DBEdit14: TDBEdit
                Left = 360
                Top = 56
                Width = 225
                Height = 21
                DataField = 'NO_PRD_TAM_COR'
                DataSource = DsTXMovIts
                TabOrder = 10
              end
              object DBEdit46: TDBEdit
                Left = 180
                Top = 16
                Width = 25
                Height = 21
                DataField = 'MovimID'
                DataSource = DsTXMovIts
                TabOrder = 11
              end
              object DBEdit53: TDBEdit
                Left = 544
                Top = 16
                Width = 41
                Height = 21
                DataField = 'NO_TTW'
                DataSource = DsTXMovIts
                TabOrder = 12
              end
              object DBEdit57: TDBEdit
                Left = 244
                Top = 56
                Width = 56
                Height = 21
                DataField = 'ClientMO'
                DataSource = DsTXMovIts
                TabOrder = 13
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 105
            Width = 597
            Height = 60
            Align = alTop
            Caption = ' Dados de Origem: '
            TabOrder = 1
            object Panel12: TPanel
              Left = 2
              Top = 15
              Width = 593
              Height = 43
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label20: TLabel
                Left = 4
                Top = 0
                Width = 51
                Height = 13
                Caption = 'IME-I orig.:'
                FocusControl = dmkDBEdit2
              end
              object Label21: TLabel
                Left = 64
                Top = 0
                Width = 29
                Height = 13
                Caption = 'Pallet:'
                Enabled = False
                FocusControl = DBEdit20
              end
              object Label22: TLabel
                Left = 124
                Top = 0
                Width = 159
                Height = 13
                Caption = 'Processo ou opera'#231#227'o de origem:'
              end
              object Label23: TLabel
                Left = 528
                Top = 0
                Width = 30
                Height = 13
                Caption = 'Artigo:'
                FocusControl = DBEdit23
              end
              object dmkDBEdit2: TdmkDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'SrcNivel2'
                DataSource = DsTXMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                PopupMenu = PMIMIEOrig
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit20: TDBEdit
                Left = 64
                Top = 16
                Width = 56
                Height = 21
                TabOrder = 1
                Visible = False
              end
              object DBEdit21: TDBEdit
                Left = 124
                Top = 16
                Width = 56
                Height = 21
                DataField = 'SrcNivel1'
                DataSource = DsTXMovIts
                TabOrder = 2
              end
              object DBEdit22: TDBEdit
                Left = 180
                Top = 16
                Width = 345
                Height = 21
                DataField = 'NO_SrcMovID'
                DataSource = DsTXMovIts
                TabOrder = 3
              end
              object DBEdit23: TDBEdit
                Left = 528
                Top = 16
                Width = 56
                Height = 21
                DataField = 'SrcGGX'
                DataSource = DsTXMovIts
                TabOrder = 4
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 165
            Width = 597
            Height = 60
            Align = alTop
            Caption = ' Dados de Destino '
            TabOrder = 2
            object Panel13: TPanel
              Left = 2
              Top = 15
              Width = 593
              Height = 43
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label24: TLabel
                Left = 4
                Top = 0
                Width = 54
                Height = 13
                Caption = 'IME-I dest.:'
                FocusControl = dmkDBEdit3
              end
              object Label25: TLabel
                Left = 64
                Top = 0
                Width = 29
                Height = 13
                Caption = 'Pallet:'
                Enabled = False
                FocusControl = DBEdit24
              end
              object Label26: TLabel
                Left = 124
                Top = 0
                Width = 162
                Height = 13
                Caption = 'Processo ou opera'#231#227'o de destino:'
              end
              object Label27: TLabel
                Left = 528
                Top = 0
                Width = 30
                Height = 13
                Caption = 'Artigo:'
                FocusControl = DBEdit27
              end
              object dmkDBEdit3: TdmkDBEdit
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                TabStop = False
                DataField = 'DstNivel2'
                DataSource = DsTXMovIts
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                PopupMenu = PMIMEIDest
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                UpdType = utYes
                Alignment = taRightJustify
              end
              object DBEdit24: TDBEdit
                Left = 64
                Top = 16
                Width = 56
                Height = 21
                TabOrder = 1
                Visible = False
              end
              object DBEdit25: TDBEdit
                Left = 124
                Top = 16
                Width = 56
                Height = 21
                DataField = 'DstNivel1'
                DataSource = DsTXMovIts
                TabOrder = 2
              end
              object DBEdit26: TDBEdit
                Left = 180
                Top = 16
                Width = 345
                Height = 21
                DataField = 'NO_DstMovID'
                DataSource = DsTXMovIts
                TabOrder = 3
              end
              object DBEdit27: TDBEdit
                Left = 528
                Top = 16
                Width = 56
                Height = 21
                DataField = 'DstGGX'
                DataSource = DsTXMovIts
                TabOrder = 4
              end
            end
          end
          object Panel15: TPanel
            Left = 0
            Top = 225
            Width = 597
            Height = 85
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 3
            object Label40: TLabel
              Left = 240
              Top = 44
              Width = 30
              Height = 13
              Caption = 'Marca'
              FocusControl = DBEdit40
            end
            object Label41: TLabel
              Left = 180
              Top = 44
              Width = 40
              Height = 13
              Caption = 'Produto:'
              FocusControl = DBEdit41
            end
            object Label33: TLabel
              Left = 120
              Top = 44
              Width = 30
              Height = 13
              Caption = 'Tal'#227'o:'
              FocusControl = DBEdit33
            end
            object Label32: TLabel
              Left = 8
              Top = 44
              Width = 27
              Height = 13
              Caption = 'S'#233'rie:'
              FocusControl = DBEdit32
            end
            object Label31: TLabel
              Left = 8
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Observ'
              FocusControl = DBEdit31
            end
            object Label52: TLabel
              Left = 300
              Top = 4
              Width = 134
              Height = 13
              Caption = 'Local (e Centro) de estoque:'
              FocusControl = DBEdit51
            end
            object Label53: TLabel
              Left = 404
              Top = 44
              Width = 27
              Height = 13
              Caption = 'RME:'
              FocusControl = DBEdit52
            end
            object Label55: TLabel
              Left = 476
              Top = 44
              Width = 106
              Height = 13
              Caption = 'Data/hora cor. apont.:'
              FocusControl = DBEdit54
            end
            object Label56: TLabel
              Left = 484
              Top = 4
              Width = 95
              Height = 13
              Caption = 'NF-e S'#233'rie/N'#250'mero:'
              FocusControl = DBEdit55
            end
            object DBEdit40: TDBEdit
              Left = 239
              Top = 60
              Width = 162
              Height = 21
              DataField = 'Marca'
              DataSource = DsTXMovIts
              TabOrder = 0
            end
            object DBEdit33: TDBEdit
              Left = 120
              Top = 60
              Width = 56
              Height = 21
              DataField = 'Talao'
              DataSource = DsTXMovIts
              TabOrder = 1
            end
            object DBEdit41: TDBEdit
              Left = 180
              Top = 60
              Width = 56
              Height = 21
              DataField = 'GraGru1'
              DataSource = DsTXMovIts
              TabOrder = 2
            end
            object DBEdit32: TDBEdit
              Left = 8
              Top = 60
              Width = 109
              Height = 21
              DataField = 'SerieTal'
              DataSource = DsTXMovIts
              TabOrder = 3
            end
            object DBEdit31: TDBEdit
              Left = 8
              Top = 20
              Width = 289
              Height = 21
              DataField = 'Observ'
              DataSource = DsTXMovIts
              TabOrder = 4
            end
            object DBEdit51: TDBEdit
              Left = 300
              Top = 20
              Width = 181
              Height = 21
              DataField = 'NO_LOC_CEN'
              DataSource = DsTXMovIts
              TabOrder = 5
            end
            object DBEdit52: TDBEdit
              Left = 404
              Top = 60
              Width = 68
              Height = 21
              DataField = 'ReqMovEstq'
              DataSource = DsTXMovIts
              TabOrder = 6
            end
            object DBEdit54: TDBEdit
              Left = 476
              Top = 60
              Width = 112
              Height = 21
              DataField = 'DtCorrApo'
              DataSource = DsTXMovIts
              TabOrder = 7
            end
            object DBEdit55: TDBEdit
              Left = 484
              Top = 20
              Width = 32
              Height = 21
              DataField = 'NFeSer'
              DataSource = DsTXMovIts
              TabOrder = 8
            end
            object DBEdit56: TDBEdit
              Left = 516
              Top = 20
              Width = 72
              Height = 21
              DataField = 'NFeNum'
              DataSource = DsTXMovIts
              TabOrder = 9
            end
          end
        end
      end
      object Panel18: TPanel
        Left = 2
        Top = 325
        Width = 1004
        Height = 42
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label59: TLabel
          Left = 8
          Top = 4
          Width = 65
          Height = 13
          Caption = 'TXMulFrnCab'
          FocusControl = DBEdit59
        end
        object Label60: TLabel
          Left = 128
          Top = 4
          Width = 70
          Height = 13
          Caption = 'TXMulNFeCab'
          FocusControl = DBEdit60
        end
        object DBEdit59: TDBEdit
          Left = 8
          Top = 20
          Width = 116
          Height = 21
          DataField = 'TXMulFrnCab'
          DataSource = DsTXMovIts
          TabOrder = 0
        end
        object DBEdit60: TDBEdit
          Left = 128
          Top = 20
          Width = 116
          Height = 21
          DataField = 'TXMulNFeCab'
          DataSource = DsTXMovIts
          TabOrder = 1
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 561
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 324
        Top = 15
        Width = 682
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Label46: TLabel
          Left = 132
          Top = 4
          Width = 146
          Height = 13
          Caption = 'Senha para exclus'#227'o de IME-I:'
        end
        object Panel2: TPanel
          Left = 549
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtIMEI: TBitBtn
          Tag = 611
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&IME-I'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIMEIClick
        end
        object EdSenha: TEdit
          Left = 132
          Top = 21
          Width = 149
          Height = 20
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          PasswordChar = 'l'
          TabOrder = 2
        end
      end
    end
    object GBMovSrc: TGroupBox
      Left = 0
      Top = 369
      Width = 1008
      Height = 105
      Align = alTop
      Caption = ' IME-Is de Origem: '
      TabOrder = 2
      object DGMovSrc: TDBGrid
        Left = 2
        Top = 15
        Width = 1004
        Height = 88
        Align = alClient
        DataSource = DsTXMovSrc
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimID'
            Title.Caption = 'Movimento'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimNiv'
            Title.Caption = 'N'#237'vel do movimento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 264
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtQtd'
            Title.Caption = 'Sdo Qtde'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = #218'ltima data/hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MovimCod'
            Visible = True
          end>
      end
    end
    object GBMovDst: TGroupBox
      Left = 0
      Top = 498
      Width = 1008
      Height = 63
      Align = alBottom
      Caption = ' IME-Is de destino:'
      TabOrder = 3
      object DGMovDst: TDBGrid
        Left = 2
        Top = 15
        Width = 1004
        Height = 46
        Align = alClient
        DataSource = DsTXMovDst
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimID'
            Title.Caption = 'Movimento'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_MovimNiv'
            Title.Caption = 'N'#237'vel do movimento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 264
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SdoVrtQtd'
            Title.Caption = 'Sdo Qtde'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = #218'ltima data/hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MovimCod'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 311
        Height = 32
        Caption = 'Gerenciamento de IME-Is'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 311
        Height = 32
        Caption = 'Gerenciamento de IME-Is'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 311
        Height = 32
        Caption = 'Gerenciamento de IME-Is'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 54
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 20
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 120
    Top = 64
  end
  object QrTXMovIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXMovItsBeforeOpen
    AfterOpen = QrTXMovItsAfterOpen
    BeforeClose = QrTXMovItsBeforeClose
    AfterScroll = QrTXMovItsAfterScroll
    OnCalcFields = QrTXMovItsCalcFields
    Left = 32
    Top = 517
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMovItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXMovItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXMovItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXMovItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXMovItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXMovItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXMovItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXMovItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXMovItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXMovItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXMovItsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXMovItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXMovItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXMovItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXMovItsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXMovItsSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXMovItsTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXMovItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXMovItsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXMovItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXMovItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXMovItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXMovItsQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovItsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Origin = 'vspalleta.Nome'
      Size = 60
    end
    object QrTXMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXMovItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXMovItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovItsNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Origin = 'txsertal.Nome'
      Size = 60
    end
    object QrTXMovItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXMovItsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrTXMovItsNO_DstMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DstMovID'
      Size = 255
      Calculated = True
    end
    object QrTXMovItsNO_SrcMovID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SrcMovID'
      Size = 255
      Calculated = True
    end
    object QrTXMovItsNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 103
    end
    object QrTXMovItsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrTXMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrTXMovItsPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrTXMovItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrTXMovItsPedItsVda: TLargeintField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrTXMovItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrTXMovItsItemNFe: TLargeintField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrTXMovItsClientMO: TLargeintField
      FieldName = 'ClientMO'
      Required = True
    end
    object QrTXMovItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      Required = True
    end
    object QrTXMovItsTXMulFrnCab: TLargeintField
      FieldName = 'TXMulFrnCab'
      Required = True
    end
    object QrTXMovItsNFeSer: TLargeintField
      FieldName = 'NFeSer'
      Required = True
    end
    object QrTXMovItsNFeNum: TLargeintField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrTXMovItsTXMulNFeCab: TLargeintField
      FieldName = 'TXMulNFeCab'
      Required = True
    end
    object QrTXMovItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsTXMovIts: TDataSource
    DataSet = QrTXMovIts
    Left = 32
    Top = 565
  end
  object QrTXMovSrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 108
    Top = 517
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMovSrcCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXMovSrcControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXMovSrcMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXMovSrcMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXMovSrcMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXMovSrcEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXMovSrcTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXMovSrcCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXMovSrcMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXMovSrcDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXMovSrcPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovSrcGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXMovSrcQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovSrcValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXMovSrcSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXMovSrcSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXMovSrcSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXMovSrcSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovSrcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXMovSrcSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXMovSrcTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXMovSrcFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXMovSrcCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovSrcDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXMovSrcDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXMovSrcDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXMovSrcDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXMovSrcQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovSrcQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovSrcStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovSrcReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXMovSrcGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrTXMovSrcNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrTXMovSrcNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrTXMovSrcNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrTXMovSrcNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXMovSrcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXMovSrcNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovSrcNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXMovSrcNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXMovSrcID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXMovSrcNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsTXMovSrc: TDataSource
    DataSet = QrTXMovSrc
    Left = 108
    Top = 565
  end
  object QrTXMovDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 184
    Top = 517
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTXMovDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTXMovDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrTXMovDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrTXMovDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrTXMovDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrTXMovDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrTXMovDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrTXMovDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrTXMovDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrTXMovDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrTXMovDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrTXMovDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrTXMovDstQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrTXMovDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrTXMovDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrTXMovDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrTXMovDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrTXMovDstSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrTXMovDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrTXMovDstSerieTal: TLargeintField
      FieldName = 'SerieTal'
      Required = True
    end
    object QrTXMovDstTalao: TLargeintField
      FieldName = 'Talao'
      Required = True
    end
    object QrTXMovDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrTXMovDstCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrTXMovDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrTXMovDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrTXMovDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrTXMovDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrTXMovDstQtdGer: TFloatField
      FieldName = 'QtdGer'
    end
    object QrTXMovDstQtdAnt: TFloatField
      FieldName = 'QtdAnt'
    end
    object QrTXMovDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrTXMovDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrTXMovDstGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrTXMovDstNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrTXMovDstNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrTXMovDstNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrTXMovDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrTXMovDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXMovDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrTXMovDstNO_SerieTal: TWideStringField
      FieldName = 'NO_SerieTal'
      Size = 60
    end
    object QrTXMovDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrTXMovDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrTXMovDstNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsTXMovDst: TDataSource
    DataSet = QrTXMovDst
    Left = 184
    Top = 565
  end
  object PMIMEI: TPopupMenu
    OnPopup = PMIMEIPopup
    Left = 528
    Top = 624
    object Recalcula1: TMenuItem
      Caption = '&Recalcula saldos'
      OnClick = Recalcula1Click
    end
    object AlteradadosdoartigodoIMEI1: TMenuItem
      Caption = '&Altera dados do artigo do IME-I'
      OnClick = AlteradadosdoartigodoIMEI1Click
    end
    object AlteradadosdoitemdeIMEI1: TMenuItem
      Caption = 'Altera dados do item de IM&E-I'
      OnClick = AlteradadosdoitemdeIMEI1Click
    end
    object IrparagerenciamentodoMovimento1: TMenuItem
      Caption = '&Ir para a janela do Movimento'
      OnClick = IrparagerenciamentodoMovimento1Click
    end
    object Irparajaneladopallet1: TMenuItem
      Caption = 'Ir para janela do &Pallet'
      OnClick = Irparajaneladopallet1Click
    end
    object IrparajaneladaFichaRMP1: TMenuItem
      Caption = 'Ir para janela da Ficha RMP'
      OnClick = IrparajaneladaFichaRMP1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ExcluiIMEI1: TMenuItem
      Caption = '&Exclui IME-I'
      OnClick = ExcluiIMEI1Click
    end
    object Recriarbaixa1: TMenuItem
      Caption = '&Recriar baixa'
      OnClick = Recriarbaixa1Click
    end
  end
  object QrInacab: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 260
    Top = 520
    object QrInacabTMI_Baix: TIntegerField
      FieldName = 'TMI_Baix'
    end
    object QrInacabTXPaClaIts: TIntegerField
      FieldName = 'TXPaClaIts'
    end
    object QrInacabBxaQtde: TFloatField
      FieldName = 'BxaQtde'
    end
    object QrInacabTMI_Qtde: TFloatField
      FieldName = 'TMI_Qtde'
    end
    object QrInacabSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrInacabTMI_Sorc: TIntegerField
      FieldName = 'TMI_Sorc'
    end
  end
  object PMIMIEOrig: TPopupMenu
    Left = 376
    Top = 120
    object IrparaajaneladoMovimento1: TMenuItem
      Caption = '&Ir para a janela do Movimento'
      OnClick = IrparaajaneladoMovimento1Click
    end
  end
  object PMIMEIDest: TPopupMenu
    Left = 376
    Top = 176
    object MenuItem1: TMenuItem
      Caption = '&Ir para a janela do Movimento'
      OnClick = MenuItem1Click
    end
  end
end
