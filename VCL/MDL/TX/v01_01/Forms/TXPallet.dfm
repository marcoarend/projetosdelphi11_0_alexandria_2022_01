object FmTXPallet: TFmTXPallet
  Left = 368
  Top = 194
  Caption = 'TEX-FAXAO-013 :: Pallet de Artigo'
  ClientHeight = 602
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 104
    Width = 1008
    Height = 498
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 161
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 120
        Height = 13
        Caption = 'Descri'#231#227'o / Observa'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 500
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label5: TLabel
        Left = 792
        Top = 96
        Width = 33
        Height = 13
        Caption = 'Status:'
        Enabled = False
      end
      object SBCliente: TSpeedButton
        Left = 972
        Top = 72
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SBClienteClick
      end
      object LaTXRibCla: TLabel
        Left = 16
        Top = 56
        Width = 84
        Height = 13
        Caption = 'Artigo (Reduzido):'
      end
      object Label4: TLabel
        Left = 560
        Top = 56
        Width = 93
        Height = 13
        Caption = 'Cliente preferencial:'
      end
      object Label12: TLabel
        Left = 844
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label23: TLabel
        Left = 796
        Top = 16
        Width = 28
        Height = 13
        Caption = 'PPP*:'
      end
      object Label62: TLabel
        Left = 16
        Top = 96
        Width = 93
        Height = 13
        Caption = 'Dono dos produtos:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 420
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 500
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 560
        Top = 32
        Width = 233
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliStat: TdmkEditCB
        Left = 560
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CliStat'
        UpdCampo = 'CliStat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliStat
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliStat: TdmkDBLookupComboBox
        Left = 616
        Top = 72
        Width = 353
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsClientes
        TabOrder = 10
        dmkEditCB = EdCliStat
        QryCampo = 'CliStat'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStatus: TdmkEditCB
        Left = 792
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Status'
        UpdCampo = 'Status'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStatus
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStatus: TdmkDBLookupComboBox
        Left = 848
        Top = 112
        Width = 141
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 14
        dmkEditCB = EdStatus
        QryCampo = 'Status'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraGruX: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 75
        Top = 72
        Width = 482
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 8
        dmkEditCB = EdGraGruX
        QryCampo = 'GraGruX'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDtHrEndAdd: TdmkEditDateTimePicker
        Left = 844
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        Enabled = False
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrEndAdd'
        UpdCampo = 'DtHrEndAdd'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrEndAdd: TdmkEdit
        Left = 952
        Top = 32
        Width = 40
        Height = 21
        Enabled = False
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrEndAdd'
        UpdCampo = 'DtHrEndAdd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdQtdPrevPc: TdmkEdit
        Left = 796
        Top = 32
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '110'
        QryCampo = 'QtdPrevPc'
        UpdCampo = 'QtdPrevPc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 110
        ValWarn = False
      end
      object EdClientMO: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClientMO'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 713
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 12
        dmkEditCB = EdClientMO
        QryCampo = 'ClientMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 435
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Label24: TLabel
        Left = 152
        Top = 34
        Width = 200
        Height = 13
        Caption = 'PPP*: Previs'#227'o de pe'#231'as no pallet.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 104
    Width = 1008
    Height = 498
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 500
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label6: TLabel
        Left = 16
        Top = 56
        Width = 194
        Height = 13
        Caption = 'Artigo de Ribeira Classificado (Reduzido):'
      end
      object Label10: TLabel
        Left = 424
        Top = 56
        Width = 93
        Height = 13
        Caption = 'Cliente preferencial:'
      end
      object Label11: TLabel
        Left = 796
        Top = 56
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object Label13: TLabel
        Left = 16
        Top = 96
        Width = 126
        Height = 13
        Caption = 'Data / hora encerramento:'
        FocusControl = DBEdit9
      end
      object Label14: TLabel
        Left = 148
        Top = 96
        Width = 30
        Height = 13
        Caption = 'Artigo:'
        FocusControl = DBEdit10
      end
      object Label15: TLabel
        Left = 420
        Top = 96
        Width = 55
        Height = 13
        Caption = 'Quantidade'
        FocusControl = DBEdit12
      end
      object Label19: TLabel
        Left = 708
        Top = 96
        Width = 39
        Height = 13
        Caption = '$ Custo:'
        FocusControl = DBEdit16
      end
      object Label20: TLabel
        Left = 780
        Top = 96
        Width = 50
        Height = 13
        Caption = 'Sdo Pe'#231'a:'
        FocusControl = DBEdit17
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTXPallet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 420
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTXPallet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 500
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTXPallet
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 560
        Top = 32
        Width = 433
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsTXPallet
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsTXPallet
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 72
        Top = 72
        Width = 349
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsTXPallet
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 424
        Top = 72
        Width = 56
        Height = 21
        DataField = 'CliStat'
        DataSource = DsTXPallet
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 480
        Top = 72
        Width = 313
        Height = 21
        DataField = 'NO_CLISTAT'
        DataSource = DsTXPallet
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 796
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Status'
        DataSource = DsTXPallet
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 852
        Top = 72
        Width = 141
        Height = 21
        DataField = 'NO_STATUS'
        DataSource = DsTXPallet
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 16
        Top = 112
        Width = 129
        Height = 21
        DataField = 'DtHrEndAdd_TXT'
        DataSource = DsTXPallet
        TabOrder = 10
      end
      object DBEdit10: TDBEdit
        Left = 148
        Top = 112
        Width = 56
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsSumPall
        TabOrder = 11
      end
      object DBEdit11: TDBEdit
        Left = 204
        Top = 112
        Width = 213
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsSumPall
        TabOrder = 12
      end
      object DBEdit12: TDBEdit
        Left = 420
        Top = 112
        Width = 68
        Height = 21
        DataField = 'Qtde'
        DataSource = DsSumPall
        TabOrder = 13
      end
      object DBEdit16: TDBEdit
        Left = 708
        Top = 112
        Width = 68
        Height = 21
        DataField = 'ValorT'
        DataSource = DsSumPall
        TabOrder = 14
      end
      object DBEdit17: TDBEdit
        Left = 780
        Top = 112
        Width = 68
        Height = 21
        DataField = 'SdoVrtQtd'
        DataSource = DsSumPall
        TabOrder = 15
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 434
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pallet'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIMEI: TBitBtn
          Tag = 589
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&IME-I'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIMEIClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 137
      Width = 1008
      Height = 112
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'IME-Is'
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 84
          Align = alClient
          DataSource = DsIMEIs
          PopupMenu = PMIMEI
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'RME'
              Width = 78
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimID'
              Title.Caption = 'Movimento'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'Nivel Movimento'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigo'
              Width = 264
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtQtd'
              Title.Caption = 'Sdo Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = #218'ltima data/hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SrcMovID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SrcNivel1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SrcNivel2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstMovID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstNivel1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstNivel2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimCod'
              Title.Caption = 'IME-C'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Gera'#231#227'o por classe/reclasse'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGTXPaIts: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 473
          Height = 86
          Align = alLeft
          DataSource = DsTXPaIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'TXPallet'
              Title.Caption = 'Pallet destino'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXPaClaIts'
              Title.Caption = 'ID add classe'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXPaRclIts'
              Title.Caption = 'ID add reclasse'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TMI_Sorc'
              Title.Caption = 'IME-I origem'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TMI_Dest'
              Title.Caption = 'IME-I destino'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Pe'#231'as'
              Width = 56
              Visible = True
            end>
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 473
          Top = 0
          Width = 529
          Height = 86
          Align = alClient
          DataSource = DsTXPaItens
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXPallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TMI_Sorc'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TMI_Dest'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TMI_Baix'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tecla'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrIni'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrFim'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Couros em classifica'#231#227'o'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1002
          Height = 86
          Align = alClient
          DataSource = DsTXCacItsAOpn
          PopupMenu = PMCacIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Pallets interdependentes'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GroupBox5: TGroupBox
          Left = 637
          Top = 0
          Width = 365
          Height = 86
          Align = alRight
          Caption = ' Destino: '
          TabOrder = 0
          object dmkDBGridZTO5: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 361
            Height = 70
            Align = alClient
            DataSource = DsPallDst
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'TXPallet'
                Title.Caption = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TMI_Dest'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrEndAdd_TXT'
                Title.Caption = 'Encerramento'
                Visible = True
              end>
          end
        end
        object GroupBox6: TGroupBox
          Left = 0
          Top = 0
          Width = 637
          Height = 86
          Align = alClient
          Caption = ' Origem: '
          TabOrder = 1
          object dmkDBGridZTO6: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 633
            Height = 70
            Align = alClient
            DataSource = DsPallOri
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_ClaRcl'
                Title.Caption = 'Forma'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CacCod'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXPallet'
                Title.Caption = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrFimCla_TXT'
                Title.Caption = 'Encerramento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end>
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Origem x destino classifica'#231#227'o'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter1: TSplitter
          Left = 497
          Top = 0
          Width = 5
          Height = 84
          ExplicitHeight = 86
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 497
          Height = 84
          Align = alLeft
          Caption = ' Destino: '
          TabOrder = 0
          object dmkDBGridZTO2: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 493
            Height = 36
            Align = alClient
            DataSource = DsDestino
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
          end
          object Panel6: TPanel
            Left = 2
            Top = 51
            Width = 493
            Height = 31
            Align = alBottom
            ParentBackground = False
            TabOrder = 1
            object Label25: TLabel
              Left = 0
              Top = 8
              Width = 84
              Height = 13
              Caption = 'Total de registros:'
            end
            object EdDestino: TdmkEdit
              Left = 88
              Top = 4
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
        object GroupBox2: TGroupBox
          Left = 502
          Top = 0
          Width = 498
          Height = 84
          Align = alClient
          Caption = ' Origem:  '
          TabOrder = 1
          object Splitter2: TSplitter
            Left = 187
            Top = 15
            Height = 67
            ExplicitHeight = 70
          end
          object GroupBox3: TGroupBox
            Left = 2
            Top = 15
            Width = 185
            Height = 67
            Align = alLeft
            Caption = 'Classifica'#231#227'o: '
            TabOrder = 0
            object dmkDBGridZTO3: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 181
              Height = 20
              Align = alClient
              DataSource = DsOriCla
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
            end
            object Panel7: TPanel
              Left = 2
              Top = 35
              Width = 181
              Height = 30
              Align = alBottom
              ParentBackground = False
              TabOrder = 1
              object Label26: TLabel
                Left = 0
                Top = 8
                Width = 84
                Height = 13
                Caption = 'Total de registros:'
              end
              object EdOriCla: TdmkEdit
                Left = 88
                Top = 4
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 190
            Top = 15
            Width = 306
            Height = 67
            Align = alClient
            Caption = 'Reclassifica'#231#227'o: '
            TabOrder = 1
            object dmkDBGridZTO4: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 302
              Height = 20
              Align = alClient
              DataSource = DsOriRcl
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
            end
            object Panel8: TPanel
              Left = 2
              Top = 35
              Width = 302
              Height = 30
              Align = alBottom
              ParentBackground = False
              TabOrder = 1
              object Label27: TLabel
                Left = 0
                Top = 8
                Width = 84
                Height = 13
                Caption = 'Total de registros:'
              end
              object EdOriRcl: TdmkEdit
                Left = 88
                Top = 4
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 183
        Height = 32
        Caption = 'Pallet de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 183
        Height = 32
        Caption = 'Pallet de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 183
        Height = 32
        Caption = 'Pallet de Artigo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 52
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 35
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 18
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 64
    Top = 56
  end
  object QrTXPallet: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTXPalletBeforeOpen
    AfterOpen = QrTXPalletAfterOpen
    BeforeClose = QrTXPalletBeforeClose
    AfterScroll = QrTXPalletAfterScroll
    OnCalcFields = QrTXPalletCalcFields
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM txpalleta let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN txpalsta   vps ON vps.Codigo=let.Status'
      'WHERE let.Codigo > 0')
    Left = 236
    Top = 69
    object QrTXPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrTXPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTXPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTXPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTXPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTXPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTXPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTXPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTXPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrTXPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrTXPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrTXPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrTXPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrTXPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrTXPalletDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrTXPalletDtHrEndAdd_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrEndAdd_TXT'
      Calculated = True
    end
    object QrTXPalletQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
    object QrTXPalletGerRclCab: TIntegerField
      FieldName = 'GerRclCab'
    end
    object QrTXPalletDtHrFimRcl: TDateTimeField
      FieldName = 'DtHrFimRcl'
    end
    object QrTXPalletMovimIDGer: TIntegerField
      FieldName = 'MovimIDGer'
    end
    object QrTXPalletClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsTXPallet: TDataSource
    DataSet = QrTXPallet
    Left = 236
    Top = 113
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 528
    Top = 504
    object CabInclui1: TMenuItem
      Caption = '&Inclui Automaticamente'
      OnClick = CabInclui1Click
    end
    object IncluiManualmente1: TMenuItem
      Caption = 'Inclui &Manualmente'
      OnClick = IncluiManualmente1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      object udo1: TMenuItem
        Caption = '&Tudo'
        OnClick = udo1Click
      end
      object ApenasaDescrio1: TMenuItem
        Caption = 'Apenas a Descri'#231#227'o'
        OnClick = ApenasaDescrio1Click
      end
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object EncerraPallet1: TMenuItem
      Caption = 'Encerra &Pallet'
      OnClick = EncerraPallet1Click
    end
    object Encerraclassificao1: TMenuItem
      Caption = 'Encerra &Classifica'#231#227'o'
      OnClick = Encerraclassificao1Click
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Atualizasaldo1: TMenuItem
      Caption = 'Atuali&za saldo'
      OnClick = Atualizasaldo1Click
    end
    object AtualizaStatus1: TMenuItem
      Caption = 'Atualiza &Status'
      OnClick = AtualizaStatus1Click
    end
    object RecalculaIMEIsdeclassereclasse1: TMenuItem
      Caption = 'Recalcula IME-Is de classe/&reclasse'
      object IMEIsdegerao1: TMenuItem
        Caption = 'IME_Is de &gera'#231#227'o'
        OnClick = IMEIsdegerao1Click
      end
    end
    object AlteraFornecedor1: TMenuItem
      Caption = 'Altera &Fornecedor'
      OnClick = AlteraFornecedor1Click
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NO_ENT')
    Left = 428
    Top = 69
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 428
    Top = 113
  end
  object QrSumPall: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.GraGruX, SUM(wmi.Qtde) Qtde, SUM(ValorT) ValorT,'
      'SUM(wmi.SdoVrtQtd) SdoVrtQtd, ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, MAX(wmi.DataHora) DataHora'
      'FROM txmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Pallet = 47')
    Left = 632
    Top = 68
    object QrSumPallGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSumPallQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumPallValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrSumPallSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumPallGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSumPallNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrSumPallDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object QrIMEIs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Controle, wmi.GraGruX, wmi.Qtde, wmi.ValorT,'
      'wmi.SdoVrtQtd, ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, wmi.DataHora,'
      'wmi.SrcNivel1, wmi.SrcNivel2,'
      'wmi.DstNivel1, wmi.DstNivel2'
      'FROM txmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Pallet = 47')
    Left = 708
    Top = 68
    object QrIMEIsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEIsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEIsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEIsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEIsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEIsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEIsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEIsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEIsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEIsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEIsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEIsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEIsQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEIsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEIsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEIsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEIsSdoVrtQtd: TFloatField
      FieldName = 'SdoVrtQtd'
    end
    object QrIMEIsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEIsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEIsCustoMOUni: TFloatField
      FieldName = 'CustoMOUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEIsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEIsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEIsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEIsQtdGer: TFloatField
      FieldName = 'QtdGer'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsQtdAnt: TFloatField
      FieldName = 'QtdAnt'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrIMEIsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrIMEIsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrIMEIsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrIMEIsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrIMEIsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEIsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEIsNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
  end
  object DsSumPall: TDataSource
    DataSet = QrSumPall
    Left = 632
    Top = 116
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 708
    Top = 116
  end
  object QrTXCacItsAOpn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM txcacitsa'
      'WHERE TXPallet=17')
    Left = 320
    Top = 344
    object QrTXCacItsAOpnCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrTXCacItsAOpnCacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrTXCacItsAOpnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXCacItsAOpnControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrTXCacItsAOpnClaAPalOri: TIntegerField
      FieldName = 'ClaAPalOri'
    end
    object QrTXCacItsAOpnRclAPalOri: TIntegerField
      FieldName = 'RclAPalOri'
    end
    object QrTXCacItsAOpnTXPaClaIts: TIntegerField
      FieldName = 'TXPaClaIts'
    end
    object QrTXCacItsAOpnTXPaRclIts: TIntegerField
      FieldName = 'TXPaRclIts'
    end
    object QrTXCacItsAOpnTXPallet: TIntegerField
      FieldName = 'TXPallet'
    end
    object QrTXCacItsAOpnTMI_Sorc: TIntegerField
      FieldName = 'TMI_Sorc'
    end
    object QrTXCacItsAOpnTMI_Dest: TIntegerField
      FieldName = 'TMI_Dest'
    end
    object QrTXCacItsAOpnBox: TIntegerField
      FieldName = 'Box'
    end
    object QrTXCacItsAOpnQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrTXCacItsAOpnRevisor: TIntegerField
      FieldName = 'Revisor'
    end
    object QrTXCacItsAOpnDigitador: TIntegerField
      FieldName = 'Digitador'
    end
    object QrTXCacItsAOpnDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXCacItsAOpnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXCacItsAOpnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXCacItsAOpnSumido: TSmallintField
      FieldName = 'Sumido'
    end
    object QrTXCacItsAOpnRclAPalDst: TIntegerField
      FieldName = 'RclAPalDst'
    end
    object QrTXCacItsAOpnTMI_Baix: TIntegerField
      FieldName = 'TMI_Baix'
    end
    object QrTXCacItsAOpnMartelo: TIntegerField
      FieldName = 'Martelo'
    end
    object QrTXCacItsAOpnFrmaIns: TSmallintField
      FieldName = 'FrmaIns'
    end
    object QrTXCacItsAOpnSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
  end
  object DsTXCacItsAOpn: TDataSource
    DataSet = QrTXCacItsAOpn
    Left = 320
    Top = 392
  end
  object QrTXPaIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTXPaItsBeforeClose
    AfterScroll = QrTXPaItsAfterScroll
    SQL.Strings = (
      'SELECT TXPallet, TXPaClaIts, TXPaRclIts, TMI_Sorc, TMI_Dest, '
      'SUM(Qtde) Qtde '
      'FROM txcacitsa'
      'WHERE TXPallet=318 '
      'GROUP BY TXPallet, TXPaClaIts, TXPaRclIts, '
      'TMI_Sorc, TMI_Dest')
    Left = 56
    Top = 356
    object QrTXPaItsTXPallet: TIntegerField
      FieldName = 'TXPallet'
    end
    object QrTXPaItsTXPaClaIts: TIntegerField
      FieldName = 'TXPaClaIts'
    end
    object QrTXPaItsTXPaRclIts: TIntegerField
      FieldName = 'TXPaRclIts'
    end
    object QrTXPaItsTMI_Sorc: TIntegerField
      FieldName = 'TMI_Sorc'
    end
    object QrTXPaItsTMI_Dest: TIntegerField
      FieldName = 'TMI_Dest'
    end
    object QrTXPaItsQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object DsTXPaIts: TDataSource
    DataSet = QrTXPaIts
    Left = 56
    Top = 404
  end
  object QrTXPaItens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, TXPallet, TMI_Sorc, TMI_Baix, TMI_Dest'
      'Tecla, DtHrIni, DtHrFim, QIt_Sorc, QIt_Baix '
      'FROM txpaclaitsa'
      'WHERE Controle=138')
    Left = 132
    Top = 356
    object QrTXPaItensCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXPaItensControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTXPaItensTXPallet: TIntegerField
      FieldName = 'TXPallet'
    end
    object QrTXPaItensTMI_Sorc: TIntegerField
      FieldName = 'TMI_Sorc'
    end
    object QrTXPaItensTMI_Dest: TIntegerField
      FieldName = 'TMI_Dest'
    end
    object QrTXPaItensTMI_Baix: TIntegerField
      FieldName = 'TMI_Baix'
    end
    object QrTXPaItensTecla: TIntegerField
      FieldName = 'Tecla'
    end
    object QrTXPaItensDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrTXPaItensDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrTXPaItensQIt_Sorc: TIntegerField
      FieldName = 'QIt_Sorc'
    end
    object QrTXPaItensQIt_Baix: TIntegerField
      FieldName = 'QIt_Baix'
    end
  end
  object DsTXPaItens: TDataSource
    DataSet = QrTXPaItens
    Left = 132
    Top = 404
  end
  object PMIMEI: TPopupMenu
    OnPopup = PMIMEIPopup
    Left = 660
    Top = 512
    object Irparajaneladomovimento1: TMenuItem
      Caption = 'Ir para janela do movimento'
      OnClick = Irparajaneladomovimento1Click
    end
    object IrparajaneladedadosdoIMEI1: TMenuItem
      Caption = 'Ir para janela de dados do IME-I '
      OnClick = IrparajaneladedadosdoIMEI1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ExcluiIMEIselecionado1: TMenuItem
      Caption = 'Exclui IME-I selecionado'
      OnClick = ExcluiIMEIselecionado1Click
    end
    object AlteraIMEIorigemdoIMEISelecionado1: TMenuItem
      Caption = 'Altera IME-I origem do IME-I Selecionado'
      OnClick = AlteraIMEIorigemdoIMEISelecionado1Click
    end
    object Atualizasaldo2: TMenuItem
      Caption = 'Atualiza saldo'
      OnClick = Atualizasaldo2Click
    end
  end
  object QrOriRcl: TmySQLQuery
    Database = Dmod.MyDB
    Left = 792
    Top = 300
  end
  object DsOriRcl: TDataSource
    DataSet = QrOriRcl
    Left = 792
    Top = 348
  end
  object QrOriCla: TmySQLQuery
    Database = Dmod.MyDB
    Left = 876
    Top = 304
  end
  object DsOriCla: TDataSource
    DataSet = QrOriCla
    Left = 876
    Top = 352
  end
  object QrDestino: TmySQLQuery
    Database = Dmod.MyDB
    Left = 736
    Top = 304
  end
  object DsDestino: TDataSource
    DataSet = QrDestino
    Left = 736
    Top = 352
  end
  object QrPallDst: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.TXPallet, SUM(Qtde) Qtde, TMI_Dest, pal.DtHrEndAdd,'
      'IF(pal.DtHrEndAdd < "1900-01-01", "",'
      'DATE_FORMAT(pal.DtHrEndAdd, "%d/%m/%Y %h:%i:%s")) DtHrEndAdd_TXT'
      'FROM txcacitsa cia'
      'LEFT JOIN txparclcaba rcl ON cia.CacCod=rcl.CacCod'
      'LEFT JOIN txpalleta   pal ON pal.Codigo=cia.TXPallet'
      'WHERE rcl.TXPallet=489'
      'GROUP BY TMI_Dest')
    Left = 520
    Top = 292
    object QrPallDstTXPallet: TIntegerField
      FieldName = 'TXPallet'
    end
    object QrPallDstQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPallDstTMI_Dest: TIntegerField
      FieldName = 'TMI_Dest'
    end
    object QrPallDstDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrPallDstDtHrEndAdd_TXT: TWideStringField
      FieldName = 'DtHrEndAdd_TXT'
      Size = 19
    end
  end
  object DsPallDst: TDataSource
    DataSet = QrPallDst
    Left = 520
    Top = 340
  end
  object QrPallOri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 2.000 ClaRcl, "Reclassifica'#231#227'o" NO_ClaRcl,'
      'cia.CacCod, cia.Codigo, SUM(cia.Qtde) Qtde,'
      'pcc.TXMovIts, pcc.TXPallet + 0.000 TXPallet, pcc.DtHrFimCla,'
      'IF(pcc.DtHrFimCla < "1900-01-01", "",'
      'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %h:%i:%s")) DtHrFimCla_TXT'
      'FROM txcacitsa cia'
      'LEFT JOIN txparclcaba pcc ON pcc.CacCod=cia.CacCod'
      'LEFT JOIN txmovits vmi ON vmi.Controle=pcc.TXMovIts'
      'WHERE cia.TXPallet IN (474, 480)'
      'AND cia.CacID=8'
      'GROUP BY cia.CacCod, cia.Codigo'
      ''
      'UNION'
      ''
      'SELECT 1.000 ClaRcl, "Classifica'#231#227'o" NO_ClaRcl,'
      'cia.CacCod, cia.Codigo, SUM(cia.Qtde) Qtde,'
      'pcc.TXMovIts, 0 TXPallet, pcc.DtHrFimCla,'
      'IF(pcc.DtHrFimCla < "1900-01-01", "",'
      'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %h:%i:%s")) DtHrFimCla_TXT'
      'FROM txcacitsa cia'
      'LEFT JOIN txpaclacaba pcc ON pcc.CacCod=cia.CacCod'
      'LEFT JOIN txmovits vmi ON vmi.Controle=pcc.TXMovIts'
      'WHERE cia.TXPallet IN (474, 480)'
      'AND cia.CacID=7'
      'GROUP BY cia.CacCod, cia.Codigo')
    Left = 596
    Top = 292
    object QrPallOriClaRcl: TFloatField
      FieldName = 'ClaRcl'
      Required = True
    end
    object QrPallOriNO_ClaRcl: TWideStringField
      FieldName = 'NO_ClaRcl'
      Required = True
      Size = 15
    end
    object QrPallOriCacCod: TIntegerField
      FieldName = 'CacCod'
      Required = True
    end
    object QrPallOriCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPallOriQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPallOriTXMovIts: TIntegerField
      FieldName = 'TXMovIts'
    end
    object QrPallOriTXPallet: TFloatField
      FieldName = 'TXPallet'
    end
    object QrPallOriDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
    end
    object QrPallOriDtHrFimCla_TXT: TWideStringField
      FieldName = 'DtHrFimCla_TXT'
      Size = 19
    end
  end
  object DsPallOri: TDataSource
    DataSet = QrPallOri
    Left = 596
    Top = 340
  end
  object PMCacIts: TPopupMenu
    OnPopup = PMCacItsPopup
    Left = 228
    Top = 376
    object Alteradadosdiitematual1: TMenuItem
      Caption = '&Altera dados do item atual'
      OnClick = Alteradadosdiitematual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrSumTMI: TmySQLQuery
    Database = Dmod.MyDB
    Left = 416
    Top = 460
    object QrSumTMIQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object PMImprime: TPopupMenu
    Left = 124
    Top = 52
    object FichaCOMonomedoPallet1: TMenuItem
      Caption = 'Ficha &COM o nome do Pallet'
      OnClick = FichaCOMonomedoPallet1Click
    end
    object FichaSEMonomedoPallet1: TMenuItem
      Caption = 'Ficha &SEM o nome do Pallet'
      OnClick = FichaSEMonomedoPallet1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Itensnopallet1: TMenuItem
      Caption = 'Itens no pallet'
      OnClick = Itensnopallet1Click
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM txnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 112
    Top = 192
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 112
    Top = 240
  end
  object QrClientMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 40
    Top = 464
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 40
    Top = 512
  end
  object frxDsTXCacItsAOpnImp: TfrxDBDataset
    UserName = 'frxDsTXCacItsAOpnImp'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CacCod=CacCod'
      'CacID=CacID'
      'Codigo=Codigo'
      'Controle=Controle'
      'ClaAPalOri=ClaAPalOri'
      'RclAPalOri=RclAPalOri'
      'TXPaClaIts=TXPaClaIts'
      'TXPaRclIts=TXPaRclIts'
      'TXPallet=TXPallet'
      'TMI_Sorc=TMI_Sorc'
      'TMI_Dest=TMI_Dest'
      'Box=Box'
      'Qtde=Qtde'
      'Revisor=Revisor'
      'Digitador=Digitador'
      'DataHora=DataHora'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Sumido=Sumido'
      'RclAPalDst=RclAPalDst'
      'TMI_Baix=TMI_Baix'
      'Martelo=Martelo'
      'FrmaIns=FrmaIns'
      'SubClass=SubClass'
      'NoRev=NoRev'
      'NoDig=NoDig')
    DataSet = QrTXCacItsAOpnImp
    BCDToCurrency = False
    Left = 884
    Top = 52
  end
  object frxWET_CURTI_013_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_013_AGetValue
    Left = 888
    Top = 100
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsTXCacItsAOpnImp
        DataSetName = 'frxDsTXCacItsAOpnImp'
      end
      item
        DataSet = frxDsTXPallet
        DataSetName = 'frxDsTXPallet'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 71.811060240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Itens no Pallet [frxDsTXPallet."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 68.031540000000010000
          Top = 56.692949999999990000
          Width = 385.511864720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Revisor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 56.692949999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitQtde: TfrxMemoView
          Left = 453.543600000000000000
          Top = 56.692949999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 529.134200000000000000
          Top = 56.692949999999990000
          Width = 151.181151180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora revis'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXPallet."Nome"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTXCacItsAOpnImp
        DataSetName = 'frxDsTXCacItsAOpnImp'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 68.031540000000000000
          Width = 41.574634720000000000
          Height = 15.118110240000000000
          DataField = 'Revisor'
          DataSet = frxDsTXCacItsAOpnImp
          DataSetName = 'frxDsTXCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTXCacItsAOpnImp."Revisor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsTXCacItsAOpnImp
          DataSetName = 'frxDsTXCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXCacItsAOpnImp."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValQtde: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsTXCacItsAOpnImp
          DataSetName = 'frxDsTXCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXCacItsAOpnImp."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181151180000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsTXCacItsAOpnImp
          DataSetName = 'frxDsTXCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTXCacItsAOpnImp."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 109.606370000000000000
          Width = 343.937034720000000000
          Height = 15.118110240000000000
          DataField = 'NoRev'
          DataSet = frxDsTXCacItsAOpnImp
          DataSetName = 'frxDsTXCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTXCacItsAOpnImp."NoRev"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000010000
          Width = 453.543404720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTValorT: TfrxMemoView
          Left = 453.543600000000000000
          Top = 15.118120000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsTXCacItsAOpnImp."Qtde">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 529.134200000000000000
          Top = 15.118120000000010000
          Width = 151.181151180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsTXPallet: TfrxDBDataset
    UserName = 'frxDsTXPallet'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Empresa=Empresa'
      'NO_EMPRESA=NO_EMPRESA'
      'Status=Status'
      'CliStat=CliStat'
      'GraGruX=GraGruX'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_STATUS=NO_STATUS'
      'DtHrEndAdd=DtHrEndAdd'
      'DtHrEndAdd_TXT=DtHrEndAdd_TXT'
      'QtdPrevPc=QtdPrevPc'
      'GerRclCab=GerRclCab'
      'DtHrFimRcl=DtHrFimRcl'
      'MovimIDGer=MovimIDGer'
      'ClientMO=ClientMO')
    DataSet = QrTXPallet
    BCDToCurrency = False
    Left = 236
    Top = 160
  end
  object QrTXCacItsAOpnImp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM txcacitsa'
      'WHERE TXPallet=17')
    Left = 884
    Top = 8
    object QrTXCacItsAOpnImpCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrTXCacItsAOpnImpCacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrTXCacItsAOpnImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTXCacItsAOpnImpControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrTXCacItsAOpnImpClaAPalOri: TIntegerField
      FieldName = 'ClaAPalOri'
    end
    object QrTXCacItsAOpnImpRclAPalOri: TIntegerField
      FieldName = 'RclAPalOri'
    end
    object QrTXCacItsAOpnImpTXPaClaIts: TIntegerField
      FieldName = 'TXPaClaIts'
    end
    object QrTXCacItsAOpnImpTXPaRclIts: TIntegerField
      FieldName = 'TXPaRclIts'
    end
    object QrTXCacItsAOpnImpTXPallet: TIntegerField
      FieldName = 'TXPallet'
    end
    object QrTXCacItsAOpnImpTMI_Sorc: TIntegerField
      FieldName = 'TMI_Sorc'
    end
    object QrTXCacItsAOpnImpTMI_Dest: TIntegerField
      FieldName = 'TMI_Dest'
    end
    object QrTXCacItsAOpnImpBox: TIntegerField
      FieldName = 'Box'
    end
    object QrTXCacItsAOpnImpQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrTXCacItsAOpnImpRevisor: TIntegerField
      FieldName = 'Revisor'
    end
    object QrTXCacItsAOpnImpDigitador: TIntegerField
      FieldName = 'Digitador'
    end
    object QrTXCacItsAOpnImpDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTXCacItsAOpnImpAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTXCacItsAOpnImpAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTXCacItsAOpnImpSumido: TSmallintField
      FieldName = 'Sumido'
    end
    object QrTXCacItsAOpnImpRclAPalDst: TIntegerField
      FieldName = 'RclAPalDst'
    end
    object QrTXCacItsAOpnImpTMI_Baix: TIntegerField
      FieldName = 'TMI_Baix'
    end
    object QrTXCacItsAOpnImpMartelo: TIntegerField
      FieldName = 'Martelo'
    end
    object QrTXCacItsAOpnImpFrmaIns: TSmallintField
      FieldName = 'FrmaIns'
    end
    object QrTXCacItsAOpnImpSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrTXCacItsAOpnImpNoRev: TWideStringField
      FieldName = 'NoRev'
      Size = 100
    end
    object QrTXCacItsAOpnImpNoDig: TWideStringField
      FieldName = 'NoDig'
      Size = 100
    end
  end
  object Qr1: TmySQLQuery
    Database = DModG.MySyncDB
    Left = 564
    Top = 464
  end
end
