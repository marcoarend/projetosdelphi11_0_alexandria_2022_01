unit UnTX_PF;

interface

uses
  System.Generics.Collections, UnMyLinguas, MyListas,
  //
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, TypInfo, System.Math,
  UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnTX_Tabs,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnGrl_Vars, UnAppEnums,
  CreateTX;

type
  THackDBGrid = class(TDBGrid);
  TUnTX_PF = class(TObject)
  private
    function AlteraVMI_CliVenda(Controle, Atual: Integer): Boolean;
    function AlteraVMI_FornecMO(Controle, Atual: Integer): Boolean;
    { Private declarations }
  public
    { Public declarations }
    // A
    procedure AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
    procedure AbreTXSerTal(QrTXSerTal: TmySQLQuery);
    function  AdicionarNovosTX_emid(): Boolean;
    function  AlteraTMI_CliVenda(Controle, Atual: Integer): Boolean;
    function  AlteraTMI_FornecMO(Controle, Atual: Integer): Boolean;
    function  AlteraTMI_StqCenLoc(Controle, Atual: Integer): Boolean;
    procedure AtualizaDescendentes(IMEI: Integer; Campos: array of String;
              Valores: array of Variant);
    procedure AtualizaDtHrFimOpe_MovimCod(MovimID: TEstqMovimID; MovimCod: Integer);
    procedure AtualizaFornecedorInd(MovimCod: Integer);
    procedure AtualizaFornecedorRRM(MovimCod: Integer);
    procedure AtualizaPalletPelosIMEIsDeGeracao(Pallet: Integer;
              QrSumTMI: TmySQLQuery);
    procedure AtualizaSaldoIMEI(Controle: Integer; Gera: Boolean);
    procedure AtualizaSaldoVirtualTXMovIts(Controle: Integer; Gera: Boolean);
    procedure AtualizaSaldoVirtualTXMovIts_Generico(Controle, MovimID, MovimNiv: Integer);
    procedure AtualizaSaldoOrigemTMI_Dest(TMI_Dest, TMI_Sorc, TMI_Baix: Integer);
    procedure AtualizaStatPall(Pallet: Integer);
    function  AtualizaTMIsDeBox(TXPallet, TMI_Dest, TMI_Baix, TMI_Sorc: Integer;
              QrSumDest, QrSumSorc, QrTMISorc, QrPalSorc: TmySQLQuery): Boolean;
    procedure AtualizaTotaisTXXxxCab(Tabela: String; MovimCod: Integer);
    procedure AtualizaTotaisTXIndCab(MovimCod: Integer);
    procedure AtualizaTotaisTXRRMCab(MovimCod: Integer);
    procedure AtualizaTXPedIts_Fin(TXPedIts: Integer);
    procedure AtualizaTXPedIts_Lib(TXPedIts, TXMovIts: Integer; LibQtde: Double);
    // C
    function  CadastraPalletInfo(Empresa, ClientMO, GraGruX, GraGruY: Integer;
              QrTXPallet: TmySQLQuery; EdPallet: TdmkEditCB; CBPallet:
              TdmkDBLookupComboBox; SBNewPallet: TSpeedButton; EdQtde:
              TdmkEdit): Integer;
    function  CadastraPalletRibCla(Empresa, ClientMO: Integer; EdPallet:
              TdmkEditCB; CBPallet: TdmkDBLookupComboBox; QrTXPallet: TmySQLQuery;
              MovimIDGer: TEstqMovimID; GraGruX: Integer): Integer;
    function  CalculoValorOrigemUni(Qtde, CustoPQ, ValorMP, CusFrtMOEnv: Double): Double;
    function  ComparaUnidMed2GGX(GGX1, GGX2: Integer): Boolean;
    function  ConfigContinuarInserindoFolhaLinha(RGModoContinuarInserindo:
              TRadioGroup; EdFolha, EdLinha: TdmkEdit): Boolean;

    // D
    procedure DefineDataHoraOuDtCorrApoCompos(const DataHora, DtCorrApo:
              TDateTime; TPData: TdmkEditDateTimePicker; EdHora: TdmkEdit);
              overload;
    procedure DefineDataHoraOuDtCorrApoCompos(const DataHora, DtCorrApo:
              TDateTime; var _DataHora: TDateTime); overload;
    function  DefineDatasTMI(const _Data_Hora_: String; var DataHora, DtCorrApo:
              String): Boolean;
    function  DefineIDs_Str(DBGridZTO: TdmkDBGridZTO; Query:
              TmySQLQuery; Campo: String): String; // Corda Lista
    function  DefineSiglaTX_Frn(Fornece: Integer): String;

    // E
    function  EditaIxx(DGDados: TDBGrid; QrTXMovIts: TmySQLQuery): Boolean;
    function  AlteraTMI_Qtde(MovimID, MovimNiv, Controle: Integer; Default:
              Double; PermiteNegativo: TSinal): Boolean;
    function  EncerraPalletNew(const Pallet: Integer; const Pergunta: Boolean):
              Boolean;
    procedure EncerraPalletSimples(Pallet, Empresa, ClientMO: Integer;
              QrTXPallet: TmySQLQuery; PallOnEdit: array of Integer);
    function  ExcluiControleTXMovIts(Tabela: TmySQLQuery; Campo: TIntegerField;
              Controle1, CtrlBaix, SrcNivel2: Integer; Gera: Boolean;
              Motivo: Integer; Pergunta: Boolean = True; Reabre: Boolean = True): Boolean;
    function  ExcluiTXNaoTMI(Pergunta, Tabela, Campo: String; Inteiro1:
              Integer; DB: TmySQLDatabase): Integer;
    function  ExcluiTXMovIts_EnviaArquivoExclu(Controle, Motivo: Integer; Query:
              TmySQLQuery; DataBase: TmySQLDatabase; Senha: String): Boolean;
    // F
    function  FatoresIncompativeis(FatorSrc, FatorDst: Integer; MeAviso: TMemo):
              Boolean;

    // G
    function  GeraNovoPallet(Empresa, ClientMO, GraGruX: Integer;
              EdGraGruX, EdPallet: TdmkEdit; CBPallet: TdmkDBLookupComboBox;
              SBNewPallet: TSpeedButton; QrTXPallet: TmySQLQuery;
              PallOnEdit: array of Integer): Integer;
    function  GeraSQLTXMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left, SQL_Wher,
              SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer = 0):
              String;
    function  GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Where, SQL_Group:
              String; Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
    function  GeraSQLTabMov(var SQL: String; const Tab: TTabToWork; const
              TemIMEIMrt: Integer = 0): Boolean;
    function  GetIDNiv(MovimID, MovimNiv: Integer): Integer;
    // H
    function  HabilitaComposTXAtivo(ID_TTW: Integer; Compos:
              array of TComponent): Boolean;
    function  HabilitaMenuInsOuAllTXAberto(Qry: TmySQLQuery; Data: TDateTime;
              Button: TWinControl; DefMenu: TPopupMenu;
              MenuItens: array of TMenuItem): Boolean;
    // I
    function  ImpedePeloBalanco(Data: TDateTime; PermiteCorrApo, Avisa: Boolean): Boolean;
    function  ImprimeEstoqueEm(Entidade, Filial, Ed00Terceiro_ValueVariant,
              RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex,
              RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex,
              RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex: Integer;
              Ck00DescrAgruNoItm_Checked: Boolean; Ed00StqCenCad_ValueVariant,
              RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA, CB00StqCenCad_Text,
              CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
              Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean;
              DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
              Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
              DataEm: TDateTime; GraCusPrc: Integer; Ed00NFeIni_ValueVariant,
              Ed00NFeFim_ValueVariant: Integer; Ck00Serie_Checked: Boolean;
              Ed00Serie_ValueVariant, MovimCod: Integer;
              LaAviso1, LaAviso2: TLabel;
              MostraFrx: Boolean): Boolean;
    function  ImprimeEstoqueReal(Entidade, Filial, Ed00Terceiro_ValueVariant,
              RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex,
              RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex,
              RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex: Integer;
              Ck00DescrAgruNoItm_Checked: Boolean; Ed00StqCenCad_ValueVariant,
              RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA, CB00StqCenCad_Text,
              CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
              Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean;
              DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
              Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
              TableSrc, DataRetroativa: String; GraCusPrc: Integer;
              DataEstoque: TDateTime;
              Ed00NFeIni_ValueVariant, Ed00NFeFim_ValueVariant: Integer;
              Ck00Serie_Checked: Boolean; Ed00Serie_ValueVariant, MovimCod:
              Integer; MostraFrx: Boolean): Boolean;
    procedure ImprimeIMEI(IMEIs: array of Integer; TXImpImeiKind:
              TXXImpImeiKind; LPFMO, FNFeRem: String; QryCab: TmySQLQuery);
    procedure ImprimePallet_Unico(Empresa, ClientMO, Pallet: Integer; JanTab:
              String; InfoNO_PALLET: Boolean);
    procedure ImprimePallet_Varios(Empresa: Integer; Pallets: array of Integer;
              JanTab: String; InfoNO_PALLET: Boolean);
    procedure ImprimePallets(Empresa: Integer; Pallets: array of Integer;
              TXMovImp4, TXLstPalBox: String; InfoNO_PALLET: Boolean;
              DestImprFichaPallet: TDestImprFichaPallet;
              ImpEmpresa: Boolean = True; EmptyLabels: Integer = 0);
    function  InformaIxx(DBGIMEI: TDBGrid; QrIMEI: TmySQLQuery; FldIMEI:
              String): Boolean;
    procedure InfoStqCenLoc(TMI, StqCenLoc: Integer; Qry: TmySQLQuery);
    procedure InfoPalletTMI(TMI, Pallet: Integer; Qry: TmySQLQuery);
    procedure InfoReqMovEstq(TMI, ReqMovEstq: Integer; Qry: TmySQLQuery);
    procedure InfoRegIntInTMI(Campo: String; TMI, Inteiro: Integer; Qry:
              TmySQLQuery);
    procedure InsereTXMovCab(Codigo: Integer; MovimID: TEstqMovimID;
              CodigoID: Integer);
    function  InsUpdTXMovIts(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv:
              TEstqMovimNiv; Pallet, GraGruX: Integer; Qtde, ValorT: Double;
              _Data_Hora_: String; SrcMovID: TEstqMovimID; SrcNivel1, SrcNivel2:
              Integer; Observ: String; CliVenda, Controle: Integer;
              CustoMOUni, CustoMOTot, ValorMP: Double; DstMovID: TEstqMovimID;
              DstNivel1, DstNivel2: Integer; QtdGer: Double; AptoUso, FornecMO:
              Integer; SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
              PedItsLib, PedItsFin, PedItsVda: Integer; ReqMovEstq, StqCenLoc,
              ItemNFe, TXMulFrnCab, ClientMO: Integer; QtdAnt: Double;
              SerieTal, Talao, GGXRcl: Integer; MovCodPai: Integer; IxxMovIX:
              TEstqMovInfo; IxxFolha, IxxLinha: Integer; ExigeClientMO,
              ExigeFornecMO, ExigeStqLoc: Boolean; InsUpdTMIPrcExecID:
              TInsUpdTMIPrcExecID): Boolean;
    function  InverteDatas(const MovimXX: Integer; const Data, Agora: TDateTime;
              var DataHora, DtCorrApo: String): Boolean;
    // L
    function  LocalizaPorCampo(EstqMovimID: TEstqMovimID; Caption, Campo:
              String): Integer;
    function  LocalizaPeloIMEC(EstqMovimID: TEstqMovimID): Integer;
    function  LocalizaPeloIMEI(EstqMovimID: TEstqMovimID): Integer;
    // M
    procedure MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc: TControl; EdStqCenLoc:
              TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox; QrStqCenLoc:
              TmySQLQuery; MovimID: TEstqMovimID);
    // O
    function  ObrigaInfoIxx(IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha:
              Integer): Boolean;
    function  ObtemControleIMEI(const SQLType: TSQLType; var Controle:
              Integer; const Senha: String): Boolean;
    function  ObtemControleMovimTwin(MovimCod, MovimTwn: Integer; MovimID:
              TEstqMovimID; MovimNiv: TEstqMovimNiv): Integer;
    function  ObtemDataBalanco(Empresa: Integer): TDateTime;
    function  ObtemFatorInteiro(const GraGruX: Integer; var FatorIntRes: Integer;
              const Avisa: Boolean; const FatorIntCompara: Integer; MeAviso:
              TMemo): Boolean;
    function  ObtemGraGruY_de_IDTipoCouro(Index: Integer): Integer;
    function  ObtemNomeFrxEstoque(Data: String; RG00_Agrupa_ItemIndex,
              RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
              RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex: Integer;
              CB00StqCenCad_Text: String): String;
    function  ObtemNomeTabelaTXXxxCab(MovimID: TEstqMovimID;
              Avisa: Boolean = True): String;
    function  ObtemProximoTalao(const Empresa: Integer; const EdSerieTal:
              TdmkEditCB; var Talao: Integer; SerieDefinida: Integer = 0):
              Boolean;
    //
    procedure ReopenPedItsXXX(QrTXPedIts: TmySQLQuery; InsPedIts,
              UpdPedIts: Integer);
    // P
    function  PesquisaPallets(const Empresa, ClientMO, CouNiv2, CouNiv1,
              StqCenCad, StqCenLoc: Integer; const Tabela: String; const GraGruYs: String;
              const GraGruXs, Pallets: array of Integer; const SQL_Especificos:
              String; const MovimID: TEstqMovimID; const MovimCod: Integer;
              var sTXMovImp4: String): Boolean;
    // R
{   N�o pode!! Pallet n�o � pelo reduzido, mas pelo Nivel1!
    function  RedefineReduzidoOnPallet(MovimID: TEstqMovimID; MovimNiv:
              TEstqMovimNiv): (*TTipoSinal*) TTipoTrocaGgxTmiPall;
}
    procedure ReopenGraGruX(Qry: TmySQLQuery; SQL_AND: String);
    procedure ReopenGraGruX_Pallet(QrGraGruX: TmySQLQuery);
    procedure ReopenIMEIsPositivos(Qry: TmySQLQuery; Empresa, GraGruX,
              Pallet, Talao, IMEI, TipoCouro, StqCenCad, StqCenLoc, ImeiSrc,
              CouNiv1, CouNiv2: Integer; DataLimite: TDateTime);
    procedure ReopenQrySaldoIMEI_Baixa(Qry1, Qry2: TmySQLQuery; SrcNivel2:
              Integer);
    procedure ReopenStqCenLoc(Qry: TmySQLQuery; Centro: Variant; Controle:
              Integer; MovimID: TEstqMovimID = TEstqMovimID.emidAjuste);
    procedure ReopenTXGerArtSrc(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; SQL_Limit: String; EstqMovimNiv: TEstqMovimNiv);
    procedure ReopenTXIts_Controle_If(Qry: TmySQLQuery; Controle, TemIMEIMrt:
              Integer);
    procedure ReopenTXItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNivz:
              array of TEstqMovimNiv; LocCtrl: Integer);
    procedure ReopenTXListaPallets(Qry: TmySQLQuery; Tabela: String;
              Empresa, GraGruX, Pallet, Terceiro: Integer; OrderBy: String);
    procedure ReopenTXMOEnvAvu(QrTXMOEnvAvu: TmySQLQuery; MovimCod, Codigo:
              Integer);
    procedure ReopenTXMOEnvATMI(QrTXMOEnvATmi: TmySQLQuery; TXMOEnvAvu, Codigo:
              Integer);
    procedure ReopenTXMOEnvEnv(QrTXMOEnvEnv: TmySQLQuery; MovimCod, Codigo:
              Integer);
    procedure ReopenTXMOEnvRet(QrTXMOEnvRet: TmySQLQuery; MovimCod, Codigo:
              Integer);
    procedure ReopenTXMOEnvETMI(QrTXMOEnvETmi: TmySQLQuery; TXMOEnvEnv, Codigo:
              Integer);
    procedure ReopenTXMOEnvGTmi(QrTXMOEnvGTmi: TmySQLQuery; TXMOEnvRet, Codigo:
              Integer);
    procedure ReopenTXMOEnvRTmi(QrTXMOEnvRTmi: TmySQLQuery; TXMOEnvRet, Codigo:
              Integer);
    procedure ReopenTXMovIts_Pallet2(QrTXMovIts, QrPallets: TmySQLQuery);
    procedure ReopenTXIndPrcBxa(Qry: TmySQLQuery; MovimCod, MovimTwn, Controle,
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
    procedure ReopenTXIndPrcDst(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenTXIndDesclDst(Qry: TmySQLQuery; Codigo, Controle, Pallet,
              TemIMEIMrt: Integer);
    procedure ReopenTXPallet(QrTXPallet: TmySQLQuery; Empresa, ClientMO, GraGruX:
              Integer; Ordem: String; PallOnEdit: array of Integer);
    procedure ReopenTXPrestador(QrPrestador: TmySQLQuery);
    procedure ReopenTXMovXXX(Qry: TmySQLQuery; Campo: String; IMEI, TemIMEIMrt,
              CtrlLoc: Integer);
    procedure ReopenTXXxxExtra(Qry: TmySQLQuery; Codigo, TemIMEIMrt: Integer;
              MovimID, DstMovID: TEstqMovimID; MovimNiv: TEstqMovimNiv);
    procedure ReopenTXXxxOris(QrTXXxxCab, QrTXXxxOriIMEI, QrTXXxxOriPallet:
              TmySQLQuery; Controle, Pallet: Integer; MovimNiv: TEstqMovimNiv);
    // S
    procedure SetaGGXUnicoEmLista(const GraGruX: Integer; var Lista: TPallArr);
    procedure SetIDTipoCouro(RG: TRadioGroup; Colunas, Default: Integer);
    function  SQL_LJ_GGX(): String;
    function  SQL_LJ_FRN(): String;
    function  SQL_LJ_CMO(): String;
    function  SQL_LJ_SCL(): String;
    function  SQL_MovIDeNiv_Pos_All(): String;
    function  SQL_NO_GGX(): String;
    function  SQL_NO_FRN(): String;
    function  SQL_NO_CMO(): String;
    function  SQL_NO_SCL(): String;
    (*function  SQL_MovIDeNiv_Pos_Inn(): String;*)
    function  SQL_TipoEstq_DefinirCodi(CAST: Boolean; NomeFld: String;
              UsaVirgula: Boolean; FldQtd: String; Prefacio: String = ''):
              String;
    //  T
    function  TalaoErro(EdSerieTal: TdmkEdit; Empresa, Controle, Talao: Integer;
              PermiteDuplicar: Boolean =  False): Boolean;
    function  TabMovTX_Fld_IMEI(Tab: TTabToWork): String;
    function  TabMovTX_Fld_Pall(Tab: TTabToWork): String;
    function  TabMovTX_Tab(Tab: TTabToWork): String;
    function  TXFic(GraGruX, Empresa, Fornecedor, Pallet: Integer;
              Qtde, ValorT: Double; EdGraGruX, EdPallet, EdQtde, EdValorT:
              TWinControl; ExigeFornecedor: Boolean; GraGruY: Integer;
              EdStqCenLoc: TWinControl; SerieTal, Talao: Integer; EdSerieTal,
              EdTalao: TWinControl): Boolean;
    // V
    function  ValidaCampoNF(Operacao: Integer; EditNF: TdmkEdit; MostraMsg:
              Boolean): Boolean;
    function  VerificaBalanco(MesesAntes: Integer): Integer;
    // Z
    procedure ZeraSaldoIMEI(Controle: Integer);

  end;

var
  TX_PF: TUnTX_PF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, CfgCadLista,
  UMySQLDB, UMySQLModule, UnGOTOy, GetValor, UnGrade_PF, ModTX_CRC,
  UnEfdIcmsIpi_PF,
  StqCenCad,
  TXReqMovEstq, TXImpEstoque, TXImpEstqEm, TXImpPallet, TXImpIMEI,
  XXDataEFDData, TXPalletManual, TXPalletAdd, TXIxx;

{ TUnTX_PF }

procedure TUnTX_PF.AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
begin
  Grade_PF.AbreGraGruXY(Qry, _AND);
end;

procedure TUnTX_PF.AbreTXSerTal(QrTXSerTal: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXSerTal, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM txsertal ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
end;

function TUnTX_PF.AdicionarNovosTX_emid: Boolean;
const
////////////////////////////////////////////////////////////////////////////////
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  Procedure criada somente para marcar lugares para implementar codigo
///  quando criar mais TEstqMovimID.emid....
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
////////////////////////////////////////////////////////////////////////////////
  sEstqMovimID: array[0..MaxEstqMovimID] of string = (
    '00', // 00
    '01', // 01
    '02', // 02
    '03', // 03
    '04', // 04
    '05', // 05
    '06', // 06
    '07', // 07
    '08', // 08
    '09', // 09
    '10', // 10
    '11', // 11
    '12', // 12
    '13', // 13
    '14', // 14
    '15', // 15
    '16', // 16
    '17', // 17
    '18', // 18
    '19', // 19
    '20', // 20
    '21', // 21
    '22', // 22
    '23', // 23
    '24', // 24
    '25', // 25
    '26', // 26
    '27', // 27
    '28', // 28
    '29', // 29
    '30', // 30
    '31', // 31
    '32', // 32
    '33', // 33
    '34', // 34
    '35', // 35
    '36', // 36
    '37', // 37
    '38' // 37

////////////////////////////////////////////////////////////////////////////////
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  Procedure criada somente para marcar lugares para implementar codigo
///  quando criar mais TEstqMovimID.emid....
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
////////////////////////////////////////////////////////////////////////////////
  );
begin
  Result := True;
end;

function TUnTX_PF.AlteraTMI_CliVenda(Controle, Atual: Integer): Boolean;
const
  MostraDados = False;
var
  CliVenda: Integer;
begin
  CliVenda := Atual;
  if Entities.SelecionaEntidade(MostraDados, Geral.ATS([
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Descricao ',
  'FROM entidades ent ',
  'WHERE ent.Cliente1="V" ',
  'ORDER BY Descricao ']), CliVenda) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
    'CliVenda'], [
    'Controle'], [
    CliVenda], [
    Controle], True);
  end;
end;

function TUnTX_PF.AlteraTMI_FornecMO(Controle, Atual: Integer): Boolean;
const
  MostraDados = False;
var
  FornecMO: Integer;
begin
  FornecMO := Atual;
  if Entities.SelecionaEntidade(MostraDados, Geral.ATS([
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Descricao ',
  'FROM entidades ent ',
  'WHERE ent.' + VAR_FLD_ENT_PRESTADOR + '="V" ',
  'ORDER BY Descricao ']), FornecMO) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
    'FornecMO'], [
    'Controle'], [
    FornecMO], [
    Controle], True);
  end;
end;

function TUnTX_PF.AlteraTMI_Qtde(MovimID, MovimNiv, Controle: Integer;
  Default: Double; PermiteNegativo: TSinal): Boolean;
var
  ResVar: Variant;
  Qtde: Double;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Default, 2, 0, '', '', True, 'Quantidade', 'Informe a quantidade: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    if (Qtde >= 0) or (PermiteNegativo = siNegativo) then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Qtde'], ['Controle'], [Qtde], [Controle], True);
      if Result then
        AtualizaSaldoVirtualTXMovIts_Generico(Controle, MovimID, MovimNiv);
    end else
      Geral.MB_Aviso('Valor inv�lido! N�o pode ser negativo!');
  end;
end;

function TUnTX_PF.AlteraTMI_StqCenLoc(Controle, Atual: Integer): Boolean;
const
  MostraDados = False;
var
  StqCenLoc: Integer;
  //
  function SelecionaStqCenLoc(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de Local de estoque';
    Prompt = 'Informe o local: [F7 para pesquisar]';
    Campo  = 'Descricao';
  var
    Codigo: Variant;
  begin
    Result := False;
    Codigo :=
      DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, Atual,
      ['SELECT scl.Controle Codigo, ',
      'CONCAT(scl.Nome, " (", scc.Nome, ")") Descricao ',
      'FROM stqcenloc scl ',
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
      'ORDER BY Descricao '], Dmod.MyDB, True);
    if Codigo <> Null then
    begin
      StqCenLoc := Codigo;
      Result    := True;
      //
    end;
  end;
begin
  if SelecionaStqCenLoc() then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
    'StqCenLoc'], [
    'Controle'], [
    StqCenLoc], [
    Controle], True);
  end;
end;

function TUnTX_PF.AlteraVMI_CliVenda(Controle, Atual: Integer): Boolean;
begin

end;

function TUnTX_PF.AlteraVMI_FornecMO(Controle, Atual: Integer): Boolean;
begin

end;

procedure TUnTX_PF.AtualizaDescendentes(IMEI: Integer; Campos: array of String;
  Valores: array of Variant);
var
  Qry: TmySQLQuery;
  Controle, K: Integer;
  Itens: array of Integer;
  //
  function  NaoEstah(Ctrl: Integer): Boolean;
  var
    I: Integer;
  begin
    Result := True;
    for I := Low(Itens) to High(Itens) do
    begin
      if Itens[I] = Ctrl then
      begin
        Result := False;
        Exit;
      end;
    end;
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Controle ',
    'FROM ' + CO_SEL_TAB_TMI + ' ',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False,
      //UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB,
      Campos, ['Controle'], Valores, [Controle], True);
      //
      Qry.Next;
    end;
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
      if NaoEstah(Controle) then
      begin
        AtualizaDescendentes(Controle, Campos, Valores);
        //
        K := Length(Itens);
        SetLength(Itens, K + 1);
        Itens[K] := Controle;
      end;
      Qry.Next;
    end;
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnTX_PF.AtualizaDtHrFimOpe_MovimCod(MovimID: TEstqMovimID;
  MovimCod: Integer);
var
  Tabela, DtHrFimOpe: String;
  Qry1, Qry2: TmySQLQuery;
  Codigo: Integer;
  AtzGerado: Boolean;
  DataFimOpe: TDateTime;
  QtdGerMan, QtdGerSrc, QtdGerSdo: Double;
  QtdeDst: Double;
  MovimNiv: TEstqMovimNiv;
begin
  AdicionarNovosTX_emid();
  //
  AtzGerado := False;
  Tabela := ObtemNomeTabelaTXXxxCab(MovimID);
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT Codigo, DtHrFimOpe, QtdeMan, QtdeSrc, QtdeSdo ',
      'FROM ' + Tabela,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      '']);
      Codigo     := Qry1.FieldByName('Codigo').AsInteger;
      DataFimOpe := Qry1.FieldByName('DtHrFimOpe').AsDateTime;
      QtdGerMan  := Qry1.FieldByName('QtdeMan').AsInteger;
      QtdGerSrc  := Qry1.FieldByName('QtdeSrc').AsInteger;
      QtdGerSdo  := Qry1.FieldByName('QtdeSdo').AsInteger;
      //
      if (QtdGerSdo <= 0) and ((QtdGerMan <> 0) or (QtdGerSrc <> 0))
      then
      begin
        case MovimID of
          TEstqMovimID.emidEmIndstrlzc:
          //TEstqMovimID.emidEmProcWE,
          //TEstqMovimID.emidEmProcSP,
          //TEstqMovimID.emidEmReprRM:
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT MAX(DataHora) DataHora  ',
            'FROM ' + CO_SEL_TAB_TMI + ' ',
            'WHERE MovimCod=' + Geral.FF0(MovimCod),
            '']);
            DtHrFimOpe := Geral.FDT(Qry2.FieldByName(CO_DATA_HORA_TMI).AsDateTime, 109);
          end;
{
          TEstqMovimID.emidEmProcCur,
          TEstqMovimID.emidEmProcCal:
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT MAX(DataHora) DataHora  ',
            'FROM ' + CO_SEL_TAB_TMI + ' ',
            'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
            'AND SrcNivel1=' + Geral.FF0(Codigo),
            '']);
            DtHrFimOpe := Geral.FDT(Qry2.FieldByName(CO_DATA_HORA_TMI).AsDateTime, 109);
          end;
}          else
          begin
            DtHrFimOpe := '0000-00-00';
            Geral.MB_Erro(
              'MovimID n�o implementado em soma de "AtualizaDtHrFimOpe_MovimCod() (1)"');
          end;
        end;
      end else
        DtHrFimOpe := '0000-00-00';
      //
      QtdeDst  := 0;
      //
      case MovimID of
        TEstqMovimID.emidEmIndstrlzc:
{
        TEstqMovimID.emidEmProcWE,
        TEstqMovimID.emidEmProcSP,
        TEstqMovimID.emidEmReprRM:
}
        begin
          case MovimID of
            TEstqMovimID.emidEmIndstrlzc: MovimNiv := eminIndzcDst;
{
            TEstqMovimID.emidEmProcWE:   MovimNiv := eminDestWEnd;
            TEstqMovimID.emidEmProcSP:   MovimNiv := eminDestPSP;
            TEstqMovimID.emidEmReprRM:   MovimNiv := eminDestRRM;
}
            else MovimNiv := eminSemNiv;
          end;
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT  SUM(Qtde) Qtde ',
          'FROM ' + CO_SEL_TAB_TMI + ' tmi  ',
          'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod),
          'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
          '']);
          Qry2.First;
          while not Qry2.Eof do
          begin
            QtdeDst  := QtdeDst  + Qry2.FieldByName('Qtde').AsFloat;
            //
            Qry2.Next;
          end;
          AtzGerado := True;
        end;
        else
        begin
          Geral.MB_ERRO('MovimID n�o implementado em "AtualizaDtHrFimOpe_MovimCod()"');
          Exit;
        end;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
       'QtdeDst', 'DtHrFimOpe'
       ], ['MovimCod'], [
        QtdeDst, DtHrFimOpe
       ], [MovimCod], True) then
       begin
         //
       end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TUnTX_PF.AtualizaFornecedorInd(MovimCod: Integer);
begin
  DmModTX_CRC.AtualizaTXMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmIndstrlzc], [eminIndzcSrc], [eminIndzcInn, eminIndzcDst, eminIndzcBxa]);
end;

procedure TUnTX_PF.AtualizaFornecedorRRM(MovimCod: Integer);
begin
  DmModTX_CRC.AtualizaTXMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmProcSP], [eminSorcRRM], [eminEmRRMInn, eminDestRRM, eminEmRRMBxa]);
end;

procedure TUnTX_PF.AtualizaPalletPelosIMEIsDeGeracao(Pallet: Integer;
  QrSumTMI: TmySQLQuery);
var
  Qry: TmySQLQuery;
  TMI_Dest, TMI_Sorc, (*Pallet,*) Controle: Integer;
  Qtde, SdoVrtQtd: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    //Pallet := QrTXPalletCodigo.Value;
    //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT cia.TMI_Baix, cia.TMI_Dest, cia.TMI_Sorc, ',
      'SUM(cia.Qtde) Qtde',
      'FROM txcacitsa cia',
      //'LEFT JOIN v s m o v i t s tmi ON tmi.Controle=cia.TMI_Baix',
      //'LEFT JOIN v s m o v i t s vmd ON vmd.Controle=cia.TMI_Dest',
      'WHERE cia.TXPallet=' + Geral.FF0(Pallet),
      'GROUP BY cia.TMI_Baix, cia.TMI_Dest, cia.TMI_Sorc',
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando item ' +
        //Geral.FF0(Qry.RecNo) + ' de ' + Geral.FF0(Qry.RecordCount));
        //
        TMI_Dest := Qry.FieldByName('TMI_Dest').AsInteger;
        if TMI_Dest <> 0 then
        begin
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrSumTMI, Dmod.MyDB, [
          'SELECT SUM(Qtde) Qtde',
          'FROM txcacitsa ',
          'WHERE TMI_Dest=' + Geral.FF0(TMI_Dest),
          '']);
          Qtde  := QrSumTMI.FieldByName('Qtde').AsFloat;
          SdoVrtQtd := Qtde;
          Controle := TMI_Dest;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
          'Qtde', 'SdoVrtQtd'], ['Controle'], [
          Qtde, SdoVrtQtd], [Controle], True) then
          begin
            AtualizaSaldoIMEI(TMI_Dest, False);
            //
            TMI_Sorc := Qry.FieldByName('TMI_Sorc').AsInteger;
            if TMI_Sorc <> 0 then
              AtualizaSaldoIMEI(TMI_Sorc, False);
          end;
        end;
        //
        Qry.Next;
      end;
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnTX_PF.AtualizaSaldoIMEI(Controle: Integer; Gera: Boolean);
begin
  VAR_BxaVrtQtd := 0;
  VAR_SdoVrtQtd := 0;
  //
  AtualizaSaldoVirtualTXMovIts(Controle, Gera);
end;

procedure TUnTX_PF.AtualizaSaldoOrigemTMI_Dest(TMI_Dest, TMI_Sorc, TMI_Baix:
  Integer);
var
  Controle: Integer;
  Qtde, SdoVrtQtd: Double;
  Qry: TmySQLQUery;
begin
  Qry := TmySQLQUery.Create(Dmod);
  try
    // TMI_Baix
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde ',
    'FROM txcacitsa ',
    'WHERE TMI_Baix=' + Geral.FF0(TMI_Baix),
    '']);
    Qtde  := -Qry.FieldByName('Qtde').AsFloat;
    Controle := TMI_Baix;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
    'Qtde'], ['Controle'], [Qtde], [Controle], True);
    //
    // TMI_Dest
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde ',
    'FROM txcacitsa ',
    'WHERE TMI_Dest=' + Geral.FF0(TMI_Dest),
    '']);
    Qtde  := Qry.FieldByName('Qtde').AsFloat;
    SdoVrtQtd := Qtde;
    Controle := TMI_Dest;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
    'Qtde', 'SdoVrtQtd'], ['Controle'], [Qtde, SdoVrtQtd], [
    Controle], True) then
    begin
      AtualizaSaldoIMEI(TMI_Dest, False);
      //TMI_Sorc := Qry.FieldByName('TMI_Sorc').AsInteger;
      if TMI_Sorc <> 0 then
        AtualizaSaldoIMEI(TMI_Sorc, False);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnTX_PF.AtualizaSaldoVirtualTXMovIts(Controle: Integer;
  Gera: Boolean);
const
  sProcName = 'TUnTX_PF.AtualizaSaldoVirtualTXMovIts()';
var
  Qry, Qr2: TmySQLQuery;
  //
var
  InnQtde, OutQtde, SdoVrtQtd, QtdGer: Double;
  MovimID, MovimCod, MovimNiv, Codigo: Integer;
  PcEfetiv, PcClasRcl: Double;
  AvisaErro: Boolean;
begin
  //Geral.MB_Info('Em desenvolvimento: ' + sProcName);
  AdicionarNovosTX_emid();
  VAR_BxaVrtQtd := 0;
  VAR_SdoVrtQtd := 0;
  // Evitar erro!!??
  if Controle = 0 then
    Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  Qr2 := TmySQLQuery.Create(Dmod);
  try
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT tmi.Codigo, tmi.MovimID, tmi.MovimNiv, tmi.MovimCod, ',
    'tmi.GraGruX, tmi.Qtde, ',
    'tmi.SdoVrtQtd, tmi.Zerado ',
    'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
    'WHERE tmi.Controle=' + Geral.FF0(Controle),
    '']);
    //  Craido para resolver problemas de erro antigo do app.
    if Qry.FieldByName('Zerado').AsInteger = 1 then
    begin
      SdoVrtQtd := 0;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'SdoVrtQtd'], ['Controle'], [SdoVrtQtd], [Controle], True);
      //
      Exit;
    end;
    //
    InnQtde   := Qry.FieldByName('Qtde').AsFloat;
    //
    MovimID   := Qry.FieldByName('MovimID').AsInteger;
    MovimNiv  := Qry.FieldByName('MovimNiv').AsInteger;
    MovimCod  := Qry.FieldByName('MovimCod').AsInteger;
    //
    Codigo    := Qry.FieldByName('Codigo').AsInteger;
    //
    SdoVrtQtd := Qry.FieldByName('SdoVrtQtd').AsInteger;
(*
    case TEstqMovimNiv(MovimNiv ) of
      TEstqMovimNiv.eminSorcCurti V S : AvisaErro := True;
      else AvisaErro := False;
    end;
    if AvisaErro then
    begin
      Geral.MB_Aviso('ERRO! MovimNiv ' + Geral.FF0(MovimNiv) +
      ' n�o permite ter estoque gerenciado!' + sLineBreak +
      'Avise a DERMATEK!');
      Exit;
    end;
*)
    // Somente se for Entrada positiva!
    if InnQtde > 0 then
    begin
      case TEstqMovimID(MovimID) of
        (*01*)TEstqMovimID.emidCompra:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT SUM(tmi.Qtde) Qtde, ',
          'SUM(tmi.QtdGer) QtdGer ',
          'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
          'WHERE tmi.SrcNivel2=' + Geral.FF0(Controle),
          'AND (tmi.Qtde < 0 ',
          '  OR tmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_TX + ') ',
          ') ',
          '']);
          //Geral.MB_SQL(nil, Qry);
        end;
{POIU
        //(*01*)emidCompra       : Acima!!!;
        //(*02*)emidVenda        : Atualiza := False;
        //(*03*)emidReclasWE     : Atualiza := False;
        //(*04*)emidBaixa        : Atualiza := False;
        //(*05*)emidIndsWE       : Atualiza := False;
        (*06*)TEstqMovimID.emidIndsXX,
        //(*09*)emidForcado      : Atualiza := False;
        (*10*)emidSemOrigem,
        //(*11*)emidEmOperacao   : Abaixo!! Especifico
        //(*12*)emidResiduoReclas: Atualiza := False;
}
        (*13*)emidInventario:
        begin
          ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle);
        end;
{POIU
        (*07*)emidClassArtXXUni,
        (*08*)emidReclasXXUni,
        (*14*)emidClassArtXXMul,
        (*15*)emidPreReclasse,
}
        (*16*)emidEntradaPlC,
{POIU
        (*20*)emidFinished,
        (*21*)emidDevolucao,
        (*22*)emidRetrabalho,
        (*23*)emidGeraSubProd,
        (*24*)emidReclasXXMul,
}
        (*25*)emidTransfLoc:
{
        (*28*)emidDesclasse,
        (*29*)emidCaleado,
        (*30*)emidEmRibPDA,
        (*31*)emidEmRibDTA,
        (*36*)emidInnSemCob:
        //(*37*)emidOutSemCob
}
        begin
          ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle);
        end;
        (*38*)TEstqMovimID.emidEmIndstrlzc:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisTXIndCab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminIndzcInn)
          or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminIndzcDst) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        else
        begin
          Geral.MB_Aviso('"MovimID" sem atualiza��o de estoque!' + sLineBreak +
          'MovimID = ' + Geral.FF0(MovimID) + sLineBreak +
          'AVISE A DERMATEK COM URG�NCIA!!!' + sLineBreak + sProcName);
          Exit;
        end;
      end;
      OutQtde := Qry.FieldByName('Qtde').AsFloat;
      QtdGer  := -Qry.FieldByName('QtdGer').AsFloat;
      //
      if Qr2.State <> dsInactive then
      begin
        OutQtde := OutQtde + Qr2.FieldByName('Qtde').AsFloat;
        QtdGer  := QtdGer  - Qr2.FieldByName('QtdGer').AsFloat;
      end;
      SdoVrtQtd := InnQtde  + OutQtde;
      //
      if Gera then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
        'SdoVrtQtd', 'QtdGer'], ['Controle'], [
        SdoVrtQtd, QtdGer], [Controle], True);
      end else
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
        'SdoVrtQtd'], ['Controle'], [
        SdoVrtQtd], [Controle], True);
    end else
    begin
      SdoVrtQtd := 0;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'SdoVrtQtd'], ['Controle'], [
      SdoVrtQtd], [Controle], True);
    end;
    VAR_BxaVrtQtd := OutQtde;
    VAR_SdoVrtQtd := SdoVrtQtd;
  finally
    Qr2.Free;
  end;
  finally
    Qry.Free;
  end;
end;

procedure TUnTX_PF.AtualizaSaldoVirtualTXMovIts_Generico(Controle, MovimID,
  MovimNiv: Integer);
const
  Gera = False;
  Sim = 1;
  Nao = 2;
  Zer = 0;
var
  Atualiza: Integer;
begin
  VAR_BxaVrtQtd := 0;
  //
  VAR_SdoVrtQtd := 0;
  //
  Atualiza := Sim;
  //
  AdicionarNovosTX_emid();
  case TEstqMovimID(MovimID) of
    (*00*)emidAjuste        : Atualiza := Nao;
    (*01*)emidCompra        : Atualiza := Sim;
    (*02*)emidVenda         : Atualiza := Nao;
{
    (*03*)emidReclasWE      : Atualiza := Nao;
    (*04*)emidBaixa         : Atualiza := Nao;
    (*05*)emidIndsWE        : Atualiza := Nao;
    (*06*)emidIndsXX        :
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminDestCurtiXX: Atualiza := Sim;
        eminSorcCurtiXX: Atualiza := Zer;
        eminBaixCurtiXX: Atualiza := Zer;
      end;
    end;
    (*07*)emidClassArtXXUni : Atualiza := Sim;
    (*08*)emidReclasXXUni   : Atualiza := Sim;
    (*09*)emidForcado       : Atualiza := Nao;
    (*10*)emidSemOrigem     : Atualiza := Nao; // deprecado! Sim;
    (*11*)emidEmOperacao    : Atualiza := Sim;
    (*12*)emidResiduoReclas : Atualiza := Nao;
}
    (*13*)emidInventario    : Atualiza := Sim;
{
    (*14*)emidClassArtXXMul : Atualiza := Sim;
    (*15*)emidPreReclasse   : Atualiza := Sim;
    (*16*)emidEntradaPlC    : Atualiza := Sim;
    (*17*)emidExtraBxa      : Atualiza := Nao;
    (*18*)emidSaldoAnterior : Atualiza := Nao;
    (*19*)emidEmProcWE      : Atualiza := Sim;
    (*20*)emidFinished      : Atualiza := Sim;
    (*21*)emidDevolucao     : Atualiza := Sim;
    (*22*)emidRetrabalho    : Atualiza := Sim;
    (*23*)emidGeraSubProd   : Atualiza := Sim;
    (*24*)emidReclasXXMul   : Atualiza := Sim;
}
    (*25*)emidTransfLoc     : Atualiza := Sim;
{
    (*26*)emidEmProcCal     : Atualiza := Sim;
    (*27*)emidEmProcCur     : Atualiza := Sim;
    (*28*)emidDesclasse     : Atualiza := Sim;
    (*29*)emidCaleado       : Atualiza := Sim;
    (*30*)emidEMRibPDA      : Atualiza := Sim;
    (*31*)emidEmRibDTA      : Atualiza := Sim;
    (*32*)emidEmProcSP      : Atualiza := Sim;
    (*33*)emidEmReprRM      : Atualiza := Sim;
    (*34*)emidCurtido       : Atualiza := Nao; // N�o existe lancto fisico
    //35*)emidMixInsum      : Atualiza := Nao;
    (*36*)emidInnSemCob     : Atualiza := Sim;
    (*37*)emidOutSemCob     : Atualiza := Nao;
}
    (*38*)emidEmIndstrlzc   : Atualiza := Sim;
    else begin
      Geral.MB_Erro(
      '"MovimID" n�o definido em "AtualizaSaldoVirtualTXMovIts_Generico()"'
      + 'Avise a DERMATEK!');
      Atualiza := Nao;
    end;
  end;
  case Atualiza of
    Zer: ZeraSaldoIMEI(Controle);
    Sim: AtualizaSaldoIMEI(Controle, Gera);
    Nao: ; // Nada
  end;
end;

procedure TUnTX_PF.AtualizaStatPall(Pallet: Integer);
var
  Qry: TmySQLQuery;
  StatPall: Integer;
begin
  StatPall := Integer(TXXStatPall.xxspIndefinido);
  Qry := TmySQLQuery.Create(Dmod);
  try
    //  Desmontando!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM txparclcaba ',
    'WHERE TXPallet=' + Geral.FF0(Pallet),
    'AND DtHrFimCla< "1900-01-01" ',
    '']);
     if Qry.RecordCount > 0 then
       StatPall := StatPall + Integer(TXXStatPall.xxspDesmontando);  // 1
       // Montando!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM txpaclaitsa ',
    'WHERE TXPallet=' + Geral.FF0(Pallet),
    'AND DtHrFim< "1900-01-01" ',
    ' ',
    'UNION ',
    ' ',
    'SELECT Controle ',
    'FROM txparclitsa ',
    'WHERE TXPallet=' + Geral.FF0(Pallet),
    'AND DtHrFim< "1900-01-01" ',
    ' ']);
     if Qry.RecordCount > 0 then
       StatPall := StatPall + Integer(TXXStatPall.xxspMontando); // 2 ou 3
    //
    //if StatPall = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DtHrEndAdd ',
      'FROM txpalleta ',
      'WHERE Codigo=' + Geral.FF0(Pallet),
      '']);
      if Qry.FieldByName('DtHrEndAdd').AsDateTime > 2 then
        StatPall := StatPall + Integer(TXXStatPall.xxspEncerrado) // 4 ou 5 ou 6 ou 7
      else if StatPall = 0 then
        StatPall := StatPall + Integer(TXXStatPall.xxspRemovido); // 8 ou ... ver no futuro?
    end;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txpalleta', False, [
    'StatPall'], ['Codigo'], [StatPall], [Pallet], True);
  finally
    Qry.Free;
  end;
end;

function TUnTX_PF.AtualizaTMIsDeBox(TXPallet, TMI_Dest, TMI_Baix,
  TMI_Sorc: Integer; QrSumDest, QrSumSorc, QrTMISorc,
  QrPalSorc: TmySQLQuery): Boolean;
const
  AptoUso = 1;
var
  QtdeBxa, QtdeDst, ValorT, CustoUni: Double;
  TMI, TMIs, Marca: String;
  Terceiro, Controle: Integer;
begin
  Result := False;
  //                                                                                                          f
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumDest, Dmod.MyDB, [
  'SELECT TMI_Baix, SUM(Qtde) Qtde, ',
  'SUM(Qtde / FatorIntSrc * FatorIntDst) QtBxa  ',
  'FROM txcacitsa ',
  'WHERE TMI_Dest=' + Geral.FF0(TMI_Dest),
  'GROUP BY TMI_Sorc ',
  '']);
  if QrSumDest.RecordCount > 1 then
  begin
    Geral.MB_Erro('ERRO! Avise a DERMATEK' + sLineBreak +
    '"TUnEncerraPallet()" > Mais de um TMI_Baix por TMI_Dest!' + sLineBreak +
    QrSumDest.SQL.Text);
    Exit;
  end;
  TMI_Baix := QrSumDest.FieldByName('TMI_Baix').AsInteger;
  QtdeBxa   := QrSumDest.FieldByName('QtBxa').AsFloat;
  QtdeDst   := QrSumDest.FieldByName('Qtde').AsFloat;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumSorc, Dmod.MyDB, [
  'SELECT TMI_Sorc, SUM(Qtde) Qtde ',
  'FROM txcacitsa ',
  'WHERE TXPallet=' + Geral.FF0(TXPallet),
  'AND TMI_Dest=' + Geral.FF0(TMI_Dest),
  'GROUP BY TMI_Sorc ',
  '']);
  //fazer query com sum do TMI_Sorc e usar no lugar do QrTXGerArtNew !
  ValorT := 0;
  TMIs   := '';
  QrSumSorc.First;
  while not QrSumSorc.Eof do
  begin
    TMI := Geral.FF0(QrSumSorc.FieldByName('TMI_Sorc').AsInteger);
    if TMIs <> '' then
      TMIs := TMIs + ', ';
    TMIS := TMIs + TMI;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrTMISorc, Dmod.MyDB, [
    'SELECT IF(Qtde=0, 0, ValorT / Qtde) CustoUni ',
    'FROM ' + CO_SEL_TAB_TMI + ' ',
    'WHERE Controle=' + TMI,
    '']);
    //
    CustoUni := QrTMISorc.FieldByName('CustoUni').AsFloat;
    ValorT   := ValorT + (QtdeDst  * CustoUni);
    //
    QrSumSorc.Next;
  end;
  //
  // S� se tiver movimento no cacits! Quando n�o tem fica texto vazio!!!!
  if TMI <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPalSorc, Dmod.MyDB, [
    'SELECT ',
    'IF(COUNT(DISTINCT tmi.Terceiro) <> 1, 0.000, tmi.Terceiro) Terceiro, ',
    'IF(COUNT(DISTINCT tmi.Marca) <> 1, "", tmi.Marca) Marca ',
    'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
    'WHERE tmi.Controle IN (' + TMIs + ')',
    '']);
    Terceiro := Trunc(QrPalSorc.FieldByName('Terceiro').AsFloat);
    Marca    := QrPalSorc.FieldByName('Marca').AsString;
  end else
  begin
    Terceiro := 0;
    Marca    := '';
  end;
  //Pallet   := 0;
  //
  Controle := TMI_Baix;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
  'Terceiro', 'Qtde', 'ValorT',
  'AptoUso', 'Marca'], [
  'Controle'], [
  Terceiro, -QtdeBxa, -ValorT,
  AptoUso, Marca], [
  Controle], True) then
  begin
    Controle := TMI_Dest;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
    'Terceiro', 'Qtde', 'ValorT',
    'AptoUso', 'Marca'], [
    'Controle'], [
    Terceiro, QtdeDst, ValorT,
    AptoUso, Marca], [
    Controle], True) then
    begin
      AtualizaSaldoIMEI(TMI_Dest, False);
      //
      AtualizaSaldoIMEI(TMI_Sorc, False);
    end;
  end;
  //
  Result := True;
end;

procedure TUnTX_PF.AtualizaTotaisTXIndCab(MovimCod: Integer);
const
  sProcName = 'TUnTX_PF.AtualizaTotaisTXIndCab()';
var
  Qry: TmySQLQuery;
  QtdeSrc, ValorTSrc, QtdeInn,  ValorTInn, QtdeDst, ValorTDst, QtdeBxa,
  ValorTBxa, QtdeSdo, ValorTSdo, CusFrtMOEnv, CusFrtMORet, CustoMOTot, ValorMP,
  ValorT: Double;
  //Controle, PedItsLib: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde, SUM(CusFrtMOEnv) CusFrtMOEnv, ',
    'SUM(CusFrtMORet) CusFrtMORet, SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminIndzcSrc)),  // 61
    '']);
    //Geral.MB_SQL(nil, Qry);
    QtdeSrc           := Qry.FieldByName('Qtde').AsFloat;
    CusFrtMOEnv       := Qry.FieldByName('CusFrtMOEnv').AsFloat;
    ValorTSrc         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde, SUM(CusFrtMOEnv) CusFrtMOEnv, ',
    'SUM(CusFrtMORet) CusFrtMORet, SUM(CustoMOTot) CustoMOTot, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminIndzcDst)),  //63
    '']);
    //Geral.MB_SQL(nil, Qry);
    QtdeDst           := Qry.FieldByName('Qtde').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    CusFrtMORet       := Qry.FieldByName('CusFrtMORet').AsFloat;
    CustoMOTot        := Qry.FieldByName('CustoMOTot').AsFloat;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
(*
    'SELECT Controle, PedItsLib ',
*)
    'SELECT SUM(Qtde) Qtde, SUM(CusFrtMOEnv) CusFrtMOEnv, ',
    'SUM(CusFrtMORet) CusFrtMORet, SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminIndzcInn)),  //62
    '']);
    QtdeInn           := Qry.FieldByName('Qtde').AsFloat;
    ValorTInn         := Qry.FieldByName('ValorT').AsFloat;
    //Controle  := Qry.FieldByName('Controle').AsInteger;
    //PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde, SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminIndzcBxa)),  //64
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_TX + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_TMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_SQL(nil, Qry);
    QtdeBxa           := Qry.FieldByName('Qtde').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
(*
    QtdeSdo           := - QtdeSrc   + QtdeBxa;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
*)
    QtdeSdo           := QtdeInn   + QtdeBxa;
    ValorTSdo         := ValorTInn + ValorTBxa;
    if QtdeSdo <=0 then
    Geral.MB_Info('Ver ser Saldo estah calculando certo!');
    //
////////////////////////////////////////////////////////////////////////////////
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txindcab', False, [
    'QtdeSrc', 'ValorTSrc',
    'QtdeInn', 'ValorTInn',
    'QtdeDst', 'ValorTDst',
    'QtdeBxa', 'ValorTBxa',
    'QtdeSdo', 'ValorTSdo'
    ], [
    'MovimCod'], [
    QtdeSrc,  ValorTSrc,
    QtdeInn,  ValorTInn,
    QtdeDst,  ValorTDst,
    QtdeBxa,  ValorTBxa,
    QtdeSdo,  ValorTSdo
    ], [
    MovimCod], True) then
    begin
{POIU // como fazer com m�ltiplos Inn ???
      ValorMP := -ValorTSrc;
      ValorT  := ValorMP + CustoMOTot + CusFrtMOEnv + CusFrtMORet;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Qtde', 'CusFrtMOEnv', 'CusFrtMORet',
      'ValorMP', 'ValorT', 'SdoVrtQtd'], [
      'Controle'], [
      -QtdeSrc, CusFrtMOEnv, CusFrtMORet,
      ValorMP, ValorT, QtdeSdo], [
      Controle], True) then
}
    end;
{POIU // como fazer com m�ltiplos Inn ???
    AtualizaTXPedIts_Lib(PedItsLib, Controle, -QtdeSrc);
}
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnTX_PF.AtualizaTotaisTXRRMCab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  QtdeSrc, ValorTSrc, QtdeDst, ValorTDst, QtdeBxa, ValorTBxa,
  QtdeSdo, ValorTSdo: Double;
  Controle, PedItsLib: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde, SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcRRM)),
    '']);
    QtdeSrc           := Qry.FieldByName('Qtde').AsFloat;
    ValorTSrc         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde, SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestRRM)),
    '']);
    QtdeDst           := Qry.FieldByName('Qtde').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde, SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmRRMBxa)),
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_TX + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_TMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_SQL(nil, Qry);
    QtdeBxa           := Qry.FieldByName('Qtde').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
    QtdeSdo           := - QtdeSrc  + QtdeBxa;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmRRMInn)),
    '']);
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
(*  Duplica baixa forcada!!!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(tmi.Qtde) Qtde, ',
    'SUM(tmi.QtdGerQtd) QtdGerQtd, ',
    'FROM t x m o v i t s tmi ',
    'LEFT JOIN txribcad vnc ON vnc.GraGruX=tmi.DstGGX',
    'WHERE tmi.SrcNivel2=' + Geral.FF0(Controle),
    'AND tmi.Pecas < 0 ',
    '']);
    QtdeSdo   := QtdeSdo  + Qry.FieldByName('Qtde').AsFloat;
*)
////////////////////////////////////////////////////////////////////////////////
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txrrmcab', False, [
    'QtdeSrc', 'ValorTSrc',
    'QtdeDst', 'ValorTDst',
    'QtdeBxa', 'ValorTBxa',
    'QtdeSdo', 'ValorTSdo'
    ], [
    'MovimCod'], [
    QtdeSrc, ValorTSrc,
    QtdeDst, ValorTDst,
    QtdeBxa, ValorTBxa,
    QtdeSdo, ValorTSdo
    ], [
    MovimCod], True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
      'Qtde', 'ValorT', 'SdoVrtQtd'], [
      'Controle'], [
      -QtdeSrc, -ValorTSrc, QtdeSdo], [
      Controle], True) then
    end;
    AtualizaTXPedIts_Lib(PedItsLib, Controle, -QtdeSrc);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnTX_PF.AtualizaTotaisTXXxxCab(Tabela: String; MovimCod: Integer);
const
  sProcName = 'TUnTX_PF.AtualizaTotaisTXXxxCab()';
var
  Qry: TmySQLQuery;
  Qtde, ValorT: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_TMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    //
    'AND (NOT MovimNiv IN (' +
    Geral.FF0(Integer(TEstqMovimNiv.eminSorcClass)) + ',' + // 1 - // Classifica��o (m�ltipla)
    Geral.FF0(Integer(TEstqMovimNiv.eminSorcLocal)) +       // 27  // Transferencia de local
    '))', // Classifica��o (m�ltipla)
    '']);
    //
    Qtde   := Qry.FieldByName('Qtde').AsFloat;
    ValorT := Qry.FieldByName('ValorT').AsFloat;

    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, LowerCase(Tabela), False, [
    'Qtde', 'ValorT'], ['MovimCod'], [
    Qtde, ValorT], [MovimCod], True);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnTX_PF.AtualizaTXPedIts_Fin(TXPedIts: Integer);
var
  Qry: TmySQLQuery;
  Controle: Integer;
  FinQtde: Double;
begin
{$IfDef sAllTX}
  if TXPedIts = 0 then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Qtde) Qtde ',
    'FROM ' + CO_SEL_TAB_TMI + ' ',
    'WHERE PedItsFin=' + Geral.FF0(TXPedIts),
    '']);
    FinQtde   := Qry.FieldByName('Qtde').AsFloat;
    Controle := TXPedIts;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txpedits', False, [
    'FinQtde'], ['Controle'], [FinQtde], [Controle], True);
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TUnTX_PF.AtualizaTXPedIts_Lib(TXPedIts, TXMovIts: Integer;
  LibQtde: Double);
var
  Controle: Integer;
begin
  if TXPedIts = 0 then
    Exit;
  Controle := TXPedIts;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'txpedits', False, [
  CO_FLD_TAB_TMI, 'LibQtde'], ['Controle'], [
  TXMovIts, LibQtde], [Controle], True);
end;

function TUnTX_PF.CadastraPalletInfo(Empresa, ClientMO, GraGruX,
  GraGruY: Integer; QrTXPallet: TmySQLQuery; EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; SBNewPallet: TSpeedButton;
  EdQtde: TdmkEdit): Integer;
var
  Nome, DtHrEndAdd: String;
  Status, CliStat, QtdPrevPc: Integer;
  PodePallet: Boolean;
  MovimIDGer: TEstqMovimID;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  Result         := -1;
  Nome           := '';
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  DtHrEndAdd     := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimIDGer     := TEstqMovimID.emidAjuste;
  QtdPrevPc      := 0;
  //
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, nil, 'Defina o Artigo!') then Exit;
  //
  PodePallet := GraGruY > CO_GraGruY_1024_TXCadNat;
  //
  if MyObjects.FIC(not PodePallet, nil,
  'Somente artigo intermedi�rio ou acabado permite informa��o de pallet!') then
    Exit;
  //
  Result := CadastraPalletRibCla(Empresa, ClientMO, EdPallet, CBPallet,
  QrTXPallet, MovimIDGer, GraGruX);
  begin
    AtualizaStatPall(Result);
    //ReopenTXPallet();
    UnDmkDAC_PF.AbreQuery(QrTXPallet, Dmod.MyDB);
    EdPallet.ValueVariant := Result;
    CBPallet.KeyValue := Result;
    //
    if SBNewPallet <> nil then
      SBNewPallet.Enabled := False;
    EdQtde.SetFocus;
  end;
end;

function TUnTX_PF.CadastraPalletRibCla(Empresa, ClientMO: Integer;
  EdPallet: TdmkEditCB; CBPallet: TdmkDBLookupComboBox; QrTXPallet: TmySQLQuery;
  MovimIDGer: TEstqMovimID; GraGruX: Integer): Integer;
var
  Habilita: Boolean;
  PalletNew: Integer;
begin
  Result := 0;
  if VAR_TXInsPalManu(*Dmod.QrControleTXInsPalManu.Value*) = 1 then
  begin
    if DBCheck.CriaFm(TFmTXPalletManual, FmTXPalletManual, afmoNegarComAviso) then
    begin
      FmTXPalletManual.ImgTipo.SQLType := stIns;
      if (EdPallet <> nil) then
      begin
        PalletNew := EdPallet.ValueVariant + 1;
        if PalletNew > 1 then
        begin
          FmTXPalletManual.EdPallet.ValueVariant := PalletNew;
          FmTXPalletManual.EdPallet.ValueVariant := PalletNew;
        end;
      end;
      FmTXPalletManual.ShowModal;
      if FmTXPalletManual.FPermiteIncluir then
        Result := FmTXPalletManual.FNumNewPallet;
      FmTXPalletManual.Destroy;
      if Result = 0 then
        Exit;
    end;
  end;
  //
  if DBCheck.CriaFm(TFmTXPalletAdd, FmTXPalletAdd, afmoNegarComAviso) then
  begin
    FmTXPalletAdd.ImgTipo.SQLType := stIns;
    FmTXPalletAdd.FNewPallet := Result;
    //
    FmTXPalletAdd.EdCodigo.ValueVariant := Result;
    FmTXPalletAdd.EdEmpresa.ValueVariant := DModG.ObtemFilialDeEntidade(Empresa);
    //Habilita := FmTXPalletAdd.EdEmpresa.ValueVariant = 0;
    FmTXPalletAdd.EdClientMO.ValueVariant := ClientMO;
    FmTXPalletAdd.CBClientMO.KeyValue     := ClientMO;
    Habilita := True;
    FmTXPalletAdd.EdEmpresa.Enabled := Habilita;
    FmTXPalletAdd.CBEmpresa.Enabled := Habilita;
    FmTXPalletAdd.EdClientMO.Enabled := Habilita;
    FmTXPalletAdd.CBClientMO.Enabled := Habilita;
    FmTXPalletAdd.EdStatus.ValueVariant := 0;//CO_STAT_TXPALLET_0100_DISPONIVEL;
    FmTXPalletAdd.CBStatus.KeyValue     := 0;//CO_STAT_TXPALLET_0100_DISPONIVEL;
    FmTXPalletAdd.FMovimIDGer := MovimIDGer;
    if GraGruX <> 0 then
    begin
      FmTXPalletAdd.EdGraGruX.ValueVariant := GraGruX;
      FmTXPalletAdd.CBGraGruX.KeyValue     := GraGruX;
    end;
    //
    FmTXPalletAdd.ShowModal;
    if FmTXPalletAdd.FPallet <> 0 then
    begin
      if (QrTXPallet <> nil) and (QrTXPallet.State <> dsInactive) then
      begin
        UnDmkDAC_PF.AbreQuery(QrTXPallet, Dmod.MyDB);
        if QrTXPallet.Locate('Codigo', FmTXPalletAdd.FPallet, []) then
        begin
          if EdPallet <> nil then
            EdPallet.ValueVariant := FmTXPalletAdd.FPallet;
          if CBPallet <> nil then
          begin
            CBPallet.KeyValue := FmTXPalletAdd.FPallet;
            try
              CBPallet.SetFocus;
            except
              // nada!
            end;
          end;
        end;
      end;
    end else
    begin
      if (QrTXPallet <> nil) and (QrTXPallet.State <> dsInactive) then
        UnDmkDAC_PF.AbreQuery(QrTXPallet, Dmod.MyDB);
    end;
    Result := FmTXPalletAdd.FPallet;
    FmTXPalletAdd.Destroy;
  end;
end;

function TUnTX_PF.CalculoValorOrigemUni(Qtde, CustoPQ, ValorMP,
  CusFrtMOEnv: Double): Double;
begin
  if Qtde = 0 then
    Result := 0
  else
    Result := (CustoPQ + ValorMP + CusFrtMOEnv) / Qtde;
end;

function TUnTX_PF.ComparaUnidMed2GGX(GGX1, GGX2: Integer): Boolean;
var
  Qry: TmySQLQuery;
  UM1, UM2: Integer;
  function AbreQry(GGX: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT gg1.UnidMed ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE ggx.Controle=' + Geral.FF0(GGX),
    '']);
    Result := Qry.FieldByName('UnidMed').AsInteger;
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UM1 := AbreQry(GGX1);
    UM2 := AbreQry(GGX2);
    Result := UM1 = UM2;
    //
    if not Result then
      Geral.MB_Aviso('Os reduzidos ' + Geral.FF0(GGX1) + ' e ' +
      Geral.FF0(GGX2) + ' n�o tem a mesma unidade de medida!');
  finally
    Qry.Free;
  end;
end;

function TUnTX_PF.ConfigContinuarInserindoFolhaLinha(
  RGModoContinuarInserindo: TRadioGroup; EdFolha, EdLinha: TdmkEdit): Boolean;
begin
  if RGModoContinuarInserindo.ItemIndex = 2 then
  begin
    Result := True;
    MyObjects.IncrementaFolhaLinha(EdFolha, EdLinha, CO_TX_MaxLin);
  end else
    Result := False;
end;

procedure TUnTX_PF.DefineDataHoraOuDtCorrApoCompos(const DataHora,
  DtCorrApo: TDateTime; TPData: TdmkEditDateTimePicker; EdHora: TdmkEdit);
var
  DtHr: TDateTime;
begin
  if DtCorrApo > 2 then
    DtHr := DtCorrApo
  else
    DtHr := DataHora;
  //
  TPData.Date               := Int(DtHr);
  EdHora.ValueVariant       := DtHr;
end;

procedure TUnTX_PF.DefineDataHoraOuDtCorrApoCompos(const DataHora,
  DtCorrApo: TDateTime; var _DataHora: TDateTime);
begin
  if DtCorrApo > 2 then
    _DataHora := DtCorrApo
  else
    _DataHora := DataHora;
end;

function TUnTX_PF.DefineDatasTMI(const _Data_Hora_: String; var DataHora,
  DtCorrApo: String): Boolean;
var
  Agora, Data, DtCA: TDateTime;
  (*GerLibEFD*)MovimXX, PeriodoData, PeriodoNow, Ano, Mes: Integer;
begin
  Result    := False;
  DtCorrApo := '0000-00-00 00:00:00';
  DataHora  := _Data_Hora_;
  {$IFDEF sAllTX}
  Data      := Geral.ValidaDataHoraSQL(_Data_Hora_);
  //
  if ImpedePeloBalanco(Data, True, True) then
    Exit;
  //
  Agora       := DModG.ObtemAgora();
  PeriodoData := Geral.Periodo2000(Data);
  PeriodoNow  := Geral.Periodo2000(Agora);
  //
  if PeriodoData < PeriodoNow then
  begin
    //GerLibEFD := EfdIcmsIpi_PF.SPEDEFDEnce_Periodo('GerLibEFD');
    //if PeriodoData < GerLibEFD then
    MovimXX := EfdIcmsIpi_PF.SPEDEFDEnce_Periodo('MovimXX');
    //
    if MovimXX < -23900 then
    begin
      if VAR_AVISOS_SPEDEFDEnce_Periodo < 3 then
      begin
        VAR_AVISOS_SPEDEFDEnce_Periodo := VAR_AVISOS_SPEDEFDEnce_Periodo + 1;
        //
        Geral.MB_Aviso('N�o h� per�odo de encerramento at� o momento!' +
          sLineBreak +
          'O(s) lan�amento(s) N�O ser�(�o) considerado(s) como apontamento(s)!');
      end;
      Result := True;
      Exit;
    end;
    if PeriodoData <= MovimXX then
    begin
      if MovimXX - PeriodoData >= 2 then // So pode 2 inventarios pelo sped!
      begin
        Geral.MB_Aviso('Per�odo de corre��o de apontamento j� encerrado!');
        Exit;
      end;
      if not InverteDatas(MovimXX, Data, Agora, DataHora, DtCorrApo) then
        Exit
      else
        Result := True;
    end else
      Result := True;
  end else
    Result := True;
  {$ELSE}
    Result := True;
  {$ENDIF}
end;

function TUnTX_PF.DefineIDs_Str(DBGridZTO: TdmkDBGridZTO; Query: TmySQLQuery;
  Campo: String): String;
var
  I: Integer;
begin
  Result := '';
  if (DBGridZTO.SelectedRows.Count > 0) and
  (DBGridZTO.SelectedRows.Count < Query.RecordCount) then
  begin
    for I := 0 to DBGridZTO.SelectedRows.Count - 1 do
    begin
      //Query.GotoBookmark(pointer(DBGridZTO.SelectedRows.Items[I]));
      Query.GotoBookmark(DBGridZTO.SelectedRows.Items[I]);
      //
      if Result <> '' then
        Result := Result + ',';
      Result := Result + Geral.FF0(Query.FieldByName(Campo).AsInteger);
    end;
  end;
end;

function TUnTX_PF.DefineSiglaTX_Frn(Fornece: Integer): String;
var
  Qry: TmySQLQuery;
  SiglaTX: String;
  SQLType: TSQLType;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    SiglaTX := '';
    UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, SiglaTX ',
    'FROM txentimp ',
    'WHERE Codigo=' + Geral.FF0(Fornece),
    '']);
    if Qry.RecordCount > 0 then
    begin
      SiglaTX := Qry.FieldByName('SiglaTX').AsString;
      if Trim(SiglaTX) <> '' then
      begin
        Result := SiglaTX;
        Exit;
      end else
        SQLType := stUpd;
    end else
    begin
      SQLType := stIns;
    end;
    if Trim(SiglaTX) = '' then
    begin
    UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
      'SELECT IF(Tipo=0, RazaoSocial, Nome) NO_ENT ',
      'FROM entidades ',
      'WHERE Codigo=' + Geral.FF0(Fornece),
      '']);
      SiglaTX := Qry.FieldByName('NO_ENT').AsString;
      InputQuery('Informe a Sigla do Fornecedor de Mat�ria Prima',
      'Sigla (m�x 10 caracteres):', SiglaTX);
    end;
    //
    if Trim(SiglaTX) = '' then
      SiglaTX := 'F' + Geral.FF0(Fornece);
    //
    SiglaTX := Copy(SiglaTX, 1, 10);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'txentimp', False, [
    'SiglaTX'
    ], ['Codigo'], [
    SiglaTX
    ], [Fornece], True) then
      Result := SiglaTX;
  finally
    Qry.Free;
  end;
end;

function TUnTX_PF.EditaIxx(DGDados: TDBGrid; QrTXMovIts: TmySQLQuery): Boolean;
var
  Campo, Texto: String;
  Controle, Inteiro: Integer;
begin
  Result := False;
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  if (Campo = 'IxxFolha') or (Campo = 'IxxLinha') then
  begin
    Inteiro := QrTXMovIts.FieldByName(Campo).AsInteger;
    Texto := Geral.FF0(Inteiro);
    if InputQuery('Novo valor para "' + Campo + '"', 'Informe o novo valor:',
    Texto) then
    begin
      Inteiro := Geral.IMV(Texto);
      Controle := QrTXMovIts.FieldByName('Controle').AsInteger;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_TMI, False, [
      Campo], ['Controle'], [Inteiro], [Controle], True) then
      begin
        UnDmkDAC_PF.AbreQuery(QrTXMovIts, Dmod.MyDB);
        QrTXMovIts.Locate('Controle', Controle, []);
        //PCItens.ActivePageIndex := 1;
      end
    end;
  end;
end;

function TUnTX_PF.EncerraPalletNew(const Pallet: Integer;
  const Pergunta: Boolean): Boolean;
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
(*
    if DBCheck.CriaFm(TDfTXMod, DfTXMod, afmoSemVerificar) then
    begin
      //DfTXMod.ShowModal;
      Result := DfTXMod.EncerraPallet(Pallet, Pergunta);
      //
      DfTXMod.Destroy;
    end;
*)
    Result := DmModTX_CRC.EncerraPallet(Pallet, Pergunta);
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TUnTX_PF.EncerraPalletSimples(Pallet, Empresa, ClientMO: Integer;
  QrTXPallet: TmySQLQuery; PallOnEdit: array of Integer);
const
  EncerrandoTodos = False;
  FromRcl  = False;
  Pergunta = True;
  Encerra  = True;
  OC       = 0;
var
  FromBox: Variant;
begin
  FromBox := Null;
  EncerraPalletNew(Pallet, Pergunta);
  ReopenTXPallet(QrTXPallet, Empresa, ClientMO, 0, '', PallOnEdit);
end;

function TUnTX_PF.ExcluiControleTXMovIts(Tabela: TmySQLQuery;
  Campo: TIntegerField; Controle1, CtrlBaix, SrcNivel2: Integer; Gera: Boolean;
  Motivo: Integer; Pergunta, Reabre: Boolean): Boolean;
var
  Continua: Boolean;
  Prox: Integer;
  Qry: TmySQLQuery;
  Texto: String;
begin
  Continua := False;
  Qry      := TmySQLQuery.Create(Dmod);
  try
    if Pergunta = True then
    begin
      if CtrlBaix <> 0 then
        Texto := 'IME-I de baixa a ser exclu�do: ' + Geral.FF0(CtrlBaix) + sLineBreak
      else
        Texto := '';
      //
      Texto := 'Confirma a exclus�o do item selecionado?' + sLineBreak +
        'IME-I a ser exclu�do: ' + Geral.FF0(Controle1) + sLineBreak +
        Texto +
        'IME-I a receber o estoque de volta: ' + Geral.FF0(SrcNivel2) +
        sLineBreak + '';
      //
      if Geral.MB_Pergunta(Texto) = ID_YES then
        Continua := True;
    end else
      Continua := True;
    //
    if Continua = True then
    begin
      if ExcluiTXMovIts_EnviaArquivoExclu(Controle1, Motivo, Dmod.QrUpd,
      Dmod.MyDB, CO_MASTER) then
      begin
        if CtrlBaix <> 0 then
          ExcluiTXMovIts_EnviaArquivoExclu(CtrlBaix, Motivo, Dmod.QrUpd,
            Dmod.MyDB, CO_MASTER);
        AtualizaSaldoIMEI(SrcNivel2, Gera);
        //
        if (Tabela <> nil) and (Reabre = True) then
        begin
          Prox := GOTOy.LocalizaPriorNextIntQr(Tabela, Campo, Controle1);
          Tabela.Close;
          UnDmkDAC_PF.AbreQuery(Tabela, Dmod.MyDB);
          Tabela.Locate('Controle', Prox, []);
        end;
        Result := True;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnTX_PF.ExcluiTXMovIts_EnviaArquivoExclu(Controle, Motivo: Integer;
  Query: TmySQLQuery; DataBase: TmySQLDatabase; Senha: String): Boolean;
const
  TabAtivo = CO_DEL_TAB_TMI;
  TabExclu = 'txmovitz';
var
  CamposZ, Dta: String;
  QryIMEC: TmySQLQuery;
  SrcNivel2, DstNivel2: Integer;
  QtdFlds: Integer;
begin
  if Senha <> CO_MASTER then
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  //
  CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabExclu, '', QtdFlds);
  CamposZ := Geral.Substitui(CamposZ,
    ', DataDel', ', "' + Dta + '" DataDel');
  CamposZ := Geral.Substitui(CamposZ,
    ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
  CamposZ := Geral.Substitui(CamposZ,
    ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
  CamposZ := Geral.Substitui(CamposZ,
    ', AWStatSinc', ', ' + Geral.FF0(Integer(stDel)) + ' AWStatSinc');
  //
  CamposZ := 'INSERT INTO ' + TabExclu + ' SELECT ' + sLineBreak +
  CamposZ + sLineBreak +
  'FROM ' + TabAtivo + sLineBreak +
  'WHERE Controle=' + Geral.FF0(Controle) + ';';
  //
  CamposZ := CamposZ + sLineBreak +
  DELETE_FROM + TabAtivo + sLineBreak +
    'WHERE Controle=' + Geral.FF0(Controle) + ';';
  //
  Query.SQL.Text := CamposZ;
  //
  if Query.Database <> DataBase then
    Query.Database := DataBase;
  //
  UMyMod.ExecutaQuery(Query);
  //
  QryIMEC := TmySQLQuery.Create(DMod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, DataBase, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_TMI,
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    SrcNivel2 := QryIMEC.FieldByName('SrcNivel2').AsInteger;
    DstNivel2 := QryIMEC.FieldByName('DstNivel2').AsInteger;
    //
    if SrcNivel2 <> 0 then
      AtualizaSaldoIMEI(SrcNivel2, False);
    //
    if DstNivel2 <> 0 then
      AtualizaSaldoIMEI(DstNivel2, False);
  finally
    QryIMEC.Free;
  end;

  Result := True;
end;

function TUnTX_PF.ExcluiTXNaoTMI(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DB: TmySQLDatabase): Integer;
begin
(*
  if (Lowercase(Tabela) = Lowercase(CO_DEL_TAB_TMI))
  or (Lowercase(Tabela) = Lowercase(CO_TAB_TMB))
  then
  begin
    Result := ID_NO;
    Geral.MB_Erro('AVISE A DERMATEK! Exclus�o de TMI a implementar!');
  end else
*)
    Result := UMyMod.ExcluiRegistroInt1(Pergunta, Tabela, Campo, Inteiro1, DB);
end;

function TUnTX_PF.FatoresIncompativeis(FatorSrc, FatorDst: Integer;
  MeAviso: TMemo): Boolean;
  //
  procedure Avisa(Aviso: String);
  begin
    if MeAviso <> nil then
      MeAviso.Text := Aviso
    else
      Geral.MB_Aviso(Aviso);
  end;
begin
  Result := True;
  if ((FatorSrc = 0) and (FatorDst <> 0))
  or ((FatorSrc <> 0) and (FatorDst = 0)) then
  begin
    Geral.MB_Aviso('Sele��o abortada!' + sLineBreak +
    'O Artigo n�o est� configurado completamente!' + sLineBreak +
    'Termine sua configura��o antes de continuar! (1)');
    Exit;
  end;
  if (FatorSrc = 0) and (FatorDst = 0) then
  begin
    Result := Geral.MB_Pergunta(
    'Fatores de convers�o de origem e destino zerados!' + slineBreak +
    'Tabela: "CouNiv1" solicite recria��o dos registros obrigat�rios � Dermatek! ' + slineBreak +
    'Deseja continuar considerando fator = 1,0?') <> ID_YES;
    Exit;
  end;
  Result := False;
  //
  if FatorSrc <> FatorDst then
    Avisa('CUIDADO!!!' + sLineBreak +
    'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
end;

function TUnTX_PF.GeraNovoPallet(Empresa, ClientMO, GraGruX: Integer; EdGraGruX,
  EdPallet: TdmkEdit; CBPallet: TdmkDBLookupComboBox; SBNewPallet: TSpeedButton;
  QrTXPallet: TmySQLQuery; PallOnEdit: array of Integer): Integer;
var
  Nome, DtHrEndAdd: String;
  Codigo, Status, CliStat, MovimIDGer, QtdPrevPc: Integer;
begin
  Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  //Empresa        := FEmpresa;
  //ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  //GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(0, 109); //DModG.ObtemAgora(), 109);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('TXPalleta', 'Codigo', '', '', tsPos, stIns, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'txpalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    AtualizaStatPall(Codigo);
    ReopenTXPallet(QrTXPallet, Empresa, ClientMO, 0, '', PallOnEdit);
    EdPallet.ValueVariant := Codigo;
    CBPallet.KeyValue := Codigo;
    //
    SBNewPallet.Enabled := False;
  end;
end;

function TUnTX_PF.GeraSQLTabMov(var SQL: String; const Tab: TTabToWork;
  const TemIMEIMrt: Integer): Boolean;
begin
  SQL := '';
  Result := ((Tab <> ttwA) and (TemIMEIMrt = 1)) or (Tab = ttwA);
end;

function TUnTX_PF.GeraSQLTXMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left, SQL_Wher,
  SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
begin
  if GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
    Result := Geral.ATS([
  SQL_Select + TabMovTX_Fld_IMEI(tab),
//  Codigo                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Codigo AS SIGNED) Codigo, ',
//  Controle                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Controle AS SIGNED) Controle, ',
//  MovimCod                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.MovimCod AS SIGNED) MovimCod, ',
//MovimNiv                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.MovimNiv AS SIGNED) MovimNiv, ',
//MovimTwn                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.MovimTwn AS SIGNED) MovimTwn, ',
//Empresa                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Empresa AS SIGNED) Empresa, ',
//ClientMO                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.ClientMO AS SIGNED) ClientMO, ',
//Terceiro                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Terceiro AS SIGNED) Terceiro, ',
//CliVenda                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.CliVenda AS SIGNED) CliVenda, ',
//MovimID                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.MovimID AS SIGNED) MovimID, ',
//DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
    'CAST(tmi.DataHora AS DATETIME) DataHora, ',
//Pallet                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Pallet AS SIGNED) Pallet, ',
//GraGruX                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.GraGruX AS SIGNED) GraGruX, ',
//Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(tmi.Qtde  AS DECIMAL (15,3)) Qtde, ',
//ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(tmi.ValorT AS DECIMAL (15,2)) ValorT, ',
//SrcMovID                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.SrcMovID AS SIGNED) SrcMovID, ',
//SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.SrcNivel1 AS SIGNED) SrcNivel1, ',
//SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.SrcNivel2 AS SIGNED) SrcNivel2, ',
//SrcGGX                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.SrcGGX AS SIGNED) SrcGGX, ',
//SdoVrtQtd                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(tmi.SdoVrtQtd AS DECIMAL (15,3)) SdoVrtQtd, ',
//Observ                         varchar(255) NOT NULL
    'CAST(tmi.Observ AS CHAR) Observ, ',
//SerieTal                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.SerieTal AS SIGNED) SerieTal, ',
//Talao                          int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Talao AS SIGNED) Talao, ',
//FornecMO                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.FornecMO AS SIGNED) FornecMO, ',
//CustoMOUni                      double(15,6) NOT NULL  DEFAULT "0.000000"
    'CAST(tmi.CustoMOUni AS DECIMAL (15,6)) CustoMOUni, ',
//CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(tmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, ',
//ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(tmi.ValorMP AS DECIMAL (15,4)) ValorMP, ',
//CustoPQ                        double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(tmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, ',
//DstMovID                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.DstMovID AS SIGNED) DstMovID, ',
//DstNivel1                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.DstNivel1 AS SIGNED) DstNivel1, ',
//DstNivel2                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.DstNivel2 AS SIGNED) DstNivel2, ',
//DstGGX                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.DstGGX AS SIGNED) DstGGX, ',
//QtdGer                         double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(tmi.QtdGer AS DECIMAL (15,3)) QtdGer, ',
//QtdGer P e c a                     double(15,3) NOT NULL  DEFAULT "0.000"
    //'CAST(tmi.QtdGer P e c a AS DECIMAL (15,3)) QtdGer P e c a, ',
//QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
    //'CAST(tmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, ',
//QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
    //'CAST(tmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, ',
//QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
    //'CAST(tmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, ',
//QtdAnt                         double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(tmi.QtdAnt AS DECIMAL (15,3)) QtdAnt, ',
//AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"
//Marca                          varchar(20)
    'CAST(tmi.Marca AS CHAR) Marca, ',
//TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"
//Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"
//PedItsLib                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.PedItsLib AS SIGNED) PedItsLib, ',
//PedItsFin                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.PedItsFin AS SIGNED) PedItsFin, ',
//PedItsVda                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.PedItsVda AS SIGNED) PedItsVda, ',
//Lk                             int(11)                DEFAULT "0"
//DataCad                        date
//DataAlt                        date
//UserCad                        int(11)                DEFAULT "0"
//UserAlt                        int(11)                DEFAULT "0"
//AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"
//Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"
//ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.ReqMovEstq AS SIGNED) ReqMovEstq, ',
//StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.StqCenLoc AS SIGNED) StqCenLoc, ',
//ItemNFe                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.ItemNFe AS SIGNED) ItemNFe, ',
//TXMorCab                       int(11)      NOT NULL  DEFAULT "0"
//TXMulFrnCab
    'CAST(tmi.TXMulFrnCab AS SIGNED) TXMulFrnCab, ',
//NFeSer
    'CAST(tmi.NFeSer + 0.000 AS SIGNED) NFeSer, ',
//NFeNum
    'CAST(tmi.NFeNum AS SIGNED) NFeNum, ',
//TXMulNFeCab
    'CAST(tmi.TXMulNFeCab AS SIGNED) TXMulNFeCab, ',
//GGXRcl                      int(11)      NOT NULL  DEFAULT "0"
    //'CAST(tmi.GGXRcl AS SIGNED) GGXRcl, ',
//DtCorrApo                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
    'CAST(tmi.DtCorrApo AS DATETIME) DtCorrApo, ',

//IxxMovIX                        tinyint(1)      NOT NULL  DEFAULT "0"
    'CAST(tmi.IxxMovIX AS UNSIGNED) IxxMovIX, ',
//IxxFolha                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.IxxFolha AS SIGNED) IxxFolha, ',
//IxxLinha                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.IxxLinha AS SIGNED) IxxLinha, ',

//CusFrtAvuls                     double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(tmi.CusFrtAvuls AS DECIMAL (15,4)) CusFrtAvuls, ',
//CusFrtMOEnv                     double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(tmi.CusFrtMOEnv AS DECIMAL (15,4)) CusFrtMOEnv, ',
//CusFrtMORet                     double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(tmi.CusFrtMORet AS DECIMAL (15,4)) CusFrtMORet, ',
//CusFrtTrnsf                     double(15,4) NOT NULL  DEFAULT "0.0000"
    //'CAST(tmi.CusFrtTrnsf AS DECIMAL (15,4)) CusFrtTrnsf, ',

    //'tmi.*,
    SQL_Flds,
    'FROM ' + TabMovTX_Tab(tab) + ' tmi ',
    SQL_Left,
    SQL_Wher,
    SQL_Group,
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
end;

function TUnTX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Where,
  SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
begin
  if GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
    Result := Geral.ATS([
    'SELECT ' + TabMovTX_Fld_Pall(tab),
    SQL_NO_GGX(),
    'txp.Nome NO_Pallet, ',
    'CAST(IF(COUNT(DISTINCT tmi.Terceiro) <> 1, 0, tmi.Terceiro) AS SIGNED) Terceiro, ',
    'CAST(IF(COUNT(DISTINCT tmi.Marca) <> 1, "", tmi.Marca) AS CHAR) Marca, ',
    'CAST(IF(COUNT(DISTINCT(tmi.Terceiro))<>1, "", ',
    'IF((tmi.Terceiro=0) OR (tmi.Terceiro IS NULL), "",   ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome))) AS CHAR) NO_FORNECE,  ',
//  Codigo                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.Codigo AS SIGNED) Codigo, ',
//  Controle                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.Controle AS SIGNED) Controle, ',
//  MovimCod                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.MovimCod AS SIGNED) MovimCod, ',
//MovimNiv                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.MovimNiv AS SIGNED) MovimNiv, ',
//MovimTwn                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.MovimTwn AS SIGNED) MovimTwn, ',
//Empresa                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.Empresa AS SIGNED) Empresa, ',
//Terceiro                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.Terceiro AS SIGNED) Terceiro, ',
//CliVenda                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.CliVenda AS SIGNED) CliVenda, ',
//MovimID                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.MovimID AS SIGNED) MovimID, ',
//DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
//    'CAST(tmi.DataHora AS DATETIME) DataHora, ',
//Pallet                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Pallet AS SIGNED) Pallet, ',
//GraGruX                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.GraGruX AS SIGNED) GraGruX, ',
//Qtde                          double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(tmi.Qtde) AS DECIMAL (15,3)) Qtde, ',
//ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(tmi.ValorT) AS DECIMAL (15,2)) ValorT, ',
//SrcMovID                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.SrcMovID AS SIGNED) SrcMovID, ',
//SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.SrcNivel1 AS SIGNED) SrcNivel1, ',
//SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.SrcNivel2 AS SIGNED) SrcNivel2, ',
//SrcGGX                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.SrcGGX AS SIGNED) SrcGGX, ',
//SdoVrtQtd                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(tmi.SdoVrtQtd) AS DECIMAL (15,3)) SdoVrtQtd, ',
//Observ                         varchar(255) NOT NULL
//    'CAST(tmi.Observ AS CHAR) Observ, ',
//SerieFch                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.SerieFch AS SIGNED) SerieFch, ',
//Ficha                          int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.Ficha AS SIGNED) Ficha, ',
//SerieTal                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.SerieTal AS SIGNED) SerieTal, ',
//Talao                          int(11)      NOT NULL  DEFAULT "0"
    'CAST(tmi.Talao AS SIGNED) Talao, ',
//FornecMO                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.FornecMO AS SIGNED) FornecMO, ',
//CustoMOUni                      double(15,6) NOT NULL  DEFAULT "0.000000"
    'CAST(tmi.CustoMOUni AS DECIMAL (15,6)) CustoMOUni, ',
//CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(tmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot ',
//ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"
//    'CAST(tmi.ValorMP AS DECIMAL (15,4)) ValorMP, ',
//DstMovID                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.DstMovID AS SIGNED) DstMovID, ',
//DstNivel1                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.DstNivel1 AS SIGNED) DstNivel1, ',
//DstNivel2                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.DstNivel2 AS SIGNED) DstNivel2, ',
//DstGGX                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.DstGGX AS SIGNED) DstGGX, ',
//QtdGer P e c a                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(tmi.QtdGer P e c a AS DECIMAL (15,3)) QtdGer P e c a, ',
//QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(tmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, ',
//QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(tmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, ',
//QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(tmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, ',
//QtdAnt P e c a                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(tmi.QtdAnt P e c a AS DECIMAL (15,3)) QtdAnt P e c a, ',
//QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(tmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, ',
//QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(tmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, ',
//QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(tmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, ',
//AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"
//Marca                          varchar(20)
//    'CAST(tmi.Marca AS CHAR) Marca, ',
//TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"
//Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"
//EmFluxo                        tinyint(1)   NOT NULL  DEFAULT "1"
//NotFluxo                       int(11)      NOT NULL  DEFAULT "0"
//FatNotaVNC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//FatNotaVRC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//PedItsLib                      int(11)      NOT NULL  DEFAULT "0"
//PedItsFin                      int(11)      NOT NULL  DEFAULT "0"
//PedItsVda                      int(11)      NOT NULL  DEFAULT "0"
//Lk                             int(11)                DEFAULT "0"
//DataCad                        date
//DataAlt                        date
//UserCad                        int(11)                DEFAULT "0"
//UserAlt                        int(11)                DEFAULT "0"
//AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"
//Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"
//CustoMOM2                      double(15,6) NOT NULL  DEFAULT "0.000000"
//    'CAST(tmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, ',
//ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.ReqMovEstq AS SIGNED) ReqMovEstq, ',
//StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.StqCenLoc AS SIGNED) StqCenLoc, ',
//ItemNFe                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(tmi.ItemNFe AS SIGNED) ItemNFe, ',
//TXMorCab                       int(11)      NOT NULL  DEFAULT "0"
    //'tmi.*,
    SQL_Flds,
    'FROM ' + TX_PF.TabMovTX_Tab(tab) + ' tmi ',
    SQL_LJ_GGX(),
    'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
    'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro ',
    SQL_Left,
    SQL_Where,
    SQL_Group,
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
end;

function TUnTX_PF.GetIDNiv(MovimID, MovimNiv: Integer): Integer;
begin
  Result := (MovimID * 1000) + MovimNiv;
end;

function TUnTX_PF.HabilitaComposTXAtivo(ID_TTW: Integer;
  Compos: array of TComponent): Boolean;
var
  Habilita: Boolean;
  I: Integer;
  PropInfo: PPropInfo;
begin
  Habilita := ID_TTW = 0;
  for I := Low(Compos) to High(Compos) do
  begin
    PropInfo := GetPropInfo(Compos[I], 'Enabled');
    if PropInfo <> nil then
    try
      Habilita := Habilita and (GetPropValue(Compos[I], 'Enabled') = True);
      SetPropValue(Compos[I], 'Enabled', Habilita);
    except
      Geral.MB_Erro(
      'N�o foi poss�vel definir a propriedade "Enabled" no objeto "' +
      TComponent(Compos[I]).Name + '"');
    end;
  end;
end;

function TUnTX_PF.HabilitaMenuInsOuAllTXAberto(Qry: TmySQLQuery;
  Data: TDateTime; Button: TWinControl; DefMenu: TPopupMenu;
  MenuItens: array of TMenuItem): Boolean;
  //
  procedure ItensDeMenu(Item: TMenuItem; Dest: TComponent);
  var
    I: Integer;
    Novo: TMenuItem;
  begin
    VAR_SeqMenuItem := VAR_SeqMenuItem + 1;
    Novo := TMenuItem.Create(Button.Owner);
    //Novo.Name := 'Item_Menu_' + Item.Name;
    Novo.Name := 'Item_Menu_' + Item.Name + Geral.FFN(VAR_SeqMenuItem, 9);
    Novo.Caption := Item.Caption;
    Novo.OnClick := Item.OnClick;
    try
      if Dest is TPopupMenu then
        TPopupMenu(Dest).Items.Add(Novo)
      else
        TMenuItem(Dest).Add(Novo);
    except
      Geral.MB_Erro('ERRO!' + sLineBreak + Novo.Caption);
    end;
    // Sub-itens
    for I := 0 to Item.Count - 1 do
    begin
      ItensDeMenu(Item.Items[I], Novo);
    end;
  end;
var
  Habilita: Boolean;
  I: Integer;
  Menu: TPopupMenu;
begin
  if (Qry <> nil) and (Qry.State <> dsInactive) and (Qry.RecordCount = 0) then
    Habilita := True
  else if (Qry <> nil) and (Qry.State = dsInactive) then
    Habilita := True
  else
    Habilita := Geral.Periodo2000(Data) > (VAR_TX_PERIODO_BAL - 1);
  if Habilita then
    MyObjects.MostraPopUpDeBotao(DefMenu, Button)
  else
  begin
    Menu := TPopupMenu.Create(Button.Owner);
    VAR_SeqMenuItem := VAR_SeqMenuItem + 1;
    Menu.Name := 'Menu_' + Geral.FFN(VAR_SeqMenuItem, 9);
    //Menu.Items.Add(MenuItens);
    for I := Low(MenuItens) to High(MenuItens) do
    begin
      ItensDeMenu(MenuItens[I], Menu);
    end;
    MyObjects.MostraPopUpDeBotao(Menu, Button);
  end;
end;

function TUnTX_PF.ImpedePeloBalanco(Data: TDateTime; PermiteCorrApo,
  Avisa: Boolean): Boolean;
var
  Dia: TDateTime;
  BalVal, Meses: Integer;
begin
  if Dmod.QrControleCanAltBalTX.Value = 1 then
    Result := False
  else begin
    Result := True;
    if PermiteCorrApo then
      Meses := 2
    else
      Meses := 0;
    //
    BalVal := VerificaBalanco(Meses);
    Dia := StrToDate(dmkPF.PrimeiroDiaAposPeriodo(BalVal, dtSystem3));
    if Data >= Dia then
      Result := False
    else
      if Avisa then
      Geral.MB_Aviso('Data inv�lida!.' + sLineBreak +
      'J� existe balan�o com data posterior.' + sLineBreak +
      'Solicite ao seu superior o desbloqueio em: ' + sLineBreak +
      'Op��es espec�ficas do aplicagtivo,' + sLineBreak +
      'na aba "' + CO_PODE_ALTERAR_ESTOQUE_PRODUTO_Aba + '" marque a op��o:' + sLineBreak+
      '"' + CO_PODE_ALTERAR_ESTOQUE_PRODUTO_Chk + '".');
  end;
end;

function TUnTX_PF.ImprimeEstoqueEm(Entidade, Filial, Ed00Terceiro_ValueVariant,
  RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
  RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex: Integer;
  Ck00DescrAgruNoItm_Checked: Boolean; Ed00StqCenCad_ValueVariant,
  RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA, CB00StqCenCad_Text,
  CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
  Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean; DBG00GraGruY,
  DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO; Qr00GraGruY, Qr00GraGruX,
  Qr00CouNiv2: TmySQLQuery; DataEm: TDateTime; GraCusPrc,
  Ed00NFeIni_ValueVariant, Ed00NFeFim_ValueVariant: Integer;
  Ck00Serie_Checked: Boolean; Ed00Serie_ValueVariant, MovimCod: Integer;
  LaAviso1, LaAviso2: TLabel; MostraFrx: Boolean): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmTXImpEstqEm, FmTXImpEstqEm, afmoLiberado) then
  begin
    FmTXImpEstqEm.FEntidade                  := Entidade;
    FmTXImpEstqEm.FFilial                    := Filial;
    FmTXImpEstqEm.Ed00Terceiro_ValueVariant  := Ed00Terceiro_ValueVariant;
    FmTXImpEstqEm.RG00_Ordem1_ItemIndex      := RG00_Ordem1_ItemIndex;
    FmTXImpEstqEm.RG00_Ordem2_ItemIndex      := RG00_Ordem2_ItemIndex;
    FmTXImpEstqEm.RG00_Ordem3_ItemIndex      := RG00_Ordem3_ItemIndex;
    FmTXImpEstqEm.RG00_Ordem4_ItemIndex      := RG00_Ordem4_ItemIndex;
    FmTXImpEstqEm.RG00_Ordem5_ItemIndex      := RG00_Ordem5_ItemIndex;
    FmTXImpEstqEm.RG00_Agrupa_ItemIndex      := RG00_Agrupa_ItemIndex;
    FmTXImpEstqEm.Ck00DescrAgruNoItm_Checked := Ck00DescrAgruNoItm_Checked;
    FmTXImpEstqEm.Ed00StqCenCad_ValueVariant := Ed00StqCenCad_ValueVariant;
    FmTXImpEstqEm.RG00ZeroNegat_ItemIndex    := RG00ZeroNegat_ItemIndex;
    FmTXImpEstqEm.FNO_EMPRESA                := FNO_EMPRESA;
    FmTXImpEstqEm.CB00StqCenCad_Text         := CB00StqCenCad_Text;
    FmTXImpEstqEm.CB00Terceiro_Text          := CB00Terceiro_Text;
    FmTXImpEstqEm.TPDataRelativa_Date        := TPDataRelativa_Date;
    FmTXImpEstqEm.Ck00DataCompra_Checked     := Ck00DataCompra_Checked;
    FmTXImpEstqEm.Ck00EmProcessoBH_Checked   := Ck00EmProcessoBH_Checked;
    FmTXImpEstqEm.DBG00GraGruY               := DBG00GraGruY;
    FmTXImpEstqEm.DBG00GraGruX               := DBG00GraGruX;
    FmTXImpEstqEm.DBG00CouNiv2               := DBG00CouNiv2;
    FmTXImpEstqEm.Qr00GraGruY                := Qr00GraGruY;
    FmTXImpEstqEm.Qr00GraGruX                := Qr00GraGruX;
    FmTXImpEstqEm.Qr00CouNiv2                := Qr00CouNiv2;
    FmTXImpEstqEm.FDataEm                    := DataEm;
    FmTXImpEstqEm.FGraCusPrc                 := GraCusPrc;
    FmTXImpEstqEm.FEd00NFeIni_ValueVariant   := Ed00NFeIni_ValueVariant;
    FmTXImpEstqEm.FEd00NFeFim_ValueVariant   := Ed00NFeFim_ValueVariant;
    FmTXImpEstqEm.FCk00Serie_Checked         := Ck00Serie_Checked;
    FmTXImpEstqEm.FEd00Serie_ValueVariant    := Ed00Serie_ValueVariant;
    FmTXImpEstqEm.FLaAviso1                  := LaAviso1;
    FmTXImpEstqEm.FLaAviso2                  := LaAviso2;
    FmTXImpEstqEm.FMovimCod                  := MovimCod;
    FmTXImpEstqEm.FMostraFrx                 := MostraFrx;
    //
    //FmTXImpEstqEm.ShowModal;
    Result := FmTXImpEstqEm.ImprimeEstoqueEm();
    FmTXImpEstqEm.Destroy;
  end;
end;

function TUnTX_PF.ImprimeEstoqueReal(Entidade, Filial,
  Ed00Terceiro_ValueVariant, RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex,
  RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex,
  RG00_Agrupa_ItemIndex: Integer; Ck00DescrAgruNoItm_Checked: Boolean;
  Ed00StqCenCad_ValueVariant, RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA,
  CB00StqCenCad_Text, CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
  Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean; DBG00GraGruY,
  DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO; Qr00GraGruY, Qr00GraGruX,
  Qr00CouNiv2: TmySQLQuery; TableSrc, DataRetroativa: String;
  GraCusPrc: Integer; DataEstoque: TDateTime; Ed00NFeIni_ValueVariant,
  Ed00NFeFim_ValueVariant: Integer; Ck00Serie_Checked: Boolean;
  Ed00Serie_ValueVariant, MovimCod: Integer; MostraFrx: Boolean): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmTXImpEstoque, FmTXImpEstoque, afmoLiberado) then
  begin
    //FmTXImpEstoque.ShowModal;
    FmTXImpEstoque.FEntidade                  := Entidade;
    FmTXImpEstoque.FFilial                    := Filial;
    FmTXImpEstoque.Ed00Terceiro_ValueVariant  := Ed00Terceiro_ValueVariant;
    FmTXImpEstoque.RG00_Ordem1_ItemIndex      := RG00_Ordem1_ItemIndex;
    FmTXImpEstoque.RG00_Ordem2_ItemIndex      := RG00_Ordem2_ItemIndex;
    FmTXImpEstoque.RG00_Ordem3_ItemIndex      := RG00_Ordem3_ItemIndex;
    FmTXImpEstoque.RG00_Ordem4_ItemIndex      := RG00_Ordem4_ItemIndex;
    FmTXImpEstoque.RG00_Ordem5_ItemIndex      := RG00_Ordem5_ItemIndex;
    FmTXImpEstoque.RG00_Agrupa_ItemIndex      := RG00_Agrupa_ItemIndex;
    FmTXImpEstoque.Ck00DescrAgruNoItm_Checked := Ck00DescrAgruNoItm_Checked;
    FmTXImpEstoque.Ed00StqCenCad_ValueVariant := Ed00StqCenCad_ValueVariant;
    FmTXImpEstoque.RG00ZeroNegat_ItemIndex    := RG00ZeroNegat_ItemIndex;
    FmTXImpEstoque.FNO_EMPRESA                := FNO_EMPRESA;
    FmTXImpEstoque.CB00StqCenCad_Text         := CB00StqCenCad_Text;
    FmTXImpEstoque.CB00Terceiro_Text          := CB00Terceiro_Text;
    FmTXImpEstoque.TPDataRelativa_Date        := TPDataRelativa_Date;
    FmTXImpEstoque.Ck00DataCompra_Checked     := Ck00DataCompra_Checked;
    FmTXImpEstoque.Ck00EmProcessoBH_Checked   := Ck00EmProcessoBH_Checked;
    FmTXImpEstoque.DBG00GraGruY               := DBG00GraGruY;
    FmTXImpEstoque.DBG00GraGruX               := DBG00GraGruX;
    FmTXImpEstoque.DBG00CouNiv2               := DBG00CouNiv2;
    FmTXImpEstoque.Qr00GraGruY                := Qr00GraGruY;
    FmTXImpEstoque.Qr00GraGruX                := Qr00GraGruX;
    FmTXImpEstoque.Qr00CouNiv2                := Qr00CouNiv2;
    FmTXImpEstoque.FTableSrc                  := TableSrc;
    FmTXImpEstoque.FDataRetroativa            := DataRetroativa;
    FmTXImpEstoque.FDataEstoque               := DataEstoque;
    FmTXImpEstoque.FGraCusPrc                 := GraCusPrc;
    FmTXImpEstoque.FEd00NFeIni_ValueVariant   := Ed00NFeIni_ValueVariant;
    FmTXImpEstoque.FEd00NFeFim_ValueVariant   := Ed00NFeFim_ValueVariant;
    FmTXImpEstoque.FCk00Serie_Checked         := Ck00Serie_Checked;
    FmTXImpEstoque.FEd00Serie_ValueVariant    := Ed00Serie_ValueVariant;
    FmTXImpEstoque.FMovimCod                  := MovimCod;
    FmTXImpEstoque.FMostraFrx                 := MostraFrx;
    //
    Result := FmTXImpEstoque.ImprimeEstoque();
    FmTXImpEstoque.Destroy;
  end;
end;

procedure TUnTX_PF.ImprimeIMEI(IMEIs: array of Integer;
  TXImpImeiKind: TXXImpImeiKind; LPFMO, FNFeRem: String; QryCab: TmySQLQuery);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmTXImpIMEI, FmTXImpIMEI, afmoLiberado) then
  begin
    //FmTXImpIMEI.ShowModal;
    FmTXImpIMEI.FQryCab := QryCab;
    FmTXImpIMEI.FTXImpImeiKind := TXImpImeiKind;
    FmTXImpIMEI.FLPFMO  := LPFMO;
    FmTXImpIMEI.FNFeRem := FNFeRem;
    SetLength(FmTXImpIMEI.FControles, Length(IMEIs));
    for I := Low(IMEIs) to High(IMEIs) do
      FmTXImpIMEI.FControles[I] := IMEIs[I];
    FmTXImpIMEI.ImprimeIMEI();
    FmTXImpIMEI.Destroy;
  end;
end;

procedure TUnTX_PF.ImprimePallets(Empresa: Integer; Pallets: array of Integer;
  TXMovImp4, TXLstPalBox: String; InfoNO_PALLET: Boolean;
  DestImprFichaPallet: TDestImprFichaPallet; ImpEmpresa: Boolean;
  EmptyLabels: Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmTXImpPallet, FmTXImpPallet, afmoLiberado) then
  begin
    //FmTXImpPallet.ShowModal;
    SetLength(FmTXImpPallet.FPallets, Length(Pallets));
    for I := Low(Pallets) to High(Pallets) do
      FmTXImpPallet.FPallets[I] := Pallets[I];
    FmTXImpPallet.FEmpresa_Cod := Empresa;
    FmTXImpPallet.FInfoNO_PALLET := InfoNO_PALLET;
    FmTXImpPallet.FDestImprFichaPallet := DestImprFichaPallet;
    FmTXImpPallet.FImpEmpresa := ImpEmpresa;
    FmTXImpPallet.FTXMovImp4  := TXMovImp4;
    if Length(Pallets) = 0 then
    begin
      FmTXImpPallet.ReopenGraGruX();
      FmTXImpPallet.PreparaPallets(TXMovImp4, TXLstPalBox, EmptyLabels);
      FmTXImpPallet.TbTXMovImp4.DataBase := DModG.MyPID_DB;
      FmTXImpPallet.TbTXMovImp4.Filter := 'Ativo <> 1';
      FmTXImpPallet.TbTXMovImp4.Filtered := True;
      FmTXImpPallet.TbTXMovImp4.TableName := TXMovImp4; //'_txmovimp4_fmtxmovimp';
      FmTXImpPallet.TbTXMovImp4.Active := True;
      FmTXImpPallet.ShowModal;
    end else
    begin
      FmTXImpPallet.PreparaPallets(TXMovImp4, TXLstPalBox, EmptyLabels);
      FmTXImpPallet.ImprimePallets(TXMovImp4, sqlordDESC);
    end;
    FmTXImpPallet.Destroy;
  end;
end;

procedure TUnTX_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet: Integer;
  JanTab: String; InfoNO_PALLET: Boolean);
const
  GraGruX  = 0;
  CouNiv2  = 0;
  CouNiv1  = 0;
  StqCenCad = 0;
  StqCenLoc = 0;
  GraGruYs = '';
  TXLstPalBox = '';
  SQL_Especificos = '';
  MovimID  = 0;
  MovimCod = 0;
var
  Tabela: String;
begin
  Geral.MB_Info('Falta implementar! TUnTX_PF.ImprimePallet_Unico()');
{
  if MyObjects.FIC(Empresa = 0, nil, 'Informe a empresa') then
    Exit;
  if PesquisaPallets(Empresa, ClientMO, CouNiv2, CouNiv1, StqCenCad, StqcenLoc,
  JanTab, //GraGruYs, [GraGruX], [Pallet], Tabela) then
  GraGruYs, [], [Pallet], SQL_Especificos, TEstqMovimID(MovimID), MovimCod,
  Tabela) then
    ImprimePallets(Empresa, [Pallet], Tabela, TXLstPalBox, InfoNO_PALLET, difpPapelA4);
}
end;

procedure TUnTX_PF.ImprimePallet_Varios(Empresa: Integer;
  Pallets: array of Integer; JanTab: String; InfoNO_PALLET: Boolean);
const
  GraGruX  = 0;
  CouNiv2  = 0;
  CouNiv1  = 0;
  ClientMO = 0;
  StqCenCad = 0;
  StqCenLoc = 0;
  GraGruYs = '';
  TXLstPalBox = '';
  SQL_Especificos = '';
  MovimID  = 0;
  MovimCod = 0;
var
  Tabela: String;
begin
  if MyObjects.FIC(Empresa = 0, nil, 'Informe a empresa') then
    Exit;
  if PesquisaPallets(Empresa, ClientMO, CouNiv2, CouNiv1, StqCenCad, StqCenLoc,
  JanTab, GraGruYs, [], Pallets, SQL_Especificos, TEstqMovimID(MovimID),
  MovimCod, Tabela) then
    ImprimePallets(Empresa, Pallets, Tabela, TXLstPalBox, InfoNO_PALLET, difpPapelA4);
end;

procedure TUnTX_PF.InfoPalletTMI(TMI, Pallet: Integer; Qry: TmySQLQuery);
begin
  InfoRegIntInTMI('Pallet', TMI, Pallet, Qry);
end;

procedure TUnTX_PF.InfoStqCenLoc(TMI, StqCenLoc: Integer; Qry: TmySQLQuery);
begin
  AlteraTMI_StqCenLoc(TMI, StqCenLoc);
  if Qry <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
    Qry.Locate('Controle', TMI, []);
  end;
end;

procedure TUnTX_PF.InfoRegIntInTMI(Campo: String; TMI, Inteiro: Integer;
  Qry: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmTXReqMovEstq, FmTXReqMovEstq, afmoLiberado) then
  begin
    FmTXReqMovEstq.ImgTipo.SQLType := stUpd;
    FmTXReqMovEstq.FCampo := Campo;
    FmTXReqMovEstq.EdControle.ValueVariant := TMI;
    FmTXReqMovEstq.EdInteiro.ValueVariant  := Inteiro;
    FmTXReqMovEstq.FQry := Qry;
    //
    FmTXReqMovEstq.ShowModal;
    FmTXReqMovEstq.Destroy;
  end;
end;

procedure TUnTX_PF.InfoReqMovEstq(TMI, ReqMovEstq: Integer; Qry: TmySQLQuery);
begin
  InfoRegIntInTMI('ReqMovEstq', TMI, ReqMovEstq, Qry);
end;

function TUnTX_PF.InformaIxx(DBGIMEI: TDBGrid; QrIMEI: TmySQLQuery;
  FldIMEI: String): Boolean;
var
  IxxMovIX, IxxFolha, IxxLinha, IMEI: Integer;
////
  procedure ReopenIMEI();
  begin
    if QrIMEI <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(QrIMEI, Dmod.MyDB);
      QrIMEI.Locate('Controle', IMEI, []);
    end;
  end;
  procedure AtualizaAtual();
  begin
    IMEI := QrIMEI.FieldByName(FldIMEI).AsInteger;
    //
    if DBCheck.CriaFm(TFmTXIxx, FmTXIxx, afmoLiberado) then
    begin
      FmTXIxx.ImgTipo.SQLType := stUpd;
      FmTXIxx.FResult := False;
      FmTXIxx.FQrIMEI := QrIMEI;
      FmTXIxx.FIMEI := IMEI;
      FmTXIxx.PreparaEdicao(IxxMovIX, IxxFolha, IxxLinha);
      //
      FmTXIxx.ShowModal;
      Result   := FmTXIxx.FResult;
      IxxMovIX := FmTXIxx.RGIxxMovIX.ItemIndex;
      IxxFolha := FmTXIxx.EdIxxFolha.ValueVariant;
      IxxLinha := FmTXIxx.EdIxxLinha.ValueVariant;
      //
      FmTXIxx.Destroy;
    end;
  end;
  var
    q: TSelType;
    I: Integer;
begin
  IxxMovIX := -1;
  IxxFolha := 0;
  IxxLinha := 0;
  //
  DBCheck.Quais_Selecionou(QrIMEI, TDBGrid(DBGIMEI), q);
  case q of
    istAtual:
    begin
      AtualizaAtual;
      ReopenIMEI();
    end;
    istSelecionados:
    begin
      with DBGIMEI.DataSource.DataSet do
      for I := 0 to DBGIMEI.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGIMEI.SelectedRows.Items[I]));
        GotoBookmark(DBGIMEI.SelectedRows.Items[I]);
        AtualizaAtual();
      end;
      ReopenIMEI();
    end;
    istTodos:
    begin
      QrIMEI.First;
      while not QrIMEI.Eof do
      begin
        AtualizaAtual();
        //
        QrIMEI.Next;
      end;
      ReopenIMEI();
    end;
  end;
end;

procedure TUnTX_PF.InsereTXMovCab(Codigo: Integer; MovimID: TEstqMovimID;
  CodigoID: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'txmovcab', False, [
  'MovimID', 'CodigoID'], [
  'Codigo'], [
  MovimID, CodigoID], [
  Codigo], True);
end;

function TUnTX_PF.InsUpdTXMovIts(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer; Qtde, ValorT: Double; _Data_Hora_: String;
  SrcMovID: TEstqMovimID; SrcNivel1, SrcNivel2: Integer; Observ: String;
  CliVenda, Controle: Integer; CustoMOUni, CustoMOTot, ValorMP: Double;
  DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer; QtdGer: Double;
  AptoUso, FornecMO, SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda: Integer; ReqMovEstq, StqCenLoc, ItemNFe,
  TXMulFrnCab, ClientMO: Integer; QtdAnt: Double; SerieTal, Talao, GGXRcl,
  MovCodPai: Integer; IxxMovIX: TEstqMovInfo;
  IxxFolha, IxxLinha: Integer;
  ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean; InsUpdTMIPrcExecID:
  TInsUpdTMIPrcExecID): Boolean;
var
  iutpei: Integer;
  DtCorrApo, DataHora, NO_iutpei: String;
begin
  //if VAR_USUARIO = -1 then
  begin
    if (StqCenLoc = 0) or (FornecMO = 0) or (ClientMO = 0) then
    begin
      iutpei    := Integer(InsUpdTMIPrcExecID);
      NO_iutpei := 'iutpei = ' +Geral.FF0(iutpei) + ' : ' +
        sListaiuvpeiTxt[iutpei];
      if ExigeStqLoc and (StqCenLoc = 0) then
        Geral.MB_Erro('StqCenLoc indefinido!' + sLineBreak +
        'MovimID=' + Geral.FF0(Integer(MovimID)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + sLineBreak +
        'MovimNiv=' + Geral.FF0(Integer(MovimNiv)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimNiv), Integer(MovimNiv)) +
        sLineBreak + NO_iutpei);
      if ExigeFornecMO and (FornecMO = 0) then
        Geral.MB_Erro('FornecMO indefinido!' + sLineBreak +
        'MovimID=' + Geral.FF0(Integer(MovimID)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + sLineBreak +
        'MovimNiv=' + Geral.FF0(Integer(MovimNiv)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimNiv), Integer(MovimNiv)) +
        sLineBreak + NO_iutpei);
      if ExigeClientMO and (ClientMO = 0) then
        Geral.MB_Erro('ClientMO indefinido!' + sLineBreak +
        'MovimID=' + Geral.FF0(Integer(MovimID)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + sLineBreak +
        'MovimNiv=' + Geral.FF0(Integer(MovimNiv)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimNiv), Integer(MovimNiv)) +
        sLineBreak + NO_iutpei);
    end;
  end;
  //
  if not DefineDatasTMI(_Data_Hora_, DataHora, DtCorrApo) then
    Exit;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_INS_TAB_TMI, False, [
  'Codigo', 'MovimCod', 'MovimNiv',
  'MovimTwn', 'Empresa', 'Terceiro',
  'CliVenda', 'MovimID', 'DataHora',
  'Pallet', 'GraGruX', 'Qtde',
  'ValorT', (*'SdoVrtQtd',*) 'SrcMovID',
  'SrcNivel1', 'SrcNivel2', 'SrcGGX',
  'Observ', 'ClientMO', (*'CustoPQ',*)
  'FornecMO', 'CustoMOUni', 'CustoMOTot',
  'ValorMP', 'DstMovID', 'DstNivel1',
  'DstNivel2', 'DstGGX', 'QtdGer',
  'QtdAnt', 'AptoUso',
  'Marca', 'TpCalcAuto', (*'Zerado',*)
  'PedItsLib', 'PedItsFin', 'PedItsVda',
  'ReqMovEstq', 'StqCenLoc', 'ItemNFe',
  (*'TXMorCab',*) 'TXMulFrnCab', (*'NFeSer',
  'NFeNum', 'TXMulNFeCab',*) 'GGXRcl',
  'DtCorrApo', 'MovCodPai', 'IxxMovIX',
  'IxxFolha', 'IxxLinha', 'Iutpei',
  'SerieTal', 'Talao'], [
  'Controle'], [
  Codigo, MovimCod, MovimNiv,
  MovimTwn, Empresa, Terceiro,
  CliVenda, MovimID, DataHora,
  Pallet, GraGruX, Qtde,
  ValorT, (*SdoVrtQtd,*) SrcMovID,
  SrcNivel1, SrcNivel2, SrcGGX,
  Observ, ClientMO, (*CustoPQ,*)
  FornecMO, CustoMOUni, CustoMOTot,
  ValorMP, DstMovID, DstNivel1,
  DstNivel2, DstGGX, QtdGer,
  QtdAnt, AptoUso,
  Marca, TpCalcAuto, (*Zerado,*)
  PedItsLib, PedItsFin, PedItsVda,
  ReqMovEstq, StqCenLoc, ItemNFe,
  (*TXMorCab,*) TXMulFrnCab, (*NFeSer,
  NFeNum, TXMulNFeCab,*) GGXRcl,
  DtCorrApo, MovCodPai, IxxMovIX,
  IxxFolha, IxxLinha, Iutpei,
  SerieTal, Talao], [
  Controle], True);
end;

function TUnTX_PF.InverteDatas(const MovimXX: Integer; const Data,
  Agora: TDateTime; var DataHora, DtCorrApo: String): Boolean;
var
  DataIni, DataFim: TDateTime;
begin
  Geral.MB_Info('Falta implementar! TUnTX_PF.InverteDatas()');
  Result  := False;
  DataIni := Geral.PeriodoToDate(MovimXX + 1, 1, True, detJustSum);
  DataFim := Geral.UltimoDiaDoMes(DataIni);
  if DBCheck.CriaFm(TFmXXDataEFDData, FmXXDataEFDData, afmoNegarComAviso) then
  begin
    FmXXDataEFDData.TPDataFisico.MinDate := Dataini;
    FmXXDataEFDData.TPDataFisico.MaxDate := DataFim;
    if Data < Dataini then
      FmXXDataEFDData.TPDataFisico.Date    := DataIni
    else
    if Data > DataFim then
      FmXXDataEFDData.TPDataFisico.Date    := DataFim
    else
      FmXXDataEFDData.TPDataFisico.Date    := Agora;
    FmXXDataEFDData.EdHoraFisico.ValueVariant := Agora;
    //
    FmXXDataEFDData.TPDataSPEDEFD.Date         := Data;
    FmXXDataEFDData.EdHoraSPEDEFD.ValueVariant := Data;
    //
    FmXXDataEFDData.ShowModal;
    //
    DataHora  := FmXXDataEFDData.FDataFisico;
    DtCorrApo := FmXXDataEFDData.FDataSPEDEFD;
    Result    := True;
    //
    FmXXDataEFDData.Destroy;
  end;
end;

function TUnTX_PF.LocalizaPeloIMEC(EstqMovimID: TEstqMovimID): Integer;
begin
  Result := LocalizaPorCampo(EstqMovimID, 'IME-C', 'MovimCod');
end;

function TUnTX_PF.LocalizaPeloIMEI(EstqMovimID: TEstqMovimID): Integer;
begin
  Result := LocalizaPorCampo(EstqMovimID, 'IME-I', 'Controle');
end;

function TUnTX_PF.LocalizaPorCampo(EstqMovimID: TEstqMovimID; Caption,
  Campo: String): Integer;
var
  Qry: TmySQLQuery;
  Valor: Integer;
  Numero: String;
  Avisa: Boolean;
begin
  Result := 0;
  Valor := 0;
  Avisa := False;
  Numero := '0';
  if InputQuery('Localiza��o de material por ' + Caption,
  Caption, Numero) then
  begin
    Valor := Geral.IMV(Numero);
    if Valor <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DISTINCT Codigo ',
        'FROM ' + CO_SEL_TAB_TMI,
        'WHERE ' + Campo + '=' + Geral.FF0(Valor),
        'AND MovimID=' + Geral.FF0(Integer(EstqMovimID)),
        '',
        'UNION',
        '',
        'SELECT DISTINCT Codigo ',
        'FROM ' + CO_TAB_TMB,
        'WHERE ' + Campo + '=' + Geral.FF0(Valor),
        'AND MovimID=' + Geral.FF0(Integer(EstqMovimID)),
        '']);
        Result := Qry.FieldByName('Codigo').AsInteger;
        Avisa := Result = 0;
      finally
        Qry.Free;
      end;
    end else
      Avisa := True;
  end;
  if Avisa then
    Geral.MB_Aviso('N�o foi possivel a obten��o do c�digo!');
end;

procedure TUnTX_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc: TControl;
  EdStqCenLoc: TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox;
  QrStqCenLoc: TmySQLQuery; MovimID: TEstqMovimID);
begin
{
  //FmTXHide.FMovimIDSel   := FMovimIDSel;
  //FmTXHide.FStqCenCadSel := FStqCenCadSel;
  //FmTXHide.FStqcenLocSel := FStqcenLocSel;
  FmTXHide.FEdStqCenLoc := EdStqCenLoc;
  FmTXHide.FCBStqCenLoc := CBStqCenLoc;
  FmTXHide.FQrStqCenLoc := QrStqCenLoc;
  FmTXHide.FMovimIDSel  := MovimID;
  MyObjects.MostraPopUpDeBotao(FmTXHide.PMTXMovIDLoc, SbStqCenLoc);
}
  Geral.MB_Info('Falta fazer TUnTX_PF.MostraPopUpDeMovID_Ou_CenLoc()');
end;

function TUnTX_PF.ObrigaInfoIxx(IxxMovIX: TEstqMovInfo; IxxFolha,
  IxxLinha: Integer): Boolean;
begin
  Result := MyObjects.FIC(
  (
    (Integer(IxxMovIX) <> 0)
    or (IxxFolha <> 0)
    or (IxxLinha <> 0)
  ) and
  (
    (Integer(IxxMovIX) = 0)
    or (IxxFolha = 0)
    or (IxxLinha = 0)
  ), nil, 'Informa��o manual de movimenta��o incompleta!' + sLineBreak +
   'Informa��o manual de movimenta��o: Informe o IEC/ICR/ISC, a Folha e a Linha!');
end;

function TUnTX_PF.ObtemControleIMEI(const SQLType: TSQLType;
  var Controle: Integer; const Senha: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  if Trim(Senha) <> '' then
  begin
    if Senha = CO_MASTER then
    begin
     Qry := TmySQLQuery.Create(Dmod);
     try
       UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
       'SELECT MIN(Controle) Controle ',
       'FROM ' + CO_TAB_TMB,
       '']);
       if Qry.RecordCount = 0 then
       begin
         UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
         'SELECT MIN(Controle) Controle ',
         'FROM ' + CO_SEL_TAB_TMI,
         '']);
       end;
       Controle := Qry.FieldByName('Controle').AsInteger;
       Controle := Controle - 1;
       if Controle = 0 then
         Controle := -1;
       Result := True;
     finally
       Qry.Free;
     end;
    end else
    begin
      Geral.MB_Aviso('Senha inv�lida!');
      Exit;
    end;
  end else
  begin
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_TMI, 'Controle', '', '', tsPos, SQLType,
    Controle);
    Result := True;
  end;
end;

function TUnTX_PF.ObtemControleMovimTwin(MovimCod, MovimTwn: Integer;
  MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_TMI,
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    'AND MovimID=' + Geral.FF0(Integer(MovimID)),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
    if Qry.RecordCount = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM ' + CO_TAB_TMB,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'AND MovimID=' + Geral.FF0(Integer(MovimID)),
      'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
      '']);
    end;
    //
    Result := Qry.FieldByName('Controle').AsInteger;
    if Result = 0 then
      Geral.MB_Aviso('Controle n�o localizado para MovimTwn ' + Geral.FF0(MovimTwn));
  finally
    Qry.Free;
  end;
end;

function TUnTX_PF.ObtemDataBalanco(Empresa: Integer): TDateTime;
var
  Periodo: Integer;
begin
  Periodo := VerificaBalanco(0) + 1;
  Result := DmkPF.PrimeiroDiaDoPeriodo_Date(Periodo);
end;

function TUnTX_PF.ObtemFatorInteiro(const GraGruX: Integer;
  var FatorIntRes: Integer; const Avisa: Boolean;
  const FatorIntCompara: Integer; MeAviso: TMemo): Boolean;
const
  Aviso = 'CUIDADO!!!' + sLineBreak +
  'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!';
var
  Qry: TmySQLQuery;
  CouNiv2: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggc.CouNiv2, co1.FatorInt ',
    'FROM couniv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(GraGruX),
    '']);
    FatorIntRes := Trunc(Qry.FieldByName('FatorInt').AsFloat * 1000);
    CouNiv2     := Qry.FieldByName('FatorInt').AsFloat;
    //
    //if (FatorIntRes = 0) and (FatorIntCompara = 0) and (CouNiv2 <> 1) then
    if  (CouNiv2 <> 1) and (CouNiv2 <> 0.5) then
    begin
      if FatorIntCompara <> 0 then
      begin
        if FatorIntCompara < 100 then
          FatorIntRes := Trunc(FatorIntCompara * 1000)
        else
          FatorIntRes := FatorIntCompara;
      end else
        FatorIntRes := 1000;
    end;
    if FatoresIncompativeis(FatorIntRes, FatorIntCompara, MeAviso) then
      Exit;
    //
    if Avisa then
    begin
      if FatorIntRes <> (FatorIntCompara * 1000) then
      begin
        if MeAviso <> nil then
          MeAviso.Text := Aviso
        else
          Geral.MB_Aviso(Aviso);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnTX_PF.ObtemGraGruY_de_IDTipoCouro(Index: Integer): Integer;
begin
  Result := sCodeGraGruY_TX[Index];
(*
  case Index of
    0: Result := CO_GraGruY_1024_TXNatCad;
    1: Result := CO_GraGruY_2048_TXRibCad;
    2: Result := CO_GraGruY_3072_TXRibCla;
    3: Result := CO_GraGruY_4096_TXRibOpe;
    4: Result := CO_GraGruY_5120_TXWetEnd;
    5: Result := CO_GraGruY_6144_TXFinCla;
    //
    6: Result := CO_GraGruY_0512_TXSubPrd;
    //
    else
    begin
      Geral.MB_Erro('"Dmod.ObtemGraGruY_de_IDTipoCouro()" n�o implementado!');
      Result := 0;
    end;
  end;
*)
end;

function TUnTX_PF.ObtemNomeFrxEstoque(Data: String; RG00_Agrupa_ItemIndex,
  RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
  RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex: Integer;
  CB00StqCenCad_Text: String): String;
begin
  Result := 'Estoque';
  if Trim(CB00StqCenCad_Text) <> '' then
  Result := Result + '_' + Trim(CB00StqCenCad_Text);
  if Data <> '' then
    Result := Result + '_' + Geral.SoNumero_TT(Data);
  Result := Result + '_' + sMax_TX_IMP_ESTQ_ORD[RG00_Ordem1_ItemIndex];
  if RG00_Agrupa_ItemIndex > 0 then
    Result := Result + '_' + sMax_TX_IMP_ESTQ_ORD[RG00_Ordem2_ItemIndex];
  if RG00_Agrupa_ItemIndex > 1 then
    Result := Result + '_' + sMax_TX_IMP_ESTQ_ORD[RG00_Ordem3_ItemIndex];
  if RG00_Agrupa_ItemIndex > 2 then
    Result := Result + '_' + sMax_TX_IMP_ESTQ_ORD[RG00_Ordem4_ItemIndex];
  if RG00_Agrupa_ItemIndex > 3 then
    Result := Result + '_' + sMax_TX_IMP_ESTQ_ORD[RG00_Ordem5_ItemIndex];
end;

function TUnTX_PF.ObtemNomeTabelaTXXxxCab(MovimID: TEstqMovimID;
  Avisa: Boolean): String;
const
  sProcName = 'ObtemNomeTabelaTXXxxCab';
begin
  AdicionarNovosTX_emid();
  Result := CO_FIVE_ASKS;
  case MovimID of
    (*00*)//emidAjuste: Result        := 'tx';
    (*01*)emidCompra: Result          := 'txinncab';
    (*02*)emidVenda: Result           := 'txoutcab';
    (*03*)//emidReclasWE: Result      := 'tx';
    (*04*)//emidBaixa: Result         := 'tx';
    (*05*)//emidIndsWE: Result        := 'tx';
{
    (*06*)emidIndsTX: Result          := 'txgerarta';
    (*07*)emidClassArtTXUni: Result   := 'txpaclacaba';
    (*08*)emidReclasTXUni: Result     := 'txgerrcla';
    (*09*)emidForcado: Result         := 'txbxacab';
    (*10*)//emidSemOrigem: Result     := 'tx';
    (*11*)emidEmOperacao: Result      := 'txopecab';
    (*12*)emidResiduoReclas: Result   := CO_TX___Cab;
}
    (*13*)emidInventario: Result      := 'txajscab';
{
    (*14*)emidClassArtTXMul: Result   := 'txpamulcaba';
    (*15*)emidPreReclasse: Result     := 'txprepalcab';
    (*16*)emidEntradaPlC: Result      := 'txplccab';
    (*17*)emidExtraBxa: Result        := 'txbxacab';
    (*18*)emidSaldoAnterior: Result   := 'tx';
    (*19*)emidEmProcWE: Result        := 'txpwecab';
    (*20*)emidFinished: Result        := 'txpwecab';
    (*21*)emidDevolucao: Result       := 'txdvlcab';
    (*22*)emidRetrabalho: Result      := 'txrtbcab';
    (*23*)emidGeraSubProd: Result     := 'txsubprdcab';
    (*24*)emidReclasTXMul: Result     := 'txpamulcabr';
}
    (*25*)emidTransfLoc: Result       := 'txtrfloccab';
{
    (*26*)emidEmProcCal: Result       := 'txcalcab';
    (*27*)emidEmProcCur: Result       := 'txcurcab';
    (*28*)emidDesclasse: Result       := 'txdsccab';
    (*29*)emidCaleado: Result         := 'txcaljmp';
    (*30*)emidEmRibPDA: Result        := 'txcalpda';
    (*31*)emidEmRibDTA: Result        := 'txcaldta';
    (*32*)emidEmProcSP: Result        := 'txpspcab';
    (*33*)emidEmReprRM: Result        := 'txrrmcab';
    (*34*)emidCurtido: Result         := 'txcurjmp';
    (*35*)emidMixInsum: Result        := 'pqm';
    (*36*)emidInnSemCob: Result       := 'txesccab';
    (*37*)emidOutSemCob: Result       := 'txssccab';}
    (*38*)emidEmIndstrlzc: Result     := 'txindcab';
    else
      if Avisa then
        Geral.MB_ERRO('"MovimID" ' + Geral.FF0(Integer(MovimID)) +
        ' n�o implementado em ' + sProcName);
  end;
end;

function TUnTX_PF.ObtemProximoTalao(const Empresa: Integer;
  const EdSerieTal: TdmkEditCB; var Talao: Integer;
  SerieDefinida: Integer): Boolean;
var
  Qry: TmySQLQuery;
  SerieTal: Integer;
begin
  Result := False;
  if SerieDefinida <> 0 then
    SerieTal := SerieDefinida
  else
    SerieTal := EdSerieTal.ValueVariant;
  if MyObjects.FIC(SerieTal = 0, EdSerieTal, 'Defina a s�rie primeiro!') then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Talao) Talao ',
    'FROM ' + CO_SEL_TAB_TMI,
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND SerieTal=' + Geral.FF0(SerieTal),
    '']);
    if Qry.FieldByName('Talao').AsInteger = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MAX(Talao) Talao ',
      'FROM ' + CO_TAB_TMB,
      'WHERE Empresa=' + Geral.FF0(Empresa),
      'AND SerieTal=' + Geral.FF0(SerieTal),
      '']);
    end;
    //
    Talao := Qry.FieldByName('Talao').AsInteger + 1;
    Result := True;
  finally
    Qry.Free;
  end;
end;

function TUnTX_PF.PesquisaPallets(const Empresa, ClientMO, CouNiv2, CouNiv1,
  StqCenCad, StqCenLoc: Integer; const Tabela, GraGruYs: String; const GraGruXs,
  Pallets: array of Integer; const SQL_Especificos: String;
  const MovimID: TEstqMovimID; const MovimCod: Integer;
  var sTXMovImp4: String): Boolean;
  //
  function SQL_ListaPallets(): String;
  var
    I, N: Integer;
  begin
    Result := '';
    N := High(Pallets);
    for I := Low(Pallets) to N do
    begin
      Result := Result + sLineBreak + Geral.FF0(Pallets[I]);
      if I < N then
        Result := Result + ',';
    end;
  end;
  function SQL_ListaGraGruXs(): String;
  var
    I, N: Integer;
  begin
    Result := '';
    N := High(GraGruXs);
    for I := Low(GraGruXs) to N do
    begin
      Result := Result + sLineBreak + Geral.FF0(GraGruXs[I]);
      if I < N then
        Result := Result + ',';
    end;
  end;
var
  SQL_IMEI, SQL_Empresa, SQL_GraGruX, SQL_CouNiv1, SQL_CouNiv2, SQL_GraGruY,
  ATT_StatPall, SQL_Pallets, SQL_ClientMO, SQL_StqCenCad, SQL_StqCenLoc,
  SQL_MovimID, SQL_MovimCod: String;
  Qry: TmySQLQuery;
begin
  if (Length(Pallets) > 0 ) and (Trim(SQL_Especificos) <> '') then
    Geral.MB_Aviso('Foram informados pallets checados e espec�ficos!' +
    sLineBreak + 'Ser�o pesquisados apenas os checados!');
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    sTXMovImp4 :=
      UnCreateTX.RecriaTempTableNovo(ntrttTXMovImp4, DModG.QrUpdPID1,
      False, 1, '_txmovimp4_' + Tabela);
    //
    if Empresa <> 0 then
      SQL_Empresa := 'AND tmi.Empresa=' + Geral.FF0(Empresa)
    else
      SQL_Empresa := '';
    //
    if ClientMO <> 0 then
      SQL_ClientMO := 'AND tmi.ClientMO=' + Geral.FF0(ClientMO)
    else
      SQL_ClientMO := '';
    //
    if Length(GraGruXs) > 0 then
      //SQL_GraGruX := 'AND txp.GraGruX=' + Geral.FF0(GraGruX)
      SQL_GraGruX := 'AND txp.GraGruX IN (' + SQL_ListaGraGruXs() + ') '
    else
      SQL_GraGruX := '';
    //
    if CouNiv2 <> 0 then
      SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2)
    else
      SQL_CouNiv2 := '';
    //
    if CouNiv1 <> 0 then
      SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1)
    else
      SQL_CouNiv1 := '';
    if Trim(GraGruYs) <> '' then
      SQL_GraGruY := 'AND ggy.Codigo IN (' + GraGruYs + ') '
    else
      SQL_GraGruY := '';
    //
    if StqCenCad <> 0 then
      SQL_StqCenCad := 'AND scc.Codigo=' + Geral.FF0(StqCenCad)
    else
      SQL_StqCenCad := '';
    //
    if StqCenLoc <> 0 then
      SQL_StqCenLoc := 'AND tmi.StqCenLoc=' + Geral.FF0(StqCenLoc)
    else
      SQL_StqCenLoc := '';
    //
    //
    if Integer(MovimID) <> 0 then
      SQL_MovimID := 'AND tmi.MovimID=' + Geral.FF0(Integer(MovimID))
    else
      SQL_MovimID := '';
    //
    if MovimCod <> 0 then
      SQL_MovimCod := 'AND tmi.MovimCod=' + Geral.FF0(MovimCod)
    else
      SQL_MovimCod := '';
    //
    SQL_IMEI := Geral.ATS([
    'tmi.Codigo, tmi.MovimCod IMEC, tmi.Controle IMEI, ',
    'tmi.MovimID, tmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
    //
    ATT_StatPall := dmkPF.ArrayToTexto('txp.StatPall', 'NO_StatPall', pvPos, True,
    sTXStatPall);
    //
    if Length(Pallets) > 0 then
    begin
      SQL_Pallets := 'AND tmi.Pallet IN (' + SQL_ListaPallets() + ') ';
    end else
    begin
      if Trim(SQL_Especificos) <> '' then
        SQL_Pallets := 'AND ' + SQL_Especificos
      else
       SQL_Pallets := '';
    end;
    //fazer ficha e testar
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DModG.MyPID_DB, [
    DELETE_FROM + ' ' + sTXMovImp4 + '; ',
    'INSERT INTO  ' + sTXMovImp4,
    'SELECT tmi.Empresa, txp.GraGruX, SUM(tmi.Qtde) Qtde,  ',
    'SUM(ValorT) ValorT,  SUM(tmi.SdoVrtQtd) SdoVrtQtd, ',
    '0 LmbQtd, ggx.GraGru1, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, tmi.Pallet, txp.Nome NO_Pallet,  ',
    'IF(COUNT(DISTINCT(tmi.terceiro))<>1, 0, ',
    'IF(tmi.Terceiro IS NULL, 0, tmi.Terceiro)), ',
    'IF(txp.CliStat IS NULL, 0, txp.CliStat), ',
    'IF(txp.Status IS NULL, 0, txp.Status), ',
    'IF(COUNT(DISTINCT(tmi.terceiro))<>1, "", ',
    'IF((tmi.Terceiro=0) OR (tmi.Terceiro IS NULL), "",   ',
    'IF(trc.Tipo=0, trc.RazaoSocial, trc.Nome))) NO_FORNECE,  ',
    'IF((txp.CliStat=0) OR (txp.CliStat IS NULL), "",   ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,  ',
    'vps.Nome NO_STATUS, ',
    'MAX(tmi.DataHora) DataHora, 0 OrdGGX,  ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    // Parei Aqui! 2015-04-11 Ver pallet nao encerrado!
    '0 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    'SUM(tmi.SdoVrtQtd * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) SdoInteiros, ',
    '0 LmbInteiros, ',
    SQL_IMEI,
    'tmi.SerieTal, txs.Nome NO_SerieTal, tmi.Talao, txp.StatPall, ',
    ATT_StatPall,
    'tmi.NFeSer, tmi.NFeNum, tmi.TXMulNFeCab, ',
    'IF(scc.Codigo IS NULL, 0, scc.Codigo) StqCenCad, scc.Nome NO_StqCenCad, ',
    //'scc.Codigo StqCenCad, scc.Nome NO_StqCenCad, ',
    'tmi.StqCenLoc, scl.Nome NO_StqcenLoc, ',
    '1 Ativo  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_TMI + ' tmi  ',
    'LEFT JOIN ' + TMeuDB + '.txpalleta  txp ON txp.Codigo=tmi.Pallet  ',
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=tmi.GraGruX  ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=txp.GraGruX  ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY  ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN ' + TMeuDB + '.entidades  trc ON trc.Codigo=tmi.Terceiro  ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=txp.CliStat  ',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=tmi.Empresa  ',
    'LEFT JOIN ' + TMeuDB + '.txpalsta   vps ON vps.Codigo=txp.Status  ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    'LEFT JOIN ' + TMeuDB + '.txsertal   txs ON txs.Codigo=tmi.SerieTal ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=tmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE tmi.Controle <> 0  ',
    'AND tmi.Pallet <> 0 ',
    'AND tmi.SdoVrtQtd > 0 ',
    SQL_Empresa,
    SQL_ClientMO,
    SQL_GraGruX,
    SQL_CouNiv2,
    SQL_CouNiv1,
    SQL_GraGruY,
    SQL_Pallets,
    SQL_StqCenCad,
    SQL_StqCenLoc,
    SQL_MovimID,
    SQL_MovimCod,
    'GROUP BY tmi.Empresa, tmi.Pallet, txp.GraGruX;  ',
    '']);
    //Geral.MB_SQL(nil, Qry);
  finally
    Qry.Free;
  end;
end;

{
procedure TUnTX_PF.ReopenStqCenLoc(Qry: TmySQLQuery; Centro: Variant;
  Controle: Integer; MovimID: TEstqMovimID);
  procedure Abre(MovID: TEstqMovimID);
  var
    SQL_StqCenCad, SQL_MovimID_LJ, SQL_MovimID_WH: String;
  begin
    if (Centro <> Null) and (Centro <> Unassigned) then
      SQL_StqCenCad := 'WHERE scl.Codigo=' + Geral.FF0(Centro)
    else
      SQL_StqCenCad := 'WHERE scl.Codigo>-999999999';
    //
    if MovID = TEstqMovimID.emidAjuste then
    begin
      SQL_MovimID_LJ := '';
      SQL_MovimID_WH := '';
    end else
    begin
      SQL_MovimID_LJ := 'LEFT JOIN ' + CO_TAB_XX_MOV_ID_LOC + ' mil ON mil.StqCenLoc=scl.Controle';
      SQL_MovimID_WH := 'AND mil.MovimID=' + Geral.FF0(Integer(MovID));
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT scc.EntiSitio, scl.Controle, scl.CodUsu,  ',
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
      'FROM stqcenloc scl ',
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
      SQL_MovimID_LJ,
      SQL_StqCenCad,
      SQL_MovimID_WH,
      VAR_CRC_StqCen_LOC,
      'ORDER BY NO_LOC_CEN ',
      '']);
    if Controle <> 0 then
      Qry.Locate('Controle', Controle, []);
    //Geral.MB_SQL(nil, Qry);
  end;
begin
  if Qry.Params.Count = 0 then
  begin
    Qry.Params.Add;
    Qry.Params[0].Value := Centro;
  end;
  Abre(MovimID);
  if (Qry.RecordCount = 0) and (MovimID <> TEstqMovimID.emidAjuste) then
  begin
    Abre(TEstqMovimID.emidAjuste);
  end;
end;
}

{
function TUnTX_PF.RedefineReduzidoOnPallet(MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv): TTipoTrocaGgxTmiPall;
const
  sProcName = 'RedefineReduzidoOnPallet()';
var
  Index: Integer;
begin
  AdicionarNovosTX_emid();
  //
  Result := ttvpIndef;
  Index  := GetIDNiv(Integer(MovimID), Integer(MovimNiv));
  case Index of
    00000: Result := ttvpIndef;
    01000: Result := ttvpGraGrux; // Reduzido � o foco!
    02000: Result := ttvpGraGrux; // Reduzido � o foco!
    //emidReclasWE=3,
    //emidBaixa=4,
    //emidIndsWE=5,
    //emidIndsXX=6 >> eminDestCurtiXX=13, eminSorcCurtiXX=14, eminBaixCurtiXX=15,
    06013: Result := ttvpNenhum; // Reduzido � o foco! Mas n�o existe pallet aqui!
    06014: Result := ttvpNenhum;
    06015: Result := ttvpNenhum;

    07001: Result := ttvpNenhum;
    07002: Result := ttvpClaRcl; // Muda em tr�s campos de dois IMVs

    08001: Result := ttvpNenhum;
    08002: Result := ttvpClaRcl; // Muda em tr�s campos de dois IMVs

    09000: Result := ttvpGraGrux; // Reduzido � o foco!

    10000: Result := ttvpGraGrux; // Reduzido � o foco!

    11007: Result := ttvpNenhum;
    11008: Result := ttvpNenhum;
    11009: Result := ttvpGraGrux; // Reduzido � o foco!
    11010: Result := ttvpNenhum;

    12000: Result := ttvpGraGrux; // Reduzido � o foco!

    13000: Result := ttvpGraGrux; // Reduzido � o foco!

    14001: Result := ttvpNenhum;
    14002: Result := ttvpGraGrux; // Reduzido � o foco!

    15011: Result := ttvpGraGrux; // Reduzido � o foco!
    15012: Result := ttvpGraGrux; // Reduzido � o foco!

    16000: Result := ttvpGraGrux; // Reduzido � o foco!

    17000: Result := ttvpGraGrux; // Reduzido � o foco!

    18000: Result := ttvpGraGrux; // Reduzido � o foco!

    19020: Result := ttvpNenhum;
    19021: Result := ttvpNenhum;
    20022: Result := ttvpGraGrux; // Reduzido � o foco!
    19023: Result := ttvpNenhum;

    21000: Result := ttvpGraGrux; // Reduzido � o foco!

    22000: Result := ttvpGraGrux; // Reduzido � o foco!

    23000: Result := ttvpGraGrux; // Tem pallet?

    24001: Result := ttvpNenhum;
    24002: Result := ttvpGraGrux; // Reduzido � o foco!

    25027: Result := ttvpGraGrux; // Reduzido � o foco!;
    25028: Result := ttvpGraGrux; // Reduzido � o foco!;

    26029: Result := ttvpNenhum;
    26030: Result := ttvpNenhum;
    26031: Result := ttvpGraGrux; // Reduzido � o foco! N�o tem Pallet!;
    26032: Result := ttvpNenhum;
    26033: Result := ttvpNenhum;

    27034: Result := ttvpNenhum;
    27035: Result := ttvpNenhum;
    27036: Result := ttvpGraGrux; // Reduzido � o foco! N�o tem Pallet!;
    27037: Result := ttvpNenhum;
    27038: Result := ttvpNenhum;

    28001: Result := ttvpNenhum;
    28002: Result := ttvpGraGrux;

    29031: Result := ttvpGraGrux; // Reduzido � o foco! N�o tem Pallet!;
    29032: Result := ttvpNenhum;

    32049: Result := ttvpNenhum;
    32050: Result := ttvpNenhum;
    32051: Result := ttvpGraGrux; // Reduzido � o foco!
    32052: Result := ttvpNenhum;
    32053: Result := ttvpNenhum;

    33054: Result := ttvpNenhum;
    33055: Result := ttvpNenhum;
    33056: Result := ttvpGraGrux; // Reduzido � o foco!
    33057: Result := ttvpNenhum;
    33058: Result := ttvpNenhum;

    36000: Result := ttvpGraGrux; // Reduzido � o foco!

    38061: Result := ttvpNenhum;
    38062: Result := ttvpNenhum;
    38063: Result := ttvpGraGrux; // Reduzido � o foco!
    38064: Result := ttvpNenhum;

    else Geral.MB_Erro('Index MovimID.Nivel ' + Geral.FF0(Index) +
    ' n�o implementado em ' + sProcName);
  end;
end;
}

procedure TUnTX_PF.ReopenGraGruX(Qry: TmySQLQuery; SQL_AND: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED ',
  'FROM gragrux ggx',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.Controle',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE ggx.Ativo = 1 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  EmptyStr]);
end;

procedure TUnTX_PF.ReopenGraGruX_Pallet(QrGraGruX: TmySQLQuery);
begin
  AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    //Geral.FF0(CO_GraGruY_1536_...) + ',' +
    //Geral.FF0(CO_GraGruY_2048_...) + ',' +
    Geral.FF0(CO_GraGruY_2048_TXCadInd) + ',' +
    Geral.FF0(CO_GraGruY_6144_TXCadFcc) +
    ')');
end;

procedure TUnTX_PF.ReopenIMEIsPositivos(Qry: TmySQLQuery; Empresa, GraGruX,
  Pallet, Talao, IMEI, TipoCouro, StqCenCad, StqCenLoc, ImeiSrc, CouNiv1,
  CouNiv2: Integer; DataLimite: TDateTime);
var
  SQL_Empresa,SQL_GraGruX, SQL_Pallet, SQL_Talao, SQL_IMEI, SQL_GraGruY,
  SQL_StqCenCad, SQL_StqCenLoc, SQL_ImeiSrc, SQL_DataLimite, SQL_CouNiv1,
  SQL_CouNiv2: String;
begin
  SQL_Empresa := '';
  SQL_Pallet  := '';
  SQL_Talao   := '';
  SQL_IMEI    := '';
  SQL_GraGruY := '';
  SQL_StqCenCad := '';
  SQL_StqCenLoc := '';
  SQL_ImeiSrc   := '';
  SQL_DataLimite := '';
  SQL_CouNiv1 := '';
  SQL_CouNiv2 := '';
  //
  if Empresa <> 0 then
    SQL_Empresa := 'AND tmi.Empresa=' + Geral.FF0(Empresa);
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND tmi.GraGruX=' + Geral.FF0(GraGruX);
  if Pallet <> 0 then
    SQL_Pallet := 'AND tmi.Pallet=' + Geral.FF0(Pallet);
  if Talao <> 0 then
    SQL_Talao  := 'AND tmi.Talao=' + Geral.FF0(Talao);
  if IMEI <> 0 then
    SQL_IMEI  := 'AND tmi.Controle=' + Geral.FF0(IMEI);
  if TipoCouro > -1 then
    SQL_GraGruY := 'AND ggx.GragruY=' + Geral.FF0(TX_PF.ObtemGraGruY_de_IDTipoCouro(TipoCouro));
  if StqCenCad <> 0 then
    SQL_StqCenCad := 'AND scc.Codigo=' + Geral.FF0(StqCenCad);
  if StqCenLoc <> 0 then
    SQL_StqCenLoc := 'AND tmi.StqCenLoc=' + Geral.FF0(StqCenLoc);
  if ImeiSrc <> 0 then
    SQL_ImeiSrc := Geral.ATS([
    'AND tmi.MovimCod=( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_TMI + ' ',
    '  WHERE Controle=' + Geral.FF0(ImeiSrc),
    ') ']);
  if CouNiv1 <> 0 then
    SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1);
  if CouNiv2 <> 0 then
    SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2);
  if DataLimite > 0 then
    SQL_DataLimite := 'AND tmi.DataHora < + "' + Geral.FDT(DataLimite + 1, 1) + '"';
  //
  TX_PF.AdicionarNovosTX_emid();
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, tmi.*,',
  TX_PF.SQL_NO_GGX(),
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  pal ON pal.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=tmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=tmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'WHERE (tmi.SdoVrtQtd > 0) ',
  'AND ( ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidIndsXX)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidClassArtXXUni)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidReclasXXUni)),
////////////////////////////////////////////////////////////////////////////////
  '  OR  ',
  '  (tmi.MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
  '  AND ',
  '  tmi.MovimNiv=' + Geral.FF0(Integer(eminDestOper)) + ')',
////////////////////////////////////////////////////////////////////////////////
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidClassArtXXMul)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidReclasXXMul)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmProcWE)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidInventario)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEntradaPlC)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidFinished)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidDevolucao)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidRetrabalho)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidGeraSubProd)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidTransfLoc)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmProcCal)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmProcCur)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidCaleado)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmRibPDA)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmRibDTA)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmProcSP)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmReprRM)),
  '  OR  ',
  '  tmi.MovimID=' + Geral.FF0(Integer(emidEmIndstrlzc)),
  '  OR  ',
  '  tmi.SrcMovID<>0 ',
  ') ',
  SQL_Empresa,
  SQL_GraGruX,
  SQL_Pallet,
  SQL_Talao,
  SQL_IMEI,
  SQL_GraGruY,
  SQL_StqCenCad,
  SQL_StqCenLoc,
  SQL_ImeiSrc,
  SQL_CouNiv1,
  SQL_CouNiv2,
  SQL_DataLimite,
  'ORDER BY DataHora, Pallet, Talao ',
  '']);
  //Geral.MB_SQL(nil, Qry);
end;

procedure TUnTX_PF.ReopenPedItsXXX(QrTXPedIts: TmySQLQuery; InsPedIts,
  UpdPedIts: Integer);
begin
{$IfDef sAllTX}
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPedIts, Dmod.MyDB, [
  'SELECT vpi.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)), ',
  '" ", vpc.PedidCli, " ", Texto)  ',
  'NO_PRD_TAM_COR ',
  'FROM txpedits vpi ',
  'LEFT JOIN txpedcab   vpc ON vpc.Codigo=vpi.Codigo ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE vpi.TXMovIts IN (' +
  Geral.FF0(UpdPedIts) + ',' + Geral.FF0(UpdPedIts) + ') ',
  '']);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappTX);
{$EndIf}
end;

procedure TUnTX_PF.ReopenQrySaldoIMEI_Baixa(Qry1, Qry2: TmySQLQuery;
  SrcNivel2: Integer);
begin
////////////////////////////////////////////////////////////////////////////////
///  C U I D A D O ! ! !
///  C U I D A D O ! ! !
///  N�o mexer aqui!
///  Esta procedure abre a query que atualiza o estoque do IMEI !!!!!!!!!!!!!!!!
///  C U I D A D O ! ! !
///  C U I D A D O ! ! !
////////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT SUM(tmi.Qtde) Qtde, ',
  'SUM(tmi.QtdGer) QtdGer ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'WHERE tmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND (tmi.Qtde < 0 ',
  'OR tmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_TX + ') ',
  ') ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
  'SELECT SUM(tmi.Qtde) Qtde, ',
  'SUM(tmi.QtdGer) QtdGer ',
  'FROM ' + CO_TAB_TMB + ' tmi ',
  'WHERE tmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND (tmi.Qtde < 0 ',
  'OR tmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_TX + ') ',
  ') ',
  '']);
///  C U I D A D O ! ! !
///  C U I D A D O ! ! !
  //Geral.MB_SQL(nil, Qry1);
  //Geral.MB_SQL(nil, Qry2);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
end;

procedure TUnTX_PF.ReopenStqCenLoc(Qry: TmySQLQuery; Centro: Variant;
  Controle: Integer; MovimID: TEstqMovimID);
begin
  Grade_PF.ReopenStqCenLoc(Qry, Centro, Controle, MovimID);
end;

procedure TUnTX_PF.ReopenTXItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNivz: array of TEstqMovimNiv;
  LocCtrl: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_Nivz: String;
  I: Integer;
  Nivz, Virgula: String;
begin
  Virgula := '';
  Nivz := '';
  for I := 0 to High(MovimNivz) do
  begin
    Nivz := Nivz + Virgula + Geral.FF0(Integer(MovimNivz[I]));
    Virgula := ',';
  end;
  SQL_Nivz := '';
  if Trim(Nivz) <> '' then
    SQL_Nivz := 'AND tmi.MovimNiv IN (' + Nivz + ') ';
  //
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  'txp.Nome NO_Pallet ',
  '']);
  //'FROM ' + TX_PF.TabMovTX_Tab(tab) + ' tmi ',
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID(SrcMovID))),
  'AND tmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  'AND tmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  SQL_Nivz,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_Info(Qry.SQL.Text);
  //
  if LocCtrl <> 0 then
    Qry.Locate('Controle', LocCtrl, []);
end;

procedure TUnTX_PF.ReopenTXIts_Controle_If(Qry: TmySQLQuery; Controle,
  TemIMEIMrt: Integer);
var
  Ctrl_TXT: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  if Controle <> 0 then
    Ctrl_TXT := Geral.FF0(Controle)
  else
    Ctrl_TXT := '-999999999';
  //
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  'txp.Nome NO_Pallet, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FORNECMO, ',
  'IF(Qtde > 0, ValorT / Qtde, 0) Custo_Qt ',
  '']);
  //'FROM ' + TabMovTX_Tab(tab) + ' tmi ',
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades  fmo ON fmo.Codigo=tmi.FornecMO ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.Controle=' + Ctrl_TXT,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY DataHora, Controle ',
  '']);
  //Geral.MB_SQL(Self, QrTXSubPrdIts);
end;

procedure TUnTX_PF.ReopenTXListaPallets(Qry: TmySQLQuery; Tabela: String;
  Empresa, GraGruX, Pallet, Terceiro: Integer; OrderBy: String);
var
 Ordem: String;
begin
  Ordem := Trim(OrderBy);
  if Ordem = '' then
    Ordem := 'ORDER BY tmi.Pallet DESC ';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
  'SELECT CONCAT(NO_StqCenLoc, " (", NO_StqCenCad, ")") NO_LOC_CEN, tmi.* ',
  'FROM  ' + Tabela + ' tmi ',
  'WHERE (tmi.Qtde >= 0.001 OR tmi.Qtde <= -0.001) ',
  'AND tmi.SdoVrtQtd > 0 ',
  Ordem,
  '']);
  //Geral.MB_SQL(nil, Qry);
  //
  Qry.Locate('Empresa;GraGruX;Pallet;Terceiro',
  VarArrayOf([Empresa, GraGruX, Pallet, Terceiro]), []);
end;

procedure TUnTX_PF.ReopenTXMOEnvATMI(QrTXMOEnvATmi: TmySQLQuery; TXMOEnvAvu,
  Codigo: Integer);
begin
//
end;

procedure TUnTX_PF.ReopenTXMOEnvAvu(QrTXMOEnvAvu: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
//
end;

procedure TUnTX_PF.ReopenTXMOEnvEnv(QrTXMOEnvEnv: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
//
end;

procedure TUnTX_PF.ReopenTXMOEnvETMI(QrTXMOEnvETmi: TmySQLQuery; TXMOEnvEnv,
  Codigo: Integer);
begin
//
end;

procedure TUnTX_PF.ReopenTXMOEnvGTmi(QrTXMOEnvGTmi: TmySQLQuery; TXMOEnvRet,
  Codigo: Integer);
begin
//
end;

procedure TUnTX_PF.ReopenTXMOEnvRet(QrTXMOEnvRet: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
//
end;

procedure TUnTX_PF.ReopenTXMOEnvRTmi(QrTXMOEnvRTmi: TmySQLQuery; TXMOEnvRet,
  Codigo: Integer);
begin
//
end;

procedure TUnTX_PF.ReopenTXMovIts_Pallet2(QrTXMovIts, QrPallets: TmySQLQuery);
var
  GraGruX, Empresa, Pallet: Integer;
begin
  ////////////////////////////////////////////////////////////
  //QrEstqR4 >> GROUP BY tmi.Empresa, tmi.Pallet, tmi.GraGruX
  /////////////////////////////////////////////////////////////
  GraGruX    := QrPallets.FieldByName('GraGruX').AsInteger;
  Empresa    := QrPallets.FieldByName('Empresa').AsInteger;
  Pallet     := QrPallets.FieldByName('Pallet').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, tmi.*, ggx.GraGruY, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_TMI + ' tmi ',
  'LEFT JOIN txpalleta pal ON pal.Codigo=tmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=tmi.Terceiro',
  'LEFT JOIN gragrux ggx ON ggx.Controle=pal.GraGruX',
  'WHERE tmi.Empresa=' + Geral.FF0(Empresa),
  'AND pal.GraGruX=' + Geral.FF0(GraGruX),
  'AND tmi.Pallet=' + Geral.FF0(Pallet),
  'AND SdoVrtQtd> 0 ',
  'ORDER BY DataHora, Pallet, Controle',
  '']);
end;

procedure TUnTX_PF.ReopenTXMovXXX(Qry: TmySQLQuery; Campo: String; IMEI,
  TemIMEIMrt, CtrlLoc: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('tmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('tmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  //
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  ATT_MovimID,
  ATT_MovimNiv,
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'tst.Nome NO_SerieTal, ',
  'txp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=tmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.' + Campo + '=' + Geral.FF0(IMEI),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
  //
  if IMEI <> 0 then
    Qry.Locate('Controle', IMEI, []);
end;

procedure TUnTX_PF.ReopenTXGerArtSrc(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; SQL_Limit: String; EstqMovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'tst.Nome NO_SerieTal, ',
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(EstqMovimNiv)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  SQL_Limit,
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnTX_PF.ReopenTXIndDesclDst(Qry: TmySQLQuery; Codigo, Controle,
  Pallet, TemIMEIMrt: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrTXPWECabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'tst.Nome NO_SerieTal, ',
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
  'AND tmi.DstMovID=' + Geral.FF0(Integer(emidFinished)),
  'AND tmi.DstNivel1=' + Geral.FF0(Codigo),//QrTXPWECabCodigo.Value),
  'AND tmi.DstNivel2=' + Geral.FF0(Controle), //QrTXPWEAtuControle.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry(*QrTXPWEDesclDst*), Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
end;

procedure TUnTX_PF.ReopenTXIndPrcBxa(Qry: TmySQLQuery; MovimCod, MovimTwn,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  //TemIMEIMrt: Integer;
begin
  //TemIMEIMrt := QrTXPWECabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'tst.Nome NO_SerieTal, ',
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN txserfch   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(MovimCod),
  //'AND tmi.MovimNiv=' + Geral.FF0(Integer(eminEmWEndBxa)),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND tmi.MovimTwn=' + Geral.FF0(MovimTwn),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
end;

procedure TUnTX_PF.ReopenTXIndPrcDst(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_IndsXX: String;
begin
  case MovimNiv of
    eminDestCal, //=31,
    eminDestCur: //=36
    begin
      SQL_IndsXX := Geral.ATS([
      'OR (MovimID=' + Geral.FF0(Integer(emidIndsXX)), // 6
      '    AND SrcNivel2 IN ',
      '    ( ',
      '      SELECT Controle ',
      '      FROM ' + CO_SEL_TAB_TMI + ' ',
      '      WHERE MovimCod=' + Geral.FF0(MovimCod),
      '    ) ',
      ') ',
      '']);
    end;
    else SQL_IndsXX := '';
  end;
  //
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  TX_PF.SQL_NO_FRN(),
  TX_PF.SQL_NO_SCL(),
  'tst.Nome NO_SerieTal, ',
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  TX_PF.SQL_LJ_FRN(),
  TX_PF.SQL_LJ_SCL(),
  'LEFT JOIN txpalleta  txp  ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst  ON tst.Codigo=tmi.SerieTal ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE (tmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  ') ' + SQL_IndsXX,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnTX_PF.ReopenTXPallet(QrTXPallet: TmySQLQuery; Empresa, ClientMO,
  GraGruX: Integer; Ordem: String; PallOnEdit: array of Integer);
var
  SQL_GraGruX, ORDERBY, SQL_Pallets: String;
begin
  if Length(PallOnEdit) > 0 then
  begin
    SQL_Pallets := 'OR (let.Codigo=' + MyObjects.CordaDeArrayInt(PallOnEdit) + ')';
  end else
    SQL_Pallets := '';
  //
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND let.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := '';
  //
  if Ordem = '' then
    ORDERBY := 'ORDER BY Codigo, NO_PRD_TAM_COR '
  else
    ORDERBY := Ordem;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXPallet, Dmod.MyDB, [
  'SELECT let.*,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,',
  'CONCAT(let.Codigo, " - ", gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome)), ',
  'IF(let.Nome<>"", CONCAT(" (", let.Nome, ")"), "")',
  ') NO_PRD_TAM_COR, vps.Nome NO_STATUS   ',
  'FROM txpalleta let  ',
  'LEFT JOIN entidades  emp ON emp.Codigo=let.Empresa  ',
  'LEFT JOIN entidades  cli ON cli.Codigo=let.CliStat  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=let.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN txpalsta   vps ON vps.Codigo=let.Status',
  'WHERE (let.DtHrEndAdd < "1900-01-01"',
  'AND let.Empresa=' + Geral.FF0(Empresa),
  'AND let.ClientMO=' + Geral.FF0(ClientMO),
  SQL_GraGruX,
  ')',
  SQL_Pallets,
  ORDERBY,
  '']);
  //Geral.MB_SQL(nil, QrTXPallet);
end;

procedure TUnTX_PF.ReopenTXPrestador(QrPrestador: TmySQLQuery);
begin
  DModG.ReopenPrestador(QrPrestador);
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrestador, Dmod.MyDB, [
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE ',
  'FROM entidades ent ',
  'WHERE ent.' + VAR_FLD_ENT_PRESTADOR + '="V" ',
  'ORDER BY NOMEENTIDADE ',
  '']);
}
end;

procedure TUnTX_PF.ReopenTXXxxExtra(Qry: TmySQLQuery; Codigo,
  TemIMEIMrt: Integer; MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrTXPWECab.FieldByName('TemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'tst.Nome NO_SerieTal, ',
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimID=' + Geral.FF0(Integer(MovimID)),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND tmi.DstMovID=' + Geral.FF0(Integer(DstMovID)), //emidFinished)),
  'AND tmi.DstNivel1=' + Geral.FF0(Codigo),
  //'AND tmi.DstNivel2=' + Geral.FF0(Controle),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
end;

procedure TUnTX_PF.ReopenTXXxxOris(QrTXXxxCab, QrTXXxxOriIMEI,
  QrTXXxxOriPallet: TmySQLQuery; Controle, Pallet: Integer;
  MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrTXXxxCab.FieldByName('TemIMEIMrt').AsInteger;
  SQL_Flds := Geral.ATS([
  TX_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'tst.Nome NO_SerieTal, ',
  'txp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  TX_PF.SQL_LJ_GGX(),
  'LEFT JOIN txpalleta  txp ON txp.Codigo=tmi.Pallet ',
  'LEFT JOIN txsertal   tst ON tst.Codigo=tmi.SerieTal ',
  'LEFT JOIN entidades  frn ON frn.Codigo=tmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXXxxCab.FieldByName('MovimCod').AsInteger),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),  // eminSorc...
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXXxxOriIMEI, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Pallet, Controle ',
  '']);
  //
  QrTXXxxOriIMEI.Locate('Controle', Controle, []);
  //
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE tmi.MovimCod=' + Geral.FF0(QrTXXxxCab.FieldByName('MovimCod').AsInteger),
  'AND tmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),  // eminSorc...
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTXXxxOriPallet, Dmod.MyDB, [
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  TX_PF.GeraSQLTXMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrTXXxxOriPallet.Locate('Pallet', Pallet, []);
end;

procedure TUnTX_PF.SetaGGXUnicoEmLista(const GraGruX: Integer;
  var Lista: TPallArr);
begin
  if GraGruX <> 0 then
  begin
    SetLength(Lista, 1);
    Lista[0] := GraGruX;
  end else
    SetLength(Lista, 0);
end;

procedure TUnTX_PF.SetIDTipoCouro(RG: TRadioGroup; Colunas, Default: Integer);
var
  I: Integer;
begin
  RG.Items.Clear;
  for I := 0 to MaxGraGruY_TX do
    RG.Items.Add(sListaGraGruY_TX[I]);
  //
  RG.Columns := Colunas;
  RG.ItemIndex := Default;
end;

function TUnTX_PF.SQL_LJ_CMO(): String;
begin
  Result := Geral.ATS([
  'LEFT JOIN entidades   cmo ON cmo.Codigo=tmi.ClientMO ']);
end;

function TUnTX_PF.SQL_LJ_FRN(): String;
begin
  Result := Geral.ATS([
  'LEFT JOIN entidades   frn ON frn.Codigo=tmi.Terceiro',
  'LEFT JOIN txmulfrncab mfc ON mfc.Codigo=tmi.TXMulFrnCab ']);
end;

function TUnTX_PF.SQL_LJ_GGX(): String;
begin
  Result := Geral.ATS([
  'LEFT JOIN gragrux    ggx ON ggx.Controle=tmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
end;

function TUnTX_PF.SQL_LJ_SCL: String;
begin
  Result := Geral.ATS([
  'LEFT JOIN stqcenloc  scl ON scl.Controle=tmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ']);
end;

function TUnTX_PF.SQL_MovIDeNiv_Pos_All: String;
begin
  AdicionarNovosTX_emid();
  Result := Geral.ATS([
    '   (MovimID =  6 AND MovimNiv = 13)',
    'OR (MovimID =  7 AND MovimNiv =  2)',
    'OR (MovimID =  8 AND MovimNiv =  2)',
    //'OR (MovimID = 11 AND MovimNiv =  8)',   // ???
    'OR (MovimID = 11 AND MovimNiv =  9)',
    'OR (MovimID = 13 AND MovimNiv =  0)',
    'OR (MovimID = 14 AND MovimNiv =  2)',
    'OR (MovimID = 15 AND MovimNiv = 12)',
    'OR (MovimID = 16 AND MovimNiv =  0)',
    //'OR (MovimID = 19 AND MovimNiv = 21)',   // ???
    'OR (MovimID = 20 AND MovimNiv = 22)',
    'OR (MovimID = 21 AND MovimNiv =  0)',
    'OR (MovimID = 22 AND MovimNiv =  0)',
    'OR (MovimID = 23 AND MovimNiv =  0)',
    'OR (MovimID = 24 AND MovimNiv =  2)',
    'OR (MovimID = 25 AND MovimNiv = 28)',
    'OR (MovimID = 26 AND MovimNiv = 30)',
    'OR (MovimID = 27 AND MovimNiv = 35)',
    'OR (MovimID = 28 AND MovimNiv =  2)',
    // 29 >> 2017-11-07
    'OR (MovimID = 29 AND MovimNiv = 31)',
    // 30, 31 ???
    'OR (MovimID = 32 AND MovimNiv = 51)',
    'OR (MovimID = 33 AND MovimNiv = 56)',
    // 38 >> 2019-06-20
    'OR (MovimID = 38 AND MovimNiv = 63)'
  ]);
end;

function TUnTX_PF.SQL_NO_CMO(): String;
begin
  Result := Geral.ATS([
  'IF(tmi.ClientMO <> 0, ',
  '  IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome), ',
  '  "") NO_ClientMO, ']);
end;

function TUnTX_PF.SQL_NO_FRN(): String;
begin
  Result := Geral.ATS([
  'IF(tmi.Terceiro <> 0, ',
  '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome), ',
  '  mfc.Nome) NO_FORNECE, ']);
end;

function TUnTX_PF.SQL_NO_GGX(): String;
begin
  Result := Geral.ATS(['CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ']);
end;

function TUnTX_PF.SQL_NO_SCL(): String;
begin
  Result := 'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ';
end;

function TUnTX_PF.SQL_TipoEstq_DefinirCodi(CAST: Boolean; NomeFld: String;
  UsaVirgula: Boolean; FldQtd, Prefacio: String): String;
const
  sProcName = 'TUnTX_PF.SQL_TipoEstq_DefinirCodi()';
var
  Cast_Pre, Cast_Pos, Virgula: String;
begin
  if UsaVirgula then
    Virgula := ', '
  else
    Virgula := '';
  //
  if CAST then
  begin
    Cast_Pre := ' CAST(';
    Cast_Pos := ' AS DECIMAL(15,0) ) ';
    //Cast_Pos := ' AS UNSIGNED) ';
  end else
  begin
    Cast_Pre := '';
    Cast_Pos := '';
  end;
  Result := Geral.ATS([
  Cast_Pre + 'IF(' + Prefacio + 'gg1.UnidMed <> 0, ' + Prefacio + 'med.Grandeza, ',
  '  IF(' + Prefacio + 'xco.Grandeza > 0, ' + Prefacio + 'xco.Grandeza, ',
  '  IF((' + Prefacio + 'ggx.GraGruY<2048 OR ',
  '  ' + Prefacio + 'xco.CouNiv2<>1) AND ' + FldQtd(*Peso*) + ' <> 0, 2, ',
  '  IF(' + Prefacio + 'xco.CouNiv2=1 AND ' + FldQtd(*ArM2*) + ' <> 0, 1, ',
  //  Erro!!! 999
  //'  IF(' + FldQtd P e c a + '=0, 5, CAST(0 - 1 AS UNSIGNED) ))))) ' + Cast_Pos + NomeFld + Virgula,
  '  IF(' + FldQtd(*P e c a*) + '=0, 5, 9 ))))) ' + Cast_Pos + NomeFld + Virgula,
  '']);
  //
end;

function TUnTX_PF.TabMovTX_Fld_IMEI(Tab: TTabToWork): String;
begin
  case Tab of
    TTabToWork.ttwA: Result := ' "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwB: Result := ' "Morto" NO_TTW, CAST(1 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwC: Result := ' "Ambos" NO_TTW, CAST(2 AS UNSIGNED) ID_TTW, ';
    else             Result := ' "?????" NO_TTW, CAST(9 AS UNSIGNED) ID_TTW, ';
  end;
end;

function TUnTX_PF.TabMovTX_Fld_Pall(Tab: TTabToWork): String;
begin
  case Tab of
    TTabToWork.ttwA: Result := ' "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwB: Result := ' "Morto" NO_TTW, CAST(1 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwC: Result := ' "Ambos" NO_TTW, CAST(2 AS UNSIGNED) ID_TTW, ';
    else             Result := ' "?????" NO_TTW, CAST(9 AS UNSIGNED) ID_TTW, ';
  end;
end;

function TUnTX_PF.TabMovTX_Tab(Tab: TTabToWork): String;
begin
  case Tab of
    TTabToWork.ttwA: Result := CO_TAB_TAB_TMI;
    TTabToWork.ttwB: Result := CO_TAB_TMB;
    else             Result := 'txmovit?'
  end;
end;

function TUnTX_PF.TalaoErro(EdSerieTal: TdmkEdit; Empresa, Controle,
  Talao: Integer; PermiteDuplicar: Boolean): Boolean;
var
  SerieTal: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  if Talao <> 0 then
  begin
    Result := True;
    SerieTal := EdSerieTal.ValueVariant;
    //
    if MyObjects.FIC(SerieTal = 0, EdSerieTal,
    'Informe a S�rie do Tal�o') then
      Exit;
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM ' + CO_SEL_TAB_TMI,
      'WHERE SerieTal=' + Geral.FF0(SerieTal),
      'AND Talao=' + Geral.FF0(Talao),
      'AND MovimID=' + Geral.FF0(Integer(emidCompra)),
      'AND Empresa=' + Geral.FF0(Empresa),
      Geral.ATS_IF(Controle <> 0, ['AND Controle<>' + Geral.FF0(Controle)]),
      '',
      'UNION',
      '',
      'SELECT Controle ',
      'FROM ' + CO_TAB_TMB,
      'WHERE SerieTal=' + Geral.FF0(SerieTal),
      'AND Talao=' + Geral.FF0(Talao),
      'AND MovimID=' + Geral.FF0(Integer(emidCompra)),
      'AND Empresa=' + Geral.FF0(Empresa),
      Geral.ATS_IF(Controle <> 0, ['AND Controle<>' + Geral.FF0(Controle)]),
      '']);
      if not PermiteDuplicar then
        Result :=
          MyObjects.FIC(Qry.RecordCount > 0, nil, 'Tal�o j� existe para a s�rie selecionada!')
      else
        Result := False;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnTX_PF.TXFic(GraGruX, Empresa, Fornecedor, Pallet:
  Integer; Qtde, ValorT: Double; EdGraGruX, EdPallet, EdQtde, EdValorT:
  TWinControl; ExigeFornecedor: Boolean; GraGruY: Integer; EdStqCenLoc:
  TWinControl; SerieTal, Talao: Integer; EdSerieTal, EdTalao: TWinControl):
  Boolean;
begin
  Result := True;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe uma mat�ria-prima!') then
    Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa inv�lida!') then
    Exit;
  if ExigeFornecedor then
    if MyObjects.FIC(Fornecedor = 0, nil, 'Fornecedor inv�lido!') then
      Exit;
  if EdPallet <> nil then
    if MyObjects.FIC(Pallet = 0, EdPallet, 'O volume � obrigat�rio!') then
      Exit;
  if EdQtde <> nil then
    if MyObjects.FIC(Qtde = 0, EdQtde, 'A quantidade � obrigat�ria!') then
      Exit;
  if EdStqCenLoc <> nil then
    if MyObjects.FIC(TDmkEditCB(EdStqCenLoc).ValueVariant = 0, EdStqCenLoc,
    'O local/centro de estoque � obrigat�rio!') then
      Exit;
    //
  if EdSerieTal <> nil then
  begin
    if MyObjects.FIC(TDmkEditCB(EdSerieTal).ValueVariant = 0, EdSerieTal,
    'A s�rie do tal�o � obrigat�ria!') then
      Exit;
    if MyObjects.FIC((Talao = 0) and (SerieTal <> -1), EdTalao,
    'O n�mero do tal�o � obrigat�ria!') then
      Exit;
  end;
  //
  Result := False;
end;

function TUnTX_PF.ValidaCampoNF(Operacao: Integer; EditNF: TdmkEdit;
  MostraMsg: Boolean): Boolean;
var
  NObrigNFeTX, Opera: Integer;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreQuery(Dmod.QrControle, Dmod.MyDB);
  //
  NObrigNFeTX := VARF_NObrigNFeTX; // Dmod.QrControleNObrigNFeTX.Value;
  Opera       := Trunc(Power(2, Operacao));
  //
  if not Geral.IntInConjunto(Opera, NObrigNFeTX) then
  begin
    if EditNF.ValueVariant = 0 then
    begin
      if MostraMsg then
      begin
        Geral.MB_Aviso('Informe a nota fiscal!');
        EditNF.SetFocus;
      end;
    end else
      Result := True;
  end else
    Result := True;
end;

function TUnTX_PF.VerificaBalanco(MesesAntes: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    'FROM spedefdicmsipience ',
    'WHERE MovimXX=1 ', // 1=Feito
    '']);
    Result := Geral.AnoMesToPeriodo(Qry.FieldByName('AnoMes').AsInteger) -
      MesesAntes - 1;
  finally
    Qry.Free;
  end;
end;

procedure TUnTX_PF.ZeraSaldoIMEI(Controle: Integer);
const
  SdoVrtQtd = 0;
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_TMI, False, [
  'SdoVrtQtd' ], [
  'Controle'], [
  SdoVrtQtd], [
  Controle], True);
end;

end.
