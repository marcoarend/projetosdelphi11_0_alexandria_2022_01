unit CreateTX;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable = (
    ntrttTXMovImp1, (*ntrttTXMovImp2, ntrttTXMovImp3,*) ntrttTXMovImp4,
    ntrttTXImpIMEI, (*ntrttTXImpRoma,
    ntrttTXCacIts, ntrttTXMovIts, ntrttTXCacGBY, ntrttTXLstPalBox,
    ntrttTXLstPalGhost, ntrttTXTMIClaSdo,*)
    ntrttTXVerifIMEI, (*ntrttTXSeqIts, ntrttTXFluxIncon, ntrttTXPsqSeqCac,
    ntrttTxTmiPsq1, ntrttTxTmiPsq2,*)
    ntrttTmiEstqEmTmi,
    ntrttTmiEstqEmTmiPosit
    (*ntrttTXImpRendIMEI, ntrttTXComparaCacIts, ntrttTXMO,
    ntrttTXUsoPQ_TMI, ntrttTXEstqPQ,
    ntrttTMIQtdEnvETMI, ntrttTMIQtdEnvGTMI, ntrttTMIQtdEnvRTMI,
    ntrttTXMOEnvCTeGer, ntrttTXMOEnvNFeGer*));
  TAcaoCreate = (acDrop, acCreate, acFind);
  TCreateTX = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttTXMovImp1(Qry: TmySQLQuery);
{POIU
    procedure Cria_ntrttTXMovImp2(Qry: TmySQLQuery);
    procedure Cria_ntrttTXMovImp3(Qry: TmySQLQuery);
}
    procedure Cria_ntrttTXMovImp4(Qry: TmySQLQuery);
    procedure Cria_ntrttTXImpIMEI(Qry: TmySQLQuery);
{
    procedure Cria_ntrttTXImpRoma(Qry: TmySQLQuery);
    procedure Cria_ntrttTXCacIts(Qry: TmySQLQuery);
    procedure Cria_ntrttTXMovIts(Qry: TmySQLQuery);
    procedure Cria_ntrttTXCacGBY(Qry: TmySQLQuery);
    procedure Cria_ntrttTXLstPalBox(Qry: TmySQLQuery);
    procedure Cria_ntrttTXLstPalGhost(Qry: TmySQLQuery);
    procedure Cria_ntrttTXTMIClaSdo(Qry: TmySQLQuery);
}

    procedure Cria_ntrttTXVerifIMEI(Qry: TmySQLQuery);
{
    procedure Cria_ntrttTXSeqIts(Qry: TmySQLQuery);
    procedure Cria_ntrttTXFluxIncon(Qry: TmySQLQuery);
    procedure Cria_ntrttTXPsqSeqCac(Qry: TmySQLQuery);
    procedure Cria_ntrttTxTmiPsq1(Qry: TmySQLQuery);
    procedure Cria_ntrttTxTmiPsq2(Qry: TmySQLQuery);
}
    procedure Cria_ntrttTmiEstqEmTmi(Qry: TmySQLQuery);
    procedure Cria_ntrttTmiEstqEmTmiPosit(Qry: TmySQLQuery);
{
    procedure Cria_ntrttTXImpRendIMEI(Qry: TmySQLQuery);
    procedure Cria_ntrttTXComparaCacIts(Qry: TmySQLQuery);
    procedure Cria_ntrttTXMO(Qry: TmySQLQuery);
    procedure Cria_ntrttTXUsoPQ_TMI(Qry: TmySQLQuery);
    procedure Cria_ntrttTXEstqPQ(Qry: TmySQLQuery);
    procedure Cria_ntrttTMIQtdEnvETMI(Qry: TmySQLQuery);
    procedure Cria_ntrttTMIQtdEnvGTMI(Qry: TmySQLQuery);
    procedure Cria_ntrttTMIQtdEnvRTMI(Qry: TmySQLQuery);
    procedure Cria_ntrttTXMOEnvCTeGer(Qry: TmySQLQuery);
    procedure Cria_ntrttTXMOEnvNFeGer(Qry: TmySQLQuery);
}
    //
  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnCreateTX: TCreateTX;

implementation

uses UnMyObjects, Module;

procedure TCreateTX.Cria_ntrttTXVerifIMEI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataEmis                       datetime                                   ,');
  Qry.SQL.Add('  IMEI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Status                         int(11)                                    ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{POIU
procedure TCreateTX.Cria_ntrttTXMO(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_FatID                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_FatNum                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_Empresa                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_nItem                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_SerNF                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_nNF                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_Qtde                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  NFCMO_CusMOUni                 double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NFCMO_ValorT                   double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NFRMP_FatID                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_FatNum                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_Empresa                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_nItem                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_SerNF                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_nNF                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_Qtde                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  NFRMP_ValorT                   double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  TXTMI_Controle                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_Codigo                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_MovimID                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_MovimNiv                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_MovimCod                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_Empresa                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_SerNF                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  TXTMI_nNF                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_Qtde                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  TXTMI_PercRendim               double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  Grandeza                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_M2Virtual                double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  SerieNF_TMI                    varchar(255)                               ,');
  Qry.SQL.Add('  SerieNF_CMO                    varchar(255)                               ,');
  Qry.SQL.Add('  SerieNF_RMP                    varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
//  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXMOEnvCTeGer(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  TipoFrete                      varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  Tabela                         varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SerCT                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  nCT                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesTrKg                        double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CusTrKg                        double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXMOEnvNFeGer(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXTMI_MovimCod                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TipoFrete                      varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  Tabela                         varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SerNF                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  nNF                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SdoQtde                        double(15,3) NOT NULL  DEFAULT "0.000"      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
}

procedure TCreateTX.Cria_ntrttTXMovImp1(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  ValorT                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbVrtQtd                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CliStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Status                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(255)                               ,');
  Qry.SQL.Add('  NO_CLISTAT                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  PalStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  CouNiv2                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CouNiv1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CouNiv2                     varchar(60)                                ,');
  Qry.SQL.Add('  NO_CouNiv1                     varchar(60)                                ,');
  Qry.SQL.Add('  FatorInt                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEC                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(30)                                ,');
  Qry.SQL.Add('  NO_MovimNiv                    varchar(40)                                ,');
  //
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  //
  Qry.SQL.Add('  StatPall                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StatPall                    varchar(60)                                ,');
  //
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenCad                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StqCenCad                   varchar(50)                                ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_LOC_CEN                     varchar(120)                               ,');
  Qry.SQL.Add('  Historico                      varchar(255)                               ,');
  Qry.SQL.Add('  TXMulFrnCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MulFornece                     int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  ClientMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ID_UNQ                         varchar(60)                                ,');
  Qry.SQL.Add('  Grandeza                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_ClientMO                    varchar(100)                               ,');
  Qry.SQL.Add('  NO_FornecMO                    varchar(100)                               ,');
  //
  Qry.SQL.Add('  SerieTal                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Talao                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_SerieTal                    varchar(60)                                ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
procedure TCreateTX.Cria_ntrttTXMovImp2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  OrdGrupSeq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  //
  Qry.SQL.Add('  ValorT                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(160)                               ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(20)                                ,');
  //
  Qry.SQL.Add('  AcumQtde                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumValorT                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  //
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  ;;
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXMovImp3(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  ValorT                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
}

procedure TCreateTX.Cria_ntrttTXMovImp4(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  ValorT                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbVrtQtd                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CliStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Status                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_CLISTAT                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  PalStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  CouNiv2                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CouNiv1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CouNiv2                     varchar(60)                                ,');
  Qry.SQL.Add('  NO_CouNiv1                     varchar(60)                                ,');
  Qry.SQL.Add('  FatorInt                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEC                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(30)                                ,');
  Qry.SQL.Add('  NO_MovimNiv                    varchar(40)                                ,');
  //
  Qry.SQL.Add('  SerieTal                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_SerieTal                    varchar(60)                                ,');
  Qry.SQL.Add('  Talao                          int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  StatPall                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StatPall                    varchar(60)                                ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  StqCenCad                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StqCenCad                   varchar(50)                                ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StqCenLoc                   varchar(50)                                ,');
  //
(* Quando mudar aqui, alterar....:
  - TUnTX_PF.PesquisaPallets(
*)

  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
procedure TCreateTX.Cria_ntrttTXMovIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOUni                     double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGer                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAnt                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
(*
  Qry.SQL.Add('  Lk                             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date                                       ,');
  Qry.SQL.Add('  UserCad                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  UserAlt                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
*)
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXPsqSeqCac(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  TXPallet                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Itens                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Posicao                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (TXPallet, Posicao)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXSeqIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Data                           date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOUni                     double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGer                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAnt                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"        ,');
  //
  Qry.SQL.Add('  Inteiros                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumInteir                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Sequencia                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IMEI_Src                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Operacao                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IMEI_Ord                       int(11)      NOT NULL  DEFAULT "0"         ,');
(*
  Qry.SQL.Add('  Lk                             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date                                       ,');
  Qry.SQL.Add('  UserCad                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  UserAlt                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
*)
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle, GraGruY)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXUsoPQ_TMI(Qry: TmySQLQuery);
begin
  // estoque: _txmovimp1_
  Qry.SQL.Add('  Estq_MovID                     int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_MovNiv                    int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_IMEI                      int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_Qtde                      double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Estq_VrtQt                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Estq_ClientMO                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_FornecMO                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_StqCenLoc                 int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Targ_ProcIMEI                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Targ_ProcQtde                  double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Targ_OriIMEI                   int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Targ_OriQtde                   double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Targ_Fator                     double(15,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Proc_ProcIMEI                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Proc_ProcQtde                  double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Proc_OriIMEI                   int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Proc_OriQtde                   double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Proc_Fator                     double(15,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Targ_Perc                      double(15,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Gerl_Fator                     double(15,12)          DEFAULT "0.000000000000",');
  // Baixa: emit * emitcus * pqx:
  Qry.SQL.Add('  Setor                          tinyint(4)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMESETOR                      varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  PercTotCus                     double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  TXMovIts                       int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  OrigemCodi                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  OrigemCtrl                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Tipo                           mediumint(4) NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliOrig                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  CliDest                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Insumo                         int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  PesoTotal                      double(24,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorTotal                     double(24,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoLote                       double(24,5)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorLote                      double(24,4)           DEFAULT "0.0000"     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXTMIClaSdo(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  TMI_Baix                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXPaClaIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  BxaQtde                        double(20,3)                               ,');
  Qry.SQL.Add('  TMI_Qtde                       double(20,3)                               ,');
  Qry.SQL.Add('  Saldo                          double(20,3)                               ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTxTmiPsq1(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTxTmiPsq2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXEstqPQ(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  IQ                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMEFO                         varchar(100)                               ,');
  Qry.SQL.Add('  NOMECI                         varchar(100)                               ,');
  Qry.SQL.Add('  Setor                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMESE                         varchar(20)                                ,');
  Qry.SQL.Add('  PQ                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMEPQ                         varchar(50)                                ,');
  Qry.SQL.Add('  CtrlPqCli                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CI                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CodProprio                     varchar(20)                                ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "-11"       ,');
  Qry.SQL.Add('  PclPeso                        double(25,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PclValor                       double(25,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  uTxPeso                        double(25,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  uTxValor                       double(25,6) NOT NULL  DEFAULT "0.000000"  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
}

procedure TCreateTX.Cria_ntrttTmiEstqEmTmi(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOUni                     double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGer                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAnt                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"        ,');
  Qry.SQL.Add('  Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsLib                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMorCab                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulFrnCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
//
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  SerieTal                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Talao                          int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTmiEstqEmTmiPosit(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)                                    ,');
  Qry.SQL.Add('  GraGruX                        int(11)                                    ,');
  Qry.SQL.Add('  Qtde                           double(20,3)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,2)                               ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(20,3)                               ,');
  Qry.SQL.Add('  PalVrtQtd                      double(20,3)                               ,');
  Qry.SQL.Add('  LmbVrtQtd                      double(20,3)                               ,');
  Qry.SQL.Add('  GraGru1                        int(11)                                    ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)                                    ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)                                    ,');
  Qry.SQL.Add('  CliStat                        int(11)                                    ,');
  Qry.SQL.Add('  Status                         int(11)                                    ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(255)                               ,');
  Qry.SQL.Add('  NO_CLISTAT                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  OrdGGX                         int(11)                                    ,');
  Qry.SQL.Add('  OrdGGY                         int(11)                                    ,');
  Qry.SQL.Add('  GraGruY                        int(11)                                    ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  Qry.SQL.Add('  PalStat                        int(11)                                    ,');
  Qry.SQL.Add('  NO_PalStat                     varchar(60)                                ,');
  Qry.SQL.Add('  NO_MovimNiv                    varchar(40)                                ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(30)                                ,');
  Qry.SQL.Add('  MovimNiv                       int(11)                                    ,');
  Qry.SQL.Add('  MovimID                        int(11)                                    ,');
  Qry.SQL.Add('  IMEC                           int(11)                                    ,');
  Qry.SQL.Add('  Codigo                         int(11)                                    ,');
  Qry.SQL.Add('  IMEI                           int(11)                                    ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  Inteiros                       double(20,3)                               ,');
  Qry.SQL.Add('  ReqMovEstq                     int(11)                                    ,');
  Qry.SQL.Add('  StqCenCad                      int(11)                                    ,');
  Qry.SQL.Add('  NO_StqCenCad                   varchar(50)                                ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)                                    ,');
  Qry.SQL.Add('  NO_LOC_CEN                     varchar(120)                               ,');
  Qry.SQL.Add('  Historico                      varchar(255)                               ,');
  Qry.SQL.Add('  TXMulFrnCab                    int(11)                                    ,');
  Qry.SQL.Add('  MulFornece                     int(11)                                    ,');
  Qry.SQL.Add('  NO_MulFornece                  varchar(10)  NOT NULL                      ,');
  //
  Qry.SQL.Add('  CouNiv2                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CouNiv1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CouNiv2                     varchar(60)                                ,');
  Qry.SQL.Add('  NO_CouNiv1                     varchar(60)                                ,');
  //
  Qry.SQL.Add('  ClientMO                       int(11)                                    ,');
  Qry.SQL.Add('  ID_UNQ                         varchar(60)                                ,');
  //
  Qry.SQL.Add('  Grandeza                       tinyint(1)                                 ,');
  Qry.SQL.Add('  GraGruValU                     double(15,7)                               ,');
  Qry.SQL.Add('  GraGruValT                     double(15,2)                               ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeAgrup                       varchar(60)  NOT NULL  DEFAULT "????"      ,');
  //
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)                                    ,');
  Qry.SQL.Add('  NO_ClientMO                    varchar(100)                               ,');
  Qry.SQL.Add('  NO_FornecMO                    varchar(100)                               ,');
  //
  //
  Qry.SQL.Add('  SerieTal                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Talao                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_SerieTal                    varchar(60)                                ,');
  //
  Qry.SQL.Add('  Ativo                          int(4)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
procedure TCreateTX.Cria_ntrttTMIQtdEnvETMI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,2)                               ,');
  Qry.SQL.Add('  NO_PRDA_TAM_COR                varchar(255)                               ,');
  Qry.SQL.Add('  IDItem                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTMIQtdEnvGTMI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,2)                               ,');
  Qry.SQL.Add('  NO_PRDA_TAM_COR                varchar(255)                               ,');
  Qry.SQL.Add('  IDItem                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTMIQtdEnvRTMI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  TXMOEnvEnv                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,2)                               ,');
  Qry.SQL.Add('  IDItem                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (TXMOEnvEnv)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXCacGBY(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CacCod                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CacID                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXPallet                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TMI_Sorc                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TMI_Dest                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TMI_Baix                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Box                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Revisor                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Digitador                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Martelo                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3)                               ,');
  Qry.SQL.Add('  Tamanho                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXCacIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CacCod                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CacID                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClaAPalOri                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RclAPalOri                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RclAPalDst                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXPaClaIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXPaRclIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXPallet                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TMI_Sorc                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TMI_Baix                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TMI_Dest                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Box                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Revisor                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Digitador                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Sumido                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  Martelo                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXComparaCacIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  A r e a M 2                    double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PalletA                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PalletB                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (A r e a M 2)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXFluxIncon(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Inteiros                       double(20,3)                               ,');
  Qry.SQL.Add('  LastDtHr                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  //
  Qry.SQL.Add('  IMEI_Src                       int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Operacao                       int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Int_Pos                        double(20,3)                               ,');
  Qry.SQL.Add('  Int_Neg                        double(20,3)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
}

procedure TCreateTX.Cria_ntrttTXImpIMEI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  ValorT                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  Cliente                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CLIENTE                     varchar(100)                               ,');
  Qry.SQL.Add('  Fornece                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                      varchar(100)                               ,');
  Qry.SQL.Add('  Status                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  PedItsLib                      int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
procedure TCreateTX.Cria_ntrttTXImpRendIMEI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  IMEIIni                        int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Seq                            int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Level1                         int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Level2                         int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Level3                         int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Qtde                           double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtQtd                      double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGer                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAnt                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulFrnCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       int(11)      NOT NULL  DEFAULT "-11"       ,');
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TXMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcMovNiv                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcQtd                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PrcKnd                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcRend                        double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Lvl1Ant                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          int(1)       NOT NULL  DEFAULT "0"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXImpRoma(Qry: TmySQLQuery);
begin
//
end;

procedure TCreateTX.Cria_ntrttTXLstPalBox(Qry: TmySQLQuery);
begin
////////////////////////////////////////////////////////////////////////////////
  EXIT;
////////////////////////////////////////////////////////////////////////////////
  Qry.SQL.Add('  MontPalt                       int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateTX.Cria_ntrttTXLstPalGhost(Qry: TmySQLQuery);
begin
////////////////////////////////////////////////////////////////////////////////
  EXIT;
////////////////////////////////////////////////////////////////////////////////
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Qtde                           double(20,3) NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
}

function TCreateTX.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrttTXMovImp1:         Nome := Lowercase('_TXMovImp1_');
{
      ntrttTXMovImp2:         Nome := Lowercase('_TXMovImp2_');
      ntrttTXMovImp3:         Nome := Lowercase('_TXMovImp3_');
}
      ntrttTXMovImp4:         Nome := Lowercase('_TXMovImp4_');
      ntrttTXImpIMEI:         Nome := Lowercase('_TXImpIMEI_');
{
      ntrttTXImpRoma:         Nome := Lowercase('_TXImpRoma_');
      ntrttTXCacIts:          Nome := Lowercase('_TXCacIts_');
      ntrttTXMovIts:          Nome := Lowercase('_TXMovIts_');
      ntrttTXCacGBY:          Nome := Lowercase('_TXCacGBY_');
      ntrttTXLstPalBox:       Nome := Lowercase('_TXLstPalBox_');
      ntrttTXLstPalGhost:     Nome := Lowercase('_TXLstPalGhost_');
      ntrttTXTMIClaSdo:       Nome := Lowercase('_TXTMIClaSdo_');
}
      ntrttTXVerifIMEI:       Nome := Lowercase('_TXVerifIMEI_');
{
      ntrttTXSeqIts:          Nome := Lowercase('_TXSeqIts_');
      ntrttTXFluxIncon:       Nome := Lowercase('_TXFluxIncon_');
      ntrttTXPsqSeqCac:       Nome := Lowercase('_TXPsqSeqCac_');
      ntrttTxTmiPsq1:         Nome := Lowercase('_TxTmiPsq1_');
      ntrttTxTmiPsq2:         Nome := Lowercase('_TxTmiPsq2_');
}
      ntrttTmiEstqEmTmi:      Nome := Lowercase('_tmi_estq_em_tmi_');
      ntrttTmiEstqEmTmiPosit: Nome := Lowercase('_tmi_estq_em_tmi_posit_');
{
      ntrttTXImpRendIMEI:     Nome := Lowercase('_tx_imp_rend_imei_');
      ntrttTXComparaCacIts:   Nome := Lowercase('_Compara_CacIts_');
      ntrttTXMO:              Nome := Lowercase('_TX_MO_');
      ntrttTXUsoPQ_TMI:       Nome := Lowercase('_TX_Uso_PQ_TMI_');
      ntrttTXEstqPQ:          Nome := Lowercase('_tx_estq_pq_');
      ntrttTMIQtdEnvETMI:     Nome := Lowercase('_tmi_qtd_env_etmi');
      ntrttTMIQtdEnvGTMI:     Nome := Lowercase('_tmi_qtd_env_gtmi');
      ntrttTMIQtdEnvRTMI:     Nome := Lowercase('_tmi_qtd_env_rtmi');
      ntrttTXMOEnvCTeGer:     Nome := Lowercase('_tx_mo_env_cte_ger');
      ntrttTXMOEnvNFeGer:     Nome := Lowercase('_tx_mo_env_nfe_ger');
}
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrttTXMovImp1:         Cria_ntrttTXMovImp1(Qry);
{
    ntrttTXMovImp2:         Cria_ntrttTXMovImp2(Qry);
    ntrttTXMovImp3:         Cria_ntrttTXMovImp3(Qry);
}
    ntrttTXMovImp4:         Cria_ntrttTXMovImp4(Qry);
    ntrttTXImpIMEI:         Cria_ntrttTXImpIMEI(Qry); // C�pia do TXMovImp4
{
    ntrttTXCacIts:          Cria_ntrttTXCacIts(Qry);
    ntrttTXMovIts:          Cria_ntrttTXMovIts(Qry);
    ntrttTXCacGBY:          Cria_ntrttTXCacGBY(Qry);
    ntrttTXLstPalBox:       Cria_ntrttTXLstPalBox(Qry);
    ntrttTXLstPalGhost:     Cria_ntrttTXLstPalGhost(Qry);
    ntrttTXTMIClaSdo:       Cria_ntrttTXTMIClaSdo(Qry);
}
    ntrttTXVerifIMEI:       Cria_ntrttTXVerifIMEI(Qry);
{
    ntrttTXSeqIts:          Cria_ntrttTXSeqIts(Qry);
    ntrttTXFluxIncon:       Cria_ntrttTXFluxIncon(Qry);
    ntrttTXPsqSeqCac:       Cria_ntrttTXPsqSeqCac(Qry);
    ntrttTxTmiPsq1:         Cria_ntrttTxTmiPsq1(Qry);
    ntrttTxTmiPsq2:         Cria_ntrttTxTmiPsq2(Qry);
}
    ntrttTmiEstqEmTmi:      Cria_ntrttTmiEstqEmTmi(Qry);
    ntrttTmiEstqEmTmiPosit: Cria_ntrttTmiEstqEmTmiPosit(Qry);
{
    ntrttTXImpRendIMEI:     Cria_ntrttTXImpRendIMEI(Qry);
    ntrttTXComparaCacIts:   Cria_ntrttTXComparaCacIts(Qry);
    ntrttTXMO:              Cria_ntrttTXMO(Qry);
    ntrttTXUsoPQ_TMI:       Cria_ntrttTXUsoPQ_TMI(Qry);
    ntrttTXEstqPQ:          Cria_ntrttTXEstqPQ(Qry);
    ntrttTMIQtdEnvETMI:     Cria_ntrttTMIQtdEnvETMI(Qry);
    ntrttTMIQtdEnvGTMI:     Cria_ntrttTMIQtdEnvGTMI(Qry);
    ntrttTMIQtdEnvRTMI:     Cria_ntrttTMIQtdEnvRTMI(Qry);
    ntrttTXMOEnvCTeGer:     Cria_ntrttTXMOEnvCTeGer(Qry);
    ntrttTXMOEnvNFeGer:     Cria_ntrttTXMOEnvNFeGer(Qry);
}
    //
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.

