unit UnTX_Tabs;
// TX - Controle de estoque para Bloco K - SPED
{ Colocar no MyListas:

Uses UnTX_Tabs;


//
function TMyListas.CriaListaTabelas:
      TX_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    TX_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    TX_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      TX_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      TX_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    TX_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  TX_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums,
  UnProjGroup_Consts, AppListas, MyListas, UnGrl_Vars;

type
  TUnTX_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(TabelaBase: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>;
             CollectDataServerRepository: Boolean = False): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
             FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  TX_Tabs: TUnTX_Tabs;

implementation

uses UMySQLModule;


function TUnTX_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
const
  QeiLnk = True;
  NoCRCTM = TCRCTableManage.crctmIndef;
var
  CDR: Boolean;
begin
  try
{
    if CO_SIGLA_APP = 'CLRC' then
    begin
      CDR := VAR_IS_CollectDataServerRepository; // or (CO_SIGLA_APP = 'BDER');
//////  C A D A S T R O S   I M P R E S C I N D � V E I S  //////////////////////////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv1'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv2'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('GraGruXCou'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Operacoes'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXNatArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCadNat'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRibCad'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRibCla'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXSerFch'), '_lst_sample');   //   - S�ries de Fichas
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPalSta'), '_lst_sample');   //   - Status do Pallet
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMrtCad'), '_lst_sample');   //   - Martelos de procedencia
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovimID'), '');
//////  C A D A S T R O S   D E   C O N F I G U R A � � O   ( A U X I L I A R ) /////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCOPCab'), ''); // Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCOPIts'), ''); // WB de origem de Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPMOCab'), (''));
//////  ? ? ? ? ? ? ? ? ? ///////////////////////////////////////////////////////////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXEntiMP'), 'EntiMP', False, CDR);
//////  M O V I M E N T O ///////////////////////////////////////////////////////////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPalletA'), Lowercase(''), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase(CO_TAB_TAB_TMI), Lowercase(CO_TAB_TAB_TMI), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovCab'), Lowercase(''), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInnCab'), Lowercase('WBInnCab'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInnNFs'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovIDLoc'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXESCCab'), '', False, CDR); // ESC = Entrada sem Cobertura
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXSSCCab'), Lowercase('TXAjsCab'), False, CDR); // SSC = Sa�da sem Cobertura
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXGerArtA'), 'TXGerArt', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXGerRclA'), 'TXGerRcl', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPrePalCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaClaCabA'), 'TXPaClaCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaClaItsA'), 'TXPaClaIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaRclCabA'), 'TXPaRclCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaRclItsA'), 'TXPaClaIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCacCab'), Lowercase('TXMovCab'), False, CDR); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCacItsA'), Lowercase('TXCacIts'), False, CDR); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulCabA'), 'TXPaMulCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulItsA'), 'TXPaMulIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulCabR'), 'TXPaMulCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulItsR'), 'TXPaMulIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovDif'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXFchRMPCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMulFrnCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMulFrnIts'), '', False, CDR);
//////  M O V I M E N T O (Compatibilidade! n�o implementado!) /////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXOutNFeCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXOutNFeIts'), '', False, CDR);
      // 2018-03-25
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPlCCab'), Lowercase('WBInnCab'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXIndCab'), '', False, CDR); // Operacao de Artigo (Classificado) com Gera��o de subprodutos
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPWECab'), 'TXIndCab', False, CDR); // Processo de WetEnd
      // Fim 2018-03-25
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXOutCab'), Lowercase('WBOutCab'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXTrfLocCab'), '', False, CDR);

//////  A R Q U I V O   M O R T O  /////////////////////////////////////////////////////////////////////////////////////
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovItb'), Lowercase(CO_TAB_TAB_TMI));
      MyLinguas.AdTbLst(Lista, True, Lowercase('TXMovItz'), Lowercase(CO_TAB_TAB_TMI));
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCacItsB'), Lowercase('TXCacIts')); // Cac  > Couro a couro
    end else
    begin
}
      CDR := False; // VAR_IS_CollectDataServerRepository;
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('GraGruXCou'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv1'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv2'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Operacoes'), '');
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('EntiMP'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXAllGGX'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXAjsCab'), '');
{
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXBalCab'), '');  Mudado para SPEDEFDEnce!
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXBalEmp'), '');  Mudado para SPEDEFDEnce!
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXArtCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXArtGGX'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXBxaCab'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCacCab'), Lowercase('TXMovCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCacItsA'), Lowercase('TXCacIts'), False, CDR, TCRCTableManage.crctmCRCUpNotSyncToward); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCacItsB'), Lowercase('TXCacIts')); // Cac  > Couro a couro
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCfgEqzCb'), ''); // config de equalize Cabecalho
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCfgEqzIt'), ''); // config de equalize Itens
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCGICab'), ''); // CGI > Classes Geradas de IME-I
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCGIIts'), ''); // CGI > Classes Geradas de IME-I
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCPMRSBCb'), ''); // Config. Plano metas rendim. subprodutos
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCPMRSBIt'), ''); // Config. Plano metas rendim. subprodutos
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXDSCCab'), 'WBRclCab'); // Processo de WetEnd
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXDsnCab'), ''); // Dsn > Desnate
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXDsnArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXDsnIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXDsnSub'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXEntiMP'), 'EntiMP', False, CDR, TCRCTableManage.crctmAllUpAndSyncOnlyIns);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXEqzCab'), ''); // Eqz> Equ�ize
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXEqzArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXEqzIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXEqzSub'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXExBCab'), '');
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXFchRMPCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXFchRslCus'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXFchRslCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXFchRslIts'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('TXGruGGX'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('TXESCCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // ESC = Entrada sem Cobertura
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovimID'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovIDLoc'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMulFrnCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMulFrnIts'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMulNFeCab'), 'TXMulFrnCab');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMulNFeIts'), '');
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXNatArt'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCadNat'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCadInd'), 'TXCadNat');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCadInt'), 'TXCadNat');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCadFcc'), 'TXCadNat');
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXNatPDA'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXProCal'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCouCal'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCouDTA'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXProCur'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCouCur'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRibCad'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRibArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRibCla'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRibOpe'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXSubPrd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPSPPro'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPSPEnd'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXTrfLocCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXWetEnd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXFinCla'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXClaWet'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCOPCab'), ''); // Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCOPIts'), ''); // WB de origem de Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMOEnvRet'), (''));
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase(CO_TAB_TAB_TMI), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward, 'TXMovItZ');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovItb'), Lowercase(CO_TAB_TAB_TMI)); J� chamado acima em arquivo morto
{
      MyLinguas.AdTbLst(Lista, False, Lowercase(CO_TAB_TAB_CRCI), Lowercase(CO_TAB_TAB_TMI));
      MyLinguas.AdTbLst(Lista, True, Lowercase('TXMovItz'), Lowercase(CO_TAB_TAB_TMI), False, CDR, TCRCTableManage.crctmCRCUpAndSyncDelSelf);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovDif'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXHisFch'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInnCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInnNFs'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInvNFe'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMotivBxa'), '_lst_sample');   //   - Motivos de Baixa Extravio
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMorCab'), '');   //   - Arquivo morto TX
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMrtCad'), '_lst_sample');   //   - Martelos de procedencia
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXObsNFx'), '');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXIndCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // Operacao de Artigo (Classificado) com Gera��o de subprodutos
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXOpeSeq'), '');
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXOutCab'), Lowercase('WBOutCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXOutNFeCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXOutNFeIts'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXOutNFI'), '');
}

      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPalletA'), Lowercase('TXPallet'), False, CDR, TCRCTableManage.crctmAllUpAndSyncToward);
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPalFrn'), Lowercase('WBPalFrn')); // Vai usar?
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPalArt'), Lowercase('WBPalArt'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXProQui'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPSPCab'), 'TXIndCab'); // Processo de Subproduto
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPWECab'), 'TXIndCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // Processo de WetEnd
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRRMCab'), 'TXIndCab'); // Reprocesso/Reparo de  Mercadoria
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCalCab'), 'TXIndCab'); // Processo de Caleiro
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCalJmp'), 'TXIndCab'); // Couro Caleirado
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCalPDA'), 'TXIndCab'); // Pr�-descarne
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCalDTA'), 'TXIndCab'); // Redescarne / divisao
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCurCab'), 'TXIndCab'); // Processo de Curtimento
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXCurJmp'), 'TXIndCab'); // Couro Curtido
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXGerArtA'), 'TXGerArt', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXGerRclA'), 'TXGerRcl', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXGerAPalA'), Lowercase('TXGerAPal'));
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXGerAPalB'), Lowercase('TXGerAPal'));
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPalSta'), '_lst_sample');   //   - Status do Pallet
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaClaCabA'), 'TXPaClaCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaClaItsA'), 'TXPaClaIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
{
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXRclAPalA'), Lowercase('TXRclAPal'));
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXRclAPalB'), Lowercase('TXRclAPal'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulCabA'), 'TXPaMulCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulItsA'), 'TXPaMulIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulCabR'), 'TXPaMulCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaMulItsR'), 'TXPaMulIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRclCab'), 'WBRclCab');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaRclCabA'), 'TXPaRclCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaRclItsA'), 'TXPaClaIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPaRclBxaA'), 'TXPaRclBxa');
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPlCCab'), Lowercase('TXInnCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPMOCab'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXDvlCab'), Lowercase('WBInnCab'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXRtbCab'), Lowercase('WBInnCab'));
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPedCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPedIts'), '');
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXPrePalCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXSerFch'), '_lst_sample');   //   - S�ries de Fichas
}
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXSubPrdCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXSerTal'), '_lst_sample');   //   - S�ries de Tal�es
{
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXReqDiv'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXReqMov'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInfInn'), 'TXReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInfMov'), 'TXReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInfOut'), 'TXReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXLstPal'), 'TXReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInfPal'), 'TXReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXInfIEC'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXVmcWrn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXLoadCRCCab'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('TXLoadCRCIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXLoadCRCTbs'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXLoadCRCWrn'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBInnCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBArtCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('TXMovCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase(CO_TAB_TAB_TMI), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBMPrCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBMPrFrn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBOutCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBPallet'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBPalFrn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBPalArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBRclCab'), '');
    end;
}
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnTX_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
var
  I: Integer;
begin
{
separador = "|"
}
  Result := True;
(*
  if Uppercase(Tabela) = Uppercase('TXCtrl') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end;
*)
  if Uppercase(Tabela) = Uppercase('CouNiv1') then
  begin
    FListaSQL.Add('Codigo|Nome|FatorInt');
    FListaSQL.Add('0|"Indefinido"|0.000');
    FListaSQL.Add('1|"Inteiro"|1.000');
(*
    FListaSQL.Add('2|"Meios"|0.500');
    FListaSQL.Add('3|"Cabe�a"|0.3000');
    FListaSQL.Add('4|"Culatra"|0.700');
    FListaSQL.Add('5|"Barrigas"|0.250');
    FListaSQL.Add('6|"Grup�o"|0.400');
    FListaSQL.Add('7|"Drops"|1.000');
    FListaSQL.Add('8|"Recortes"|0.000');
    FListaSQL.Add('9|"Retalhos"|0.000');
    FListaSQL.Add('10|"Carna�a"|0.000');
    FListaSQL.Add('11|"Sebo"|0.000');
*)
  end else if Uppercase(Tabela) = Uppercase('Operacoes') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"(N�O DEFINIDA)"');
{
  end else
  if Uppercase(Tabela) = Uppercase('TXPalSta') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"Bloqueado"');
    FListaSQL.Add(Geral.FF0(CO_STAT_TXPALLET_0100_DISPONIVEL) + '|"Dispon�vel"');
    FListaSQL.Add(Geral.FF0(CO_STAT_TXPALLET_0300_INDICADO)   + '|"Indicado"');
    FListaSQL.Add(Geral.FF0(CO_STAT_TXPALLET_0500_RESERVADO)  + '|"Reservado"');
    FListaSQL.Add(Geral.FF0(CO_STAT_TXPALLET_0900_PROMETIDO)  + '|"Prometido"');
  end else
  if Uppercase(Tabela) = Uppercase('TXMovimID') then
  begin
    FListaSQL.Add('Codigo|Nome');
    for I := 0 to MaxEstqMovimID do
      if sEstqMovimID[I] <> '' then
        FListaSQL.Add(Geral.FF0(Integer(TEstqMovimID(I))) + '|"' + sEstqMovimID[I] + '"');
}
  end else
  if Uppercase(Tabela) = Uppercase('TXMulFrnCab') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('-1|"Saldo inicial"');
  end else
  if Uppercase(Tabela) = Uppercase('TXSerTal') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('-1|""');
{
  end else
  if Uppercase(Tabela) = Uppercase('TXVmcWrn') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"OK"');
    FListaSQL.Add('16|"Info faltando"');
    FListaSQL.Add('32|"Info parcial"');
    FListaSQL.Add('48|"Info dubia"');
    FListaSQL.Add('64|"Info errada"');
}
  end;
end;

function TUnTX_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
end;


function TUnTX_Tabs.CarregaListaFRIndices(TabelaBase: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>; CollectDataServerRepository: Boolean): Boolean;
begin
  Result := True;
  //
  if Uppercase(TabelaBase) = Uppercase('GraGruXCou') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CouNiv1') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(TabelaBase) = Uppercase('CouNiv2') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(TabelaBase) = Uppercase('Operacoes') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
{
  end else if Uppercase(TabelaBase) = Uppercase('EntiMP') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXArtCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXArtGGX') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXBalCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Periodo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXBalEmp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Periodo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXBxaCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCacIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCfgEqzCb') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCfgEqzIt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SubClass';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCGICab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCGIIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCPMRSBCb') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCPMRSBIt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXDsnCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXDsnArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXDsnIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'TXCacCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXDsnSub') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SubClass';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXEqzCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXEqzIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Pallet';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXEqzSub') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SubClass';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXESCCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXExBCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
{
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('TXFchRslCus') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('TXFchRMPCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'MovimID';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXFchRslCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXFchRslIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXInfIEC') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXInnNFs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXInvNFe') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMorCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('TXObsNFx') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCadNat') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXNatPDA') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXProCal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCouCal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCouDTA') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXProCur') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCouCur') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  (*
  end else
  if Uppercase(TabelaBase) = Uppercase('TXAllGGX') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  *)
  end else
  if Uppercase(TabelaBase) = Uppercase('TXGruGGX') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMovimID') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMovIDLoc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'StqCenLoc';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMulFrnCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMulFrnIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMulNFeIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXNatArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXRibCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXRibArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXRibCla') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXRibOpe') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXSubPrd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPSPPro') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPSPEnd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXTrfLocCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXWetEnd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXFinCla') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXClaWet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCOPCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXCOPIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMOEnvRet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMovDif') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXHisFch') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXIndCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXGerArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXGerAPal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPaClaCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'TXGerArt';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := CO_FLD_TAB_TMI;
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'TXESCCab';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPaClaIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'TMI_Baix';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'TMI_Dest';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE2';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'TMI_Dest';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPaMulCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPaMulIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPaRclCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'TXGerRcl';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := CO_FLD_TAB_TMI;
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPaRclBxa') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'TMI_Baix';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPrePalCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXGerRcl') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPMOCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPedCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPedIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXRclAPal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXSubPrdCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXReqDiv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXReqMov') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXOpeSeq') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('TXOutNFeCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'ide_serie';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'ide_nNF';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXOutNFeIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('TXOutNFI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'ItemNFe';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('TXProQui') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SetorID';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXVmcWrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXVmcWrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXLoadCRCCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXLoadCRCIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXLoadCRCTbs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXLoadCRCWrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBArtCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXAjsCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXInnCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('TXMovCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase(CO_TAB_TAB_TMI) then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('WBMPrCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBMPrFrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Fornece';
    FLIndices.Add(FRIndices);
    //
}
  end else
  if Uppercase(TabelaBase) = Uppercase('TXPallet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
{
  end else
  if Uppercase(TabelaBase) = Uppercase('WBPalFrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBPalArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBOutCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBRclCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
}
  end;
end;

function TUnTX_Tabs.CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := False; // mudar para true
  //
{
  if Uppercase(TabelaCria) = Uppercase('TXCacCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CodigoID';
    FRQeiLnk.RplyTab       := CO_TXXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXEntiMP') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPSrvrIncOver;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MPVImpOpc';
    FRQeiLnk.RplyTab       := 'MPVImpOpc';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
  end else
  if Uppercase(TabelaCria) = Uppercase('TXEscCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXFchRMPCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'SerieFch';
    FRQeiLnk.RplyTab       := 'TXSerFch';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncRelatSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Ficha';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimID';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXGerArtA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'TXCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXGerRclA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'TXCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
(*
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TXPallet';
    FRQeiLnk.RplyTab       := 'TXPalletA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
*)
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXInnCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXInnNFs') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'TXInnCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'TXMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Conta';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXMovCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CodigoID';
    FRQeiLnk.RplyTab       := CO_TXXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXMovDif') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'TXMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncRelatSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXMovIDLoc') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXMovIts') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := CO_TXXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimTwn';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'MovimTwn';
    FRQeiLnk.MTbsCol       := 'TXMovimTwn';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncOrfao;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'SrcNivel1';
    FRQeiLnk.RplyTab       := CO_TXXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'SrcMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'SrcNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'DstNivel1';
    FRQeiLnk.RplyTab       := CO_TXXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'DstMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'DstNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPInnNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPArtNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'JmpNivel1';
    FRQeiLnk.RplyTab       := CO_TXXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'JmpMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'JmpNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'RmsNivel1';
    FRQeiLnk.RplyTab       := CO_TXXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'RmsMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'RmsNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPSrcNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPJmpNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovCodPai';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXMovItz') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'TXMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRDeleteSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXMulFrnCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovTypeCod';
    FRQeiLnk.RplyTab       := 'TEstqDefMulFldTX';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimType';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXMulFrnIts') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'TXMulFrnCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXIndCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'TXCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'EmitGru';
    FRQeiLnk.RplyTab       := 'EmitGru';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXOutCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXOutNFeCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'OriCod';
    FRQeiLnk.RplyTab       := 'TXOutCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXOutNFeIts') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'TXOutNFeIts';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXPaClaCabA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TXGerArt';
    FRQeiLnk.RplyTab       := 'TXGerArtA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TXMovIts';
    FRQeiLnk.RplyTab       := 'TXMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'TXCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if (Uppercase(TabelaCria) = Uppercase('TXPaMulCabA'))
  or (Uppercase(TabelaCria) = Uppercase('TXPaMulCabR')) then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'TXCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TMI_Sorc';
    FRQeiLnk.RplyTab       := 'TXMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TXGerArt';
    FRQeiLnk.RplyTab       := 'TXGerArtA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if (Uppercase(TabelaCria) = Uppercase('TXPaMulItsA'))
  or (Uppercase(TabelaCria) = Uppercase('TXPaMulItsR')) then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := Geral.Substitui(Lowercase(TabelaCria), 'its', 'cab');
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TMI_Sorc';
    FRQeiLnk.RplyTab       := 'TXMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TMI_Dest';
    FRQeiLnk.RplyTab       := 'TXMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TMI_Baix';
    FRQeiLnk.RplyTab       := 'TXMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXPaRclCabA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TXGerRcl';
    FRQeiLnk.RplyTab       := 'TXGerRclA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TXMovIts';
    FRQeiLnk.RplyTab       := 'TXMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'TXCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if (Uppercase(TabelaCria) = Uppercase('TXPaClaItsA'))
  or (Uppercase(TabelaCria) = Uppercase('TXPaRclItsA')) then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := Geral.Substitui(Lowercase(TabelaCria), 'its', 'cab');
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TMI_Sorc';
    FRQeiLnk.RplyTab       := CO_TAB_TMI;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TMI_Baix';
    FRQeiLnk.RplyTab       := CO_TAB_TMI;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TMI_Dest';
    FRQeiLnk.RplyTab       := CO_TAB_TMI;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXPalletA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpAllSrvrIncUniq;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GerRclCab';
    FRQeiLnk.RplyTab       := 'TXGerRclA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXPlCCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXPrePalCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
(*
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'TXPallet';
    FRQeiLnk.RplyTab       := 'TXPalletA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
*)
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXPWECab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'TXCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'EmitGru';
    FRQeiLnk.RplyTab       := 'EmitGru';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('TXTrfLocCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'TXMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end;
}
end;

function TUnTX_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
{
    //end else
    if Uppercase(Tabela) = Uppercase('EntiMP') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPSrvrIncOver;
      FLCampos.Add(FRCampos);
      //
      // Limite percentual de quebra de viagem
      New(FRCampos);
      FRCampos.Field      := 'CMP_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '2.5000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPL_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGI_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMI_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      // Indice de devolu��o de quebra de viagem
      // 0-nada, 1-Tudo, 2-Excedente
      New(FRCampos);
      FRCampos.Field      := 'CMP_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPL_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGI_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMI_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLSit'; // Como complementa valores (sobre kg ou pe�as, etc)
      FRCampos.Tipo       := 'tinyint(1)'; // 0-Nota fiscal, 1-Entrada curtume
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaskLetras';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaskFormat';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PreDescarn';
      FRCampos.Tipo       := 'tinyint(1)';  // 0- Curtume 1-N�o 2-J� vem
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      // CMP - Compra da m�t�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //

      // CPL - Complemanro do pagamento da mat�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //

      // AGI - Pagamento de �gio na compra da mat�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPVImpOpc'; // Cadastro Op��es de impress�o de comiss�es
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //

      // CMI - Comiss�o na compra da mat�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contatos';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LocalEntrg';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Corretor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Comissao';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondPagto';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondComis';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescAdiant'; // Valor a descontar (parcelamento de adiantamento)
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abate'; //  0=Matadouro, 1=Frigorifico
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporte';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoIniPele';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoteLetras';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoteFormat';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NotaTX';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SiglaTX';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
    end else
}
    if Uppercase(Tabela) = Uppercase('GraGruXCou') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CouNiv1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CouNiv2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtigoImp';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClasseImp';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevPcPal'; // PPP - Previs�o de pe�as no Pallet
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevAMPal'; // PMP - Previs�o de a r e a em m2 no Pallet
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevKgPal'; // PKP - Previs�o de Kg no Pallet
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMinM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMaxM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMinKg';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMaxKg';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza';
      FRCampos.Tipo       := 'tinyint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Bastidao';
      FRCampos.Tipo       := 'tinyint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXPronto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CouNiv1') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'ND';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorInt';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CouNiv2') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'ND';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Operacoes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RedefnFrmt'; // TTXRedefnFrmt
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXOpeSeq'; // tabela TXOpeSeq.Codigo ou CO_SIMS_...
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else
    if Uppercase(Tabela) = Uppercase('TXArtCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRecu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRefu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiAcab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXtMPs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observa';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TXArtGGX') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TXBalCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXBalEmp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXBxaCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtBaixa';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXCfgEqzCb') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNotZero';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCfgEqzIt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorEqz';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCGICab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desnatador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCGIIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_TMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCPMRSBCb') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataIni';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCPMRSBIt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DivKgSPpcCou';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercKgSPKgCou';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXDsnCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desnatador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNotZero';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXDsnArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Interesse';
      FRCampos.Tipo       := 'tinyint(1)';  // 0=Nao, 1=Sim
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXDsnIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXCacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXDsnSub') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'Mul';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Interesse';
      FRCampos.Tipo       := 'tinyint(1)';  // 0=Nao, 1=Sim
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXEqzCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classificador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNotZero';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXCfgEqzCb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXEqzIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXEqzSub') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'Mul';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Interesse';
      FRCampos.Tipo       := 'tinyint(1)';  // 0=Nao, 1=Sim
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorEqz';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXESCCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'emi_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'emi_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovIDAsc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXExBCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtBaixa';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MotivBxa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXFchRMPCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncRelatSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReInseriu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXFchRslCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmPago';    // 0-N�o info
      FRCampos.Tipo       := 'tinyint(1)'; // 1-K g
      FRCampos.Null       := '';           // 2-P e c a
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoAll';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribtMP';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribtMO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribtAll';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoLiq';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXFchRslIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFretKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpostP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FreteM2';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcMPAG';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFretTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoINTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpostCred';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpostTotV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissTotV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FretM2Tot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BrutoV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MargemV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MargemP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
    end else if Uppercase(Tabela) = Uppercase('TXFchRslCus') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MOKg';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FreteKg';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('TXInfIEC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXInfFol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXInfLin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornecedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXInnNFs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motorista';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'emi_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'emi_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXInvNFe') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovCodOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXMorCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IMEILimite';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaLimite';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
    end else if Uppercase(Tabela) = Uppercase('TXObsNFx') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      /
      New(FRCampos);
      FRCampos.Field      := '';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('TXCadNat') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXNatPDA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXProCal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCouCal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCouDTA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXProCur') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCouCur') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXGruGGX') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(3)'; // 1 Curtido
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXIntegrl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXLaminad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDivTrip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDivCurt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXMovimID') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXMovIDLoc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenLoc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TXMulFrnCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimType';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovTypeCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXMulFrnIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Remistura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodRemix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'TMIOrig';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXMulNFeIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Remistura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodRemix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeSer';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXNatArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXNatCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXRibCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXRibCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXRibArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXRibCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXRibCla';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXRibCla') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'SubProd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // TEstqSubProd
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('TXRibOpe') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXSubPrd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXPSPPro') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXPSPEnd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXTrfLocCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtVenda';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtViagem';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEntrega';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXWetEnd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXFinCla') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXClaWet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXRibCla';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXWetEnd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXCOPCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDst';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ForneceMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenLoc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsLib';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacoes'; // Operacoes.Codigo > para saber o RedefnFrmt (TTXRedefnFrmt)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPMOCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TXCOPIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('TXHisFch') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_TMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXMOEnvRet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_CusMOUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXTMI_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXMovDif') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncRelatSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfQtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifQtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQbrViag';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQbrSal';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoSalKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstCouPc';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstCouKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstCouVl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstSalKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstSalVl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstTotVl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribDefSel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXIndCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FluxoCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClientMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FornecMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenLoc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsLib';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrAberto';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrLibOpe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgOpe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimOpe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeMan';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTMan';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeSrc';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTSrc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeInn';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTInn';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeDst';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTDst';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeBxa';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTBxa';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeSdo';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTSdo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManMP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieRem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeRem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LPFMO';  // Lote de Produ��o do Fornecedor de Mao de Obra
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXCOPCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXGerArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrAberto';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrLibCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdeMan';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManMP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXGerRcl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'DtHrAberto';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'DtHrLibCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'QtdeMan';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXGerAPal') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPaClaIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Box';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Revisor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Digitador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXPaClaCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXGerArt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_TMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal06';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal07';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal08';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal09';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal10';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal11';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal12';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal13';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal14';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal15';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXESCCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXPaClaIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Baix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tecla';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QIt_Sorc'; // Quantidade de TMI_Sorc (pode ser mais de um!)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QIt_Baix'; // Quantidade de TMI_Baix (pode ser mais de um!)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXPaMulCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXGerArt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieRem';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeRem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXPaMulIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Baix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXPaRclCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXGerRcl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_TMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal06';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal07';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal08';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal09';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal10';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal11';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal12';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal13';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal14';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal15';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXPaRclBxa') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Baix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXPrePalCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
}
    end else if Uppercase(Tabela) = Uppercase('TXCacIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod'; // TXCacCab.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacID';  // TXCacCab.MovimID
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // TXPaClaCab.Codigo  ou TXPaRclCab.Codigo !!!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';  // Primary Key
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClaAPalOri'; // TXCacIts[A][B].Controle de Origem para classificacao
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RclAPalOri'; // TXCacIts[A][B].Controle de Origem para reclassificacao
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RclAPalDst'; // ClaAPalOri ou RclAPalOri destinado na reclassificacao
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPaClaIts';  // TXPaClaIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPaRclIts'; // TXPaRclIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPallet'; // TXPallet.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Sorc'; // TXMovIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Baix'; // TXMovIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TMI_Dest'; // TXMovIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Box';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Martelo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Revisor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Digitador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sumido';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmaIns';  // Forma de insercao
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // Manual
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorIntSrc';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorIntDst';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXMorCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Atualizou';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXPMOCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXPedCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vendedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (*New(FRCampos);
      FRCampos.Field      := 'ComissV_Per';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissV_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      (*New(FRCampos);
      FRCampos.Field      := 'DescoExtra';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      (*New(FRCampos);
      FRCampos.Field      := 'Volumes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      (*New(FRCampos);
      FRCampos.Field      := 'Obs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
      New(FRCampos);
      FRCampos.Field      := 'Obz';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedidCli';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXPedIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_TMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoTipo';
      FRCampos.Tipo       := 'tinyint(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoMoeda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoVal';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entrega';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pronto';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoProd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LibQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      New(FRCampos);
      //
      FRCampos.Field      := 'FinQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VdaQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXSubPrdCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXReqDiv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsa';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXReqMov') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsa';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXOpeSeq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('TXOutNFeCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXOutNFeIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpCalcVal';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorU';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribDefSel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
(*
    end else if Uppercase(Tabela) = Uppercase('TXOutNFI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumrNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpCalcVal';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorU';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribDefSel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('TXProQui') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SetorID';
      FRCampos.Tipo       := 'tinyint(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';  // Qtde no setor
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusPQ';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fuloes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MinDta';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaxDta';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXVmcWrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXLoadCRCCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriServrID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXLoadCRCIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // TTXTabelas.txtab....
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Niveis';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXLoadCRCTbs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registros';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXLoadCRCWrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBArtCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRecu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRefu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiAcab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXtMPs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observa';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXAjsCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXInnCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtCompra';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtViagem';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEntrada';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornecedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Procednc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motorista';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'emi_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'emi_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEnceRend';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpEnceRend';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o encerr, 1=Automatico, 2=Manual
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('TXMovCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodigoID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase(CO_TAB_TAB_TMI) then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncOrfao;
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliVenda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_TMI;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoVrtQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'YES';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
}
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieTal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Talao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'Misturou';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'ClientMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoPQ';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FornecMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'CustoMOM2';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'CustoMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorMP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGer';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'QtdGerPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGerArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGerArP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'QtdAnt';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'QtdAntPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdAntArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdAntArP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'AptoUso';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'NotaMPAG';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Marca';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpCalcAuto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';  // O que calculou automatico   -1: nao se sabe 0: Nada 1: P e c as 2: Peso 4: m2 8: p2
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Zerado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0: Nao 1: Sim
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'EmFluxo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';  // 0: Sim 1: Nao
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NotFluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0:  em todos 1: Nao no In Natura
      FRCampos.Extra      := '';   // 2: Nao no Artigo gerado 4: Nao no artigo classif.
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);      // 8: Nao no Artigo em operacao
      //
}
{
      New(FRCampos);
      FRCampos.Field      := 'FatNotaVNC';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNotaVRC';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'PedItsLib';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsFin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsVda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);         // DEPRECADO !!!!
      FRCampos.Field      := 'GSPInnNiv2'; // In Natura de onde tirou o subproduto
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);         // DEPRECADO !!!!
      FRCampos.Field      := 'GSPArtNiv2'; // Artigo Gerado que saiu do In Natura que gerou o produto
      FRCampos.Tipo       := 'int(11)';    // Permitir zero. Pode ter varios !!!
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'ReqMovEstq'; // Requisicao de movimentacao de estoque
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenLoc'; // Local do centro de estoque
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXMorCab';  // Arquivo morto!!! ???
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXMulFrnCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'KgCouPQ';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'NFeSer';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXMulNFeCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXRcl'; // GGX usado no lugar do GraGruX para faturar, etc  EFD Bloco K220!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'JmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPSrcMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPSrcNiv2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPJmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPJmpNiv2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'DtCorrApo'; // Data de corre��o de apontamento!
      FRCampos.Tipo       := 'datetime';   // Registros K270 / K275 do SPED Fiscal!
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovCodPai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IxxMovIX';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IxxFolha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IxxLinha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Iutpei';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtAvuls';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtMOEnv';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtMORet';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('WBMPrCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BRLMedM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBMPrFrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Medio';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else if Uppercase(Tabela) = Uppercase('TXPallet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpAllSrvrIncUniq;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClientMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliStat'; // Cliente referente ao status
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX'; // Artigo Classificado
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrEndAdd'; // Fim da adi��o de couros (encerramento) em classifica��o
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GerRclCab'; // �ltima Reclassifica��o atrelada
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimRcl'; // Fim da adi��o de couros (encerramento) em reclassifica��o
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimIDGer'; // MovimID que gerou o Pallet
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdPrevPc'; // Quantidade previsivel de pe�as
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      //
      New(FRCampos);
      FRCampos.Field      := 'StatPall';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '4'; // Encerrado
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
{
    end else if Uppercase(Tabela) = Uppercase('WBPalArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBPalFrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'M2Medio';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('WBOutCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtVenda';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtViagem';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEntrega';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieV';  // NF Venda
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFV';  // NF Venda
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBRclCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnTX_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'TXPwdDdClas';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPwdDdData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXPwdDdUser';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TXImpRandStr';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NObrigNFeTX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_PDA';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 210/215
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_EmCal';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Caleado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_DTA';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 210/215
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_EmCur';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Curtido';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_ArtCur';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Operacao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 210/215
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Recurt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_SubProd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Reparo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '4'; // 260/265
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (* Foi movido para CMPT_Tabs
      New(FRCampos);
      FRCampos.Field      := 'CMPPVai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPVaiIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPVem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      *)
      //
      New(FRCampos);
      FRCampos.Field      := 'TXImpMediaPall';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // Imprime m�dia no pallet
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnTX_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // FLU-OPERA-001 :: Opera��es de Fluxos
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-OPERA-001';
  FRJanelas.Nome      := 'FmOperacoes';
  FRJanelas.Descricao := 'Opera��es de Fluxos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-001 :: Configura��o de Mat�rias-primas
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-001';
  FRJanelas.Nome      := 'FmTXCadNat';
  FRJanelas.Descricao := 'Configura��o de Mat�rias-primas';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-002 :: Configura��o de Produto em Processo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-002';
  FRJanelas.Nome      := 'FmTXCadInd';
  FRJanelas.Descricao := 'Configura��o de Produto em Processo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-003 :: Configura��o de Produto Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-003';
  FRJanelas.Nome      := 'FmTXCadFcc';
  FRJanelas.Descricao := 'Configura��o de Produto Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-004 :: Configura��o de Produto Intermedi�rio
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-004';
  FRJanelas.Nome      := 'FmTXCadInt';
  FRJanelas.Descricao := 'Configura��o de Produto Intermedi�rio';
  FLJanelas.Add(FRJanelas);
  //
{
  // WET-CURTI-002 :: Configura��o de Artigos de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-002';
  FRJanelas.Nome      := 'FmTXRibCad';
  FRJanelas.Descricao := 'Configura��o de Artigos de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-003 :: Mat�ria-prima In Natura X Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-003';
  FRJanelas.Nome      := 'FmTXNatArt';
  FRJanelas.Descricao := 'Mat�ria-prima In Natura X Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-004 :: Configura��o de Artigos de Ribeira Classificados
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-004';
  FRJanelas.Nome      := 'FmTXRibCla';
  FRJanelas.Descricao := 'Configura��o de Artigos de Ribeira Classificados';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-005 :: Artigo de Ribeira X Artigo de Ribeira Classificado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-005';
  FRJanelas.Nome      := 'FmTXRibArt';
  FRJanelas.Descricao := 'Artigo de Ribeira X Artigo de Ribeira Classificado';
  FLJanelas.Add(FRJanelas);
  //
}
  // TEX-FAXAO-006 :: Entrada de Mat�ria Prima
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-006';
  FRJanelas.Nome      := 'FmTXInnCab';
  FRJanelas.Descricao := 'Entrada de Mat�ria Prima';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-007 :: Item de Entrada de Mat�ria Prima
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-007';
  FRJanelas.Nome      := 'FmTXInnIts';
  FRJanelas.Descricao := 'Item de Entrada de Mat�ria Prima';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-008 :: Gera��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-008';
  FRJanelas.Nome      := 'FmTXGerArtCab';
  FRJanelas.Descricao := 'Gera��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-009 :: Item de Gera��o de Artigo de Ribeira da Barraca
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-009';
  FRJanelas.Nome      := 'FmTXGerArtItsBar]';
  FRJanelas.Descricao := 'Item de Gera��o de Artigo de Ribeira da Barraca';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-010 :: Encerramento de Gera��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-010';
  FRJanelas.Nome      := 'FmTXGerArtEnc';
  FRJanelas.Descricao := 'Libera��o de Artigo de Ribeira para Classificar';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-011 :: Configura��o de Classifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-011';
  FRJanelas.Nome      := 'FmTXClaArtPrp';
  FRJanelas.Descricao := 'Configura��o de Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-012 :: Classifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-012';
  FRJanelas.Nome      := 'FmTXClaArtOne';
  FRJanelas.Descricao := 'Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-013 :: Pallet de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-013';
  FRJanelas.Nome      := 'FmTXPallet';
  FRJanelas.Descricao := 'Pallet de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-014 :: Cria��o de Pallet de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-014';
  FRJanelas.Nome      := 'FmTXPalletAdd';
  FRJanelas.Descricao := 'Cria��o de Pallet de Artigo';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-015 :: Classifica��o de Artigo de Ribeira Couro a Couro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-015';
  FRJanelas.Nome      := 'FmTXClassifOne';
  FRJanelas.Descricao := 'Classifica��o de Artigo de Ribeira Couro a Couro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-016 :: Continua��o de Classifica��o de Artigo de Ribeira (IMEI)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-016';
  FRJanelas.Nome      := 'FmTXClaArtSelA';
  FRJanelas.Descricao := 'Continua��o de Classifica��o de Artigo de Ribeira (IMEI)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-017 :: Adi��o de Pallet em Classifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-017';
  FRJanelas.Nome      := 'FmTXClaArtPalAdd';
  FRJanelas.Descricao := 'Adi��o de Pallet em Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-018 :: Central de Relat�rios
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-018';
  FRJanelas.Nome      := 'FmTXMovImp';
  FRJanelas.Descricao := 'Central de Relat�rios';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-019 :: Sa�da de Wet Blue do Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-019';
  FRJanelas.Nome      := 'FmTXOutCab';
  FRJanelas.Descricao := 'Sa�da de Wet Blue do Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-020 :: Item de Venda de Wet Blue Com Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-020';
  FRJanelas.Nome      := 'FmTXOutKno';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue Com Rastreio';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-021 :: Item de Venda de Wet Blue Sem Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-021';
  FRJanelas.Nome      := 'FmTXOutUnk';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue Sem Rastreio';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-022 :: Item de Baixa For�ada de Couros na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-022';
  FRJanelas.Nome      := 'FmTXOutIts';
  FRJanelas.Descricao := 'Item de Baixa For�ada de Couros na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
}
  // TEX-FAXAO-023 :: Ajuste de Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-023';
  FRJanelas.Nome      := 'FmTXAjsCab';
  FRJanelas.Descricao := 'Ajuste de Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-024 :: Item de Ajuste de Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-024';
  FRJanelas.Nome      := 'FmTXAjsIts';
  FRJanelas.Descricao := 'Item de Ajuste de Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-025 :: Impress�o de IME-I
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-025';
  FRJanelas.Nome      := 'FmTXImpIMEI';
  FRJanelas.Descricao := 'Impress�o de IME-I';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-026 :: Impress�o de Pallet TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-026';
  FRJanelas.Nome      := 'FmTXImpPallet';
  FRJanelas.Descricao := 'Impress�o de Pallet TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-027 :: Baixa For�ada
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-027';
  FRJanelas.Nome      := 'FmTXBxaCab';
  FRJanelas.Descricao := 'Baixa For�ada';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-028 :: Item de Baixa For�ada
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-028';
  FRJanelas.Nome      := 'FmTXBxaIts';
  FRJanelas.Descricao := 'Item de Baixa For�ada';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-029 :: Reclassifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-029';
  FRJanelas.Nome      := 'FmTXRclCab';
  FRJanelas.Descricao := 'Reclassifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-030 :: Reclassifica��o de Artigo de Ribeira Couro a Couro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-030';
  FRJanelas.Nome      := 'FmTXReclassifOne';
  FRJanelas.Descricao := 'Reclassifica��o de Artigo de Ribeira Couro a Couro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-031 :: Configura��o de Reclassifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-031';
  FRJanelas.Nome      := 'FmTXReclaArtPrp';
  FRJanelas.Descricao := 'Configura��o de Reclassifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-032 :: Continua��o de Reclassifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-032';
  FRJanelas.Nome      := 'FmTXRclArtSel';
  FRJanelas.Descricao := 'Continua��o de Reclassifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
(*  Nao efetivado!!! Usado o TEX-FAXAO-044
  // TEX-FAXAO-033 :: Habilita��o de Pallet para Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-033';
  FRJanelas.Nome      := 'FmTXGerRclCab';
  FRJanelas.Descricao := 'Habilita��o de Pallet para Classifica��o';
  FLJanelas.Add(FRJanelas);
*)
  //
  // TEX-FAXAO-034 :: Impress�o de Packing List de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-034';
  FRJanelas.Nome      := 'FmTXImpPackList';
  FRJanelas.Descricao := 'Impress�o de Packing List de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-035 :: Item de Venda de Wet Blue por Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-035';
  FRJanelas.Nome      := 'FmTXOutPal';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue por Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-036 :: Classes Geradas - IMEI
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-036';
  FRJanelas.Nome      := 'FmTXImpClaIMEI';
  FRJanelas.Descricao := 'Classes Geradas - IMEI';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-037 :: Pesquisa de Movimento de Materiais
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-037';
  FRJanelas.Nome      := 'FmTXTalPsq';
  FRJanelas.Descricao := 'Pesquisa de Movimento de Materiais';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-038 :: Artigo em Industrializa��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-038';
  FRJanelas.Nome      := 'FmTXIndCab';
  FRJanelas.Descricao := 'Artigo em Industrializa��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-039 :: Item de Artigo em Industrializa��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-039';
  FRJanelas.Nome      := 'FmTXIndInn';
  FRJanelas.Descricao := 'Item de Artigo em Industrializa��o';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-039 :: Configura��o de Artigos de Ribeira Em Opera��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-039';
  FRJanelas.Nome      := 'FmTXRibOpe';
  FRJanelas.Descricao := 'Configura��o de Artigos de Ribeira Em Opera��o';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-040 :: Origem de Artigo em Industraliza��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-040';
  FRJanelas.Nome      := 'FmTXIndOri';
  FRJanelas.Descricao := 'Origem de Artigo em Industraliza��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-041 :: Destino de Artigo em Industraliza��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-041';
  FRJanelas.Nome      := 'FmTXIndDst';
  FRJanelas.Descricao := 'Destino de Artigo em Industraliza��o';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-042 :: Defini��o de Produto de Artigo TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-042';
  FRJanelas.Nome      := 'FmTXSubProd';
  FRJanelas.Descricao := 'Defini��o de Produto de Artigo TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-043 :: Continua��o de Classifica��o de Artigo de Ribeira (RMP)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-043';
  FRJanelas.Nome      := 'FmTXClaArtSelB';
  FRJanelas.Descricao := 'Continua��o de Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-044 :: Gerenciamento de Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-044';
  FRJanelas.Nome      := 'FmTXGerRclCab';  // A e B ???
  FRJanelas.Descricao := 'Gerenciamento de Reclassifica��o';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-045 :: Gerenciamento de IME-Is
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-045';
  FRJanelas.Nome      := 'FmTXMovIts';  // A e B ???
  FRJanelas.Descricao := 'Gerenciamento de IME-Is';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-046 :: Gerenciamento de Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-046';
  FRJanelas.Nome      := 'FmTXGerClaCab';  // A e B ???
  FRJanelas.Descricao := 'Gerenciamento de Classifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-047 :: Impress�o de N�mero de Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-047';
  FRJanelas.Nome      := 'FmTXPalNumImp';
  FRJanelas.Descricao := 'Impress�o de N�mero de Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-048 :: Reclasses Geradas
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-048';
  FRJanelas.Nome      := 'FmTXImpRecla';
  FRJanelas.Descricao := 'Reclasses Geradas';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-049 :: Gerenciamento de Desnate
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-049';
  FRJanelas.Nome      := 'FmTXDsnCab';
  FRJanelas.Descricao := 'Gerenciamento de Desnate';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-050 :: Gerenciamento de Desnate - Artigos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-050';
  FRJanelas.Nome      := 'FmTXDsnArt';
  FRJanelas.Descricao := 'Gerenciamento de Desnate - Artigos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-051 :: Gerenciamento de Desnate - IME-Cs
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-051';
  FRJanelas.Nome      := 'FmTXDsnIts';
  FRJanelas.Descricao := 'Gerenciamento de Desnate - IME-Cs';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-052 :: Gerenciamento de Boxes de Classifica��o / Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-052';
  FRJanelas.Nome      := 'FmTXPaCRIts';
  FRJanelas.Descricao := 'Gerenciamento de Boxes de Classifica��o / Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-053 :: Gerenciamento de Classifica��es / Reclassifica��es
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-053';
  FRJanelas.Nome      := 'FmTXPaCRCab';
  FRJanelas.Descricao := 'Gerenciamento de Classifica��es / Reclassifica��es';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-054 :: Origem de Artigo de Ribeira Em Opera��o (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-054';
  FRJanelas.Nome      := 'FmTXOpeOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo de Ribeira Em Opera��o (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-055 :: Gerenciamento de Desnate - Sub Classe
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-055';
  FRJanelas.Nome      := 'FmTXDsnSub';
  FRJanelas.Descricao := 'Gerenciamento de Desnate - Sub Classe';
  FLJanelas.Add(FRJanelas);
  //
}
  // TEX-FAXAO-056 :: Altera��o Manual de IME-I
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-056';
  FRJanelas.Nome      := 'FmTXMovItsAlt';
  FRJanelas.Descricao := 'Altera��o Manual de IME-I';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-057 :: Gerenciamento de Fichas
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-057';
  FRJanelas.Nome      := 'FmTXFchGerCab';
  FRJanelas.Descricao := 'Gerenciamento de Fichas';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-058 :: Resultado de Fichas
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-058';
  FRJanelas.Nome      := 'FmTXFchRslCab';
  FRJanelas.Descricao := 'Resultado de Fichas';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-059 :: Item de Resultado de Ficha
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-059';
  FRJanelas.Nome      := 'FmTXFchRslIts';
  FRJanelas.Descricao := 'Item de Resultado de Ficha';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-060 :: Classifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-060';
  FRJanelas.Nome      := 'FmTXPaMulCab';
  FRJanelas.Descricao := 'Classifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-061 :: Item de Classifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-061';
  FRJanelas.Nome      := 'FmTXPaMulIts';
  FRJanelas.Descricao := 'Item de Classifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-062 :: Pallet em Classifica��o / Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-062';
  FRJanelas.Nome      := 'FmTXPalInClaRcl';
  FRJanelas.Descricao := 'Pallet em Classifica��o / Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-063 :: Prepara��o de Pallet para Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-063';
  FRJanelas.Nome      := 'FmTXReclassPrePal';
  FRJanelas.Descricao := 'Prepara��o de Pallet para Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-064 :: Configura��o de Classifica��o de Artigo de Ribeira (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-064';
  FRJanelas.Nome      := 'FmTXRclArtPrpNew';
  FRJanelas.Descricao := 'Configura��o de Classifica��o de Artigo de Ribeira (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-065 :: Hist�rico de Ficha RMP
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-065';
  FRJanelas.Nome      := 'FmTXHisFchAdd';
  FRJanelas.Descricao := 'Hist�rico de Ficha RMP';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-066 :: Gerenciamento de OCs
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-066';
  FRJanelas.Nome      := 'FmTXOSGerCab';
  FRJanelas.Descricao := 'Gerenciamento de OCs';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-067 :: Gerenciamento de Equ�lize
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-067';
  FRJanelas.Nome      := 'FmTXEqzCab';
  FRJanelas.Descricao := 'Gerenciamento de Equ�lize';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-068 :: Gerenciamento de Equ�lize - Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-068';
  FRJanelas.Nome      := 'FmTXEqzIts';
  FRJanelas.Descricao := 'Gerenciamento de Equ�lize - Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-069 :: Gerenciamento de Equ�lize - Sub Classe
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-069';
  FRJanelas.Nome      := 'FmTXEqzSub';
  FRJanelas.Descricao := 'Gerenciamento de Equ�lize - Sub Classe';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-070 :: Classes Geradas - Ficha RMP
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-070';
  FRJanelas.Nome      := 'FmTXImpClaFichaRMP';
  FRJanelas.Descricao := 'Classes Geradas - Ficha RMP';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-071 :: Configura��o de equ�lize
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-071';
  FRJanelas.Nome      := 'FmTXCfgEqzCb';
  FRJanelas.Descricao := 'Configura��o de equ�lize';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-072 :: Item de Configura��o de equ�lize
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-072';
  FRJanelas.Nome      := 'FmTXCfgEqzIt';
  FRJanelas.Descricao := 'Item de Configura��o de equ�lize';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-073 :: Troca de Ficha em Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-073';
  FRJanelas.Nome      := 'FmTXClassifOneRetFichaRMP';
  FRJanelas.Descricao := 'Troca de Ficha em Classifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-074 :: Troca de IMEI em Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-074';
  FRJanelas.Nome      := 'FmTXClassifOneRetIMEI';
  FRJanelas.Descricao := 'Troca de IMEI em Classifica��o';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-075 :: Baixas For�adas de IMEIs Completos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-075';
  FRJanelas.Nome      := 'FmTXBxaItsIMEIs';
  FRJanelas.Descricao := 'Baixas For�adas de IMEIs Completos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-076 :: Entrada de Artigo Semi Pronto por Compra
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-076';
  FRJanelas.Nome      := 'FmTXPlCCab';
  FRJanelas.Descricao := 'Entrada de Artigo Semi Pronto por Compra';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-077 :: Item de Entrada de Artigo Semi Pronto por Compra
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-077';
  FRJanelas.Nome      := 'FmTXPlCIts';
  FRJanelas.Descricao := 'Item de Entrada de Artigo Semi Pronto por Compra';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-078 :: Baixa Extra
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-078';
  FRJanelas.Nome      := 'FmTXExBCab';
  FRJanelas.Descricao := 'Baixa Extra ';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-079 :: Item de Baixa Extra
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-079';
  FRJanelas.Nome      := 'FmTXExBIts';
  FRJanelas.Descricao := 'Item de Baixa Extra de Couro na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-080 :: Baixas Extras de IMEIs Completos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-080';
  FRJanelas.Nome      := 'FmTXExBItsIMEIs';
  FRJanelas.Descricao := 'Baixas Extras de IMEIs Completos';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-081 :: Impress�o de Fluxo de Couros Inteiros TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-081';
  FRJanelas.Nome      := 'FmTXImpFluxo';
  FRJanelas.Descricao := 'Impress�o de Fluxo de Couros Inteiros TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-082 :: Configura��o de Fornecedore s de MP-TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-082';
  FRJanelas.Nome      := 'FmTXEntiMP';
  FRJanelas.Descricao := 'Configura��o de Fornecedores de MP-TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-083 :: Invent�rio de Couros TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-083';
  FRJanelas.Nome      := 'FmTXBalCab';
  FRJanelas.Descricao := 'Invent�rio de couros TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-084 :: Adi��o de Empresa a Invent�rio de Couros TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-084';
  FRJanelas.Nome      := 'FmTXBalEmp';
  FRJanelas.Descricao := 'Adi��o de Empresa a Invent�rio de Couros TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-085 :: Novo Invent�rio de Couros TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-085';
  FRJanelas.Nome      := 'FmTXBalCabNew';
  FRJanelas.Descricao := 'Novo Invent�rio de Couros TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-086 :: Impress�o de Diferen�as de Couros TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-086';
  FRJanelas.Nome      := 'FmTXImpDif';
  FRJanelas.Descricao := 'Impress�o de Diferen�as de Couros TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-087 :: Classes Geradas de IME-Is TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-087';
  FRJanelas.Nome      := 'FmTXCGICab';
  FRJanelas.Descricao := 'Classes Geradas de IME-Is TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-088 :: Sele��o de IME-Is TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-088';
  FRJanelas.Nome      := 'FmTXCGIIts';
  FRJanelas.Descricao := 'Sele��o de IME-Is TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-089 :: Configura��o de Artigos Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-089';
  FRJanelas.Nome      := 'FmTXWetEnd';
  FRJanelas.Descricao := 'Configura��o de Artigos Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-090 :: Artigo Classificado X Artigos Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-090';
  FRJanelas.Nome      := 'FmTXClaWet';
  FRJanelas.Descricao := 'Artigo Classificado X Artigos Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
}
  // TEX-FAXAO-091 :: Pedido de Venda
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-091';
  FRJanelas.Nome      := 'FmTXPedCab';
  FRJanelas.Descricao := 'Pedido de Venda';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-092 :: Item de Pedido de Venda
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-092';
  FRJanelas.Nome      := 'FmTXPedIts';
  FRJanelas.Descricao := 'Item de Pedido de Venda';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-093 :: Processo de Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-093';
  FRJanelas.Nome      := 'FmTXPWECab';
  FRJanelas.Descricao := 'Processo de Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-094 :: Origem de Artigo de Semi Acabado em Processo (Pallet)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-094';
  FRJanelas.Nome      := 'FmTXPWEOriPall';
  FRJanelas.Descricao := 'Origem de Artigo de Semi Acabado em Processo (Pallet)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-095 :: Origem de Artigo de Semi Acabado em Processo (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-095';
  FRJanelas.Nome      := 'FmTXPWEOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo de Semi Acabado em Processo (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-096 :: Destino de Artigo de Semi Acabado em Processo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-096';
  FRJanelas.Nome      := 'FmTXPWEDst';
  FRJanelas.Descricao := 'Destino de Artigo de Semi Acabado em Processo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-097 :: Cria��o de Pallet de Artigo de Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-097';
  FRJanelas.Nome      := 'FmTXPalletPWEAdd';
  FRJanelas.Descricao := 'Cria��o de Pallet de Artigo de Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-098 :: Configura��o de Artigo Acabado Classificado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-098';
  FRJanelas.Nome      := 'FmTXFinCla';
  FRJanelas.Descricao := 'Configura��o de Artigo Acabado Classificado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-099 :: Artigo TX sem Tipo de Material Definido
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-099';
  FRJanelas.Nome      := 'FmTXImpMatNoDef';
  FRJanelas.Descricao := 'Artigo TX sem Tipo de Material Definido';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-100 :: Impress�o de Movimento de Compra e Venda
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-100';
  FRJanelas.Nome      := 'FmTXImpMatNoDef';
  FRJanelas.Descricao := 'Impress�o de Movimento de Compra e Venda';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-101 :: Divis�o ao Meio de Couro TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-101';
  FRJanelas.Nome      := 'FmTXDivCouMeio';
  FRJanelas.Descricao := 'Divis�o ao Meio de Couros TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-102 :: Pesquisa Sequ�ncia de Pe�as
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-102';
  FRJanelas.Nome      := 'FmTXPesqSeqQtde';
  FRJanelas.Descricao := 'Pesquisa Sequ�ncia de Quantidade';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-103 :: Altera��o de Quantidades de Artigo Gerado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-103';
  FRJanelas.Nome      := 'FmTXGerArtAltQtd';
  FRJanelas.Descricao := 'Altera��o de Quantidades de Artigo Gerado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-104 :: Altera��o de In Natura de Artigo Gerado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-104';
  FRJanelas.Nome      := 'FmTXGerArtAltInN';
  FRJanelas.Descricao := 'Altera��o de In Natura de Artigo Gerado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-105 :: �rvore de Artigos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-105';
  FRJanelas.Nome      := 'FmTXArvoreArtigos';
  FRJanelas.Descricao := '�rvore de Artigos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-106 :: Entrada de Artigo Por Devolu��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-106';
  FRJanelas.Nome      := 'FmTXDvlCab';
  FRJanelas.Descricao := 'Entrada de Artigo Por Devolu��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-107 :: Item de Entrada de Artigo Por Devolu��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-107';
  FRJanelas.Nome      := 'FmTXDvlIts';
  FRJanelas.Descricao := 'Item de Entrada de Artigo Por Devolu��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-108 :: Entrada de Artigo Vendido Para Retrabalho
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-108';
  FRJanelas.Nome      := 'FmTXRtbCab';
  FRJanelas.Descricao := 'Entrada de Artigo Vendido Para Retrabalho';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-109 :: Item de Entrada de Artigo Vendido Para Retrabalho
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-109';
  FRJanelas.Nome      := 'FmTXRtbIts';
  FRJanelas.Descricao := 'Item de Entrada de Artigo Vendido Para Retrabalho';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-110 :: Configura��o de Subproduto In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-110';
  FRJanelas.Nome      := 'FmTXSubPrd';
  FRJanelas.Descricao := 'Configura��o de Subproduto In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-111 :: Item de Sub Produto
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-111';
  FRJanelas.Nome      := 'FmTXSubPrdIts';
  FRJanelas.Descricao := 'Item de Sub Produto';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-112 :: Item de Venda por Peso
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-112';
  FRJanelas.Nome      := 'FmTXOutPeso';
  FRJanelas.Descricao := 'Item de Venda por Peso';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-113 :: Datas de Gera��o de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-113';
  FRJanelas.Nome      := 'FmTXGerArtDatas';
  FRJanelas.Descricao := 'Datas de Gera��o de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-114 :: Relassifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-114';
  FRJanelas.Nome      := 'FmTXPaMulCabR';
  FRJanelas.Descricao := 'Reclassifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-115 :: Item de Reclassifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-115';
  FRJanelas.Nome      := 'FmTXPaMulItsR';
  FRJanelas.Descricao := 'Item de Reclassifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-116 :: Informe de N�mero
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-116';
  FRJanelas.Nome      := 'FmTXReqMovEstq';
  FRJanelas.Descricao := 'Informe de N�mero';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-117 :: RME - Requisi��o de Movimenta��o de Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-117';
  FRJanelas.Nome      := 'FmTXReqMov';
  FRJanelas.Descricao := 'RME - Requisi��o de Movimenta��o de Estoque';
  FLJanelas.Add(FRJanelas);
  //
(*
  // TEX-FAXAO-118 :: Sa�da de Couro - Item de NFe
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-118';
  FRJanelas.Nome      := 'FmTXOutNFI';
  FRJanelas.Descricao := 'Sa�da de Couro - Item de NFe';
  FLJanelas.Add(FRJanelas);
  //
*)
  // TEX-FAXAO-119 :: Adi��o ao Arquivo Morto TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-119';
  FRJanelas.Nome      := 'FmTXMovItbAdd';
  FRJanelas.Descricao := 'Adi��o ao Arquivo Morto TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-120 :: Impress�o de Hist�rico
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-120';
  FRJanelas.Nome      := 'FmTXImpHistorico';
  FRJanelas.Descricao := 'Impress�o de Hist�rico';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-121 :: Impress�o de Nota MPAG
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-121';
  FRJanelas.Nome      := 'FmTXImpNotaMPAG';
  FRJanelas.Descricao := 'Impress�o de Nota MPAG';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-122 :: Impress�o de Pesquisa por Martelo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-122';
  FRJanelas.Nome      := 'FmTXImpMartelo';
  FRJanelas.Descricao := 'Impress�o de Pesquisa por Martelo';
  FLJanelas.Add(FRJanelas);
  //
}
  // TEX-FAXAO-123 :: Impress�o de Estoque TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-123';
  FRJanelas.Nome      := 'FmTXImpEstoque';
  FRJanelas.Descricao := 'Impress�o de Estoque TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-124 :: Mistura de Fornecedores
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-124';
  FRJanelas.Nome      := 'FmTXMulFrnCab';
  FRJanelas.Descricao := 'Mistura de Fornecedores';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-125 :: Item de Mistura de Fornecedores
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-125';
  FRJanelas.Nome      := 'FmTXMulFrnIts';
  FRJanelas.Descricao := 'Item de Mistura de Fornecedores';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-126 :: Gerenciamento de Pr� Reclasse
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-126';
  FRJanelas.Nome      := 'FmTXPrePalCab';
  FRJanelas.Descricao := 'Gerenciamento de Pr� Reclasse';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-127 :: Corrige IME-Is sem Fornecedor
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-127';
  FRJanelas.Nome      := 'FmTXCorrigeMulFrn';
  FRJanelas.Descricao := 'Corrige IME-Is sem Fornecedor';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-128 :: Corrige IME-Is G�meos Orf�os
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-128';
  FRJanelas.Nome      := 'FmTXCorrigeMovimTwn';
  FRJanelas.Descricao := 'Corrige IME-Is G�meos Orf�os';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-129 :: Estoque TX Em...
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-129';
  FRJanelas.Nome      := 'FmTXImpEstqEm';
  FRJanelas.Descricao := 'Estoque TX Em...';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-130 :: Hist�rico de Pallets
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-130';
  FRJanelas.Nome      := 'FmTXImpHistPall';
  FRJanelas.Descricao := 'Hist�rico de Pallets';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-131 :: Transfer�ncia de Local de Estoque TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-131';
  FRJanelas.Nome      := 'FmTXTrfLocCab';
  FRJanelas.Descricao := 'Transfer�ncia de Local de Estoque TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-132 :: Transfer�ncia de Local de Estoque TX - por IMEI
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-132';
  FRJanelas.Nome      := 'FmTXTrfLocIMEI';
  FRJanelas.Descricao := 'Transfer�ncia de Local de Estoque TX - por IMEI';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-133 :: Transfer�ncia de Local de Estoque TX - por Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-133';
  FRJanelas.Nome      := 'FmTXTrfLocPal';
  FRJanelas.Descricao := 'Transfer�ncia de Local de Estoque TX - por Pallet';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-134 :: S�rie/N�mero de NFe - TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-134';
  FRJanelas.Nome      := 'FmTXOutNfeCab';
  FRJanelas.Descricao := 'S�rie/N�mero de NFe - TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-135 :: Item de NFe - TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-135';
  FRJanelas.Nome      := 'FmTXOutNfeIts';
  FRJanelas.Descricao := 'Item de NFe - TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-136 :: IMEIS sem Refer�ncia de Origem
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-136';
  FRJanelas.Nome      := 'FmTXCorrigeSN2Orfao';
  FRJanelas.Descricao := 'IMEIS sem Refer�ncia de Origem';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-137 :: Adi��o de IME-I em Pesagem
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-137';
  FRJanelas.Nome      := 'FmTXEmitCus';
  FRJanelas.Descricao := 'Adi��o de IME-I em Pesagem';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-138 :: Resultados de V�rias Fichas RMP
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-138';
  FRJanelas.Nome      := 'FmTXImpClaMulRMP';
  FRJanelas.Descricao := 'Resultados de V�rias Fichas RMP';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-139 :: Senha TX do Dia
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-139';
  FRJanelas.Nome      := 'FmTXPwdDd';
  FRJanelas.Descricao := 'Senha TX do Dia';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-140 :: Configura��o de Couro em Caleiro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-140';
  FRJanelas.Nome      := 'FmTXProCal';
  FRJanelas.Descricao := 'Configura��o de Couro em Caleiro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-141 :: Configura��o de Couro em Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-141';
  FRJanelas.Nome      := 'FmTXProCur';
  FRJanelas.Descricao := 'Configura��o de Couro em Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-142 :: Artigo em Processo de Caleiro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-142';
  FRJanelas.Nome      := 'FmTXCalCab';
  FRJanelas.Descricao := 'Artigo em Processo de Caleiro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-143 :: Origem de Artigo em Processo de Caleiro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-143';
  FRJanelas.Nome      := 'FmTXCalOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo em Processo de Caleiro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-144 :: Destino de Artigo em Processo de Caleiro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-144';
  FRJanelas.Nome      := 'FmTXCalDst';
  FRJanelas.Descricao := 'Destino de Artigo em Processo de Caleiro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-145 :: Artigo em Processo de Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-145';
  FRJanelas.Nome      := 'FmTXCurCab';
  FRJanelas.Descricao := 'Artigo em Processo de Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-146 :: Origem de Artigo em Processo de Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-146';
  FRJanelas.Nome      := 'FmTXCurOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo em Processo de Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-147 :: Destino de Artigo em Processo de Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-147';
  FRJanelas.Nome      := 'FmTXCurDst';
  FRJanelas.Descricao := 'Destino de Artigo em Processo de Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-148 :: Item de Sub Produto (Origem M�ltipla)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-148';
  FRJanelas.Nome      := 'FmTXSubPrdItsMul';
  FRJanelas.Descricao := 'Item de Sub Produto (Origem M�ltipla)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-149 :: Item de Gera��o de Artigo de Ribeira do Curtimento - Uni
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-149';
  FRJanelas.Nome      := 'FmTXGerArtItsCurUni';
  FRJanelas.Descricao := 'Item de Gera��o de Artigo de Ribeira do Curtimento - Uni';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-150 :: RDC - Requisi��o de Divis�o de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-150';
  FRJanelas.Nome      := 'FmTXReqDiv';
  FRJanelas.Descricao := 'RDC - Requisi��o de Divis�o de Couro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-151 :: Impress�o de Couros em Processo BH
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-151';
  FRJanelas.Nome      := 'FmTXImpEmProcBH';
  FRJanelas.Descricao := 'Impress�o de Couros em Processo BH';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-152 :: Item de Gera��o de Artigo de Ribeira do Curtimento - Mul
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-152';
  FRJanelas.Nome      := 'FmTXGerArtItsCurMul';
  FRJanelas.Descricao := 'Item de Gera��o de Artigo de Ribeira do Curtimento - Mul';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-153 :: Impress�o de Rendimento de Semi e Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-153';
  FRJanelas.Nome      := 'FmTXImpRendPWE';
  FRJanelas.Descricao := 'Impress�o de Rendimento de Semi e Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-154 :: Grupos de Artigos TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-154';
  FRJanelas.Nome      := 'FmTXGruGGX';
  FRJanelas.Descricao := 'Grupos de Artigos TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-155 :: Configura��o de Opera��es e Processos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-155';
  FRJanelas.Nome      := 'FmTXCOPCab';
  FRJanelas.Descricao := 'Configura��o de Opera��es e Processos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-156 :: Itens de Configura��o de Opera��es e Processos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-156';
  FRJanelas.Nome      := 'FmTXCOPIts';
  FRJanelas.Descricao := 'Itens de Configura��o de Opera��es e Processos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-157 :: Pre�os de M�o-de-Obra
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-157';
  FRJanelas.Nome      := 'FmTXPMOCab';
  FRJanelas.Descricao := 'Pre�os de M�o-de-Obra';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-158 :: Configura��o de Couro Caleirado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-158';
  FRJanelas.Nome      := 'FmTXCouCal';
  FRJanelas.Descricao := 'Configura��o de Couro Caleirado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-159 :: Configura��o de Couro Curtido
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-159';
  FRJanelas.Nome      := 'FmTXCouCur';
  FRJanelas.Descricao := 'Configura��o de Couro Curtido';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-160 :: IME-Cs
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-160';
  FRJanelas.Nome      := 'FmTXMovCab';
  FRJanelas.Descricao := 'IME-Cs';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-161 :: Desclassifica��o de Mat�ria-prima em Processo de Semi
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-161';
  FRJanelas.Nome      := 'FmTXPWEDescl';
  FRJanelas.Descricao := 'Desclassifica��o de Mat�ria-prima em Processo de Semi';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-162 :: Impress�o de Campara��o de Pallets
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-162';
  FRJanelas.Nome      := 'FmTXComparaCacIts';
  FRJanelas.Descricao := 'Impress�o de Campara��o de Pallets';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-163 :: Confer�ncia de Movimento de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-163';
  FRJanelas.Nome      := 'FmTXCfgMovEFD';
  FRJanelas.Descricao := 'Confer�ncia de Movimento de Couro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-164 :: Gera��o de Raspa WB em Processo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-164';
  FRJanelas.Nome      := 'FmTXPWESubPrd';
  FRJanelas.Descricao := 'Gera��o de Raspa WB em Processo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-165 :: Gerenciamento de MO de Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-165';
  FRJanelas.Nome      := 'FmTXMOPWEGer';
  FRJanelas.Descricao := 'Gerenciamento de MO de Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-166 :: Altera��o de IME-I de Sa�da
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-166';
  FRJanelas.Nome      := 'FmTXOutAltTMI';
  FRJanelas.Descricao := 'Altera��o de IME-I de Sa�da';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-167 :: Edi��o de Controle de Cobran�a de MO
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-167';
  FRJanelas.Nome      := 'FmTXMOEnvRet';
  FRJanelas.Descricao := 'Edi��o de Controle de Cobran�a de MO';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-168 :: Relat�rio de Controle de Cobran�a de MO
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-168';
  FRJanelas.Nome      := 'FmTXImpMOEnvRet';
  FRJanelas.Descricao := 'Relat�rio de Controle de Cobran�a de MO';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-169 :: NFe TX Outros ou N�o Autorizada
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-169';
  FRJanelas.Nome      := 'FmTXInvNFe';
  FRJanelas.Descricao := 'NFe TX Outros ou N�o Autorizada';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-170 :: Lista Resultado TX
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-170';
  FRJanelas.Nome      := 'FmTXImpResultTX';
  FRJanelas.Descricao := 'Lista Resultado TX';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-171 :: Configura��o de Mat�rias-primas PDA
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-171';
  FRJanelas.Nome      := 'FmTXNatPDA';
  FRJanelas.Descricao := 'Configura��o de Mat�rias-primas PDA';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-172 :: Configura��o de Couro DTA
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-172';
  FRJanelas.Nome      := 'FmTXCouDTA';
  FRJanelas.Descricao := 'Configura��o de Couro DTA';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-173 :: Processamento de Subproduto
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-173';
  FRJanelas.Nome      := 'FmTXPSPCab';
  FRJanelas.Descricao := 'Processamento de Subproduto';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-174 :: IME-I Origem de Processamento de Subproduto
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-174';
  FRJanelas.Nome      := 'FmTXPSPOriIMEI';
  FRJanelas.Descricao := 'IME-I Origem de Processamento de Subproduto';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-175 :: Configura��o de Subproduto em Processo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-175';
  FRJanelas.Nome      := 'FmTXPSPPro';
  FRJanelas.Descricao := 'Configura��o de Subproduto em Processo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-176 :: Configura��o de Subproduto Processado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-176';
  FRJanelas.Nome      := 'FmTXPSPEnd';
  FRJanelas.Descricao := 'Configura��o de Subproduto em Processado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-177 :: Subproduto Processado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-177';
  FRJanelas.Nome      := 'FmTXPSPDst';
  FRJanelas.Descricao := 'Subproduto Processado';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-178 :: Reprocesso / Reparo de Material
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-178';
  FRJanelas.Nome      := 'FmTXRRMCab';
  FRJanelas.Descricao := 'Reprocesso / Reparo de Material';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-179 :: Origem de Artigo de Reprocesso / Reparo (Pallet)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-179';
  FRJanelas.Nome      := 'FmTXRRMOriPall';
  FRJanelas.Descricao := 'Origem de Artigo de Reprocesso / Reparo (Pallet)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-180 :: Origem de Artigo de Reprocesso / Reparo (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-180';
  FRJanelas.Nome      := 'FmTXRRMOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo de Reprocesso / Reparo (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-181 :: Destino de Artigo de Reprocesso / Reparo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-181';
  FRJanelas.Nome      := 'FmTXRRMDst';
  FRJanelas.Descricao := 'Destino de Artigo de Reprocesso / Reparo';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-182 :: Datas Lan�amento Retroativo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-182';
  FRJanelas.Nome      := 'FmTXDataEFDData';
  FRJanelas.Descricao := 'Datas Lan�amento Retroativo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-183 :: Impress�o de Ordem de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-183';
  FRJanelas.Nome      := 'FmTXImpOrdem';
  FRJanelas.Descricao := 'Impress�o de Ordem de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-184 :: Altera��o de IME-I de Origem de Processo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-184';
  FRJanelas.Nome      := 'FmTXPWEAltOriIMEI';
  FRJanelas.Descricao := 'Altera��o de IME-I de Origem de Processo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-185 :: Altera��o de Data e N�mero de NFe
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-185';
  FRJanelas.Nome      := 'FmTXDtHrSerNumNFe';
  FRJanelas.Descricao := 'Altera��o de Data e N�mero de NFe';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-186 :: Configura��o de Artigo Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-186';
  FRJanelas.Nome      := 'FmTXArtCab';
  FRJanelas.Descricao := 'Configura��o de Artigo Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-187 :: Atrelamento de Itens de Configura��o de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-187';
  FRJanelas.Nome      := 'FmTXArtGGX';
  FRJanelas.Descricao := 'Atrelamento de Itens de Configura��o de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-188 :: Informa��es do movimento (Status)
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-188';
  FRJanelas.Nome      := 'FmTXVmcObs';
  FRJanelas.Descricao := 'Informa��es do movimento (Status)';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-189 :: Inclus�o de Pallet Manualmente
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-189';
  FRJanelas.Nome      := 'FmTXPalletManual';
  FRJanelas.Descricao := 'Inclus�o de Pallet Manualmente';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-190 :: NF-e de Entrada In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-190';
  FRJanelas.Nome      := 'FmTXInnNFs';
  FRJanelas.Descricao := 'NF-e de Entrada In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-191 :: Configura��o de Plano de Metas de Rendimento de Subprodutos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-191';
  FRJanelas.Nome      := 'FmTXCPMRSBCb';
  FRJanelas.Descricao := 'Configura��o de Plano de Metas de Rendimento de Subprodutos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-192 :: Item de Plano de Metas de Rendimento de Subprodutos
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-192';
  FRJanelas.Nome      := 'FmTXCPMRSBIt';
  FRJanelas.Descricao := 'Item de Plano de Metas de Rendimento de Subprodutos';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-193 :: Encerramento de Rendimento de Lote
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-193';
  FRJanelas.Nome      := 'FmTXCPMRSBER';
  FRJanelas.Descricao := 'Encerramento de Rendimento de Lote';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-194 :: IEC - Informa��o de Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-194';
  FRJanelas.Nome      := 'FmTXInfInn';
  FRJanelas.Descricao := 'IEC - Informa��o de Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-195 :: ICR - Informa��o de Classe/Reclasse de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-195';
  FRJanelas.Nome      := 'FmTXInfMov';
  FRJanelas.Descricao := 'ICR - Informa��o de Classe/Reclasse de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-196 :: ISC - Informa��o de Sa�da de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-196';
  FRJanelas.Nome      := 'FmTXInfOut';
  FRJanelas.Descricao := 'ISC - Informa��o de Sa�da de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-197 :: LSP - Lista Sequencial de Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-197';
  FRJanelas.Nome      := 'FmTXLstPal';
  FRJanelas.Descricao := 'LSP - Lista Sequencial de Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-198 :: IEC - Informa��o de Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-198';
  FRJanelas.Nome      := 'FmTXInfIEC';
  FRJanelas.Descricao := 'IEC - Informa��o de Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-199 :: IPM - Informa��o de Pallets Movimentados
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-199';
  FRJanelas.Nome      := 'FmTXInfPal';
  FRJanelas.Descricao := 'IPM - Informa��o de Pallets Movimentados';
  FLJanelas.Add(FRJanelas);
  //
}
  // TEX-FAXAO-200 :: IXX - Atrelamento de Informa��o Manual
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-200';
  FRJanelas.Nome      := 'FmTXIxx';
  FRJanelas.Descricao := 'Atrelamento de Informa��o Manual';
  FLJanelas.Add(FRJanelas);
  //
{
  // TEX-FAXAO-201 :: Estoque - Custo Integrado
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-201';
  FRJanelas.Nome      := 'FmTXEstqCustoIntegr';
  FRJanelas.Descricao := 'Estoque - Custo Integrado';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-202 :: ID Movimento
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-202';
  FRJanelas.Nome      := 'FmTXMovimID';
  FRJanelas.Descricao := 'ID Movimento';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-203 :: Local x ID Movimento
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-203';
  FRJanelas.Nome      := 'FmTXMovIDLoc';
  FRJanelas.Descricao := 'Local x ID Movimento';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-204 :: Fun�oes TX em Janela Oculta
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-204';
  FRJanelas.Nome      := 'FmTXHide';
  FRJanelas.Descricao := 'Fun�oes TX em Janela Oculta';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-205 :: Entrada de Couros sem Cobertura
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-205';
  FRJanelas.Nome      := 'FmTXESCCab';
  FRJanelas.Descricao := 'Entrada de Couros sem Cobertura';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-206 :: Item de Entrada de Couros sem Cobertura
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-206';
  FRJanelas.Nome      := 'FmTXESCIts';
  FRJanelas.Descricao := 'Item de Entrada de Couros sem Cobertura';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-207 :: Configura��o de Classifica��o de Artigo de Ribeira por Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-207';
  FRJanelas.Nome      := 'FmTXClaPalPrpQnz';
  FRJanelas.Descricao := 'Configura��o de Classifica��o de Artigo de Ribeira por Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-208 :: Importa��o de Dados do ClaReCo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-208';
  FRJanelas.Nome      := 'TXLoadCRCCab';
  FRJanelas.Descricao := 'Importa��o de Dados do ClaReCo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-209 :: Item de Importa��o de Dados do ClaReCo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-209';
  FRJanelas.Nome      := 'TXLoadCRCIts';
  FRJanelas.Descricao := 'Item de Importa��o de Dados do ClaReCo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-210 :: ClaReCo - Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-210';
  FRJanelas.Nome      := 'TXLoadCRCPalletA';
  FRJanelas.Descricao := 'ClaReCo - Pallet';
  FLJanelas.Add(FRJanelas);
  //
}
{
  Movido para grade! >> PRD-GRUPO-045 ::
  // TEX-FAXAO-211 :: Configura��o de Reduzidos Orf�os
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-211';
  FRJanelas.Nome      := 'GraGruYIncorpora';
  FRJanelas.Descricao := 'Configura��o de Reduzidos Orf�os';
  FLJanelas.Add(FRJanelas);
  //
}
{
  // TEX-FAXAO-212 :: Configura��o de Reduzido de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-212';
  FRJanelas.Nome      := 'GraGruXCou';
  FRJanelas.Descricao := 'Configura��o de Reduzido de Couro';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-213 :: Datas de Reclassifica��o de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-213';
  FRJanelas.Nome      := 'FmTXGerRclDatas';
  FRJanelas.Descricao := 'Datas de Reclassifica��o de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-214 :: Confer�ncia de Baixas x Estoque de Couro In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-214';
  FRJanelas.Nome      := 'FmTXCfgEstqInNatEFD';
  FRJanelas.Descricao := 'Confer�ncia de Baixas x Estoque de Couro In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-215 :: Confer�ncia de Baixas x Estoque de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-215';
  FRJanelas.Nome      := 'FmTXCfgEstqOthersEFD';
  FRJanelas.Descricao := 'Confer�ncia de Baixas x Estoque de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-216 :: Confer�ncia de Gera��o x Estoque de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-216';
  FRJanelas.Nome      := 'FmTXCfgEstqGeradoEFD';
  FRJanelas.Descricao := 'Confer�ncia de Gera��o x Estoque de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // TEX-FAXAO-217 :: Confer�ncia de Processos x Pesagem de PQ
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-217';
  FRJanelas.Nome      := 'FmTXCfgEstqTXxPQ_EFD';
  FRJanelas.Descricao := 'Confer�ncia de Processos x Pesagem de PQ';
  FLJanelas.Add(FRJanelas);
}
  //
  // TEX-FAXAO-218 :: Item em Reprocesso / Reparo de Material
  New(FRJanelas);
  FRJanelas.ID        := 'TEX-FAXAO-218';
  FRJanelas.Nome      := 'FmTXRRMInn';
  FRJanelas.Descricao := 'Item em Reprocesso / Reparo de Material';
  FLJanelas.Add(FRJanelas);
  //
{
  // WET-RECUR-001 :: Entrada de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-001';
  FRJanelas.Nome      := 'FmWBInnCab';
  FRJanelas.Descricao := 'Entrada de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-002 :: Item de Entrada de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-002';
  FRJanelas.Nome      := 'FmWBInnIts';
  FRJanelas.Descricao := 'Item de Entrada de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-003 :: Reclassifica��o de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-003';
  FRJanelas.Nome      := 'FmWBRclIns';
  FRJanelas.Descricao := 'Reclassifica��o de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-004 :: Relat�rios de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-004';
  FRJanelas.Nome      := 'FmWBMovImp';
  FRJanelas.Descricao := 'Relat�rios de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-005 :: Sa�da de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-005';
  FRJanelas.Nome      := 'FmWBOutCab';
  FRJanelas.Descricao := 'Sa�da de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-006 :: Item de Venda de Wet Blue Sem Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-006';
  FRJanelas.Nome      := 'FmWBOutUnk';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-007 :: Item de Venda de Wet Blue com Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-007';
  FRJanelas.Nome      := 'FmWBOutKno';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-008 :: Configura��o de Artigo Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-008';
  FRJanelas.Nome      := 'FmWBArtCab';
  FRJanelas.Descricao := 'Configura��o de Artigo Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-009 :: Item de Baixa For�ada de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-009';
  FRJanelas.Nome      := 'FmWBOutIts';
  FRJanelas.Descricao := 'Item de Baixa For�ada de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-010 :: Ajuste de Estoque de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-010';
  FRJanelas.Nome      := 'FmWBAjsCab';
  FRJanelas.Descricao := 'Ajuste de Estoque de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-011 :: Item de Ajuste de Estoque de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-011';
  FRJanelas.Nome      := 'FmWBAjsIts';
  FRJanelas.Descricao := 'Item de Ajuste de Estoque de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-012 :: Industrializa��o de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-012';
  FRJanelas.Nome      := 'FmWBIndsWE';
  FRJanelas.Descricao := 'Industrializa��o de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-013 :: Configura��o de Mat�ria-prima para Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-013';
  FRJanelas.Nome      := 'FmWBMPrCab';
  FRJanelas.Descricao := 'Configura��o de Mat�ria-prima para Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-014 :: Reclassifica��o de Mat�ria-prima para Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-014';
  FRJanelas.Nome      := 'FmWBRclCab';
  FRJanelas.Descricao := 'Reclassifica��o de Mat�ria-prima para Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-015 :: Cadastro de Pallet de Mat�ria-prima para Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-015';
  FRJanelas.Nome      := 'FmWBPallet';
  FRJanelas.Descricao := 'Cadastro de Pallet de Mat�ria-prima para Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-016 :: MP Permitido em Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-016';
  FRJanelas.Nome      := 'FmWBPalArt';
  FRJanelas.Descricao := 'MP Permitido em Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-017 :: Fornecedor Permitido em MP de Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-017';
  FRJanelas.Nome      := 'FmWBPalFrn';
  FRJanelas.Descricao := 'Fornecedor Permitido em MP de Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-018 :: �rea M�dia por Fornecedor
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-018';
  FRJanelas.Nome      := 'FmWBMPrFrn';
  FRJanelas.Descricao := '�rea M�dia por Fornecedor';
  FLJanelas.Add(FRJanelas);
 }
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;


end.
