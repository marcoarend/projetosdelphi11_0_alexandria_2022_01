unit UnTX_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, UnProjGroup_Consts, DmkGeral, Variants,
  dmkDBGridZTO, UnAppEnums;

type
  TUnTX_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormFluxoCab(Codigo: Integer);
    procedure MostraFormOperacoes(Codigo: Integer);
    procedure MostraFormSetorCad(Codigo: Integer);
    procedure MostraFormTXAjsCab(Codigo, Controle: Integer);
    procedure MostraFormTXCadNat(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormTXCadInd(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormTXCadInt(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormTXCadFcc(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormTXIndCab(Codigo, TXSrc, TXInn, TXDst, TXBxa: Integer);
    procedure MostraFormTXInnCab(Codigo, TXInnIts, TXReclas: Integer);
    function  MostraFormTXTalPsq(const MovimID: TEstqMovimID; const MovimNivs:
              array of TEstqMovimNiv; const SQLType: TSQLType; var Codigo,
              Controle: Integer; OnlySelIMEI: Boolean): Boolean;
    procedure MostraFormTXPallet(Codigo: Integer);
    procedure MostraFormTXPallet1(Codigo: Integer);
    procedure MostraFormTXPalSta();
    procedure MostraFormTXMovIts(Controle: Integer);
    procedure MostraFormTX_Do_IMEI(Controle: Integer);
    procedure MostraFormTXExBCab(Codigo, Controle: Integer);
    procedure MostraFormTXBxaCab(Codigo, Controle: Integer);
    procedure MostraFormTXMotivBxa((*Codigo: Integer*));
    procedure MostraFormTXMovImpEstoque(EstoqueEm: Boolean);
    procedure MostraFormTXSerTal();
    procedure MostraFormTXTrfLocCab(Codigo: Integer);
    procedure MostraFormTXMovItsAlt(Controle: Integer;
              AtualizaSaldoModoGenerico: Boolean;
              GroupBoxes: array of TEstqEditGB);
    procedure MostraFormTXRRMCab(Codigo: Integer);
    procedure MostraFormTXPlCCab(Codigo, Controle: Integer);
 end;

var
  TX_Jan: TUnTX_Jan;


implementation

uses
  MyDBCheck, Module, DmkDAC_PF, CfgCadLista,
  Operacoes, FluxoCab, SetorCad,
  TXMovImpEstoque, TXTalPsq, TXMovItsAlt,
  TXCadNat, TXCadInd, TXCadInt, TXCadFcc, TXInnCab, TXIndCab, TXPallet,
  TXBxaCab, TXExBCab, TXMovIts, TXAjsCab, TXTrfLocCab, TXRRMCab, TXPlCCab;

{ TUnTX_Jan }

procedure TUnTX_Jan.MostraFormFluxoCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFluxoCab, FmFluxoCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmFluxoCab.LocCod(Codigo, Codigo);
    FmFluxoCab.ShowModal;
    FmFluxoCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormOperacoes(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOperacoes, FmOperacoes, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOperacoes.LocCod(Codigo, Codigo);
    FmOperacoes.ShowModal;
    FmOperacoes.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormSetorCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSetorCad, FmSetorCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSetorCad.LocCod(Codigo, Codigo);
    FmSetorCad.ShowModal;
    FmSetorCad.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXAjsCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmTXAjsCab, FmTXAjsCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTXAjsCab.LocCod(Codigo, Codigo);
      FmTXAjsCab.QrTXAjsIts.Locate('Controle', Controle, []);
    end;
    FmTXAjsCab.ShowModal;
    FmTXAjsCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXBxaCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmTXBxaCab, FmTXBxaCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTXBxaCab.LocCod(Codigo, Codigo);
      FmTXBxaCab.QrTXBxaIts.Locate('Controle', Controle, []);
    end;
    FmTXBxaCab.ShowModal;
    FmTXBxaCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXCadFcc(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmTXCadFcc, FmTXCadFcc, afmoNegarComAviso) then
  begin
    FmTXCadFcc.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmTXCadFcc.LocCod(GraGruX, GraGruX);
      if FmTXCadFcc.QrTXCadFccGraGruX.Value = GraGruX then
        FmTXCadFcc.AlteraProdutoAcabadoselecionado1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmTXCadFcc.FSeq := 1;
    FmTXCadFcc.ShowModal;
    FmTXCadFcc.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXCadNat(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmTXCadNat, FmTXCadNat, afmoNegarComAviso) then
  begin
    FmTXCadNat.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmTXCadNat.LocCod(GraGruX, GraGruX);
      if FmTXCadNat.QrTXCadNatGraGruX.Value = GraGruX then
        FmTXCadNat.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmTXCadNat.FSeq := 1;
    FmTXCadNat.ShowModal;
    FmTXCadNat.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXExBCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmTXExBCab, FmTXExBCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTXExBCab.LocCod(Codigo, Codigo);
    end;
    FmTXExBCab.ShowModal;
    FmTXExBCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXCadInd(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmTXCadInd, FmTXCadInd, afmoNegarComAviso) then
  begin
    FmTXCadInd.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmTXCadInd.LocCod(GraGruX, GraGruX);
      if FmTXCadInd.QrTXCadIndGraGruX.Value = GraGruX then
        FmTXCadInd.Alteraprodutoemprocessoselecionado1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmTXCadInd.FSeq := 1;
    FmTXCadInd.ShowModal;
    FmTXCadInd.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXCadInt(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmTXCadInt, FmTXCadInt, afmoNegarComAviso) then
  begin
    FmTXCadInt.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmTXCadInt.LocCod(GraGruX, GraGruX);
      if FmTXCadInt.QrTXCadIntGraGruX.Value = GraGruX then
        FmTXCadInt.Alteraprodutoemprocessoselecionado1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmTXCadInt.FSeq := 1;
    FmTXCadInt.ShowModal;
    FmTXCadInt.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXIndCab(Codigo, TXSrc, TXInn, TXDst,
  TXBxa: Integer);
begin
  if DBCheck.CriaFm(TFmTXIndCab, FmTXIndCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTXIndCab.LocCod(Codigo, Codigo);
      //
{POIU
      if (FmTXIndCab.QrTXInnIts.State <> dsInactive) and
        (FmTXIndCab.QrTXInnIts.RecordCount > 0)
      then
        FmTXIndCab.QrTXInnIts.Locate('Controle', TXInnIts, []);
      //
      if (FmTXIndCab.QrTXItsBxa.State <> dsInactive) and
        (FmTXIndCab.QrTXItsBxa.RecordCount > 0) then
      begin
        if not FmTXIndCab.QrTXItsBxa.Locate('Controle', TXReclas, []) then
        begin
          // tenta o item par
          FmTXIndCab.QrTXItsBxa.Locate('Controle', TXReclas + 1, []);
        end;
      end;
}
    end;
    FmTXIndCab.ShowModal;
    FmTXIndCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXInnCab(Codigo, TXInnIts, TXReclas: Integer);
begin
  if DBCheck.CriaFm(TFmTXInnCab, FmTXInnCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTXInnCab.LocCod(Codigo, Codigo);
      //
      if (FmTXInnCab.QrTXInnIts.State <> dsInactive) and
        (FmTXInnCab.QrTXInnIts.RecordCount > 0)
      then
        FmTXInnCab.QrTXInnIts.Locate('Controle', TXInnIts, []);
      //
      if (FmTXInnCab.QrTXItsBxa.State <> dsInactive) and
        (FmTXInnCab.QrTXItsBxa.RecordCount > 0) then
      begin
        if not FmTXInnCab.QrTXItsBxa.Locate('Controle', TXReclas, []) then
        begin
          // tenta o item par
          FmTXInnCab.QrTXItsBxa.Locate('Controle', TXReclas + 1, []);
        end;
      end;
    end;
    FmTXInnCab.ShowModal;
    FmTXInnCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXMotivBxa();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'TXMotivBxa', 60,
  UnDmkEnums.TNovoCodigo.ncGerlSeq1, 'Motivo de Baixa Extra',
  [], False, Null, [], [], False);
end;

procedure TUnTX_Jan.MostraFormTXMovImpEstoque(EstoqueEm: Boolean);
begin
  if DBCheck.CriaFm(TFmTXMovImpEstoque, FmTXMovImpEstoque, afmoNegarComAviso) then
  begin
    // Data
    FmTXMovImpEstoque.TP00EstoqueEm.Date := Date();
    if EstoqueEm then
      FmTXMovImpEstoque.Ck00EstoqueEm.Checked := True;
    //
    FmTXMovImpEstoque.ShowModal;
    FmTXMovImpEstoque.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXMovIts(Controle: Integer);
begin
  if DBCheck.CriaFm(TFmTXMovIts, FmTXMovIts, afmoNegarComAviso) then
  begin
    if Controle <> 0 then
      FmTXMovIts.LocCod(Controle, Controle);
    FmTXMovIts.ShowModal;
    FmTXMovIts.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXMovItsAlt(Controle: Integer;
  AtualizaSaldoModoGenerico: Boolean; GroupBoxes: array of TEstqEditGB);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmTXMovItsAlt, FmTXMovItsAlt, afmoNegarComAviso) then
  begin
    FmTXMovItsAlt.DefineImeiAEditar(Controle);
    for I := Low(GroupBoxes) to  High(GroupBoxes) do
    begin
      case GroupBoxes[I] of
        (*0*)eegbNone: ;//
        (*1*)eegbQtdOriginal: FmTXMovItsAlt.GBQtdOriginal.Enabled := True;
        (*2*)eegbDadosArtigo: FmTXMovItsAlt.GBDadosArtigo.Enabled := True;
        else Geral.MB_Aviso(
        '"TEstqEditGB" n�o implementado em "MostraFormTXMovItsAlt()"');
      end;
    end;
    FmTXMovItsAlt.FAtualizaSaldoModoGenerico := AtualizaSaldoModoGenerico;
    FmTXMovItsAlt.ShowModal;
    FmTXMovItsAlt.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXPallet(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTXPallet, FmTXPallet, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTXPallet.LocCod(Codigo, Codigo);
    FmTXPallet.ShowModal;
    FmTXPallet.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXPallet1(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTXPallet, FmTXPallet1, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTXPallet1.LocCod(Codigo, Codigo);
    FmTXPallet1.ShowModal;
    FmTXPallet1.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXPalSta;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'TXPalSta', 60,
  UnDmkEnums.TNovoCodigo.ncGerlSeq1, 'Status de Pallet',
  [], True, 2000, [], [], False);
end;

procedure TUnTX_Jan.MostraFormTXPlCCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmTXPlCCab, FmTXPlCCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTXPlCCab.LocCod(Codigo, Codigo);
      if Controle <> 0 then
        FmTXPlCCab.QrTXPlCIts.Locate('Controle', Controle, []);
    end;
    FmTXPlCCab.ShowModal;
    FmTXPlCCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXRRMCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTXRRMCab, FmTXRRMCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTXRRMCab.LocCod(Codigo, Codigo);
    FmTXRRMCab.ShowModal;
    FmTXRRMCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTXSerTal();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'TXSerTal', 60,
  UnDmkEnums.TNovoCodigo.ncGerlSeq1, 'S�ries de Tal�o',
  [], False, Null, [], [], False);
end;

function TUnTX_Jan.MostraFormTXTalPsq(const MovimID: TEstqMovimID;
  const MovimNivs: array of TEstqMovimNiv; const SQLType: TSQLType; var Codigo,
  Controle: Integer; OnlySelIMEI: Boolean): Boolean;
var
  I: Integer;
begin
  Codigo   := 0;
  Controle := 0;
  //
  if DBCheck.CriaFm(TFmTXTalPsq, FmTXTalPsq, afmoNegarComAviso) then
  begin
    FmTXTalPsq.ImgTipo.SQLType := SQLType;
    FmTXTalPsq.FMovimID  := MovimID;
    FmTXTalPsq.FCodigo   := 0;
    FmTXTalPsq.FControle := 0;
    FmTXTalPsq.ReopenMovimNiv(MovimID);
    FmTXTalPsq.FOnlySelIMEI := OnlySelIMEI;
    if Length(MovimNivs) > 0 then
    begin
      FmTXTalPsq.QrMovimNiv.First;
      while not FmTXTalPsq.QrMovimNiv.Eof do
      begin
        for I := Low(MovimNivs) to High(MovimNivs) do
        begin
          if TEstqMovimNiv(FmTXTalPsq.QrMovimNivMovimNiv.Value) = MovimNivs[I] then
            FmTXTalPsq.DBGMovimNiv.SelectedRows.CurrentRowSelected := True;
        end;
        FmTXTalPsq.QrMovimNiv.Next;
      end;
      FmTXTalPsq.QrMovimNiv.First;
    end;
    //
    FmTXTalPsq.ShowModal;
    //
    Codigo   := FmTXTalPsq.FCodigo;
    Controle := FmTXTalPsq.FControle;
    //
    FmTXTalPsq.Destroy;
  end;
  if (Codigo <> 0) and (Controle <> 0) then
    Result := True
  else
    Result := False;
end;

procedure TUnTX_Jan.MostraFormTXTrfLocCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTXTrfLocCab, FmTXTrfLocCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTXTrfLocCab.LocCod(Codigo, Codigo);
      //FmTXTrfLocCab.QrTXTrfLocIts.Locate('Controle', Controle, []);
    end;
    FmTXTrfLocCab.ShowModal;
    FmTXTrfLocCab.Destroy;
  end;
end;

procedure TUnTX_Jan.MostraFormTX_Do_IMEI(Controle: Integer);
var
  Qry: TmySQLQuery;
  MovimID, Codigo: Integer;
begin
  Geral.MB_Info('Falta fazer!! TUnTX_Jan.MostraFormTX_Do_IMEI()');
{ POIU
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, MovimID ',
    'FROM ' + CO_SEL_TAB_TMI,
    'WHERE Controle=' + Geral.FF0(Controle),
    '',
    'UNION',
    '',
    'SELECT Codigo, MovimID ',
    'FROM ' + CO_TAB_TMB,
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    if Qry.RecordCount > 0 then
    begin
      Codigo  := Qry.FieldByName('Codigo').AsInteger;
      MovimID := Qry.FieldByName('MovimID').AsInteger;
      //
      MostraFormTX_XXX(MovimID, Codigo, Controle);
    end else
      Geral.MB_Aviso(
      'N�o foi poss�vel localizar a janela de movimento do o IME-I: ' +
      Geral.FF0(Controle));
  finally
    Qry.Free;
  end;
}
end;

end.
