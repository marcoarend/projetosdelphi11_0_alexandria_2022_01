unit TabePrcImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, DB, dmkGeral, mySQLDbTables, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmTabePrcImp = class(TForm)
    Panel1: TPanel;
    StaticText2: TStaticText;
    StaticText1: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    PB1: TProgressBar;
    QrGraTamI: TmySQLQuery;
    QrGraTamIControle: TAutoIncField;
    QrGraTamINome: TWideStringField;
    DsGraTamI: TDataSource;
    QrGraGruC: TmySQLQuery;
    QrGraGruCNivel1: TIntegerField;
    QrGraGruCControle: TIntegerField;
    QrGraGruCGraCorCad: TIntegerField;
    QrGraGruCCodUsuGTC: TIntegerField;
    QrGraGruCNomeGTC: TWideStringField;
    DsGraGruC: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  //protected
    { Protected declarations }
    //procedure CreateParams(var Params: TCreateParams); override;
  public
    { Public declarations }
    procedure ImprimeListaPrecos();
    procedure ReopenGraGruC();
    procedure ReopenGraTamI();
  end;

  var
  FmTabePrcImp: TFmTabePrcImp;

implementation

uses UnMyObjects, TabePrcCab, Module;

{$R *.DFM}

{
procedure TFmTabePrcImp.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  //Params.ExStyle := Params.Style or WS_CHILD;//WS_CHILD;//Para MSN > Params.ExStyle or WS_EX_APPWINDOW;
  //Params.WndParent := Application.MainForm.Handle;//Para MSN > GetDesktopWindow;
end;                  //FmTabePrcCab.Handle; ->> ERRO: Par�metro incorreto
}

procedure TFmTabePrcImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTabePrcImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTabePrcImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmTabePrcImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTabePrcImp.ImprimeListaPrecos;
//var
  //QrGru: TmySQLQuery;
begin
  //QrGru :=
  PB1.Position := 0;
  PB1.Max := FmTabePrcCab.QrTabePrcGrG.RecordCount;
  FmTabePrcCab.QrTabePrcGrG.First;
  while not FmTabePrcCab.QrTabePrcGrG.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    ReopenGraTamI();
    ReopenGraGruC();
    // Parei aqui estava fazendo o qu�?
    //
    FmTabePrcCab.QrTabePrcGrG.Next;
  end;
end;

procedure TFmTabePrcImp.ReopenGraGruC();
begin
  QrGraGruC.Close;
  QrGraGruC.Params[0].AsInteger := FmTabePrcCab.QrTabePrcGrGNivel1.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGruC, Dmod.MyDB);
end;

procedure TFmTabePrcImp.ReopenGraTamI();
begin
  QrGraTamI.Close;
  QrGraTamI.Params[0].AsInteger := FmTabePrcCab.QrTabePrcGrGNivel1.Value;
  UnDmkDAC_PF.AbreQuery(QrGraTamI, Dmod.MyDB);
  //
end;

end.
