object FmFatDivRef: TFmFatDivRef
  Left = 339
  Top = 185
  Caption = 'NFe-GERAL-005 :: Itens de Venda - Por Refer'#234'ncia'
  ClientHeight = 847
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 64
    Width = 1241
    Height = 650
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 369
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 71
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 10
          Top = 15
          Width = 58
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Empresa:'
        end
        object Label5: TLabel
          Left = 10
          Top = 44
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Pedido:'
        end
        object Label6: TLabel
          Left = 460
          Top = 44
          Width = 66
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Prioridade:'
        end
        object Label9: TLabel
          Left = 603
          Top = 44
          Width = 124
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Previs'#227'o entrega:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 896
          Top = 44
          Width = 71
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID Faturam.:'
        end
        object Label11: TLabel
          Left = 1044
          Top = 44
          Width = 102
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo Faturam.:'
        end
        object Label36: TLabel
          Left = 1009
          Top = 15
          Width = 66
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Encerrado:'
        end
        object Label37: TLabel
          Left = 807
          Top = 15
          Width = 54
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Abertura:'
        end
        object DBEdit5: TDBEdit
          Left = 74
          Top = 10
          Width = 65
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Filial'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 140
          Top = 10
          Width = 664
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMEEMP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 527
          Top = 39
          Width = 65
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Prioridade'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit9: TDBEdit
          Left = 732
          Top = 39
          Width = 159
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'DtaPrevi'
          DataSource = DsPediVda
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit53: TDBEdit
          Left = 970
          Top = 39
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Codigo'
          DataSource = DsFatPedCab
          TabOrder = 4
        end
        object DBEdit54: TDBEdit
          Left = 1147
          Top = 39
          Width = 69
          Height = 27
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CodUsu'
          DataSource = DsFatPedCab
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object DBEdit57: TDBEdit
          Left = 69
          Top = 39
          Width = 114
          Height = 27
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'PEDIDO_TXT'
          DataSource = DsFatPedCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object DBEdit59: TDBEdit
          Left = 1078
          Top = 10
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ENCERROU_TXT'
          DataSource = DsFatPedCab
          TabOrder = 7
        end
        object DBEdit58: TDBEdit
          Left = 866
          Top = 10
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Abertura'
          DataSource = DsFatPedCab
          TabOrder = 8
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 71
        Width = 1241
        Height = 101
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 1
        object Label12: TLabel
          Left = 10
          Top = 15
          Width = 44
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente:'
        end
        object DBText2: TDBText
          Left = 1024
          Top = 15
          Width = 55
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOME_TIPO_DOC'
          DataSource = DsCli
        end
        object DBEdit10: TDBEdit
          Left = 69
          Top = 10
          Width = 89
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CodUsu'
          DataSource = DsCli
          Enabled = False
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 162
          Top = 10
          Width = 853
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOME_ENT'
          DataSource = DsCli
          Enabled = False
          TabOrder = 1
        end
        object DBEdit14: TDBEdit
          Left = 1078
          Top = 10
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CNPJ_TXT'
          DataSource = DsCli
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object DBMemo3: TDBMemo
          Left = 69
          Top = 38
          Width = 1147
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'E_ALL'
          DataSource = DsCli
          Enabled = False
          TabOrder = 3
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 172
        Width = 1241
        Height = 42
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 2
        object Label14: TLabel
          Left = 10
          Top = 15
          Width = 92
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Representante:'
        end
        object Label15: TLabel
          Left = 788
          Top = 14
          Width = 150
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% comiss'#227'o faturamento:'
        end
        object Label19: TLabel
          Left = 1008
          Top = 14
          Width = 155
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% comiss'#227'o recebimento:'
        end
        object DBEdit15: TDBEdit
          Left = 103
          Top = 10
          Width = 71
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CODUSU_ACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit17: TDBEdit
          Left = 177
          Top = 10
          Width = 602
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMEACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit18: TDBEdit
          Left = 935
          Top = 10
          Width = 59
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ComisFat'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit19: TDBEdit
          Left = 1156
          Top = 10
          Width = 59
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ComisRec'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 214
        Width = 1241
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = ' Fiscal:'
        TabOrder = 3
        object Label20: TLabel
          Left = 10
          Top = 25
          Width = 92
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Movimenta'#231#227'o:'
        end
        object Label21: TLabel
          Left = 788
          Top = 25
          Width = 70
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Modelo NF:'
        end
        object DBEdit20: TDBEdit
          Left = 103
          Top = 20
          Width = 71
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CODUSU_FRC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit21: TDBEdit
          Left = 177
          Top = 20
          Width = 602
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMEFISREGCAD'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit22: TDBEdit
          Left = 862
          Top = 20
          Width = 353
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMEMODELONF'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 267
        Width = 1241
        Height = 102
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 4
        object Label23: TLabel
          Left = 10
          Top = 44
          Width = 97
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Transportadora:'
        end
        object Label24: TLabel
          Left = 10
          Top = 74
          Width = 82
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Redespacho:'
        end
        object Label4: TLabel
          Left = 10
          Top = 15
          Width = 57
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Frete por:'
        end
        object Label7: TLabel
          Left = 330
          Top = 15
          Width = 127
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Desp. acess'#243'rias: '
          Visible = False
        end
        object Label22: TLabel
          Left = 497
          Top = 15
          Width = 122
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '$ Desp. acess'#243'rias: '
          Visible = False
        end
        object Label25: TLabel
          Left = 704
          Top = 15
          Width = 49
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Frete:'
          Visible = False
        end
        object Label26: TLabel
          Left = 802
          Top = 15
          Width = 44
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '$ Frete:'
          Visible = False
        end
        object Label27: TLabel
          Left = 950
          Top = 15
          Width = 62
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Seguro:'
          Visible = False
        end
        object Label28: TLabel
          Left = 1063
          Top = 15
          Width = 57
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '$ Seguro:'
          Visible = False
        end
        object DBEdit26: TDBEdit
          Left = 108
          Top = 39
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CODUSU_TRA'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit27: TDBEdit
          Left = 108
          Top = 69
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CODUSU_RED'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit28: TDBEdit
          Left = 182
          Top = 39
          Width = 1033
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMETRANSP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit29: TDBEdit
          Left = 182
          Top = 69
          Width = 1033
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMEREDESP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
        object DBEdit24: TDBEdit
          Left = 69
          Top = 10
          Width = 26
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'FretePor'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 4
        end
        object DBEdit25: TDBEdit
          Left = 94
          Top = 10
          Width = 231
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMEFRETEPOR'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 5
        end
        object DBEdit30: TDBEdit
          Left = 448
          Top = 10
          Width = 44
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'DesoAces_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 6
          Visible = False
        end
        object DBEdit31: TDBEdit
          Left = 620
          Top = 10
          Width = 79
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'DesoAces_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 7
          Visible = False
        end
        object DBEdit32: TDBEdit
          Left = 753
          Top = 10
          Width = 45
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Frete_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 8
          Visible = False
        end
        object DBEdit33: TDBEdit
          Left = 857
          Top = 10
          Width = 88
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Frete_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 9
          Visible = False
        end
        object DBEdit51: TDBEdit
          Left = 1014
          Top = 10
          Width = 44
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Seguro_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 10
          Visible = False
        end
        object DBEdit52: TDBEdit
          Left = 1127
          Top = 10
          Width = 88
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Seguro_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 11
          Visible = False
        end
      end
    end
    object DBGFatDivRef: TdmkDBGridDAC
      Left = 0
      Top = 369
      Width = 1241
      Height = 105
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          ReadOnly = True
          Title.Caption = 'Item'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Referencia'
          Title.Caption = 'Refer'#234'ncia'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerDescoVal'
          Title.Caption = '% DA'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoAut'
          Title.Caption = 'Pre'#231'o autoriz.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoVen'
          Title.Caption = 'Pre'#231'o vendido'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerComissF'
          Title.Caption = '% CF'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerComissR'
          Title.Caption = '%CR'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VOLUME'
          Title.Caption = 'Volume'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Bxa Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Title.Caption = 'Baixa kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = 'Baixa m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CENTRO'
          Title.Caption = 'CE'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          ReadOnly = True
          Title.Caption = 'Reduzido'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_GGX'
          ReadOnly = True
          Title.Caption = 'Nome / tamanho / cor do produto'
          Width = 179
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoOri'
          ReadOnly = True
          Title.Caption = 'Pre'#231'o tabela'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoPrz'
          Title.Caption = 'Pre'#231'o prazo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTotBru'
          ReadOnly = True
          Title.Caption = 'Total bruto'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTotLiq'
          ReadOnly = True
          Title.Caption = 'Total L'#237'quido'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerComissNiv'
          Title.Caption = 'N'
          Width = 16
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TIPOCALC_TXT'
          Title.Caption = 'M'
          Width = 16
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IPI_ALQ'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MADEBY'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PROD_INDTOT'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFatDivRef
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnColEnter = DBGFatDivRefColEnter
      OnKeyDown = DBGFatDivRefKeyDown
      OnKeyUp = DBGFatDivRefKeyUp
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          ReadOnly = True
          Title.Caption = 'Item'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Referencia'
          Title.Caption = 'Refer'#234'ncia'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerDescoVal'
          Title.Caption = '% DA'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoAut'
          Title.Caption = 'Pre'#231'o autoriz.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoVen'
          Title.Caption = 'Pre'#231'o vendido'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerComissF'
          Title.Caption = '% CF'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerComissR'
          Title.Caption = '%CR'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VOLUME'
          Title.Caption = 'Volume'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pecas'
          Title.Caption = 'Bxa Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Peso'
          Title.Caption = 'Baixa kg'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = 'Baixa m'#178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CENTRO'
          Title.Caption = 'CE'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          ReadOnly = True
          Title.Caption = 'Reduzido'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_GGX'
          ReadOnly = True
          Title.Caption = 'Nome / tamanho / cor do produto'
          Width = 179
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoOri'
          ReadOnly = True
          Title.Caption = 'Pre'#231'o tabela'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoPrz'
          Title.Caption = 'Pre'#231'o prazo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTotBru'
          ReadOnly = True
          Title.Caption = 'Total bruto'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTotLiq'
          ReadOnly = True
          Title.Caption = 'Total L'#237'quido'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PerComissNiv'
          Title.Caption = 'N'
          Width = 16
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TIPOCALC_TXT'
          Title.Caption = 'M'
          Width = 16
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IPI_ALQ'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MADEBY'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PROD_INDTOT'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 518
      Width = 1241
      Height = 132
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox9: TGroupBox
        Left = 0
        Top = 0
        Width = 748
        Height = 132
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Item Selecionado: '
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 2
          Top = 18
          Width = 186
          Height = 79
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Comiss'#227'o no Faturamento: '
          TabOrder = 0
          object Label18: TLabel
            Left = 5
            Top = 20
            Width = 79
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% Comiss'#227'o:'
            FocusControl = DBEdit2
          end
          object Label31: TLabel
            Left = 84
            Top = 20
            Width = 82
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor L'#237'quido:'
            FocusControl = DBEdit6
          end
          object DBEdit2: TDBEdit
            Left = 5
            Top = 39
            Width = 74
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PerComissF'
            DataSource = DsFatDivRef
            TabOrder = 0
          end
          object DBEdit6: TDBEdit
            Left = 84
            Top = 39
            Width = 88
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValComisLiqF'
            DataSource = DsFatDivRef
            TabOrder = 1
          end
        end
        object GroupBox7: TGroupBox
          Left = 188
          Top = 18
          Width = 187
          Height = 79
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Comiss'#227'o no Recebimento: '
          TabOrder = 1
          object Label32: TLabel
            Left = 5
            Top = 20
            Width = 79
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% Comiss'#227'o:'
            FocusControl = DBEdit11
          end
          object Label35: TLabel
            Left = 84
            Top = 20
            Width = 82
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor L'#237'quido:'
            FocusControl = DBEdit23
          end
          object DBEdit11: TDBEdit
            Left = 5
            Top = 39
            Width = 74
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PerComissR'
            DataSource = DsFatDivRef
            TabOrder = 0
          end
          object DBEdit23: TDBEdit
            Left = 84
            Top = 39
            Width = 88
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValComisLiqR'
            DataSource = DsFatDivRef
            TabOrder = 1
          end
        end
        object GroupBox8: TGroupBox
          Left = 375
          Top = 18
          Width = 371
          Height = 79
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Comiss'#227'o Faturamento + recebimento: '
          TabOrder = 2
          object Label38: TLabel
            Left = 5
            Top = 20
            Width = 79
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% Comiss'#227'o:'
            FocusControl = DBEdit34
          end
          object Label39: TLabel
            Left = 84
            Top = 20
            Width = 68
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor bruto:'
            FocusControl = DBEdit35
          end
          object Label40: TLabel
            Left = 177
            Top = 20
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Desconto:'
            FocusControl = DBEdit36
          end
          object Label41: TLabel
            Left = 271
            Top = 20
            Width = 82
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor L'#237'quido:'
            FocusControl = DBEdit37
          end
          object DBEdit34: TDBEdit
            Left = 5
            Top = 39
            Width = 74
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PerComissT'
            DataSource = DsFatDivRef
            TabOrder = 0
          end
          object DBEdit35: TDBEdit
            Left = 84
            Top = 39
            Width = 88
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValComisBruT'
            DataSource = DsFatDivRef
            TabOrder = 1
          end
          object DBEdit36: TDBEdit
            Left = 177
            Top = 39
            Width = 89
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValComisDesT'
            DataSource = DsFatDivRef
            TabOrder = 2
          end
          object DBEdit37: TDBEdit
            Left = 271
            Top = 39
            Width = 88
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValComisLiqT'
            DataSource = DsFatDivRef
            TabOrder = 3
          end
        end
        object Panel4: TPanel
          Left = 2
          Top = 97
          Width = 744
          Height = 33
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 3
          object PB1: TProgressBar
            Left = 33
            Top = 7
            Width = 666
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
          end
        end
      end
      object GroupBox10: TGroupBox
        Left = 748
        Top = 0
        Width = 493
        Height = 132
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Totais: '
        TabOrder = 1
        object Label29: TLabel
          Left = 10
          Top = 15
          Width = 31
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Itens:'
          FocusControl = DBEdit3
        end
        object Label30: TLabel
          Left = 49
          Top = 15
          Width = 73
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Quantidade:'
          FocusControl = DBEdit4
        end
        object Label33: TLabel
          Left = 158
          Top = 15
          Width = 96
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor total bruto:'
          FocusControl = DBEdit12
        end
        object Label34: TLabel
          Left = 266
          Top = 15
          Width = 98
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor tot. l'#237'quido:'
          FocusControl = DBEdit16
        end
        object Label42: TLabel
          Left = 374
          Top = 15
          Width = 85
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Val. comis. fat:'
          FocusControl = DBEdit38
        end
        object Label43: TLabel
          Left = 10
          Top = 64
          Width = 90
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Val comis. rec.:'
          FocusControl = DBEdit39
        end
        object Label44: TLabel
          Left = 113
          Top = 64
          Width = 92
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Comis. tot. brut.:'
          FocusControl = DBEdit40
        end
        object Label45: TLabel
          Left = 217
          Top = 64
          Width = 97
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Comis. tot desc.:'
          FocusControl = DBEdit41
        end
        object Label46: TLabel
          Left = 320
          Top = 64
          Width = 84
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Comis. tot. liq.:'
          FocusControl = DBEdit42
        end
        object Label13: TLabel
          Left = 418
          Top = 64
          Width = 59
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Comis.:'
          FocusControl = DBEdit43
        end
        object DBEdit3: TDBEdit
          Left = 10
          Top = 34
          Width = 34
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ITENS'
          DataSource = DsSumRef
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 49
          Top = 34
          Width = 104
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Qtde'
          DataSource = DsSumRef
          TabOrder = 1
        end
        object DBEdit12: TDBEdit
          Left = 158
          Top = 34
          Width = 103
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ValorTotBru'
          DataSource = DsSumRef
          TabOrder = 2
        end
        object DBEdit16: TDBEdit
          Left = 266
          Top = 34
          Width = 103
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ValorTotLiq'
          DataSource = DsSumRef
          TabOrder = 3
        end
        object DBEdit38: TDBEdit
          Left = 374
          Top = 34
          Width = 104
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ValComisLiqF'
          DataSource = DsSumRef
          TabOrder = 4
        end
        object DBEdit39: TDBEdit
          Left = 10
          Top = 84
          Width = 98
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ValComisLiqR'
          DataSource = DsSumRef
          TabOrder = 5
        end
        object DBEdit40: TDBEdit
          Left = 113
          Top = 84
          Width = 99
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ValComisBruT'
          DataSource = DsSumRef
          TabOrder = 6
        end
        object DBEdit41: TDBEdit
          Left = 217
          Top = 84
          Width = 98
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ValComisDesT'
          DataSource = DsSumRef
          TabOrder = 7
        end
        object DBEdit42: TDBEdit
          Left = 320
          Top = 84
          Width = 94
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ValComisLiqT'
          DataSource = DsSumRef
          TabOrder = 8
        end
        object DBEdit43: TDBEdit
          Left = 418
          Top = 84
          Width = 60
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'PERCOMISSL'
          DataSource = DsSumRef
          TabOrder = 9
        end
      end
    end
    object ST1: TStaticText
      Left = 0
      Top = 474
      Width = 32
      Height = 22
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Alignment = taCenter
      Caption = 'ST1'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 3
      Transparent = False
      Visible = False
    end
    object ST2: TStaticText
      Left = 0
      Top = 496
      Width = 32
      Height = 22
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Alignment = taCenter
      Caption = 'ST2'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      Transparent = False
      Visible = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 361
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Caption = 
        ' Key:    Campo:                    Valor:                      C' +
        'onfirmou: '
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 18
        Width = 357
        Height = 365
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object EdKey: TdmkEdit
          Left = 5
          Top = 5
          Width = 41
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCampo: TdmkEdit
          Left = 49
          Top = 5
          Width = 113
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdValor: TdmkEdit
          Left = 167
          Top = 5
          Width = 114
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdConfirmou: TdmkEdit
          Left = 286
          Top = 5
          Width = 64
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
    object GB_M: TGroupBox
      Left = 361
      Top = 0
      Width = 821
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 456
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Venda - Por Refer'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 456
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Venda - Por Refer'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 456
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Venda - Por Refer'#234'ncia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 714
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 768
    Width = 1241
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1059
        Top = 0
        Width = 178
        Height = 0
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
      end
      object PainelConfirma: TPanel
        Left = 0
        Top = 0
        Width = 1237
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Label47: TLabel
          Left = 143
          Top = 2
          Width = 144
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CE = Centro de estoque.'
        end
        object Label48: TLabel
          Left = 143
          Top = 22
          Width = 175
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% DA = Desconto/acr'#233'scimo.'
        end
        object Label49: TLabel
          Left = 340
          Top = 2
          Width = 200
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% CF = Comiss'#227'o no faturamento.'
        end
        object Label50: TLabel
          Left = 340
          Top = 22
          Width = 207
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% CR = Comiss'#227'o no recebimento.'
        end
        object Label51: TLabel
          Left = 143
          Top = 39
          Width = 420
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'N = N'#237'vel de cadastro de produto em que foi obtido os % de comis' +
            's'#227'o.'
        end
        object Label52: TLabel
          Left = 561
          Top = 39
          Width = 307
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M = Mov. estoque [B] Baixa, [A] Adiciona e [N] Nulo.'
        end
        object BtConverte: TBitBtn
          Tag = 14
          Left = 25
          Top = 5
          Width = 110
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Converte'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConverteClick
        end
        object Panel2: TPanel
          Left = 1080
          Top = 0
          Width = 157
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 15
            Left = 22
            Top = 4
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object PnJuros: TPanel
          Left = 887
          Top = 0
          Width = 193
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvLowered
          Enabled = False
          TabOrder = 2
          object Label16: TLabel
            Left = 10
            Top = 5
            Width = 80
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% Juros m'#234's:'
          end
          object Label17: TLabel
            Left = 94
            Top = 5
            Width = 93
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% Custo financ.:'
          end
          object EdCustoFin: TdmkEdit
            Left = 94
            Top = 25
            Width = 93
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdJurosMes: TdmkEdit
            Left = 10
            Top = 25
            Width = 80
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
  object DsFatPedCab: TDataSource
    DataSet = FmFatDivGer.QrFatPedCab
    Left = 848
    Top = 16
  end
  object DsPediVda: TDataSource
    DataSet = FmFatDivGer.QrPediVda
    Left = 876
    Top = 16
  end
  object DsCli: TDataSource
    DataSet = FmFatDivGer.QrCli
    Left = 904
    Top = 16
  end
  object TbFatDivRef: TMySQLTable
    Database = Dmod.MyDB
    Filtered = True
    BeforeOpen = TbFatDivRefBeforeOpen
    BeforePost = TbFatDivRefBeforePost
    AfterPost = TbFatDivRefAfterPost
    BeforeDelete = TbFatDivRefBeforeDelete
    AfterDelete = TbFatDivRefAfterDelete
    OnCalcFields = TbFatDivRefCalcFields
    TableName = 'fatdivref'
    Left = 12
    Top = 380
    object TbFatDivRefSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      DisplayFormat = '000;-000; '
      Calculated = True
    end
    object TbFatDivRefCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatdivref.Codigo'
    end
    object TbFatDivRefIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'fatdivref.IDCtrl'
    end
    object TbFatDivRefStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'fatdivref.StqCenCad'
      DisplayFormat = '0;-0; '
    end
    object TbFatDivRefReferencia: TWideStringField
      FieldName = 'Referencia'
      Origin = 'fatdivref.Referencia'
      Size = 10
    end
    object TbFatDivRefGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'fatdivref.GraGruX'
      DisplayFormat = '0;-0; '
    end
    object TbFatDivRefQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'fatdivref.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object TbFatDivRefPrecoOri: TFloatField
      FieldName = 'PrecoOri'
      Origin = 'fatdivref.PrecoOri'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object TbFatDivRefPrecoPrz: TFloatField
      FieldName = 'PrecoPrz'
      Origin = 'fatdivref.PrecoPrz'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object TbFatDivRefPrecoAut: TFloatField
      FieldName = 'PrecoAut'
      Origin = 'fatdivref.PrecoAut'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object TbFatDivRefPrecoVen: TFloatField
      FieldName = 'PrecoVen'
      Origin = 'fatdivref.PrecoVen'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object TbFatDivRefAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'fatdivref.Ativo'
    end
    object TbFatDivRefValorTotBru: TFloatField
      FieldName = 'ValorTotBru'
      Origin = 'fatdivref.ValorTotBru'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefPerDescoVal: TFloatField
      FieldName = 'PerDescoVal'
      Origin = 'fatdivref.PerDescoVal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefValorTotLiq: TFloatField
      FieldName = 'ValorTotLiq'
      Origin = 'fatdivref.ValorTotLiq'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefPerComissF: TFloatField
      FieldName = 'PerComissF'
      Origin = 'fatdivref.PerComissF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefValComisLiqF: TFloatField
      FieldName = 'ValComisLiqF'
      Origin = 'fatdivref.ValComisLiqF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefPerComissR: TFloatField
      FieldName = 'PerComissR'
      Origin = 'fatdivref.PerComissR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefValComisLiqR: TFloatField
      FieldName = 'ValComisLiqR'
      Origin = 'fatdivref.ValComisLiqR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefPerComissT: TFloatField
      FieldName = 'PerComissT'
      Origin = 'fatdivref.PerComissT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefValComisBruT: TFloatField
      FieldName = 'ValComisBruT'
      Origin = 'fatdivref.ValComisBruT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefValComisDesT: TFloatField
      FieldName = 'ValComisDesT'
      Origin = 'fatdivref.ValComisDesT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefValComisLiqT: TFloatField
      FieldName = 'ValComisLiqT'
      Origin = 'fatdivref.ValComisLiqT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefPerComissNiv: TWideStringField
      FieldName = 'PerComissNiv'
      Origin = 'fatdivref.PerComissNiv'
      Size = 1
    end
    object TbFatDivRefVolCnta: TIntegerField
      FieldName = 'VolCnta'
      Origin = 'fatdivref.VolCnta'
    end
    object TbFatDivRefTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
    object TbFatDivRefVOLUME: TIntegerField
      FieldKind = fkLookup
      FieldName = 'VOLUME'
      LookupDataSet = QrFatPedVol
      LookupKeyFields = 'Cnta'
      LookupResultField = 'Cnta'
      KeyFields = 'VolCnta'
      Lookup = True
    end
    object TbFatDivRefCENTRO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CENTRO'
      LookupDataSet = QrStqCenCad
      LookupKeyFields = 'CodUsu'
      LookupResultField = 'Codigo'
      KeyFields = 'StqCenCad'
      Lookup = True
    end
    object TbFatDivRefNO_GGX: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_GGX'
      LookupDataSet = QrFatDivRef
      LookupKeyFields = 'IDCtrl'
      LookupResultField = 'NO_PRD_TAM_COR'
      KeyFields = 'IDCtrl'
      Size = 150
      Lookup = True
    end
    object TbFatDivRefPROD_INDTOT: TSmallintField
      FieldKind = fkLookup
      FieldName = 'PROD_INDTOT'
      LookupDataSet = QrFatDivRef
      LookupKeyFields = 'IDCtrl'
      LookupResultField = 'prod_indTot'
      KeyFields = 'IDCtrl'
      Lookup = True
    end
    object TbFatDivRefMADEBY: TSmallintField
      FieldKind = fkLookup
      FieldName = 'MADEBY'
      LookupDataSet = QrFatDivRef
      LookupKeyFields = 'IDCtrl'
      LookupResultField = 'MadeBy'
      KeyFields = 'IDCtrl'
      Lookup = True
    end
    object TbFatDivRefIPI_ALQ: TFloatField
      FieldKind = fkLookup
      FieldName = 'IPI_ALQ'
      LookupDataSet = QrFatDivRef
      LookupKeyFields = 'IDCtrl'
      LookupResultField = 'IPI_Alq'
      KeyFields = 'IDCtrl'
      Lookup = True
    end
    object TbFatDivRefTIPOCALC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOCALC_TXT'
      Size = 1
      Calculated = True
    end
    object TbFatDivRefHOWBXAESTQ: TSmallintField
      FieldKind = fkLookup
      FieldName = 'HOWBXAESTQ'
      LookupDataSet = QrFatDivRef
      LookupKeyFields = 'IDCtrl'
      LookupResultField = 'HowBxaEstq'
      KeyFields = 'IDCtrl'
      Lookup = True
    end
    object TbFatDivRefPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object TbFatDivRefPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object TbFatDivRefAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbFatDivRefGerBxaEstq: TSmallintField
      FieldKind = fkLookup
      FieldName = 'GerBxaEstq'
      LookupDataSet = QrFatDivRef
      LookupKeyFields = 'IDCtrl'
      LookupResultField = 'GerBxaEstq'
      KeyFields = 'IDCtrl'
      Lookup = True
    end
  end
  object DsFatDivRef: TDataSource
    DataSet = TbFatDivRef
    Left = 40
    Top = 380
  end
  object QrLista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 732
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object QrLocComiss1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PerComissF, PerComissR, PerComissZ'
      'FROM GraGru1'
      'WHERE Nivel1=:P0')
    Left = 756
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocComiss1PerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrLocComiss1PerComissR: TFloatField
      FieldName = 'PerComissR'
    end
    object QrLocComiss1PerComissZ: TSmallintField
      FieldName = 'PerComissZ'
    end
  end
  object QrLocComissX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 784
    Top = 8
    object QrLocComissXPerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrLocComissXPerComissR: TFloatField
      FieldName = 'PerComissR'
    end
  end
  object DsSumRef: TDataSource
    DataSet = QrSumRef
    Left = 96
    Top = 380
  end
  object QrSumRef: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSumRefAfterOpen
    BeforeClose = QrSumRefBeforeClose
    OnCalcFields = QrSumRefCalcFields
    SQL.Strings = (
      'SELECT COUNT(IDCtrl) ITENS, SUM(Qtde) Qtde,'
      'SUM(ValorTotBru) ValorTotBru,'
      'SUM(ValorTotLiq) ValorTotLiq,'
      'SUM(ValComisLiqF) ValComisLiqF,'
      'SUM(ValComisLiqR) ValComisLiqR,'
      'SUM(ValComisBruT) ValComisBruT,'
      'SUM(ValComisDesT) ValComisDesT,'
      'SUM(ValComisLiqT) ValComisLiqT'
      'FROM fatdivref'
      'WHERE Codigo=:P0')
    Left = 68
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumRefITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
      DisplayFormat = '000;-000; '
    end
    object QrSumRefQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSumRefValorTotBru: TFloatField
      FieldName = 'ValorTotBru'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumRefValorTotLiq: TFloatField
      FieldName = 'ValorTotLiq'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumRefValComisLiqF: TFloatField
      FieldName = 'ValComisLiqF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumRefValComisLiqR: TFloatField
      FieldName = 'ValComisLiqR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumRefValComisBruT: TFloatField
      FieldName = 'ValComisBruT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumRefValComisDesT: TFloatField
      FieldName = 'ValComisDesT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumRefValComisLiqT: TFloatField
      FieldName = 'ValComisLiqT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumRefPERCOMISSL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERCOMISSL'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
      Calculated = True
    end
  end
  object QrFatPedVol: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Cnta '
      'FROM fatpedvol'
      'WHERE Codigo=:P0')
    Left = 124
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatPedVolCnta: TIntegerField
      FieldName = 'Cnta'
    end
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad scc'
      'WHERE Codigo IN '
      '('
      '  SELECT StqCenCad'
      '  FROM fisregmvt'
      '  WHERE Codigo=:P0'
      '  AND Empresa=:P1'
      ') '
      'ORDER BY Nome')
    Left = 152
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object QrFatDivRef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fdr.IDCtrl, fdr.GraGruX, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.prod_indTot, gg1.IPI_Alq,'
      'pgt.MadeBy, gg1.HowBxaEstq, gg1.GerBxaEstq, '
      'pgt.Fracio'
      'FROM fatdivref fdr'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=fdr.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fdr.Codigo=:P0')
    Left = 12
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatDivRefIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrFatDivRefGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFatDivRefNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrFatDivRefprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrFatDivRefIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrFatDivRefMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrFatDivRefHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrFatDivRefGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrFatDivRefFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object QrLocSMIA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM stqmovitsa'
      'WHERE IDCtrl=:P0')
    Left = 180
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
end
