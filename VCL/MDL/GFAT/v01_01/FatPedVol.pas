unit FatPedVol;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral, Variants, dmkValUsu,
  dmkImage, UnDmkEnums, DmkDAC_PF, UnProjGroup_Vars, UnGFat_Jan, UnGrl_Vars;

type
  TFmFatPedVol = class(TForm)
    Panel1: TPanel;
    EdCnta: TdmkEdit;
    Label2: TLabel;
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedNome: TWideStringField;
    QrUnidMedSigla: TWideStringField;
    DsUnidMed: TDataSource;
    EdUnidMed: TdmkEditCB;
    Label1: TLabel;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenUnidMed();
  public
    { Public declarations }
    FChamouFatPedVol: TChamouFatPedVol;
    FIncluiuVol: Boolean;
  end;

  var
  FmFatPedVol: TFmFatPedVol;

implementation

uses UnMyObjects, Module, UMySQLModule, Principal, UnInternalConsts,
{$IfNDef SemPediVda} FatPedCab1, FatPedCab2, {$EndIf}
FatDivGer1, FatDivGer2, UnGrade_Jan;

{$R *.DFM}

procedure TFmFatPedVol.BtOKClick(Sender: TObject);
var
  Codigo, UnidMed, Cnta: Integer;
begin
  UnidMed := Geral.IMV(EdUnidMed.Text);
  //
  if MyObjects.FIC(UnidMed = 0, EdUnidMed, 'Informe a unidade de medida!') then Exit;
  //
  case FChamouFatPedVol of
{$IfNDef SemPediVda}
    cfpvFatPedCab:
    begin
      if VAR_NT2018_05v120 then
        Codigo := FmFatPedCab2.QrFatPedCabCodigo.Value
      else
        Codigo := FmFatPedCab1.QrFatPedCabCodigo.Value;
    end;
{$EndIf}
    cfpvFatDivGer:
    begin
      if VAR_NT2018_05v120 then
        Codigo := FmFatDivGer2.QrFatPedCabCodigo.Value
      else
        Codigo := FmFatDivGer1.QrFatPedCabCodigo.Value;
    end
    else
    begin
      Codigo := 0;
      //
      Geral.MB_Aviso('ERRO. N�o foi definido o formul�rio fonte!');
    end;
  end;
  Cnta := EdCnta.ValueVariant;
  Cnta := UMyMod.BuscaEmLivreY_Def('fatpedvol', 'Cnta', ImgTipo.SQLType, Cnta);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fatpedvol', False, [
  'Codigo', 'UnidMed'], ['Cnta'], [Codigo, UnidMed], [Cnta], True) then
  begin
    case FChamouFatPedVol of
{$IfNDef SemPediVda}
      cfpvFatPedCab:
      begin
        if VAR_NT2018_05v120 then
          FmFatPedCab2.ReopenFatPedVol(Cnta)
        else
          FmFatPedCab1.ReopenFatPedVol(Cnta);
      end;
{$EndIf}
      cfpvFatDivGer:
      begin
        if VAR_NT2018_05v120 then
          FmFatDivGer2.ReopenFatPedVol(Cnta)
        else
          FmFatDivGer1.ReopenFatPedVol(Cnta);
      end;
    end;
    FIncluiuVol := True;
    Close;
  end;
end;

procedure TFmFatPedVol.BtSaidaClick(Sender: TObject);
begin
  FIncluiuVol := False;
  Close;
end;

procedure TFmFatPedVol.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedVol.FormCreate(Sender: TObject);
begin
  FIncluiuVol := False;
  ReopenUnidMed();
end;

procedure TFmFatPedVol.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatPedVol.ReopenUnidMed();
begin
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
end;

procedure TFmFatPedVol.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if Grade_Jan.MostraFormUnidMed(EdUnidMed.ValueVariant) then
  begin
    ReopenUnidMed();
    EdUnidMed.ValueVariant := VAR_CADASTRO;
    CBUnidMed.KeyValue     := VAR_CADASTRO;
  end;
end;

end.
