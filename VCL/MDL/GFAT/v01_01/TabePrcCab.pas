unit TabePrcCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, DmkDAC_PF, dmkLabel, dmkRadioGroup, Grids,
  DBGrids, dmkDBGrid, Menus, ComCtrls, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupComboBox, dmkEditCB, dmkValUsu, dmkDBEdit, frxClass,
  frxDBSet, Variants, dmkImage, UnDmkEnums;

type
  TFmTabePrcCab = class(TForm)
    PainelDados: TPanel;
    DsTabePrcCab: TDataSource;
    QrTabePrcCab: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMTabela: TPopupMenu;
    QrTabePrcGrG: TmySQLQuery;
    DsTabePrcGrG: TDataSource;
    Panel4: TPanel;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    EdJurosMes: TdmkEdit;
    EdDescoMax: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    QrTabePrcPzG: TmySQLQuery;
    DsTabePrcPzG: TDataSource;
    QrTabePrcCabCodigo: TIntegerField;
    QrTabePrcCabCodUsu: TIntegerField;
    QrTabePrcCabNome: TWideStringField;
    QrTabePrcCabDescoMax: TFloatField;
    QrTabePrcCabJurosMes: TFloatField;
    QrTabePrcCabPrazoMed: TFloatField;
    QrTabePrcCabSigla: TWideStringField;
    QrTabePrcCabAplicacao: TSmallintField;
    QrTabePrcCabDataI: TDateField;
    QrTabePrcCabDataF: TDateField;
    Label4: TLabel;
    EdPrazoMed: TdmkEdit;
    dmkEditDateTimePicker1: TdmkEditDateTimePicker;
    dmkEditDateTimePicker2: TdmkEditDateTimePicker;
    Label5: TLabel;
    dmkEdit1: TdmkEdit;
    Label6: TLabel;
    Label10: TLabel;
    DBEdit2: TDBEdit;
    Label24: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    CGAplicacao: TdmkCheckGroup;
    Incluinovatabeladepreos1: TMenuItem;
    Alteratabelaselecionada1: TMenuItem;
    Excluitabelaselecionada1: TMenuItem;
    EdMoeda: TdmkEditCB;
    CBMoeda: TdmkDBLookupComboBox;
    Label11: TLabel;
    QrCambioMda: TmySQLQuery;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    DsCambioMda: TDataSource;
    dmkValUsu1: TdmkValUsu;
    DBEdit6: TDBEdit;
    Label14: TLabel;
    QrTabePrcCabNOMEMOEDA: TWideStringField;
    QrTabePrcCabMoeda: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PMGrupos: TPopupMenu;
    Incluinovoprodutoumaum1: TMenuItem;
    QrTabePrcGrGNOME1: TWideStringField;
    QrTabePrcGrGCodigo: TIntegerField;
    QrTabePrcGrGNivel1: TIntegerField;
    QrTabePrcGrGPreco: TFloatField;
    QrTabePrcGrGCodUsu: TIntegerField;
    Incluidiversosprodutos1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet5: TTabSheet;
    GradeP: TStringGrid;
    StaticText1: TStaticText;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrTabePrcGrGGraTamCad: TIntegerField;
    PMPrazos: TPopupMenu;
    Incluinovoperododeprazos1: TMenuItem;
    QrTabePrcPzGCodigo: TIntegerField;
    QrTabePrcPzGControle: TIntegerField;
    QrTabePrcPzGNome: TWideStringField;
    QrTabePrcPzGDiasIni: TSmallintField;
    QrTabePrcPzGDiasFim: TSmallintField;
    QrTabePrcPzGVariacao: TFloatField;
    DBGPrazos: TDBGrid;
    QrTabePrcPzGPRECO: TFloatField;
    Alteraperododeprazosatual1: TMenuItem;
    Excluiperododeprazosatual1: TMenuItem;
    TabSheet4: TTabSheet;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrPrecoPrz: TmySQLQuery;
    DsPrecoPrz: TDataSource;
    QrPrecoPrzCodigo: TIntegerField;
    QrPrecoPrzControle: TIntegerField;
    QrPrecoPrzNome: TWideStringField;
    QrPrecoPrzDiasIni: TSmallintField;
    QrPrecoPrzDiasFim: TSmallintField;
    QrPrecoPrzVariacao: TFloatField;
    QrPrecoPrzPRECO: TFloatField;
    PageControl3: TPageControl;
    GradeZ: TStringGrid;
    frxDsTabPrcGru: TfrxDBDataset;
    frxDsTabePrcCab: TfrxDBDataset;
    frxDsTabePrcPzG: TfrxDBDataset;
    QrTabPrcPrz: TmySQLQuery;
    frxDsTabPrcPrz: TfrxDBDataset;
    QrTabPrcPrzNOME1: TWideStringField;
    QrTabPrcPrzCodUsu: TIntegerField;
    QrTabPrcPrzNivel1: TIntegerField;
    QrTabPrcPrzPreco: TFloatField;
    QrTabPrcPrzCtrlPrz: TIntegerField;
    QrTabPrcPrzNOMEPRZ: TWideStringField;
    QrTabPrcPrzDiasIni: TSmallintField;
    QrTabPrcPrzDiasFim: TSmallintField;
    QrTabPrcPrzVariacao: TFloatField;
    QrTabPrcPrzPRC_PRZ: TFloatField;
    QrTabPrcPrzFATOR: TFloatField;
    QrTabPrcPrzPRC_PRZ_TXT: TWideStringField;
    QrPrcPrzIts: TmySQLQuery;
    QrPrcPrzItsNOMECOR: TWideStringField;
    QrPrcPrzItsNOMETAM: TWideStringField;
    QrPrcPrzItsPreco: TFloatField;
    QrPrcPrzItsNOMEPRZ: TWideStringField;
    QrPrcPrzItsFATOR: TFloatField;
    QrPrcPrzItsVAL_PRZ_PRD: TFloatField;
    QrPrcPrzItsNOMEGRU: TWideStringField;
    QrTabePrcGrGKGT: TIntegerField;
    QrTabPrcGru: TmySQLQuery;
    QrTabPrcGruKGT: TIntegerField;
    QrTabPrcGruNOME1: TWideStringField;
    QrTabPrcGruCodUsu: TIntegerField;
    QrTabPrcGruGraTamCad: TIntegerField;
    QrTabPrcGruCodigo: TIntegerField;
    QrTabPrcGruNivel1: TIntegerField;
    QrTabPrcGruPreco: TFloatField;
    frxPrcGruC_Port: TfrxReport;
    QrTabePrcPzGCOL_TXT: TWideStringField;
    frxPrcGruSem: TfrxReport;
    frxDsPrcPrzIts: TfrxDBDataset;
    PMImprime: TPopupMenu;
    GruposComGrades1: TMenuItem;
    GruposSemGrades1: TMenuItem;
    QrPrcPrzItsReduz: TIntegerField;
    Retrato1: TMenuItem;
    Paisagem1: TMenuItem;
    PMProdutos: TPopupMenu;
    Limpapreosdesnecessrios1: TMenuItem;
    frxPrcGruC_Land: TfrxReport;
    Reduzido1: TMenuItem;
    Alterapreodogrupoatual1: TMenuItem;
    Panel6: TPanel;
    DBGGrupos: TdmkDBGrid;
    Panel7: TPanel;
    EdFiltro1: TEdit;
    Label17: TLabel;
    TabSheet7: TTabSheet;
    PMNivel: TPopupMenu;
    IncluiNovoPrecodeNivel1: TMenuItem;
    QrTabePrcNiv: TmySQLQuery;
    DsTabePrcNiv: TDataSource;
    QrTabePrcNivCodigo: TIntegerField;
    QrTabePrcNivNivel: TIntegerField;
    QrTabePrcNivCodNiv: TIntegerField;
    QrTabePrcNivPreco: TFloatField;
    DBGNiveis: TdmkDBGrid;
    QrTabePrcNivNO_CODNIV: TWideStringField;
    AlteraPreodoItemSelecionado1: TMenuItem;
    Excluiitemns1: TMenuItem;
    Panel8: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtTabela: TBitBtn;
    BtGrupos: TBitBtn;
    BtPrazos: TBitBtn;
    BtProdutos: TBitBtn;
    BtNivel: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel10: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTabePrcCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTabePrcCabBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtTabelaClick(Sender: TObject);
    procedure PMTabelaPopup(Sender: TObject);
    procedure QrTabePrcCabBeforeClose(DataSet: TDataSet);
    procedure QrTabePrcCabAfterScroll(DataSet: TDataSet);
    procedure Incluinovatabeladepreos1Click(Sender: TObject);
    procedure Alteratabelaselecionada1Click(Sender: TObject);
    procedure Incluinovoprodutoumaum1Click(Sender: TObject);
    procedure Incluidiversosprodutos1Click(Sender: TObject);
    procedure GradePDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradePClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrTabePrcGrGAfterScroll(DataSet: TDataSet);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtGruposClick(Sender: TObject);
    procedure Incluinovoperododeprazos1Click(Sender: TObject);
    procedure BtPrazosClick(Sender: TObject);
    procedure Excluiperododeprazosatual1Click(Sender: TObject);
    procedure Alteraperododeprazosatual1Click(Sender: TObject);
    procedure QrPrecoPrzCalcFields(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure QrPrecoPrzAfterScroll(DataSet: TDataSet);
    procedure GradeZDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrTabPrcPrzCalcFields(DataSet: TDataSet);
    procedure QrPrcPrzItsCalcFields(DataSet: TDataSet);
    procedure QrTabePrcGrGCalcFields(DataSet: TDataSet);
    procedure QrTabPrcGruAfterScroll(DataSet: TDataSet);
    procedure QrTabPrcGruCalcFields(DataSet: TDataSet);
    procedure QrTabePrcPzGCalcFields(DataSet: TDataSet);
    procedure GruposSemGrades1Click(Sender: TObject);
    procedure Retrato1Click(Sender: TObject);
    procedure Paisagem1Click(Sender: TObject);
    procedure BtProdutosClick(Sender: TObject);
    procedure Limpapreosdesnecessrios1Click(Sender: TObject);
    procedure frxPrcGruC_LandGetValue(const VarName: string;
      var Value: Variant);
    procedure Reduzido1Click(Sender: TObject);
    procedure Alterapreodogrupoatual1Click(Sender: TObject);
    procedure EdFiltro1Change(Sender: TObject);
    procedure IncluiNovoPrecodeNivel1Click(Sender: TObject);
    procedure BtNivelClick(Sender: TObject);
    procedure AlteraPreodoItemSelecionado1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
  private
    //FQtdPrzs: Integer;
    FImpGrades: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    // Pre�os
    function PrecoItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function PrecoVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    procedure ReopenPrecosPrz();
    //procedure RecriaQrPrzPrcPrd();
    procedure ImprimeGrupos(ImprimeGrades, Landscape: Boolean);
    procedure ReconfiguraGradeP();
    procedure ReconfiguraGradeZ();
    procedure MostraFmTabePrcNiv(SQLType: TSQLType);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTabePrcGrG(Nivel1: Integer);
    procedure ReopenTabePrcPzG(Controle: Integer);
    procedure ReopenTabePrcNiv(Nivel, CodNiv: Integer);
  end;

var
  FmTabePrcCab: TFmTabePrcCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MasterSelFilial, TabePrcGrG, TabePrcGrGMul,
  ModProd, GraGruVal, MyVCLSkin, TabePrcPzG, TabePrcImp, TabePrcNiv, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTabePrcCab.Limpapreosdesnecessrios1Click(Sender: TObject);
var
  Nivel1: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM tabeprcgri WHERE Preco=0');
    Dmod.QrUpd.ExecSQL;
    //
    Nivel1 := QrTabePrcGrGNivel1.Value;
    QrTabePrcGrG.First;
    while not QrTabePrcGrG.Eof do
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM tabeprcgri ');
      Dmod.QrUpd.SQL.Add('WHERE TabePrcCab=' + FormatFloat('0', QrTabePrcCabCodigo.Value));
      Dmod.QrUpd.SQL.Add('AND GraGru1=' + FormatFloat('0', QrTabePrcGrGNivel1.Value));
      Dmod.QrUpd.SQL.Add('AND Preco=' + dmkPF.FFP(QrTabePrcGrGPreco.Value, 15));
      Dmod.QrUpd.ExecSQL;
      //
      QrTabePrcGrG.Next;
    end;
    // For�ar atualiza��o
    QrTabePrcGrG.First;
    QrTabePrcGrG.Locate('Nivel1', Nivel1, []);
    //ReconfiguraGradeP;
    // reconfigura z autom�tico ap�s p
    //ReconfiguraGradeZ;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTabePrcCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTabePrcCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTabePrcCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTabePrcCab.DefParams;
begin
  VAR_GOTOTABELA := 'TabePrcCab';
  VAR_GOTOMYSQLTABLE := QrTabePrcCab;
  VAR_GOTONEG := gotoNiP;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT tpc.Codigo, tpc.CodUsu, tpc.Nome, tpc.DescoMax, ');
  VAR_SQLx.Add('tpc.JurosMes, tpc.PrazoMed, tpc.Sigla, tpc.Aplicacao, ');
  VAR_SQLx.Add('tpc.DataI, tpc.DataF, tpc.Moeda, ');
  VAR_SQLx.Add('CONCAT(mda.Sigla," - ", mda.Nome) NOMEMOEDA');
  VAR_SQLx.Add('FROM tabeprccab tpc');
  VAR_SQLx.Add('LEFT JOIN cambiomda mda ON mda.Codigo=tpc.Moeda');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('WHERE tpc.Codigo=:P0');
  //
  VAR_SQL2.Add('WHERE tpc.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE tpc.Nome Like :P0');
  //
end;

procedure TFmTabePrcCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'TabePrcCab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmTabePrcCab.EdFiltro1Change(Sender: TObject);
begin
  ReopenTabePrcGrG(QrTabePrcGrgCodigo.Value);
end;

procedure TFmTabePrcCab.Excluiitemns1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrTabePrcNiv,  TDBGrid(DBGNiveis),
  'TabePrcNiv', ['Codigo','Nivel','CodNiv'], ['Codigo','Nivel','CodNiv'],
  istPergunta, '');
end;

procedure TFmTabePrcCab.Excluiperododeprazosatual1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrTabePrcPzG,  DBGPrazos,
  'TabePrcPzG', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmTabePrcCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTabePrcCab.MostraFmTabePrcNiv(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmTabePrcNiv, FmTabePrcNiv, afmoNegarComAviso,
  QrTabePrcNiv, SQLType) then
  begin
    FmTabePrcNiv.EdCodigo.ValueVariant := QrTabePrcCabCodigo.Value;
    FmTabePrcNiv.EdNO_TAB.Text := QrTabePrcCabNome.Value;
    //
    if SQLType = stUpd then
    begin
      FmTabePrcNiv.RGNivel.Enabled := False;
      FmTabePrcNiv.EdCodNiv.Enabled := False;
      FmTabePrcNiv.CBCodNiv.Enabled := False;
    end;
    //
    FmTabePrcNiv.ShowModal;
    FmTabePrcNiv.Destroy;
  end;
end;

procedure TFmTabePrcCab.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 2 then
    ReopenPrecosPrz;
end;

procedure TFmTabePrcCab.Paisagem1Click(Sender: TObject);
begin
  ImprimeGrupos(True, True);
end;

procedure TFmTabePrcCab.PMTabelaPopup(Sender: TObject);
begin
  Alteratabelaselecionada1.Enabled :=
    (QrTabePrcCab.State <> dsInactive) and (QrTabePrcCab.RecordCount > 0);
end;

procedure TFmTabePrcCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTabePrcCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTabePrcCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTabePrcCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTabePrcCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTabePrcCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTabePrcCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTabePrcCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTabePrcCabCodigo.Value;
  Close;
end;

procedure TFmTabePrcCab.Alteraperododeprazosatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmTabePrcPzG, FmTabePrcPzG, afmoNegarComAviso,
  QrTabePrcPzG, stUpd);
end;

procedure TFmTabePrcCab.Alterapreodogrupoatual1Click(Sender: TObject);
var
  OldTxt, NewTxt: String;
  ValNum: Double;
begin
  OldTxt := Geral.FFT(QrTabePrcGrGPreco.Value, 7, siPositivo);
  NewTxt := OldTxt;
  if InputQuery('Pre�o de Grupo de Produtos', 'Informe o novo pre�o:', NewTxt) then
  begin
    ValNum := Geral.DMV(NewTxt);
    NewTxt := Geral.FFT(ValNum, 7, siPositivo);
    if Application.MessageBox(PChar('Confirma a altera��o deo pre�o do grupo ' +
      'de produtos "' + QrTabePrcGrGNOME1.Value + '" de ' + OldTxt + ' para ' +
      NewTxt + '?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE tabeprcgrg SET Preco=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Nivel1=:P2');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsFloat   := ValNum;
      Dmod.QrUpd.Params[01].AsInteger := QrTabePrcCabCodigo.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrTabePrcGrGNivel1.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM tabeprcgri ');
      Dmod.QrUpd.SQL.Add('WHERE TabePrcCab=' + FormatFloat('0', QrTabePrcCabCodigo.Value));
      Dmod.QrUpd.SQL.Add('AND GraGru1=' + FormatFloat('0', QrTabePrcGrGNivel1.Value));
      Dmod.QrUpd.SQL.Add('AND Preco=' + dmkPF.FFP(ValNum, 15));
      Dmod.QrUpd.ExecSQL;
      //
      ReopenTabePrcGrG(QrTabePrcGrGNivel1.Value);
    end;
  end;
end;

procedure TFmTabePrcCab.AlteraPreodoItemSelecionado1Click(Sender: TObject);
begin
  MostraFmTabePrcNiv(stUpd);
end;

procedure TFmTabePrcCab.Alteratabelaselecionada1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrTabePrcCab, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'tabeprccab');
end;

procedure TFmTabePrcCab.BtTabelaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTabela, BtTabela);
end;

procedure TFmTabePrcCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('TabePrcCab', 'Codigo', ImgTipo.SQLType,
    QrTabePrcCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmTabePrcCab, PainelEdit,
    'TabePrcCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTabePrcCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'TabePrcCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TabePrcCab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TabePrcCab', 'Codigo');
end;

procedure TFmTabePrcCab.BtGruposClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMGrupos, BtGrupos);
end;

procedure TFmTabePrcCab.BtNivelClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMNivel, BtNivel);
end;

procedure TFmTabePrcCab.BtPrazosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMPrazos, BtPrazos);
end;

procedure TFmTabePrcCab.BtProdutosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProdutos, BtProdutos);
end;

procedure TFmTabePrcCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 1;
  PageControl3.ActivePageIndex := 1;
  //
  UnDmkDAC_PF.AbreQuery(QrCambioMda, Dmod.MyDB);
  //
  PainelEdit.Align := alClient;
  Panel4.Align     := alClient;
  //
  QrTabePrcGrGPreco.DisplayFormat := Dmod.FStrFmtPrc;
  QrPrecoPrzPRECO.DisplayFormat   := Dmod.FStrFmtPrc;
  //
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeP.ColWidths[0] := 128;
  GradeZ.ColWidths[0] := 128;
  CriaOForm;
end;

procedure TFmTabePrcCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTabePrcCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTabePrcCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
  {
  FQtdPrzs := DmProd.ObtemQtdePrazos(QrTabePrcCabCodigo.Value);
  if FQtdPrzs = 0 then FQtdPrzs := 1;
  //
  RecriaQrPrzPrcPrd();
  }
end;

procedure TFmTabePrcCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTabePrcCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTabePrcCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmTabePrcCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTabePrcCab.QrPrcPrzItsCalcFields(DataSet: TDataSet);
begin
  QrPrcPrzItsVAL_PRZ_PRD.Value := dmkPF.FFF(QrPrcPrzItsPreco.Value *
    QrPrcPrzItsFATOR.Value, Dmod.QrControleCasasProd.Value, siNegativo);
end;

procedure TFmTabePrcCab.QrPrecoPrzAfterScroll(DataSet: TDataSet);
begin
  ReconfiguraGradeZ();
end;

procedure TFmTabePrcCab.QrPrecoPrzCalcFields(DataSet: TDataSet);
var
  Variacao: Double;
begin
  Variacao := (QrPrecoPrzVariacao.Value / 100) + 1;
  QrPrecoPrzPRECO.Value := dmkPF.FFF(QrTabePrcGrGPreco.Value * Variacao,
    Dmod.QrControleCasasProd.Value, siNegativo);
end;

procedure TFmTabePrcCab.QrTabePrcCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTabePrcCab.QrTabePrcCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTabePrcGrG(0);
  ReopenTabePrcPzG(0);
  ReopenTabePrcNiv(0,0);
end;

procedure TFmTabePrcCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTabePrcCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTabePrcCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'TabePrcCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTabePrcCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTabePrcCab.frxPrcGruC_LandGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp2(QrTabePrcCabDataI.Value,
      QrTabePrcCabDataF.Value, True, True,'Validade: ', '', ' ');
  if AnsiCompareText(VarName, 'VARF_TEM_GRADE') = 0 then
    Value := (QrPrcPrzIts.State <> dsInactive) and (QrPrcPrzIts.RecordCount > 0);
end;

procedure TFmTabePrcCab.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnCabeca.Color, taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnCabeca.Color, taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmTabePrcCab.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnCabeca.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor, taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmTabePrcCab.GradePClick(Sender: TObject);
begin
  if (GradeP.Col = 0) or (GradeP.Row = 0) then
    PrecoVariosItensCorTam(GradeP, GradeA, GradeC, GradeX)
  else
    PrecoItemCorTam(GradeP, GradeC, GradeX);
end;

procedure TFmTabePrcCab.GradePDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnCabeca.Color
  else if GradeA.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taLeftJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor, taRightJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmTabePrcCab.GradeZDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnCabeca.Color
  else if GradeA.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeZ, Rect, clBlack,
      Cor, taLeftJustify,
      GradeZ.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeZ, Rect, clBlack,
      Cor, taRightJustify,
      GradeZ.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmTabePrcCab.GruposSemGrades1Click(Sender: TObject);
begin
  ImprimeGrupos(False, False);
end;

procedure TFmTabePrcCab.ImprimeGrupos(ImprimeGrades, Landscape: Boolean);
const
  Titulo = 'Lista de pre�os de Grupos de Produtos';
begin
  Screen.Cursor := crHourGlass;
  try
    FImpGrades := ImprimeGrades;
    //
    QrTabPrcGru.Close;
    QrTabPrcGru.Params[0].AsInteger := QrTabePrcCabCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrTabPrcGru, Dmod.MyDB);
    //
    Screen.Cursor := crDefault;
    if ImprimeGrades then
    begin
      if Landscape then
      begin
        MyObjects.frxDefineDataSets(frxPrcGruC_Land, [
          DModG.frxDsCST,
          frxDsPrcPrzIts,
          frxDsTabePrcCab,
          frxDsTabePrcPzG,
          frxDsTabPrcGru,
          frxDsTabPrcPrz
          ]);
        MyObjects.frxMostra(frxPrcGruC_Land, Titulo);
      end else
      begin
        MyObjects.frxDefineDataSets(frxPrcGruC_Port, [
          DModG.frxDsCST,
          frxDsPrcPrzIts,
          frxDsTabePrcCab,
          frxDsTabePrcPzG,
          frxDsTabPrcGru,
          frxDsTabPrcPrz
          ]);
        MyObjects.frxMostra(frxPrcGruC_Port, Titulo);
      end;
    end else
    begin
      MyObjects.frxDefineDataSets(frxPrcGruSem, [
        DModG.frxDsCST,
        frxDsPrcPrzIts,
        frxDsTabePrcCab,
        frxDsTabePrcPzG,
        frxDsTabPrcGru,
        frxDsTabPrcPrz
        ]);
      MyObjects.frxMostra(frxPrcGruSem, Titulo);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTabePrcCab.Incluidiversosprodutos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTabePrcGrGMul, FmTabePrcGrGMul, afmoNegarComAviso) then
  begin
    FmTabePrcGrGMul.ShowModal;
    FmTabePrcGrGMul.Destroy;
  end;
end;

procedure TFmTabePrcCab.Incluinovatabeladepreos1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrTabePrcCab, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'tabeprccab');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'tabeprccab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
  CGAplicacao.Value := 1;
end;

procedure TFmTabePrcCab.Incluinovoperododeprazos1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmTabePrcPzG, FmTabePrcPzG, afmoNegarComAviso,
  QrTabePrcPzG, stIns);
end;

procedure TFmTabePrcCab.IncluiNovoPrecodeNivel1Click(Sender: TObject);
begin
  MostraFmTabePrcNiv(stIns);
end;

procedure TFmTabePrcCab.Incluinovoprodutoumaum1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmTabePrcGrG, FmTabePrcGrG, afmoNegarComAviso,
  QrTabePrcGrG, stIns);
end;

procedure TFmTabePrcCab.QrTabePrcCabBeforeClose(DataSet: TDataSet);
begin
  QrTabePrcGrG.Close;
  QrTabePrcPzG.Close;
  QrTabePrcNiv.Close;
end;

procedure TFmTabePrcCab.QrTabePrcCabBeforeOpen(DataSet: TDataSet);
begin
  QrTabePrcCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTabePrcCab.QrTabePrcGrGAfterScroll(DataSet: TDataSet);
begin
  ReconfiguraGradeP();
end;

procedure TFmTabePrcCab.QrTabePrcGrGCalcFields(DataSet: TDataSet);
begin
  QrTabePrcGrGKGT.Value := 0;
end;

procedure TFmTabePrcCab.QrTabePrcPzGCalcFields(DataSet: TDataSet);
begin
  QrTabePrcPzGCOL_TXT.Value := '';
end;

procedure TFmTabePrcCab.QrTabPrcGruAfterScroll(DataSet: TDataSet);
begin
  QrTabPrcPrz.Close;
  QrTabPrcPrz.Params[00].AsInteger := QrTabePrcCabCodigo.Value;
  QrTabPrcPrz.Params[01].AsInteger := QrTabPrcGruNivel1.Value;
  UnDmkDAC_PF.AbreQuery(QrTabPrcPrz, Dmod.MyDB);
  if FImpGrades then
  begin
    QrPrcPrzIts.Close;
    QrPrcPrzIts.Params[00].AsInteger := QrTabePrcCabCodigo.Value;
    QrPrcPrzIts.Params[01].AsInteger := QrTabPrcGruNivel1.Value;
    UnDmkDAC_PF.AbreQuery(QrPrcPrzIts, Dmod.MyDB);
  end;
end;

procedure TFmTabePrcCab.QrTabPrcGruCalcFields(DataSet: TDataSet);
begin
  QrTabPrcGruKGT.Value := 0;
end;

procedure TFmTabePrcCab.QrTabPrcPrzCalcFields(DataSet: TDataSet);
begin
  QrTabPrcPrzPRC_PRZ.Value := dmkPF.FFF(QrTabPrcPrzPreco.Value *
    QrTabPrcPrzFATOR.Value, Dmod.QrControleCasasProd.Value, siNegativo);
  QrTabPrcPrzPRC_PRZ_TXT.Value := Geral.FFT(QrTabPrcPrzPRC_PRZ.Value,
    Dmod.QrControleCasasProd.Value, siNegativo);
end;

procedure TFmTabePrcCab.ReconfiguraGradeP;
begin
  DmProd.ConfigGrades2(QrTabePrcGrGGraTamCad.Value, QrTabePrcGrGNivel1.Value,
    QrTabePrcCabCodigo.Value, GradeA, GradeX, GradeC, GradeP, 0);
  if PageControl1.ActivePageIndex = 2 then
    ReopenPrecosPrz;
end;

procedure TFmTabePrcCab.ReconfiguraGradeZ;
begin
  DmProd.ConfigGrades2(QrTabePrcGrGGraTamCad.Value, QrTabePrcGrGNivel1.Value,
    QrTabePrcCabCodigo.Value, GradeA, GradeX, GradeC, GradeZ,
    QrPrecoPrzVariacao.Value);
end;

procedure TFmTabePrcCab.Reduzido1Click(Sender: TObject);
var
  FmTabePrcImp: TFmTabePrcImp;
begin
  FmTabePrcImp := TFmTabePrcImp.Create(nil);
  try
    FmTabePrcImp.Show;
    FmTabePrcImp.ImprimeListaPrecos;
    Application.ProcessMessages;
  finally
    FmTabePrcImp.Free;
  end;
end;

procedure TFmTabePrcCab.ReopenPrecosPrz();
begin
  Screen.Cursor := crHourGlass;
  try
    QrPrecoPrz.Close;
    QrPrecoPrz.Params[0].AsInteger := QrTabePrcCabCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrPrecoPrz, Dmod.MyDB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTabePrcCab.ReopenTabePrcGrG(Nivel1: Integer);
begin
  QrTabePrcGrG.Close;
  QrTabePrcGrG.SQL.Clear;
  QrTabePrcGrG.SQL.Add('SELECT gg1.Nome NOME1, gg1.CodUsu, gg1.GraTamCad, tpg.*');
  QrTabePrcGrG.SQL.Add('FROM tabeprcgrg tpg');
  QrTabePrcGrG.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpg.Nivel1');
  QrTabePrcGrG.SQL.Add('WHERE Codigo=:P0');
  if Length(EdFiltro1.Text) > 2 then
    QrTabePrcGrG.SQL.Add('AND gg1.Nome LIKE "%' + EdFiltro1.Text + '%"');
  QrTabePrcGrG.SQL.Add('');
  QrTabePrcGrG.Params[0].AsInteger := QrTabePrcCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrTabePrcGrG, Dmod.MyDB);
  //
  if Nivel1 <> 0 then
    QrTabePrcGrG.Locate('Nivel1', Nivel1, []);
end;

procedure TFmTabePrcCab.ReopenTabePrcNiv(Nivel, CodNiv: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTabePrcNiv, Dmod.MyDB, [
  'SELECT IF(tpn.Nivel=1, gg1.Nome, IF(tpn.Nivel=2, ',
  'gg2.Nome, IF(tpn.Nivel=3, gg3.Nome, pgt.Nome))) ',
  'NO_CODNIV, tpn.* ',
  'FROM tabeprcniv tpn',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpn.CodNiv AND tpn.Nivel=1',
  'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=tpn.CodNiv AND tpn.Nivel=2',
  'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=tpn.CodNiv AND tpn.Nivel=3',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=tpn.CodNiv AND tpn.Nivel=4',
  'WHERE tpn.Codigo=' + FormatFloat('0', QrTabePrcCabCodigo.Value),
  'ORDER BY Nivel, CodNiv',
  '']);
  //
  QrTabePrcNiv.Locate('Nivel;CodNiv', VarArrayOf([Nivel, CodNiv]), []);
end;

procedure TFmTabePrcCab.ReopenTabePrcPzG(Controle: Integer);
begin
  QrTabePrcPzG.Close;
  QrTabePrcPzG.Params[0].AsInteger :=   QrTabePrcCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrTabePrcPzG, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrTabePrcPzG.Locate('Controle', Controle, []);
end;

procedure TFmTabePrcCab.Retrato1Click(Sender: TObject);
begin
  ImprimeGrupos(True, False);
end;

function TFmTabePrcCab.PrecoItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Controle, GraGruX, Nivel1, Coluna, Linha, Lista: Integer;
  Preco: Double;
  SQLType: TSQLType;
  c, l: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := GridX.Cells[Coluna, 0];
  l := GridX.Cells[0, Linha];
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = '' then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = '' then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Application.MessageBox('O item nunca foi ativado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = '') or (l = '') or (GraGruX = 0) then Exit;
  //
  Nivel1 := QrTabePrcGrGNivel1.Value;
  Lista  := QrTabePrcCabCodigo.Value;
  Preco  := Geral.DMV(GridP.Cells[Coluna, Linha]);
  if DmProd.ObtemPreco(Preco) then
  begin
    DmProd.QrLocTPGI.Close;
    DmProd.QrLocTPGI.Params[00].AsInteger := GraGruX;
    DmProd.QrLocTPGI.Params[01].AsInteger := Nivel1;
    DmProd.QrLocTPGI.Params[02].AsInteger := Lista;
    UnDmkDAC_PF.AbreQuery(DmProd.QrLocTPGI, Dmod.MyDB);
    //
    Controle := DmProd.QrLocTPGIControle.Value;
    if Controle = 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'TabePrcGrI', 'TabePrcGrI', 'Controle');
      SQLType := stIns;
    end else SQLType := stUpd;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'TabePrcGrI', False, [
    'GraGruX', 'GraGru1', 'TabePrcCab', 'Preco'], ['Controle'], [
    GraGruX, Nivel1, Lista, Preco], [Controle], True) then
    DmProd.AtualizaGradesPrecos2(Nivel1, Lista, GridP, 0);
  end;
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmTabePrcCab.PrecoVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Controle, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, Nivel1, GraGruX, Lista: Integer;
  Preco: Double;
  PrecoTxt, ItensTxt: String;
  SQLType: TSQLType;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Nivel1 := QrTabePrcGrGNivel1.Value;
  Lista  := QrTabePrcCabCodigo.Value;
  Preco  := Geral.DMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if DmProd.ObtemPreco(Preco) then
  begin
    PrecoTxt := QrTabePrcCabNOMEMOEDA.Value + ' ' +
      Geral.FFT(Preco, Dmod.QrControleCasasProd.Value, siNegativo);
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Application.MessageBox(PChar('Confirma o valor de ' + PrecoTxt + ' para ' +
    ItensTxt + '?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
      begin
        DmProd.QrLocTPGI.Close;
        DmProd.QrLocTPGI.Params[00].AsInteger := GraGruX;
        DmProd.QrLocTPGI.Params[01].AsInteger := Nivel1;
        DmProd.QrLocTPGI.Params[02].AsInteger := Lista;
        UnDmkDAC_PF.AbreQuery(DmProd.QrLocTPGI, Dmod.MyDB);
        //
        Controle := DmProd.QrLocTPGIControle.Value;
        if Controle = 0 then
        begin
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'TabePrcGrI', 'TabePrcGrI', 'Controle');
          SQLType := stIns;
        end else SQLType := stUpd;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'TabePrcGrI', False, [
        'GraGruX', 'GraGru1', 'TabePrcCab', 'Preco'], ['Controle'], [
        GraGruX, Nivel1, Lista, Preco], [Controle], True);
      end;
    end;
    DmProd.AtualizaGradesPrecos2(Nivel1, Lista, GridP, 0);
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

{
procedure TFmTabePrcCab.RecriaQrPrzPrcPrd();
var
  i: Integer;
  Campo, Lin1, Lin2, Lin3, Varia: String;
begin
  //
  QrPrzPrcPrd.Close;
  QrPrzPrcPrd.SQL.Clear;
  QrPrzPrcPrd.SQL.Add('DROP TABLE PrzPrcPrd; ');
  QrPrzPrcPrd.SQL.Add('CREATE TABLE PrzPrcPrd  (');
  QrTabePrcPzG.First;
  i := 0;
  Lin1 := '';
  Lin2 := '';
  while not QrTabePrcPzG.Eof do
  begin
    i := i + 1;
    if i > 1 then
    begin
      Lin1 := Lin1 + ', ';
      Lin2 := Lin2 + ', ';
      Lin3 := Lin3 + ', ';
    end;
    Campo := 'Prz' + FormatFloat('000', i);
    Varia := dmkPF.FFP(QrTabePrcPzGVariacao.Value, 2);
    QrPrzPrcPrd.SQL.Add('  ' + Campo + ' varchar(15),');
    Lin1 := Lin1 + Campo;
    Lin2 := Lin2 + '"' + QrTabePrcPzGNome.Value + '"';
    Lin3 := Lin3 + Varia;
    //
    QrTabePrcPzG.Next;
  end;
  QrPrzPrcPrd.SQL.Add('  Ativo smallint        ');
  QrPrzPrcPrd.SQL.Add(');');
  Lin1 := 'INSERT INTO przprcprd (' + Lin1 + ') VALUES (';
  QrPrzPrcPrd.SQL.Add(Lin1 + Lin2 + ');');
  QrPrzPrcPrd.SQL.Add(Lin1 + Lin3 + ');');
  QrPrzPrcPrd.SQL.Add('SELECT * FROM przprcprd;');

  UnDmkDAC_PF.AbreQuery(QrPrzPrcPrd, Dmod.MyDB);
end;
}

end.

