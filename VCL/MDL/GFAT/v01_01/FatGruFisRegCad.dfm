object FmFatGruFisRegCad: TFmFatGruFisRegCad
  Left = 339
  Top = 185
  Caption = 'NFe-GERAL-008 :: Itens de Venda - Dados Fiscais'
  ClientHeight = 503
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 766
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 718
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 670
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 380
        Height = 32
        Caption = 'Itens de Venda - Dados Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 380
        Height = 32
        Caption = 'Itens de Venda - Dados Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 380
        Height = 32
        Caption = 'Itens de Venda - Dados Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 766
    Height = 341
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 766
      Height = 341
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 766
        Height = 341
        Align = alClient
        TabOrder = 0
        object PnDados: TPanel
          Left = 2
          Top = 15
          Width = 762
          Height = 45
          Align = alTop
          TabOrder = 0
          ExplicitWidth = 763
          object RGMostra: TdmkRadioGroup
            Left = 1
            Top = 1
            Width = 350
            Height = 44
            Align = alLeft
            Caption = ' Dados fiscais: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Utilizar dados da regra fiscal'
              'Informar manualmente')
            TabOrder = 0
            OnClick = RGMostraClick
            QryCampo = 'Tipo'
            UpdCampo = 'Tipo'
            UpdType = utYes
            OldValor = 0
          end
        end
        object PnFisRegCad: TPanel
          Left = 2
          Top = 60
          Width = 762
          Height = 279
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 763
          ExplicitHeight = 280
          object Label4: TLabel
            Left = 12
            Top = 4
            Width = 52
            Height = 13
            Caption = 'CFOP: [F7]'
          end
          object SpeedButton1: TSpeedButton
            Left = 466
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object EdCFOP: TdmkEditCB
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CFOP'
            UpdCampo = 'CFOP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            DBLookupComboBox = CBCFOP
            IgnoraDBLookupComboBox = False
          end
          object CBCFOP: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 395
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCFOP
            TabOrder = 1
            dmkEditCB = EdCFOP
            QryCampo = 'CFOP'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object GroupBox2: TGroupBox
            Left = 12
            Top = 47
            Width = 475
            Height = 57
            Caption = 
              'Caso n'#227'o queira um CST '#250'nico para o ICMS deixe o campo em branco' +
              '!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            object Label11: TLabel
              Left = 8
              Top = 16
              Width = 234
              Height = 13
              Caption = 'N12 - C'#243'digo situa'#231#227'o tribut'#225'ria (CST ICMS): [F3] '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object EdCST_B: TdmkEdit
              Left = 8
              Top = 32
              Width = 29
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CST_B'
              UpdCampo = 'CST_B'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCST_BChange
              OnKeyDown = EdCST_BKeyDown
            end
            object EdTextoB: TdmkEdit
              Left = 39
              Top = 32
              Width = 430
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox3: TGroupBox
            Left = 12
            Top = 110
            Width = 475
            Height = 72
            Caption = 
              'Caso n'#227'o queira um modBC '#250'nico para o ICMS clique no bot'#227'o "Desm' +
              'arcar"!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            object Panel9: TPanel
              Left = 2
              Top = 15
              Width = 100
              Height = 56
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object BtNaoSel: TBitBtn
                Left = 3
                Top = 8
                Width = 90
                Height = 40
                Hint = 'Desmarca todos itens'
                Caption = 'Desmarcar'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                TabOrder = 0
                OnClick = BtNaoSelClick
              end
            end
            object RGmodBC: TdmkRadioGroup
              Left = 102
              Top = 15
              Width = 371
              Height = 56
              Align = alClient
              Caption = 'Modalidade de determina'#231#227'o da BC do ICMS: '
              Columns = 2
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Margem valor agregado (%)'
                'Pauta (valor)'
                'Pre'#231'o tabelado m'#225'ximo (valor)'
                'Valor da opera'#231#227'o')
              ParentFont = False
              TabOrder = 1
              QryCampo = 'modBC'
              UpdCampo = 'modBC'
              UpdType = utYes
              OldValor = 0
            end
          end
          object GroupBox4: TGroupBox
            Left = 12
            Top = 190
            Width = 475
            Height = 42
            Caption = 
              'Caso n'#227'o queira um % red. BC '#250'nico para o ICMS deixe o campo zer' +
              'ado!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            object Label8: TLabel
              Left = 10
              Top = 19
              Width = 204
              Height = 13
              Caption = 'Percentual de redu'#231#227'o da base de c'#225'lculo:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object EdpRedBC: TdmkEdit
              Left = 220
              Top = 16
              Width = 57
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'pRedBC'
              UpdCampo = 'pRedBC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object Panel10: TPanel
            Left = 504
            Top = 8
            Width = 245
            Height = 224
            BevelOuter = bvNone
            TabOrder = 5
            object GroupBox5: TGroupBox
              Left = 0
              Top = 0
              Width = 121
              Height = 224
              Align = alLeft
              Caption = 'Origem:'
              TabOrder = 0
              object Panel11: TPanel
                Left = 2
                Top = 15
                Width = 117
                Height = 208
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label24: TLabel
                  Left = 12
                  Top = 4
                  Width = 49
                  Height = 13
                  Caption = 'Minha UF:'
                end
                object Label9: TLabel
                  Left = 12
                  Top = 44
                  Width = 40
                  Height = 13
                  Caption = '% ICMS:'
                end
                object Label12: TLabel
                  Left = 12
                  Top = 84
                  Width = 31
                  Height = 13
                  Caption = '% PIS:'
                  Enabled = False
                  Visible = False
                end
                object Label13: TLabel
                  Left = 12
                  Top = 124
                  Width = 53
                  Height = 13
                  Caption = '% COFINS:'
                  Enabled = False
                  Visible = False
                end
                object Label15: TLabel
                  Left = 12
                  Top = 164
                  Width = 103
                  Height = 13
                  Caption = '% ICMS interestadual:'
                end
                object EdUFEmit: TdmkEdit
                  Left = 13
                  Top = 20
                  Width = 44
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtUF
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'EUF'
                  UpdCampo = 'EUF'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdICMSAliq: TdmkEdit
                  Left = 12
                  Top = 60
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdPISAliq: TdmkEdit
                  Left = 12
                  Top = 100
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 2
                  Visible = False
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCOFINSAliq: TdmkEdit
                  Left = 12
                  Top = 140
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 3
                  Visible = False
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdpICMSInter: TdmkEdit
                  Left = 12
                  Top = 180
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object GroupBox6: TGroupBox
              Left = 121
              Top = 0
              Width = 124
              Height = 224
              Align = alClient
              Caption = 'Destino:'
              TabOrder = 1
              object Panel12: TPanel
                Left = 2
                Top = 15
                Width = 121
                Height = 208
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label16: TLabel
                  Left = 11
                  Top = 4
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                end
                object Label17: TLabel
                  Left = 12
                  Top = 44
                  Width = 75
                  Height = 13
                  Caption = '% ICMS interno:'
                end
                object Label18: TLabel
                  Left = 12
                  Top = 84
                  Width = 67
                  Height = 13
                  Caption = '% ICMS FCP*:'
                end
                object Label19: TLabel
                  Left = 12
                  Top = 124
                  Width = 85
                  Height = 13
                  Caption = '% BC da BC Orig.:'
                end
                object LapICMSInterPart: TLabel
                  Left = 13
                  Top = 164
                  Width = 77
                  Height = 13
                  Caption = '% ICMS partilha:'
                end
                object EdUFDest: TdmkEdit
                  Left = 12
                  Top = 20
                  Width = 44
                  Height = 21
                  CharCase = ecUpperCase
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtUF
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'EUF'
                  UpdCampo = 'EUF'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdpICMSUFDest: TdmkEdit
                  Left = 12
                  Top = 60
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdpFCPUFDest: TdmkEdit
                  Left = 12
                  Top = 100
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdpBCUFDest: TdmkEdit
                  Left = 12
                  Top = 140
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 10
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '100,0000000000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 100.000000000000000000
                  ValWarn = False
                end
                object EdpICMSInterPart: TdmkEdit
                  Left = 12
                  Top = 180
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object CkUsaInterPartLei: TCheckBox
            Left = 12
            Top = 238
            Width = 737
            Height = 17
            Caption = 
              'Usar % ICMS partilha conforme emenda constitucional 87/2015. 40%' +
              ' em 2016  -  60% em 2017 80% em 2018  -  100% a partir de 2019.'
            TabOrder = 6
            WordWrap = True
            OnClick = CkUsaInterPartLeiClick
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 389
    Width = 766
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 433
    Width = 766
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 621
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 619
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 572
    Top = 64
  end
  object QrCFOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cfop2003'
      'ORDER BY Nome')
    Left = 544
    Top = 64
    object QrCFOPCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrTmp_FatPedFis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 736
    Top = 80
  end
end
