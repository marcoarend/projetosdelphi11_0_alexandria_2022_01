unit FatDivItemAlt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmFatDivItemAlt = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    Label247: TLabel;
    EdvFrete: TdmkEdit;
    Label248: TLabel;
    EdvSeg: TdmkEdit;
    Label249: TLabel;
    EdvDesc: TdmkEdit;
    Label6: TLabel;
    EdvOutro: TdmkEdit;
    Label1: TLabel;
    EdvBC: TdmkEdit;
    EdvProd: TdmkEdit;
    Label2: TLabel;
    Panel5: TPanel;
    EdIDCtrl: TdmkEdit;
    Label3: TLabel;
    EdNO_NIVEL1: TdmkEdit;
    Label4: TLabel;
    EdGraGruX: TdmkEdit;
    Label5: TLabel;
    EdNO_COR: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdNO_TAM: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label9: TLabel;
    EdOriCtrl: TdmkEdit;
    Label10: TLabel;
    EdOriCodi: TdmkEdit;
    Label11: TLabel;
    EdTipo: TdmkEdit;
    Label12: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdobsCont_xCampo: TdmkEdit;
    Label13: TLabel;
    EdobsCont_xTexto: TdmkEdit;
    Label14: TLabel;
    GroupBox3: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    EdobsFisco_xCampo: TdmkEdit;
    EdobsFisco_xTexto: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Recalcula_vBC(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFatDivItemAlt: TFmFatDivItemAlt;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmFatDivItemAlt.BtOKClick(Sender: TObject);
var
{
  DataHora: String;
}
  IDCtrl, Tipo, OriCodi, OriCtrl, Empresa: Integer;
{  Tipo, OriCodi, OriCtrl, OriCnta, OriPart, Empresa, StqCenCad, GraGruX, QuemUsou, Retorno, ParTipo, ParCodi, DebCtrl, SMIMultIns, GrupoBal, Baixa, UnidMed, ValiStq, StqCenLoc, AtzPrcMed, PackGGX, PackUnMed: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, FatorClas, CustoAll, ValorAll, AntQtde, CustoBuy, CustoFrt, PackQtde, vFrete, vSeg, vDesc, vOutro: Double;
  SQLType: TSQLType;
}
  vFrete, vSeg, vDesc, vOutro: Double;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  SQLType: TSQLType;
  obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto: String;
begin
  SQLType := stUpd;
  //
  IDCtrl   := EdIDCtrl.ValueVariant;
  //
  vFrete   := EdvFrete.ValueVariant;
  vSeg     := EdvSeg.ValueVariant;
  vDesc    := EdvDesc.ValueVariant;
  vOutro   := EdvOutro.ValueVariant;
  //
  Tipo     := EdTipo.ValueVariant;
  OriCodi  := EdOriCodi.ValueVariant;
  OriCtrl  := EdOriCtrl.ValueVariant;
  Empresa  := EdEmpresa.ValueVariant;
  //
  obsCont_xCampo  := EdobsCont_xCampo.ValueVariant;
  obsCont_xTexto  := EdobsCont_xTexto.ValueVariant;
  obsFisco_xCampo := EdobsFisco_xCampo.ValueVariant;
  obsFisco_xTexto := EdobsFisco_xTexto.ValueVariant;

  if Trim(obsCont_xCampo) + Trim(obsCont_xTexto) <> EmptyStr then
  begin
    if MyObjects.FIC(Trim(obsCont_xCampo) = EmptyStr, EdobsCont_xCampo,
    'Informe a identifica��o do campo do uso livre do contribuinte!') then Exit;

    if MyObjects.FIC(Trim(obsCont_xTexto) = EmptyStr, EdobsCont_xTexto,
    'Informe o conte�do do campo do uso livre do contribuinte!') then Exit;
  end;

  if Trim(obsFisco_xCampo) + Trim(obsFisco_xTexto) <> EmptyStr then
  begin
    if MyObjects.FIC(Trim(obsFisco_xCampo) = EmptyStr, EdobsFisco_xCampo,
    'Informe a identifica��o do campo do uso livre do fisco!') then Exit;

    if MyObjects.FIC(Trim(obsFisco_xTexto) = EmptyStr, EdobsFisco_xTexto,
    'Informe o conte�do do campo do uso livre do fisco!') then Exit;
  end;
{
  DataHora       := ;
  Tipo           := ;
  OriCodi        := ;
  OriCtrl        := ;
  OriCnta        := ;
  OriPart        := ;
  Empresa        := ;
  StqCenCad      := ;
  GraGruX        := ;
  Qtde           := ;
  Pecas          := ;
  Peso           := ;
  AreaM2         := ;
  AreaP2         := ;
  FatorClas      := ;
  QuemUsou       := ;
  Retorno        := ;
  ParTipo        := ;
  ParCodi        := ;
  DebCtrl        := ;
  SMIMultIns     := ;
  CustoAll       := ;
  ValorAll       := ;
  GrupoBal       := ;
  Baixa          := ;
  AntQtde        := ;
  UnidMed        := ;
  ValiStq        := ;
  StqCenLoc      := ;
  AtzPrcMed      := ;
  CustoBuy       := ;
  CustoFrt       := ;
  PackGGX        := ;
  PackUnMed      := ;
  PackQtde       := ;
}

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
  (*'DataHora', 'Tipo', 'OriCodi',
  'OriCtrl', 'OriCnta', 'OriPart',
  'Empresa', 'StqCenCad', 'GraGruX',
  'Qtde', 'Pecas', 'Peso',
  'AreaM2', 'AreaP2', 'FatorClas',
  'QuemUsou', 'Retorno', 'ParTipo',
  'ParCodi', 'DebCtrl', 'SMIMultIns',
  'CustoAll', 'ValorAll', 'GrupoBal',
  'Baixa', 'AntQtde', 'UnidMed',
  'ValiStq', 'StqCenLoc', 'AtzPrcMed',
  'CustoBuy', 'CustoFrt', 'PackGGX',
  'PackUnMed', 'PackQtde',*) 'vFrete',
  'vSeg', 'vDesc', 'vOutro'], [
  'IDCtrl'], [
  (*DataHora, Tipo, OriCodi,
  OriCtrl, OriCnta, OriPart,
  Empresa, StqCenCad, GraGruX,
  Qtde, Pecas, Peso,
  AreaM2, AreaP2, FatorClas,
  QuemUsou, Retorno, ParTipo,
  ParCodi, DebCtrl, SMIMultIns,
  CustoAll, ValorAll, GrupoBal,
  Baixa, AntQtde, UnidMed,
  ValiStq, StqCenLoc, AtzPrcMed,
  CustoBuy, CustoFrt, PackGGX,
  PackUnMed, PackQtde,*) vFrete,
  vSeg, vDesc, vOutro], [
  IDCtrl], False) then
  begin
    //fazer tambem no stqmovvala !
    prod_vFrete  := vFrete;
    prod_vSeg    := vSeg;
    prod_vDesc   := vDesc;
    prod_vOutro  := vOutro;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovvala', False, [
    'prod_vFrete',
    'prod_vSeg', 'prod_vDesc', 'prod_vOutro',
    'obsCont_xCampo', 'obsCont_xTexto',
    'obsFisco_xCampo', 'obsFisco_xTexto'], [
    'IDCtrl', 'Tipo', 'OriCodi', 'OriCtrl', 'Empresa'], [
    prod_vFrete,
    prod_vSeg, prod_vDesc, prod_vOutro,
    obsCont_xCampo, obsCont_xTexto,
    obsFisco_xCampo, obsFisco_xTexto], [
    IDCtrl, Tipo, OriCodi, OriCtrl, Empresa], False);
    //
    Close;
  end;
end;

procedure TFmFatDivItemAlt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatDivItemAlt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatDivItemAlt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFatDivItemAlt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatDivItemAlt.Recalcula_vBC(Sender: TObject);
var
  vProd, vFrete, vSeg, vDesc, vOutro, vBC: Double;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  //
  vProd    := EdvProd.ValueVariant;
  vFrete   := EdvFrete.ValueVariant;
  vSeg     := EdvSeg.ValueVariant;
  vDesc    := EdvDesc.ValueVariant;
  vOutro   := EdvOutro.ValueVariant;
  //
  vBC      := vProd + vFrete + vSeg - vDesc + vOutro;
  EdvBC.ValueVariant := vBC;
end;

end.
