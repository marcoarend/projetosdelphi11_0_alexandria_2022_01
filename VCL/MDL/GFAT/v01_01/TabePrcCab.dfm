object FmTabePrcCab: TFmTabePrcCab
  Left = 368
  Top = 194
  Caption = 'TAB-PRECO-001 :: Tabelas de Pre'#231'os'
  ClientHeight = 563
  ClientWidth = 1014
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1014
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 0
      Top = 0
      Width = 1014
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 4
        Top = 44
        Width = 66
        Height = 13
        Caption = '% juros / m'#234's:'
        FocusControl = DBEdit7
      end
      object Label13: TLabel
        Left = 96
        Top = 44
        Width = 83
        Height = 13
        Caption = '% desconto m'#225'x.:'
        FocusControl = DBEdit8
      end
      object Label24: TLabel
        Left = 188
        Top = 44
        Width = 61
        Height = 13
        Caption = 'Prazo m'#233'dio:'
        FocusControl = DBEdit2
      end
      object Label25: TLabel
        Left = 280
        Top = 44
        Width = 73
        Height = 13
        Caption = 'In'#237'cio vig'#234'ncia:'
        FocusControl = DBEdit8
      end
      object Label26: TLabel
        Left = 388
        Top = 44
        Width = 68
        Height = 13
        Caption = 'Final vig'#234'ncia:'
        FocusControl = DBEdit8
      end
      object Label27: TLabel
        Left = 592
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdNome
      end
      object Label14: TLabel
        Left = 496
        Top = 44
        Width = 36
        Height = 13
        Caption = 'Moeda:'
        FocusControl = DBEdit8
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 24
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTabePrcCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 20
        Width = 441
        Height = 24
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTabePrcCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 24
        DataField = 'CodUsu'
        DataSource = DsTabePrcCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit7: TDBEdit
        Left = 4
        Top = 60
        Width = 88
        Height = 24
        DataField = 'JurosMes'
        DataSource = DsTabePrcCab
        TabOrder = 3
      end
      object DBEdit8: TDBEdit
        Left = 96
        Top = 60
        Width = 88
        Height = 24
        DataField = 'DescoMax'
        DataSource = DsTabePrcCab
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 188
        Top = 60
        Width = 88
        Height = 24
        DataField = 'PrazoMed'
        DataSource = DsTabePrcCab
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 280
        Top = 60
        Width = 104
        Height = 24
        DataField = 'DataI'
        DataSource = DsTabePrcCab
        TabOrder = 6
      end
      object DBEdit4: TDBEdit
        Left = 388
        Top = 60
        Width = 104
        Height = 24
        DataField = 'DataF'
        DataSource = DsTabePrcCab
        TabOrder = 7
      end
      object DBEdit5: TDBEdit
        Left = 592
        Top = 20
        Width = 72
        Height = 24
        DataField = 'Sigla'
        DataSource = DsTabePrcCab
        TabOrder = 8
      end
      object dmkDBCheckGroup1: TdmkDBCheckGroup
        Left = 668
        Top = 11
        Width = 337
        Height = 70
        Caption = ' Aplica'#231#227'o: '
        Columns = 2
        DataField = 'Aplicacao'
        DataSource = DsTabePrcCab
        Items.Strings = (
          'Pedido de venda'
          'Venda balc'#227'o'
          'Condicional'
          'Relat'#243'rios')
        ParentBackground = False
        TabOrder = 9
      end
      object DBEdit6: TDBEdit
        Left = 496
        Top = 60
        Width = 169
        Height = 24
        DataField = 'NOMEMOEDA'
        DataSource = DsTabePrcCab
        TabOrder = 10
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 152
      Width = 1014
      Height = 251
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 1014
        Height = 251
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = ' Por grupo de produtos '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl2: TPageControl
            Left = 408
            Top = 0
            Width = 598
            Height = 223
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 0
            ExplicitHeight = 220
            object TabSheet3: TTabSheet
              Caption = ' Ativos '
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object GradeA: TStringGrid
                Left = 0
                Top = 16
                Width = 593
                Height = 185
                Align = alClient
                ColCount = 2
                DefaultColWidth = 65
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                TabOrder = 0
                OnDrawCell = GradeADrawCell
                RowHeights = (
                  18
                  18)
              end
              object StaticText2: TStaticText
                Left = 0
                Top = 0
                Width = 475
                Height = 17
                Align = alTop
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 
                  'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
                  'esativar o produto.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                Visible = False
              end
            end
            object TabSheet5: TTabSheet
              Caption = ' Pre'#231'os '
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object GradeP: TStringGrid
                Left = 0
                Top = 16
                Width = 593
                Height = 185
                Align = alClient
                ColCount = 2
                DefaultColWidth = 100
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
                ParentFont = False
                TabOrder = 0
                OnClick = GradePClick
                OnDrawCell = GradePDrawCell
              end
              object StaticText1: TStaticText
                Left = 0
                Top = 0
                Width = 446
                Height = 17
                Align = alTop
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 
                  'Clique na c'#233'lula, coluna ou linha correspondente para incluir / ' +
                  'alterar o valor.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object TabSheet6: TTabSheet
              Caption = ' C'#243'digos '
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object GradeC: TStringGrid
                Left = 0
                Top = 16
                Width = 593
                Height = 185
                Align = alClient
                ColCount = 2
                DefaultColWidth = 65
                DefaultRowHeight = 18
                RowCount = 2
                TabOrder = 0
                OnDrawCell = GradeCDrawCell
                RowHeights = (
                  18
                  19)
              end
              object StaticText6: TStaticText
                Left = 0
                Top = 0
                Width = 502
                Height = 17
                Align = alTop
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 
                  'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
                  'dente na guia "Ativos".'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                Visible = False
              end
            end
            object TabSheet9: TTabSheet
              Caption = ' X '
              ImageIndex = 6
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object GradeX: TStringGrid
                Left = 0
                Top = 0
                Width = 593
                Height = 201
                Align = alClient
                ColCount = 2
                DefaultColWidth = 65
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                TabOrder = 0
                RowHeights = (
                  18
                  18)
              end
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 408
            Height = 223
            Align = alLeft
            ParentBackground = False
            TabOrder = 1
            ExplicitHeight = 226
            object DBGGrupos: TdmkDBGrid
              Left = 1
              Top = 29
              Width = 406
              Height = 196
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'Grupo'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME1'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Preco'
                  Title.Caption = 'Pre'#231'o'
                  Width = 68
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsTabePrcGrG
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'Grupo'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME1'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Preco'
                  Title.Caption = 'Pre'#231'o'
                  Width = 68
                  Visible = True
                end>
            end
            object Panel7: TPanel
              Left = 1
              Top = 1
              Width = 406
              Height = 28
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label17: TLabel
                Left = 4
                Top = 8
                Width = 189
                Height = 13
                Caption = 'Filtro da descri'#231#227'o (pelo menos 3 letras):'
              end
              object EdFiltro1: TEdit
                Left = 196
                Top = 4
                Width = 201
                Height = 24
                TabOrder = 0
                OnChange = EdFiltro1Change
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Por Prazo M'#233'dio '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGPrazos: TDBGrid
            Left = 0
            Top = 0
            Width = 1008
            Height = 226
            Align = alClient
            DataSource = DsTabePrcPzG
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DiasIni'
                Title.Caption = 'Dias I'
                Width = 34
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DiasFim'
                Title.Caption = 'Dias F'
                Width = 34
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Variacao'
                Title.Caption = 'Varia'#231#227'o'
                Visible = True
              end>
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Visualiza'#231#227'o por prazo m'#233'dio'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 401
            Height = 226
            Align = alLeft
            DataSource = DsTabePrcGrG
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'Grupo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME1'
                Title.Caption = 'Descri'#231#227'o'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Preco'
                Title.Caption = 'Pre'#231'o'
                Width = 68
                Visible = True
              end>
          end
          object DBGrid2: TDBGrid
            Left = 401
            Top = 0
            Width = 363
            Height = 226
            Align = alLeft
            DataSource = DsPrecoPrz
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DiasIni'
                Title.Caption = 'Dias I'
                Width = 34
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DiasFim'
                Title.Caption = 'Dias F'
                Width = 34
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Variacao'
                Title.Caption = 'Varia'#231#227'o'
                Width = 47
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end>
          end
          object PageControl3: TPageControl
            Left = 764
            Top = 0
            Width = 244
            Height = 226
            Align = alClient
            TabOrder = 2
          end
          object GradeZ: TStringGrid
            Left = 764
            Top = 0
            Width = 244
            Height = 226
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 3
            OnDrawCell = GradeZDrawCell
          end
        end
        object TabSheet7: TTabSheet
          Caption = ' Por N'#237'vel '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGNiveis: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 1008
            Height = 226
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Nivel'
                Title.Caption = 'N'#237'vel'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodNiv'
                Title.Caption = 'C'#243'd. n'#237'v.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CODNIV'
                Title.Caption = 'Descri'#231#227'o'
                Width = 400
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Preco'
                Title.Caption = 'Pre'#231'o'
                Width = 93
                Visible = True
              end>
            Color = clWindow
            DataSource = DsTabePrcNiv
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Nivel'
                Title.Caption = 'N'#237'vel'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodNiv'
                Title.Caption = 'C'#243'd. n'#237'v.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CODNIV'
                Title.Caption = 'Descri'#231#227'o'
                Width = 400
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Preco'
                Title.Caption = 'Pre'#231'o'
                Width = 93
                Visible = True
              end>
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 403
      Width = 1014
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 272
        Top = 15
        Width = 740
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 607
          Top = 0
          Width = 133
          Height = 48
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtTabela: TBitBtn
          Tag = 10107
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tabela'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTabelaClick
        end
        object BtGrupos: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtGruposClick
        end
        object BtPrazos: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Prazos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtPrazosClick
        end
        object BtProdutos: TBitBtn
          Tag = 30
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Produtos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtProdutosClick
        end
        object BtNivel: TBitBtn
          Tag = 30
          Left = 371
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&N'#237'vel'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtNivelClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1014
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1014
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label15: TLabel
        Left = 4
        Top = 44
        Width = 66
        Height = 13
        Caption = '% juros / m'#234's:'
        FocusControl = DBEdit7
      end
      object Label16: TLabel
        Left = 96
        Top = 44
        Width = 83
        Height = 13
        Caption = '% desconto m'#225'x.:'
        FocusControl = DBEdit8
      end
      object Label4: TLabel
        Left = 188
        Top = 44
        Width = 61
        Height = 13
        Caption = 'Prazo m'#233'dio:'
        FocusControl = DBEdit8
      end
      object Label5: TLabel
        Left = 588
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdNome
      end
      object Label6: TLabel
        Left = 280
        Top = 44
        Width = 73
        Height = 13
        Caption = 'In'#237'cio vig'#234'ncia:'
        FocusControl = DBEdit8
      end
      object Label10: TLabel
        Left = 388
        Top = 44
        Width = 68
        Height = 13
        Caption = 'Final vig'#234'ncia:'
        FocusControl = DBEdit8
      end
      object Label11: TLabel
        Left = 496
        Top = 44
        Width = 36
        Height = 13
        Caption = 'Moeda:'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 437
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdJurosMes: TdmkEdit
        Left = 4
        Top = 60
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'JurosMes'
        UpdCampo = 'JurosMes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDescoMax: TdmkEdit
        Left = 96
        Top = 60
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'DescoMax'
        UpdCampo = 'DescoMax'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPrazoMed: TdmkEdit
        Left = 188
        Top = 60
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'PrazoMed'
        UpdCampo = 'PrazoMed'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEditDateTimePicker1: TdmkEditDateTimePicker
        Left = 280
        Top = 60
        Width = 104
        Height = 21
        Date = 39816.841424884260000000
        Time = 39816.841424884260000000
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataI'
        UpdCampo = 'DataI'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object dmkEditDateTimePicker2: TdmkEditDateTimePicker
        Left = 388
        Top = 60
        Width = 104
        Height = 21
        Date = 39816.841435625000000000
        Time = 39816.841435625000000000
        TabOrder = 8
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataF'
        UpdCampo = 'DataF'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object dmkEdit1: TdmkEdit
        Left = 588
        Top = 20
        Width = 72
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CGAplicacao: TdmkCheckGroup
        Left = 664
        Top = 11
        Width = 341
        Height = 70
        Caption = ' Aplica'#231#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Pedido de venda'
          'Venda balc'#227'o'
          'Condicional'
          'Relat'#243'rios')
        TabOrder = 11
        QryCampo = 'Aplicacao'
        UpdCampo = 'Aplicacao'
        UpdType = utYes
        Value = 1
        OldValor = 0
      end
      object EdMoeda: TdmkEditCB
        Left = 496
        Top = 60
        Width = 32
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMoeda
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMoeda: TdmkDBLookupComboBox
        Left = 528
        Top = 60
        Width = 133
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsCambioMda
        TabOrder = 10
        dmkEditCB = EdMoeda
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 403
      Width = 1014
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel10: TPanel
        Left = 2
        Top = 15
        Width = 1010
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 867
          Top = 0
          Width = 144
          Height = 48
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 0
    Width = 1014
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 966
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 750
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 223
        Height = 32
        Caption = 'Tabelas de Pre'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 223
        Height = 32
        Caption = 'Tabelas de Pre'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 223
        Height = 32
        Caption = 'Tabelas de Pre'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1014
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1010
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsTabePrcCab: TDataSource
    DataSet = QrTabePrcCab
    Left = 276
    Top = 12
  end
  object QrTabePrcCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTabePrcCabBeforeOpen
    AfterOpen = QrTabePrcCabAfterOpen
    BeforeClose = QrTabePrcCabBeforeClose
    AfterScroll = QrTabePrcCabAfterScroll
    SQL.Strings = (
      'SELECT tpc.Codigo, tpc.CodUsu, tpc.Nome, tpc.DescoMax, '
      'tpc.JurosMes, tpc.PrazoMed, tpc.Sigla, tpc.Aplicacao, '
      'tpc.DataI, tpc.DataF, tpc.Moeda,'
      'CONCAT(mda.Sigla," - ", mda.Nome) NOMEMOEDA'
      'FROM tabeprccab tpc'
      'LEFT JOIN cambiomda mda ON mda.Codigo=tpc.Moeda')
    Left = 248
    Top = 12
    object QrTabePrcCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tabeprccab.Codigo'
      Required = True
    end
    object QrTabePrcCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'tabeprccab.CodUsu'
      Required = True
    end
    object QrTabePrcCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'tabeprccab.Nome'
      Required = True
      Size = 50
    end
    object QrTabePrcCabDescoMax: TFloatField
      FieldName = 'DescoMax'
      Origin = 'tabeprccab.DescoMax'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTabePrcCabJurosMes: TFloatField
      FieldName = 'JurosMes'
      Origin = 'tabeprccab.JurosMes'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTabePrcCabPrazoMed: TFloatField
      FieldName = 'PrazoMed'
      Origin = 'tabeprccab.PrazoMed'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTabePrcCabSigla: TWideStringField
      DisplayWidth = 8
      FieldName = 'Sigla'
      Origin = 'tabeprccab.Sigla'
      Required = True
      Size = 8
    end
    object QrTabePrcCabAplicacao: TSmallintField
      FieldName = 'Aplicacao'
      Origin = 'tabeprccab.Aplicacao'
      Required = True
    end
    object QrTabePrcCabDataI: TDateField
      FieldName = 'DataI'
      Origin = 'tabeprccab.DataI'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrTabePrcCabDataF: TDateField
      FieldName = 'DataF'
      Origin = 'tabeprccab.DataF'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrTabePrcCabNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 38
    end
    object QrTabePrcCabMoeda: TIntegerField
      FieldName = 'Moeda'
      Origin = 'tabeprccab.Moeda'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel10
    Left = 332
    Top = 12
  end
  object PMTabela: TPopupMenu
    OnPopup = PMTabelaPopup
    Left = 408
    Top = 380
    object Incluinovatabeladepreos1: TMenuItem
      Caption = '&Inclui nova tabela de pre'#231'os'
      OnClick = Incluinovatabeladepreos1Click
    end
    object Alteratabelaselecionada1: TMenuItem
      Caption = '&Altera tabela selecionada'
      OnClick = Alteratabelaselecionada1Click
    end
    object Excluitabelaselecionada1: TMenuItem
      Caption = '&Exclui tabela selecionada'
      Enabled = False
    end
  end
  object QrTabePrcGrG: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrTabePrcGrGAfterScroll
    OnCalcFields = QrTabePrcGrGCalcFields
    SQL.Strings = (
      'SELECT gg1.Nome NOME1, gg1.CodUsu, gg1.GraTamCad, tpg.*'
      'FROM tabeprcgrg tpg'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpg.Nivel1'
      'WHERE Codigo=:P0'
      ''
      '')
    Left = 532
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTabePrcGrGNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'tabeprcgrg.Nivel1'
      Required = True
    end
    object QrTabePrcGrGCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrTabePrcGrGNOME1: TWideStringField
      FieldName = 'NOME1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrTabePrcGrGCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tabeprcgrg.Codigo'
      Required = True
    end
    object QrTabePrcGrGPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'tabeprcgrg.Preco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrTabePrcGrGGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrTabePrcGrGKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
  end
  object DsTabePrcGrG: TDataSource
    DataSet = QrTabePrcGrG
    Left = 560
    Top = 8
  end
  object QrTabePrcPzG: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTabePrcPzGCalcFields
    SQL.Strings = (
      'SELECT tpp.*'
      'FROM tabeprcpzg tpp'
      'WHERE Codigo=:P0'
      'ORDER BY DiasIni, DiasFim')
    Left = 620
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTabePrcPzGCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTabePrcPzGControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTabePrcPzGNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 15
    end
    object QrTabePrcPzGDiasIni: TSmallintField
      FieldName = 'DiasIni'
      Required = True
      DisplayFormat = '000'
    end
    object QrTabePrcPzGDiasFim: TSmallintField
      FieldName = 'DiasFim'
      Required = True
      DisplayFormat = '000'
    end
    object QrTabePrcPzGVariacao: TFloatField
      FieldName = 'Variacao'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrTabePrcPzGPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrTabePrcPzGCOL_TXT: TWideStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'COL_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsTabePrcPzG: TDataSource
    DataSet = QrTabePrcPzG
    Left = 648
    Top = 8
  end
  object QrCambioMda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome '
      'FROM cambiomda'
      'ORDER BY Nome'
      ''
      ''
      '')
    Left = 472
    Top = 8
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 500
    Top = 8
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdMoeda
    Panel = PainelEdita
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 360
    Top = 12
  end
  object PMGrupos: TPopupMenu
    Left = 504
    Top = 380
    object Incluinovoprodutoumaum1: TMenuItem
      Caption = 'Inclui novo produto (&um a um)'
      OnClick = Incluinovoprodutoumaum1Click
    end
    object Incluidiversosprodutos1: TMenuItem
      Caption = 'Inclui &diversos produtos'
      OnClick = Incluidiversosprodutos1Click
    end
    object Alterapreodogrupoatual1: TMenuItem
      Caption = '&Altera pre'#231'o do grupo atual'
      OnClick = Alterapreodogrupoatual1Click
    end
  end
  object PMPrazos: TPopupMenu
    Left = 600
    Top = 380
    object Incluinovoperododeprazos1: TMenuItem
      Caption = '&Inclui novo per'#237'odo de prazos'
      OnClick = Incluinovoperododeprazos1Click
    end
    object Alteraperododeprazosatual1: TMenuItem
      Caption = '&Altera per'#237'odo de prazos atual'
      OnClick = Alteraperododeprazosatual1Click
    end
    object Excluiperododeprazosatual1: TMenuItem
      Caption = '&Exclui per'#237'odo de prazos atual'
      OnClick = Excluiperododeprazosatual1Click
    end
  end
  object QrPrecoPrz: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPrecoPrzAfterScroll
    OnCalcFields = QrPrecoPrzCalcFields
    SQL.Strings = (
      'SELECT tpp.*'
      'FROM tabeprcpzg tpp'
      'WHERE Codigo=:P0'
      'ORDER BY DiasIni, DiasFim')
    Left = 708
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrecoPrzCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrecoPrzControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrecoPrzNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 15
    end
    object QrPrecoPrzDiasIni: TSmallintField
      FieldName = 'DiasIni'
      Required = True
    end
    object QrPrecoPrzDiasFim: TSmallintField
      FieldName = 'DiasFim'
      Required = True
    end
    object QrPrecoPrzVariacao: TFloatField
      FieldName = 'Variacao'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPrecoPrzPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      Calculated = True
    end
  end
  object DsPrecoPrz: TDataSource
    DataSet = QrPrecoPrz
    Left = 736
    Top = 8
  end
  object frxDsTabPrcGru: TfrxDBDataset
    UserName = 'frxDsTabPrcGru'
    CloseDataSource = False
    DataSet = QrTabPrcGru
    BCDToCurrency = False
    Left = 812
    Top = 180
  end
  object frxDsTabePrcCab: TfrxDBDataset
    UserName = 'frxDsTabePrcCab'
    CloseDataSource = False
    DataSet = QrTabePrcCab
    BCDToCurrency = False
    Left = 304
    Top = 12
  end
  object frxDsTabePrcPzG: TfrxDBDataset
    UserName = 'frxDsTabePrcPzG'
    CloseDataSource = False
    DataSet = QrTabePrcPzG
    BCDToCurrency = False
    Left = 676
    Top = 8
  end
  object QrTabPrcPrz: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTabPrcPrzCalcFields
    SQL.Strings = (
      'SELECT gg1.Nome NOME1, gg1.CodUsu, tpg.Nivel1, tpg.Preco, '
      'tpp.Controle CtrlPrz, tpp.Nome NOMEPRZ, '
      'tpp.DiasIni, tpp.DiasFim, tpp.Variacao,'
      '(tpp.Variacao/100) + 1 FATOR'
      'FROM tabeprcgrg tpg'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpg.Nivel1'
      'LEFT JOIN tabeprcpzg tpp ON tpp.Codigo=tpg.Codigo'
      'WHERE tpg.Codigo=:P0'
      ''
      'AND tpg.Nivel1=:P1')
    Left = 784
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTabPrcPrzNOME1: TWideStringField
      FieldName = 'NOME1'
      Size = 30
    end
    object QrTabPrcPrzCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrTabPrcPrzNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrTabPrcPrzPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrTabPrcPrzCtrlPrz: TIntegerField
      FieldName = 'CtrlPrz'
      Required = True
    end
    object QrTabPrcPrzNOMEPRZ: TWideStringField
      FieldName = 'NOMEPRZ'
      Size = 15
    end
    object QrTabPrcPrzDiasIni: TSmallintField
      FieldName = 'DiasIni'
    end
    object QrTabPrcPrzDiasFim: TSmallintField
      FieldName = 'DiasFim'
    end
    object QrTabPrcPrzVariacao: TFloatField
      FieldName = 'Variacao'
    end
    object QrTabPrcPrzPRC_PRZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRC_PRZ'
      Calculated = True
    end
    object QrTabPrcPrzFATOR: TFloatField
      FieldName = 'FATOR'
    end
    object QrTabPrcPrzPRC_PRZ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRC_PRZ_TXT'
      Size = 30
      Calculated = True
    end
  end
  object frxDsTabPrcPrz: TfrxDBDataset
    UserName = 'frxDsTabPrcPrz'
    CloseDataSource = False
    DataSet = QrTabPrcPrz
    BCDToCurrency = False
    Left = 812
    Top = 224
  end
  object QrPrcPrzIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPrcPrzItsCalcFields
    SQL.Strings = (
      'SELECT ggx.Controle Reduz, gcc.Nome NOMECOR, gti.Nome NOMETAM, '
      'tpi.Preco, ggx.GraGruC, ggx.GraTamI, gg1.Nome NOMEGRU, '
      'gg1.CodUsu, tpg.Nivel1, tpp.Controle CtrlPrz, tpp.Nome NOMEPRZ, '
      
        'tpp.DiasIni, tpp.DiasFim, tpp.Variacao, (tpp.Variacao/100) + 1 F' +
        'ATOR'
      'FROM tabeprcgrg tpg'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpg.Nivel1'
      'LEFT JOIN tabeprcpzg tpp ON tpp.Codigo=tpg.Codigo'#13
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=tpg.Nivel1'
      'LEFT JOIN tabeprcgri tpi ON tpi.GraGruX=ggx.Controle '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE tpi.Ativo=1'#13
      'AND tpg.Codigo=:P0'
      'AND gg1.Nivel1=:P1'
      'ORDER BY gg1.CodUsu, gti.Controle, NOMECOR, CtrlPrz')
    Left = 896
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrcPrzItsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
    object QrPrcPrzItsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrPrcPrzItsPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrPrcPrzItsNOMEPRZ: TWideStringField
      FieldName = 'NOMEPRZ'
      Size = 15
    end
    object QrPrcPrzItsFATOR: TFloatField
      FieldName = 'FATOR'
    end
    object QrPrcPrzItsVAL_PRZ_PRD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_PRZ_PRD'
      Calculated = True
    end
    object QrPrcPrzItsNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 30
    end
    object QrPrcPrzItsReduz: TIntegerField
      FieldName = 'Reduz'
      Required = True
    end
  end
  object frxPrcGruC_Port: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39867.737163726910000000
    ReportOptions.LastChange = 39867.737163726910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Child3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child3.Visible := <VARF_TEM_GRADE>;                           ' +
        '                 '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxPrcGruC_LandGetValue
    Left = 840
    Top = 180
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = frxDsPrcPrzIts
        DataSetName = 'frxDsPrcPrzIts'
      end
      item
        DataSet = frxDsTabePrcCab
        DataSetName = 'frxDsTabePrcCab'
      end
      item
        DataSet = frxDsTabePrcPzG
        DataSetName = 'frxDsTabePrcPzG'
      end
      item
        DataSet = frxDsTabPrcGru
        DataSetName = 'frxDsTabPrcGru'
      end
      item
        DataSet = frxDsTabPrcPrz
        DataSetName = 'frxDsTabPrcPrz'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 702.992580000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo3: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista [frxDsTabePrcCab."Sigla"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 41.574830000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTabePrcCab."Nome"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 64.252010000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Top = 283.464750000000000000
        Width = 718.110700000000000000
        Child = frxPrcGruC_Port.Child2
        DataSet = frxDsTabPrcGru
        DataSetName = 'frxDsTabPrcGru'
        RowCount = 0
      end
      object Child1: TfrxChild
        FillType = ftBrush
        Height = 70.236220480000000000
        Top = 188.976500000000000000
        Width = 718.110700000000000000
        object DBCross1: TfrxDBCrossView
          Width = 304.566929132205000000
          Height = 70.236220479999990000
          AutoSize = False
          DownThenAcross = False
          JoinEqualCells = True
          KeepTogether = True
          RowLevels = 3
          ShowColumnTotal = False
          ShowRowTotal = False
          ShowTitle = False
          CellFields.Strings = (
            'Variacao')
          ColumnFields.Strings = (
            'Nome')
          DataSet = frxDsTabePrcPzG
          DataSetName = 'frxDsTabePrcPzG'
          RowFields.Strings = (
            'COL_TXT'
            'COL_TXT'
            'COL_TXT')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D223234362C
            3737313635333534323230352220546F703D223232342C303934363130323422
            2057696474683D2233372C373935323735353922204865696768743D2231352C
            313138313130323422205265737472696374696F6E733D2232342220416C6C6F
            7745787072657373696F6E733D2246616C73652220446973706C6179466F726D
            61742E466F726D61745374723D2225322E326E2220446973706C6179466F726D
            61742E4B696E643D22666B4E756D657269632220466F6E742E43686172736574
            3D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D
            222D382220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223022204672616D652E436F6C6F723D2231363737373231352220467261
            6D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2231303231
            383439352220476170583D22332220476170593D2233222048416C69676E3D22
            686152696768742220506172656E74466F6E743D2246616C73652220576F7264
            577261703D2246616C7365222056416C69676E3D22766143656E746572222054
            6578743D22302C3030222F3E3C546672784D656D6F56696577204C6566743D22
            3339392C30363239393231322220546F703D2236372C39343438383138392220
            57696474683D2236382C303331343936303622204865696768743D2232322220
            5265737472696374696F6E733D2232342220416C6C6F7745787072657373696F
            6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E74
            2E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D22313032313834393522204761
            70583D22332220476170593D2233222048416C69676E3D226861526967687422
            20506172656E74466F6E743D2246616C7365222056416C69676E3D2276614365
            6E7465722220546578743D22302C3030222F3E3C546672784D656D6F56696577
            204C6566743D223437332C30363239393231322220546F703D2236372C393434
            3838313839222057696474683D2236382C303331343936303622204865696768
            743D22323222205265737472696374696F6E733D2232342220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223130323138
            3439352220476170583D22332220476170593D2233222048416C69676E3D2268
            6152696768742220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D22302C3030222F3E3C546672784D
            656D6F56696577204C6566743D22302220546F703D2230222057696474683D22
            3022204865696768743D223022205265737472696374696F6E733D2238222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2231303231383439352220476170583D22332220476170593D2233222048416C
            69676E3D22686152696768742220506172656E74466F6E743D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C546672
            784D656D6F56696577204C6566743D22302220546F703D223022205769647468
            3D223022204865696768743D223022205265737472696374696F6E733D223822
            20416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43
            6861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48
            65696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F
            6E742E5374796C653D223022204672616D652E436F6C6F723D22313637373732
            313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F
            723D2231303231383439352220476170583D22332220476170593D2233222048
            416C69676E3D22686152696768742220506172656E74466F6E743D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D22222F3E3C54
            6672784D656D6F56696577204C6566743D22302220546F703D22302220576964
            74683D223022204865696768743D223022205265737472696374696F6E733D22
            382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231303231383439352220476170583D22332220476170593D223322
            2048416C69676E3D22686152696768742220506172656E74466F6E743D224661
            6C7365222056416C69676E3D22766143656E7465722220546578743D22222F3E
            3C546672784D656D6F56696577204C6566743D22302220546F703D2230222057
            696474683D223022204865696768743D223022205265737472696374696F6E73
            3D22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2231303231383439352220476170583D22332220476170593D22
            33222048416C69676E3D22686152696768742220506172656E74466F6E743D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2222
            2F3E3C546672784D656D6F56696577204C6566743D22302220546F703D223022
            2057696474683D223022204865696768743D223022205265737472696374696F
            6E733D22382220416C6C6F7745787072657373696F6E733D2246616C73652220
            466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220
            466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269
            616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D22
            313637373732313522204672616D652E5479703D223135222046696C6C2E4261
            636B436F6C6F723D2231303231383439352220476170583D2233222047617059
            3D2233222048416C69676E3D22686152696768742220506172656E74466F6E74
            3D2246616C7365222056416C69676E3D22766143656E7465722220546578743D
            22222F3E3C2F63656C6C6D656D6F733E3C63656C6C6865616465726D656D6F73
            3E3C546672784D656D6F56696577204C6566743D22302220546F703D22302220
            57696474683D223022204865696768743D223022205265737472696374696F6E
            733D22382220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241726961
            6C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231
            3637373732313522204672616D652E5479703D223135222046696C6C2E426163
            6B436F6C6F723D22343634333538332220476170583D22332220476170593D22
            332220506172656E74466F6E743D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22566172696163616F222F3E3C546672784D65
            6D6F56696577204C6566743D22302220546F703D2230222057696474683D2230
            22204865696768743D223022205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            343634333538332220476170583D22332220476170593D22332220506172656E
            74466F6E743D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D22566172696163616F222F3E3C546672784D656D6F56696577204C
            6566743D22302220546F703D2230222057696474683D22302220486569676874
            3D223022205265737472696374696F6E733D22382220416C6C6F774578707265
            7373696F6E733D2246616C73652220466F6E742E436861727365743D22312220
            466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D313322
            20466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230
            22204672616D652E436F6C6F723D22313637373732313522204672616D652E54
            79703D223135222046696C6C2E4261636B436F6C6F723D223436343335383322
            20476170583D22332220476170593D22332220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D225661
            72696163616F222F3E3C546672784D656D6F56696577204C6566743D22302220
            546F703D2230222057696474683D223022204865696768743D22302220526573
            7472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E61
            6D653D22417269616C2220466F6E742E5374796C653D223022204672616D652E
            436F6C6F723D22313637373732313522204672616D652E5479703D2231352220
            46696C6C2E4261636B436F6C6F723D22343634333538332220476170583D2233
            2220476170593D22332220506172656E74466F6E743D2246616C736522205641
            6C69676E3D22766143656E7465722220546578743D22566172696163616F222F
            3E3C2F63656C6C6865616465726D656D6F733E3C636F6C756D6E6D656D6F733E
            3C546672784D656D6F56696577204C6566743D223234362C3737313635333534
            323230352220546F703D223230382C39373635222057696474683D2233372C37
            3935323735353922204865696768743D2231352C313138313130323422205265
            737472696374696F6E733D2232342220416C6C6F7745787072657373696F6E73
            3D2246616C73652220466F6E742E436861727365743D22312220466F6E742E43
            6F6C6F723D22302220466F6E742E4865696768743D222D382220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223022204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D22343634333538332220476170583D22
            332220476170593D2233222048416C69676E3D22686152696768742220506172
            656E74466F6E743D2246616C73652220576F7264577261703D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C2F636F
            6C756D6E6D656D6F733E3C636F6C756D6E746F74616C6D656D6F733E3C546672
            784D656D6F56696577204C6566743D2235302220546F703D2232312220576964
            74683D22383122204865696768743D22323222205265737472696374696F6E73
            3D2238222056697369626C653D2246616C73652220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223122
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22343634333538332220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D224772616E6420546F74616C222F3E3C2F63
            6F6C756D6E746F74616C6D656D6F733E3C636F726E65726D656D6F733E3C5466
            72784D656D6F56696577204C6566743D2232302220546F703D223230382C3937
            3635222057696474683D223232362C3737313635333534323230352220486569
            6768743D223022205265737472696374696F6E733D2238222056697369626C65
            3D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C7365
            2220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230
            2220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241
            7269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F72
            3D22313637373732313522204672616D652E5479703D223135222046696C6C2E
            4261636B436F6C6F723D22343634333538332220476170583D22332220476170
            593D2233222048416C69676E3D22686143656E7465722220506172656E74466F
            6E743D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D22566172696163616F222F3E3C546672784D656D6F56696577204C656674
            3D223234362C3737313635333534323230352220546F703D223230382C393736
            35222057696474683D2233372C373935323735353922204865696768743D2230
            22205265737472696374696F6E733D2238222056697369626C653D2246616C73
            652220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D22343634333538332220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E7465722220506172656E74466F6E743D224661
            6C7365222056416C69676E3D22766143656E7465722220546578743D2225222F
            3E3C546672784D656D6F56696577204C6566743D22302220546F703D22302220
            57696474683D223022204865696768743D223022205265737472696374696F6E
            733D2238222056697369626C653D2246616C73652220416C6C6F774578707265
            7373696F6E733D2246616C73652220466F6E742E436861727365743D22312220
            466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D313322
            20466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230
            22204672616D652E436F6C6F723D22313637373732313522204672616D652E54
            79703D223135222046696C6C2E4261636B436F6C6F723D223436343335383322
            20476170583D22332220476170593D2233222048416C69676E3D22686143656E
            7465722220506172656E74466F6E743D2246616C7365222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C546672784D656D6F56696577
            204C6566743D2232302220546F703D223230382C39373635222057696474683D
            223135312C31383131303233363232303522204865696768743D2231352C3131
            38313130323422205265737472696374696F6E733D22382220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            382220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223022204672616D652E436F6C6F723D22313637373732313522204672616D65
            2E5479703D223135222046696C6C2E4261636B436F6C6F723D22343634333538
            332220476170583D22332220476170593D22332220506172656E74466F6E743D
            2246616C73652220576F7264577261703D2246616C7365222056416C69676E3D
            22766143656E7465722220546578743D22546162656C61206461207661726961
            C3A7C3A36F20656D2025222F3E3C546672784D656D6F56696577204C6566743D
            223137312C3138313130323336323230352220546F703D223230382C39373635
            222057696474683D2233372C373935323735353922204865696768743D223135
            2C313138313130323422205265737472696374696F6E733D22382220416C6C6F
            7745787072657373696F6E733D2246616C73652220466F6E742E436861727365
            743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569676874
            3D222D382220466F6E742E4E616D653D22417269616C2220466F6E742E537479
            6C653D223022204672616D652E436F6C6F723D22313637373732313522204672
            616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22343634
            333538332220476170583D22332220476170593D2233222048416C69676E3D22
            686143656E7465722220506172656E74466F6E743D2246616C73652220576F72
            64577261703D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D2243C3B36469676F222F3E3C546672784D656D6F56696577204C65
            66743D223230382C3937363337373935323230352220546F703D223230382C39
            373635222057696474683D2233372C373935323735353922204865696768743D
            2231352C313138313130323422205265737472696374696F6E733D2238222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D382220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            343634333538332220476170583D22332220476170593D2233222048416C6967
            6E3D22686143656E7465722220506172656E74466F6E743D2246616C73652220
            576F7264577261703D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D22507265C3A76F222F3E3C2F636F726E65726D656D6F733E
            3C726F776D656D6F733E3C546672784D656D6F56696577204C6566743D223230
            2220546F703D223232342C3039343631303234222057696474683D223135312C
            31383131303233363232303522204865696768743D2231352C31313831313032
            3422205265737472696374696F6E733D2232342220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D38222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2234363433353833222047
            6170583D22332220476170593D22332220506172656E74466F6E743D2246616C
            73652220576F7264577261703D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C65
            66743D223137312C3138313130323336323230352220546F703D223232342C30
            39343631303234222057696474683D2233372C37393532373535392220486569
            6768743D2231352C313138313130323422205265737472696374696F6E733D22
            32342220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D382220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D22343634333538332220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E7465722220506172656E74466F6E743D224661
            6C73652220576F7264577261703D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C
            6566743D223230382C3937363337373935323230352220546F703D223232342C
            3039343631303234222057696474683D2233372C373935323735353922204865
            696768743D2231352C313138313130323422205265737472696374696F6E733D
            2232342220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D382220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223022204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D22343634333538332220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C73652220576F7264577261703D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22222F3E3C2F726F776D656D6F733E3C726F
            77746F74616C6D656D6F733E3C546672784D656D6F56696577204C6566743D22
            302220546F703D2230222057696474683D223022204865696768743D22302220
            5265737472696374696F6E733D2238222056697369626C653D2246616C736522
            20416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43
            6861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48
            65696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F
            6E742E5374796C653D223122204672616D652E436F6C6F723D22313637373732
            313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F
            723D22343634333538332220476170583D22332220476170593D223322204841
            6C69676E3D22686143656E7465722220506172656E74466F6E743D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D224772616E64
            20546F74616C222F3E3C546672784D656D6F56696577204C6566743D22323030
            2220546F703D223436222057696474683D22353422204865696768743D223232
            22205265737472696374696F6E733D2238222056697369626C653D2246616C73
            652220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223122204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D22343634333538332220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E7465722220506172656E74466F6E743D224661
            6C7365222056416C69676E3D22766143656E7465722220546578743D22546F74
            616C222F3E3C546672784D656D6F56696577204C6566743D223236382C303331
            3439363036323939322220546F703D223638222057696474683D223534222048
            65696768743D22323222205265737472696374696F6E733D2238222056697369
            626C653D2246616C73652220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223122204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D22343634333538332220476170583D22332220
            476170593D2233222048416C69676E3D22686143656E7465722220506172656E
            74466F6E743D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D22546F74616C222F3E3C2F726F77746F74616C6D656D6F733E3C63
            656C6C66756E6374696F6E733E3C6974656D20312F3E3C2F63656C6C66756E63
            74696F6E733E3C636F6C756D6E736F72743E3C6974656D20302F3E3C2F636F6C
            756D6E736F72743E3C726F77736F72743E3C6974656D20302F3E3C6974656D20
            302F3E3C6974656D20302F3E3C2F726F77736F72743E3C2F63726F73733E}
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        Child = frxPrcGruC_Port.Child1
        RowCount = 1
      end
      object Child2: TfrxChild
        FillType = ftBrush
        Height = 74.015750480000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        Child = frxPrcGruC_Port.Child3
        object DBCross3: TfrxDBCrossView
          Top = 3.779530000000022000
          Width = 307.771653542204900000
          Height = 70.236220480000040000
          AutoSize = False
          DownThenAcross = False
          JoinEqualCells = True
          KeepTogether = True
          RowLevels = 3
          ShowColumnTotal = False
          ShowRowTotal = False
          ShowTitle = False
          CellFields.Strings = (
            'PRC_PRZ')
          ColumnFields.Strings = (
            'NOMEPRZ')
          DataSet = frxDsTabPrcPrz
          DataSetName = 'frxDsTabPrcPrz'
          RowFields.Strings = (
            'NOME1'
            'CodUsu'
            'Preco')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D223234362C
            3737313635333534323230352220546F703D223334352C303339353730323422
            2057696474683D22343122204865696768743D2231352C313138313130323422
            205265737472696374696F6E733D2232342220416C6C6F774578707265737369
            6F6E733D2246616C73652220446973706C6179466F726D61742E446563696D61
            6C536570617261746F723D222C2220446973706C6179466F726D61742E466F72
            6D61745374723D2225322E326E2220446973706C6179466F726D61742E4B696E
            643D22666B4E756D657269632220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D38222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2231363730303334362220
            476170583D22332220476170593D2233222048416C69676E3D22686152696768
            742220506172656E74466F6E743D2246616C73652220576F7264577261703D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2230
            2C3030222F3E3C546672784D656D6F56696577205461673D223122204C656674
            3D223333302C3033313439363036323939322220546F703D2232312220576964
            74683D2236382C3033313439363036323939323122204865696768743D223232
            22205265737472696374696F6E733D2232342220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2231363730303334362220
            476170583D22332220476170593D2233222048416C69676E3D22686152696768
            742220506172656E74466F6E743D2246616C736522205374796C653D2263656C
            6C222056416C69676E3D22766143656E7465722220546578743D22302C303022
            2F3E3C546672784D656D6F56696577205461673D223222204C6566743D223230
            352220546F703D223433222057696474683D22383122204865696768743D2232
            3122205265737472696374696F6E733D2232342220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223022
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22313637303033343622
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            68742220506172656E74466F6E743D2246616C736522205374796C653D226365
            6C6C222056416C69676E3D22766143656E7465722220546578743D2252242030
            2C3030222F3E3C546672784D656D6F56696577205461673D223322204C656674
            3D223230352220546F703D223634222057696474683D22383122204865696768
            743D22323222205265737472696374696F6E733D2232342220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223136373030
            3334362220476170583D22332220476170593D2233222048416C69676E3D2268
            6152696768742220506172656E74466F6E743D2246616C736522205374796C65
            3D2263656C6C222056416C69676E3D22766143656E7465722220546578743D22
            522420302C3030222F3E3C546672784D656D6F56696577204C6566743D223022
            20546F703D2230222057696474683D223022204865696768743D223022205265
            737472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D
            2246616C73652220466F6E742E436861727365743D22312220466F6E742E436F
            6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223022204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D2231363730303334362220476170583D
            22332220476170593D2233222048416C69676E3D226861526967687422205061
            72656E74466F6E743D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C546672784D656D6F56696577204C6566743D22
            302220546F703D2230222057696474683D223022204865696768743D22302220
            5265737472696374696F6E733D22382220416C6C6F7745787072657373696F6E
            733D2246616C73652220466F6E742E436861727365743D22312220466F6E742E
            436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E74
            2E4E616D653D22417269616C2220466F6E742E5374796C653D22302220467261
            6D652E436F6C6F723D22313637373732313522204672616D652E5479703D2231
            35222046696C6C2E4261636B436F6C6F723D2231363730303334362220476170
            583D22332220476170593D2233222048416C69676E3D22686152696768742220
            506172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E
            7465722220546578743D22222F3E3C546672784D656D6F56696577204C656674
            3D22302220546F703D2230222057696474683D223022204865696768743D2230
            22205265737472696374696F6E733D22382220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223136373030333436222047
            6170583D22332220476170593D2233222048416C69676E3D2268615269676874
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C65
            66743D22302220546F703D2230222057696474683D223022204865696768743D
            223022205265737472696374696F6E733D22382220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223022
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22313637303033343622
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            68742220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22222F3E3C2F63656C6C6D656D6F733E3C63
            656C6C6865616465726D656D6F733E3C546672784D656D6F5669657720546167
            3D2236303022204C6566743D22302220546F703D2230222057696474683D2230
            22204865696768743D223022205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            31363632393134332220476170583D22332220476170593D2233222050617265
            6E74466F6E743D2246616C736522205374796C653D2263656C6C686561646572
            222056416C69676E3D22766143656E7465722220546578743D225052435F5052
            5A222F3E3C546672784D656D6F56696577205461673D2236303122204C656674
            3D22302220546F703D2230222057696474683D223022204865696768743D2230
            22205265737472696374696F6E733D22382220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223136363239313433222047
            6170583D22332220476170593D22332220506172656E74466F6E743D2246616C
            736522205374796C653D2263656C6C686561646572222056416C69676E3D2276
            6143656E7465722220546578743D225052435F50525A222F3E3C546672784D65
            6D6F56696577204C6566743D22302220546F703D2230222057696474683D2230
            22204865696768743D223022205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            31363632393134332220476170583D22332220476170593D2233222050617265
            6E74466F6E743D2246616C7365222056416C69676E3D22766143656E74657222
            20546578743D225052435F50525A222F3E3C546672784D656D6F56696577204C
            6566743D22302220546F703D2230222057696474683D22302220486569676874
            3D223022205265737472696374696F6E733D22382220416C6C6F774578707265
            7373696F6E733D2246616C73652220466F6E742E436861727365743D22312220
            466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D313322
            20466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230
            22204672616D652E436F6C6F723D22313637373732313522204672616D652E54
            79703D223135222046696C6C2E4261636B436F6C6F723D223136363239313433
            2220476170583D22332220476170593D22332220506172656E74466F6E743D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2250
            52435F50525A222F3E3C2F63656C6C6865616465726D656D6F733E3C636F6C75
            6D6E6D656D6F733E3C546672784D656D6F56696577205461673D223130302220
            4C6566743D223234362C3737313635333534323230352220546F703D22333239
            2C3932313436222057696474683D22343122204865696768743D2231352C3131
            38313130323422205265737472696374696F6E733D2232342220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D382220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223136363239
            3134332220476170583D22332220476170593D2233222048416C69676E3D2268
            6152696768742220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D22222F3E3C2F636F6C756D6E6D65
            6D6F733E3C636F6C756D6E746F74616C6D656D6F733E3C546672784D656D6F56
            696577205461673D2233303022204C6566743D223230352220546F703D223231
            222057696474683D22383122204865696768743D223232222052657374726963
            74696F6E733D2238222056697369626C653D2246616C73652220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223122204672616D652E436F6C6F723D2231363737373231352220467261
            6D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2231363632
            393134332220476170583D22332220476170593D2233222048416C69676E3D22
            686143656E7465722220506172656E74466F6E743D2246616C73652220537479
            6C653D22636F6C6772616E64222056416C69676E3D22766143656E7465722220
            546578743D224772616E6420546F74616C222F3E3C2F636F6C756D6E746F7461
            6C6D656D6F733E3C636F726E65726D656D6F733E3C546672784D656D6F566965
            77205461673D2235303022204C6566743D2232302220546F703D223332392C39
            32313436222057696474683D223232362C373731363533353432323035222048
            65696768743D223022205265737472696374696F6E733D223822205669736962
            6C653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C
            73652220446973706C6179466F726D61742E446563696D616C53657061726174
            6F723D222C2220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E61
            6D653D22417269616C2220466F6E742E5374796C653D223022204672616D652E
            436F6C6F723D22313637373732313522204672616D652E5479703D2231352220
            46696C6C2E4261636B436F6C6F723D2231363632393134332220476170583D22
            332220476170593D2233222048416C69676E3D22686143656E74657222205061
            72656E74466F6E743D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D22477275706F732064652050726F6475746F73222F3E3C54
            6672784D656D6F56696577205461673D2235303122204C6566743D223234362C
            3737313635333534323230352220546F703D223332392C393231343622205769
            6474683D22343122204865696768743D223022205265737472696374696F6E73
            3D2238222056697369626C653D2246616C73652220416C6C6F77457870726573
            73696F6E733D2246616C73652220446973706C6179466F726D61742E44656369
            6D616C536570617261746F723D222C2220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31
            332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223022204672616D652E436F6C6F723D22313637373732313522204672616D65
            2E5479703D223135222046696C6C2E4261636B436F6C6F723D22313636323931
            34332220476170583D22332220476170593D2233222048416C69676E3D226861
            43656E7465722220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D225072617A6F222F3E3C54667278
            4D656D6F56696577205461673D2235303222204C6566743D22302220546F703D
            2230222057696474683D223022204865696768743D2230222052657374726963
            74696F6E733D2238222056697369626C653D2246616C73652220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223022204672616D652E436F6C6F723D2231363737373231352220467261
            6D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2231363632
            393134332220476170583D22332220476170593D2233222048416C69676E3D22
            686143656E7465722220506172656E74466F6E743D2246616C73652220537479
            6C653D22636F726E6572222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C546672784D656D6F56696577205461673D2235303322204C
            6566743D2232302220546F703D223332392C3932313436222057696474683D22
            3135312C31383131303233363232303522204865696768743D2231352C313138
            313130323422205265737472696374696F6E733D22382220416C6C6F77457870
            72657373696F6E733D2246616C73652220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D38
            2220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22
            3022204672616D652E436F6C6F723D22313637373732313522204672616D652E
            5479703D223135222046696C6C2E4261636B436F6C6F723D2231363632393134
            332220476170583D22332220476170593D22332220506172656E74466F6E743D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            546162656C6120646F7320707265C3A76F7320646F7320677275706F73222F3E
            3C546672784D656D6F56696577204C6566743D223137312C3138313130323336
            323230352220546F703D223332392C3932313436222057696474683D2233372C
            373935323735353922204865696768743D2231352C3131383131303234222052
            65737472696374696F6E733D22382220416C6C6F7745787072657373696F6E73
            3D2246616C73652220466F6E742E436861727365743D22312220466F6E742E43
            6F6C6F723D22302220466F6E742E4865696768743D222D382220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223022204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D2231363632393134332220476170583D
            22332220476170593D2233222048416C69676E3D22686143656E746572222050
            6172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E74
            65722220546578743D2243C3B36469676F222F3E3C546672784D656D6F566965
            77204C6566743D223230382C3937363337373935323230352220546F703D2233
            32392C3932313436222057696474683D2233372C373935323735353922204865
            696768743D2231352C313138313130323422205265737472696374696F6E733D
            22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D382220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231363632393134332220476170583D22332220476170593D223322
            2048416C69676E3D22686152696768742220506172656E74466F6E743D224661
            6C7365222056416C69676E3D22766143656E7465722220546578743D22507265
            C3A76F222F3E3C2F636F726E65726D656D6F733E3C726F776D656D6F733E3C54
            6672784D656D6F56696577205461673D2232303022204C6566743D2232302220
            546F703D223334352C3033393537303234222057696474683D223135312C3138
            3131303233363232303522204865696768743D2231352C313138313130323422
            205265737472696374696F6E733D2232342220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D382220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D22313636323931343322204761
            70583D22332220476170593D22332220506172656E74466F6E743D2246616C73
            652220576F7264577261703D2246616C7365222056416C69676E3D2276614365
            6E7465722220546578743D22222F3E3C546672784D656D6F56696577204C6566
            743D223137312C3138313130323336323230352220546F703D223334352C3033
            393537303234222057696474683D2233372C3739353237353539222048656967
            68743D2231352C313138313130323422205265737472696374696F6E733D2232
            342220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D382220466F6E742E4E616D653D22417269616C222046
            6F6E742E5374796C653D223022204672616D652E436F6C6F723D223136373737
            32313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C
            6F723D2231363632393134332220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E7465722220506172656E74466F6E743D224661
            6C73652220576F7264577261703D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C
            6566743D223230382C3937363337373935323230352220546F703D223334352C
            3033393537303234222057696474683D2233372C373935323735353922204865
            696768743D2231352C313138313130323422205265737472696374696F6E733D
            2232342220416C6C6F7745787072657373696F6E733D2246616C736522204469
            73706C6179466F726D61742E466F726D61745374723D2225322E326E22204469
            73706C6179466F726D61742E4B696E643D22666B4E756D657269632220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D382220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231363632393134332220476170583D22332220476170593D223322
            2048416C69676E3D22686152696768742220506172656E74466F6E743D224661
            6C73652220576F7264577261703D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22222F3E3C2F726F776D656D6F733E3C726F77
            746F74616C6D656D6F733E3C546672784D656D6F56696577205461673D223430
            3022204C6566743D22302220546F703D223634222057696474683D2231323522
            204865696768743D22323222205265737472696374696F6E733D223822205669
            7369626C653D2246616C73652220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E61
            6D653D22417269616C2220466F6E742E5374796C653D223122204672616D652E
            436F6C6F723D22313637373732313522204672616D652E5479703D2231352220
            46696C6C2E4261636B436F6C6F723D2231363632393134332220476170583D22
            332220476170593D2233222048416C69676E3D22686143656E74657222205061
            72656E74466F6E743D2246616C736522205374796C653D22726F776772616E64
            222056416C69676E3D22766143656E7465722220546578743D224772616E6420
            546F74616C222F3E3C546672784D656D6F56696577204C6566743D2232303022
            20546F703D223436222057696474683D22343922204865696768743D22323222
            205265737472696374696F6E733D2238222056697369626C653D2246616C7365
            2220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E
            436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E
            4865696768743D222D31332220466F6E742E4E616D653D22417269616C222046
            6F6E742E5374796C653D223122204672616D652E436F6C6F723D223136373737
            32313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C
            6F723D2231363632393134332220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E7465722220506172656E74466F6E743D224661
            6C7365222056416C69676E3D22766143656E7465722220546578743D22546F74
            616C222F3E3C546672784D656D6F56696577204C6566743D223236382C303331
            3439363036323939322220546F703D223231222057696474683D223632222048
            65696768743D22323222205265737472696374696F6E733D2238222056697369
            626C653D2246616C73652220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223122204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D2231363632393134332220476170583D223322
            20476170593D2233222048416C69676E3D22686143656E746572222050617265
            6E74466F6E743D2246616C7365222056416C69676E3D22766143656E74657222
            20546578743D22546F74616C222F3E3C2F726F77746F74616C6D656D6F733E3C
            63656C6C66756E6374696F6E733E3C6974656D20312F3E3C2F63656C6C66756E
            6374696F6E733E3C636F6C756D6E736F72743E3C6974656D20302F3E3C2F636F
            6C756D6E736F72743E3C726F77736F72743E3C6974656D20302F3E3C6974656D
            20302F3E3C6974656D20302F3E3C2F726F77736F72743E3C2F63726F73733E}
        end
      end
      object Child3: TfrxChild
        FillType = ftBrush
        Height = 130.708661420000000000
        Top = 404.409710000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'Child3OnBeforePrint'
        object DBCross2: TfrxDBCrossView
          Width = 198.590551180000000000
          Height = 130.708661421102500000
          AutoSize = False
          CellLevels = 2
          ColumnLevels = 3
          DownThenAcross = False
          KeepTogether = True
          PlainCells = True
          ShowColumnTotal = False
          ShowRowTotal = False
          CellFields.Strings = (
            'Reduz'
            'VAL_PRZ_PRD')
          ColumnFields.Strings = (
            'NOMEGRU'
            'NOMETAM'
            'NOMEPRZ')
          DataSet = frxDsPrcPrzIts
          DataSetName = 'frxDsPrcPrzIts'
          RowFields.Strings = (
            'NOMECOR')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D2231303322
            20546F703D223530302C303030323631313834383832222057696474683D2233
            372C373935323735353922204865696768743D2231352C313138313130323336
            3232303522205265737472696374696F6E733D2232342220416C6C6F77457870
            72657373696F6E733D2246616C73652220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D38
            2220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22
            3022204672616D652E436F6C6F723D22313637373732313522204672616D652E
            5479703D223135222046696C6C2E4261636B436F6C6F723D2235333931382220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C73652220576F7264577261703D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            30222F3E3C546672784D656D6F56696577204C6566743D223134302C37393532
            373535392220546F703D223530302C3030303236313138343838322220576964
            74683D2233372C373935323735353922204865696768743D2231352C31313831
            31303233363232303522205265737472696374696F6E733D2232342220416C6C
            6F7745787072657373696F6E733D2246616C73652220446973706C6179466F72
            6D61742E466F726D61745374723D2225322E326E2220446973706C6179466F72
            6D61742E4B696E643D22666B4E756D657269632220466F6E742E436861727365
            743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569676874
            3D222D382220466F6E742E4E616D653D22417269616C2220466F6E742E537479
            6C653D223022204672616D652E436F6C6F723D22313637373732313522204672
            616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22353339
            31382220476170583D22332220476170593D2233222048416C69676E3D226861
            52696768742220506172656E74466F6E743D2246616C7365222056416C69676E
            3D22766143656E7465722220546578743D22302C3030222F3E3C546672784D65
            6D6F56696577204C6566743D223139362C33383538323637372220546F703D22
            3634222057696474683D2231313722204865696768743D223231222052657374
            72696374696F6E733D2232342220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E61
            6D653D22417269616C2220466F6E742E5374796C653D223022204672616D652E
            436F6C6F723D22313637373732313522204672616D652E5479703D2231352220
            46696C6C2E4261636B436F6C6F723D2235333931382220476170583D22332220
            476170593D2233222048416C69676E3D22686152696768742220506172656E74
            466F6E743D2246616C7365222056416C69676E3D22766143656E746572222054
            6578743D22302C3030222F3E3C546672784D656D6F56696577204C6566743D22
            302220546F703D2230222057696474683D223022204865696768743D22302220
            5265737472696374696F6E733D22382220416C6C6F7745787072657373696F6E
            733D2246616C73652220466F6E742E436861727365743D22312220466F6E742E
            436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E74
            2E4E616D653D22417269616C2220466F6E742E5374796C653D22302220467261
            6D652E436F6C6F723D22313637373732313522204672616D652E5479703D2231
            35222046696C6C2E4261636B436F6C6F723D2235333931382220476170583D22
            332220476170593D2233222048416C69676E3D22686152696768742220506172
            656E74466F6E743D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D22222F3E3C546672784D656D6F56696577204C6566743D2231
            38332220546F703D223633222057696474683D22383122204865696768743D22
            323122205265737472696374696F6E733D2232342220416C6C6F774578707265
            7373696F6E733D2246616C73652220466F6E742E436861727365743D22312220
            466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D313322
            20466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230
            22204672616D652E436F6C6F723D22313637373732313522204672616D652E54
            79703D223135222046696C6C2E4261636B436F6C6F723D223533393138222047
            6170583D22332220476170593D2233222048416C69676E3D2268615269676874
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22302C3030222F3E3C546672784D656D6F566965
            77204C6566743D223138332220546F703D223834222057696474683D22383122
            204865696768743D22323222205265737472696374696F6E733D223234222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2235333931382220476170583D22332220476170593D2233222048416C69676E
            3D22686152696768742220506172656E74466F6E743D2246616C736522205641
            6C69676E3D22766143656E7465722220546578743D22302C3030222F3E3C5466
            72784D656D6F56696577204C6566743D22302220546F703D2230222057696474
            683D223022204865696768743D223022205265737472696374696F6E733D2238
            2220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E
            436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E
            4865696768743D222D31332220466F6E742E4E616D653D22417269616C222046
            6F6E742E5374796C653D223022204672616D652E436F6C6F723D223136373737
            32313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C
            6F723D2235333931382220476170583D22332220476170593D2233222048416C
            69676E3D22686152696768742220506172656E74466F6E743D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C546672
            784D656D6F56696577204C6566743D22302220546F703D223022205769647468
            3D223022204865696768743D223022205265737472696374696F6E733D223822
            20416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43
            6861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48
            65696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F
            6E742E5374796C653D223022204672616D652E436F6C6F723D22313637373732
            313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F
            723D2235333931382220476170583D22332220476170593D2233222048416C69
            676E3D22686152696768742220506172656E74466F6E743D2246616C73652220
            56416C69676E3D22766143656E7465722220546578743D22222F3E3C54667278
            4D656D6F56696577204C6566743D22302220546F703D2230222057696474683D
            223022204865696768743D223022205265737472696374696F6E733D22382220
            416C6C6F7745787072657373696F6E733D2246616C736522204672616D652E54
            79703D2231352220476170583D22332220476170593D2233222048416C69676E
            3D2268615269676874222056416C69676E3D22766143656E7465722220546578
            743D22222F3E3C546672784D656D6F56696577204C6566743D22302220546F70
            3D2230222057696474683D223022204865696768743D22302220526573747269
            6374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C
            736522204672616D652E5479703D2231352220476170583D2233222047617059
            3D2233222048416C69676E3D2268615269676874222056416C69676E3D227661
            43656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C
            6566743D22302220546F703D2230222057696474683D22302220486569676874
            3D223022205265737472696374696F6E733D22382220416C6C6F774578707265
            7373696F6E733D2246616C736522204672616D652E5479703D22313522204761
            70583D22332220476170593D2233222048416C69676E3D226861526967687422
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C546672
            784D656D6F56696577204C6566743D22302220546F703D223022205769647468
            3D223022204865696768743D223022205265737472696374696F6E733D223822
            20416C6C6F7745787072657373696F6E733D2246616C736522204672616D652E
            5479703D2231352220476170583D22332220476170593D2233222048416C6967
            6E3D2268615269676874222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C546672784D656D6F56696577204C6566743D22302220546F
            703D2230222057696474683D223022204865696768743D223022205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C736522204672616D652E5479703D2231352220476170583D22332220476170
            593D2233222048416C69676E3D2268615269676874222056416C69676E3D2276
            6143656E7465722220546578743D22222F3E3C546672784D656D6F5669657720
            4C6566743D22302220546F703D2230222057696474683D223022204865696768
            743D223022205265737472696374696F6E733D22382220416C6C6F7745787072
            657373696F6E733D2246616C736522204672616D652E5479703D223135222047
            6170583D22332220476170593D2233222048416C69676E3D2268615269676874
            222056416C69676E3D22766143656E7465722220546578743D22222F3E3C5466
            72784D656D6F56696577204C6566743D22302220546F703D2230222057696474
            683D223022204865696768743D223022205265737472696374696F6E733D2238
            2220416C6C6F7745787072657373696F6E733D2246616C736522204672616D65
            2E5479703D2231352220476170583D22332220476170593D2233222048416C69
            676E3D2268615269676874222056416C69676E3D22766143656E746572222054
            6578743D22222F3E3C546672784D656D6F56696577204C6566743D2230222054
            6F703D2230222057696474683D223022204865696768743D2230222052657374
            72696374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246
            616C736522204672616D652E5479703D2231352220476170583D223322204761
            70593D2233222048416C69676E3D2268615269676874222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C2F63656C6C6D656D6F733E3C
            63656C6C6865616465726D656D6F733E3C546672784D656D6F56696577204C65
            66743D223130332220546F703D223438342C3838323135303934343838322220
            57696474683D2233372C373935323735353922204865696768743D2231352C31
            3138313130323422205265737472696374696F6E733D22382220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D382220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223432313037
            2220476170583D22332220476170593D2233222048416C69676E3D2268614365
            6E7465722220506172656E74466F6E743D2246616C7365222056416C69676E3D
            22766143656E7465722220546578743D2243C3B36469676F222F3E3C54667278
            4D656D6F56696577204C6566743D223134302C37393532373535392220546F70
            3D223438342C383832313530393434383832222057696474683D2233372C3739
            35323735353922204865696768743D2231352C31313831313032342220526573
            7472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D382220466F6E742E4E616D
            653D22417269616C2220466F6E742E5374796C653D223022204672616D652E43
            6F6C6F723D22313637373732313522204672616D652E5479703D223135222046
            696C6C2E4261636B436F6C6F723D2234323130372220476170583D2233222047
            6170593D2233222048416C69676E3D22686152696768742220506172656E7446
            6F6E743D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D22507265C3A76F222F3E3C546672784D656D6F56696577204C6566743D
            22302220546F703D2230222057696474683D223022204865696768743D223022
            205265737472696374696F6E733D22382220416C6C6F7745787072657373696F
            6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E74
            2E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D2234323130372220476170583D
            22332220476170593D22332220506172656E74466F6E743D2246616C73652220
            56416C69676E3D22766143656E7465722220546578743D22526564757A222F3E
            3C546672784D656D6F56696577204C6566743D22302220546F703D2230222057
            696474683D223022204865696768743D223022205265737472696374696F6E73
            3D22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2234323130372220476170583D22332220476170593D22332220
            506172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E
            7465722220546578743D2256414C5F50525A5F505244222F3E3C546672784D65
            6D6F56696577204C6566743D22302220546F703D2230222057696474683D2230
            22204865696768743D223022205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C736522204672616D652E547970
            3D2231352220476170583D22332220476170593D2233222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C546672784D656D6F56696577
            204C6566743D22302220546F703D2230222057696474683D2230222048656967
            68743D223022205265737472696374696F6E733D22382220416C6C6F77457870
            72657373696F6E733D2246616C736522204672616D652E5479703D2231352220
            476170583D22332220476170593D2233222056416C69676E3D22766143656E74
            65722220546578743D22222F3E3C546672784D656D6F56696577204C6566743D
            22302220546F703D2230222057696474683D223022204865696768743D223022
            205265737472696374696F6E733D22382220416C6C6F7745787072657373696F
            6E733D2246616C736522204672616D652E5479703D2231352220476170583D22
            332220476170593D2233222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C546672784D656D6F56696577204C6566743D22302220546F
            703D2230222057696474683D223022204865696768743D223022205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C736522204672616D652E5479703D2231352220476170583D22332220476170
            593D2233222056416C69676E3D22766143656E7465722220546578743D22222F
            3E3C2F63656C6C6865616465726D656D6F733E3C636F6C756D6E6D656D6F733E
            3C546672784D656D6F56696577204C6566743D223130332220546F703D223433
            392C353237383230323336323231222057696474683D2237352C353930353531
            313822204865696768743D2231352C3131383131303233363232303522205265
            737472696374696F6E733D2232342220416C6C6F7745787072657373696F6E73
            3D2246616C73652220466F6E742E436861727365743D22312220466F6E742E43
            6F6C6F723D22302220466F6E742E4865696768743D222D382220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223022204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D2234323130372220476170583D223322
            20476170593D2233222048416C69676E3D22686143656E746572222050617265
            6E74466F6E743D2246616C73652220576F7264577261703D2246616C73652220
            56416C69676E3D22766143656E7465722220546578743D22222F3E3C54667278
            4D656D6F56696577204C6566743D223130332220546F703D223435342C363435
            393330343732343431222057696474683D2237352C3539303535313138222048
            65696768743D2231352C31313831313032333632323035222052657374726963
            74696F6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D382220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2234323130372220476170583D2233222047617059
            3D2233222048416C69676E3D22686143656E7465722220506172656E74466F6E
            743D2246616C73652220576F7264577261703D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D22222F3E3C546672784D656D6F56
            696577204C6566743D223130332220546F703D223436392C3736343034303730
            38363632222057696474683D2237352C35393035353131382220486569676874
            3D2231352C3131383131303233363232303522205265737472696374696F6E73
            3D2232342220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D382220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2234323130372220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E7465722220506172656E74466F6E743D224661
            6C73652220576F7264577261703D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22222F3E3C2F636F6C756D6E6D656D6F733E3C
            636F6C756D6E746F74616C6D656D6F733E3C546672784D656D6F56696577204C
            6566743D223138332220546F703D223231222057696474683D22383122204865
            696768743D22343222205265737472696374696F6E733D223822205669736962
            6C653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D
            22417269616C2220466F6E742E5374796C653D223122204672616D652E436F6C
            6F723D22313637373732313522204672616D652E5479703D223135222046696C
            6C2E4261636B436F6C6F723D2234323130372220476170583D22332220476170
            593D2233222048416C69676E3D22686143656E7465722220506172656E74466F
            6E743D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D224772616E6420546F74616C222F3E3C546672784D656D6F56696577204C
            6566743D22302220546F703D2230222057696474683D22302220486569676874
            3D223022205265737472696374696F6E733D2238222056697369626C653D2246
            616C73652220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241726961
            6C2220466F6E742E5374796C653D223122204672616D652E436F6C6F723D2231
            3637373732313522204672616D652E5479703D223135222046696C6C2E426163
            6B436F6C6F723D2234323130372220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22546F
            74616C222F3E3C546672784D656D6F56696577204C6566743D223139362C3338
            3538323637372220546F703D223432222057696474683D223131372220486569
            6768743D22323222205265737472696374696F6E733D2238222056697369626C
            653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C73
            652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22
            302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223122204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2234323130372220476170583D2233222047617059
            3D2233222048416C69676E3D22686143656E7465722220506172656E74466F6E
            743D2246616C7365222056416C69676E3D22766143656E746572222054657874
            3D22546F74616C222F3E3C2F636F6C756D6E746F74616C6D656D6F733E3C636F
            726E65726D656D6F733E3C546672784D656D6F56696577204C6566743D223230
            2220546F703D223432342C3430393731222057696474683D2238332220486569
            6768743D2231352C313138313130323336323230352220526573747269637469
            6F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D382220466F6E742E4E616D653D22417269
            616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D22
            313637373732313522204672616D652E5479703D223135222046696C6C2E4261
            636B436F6C6F723D2234323130372220476170583D22332220476170593D2233
            2220506172656E74466F6E743D2246616C73652220576F7264577261703D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22222F
            3E3C546672784D656D6F56696577204C6566743D223130332220546F703D2234
            32342C3430393731222057696474683D2237352C353930353531313822204865
            696768743D2231352C3131383131303233363232303522205265737472696374
            696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C7365
            2220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230
            2220466F6E742E4865696768743D222D382220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2234323130372220476170583D22332220476170593D22
            332220506172656E74466F6E743D2246616C73652220576F7264577261703D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2222
            2F3E3C546672784D656D6F56696577204C6566743D2238332220546F703D2231
            38222057696474683D2231303122204865696768743D2235342C343235313936
            383422205265737472696374696F6E733D2238222056697369626C653D224661
            6C73652220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C
            2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D223136
            37373732313522204672616D652E5479703D223135222046696C6C2E4261636B
            436F6C6F723D2234323130372220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E7465722220506172656E74466F6E743D224661
            6C7365222056416C69676E3D22766143656E7465722220546578743D22222F3E
            3C546672784D656D6F56696577204C6566743D2232302220546F703D22343339
            2C353237383230323336323231222057696474683D2238332220486569676874
            3D2236302C3437323434303934383636313522205265737472696374696F6E73
            3D22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D382220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223022204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D2234323130372220476170583D22332220476170593D2233222050
            6172656E74466F6E743D2246616C73652220576F7264577261703D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D22436F72222F
            3E3C2F636F726E65726D656D6F733E3C726F776D656D6F733E3C546672784D65
            6D6F56696577204C6566743D2232302220546F703D223530302C303030323631
            313834383832222057696474683D22383322204865696768743D2231352C3131
            383131303233363232303522205265737472696374696F6E733D223234222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D382220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            34323130372220476170583D22332220476170593D22332220506172656E7446
            6F6E743D2246616C73652220576F7264577261703D2246616C7365222056416C
            69676E3D22766143656E7465722220546578743D22222F3E3C2F726F776D656D
            6F733E3C726F77746F74616C6D656D6F733E3C546672784D656D6F5669657720
            4C6566743D22302220546F703D223834222057696474683D2231303122204865
            696768743D22323222205265737472696374696F6E733D223822205669736962
            6C653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D
            22417269616C2220466F6E742E5374796C653D223122204672616D652E436F6C
            6F723D22313637373732313522204672616D652E5479703D223135222046696C
            6C2E4261636B436F6C6F723D2234323130372220476170583D22332220476170
            593D2233222048416C69676E3D22686143656E7465722220506172656E74466F
            6E743D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D224772616E6420546F74616C222F3E3C2F726F77746F74616C6D656D6F73
            3E3C63656C6C66756E6374696F6E733E3C6974656D20302F3E3C6974656D2030
            2F3E3C2F63656C6C66756E6374696F6E733E3C636F6C756D6E736F72743E3C69
            74656D20322F3E3C6974656D20322F3E3C6974656D20322F3E3C2F636F6C756D
            6E736F72743E3C726F77736F72743E3C6974656D20322F3E3C2F726F77736F72
            743E3C2F63726F73733E}
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 597.165740000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 529.134200000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrTabPrcGru: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrTabPrcGruAfterScroll
    OnCalcFields = QrTabPrcGruCalcFields
    SQL.Strings = (
      'SELECT gg1.Nome NOME1, gg1.CodUsu, gg1.GraTamCad, tpg.*'
      'FROM tabeprcgrg tpg'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpg.Nivel1'
      'WHERE Codigo=:P0'
      ''
      '')
    Left = 784
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTabPrcGruKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
    object QrTabPrcGruNOME1: TWideStringField
      FieldName = 'NOME1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrTabPrcGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru1.CodUsu'
      Required = True
    end
    object QrTabPrcGruGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Origin = 'gragru1.GraTamCad'
    end
    object QrTabPrcGruCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tabeprcgrg.Codigo'
      Required = True
    end
    object QrTabPrcGruNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'tabeprcgrg.Nivel1'
      Required = True
    end
    object QrTabPrcGruPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'tabeprcgrg.Preco'
      Required = True
    end
  end
  object frxPrcGruSem: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39867.737163726910000000
    ReportOptions.LastChange = 39867.737163726910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPrcGruC_LandGetValue
    Left = 852
    Top = 84
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = frxDsTabePrcCab
        DataSetName = 'frxDsTabePrcCab'
      end
      item
        DataSet = frxDsTabePrcPzG
        DataSetName = 'frxDsTabePrcPzG'
      end
      item
        DataSet = frxDsTabPrcGru
        DataSetName = 'frxDsTabPrcGru'
      end
      item
        DataSet = frxDsTabPrcPrz
        DataSetName = 'frxDsTabPrcPrz'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 702.992580000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo3: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista [frxDsTabePrcCab."Sigla"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 41.574830000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTabePrcCab."Nome"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 64.252010000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        Child = frxPrcGruSem.Child2
        DataSet = frxDsTabPrcGru
        DataSetName = 'frxDsTabPrcGru'
        RowCount = 0
      end
      object Child1: TfrxChild
        FillType = ftBrush
        Height = 73.259842520000000000
        Top = 211.653680000000000000
        Width = 718.110700000000000000
        object DBCross1: TfrxDBCrossView
          Width = 395.275590540000000000
          Height = 73.629921260000000000
          AutoSize = False
          DownThenAcross = False
          JoinEqualCells = True
          RowLevels = 3
          ShowColumnTotal = False
          ShowRowTotal = False
          ShowTitle = False
          CellFields.Strings = (
            'Variacao')
          ColumnFields.Strings = (
            'Nome')
          DataSet = frxDsTabePrcPzG
          DataSetName = 'frxDsTabePrcPzG'
          RowFields.Strings = (
            'COL_TXT'
            'COL_TXT'
            'COL_TXT')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D223331342C
            383033313439362220546F703D223234382C3635333638222057696474683D22
            36302C343732343430393422204865696768743D2231362C3632393932313236
            22205265737472696374696F6E733D2232342220416C6C6F7745787072657373
            696F6E733D2246616C73652220446973706C6179466F726D61742E466F726D61
            745374723D2225322E326E2220446973706C6179466F726D61742E4B696E643D
            22666B4E756D657269632220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31312220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223130323138343935222047
            6170583D22332220476170593D2233222048416C69676E3D22686143656E7465
            722220506172656E74466F6E743D2246616C73652220576F7264577261703D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2230
            2C3030222F3E3C546672784D656D6F56696577204C6566743D223339392C3036
            3239393231322220546F703D2236372C3934343838313839222057696474683D
            2236382C303331343936303622204865696768743D2232322220526573747269
            6374696F6E733D2232342220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223022204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D2231303231383439352220476170583D223322
            20476170593D2233222048416C69676E3D22686152696768742220506172656E
            74466F6E743D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D22302C3030222F3E3C546672784D656D6F56696577204C6566743D
            223437332C30363239393231322220546F703D2236372C393434383831383922
            2057696474683D2236382C303331343936303622204865696768743D22323222
            205265737472696374696F6E733D2232342220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223130323138343935222047
            6170583D22332220476170593D2233222048416C69676E3D2268615269676874
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22302C3030222F3E3C546672784D656D6F566965
            77204C6566743D22302220546F703D2230222057696474683D22302220486569
            6768743D223022205265737472696374696F6E733D22382220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223130323138
            3439352220476170583D22332220476170593D2233222048416C69676E3D2268
            6152696768742220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D22222F3E3C546672784D656D6F56
            696577204C6566743D22302220546F703D2230222057696474683D2230222048
            65696768743D223022205265737472696374696F6E733D22382220416C6C6F77
            45787072657373696F6E733D2246616C73652220466F6E742E43686172736574
            3D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D
            222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E537479
            6C653D223022204672616D652E436F6C6F723D22313637373732313522204672
            616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22313032
            31383439352220476170583D22332220476170593D2233222048416C69676E3D
            22686152696768742220506172656E74466F6E743D2246616C7365222056416C
            69676E3D22766143656E7465722220546578743D22222F3E3C546672784D656D
            6F56696577204C6566743D22302220546F703D2230222057696474683D223022
            204865696768743D223022205265737472696374696F6E733D22382220416C6C
            6F7745787072657373696F6E733D2246616C73652220466F6E742E4368617273
            65743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768
            743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2231
            303231383439352220476170583D22332220476170593D2233222048416C6967
            6E3D22686152696768742220506172656E74466F6E743D2246616C7365222056
            416C69676E3D22766143656E7465722220546578743D22222F3E3C546672784D
            656D6F56696577204C6566743D22302220546F703D2230222057696474683D22
            3022204865696768743D223022205265737472696374696F6E733D2238222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2231303231383439352220476170583D22332220476170593D2233222048416C
            69676E3D22686152696768742220506172656E74466F6E743D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C546672
            784D656D6F56696577204C6566743D22302220546F703D223022205769647468
            3D223022204865696768743D223022205265737472696374696F6E733D223822
            20416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43
            6861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48
            65696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F
            6E742E5374796C653D223022204672616D652E436F6C6F723D22313637373732
            313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F
            723D2231303231383439352220476170583D22332220476170593D2233222048
            416C69676E3D22686152696768742220506172656E74466F6E743D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D22222F3E3C2F
            63656C6C6D656D6F733E3C63656C6C6865616465726D656D6F733E3C54667278
            4D656D6F56696577204C6566743D22302220546F703D2230222057696474683D
            223022204865696768743D223022205265737472696374696F6E733D22382220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E
            742E5374796C653D223022204672616D652E436F6C6F723D2231363737373231
            3522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F72
            3D22343634333538332220476170583D22332220476170593D22332220506172
            656E74466F6E743D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D22566172696163616F222F3E3C546672784D656D6F56696577
            204C6566743D22302220546F703D2230222057696474683D2230222048656967
            68743D223022205265737472696374696F6E733D22382220416C6C6F77457870
            72657373696F6E733D2246616C73652220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31
            332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223022204672616D652E436F6C6F723D22313637373732313522204672616D65
            2E5479703D223135222046696C6C2E4261636B436F6C6F723D22343634333538
            332220476170583D22332220476170593D22332220506172656E74466F6E743D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            566172696163616F222F3E3C546672784D656D6F56696577204C6566743D2230
            2220546F703D2230222057696474683D223022204865696768743D2230222052
            65737472696374696F6E733D22382220416C6C6F7745787072657373696F6E73
            3D2246616C73652220466F6E742E436861727365743D22312220466F6E742E43
            6F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E
            4E616D653D22417269616C2220466F6E742E5374796C653D223022204672616D
            652E436F6C6F723D22313637373732313522204672616D652E5479703D223135
            222046696C6C2E4261636B436F6C6F723D22343634333538332220476170583D
            22332220476170593D22332220506172656E74466F6E743D2246616C73652220
            56416C69676E3D22766143656E7465722220546578743D22566172696163616F
            222F3E3C546672784D656D6F56696577204C6566743D22302220546F703D2230
            222057696474683D223022204865696768743D22302220526573747269637469
            6F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D22343634333538332220476170583D2233222047617059
            3D22332220506172656E74466F6E743D2246616C7365222056416C69676E3D22
            766143656E7465722220546578743D22566172696163616F222F3E3C2F63656C
            6C6865616465726D656D6F733E3C636F6C756D6E6D656D6F733E3C546672784D
            656D6F56696577204C6566743D223331342C383033313439362220546F703D22
            3233312C3635333638222057696474683D2236302C3437323434303934222048
            65696768743D22313722205265737472696374696F6E733D2232342220416C6C
            6F7745787072657373696F6E733D2246616C73652220466F6E742E4368617273
            65743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768
            743D222D31312220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2234
            3634333538332220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E7465722220506172656E74466F6E743D2246616C7365222057
            6F7264577261703D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D22222F3E3C2F636F6C756D6E6D656D6F733E3C636F6C756D6E
            746F74616C6D656D6F733E3C546672784D656D6F56696577204C6566743D2235
            302220546F703D223231222057696474683D22383122204865696768743D2232
            3222205265737472696374696F6E733D2238222056697369626C653D2246616C
            73652220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223122204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D22343634333538332220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D224772
            616E6420546F74616C222F3E3C2F636F6C756D6E746F74616C6D656D6F733E3C
            636F726E65726D656D6F733E3C546672784D656D6F56696577204C6566743D22
            32302220546F703D223233312C3635333638222057696474683D223239342C38
            30333134393622204865696768743D223022205265737472696374696F6E733D
            2238222056697369626C653D2246616C73652220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2234363433353833222047
            6170583D22332220476170593D2233222048416C69676E3D22686143656E7465
            722220506172656E74466F6E743D2246616C7365222056416C69676E3D227661
            43656E7465722220546578743D22566172696163616F222F3E3C546672784D65
            6D6F56696577204C6566743D223331342C383033313439362220546F703D2232
            33312C3635333638222057696474683D2236302C343732343430393422204865
            696768743D223022205265737472696374696F6E733D2238222056697369626C
            653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C73
            652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22
            302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D22343634333538332220476170583D223322204761
            70593D2233222048416C69676E3D22686143656E7465722220506172656E7446
            6F6E743D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D2225222F3E3C546672784D656D6F56696577204C6566743D2230222054
            6F703D2230222057696474683D223022204865696768743D2230222052657374
            72696374696F6E733D2238222056697369626C653D2246616C73652220416C6C
            6F7745787072657373696F6E733D2246616C73652220466F6E742E4368617273
            65743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768
            743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2234
            3634333538332220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E7465722220506172656E74466F6E743D2246616C7365222056
            416C69676E3D22766143656E7465722220546578743D22222F3E3C546672784D
            656D6F56696577204C6566743D2232302220546F703D223233312C3635333638
            222057696474683D223137332C383538323637373222204865696768743D2231
            3722205265737472696374696F6E733D22382220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3131222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2234363433353833222047
            6170583D22332220476170593D22332220506172656E74466F6E743D2246616C
            73652220576F7264577261703D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D224E6F6D6520646F20677275706F206465207072
            6F6475746F73222F3E3C546672784D656D6F56696577204C6566743D22313933
            2C38353832363737322220546F703D223233312C363533363822205769647468
            3D2236302C343732343430393422204865696768743D22313722205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31312220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223022204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D22343634333538332220476170583D22332220
            476170593D2233222048416C69676E3D22686143656E7465722220506172656E
            74466F6E743D2246616C73652220576F7264577261703D2246616C7365222056
            416C69676E3D22766143656E7465722220546578743D2243C3B36469676F222F
            3E3C546672784D656D6F56696577204C6566743D223235342C33333037303836
            362220546F703D223233312C3635333638222057696474683D2236302C343732
            343430393422204865696768743D22313722205265737472696374696F6E733D
            22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D31312220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223022204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D22343634333538332220476170583D22332220476170593D223322
            2048416C69676E3D22686143656E7465722220506172656E74466F6E743D2246
            616C73652220576F7264577261703D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22507265C3A76F222F3E3C2F636F726E6572
            6D656D6F733E3C726F776D656D6F733E3C546672784D656D6F56696577204C65
            66743D2232302220546F703D223234382C3635333638222057696474683D2231
            37332C383538323637373222204865696768743D2231362C3632393932313236
            22205265737472696374696F6E733D2232342220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3131222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2234363433353833222047
            6170583D22332220476170593D22332220506172656E74466F6E743D2246616C
            73652220576F7264577261703D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C65
            66743D223139332C38353832363737322220546F703D223234382C3635333638
            222057696474683D2236302C343732343430393422204865696768743D223136
            2C363239393231323622205265737472696374696F6E733D2232342220416C6C
            6F7745787072657373696F6E733D2246616C73652220466F6E742E4368617273
            65743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768
            743D222D31312220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2234
            3634333538332220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E7465722220506172656E74466F6E743D2246616C7365222057
            6F7264577261703D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D22222F3E3C546672784D656D6F56696577204C6566743D2232
            35342C33333037303836362220546F703D223234382C36353336382220576964
            74683D2236302C343732343430393422204865696768743D2231362C36323939
            3231323622205265737472696374696F6E733D2232342220416C6C6F77457870
            72657373696F6E733D2246616C73652220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31
            312220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223022204672616D652E436F6C6F723D22313637373732313522204672616D65
            2E5479703D223135222046696C6C2E4261636B436F6C6F723D22343634333538
            332220476170583D22332220476170593D2233222048416C69676E3D22686143
            656E7465722220506172656E74466F6E743D2246616C73652220576F72645772
            61703D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D22222F3E3C2F726F776D656D6F733E3C726F77746F74616C6D656D6F733E
            3C546672784D656D6F56696577204C6566743D22302220546F703D2230222057
            696474683D223022204865696768743D223022205265737472696374696F6E73
            3D2238222056697369626C653D2246616C73652220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223122
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22343634333538332220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D224772616E6420546F74616C222F3E3C5466
            72784D656D6F56696577204C6566743D223230302220546F703D223436222057
            696474683D22353422204865696768743D22323222205265737472696374696F
            6E733D2238222056697369626C653D2246616C73652220416C6C6F7745787072
            657373696F6E733D2246616C73652220466F6E742E436861727365743D223122
            20466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133
            2220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22
            3122204672616D652E436F6C6F723D22313637373732313522204672616D652E
            5479703D223135222046696C6C2E4261636B436F6C6F723D2234363433353833
            2220476170583D22332220476170593D2233222048416C69676E3D2268614365
            6E7465722220506172656E74466F6E743D2246616C7365222056416C69676E3D
            22766143656E7465722220546578743D22546F74616C222F3E3C546672784D65
            6D6F56696577204C6566743D223236382C303331343936303632393932222054
            6F703D223638222057696474683D22353422204865696768743D223232222052
            65737472696374696F6E733D2238222056697369626C653D2246616C73652220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E
            742E5374796C653D223122204672616D652E436F6C6F723D2231363737373231
            3522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F72
            3D22343634333538332220476170583D22332220476170593D2233222048416C
            69676E3D22686143656E7465722220506172656E74466F6E743D2246616C7365
            222056416C69676E3D22766143656E7465722220546578743D22546F74616C22
            2F3E3C2F726F77746F74616C6D656D6F733E3C63656C6C66756E6374696F6E73
            3E3C6974656D20312F3E3C2F63656C6C66756E6374696F6E733E3C636F6C756D
            6E736F72743E3C6974656D20302F3E3C2F636F6C756D6E736F72743E3C726F77
            736F72743E3C6974656D20302F3E3C6974656D20302F3E3C6974656D20302F3E
            3C2F726F77736F72743E3C2F63726F73733E}
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        Child = frxPrcGruSem.Child1
        RowCount = 1
        object Memo13: TfrxMemoView
          Width = 718.110700000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = -2147483640
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clWhite
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 52479
          GapX = 10.000000000000000000
          Memo.UTF8W = (
            'Tabela de Varia'#231#227'o do pre'#231'o em %')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child2: TfrxChild
        FillType = ftBrush
        Height = 56.629921260000000000
        Top = 328.819110000000000000
        Width = 718.110700000000000000
        object DBCross3: TfrxDBCrossView
          Width = 534.803149609763800000
          Height = 65.000000000000000000
          AutoSize = False
          DownThenAcross = False
          JoinEqualCells = True
          RowLevels = 3
          ShowColumnHeader = False
          ShowColumnTotal = False
          ShowRowTotal = False
          ShowTitle = False
          CellFields.Strings = (
            'PRC_PRZ')
          ColumnFields.Strings = (
            'NOMEPRZ')
          DataSet = frxDsTabPrcPrz
          DataSetName = 'frxDsTabPrcPrz'
          RowFields.Strings = (
            'NOME1'
            'CodUsu'
            'Preco')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D223331342C
            3830333134393630393736342220546F703D223334382C383139313122205769
            6474683D2232303022204865696768743D2231362C3632393932313236222052
            65737472696374696F6E733D2232342220416C6C6F7745787072657373696F6E
            733D2246616C73652220446973706C6179466F726D61742E446563696D616C53
            6570617261746F723D222C2220446973706C6179466F726D61742E466F726D61
            745374723D2225322E326E2220446973706C6179466F726D61742E4B696E643D
            22666B4E756D657269632220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31312220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223136373030333436222047
            6170583D22332220476170593D2233222048416C69676E3D22686143656E7465
            722220506172656E74466F6E743D2246616C73652220576F7264577261703D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2230
            2C3030222F3E3C546672784D656D6F56696577205461673D223122204C656674
            3D223333302C3033313439363036323939322220546F703D2232312220576964
            74683D2236382C3033313439363036323939323122204865696768743D223232
            22205265737472696374696F6E733D2232342220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2231363730303334362220
            476170583D22332220476170593D2233222048416C69676E3D22686152696768
            742220506172656E74466F6E743D2246616C736522205374796C653D2263656C
            6C222056416C69676E3D22766143656E7465722220546578743D22302C303022
            2F3E3C546672784D656D6F56696577205461673D223222204C6566743D223230
            352220546F703D223433222057696474683D22383122204865696768743D2232
            3122205265737472696374696F6E733D2232342220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223022
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22313637303033343622
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            68742220506172656E74466F6E743D2246616C736522205374796C653D226365
            6C6C222056416C69676E3D22766143656E7465722220546578743D2252242030
            2C3030222F3E3C546672784D656D6F56696577205461673D223322204C656674
            3D223230352220546F703D223634222057696474683D22383122204865696768
            743D22323222205265737472696374696F6E733D2232342220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223136373030
            3334362220476170583D22332220476170593D2233222048416C69676E3D2268
            6152696768742220506172656E74466F6E743D2246616C736522205374796C65
            3D2263656C6C222056416C69676E3D22766143656E7465722220546578743D22
            522420302C3030222F3E3C546672784D656D6F56696577204C6566743D223022
            20546F703D2230222057696474683D223022204865696768743D223022205265
            737472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D
            2246616C73652220466F6E742E436861727365743D22312220466F6E742E436F
            6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223022204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D2231363730303334362220476170583D
            22332220476170593D2233222048416C69676E3D226861526967687422205061
            72656E74466F6E743D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C546672784D656D6F56696577204C6566743D22
            302220546F703D2230222057696474683D223022204865696768743D22302220
            5265737472696374696F6E733D22382220416C6C6F7745787072657373696F6E
            733D2246616C73652220466F6E742E436861727365743D22312220466F6E742E
            436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E74
            2E4E616D653D22417269616C2220466F6E742E5374796C653D22302220467261
            6D652E436F6C6F723D22313637373732313522204672616D652E5479703D2231
            35222046696C6C2E4261636B436F6C6F723D2231363730303334362220476170
            583D22332220476170593D2233222048416C69676E3D22686152696768742220
            506172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E
            7465722220546578743D22222F3E3C546672784D656D6F56696577204C656674
            3D22302220546F703D2230222057696474683D223022204865696768743D2230
            22205265737472696374696F6E733D22382220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223136373030333436222047
            6170583D22332220476170593D2233222048416C69676E3D2268615269676874
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C65
            66743D22302220546F703D2230222057696474683D223022204865696768743D
            223022205265737472696374696F6E733D22382220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223022
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22313637303033343622
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            68742220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22222F3E3C2F63656C6C6D656D6F733E3C63
            656C6C6865616465726D656D6F733E3C546672784D656D6F5669657720546167
            3D2236303022204C6566743D22302220546F703D2230222057696474683D2230
            22204865696768743D223022205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            31363632393134332220476170583D22332220476170593D2233222050617265
            6E74466F6E743D2246616C736522205374796C653D2263656C6C686561646572
            222056416C69676E3D22766143656E7465722220546578743D225052435F5052
            5A222F3E3C546672784D656D6F56696577205461673D2236303122204C656674
            3D22302220546F703D2230222057696474683D223022204865696768743D2230
            22205265737472696374696F6E733D22382220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223136363239313433222047
            6170583D22332220476170593D22332220506172656E74466F6E743D2246616C
            736522205374796C653D2263656C6C686561646572222056416C69676E3D2276
            6143656E7465722220546578743D225052435F50525A222F3E3C546672784D65
            6D6F56696577204C6566743D22302220546F703D2230222057696474683D2230
            22204865696768743D223022205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            31363632393134332220476170583D22332220476170593D2233222050617265
            6E74466F6E743D2246616C7365222056416C69676E3D22766143656E74657222
            20546578743D225052435F50525A222F3E3C546672784D656D6F56696577204C
            6566743D22302220546F703D2230222057696474683D22302220486569676874
            3D223022205265737472696374696F6E733D22382220416C6C6F774578707265
            7373696F6E733D2246616C73652220466F6E742E436861727365743D22312220
            466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D313322
            20466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230
            22204672616D652E436F6C6F723D22313637373732313522204672616D652E54
            79703D223135222046696C6C2E4261636B436F6C6F723D223136363239313433
            2220476170583D22332220476170593D22332220506172656E74466F6E743D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2250
            52435F50525A222F3E3C2F63656C6C6865616465726D656D6F733E3C636F6C75
            6D6E6D656D6F733E3C546672784D656D6F56696577205461673D223130302220
            4C6566743D223331342C3830333134393630393736342220546F703D22333438
            2C3831393131222057696474683D2232303022204865696768743D2232352220
            5265737472696374696F6E733D2232342220416C6C6F7745787072657373696F
            6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E74
            2E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D22313636323931343322204761
            70583D22332220476170593D2233222048416C69676E3D22686143656E746572
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22222F3E3C2F636F6C756D6E6D656D6F733E3C63
            6F6C756D6E746F74616C6D656D6F733E3C546672784D656D6F56696577205461
            673D2233303022204C6566743D223230352220546F703D223231222057696474
            683D22383122204865696768743D22323222205265737472696374696F6E733D
            2238222056697369626C653D2246616C73652220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22312220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2231363632393134332220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C736522205374796C653D22636F
            6C6772616E64222056416C69676E3D22766143656E7465722220546578743D22
            4772616E6420546F74616C222F3E3C2F636F6C756D6E746F74616C6D656D6F73
            3E3C636F726E65726D656D6F733E3C546672784D656D6F56696577205461673D
            2235303022204C6566743D22302220546F703D2230222057696474683D223230
            3022204865696768743D223022205265737472696374696F6E733D2238222056
            697369626C653D2246616C73652220416C6C6F7745787072657373696F6E733D
            2246616C73652220446973706C6179466F726D61742E446563696D616C536570
            617261746F723D222C2220466F6E742E436861727365743D22312220466F6E74
            2E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D22313636323931343322204761
            70583D22332220476170593D2233222048416C69676E3D22686143656E746572
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22477275706F732064652050726F6475746F7322
            2F3E3C546672784D656D6F56696577205461673D2235303122204C6566743D22
            3331342C3830333134393630393736342220546F703D223334382C3831393131
            222057696474683D2232303022204865696768743D2230222052657374726963
            74696F6E733D2238222056697369626C653D2246616C73652220416C6C6F7745
            787072657373696F6E733D2246616C73652220446973706C6179466F726D6174
            2E446563696D616C536570617261746F723D222C2220466F6E742E4368617273
            65743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768
            743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2231
            363632393134332220476170583D22332220476170593D2233222048416C6967
            6E3D22686143656E7465722220506172656E74466F6E743D2246616C73652220
            56416C69676E3D22766143656E7465722220546578743D225072617A6F222F3E
            3C546672784D656D6F56696577205461673D2235303222204C6566743D223022
            20546F703D2230222057696474683D223022204865696768743D223022205265
            737472696374696F6E733D2238222056697369626C653D2246616C7365222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2231363632393134332220476170583D22332220476170593D2233222048416C
            69676E3D22686143656E7465722220506172656E74466F6E743D2246616C7365
            22205374796C653D22636F726E6572222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C546672784D656D6F56696577205461673D2235
            303322204C6566743D22302220546F703D2230222057696474683D223137332C
            38353832363737313635333522204865696768743D2232352220526573747269
            6374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D
            22417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C
            6F723D22313637373732313522204672616D652E5479703D223135222046696C
            6C2E4261636B436F6C6F723D2231363632393134332220476170583D22332220
            476170593D22332220506172656E74466F6E743D2246616C7365222056416C69
            676E3D22766143656E7465722220546578743D224E4F4D4531222F3E3C546672
            784D656D6F56696577204C6566743D223137332C383538323637373136353335
            2220546F703D2230222057696474683D2236302C343732343430393434383831
            3922204865696768743D22323522205265737472696374696F6E733D22382220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E
            742E5374796C653D223022204672616D652E436F6C6F723D2231363737373231
            3522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F72
            3D2231363632393134332220476170583D22332220476170593D223322204841
            6C69676E3D22686143656E7465722220506172656E74466F6E743D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D22436F645573
            75222F3E3C546672784D656D6F56696577204C6566743D223233342C33333037
            30383636313431372220546F703D2230222057696474683D2236302C34373234
            34303934343838313922204865696768743D2232352220526573747269637469
            6F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2231363632393134332220476170583D22332220476170
            593D2233222048416C69676E3D22686143656E7465722220506172656E74466F
            6E743D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D22507265636F222F3E3C2F636F726E65726D656D6F733E3C726F776D656D
            6F733E3C546672784D656D6F56696577205461673D2232303022204C6566743D
            2232302220546F703D223334382C3831393131222057696474683D223137332C
            383538323637373222204865696768743D2231362C3632393932313236222052
            65737472696374696F6E733D2232342220416C6C6F7745787072657373696F6E
            733D2246616C73652220466F6E742E436861727365743D22312220466F6E742E
            436F6C6F723D22302220466F6E742E4865696768743D222D31312220466F6E74
            2E4E616D653D22417269616C2220466F6E742E5374796C653D22302220467261
            6D652E436F6C6F723D22313637373732313522204672616D652E5479703D2231
            35222046696C6C2E4261636B436F6C6F723D2231363632393134332220476170
            583D22332220476170593D22332220506172656E74466F6E743D2246616C7365
            2220576F7264577261703D2246616C7365222056416C69676E3D22766143656E
            7465722220546578743D22222F3E3C546672784D656D6F56696577204C656674
            3D223139332C38353832363737322220546F703D223334382C38313931312220
            57696474683D2236302C3437323434303934343838313922204865696768743D
            2231362C363239393231323622205265737472696374696F6E733D2232342220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D31312220466F6E742E4E616D653D22417269616C2220466F6E
            742E5374796C653D223022204672616D652E436F6C6F723D2231363737373231
            3522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F72
            3D2231363632393134332220476170583D22332220476170593D223322204841
            6C69676E3D22686143656E7465722220506172656E74466F6E743D2246616C73
            652220576F7264577261703D2246616C7365222056416C69676E3D2276614365
            6E7465722220546578743D22222F3E3C546672784D656D6F56696577204C6566
            743D223235342C3333303730383636343838322220546F703D223334382C3831
            393131222057696474683D2236302C3437323434303934343838313922204865
            696768743D2231362C363239393231323622205265737472696374696F6E733D
            2232342220416C6C6F7745787072657373696F6E733D2246616C736522204469
            73706C6179466F726D61742E466F726D61745374723D2225322E326E22204469
            73706C6179466F726D61742E4B696E643D22666B4E756D657269632220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D31312220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223022204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D2231363632393134332220476170583D22332220476170593D2233
            222048416C69676E3D22686143656E7465722220506172656E74466F6E743D22
            46616C73652220576F7264577261703D2246616C7365222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C2F726F776D656D6F733E3C72
            6F77746F74616C6D656D6F733E3C546672784D656D6F56696577205461673D22
            34303022204C6566743D22302220546F703D223634222057696474683D223132
            3522204865696768743D22323222205265737472696374696F6E733D22382220
            56697369626C653D2246616C73652220416C6C6F7745787072657373696F6E73
            3D2246616C73652220466F6E742E436861727365743D22312220466F6E742E43
            6F6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E
            4E616D653D22417269616C2220466F6E742E5374796C653D223122204672616D
            652E436F6C6F723D22313637373732313522204672616D652E5479703D223135
            222046696C6C2E4261636B436F6C6F723D223136363239313433222047617058
            3D22332220476170593D2233222048416C69676E3D22686143656E7465722220
            506172656E74466F6E743D2246616C736522205374796C653D22726F77677261
            6E64222056416C69676E3D22766143656E7465722220546578743D224772616E
            6420546F74616C222F3E3C546672784D656D6F56696577204C6566743D223230
            302220546F703D223436222057696474683D22343922204865696768743D2232
            3222205265737472696374696F6E733D2238222056697369626C653D2246616C
            73652220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223122204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D2231363632393134332220476170583D22332220476170593D2233
            222048416C69676E3D22686143656E7465722220506172656E74466F6E743D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2254
            6F74616C222F3E3C546672784D656D6F56696577204C6566743D223236382C30
            33313439363036323939322220546F703D223231222057696474683D22363222
            204865696768743D22323222205265737472696374696F6E733D223822205669
            7369626C653D2246616C73652220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E61
            6D653D22417269616C2220466F6E742E5374796C653D223122204672616D652E
            436F6C6F723D22313637373732313522204672616D652E5479703D2231352220
            46696C6C2E4261636B436F6C6F723D2231363632393134332220476170583D22
            332220476170593D2233222048416C69676E3D22686143656E74657222205061
            72656E74466F6E743D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D22546F74616C222F3E3C2F726F77746F74616C6D656D6F73
            3E3C63656C6C66756E6374696F6E733E3C6974656D20312F3E3C2F63656C6C66
            756E6374696F6E733E3C636F6C756D6E736F72743E3C6974656D20302F3E3C2F
            636F6C756D6E736F72743E3C726F77736F72743E3C6974656D20302F3E3C6974
            656D20302F3E3C6974656D20302F3E3C2F726F77736F72743E3C2F63726F7373
            3E}
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 445.984540000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 529.134200000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPrcPrzIts: TfrxDBDataset
    UserName = 'frxDsPrcPrzIts'
    CloseDataSource = False
    DataSet = QrPrcPrzIts
    BCDToCurrency = False
    Left = 924
    Top = 224
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 20
    object GruposComGrades1: TMenuItem
      Caption = 'Grupos &Com Grades'
      object Retrato1: TMenuItem
        Caption = '&Retrato'
        OnClick = Retrato1Click
      end
      object Paisagem1: TMenuItem
        Caption = '&Paisagem'
        OnClick = Paisagem1Click
      end
    end
    object GruposSemGrades1: TMenuItem
      Caption = 'Grupos &Sem Grades'
      OnClick = GruposSemGrades1Click
    end
    object Reduzido1: TMenuItem
      Caption = '&Reduzido'
      OnClick = Reduzido1Click
    end
  end
  object PMProdutos: TPopupMenu
    Left = 680
    Top = 380
    object Limpapreosdesnecessrios1: TMenuItem
      Caption = '&Limpa pre'#231'os desnecess'#225'rios'
      OnClick = Limpapreosdesnecessrios1Click
    end
  end
  object frxPrcGruC_Land: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39867.737163726910000000
    ReportOptions.LastChange = 39867.737163726910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Child3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child3.Visible := <VARF_TEM_GRADE>;                           ' +
        '                 '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxPrcGruC_LandGetValue
    Left = 928
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = frxDsPrcPrzIts
        DataSetName = 'frxDsPrcPrzIts'
      end
      item
        DataSet = frxDsTabePrcCab
        DataSetName = 'frxDsTabePrcCab'
      end
      item
        DataSet = frxDsTabePrcPzG
        DataSetName = 'frxDsTabePrcPzG'
      end
      item
        DataSet = frxDsTabPrcGru
        DataSetName = 'frxDsTabPrcGru'
      end
      item
        DataSet = frxDsTabPrcPrz
        DataSetName = 'frxDsTabPrcPrz'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 309.921460000000000000
        Width = 1046.929810000000000000
        object Shape1: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo3: TfrxMemoView
          Left = 196.535560000000000000
          Top = 18.897650000000000000
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista [frxDsTabePrcCab."Sigla"] - [frxDsTabePrcCab."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 857.953310000000000000
          Top = 18.897650000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Top = 185.196970000000000000
        Width = 1046.929810000000000000
        Child = frxPrcGruC_Land.Child2
        DataSet = frxDsTabPrcGru
        DataSetName = 'frxDsTabPrcGru'
        RowCount = 0
      end
      object Child1: TfrxChild
        FillType = ftBrush
        Height = 64.188976380000000000
        Top = 98.267780000000000000
        Width = 1046.929810000000000000
        object DBCross1: TfrxDBCrossView
          Width = 244.094488188976700000
          Height = 64.188976380000010000
          AutoSize = False
          DownThenAcross = False
          JoinEqualCells = True
          KeepTogether = True
          RowLevels = 3
          ShowColumnTotal = False
          ShowRowTotal = False
          ShowTitle = False
          CellFields.Strings = (
            'Variacao')
          ColumnFields.Strings = (
            'Nome')
          DataSet = frxDsTabePrcPzG
          DataSetName = 'frxDsTabePrcPzG'
          RowFields.Strings = (
            'COL_TXT'
            'COL_TXT'
            'COL_TXT')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D223139332C
            3835383236373731363533362220546F703D223133302C333632323638313922
            2057696474683D2233302C323336323230343732343430392220486569676874
            3D2231322C303934343838313922205265737472696374696F6E733D22323422
            20416C6C6F7745787072657373696F6E733D2246616C73652220446973706C61
            79466F726D61742E466F726D61745374723D2225322E326E2220446973706C61
            79466F726D61742E4B696E643D22666B4E756D657269632220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D372220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2231303231383439352220476170583D22332220476170593D2233222048416C
            69676E3D22686152696768742220506172656E74466F6E743D2246616C736522
            20576F7264577261703D2246616C7365222056416C69676E3D22766143656E74
            65722220546578743D22302C3030222F3E3C546672784D656D6F56696577204C
            6566743D223339392C30363239393231322220546F703D2236372C3934343838
            313839222057696474683D2236382C303331343936303622204865696768743D
            22323222205265737472696374696F6E733D2232342220416C6C6F7745787072
            657373696F6E733D2246616C73652220466F6E742E436861727365743D223122
            20466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133
            2220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22
            3022204672616D652E436F6C6F723D22313637373732313522204672616D652E
            5479703D223135222046696C6C2E4261636B436F6C6F723D2231303231383439
            352220476170583D22332220476170593D2233222048416C69676E3D22686152
            696768742220506172656E74466F6E743D2246616C7365222056416C69676E3D
            22766143656E7465722220546578743D22302C3030222F3E3C546672784D656D
            6F56696577204C6566743D223437332C30363239393231322220546F703D2236
            372C3934343838313839222057696474683D2236382C30333134393630362220
            4865696768743D22323222205265737472696374696F6E733D2232342220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            31303231383439352220476170583D22332220476170593D2233222048416C69
            676E3D22686152696768742220506172656E74466F6E743D2246616C73652220
            56416C69676E3D22766143656E7465722220546578743D22302C3030222F3E3C
            546672784D656D6F56696577204C6566743D22302220546F703D223022205769
            6474683D223022204865696768743D223022205265737472696374696F6E733D
            22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223022204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D2231303231383439352220476170583D22332220476170593D2233
            222048416C69676E3D22686152696768742220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22222F
            3E3C546672784D656D6F56696577204C6566743D22302220546F703D22302220
            57696474683D223022204865696768743D223022205265737472696374696F6E
            733D22382220416C6C6F7745787072657373696F6E733D2246616C7365222046
            6F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230222046
            6F6E742E4865696768743D222D31332220466F6E742E4E616D653D2241726961
            6C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231
            3637373732313522204672616D652E5479703D223135222046696C6C2E426163
            6B436F6C6F723D2231303231383439352220476170583D22332220476170593D
            2233222048416C69676E3D22686152696768742220506172656E74466F6E743D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            222F3E3C546672784D656D6F56696577204C6566743D22302220546F703D2230
            222057696474683D223022204865696768743D22302220526573747269637469
            6F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2231303231383439352220476170583D22332220476170
            593D2233222048416C69676E3D22686152696768742220506172656E74466F6E
            743D2246616C7365222056416C69676E3D22766143656E746572222054657874
            3D22222F3E3C546672784D656D6F56696577204C6566743D22302220546F703D
            2230222057696474683D223022204865696768743D2230222052657374726963
            74696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C73
            652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22
            302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2231303231383439352220476170583D2233222047
            6170593D2233222048416C69676E3D22686152696768742220506172656E7446
            6F6E743D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C546672784D656D6F56696577204C6566743D22302220546F
            703D2230222057696474683D223022204865696768743D223022205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223022204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D2231303231383439352220476170583D223322
            20476170593D2233222048416C69676E3D22686152696768742220506172656E
            74466F6E743D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D22222F3E3C2F63656C6C6D656D6F733E3C63656C6C686561646572
            6D656D6F733E3C546672784D656D6F56696577204C6566743D22302220546F70
            3D2230222057696474683D223022204865696768743D22302220526573747269
            6374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D
            22417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C
            6F723D22313637373732313522204672616D652E5479703D223135222046696C
            6C2E4261636B436F6C6F723D22343634333538332220476170583D2233222047
            6170593D22332220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D22566172696163616F222F3E3C54
            6672784D656D6F56696577204C6566743D22302220546F703D22302220576964
            74683D223022204865696768743D223022205265737472696374696F6E733D22
            382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D22343634333538332220476170583D22332220476170593D22332220
            506172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E
            7465722220546578743D22566172696163616F222F3E3C546672784D656D6F56
            696577204C6566743D22302220546F703D2230222057696474683D2230222048
            65696768743D223022205265737472696374696F6E733D22382220416C6C6F77
            45787072657373696F6E733D2246616C73652220466F6E742E43686172736574
            3D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D
            222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E537479
            6C653D223022204672616D652E436F6C6F723D22313637373732313522204672
            616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22343634
            333538332220476170583D22332220476170593D22332220506172656E74466F
            6E743D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D22566172696163616F222F3E3C546672784D656D6F56696577204C656674
            3D22302220546F703D2230222057696474683D223022204865696768743D2230
            22205265737472696374696F6E733D22382220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223436343335383322204761
            70583D22332220476170593D22332220506172656E74466F6E743D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D225661726961
            63616F222F3E3C2F63656C6C6865616465726D656D6F733E3C636F6C756D6E6D
            656D6F733E3C546672784D656D6F56696577204C6566743D223139332C383538
            3236373731363533362220546F703D223131382C323637373822205769647468
            3D2233302C3233363232303437323434303922204865696768743D2231322C30
            3934343838313922205265737472696374696F6E733D2232342220416C6C6F77
            45787072657373696F6E733D2246616C73652220466F6E742E43686172736574
            3D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D
            222D372220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223022204672616D652E436F6C6F723D2231363737373231352220467261
            6D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2234363433
            3538332220476170583D22332220476170593D2233222048416C69676E3D2268
            6152696768742220506172656E74466F6E743D2246616C73652220576F726457
            7261703D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C2F636F6C756D6E6D656D6F733E3C636F6C756D6E746F7461
            6C6D656D6F733E3C546672784D656D6F56696577204C6566743D223530222054
            6F703D223231222057696474683D22383122204865696768743D223232222052
            65737472696374696F6E733D2238222056697369626C653D2246616C73652220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E
            742E5374796C653D223122204672616D652E436F6C6F723D2231363737373231
            3522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F72
            3D22343634333538332220476170583D22332220476170593D2233222048416C
            69676E3D22686143656E7465722220506172656E74466F6E743D2246616C7365
            222056416C69676E3D22766143656E7465722220546578743D224772616E6420
            546F74616C222F3E3C2F636F6C756D6E746F74616C6D656D6F733E3C636F726E
            65726D656D6F733E3C546672784D656D6F56696577204C6566743D2232302220
            546F703D223131382C3236373738222057696474683D223137332C3835383236
            3737313635333622204865696768743D223022205265737472696374696F6E73
            3D2238222056697369626C653D2246616C73652220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223022
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22343634333538332220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22566172696163616F222F3E3C546672784D
            656D6F56696577204C6566743D223139332C3835383236373731363533362220
            546F703D223131382C3236373738222057696474683D2233302C323336323230
            3437323434303922204865696768743D223022205265737472696374696F6E73
            3D2238222056697369626C653D2246616C73652220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223022
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22343634333538332220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D2225222F3E3C546672784D656D6F56696577
            204C6566743D22302220546F703D2230222057696474683D2230222048656967
            68743D223022205265737472696374696F6E733D2238222056697369626C653D
            2246616C73652220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D22343634333538332220476170583D2233222047617059
            3D2233222048416C69676E3D22686143656E7465722220506172656E74466F6E
            743D2246616C7365222056416C69676E3D22766143656E746572222054657874
            3D22222F3E3C546672784D656D6F56696577204C6566743D2232302220546F70
            3D223131382C3236373738222057696474683D223131332C3338353832363737
            3136353422204865696768743D2231322C303934343838313922205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D372220466F6E742E4E616D653D
            22417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C
            6F723D22313637373732313522204672616D652E5479703D223135222046696C
            6C2E4261636B436F6C6F723D22343634333538332220476170583D2233222047
            6170593D22332220506172656E74466F6E743D2246616C73652220576F726457
            7261703D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D22546162656C61206461207661726961C3A7C3A36F20656D2025222F3E
            3C546672784D656D6F56696577204C6566743D223133332C3338353832363737
            313635342220546F703D223131382C3236373738222057696474683D2233302C
            3233363232303437323434303922204865696768743D2231322C303934343838
            313922205265737472696374696F6E733D22382220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D37222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2234363433353833222047
            6170583D22332220476170593D2233222048416C69676E3D22686143656E7465
            722220506172656E74466F6E743D2246616C73652220576F7264577261703D22
            46616C7365222056416C69676E3D22766143656E7465722220546578743D2243
            C3B36469676F222F3E3C546672784D656D6F56696577204C6566743D22313633
            2C3632323034373234343039352220546F703D223131382C3236373738222057
            696474683D2233302C3233363232303437323434303922204865696768743D22
            31322C303934343838313922205265737472696374696F6E733D22382220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D372220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2234
            3634333538332220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E7465722220506172656E74466F6E743D2246616C7365222057
            6F7264577261703D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D22507265C3A76F222F3E3C2F636F726E65726D656D6F733E3C
            726F776D656D6F733E3C546672784D656D6F56696577204C6566743D22323022
            20546F703D223133302C3336323236383139222057696474683D223131332C33
            383538323637373136353422204865696768743D2231322C3039343438383139
            22205265737472696374696F6E733D2232342220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D372220466F
            6E742E4E616D653D22417269616C2220466F6E742E5374796C653D2230222046
            72616D652E436F6C6F723D22313637373732313522204672616D652E5479703D
            223135222046696C6C2E4261636B436F6C6F723D223436343335383322204761
            70583D22332220476170593D22332220506172656E74466F6E743D2246616C73
            652220576F7264577261703D2246616C7365222056416C69676E3D2276614365
            6E7465722220546578743D22222F3E3C546672784D656D6F56696577204C6566
            743D223133332C3338353832363737313635342220546F703D223133302C3336
            323236383139222057696474683D2233302C3233363232303437323434303922
            204865696768743D2231322C303934343838313922205265737472696374696F
            6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D372220466F6E742E4E616D653D22417269
            616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D22
            313637373732313522204672616D652E5479703D223135222046696C6C2E4261
            636B436F6C6F723D22343634333538332220476170583D22332220476170593D
            2233222048416C69676E3D22686143656E7465722220506172656E74466F6E74
            3D2246616C73652220576F7264577261703D2246616C7365222056416C69676E
            3D22766143656E7465722220546578743D22222F3E3C546672784D656D6F5669
            6577204C6566743D223136332C3632323034373234343039352220546F703D22
            3133302C3336323236383139222057696474683D2233302C3233363232303437
            323434303922204865696768743D2231322C3039343438383139222052657374
            72696374696F6E733D2232342220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D372220466F6E742E4E616D
            653D22417269616C2220466F6E742E5374796C653D223022204672616D652E43
            6F6C6F723D22313637373732313522204672616D652E5479703D223135222046
            696C6C2E4261636B436F6C6F723D22343634333538332220476170583D223322
            20476170593D2233222048416C69676E3D22686143656E746572222050617265
            6E74466F6E743D2246616C73652220576F7264577261703D2246616C73652220
            56416C69676E3D22766143656E7465722220546578743D22222F3E3C2F726F77
            6D656D6F733E3C726F77746F74616C6D656D6F733E3C546672784D656D6F5669
            6577204C6566743D22302220546F703D2230222057696474683D223022204865
            696768743D223022205265737472696374696F6E733D2238222056697369626C
            653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C73
            652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22
            302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223122204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D22343634333538332220476170583D223322204761
            70593D2233222048416C69676E3D22686143656E7465722220506172656E7446
            6F6E743D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D224772616E6420546F74616C222F3E3C546672784D656D6F5669657720
            4C6566743D223230302220546F703D223436222057696474683D223534222048
            65696768743D22323222205265737472696374696F6E733D2238222056697369
            626C653D2246616C73652220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223122204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D22343634333538332220476170583D22332220
            476170593D2233222048416C69676E3D22686143656E7465722220506172656E
            74466F6E743D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D22546F74616C222F3E3C546672784D656D6F56696577204C656674
            3D223236382C3033313439363036323939322220546F703D2236382220576964
            74683D22353422204865696768743D22323222205265737472696374696F6E73
            3D2238222056697369626C653D2246616C73652220416C6C6F77457870726573
            73696F6E733D2246616C73652220466F6E742E436861727365743D2231222046
            6F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31332220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223122
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22343634333538332220
            476170583D22332220476170593D2233222048416C69676E3D22686143656E74
            65722220506172656E74466F6E743D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22546F74616C222F3E3C2F726F77746F7461
            6C6D656D6F733E3C63656C6C66756E6374696F6E733E3C6974656D20312F3E3C
            2F63656C6C66756E6374696F6E733E3C636F6C756D6E736F72743E3C6974656D
            20302F3E3C2F636F6C756D6E736F72743E3C726F77736F72743E3C6974656D20
            302F3E3C6974656D20302F3E3C6974656D20302F3E3C2F726F77736F72743E3C
            2F63726F73733E}
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        Child = frxPrcGruC_Land.PageFooter1
        RowCount = 1
      end
      object Child2: TfrxChild
        FillType = ftBrush
        Height = 80.062994570000000000
        Top = 207.874150000000000000
        Width = 1046.929810000000000000
        Child = frxPrcGruC_Land.PageHeader1
        object DBCross3: TfrxDBCrossView
          Top = 3.779529999999994000
          Width = 244.094488188976700000
          Height = 76.283464570000010000
          AutoSize = False
          DownThenAcross = False
          JoinEqualCells = True
          KeepTogether = True
          RowLevels = 3
          ShowColumnTotal = False
          ShowRowTotal = False
          CellFields.Strings = (
            'PRC_PRZ')
          ColumnFields.Strings = (
            'NOMEPRZ')
          DataSet = frxDsTabPrcPrz
          DataSetName = 'frxDsTabPrcPrz'
          RowFields.Strings = (
            'NOME1'
            'CodUsu'
            'Preco')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D223139332C
            3835383236373731363533362220546F703D223235352C383432363536333822
            2057696474683D2233302C323336323230343732343430392220486569676874
            3D2231322C303934343838313922205265737472696374696F6E733D22323422
            20416C6C6F7745787072657373696F6E733D2246616C73652220446973706C61
            79466F726D61742E446563696D616C536570617261746F723D222C2220446973
            706C6179466F726D61742E466F726D61745374723D2225322E326E2220446973
            706C6179466F726D61742E4B696E643D22666B4E756D657269632220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D372220466F6E742E4E616D653D22417269616C222046
            6F6E742E5374796C653D223022204672616D652E436F6C6F723D223136373737
            32313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C
            6F723D2231363730303334362220476170583D22332220476170593D22332220
            48416C69676E3D22686152696768742220506172656E74466F6E743D2246616C
            73652220576F7264577261703D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D22302C3030222F3E3C546672784D656D6F566965
            77205461673D223122204C6566743D223333302C303331343936303632393932
            2220546F703D223231222057696474683D2236382C3033313439363036323939
            323122204865696768743D22323222205265737472696374696F6E733D223234
            2220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E
            436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E
            4865696768743D222D31332220466F6E742E4E616D653D22417269616C222046
            6F6E742E5374796C653D223022204672616D652E436F6C6F723D223136373737
            32313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C
            6F723D2231363730303334362220476170583D22332220476170593D22332220
            48416C69676E3D22686152696768742220506172656E74466F6E743D2246616C
            736522205374796C653D2263656C6C222056416C69676E3D22766143656E7465
            722220546578743D22302C3030222F3E3C546672784D656D6F56696577205461
            673D223222204C6566743D223230352220546F703D223433222057696474683D
            22383122204865696768743D22323122205265737472696374696F6E733D2232
            342220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231363730303334362220476170583D22332220476170593D223322
            2048416C69676E3D22686152696768742220506172656E74466F6E743D224661
            6C736522205374796C653D2263656C6C222056416C69676E3D22766143656E74
            65722220546578743D22522420302C3030222F3E3C546672784D656D6F566965
            77205461673D223322204C6566743D223230352220546F703D22363422205769
            6474683D22383122204865696768743D22323222205265737472696374696F6E
            733D2232342220416C6C6F7745787072657373696F6E733D2246616C73652220
            466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220
            466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269
            616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D22
            313637373732313522204672616D652E5479703D223135222046696C6C2E4261
            636B436F6C6F723D2231363730303334362220476170583D2233222047617059
            3D2233222048416C69676E3D22686152696768742220506172656E74466F6E74
            3D2246616C736522205374796C653D2263656C6C222056416C69676E3D227661
            43656E7465722220546578743D22522420302C3030222F3E3C546672784D656D
            6F56696577204C6566743D22302220546F703D2230222057696474683D223022
            204865696768743D223022205265737472696374696F6E733D22382220416C6C
            6F7745787072657373696F6E733D2246616C73652220466F6E742E4368617273
            65743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768
            743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2231
            363730303334362220476170583D22332220476170593D2233222048416C6967
            6E3D22686152696768742220506172656E74466F6E743D2246616C7365222056
            416C69676E3D22766143656E7465722220546578743D22222F3E3C546672784D
            656D6F56696577204C6566743D22302220546F703D2230222057696474683D22
            3022204865696768743D223022205265737472696374696F6E733D2238222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2231363730303334362220476170583D22332220476170593D2233222048416C
            69676E3D22686152696768742220506172656E74466F6E743D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C546672
            784D656D6F56696577204C6566743D22302220546F703D223022205769647468
            3D223022204865696768743D223022205265737472696374696F6E733D223822
            20416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43
            6861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48
            65696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F
            6E742E5374796C653D223022204672616D652E436F6C6F723D22313637373732
            313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F
            723D2231363730303334362220476170583D22332220476170593D2233222048
            416C69676E3D22686152696768742220506172656E74466F6E743D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D22222F3E3C54
            6672784D656D6F56696577204C6566743D22302220546F703D22302220576964
            74683D223022204865696768743D223022205265737472696374696F6E733D22
            382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E74
            2E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E74
            2E4865696768743D222D31332220466F6E742E4E616D653D22417269616C2220
            466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231363737
            3732313522204672616D652E5479703D223135222046696C6C2E4261636B436F
            6C6F723D2231363730303334362220476170583D22332220476170593D223322
            2048416C69676E3D22686152696768742220506172656E74466F6E743D224661
            6C7365222056416C69676E3D22766143656E7465722220546578743D22222F3E
            3C2F63656C6C6D656D6F733E3C63656C6C6865616465726D656D6F733E3C5466
            72784D656D6F56696577205461673D2236303022204C6566743D22302220546F
            703D2230222057696474683D223022204865696768743D223022205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223022204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D2231363632393134332220476170583D223322
            20476170593D22332220506172656E74466F6E743D2246616C73652220537479
            6C653D2263656C6C686561646572222056416C69676E3D22766143656E746572
            2220546578743D225052435F50525A222F3E3C546672784D656D6F5669657720
            5461673D2236303122204C6566743D22302220546F703D223022205769647468
            3D223022204865696768743D223022205265737472696374696F6E733D223822
            20416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43
            6861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48
            65696768743D222D31332220466F6E742E4E616D653D22417269616C2220466F
            6E742E5374796C653D223022204672616D652E436F6C6F723D22313637373732
            313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F
            723D2231363632393134332220476170583D22332220476170593D2233222050
            6172656E74466F6E743D2246616C736522205374796C653D2263656C6C686561
            646572222056416C69676E3D22766143656E7465722220546578743D22505243
            5F50525A222F3E3C546672784D656D6F56696577204C6566743D22302220546F
            703D2230222057696474683D223022204865696768743D223022205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223022204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D2231363632393134332220476170583D223322
            20476170593D22332220506172656E74466F6E743D2246616C7365222056416C
            69676E3D22766143656E7465722220546578743D225052435F50525A222F3E3C
            546672784D656D6F56696577204C6566743D22302220546F703D223022205769
            6474683D223022204865696768743D223022205265737472696374696F6E733D
            22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223022204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D2231363632393134332220476170583D22332220476170593D2233
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D225052435F50525A222F3E3C2F63656C6C686561
            6465726D656D6F733E3C636F6C756D6E6D656D6F733E3C546672784D656D6F56
            696577205461673D2231303022204C6566743D223139332C3835383236373731
            363533362220546F703D223234332C3734383136383139222057696474683D22
            33302C3233363232303437323434303922204865696768743D2231322C303934
            343838313922205265737472696374696F6E733D2232342220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            372220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223022204672616D652E436F6C6F723D22313637373732313522204672616D65
            2E5479703D223135222046696C6C2E4261636B436F6C6F723D22313636323931
            34332220476170583D22332220476170593D2233222048416C69676E3D226861
            52696768742220506172656E74466F6E743D2246616C73652220576F72645772
            61703D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D22222F3E3C2F636F6C756D6E6D656D6F733E3C636F6C756D6E746F74616C
            6D656D6F733E3C546672784D656D6F56696577205461673D2233303022204C65
            66743D223230352220546F703D223231222057696474683D2238312220486569
            6768743D22323222205265737472696374696F6E733D2238222056697369626C
            653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C73
            652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22
            302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223122204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2231363632393134332220476170583D2233222047
            6170593D2233222048416C69676E3D22686143656E7465722220506172656E74
            466F6E743D2246616C736522205374796C653D22636F6C6772616E6422205641
            6C69676E3D22766143656E7465722220546578743D224772616E6420546F7461
            6C222F3E3C2F636F6C756D6E746F74616C6D656D6F733E3C636F726E65726D65
            6D6F733E3C546672784D656D6F56696577205461673D2235303022204C656674
            3D2232302220546F703D223233312C3635333638222057696474683D22313733
            2C38353832363737313635333622204865696768743D2231322C303934343838
            313922205265737472696374696F6E733D22382220416C6C6F77457870726573
            73696F6E733D2246616C73652220446973706C6179466F726D61742E44656369
            6D616C536570617261746F723D222C2220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D37
            2220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22
            3022204672616D652E436F6C6F723D22313637373732313522204672616D652E
            5479703D223135222046696C6C2E4261636B436F6C6F723D2231363632393134
            332220476170583D22332220476170593D2233222048416C69676E3D22686143
            656E7465722220506172656E74466F6E743D2246616C7365222056416C69676E
            3D22766143656E7465722220546578743D22477275706F732064652050726F64
            75746F73222F3E3C546672784D656D6F56696577205461673D2235303122204C
            6566743D223139332C3835383236373731363533362220546F703D223233312C
            3635333638222057696474683D2233302C323336323230343732343430392220
            4865696768743D2231322C303934343838313922205265737472696374696F6E
            733D22382220416C6C6F7745787072657373696F6E733D2246616C7365222044
            6973706C6179466F726D61742E446563696D616C536570617261746F723D222C
            2220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D2230
            2220466F6E742E4865696768743D222D372220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2231363632393134332220476170583D22332220476170
            593D2233222048416C69676E3D22686143656E7465722220506172656E74466F
            6E743D2246616C7365222056416C69676E3D22766143656E7465722220546578
            743D225072617A6F222F3E3C546672784D656D6F56696577205461673D223530
            3222204C6566743D22302220546F703D2230222057696474683D223022204865
            696768743D223022205265737472696374696F6E733D2238222056697369626C
            653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C73
            652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22
            302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2231363632393134332220476170583D2233222047
            6170593D2233222048416C69676E3D22686143656E7465722220506172656E74
            466F6E743D2246616C736522205374796C653D22636F726E6572222056416C69
            676E3D22766143656E7465722220546578743D22222F3E3C546672784D656D6F
            56696577205461673D2235303322204C6566743D2232302220546F703D223234
            332C3734383136383139222057696474683D223131332C333835383236373731
            36353422204865696768743D2231322C30393434383831392220526573747269
            6374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D372220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2231363632393134332220476170583D2233222047
            6170593D22332220506172656E74466F6E743D2246616C7365222056416C6967
            6E3D22766143656E7465722220546578743D22546162656C6120646F73207072
            65C3A76F7320646F7320677275706F73222F3E3C546672784D656D6F56696577
            204C6566743D223133332C3338353832363737313635342220546F703D223234
            332C3734383136383139222057696474683D2233302C32333632323034373234
            34303922204865696768743D2231322C30393434383831392220526573747269
            6374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D372220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2231363632393134332220476170583D2233222047
            6170593D2233222048416C69676E3D22686143656E7465722220506172656E74
            466F6E743D2246616C7365222056416C69676E3D22766143656E746572222054
            6578743D2243C3B36469676F222F3E3C546672784D656D6F56696577204C6566
            743D223136332C3632323034373234343039352220546F703D223234332C3734
            383136383139222057696474683D2233302C3233363232303437323434303922
            204865696768743D2231322C303934343838313922205265737472696374696F
            6E733D22382220416C6C6F7745787072657373696F6E733D2246616C73652220
            466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220
            466F6E742E4865696768743D222D372220466F6E742E4E616D653D2241726961
            6C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D2231
            3637373732313522204672616D652E5479703D223135222046696C6C2E426163
            6B436F6C6F723D2231363632393134332220476170583D22332220476170593D
            2233222048416C69676E3D22686152696768742220506172656E74466F6E743D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            507265C3A76F222F3E3C2F636F726E65726D656D6F733E3C726F776D656D6F73
            3E3C546672784D656D6F56696577205461673D2232303022204C6566743D2232
            302220546F703D223235352C3834323635363338222057696474683D22313133
            2C33383538323637373136353422204865696768743D2231322C303934343838
            313922205265737472696374696F6E733D2232342220416C6C6F774578707265
            7373696F6E733D2246616C73652220466F6E742E436861727365743D22312220
            466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D372220
            466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D223022
            204672616D652E436F6C6F723D22313637373732313522204672616D652E5479
            703D223135222046696C6C2E4261636B436F6C6F723D22313636323931343322
            20476170583D22332220476170593D22332220506172656E74466F6E743D2246
            616C73652220576F7264577261703D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22222F3E3C546672784D656D6F5669657720
            4C6566743D223133332C3338353832363737313635342220546F703D22323535
            2C3834323635363338222057696474683D2233302C3233363232303437323434
            303922204865696768743D2231322C3039343438383139222052657374726963
            74696F6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D372220466F6E742E4E616D653D22
            417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F
            723D22313637373732313522204672616D652E5479703D223135222046696C6C
            2E4261636B436F6C6F723D2231363632393134332220476170583D2233222047
            6170593D2233222048416C69676E3D22686143656E7465722220506172656E74
            466F6E743D2246616C73652220576F7264577261703D2246616C736522205641
            6C69676E3D22766143656E7465722220546578743D22222F3E3C546672784D65
            6D6F56696577204C6566743D223136332C363232303437323434303935222054
            6F703D223235352C3834323635363338222057696474683D2233302C32333632
            32303437323434303922204865696768743D2231322C30393434383831392220
            5265737472696374696F6E733D2232342220416C6C6F7745787072657373696F
            6E733D2246616C73652220446973706C6179466F726D61742E466F726D617453
            74723D2225322E326E2220446973706C6179466F726D61742E4B696E643D2266
            6B4E756D657269632220466F6E742E436861727365743D22312220466F6E742E
            436F6C6F723D22302220466F6E742E4865696768743D222D372220466F6E742E
            4E616D653D22417269616C2220466F6E742E5374796C653D223022204672616D
            652E436F6C6F723D22313637373732313522204672616D652E5479703D223135
            222046696C6C2E4261636B436F6C6F723D223136363239313433222047617058
            3D22332220476170593D2233222048416C69676E3D2268615269676874222050
            6172656E74466F6E743D2246616C73652220576F7264577261703D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D22222F3E3C2F
            726F776D656D6F733E3C726F77746F74616C6D656D6F733E3C546672784D656D
            6F56696577205461673D2234303022204C6566743D22302220546F703D223634
            222057696474683D2231323522204865696768743D2232322220526573747269
            6374696F6E733D2238222056697369626C653D2246616C73652220416C6C6F77
            45787072657373696F6E733D2246616C73652220466F6E742E43686172736574
            3D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D
            222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E537479
            6C653D223122204672616D652E436F6C6F723D22313637373732313522204672
            616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22313636
            32393134332220476170583D22332220476170593D2233222048416C69676E3D
            22686143656E7465722220506172656E74466F6E743D2246616C736522205374
            796C653D22726F776772616E64222056416C69676E3D22766143656E74657222
            20546578743D224772616E6420546F74616C222F3E3C546672784D656D6F5669
            6577204C6566743D223230302220546F703D223436222057696474683D223439
            22204865696768743D22323222205265737472696374696F6E733D2238222056
            697369626C653D2246616C73652220416C6C6F7745787072657373696F6E733D
            2246616C73652220466F6E742E436861727365743D22312220466F6E742E436F
            6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223122204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D2231363632393134332220476170583D
            22332220476170593D2233222048416C69676E3D22686143656E746572222050
            6172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E74
            65722220546578743D22546F74616C222F3E3C546672784D656D6F5669657720
            4C6566743D223236382C3033313439363036323939322220546F703D22323122
            2057696474683D22363222204865696768743D22323222205265737472696374
            696F6E733D2238222056697369626C653D2246616C73652220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223122204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223136363239
            3134332220476170583D22332220476170593D2233222048416C69676E3D2268
            6143656E7465722220506172656E74466F6E743D2246616C7365222056416C69
            676E3D22766143656E7465722220546578743D22546F74616C222F3E3C2F726F
            77746F74616C6D656D6F733E3C63656C6C66756E6374696F6E733E3C6974656D
            20312F3E3C2F63656C6C66756E6374696F6E733E3C636F6C756D6E736F72743E
            3C6974656D20302F3E3C2F636F6C756D6E736F72743E3C726F77736F72743E3C
            6974656D20302F3E3C6974656D20302F3E3C6974656D20302F3E3C2F726F7773
            6F72743E3C2F63726F73733E}
        end
      end
      object Child3: TfrxChild
        FillType = ftBrush
        Height = 112.566929140000000000
        Top = 374.173470000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'Child3OnBeforePrint'
        object DBCross2: TfrxDBCrossView
          Width = 168.503937005433400000
          Height = 112.566929136929200000
          AutoSize = False
          CellLevels = 2
          ColumnLevels = 3
          DownThenAcross = False
          KeepTogether = True
          PlainCells = True
          ShowColumnTotal = False
          ShowRowTotal = False
          CellFields.Strings = (
            'Reduz'
            'VAL_PRZ_PRD')
          ColumnFields.Strings = (
            'NOMEGRU'
            'NOMETAM'
            'NOMEPRZ')
          DataSet = frxDsPrcPrzIts
          DataSetName = 'frxDsPrcPrzIts'
          RowFields.Strings = (
            'NOMECOR')
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D2238382C30
            3331343936303632393932312220546F703D223435342C363435393130393436
            393239222057696474683D2233302C323336323230343722204865696768743D
            2231322C303934343838313922205265737472696374696F6E733D2232342220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D372220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2235333931382220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E7465722220506172656E74466F6E743D2246616C7365222057
            6F7264577261703D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D2230222F3E3C546672784D656D6F56696577204C6566743D22
            3131382C3236373731363533323939322220546F703D223435342C3634353931
            30393436393239222057696474683D2233302C32333632323034373234343133
            22204865696768743D2231322C30393434383831392220526573747269637469
            6F6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C7365
            2220446973706C6179466F726D61742E466F726D61745374723D2225322E326E
            2220446973706C6179466F726D61742E4B696E643D22666B4E756D6572696322
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D372220466F6E742E4E616D653D22417269
            616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D22
            313637373732313522204672616D652E5479703D223135222046696C6C2E4261
            636B436F6C6F723D2235333931382220476170583D22332220476170593D2233
            222048416C69676E3D22686152696768742220506172656E74466F6E743D2246
            616C7365222056416C69676E3D22766143656E7465722220546578743D22302C
            3030222F3E3C546672784D656D6F56696577204C6566743D223139362C333835
            38323637372220546F703D223634222057696474683D22313137222048656967
            68743D22323122205265737472696374696F6E733D2232342220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223022204672616D652E436F6C6F723D2231363737373231352220467261
            6D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2235333931
            382220476170583D22332220476170593D2233222048416C69676E3D22686152
            696768742220506172656E74466F6E743D2246616C7365222056416C69676E3D
            22766143656E7465722220546578743D22302C3030222F3E3C546672784D656D
            6F56696577204C6566743D22302220546F703D2230222057696474683D223022
            204865696768743D223022205265737472696374696F6E733D22382220416C6C
            6F7745787072657373696F6E733D2246616C73652220466F6E742E4368617273
            65743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768
            743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E53
            74796C653D223022204672616D652E436F6C6F723D2231363737373231352220
            4672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2235
            333931382220476170583D22332220476170593D2233222048416C69676E3D22
            686152696768742220506172656E74466F6E743D2246616C7365222056416C69
            676E3D22766143656E7465722220546578743D22222F3E3C546672784D656D6F
            56696577204C6566743D223138332220546F703D223633222057696474683D22
            383122204865696768743D22323122205265737472696374696F6E733D223234
            2220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E
            436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E
            4865696768743D222D31332220466F6E742E4E616D653D22417269616C222046
            6F6E742E5374796C653D223022204672616D652E436F6C6F723D223136373737
            32313522204672616D652E5479703D223135222046696C6C2E4261636B436F6C
            6F723D2235333931382220476170583D22332220476170593D2233222048416C
            69676E3D22686152696768742220506172656E74466F6E743D2246616C736522
            2056416C69676E3D22766143656E7465722220546578743D22302C3030222F3E
            3C546672784D656D6F56696577204C6566743D223138332220546F703D223834
            222057696474683D22383122204865696768743D223232222052657374726963
            74696F6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C
            73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F723D
            22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D653D
            22417269616C2220466F6E742E5374796C653D223022204672616D652E436F6C
            6F723D22313637373732313522204672616D652E5479703D223135222046696C
            6C2E4261636B436F6C6F723D2235333931382220476170583D22332220476170
            593D2233222048416C69676E3D22686152696768742220506172656E74466F6E
            743D2246616C7365222056416C69676E3D22766143656E746572222054657874
            3D22302C3030222F3E3C546672784D656D6F56696577204C6566743D22302220
            546F703D2230222057696474683D223022204865696768743D22302220526573
            7472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D22
            46616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C
            6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E61
            6D653D22417269616C2220466F6E742E5374796C653D223022204672616D652E
            436F6C6F723D22313637373732313522204672616D652E5479703D2231352220
            46696C6C2E4261636B436F6C6F723D2235333931382220476170583D22332220
            476170593D2233222048416C69676E3D22686152696768742220506172656E74
            466F6E743D2246616C7365222056416C69676E3D22766143656E746572222054
            6578743D22222F3E3C546672784D656D6F56696577204C6566743D2230222054
            6F703D2230222057696474683D223022204865696768743D2230222052657374
            72696374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246
            616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F
            723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D
            653D22417269616C2220466F6E742E5374796C653D223022204672616D652E43
            6F6C6F723D22313637373732313522204672616D652E5479703D223135222046
            696C6C2E4261636B436F6C6F723D2235333931382220476170583D2233222047
            6170593D2233222048416C69676E3D22686152696768742220506172656E7446
            6F6E743D2246616C7365222056416C69676E3D22766143656E74657222205465
            78743D22222F3E3C546672784D656D6F56696577204C6566743D22302220546F
            703D2230222057696474683D223022204865696768743D223022205265737472
            696374696F6E733D22382220416C6C6F7745787072657373696F6E733D224661
            6C736522204672616D652E5479703D2231352220476170583D22332220476170
            593D2233222048416C69676E3D2268615269676874222056416C69676E3D2276
            6143656E7465722220546578743D22222F3E3C546672784D656D6F5669657720
            4C6566743D22302220546F703D2230222057696474683D223022204865696768
            743D223022205265737472696374696F6E733D22382220416C6C6F7745787072
            657373696F6E733D2246616C736522204672616D652E5479703D223135222047
            6170583D22332220476170593D2233222048416C69676E3D2268615269676874
            222056416C69676E3D22766143656E7465722220546578743D22222F3E3C5466
            72784D656D6F56696577204C6566743D22302220546F703D2230222057696474
            683D223022204865696768743D223022205265737472696374696F6E733D2238
            2220416C6C6F7745787072657373696F6E733D2246616C736522204672616D65
            2E5479703D2231352220476170583D22332220476170593D2233222048416C69
            676E3D2268615269676874222056416C69676E3D22766143656E746572222054
            6578743D22222F3E3C546672784D656D6F56696577204C6566743D2230222054
            6F703D2230222057696474683D223022204865696768743D2230222052657374
            72696374696F6E733D22382220416C6C6F7745787072657373696F6E733D2246
            616C736522204672616D652E5479703D2231352220476170583D223322204761
            70593D2233222048416C69676E3D2268615269676874222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C546672784D656D6F56696577
            204C6566743D22302220546F703D2230222057696474683D2230222048656967
            68743D223022205265737472696374696F6E733D22382220416C6C6F77457870
            72657373696F6E733D2246616C736522204672616D652E5479703D2231352220
            476170583D22332220476170593D2233222048416C69676E3D22686152696768
            74222056416C69676E3D22766143656E7465722220546578743D22222F3E3C54
            6672784D656D6F56696577204C6566743D22302220546F703D22302220576964
            74683D223022204865696768743D223022205265737472696374696F6E733D22
            382220416C6C6F7745787072657373696F6E733D2246616C736522204672616D
            652E5479703D2231352220476170583D22332220476170593D2233222048416C
            69676E3D2268615269676874222056416C69676E3D22766143656E7465722220
            546578743D22222F3E3C546672784D656D6F56696577204C6566743D22302220
            546F703D2230222057696474683D223022204865696768743D22302220526573
            7472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D22
            46616C736522204672616D652E5479703D2231352220476170583D2233222047
            6170593D2233222048416C69676E3D2268615269676874222056416C69676E3D
            22766143656E7465722220546578743D22222F3E3C546672784D656D6F566965
            77204C6566743D22302220546F703D2230222057696474683D22302220486569
            6768743D223022205265737472696374696F6E733D22382220416C6C6F774578
            7072657373696F6E733D2246616C736522204672616D652E5479703D22313522
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            6874222056416C69676E3D22766143656E7465722220546578743D22222F3E3C
            2F63656C6C6D656D6F733E3C63656C6C6865616465726D656D6F733E3C546672
            784D656D6F56696577204C6566743D2238382C30333134393630363239393231
            2220546F703D223434322C353531343232373536393239222057696474683D22
            33302C323336323230343722204865696768743D2231322C3039343438383139
            22205265737472696374696F6E733D22382220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D372220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D2234323130372220476170583D
            22332220476170593D2233222048416C69676E3D22686143656E746572222050
            6172656E74466F6E743D2246616C7365222056416C69676E3D22766143656E74
            65722220546578743D2243C3B36469676F222F3E3C546672784D656D6F566965
            77204C6566743D223131382C3236373731363533323939322220546F703D2234
            34322C353531343232373536393239222057696474683D2233302C3233363232
            303437323434313322204865696768743D2231322C3039343438383139222052
            65737472696374696F6E733D22382220416C6C6F7745787072657373696F6E73
            3D2246616C73652220466F6E742E436861727365743D22312220466F6E742E43
            6F6C6F723D22302220466F6E742E4865696768743D222D372220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223022204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D2234323130372220476170583D223322
            20476170593D2233222048416C69676E3D22686152696768742220506172656E
            74466F6E743D2246616C7365222056416C69676E3D22766143656E7465722220
            546578743D22507265C3A76F222F3E3C546672784D656D6F56696577204C6566
            743D22302220546F703D2230222057696474683D223022204865696768743D22
            3022205265737472696374696F6E733D22382220416C6C6F7745787072657373
            696F6E733D2246616C73652220466F6E742E436861727365743D22312220466F
            6E742E436F6C6F723D22302220466F6E742E4865696768743D222D3133222046
            6F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D22302220
            4672616D652E436F6C6F723D22313637373732313522204672616D652E547970
            3D223135222046696C6C2E4261636B436F6C6F723D2234323130372220476170
            583D22332220476170593D22332220506172656E74466F6E743D2246616C7365
            222056416C69676E3D22766143656E7465722220546578743D22526564757A22
            2F3E3C546672784D656D6F56696577204C6566743D22302220546F703D223022
            2057696474683D223022204865696768743D223022205265737472696374696F
            6E733D22382220416C6C6F7745787072657373696F6E733D2246616C73652220
            466F6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220
            466F6E742E4865696768743D222D31332220466F6E742E4E616D653D22417269
            616C2220466F6E742E5374796C653D223022204672616D652E436F6C6F723D22
            313637373732313522204672616D652E5479703D223135222046696C6C2E4261
            636B436F6C6F723D2234323130372220476170583D22332220476170593D2233
            2220506172656E74466F6E743D2246616C7365222056416C69676E3D22766143
            656E7465722220546578743D2256414C5F50525A5F505244222F3E3C54667278
            4D656D6F56696577204C6566743D22302220546F703D2230222057696474683D
            223022204865696768743D223022205265737472696374696F6E733D22382220
            416C6C6F7745787072657373696F6E733D2246616C736522204672616D652E54
            79703D2231352220476170583D22332220476170593D2233222056416C69676E
            3D22766143656E7465722220546578743D22222F3E3C546672784D656D6F5669
            6577204C6566743D22302220546F703D2230222057696474683D223022204865
            696768743D223022205265737472696374696F6E733D22382220416C6C6F7745
            787072657373696F6E733D2246616C736522204672616D652E5479703D223135
            2220476170583D22332220476170593D2233222056416C69676E3D2276614365
            6E7465722220546578743D22222F3E3C546672784D656D6F56696577204C6566
            743D22302220546F703D2230222057696474683D223022204865696768743D22
            3022205265737472696374696F6E733D22382220416C6C6F7745787072657373
            696F6E733D2246616C736522204672616D652E5479703D223135222047617058
            3D22332220476170593D2233222056416C69676E3D22766143656E7465722220
            546578743D22222F3E3C546672784D656D6F56696577204C6566743D22302220
            546F703D2230222057696474683D223022204865696768743D22302220526573
            7472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D22
            46616C736522204672616D652E5479703D2231352220476170583D2233222047
            6170593D2233222056416C69676E3D22766143656E7465722220546578743D22
            222F3E3C2F63656C6C6865616465726D656D6F733E3C636F6C756D6E6D656D6F
            733E3C546672784D656D6F56696577204C6566743D2238382C30333134393630
            3632393932312220546F703D223430362C323637393538313922205769647468
            3D2236302C3437323434303934323434313322204865696768743D2231322C30
            39343438383138383937363422205265737472696374696F6E733D2232342220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D372220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2234323130372220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E7465722220506172656E74466F6E743D2246616C7365222057
            6F7264577261703D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D22222F3E3C546672784D656D6F56696577204C6566743D2238
            382C303331343936303632393932312220546F703D223431382C333632343436
            333738393736222057696474683D2236302C3437323434303934323434313322
            204865696768743D2231322C3039343438383138383937363422205265737472
            696374696F6E733D2232342220416C6C6F7745787072657373696F6E733D2246
            616C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F
            723D22302220466F6E742E4865696768743D222D372220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223022204672616D652E436F
            6C6F723D22313637373732313522204672616D652E5479703D22313522204669
            6C6C2E4261636B436F6C6F723D2234323130372220476170583D223322204761
            70593D2233222048416C69676E3D22686143656E7465722220506172656E7446
            6F6E743D2246616C73652220576F7264577261703D2246616C7365222056416C
            69676E3D22766143656E7465722220546578743D22222F3E3C546672784D656D
            6F56696577204C6566743D2238382C303331343936303632393932312220546F
            703D223433302C343536393334353637393533222057696474683D2236302C34
            37323434303934323434313322204865696768743D2231322C30393434383831
            38383937363422205265737472696374696F6E733D2232342220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D372220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C65
            3D223022204672616D652E436F6C6F723D22313637373732313522204672616D
            652E5479703D223135222046696C6C2E4261636B436F6C6F723D223432313037
            2220476170583D22332220476170593D2233222048416C69676E3D2268614365
            6E7465722220506172656E74466F6E743D2246616C73652220576F7264577261
            703D2246616C7365222056416C69676E3D22766143656E746572222054657874
            3D22222F3E3C2F636F6C756D6E6D656D6F733E3C636F6C756D6E746F74616C6D
            656D6F733E3C546672784D656D6F56696577204C6566743D223138332220546F
            703D223231222057696474683D22383122204865696768743D22343222205265
            737472696374696F6E733D2238222056697369626C653D2246616C7365222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223122204672616D652E436F6C6F723D223136373737323135
            22204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D
            2234323130372220476170583D22332220476170593D2233222048416C69676E
            3D22686143656E7465722220506172656E74466F6E743D2246616C7365222056
            416C69676E3D22766143656E7465722220546578743D224772616E6420546F74
            616C222F3E3C546672784D656D6F56696577204C6566743D22302220546F703D
            2230222057696474683D223022204865696768743D2230222052657374726963
            74696F6E733D2238222056697369626C653D2246616C73652220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223122204672616D652E436F6C6F723D2231363737373231352220467261
            6D652E5479703D223135222046696C6C2E4261636B436F6C6F723D2234323130
            372220476170583D22332220476170593D2233222048416C69676E3D22686143
            656E7465722220506172656E74466F6E743D2246616C7365222056416C69676E
            3D22766143656E7465722220546578743D22546F74616C222F3E3C546672784D
            656D6F56696577204C6566743D223139362C33383538323637372220546F703D
            223432222057696474683D2231313722204865696768743D2232322220526573
            7472696374696F6E733D2238222056697369626C653D2246616C73652220416C
            6C6F7745787072657373696F6E733D2246616C73652220466F6E742E43686172
            7365743D22312220466F6E742E436F6C6F723D22302220466F6E742E48656967
            68743D222D31332220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223122204672616D652E436F6C6F723D22313637373732313522
            204672616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22
            34323130372220476170583D22332220476170593D2233222048416C69676E3D
            22686143656E7465722220506172656E74466F6E743D2246616C736522205641
            6C69676E3D22766143656E7465722220546578743D22546F74616C222F3E3C2F
            636F6C756D6E746F74616C6D656D6F733E3C636F726E65726D656D6F733E3C54
            6672784D656D6F56696577204C6566743D2232302220546F703D223339342C31
            37333437222057696474683D2236382C30333134393630363239393231222048
            65696768743D2231322C303934343838313922205265737472696374696F6E73
            3D22382220416C6C6F7745787072657373696F6E733D2246616C73652220466F
            6E742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F
            6E742E4865696768743D222D372220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223022204672616D652E436F6C6F723D22313637
            373732313522204672616D652E5479703D223135222046696C6C2E4261636B43
            6F6C6F723D2234323130372220476170583D22332220476170593D2233222050
            6172656E74466F6E743D2246616C73652220576F7264577261703D2246616C73
            65222056416C69676E3D22766143656E7465722220546578743D22222F3E3C54
            6672784D656D6F56696577204C6566743D2238382C3033313439363036323939
            32312220546F703D223339342C3137333437222057696474683D2236302C3437
            323434303934323434313322204865696768743D2231322C3039343438383139
            22205265737472696374696F6E733D22382220416C6C6F774578707265737369
            6F6E733D2246616C73652220466F6E742E436861727365743D22312220466F6E
            742E436F6C6F723D22302220466F6E742E4865696768743D222D372220466F6E
            742E4E616D653D22417269616C2220466F6E742E5374796C653D223022204672
            616D652E436F6C6F723D22313637373732313522204672616D652E5479703D22
            3135222046696C6C2E4261636B436F6C6F723D2234323130372220476170583D
            22332220476170593D22332220506172656E74466F6E743D2246616C73652220
            576F7264577261703D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C546672784D656D6F56696577204C6566743D22
            38332220546F703D223138222057696474683D2231303122204865696768743D
            2235342C343235313936383422205265737472696374696F6E733D2238222056
            697369626C653D2246616C73652220416C6C6F7745787072657373696F6E733D
            2246616C73652220466F6E742E436861727365743D22312220466F6E742E436F
            6C6F723D22302220466F6E742E4865696768743D222D31332220466F6E742E4E
            616D653D22417269616C2220466F6E742E5374796C653D223022204672616D65
            2E436F6C6F723D22313637373732313522204672616D652E5479703D22313522
            2046696C6C2E4261636B436F6C6F723D2234323130372220476170583D223322
            20476170593D2233222048416C69676E3D22686143656E746572222050617265
            6E74466F6E743D2246616C7365222056416C69676E3D22766143656E74657222
            20546578743D22222F3E3C546672784D656D6F56696577204C6566743D223230
            2220546F703D223430362C3236373935383139222057696474683D2236382C30
            33313439363036323939323122204865696768743D2234382C33373739353237
            35363932393222205265737472696374696F6E733D22382220416C6C6F774578
            7072657373696F6E733D2246616C73652220466F6E742E436861727365743D22
            312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D
            372220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223022204672616D652E436F6C6F723D22313637373732313522204672616D65
            2E5479703D223135222046696C6C2E4261636B436F6C6F723D22343231303722
            20476170583D22332220476170593D22332220506172656E74466F6E743D2246
            616C73652220576F7264577261703D2246616C7365222056416C69676E3D2276
            6143656E7465722220546578743D22436F72222F3E3C2F636F726E65726D656D
            6F733E3C726F776D656D6F733E3C546672784D656D6F56696577204C6566743D
            2232302220546F703D223435342C363435393130393436393239222057696474
            683D2236382C3033313439363036323939323122204865696768743D2231322C
            303934343838313922205265737472696374696F6E733D2232342220416C6C6F
            7745787072657373696F6E733D2246616C73652220466F6E742E436861727365
            743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569676874
            3D222D372220466F6E742E4E616D653D22417269616C2220466F6E742E537479
            6C653D223022204672616D652E436F6C6F723D22313637373732313522204672
            616D652E5479703D223135222046696C6C2E4261636B436F6C6F723D22343231
            30372220476170583D22332220476170593D22332220506172656E74466F6E74
            3D2246616C73652220576F7264577261703D2246616C7365222056416C69676E
            3D22766143656E7465722220546578743D22222F3E3C2F726F776D656D6F733E
            3C726F77746F74616C6D656D6F733E3C546672784D656D6F56696577204C6566
            743D22302220546F703D223834222057696474683D2231303122204865696768
            743D22323222205265737472696374696F6E733D2238222056697369626C653D
            2246616C73652220416C6C6F7745787072657373696F6E733D2246616C736522
            20466F6E742E436861727365743D22312220466F6E742E436F6C6F723D223022
            20466F6E742E4865696768743D222D31332220466F6E742E4E616D653D224172
            69616C2220466F6E742E5374796C653D223122204672616D652E436F6C6F723D
            22313637373732313522204672616D652E5479703D223135222046696C6C2E42
            61636B436F6C6F723D2234323130372220476170583D22332220476170593D22
            33222048416C69676E3D22686143656E7465722220506172656E74466F6E743D
            2246616C7365222056416C69676E3D22766143656E7465722220546578743D22
            4772616E6420546F74616C222F3E3C2F726F77746F74616C6D656D6F733E3C63
            656C6C66756E6374696F6E733E3C6974656D20302F3E3C6974656D20302F3E3C
            2F63656C6C66756E6374696F6E733E3C636F6C756D6E736F72743E3C6974656D
            20322F3E3C6974656D20322F3E3C6974656D20322F3E3C2F636F6C756D6E736F
            72743E3C726F77736F72743E3C6974656D20322F3E3C2F726F77736F72743E3C
            2F63726F73733E}
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 41.574830000000000000
        Width = 1046.929810000000000000
        object Memo1: TfrxMemoView
          Left = 857.953310000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object PMNivel: TPopupMenu
    Left = 768
    Top = 384
    object IncluiNovoPrecodeNivel1: TMenuItem
      Caption = '&Inclui Novo Pre'#231'o de N'#237'vel'
      OnClick = IncluiNovoPrecodeNivel1Click
    end
    object AlteraPreodoItemSelecionado1: TMenuItem
      Caption = '&Altera Pre'#231'o do Item Selecionado'
      OnClick = AlteraPreodoItemSelecionado1Click
    end
    object Excluiitemns1: TMenuItem
      Caption = '&Exclui item(ns)'
      OnClick = Excluiitemns1Click
    end
  end
  object QrTabePrcNiv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT IF(tpn.Nivel=1, gg1.Nome, IF(tpn.Nivel=2, gg2.Nome, IF(tp' +
        'n.Nivel=3, gg3.Nome, pgt.Nome))) '
      'NO_CODNIV, tpn.* '
      'FROM tabeprcniv tpn'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=tpn.CodNiv AND tpn.Nivel=1'
      'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=tpn.CodNiv AND tpn.Nivel=2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=tpn.CodNiv AND tpn.Nivel=3'
      
        'LEFT JOIN prdgruptip pgt ON pgt.Codigo=tpn.CodNiv AND tpn.Nivel=' +
        '4'
      'WHERE tpn.Codigo=1'
      '')
    Left = 80
    Top = 304
    object QrTabePrcNivCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tabeprcniv.Codigo'
    end
    object QrTabePrcNivNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'tabeprcniv.Nivel'
    end
    object QrTabePrcNivCodNiv: TIntegerField
      FieldName = 'CodNiv'
      Origin = 'tabeprcniv.CodNiv'
    end
    object QrTabePrcNivPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'tabeprcniv.Preco'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrTabePrcNivNO_CODNIV: TWideStringField
      FieldName = 'NO_CODNIV'
      Size = 120
    end
  end
  object DsTabePrcNiv: TDataSource
    DataSet = QrTabePrcNiv
    Left = 108
    Top = 304
  end
end
