unit TabePrcNiv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  Variants, dmkRadioGroup, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmTabePrcNiv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    RGNivel: TdmkRadioGroup;
    Panel9: TPanel;
    Label5: TLabel;
    EdCodNiv: TdmkEditCB;
    CBCodNiv: TdmkDBLookupComboBox;
    QrCodNivel: TmySQLQuery;
    DsCodNivel: TDataSource;
    EdPreco: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    EdNO_TAB: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGNivelClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FNivel: Integer;
  public
    { Public declarations }
  end;

  var
  FmTabePrcNiv: TFmTabePrcNiv;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab;

{$R *.DFM}

procedure TFmTabePrcNiv.BtOKClick(Sender: TObject);
var
  Codigo(*, Controle*): Integer;
begin
  //if ImpedePorDuplicidade() then Exit;
  Codigo := FmTabePrcCab.QrTabePrcCabCodigo.Value;
  if UMyMod.ExecSQLInsUpdFm(FmTabePrcNiv, ImgTipo.SQLType, 'TabePrcNiv', Codigo,
  Dmod.QrUpd) then
  begin
    FmTabePrcCab.ReopenTabePrcNiv(RGNivel.ItemIndex, EdCodNiv.ValueVariant);
    if CkContinuar.Checked then
    begin
      Imgtipo.SQLType        := stIns;
      EdCodNiv.ValueVariant  := 0;
      EdPreco.ValueVariant   := 0;
    end else Close;
  end;
end;

procedure TFmTabePrcNiv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTabePrcNiv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdPreco.DecimalSize := Dmod.FFmtPrc;
end;

procedure TFmTabePrcNiv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTabePrcNiv.RGNivelClick(Sender: TObject);
begin
  QrCodNivel.Close;
  //
  case RGNivel.ItemIndex of
    1:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Nivel1 Codigo, Nome  ',
        'FROM gragru1',
        'ORDER BY Nome ',
        '']);
    2:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Nivel2 Codigo, Nome  ',
        'FROM gragru2',
        'ORDER BY Nome ',
        '']);
    3:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Nivel3 Codigo, Nome  ',
        'FROM gragru3',
        'ORDER BY Nome ',
        '']);
    4:  UnDmkDAC_PF.AbreMySQLQuery0(QrCodNivel, Dmod.MyDB, [
        'SELECT Codigo, Nome  ',
        'FROM prdgruptip',
        'ORDER BY Nome ',
        '']);
  end;
  if (FNivel <> RGNivel.ItemIndex) or (RGNivel.ItemIndex = 0) then
  begin
    EdCodNiv.ValueVariant := 0;
    CBCodNiv.KeyValue := Null;
    //
    FNivel := RGNivel.ItemIndex;
  end;
end;

end.
